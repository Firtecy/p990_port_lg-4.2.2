.class public final Landroid/app/ActivityThread;
.super Ljava/lang/Object;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/ActivityThread$DropBoxReporter;,
        Landroid/app/ActivityThread$EventLoggingReporter;,
        Landroid/app/ActivityThread$ProviderRefCount;,
        Landroid/app/ActivityThread$StopInfo;,
        Landroid/app/ActivityThread$ResourcesKey;,
        Landroid/app/ActivityThread$GcIdler;,
        Landroid/app/ActivityThread$Idler;,
        Landroid/app/ActivityThread$H;,
        Landroid/app/ActivityThread$ApplicationThread;,
        Landroid/app/ActivityThread$UpdateCompatibilityData;,
        Landroid/app/ActivityThread$DumpHeapData;,
        Landroid/app/ActivityThread$ProfilerControlData;,
        Landroid/app/ActivityThread$ContextCleanupInfo;,
        Landroid/app/ActivityThread$ResultData;,
        Landroid/app/ActivityThread$DumpComponentInfo;,
        Landroid/app/ActivityThread$Profiler;,
        Landroid/app/ActivityThread$AppBindData;,
        Landroid/app/ActivityThread$ServiceArgsData;,
        Landroid/app/ActivityThread$BindServiceData;,
        Landroid/app/ActivityThread$CreateServiceData;,
        Landroid/app/ActivityThread$CreateBackupAgentData;,
        Landroid/app/ActivityThread$ReceiverData;,
        Landroid/app/ActivityThread$NewIntentData;,
        Landroid/app/ActivityThread$ProviderClientRecord;,
        Landroid/app/ActivityThread$ActivityClientRecord;,
        Landroid/app/ActivityThread$ProviderKey;
    }
.end annotation


# static fields
.field private static final DEBUG_BACKUP:Z = false

.field public static final DEBUG_BROADCAST:Z = false

.field private static final DEBUG_CONFIGURATION:Z = false

.field private static final DEBUG_MEMORY_TRIM:Z = false

.field static final DEBUG_MESSAGES:Z = false

.field private static final DEBUG_PROVIDER:Z = false

.field private static final DEBUG_RESULTS:Z = false

.field private static final DEBUG_SERVICE:Z = false

.field private static final LOG_ON_PAUSE_CALLED:I = 0x7545

.field private static final LOG_ON_RESUME_CALLED:I = 0x7546

.field private static final MIN_TIME_BETWEEN_GCS:J = 0x1388L

.field private static final PATTERN_SEMICOLON:Ljava/util/regex/Pattern; = null

.field private static final SQLITE_MEM_RELEASED_EVENT_LOG_TAG:I = 0x124fb

.field public static final TAG:Ljava/lang/String; = "ActivityThread"

.field private static final THUMBNAIL_FORMAT:Landroid/graphics/Bitmap$Config;

.field static final localLOGV:Z

.field static mSystemContext:Landroid/app/ContextImpl;

.field private static final sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field static sMainThreadHandler:Landroid/os/Handler;

.field static sPackageManager:Landroid/content/pm/IPackageManager;

.field static final sThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/app/ActivityThread;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mActiveResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/app/ActivityThread$ResourcesKey;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/res/Resources;",
            ">;>;"
        }
    .end annotation
.end field

.field final mActivities:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/app/ActivityThread$ActivityClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mAllApplications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field final mAppThread:Landroid/app/ActivityThread$ApplicationThread;

.field private mAvailThumbnailBitmap:Landroid/graphics/Bitmap;

.field final mBackupAgents:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/backup/BackupAgent;",
            ">;"
        }
    .end annotation
.end field

.field mBoundApplication:Landroid/app/ActivityThread$AppBindData;

.field mCompatConfiguration:Landroid/content/res/Configuration;

.field mConfiguration:Landroid/content/res/Configuration;

.field mCoreSettings:Landroid/os/Bundle;

.field mCurDefaultDisplayDpi:I

.field final mDefaultDisplayMetrics:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/res/CompatibilityInfo;",
            "Landroid/util/DisplayMetrics;",
            ">;"
        }
    .end annotation
.end field

.field mDensityCompatMode:Z

.field final mGcIdler:Landroid/app/ActivityThread$GcIdler;

.field mGcIdlerScheduled:Z

.field final mH:Landroid/app/ActivityThread$H;

.field mInitialApplication:Landroid/app/Application;

.field mInstrumentation:Landroid/app/Instrumentation;

.field mInstrumentationAppDir:Ljava/lang/String;

.field mInstrumentationAppLibraryDir:Ljava/lang/String;

.field mInstrumentationAppPackage:Ljava/lang/String;

.field mInstrumentedAppDir:Ljava/lang/String;

.field mInstrumentedAppLibraryDir:Ljava/lang/String;

.field mJitEnabled:Z

.field final mLocalProviders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/app/ActivityThread$ProviderClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mLocalProvidersByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Landroid/app/ActivityThread$ProviderClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mLooper:Landroid/os/Looper;

.field private mMainThreadConfig:Landroid/content/res/Configuration;

.field mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

.field mNumVisibleActivities:I

.field final mOnPauseListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/app/Activity;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/OnActivityPausedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field final mPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/LoadedApk;",
            ">;>;"
        }
    .end annotation
.end field

.field mPendingConfiguration:Landroid/content/res/Configuration;

.field mProfiler:Landroid/app/ActivityThread$Profiler;

.field final mProviderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/app/ActivityThread$ProviderKey;",
            "Landroid/app/ActivityThread$ProviderClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mProviderRefCountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/app/ActivityThread$ProviderRefCount;",
            ">;"
        }
    .end annotation
.end field

.field final mRelaunchingActivities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/ActivityThread$ActivityClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

.field mResConfiguration:Landroid/content/res/Configuration;

.field final mResourcePackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/LoadedApk;",
            ">;>;"
        }
    .end annotation
.end field

.field final mServices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/app/Service;",
            ">;"
        }
    .end annotation
.end field

.field mSystemThread:Z

.field mThemeIconEnabled:Z

.field private mThumbnailCanvas:Landroid/graphics/Canvas;

.field private mThumbnailHeight:I

.field private mThumbnailWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 149
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@2
    sput-object v0, Landroid/app/ActivityThread;->THUMBNAIL_FORMAT:Landroid/graphics/Bitmap$Config;

    #@4
    .line 161
    const-string v0, ";"

    #@6
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@9
    move-result-object v0

    #@a
    sput-object v0, Landroid/app/ActivityThread;->PATTERN_SEMICOLON:Ljava/util/regex/Pattern;

    #@c
    .line 166
    const/4 v0, 0x0

    #@d
    sput-object v0, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@f
    .line 195
    new-instance v0, Ljava/lang/ThreadLocal;

    #@11
    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    #@14
    sput-object v0, Landroid/app/ActivityThread;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@16
    .line 2346
    new-instance v0, Ljava/lang/ThreadLocal;

    #@18
    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    #@1b
    sput-object v0, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@1d
    return-void
.end method

.method constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    .line 1885
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 170
    new-instance v0, Landroid/app/ActivityThread$ApplicationThread;

    #@8
    invoke-direct {v0, p0, v1}, Landroid/app/ActivityThread$ApplicationThread;-><init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V

    #@b
    iput-object v0, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@d
    .line 171
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/app/ActivityThread;->mLooper:Landroid/os/Looper;

    #@13
    .line 172
    new-instance v0, Landroid/app/ActivityThread$H;

    #@15
    invoke-direct {v0, p0, v1}, Landroid/app/ActivityThread$H;-><init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V

    #@18
    iput-object v0, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@1a
    .line 173
    new-instance v0, Ljava/util/HashMap;

    #@1c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@21
    .line 177
    iput-object v1, p0, Landroid/app/ActivityThread;->mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

    #@23
    .line 179
    iput v2, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@25
    .line 180
    new-instance v0, Ljava/util/HashMap;

    #@27
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2a
    iput-object v0, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@2c
    .line 191
    new-instance v0, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v0, p0, Landroid/app/ActivityThread;->mAllApplications:Ljava/util/ArrayList;

    #@33
    .line 194
    new-instance v0, Ljava/util/HashMap;

    #@35
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@38
    iput-object v0, p0, Landroid/app/ActivityThread;->mBackupAgents:Ljava/util/HashMap;

    #@3a
    .line 197
    iput-object v1, p0, Landroid/app/ActivityThread;->mInstrumentationAppDir:Ljava/lang/String;

    #@3c
    .line 198
    iput-object v1, p0, Landroid/app/ActivityThread;->mInstrumentationAppLibraryDir:Ljava/lang/String;

    #@3e
    .line 199
    iput-object v1, p0, Landroid/app/ActivityThread;->mInstrumentationAppPackage:Ljava/lang/String;

    #@40
    .line 200
    iput-object v1, p0, Landroid/app/ActivityThread;->mInstrumentedAppDir:Ljava/lang/String;

    #@42
    .line 201
    iput-object v1, p0, Landroid/app/ActivityThread;->mInstrumentedAppLibraryDir:Ljava/lang/String;

    #@44
    .line 202
    iput-boolean v2, p0, Landroid/app/ActivityThread;->mSystemThread:Z

    #@46
    .line 203
    iput-boolean v2, p0, Landroid/app/ActivityThread;->mJitEnabled:Z

    #@48
    .line 204
    iput-boolean v2, p0, Landroid/app/ActivityThread;->mThemeIconEnabled:Z

    #@4a
    .line 214
    new-instance v0, Ljava/util/HashMap;

    #@4c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4f
    iput-object v0, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@51
    .line 216
    new-instance v0, Ljava/util/HashMap;

    #@53
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@56
    iput-object v0, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@58
    .line 218
    new-instance v0, Ljava/util/HashMap;

    #@5a
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5d
    iput-object v0, p0, Landroid/app/ActivityThread;->mDefaultDisplayMetrics:Ljava/util/HashMap;

    #@5f
    .line 220
    new-instance v0, Ljava/util/HashMap;

    #@61
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@64
    iput-object v0, p0, Landroid/app/ActivityThread;->mActiveResources:Ljava/util/HashMap;

    #@66
    .line 222
    new-instance v0, Ljava/util/ArrayList;

    #@68
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@6b
    iput-object v0, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@6d
    .line 224
    iput-object v1, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@6f
    .line 251
    new-instance v0, Ljava/util/HashMap;

    #@71
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@74
    iput-object v0, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@76
    .line 253
    new-instance v0, Ljava/util/HashMap;

    #@78
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@7b
    iput-object v0, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@7d
    .line 255
    new-instance v0, Ljava/util/HashMap;

    #@7f
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@82
    iput-object v0, p0, Landroid/app/ActivityThread;->mLocalProviders:Ljava/util/HashMap;

    #@84
    .line 257
    new-instance v0, Ljava/util/HashMap;

    #@86
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@89
    iput-object v0, p0, Landroid/app/ActivityThread;->mLocalProvidersByName:Ljava/util/HashMap;

    #@8b
    .line 260
    new-instance v0, Ljava/util/HashMap;

    #@8d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@90
    iput-object v0, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@92
    .line 263
    new-instance v0, Landroid/app/ActivityThread$GcIdler;

    #@94
    invoke-direct {v0, p0}, Landroid/app/ActivityThread$GcIdler;-><init>(Landroid/app/ActivityThread;)V

    #@97
    iput-object v0, p0, Landroid/app/ActivityThread;->mGcIdler:Landroid/app/ActivityThread$GcIdler;

    #@99
    .line 264
    iput-boolean v2, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@9b
    .line 268
    iput-object v1, p0, Landroid/app/ActivityThread;->mCoreSettings:Landroid/os/Bundle;

    #@9d
    .line 1640
    new-instance v0, Landroid/content/res/Configuration;

    #@9f
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@a2
    iput-object v0, p0, Landroid/app/ActivityThread;->mMainThreadConfig:Landroid/content/res/Configuration;

    #@a4
    .line 2909
    iput v3, p0, Landroid/app/ActivityThread;->mThumbnailWidth:I

    #@a6
    .line 2910
    iput v3, p0, Landroid/app/ActivityThread;->mThumbnailHeight:I

    #@a8
    .line 2911
    iput-object v1, p0, Landroid/app/ActivityThread;->mAvailThumbnailBitmap:Landroid/graphics/Bitmap;

    #@aa
    .line 2912
    iput-object v1, p0, Landroid/app/ActivityThread;->mThumbnailCanvas:Landroid/graphics/Canvas;

    #@ac
    .line 1886
    return-void
.end method

.method static synthetic access$1000(Landroid/app/ActivityThread;Landroid/os/IBinder;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/app/ActivityThread;->handleWindowVisibility(Landroid/os/IBinder;Z)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ResultData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleSendResult(Landroid/app/ActivityThread$ResultData;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Landroid/app/ActivityThread;Landroid/os/IBinder;ZIZ)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/ActivityThread;->handleDestroyActivity(Landroid/os/IBinder;ZIZ)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/app/ActivityThread;Landroid/app/ActivityThread$AppBindData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleBindApplication(Landroid/app/ActivityThread$AppBindData;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Landroid/app/ActivityThread;Landroid/app/ActivityThread$NewIntentData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleNewIntent(Landroid/app/ActivityThread$NewIntentData;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ReceiverData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleReceiver(Landroid/app/ActivityThread$ReceiverData;)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateServiceData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleCreateService(Landroid/app/ActivityThread$CreateServiceData;)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Landroid/app/ActivityThread;Landroid/app/ActivityThread$BindServiceData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleBindService(Landroid/app/ActivityThread$BindServiceData;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Landroid/app/ActivityThread;Landroid/app/ActivityThread$BindServiceData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleUnbindService(Landroid/app/ActivityThread$BindServiceData;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ServiceArgsData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleServiceArgs(Landroid/app/ActivityThread$ServiceArgsData;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/app/ActivityThread;ILjava/lang/Object;II)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;II)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Landroid/app/ActivityThread;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleStopService(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Landroid/app/ActivityThread;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleRequestThumbnail(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleDumpService(Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateBackupAgentData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleCreateBackupAgent(Landroid/app/ActivityThread$CreateBackupAgentData;)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Landroid/app/ActivityThread;Landroid/app/ActivityThread$CreateBackupAgentData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleDestroyBackupAgent(Landroid/app/ActivityThread$CreateBackupAgentData;)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleDumpActivity(Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleDumpProvider(Landroid/app/ActivityThread$DumpComponentInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Landroid/app/ActivityThread;Landroid/os/IBinder;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/app/ActivityThread;->handleSleeping(Landroid/os/IBinder;Z)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Landroid/app/ActivityThread;Landroid/os/Bundle;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleSetCoreSettings(Landroid/os/Bundle;)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Landroid/app/ActivityThread;Landroid/app/ActivityThread$UpdateCompatibilityData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleUpdatePackageCompatibilityInfo(Landroid/app/ActivityThread$UpdateCompatibilityData;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/app/ActivityThread;Ljava/io/FileDescriptor;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->dumpGraphicsInfo(Ljava/io/FileDescriptor;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Landroid/app/ActivityThread;->handleLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Landroid/app/ActivityThread;Landroid/app/ActivityThread$ActivityClientRecord;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1}, Landroid/app/ActivityThread;->handleRelaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Landroid/app/ActivityThread;Landroid/os/IBinder;ZZI)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/ActivityThread;->handlePauseActivity(Landroid/os/IBinder;ZZI)V

    #@3
    return-void
.end method

.method static synthetic access$900(Landroid/app/ActivityThread;Landroid/os/IBinder;ZI)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Landroid/app/ActivityThread;->handleStopActivity(Landroid/os/IBinder;ZI)V

    #@3
    return-void
.end method

.method private attach(Z)V
    .registers 9
    .parameter "system"

    #@0
    .prologue
    .line 5021
    sget-object v4, Landroid/app/ActivityThread;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v4, p0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@5
    .line 5022
    iput-boolean p1, p0, Landroid/app/ActivityThread;->mSystemThread:Z

    #@7
    .line 5023
    if-nez p1, :cond_3d

    #@9
    .line 5024
    new-instance v4, Landroid/app/ActivityThread$1;

    #@b
    invoke-direct {v4, p0}, Landroid/app/ActivityThread$1;-><init>(Landroid/app/ActivityThread;)V

    #@e
    invoke-static {v4}, Landroid/view/ViewRootImpl;->addFirstDrawHandler(Ljava/lang/Runnable;)V

    #@11
    .line 5029
    const-string v4, "<pre-initialized>"

    #@13
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@16
    move-result v5

    #@17
    invoke-static {v4, v5}, Landroid/ddm/DdmHandleAppName;->setAppName(Ljava/lang/String;I)V

    #@1a
    .line 5031
    iget-object v4, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@1c
    invoke-virtual {v4}, Landroid/app/ActivityThread$ApplicationThread;->asBinder()Landroid/os/IBinder;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v4}, Lcom/android/internal/os/RuntimeInit;->setApplicationObject(Landroid/os/IBinder;)V

    #@23
    .line 5032
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@26
    move-result-object v3

    #@27
    .line 5034
    .local v3, mgr:Landroid/app/IActivityManager;
    :try_start_27
    iget-object v4, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@29
    invoke-interface {v3, v4}, Landroid/app/IActivityManager;->attachApplication(Landroid/app/IApplicationThread;)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_2c} :catch_8c

    #@2c
    .line 5058
    .end local v3           #mgr:Landroid/app/IActivityManager;
    :goto_2c
    new-instance v4, Landroid/app/ActivityThread$DropBoxReporter;

    #@2e
    invoke-direct {v4, p0}, Landroid/app/ActivityThread$DropBoxReporter;-><init>(Landroid/app/ActivityThread;)V

    #@31
    invoke-static {v4}, Llibcore/io/DropBox;->setReporter(Llibcore/io/DropBox$Reporter;)V

    #@34
    .line 5060
    new-instance v4, Landroid/app/ActivityThread$2;

    #@36
    invoke-direct {v4, p0}, Landroid/app/ActivityThread$2;-><init>(Landroid/app/ActivityThread;)V

    #@39
    invoke-static {v4}, Landroid/view/ViewRootImpl;->addConfigCallback(Landroid/content/ComponentCallbacks;)V

    #@3c
    .line 5083
    return-void

    #@3d
    .line 5041
    :cond_3d
    const-string/jumbo v4, "system_process"

    #@40
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@43
    move-result v5

    #@44
    invoke-static {v4, v5}, Landroid/ddm/DdmHandleAppName;->setAppName(Ljava/lang/String;I)V

    #@47
    .line 5044
    :try_start_47
    new-instance v4, Landroid/app/Instrumentation;

    #@49
    invoke-direct {v4}, Landroid/app/Instrumentation;-><init>()V

    #@4c
    iput-object v4, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@4e
    .line 5045
    new-instance v1, Landroid/app/ContextImpl;

    #@50
    invoke-direct {v1}, Landroid/app/ContextImpl;-><init>()V

    #@53
    .line 5046
    .local v1, context:Landroid/app/ContextImpl;
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    #@56
    move-result-object v4

    #@57
    iget-object v4, v4, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@59
    const/4 v5, 0x0

    #@5a
    invoke-virtual {v1, v4, v5, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@5d
    .line 5047
    const-class v4, Landroid/app/Application;

    #@5f
    invoke-static {v4, v1}, Landroid/app/Instrumentation;->newApplication(Ljava/lang/Class;Landroid/content/Context;)Landroid/app/Application;

    #@62
    move-result-object v0

    #@63
    .line 5048
    .local v0, app:Landroid/app/Application;
    iget-object v4, p0, Landroid/app/ActivityThread;->mAllApplications:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@68
    .line 5049
    iput-object v0, p0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@6a
    .line 5050
    invoke-virtual {v0}, Landroid/app/Application;->onCreate()V
    :try_end_6d
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_6d} :catch_6e

    #@6d
    goto :goto_2c

    #@6e
    .line 5051
    .end local v0           #app:Landroid/app/Application;
    .end local v1           #context:Landroid/app/ContextImpl;
    :catch_6e
    move-exception v2

    #@6f
    .line 5052
    .local v2, e:Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    #@71
    new-instance v5, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v6, "Unable to instantiate Application():"

    #@78
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v5

    #@88
    invoke-direct {v4, v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8b
    throw v4

    #@8c
    .line 5035
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v3       #mgr:Landroid/app/IActivityManager;
    :catch_8c
    move-exception v4

    #@8d
    goto :goto_2c
.end method

.method static final cleanUpPendingRemoveWindows(Landroid/app/ActivityThread$ActivityClientRecord;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2775
    iget-object v1, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@3
    if-eqz v1, :cond_27

    #@5
    .line 2776
    iget-object v1, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindowManager:Landroid/view/WindowManager;

    #@7
    iget-object v2, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@9
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    #@c
    .line 2777
    iget-object v1, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@e
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@11
    move-result-object v0

    #@12
    .line 2778
    .local v0, wtoken:Landroid/os/IBinder;
    if-eqz v0, :cond_27

    #@14
    .line 2779
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@17
    move-result-object v1

    #@18
    iget-object v2, p0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1a
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    const-string v3, "Activity"

    #@24
    invoke-virtual {v1, v0, v2, v3}, Landroid/view/WindowManagerGlobal;->closeAll(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 2783
    .end local v0           #wtoken:Landroid/os/IBinder;
    :cond_27
    iput-object v4, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@29
    .line 2784
    iput-object v4, p0, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindowManager:Landroid/view/WindowManager;

    #@2b
    .line 2785
    return-void
.end method

.method private createBaseContextForActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/Activity;)Landroid/content/Context;
    .registers 14
    .parameter "r"
    .parameter "activity"

    #@0
    .prologue
    .line 2213
    new-instance v0, Landroid/app/ContextImpl;

    #@2
    invoke-direct {v0}, Landroid/app/ContextImpl;-><init>()V

    #@5
    .line 2214
    .local v0, appContext:Landroid/app/ContextImpl;
    iget-object v9, p1, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@7
    iget-object v10, p1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@9
    invoke-virtual {v0, v9, v10, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@c
    .line 2215
    invoke-virtual {v0, p2}, Landroid/app/ContextImpl;->setOuterContext(Landroid/content/Context;)V

    #@f
    .line 2220
    move-object v2, v0

    #@10
    .line 2221
    .local v2, baseContext:Landroid/content/Context;
    const-string v9, "debug.second-display.pkg"

    #@12
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v8

    #@16
    .line 2222
    .local v8, pkgName:Ljava/lang/String;
    if-eqz v8, :cond_40

    #@18
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    #@1b
    move-result v9

    #@1c
    if-nez v9, :cond_40

    #@1e
    iget-object v9, p1, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@20
    iget-object v9, v9, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@22
    invoke-virtual {v9, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@25
    move-result v9

    #@26
    if-eqz v9, :cond_40

    #@28
    .line 2224
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@2b
    move-result-object v5

    #@2c
    .line 2225
    .local v5, dm:Landroid/hardware/display/DisplayManagerGlobal;
    invoke-virtual {v5}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayIds()[I

    #@2f
    move-result-object v1

    #@30
    .local v1, arr$:[I
    array-length v7, v1

    #@31
    .local v7, len$:I
    const/4 v6, 0x0

    #@32
    .local v6, i$:I
    :goto_32
    if-ge v6, v7, :cond_40

    #@34
    aget v4, v1, v6

    #@36
    .line 2226
    .local v4, displayId:I
    if-eqz v4, :cond_41

    #@38
    .line 2227
    invoke-virtual {v5, v4}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    #@3b
    move-result-object v3

    #@3c
    .line 2228
    .local v3, display:Landroid/view/Display;
    invoke-virtual {v0, v3}, Landroid/app/ContextImpl;->createDisplayContext(Landroid/view/Display;)Landroid/content/Context;

    #@3f
    move-result-object v2

    #@40
    .line 2233
    .end local v1           #arr$:[I
    .end local v3           #display:Landroid/view/Display;
    .end local v4           #displayId:I
    .end local v5           #dm:Landroid/hardware/display/DisplayManagerGlobal;
    .end local v6           #i$:I
    .end local v7           #len$:I
    :cond_40
    return-object v2

    #@41
    .line 2225
    .restart local v1       #arr$:[I
    .restart local v4       #displayId:I
    .restart local v5       #dm:Landroid/hardware/display/DisplayManagerGlobal;
    .restart local v6       #i$:I
    .restart local v7       #len$:I
    :cond_41
    add-int/lit8 v6, v6, 0x1

    #@43
    goto :goto_32
.end method

.method private createThumbnailBitmap(Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/graphics/Bitmap;
    .registers 11
    .parameter "r"

    #@0
    .prologue
    .line 2915
    iget-object v4, p0, Landroid/app/ActivityThread;->mAvailThumbnailBitmap:Landroid/graphics/Bitmap;

    #@2
    .line 2917
    .local v4, thumbnail:Landroid/graphics/Bitmap;
    if-nez v4, :cond_38

    #@4
    .line 2918
    :try_start_4
    iget v5, p0, Landroid/app/ActivityThread;->mThumbnailWidth:I

    #@6
    .line 2920
    .local v5, w:I
    if-gez v5, :cond_58

    #@8
    .line 2921
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@a
    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v3

    #@e
    .line 2922
    .local v3, res:Landroid/content/res/Resources;
    const v6, 0x1050001

    #@11
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@14
    move-result v2

    #@15
    .local v2, h:I
    iput v2, p0, Landroid/app/ActivityThread;->mThumbnailHeight:I

    #@17
    .line 2925
    const v6, 0x1050002

    #@1a
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@1d
    move-result v5

    #@1e
    iput v5, p0, Landroid/app/ActivityThread;->mThumbnailWidth:I

    #@20
    .line 2932
    .end local v3           #res:Landroid/content/res/Resources;
    :goto_20
    if-lez v5, :cond_38

    #@22
    if-lez v2, :cond_38

    #@24
    .line 2933
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@26
    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@2d
    move-result-object v6

    #@2e
    sget-object v7, Landroid/app/ActivityThread;->THUMBNAIL_FORMAT:Landroid/graphics/Bitmap$Config;

    #@30
    invoke-static {v6, v5, v2, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@33
    move-result-object v4

    #@34
    .line 2935
    const/4 v6, 0x0

    #@35
    invoke-virtual {v4, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    #@38
    .line 2939
    .end local v2           #h:I
    .end local v5           #w:I
    :cond_38
    if-eqz v4, :cond_57

    #@3a
    .line 2940
    iget-object v0, p0, Landroid/app/ActivityThread;->mThumbnailCanvas:Landroid/graphics/Canvas;

    #@3c
    .line 2941
    .local v0, cv:Landroid/graphics/Canvas;
    if-nez v0, :cond_45

    #@3e
    .line 2942
    new-instance v0, Landroid/graphics/Canvas;

    #@40
    .end local v0           #cv:Landroid/graphics/Canvas;
    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    #@43
    .restart local v0       #cv:Landroid/graphics/Canvas;
    iput-object v0, p0, Landroid/app/ActivityThread;->mThumbnailCanvas:Landroid/graphics/Canvas;

    #@45
    .line 2945
    :cond_45
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@48
    .line 2946
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@4a
    invoke-virtual {v6, v4, v0}, Landroid/app/Activity;->onCreateThumbnail(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;)Z

    #@4d
    move-result v6

    #@4e
    if-nez v6, :cond_53

    #@50
    .line 2947
    iput-object v4, p0, Landroid/app/ActivityThread;->mAvailThumbnailBitmap:Landroid/graphics/Bitmap;

    #@52
    .line 2948
    const/4 v4, 0x0

    #@53
    .line 2950
    :cond_53
    const/4 v6, 0x0

    #@54
    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@57
    .line 2963
    .end local v0           #cv:Landroid/graphics/Canvas;
    :cond_57
    :goto_57
    return-object v4

    #@58
    .line 2928
    .restart local v5       #w:I
    :cond_58
    iget v2, p0, Landroid/app/ActivityThread;->mThumbnailHeight:I
    :try_end_5a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_5a} :catch_5b

    #@5a
    .restart local v2       #h:I
    goto :goto_20

    #@5b
    .line 2953
    .end local v2           #h:I
    .end local v5           #w:I
    :catch_5b
    move-exception v1

    #@5c
    .line 2954
    .local v1, e:Ljava/lang/Exception;
    iget-object v6, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@5e
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@60
    invoke-virtual {v6, v7, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@63
    move-result v6

    #@64
    if-nez v6, :cond_97

    #@66
    .line 2955
    new-instance v6, Ljava/lang/RuntimeException;

    #@68
    new-instance v7, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v8, "Unable to create thumbnail of "

    #@6f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v7

    #@73
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@75
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@78
    move-result-object v8

    #@79
    invoke-virtual {v8}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v7

    #@81
    const-string v8, ": "

    #@83
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v7

    #@87
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v7

    #@8f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v7

    #@93
    invoke-direct {v6, v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@96
    throw v6

    #@97
    .line 2960
    :cond_97
    const/4 v4, 0x0

    #@98
    goto :goto_57
.end method

.method public static currentActivityThread()Landroid/app/ActivityThread;
    .registers 1

    #@0
    .prologue
    .line 1573
    sget-object v0, Landroid/app/ActivityThread;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread;

    #@8
    return-object v0
.end method

.method public static currentApplication()Landroid/app/Application;
    .registers 2

    #@0
    .prologue
    .line 1583
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@3
    move-result-object v0

    #@4
    .line 1584
    .local v0, am:Landroid/app/ActivityThread;
    if-eqz v0, :cond_9

    #@6
    iget-object v1, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@8
    :goto_8
    return-object v1

    #@9
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_8
.end method

.method public static currentPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1577
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@3
    move-result-object v0

    #@4
    .line 1578
    .local v0, am:Landroid/app/ActivityThread;
    if-eqz v0, :cond_f

    #@6
    iget-object v1, v0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@8
    if-eqz v1, :cond_f

    #@a
    iget-object v1, v0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@c
    iget-object v1, v1, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method private deliverNewIntents(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V
    .registers 8
    .parameter "r"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/ActivityThread$ActivityClientRecord;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2316
    .local p2, intents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    .line 2317
    .local v0, N:I
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    if-ge v1, v0, :cond_27

    #@7
    .line 2318
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v2

    #@b
    check-cast v2, Landroid/content/Intent;

    #@d
    .line 2319
    .local v2, intent:Landroid/content/Intent;
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@f
    invoke-virtual {v3}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@16
    .line 2320
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@18
    iget-object v3, v3, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@1a
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@1d
    .line 2321
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@1f
    iget-object v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@21
    invoke-virtual {v3, v4, v2}, Landroid/app/Instrumentation;->callActivityOnNewIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    #@24
    .line 2317
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_5

    #@27
    .line 2323
    .end local v2           #intent:Landroid/content/Intent;
    :cond_27
    return-void
.end method

.method private deliverResults(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V
    .registers 12
    .parameter "r"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/ActivityThread$ActivityClientRecord;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ResultInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3338
    .local p2, results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    .line 3339
    .local v0, N:I
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    if-ge v2, v0, :cond_72

    #@7
    .line 3340
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@a
    move-result-object v3

    #@b
    check-cast v3, Landroid/app/ResultInfo;

    #@d
    .line 3342
    .local v3, ri:Landroid/app/ResultInfo;
    :try_start_d
    iget-object v4, v3, Landroid/app/ResultInfo;->mData:Landroid/content/Intent;

    #@f
    if-eqz v4, :cond_1c

    #@11
    .line 3343
    iget-object v4, v3, Landroid/app/ResultInfo;->mData:Landroid/content/Intent;

    #@13
    iget-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@15
    invoke-virtual {v5}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@1c
    .line 3347
    :cond_1c
    iget-object v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1e
    iget-object v5, v3, Landroid/app/ResultInfo;->mResultWho:Ljava/lang/String;

    #@20
    iget v6, v3, Landroid/app/ResultInfo;->mRequestCode:I

    #@22
    iget v7, v3, Landroid/app/ResultInfo;->mResultCode:I

    #@24
    iget-object v8, v3, Landroid/app/ResultInfo;->mData:Landroid/content/Intent;

    #@26
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/app/Activity;->dispatchActivityResult(Ljava/lang/String;IILandroid/content/Intent;)V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_29} :catch_2c

    #@29
    .line 3339
    :cond_29
    add-int/lit8 v2, v2, 0x1

    #@2b
    goto :goto_5

    #@2c
    .line 3349
    :catch_2c
    move-exception v1

    #@2d
    .line 3350
    .local v1, e:Ljava/lang/Exception;
    iget-object v4, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@2f
    iget-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@31
    invoke-virtual {v4, v5, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@34
    move-result v4

    #@35
    if-nez v4, :cond_29

    #@37
    .line 3351
    new-instance v4, Ljava/lang/RuntimeException;

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "Failure delivering result "

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    const-string v6, " to activity "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@50
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    const-string v6, ": "

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@71
    throw v4

    #@72
    .line 3358
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #ri:Landroid/app/ResultInfo;
    :cond_72
    return-void
.end method

.method private native dumpGraphicsInfo(Ljava/io/FileDescriptor;)V
.end method

.method private flushDisplayMetricsLocked()V
    .registers 2

    #@0
    .prologue
    .line 1600
    iget-object v0, p0, Landroid/app/ActivityThread;->mDefaultDisplayMetrics:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 1601
    return-void
.end method

.method public static getIntentBeingBroadcast()Landroid/content/Intent;
    .registers 1

    #@0
    .prologue
    .line 2354
    sget-object v0, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/content/Intent;

    #@8
    return-object v0
.end method

.method private getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZ)Landroid/app/LoadedApk;
    .registers 16
    .parameter "aInfo"
    .parameter "compatInfo"
    .parameter "baseLoader"
    .parameter "securityViolation"
    .parameter "includeCode"

    #@0
    .prologue
    .line 1854
    iget-object v9, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@2
    monitor-enter v9

    #@3
    .line 1856
    if-eqz p5, :cond_4e

    #@5
    .line 1857
    :try_start_5
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@7
    iget-object v2, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v8

    #@d
    check-cast v8, Ljava/lang/ref/WeakReference;

    #@f
    .line 1861
    .local v8, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :goto_f
    if-eqz v8, :cond_59

    #@11
    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/app/LoadedApk;

    #@17
    move-object v0, v1

    #@18
    .line 1862
    .local v0, packageInfo:Landroid/app/LoadedApk;
    :goto_18
    if-eqz v0, :cond_2a

    #@1a
    iget-object v1, v0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@1c
    if-eqz v1, :cond_4c

    #@1e
    iget-object v1, v0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@20
    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1}, Landroid/content/res/AssetManager;->isUpToDate()Z

    #@27
    move-result v1

    #@28
    if-nez v1, :cond_4c

    #@2a
    .line 1869
    :cond_2a
    new-instance v0, Landroid/app/LoadedApk;

    #@2c
    .end local v0           #packageInfo:Landroid/app/LoadedApk;
    if-eqz p5, :cond_5b

    #@2e
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    #@30
    and-int/lit8 v1, v1, 0x4

    #@32
    if-eqz v1, :cond_5b

    #@34
    const/4 v7, 0x1

    #@35
    :goto_35
    move-object v1, p0

    #@36
    move-object v2, p1

    #@37
    move-object v3, p2

    #@38
    move-object v4, p0

    #@39
    move-object v5, p3

    #@3a
    move v6, p4

    #@3b
    invoke-direct/range {v0 .. v7}, Landroid/app/LoadedApk;-><init>(Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Landroid/app/ActivityThread;Ljava/lang/ClassLoader;ZZ)V

    #@3e
    .line 1873
    .restart local v0       #packageInfo:Landroid/app/LoadedApk;
    if-eqz p5, :cond_5d

    #@40
    .line 1874
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@42
    iget-object v2, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@44
    new-instance v3, Ljava/lang/ref/WeakReference;

    #@46
    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@49
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 1881
    :cond_4c
    :goto_4c
    monitor-exit v9

    #@4d
    return-object v0

    #@4e
    .line 1859
    .end local v0           #packageInfo:Landroid/app/LoadedApk;
    .end local v8           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :cond_4e
    iget-object v1, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@50
    iget-object v2, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@52
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    move-result-object v8

    #@56
    check-cast v8, Ljava/lang/ref/WeakReference;

    #@58
    .restart local v8       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    goto :goto_f

    #@59
    .line 1861
    :cond_59
    const/4 v0, 0x0

    #@5a
    goto :goto_18

    #@5b
    .line 1869
    :cond_5b
    const/4 v7, 0x0

    #@5c
    goto :goto_35

    #@5d
    .line 1877
    .restart local v0       #packageInfo:Landroid/app/LoadedApk;
    :cond_5d
    iget-object v1, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@5f
    iget-object v2, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@61
    new-instance v3, Ljava/lang/ref/WeakReference;

    #@63
    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@66
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    goto :goto_4c

    #@6a
    .line 1882
    .end local v0           #packageInfo:Landroid/app/LoadedApk;
    .end local v8           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :catchall_6a
    move-exception v1

    #@6b
    monitor-exit v9
    :try_end_6c
    .catchall {:try_start_5 .. :try_end_6c} :catchall_6a

    #@6c
    throw v1
.end method

.method public static getPackageManager()Landroid/content/pm/IPackageManager;
    .registers 2

    #@0
    .prologue
    .line 1588
    sget-object v1, Landroid/app/ActivityThread;->sPackageManager:Landroid/content/pm/IPackageManager;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 1590
    sget-object v1, Landroid/app/ActivityThread;->sPackageManager:Landroid/content/pm/IPackageManager;

    #@6
    .line 1596
    .local v0, b:Landroid/os/IBinder;
    :goto_6
    return-object v1

    #@7
    .line 1592
    .end local v0           #b:Landroid/os/IBinder;
    :cond_7
    const-string/jumbo v1, "package"

    #@a
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@d
    move-result-object v0

    #@e
    .line 1594
    .restart local v0       #b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    #@11
    move-result-object v1

    #@12
    sput-object v1, Landroid/app/ActivityThread;->sPackageManager:Landroid/content/pm/IPackageManager;

    #@14
    .line 1596
    sget-object v1, Landroid/app/ActivityThread;->sPackageManager:Landroid/content/pm/IPackageManager;

    #@16
    goto :goto_6
.end method

.method private handleBindApplication(Landroid/app/ActivityThread$AppBindData;)V
    .registers 30
    .parameter "data"

    #@0
    .prologue
    .line 4202
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p0

    #@4
    iput-object v0, v1, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@6
    .line 4203
    new-instance v2, Landroid/content/res/Configuration;

    #@8
    move-object/from16 v0, p1

    #@a
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@c
    invoke-direct {v2, v4}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@f
    move-object/from16 v0, p0

    #@11
    iput-object v2, v0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@13
    .line 4204
    new-instance v2, Landroid/content/res/Configuration;

    #@15
    move-object/from16 v0, p1

    #@17
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@19
    invoke-direct {v2, v4}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@1c
    move-object/from16 v0, p0

    #@1e
    iput-object v2, v0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@20
    .line 4206
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_THEMEICON:Z

    #@22
    if-eqz v2, :cond_37

    #@24
    .line 4207
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@27
    move-result-object v2

    #@28
    move-object/from16 v0, p1

    #@2a
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2c
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-static {v2, v4, v5}, Landroid/content/thm/ThemeIconManager;->isUseThemeIcon(Landroid/content/pm/IPackageManager;Ljava/lang/String;Z)Z

    #@32
    move-result v2

    #@33
    move-object/from16 v0, p0

    #@35
    iput-boolean v2, v0, Landroid/app/ActivityThread;->mThemeIconEnabled:Z

    #@37
    .line 4211
    :cond_37
    new-instance v2, Landroid/app/ActivityThread$Profiler;

    #@39
    invoke-direct {v2}, Landroid/app/ActivityThread$Profiler;-><init>()V

    #@3c
    move-object/from16 v0, p0

    #@3e
    iput-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@40
    .line 4212
    move-object/from16 v0, p0

    #@42
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@44
    move-object/from16 v0, p1

    #@46
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->initProfileFile:Ljava/lang/String;

    #@48
    iput-object v4, v2, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@4a
    .line 4213
    move-object/from16 v0, p0

    #@4c
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@4e
    move-object/from16 v0, p1

    #@50
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->initProfileFd:Landroid/os/ParcelFileDescriptor;

    #@52
    iput-object v4, v2, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@54
    .line 4214
    move-object/from16 v0, p0

    #@56
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@58
    move-object/from16 v0, p1

    #@5a
    iget-boolean v4, v0, Landroid/app/ActivityThread$AppBindData;->initAutoStopProfiler:Z

    #@5c
    iput-boolean v4, v2, Landroid/app/ActivityThread$Profiler;->autoStopProfiler:Z

    #@5e
    .line 4217
    move-object/from16 v0, p1

    #@60
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@62
    invoke-static {v2}, Landroid/os/Process;->setArgV0(Ljava/lang/String;)V

    #@65
    .line 4219
    const-string v2, "dalvik.vm.heaputilization"

    #@67
    const-string v4, ""

    #@69
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6c
    move-result-object v27

    #@6d
    .line 4220
    .local v27, str:Ljava/lang/String;
    const-string v2, ""

    #@6f
    move-object/from16 v0, v27

    #@71
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v2

    #@75
    if-nez v2, :cond_a7

    #@77
    .line 4221
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    #@82
    move-result v19

    #@83
    .line 4222
    .local v19, heapUtil:F
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@86
    move-result-object v2

    #@87
    move/from16 v0, v19

    #@89
    invoke-virtual {v2, v0}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    #@8c
    .line 4223
    const-string v2, "ActivityThread"

    #@8e
    new-instance v4, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string/jumbo v5, "setTargetHeapUtilization:"

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    move-object/from16 v0, v27

    #@9c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v4

    #@a0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v4

    #@a4
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 4225
    .end local v19           #heapUtil:F
    :cond_a7
    const-string v2, "dalvik.vm.heapMinFree"

    #@a9
    const/4 v4, 0x0

    #@aa
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@ad
    move-result v18

    #@ae
    .line 4226
    .local v18, heapMinFree:I
    if-lez v18, :cond_d4

    #@b0
    .line 4227
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@b3
    move-result-object v2

    #@b4
    move/from16 v0, v18

    #@b6
    invoke-virtual {v2, v0}, Ldalvik/system/VMRuntime;->setTargetHeapMinFree(I)F

    #@b9
    .line 4228
    const-string v2, "ActivityThread"

    #@bb
    new-instance v4, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string/jumbo v5, "setTargetHeapMinFree:"

    #@c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v4

    #@c7
    move/from16 v0, v18

    #@c9
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v4

    #@cd
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v4

    #@d1
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 4230
    :cond_d4
    const-string v2, "dalvik.vm.heapconcurrentstart"

    #@d6
    const/4 v4, 0x0

    #@d7
    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@da
    move-result v17

    #@db
    .line 4231
    .local v17, heapConcurrentStart:I
    if-lez v17, :cond_101

    #@dd
    .line 4232
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@e0
    move-result-object v2

    #@e1
    move/from16 v0, v17

    #@e3
    invoke-virtual {v2, v0}, Ldalvik/system/VMRuntime;->setTargetHeapConcurrentStart(I)F

    #@e6
    .line 4233
    const-string v2, "ActivityThread"

    #@e8
    new-instance v4, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string/jumbo v5, "setTargetHeapConcurrentStart:"

    #@f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v4

    #@f4
    move/from16 v0, v17

    #@f6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v4

    #@fa
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v4

    #@fe
    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 4247
    :cond_101
    move-object/from16 v0, p1

    #@103
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@105
    const-string v4, "com.android.gallery3d"

    #@107
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10a
    move-result v2

    #@10b
    if-eqz v2, :cond_116

    #@10d
    .line 4248
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@110
    move-result-object v2

    #@111
    const/high16 v4, 0x80

    #@113
    invoke-virtual {v2, v4}, Ldalvik/system/VMRuntime;->setTargetHeapMinFree(I)F

    #@116
    .line 4252
    :cond_116
    move-object/from16 v0, p1

    #@118
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@11a
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@11d
    move-result v4

    #@11e
    invoke-static {v2, v4}, Landroid/ddm/DdmHandleAppName;->setAppName(Ljava/lang/String;I)V

    #@121
    .line 4255
    move-object/from16 v0, p1

    #@123
    iget-boolean v2, v0, Landroid/app/ActivityThread$AppBindData;->persistent:Z

    #@125
    if-eqz v2, :cond_131

    #@127
    .line 4259
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@12a
    move-result v2

    #@12b
    if-nez v2, :cond_131

    #@12d
    .line 4260
    const/4 v2, 0x0

    #@12e
    invoke-static {v2}, Landroid/view/HardwareRenderer;->disable(Z)V

    #@131
    .line 4264
    :cond_131
    move-object/from16 v0, p0

    #@133
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@135
    iget-object v2, v2, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@137
    if-eqz v2, :cond_140

    #@139
    .line 4265
    move-object/from16 v0, p0

    #@13b
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@13d
    invoke-virtual {v2}, Landroid/app/ActivityThread$Profiler;->startProfiling()V

    #@140
    .line 4272
    :cond_140
    move-object/from16 v0, p1

    #@142
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@144
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@146
    const/16 v4, 0xc

    #@148
    if-gt v2, v4, :cond_14f

    #@14a
    .line 4273
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@14c
    invoke-static {v2}, Landroid/os/AsyncTask;->setDefaultExecutor(Ljava/util/concurrent/Executor;)V

    #@14f
    .line 4282
    :cond_14f
    const/4 v2, 0x0

    #@150
    invoke-static {v2}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    #@153
    .line 4287
    move-object/from16 v0, p1

    #@155
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@157
    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@159
    invoke-static {v2}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    #@15c
    .line 4290
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@15e
    if-eqz v2, :cond_163

    #@160
    .line 4291
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityThread;->updateFontConfigurationLocked()V

    #@163
    .line 4300
    :cond_163
    move-object/from16 v0, p1

    #@165
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@167
    move-object/from16 v0, p1

    #@169
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@16b
    move-object/from16 v0, p0

    #@16d
    invoke-virtual {v0, v2, v4}, Landroid/app/ActivityThread;->applyConfigurationToResourcesLocked(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z

    #@170
    .line 4301
    move-object/from16 v0, p1

    #@172
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@174
    iget v2, v2, Landroid/content/res/Configuration;->densityDpi:I

    #@176
    move-object/from16 v0, p0

    #@178
    iput v2, v0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@17a
    .line 4302
    move-object/from16 v0, p0

    #@17c
    iget v2, v0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@17e
    move-object/from16 v0, p0

    #@180
    invoke-virtual {v0, v2}, Landroid/app/ActivityThread;->applyCompatConfiguration(I)Landroid/content/res/Configuration;

    #@183
    .line 4304
    move-object/from16 v0, p1

    #@185
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@187
    move-object/from16 v0, p1

    #@189
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@18b
    move-object/from16 v0, p0

    #@18d
    invoke-virtual {v0, v2, v4}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@190
    move-result-object v2

    #@191
    move-object/from16 v0, p1

    #@193
    iput-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@195
    .line 4309
    move-object/from16 v0, p1

    #@197
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@199
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@19b
    and-int/lit16 v2, v2, 0x2000

    #@19d
    if-nez v2, :cond_1a9

    #@19f
    .line 4311
    const/4 v2, 0x1

    #@1a0
    move-object/from16 v0, p0

    #@1a2
    iput-boolean v2, v0, Landroid/app/ActivityThread;->mDensityCompatMode:Z

    #@1a4
    .line 4312
    const/16 v2, 0xa0

    #@1a6
    invoke-static {v2}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    #@1a9
    .line 4314
    :cond_1a9
    invoke-direct/range {p0 .. p0}, Landroid/app/ActivityThread;->updateDefaultDensity()V

    #@1ac
    .line 4316
    new-instance v11, Landroid/app/ContextImpl;

    #@1ae
    invoke-direct {v11}, Landroid/app/ContextImpl;-><init>()V

    #@1b1
    .line 4317
    .local v11, appContext:Landroid/app/ContextImpl;
    move-object/from16 v0, p1

    #@1b3
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@1b5
    const/4 v4, 0x0

    #@1b6
    move-object/from16 v0, p0

    #@1b8
    invoke-virtual {v11, v2, v4, v0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@1bb
    .line 4318
    invoke-static {}, Landroid/os/Process;->isIsolated()Z

    #@1be
    move-result v2

    #@1bf
    if-nez v2, :cond_1da

    #@1c1
    .line 4319
    invoke-virtual {v11}, Landroid/app/ContextImpl;->getCacheDir()Ljava/io/File;

    #@1c4
    move-result-object v13

    #@1c5
    .line 4321
    .local v13, cacheDir:Ljava/io/File;
    if-eqz v13, :cond_2a6

    #@1c7
    .line 4323
    const-string/jumbo v2, "java.io.tmpdir"

    #@1ca
    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@1cd
    move-result-object v4

    #@1ce
    invoke-static {v2, v4}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1d1
    .line 4325
    move-object/from16 v0, p1

    #@1d3
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@1d5
    move-object/from16 v0, p0

    #@1d7
    invoke-direct {v0, v2, v13}, Landroid/app/ActivityThread;->setupGraphicsSupport(Landroid/app/LoadedApk;Ljava/io/File;)V

    #@1da
    .line 4334
    .end local v13           #cacheDir:Ljava/io/File;
    :cond_1da
    :goto_1da
    move-object/from16 v0, p1

    #@1dc
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1de
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1e0
    and-int/lit16 v2, v2, 0x81

    #@1e2
    if-eqz v2, :cond_1e7

    #@1e4
    .line 4337
    invoke-static {}, Landroid/os/StrictMode;->conditionallyEnableDebugLogging()Z

    #@1e7
    .line 4347
    :cond_1e7
    move-object/from16 v0, p1

    #@1e9
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1eb
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@1ed
    const/16 v4, 0x9

    #@1ef
    if-le v2, v4, :cond_1f4

    #@1f1
    .line 4348
    invoke-static {}, Landroid/os/StrictMode;->enableDeathOnNetwork()V

    #@1f4
    .line 4354
    :cond_1f4
    move-object/from16 v0, p1

    #@1f6
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1f8
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@1fa
    and-int/lit16 v2, v2, 0x2000

    #@1fc
    if-nez v2, :cond_2af

    #@1fe
    .line 4356
    const/16 v2, 0xa0

    #@200
    invoke-static {v2}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    #@203
    .line 4368
    :cond_203
    :goto_203
    move-object/from16 v0, p1

    #@205
    iget v2, v0, Landroid/app/ActivityThread$AppBindData;->debugMode:I

    #@207
    if-eqz v2, :cond_256

    #@209
    .line 4370
    const/16 v2, 0x1fa4

    #@20b
    invoke-static {v2}, Landroid/os/Debug;->changeDebugPort(I)V

    #@20e
    .line 4371
    move-object/from16 v0, p1

    #@210
    iget v2, v0, Landroid/app/ActivityThread$AppBindData;->debugMode:I

    #@212
    const/4 v4, 0x2

    #@213
    if-ne v2, v4, :cond_2d1

    #@215
    .line 4372
    const-string v2, "ActivityThread"

    #@217
    new-instance v4, Ljava/lang/StringBuilder;

    #@219
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@21c
    const-string v5, "Application "

    #@21e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v4

    #@222
    move-object/from16 v0, p1

    #@224
    iget-object v5, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@226
    invoke-virtual {v5}, Landroid/app/LoadedApk;->getPackageName()Ljava/lang/String;

    #@229
    move-result-object v5

    #@22a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v4

    #@22e
    const-string v5, " is waiting for the debugger on port 8100..."

    #@230
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v4

    #@234
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v4

    #@238
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    .line 4375
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@23e
    move-result-object v21

    #@23f
    .line 4377
    .local v21, mgr:Landroid/app/IActivityManager;
    :try_start_23f
    move-object/from16 v0, p0

    #@241
    iget-object v2, v0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@243
    const/4 v4, 0x1

    #@244
    move-object/from16 v0, v21

    #@246
    invoke-interface {v0, v2, v4}, Landroid/app/IActivityManager;->showWaitingForDebugger(Landroid/app/IApplicationThread;Z)V
    :try_end_249
    .catch Landroid/os/RemoteException; {:try_start_23f .. :try_end_249} :catch_4e0

    #@249
    .line 4381
    :goto_249
    invoke-static {}, Landroid/os/Debug;->waitForDebugger()V

    #@24c
    .line 4384
    :try_start_24c
    move-object/from16 v0, p0

    #@24e
    iget-object v2, v0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@250
    const/4 v4, 0x0

    #@251
    move-object/from16 v0, v21

    #@253
    invoke-interface {v0, v2, v4}, Landroid/app/IActivityManager;->showWaitingForDebugger(Landroid/app/IApplicationThread;Z)V
    :try_end_256
    .catch Landroid/os/RemoteException; {:try_start_24c .. :try_end_256} :catch_4dd

    #@256
    .line 4395
    .end local v21           #mgr:Landroid/app/IActivityManager;
    :cond_256
    :goto_256
    move-object/from16 v0, p1

    #@258
    iget-boolean v2, v0, Landroid/app/ActivityThread$AppBindData;->enableOpenGlTrace:Z

    #@25a
    if-eqz v2, :cond_25f

    #@25c
    .line 4396
    invoke-static {}, Landroid/opengl/GLUtils;->enableTracing()V

    #@25f
    .line 4402
    :cond_25f
    const-string v2, "connectivity"

    #@261
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@264
    move-result-object v12

    #@265
    .line 4403
    .local v12, b:Landroid/os/IBinder;
    if-eqz v12, :cond_272

    #@267
    .line 4407
    invoke-static {v12}, Landroid/net/IConnectivityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/IConnectivityManager;

    #@26a
    move-result-object v26

    #@26b
    .line 4409
    .local v26, service:Landroid/net/IConnectivityManager;
    :try_start_26b
    invoke-interface/range {v26 .. v26}, Landroid/net/IConnectivityManager;->getProxy()Landroid/net/ProxyProperties;

    #@26e
    move-result-object v24

    #@26f
    .line 4410
    .local v24, proxyProperties:Landroid/net/ProxyProperties;
    invoke-static/range {v24 .. v24}, Landroid/net/Proxy;->setHttpProxySystemProperty(Landroid/net/ProxyProperties;)V
    :try_end_272
    .catch Landroid/os/RemoteException; {:try_start_26b .. :try_end_272} :catch_4da

    #@272
    .line 4414
    .end local v24           #proxyProperties:Landroid/net/ProxyProperties;
    .end local v26           #service:Landroid/net/IConnectivityManager;
    :cond_272
    :goto_272
    move-object/from16 v0, p1

    #@274
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@276
    if-eqz v2, :cond_461

    #@278
    .line 4415
    const/16 v20, 0x0

    #@27a
    .line 4417
    .local v20, ii:Landroid/content/pm/InstrumentationInfo;
    :try_start_27a
    invoke-virtual {v11}, Landroid/app/ContextImpl;->getPackageManager()Landroid/content/pm/PackageManager;

    #@27d
    move-result-object v2

    #@27e
    move-object/from16 v0, p1

    #@280
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@282
    const/4 v5, 0x0

    #@283
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getInstrumentationInfo(Landroid/content/ComponentName;I)Landroid/content/pm/InstrumentationInfo;
    :try_end_286
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_27a .. :try_end_286} :catch_4d7

    #@286
    move-result-object v20

    #@287
    .line 4421
    :goto_287
    if-nez v20, :cond_2f9

    #@289
    .line 4422
    new-instance v2, Ljava/lang/RuntimeException;

    #@28b
    new-instance v4, Ljava/lang/StringBuilder;

    #@28d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@290
    const-string v5, "Unable to find instrumentation info for: "

    #@292
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@295
    move-result-object v4

    #@296
    move-object/from16 v0, p1

    #@298
    iget-object v5, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@29a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29d
    move-result-object v4

    #@29e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a1
    move-result-object v4

    #@2a2
    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2a5
    throw v2

    #@2a6
    .line 4327
    .end local v12           #b:Landroid/os/IBinder;
    .end local v20           #ii:Landroid/content/pm/InstrumentationInfo;
    .restart local v13       #cacheDir:Ljava/io/File;
    :cond_2a6
    const-string v2, "ActivityThread"

    #@2a8
    const-string v4, "Unable to setupGraphicsSupport due to missing cache directory"

    #@2aa
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2ad
    goto/16 :goto_1da

    #@2af
    .line 4360
    .end local v13           #cacheDir:Ljava/io/File;
    :cond_2af
    move-object/from16 v0, p1

    #@2b1
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@2b3
    invoke-virtual {v2}, Landroid/content/res/CompatibilityInfo;->requiresWvgaAspect()Z

    #@2b6
    move-result v2

    #@2b7
    if-eqz v2, :cond_2c0

    #@2b9
    .line 4361
    const/16 v2, 0xf0

    #@2bb
    invoke-static {v2}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    #@2be
    goto/16 :goto_203

    #@2c0
    .line 4364
    :cond_2c0
    move-object/from16 v0, p1

    #@2c2
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@2c4
    invoke-virtual {v2}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@2c7
    move-result v2

    #@2c8
    if-eqz v2, :cond_203

    #@2ca
    .line 4365
    sget v2, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@2cc
    invoke-static {v2}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    #@2cf
    goto/16 :goto_203

    #@2d1
    .line 4389
    :cond_2d1
    const-string v2, "ActivityThread"

    #@2d3
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d8
    const-string v5, "Application "

    #@2da
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2dd
    move-result-object v4

    #@2de
    move-object/from16 v0, p1

    #@2e0
    iget-object v5, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@2e2
    invoke-virtual {v5}, Landroid/app/LoadedApk;->getPackageName()Ljava/lang/String;

    #@2e5
    move-result-object v5

    #@2e6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e9
    move-result-object v4

    #@2ea
    const-string v5, " can be debugged on port 8100..."

    #@2ec
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ef
    move-result-object v4

    #@2f0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f3
    move-result-object v4

    #@2f4
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f7
    goto/16 :goto_256

    #@2f9
    .line 4427
    .restart local v12       #b:Landroid/os/IBinder;
    .restart local v20       #ii:Landroid/content/pm/InstrumentationInfo;
    :cond_2f9
    move-object/from16 v0, v20

    #@2fb
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->sourceDir:Ljava/lang/String;

    #@2fd
    move-object/from16 v0, p0

    #@2ff
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentationAppDir:Ljava/lang/String;

    #@301
    .line 4428
    move-object/from16 v0, v20

    #@303
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@305
    move-object/from16 v0, p0

    #@307
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentationAppLibraryDir:Ljava/lang/String;

    #@309
    .line 4429
    move-object/from16 v0, v20

    #@30b
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@30d
    move-object/from16 v0, p0

    #@30f
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentationAppPackage:Ljava/lang/String;

    #@311
    .line 4430
    move-object/from16 v0, p1

    #@313
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@315
    invoke-virtual {v2}, Landroid/app/LoadedApk;->getAppDir()Ljava/lang/String;

    #@318
    move-result-object v2

    #@319
    move-object/from16 v0, p0

    #@31b
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentedAppDir:Ljava/lang/String;

    #@31d
    .line 4431
    move-object/from16 v0, p1

    #@31f
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@321
    invoke-virtual {v2}, Landroid/app/LoadedApk;->getLibDir()Ljava/lang/String;

    #@324
    move-result-object v2

    #@325
    move-object/from16 v0, p0

    #@327
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentedAppLibraryDir:Ljava/lang/String;

    #@329
    .line 4433
    new-instance v3, Landroid/content/pm/ApplicationInfo;

    #@32b
    invoke-direct {v3}, Landroid/content/pm/ApplicationInfo;-><init>()V

    #@32e
    .line 4434
    .local v3, instrApp:Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, v20

    #@330
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@332
    iput-object v2, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@334
    .line 4435
    move-object/from16 v0, v20

    #@336
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->sourceDir:Ljava/lang/String;

    #@338
    iput-object v2, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@33a
    .line 4436
    move-object/from16 v0, v20

    #@33c
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->publicSourceDir:Ljava/lang/String;

    #@33e
    iput-object v2, v3, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@340
    .line 4437
    move-object/from16 v0, v20

    #@342
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->dataDir:Ljava/lang/String;

    #@344
    iput-object v2, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@346
    .line 4438
    move-object/from16 v0, v20

    #@348
    iget-object v2, v0, Landroid/content/pm/InstrumentationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@34a
    iput-object v2, v3, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@34c
    .line 4439
    move-object/from16 v0, p1

    #@34e
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@350
    invoke-virtual {v11}, Landroid/app/ContextImpl;->getClassLoader()Ljava/lang/ClassLoader;

    #@353
    move-result-object v5

    #@354
    const/4 v6, 0x0

    #@355
    const/4 v7, 0x1

    #@356
    move-object/from16 v2, p0

    #@358
    invoke-direct/range {v2 .. v7}, Landroid/app/ActivityThread;->getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZ)Landroid/app/LoadedApk;

    #@35b
    move-result-object v22

    #@35c
    .line 4441
    .local v22, pi:Landroid/app/LoadedApk;
    new-instance v6, Landroid/app/ContextImpl;

    #@35e
    invoke-direct {v6}, Landroid/app/ContextImpl;-><init>()V

    #@361
    .line 4442
    .local v6, instrContext:Landroid/app/ContextImpl;
    const/4 v2, 0x0

    #@362
    move-object/from16 v0, v22

    #@364
    move-object/from16 v1, p0

    #@366
    invoke-virtual {v6, v0, v2, v1}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@369
    .line 4445
    :try_start_369
    invoke-virtual {v6}, Landroid/app/ContextImpl;->getClassLoader()Ljava/lang/ClassLoader;

    #@36c
    move-result-object v14

    #@36d
    .line 4446
    .local v14, cl:Ljava/lang/ClassLoader;
    move-object/from16 v0, p1

    #@36f
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@371
    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@374
    move-result-object v2

    #@375
    invoke-virtual {v14, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@378
    move-result-object v2

    #@379
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@37c
    move-result-object v2

    #@37d
    check-cast v2, Landroid/app/Instrumentation;

    #@37f
    move-object/from16 v0, p0

    #@381
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;
    :try_end_383
    .catch Ljava/lang/Exception; {:try_start_369 .. :try_end_383} :catch_435

    #@383
    .line 4454
    move-object/from16 v0, p0

    #@385
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@387
    new-instance v8, Landroid/content/ComponentName;

    #@389
    move-object/from16 v0, v20

    #@38b
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@38d
    move-object/from16 v0, v20

    #@38f
    iget-object v5, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@391
    invoke-direct {v8, v2, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@394
    move-object/from16 v0, p1

    #@396
    iget-object v9, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationWatcher:Landroid/app/IInstrumentationWatcher;

    #@398
    move-object/from16 v5, p0

    #@39a
    move-object v7, v11

    #@39b
    invoke-virtual/range {v4 .. v9}, Landroid/app/Instrumentation;->init(Landroid/app/ActivityThread;Landroid/content/Context;Landroid/content/Context;Landroid/content/ComponentName;Landroid/app/IInstrumentationWatcher;)V

    #@39e
    .line 4457
    move-object/from16 v0, p0

    #@3a0
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@3a2
    iget-object v2, v2, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@3a4
    if-eqz v2, :cond_3d8

    #@3a6
    move-object/from16 v0, v20

    #@3a8
    iget-boolean v2, v0, Landroid/content/pm/InstrumentationInfo;->handleProfiling:Z

    #@3aa
    if-nez v2, :cond_3d8

    #@3ac
    move-object/from16 v0, p0

    #@3ae
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@3b0
    iget-object v2, v2, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@3b2
    if-nez v2, :cond_3d8

    #@3b4
    .line 4459
    move-object/from16 v0, p0

    #@3b6
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@3b8
    const/4 v4, 0x1

    #@3b9
    iput-boolean v4, v2, Landroid/app/ActivityThread$Profiler;->handlingProfiling:Z

    #@3bb
    .line 4460
    new-instance v16, Ljava/io/File;

    #@3bd
    move-object/from16 v0, p0

    #@3bf
    iget-object v2, v0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@3c1
    iget-object v2, v2, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@3c3
    move-object/from16 v0, v16

    #@3c5
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3c8
    .line 4461
    .local v16, file:Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@3cb
    move-result-object v2

    #@3cc
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    #@3cf
    .line 4462
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->toString()Ljava/lang/String;

    #@3d2
    move-result-object v2

    #@3d3
    const/high16 v4, 0x80

    #@3d5
    invoke-static {v2, v4}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;I)V

    #@3d8
    .line 4469
    .end local v3           #instrApp:Landroid/content/pm/ApplicationInfo;
    .end local v6           #instrContext:Landroid/app/ContextImpl;
    .end local v14           #cl:Ljava/lang/ClassLoader;
    .end local v16           #file:Ljava/io/File;
    .end local v20           #ii:Landroid/content/pm/InstrumentationInfo;
    .end local v22           #pi:Landroid/app/LoadedApk;
    :cond_3d8
    :goto_3d8
    move-object/from16 v0, p1

    #@3da
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@3dc
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    #@3de
    const/high16 v4, 0x10

    #@3e0
    and-int/2addr v2, v4

    #@3e1
    if-eqz v2, :cond_3ea

    #@3e3
    .line 4470
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@3e6
    move-result-object v2

    #@3e7
    invoke-virtual {v2}, Ldalvik/system/VMRuntime;->clearGrowthLimit()V

    #@3ea
    .line 4476
    :cond_3ea
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    #@3ed
    move-result-object v25

    #@3ee
    .line 4480
    .local v25, savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :try_start_3ee
    move-object/from16 v0, p1

    #@3f0
    iget-object v2, v0, Landroid/app/ActivityThread$AppBindData;->info:Landroid/app/LoadedApk;

    #@3f2
    move-object/from16 v0, p1

    #@3f4
    iget-boolean v4, v0, Landroid/app/ActivityThread$AppBindData;->restrictedBackupMode:Z

    #@3f6
    const/4 v5, 0x0

    #@3f7
    invoke-virtual {v2, v4, v5}, Landroid/app/LoadedApk;->makeApplication(ZLandroid/app/Instrumentation;)Landroid/app/Application;

    #@3fa
    move-result-object v10

    #@3fb
    .line 4481
    .local v10, app:Landroid/app/Application;
    move-object/from16 v0, p0

    #@3fd
    iput-object v10, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@3ff
    .line 4485
    move-object/from16 v0, p1

    #@401
    iget-boolean v2, v0, Landroid/app/ActivityThread$AppBindData;->restrictedBackupMode:Z

    #@403
    if-nez v2, :cond_41f

    #@405
    .line 4486
    move-object/from16 v0, p1

    #@407
    iget-object v0, v0, Landroid/app/ActivityThread$AppBindData;->providers:Ljava/util/List;

    #@409
    move-object/from16 v23, v0

    #@40b
    .line 4487
    .local v23, providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    if-eqz v23, :cond_41f

    #@40d
    .line 4488
    move-object/from16 v0, p0

    #@40f
    move-object/from16 v1, v23

    #@411
    invoke-direct {v0, v10, v1}, Landroid/app/ActivityThread;->installContentProviders(Landroid/content/Context;Ljava/util/List;)V

    #@414
    .line 4491
    move-object/from16 v0, p0

    #@416
    iget-object v2, v0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@418
    const/16 v4, 0x84

    #@41a
    const-wide/16 v7, 0x2710

    #@41c
    invoke-virtual {v2, v4, v7, v8}, Landroid/app/ActivityThread$H;->sendEmptyMessageDelayed(IJ)Z
    :try_end_41f
    .catchall {:try_start_3ee .. :try_end_41f} :catchall_498

    #@41f
    .line 4498
    .end local v23           #providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    :cond_41f
    :try_start_41f
    move-object/from16 v0, p0

    #@421
    iget-object v2, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@423
    move-object/from16 v0, p1

    #@425
    iget-object v4, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationArgs:Landroid/os/Bundle;

    #@427
    invoke-virtual {v2, v4}, Landroid/app/Instrumentation;->onCreate(Landroid/os/Bundle;)V
    :try_end_42a
    .catchall {:try_start_41f .. :try_end_42a} :catchall_498
    .catch Ljava/lang/Exception; {:try_start_41f .. :try_end_42a} :catch_46c

    #@42a
    .line 4507
    :try_start_42a
    move-object/from16 v0, p0

    #@42c
    iget-object v2, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@42e
    invoke-virtual {v2, v10}, Landroid/app/Instrumentation;->callApplicationOnCreate(Landroid/app/Application;)V
    :try_end_431
    .catchall {:try_start_42a .. :try_end_431} :catchall_498
    .catch Ljava/lang/Exception; {:try_start_42a .. :try_end_431} :catch_49d

    #@431
    .line 4516
    :cond_431
    invoke-static/range {v25 .. v25}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@434
    .line 4518
    return-void

    #@435
    .line 4448
    .end local v10           #app:Landroid/app/Application;
    .end local v25           #savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    .restart local v3       #instrApp:Landroid/content/pm/ApplicationInfo;
    .restart local v6       #instrContext:Landroid/app/ContextImpl;
    .restart local v20       #ii:Landroid/content/pm/InstrumentationInfo;
    .restart local v22       #pi:Landroid/app/LoadedApk;
    :catch_435
    move-exception v15

    #@436
    .line 4449
    .local v15, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    #@438
    new-instance v4, Ljava/lang/StringBuilder;

    #@43a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43d
    const-string v5, "Unable to instantiate instrumentation "

    #@43f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@442
    move-result-object v4

    #@443
    move-object/from16 v0, p1

    #@445
    iget-object v5, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@447
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44a
    move-result-object v4

    #@44b
    const-string v5, ": "

    #@44d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@450
    move-result-object v4

    #@451
    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@454
    move-result-object v5

    #@455
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@458
    move-result-object v4

    #@459
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45c
    move-result-object v4

    #@45d
    invoke-direct {v2, v4, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@460
    throw v2

    #@461
    .line 4466
    .end local v3           #instrApp:Landroid/content/pm/ApplicationInfo;
    .end local v6           #instrContext:Landroid/app/ContextImpl;
    .end local v15           #e:Ljava/lang/Exception;
    .end local v20           #ii:Landroid/content/pm/InstrumentationInfo;
    .end local v22           #pi:Landroid/app/LoadedApk;
    :cond_461
    new-instance v2, Landroid/app/Instrumentation;

    #@463
    invoke-direct {v2}, Landroid/app/Instrumentation;-><init>()V

    #@466
    move-object/from16 v0, p0

    #@468
    iput-object v2, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@46a
    goto/16 :goto_3d8

    #@46c
    .line 4500
    .restart local v10       #app:Landroid/app/Application;
    .restart local v25       #savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :catch_46c
    move-exception v15

    #@46d
    .line 4501
    .restart local v15       #e:Ljava/lang/Exception;
    :try_start_46d
    new-instance v2, Ljava/lang/RuntimeException;

    #@46f
    new-instance v4, Ljava/lang/StringBuilder;

    #@471
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@474
    const-string v5, "Exception thrown in onCreate() of "

    #@476
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@479
    move-result-object v4

    #@47a
    move-object/from16 v0, p1

    #@47c
    iget-object v5, v0, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@47e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@481
    move-result-object v4

    #@482
    const-string v5, ": "

    #@484
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@487
    move-result-object v4

    #@488
    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@48b
    move-result-object v5

    #@48c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48f
    move-result-object v4

    #@490
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@493
    move-result-object v4

    #@494
    invoke-direct {v2, v4, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@497
    throw v2
    :try_end_498
    .catchall {:try_start_46d .. :try_end_498} :catchall_498

    #@498
    .line 4516
    .end local v10           #app:Landroid/app/Application;
    .end local v15           #e:Ljava/lang/Exception;
    :catchall_498
    move-exception v2

    #@499
    invoke-static/range {v25 .. v25}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@49c
    throw v2

    #@49d
    .line 4508
    .restart local v10       #app:Landroid/app/Application;
    :catch_49d
    move-exception v15

    #@49e
    .line 4509
    .restart local v15       #e:Ljava/lang/Exception;
    :try_start_49e
    move-object/from16 v0, p0

    #@4a0
    iget-object v2, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@4a2
    invoke-virtual {v2, v10, v15}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@4a5
    move-result v2

    #@4a6
    if-nez v2, :cond_431

    #@4a8
    .line 4510
    new-instance v2, Ljava/lang/RuntimeException;

    #@4aa
    new-instance v4, Ljava/lang/StringBuilder;

    #@4ac
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4af
    const-string v5, "Unable to create application "

    #@4b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b4
    move-result-object v4

    #@4b5
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4b8
    move-result-object v5

    #@4b9
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@4bc
    move-result-object v5

    #@4bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c0
    move-result-object v4

    #@4c1
    const-string v5, ": "

    #@4c3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v4

    #@4c7
    invoke-virtual {v15}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@4ca
    move-result-object v5

    #@4cb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ce
    move-result-object v4

    #@4cf
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d2
    move-result-object v4

    #@4d3
    invoke-direct {v2, v4, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4d6
    throw v2
    :try_end_4d7
    .catchall {:try_start_49e .. :try_end_4d7} :catchall_498

    #@4d7
    .line 4419
    .end local v10           #app:Landroid/app/Application;
    .end local v15           #e:Ljava/lang/Exception;
    .end local v25           #savedPolicy:Landroid/os/StrictMode$ThreadPolicy;
    .restart local v20       #ii:Landroid/content/pm/InstrumentationInfo;
    :catch_4d7
    move-exception v2

    #@4d8
    goto/16 :goto_287

    #@4da
    .line 4411
    .end local v20           #ii:Landroid/content/pm/InstrumentationInfo;
    .restart local v26       #service:Landroid/net/IConnectivityManager;
    :catch_4da
    move-exception v2

    #@4db
    goto/16 :goto_272

    #@4dd
    .line 4385
    .end local v12           #b:Landroid/os/IBinder;
    .end local v26           #service:Landroid/net/IConnectivityManager;
    .restart local v21       #mgr:Landroid/app/IActivityManager;
    :catch_4dd
    move-exception v2

    #@4de
    goto/16 :goto_256

    #@4e0
    .line 4378
    :catch_4e0
    move-exception v2

    #@4e1
    goto/16 :goto_249
.end method

.method private handleBindService(Landroid/app/ActivityThread$BindServiceData;)V
    .registers 10
    .parameter "data"

    #@0
    .prologue
    .line 2569
    iget-object v3, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@2
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@4
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/app/Service;

    #@a
    .line 2572
    .local v2, s:Landroid/app/Service;
    if-eqz v2, :cond_2d

    #@c
    .line 2574
    :try_start_c
    iget-object v3, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@e
    invoke-virtual {v2}, Landroid/app/Service;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_15} :catch_42

    #@15
    .line 2576
    :try_start_15
    iget-boolean v3, p1, Landroid/app/ActivityThread$BindServiceData;->rebind:Z

    #@17
    if-nez v3, :cond_2e

    #@19
    .line 2577
    iget-object v3, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@1b
    invoke-virtual {v2, v3}, Landroid/app/Service;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    #@1e
    move-result-object v0

    #@1f
    .line 2578
    .local v0, binder:Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@22
    move-result-object v3

    #@23
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@25
    iget-object v5, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@27
    invoke-interface {v3, v4, v5, v0}, Landroid/app/IActivityManager;->publishService(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/IBinder;)V

    #@2a
    .line 2585
    .end local v0           #binder:Landroid/os/IBinder;
    :goto_2a
    invoke-virtual {p0}, Landroid/app/ActivityThread;->ensureJitEnabled()V

    #@2d
    .line 2596
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 2581
    :cond_2e
    iget-object v3, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@30
    invoke-virtual {v2, v3}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    #@33
    .line 2582
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@36
    move-result-object v3

    #@37
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@39
    const/4 v5, 0x0

    #@3a
    const/4 v6, 0x0

    #@3b
    const/4 v7, 0x0

    #@3c
    invoke-interface {v3, v4, v5, v6, v7}, Landroid/app/IActivityManager;->serviceDoneExecuting(Landroid/os/IBinder;III)V
    :try_end_3f
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_3f} :catch_40
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_3f} :catch_42

    #@3f
    goto :goto_2a

    #@40
    .line 2586
    :catch_40
    move-exception v3

    #@41
    goto :goto_2d

    #@42
    .line 2588
    :catch_42
    move-exception v1

    #@43
    .line 2589
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@45
    invoke-virtual {v3, v2, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@48
    move-result v3

    #@49
    if-nez v3, :cond_2d

    #@4b
    .line 2590
    new-instance v3, Ljava/lang/RuntimeException;

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, "Unable to bind to service "

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    const-string v5, " with "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    iget-object v5, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    const-string v5, ": "

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v4

    #@7a
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@7d
    throw v3
.end method

.method private handleCreateBackupAgent(Landroid/app/ActivityThread$CreateBackupAgentData;)V
    .registers 17
    .parameter "data"

    #@0
    .prologue
    const/4 v14, 0x3

    #@1
    .line 2424
    :try_start_1
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4
    move-result-object v10

    #@5
    move-object/from16 v0, p1

    #@7
    iget-object v11, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@9
    iget-object v11, v11, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@b
    const/4 v12, 0x0

    #@c
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@f
    move-result v13

    #@10
    invoke-interface {v10, v11, v12, v13}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    #@13
    move-result-object v9

    #@14
    .line 2426
    .local v9, requestedPackage:Landroid/content/pm/PackageInfo;
    iget-object v10, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@16
    iget v10, v10, Landroid/content/pm/ApplicationInfo;->uid:I

    #@18
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@1b
    move-result v11

    #@1c
    if-eq v10, v11, :cond_46

    #@1e
    .line 2427
    const-string v10, "ActivityThread"

    #@20
    new-instance v11, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v12, "Asked to instantiate non-matching package "

    #@27
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v11

    #@2b
    move-object/from16 v0, p1

    #@2d
    iget-object v12, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2f
    iget-object v12, v12, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@31
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v11

    #@35
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v11

    #@39
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_3c} :catch_3d

    #@3c
    .line 2500
    .end local v9           #requestedPackage:Landroid/content/pm/PackageInfo;
    :goto_3c
    return-void

    #@3d
    .line 2431
    :catch_3d
    move-exception v6

    #@3e
    .line 2432
    .local v6, e:Landroid/os/RemoteException;
    const-string v10, "ActivityThread"

    #@40
    const-string v11, "Can\'t reach package manager"

    #@42
    invoke-static {v10, v11, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    goto :goto_3c

    #@46
    .line 2437
    .end local v6           #e:Landroid/os/RemoteException;
    .restart local v9       #requestedPackage:Landroid/content/pm/PackageInfo;
    :cond_46
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@49
    .line 2440
    move-object/from16 v0, p1

    #@4b
    iget-object v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@4d
    move-object/from16 v0, p1

    #@4f
    iget-object v11, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@51
    invoke-virtual {p0, v10, v11}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@54
    move-result-object v7

    #@55
    .line 2441
    .local v7, packageInfo:Landroid/app/LoadedApk;
    iget-object v8, v7, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@57
    .line 2442
    .local v8, packageName:Ljava/lang/String;
    if-nez v8, :cond_61

    #@59
    .line 2443
    const-string v10, "ActivityThread"

    #@5b
    const-string v11, "Asked to create backup agent for nonexistent package"

    #@5d
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_3c

    #@61
    .line 2447
    :cond_61
    iget-object v10, p0, Landroid/app/ActivityThread;->mBackupAgents:Ljava/util/HashMap;

    #@63
    invoke-virtual {v10, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@66
    move-result-object v10

    #@67
    if-eqz v10, :cond_88

    #@69
    .line 2448
    const-string v10, "ActivityThread"

    #@6b
    new-instance v11, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v12, "BackupAgent   for "

    #@72
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v11

    #@76
    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v11

    #@7a
    const-string v12, " already exists"

    #@7c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v11

    #@80
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v11

    #@84
    invoke-static {v10, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    goto :goto_3c

    #@88
    .line 2453
    :cond_88
    const/4 v1, 0x0

    #@89
    .line 2454
    .local v1, agent:Landroid/app/backup/BackupAgent;
    move-object/from16 v0, p1

    #@8b
    iget-object v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@8d
    iget-object v4, v10, Landroid/content/pm/ApplicationInfo;->backupAgentName:Ljava/lang/String;

    #@8f
    .line 2457
    .local v4, classname:Ljava/lang/String;
    if-nez v4, :cond_a0

    #@91
    move-object/from16 v0, p1

    #@93
    iget v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->backupMode:I

    #@95
    const/4 v11, 0x1

    #@96
    if-eq v10, v11, :cond_9e

    #@98
    move-object/from16 v0, p1

    #@9a
    iget v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->backupMode:I

    #@9c
    if-ne v10, v14, :cond_a0

    #@9e
    .line 2459
    :cond_9e
    const-string v4, "android.app.backup.FullBackupAgent"

    #@a0
    .line 2463
    :cond_a0
    const/4 v2, 0x0

    #@a1
    .line 2467
    .local v2, binder:Landroid/os/IBinder;
    :try_start_a1
    invoke-virtual {v7}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@a4
    move-result-object v3

    #@a5
    .line 2468
    .local v3, cl:Ljava/lang/ClassLoader;
    invoke-virtual {v3, v4}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@a8
    move-result-object v10

    #@a9
    invoke-virtual {v10}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@ac
    move-result-object v10

    #@ad
    move-object v0, v10

    #@ae
    check-cast v0, Landroid/app/backup/BackupAgent;

    #@b0
    move-object v1, v0

    #@b1
    .line 2471
    new-instance v5, Landroid/app/ContextImpl;

    #@b3
    invoke-direct {v5}, Landroid/app/ContextImpl;-><init>()V

    #@b6
    .line 2472
    .local v5, context:Landroid/app/ContextImpl;
    const/4 v10, 0x0

    #@b7
    invoke-virtual {v5, v7, v10, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@ba
    .line 2473
    invoke-virtual {v5, v1}, Landroid/app/ContextImpl;->setOuterContext(Landroid/content/Context;)V

    #@bd
    .line 2474
    invoke-virtual {v1, v5}, Landroid/app/backup/BackupAgent;->attach(Landroid/content/Context;)V

    #@c0
    .line 2476
    invoke-virtual {v1}, Landroid/app/backup/BackupAgent;->onCreate()V

    #@c3
    .line 2477
    invoke-virtual {v1}, Landroid/app/backup/BackupAgent;->onBind()Landroid/os/IBinder;

    #@c6
    move-result-object v2

    #@c7
    .line 2478
    iget-object v10, p0, Landroid/app/ActivityThread;->mBackupAgents:Ljava/util/HashMap;

    #@c9
    invoke-virtual {v10, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_cc
    .catch Ljava/lang/Exception; {:try_start_a1 .. :try_end_cc} :catch_d8

    #@cc
    .line 2492
    .end local v3           #cl:Ljava/lang/ClassLoader;
    .end local v5           #context:Landroid/app/ContextImpl;
    :cond_cc
    :try_start_cc
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@cf
    move-result-object v10

    #@d0
    invoke-interface {v10, v8, v2}, Landroid/app/IActivityManager;->backupAgentCreated(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_d3
    .catch Landroid/os/RemoteException; {:try_start_cc .. :try_end_d3} :catch_d5
    .catch Ljava/lang/Exception; {:try_start_cc .. :try_end_d3} :catch_ff

    #@d3
    goto/16 :goto_3c

    #@d5
    .line 2493
    :catch_d5
    move-exception v10

    #@d6
    goto/16 :goto_3c

    #@d8
    .line 2479
    :catch_d8
    move-exception v6

    #@d9
    .line 2482
    .local v6, e:Ljava/lang/Exception;
    :try_start_d9
    const-string v10, "ActivityThread"

    #@db
    new-instance v11, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v12, "Agent threw during creation: "

    #@e2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v11

    #@e6
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v11

    #@ea
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v11

    #@ee
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    .line 2483
    move-object/from16 v0, p1

    #@f3
    iget v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->backupMode:I

    #@f5
    const/4 v11, 0x2

    #@f6
    if-eq v10, v11, :cond_cc

    #@f8
    move-object/from16 v0, p1

    #@fa
    iget v10, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->backupMode:I

    #@fc
    if-eq v10, v14, :cond_cc

    #@fe
    .line 2485
    throw v6
    :try_end_ff
    .catch Ljava/lang/Exception; {:try_start_d9 .. :try_end_ff} :catch_ff

    #@ff
    .line 2496
    .end local v6           #e:Ljava/lang/Exception;
    :catch_ff
    move-exception v6

    #@100
    .line 2497
    .restart local v6       #e:Ljava/lang/Exception;
    new-instance v10, Ljava/lang/RuntimeException;

    #@102
    new-instance v11, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v12, "Unable to create BackupAgent "

    #@109
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v11

    #@10d
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v11

    #@111
    const-string v12, ": "

    #@113
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v11

    #@117
    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@11a
    move-result-object v12

    #@11b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v11

    #@11f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v11

    #@123
    invoke-direct {v10, v11, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@126
    throw v10
.end method

.method private handleCreateService(Landroid/app/ActivityThread$CreateServiceData;)V
    .registers 14
    .parameter "data"

    #@0
    .prologue
    .line 2525
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@3
    .line 2527
    iget-object v3, p1, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@5
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@7
    iget-object v4, p1, Landroid/app/ActivityThread$CreateServiceData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@9
    invoke-virtual {p0, v3, v4}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@c
    move-result-object v10

    #@d
    .line 2529
    .local v10, packageInfo:Landroid/app/LoadedApk;
    const/4 v1, 0x0

    #@e
    .line 2531
    .local v1, service:Landroid/app/Service;
    :try_start_e
    invoke-virtual {v10}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v8

    #@12
    .line 2532
    .local v8, cl:Ljava/lang/ClassLoader;
    iget-object v3, p1, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@14
    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@16
    invoke-virtual {v8, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    move-object v0, v3

    #@1f
    check-cast v0, Landroid/app/Service;

    #@21
    move-object v1, v0
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_22} :catch_5a

    #@22
    .line 2544
    .end local v8           #cl:Ljava/lang/ClassLoader;
    :cond_22
    :try_start_22
    new-instance v2, Landroid/app/ContextImpl;

    #@24
    invoke-direct {v2}, Landroid/app/ContextImpl;-><init>()V

    #@27
    .line 2545
    .local v2, context:Landroid/app/ContextImpl;
    const/4 v3, 0x0

    #@28
    invoke-virtual {v2, v10, v3, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@2b
    .line 2547
    const/4 v3, 0x0

    #@2c
    iget-object v4, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@2e
    invoke-virtual {v10, v3, v4}, Landroid/app/LoadedApk;->makeApplication(ZLandroid/app/Instrumentation;)Landroid/app/Application;

    #@31
    move-result-object v6

    #@32
    .line 2548
    .local v6, app:Landroid/app/Application;
    invoke-virtual {v2, v1}, Landroid/app/ContextImpl;->setOuterContext(Landroid/content/Context;)V

    #@35
    .line 2549
    iget-object v3, p1, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@37
    iget-object v4, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@39
    iget-object v5, p1, Landroid/app/ActivityThread$CreateServiceData;->token:Landroid/os/IBinder;

    #@3b
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3e
    move-result-object v7

    #@3f
    move-object v3, p0

    #@40
    invoke-virtual/range {v1 .. v7}, Landroid/app/Service;->attach(Landroid/content/Context;Landroid/app/ActivityThread;Ljava/lang/String;Landroid/os/IBinder;Landroid/app/Application;Ljava/lang/Object;)V

    #@43
    .line 2551
    invoke-virtual {v1}, Landroid/app/Service;->onCreate()V

    #@46
    .line 2552
    iget-object v3, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@48
    iget-object v4, p1, Landroid/app/ActivityThread$CreateServiceData;->token:Landroid/os/IBinder;

    #@4a
    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_4d} :catch_8e

    #@4d
    .line 2554
    :try_start_4d
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@50
    move-result-object v3

    #@51
    iget-object v4, p1, Landroid/app/ActivityThread$CreateServiceData;->token:Landroid/os/IBinder;

    #@53
    const/4 v5, 0x0

    #@54
    const/4 v7, 0x0

    #@55
    const/4 v11, 0x0

    #@56
    invoke-interface {v3, v4, v5, v7, v11}, Landroid/app/IActivityManager;->serviceDoneExecuting(Landroid/os/IBinder;III)V
    :try_end_59
    .catch Landroid/os/RemoteException; {:try_start_4d .. :try_end_59} :catch_c2
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_59} :catch_8e

    #@59
    .line 2566
    .end local v2           #context:Landroid/app/ContextImpl;
    .end local v6           #app:Landroid/app/Application;
    :cond_59
    :goto_59
    return-void

    #@5a
    .line 2533
    :catch_5a
    move-exception v9

    #@5b
    .line 2534
    .local v9, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@5d
    invoke-virtual {v3, v1, v9}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@60
    move-result v3

    #@61
    if-nez v3, :cond_22

    #@63
    .line 2535
    new-instance v3, Ljava/lang/RuntimeException;

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v5, "Unable to instantiate service "

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    iget-object v5, p1, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@72
    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    const-string v5, ": "

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    invoke-direct {v3, v4, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8d
    throw v3

    #@8e
    .line 2559
    .end local v9           #e:Ljava/lang/Exception;
    :catch_8e
    move-exception v9

    #@8f
    .line 2560
    .restart local v9       #e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@91
    invoke-virtual {v3, v1, v9}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@94
    move-result v3

    #@95
    if-nez v3, :cond_59

    #@97
    .line 2561
    new-instance v3, Ljava/lang/RuntimeException;

    #@99
    new-instance v4, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v5, "Unable to create service "

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    iget-object v5, p1, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@a6
    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    const-string v5, ": "

    #@ae
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v4

    #@b2
    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v4

    #@ba
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v4

    #@be
    invoke-direct {v3, v4, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@c1
    throw v3

    #@c2
    .line 2556
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v2       #context:Landroid/app/ContextImpl;
    .restart local v6       #app:Landroid/app/Application;
    :catch_c2
    move-exception v3

    #@c3
    goto :goto_59
.end method

.method private handleDestroyActivity(Landroid/os/IBinder;ZIZ)V
    .registers 13
    .parameter "token"
    .parameter "finishing"
    .parameter "configChanges"
    .parameter "getNonConfigInstance"

    #@0
    .prologue
    .line 3501
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/ActivityThread;->performDestroyActivity(Landroid/os/IBinder;ZIZ)Landroid/app/ActivityThread$ActivityClientRecord;

    #@3
    move-result-object v1

    #@4
    .line 3503
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v1, :cond_83

    #@6
    .line 3504
    invoke-static {v1}, Landroid/app/ActivityThread;->cleanUpPendingRemoveWindows(Landroid/app/ActivityThread$ActivityClientRecord;)V

    #@9
    .line 3505
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b
    invoke-virtual {v5}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    #@e
    move-result-object v3

    #@f
    .line 3506
    .local v3, wm:Landroid/view/WindowManager;
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@11
    iget-object v2, v5, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@13
    .line 3507
    .local v2, v:Landroid/view/View;
    if-eqz v2, :cond_51

    #@15
    .line 3508
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@17
    iget-boolean v5, v5, Landroid/app/Activity;->mVisibleFromServer:Z

    #@19
    if-eqz v5, :cond_21

    #@1b
    .line 3509
    iget v5, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@1d
    add-int/lit8 v5, v5, -0x1

    #@1f
    iput v5, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@21
    .line 3511
    :cond_21
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@24
    move-result-object v4

    #@25
    .line 3512
    .local v4, wtoken:Landroid/os/IBinder;
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@27
    iget-boolean v5, v5, Landroid/app/Activity;->mWindowAdded:Z

    #@29
    if-eqz v5, :cond_33

    #@2b
    .line 3513
    iget-boolean v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@2d
    if-eqz v5, :cond_8d

    #@2f
    .line 3516
    iput-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@31
    .line 3517
    iput-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindowManager:Landroid/view/WindowManager;

    #@33
    .line 3522
    :cond_33
    :goto_33
    if-eqz v4, :cond_4c

    #@35
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@37
    if-nez v5, :cond_4c

    #@39
    .line 3523
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@3c
    move-result-object v5

    #@3d
    iget-object v6, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@3f
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@46
    move-result-object v6

    #@47
    const-string v7, "Activity"

    #@49
    invoke-virtual {v5, v4, v6, v7}, Landroid/view/WindowManagerGlobal;->closeAll(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    .line 3526
    :cond_4c
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@4e
    const/4 v6, 0x0

    #@4f
    iput-object v6, v5, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@51
    .line 3528
    .end local v4           #wtoken:Landroid/os/IBinder;
    :cond_51
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->mPendingRemoveWindow:Landroid/view/View;

    #@53
    if-nez v5, :cond_68

    #@55
    .line 3535
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@58
    move-result-object v5

    #@59
    iget-object v6, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@5b
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@62
    move-result-object v6

    #@63
    const-string v7, "Activity"

    #@65
    invoke-virtual {v5, p1, v6, v7}, Landroid/view/WindowManagerGlobal;->closeAll(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V

    #@68
    .line 3543
    :cond_68
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@6a
    invoke-virtual {v5}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    #@6d
    move-result-object v0

    #@6e
    .line 3544
    .local v0, c:Landroid/content/Context;
    instance-of v5, v0, Landroid/app/ContextImpl;

    #@70
    if-eqz v5, :cond_83

    #@72
    .line 3545
    check-cast v0, Landroid/app/ContextImpl;

    #@74
    .end local v0           #c:Landroid/content/Context;
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@76
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    const-string v6, "Activity"

    #@80
    invoke-virtual {v0, v5, v6}, Landroid/app/ContextImpl;->scheduleFinalCleanup(Ljava/lang/String;Ljava/lang/String;)V

    #@83
    .line 3549
    .end local v2           #v:Landroid/view/View;
    .end local v3           #wm:Landroid/view/WindowManager;
    :cond_83
    if-eqz p2, :cond_8c

    #@85
    .line 3551
    :try_start_85
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@88
    move-result-object v5

    #@89
    invoke-interface {v5, p1}, Landroid/app/IActivityManager;->activityDestroyed(Landroid/os/IBinder;)V
    :try_end_8c
    .catch Landroid/os/RemoteException; {:try_start_85 .. :try_end_8c} :catch_91

    #@8c
    .line 3556
    :cond_8c
    :goto_8c
    return-void

    #@8d
    .line 3519
    .restart local v2       #v:Landroid/view/View;
    .restart local v3       #wm:Landroid/view/WindowManager;
    .restart local v4       #wtoken:Landroid/os/IBinder;
    :cond_8d
    invoke-interface {v3, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    #@90
    goto :goto_33

    #@91
    .line 3552
    .end local v2           #v:Landroid/view/View;
    .end local v3           #wm:Landroid/view/WindowManager;
    .end local v4           #wtoken:Landroid/os/IBinder;
    :catch_91
    move-exception v5

    #@92
    goto :goto_8c
.end method

.method private handleDestroyBackupAgent(Landroid/app/ActivityThread$CreateBackupAgentData;)V
    .registers 9
    .parameter "data"

    #@0
    .prologue
    .line 2506
    iget-object v4, p1, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    iget-object v5, p1, Landroid/app/ActivityThread$CreateBackupAgentData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@4
    invoke-virtual {p0, v4, v5}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@7
    move-result-object v2

    #@8
    .line 2507
    .local v2, packageInfo:Landroid/app/LoadedApk;
    iget-object v3, v2, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@a
    .line 2508
    .local v3, packageName:Ljava/lang/String;
    iget-object v4, p0, Landroid/app/ActivityThread;->mBackupAgents:Ljava/util/HashMap;

    #@c
    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/backup/BackupAgent;

    #@12
    .line 2509
    .local v0, agent:Landroid/app/backup/BackupAgent;
    if-eqz v0, :cond_3c

    #@14
    .line 2511
    :try_start_14
    invoke-virtual {v0}, Landroid/app/backup/BackupAgent;->onDestroy()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_17} :catch_1d

    #@17
    .line 2516
    :goto_17
    iget-object v4, p0, Landroid/app/ActivityThread;->mBackupAgents:Ljava/util/HashMap;

    #@19
    invoke-virtual {v4, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    .line 2520
    :goto_1c
    return-void

    #@1d
    .line 2512
    :catch_1d
    move-exception v1

    #@1e
    .line 2513
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "ActivityThread"

    #@20
    new-instance v5, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v6, "Exception thrown in onDestroy by backup agent of "

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    iget-object v6, p1, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v5

    #@35
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 2514
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@3b
    goto :goto_17

    #@3c
    .line 2518
    .end local v1           #e:Ljava/lang/Exception;
    :cond_3c
    const-string v4, "ActivityThread"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "Attempt to destroy unknown backup agent "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_1c
.end method

.method private handleDumpActivity(Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 9
    .parameter "info"

    #@0
    .prologue
    .line 2640
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v0

    #@4
    .line 2642
    .local v0, oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :try_start_4
    iget-object v3, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@6
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@8
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@e
    .line 2643
    .local v2, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v2, :cond_36

    #@10
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@12
    if-eqz v3, :cond_36

    #@14
    .line 2644
    new-instance v1, Ljava/io/PrintWriter;

    #@16
    new-instance v3, Ljava/io/FileOutputStream;

    #@18
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@1a
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1d
    move-result-object v4

    #@1e
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@21
    invoke-direct {v1, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@24
    .line 2645
    .local v1, pw:Ljava/io/PrintWriter;
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@26
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->prefix:Ljava/lang/String;

    #@28
    iget-object v5, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@2a
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@2d
    move-result-object v5

    #@2e
    iget-object v6, p1, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@30
    invoke-virtual {v3, v4, v5, v1, v6}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@33
    .line 2646
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V
    :try_end_36
    .catchall {:try_start_4 .. :try_end_36} :catchall_3f

    #@36
    .line 2649
    .end local v1           #pw:Ljava/io/PrintWriter;
    :cond_36
    iget-object v3, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@38
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3b
    .line 2650
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@3e
    .line 2652
    return-void

    #@3f
    .line 2649
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :catchall_3f
    move-exception v3

    #@40
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@42
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@45
    .line 2650
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@48
    .line 2649
    throw v3
.end method

.method static final handleDumpHeap(ZLandroid/app/ActivityThread$DumpHeapData;)V
    .registers 6
    .parameter "managed"
    .parameter "dhd"

    #@0
    .prologue
    .line 4087
    if-eqz p0, :cond_4a

    #@2
    .line 4089
    :try_start_2
    iget-object v1, p1, Landroid/app/ActivityThread$DumpHeapData;->path:Ljava/lang/String;

    #@4
    iget-object v2, p1, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@9
    move-result-object v2

    #@a
    invoke-static {v1, v2}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;Ljava/io/FileDescriptor;)V
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_43
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_d} :catch_13

    #@d
    .line 4095
    :try_start_d
    iget-object v1, p1, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@f
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_12} :catch_5d

    #@12
    .line 4103
    :goto_12
    return-void

    #@13
    .line 4090
    :catch_13
    move-exception v0

    #@14
    .line 4091
    .local v0, e:Ljava/io/IOException;
    :try_start_14
    const-string v1, "ActivityThread"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Managed heap dump failed on path "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    iget-object v3, p1, Landroid/app/ActivityThread$DumpHeapData;->path:Ljava/lang/String;

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, " -- can the process access this path?"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_14 .. :try_end_34} :catchall_43

    #@34
    .line 4095
    :try_start_34
    iget-object v1, p1, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@36
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_39} :catch_3a

    #@39
    goto :goto_12

    #@3a
    .line 4096
    :catch_3a
    move-exception v0

    #@3b
    .line 4097
    const-string v1, "ActivityThread"

    #@3d
    const-string v2, "Failure closing profile fd"

    #@3f
    :goto_3f
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    goto :goto_12

    #@43
    .line 4094
    .end local v0           #e:Ljava/io/IOException;
    :catchall_43
    move-exception v1

    #@44
    .line 4095
    :try_start_44
    iget-object v2, p1, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@46
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_49} :catch_54

    #@49
    .line 4094
    :goto_49
    throw v1

    #@4a
    .line 4101
    :cond_4a
    iget-object v1, p1, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@4c
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@4f
    move-result-object v1

    #@50
    invoke-static {v1}, Landroid/os/Debug;->dumpNativeHeap(Ljava/io/FileDescriptor;)V

    #@53
    goto :goto_12

    #@54
    .line 4096
    :catch_54
    move-exception v0

    #@55
    .line 4097
    .restart local v0       #e:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@57
    const-string v3, "Failure closing profile fd"

    #@59
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5c
    goto :goto_49

    #@5d
    .line 4096
    .end local v0           #e:Ljava/io/IOException;
    :catch_5d
    move-exception v0

    #@5e
    .line 4097
    .restart local v0       #e:Ljava/io/IOException;
    const-string v1, "ActivityThread"

    #@60
    const-string v2, "Failure closing profile fd"

    #@62
    goto :goto_3f
.end method

.method private handleDumpProvider(Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 8
    .parameter "info"

    #@0
    .prologue
    .line 2655
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v0

    #@4
    .line 2657
    .local v0, oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :try_start_4
    iget-object v3, p0, Landroid/app/ActivityThread;->mLocalProviders:Ljava/util/HashMap;

    #@6
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@8
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Landroid/app/ActivityThread$ProviderClientRecord;

    #@e
    .line 2658
    .local v2, r:Landroid/app/ActivityThread$ProviderClientRecord;
    if-eqz v2, :cond_34

    #@10
    iget-object v3, v2, Landroid/app/ActivityThread$ProviderClientRecord;->mLocalProvider:Landroid/content/ContentProvider;

    #@12
    if-eqz v3, :cond_34

    #@14
    .line 2659
    new-instance v1, Ljava/io/PrintWriter;

    #@16
    new-instance v3, Ljava/io/FileOutputStream;

    #@18
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@1a
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@1d
    move-result-object v4

    #@1e
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@21
    invoke-direct {v1, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@24
    .line 2660
    .local v1, pw:Ljava/io/PrintWriter;
    iget-object v3, v2, Landroid/app/ActivityThread$ProviderClientRecord;->mLocalProvider:Landroid/content/ContentProvider;

    #@26
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@28
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@2b
    move-result-object v4

    #@2c
    iget-object v5, p1, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@2e
    invoke-virtual {v3, v4, v1, v5}, Landroid/content/ContentProvider;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@31
    .line 2661
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V
    :try_end_34
    .catchall {:try_start_4 .. :try_end_34} :catchall_3d

    #@34
    .line 2664
    .end local v1           #pw:Ljava/io/PrintWriter;
    :cond_34
    iget-object v3, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@36
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@39
    .line 2665
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@3c
    .line 2667
    return-void

    #@3d
    .line 2664
    .end local v2           #r:Landroid/app/ActivityThread$ProviderClientRecord;
    :catchall_3d
    move-exception v3

    #@3e
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@40
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@43
    .line 2665
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@46
    .line 2664
    throw v3
.end method

.method private handleDumpService(Landroid/app/ActivityThread$DumpComponentInfo;)V
    .registers 7
    .parameter "info"

    #@0
    .prologue
    .line 2625
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    #@3
    move-result-object v0

    #@4
    .line 2627
    .local v0, oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    :try_start_4
    iget-object v3, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@6
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@8
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    check-cast v2, Landroid/app/Service;

    #@e
    .line 2628
    .local v2, s:Landroid/app/Service;
    if-eqz v2, :cond_2e

    #@10
    .line 2629
    new-instance v1, Ljava/io/PrintWriter;

    #@12
    new-instance v3, Ljava/io/FileOutputStream;

    #@14
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@16
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@19
    move-result-object v4

    #@1a
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@1d
    invoke-direct {v1, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@20
    .line 2630
    .local v1, pw:Ljava/io/PrintWriter;
    iget-object v3, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@22
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@25
    move-result-object v3

    #@26
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@28
    invoke-virtual {v2, v3, v1, v4}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@2b
    .line 2631
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V
    :try_end_2e
    .catchall {:try_start_4 .. :try_end_2e} :catchall_37

    #@2e
    .line 2634
    .end local v1           #pw:Ljava/io/PrintWriter;
    :cond_2e
    iget-object v3, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@30
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@33
    .line 2635
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@36
    .line 2637
    return-void

    #@37
    .line 2634
    .end local v2           #s:Landroid/app/Service;
    :catchall_37
    move-exception v3

    #@38
    iget-object v4, p1, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@3a
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@3d
    .line 2635
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@40
    .line 2634
    throw v3
.end method

.method private handleLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)V
    .registers 12
    .parameter "r"
    .parameter "customIntent"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v5, 0x0

    #@3
    .line 2239
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@6
    .line 2241
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@8
    if-eqz v3, :cond_1e

    #@a
    .line 2242
    iget-object v3, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@c
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->profileFile:Ljava/lang/String;

    #@e
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@10
    invoke-virtual {v3, v6, v7}, Landroid/app/ActivityThread$Profiler;->setProfiler(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    #@13
    .line 2243
    iget-object v3, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@15
    invoke-virtual {v3}, Landroid/app/ActivityThread$Profiler;->startProfiling()V

    #@18
    .line 2244
    iget-object v3, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@1a
    iget-boolean v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->autoStopProfiler:Z

    #@1c
    iput-boolean v6, v3, Landroid/app/ActivityThread$Profiler;->autoStopProfiler:Z

    #@1e
    .line 2248
    :cond_1e
    invoke-virtual {p0, v8, v8}, Landroid/app/ActivityThread;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@21
    .line 2252
    invoke-direct {p0, p1, p2}, Landroid/app/ActivityThread;->performLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)Landroid/app/Activity;

    #@24
    move-result-object v0

    #@25
    .line 2254
    .local v0, a:Landroid/app/Activity;
    if-eqz v0, :cond_d4

    #@27
    .line 2255
    new-instance v3, Landroid/content/res/Configuration;

    #@29
    iget-object v6, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@2b
    invoke-direct {v3, v6}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@2e
    iput-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@30
    .line 2256
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@32
    .line 2257
    .local v2, oldState:Landroid/os/Bundle;
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@34
    iget-boolean v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->isForward:Z

    #@36
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@38
    iget-boolean v3, v3, Landroid/app/Activity;->mFinished:Z

    #@3a
    if-nez v3, :cond_93

    #@3c
    iget-boolean v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@3e
    if-nez v3, :cond_93

    #@40
    move v3, v4

    #@41
    :goto_41
    invoke-virtual {p0, v6, v5, v7, v3}, Landroid/app/ActivityThread;->handleResumeActivity(Landroid/os/IBinder;ZZZ)V

    #@44
    .line 2260
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@46
    iget-boolean v3, v3, Landroid/app/Activity;->mFinished:Z

    #@48
    if-nez v3, :cond_d3

    #@4a
    iget-boolean v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@4c
    if-eqz v3, :cond_d3

    #@4e
    .line 2271
    :try_start_4e
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@50
    const/4 v5, 0x0

    #@51
    iput-boolean v5, v3, Landroid/app/Activity;->mCalled:Z

    #@53
    .line 2272
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@55
    iget-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@57
    invoke-virtual {v3, v5}, Landroid/app/Instrumentation;->callActivityOnPause(Landroid/app/Activity;)V

    #@5a
    .line 2280
    invoke-virtual {p1}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@5d
    move-result v3

    #@5e
    if-eqz v3, :cond_62

    #@60
    .line 2281
    iput-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@62
    .line 2283
    :cond_62
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@64
    iget-boolean v3, v3, Landroid/app/Activity;->mCalled:Z

    #@66
    if-nez v3, :cond_d1

    #@68
    .line 2284
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@6a
    new-instance v5, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v6, "Activity "

    #@71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@77
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@7a
    move-result-object v6

    #@7b
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v5

    #@83
    const-string v6, " did not call through to super.onPause()"

    #@85
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v5

    #@89
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    invoke-direct {v3, v5}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@90
    throw v3
    :try_end_91
    .catch Landroid/app/SuperNotCalledException; {:try_start_4e .. :try_end_91} :catch_91
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_91} :catch_95

    #@91
    .line 2289
    :catch_91
    move-exception v1

    #@92
    .line 2290
    .local v1, e:Landroid/app/SuperNotCalledException;
    throw v1

    #@93
    .end local v1           #e:Landroid/app/SuperNotCalledException;
    :cond_93
    move v3, v5

    #@94
    .line 2257
    goto :goto_41

    #@95
    .line 2292
    :catch_95
    move-exception v1

    #@96
    .line 2293
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@98
    iget-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@9a
    invoke-virtual {v3, v5, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@9d
    move-result v3

    #@9e
    if-nez v3, :cond_d1

    #@a0
    .line 2294
    new-instance v3, Ljava/lang/RuntimeException;

    #@a2
    new-instance v4, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v5, "Unable to pause activity "

    #@a9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v4

    #@ad
    iget-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@af
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@b2
    move-result-object v5

    #@b3
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@b6
    move-result-object v5

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    const-string v5, ": "

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@c4
    move-result-object v5

    #@c5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v4

    #@c9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v4

    #@cd
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d0
    throw v3

    #@d1
    .line 2300
    .end local v1           #e:Ljava/lang/Exception;
    :cond_d1
    iput-boolean v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@d3
    .line 2312
    .end local v2           #oldState:Landroid/os/Bundle;
    :cond_d3
    :goto_d3
    return-void

    #@d4
    .line 2306
    :cond_d4
    :try_start_d4
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@d7
    move-result-object v3

    #@d8
    iget-object v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@da
    const/4 v5, 0x0

    #@db
    const/4 v6, 0x0

    #@dc
    invoke-interface {v3, v4, v5, v6}, Landroid/app/IActivityManager;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Z
    :try_end_df
    .catch Landroid/os/RemoteException; {:try_start_d4 .. :try_end_df} :catch_e0

    #@df
    goto :goto_d3

    #@e0
    .line 2308
    :catch_e0
    move-exception v3

    #@e1
    goto :goto_d3
.end method

.method private handleNewIntent(Landroid/app/ActivityThread$NewIntentData;)V
    .registers 4
    .parameter "data"

    #@0
    .prologue
    .line 2343
    iget-object v0, p1, Landroid/app/ActivityThread$NewIntentData;->token:Landroid/os/IBinder;

    #@2
    iget-object v1, p1, Landroid/app/ActivityThread$NewIntentData;->intents:Ljava/util/List;

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/app/ActivityThread;->performNewIntents(Landroid/os/IBinder;Ljava/util/List;)V

    #@7
    .line 2344
    return-void
.end method

.method private handlePauseActivity(Landroid/os/IBinder;ZZI)V
    .registers 8
    .parameter "token"
    .parameter "finished"
    .parameter "userLeaving"
    .parameter "configChanges"

    #@0
    .prologue
    .line 2968
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 2969
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v0, :cond_2d

    #@a
    .line 2971
    if-eqz p3, :cond_f

    #@c
    .line 2972
    invoke-virtual {p0, v0}, Landroid/app/ActivityThread;->performUserLeavingActivity(Landroid/app/ActivityThread$ActivityClientRecord;)V

    #@f
    .line 2975
    :cond_f
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@11
    iget v2, v1, Landroid/app/Activity;->mConfigChangeFlags:I

    #@13
    or-int/2addr v2, p4

    #@14
    iput v2, v1, Landroid/app/Activity;->mConfigChangeFlags:I

    #@16
    .line 2976
    invoke-virtual {v0}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@19
    move-result v1

    #@1a
    invoke-virtual {p0, p1, p2, v1}, Landroid/app/ActivityThread;->performPauseActivity(Landroid/os/IBinder;ZZ)Landroid/os/Bundle;

    #@1d
    .line 2979
    invoke-virtual {v0}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@20
    move-result v1

    #@21
    if-eqz v1, :cond_26

    #@23
    .line 2980
    invoke-static {}, Landroid/app/QueuedWork;->waitToFinish()V

    #@26
    .line 2985
    :cond_26
    :try_start_26
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@29
    move-result-object v1

    #@2a
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->activityPaused(Landroid/os/IBinder;)V
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_2d} :catch_2e

    #@2d
    .line 2989
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 2986
    :catch_2e
    move-exception v1

    #@2f
    goto :goto_2d
.end method

.method private handleReceiver(Landroid/app/ActivityThread$ReceiverData;)V
    .registers 14
    .parameter "data"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 2360
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@4
    .line 2362
    iget-object v8, p1, Landroid/app/ActivityThread$ReceiverData;->intent:Landroid/content/Intent;

    #@6
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@9
    move-result-object v8

    #@a
    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 2364
    .local v2, component:Ljava/lang/String;
    iget-object v8, p1, Landroid/app/ActivityThread$ReceiverData;->info:Landroid/content/pm/ActivityInfo;

    #@10
    iget-object v8, v8, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@12
    iget-object v9, p1, Landroid/app/ActivityThread$ReceiverData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@14
    invoke-virtual {p0, v8, v9}, Landroid/app/ActivityThread;->getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;

    #@17
    move-result-object v6

    #@18
    .line 2367
    .local v6, packageInfo:Landroid/app/LoadedApk;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1b
    move-result-object v5

    #@1c
    .line 2371
    .local v5, mgr:Landroid/app/IActivityManager;
    :try_start_1c
    invoke-virtual {v6}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@1f
    move-result-object v1

    #@20
    .line 2372
    .local v1, cl:Ljava/lang/ClassLoader;
    iget-object v8, p1, Landroid/app/ActivityThread$ReceiverData;->intent:Landroid/content/Intent;

    #@22
    invoke-virtual {v8, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@25
    .line 2373
    invoke-virtual {p1, v1}, Landroid/app/ActivityThread$ReceiverData;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@28
    .line 2374
    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@2f
    move-result-object v7

    #@30
    check-cast v7, Landroid/content/BroadcastReceiver;
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_32} :catch_61

    #@32
    .line 2385
    .local v7, receiver:Landroid/content/BroadcastReceiver;
    const/4 v8, 0x0

    #@33
    :try_start_33
    iget-object v9, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@35
    invoke-virtual {v6, v8, v9}, Landroid/app/LoadedApk;->makeApplication(ZLandroid/app/Instrumentation;)Landroid/app/Application;

    #@38
    move-result-object v0

    #@39
    .line 2395
    .local v0, app:Landroid/app/Application;
    invoke-virtual {v0}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    #@3c
    move-result-object v3

    #@3d
    check-cast v3, Landroid/app/ContextImpl;

    #@3f
    .line 2396
    .local v3, context:Landroid/app/ContextImpl;
    sget-object v8, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@41
    iget-object v9, p1, Landroid/app/ActivityThread$ReceiverData;->intent:Landroid/content/Intent;

    #@43
    invoke-virtual {v8, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@46
    .line 2397
    invoke-virtual {v7, p1}, Landroid/content/BroadcastReceiver;->setPendingResult(Landroid/content/BroadcastReceiver$PendingResult;)V

    #@49
    .line 2398
    invoke-virtual {v3}, Landroid/app/ContextImpl;->getReceiverRestrictedContext()Landroid/content/Context;

    #@4c
    move-result-object v8

    #@4d
    iget-object v9, p1, Landroid/app/ActivityThread$ReceiverData;->intent:Landroid/content/Intent;

    #@4f
    invoke-virtual {v7, v8, v9}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_52
    .catchall {:try_start_33 .. :try_end_52} :catchall_bf
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_52} :catch_8c

    #@52
    .line 2410
    sget-object v8, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@54
    .end local v0           #app:Landroid/app/Application;
    .end local v3           #context:Landroid/app/ContextImpl;
    :goto_54
    invoke-virtual {v8, v11}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@57
    .line 2413
    invoke-virtual {v7}, Landroid/content/BroadcastReceiver;->getPendingResult()Landroid/content/BroadcastReceiver$PendingResult;

    #@5a
    move-result-object v8

    #@5b
    if-eqz v8, :cond_60

    #@5d
    .line 2414
    invoke-virtual {p1}, Landroid/app/ActivityThread$ReceiverData;->finish()V

    #@60
    .line 2416
    :cond_60
    return-void

    #@61
    .line 2375
    .end local v1           #cl:Ljava/lang/ClassLoader;
    .end local v7           #receiver:Landroid/content/BroadcastReceiver;
    :catch_61
    move-exception v4

    #@62
    .line 2378
    .local v4, e:Ljava/lang/Exception;
    invoke-virtual {p1, v5}, Landroid/app/ActivityThread$ReceiverData;->sendFinished(Landroid/app/IActivityManager;)V

    #@65
    .line 2379
    new-instance v8, Ljava/lang/RuntimeException;

    #@67
    new-instance v9, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v10, "Unable to instantiate receiver "

    #@6e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v9

    #@72
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v9

    #@76
    const-string v10, ": "

    #@78
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@7f
    move-result-object v10

    #@80
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v9

    #@88
    invoke-direct {v8, v9, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8b
    throw v8

    #@8c
    .line 2400
    .end local v4           #e:Ljava/lang/Exception;
    .restart local v1       #cl:Ljava/lang/ClassLoader;
    .restart local v7       #receiver:Landroid/content/BroadcastReceiver;
    :catch_8c
    move-exception v4

    #@8d
    .line 2403
    .restart local v4       #e:Ljava/lang/Exception;
    :try_start_8d
    invoke-virtual {p1, v5}, Landroid/app/ActivityThread$ReceiverData;->sendFinished(Landroid/app/IActivityManager;)V

    #@90
    .line 2404
    iget-object v8, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@92
    invoke-virtual {v8, v7, v4}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@95
    move-result v8

    #@96
    if-nez v8, :cond_c6

    #@98
    .line 2405
    new-instance v8, Ljava/lang/RuntimeException;

    #@9a
    new-instance v9, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v10, "Unable to start receiver "

    #@a1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v9

    #@a5
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    const-string v10, ": "

    #@ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v9

    #@af
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@b2
    move-result-object v10

    #@b3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v9

    #@b7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v9

    #@bb
    invoke-direct {v8, v9, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@be
    throw v8
    :try_end_bf
    .catchall {:try_start_8d .. :try_end_bf} :catchall_bf

    #@bf
    .line 2410
    .end local v4           #e:Ljava/lang/Exception;
    :catchall_bf
    move-exception v8

    #@c0
    sget-object v9, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@c2
    invoke-virtual {v9, v11}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@c5
    throw v8

    #@c6
    .restart local v4       #e:Ljava/lang/Exception;
    :cond_c6
    sget-object v8, Landroid/app/ActivityThread;->sCurrentBroadcastIntent:Ljava/lang/ThreadLocal;

    #@c8
    goto :goto_54
.end method

.method private handleRelaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;)V
    .registers 15
    .parameter "tmp"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v10, 0x0

    #@3
    .line 3617
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@6
    .line 3619
    const/4 v1, 0x0

    #@7
    .line 3620
    .local v1, changedConfig:Landroid/content/res/Configuration;
    const/4 v2, 0x0

    #@8
    .line 3625
    .local v2, configChanges:I
    iget-object v8, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@a
    monitor-enter v8

    #@b
    .line 3626
    :try_start_b
    iget-object v7, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 3627
    .local v0, N:I
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@13
    .line 3628
    .local v6, token:Landroid/os/IBinder;
    const/4 p1, 0x0

    #@14
    .line 3629
    const/4 v4, 0x0

    #@15
    .local v4, i:I
    :goto_15
    if-ge v4, v0, :cond_33

    #@17
    .line 3630
    iget-object v7, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v5

    #@1d
    check-cast v5, Landroid/app/ActivityThread$ActivityClientRecord;

    #@1f
    .line 3631
    .local v5, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@21
    if-ne v7, v6, :cond_30

    #@23
    .line 3632
    move-object p1, v5

    #@24
    .line 3633
    iget v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingConfigChanges:I

    #@26
    or-int/2addr v2, v7

    #@27
    .line 3634
    iget-object v7, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2c
    .line 3635
    add-int/lit8 v4, v4, -0x1

    #@2e
    .line 3636
    add-int/lit8 v0, v0, -0x1

    #@30
    .line 3629
    :cond_30
    add-int/lit8 v4, v4, 0x1

    #@32
    goto :goto_15

    #@33
    .line 3640
    .end local v5           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_33
    if-nez p1, :cond_37

    #@35
    .line 3642
    monitor-exit v8

    #@36
    .line 3725
    :cond_36
    :goto_36
    return-void

    #@37
    .line 3649
    :cond_37
    iget-object v7, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@39
    if-eqz v7, :cond_40

    #@3b
    .line 3650
    iget-object v1, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@3d
    .line 3651
    const/4 v7, 0x0

    #@3e
    iput-object v7, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@40
    .line 3653
    :cond_40
    monitor-exit v8
    :try_end_41
    .catchall {:try_start_b .. :try_end_41} :catchall_f2

    #@41
    .line 3655
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@43
    if-eqz v7, :cond_69

    #@45
    .line 3659
    iget-object v7, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@47
    if-eqz v7, :cond_5d

    #@49
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@4b
    iget-object v8, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@4d
    invoke-virtual {v7, v8}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@50
    move-result v7

    #@51
    if-eqz v7, :cond_69

    #@53
    iget-object v7, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@55
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@57
    invoke-virtual {v7, v8}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@5a
    move-result v7

    #@5b
    if-eqz v7, :cond_69

    #@5d
    .line 3662
    :cond_5d
    if-eqz v1, :cond_67

    #@5f
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@61
    invoke-virtual {v7, v1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@64
    move-result v7

    #@65
    if-eqz v7, :cond_69

    #@67
    .line 3664
    :cond_67
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@69
    .line 3673
    :cond_69
    if-eqz v1, :cond_75

    #@6b
    .line 3674
    iget v7, v1, Landroid/content/res/Configuration;->densityDpi:I

    #@6d
    iput v7, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@6f
    .line 3675
    invoke-direct {p0}, Landroid/app/ActivityThread;->updateDefaultDensity()V

    #@72
    .line 3676
    invoke-virtual {p0, v1, v10}, Landroid/app/ActivityThread;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@75
    .line 3679
    :cond_75
    iget-object v7, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@77
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@79
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7c
    move-result-object v5

    #@7d
    check-cast v5, Landroid/app/ActivityThread$ActivityClientRecord;

    #@7f
    .line 3681
    .restart local v5       #r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v5, :cond_36

    #@81
    .line 3685
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@83
    iget v8, v7, Landroid/app/Activity;->mConfigChangeFlags:I

    #@85
    or-int/2addr v8, v2

    #@86
    iput v8, v7, Landroid/app/Activity;->mConfigChangeFlags:I

    #@88
    .line 3686
    iget-boolean v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@8a
    iput-boolean v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@8c
    .line 3687
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@8e
    iget-object v3, v7, Landroid/app/Activity;->mIntent:Landroid/content/Intent;

    #@90
    .line 3689
    .local v3, currentIntent:Landroid/content/Intent;
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@92
    iput-boolean v12, v7, Landroid/app/Activity;->mChangingConfigurations:Z

    #@94
    .line 3692
    iget-boolean v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@96
    if-nez v7, :cond_a1

    #@98
    .line 3693
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@9a
    invoke-virtual {v5}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@9d
    move-result v8

    #@9e
    invoke-virtual {p0, v7, v11, v8}, Landroid/app/ActivityThread;->performPauseActivity(Landroid/os/IBinder;ZZ)Landroid/os/Bundle;

    #@a1
    .line 3695
    :cond_a1
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@a3
    if-nez v7, :cond_c4

    #@a5
    iget-boolean v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@a7
    if-nez v7, :cond_c4

    #@a9
    invoke-virtual {v5}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@ac
    move-result v7

    #@ad
    if-nez v7, :cond_c4

    #@af
    .line 3696
    new-instance v7, Landroid/os/Bundle;

    #@b1
    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    #@b4
    iput-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@b6
    .line 3697
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@b8
    invoke-virtual {v7, v11}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@bb
    .line 3698
    iget-object v7, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@bd
    iget-object v8, v5, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@bf
    iget-object v9, v5, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@c1
    invoke-virtual {v7, v8, v9}, Landroid/app/Instrumentation;->callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@c4
    .line 3701
    :cond_c4
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@c6
    invoke-direct {p0, v7, v11, v2, v12}, Landroid/app/ActivityThread;->handleDestroyActivity(Landroid/os/IBinder;ZIZ)V

    #@c9
    .line 3703
    iput-object v10, v5, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@cb
    .line 3704
    iput-object v10, v5, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@cd
    .line 3705
    iput-boolean v11, v5, Landroid/app/ActivityThread$ActivityClientRecord;->hideForNow:Z

    #@cf
    .line 3706
    iput-object v10, v5, Landroid/app/ActivityThread$ActivityClientRecord;->nextIdle:Landroid/app/ActivityThread$ActivityClientRecord;

    #@d1
    .line 3708
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@d3
    if-eqz v7, :cond_dd

    #@d5
    .line 3709
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@d7
    if-nez v7, :cond_f5

    #@d9
    .line 3710
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@db
    iput-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@dd
    .line 3715
    :cond_dd
    :goto_dd
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@df
    if-eqz v7, :cond_e9

    #@e1
    .line 3716
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@e3
    if-nez v7, :cond_fd

    #@e5
    .line 3717
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@e7
    iput-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@e9
    .line 3722
    :cond_e9
    :goto_e9
    iget-boolean v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@eb
    iput-boolean v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@ed
    .line 3724
    invoke-direct {p0, v5, v3}, Landroid/app/ActivityThread;->handleLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)V

    #@f0
    goto/16 :goto_36

    #@f2
    .line 3653
    .end local v0           #N:I
    .end local v3           #currentIntent:Landroid/content/Intent;
    .end local v4           #i:I
    .end local v5           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    .end local v6           #token:Landroid/os/IBinder;
    :catchall_f2
    move-exception v7

    #@f3
    :try_start_f3
    monitor-exit v8
    :try_end_f4
    .catchall {:try_start_f3 .. :try_end_f4} :catchall_f2

    #@f4
    throw v7

    #@f5
    .line 3712
    .restart local v0       #N:I
    .restart local v3       #currentIntent:Landroid/content/Intent;
    .restart local v4       #i:I
    .restart local v5       #r:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v6       #token:Landroid/os/IBinder;
    :cond_f5
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@f7
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@f9
    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@fc
    goto :goto_dd

    #@fd
    .line 3719
    :cond_fd
    iget-object v7, v5, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@ff
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@101
    invoke-interface {v7, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@104
    goto :goto_e9
.end method

.method private handleRequestThumbnail(Landroid/os/IBinder;)V
    .registers 9
    .parameter "token"

    #@0
    .prologue
    .line 3728
    iget-object v4, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 3729
    .local v2, r:Landroid/app/ActivityThread$ActivityClientRecord;
    invoke-direct {p0, v2}, Landroid/app/ActivityThread;->createThumbnailBitmap(Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/graphics/Bitmap;

    #@b
    move-result-object v3

    #@c
    .line 3730
    .local v3, thumbnail:Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    #@d
    .line 3732
    .local v0, description:Ljava/lang/CharSequence;
    :try_start_d
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@f
    invoke-virtual {v4}, Landroid/app/Activity;->onCreateDescription()Ljava/lang/CharSequence;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_12} :catch_1b

    #@12
    move-result-object v0

    #@13
    .line 3743
    :cond_13
    :try_start_13
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@16
    move-result-object v4

    #@17
    invoke-interface {v4, p1, v3, v0}, Landroid/app/IActivityManager;->reportThumbnail(Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_1a} :catch_57

    #@1a
    .line 3747
    :goto_1a
    return-void

    #@1b
    .line 3733
    :catch_1b
    move-exception v1

    #@1c
    .line 3734
    .local v1, e:Ljava/lang/Exception;
    iget-object v4, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@1e
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@20
    invoke-virtual {v4, v5, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@23
    move-result v4

    #@24
    if-nez v4, :cond_13

    #@26
    .line 3735
    new-instance v4, Ljava/lang/RuntimeException;

    #@28
    new-instance v5, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v6, "Unable to create description of activity "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    iget-object v6, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@35
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    const-string v6, ": "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@56
    throw v4

    #@57
    .line 3745
    .end local v1           #e:Ljava/lang/Exception;
    :catch_57
    move-exception v4

    #@58
    goto :goto_1a
.end method

.method private handleSendResult(Landroid/app/ActivityThread$ResultData;)V
    .registers 9
    .parameter "res"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3361
    iget-object v5, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@4
    iget-object v6, p1, Landroid/app/ActivityThread$ResultData;->token:Landroid/os/IBinder;

    #@6
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Landroid/app/ActivityThread$ActivityClientRecord;

    #@c
    .line 3363
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v1, :cond_ba

    #@e
    .line 3364
    iget-boolean v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@10
    if-nez v5, :cond_6c

    #@12
    move v2, v3

    #@13
    .line 3365
    .local v2, resumed:Z
    :goto_13
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@15
    iget-boolean v5, v5, Landroid/app/Activity;->mFinished:Z

    #@17
    if-nez v5, :cond_28

    #@19
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1b
    iget-object v5, v5, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@1d
    if-eqz v5, :cond_28

    #@1f
    iget-boolean v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->hideForNow:Z

    #@21
    if-eqz v5, :cond_28

    #@23
    if-eqz v2, :cond_28

    #@25
    .line 3370
    invoke-direct {p0, v1, v3}, Landroid/app/ActivityThread;->updateVisibility(Landroid/app/ActivityThread$ActivityClientRecord;Z)V

    #@28
    .line 3372
    :cond_28
    if-eqz v2, :cond_aa

    #@2a
    .line 3375
    :try_start_2a
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2c
    const/4 v5, 0x0

    #@2d
    iput-boolean v5, v3, Landroid/app/Activity;->mCalled:Z

    #@2f
    .line 3376
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@31
    const/4 v5, 0x1

    #@32
    iput-boolean v5, v3, Landroid/app/Activity;->mTemporaryPause:Z

    #@34
    .line 3377
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@36
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@38
    invoke-virtual {v3, v5}, Landroid/app/Instrumentation;->callActivityOnPause(Landroid/app/Activity;)V

    #@3b
    .line 3378
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@3d
    iget-boolean v3, v3, Landroid/app/Activity;->mCalled:Z

    #@3f
    if-nez v3, :cond_aa

    #@41
    .line 3379
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "Activity "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    iget-object v6, v1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@50
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    const-string v6, " did not call through to super.onPause()"

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    invoke-direct {v3, v5}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@69
    throw v3
    :try_end_6a
    .catch Landroid/app/SuperNotCalledException; {:try_start_2a .. :try_end_6a} :catch_6a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_6a} :catch_6e

    #@6a
    .line 3383
    :catch_6a
    move-exception v0

    #@6b
    .line 3384
    .local v0, e:Landroid/app/SuperNotCalledException;
    throw v0

    #@6c
    .end local v0           #e:Landroid/app/SuperNotCalledException;
    .end local v2           #resumed:Z
    :cond_6c
    move v2, v4

    #@6d
    .line 3364
    goto :goto_13

    #@6e
    .line 3385
    .restart local v2       #resumed:Z
    :catch_6e
    move-exception v0

    #@6f
    .line 3386
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@71
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@73
    invoke-virtual {v3, v5, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@76
    move-result v3

    #@77
    if-nez v3, :cond_aa

    #@79
    .line 3387
    new-instance v3, Ljava/lang/RuntimeException;

    #@7b
    new-instance v4, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v5, "Unable to pause activity "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@88
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@8f
    move-result-object v5

    #@90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    const-string v5, ": "

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@9d
    move-result-object v5

    #@9e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v4

    #@a6
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a9
    throw v3

    #@aa
    .line 3394
    .end local v0           #e:Ljava/lang/Exception;
    :cond_aa
    iget-object v3, p1, Landroid/app/ActivityThread$ResultData;->results:Ljava/util/List;

    #@ac
    invoke-direct {p0, v1, v3}, Landroid/app/ActivityThread;->deliverResults(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V

    #@af
    .line 3395
    if-eqz v2, :cond_ba

    #@b1
    .line 3396
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b3
    invoke-virtual {v3}, Landroid/app/Activity;->performResume()V

    #@b6
    .line 3397
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b8
    iput-boolean v4, v3, Landroid/app/Activity;->mTemporaryPause:Z

    #@ba
    .line 3400
    .end local v2           #resumed:Z
    :cond_ba
    return-void
.end method

.method private handleServiceArgs(Landroid/app/ActivityThread$ServiceArgsData;)V
    .registers 9
    .parameter "data"

    #@0
    .prologue
    .line 2670
    iget-object v3, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@2
    iget-object v4, p1, Landroid/app/ActivityThread$ServiceArgsData;->token:Landroid/os/IBinder;

    #@4
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/app/Service;

    #@a
    .line 2671
    .local v2, s:Landroid/app/Service;
    if-eqz v2, :cond_39

    #@c
    .line 2673
    :try_start_c
    iget-object v3, p1, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@e
    if-eqz v3, :cond_19

    #@10
    .line 2674
    iget-object v3, p1, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@12
    invoke-virtual {v2}, Landroid/app/Service;->getClassLoader()Ljava/lang/ClassLoader;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@19
    .line 2677
    :cond_19
    iget-boolean v3, p1, Landroid/app/ActivityThread$ServiceArgsData;->taskRemoved:Z

    #@1b
    if-nez v3, :cond_3a

    #@1d
    .line 2678
    iget-object v3, p1, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@1f
    iget v4, p1, Landroid/app/ActivityThread$ServiceArgsData;->flags:I

    #@21
    iget v5, p1, Landroid/app/ActivityThread$ServiceArgsData;->startId:I

    #@23
    invoke-virtual {v2, v3, v4, v5}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    #@26
    move-result v1

    #@27
    .line 2684
    .local v1, res:I
    :goto_27
    invoke-static {}, Landroid/app/QueuedWork;->waitToFinish()V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2a} :catch_42

    #@2a
    .line 2687
    :try_start_2a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2d
    move-result-object v3

    #@2e
    iget-object v4, p1, Landroid/app/ActivityThread$ServiceArgsData;->token:Landroid/os/IBinder;

    #@30
    const/4 v5, 0x1

    #@31
    iget v6, p1, Landroid/app/ActivityThread$ServiceArgsData;->startId:I

    #@33
    invoke-interface {v3, v4, v5, v6, v1}, Landroid/app/IActivityManager;->serviceDoneExecuting(Landroid/os/IBinder;III)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_36} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_36} :catch_42

    #@36
    .line 2692
    :goto_36
    :try_start_36
    invoke-virtual {p0}, Landroid/app/ActivityThread;->ensureJitEnabled()V

    #@39
    .line 2701
    .end local v1           #res:I
    :cond_39
    return-void

    #@3a
    .line 2680
    :cond_3a
    iget-object v3, p1, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@3c
    invoke-virtual {v2, v3}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_3f} :catch_42

    #@3f
    .line 2681
    const/16 v1, 0x3e8

    #@41
    .restart local v1       #res:I
    goto :goto_27

    #@42
    .line 2693
    .end local v1           #res:I
    :catch_42
    move-exception v0

    #@43
    .line 2694
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@45
    invoke-virtual {v3, v2, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@48
    move-result v3

    #@49
    if-nez v3, :cond_39

    #@4b
    .line 2695
    new-instance v3, Ljava/lang/RuntimeException;

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, "Unable to start service "

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    const-string v5, " with "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    iget-object v5, p1, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    const-string v5, ": "

    #@6a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v4

    #@7a
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@7d
    throw v3

    #@7e
    .line 2689
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #res:I
    :catch_7e
    move-exception v3

    #@7f
    goto :goto_36
.end method

.method private handleSetCoreSettings(Landroid/os/Bundle;)V
    .registers 4
    .parameter "coreSettings"

    #@0
    .prologue
    .line 3319
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 3320
    :try_start_3
    iput-object p1, p0, Landroid/app/ActivityThread;->mCoreSettings:Landroid/os/Bundle;

    #@5
    .line 3321
    monitor-exit v1

    #@6
    .line 3322
    return-void

    #@7
    .line 3321
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method private handleSleeping(Landroid/os/IBinder;Z)V
    .registers 8
    .parameter "token"
    .parameter "sleeping"

    #@0
    .prologue
    .line 3277
    iget-object v2, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 3279
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-nez v1, :cond_23

    #@a
    .line 3280
    const-string v2, "ActivityThread"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "handleSleeping: no activity for token "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 3316
    :cond_22
    :goto_22
    return-void

    #@23
    .line 3284
    :cond_23
    if-eqz p2, :cond_88

    #@25
    .line 3285
    iget-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@27
    if-nez v2, :cond_37

    #@29
    invoke-virtual {v1}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@2c
    move-result v2

    #@2d
    if-nez v2, :cond_37

    #@2f
    .line 3288
    :try_start_2f
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@31
    invoke-virtual {v2}, Landroid/app/Activity;->performStop()V
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_34} :catch_4c

    #@34
    .line 3297
    :cond_34
    const/4 v2, 0x1

    #@35
    iput-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@37
    .line 3301
    :cond_37
    invoke-virtual {v1}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@3a
    move-result v2

    #@3b
    if-nez v2, :cond_40

    #@3d
    .line 3302
    invoke-static {}, Landroid/app/QueuedWork;->waitToFinish()V

    #@40
    .line 3307
    :cond_40
    :try_start_40
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@43
    move-result-object v2

    #@44
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@46
    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->activitySlept(Landroid/os/IBinder;)V
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_40 .. :try_end_49} :catch_4a

    #@49
    goto :goto_22

    #@4a
    .line 3308
    :catch_4a
    move-exception v2

    #@4b
    goto :goto_22

    #@4c
    .line 3289
    :catch_4c
    move-exception v0

    #@4d
    .line 3290
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@4f
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@51
    invoke-virtual {v2, v3, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@54
    move-result v2

    #@55
    if-nez v2, :cond_34

    #@57
    .line 3291
    new-instance v2, Ljava/lang/RuntimeException;

    #@59
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v4, "Unable to stop activity "

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    iget-object v4, v1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@66
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    const-string v4, ": "

    #@74
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v3

    #@78
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@87
    throw v2

    #@88
    .line 3311
    .end local v0           #e:Ljava/lang/Exception;
    :cond_88
    iget-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@8a
    if-eqz v2, :cond_22

    #@8c
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@8e
    iget-boolean v2, v2, Landroid/app/Activity;->mVisibleFromServer:Z

    #@90
    if-eqz v2, :cond_22

    #@92
    .line 3312
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@94
    invoke-virtual {v2}, Landroid/app/Activity;->performRestart()V

    #@97
    .line 3313
    const/4 v2, 0x0

    #@98
    iput-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@9a
    goto :goto_22
.end method

.method private handleStopActivity(Landroid/os/IBinder;ZI)V
    .registers 8
    .parameter "token"
    .parameter "show"
    .parameter "configChanges"

    #@0
    .prologue
    .line 3216
    iget-object v2, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 3217
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@a
    iget v3, v2, Landroid/app/Activity;->mConfigChangeFlags:I

    #@c
    or-int/2addr v3, p3

    #@d
    iput v3, v2, Landroid/app/Activity;->mConfigChangeFlags:I

    #@f
    .line 3219
    new-instance v0, Landroid/app/ActivityThread$StopInfo;

    #@11
    const/4 v2, 0x0

    #@12
    invoke-direct {v0, v2}, Landroid/app/ActivityThread$StopInfo;-><init>(Landroid/app/ActivityThread$1;)V

    #@15
    .line 3220
    .local v0, info:Landroid/app/ActivityThread$StopInfo;
    const/4 v2, 0x1

    #@16
    invoke-direct {p0, v1, v0, p2, v2}, Landroid/app/ActivityThread;->performStopActivityInner(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/ActivityThread$StopInfo;ZZ)V

    #@19
    .line 3226
    invoke-direct {p0, v1, p2}, Landroid/app/ActivityThread;->updateVisibility(Landroid/app/ActivityThread$ActivityClientRecord;Z)V

    #@1c
    .line 3229
    invoke-virtual {v1}, Landroid/app/ActivityThread$ActivityClientRecord;->isPreHoneycomb()Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_25

    #@22
    .line 3230
    invoke-static {}, Landroid/app/QueuedWork;->waitToFinish()V

    #@25
    .line 3238
    :cond_25
    iput-object v1, v0, Landroid/app/ActivityThread$StopInfo;->activity:Landroid/app/ActivityThread$ActivityClientRecord;

    #@27
    .line 3239
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@29
    iput-object v2, v0, Landroid/app/ActivityThread$StopInfo;->state:Landroid/os/Bundle;

    #@2b
    .line 3240
    iget-object v2, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@2d
    invoke-virtual {v2, v0}, Landroid/app/ActivityThread$H;->post(Ljava/lang/Runnable;)Z

    #@30
    .line 3241
    return-void
.end method

.method private handleStopService(Landroid/os/IBinder;)V
    .registers 10
    .parameter "token"

    #@0
    .prologue
    .line 2704
    iget-object v4, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@2
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Landroid/app/Service;

    #@8
    .line 2705
    .local v2, s:Landroid/app/Service;
    if-eqz v2, :cond_2d

    #@a
    .line 2708
    :try_start_a
    invoke-virtual {v2}, Landroid/app/Service;->onDestroy()V

    #@d
    .line 2709
    invoke-virtual {v2}, Landroid/app/Service;->getBaseContext()Landroid/content/Context;

    #@10
    move-result-object v0

    #@11
    .line 2710
    .local v0, context:Landroid/content/Context;
    instance-of v4, v0, Landroid/app/ContextImpl;

    #@13
    if-eqz v4, :cond_20

    #@15
    .line 2711
    invoke-virtual {v2}, Landroid/app/Service;->getClassName()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 2712
    .local v3, who:Ljava/lang/String;
    check-cast v0, Landroid/app/ContextImpl;

    #@1b
    .end local v0           #context:Landroid/content/Context;
    const-string v4, "Service"

    #@1d
    invoke-virtual {v0, v3, v4}, Landroid/app/ContextImpl;->scheduleFinalCleanup(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 2715
    .end local v3           #who:Ljava/lang/String;
    :cond_20
    invoke-static {}, Landroid/app/QueuedWork;->waitToFinish()V
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_23} :catch_2e

    #@23
    .line 2718
    :try_start_23
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@26
    move-result-object v4

    #@27
    const/4 v5, 0x0

    #@28
    const/4 v6, 0x0

    #@29
    const/4 v7, 0x0

    #@2a
    invoke-interface {v4, p1, v5, v6, v7}, Landroid/app/IActivityManager;->serviceDoneExecuting(Landroid/os/IBinder;III)V
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_2d} :catch_5e
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_2d} :catch_2e

    #@2d
    .line 2732
    :cond_2d
    :goto_2d
    return-void

    #@2e
    .line 2723
    :catch_2e
    move-exception v1

    #@2f
    .line 2724
    .local v1, e:Ljava/lang/Exception;
    iget-object v4, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@31
    invoke-virtual {v4, v2, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@34
    move-result v4

    #@35
    if-nez v4, :cond_2d

    #@37
    .line 2725
    new-instance v4, Ljava/lang/RuntimeException;

    #@39
    new-instance v5, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v6, "Unable to stop service "

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    const-string v6, ": "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5d
    throw v4

    #@5e
    .line 2720
    .end local v1           #e:Ljava/lang/Exception;
    :catch_5e
    move-exception v4

    #@5f
    goto :goto_2d
.end method

.method private handleUnbindService(Landroid/app/ActivityThread$BindServiceData;)V
    .registers 10
    .parameter "data"

    #@0
    .prologue
    .line 2599
    iget-object v3, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@2
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@4
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/app/Service;

    #@a
    .line 2600
    .local v2, s:Landroid/app/Service;
    if-eqz v2, :cond_28

    #@c
    .line 2602
    :try_start_c
    iget-object v3, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@e
    invoke-virtual {v2}, Landroid/app/Service;->getClassLoader()Ljava/lang/ClassLoader;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@15
    .line 2603
    iget-object v3, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@17
    invoke-virtual {v2, v3}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_1a} :catch_38

    #@1a
    move-result v0

    #@1b
    .line 2605
    .local v0, doRebind:Z
    if-eqz v0, :cond_29

    #@1d
    .line 2606
    :try_start_1d
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@20
    move-result-object v3

    #@21
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@23
    iget-object v5, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@25
    invoke-interface {v3, v4, v5, v0}, Landroid/app/IActivityManager;->unbindFinished(Landroid/os/IBinder;Landroid/content/Intent;Z)V

    #@28
    .line 2622
    .end local v0           #doRebind:Z
    :cond_28
    :goto_28
    return-void

    #@29
    .line 2609
    .restart local v0       #doRebind:Z
    :cond_29
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2c
    move-result-object v3

    #@2d
    iget-object v4, p1, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@2f
    const/4 v5, 0x0

    #@30
    const/4 v6, 0x0

    #@31
    const/4 v7, 0x0

    #@32
    invoke-interface {v3, v4, v5, v6, v7}, Landroid/app/IActivityManager;->serviceDoneExecuting(Landroid/os/IBinder;III)V
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_35} :catch_36
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_35} :catch_38

    #@35
    goto :goto_28

    #@36
    .line 2612
    :catch_36
    move-exception v3

    #@37
    goto :goto_28

    #@38
    .line 2614
    .end local v0           #doRebind:Z
    :catch_38
    move-exception v1

    #@39
    .line 2615
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@3b
    invoke-virtual {v3, v2, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@3e
    move-result v3

    #@3f
    if-nez v3, :cond_28

    #@41
    .line 2616
    new-instance v3, Ljava/lang/RuntimeException;

    #@43
    new-instance v4, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v5, "Unable to unbind to service "

    #@4a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    const-string v5, " with "

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    iget-object v5, p1, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    const-string v5, ": "

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@73
    throw v3
.end method

.method private handleUpdatePackageCompatibilityInfo(Landroid/app/ActivityThread$UpdateCompatibilityData;)V
    .registers 5
    .parameter "data"

    #@0
    .prologue
    .line 3325
    iget-object v1, p1, Landroid/app/ActivityThread$UpdateCompatibilityData;->pkg:Ljava/lang/String;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p0, v1, v2}, Landroid/app/ActivityThread;->peekPackageInfo(Ljava/lang/String;Z)Landroid/app/LoadedApk;

    #@6
    move-result-object v0

    #@7
    .line 3326
    .local v0, apk:Landroid/app/LoadedApk;
    if-eqz v0, :cond_10

    #@9
    .line 3327
    iget-object v1, v0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@b
    iget-object v2, p1, Landroid/app/ActivityThread$UpdateCompatibilityData;->info:Landroid/content/res/CompatibilityInfo;

    #@d
    invoke-virtual {v1, v2}, Landroid/view/CompatibilityInfoHolder;->set(Landroid/content/res/CompatibilityInfo;)V

    #@10
    .line 3329
    :cond_10
    iget-object v1, p1, Landroid/app/ActivityThread$UpdateCompatibilityData;->pkg:Ljava/lang/String;

    #@12
    const/4 v2, 0x1

    #@13
    invoke-virtual {p0, v1, v2}, Landroid/app/ActivityThread;->peekPackageInfo(Ljava/lang/String;Z)Landroid/app/LoadedApk;

    #@16
    move-result-object v0

    #@17
    .line 3330
    if-eqz v0, :cond_20

    #@19
    .line 3331
    iget-object v1, v0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@1b
    iget-object v2, p1, Landroid/app/ActivityThread$UpdateCompatibilityData;->info:Landroid/content/res/CompatibilityInfo;

    #@1d
    invoke-virtual {v1, v2}, Landroid/view/CompatibilityInfoHolder;->set(Landroid/content/res/CompatibilityInfo;)V

    #@20
    .line 3333
    :cond_20
    iget-object v1, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@22
    iget-object v2, p1, Landroid/app/ActivityThread$UpdateCompatibilityData;->info:Landroid/content/res/CompatibilityInfo;

    #@24
    invoke-virtual {p0, v1, v2}, Landroid/app/ActivityThread;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@27
    .line 3334
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@2d
    invoke-virtual {v1, v2}, Landroid/view/WindowManagerGlobal;->reportNewConfiguration(Landroid/content/res/Configuration;)V

    #@30
    .line 3335
    return-void
.end method

.method private handleWindowVisibility(Landroid/os/IBinder;Z)V
    .registers 7
    .parameter "token"
    .parameter "show"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3252
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@9
    .line 3254
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-nez v0, :cond_24

    #@b
    .line 3255
    const-string v1, "ActivityThread"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "handleWindowVisibility: no activity for token "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 3274
    :cond_23
    :goto_23
    return-void

    #@24
    .line 3259
    :cond_24
    if-nez p2, :cond_38

    #@26
    iget-boolean v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@28
    if-nez v1, :cond_38

    #@2a
    .line 3260
    const/4 v1, 0x0

    #@2b
    invoke-direct {p0, v0, v1, p2, v2}, Landroid/app/ActivityThread;->performStopActivityInner(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/ActivityThread$StopInfo;ZZ)V

    #@2e
    .line 3269
    :cond_2e
    :goto_2e
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@30
    iget-object v1, v1, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@32
    if-eqz v1, :cond_23

    #@34
    .line 3272
    invoke-direct {p0, v0, p2}, Landroid/app/ActivityThread;->updateVisibility(Landroid/app/ActivityThread$ActivityClientRecord;Z)V

    #@37
    goto :goto_23

    #@38
    .line 3261
    :cond_38
    if-eqz p2, :cond_2e

    #@3a
    iget-boolean v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@3c
    if-eqz v1, :cond_2e

    #@3e
    .line 3264
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@41
    .line 3266
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@43
    invoke-virtual {v1}, Landroid/app/Activity;->performRestart()V

    #@46
    .line 3267
    iput-boolean v2, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@48
    goto :goto_2e
.end method

.method private final incProviderRefLocked(Landroid/app/ActivityThread$ProviderRefCount;Z)V
    .registers 9
    .parameter "prc"
    .parameter "stable"

    #@0
    .prologue
    const/16 v5, 0x83

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v3, 0x1

    #@4
    .line 4595
    if-eqz p2, :cond_46

    #@6
    .line 4596
    iget v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@8
    add-int/lit8 v2, v2, 0x1

    #@a
    iput v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@c
    .line 4597
    iget v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@e
    if-ne v2, v3, :cond_28

    #@10
    .line 4600
    iget-boolean v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@12
    if-eqz v2, :cond_29

    #@14
    .line 4604
    const/4 v1, -0x1

    #@15
    .line 4610
    .local v1, unstableDelta:I
    iput-boolean v4, p1, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@17
    .line 4611
    iget-object v2, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@19
    invoke-virtual {v2, v5, p1}, Landroid/app/ActivityThread$H;->removeMessages(ILjava/lang/Object;)V

    #@1c
    .line 4621
    :goto_1c
    :try_start_1c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1f
    move-result-object v2

    #@20
    iget-object v3, p1, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@22
    iget-object v3, v3, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@24
    const/4 v4, 0x1

    #@25
    invoke-interface {v2, v3, v4, v1}, Landroid/app/IActivityManager;->refContentProvider(Landroid/os/IBinder;II)Z
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_28} :catch_87
    .catch Ljava/lang/IllegalStateException; {:try_start_1c .. :try_end_28} :catch_2b

    #@28
    .line 4662
    .end local v1           #unstableDelta:I
    :cond_28
    :goto_28
    return-void

    #@29
    .line 4613
    :cond_29
    const/4 v1, 0x0

    #@2a
    .restart local v1       #unstableDelta:I
    goto :goto_1c

    #@2b
    .line 4625
    :catch_2b
    move-exception v0

    #@2c
    .line 4626
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v2, "ActivityThread"

    #@2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string/jumbo v4, "state problem : "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v3

    #@42
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_28

    #@46
    .line 4630
    .end local v0           #e:Ljava/lang/IllegalStateException;
    .end local v1           #unstableDelta:I
    :cond_46
    iget v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@48
    add-int/lit8 v2, v2, 0x1

    #@4a
    iput v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@4c
    .line 4631
    iget v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@4e
    if-ne v2, v3, :cond_28

    #@50
    .line 4633
    iget-boolean v2, p1, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@52
    if-eqz v2, :cond_5c

    #@54
    .line 4642
    iput-boolean v4, p1, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@56
    .line 4643
    iget-object v2, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@58
    invoke-virtual {v2, v5, p1}, Landroid/app/ActivityThread$H;->removeMessages(ILjava/lang/Object;)V

    #@5b
    goto :goto_28

    #@5c
    .line 4652
    :cond_5c
    :try_start_5c
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5f
    move-result-object v2

    #@60
    iget-object v3, p1, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@62
    iget-object v3, v3, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@64
    const/4 v4, 0x0

    #@65
    const/4 v5, 0x1

    #@66
    invoke-interface {v2, v3, v4, v5}, Landroid/app/IActivityManager;->refContentProvider(Landroid/os/IBinder;II)Z
    :try_end_69
    .catch Landroid/os/RemoteException; {:try_start_5c .. :try_end_69} :catch_6a
    .catch Ljava/lang/IllegalStateException; {:try_start_5c .. :try_end_69} :catch_6c

    #@69
    goto :goto_28

    #@6a
    .line 4654
    :catch_6a
    move-exception v2

    #@6b
    goto :goto_28

    #@6c
    .line 4656
    :catch_6c
    move-exception v0

    #@6d
    .line 4657
    .restart local v0       #e:Ljava/lang/IllegalStateException;
    const-string v2, "ActivityThread"

    #@6f
    new-instance v3, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string/jumbo v4, "state problem : "

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_28

    #@87
    .line 4623
    .end local v0           #e:Ljava/lang/IllegalStateException;
    .restart local v1       #unstableDelta:I
    :catch_87
    move-exception v2

    #@88
    goto :goto_28
.end method

.method private installContentProviders(Landroid/content/Context;Ljava/util/List;)V
    .registers 13
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ProviderInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    const/4 v5, 0x1

    #@1
    .line 4536
    new-instance v9, Ljava/util/ArrayList;

    #@3
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 4539
    .local v9, results:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/IActivityManager$ContentProviderHolder;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v8

    #@a
    .local v8, i$:Ljava/util/Iterator;
    :cond_a
    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_27

    #@10
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Landroid/content/pm/ProviderInfo;

    #@16
    .line 4548
    .local v3, cpi:Landroid/content/pm/ProviderInfo;
    const/4 v2, 0x0

    #@17
    const/4 v4, 0x0

    #@18
    move-object v0, p0

    #@19
    move-object v1, p1

    #@1a
    move v6, v5

    #@1b
    invoke-direct/range {v0 .. v6}, Landroid/app/ActivityThread;->installProvider(Landroid/content/Context;Landroid/app/IActivityManager$ContentProviderHolder;Landroid/content/pm/ProviderInfo;ZZZ)Landroid/app/IActivityManager$ContentProviderHolder;

    #@1e
    move-result-object v7

    #@1f
    .line 4550
    .local v7, cph:Landroid/app/IActivityManager$ContentProviderHolder;
    if-eqz v7, :cond_a

    #@21
    .line 4551
    iput-boolean v5, v7, Landroid/app/IActivityManager$ContentProviderHolder;->noReleaseNeeded:Z

    #@23
    .line 4552
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    goto :goto_a

    #@27
    .line 4557
    .end local v3           #cpi:Landroid/content/pm/ProviderInfo;
    .end local v7           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    :cond_27
    :try_start_27
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@2e
    move-result-object v1

    #@2f
    invoke-interface {v0, v1, v9}, Landroid/app/IActivityManager;->publishContentProviders(Landroid/app/IApplicationThread;Ljava/util/List;)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_32} :catch_33

    #@32
    .line 4561
    :goto_32
    return-void

    #@33
    .line 4559
    :catch_33
    move-exception v0

    #@34
    goto :goto_32
.end method

.method private installProvider(Landroid/content/Context;Landroid/app/IActivityManager$ContentProviderHolder;Landroid/content/pm/ProviderInfo;ZZZ)Landroid/app/IActivityManager$ContentProviderHolder;
    .registers 25
    .parameter "context"
    .parameter "holder"
    .parameter "info"
    .parameter "noisy"
    .parameter "noReleaseNeeded"
    .parameter "stable"

    #@0
    .prologue
    .line 4900
    const/4 v10, 0x0

    #@1
    .line 4902
    .local v10, localProvider:Landroid/content/ContentProvider;
    if-eqz p2, :cond_9

    #@3
    move-object/from16 v0, p2

    #@5
    iget-object v15, v0, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@7
    if-nez v15, :cond_172

    #@9
    .line 4903
    :cond_9
    if-eqz p4, :cond_39

    #@b
    .line 4904
    const-string v15, "ActivityThread"

    #@d
    new-instance v16, Ljava/lang/StringBuilder;

    #@f
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v17, "Loading provider "

    #@14
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v16

    #@18
    move-object/from16 v0, p3

    #@1a
    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@1c
    move-object/from16 v17, v0

    #@1e
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v16

    #@22
    const-string v17, ": "

    #@24
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v16

    #@28
    move-object/from16 v0, p3

    #@2a
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2c
    move-object/from16 v17, v0

    #@2e
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v16

    #@32
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v16

    #@36
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 4907
    :cond_39
    const/4 v3, 0x0

    #@3a
    .line 4908
    .local v3, c:Landroid/content/Context;
    move-object/from16 v0, p3

    #@3c
    iget-object v2, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@3e
    .line 4909
    .local v2, ai:Landroid/content/pm/ApplicationInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@41
    move-result-object v15

    #@42
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@44
    move-object/from16 v16, v0

    #@46
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v15

    #@4a
    if-eqz v15, :cond_7e

    #@4c
    .line 4910
    move-object/from16 v3, p1

    #@4e
    .line 4922
    :goto_4e
    if-nez v3, :cond_a8

    #@50
    .line 4923
    const-string v15, "ActivityThread"

    #@52
    new-instance v16, Ljava/lang/StringBuilder;

    #@54
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v17, "Unable to get context for package "

    #@59
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v16

    #@5d
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@5f
    move-object/from16 v17, v0

    #@61
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v16

    #@65
    const-string v17, " while loading content provider "

    #@67
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v16

    #@6b
    move-object/from16 v0, p3

    #@6d
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@6f
    move-object/from16 v17, v0

    #@71
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v16

    #@75
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v16

    #@79
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 4927
    const/4 v14, 0x0

    #@7d
    .line 5017
    .end local v2           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v3           #c:Landroid/content/Context;
    :goto_7d
    return-object v14

    #@7e
    .line 4911
    .restart local v2       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v3       #c:Landroid/content/Context;
    :cond_7e
    move-object/from16 v0, p0

    #@80
    iget-object v15, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@82
    if-eqz v15, :cond_9b

    #@84
    move-object/from16 v0, p0

    #@86
    iget-object v15, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@88
    invoke-virtual {v15}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    #@8b
    move-result-object v15

    #@8c
    iget-object v0, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8e
    move-object/from16 v16, v0

    #@90
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@93
    move-result v15

    #@94
    if-eqz v15, :cond_9b

    #@96
    .line 4913
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@9a
    goto :goto_4e

    #@9b
    .line 4916
    :cond_9b
    :try_start_9b
    iget-object v15, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@9d
    const/16 v16, 0x1

    #@9f
    move-object/from16 v0, p1

    #@a1
    move/from16 v1, v16

    #@a3
    invoke-virtual {v0, v15, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_a6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9b .. :try_end_a6} :catch_206

    #@a6
    move-result-object v3

    #@a7
    goto :goto_4e

    #@a8
    .line 4930
    :cond_a8
    :try_start_a8
    invoke-virtual {v3}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@ab
    move-result-object v4

    #@ac
    .line 4931
    .local v4, cl:Ljava/lang/ClassLoader;
    move-object/from16 v0, p3

    #@ae
    iget-object v15, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@b0
    invoke-virtual {v4, v15}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@b3
    move-result-object v15

    #@b4
    invoke-virtual {v15}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@b7
    move-result-object v15

    #@b8
    move-object v0, v15

    #@b9
    check-cast v0, Landroid/content/ContentProvider;

    #@bb
    move-object v10, v0

    #@bc
    .line 4933
    invoke-virtual {v10}, Landroid/content/ContentProvider;->getIContentProvider()Landroid/content/IContentProvider;

    #@bf
    move-result-object v13

    #@c0
    .line 4934
    .local v13, provider:Landroid/content/IContentProvider;
    if-nez v13, :cond_f8

    #@c2
    .line 4935
    const-string v15, "ActivityThread"

    #@c4
    new-instance v16, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v17, "Failed to instantiate class "

    #@cb
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v16

    #@cf
    move-object/from16 v0, p3

    #@d1
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@d3
    move-object/from16 v17, v0

    #@d5
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v16

    #@d9
    const-string v17, " from sourceDir "

    #@db
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v16

    #@df
    move-object/from16 v0, p3

    #@e1
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@e3
    move-object/from16 v17, v0

    #@e5
    move-object/from16 v0, v17

    #@e7
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@e9
    move-object/from16 v17, v0

    #@eb
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v16

    #@ef
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v16

    #@f3
    invoke-static/range {v15 .. v16}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    .line 4938
    const/4 v14, 0x0

    #@f7
    goto :goto_7d

    #@f8
    .line 4943
    :cond_f8
    move-object/from16 v0, p3

    #@fa
    invoke-virtual {v10, v3, v0}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    :try_end_fd
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_fd} :catch_131

    #@fd
    .line 4960
    .end local v2           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v3           #c:Landroid/content/Context;
    .end local v4           #cl:Ljava/lang/ClassLoader;
    :goto_fd
    move-object/from16 v0, p0

    #@ff
    iget-object v0, v0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@101
    move-object/from16 v16, v0

    #@103
    monitor-enter v16

    #@104
    .line 4963
    :try_start_104
    invoke-interface {v13}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@107
    move-result-object v9

    #@108
    .line 4964
    .local v9, jBinder:Landroid/os/IBinder;
    if-eqz v10, :cond_19a

    #@10a
    .line 4965
    new-instance v6, Landroid/content/ComponentName;

    #@10c
    move-object/from16 v0, p3

    #@10e
    iget-object v15, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@110
    move-object/from16 v0, p3

    #@112
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@114
    move-object/from16 v17, v0

    #@116
    move-object/from16 v0, v17

    #@118
    invoke-direct {v6, v15, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11b
    .line 4966
    .local v6, cname:Landroid/content/ComponentName;
    move-object/from16 v0, p0

    #@11d
    iget-object v15, v0, Landroid/app/ActivityThread;->mLocalProvidersByName:Ljava/util/HashMap;

    #@11f
    invoke-virtual {v15, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@122
    move-result-object v11

    #@123
    check-cast v11, Landroid/app/ActivityThread$ProviderClientRecord;

    #@125
    .line 4967
    .local v11, pr:Landroid/app/ActivityThread$ProviderClientRecord;
    if-eqz v11, :cond_177

    #@127
    .line 4972
    iget-object v13, v11, Landroid/app/ActivityThread$ProviderClientRecord;->mProvider:Landroid/content/IContentProvider;

    #@129
    .line 4981
    :goto_129
    iget-object v14, v11, Landroid/app/ActivityThread$ProviderClientRecord;->mHolder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@12b
    .line 5015
    .end local v6           #cname:Landroid/content/ComponentName;
    .end local v11           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    .local v14, retHolder:Landroid/app/IActivityManager$ContentProviderHolder;
    :goto_12b
    monitor-exit v16

    #@12c
    goto/16 :goto_7d

    #@12e
    .end local v9           #jBinder:Landroid/os/IBinder;
    .end local v14           #retHolder:Landroid/app/IActivityManager$ContentProviderHolder;
    :catchall_12e
    move-exception v15

    #@12f
    :goto_12f
    monitor-exit v16
    :try_end_130
    .catchall {:try_start_104 .. :try_end_130} :catchall_12e

    #@130
    throw v15

    #@131
    .line 4944
    .end local v13           #provider:Landroid/content/IContentProvider;
    .restart local v2       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v3       #c:Landroid/content/Context;
    :catch_131
    move-exception v7

    #@132
    .line 4945
    .local v7, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    #@134
    iget-object v15, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@136
    const/16 v16, 0x0

    #@138
    move-object/from16 v0, v16

    #@13a
    invoke-virtual {v15, v0, v7}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@13d
    move-result v15

    #@13e
    if-nez v15, :cond_16f

    #@140
    .line 4946
    new-instance v15, Ljava/lang/RuntimeException;

    #@142
    new-instance v16, Ljava/lang/StringBuilder;

    #@144
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v17, "Unable to get provider "

    #@149
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v16

    #@14d
    move-object/from16 v0, p3

    #@14f
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@151
    move-object/from16 v17, v0

    #@153
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v16

    #@157
    const-string v17, ": "

    #@159
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v16

    #@15d
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@160
    move-result-object v17

    #@161
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v16

    #@165
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v16

    #@169
    move-object/from16 v0, v16

    #@16b
    invoke-direct {v15, v0, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@16e
    throw v15

    #@16f
    .line 4950
    :cond_16f
    const/4 v14, 0x0

    #@170
    goto/16 :goto_7d

    #@172
    .line 4953
    .end local v2           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v3           #c:Landroid/content/Context;
    .end local v7           #e:Ljava/lang/Exception;
    :cond_172
    move-object/from16 v0, p2

    #@174
    iget-object v13, v0, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@176
    .restart local v13       #provider:Landroid/content/IContentProvider;
    goto :goto_fd

    #@177
    .line 4974
    .restart local v6       #cname:Landroid/content/ComponentName;
    .restart local v9       #jBinder:Landroid/os/IBinder;
    .restart local v11       #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    :cond_177
    :try_start_177
    new-instance v8, Landroid/app/IActivityManager$ContentProviderHolder;

    #@179
    move-object/from16 v0, p3

    #@17b
    invoke-direct {v8, v0}, Landroid/app/IActivityManager$ContentProviderHolder;-><init>(Landroid/content/pm/ProviderInfo;)V
    :try_end_17e
    .catchall {:try_start_177 .. :try_end_17e} :catchall_12e

    #@17e
    .line 4975
    .end local p2
    .local v8, holder:Landroid/app/IActivityManager$ContentProviderHolder;
    :try_start_17e
    iput-object v13, v8, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@180
    .line 4976
    const/4 v15, 0x1

    #@181
    iput-boolean v15, v8, Landroid/app/IActivityManager$ContentProviderHolder;->noReleaseNeeded:Z

    #@183
    .line 4977
    move-object/from16 v0, p0

    #@185
    invoke-direct {v0, v13, v10, v8}, Landroid/app/ActivityThread;->installProviderAuthoritiesLocked(Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/IActivityManager$ContentProviderHolder;)Landroid/app/ActivityThread$ProviderClientRecord;

    #@188
    move-result-object v11

    #@189
    .line 4978
    move-object/from16 v0, p0

    #@18b
    iget-object v15, v0, Landroid/app/ActivityThread;->mLocalProviders:Ljava/util/HashMap;

    #@18d
    invoke-virtual {v15, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@190
    .line 4979
    move-object/from16 v0, p0

    #@192
    iget-object v15, v0, Landroid/app/ActivityThread;->mLocalProvidersByName:Ljava/util/HashMap;

    #@194
    invoke-virtual {v15, v6, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_197
    .catchall {:try_start_17e .. :try_end_197} :catchall_1ff

    #@197
    move-object/from16 p2, v8

    #@199
    .end local v8           #holder:Landroid/app/IActivityManager$ContentProviderHolder;
    .restart local p2
    goto :goto_129

    #@19a
    .line 4983
    .end local v6           #cname:Landroid/content/ComponentName;
    .end local v11           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    :cond_19a
    :try_start_19a
    move-object/from16 v0, p0

    #@19c
    iget-object v15, v0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@19e
    invoke-virtual {v15, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1a1
    move-result-object v12

    #@1a2
    check-cast v12, Landroid/app/ActivityThread$ProviderRefCount;

    #@1a4
    .line 4984
    .local v12, prc:Landroid/app/ActivityThread$ProviderRefCount;
    if-eqz v12, :cond_1c4

    #@1a6
    .line 4992
    if-nez p5, :cond_1c0

    #@1a8
    .line 4993
    move-object/from16 v0, p0

    #@1aa
    move/from16 v1, p6

    #@1ac
    invoke-direct {v0, v12, v1}, Landroid/app/ActivityThread;->incProviderRefLocked(Landroid/app/ActivityThread$ProviderRefCount;Z)V
    :try_end_1af
    .catchall {:try_start_19a .. :try_end_1af} :catchall_12e

    #@1af
    .line 4995
    :try_start_1af
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1b2
    move-result-object v15

    #@1b3
    move-object/from16 v0, p2

    #@1b5
    iget-object v0, v0, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@1b7
    move-object/from16 v17, v0

    #@1b9
    move-object/from16 v0, v17

    #@1bb
    move/from16 v1, p6

    #@1bd
    invoke-interface {v15, v0, v1}, Landroid/app/IActivityManager;->removeContentProvider(Landroid/os/IBinder;Z)V
    :try_end_1c0
    .catchall {:try_start_1af .. :try_end_1c0} :catchall_12e
    .catch Landroid/os/RemoteException; {:try_start_1af .. :try_end_1c0} :catch_204

    #@1c0
    .line 5013
    :cond_1c0
    :goto_1c0
    :try_start_1c0
    iget-object v14, v12, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@1c2
    .restart local v14       #retHolder:Landroid/app/IActivityManager$ContentProviderHolder;
    goto/16 :goto_12b

    #@1c4
    .line 5002
    .end local v14           #retHolder:Landroid/app/IActivityManager$ContentProviderHolder;
    :cond_1c4
    move-object/from16 v0, p0

    #@1c6
    move-object/from16 v1, p2

    #@1c8
    invoke-direct {v0, v13, v10, v1}, Landroid/app/ActivityThread;->installProviderAuthoritiesLocked(Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/IActivityManager$ContentProviderHolder;)Landroid/app/ActivityThread$ProviderClientRecord;

    #@1cb
    move-result-object v5

    #@1cc
    .line 5004
    .local v5, client:Landroid/app/ActivityThread$ProviderClientRecord;
    if-eqz p5, :cond_1e3

    #@1ce
    .line 5005
    new-instance v12, Landroid/app/ActivityThread$ProviderRefCount;

    #@1d0
    .end local v12           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    const/16 v15, 0x3e8

    #@1d2
    const/16 v17, 0x3e8

    #@1d4
    move-object/from16 v0, p2

    #@1d6
    move/from16 v1, v17

    #@1d8
    invoke-direct {v12, v0, v5, v15, v1}, Landroid/app/ActivityThread$ProviderRefCount;-><init>(Landroid/app/IActivityManager$ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V

    #@1db
    .line 5011
    .restart local v12       #prc:Landroid/app/ActivityThread$ProviderRefCount;
    :goto_1db
    move-object/from16 v0, p0

    #@1dd
    iget-object v15, v0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@1df
    invoke-virtual {v15, v9, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e2
    goto :goto_1c0

    #@1e3
    .line 5007
    :cond_1e3
    if-eqz p6, :cond_1f2

    #@1e5
    new-instance v12, Landroid/app/ActivityThread$ProviderRefCount;

    #@1e7
    .end local v12           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    const/4 v15, 0x1

    #@1e8
    const/16 v17, 0x0

    #@1ea
    move-object/from16 v0, p2

    #@1ec
    move/from16 v1, v17

    #@1ee
    invoke-direct {v12, v0, v5, v15, v1}, Landroid/app/ActivityThread$ProviderRefCount;-><init>(Landroid/app/IActivityManager$ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V

    #@1f1
    .restart local v12       #prc:Landroid/app/ActivityThread$ProviderRefCount;
    :goto_1f1
    goto :goto_1db

    #@1f2
    :cond_1f2
    new-instance v12, Landroid/app/ActivityThread$ProviderRefCount;

    #@1f4
    .end local v12           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    const/4 v15, 0x0

    #@1f5
    const/16 v17, 0x1

    #@1f7
    move-object/from16 v0, p2

    #@1f9
    move/from16 v1, v17

    #@1fb
    invoke-direct {v12, v0, v5, v15, v1}, Landroid/app/ActivityThread$ProviderRefCount;-><init>(Landroid/app/IActivityManager$ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V
    :try_end_1fe
    .catchall {:try_start_1c0 .. :try_end_1fe} :catchall_12e

    #@1fe
    goto :goto_1f1

    #@1ff
    .line 5015
    .end local v5           #client:Landroid/app/ActivityThread$ProviderClientRecord;
    .end local p2
    .restart local v6       #cname:Landroid/content/ComponentName;
    .restart local v8       #holder:Landroid/app/IActivityManager$ContentProviderHolder;
    .restart local v11       #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    :catchall_1ff
    move-exception v15

    #@200
    move-object/from16 p2, v8

    #@202
    .end local v8           #holder:Landroid/app/IActivityManager$ContentProviderHolder;
    .restart local p2
    goto/16 :goto_12f

    #@204
    .line 4997
    .end local v6           #cname:Landroid/content/ComponentName;
    .end local v11           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    .restart local v12       #prc:Landroid/app/ActivityThread$ProviderRefCount;
    :catch_204
    move-exception v15

    #@205
    goto :goto_1c0

    #@206
    .line 4918
    .end local v9           #jBinder:Landroid/os/IBinder;
    .end local v12           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    .end local v13           #provider:Landroid/content/IContentProvider;
    .restart local v2       #ai:Landroid/content/pm/ApplicationInfo;
    .restart local v3       #c:Landroid/content/Context;
    :catch_206
    move-exception v15

    #@207
    goto/16 :goto_4e
.end method

.method private installProviderAuthoritiesLocked(Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/IActivityManager$ContentProviderHolder;)Landroid/app/ActivityThread$ProviderClientRecord;
    .registers 18
    .parameter "provider"
    .parameter "localProvider"
    .parameter "holder"

    #@0
    .prologue
    .line 4865
    sget-object v2, Landroid/app/ActivityThread;->PATTERN_SEMICOLON:Ljava/util/regex/Pattern;

    #@2
    move-object/from16 v0, p3

    #@4
    iget-object v4, v0, Landroid/app/IActivityManager$ContentProviderHolder;->info:Landroid/content/pm/ProviderInfo;

    #@6
    iget-object v4, v4, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    #@8
    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 4866
    .local v3, auths:[Ljava/lang/String;
    move-object/from16 v0, p3

    #@e
    iget-object v2, v0, Landroid/app/IActivityManager$ContentProviderHolder;->info:Landroid/content/pm/ProviderInfo;

    #@10
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@12
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@14
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    #@17
    move-result v13

    #@18
    .line 4868
    .local v13, userId:I
    new-instance v1, Landroid/app/ActivityThread$ProviderClientRecord;

    #@1a
    move-object v2, p0

    #@1b
    move-object v4, p1

    #@1c
    move-object/from16 v5, p2

    #@1e
    move-object/from16 v6, p3

    #@20
    invoke-direct/range {v1 .. v6}, Landroid/app/ActivityThread$ProviderClientRecord;-><init>(Landroid/app/ActivityThread;[Ljava/lang/String;Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/IActivityManager$ContentProviderHolder;)V

    #@23
    .line 4870
    .local v1, pcr:Landroid/app/ActivityThread$ProviderClientRecord;
    move-object v7, v3

    #@24
    .local v7, arr$:[Ljava/lang/String;
    array-length v12, v7

    #@25
    .local v12, len$:I
    const/4 v10, 0x0

    #@26
    .local v10, i$:I
    :goto_26
    if-ge v10, v12, :cond_6a

    #@28
    aget-object v8, v7, v10

    #@2a
    .line 4871
    .local v8, auth:Ljava/lang/String;
    new-instance v11, Landroid/app/ActivityThread$ProviderKey;

    #@2c
    invoke-direct {v11, v8, v13}, Landroid/app/ActivityThread$ProviderKey;-><init>(Ljava/lang/String;I)V

    #@2f
    .line 4872
    .local v11, key:Landroid/app/ActivityThread$ProviderKey;
    iget-object v2, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@31
    invoke-virtual {v2, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v9

    #@35
    check-cast v9, Landroid/app/ActivityThread$ProviderClientRecord;

    #@37
    .line 4873
    .local v9, existing:Landroid/app/ActivityThread$ProviderClientRecord;
    if-eqz v9, :cond_64

    #@39
    .line 4874
    const-string v2, "ActivityThread"

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "Content provider "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    iget-object v5, v1, Landroid/app/ActivityThread$ProviderClientRecord;->mHolder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@48
    iget-object v5, v5, Landroid/app/IActivityManager$ContentProviderHolder;->info:Landroid/content/pm/ProviderInfo;

    #@4a
    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v4

    #@50
    const-string v5, " already published as "

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-static {v2, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 4870
    :goto_61
    add-int/lit8 v10, v10, 0x1

    #@63
    goto :goto_26

    #@64
    .line 4877
    :cond_64
    iget-object v2, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@66
    invoke-virtual {v2, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@69
    goto :goto_61

    #@6a
    .line 4880
    .end local v8           #auth:Ljava/lang/String;
    .end local v9           #existing:Landroid/app/ActivityThread$ProviderClientRecord;
    .end local v11           #key:Landroid/app/ActivityThread$ProviderKey;
    :cond_6a
    return-object v1
.end method

.method public static main([Ljava/lang/String;)V
    .registers 5
    .parameter "args"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 5138
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->start()V

    #@4
    .line 5143
    invoke-static {v3}, Ldalvik/system/CloseGuard;->setEnabled(Z)V

    #@7
    .line 5145
    invoke-static {}, Landroid/os/Environment;->initForCurrentUser()V

    #@a
    .line 5148
    new-instance v1, Landroid/app/ActivityThread$EventLoggingReporter;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-direct {v1, v2}, Landroid/app/ActivityThread$EventLoggingReporter;-><init>(Landroid/app/ActivityThread$1;)V

    #@10
    invoke-static {v1}, Llibcore/io/EventLogger;->setReporter(Llibcore/io/EventLogger$Reporter;)V

    #@13
    .line 5150
    const-string v1, "<pre-initialized>"

    #@15
    invoke-static {v1}, Landroid/os/Process;->setArgV0(Ljava/lang/String;)V

    #@18
    .line 5152
    invoke-static {}, Landroid/os/Looper;->prepareMainLooper()V

    #@1b
    .line 5154
    new-instance v0, Landroid/app/ActivityThread;

    #@1d
    invoke-direct {v0}, Landroid/app/ActivityThread;-><init>()V

    #@20
    .line 5155
    .local v0, thread:Landroid/app/ActivityThread;
    invoke-direct {v0, v3}, Landroid/app/ActivityThread;->attach(Z)V

    #@23
    .line 5157
    sget-object v1, Landroid/app/ActivityThread;->sMainThreadHandler:Landroid/os/Handler;

    #@25
    if-nez v1, :cond_2d

    #@27
    .line 5158
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@2a
    move-result-object v1

    #@2b
    sput-object v1, Landroid/app/ActivityThread;->sMainThreadHandler:Landroid/os/Handler;

    #@2d
    .line 5161
    :cond_2d
    invoke-static {}, Landroid/os/AsyncTask;->init()V

    #@30
    .line 5168
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@33
    .line 5170
    new-instance v1, Ljava/lang/RuntimeException;

    #@35
    const-string v2, "Main thread loop unexpectedly exited"

    #@37
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v1
.end method

.method private static performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V
    .registers 8
    .parameter "cb"
    .parameter "config"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3805
    instance-of v3, p0, Landroid/app/Activity;

    #@3
    if-eqz v3, :cond_43

    #@5
    move-object v3, p0

    #@6
    check-cast v3, Landroid/app/Activity;

    #@8
    move-object v0, v3

    #@9
    .line 3806
    .local v0, activity:Landroid/app/Activity;
    :goto_9
    if-eqz v0, :cond_d

    #@b
    .line 3807
    iput-boolean v4, v0, Landroid/app/Activity;->mCalled:Z

    #@d
    .line 3810
    :cond_d
    const/4 v2, 0x0

    #@e
    .line 3811
    .local v2, shouldChangeConfig:Z
    if-eqz v0, :cond_14

    #@10
    iget-object v3, v0, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@12
    if-nez v3, :cond_45

    #@14
    .line 3812
    :cond_14
    const/4 v2, 0x1

    #@15
    .line 3831
    :cond_15
    :goto_15
    if-eqz v2, :cond_63

    #@17
    .line 3832
    invoke-interface {p0, p1}, Landroid/content/ComponentCallbacks2;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@1a
    .line 3834
    if-eqz v0, :cond_63

    #@1c
    .line 3835
    iget-boolean v3, v0, Landroid/app/Activity;->mCalled:Z

    #@1e
    if-nez v3, :cond_5a

    #@20
    .line 3836
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "Activity "

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    const-string v5, " did not call through to super.onConfigurationChanged()"

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@42
    throw v3

    #@43
    .line 3805
    .end local v0           #activity:Landroid/app/Activity;
    .end local v2           #shouldChangeConfig:Z
    :cond_43
    const/4 v0, 0x0

    #@44
    goto :goto_9

    #@45
    .line 3818
    .restart local v0       #activity:Landroid/app/Activity;
    .restart local v2       #shouldChangeConfig:Z
    :cond_45
    iget-object v3, v0, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@47
    invoke-virtual {v3, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@4a
    move-result v1

    #@4b
    .line 3819
    .local v1, diff:I
    if-eqz v1, :cond_15

    #@4d
    .line 3823
    iget-object v3, v0, Landroid/app/Activity;->mActivityInfo:Landroid/content/pm/ActivityInfo;

    #@4f
    invoke-virtual {v3}, Landroid/content/pm/ActivityInfo;->getRealConfigChanged()I

    #@52
    move-result v3

    #@53
    xor-int/lit8 v3, v3, -0x1

    #@55
    and-int/2addr v3, v1

    #@56
    if-nez v3, :cond_15

    #@58
    .line 3824
    const/4 v2, 0x1

    #@59
    goto :goto_15

    #@5a
    .line 3840
    .end local v1           #diff:I
    :cond_5a
    iput v4, v0, Landroid/app/Activity;->mConfigChangeFlags:I

    #@5c
    .line 3841
    new-instance v3, Landroid/content/res/Configuration;

    #@5e
    invoke-direct {v3, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@61
    iput-object v3, v0, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@63
    .line 3844
    :cond_63
    return-void
.end method

.method private performDestroyActivity(Landroid/os/IBinder;ZIZ)Landroid/app/ActivityThread$ActivityClientRecord;
    .registers 13
    .parameter "token"
    .parameter "finishing"
    .parameter "configChanges"
    .parameter "getNonConfigInstance"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 3408
    iget-object v3, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@9
    .line 3409
    .local v2, r:Landroid/app/ActivityThread$ActivityClientRecord;
    const/4 v0, 0x0

    #@a
    .line 3411
    .local v0, activityClass:Ljava/lang/Class;
    if-eqz v2, :cond_183

    #@c
    .line 3412
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@e
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@11
    move-result-object v0

    #@12
    .line 3413
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@14
    iget v4, v3, Landroid/app/Activity;->mConfigChangeFlags:I

    #@16
    or-int/2addr v4, p3

    #@17
    iput v4, v3, Landroid/app/Activity;->mConfigChangeFlags:I

    #@19
    .line 3414
    if-eqz p2, :cond_1f

    #@1b
    .line 3415
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1d
    iput-boolean v7, v3, Landroid/app/Activity;->mFinished:Z

    #@1f
    .line 3417
    :cond_1f
    iget-boolean v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@21
    if-nez v3, :cond_b6

    #@23
    .line 3419
    :try_start_23
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@25
    const/4 v4, 0x0

    #@26
    iput-boolean v4, v3, Landroid/app/Activity;->mCalled:Z

    #@28
    .line 3420
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@2a
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2c
    invoke-virtual {v3, v4}, Landroid/app/Instrumentation;->callActivityOnPause(Landroid/app/Activity;)V

    #@2f
    .line 3421
    const/16 v3, 0x7545

    #@31
    const/4 v4, 0x2

    #@32
    new-array v4, v4, [Ljava/lang/Object;

    #@34
    const/4 v5, 0x0

    #@35
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@38
    move-result v6

    #@39
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v6

    #@3d
    aput-object v6, v4, v5

    #@3f
    const/4 v5, 0x1

    #@40
    iget-object v6, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@42
    invoke-virtual {v6}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    aput-object v6, v4, v5

    #@4c
    invoke-static {v3, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@4f
    .line 3423
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@51
    iget-boolean v3, v3, Landroid/app/Activity;->mCalled:Z

    #@53
    if-nez v3, :cond_b4

    #@55
    .line 3424
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@57
    new-instance v4, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "Activity "

    #@5e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@64
    invoke-static {v5}, Landroid/app/ActivityThread;->safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    const-string v5, " did not call through to super.onPause()"

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v4

    #@76
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@79
    throw v3
    :try_end_7a
    .catch Landroid/app/SuperNotCalledException; {:try_start_23 .. :try_end_7a} :catch_7a
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_7a} :catch_7c

    #@7a
    .line 3428
    :catch_7a
    move-exception v1

    #@7b
    .line 3429
    .local v1, e:Landroid/app/SuperNotCalledException;
    throw v1

    #@7c
    .line 3430
    .end local v1           #e:Landroid/app/SuperNotCalledException;
    :catch_7c
    move-exception v1

    #@7d
    .line 3431
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@7f
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@81
    invoke-virtual {v3, v4, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@84
    move-result v3

    #@85
    if-nez v3, :cond_b4

    #@87
    .line 3432
    new-instance v3, Ljava/lang/RuntimeException;

    #@89
    new-instance v4, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v5, "Unable to pause activity "

    #@90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@96
    invoke-static {v5}, Landroid/app/ActivityThread;->safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    const-string v5, ": "

    #@a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v4

    #@a4
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@a7
    move-result-object v5

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v4

    #@b0
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@b3
    throw v3

    #@b4
    .line 3438
    .end local v1           #e:Ljava/lang/Exception;
    :cond_b4
    iput-boolean v7, v2, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@b6
    .line 3440
    :cond_b6
    iget-boolean v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@b8
    if-nez v3, :cond_c1

    #@ba
    .line 3442
    :try_start_ba
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@bc
    invoke-virtual {v3}, Landroid/app/Activity;->performStop()V
    :try_end_bf
    .catch Landroid/app/SuperNotCalledException; {:try_start_ba .. :try_end_bf} :catch_104
    .catch Ljava/lang/Exception; {:try_start_ba .. :try_end_bf} :catch_106

    #@bf
    .line 3453
    :cond_bf
    iput-boolean v7, v2, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@c1
    .line 3455
    :cond_c1
    if-eqz p4, :cond_cb

    #@c3
    .line 3457
    :try_start_c3
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@c5
    invoke-virtual {v3}, Landroid/app/Activity;->retainNonConfigurationInstances()Landroid/app/Activity$NonConfigurationInstances;

    #@c8
    move-result-object v3

    #@c9
    iput-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->lastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;
    :try_end_cb
    .catch Ljava/lang/Exception; {:try_start_c3 .. :try_end_cb} :catch_13e

    #@cb
    .line 3469
    :cond_cb
    :try_start_cb
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@cd
    const/4 v4, 0x0

    #@ce
    iput-boolean v4, v3, Landroid/app/Activity;->mCalled:Z

    #@d0
    .line 3470
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@d2
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@d4
    invoke-virtual {v3, v4}, Landroid/app/Instrumentation;->callActivityOnDestroy(Landroid/app/Activity;)V

    #@d7
    .line 3471
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@d9
    iget-boolean v3, v3, Landroid/app/Activity;->mCalled:Z

    #@db
    if-nez v3, :cond_17a

    #@dd
    .line 3472
    new-instance v3, Landroid/app/SuperNotCalledException;

    #@df
    new-instance v4, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v5, "Activity "

    #@e6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v4

    #@ea
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@ec
    invoke-static {v5}, Landroid/app/ActivityThread;->safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;

    #@ef
    move-result-object v5

    #@f0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v4

    #@f4
    const-string v5, " did not call through to super.onDestroy()"

    #@f6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v4

    #@fa
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v4

    #@fe
    invoke-direct {v3, v4}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@101
    throw v3
    :try_end_102
    .catch Landroid/app/SuperNotCalledException; {:try_start_cb .. :try_end_102} :catch_102
    .catch Ljava/lang/Exception; {:try_start_cb .. :try_end_102} :catch_18c

    #@102
    .line 3479
    :catch_102
    move-exception v1

    #@103
    .line 3480
    .local v1, e:Landroid/app/SuperNotCalledException;
    throw v1

    #@104
    .line 3443
    .end local v1           #e:Landroid/app/SuperNotCalledException;
    :catch_104
    move-exception v1

    #@105
    .line 3444
    .restart local v1       #e:Landroid/app/SuperNotCalledException;
    throw v1

    #@106
    .line 3445
    .end local v1           #e:Landroid/app/SuperNotCalledException;
    :catch_106
    move-exception v1

    #@107
    .line 3446
    .local v1, e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@109
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@10b
    invoke-virtual {v3, v4, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@10e
    move-result v3

    #@10f
    if-nez v3, :cond_bf

    #@111
    .line 3447
    new-instance v3, Ljava/lang/RuntimeException;

    #@113
    new-instance v4, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v5, "Unable to stop activity "

    #@11a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v4

    #@11e
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@120
    invoke-static {v5}, Landroid/app/ActivityThread;->safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;

    #@123
    move-result-object v5

    #@124
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v4

    #@128
    const-string v5, ": "

    #@12a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v4

    #@12e
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@131
    move-result-object v5

    #@132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v4

    #@13a
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@13d
    throw v3

    #@13e
    .line 3459
    .end local v1           #e:Ljava/lang/Exception;
    :catch_13e
    move-exception v1

    #@13f
    .line 3460
    .restart local v1       #e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@141
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@143
    invoke-virtual {v3, v4, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@146
    move-result v3

    #@147
    if-nez v3, :cond_cb

    #@149
    .line 3461
    new-instance v3, Ljava/lang/RuntimeException;

    #@14b
    new-instance v4, Ljava/lang/StringBuilder;

    #@14d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@150
    const-string v5, "Unable to retain activity "

    #@152
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v4

    #@156
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@158
    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@15b
    move-result-object v5

    #@15c
    invoke-virtual {v5}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@15f
    move-result-object v5

    #@160
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v4

    #@164
    const-string v5, ": "

    #@166
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v4

    #@16a
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@16d
    move-result-object v5

    #@16e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v4

    #@172
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v4

    #@176
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@179
    throw v3

    #@17a
    .line 3476
    .end local v1           #e:Ljava/lang/Exception;
    :cond_17a
    :try_start_17a
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@17c
    if-eqz v3, :cond_183

    #@17e
    .line 3477
    iget-object v3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@180
    invoke-virtual {v3}, Landroid/view/Window;->closeAllPanels()V
    :try_end_183
    .catch Landroid/app/SuperNotCalledException; {:try_start_17a .. :try_end_183} :catch_102
    .catch Ljava/lang/Exception; {:try_start_17a .. :try_end_183} :catch_18c

    #@183
    .line 3489
    :cond_183
    iget-object v3, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@185
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@188
    .line 3490
    invoke-static {v0}, Landroid/os/StrictMode;->decrementExpectedActivityCount(Ljava/lang/Class;)V

    #@18b
    .line 3491
    return-object v2

    #@18c
    .line 3481
    :catch_18c
    move-exception v1

    #@18d
    .line 3482
    .restart local v1       #e:Ljava/lang/Exception;
    iget-object v3, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@18f
    iget-object v4, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@191
    invoke-virtual {v3, v4, v1}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@194
    move-result v3

    #@195
    if-nez v3, :cond_183

    #@197
    .line 3483
    new-instance v3, Ljava/lang/RuntimeException;

    #@199
    new-instance v4, Ljava/lang/StringBuilder;

    #@19b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19e
    const-string v5, "Unable to destroy activity "

    #@1a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v4

    #@1a4
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@1a6
    invoke-static {v5}, Landroid/app/ActivityThread;->safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;

    #@1a9
    move-result-object v5

    #@1aa
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ad
    move-result-object v4

    #@1ae
    const-string v5, ": "

    #@1b0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v4

    #@1b4
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@1b7
    move-result-object v5

    #@1b8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bb
    move-result-object v4

    #@1bc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bf
    move-result-object v4

    #@1c0
    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1c3
    throw v3
.end method

.method private performLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)Landroid/app/Activity;
    .registers 24
    .parameter "r"
    .parameter "customIntent"

    #@0
    .prologue
    .line 2098
    move-object/from16 v0, p1

    #@2
    iget-object v0, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4
    move-object/from16 v16, v0

    #@6
    .line 2099
    .local v16, aInfo:Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p1

    #@8
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@a
    if-nez v4, :cond_1f

    #@c
    .line 2100
    move-object/from16 v0, v16

    #@e
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@10
    move-object/from16 v0, p1

    #@12
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@14
    const/4 v6, 0x1

    #@15
    move-object/from16 v0, p0

    #@17
    invoke-virtual {v0, v4, v5, v6}, Landroid/app/ActivityThread;->getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)Landroid/app/LoadedApk;

    #@1a
    move-result-object v4

    #@1b
    move-object/from16 v0, p1

    #@1d
    iput-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@1f
    .line 2104
    :cond_1f
    move-object/from16 v0, p1

    #@21
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@23
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@26
    move-result-object v18

    #@27
    .line 2105
    .local v18, component:Landroid/content/ComponentName;
    if-nez v18, :cond_42

    #@29
    .line 2106
    move-object/from16 v0, p1

    #@2b
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v5, v0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@31
    invoke-virtual {v5}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v4, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@38
    move-result-object v18

    #@39
    .line 2108
    move-object/from16 v0, p1

    #@3b
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@3d
    move-object/from16 v0, v18

    #@3f
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@42
    .line 2111
    :cond_42
    move-object/from16 v0, p1

    #@44
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@46
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@48
    if-eqz v4, :cond_5d

    #@4a
    .line 2112
    new-instance v18, Landroid/content/ComponentName;

    #@4c
    .end local v18           #component:Landroid/content/ComponentName;
    move-object/from16 v0, p1

    #@4e
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@50
    iget-object v4, v4, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@52
    move-object/from16 v0, p1

    #@54
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@56
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    #@58
    move-object/from16 v0, v18

    #@5a
    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 2116
    .restart local v18       #component:Landroid/content/ComponentName;
    :cond_5d
    const/4 v2, 0x0

    #@5e
    .line 2118
    .local v2, activity:Landroid/app/Activity;
    :try_start_5e
    move-object/from16 v0, p1

    #@60
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@62
    invoke-virtual {v4}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@65
    move-result-object v17

    #@66
    .line 2119
    .local v17, cl:Ljava/lang/ClassLoader;
    move-object/from16 v0, p0

    #@68
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@6a
    invoke-virtual/range {v18 .. v18}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    move-object/from16 v0, p1

    #@70
    iget-object v6, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@72
    move-object/from16 v0, v17

    #@74
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/Instrumentation;->newActivity(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Intent;)Landroid/app/Activity;

    #@77
    move-result-object v2

    #@78
    .line 2121
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@7b
    move-result-object v4

    #@7c
    invoke-static {v4}, Landroid/os/StrictMode;->incrementExpectedActivityCount(Ljava/lang/Class;)V

    #@7f
    .line 2122
    move-object/from16 v0, p1

    #@81
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@83
    move-object/from16 v0, v17

    #@85
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    #@88
    .line 2123
    move-object/from16 v0, p1

    #@8a
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@8c
    if-eqz v4, :cond_97

    #@8e
    .line 2124
    move-object/from16 v0, p1

    #@90
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@92
    move-object/from16 v0, v17

    #@94
    invoke-virtual {v4, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_97
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_97} :catch_144

    #@97
    .line 2135
    .end local v17           #cl:Ljava/lang/ClassLoader;
    :cond_97
    :try_start_97
    move-object/from16 v0, p1

    #@99
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@9b
    const/4 v5, 0x0

    #@9c
    move-object/from16 v0, p0

    #@9e
    iget-object v6, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@a0
    invoke-virtual {v4, v5, v6}, Landroid/app/LoadedApk;->makeApplication(ZLandroid/app/Instrumentation;)Landroid/app/Application;

    #@a3
    move-result-object v8

    #@a4
    .line 2145
    .local v8, app:Landroid/app/Application;
    if-eqz v2, :cond_22b

    #@a6
    .line 2146
    move-object/from16 v0, p0

    #@a8
    move-object/from16 v1, p1

    #@aa
    invoke-direct {v0, v1, v2}, Landroid/app/ActivityThread;->createBaseContextForActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/Activity;)Landroid/content/Context;

    #@ad
    move-result-object v3

    #@ae
    .line 2147
    .local v3, appContext:Landroid/content/Context;
    move-object/from16 v0, p1

    #@b0
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@b2
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b5
    move-result-object v5

    #@b6
    invoke-virtual {v4, v5}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@b9
    move-result-object v11

    #@ba
    .line 2148
    .local v11, title:Ljava/lang/CharSequence;
    new-instance v15, Landroid/content/res/Configuration;

    #@bc
    move-object/from16 v0, p0

    #@be
    iget-object v4, v0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@c0
    invoke-direct {v15, v4}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@c3
    .line 2151
    .local v15, config:Landroid/content/res/Configuration;
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@c6
    move-result-object v5

    #@c7
    move-object/from16 v0, p1

    #@c9
    iget-object v6, v0, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@cb
    move-object/from16 v0, p1

    #@cd
    iget v7, v0, Landroid/app/ActivityThread$ActivityClientRecord;->ident:I

    #@cf
    move-object/from16 v0, p1

    #@d1
    iget-object v9, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@d3
    move-object/from16 v0, p1

    #@d5
    iget-object v10, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@d7
    move-object/from16 v0, p1

    #@d9
    iget-object v12, v0, Landroid/app/ActivityThread$ActivityClientRecord;->parent:Landroid/app/Activity;

    #@db
    move-object/from16 v0, p1

    #@dd
    iget-object v13, v0, Landroid/app/ActivityThread$ActivityClientRecord;->embeddedID:Ljava/lang/String;

    #@df
    move-object/from16 v0, p1

    #@e1
    iget-object v14, v0, Landroid/app/ActivityThread$ActivityClientRecord;->lastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@e3
    move-object/from16 v4, p0

    #@e5
    invoke-virtual/range {v2 .. v15}, Landroid/app/Activity;->attach(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/app/Instrumentation;Landroid/os/IBinder;ILandroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Landroid/app/Activity$NonConfigurationInstances;Landroid/content/res/Configuration;)V

    #@e8
    .line 2155
    if-eqz p2, :cond_ee

    #@ea
    .line 2156
    move-object/from16 v0, p2

    #@ec
    iput-object v0, v2, Landroid/app/Activity;->mIntent:Landroid/content/Intent;

    #@ee
    .line 2158
    :cond_ee
    const/4 v4, 0x0

    #@ef
    move-object/from16 v0, p1

    #@f1
    iput-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->lastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@f3
    .line 2159
    const/4 v4, 0x0

    #@f4
    iput-boolean v4, v2, Landroid/app/Activity;->mStartedActivity:Z

    #@f6
    .line 2160
    move-object/from16 v0, p1

    #@f8
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@fa
    invoke-virtual {v4}, Landroid/content/pm/ActivityInfo;->getThemeResource()I

    #@fd
    move-result v20

    #@fe
    .line 2161
    .local v20, theme:I
    if-eqz v20, :cond_105

    #@100
    .line 2162
    move/from16 v0, v20

    #@102
    invoke-virtual {v2, v0}, Landroid/app/Activity;->setTheme(I)V

    #@105
    .line 2165
    :cond_105
    const/4 v4, 0x0

    #@106
    iput-boolean v4, v2, Landroid/app/Activity;->mCalled:Z

    #@108
    .line 2166
    move-object/from16 v0, p0

    #@10a
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@10c
    move-object/from16 v0, p1

    #@10e
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@110
    invoke-virtual {v4, v2, v5}, Landroid/app/Instrumentation;->callActivityOnCreate(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@113
    .line 2167
    iget-boolean v4, v2, Landroid/app/Activity;->mCalled:Z

    #@115
    if-nez v4, :cond_17c

    #@117
    .line 2168
    new-instance v4, Landroid/app/SuperNotCalledException;

    #@119
    new-instance v5, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v6, "Activity "

    #@120
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v5

    #@124
    move-object/from16 v0, p1

    #@126
    iget-object v6, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@128
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@12b
    move-result-object v6

    #@12c
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@12f
    move-result-object v6

    #@130
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v5

    #@134
    const-string v6, " did not call through to super.onCreate()"

    #@136
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v5

    #@13a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v5

    #@13e
    invoke-direct {v4, v5}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@141
    throw v4
    :try_end_142
    .catch Landroid/app/SuperNotCalledException; {:try_start_97 .. :try_end_142} :catch_142
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_142} :catch_1f3

    #@142
    .line 2197
    .end local v3           #appContext:Landroid/content/Context;
    .end local v8           #app:Landroid/app/Application;
    .end local v11           #title:Ljava/lang/CharSequence;
    .end local v15           #config:Landroid/content/res/Configuration;
    .end local v20           #theme:I
    :catch_142
    move-exception v19

    #@143
    .line 2198
    .local v19, e:Landroid/app/SuperNotCalledException;
    throw v19

    #@144
    .line 2126
    .end local v19           #e:Landroid/app/SuperNotCalledException;
    :catch_144
    move-exception v19

    #@145
    .line 2127
    .local v19, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    #@147
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@149
    move-object/from16 v0, v19

    #@14b
    invoke-virtual {v4, v2, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@14e
    move-result v4

    #@14f
    if-nez v4, :cond_97

    #@151
    .line 2128
    new-instance v4, Ljava/lang/RuntimeException;

    #@153
    new-instance v5, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v6, "Unable to instantiate activity "

    #@15a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v5

    #@15e
    move-object/from16 v0, v18

    #@160
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v5

    #@164
    const-string v6, ": "

    #@166
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v5

    #@16a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@16d
    move-result-object v6

    #@16e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v5

    #@172
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v5

    #@176
    move-object/from16 v0, v19

    #@178
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@17b
    throw v4

    #@17c
    .line 2172
    .end local v19           #e:Ljava/lang/Exception;
    .restart local v3       #appContext:Landroid/content/Context;
    .restart local v8       #app:Landroid/app/Application;
    .restart local v11       #title:Ljava/lang/CharSequence;
    .restart local v15       #config:Landroid/content/res/Configuration;
    .restart local v20       #theme:I
    :cond_17c
    :try_start_17c
    move-object/from16 v0, p1

    #@17e
    iput-object v2, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@180
    .line 2173
    const/4 v4, 0x1

    #@181
    move-object/from16 v0, p1

    #@183
    iput-boolean v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@185
    .line 2174
    move-object/from16 v0, p1

    #@187
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@189
    iget-boolean v4, v4, Landroid/app/Activity;->mFinished:Z

    #@18b
    if-nez v4, :cond_195

    #@18d
    .line 2175
    invoke-virtual {v2}, Landroid/app/Activity;->performStart()V

    #@190
    .line 2176
    const/4 v4, 0x0

    #@191
    move-object/from16 v0, p1

    #@193
    iput-boolean v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@195
    .line 2178
    :cond_195
    move-object/from16 v0, p1

    #@197
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@199
    iget-boolean v4, v4, Landroid/app/Activity;->mFinished:Z

    #@19b
    if-nez v4, :cond_1ae

    #@19d
    .line 2179
    move-object/from16 v0, p1

    #@19f
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@1a1
    if-eqz v4, :cond_1ae

    #@1a3
    .line 2180
    move-object/from16 v0, p0

    #@1a5
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@1a7
    move-object/from16 v0, p1

    #@1a9
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@1ab
    invoke-virtual {v4, v2, v5}, Landroid/app/Instrumentation;->callActivityOnRestoreInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@1ae
    .line 2183
    :cond_1ae
    move-object/from16 v0, p1

    #@1b0
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1b2
    iget-boolean v4, v4, Landroid/app/Activity;->mFinished:Z

    #@1b4
    if-nez v4, :cond_22b

    #@1b6
    .line 2184
    const/4 v4, 0x0

    #@1b7
    iput-boolean v4, v2, Landroid/app/Activity;->mCalled:Z

    #@1b9
    .line 2185
    move-object/from16 v0, p0

    #@1bb
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@1bd
    move-object/from16 v0, p1

    #@1bf
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@1c1
    invoke-virtual {v4, v2, v5}, Landroid/app/Instrumentation;->callActivityOnPostCreate(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@1c4
    .line 2186
    iget-boolean v4, v2, Landroid/app/Activity;->mCalled:Z

    #@1c6
    if-nez v4, :cond_22b

    #@1c8
    .line 2187
    new-instance v4, Landroid/app/SuperNotCalledException;

    #@1ca
    new-instance v5, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v6, "Activity "

    #@1d1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v5

    #@1d5
    move-object/from16 v0, p1

    #@1d7
    iget-object v6, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@1d9
    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@1dc
    move-result-object v6

    #@1dd
    invoke-virtual {v6}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@1e0
    move-result-object v6

    #@1e1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e4
    move-result-object v5

    #@1e5
    const-string v6, " did not call through to super.onPostCreate()"

    #@1e7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v5

    #@1eb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ee
    move-result-object v5

    #@1ef
    invoke-direct {v4, v5}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@1f2
    throw v4
    :try_end_1f3
    .catch Landroid/app/SuperNotCalledException; {:try_start_17c .. :try_end_1f3} :catch_142
    .catch Ljava/lang/Exception; {:try_start_17c .. :try_end_1f3} :catch_1f3

    #@1f3
    .line 2200
    .end local v3           #appContext:Landroid/content/Context;
    .end local v8           #app:Landroid/app/Application;
    .end local v11           #title:Ljava/lang/CharSequence;
    .end local v15           #config:Landroid/content/res/Configuration;
    .end local v20           #theme:I
    :catch_1f3
    move-exception v19

    #@1f4
    .line 2201
    .restart local v19       #e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    #@1f6
    iget-object v4, v0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@1f8
    move-object/from16 v0, v19

    #@1fa
    invoke-virtual {v4, v2, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@1fd
    move-result v4

    #@1fe
    if-nez v4, :cond_23d

    #@200
    .line 2202
    new-instance v4, Ljava/lang/RuntimeException;

    #@202
    new-instance v5, Ljava/lang/StringBuilder;

    #@204
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@207
    const-string v6, "Unable to start activity "

    #@209
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v5

    #@20d
    move-object/from16 v0, v18

    #@20f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@212
    move-result-object v5

    #@213
    const-string v6, ": "

    #@215
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@218
    move-result-object v5

    #@219
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@21c
    move-result-object v6

    #@21d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v5

    #@221
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@224
    move-result-object v5

    #@225
    move-object/from16 v0, v19

    #@227
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@22a
    throw v4

    #@22b
    .line 2193
    .end local v19           #e:Ljava/lang/Exception;
    .restart local v8       #app:Landroid/app/Application;
    :cond_22b
    const/4 v4, 0x1

    #@22c
    :try_start_22c
    move-object/from16 v0, p1

    #@22e
    iput-boolean v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@230
    .line 2195
    move-object/from16 v0, p0

    #@232
    iget-object v4, v0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@234
    move-object/from16 v0, p1

    #@236
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@238
    move-object/from16 v0, p1

    #@23a
    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_23d
    .catch Landroid/app/SuperNotCalledException; {:try_start_22c .. :try_end_23d} :catch_142
    .catch Ljava/lang/Exception; {:try_start_22c .. :try_end_23d} :catch_1f3

    #@23d
    .line 2208
    .end local v8           #app:Landroid/app/Application;
    :cond_23d
    return-object v2
.end method

.method private performStopActivityInner(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/ActivityThread$StopInfo;ZZ)V
    .registers 10
    .parameter "r"
    .parameter "info"
    .parameter "keepShown"
    .parameter "saveState"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 3119
    const/4 v1, 0x0

    #@2
    .line 3120
    .local v1, state:Landroid/os/Bundle;
    if-eqz p1, :cond_10

    #@4
    .line 3121
    if-nez p3, :cond_3c

    #@6
    iget-boolean v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@8
    if-eqz v2, :cond_3c

    #@a
    .line 3122
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@c
    iget-boolean v2, v2, Landroid/app/Activity;->mFinished:Z

    #@e
    if-eqz v2, :cond_11

    #@10
    .line 3180
    :cond_10
    :goto_10
    return-void

    #@11
    .line 3128
    :cond_11
    new-instance v0, Ljava/lang/RuntimeException;

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "Performing stop of activity that is not resumed: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@20
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@33
    .line 3131
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "ActivityThread"

    #@35
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3c
    .line 3134
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_3c
    if-eqz p2, :cond_49

    #@3e
    .line 3139
    const/4 v2, 0x0

    #@3f
    :try_start_3f
    iput-object v2, p2, Landroid/app/ActivityThread$StopInfo;->thumbnail:Landroid/graphics/Bitmap;

    #@41
    .line 3140
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@43
    invoke-virtual {v2}, Landroid/app/Activity;->onCreateDescription()Ljava/lang/CharSequence;

    #@46
    move-result-object v2

    #@47
    iput-object v2, p2, Landroid/app/ActivityThread$StopInfo;->description:Ljava/lang/CharSequence;
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_49} :catch_73

    #@49
    .line 3152
    :cond_49
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@4b
    iget-boolean v2, v2, Landroid/app/Activity;->mFinished:Z

    #@4d
    if-nez v2, :cond_67

    #@4f
    if-eqz p4, :cond_67

    #@51
    .line 3153
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@53
    if-nez v2, :cond_af

    #@55
    .line 3154
    new-instance v1, Landroid/os/Bundle;

    #@57
    .end local v1           #state:Landroid/os/Bundle;
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@5a
    .line 3155
    .restart local v1       #state:Landroid/os/Bundle;
    const/4 v2, 0x0

    #@5b
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@5e
    .line 3156
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@60
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@62
    invoke-virtual {v2, v3, v1}, Landroid/app/Instrumentation;->callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@65
    .line 3157
    iput-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@67
    .line 3163
    :cond_67
    :goto_67
    if-nez p3, :cond_70

    #@69
    .line 3166
    :try_start_69
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@6b
    invoke-virtual {v2}, Landroid/app/Activity;->performStop()V
    :try_end_6e
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_6e} :catch_b2

    #@6e
    .line 3175
    :cond_6e
    iput-boolean v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@70
    .line 3178
    :cond_70
    iput-boolean v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@72
    goto :goto_10

    #@73
    .line 3141
    :catch_73
    move-exception v0

    #@74
    .line 3142
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@76
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@78
    invoke-virtual {v2, v3, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@7b
    move-result v2

    #@7c
    if-nez v2, :cond_49

    #@7e
    .line 3143
    new-instance v2, Ljava/lang/RuntimeException;

    #@80
    new-instance v3, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v4, "Unable to save state of activity "

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    iget-object v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@8d
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@90
    move-result-object v4

    #@91
    invoke-virtual {v4}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v3

    #@99
    const-string v4, ": "

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@a2
    move-result-object v4

    #@a3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ae
    throw v2

    #@af
    .line 3159
    .end local v0           #e:Ljava/lang/Exception;
    :cond_af
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@b1
    goto :goto_67

    #@b2
    .line 3167
    :catch_b2
    move-exception v0

    #@b3
    .line 3168
    .restart local v0       #e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@b5
    iget-object v3, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b7
    invoke-virtual {v2, v3, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@ba
    move-result v2

    #@bb
    if-nez v2, :cond_6e

    #@bd
    .line 3169
    new-instance v2, Ljava/lang/RuntimeException;

    #@bf
    new-instance v3, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v4, "Unable to stop activity "

    #@c6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v3

    #@ca
    iget-object v4, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@cc
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@cf
    move-result-object v4

    #@d0
    invoke-virtual {v4}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@d3
    move-result-object v4

    #@d4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v3

    #@d8
    const-string v4, ": "

    #@da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v3

    #@de
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@e1
    move-result-object v4

    #@e2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v3

    #@e6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v3

    #@ea
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@ed
    throw v2
.end method

.method private queueOrSendMessage(ILjava/lang/Object;)V
    .registers 4
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 2065
    invoke-direct {p0, p1, p2, v0, v0}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;II)V

    #@4
    .line 2066
    return-void
.end method

.method private queueOrSendMessage(ILjava/lang/Object;I)V
    .registers 5
    .parameter "what"
    .parameter "obj"
    .parameter "arg1"

    #@0
    .prologue
    .line 2069
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;II)V

    #@4
    .line 2070
    return-void
.end method

.method private queueOrSendMessage(ILjava/lang/Object;II)V
    .registers 7
    .parameter "what"
    .parameter "obj"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 2073
    monitor-enter p0

    #@1
    .line 2077
    :try_start_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@4
    move-result-object v0

    #@5
    .line 2078
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@7
    .line 2079
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 2080
    iput p3, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 2081
    iput p4, v0, Landroid/os/Message;->arg2:I

    #@d
    .line 2082
    iget-object v1, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@f
    invoke-virtual {v1, v0}, Landroid/app/ActivityThread$H;->sendMessage(Landroid/os/Message;)Z

    #@12
    .line 2083
    monitor-exit p0

    #@13
    .line 2084
    return-void

    #@14
    .line 2083
    .end local v0           #msg:Landroid/os/Message;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method private static safeToComponentShortString(Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 3495
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@3
    move-result-object v0

    #@4
    .line 3496
    .local v0, component:Landroid/content/ComponentName;
    if-nez v0, :cond_9

    #@6
    const-string v1, "[Unknown]"

    #@8
    :goto_8
    return-object v1

    #@9
    :cond_9
    invoke-virtual {v0}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    goto :goto_8
.end method

.method private setupGraphicsSupport(Landroid/app/LoadedApk;Ljava/io/File;)V
    .registers 7
    .parameter "info"
    .parameter "cacheDir"

    #@0
    .prologue
    .line 4170
    invoke-static {}, Landroid/os/Process;->isIsolated()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_7

    #@6
    .line 4187
    :cond_6
    :goto_6
    return-void

    #@7
    .line 4175
    :cond_7
    :try_start_7
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@a
    move-result v1

    #@b
    .line 4176
    .local v1, uid:I
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2, v1}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 4180
    .local v0, packages:[Ljava/lang/String;
    if-eqz v0, :cond_6

    #@15
    array-length v2, v0

    #@16
    const/4 v3, 0x1

    #@17
    if-ne v2, v3, :cond_6

    #@19
    .line 4181
    invoke-static {p2}, Landroid/view/HardwareRenderer;->setupDiskCache(Ljava/io/File;)V

    #@1c
    .line 4182
    invoke-static {p2}, Landroid/renderscript/RenderScript;->setupDiskCache(Ljava/io/File;)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_1f} :catch_20

    #@1f
    goto :goto_6

    #@20
    .line 4184
    .end local v0           #packages:[Ljava/lang/String;
    .end local v1           #uid:I
    :catch_20
    move-exception v2

    #@21
    goto :goto_6
.end method

.method public static systemMain()Landroid/app/ActivityThread;
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 5086
    invoke-static {v2}, Landroid/view/HardwareRenderer;->disable(Z)V

    #@5
    .line 5087
    new-instance v0, Landroid/app/ActivityThread;

    #@7
    invoke-direct {v0}, Landroid/app/ActivityThread;-><init>()V

    #@a
    .line 5088
    .local v0, thread:Landroid/app/ActivityThread;
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_THEMEICON:Z

    #@c
    if-eqz v1, :cond_14

    #@e
    .line 5089
    invoke-static {v3, v3, v2}, Landroid/content/thm/ThemeIconManager;->isUseThemeIcon(Landroid/content/pm/IPackageManager;Ljava/lang/String;Z)Z

    #@11
    move-result v1

    #@12
    iput-boolean v1, v0, Landroid/app/ActivityThread;->mThemeIconEnabled:Z

    #@14
    .line 5091
    :cond_14
    invoke-direct {v0, v2}, Landroid/app/ActivityThread;->attach(Z)V

    #@17
    .line 5092
    return-object v0
.end method

.method private updateDefaultDensity()V
    .registers 4

    #@0
    .prologue
    .line 4190
    iget v0, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@2
    if-eqz v0, :cond_3d

    #@4
    iget v0, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@6
    sget v1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@8
    if-eq v0, v1, :cond_3d

    #@a
    iget-boolean v0, p0, Landroid/app/ActivityThread;->mDensityCompatMode:Z

    #@c
    if-nez v0, :cond_3d

    #@e
    .line 4193
    const-string v0, "ActivityThread"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Switching default density from "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    sget v2, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, " to "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    iget v2, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 4196
    iget v0, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@36
    sput v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@38
    .line 4197
    const/16 v0, 0xa0

    #@3a
    invoke-static {v0}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    #@3d
    .line 4199
    :cond_3d
    return-void
.end method

.method private updateVisibility(Landroid/app/ActivityThread$ActivityClientRecord;Z)V
    .registers 6
    .parameter "r"
    .parameter "show"

    #@0
    .prologue
    .line 3183
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2
    iget-object v0, v1, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@4
    .line 3184
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_50

    #@6
    .line 3185
    if-eqz p2, :cond_51

    #@8
    .line 3186
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@a
    iget-boolean v1, v1, Landroid/app/Activity;->mVisibleFromServer:Z

    #@c
    if-nez v1, :cond_24

    #@e
    .line 3187
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@10
    const/4 v2, 0x1

    #@11
    iput-boolean v2, v1, Landroid/app/Activity;->mVisibleFromServer:Z

    #@13
    .line 3188
    iget v1, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@15
    add-int/lit8 v1, v1, 0x1

    #@17
    iput v1, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@19
    .line 3189
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1b
    iget-boolean v1, v1, Landroid/app/Activity;->mVisibleFromClient:Z

    #@1d
    if-eqz v1, :cond_24

    #@1f
    .line 3190
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@21
    invoke-virtual {v1}, Landroid/app/Activity;->makeVisible()V

    #@24
    .line 3193
    :cond_24
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@26
    if-eqz v1, :cond_50

    #@28
    .line 3196
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2a
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@2c
    invoke-static {v1, v2}, Landroid/app/ActivityThread;->performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V

    #@2f
    .line 3198
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@31
    if-eqz v1, :cond_40

    #@33
    .line 3199
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@35
    iget-object v1, v1, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@37
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@39
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@3c
    move-result v1

    #@3d
    invoke-virtual {p0, v1}, Landroid/app/ActivityThread;->updateFontConfigurationIfNeeded(I)V

    #@40
    .line 3202
    :cond_40
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@42
    iget-object v1, v1, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@44
    iget-object v2, p1, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@46
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@49
    move-result v1

    #@4a
    invoke-virtual {p0, v1}, Landroid/app/ActivityThread;->freeTextLayoutCachesIfNeeded(I)V

    #@4d
    .line 3203
    const/4 v1, 0x0

    #@4e
    iput-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@50
    .line 3213
    :cond_50
    :goto_50
    return-void

    #@51
    .line 3206
    :cond_51
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@53
    iget-boolean v1, v1, Landroid/app/Activity;->mVisibleFromServer:Z

    #@55
    if-eqz v1, :cond_50

    #@57
    .line 3207
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@59
    const/4 v2, 0x0

    #@5a
    iput-boolean v2, v1, Landroid/app/Activity;->mVisibleFromServer:Z

    #@5c
    .line 3208
    iget v1, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@5e
    add-int/lit8 v1, v1, -0x1

    #@60
    iput v1, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@62
    .line 3209
    const/4 v1, 0x4

    #@63
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@66
    goto :goto_50
.end method


# virtual methods
.method public final acquireExistingProvider(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/IContentProvider;
    .registers 15
    .parameter "c"
    .parameter "auth"
    .parameter "userId"
    .parameter "stable"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4666
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@3
    monitor-enter v6

    #@4
    .line 4667
    :try_start_4
    new-instance v1, Landroid/app/ActivityThread$ProviderKey;

    #@6
    invoke-direct {v1, p2, p3}, Landroid/app/ActivityThread$ProviderKey;-><init>(Ljava/lang/String;I)V

    #@9
    .line 4668
    .local v1, key:Landroid/app/ActivityThread$ProviderKey;
    iget-object v7, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@b
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/app/ActivityThread$ProviderClientRecord;

    #@11
    .line 4669
    .local v2, pr:Landroid/app/ActivityThread$ProviderClientRecord;
    if-nez v2, :cond_16

    #@13
    .line 4670
    monitor-exit v6

    #@14
    move-object v4, v5

    #@15
    .line 4690
    :goto_15
    return-object v4

    #@16
    .line 4673
    :cond_16
    iget-object v4, v2, Landroid/app/ActivityThread$ProviderClientRecord;->mProvider:Landroid/content/IContentProvider;

    #@18
    .line 4674
    .local v4, provider:Landroid/content/IContentProvider;
    invoke-interface {v4}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v0

    #@1c
    .line 4675
    .local v0, jBinder:Landroid/os/IBinder;
    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    #@1f
    move-result v7

    #@20
    if-nez v7, :cond_51

    #@22
    .line 4678
    const-string v7, "ActivityThread"

    #@24
    new-instance v8, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v9, "Acquiring provider "

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    const-string v9, " for user "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    const-string v9, ": existing object\'s process dead"

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v8

    #@47
    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 4680
    const/4 v7, 0x1

    #@4b
    invoke-virtual {p0, v0, v7}, Landroid/app/ActivityThread;->handleUnstableProviderDiedLocked(Landroid/os/IBinder;Z)V

    #@4e
    .line 4681
    monitor-exit v6

    #@4f
    move-object v4, v5

    #@50
    goto :goto_15

    #@51
    .line 4686
    :cond_51
    iget-object v5, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@53
    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    move-result-object v3

    #@57
    check-cast v3, Landroid/app/ActivityThread$ProviderRefCount;

    #@59
    .line 4687
    .local v3, prc:Landroid/app/ActivityThread$ProviderRefCount;
    if-eqz v3, :cond_5e

    #@5b
    .line 4688
    invoke-direct {p0, v3, p4}, Landroid/app/ActivityThread;->incProviderRefLocked(Landroid/app/ActivityThread$ProviderRefCount;Z)V

    #@5e
    .line 4690
    :cond_5e
    monitor-exit v6

    #@5f
    goto :goto_15

    #@60
    .line 4691
    .end local v0           #jBinder:Landroid/os/IBinder;
    .end local v1           #key:Landroid/app/ActivityThread$ProviderKey;
    .end local v2           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    .end local v3           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    .end local v4           #provider:Landroid/content/IContentProvider;
    :catchall_60
    move-exception v5

    #@61
    monitor-exit v6
    :try_end_62
    .catchall {:try_start_4 .. :try_end_62} :catchall_60

    #@62
    throw v5
.end method

.method public final acquireProvider(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/IContentProvider;
    .registers 13
    .parameter "c"
    .parameter "auth"
    .parameter "userId"
    .parameter "stable"

    #@0
    .prologue
    .line 4565
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/app/ActivityThread;->acquireExistingProvider(Landroid/content/Context;Ljava/lang/String;IZ)Landroid/content/IContentProvider;

    #@3
    move-result-object v7

    #@4
    .line 4566
    .local v7, provider:Landroid/content/IContentProvider;
    if-eqz v7, :cond_7

    #@6
    .line 4591
    .end local v7           #provider:Landroid/content/IContentProvider;
    :goto_6
    return-object v7

    #@7
    .line 4576
    .restart local v7       #provider:Landroid/content/IContentProvider;
    :cond_7
    const/4 v2, 0x0

    #@8
    .line 4578
    .local v2, holder:Landroid/app/IActivityManager$ContentProviderHolder;
    :try_start_8
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v0, v1, p2, p3, p4}, Landroid/app/IActivityManager;->getContentProvider(Landroid/app/IApplicationThread;Ljava/lang/String;IZ)Landroid/app/IActivityManager$ContentProviderHolder;
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_13} :catch_3f

    #@13
    move-result-object v2

    #@14
    .line 4582
    :goto_14
    if-nez v2, :cond_30

    #@16
    .line 4583
    const-string v0, "ActivityThread"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Failed to find provider info for "

    #@1f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 4584
    const/4 v7, 0x0

    #@2f
    goto :goto_6

    #@30
    .line 4589
    :cond_30
    iget-object v3, v2, Landroid/app/IActivityManager$ContentProviderHolder;->info:Landroid/content/pm/ProviderInfo;

    #@32
    const/4 v4, 0x1

    #@33
    iget-boolean v5, v2, Landroid/app/IActivityManager$ContentProviderHolder;->noReleaseNeeded:Z

    #@35
    move-object v0, p0

    #@36
    move-object v1, p1

    #@37
    move v6, p4

    #@38
    invoke-direct/range {v0 .. v6}, Landroid/app/ActivityThread;->installProvider(Landroid/content/Context;Landroid/app/IActivityManager$ContentProviderHolder;Landroid/content/pm/ProviderInfo;ZZZ)Landroid/app/IActivityManager$ContentProviderHolder;

    #@3b
    move-result-object v2

    #@3c
    .line 4591
    iget-object v7, v2, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@3e
    goto :goto_6

    #@3f
    .line 4580
    :catch_3f
    move-exception v0

    #@40
    goto :goto_14
.end method

.method final applyCompatConfiguration(I)Landroid/content/res/Configuration;
    .registers 5
    .parameter "displayDensity"

    #@0
    .prologue
    .line 3960
    iget-object v0, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@2
    .line 3961
    .local v0, config:Landroid/content/res/Configuration;
    iget-object v1, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@4
    if-nez v1, :cond_d

    #@6
    .line 3962
    new-instance v1, Landroid/content/res/Configuration;

    #@8
    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    #@b
    iput-object v1, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@d
    .line 3964
    :cond_d
    iget-object v1, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@f
    iget-object v2, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@11
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@14
    .line 3965
    iget-object v1, p0, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@16
    if-eqz v1, :cond_29

    #@18
    iget-object v1, p0, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@1a
    invoke-virtual {v1}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_29

    #@20
    .line 3966
    iget-object v1, p0, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@22
    iget-object v2, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@24
    invoke-virtual {v1, p1, v2}, Landroid/content/res/CompatibilityInfo;->applyToConfiguration(ILandroid/content/res/Configuration;)V

    #@27
    .line 3967
    iget-object v0, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@29
    .line 3969
    :cond_29
    return-object v0
.end method

.method applyConfigCompatMainThread(ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Configuration;
    .registers 5
    .parameter "displayDensity"
    .parameter "config"
    .parameter "compat"

    #@0
    .prologue
    .line 1643
    if-nez p2, :cond_4

    #@2
    .line 1644
    const/4 v0, 0x0

    #@3
    .line 1651
    :goto_3
    return-object v0

    #@4
    .line 1646
    :cond_4
    if-eqz p3, :cond_16

    #@6
    invoke-virtual {p3}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_16

    #@c
    .line 1647
    iget-object v0, p0, Landroid/app/ActivityThread;->mMainThreadConfig:Landroid/content/res/Configuration;

    #@e
    invoke-virtual {v0, p2}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@11
    .line 1648
    iget-object p2, p0, Landroid/app/ActivityThread;->mMainThreadConfig:Landroid/content/res/Configuration;

    #@13
    .line 1649
    invoke-virtual {p3, p1, p2}, Landroid/content/res/CompatibilityInfo;->applyToConfiguration(ILandroid/content/res/Configuration;)V

    #@16
    :cond_16
    move-object v0, p2

    #@17
    .line 1651
    goto :goto_3
.end method

.method public final applyConfigurationToResources(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 3847
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 3848
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-virtual {p0, p1, v0}, Landroid/app/ActivityThread;->applyConfigurationToResourcesLocked(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z

    #@7
    .line 3849
    monitor-exit v1

    #@8
    .line 3850
    return-void

    #@9
    .line 3849
    :catchall_9
    move-exception v0

    #@a
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method final applyConfigurationToResourcesLocked(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z
    .registers 19
    .parameter "config"
    .parameter "compat"

    #@0
    .prologue
    .line 3854
    move-object/from16 v0, p0

    #@2
    iget-object v4, v0, Landroid/app/ActivityThread;->mResConfiguration:Landroid/content/res/Configuration;

    #@4
    if-nez v4, :cond_f

    #@6
    .line 3855
    new-instance v4, Landroid/content/res/Configuration;

    #@8
    invoke-direct {v4}, Landroid/content/res/Configuration;-><init>()V

    #@b
    move-object/from16 v0, p0

    #@d
    iput-object v4, v0, Landroid/app/ActivityThread;->mResConfiguration:Landroid/content/res/Configuration;

    #@f
    .line 3857
    :cond_f
    move-object/from16 v0, p0

    #@11
    iget-object v4, v0, Landroid/app/ActivityThread;->mResConfiguration:Landroid/content/res/Configuration;

    #@13
    move-object/from16 v0, p1

    #@15
    invoke-virtual {v4, v0}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@18
    move-result v4

    #@19
    if-nez v4, :cond_1f

    #@1b
    if-nez p2, :cond_1f

    #@1d
    .line 3860
    const/4 v4, 0x0

    #@1e
    .line 3934
    :goto_1e
    return v4

    #@1f
    .line 3862
    :cond_1f
    move-object/from16 v0, p0

    #@21
    iget-object v4, v0, Landroid/app/ActivityThread;->mResConfiguration:Landroid/content/res/Configuration;

    #@23
    move-object/from16 v0, p1

    #@25
    invoke-virtual {v4, v0}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@28
    move-result v8

    #@29
    .line 3863
    .local v8, changes:I
    invoke-direct/range {p0 .. p0}, Landroid/app/ActivityThread;->flushDisplayMetricsLocked()V

    #@2c
    .line 3864
    const/4 v4, 0x0

    #@2d
    const/4 v5, 0x0

    #@2e
    move-object/from16 v0, p0

    #@30
    invoke-virtual {v0, v4, v5}, Landroid/app/ActivityThread;->getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;

    #@33
    move-result-object v9

    #@34
    .line 3867
    .local v9, defaultDisplayMetrics:Landroid/util/DisplayMetrics;
    if-eqz p2, :cond_50

    #@36
    move-object/from16 v0, p0

    #@38
    iget-object v4, v0, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@3a
    if-eqz v4, :cond_48

    #@3c
    move-object/from16 v0, p0

    #@3e
    iget-object v4, v0, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@40
    move-object/from16 v0, p2

    #@42
    invoke-virtual {v4, v0}, Landroid/content/res/CompatibilityInfo;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v4

    #@46
    if-nez v4, :cond_50

    #@48
    .line 3869
    :cond_48
    move-object/from16 v0, p2

    #@4a
    move-object/from16 v1, p0

    #@4c
    iput-object v0, v1, Landroid/app/ActivityThread;->mResCompatibilityInfo:Landroid/content/res/CompatibilityInfo;

    #@4e
    .line 3870
    or-int/lit16 v8, v8, 0xd00

    #@50
    .line 3876
    :cond_50
    move-object/from16 v0, p1

    #@52
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@54
    if-eqz v4, :cond_5d

    #@56
    .line 3877
    move-object/from16 v0, p1

    #@58
    iget-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@5a
    invoke-static {v4}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    #@5d
    .line 3880
    :cond_5d
    move-object/from16 v0, p1

    #@5f
    move-object/from16 v1, p2

    #@61
    invoke-static {v0, v9, v1}, Landroid/content/res/Resources;->updateSystemConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@64
    .line 3881
    move-object/from16 v0, p0

    #@66
    iget-boolean v4, v0, Landroid/app/ActivityThread;->mThemeIconEnabled:Z

    #@68
    if-eqz v4, :cond_71

    #@6a
    .line 3882
    move-object/from16 v0, p1

    #@6c
    move-object/from16 v1, p2

    #@6e
    invoke-static {v0, v9, v1}, Landroid/content/thm/ThemeIconManager;->updateThemeConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@71
    .line 3885
    :cond_71
    invoke-static {}, Landroid/app/ApplicationPackageManager;->configurationChanged()V

    #@74
    .line 3888
    const/4 v15, 0x0

    #@75
    .line 3890
    .local v15, tmpConfig:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@77
    iget-object v4, v0, Landroid/app/ActivityThread;->mActiveResources:Ljava/util/HashMap;

    #@79
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@7c
    move-result-object v4

    #@7d
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@80
    move-result-object v13

    #@81
    .line 3892
    .local v13, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/app/ActivityThread$ResourcesKey;Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;>;>;"
    :cond_81
    :goto_81
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    #@84
    move-result v4

    #@85
    if-eqz v4, :cond_ff

    #@87
    .line 3893
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8a
    move-result-object v11

    #@8b
    check-cast v11, Ljava/util/Map$Entry;

    #@8d
    .line 3894
    .local v11, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/ActivityThread$ResourcesKey;Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@90
    move-result-object v4

    #@91
    check-cast v4, Ljava/lang/ref/WeakReference;

    #@93
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@96
    move-result-object v3

    #@97
    check-cast v3, Landroid/content/res/Resources;

    #@99
    .line 3895
    .local v3, r:Landroid/content/res/Resources;
    if-eqz v3, :cond_fb

    #@9b
    .line 3898
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@9e
    move-result-object v4

    #@9f
    check-cast v4, Landroid/app/ActivityThread$ResourcesKey;

    #@a1
    invoke-static {v4}, Landroid/app/ActivityThread$ResourcesKey;->access$3300(Landroid/app/ActivityThread$ResourcesKey;)I

    #@a4
    move-result v10

    #@a5
    .line 3899
    .local v10, displayId:I
    if-nez v10, :cond_ef

    #@a7
    const/4 v12, 0x1

    #@a8
    .line 3900
    .local v12, isDefaultDisplay:Z
    :goto_a8
    move-object v6, v9

    #@a9
    .line 3901
    .local v6, dm:Landroid/util/DisplayMetrics;
    invoke-interface {v11}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@ac
    move-result-object v4

    #@ad
    check-cast v4, Landroid/app/ActivityThread$ResourcesKey;

    #@af
    invoke-static {v4}, Landroid/app/ActivityThread$ResourcesKey;->access$3000(Landroid/app/ActivityThread$ResourcesKey;)Landroid/content/res/Configuration;

    #@b2
    move-result-object v14

    #@b3
    .line 3902
    .local v14, overrideConfig:Landroid/content/res/Configuration;
    if-eqz v12, :cond_b7

    #@b5
    if-eqz v14, :cond_f1

    #@b7
    .line 3903
    :cond_b7
    if-nez v15, :cond_be

    #@b9
    .line 3904
    new-instance v15, Landroid/content/res/Configuration;

    #@bb
    .end local v15           #tmpConfig:Landroid/content/res/Configuration;
    invoke-direct {v15}, Landroid/content/res/Configuration;-><init>()V

    #@be
    .line 3906
    .restart local v15       #tmpConfig:Landroid/content/res/Configuration;
    :cond_be
    move-object/from16 v0, p1

    #@c0
    invoke-virtual {v15, v0}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    #@c3
    .line 3907
    if-nez v12, :cond_d1

    #@c5
    .line 3908
    const/4 v4, 0x0

    #@c6
    move-object/from16 v0, p0

    #@c8
    invoke-virtual {v0, v10, v4}, Landroid/app/ActivityThread;->getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;

    #@cb
    move-result-object v6

    #@cc
    .line 3909
    move-object/from16 v0, p0

    #@ce
    invoke-virtual {v0, v6, v15}, Landroid/app/ActivityThread;->applyNonDefaultDisplayMetricsToConfigurationLocked(Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    #@d1
    .line 3911
    :cond_d1
    if-eqz v14, :cond_d6

    #@d3
    .line 3912
    invoke-virtual {v15, v14}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@d6
    .line 3914
    :cond_d6
    move-object/from16 v0, p2

    #@d8
    invoke-virtual {v3, v15, v6, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@db
    .line 3921
    :goto_db
    invoke-virtual {v3}, Landroid/content/res/Resources;->getThemeIconManager()Landroid/content/thm/ThemeIconManager;

    #@de
    move-result-object v2

    #@df
    .line 3922
    .local v2, tm:Landroid/content/thm/ThemeIconManager;
    if-eqz v2, :cond_81

    #@e1
    .line 3923
    const/high16 v4, 0x1000

    #@e3
    and-int/2addr v4, v8

    #@e4
    if-eqz v4, :cond_f9

    #@e6
    const/4 v4, 0x1

    #@e7
    :goto_e7
    move-object/from16 v5, p1

    #@e9
    move-object/from16 v7, p2

    #@eb
    invoke-virtual/range {v2 .. v7}, Landroid/content/thm/ThemeIconManager;->updateConfiguration(Landroid/content/res/Resources;ZLandroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@ee
    goto :goto_81

    #@ef
    .line 3899
    .end local v2           #tm:Landroid/content/thm/ThemeIconManager;
    .end local v6           #dm:Landroid/util/DisplayMetrics;
    .end local v12           #isDefaultDisplay:Z
    .end local v14           #overrideConfig:Landroid/content/res/Configuration;
    :cond_ef
    const/4 v12, 0x0

    #@f0
    goto :goto_a8

    #@f1
    .line 3916
    .restart local v6       #dm:Landroid/util/DisplayMetrics;
    .restart local v12       #isDefaultDisplay:Z
    .restart local v14       #overrideConfig:Landroid/content/res/Configuration;
    :cond_f1
    move-object/from16 v0, p1

    #@f3
    move-object/from16 v1, p2

    #@f5
    invoke-virtual {v3, v0, v6, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@f8
    goto :goto_db

    #@f9
    .line 3923
    .restart local v2       #tm:Landroid/content/thm/ThemeIconManager;
    :cond_f9
    const/4 v4, 0x0

    #@fa
    goto :goto_e7

    #@fb
    .line 3930
    .end local v2           #tm:Landroid/content/thm/ThemeIconManager;
    .end local v6           #dm:Landroid/util/DisplayMetrics;
    .end local v10           #displayId:I
    .end local v12           #isDefaultDisplay:Z
    .end local v14           #overrideConfig:Landroid/content/res/Configuration;
    :cond_fb
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    #@fe
    goto :goto_81

    #@ff
    .line 3934
    .end local v3           #r:Landroid/content/res/Resources;
    .end local v11           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/app/ActivityThread$ResourcesKey;Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;>;"
    :cond_ff
    if-eqz v8, :cond_104

    #@101
    const/4 v4, 0x1

    #@102
    goto/16 :goto_1e

    #@104
    :cond_104
    const/4 v4, 0x0

    #@105
    goto/16 :goto_1e
.end method

.method final applyNonDefaultDisplayMetricsToConfigurationLocked(Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V
    .registers 7
    .parameter "dm"
    .parameter "config"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 3939
    iput v3, p2, Landroid/content/res/Configuration;->touchscreen:I

    #@3
    .line 3940
    iget v1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@5
    iput v1, p2, Landroid/content/res/Configuration;->densityDpi:I

    #@7
    .line 3941
    iget v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@9
    int-to-float v1, v1

    #@a
    iget v2, p1, Landroid/util/DisplayMetrics;->density:F

    #@c
    div-float/2addr v1, v2

    #@d
    float-to-int v1, v1

    #@e
    iput v1, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@10
    .line 3942
    iget v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@12
    int-to-float v1, v1

    #@13
    iget v2, p1, Landroid/util/DisplayMetrics;->density:F

    #@15
    div-float/2addr v1, v2

    #@16
    float-to-int v1, v1

    #@17
    iput v1, p2, Landroid/content/res/Configuration;->screenHeightDp:I

    #@19
    .line 3943
    iget v1, p2, Landroid/content/res/Configuration;->screenLayout:I

    #@1b
    invoke-static {v1}, Landroid/content/res/Configuration;->resetScreenLayout(I)I

    #@1e
    move-result v0

    #@1f
    .line 3944
    .local v0, sl:I
    iget v1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    #@21
    iget v2, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    #@23
    if-le v1, v2, :cond_43

    #@25
    .line 3945
    const/4 v1, 0x2

    #@26
    iput v1, p2, Landroid/content/res/Configuration;->orientation:I

    #@28
    .line 3946
    iget v1, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@2a
    iget v2, p2, Landroid/content/res/Configuration;->screenHeightDp:I

    #@2c
    invoke-static {v0, v1, v2}, Landroid/content/res/Configuration;->reduceScreenLayout(III)I

    #@2f
    move-result v1

    #@30
    iput v1, p2, Landroid/content/res/Configuration;->screenLayout:I

    #@32
    .line 3953
    :goto_32
    iget v1, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@34
    iput v1, p2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@36
    .line 3954
    iget v1, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@38
    iput v1, p2, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    #@3a
    .line 3955
    iget v1, p2, Landroid/content/res/Configuration;->screenHeightDp:I

    #@3c
    iput v1, p2, Landroid/content/res/Configuration;->compatScreenHeightDp:I

    #@3e
    .line 3956
    iget v1, p2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@40
    iput v1, p2, Landroid/content/res/Configuration;->compatSmallestScreenWidthDp:I

    #@42
    .line 3957
    return-void

    #@43
    .line 3949
    :cond_43
    iput v3, p2, Landroid/content/res/Configuration;->orientation:I

    #@45
    .line 3950
    iget v1, p2, Landroid/content/res/Configuration;->screenHeightDp:I

    #@47
    iget v2, p2, Landroid/content/res/Configuration;->screenWidthDp:I

    #@49
    invoke-static {v0, v1, v2}, Landroid/content/res/Configuration;->reduceScreenLayout(III)I

    #@4c
    move-result v1

    #@4d
    iput v1, p2, Landroid/content/res/Configuration;->screenLayout:I

    #@4f
    goto :goto_32
.end method

.method collectComponentCallbacks(ZLandroid/content/res/Configuration;)Ljava/util/ArrayList;
    .registers 15
    .parameter "allActivities"
    .parameter "newConfig"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Landroid/content/res/Configuration;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ComponentCallbacks2;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 3751
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 3754
    .local v3, callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentCallbacks2;>;"
    iget-object v10, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@7
    monitor-enter v10

    #@8
    .line 3755
    :try_start_8
    iget-object v9, p0, Landroid/app/ActivityThread;->mAllApplications:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    .line 3756
    .local v0, N:I
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    if-ge v4, v0, :cond_1d

    #@11
    .line 3757
    iget-object v9, p0, Landroid/app/ActivityThread;->mAllApplications:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 3756
    add-int/lit8 v4, v4, 0x1

    #@1c
    goto :goto_f

    #@1d
    .line 3759
    :cond_1d
    iget-object v9, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@1f
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@22
    move-result v9

    #@23
    if-lez v9, :cond_65

    #@25
    .line 3760
    iget-object v9, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@27
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@2a
    move-result-object v9

    #@2b
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2e
    move-result-object v5

    #@2f
    .local v5, i$:Ljava/util/Iterator;
    :cond_2f
    :goto_2f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@32
    move-result v9

    #@33
    if-eqz v9, :cond_65

    #@35
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@3b
    .line 3761
    .local v2, ar:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v1, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@3d
    .line 3762
    .local v1, a:Landroid/app/Activity;
    if-eqz v1, :cond_2f

    #@3f
    .line 3763
    iget v9, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@41
    iget-object v11, v2, Landroid/app/ActivityThread$ActivityClientRecord;->packageInfo:Landroid/app/LoadedApk;

    #@43
    iget-object v11, v11, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@45
    invoke-virtual {v11}, Landroid/view/CompatibilityInfoHolder;->getIfNeeded()Landroid/content/res/CompatibilityInfo;

    #@48
    move-result-object v11

    #@49
    invoke-virtual {p0, v9, p2, v11}, Landroid/app/ActivityThread;->applyConfigCompatMainThread(ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Configuration;

    #@4c
    move-result-object v8

    #@4d
    .line 3765
    .local v8, thisConfig:Landroid/content/res/Configuration;
    iget-object v9, v2, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@4f
    iget-boolean v9, v9, Landroid/app/Activity;->mFinished:Z

    #@51
    if-nez v9, :cond_60

    #@53
    if-nez p1, :cond_59

    #@55
    iget-boolean v9, v2, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@57
    if-nez v9, :cond_60

    #@59
    .line 3768
    :cond_59
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5c
    goto :goto_2f

    #@5d
    .line 3789
    .end local v0           #N:I
    .end local v1           #a:Landroid/app/Activity;
    .end local v2           #ar:Landroid/app/ActivityThread$ActivityClientRecord;
    .end local v4           #i:I
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v8           #thisConfig:Landroid/content/res/Configuration;
    :catchall_5d
    move-exception v9

    #@5e
    monitor-exit v10
    :try_end_5f
    .catchall {:try_start_8 .. :try_end_5f} :catchall_5d

    #@5f
    throw v9

    #@60
    .line 3769
    .restart local v0       #N:I
    .restart local v1       #a:Landroid/app/Activity;
    .restart local v2       #ar:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v4       #i:I
    .restart local v5       #i$:Ljava/util/Iterator;
    .restart local v8       #thisConfig:Landroid/content/res/Configuration;
    :cond_60
    if-eqz v8, :cond_2f

    #@62
    .line 3779
    :try_start_62
    iput-object v8, v2, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@64
    goto :goto_2f

    #@65
    .line 3784
    .end local v1           #a:Landroid/app/Activity;
    .end local v2           #ar:Landroid/app/ActivityThread$ActivityClientRecord;
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v8           #thisConfig:Landroid/content/res/Configuration;
    :cond_65
    iget-object v9, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@67
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@6a
    move-result v9

    #@6b
    if-lez v9, :cond_87

    #@6d
    .line 3785
    iget-object v9, p0, Landroid/app/ActivityThread;->mServices:Ljava/util/HashMap;

    #@6f
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@72
    move-result-object v9

    #@73
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@76
    move-result-object v5

    #@77
    .restart local v5       #i$:Ljava/util/Iterator;
    :goto_77
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@7a
    move-result v9

    #@7b
    if-eqz v9, :cond_87

    #@7d
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@80
    move-result-object v7

    #@81
    check-cast v7, Landroid/app/Service;

    #@83
    .line 3786
    .local v7, service:Landroid/app/Service;
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@86
    goto :goto_77

    #@87
    .line 3789
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v7           #service:Landroid/app/Service;
    :cond_87
    monitor-exit v10
    :try_end_88
    .catchall {:try_start_62 .. :try_end_88} :catchall_5d

    #@88
    .line 3790
    iget-object v10, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@8a
    monitor-enter v10

    #@8b
    .line 3791
    :try_start_8b
    iget-object v9, p0, Landroid/app/ActivityThread;->mLocalProviders:Ljava/util/HashMap;

    #@8d
    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    #@90
    move-result v9

    #@91
    if-lez v9, :cond_b2

    #@93
    .line 3792
    iget-object v9, p0, Landroid/app/ActivityThread;->mLocalProviders:Ljava/util/HashMap;

    #@95
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@98
    move-result-object v9

    #@99
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9c
    move-result-object v5

    #@9d
    .restart local v5       #i$:Ljava/util/Iterator;
    :goto_9d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@a0
    move-result v9

    #@a1
    if-eqz v9, :cond_b2

    #@a3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a6
    move-result-object v6

    #@a7
    check-cast v6, Landroid/app/ActivityThread$ProviderClientRecord;

    #@a9
    .line 3793
    .local v6, providerClientRecord:Landroid/app/ActivityThread$ProviderClientRecord;
    iget-object v9, v6, Landroid/app/ActivityThread$ProviderClientRecord;->mLocalProvider:Landroid/content/ContentProvider;

    #@ab
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@ae
    goto :goto_9d

    #@af
    .line 3796
    .end local v5           #i$:Ljava/util/Iterator;
    .end local v6           #providerClientRecord:Landroid/app/ActivityThread$ProviderClientRecord;
    :catchall_af
    move-exception v9

    #@b0
    monitor-exit v10
    :try_end_b1
    .catchall {:try_start_8b .. :try_end_b1} :catchall_af

    #@b1
    throw v9

    #@b2
    :cond_b2
    :try_start_b2
    monitor-exit v10
    :try_end_b3
    .catchall {:try_start_b2 .. :try_end_b3} :catchall_af

    #@b3
    .line 3798
    return-object v3
.end method

.method final completeRemoveProvider(Landroid/app/ActivityThread$ProviderRefCount;)V
    .registers 10
    .parameter "prc"

    #@0
    .prologue
    .line 4788
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@2
    monitor-enter v6

    #@3
    .line 4789
    :try_start_3
    iget-boolean v5, p1, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@5
    if-nez v5, :cond_9

    #@7
    .line 4795
    monitor-exit v6

    #@8
    .line 4824
    :goto_8
    return-void

    #@9
    .line 4798
    :cond_9
    iget-object v5, p1, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@b
    iget-object v5, v5, Landroid/app/IActivityManager$ContentProviderHolder;->provider:Landroid/content/IContentProvider;

    #@d
    invoke-interface {v5}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    .line 4799
    .local v2, jBinder:Landroid/os/IBinder;
    iget-object v5, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@13
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/app/ActivityThread$ProviderRefCount;

    #@19
    .line 4800
    .local v0, existingPrc:Landroid/app/ActivityThread$ProviderRefCount;
    if-ne v0, p1, :cond_20

    #@1b
    .line 4801
    iget-object v5, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@1d
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 4804
    :cond_20
    iget-object v5, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@22
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@25
    move-result-object v5

    #@26
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@29
    move-result-object v1

    #@2a
    .line 4805
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/ActivityThread$ProviderClientRecord;>;"
    :cond_2a
    :goto_2a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@2d
    move-result v5

    #@2e
    if-eqz v5, :cond_45

    #@30
    .line 4806
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@33
    move-result-object v4

    #@34
    check-cast v4, Landroid/app/ActivityThread$ProviderClientRecord;

    #@36
    .line 4807
    .local v4, pr:Landroid/app/ActivityThread$ProviderClientRecord;
    iget-object v5, v4, Landroid/app/ActivityThread$ProviderClientRecord;->mProvider:Landroid/content/IContentProvider;

    #@38
    invoke-interface {v5}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@3b
    move-result-object v3

    #@3c
    .line 4808
    .local v3, myBinder:Landroid/os/IBinder;
    if-ne v3, v2, :cond_2a

    #@3e
    .line 4809
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@41
    goto :goto_2a

    #@42
    .line 4812
    .end local v0           #existingPrc:Landroid/app/ActivityThread$ProviderRefCount;
    .end local v1           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/ActivityThread$ProviderClientRecord;>;"
    .end local v2           #jBinder:Landroid/os/IBinder;
    .end local v3           #myBinder:Landroid/os/IBinder;
    .end local v4           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    :catchall_42
    move-exception v5

    #@43
    monitor-exit v6
    :try_end_44
    .catchall {:try_start_3 .. :try_end_44} :catchall_42

    #@44
    throw v5

    #@45
    .restart local v0       #existingPrc:Landroid/app/ActivityThread$ProviderRefCount;
    .restart local v1       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/ActivityThread$ProviderClientRecord;>;"
    .restart local v2       #jBinder:Landroid/os/IBinder;
    :cond_45
    :try_start_45
    monitor-exit v6
    :try_end_46
    .catchall {:try_start_45 .. :try_end_46} :catchall_42

    #@46
    .line 4819
    :try_start_46
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@49
    move-result-object v5

    #@4a
    iget-object v6, p1, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@4c
    iget-object v6, v6, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@4e
    const/4 v7, 0x0

    #@4f
    invoke-interface {v5, v6, v7}, Landroid/app/IActivityManager;->removeContentProvider(Landroid/os/IBinder;Z)V
    :try_end_52
    .catch Landroid/os/RemoteException; {:try_start_46 .. :try_end_52} :catch_53

    #@52
    goto :goto_8

    #@53
    .line 4821
    :catch_53
    move-exception v5

    #@54
    goto :goto_8
.end method

.method doGcIfNeeded()V
    .registers 7

    #@0
    .prologue
    .line 1978
    const/4 v2, 0x0

    #@1
    iput-boolean v2, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@3
    .line 1979
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v0

    #@7
    .line 1982
    .local v0, now:J
    invoke-static {}, Lcom/android/internal/os/BinderInternal;->getLastGcTime()J

    #@a
    move-result-wide v2

    #@b
    const-wide/16 v4, 0x1388

    #@d
    add-long/2addr v2, v4

    #@e
    cmp-long v2, v2, v0

    #@10
    if-gez v2, :cond_17

    #@12
    .line 1984
    const-string v2, "bg"

    #@14
    invoke-static {v2}, Lcom/android/internal/os/BinderInternal;->forceGc(Ljava/lang/String;)V

    #@17
    .line 1986
    :cond_17
    return-void
.end method

.method ensureJitEnabled()V
    .registers 2

    #@0
    .prologue
    .line 1955
    iget-boolean v0, p0, Landroid/app/ActivityThread;->mJitEnabled:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 1956
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/app/ActivityThread;->mJitEnabled:Z

    #@7
    .line 1957
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->startJitCompilation()V

    #@e
    .line 1959
    :cond_e
    return-void
.end method

.method final finishInstrumentation(ILandroid/os/Bundle;)V
    .registers 5
    .parameter "resultCode"
    .parameter "results"

    #@0
    .prologue
    .line 4521
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    .line 4522
    .local v0, am:Landroid/app/IActivityManager;
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@6
    iget-object v1, v1, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@8
    if-eqz v1, :cond_19

    #@a
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@c
    iget-boolean v1, v1, Landroid/app/ActivityThread$Profiler;->handlingProfiling:Z

    #@e
    if-eqz v1, :cond_19

    #@10
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@12
    iget-object v1, v1, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@14
    if-nez v1, :cond_19

    #@16
    .line 4524
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    #@19
    .line 4529
    :cond_19
    :try_start_19
    iget-object v1, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@1b
    invoke-interface {v0, v1, p1, p2}, Landroid/app/IActivityManager;->finishInstrumentation(Landroid/app/IApplicationThread;ILandroid/os/Bundle;)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_1e} :catch_1f

    #@1e
    .line 4532
    :goto_1e
    return-void

    #@1f
    .line 4530
    :catch_1f
    move-exception v1

    #@20
    goto :goto_1e
.end method

.method final freeTextLayoutCachesIfNeeded(I)V
    .registers 4
    .parameter "configDiff"

    #@0
    .prologue
    .line 4027
    if-eqz p1, :cond_c

    #@2
    .line 4029
    and-int/lit8 v1, p1, 0x4

    #@4
    if-eqz v1, :cond_d

    #@6
    const/4 v0, 0x1

    #@7
    .line 4030
    .local v0, hasLocaleConfigChange:Z
    :goto_7
    if-eqz v0, :cond_c

    #@9
    .line 4031
    invoke-static {}, Landroid/graphics/Canvas;->freeTextLayoutCaches()V

    #@c
    .line 4035
    .end local v0           #hasLocaleConfigChange:Z
    :cond_c
    return-void

    #@d
    .line 4029
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_7
.end method

.method public final getActivity(Landroid/os/IBinder;)Landroid/app/Activity;
    .registers 3
    .parameter "token"

    #@0
    .prologue
    .line 2049
    iget-object v0, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    iget-object v0, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@a
    return-object v0
.end method

.method public getApplication()Landroid/app/Application;
    .registers 2

    #@0
    .prologue
    .line 1916
    iget-object v0, p0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@2
    return-object v0
.end method

.method public getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;
    .registers 2

    #@0
    .prologue
    .line 1890
    iget-object v0, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@2
    return-object v0
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .registers 2

    #@0
    .prologue
    .line 1899
    iget-object v0, p0, Landroid/app/ActivityThread;->mResConfiguration:Landroid/content/res/Configuration;

    #@2
    return-object v0
.end method

.method getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;
    .registers 10
    .parameter "displayId"
    .parameter "ci"

    #@0
    .prologue
    .line 1604
    if-nez p1, :cond_12

    #@2
    const/4 v5, 0x1

    #@3
    .line 1605
    .local v5, isDefaultDisplay:Z
    :goto_3
    if-eqz v5, :cond_14

    #@5
    iget-object v6, p0, Landroid/app/ActivityThread;->mDefaultDisplayMetrics:Ljava/util/HashMap;

    #@7
    invoke-virtual {v6, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v6

    #@b
    check-cast v6, Landroid/util/DisplayMetrics;

    #@d
    move-object v3, v6

    #@e
    .line 1606
    .local v3, dm:Landroid/util/DisplayMetrics;
    :goto_e
    if-eqz v3, :cond_16

    #@10
    move-object v4, v3

    #@11
    .line 1637
    .end local v3           #dm:Landroid/util/DisplayMetrics;
    .local v4, dm:Ljava/lang/Object;
    :goto_11
    return-object v4

    #@12
    .line 1604
    .end local v4           #dm:Ljava/lang/Object;
    .end local v5           #isDefaultDisplay:Z
    :cond_12
    const/4 v5, 0x0

    #@13
    goto :goto_3

    #@14
    .line 1605
    .restart local v5       #isDefaultDisplay:Z
    :cond_14
    const/4 v3, 0x0

    #@15
    goto :goto_e

    #@16
    .line 1609
    .restart local v3       #dm:Landroid/util/DisplayMetrics;
    :cond_16
    new-instance v3, Landroid/util/DisplayMetrics;

    #@18
    .end local v3           #dm:Landroid/util/DisplayMetrics;
    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    #@1b
    .line 1611
    .restart local v3       #dm:Landroid/util/DisplayMetrics;
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@1e
    move-result-object v2

    #@1f
    .line 1612
    .local v2, displayManager:Landroid/hardware/display/DisplayManagerGlobal;
    if-nez v2, :cond_26

    #@21
    .line 1614
    invoke-virtual {v3}, Landroid/util/DisplayMetrics;->setToDefaults()V

    #@24
    move-object v4, v3

    #@25
    .line 1615
    .restart local v4       #dm:Ljava/lang/Object;
    goto :goto_11

    #@26
    .line 1618
    .end local v4           #dm:Ljava/lang/Object;
    :cond_26
    if-eqz v5, :cond_2d

    #@28
    .line 1619
    iget-object v6, p0, Landroid/app/ActivityThread;->mDefaultDisplayMetrics:Ljava/util/HashMap;

    #@2a
    invoke-virtual {v6, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 1622
    :cond_2d
    new-instance v0, Landroid/view/CompatibilityInfoHolder;

    #@2f
    invoke-direct {v0}, Landroid/view/CompatibilityInfoHolder;-><init>()V

    #@32
    .line 1623
    .local v0, cih:Landroid/view/CompatibilityInfoHolder;
    invoke-virtual {v0, p2}, Landroid/view/CompatibilityInfoHolder;->set(Landroid/content/res/CompatibilityInfo;)V

    #@35
    .line 1624
    invoke-virtual {v2, p1, v0}, Landroid/hardware/display/DisplayManagerGlobal;->getCompatibleDisplay(ILandroid/view/CompatibilityInfoHolder;)Landroid/view/Display;

    #@38
    move-result-object v1

    #@39
    .line 1625
    .local v1, d:Landroid/view/Display;
    if-eqz v1, :cond_40

    #@3b
    .line 1626
    invoke-virtual {v1, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@3e
    :goto_3e
    move-object v4, v3

    #@3f
    .line 1637
    .restart local v4       #dm:Ljava/lang/Object;
    goto :goto_11

    #@40
    .line 1632
    .end local v4           #dm:Ljava/lang/Object;
    :cond_40
    invoke-virtual {v3}, Landroid/util/DisplayMetrics;->setToDefaults()V

    #@43
    goto :goto_3e
.end method

.method final getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 1761
    iget-object v0, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@2
    return-object v0
.end method

.method public getInstrumentation()Landroid/app/Instrumentation;
    .registers 2

    #@0
    .prologue
    .line 1895
    iget-object v0, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    return-object v0
.end method

.method public getIntCoreSetting(Ljava/lang/String;I)I
    .registers 5
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 5102
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 5103
    :try_start_3
    iget-object v0, p0, Landroid/app/ActivityThread;->mCoreSettings:Landroid/os/Bundle;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 5104
    iget-object v0, p0, Landroid/app/ActivityThread;->mCoreSettings:Landroid/os/Bundle;

    #@9
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@c
    move-result p2

    #@d
    .end local p2
    monitor-exit v1

    #@e
    .line 5106
    :goto_e
    return p2

    #@f
    .restart local p2
    :cond_f
    monitor-exit v1

    #@10
    goto :goto_e

    #@11
    .line 5108
    .end local p2
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public getLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 1912
    iget-object v0, p0, Landroid/app/ActivityThread;->mLooper:Landroid/os/Looper;

    #@2
    return-object v0
.end method

.method public final getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)Landroid/app/LoadedApk;
    .registers 11
    .parameter "ai"
    .parameter "compatInfo"
    .parameter "flags"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 1813
    and-int/lit8 v1, p3, 0x1

    #@4
    if-eqz v1, :cond_8e

    #@6
    move v5, v0

    #@7
    .line 1814
    .local v5, includeCode:Z
    :goto_7
    if-eqz v5, :cond_26

    #@9
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@b
    if-eqz v1, :cond_26

    #@d
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@f
    const/16 v2, 0x3e8

    #@11
    if-eq v1, v2, :cond_26

    #@13
    iget-object v1, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@15
    if-eqz v1, :cond_25

    #@17
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@19
    iget-object v2, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@1b
    iget-object v2, v2, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@1d
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@1f
    invoke-static {v1, v2}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_26

    #@25
    :cond_25
    move v4, v0

    #@26
    .line 1818
    .local v4, securityViolation:Z
    :cond_26
    and-int/lit8 v1, p3, 0x3

    #@28
    if-ne v1, v0, :cond_91

    #@2a
    .line 1821
    if-eqz v4, :cond_91

    #@2c
    .line 1822
    new-instance v0, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v1, "Requesting code from "

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    iget-object v1, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    const-string v1, " (with uid "

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    const-string v1, ")"

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    .line 1824
    .local v6, msg:Ljava/lang/String;
    iget-object v0, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@55
    if-eqz v0, :cond_88

    #@57
    .line 1825
    new-instance v0, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    const-string v1, " to be run in process "

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    iget-object v1, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@68
    iget-object v1, v1, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    const-string v1, " (with uid "

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    iget-object v1, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@76
    iget-object v1, v1, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@78
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    const-string v1, ")"

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v6

    #@88
    .line 1829
    :cond_88
    new-instance v0, Ljava/lang/SecurityException;

    #@8a
    invoke-direct {v0, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@8d
    throw v0

    #@8e
    .end local v4           #securityViolation:Z
    .end local v5           #includeCode:Z
    .end local v6           #msg:Ljava/lang/String;
    :cond_8e
    move v5, v4

    #@8f
    .line 1813
    goto/16 :goto_7

    #@91
    .line 1832
    .restart local v4       #securityViolation:Z
    .restart local v5       #includeCode:Z
    :cond_91
    const/4 v3, 0x0

    #@92
    move-object v0, p0

    #@93
    move-object v1, p1

    #@94
    move-object v2, p2

    #@95
    invoke-direct/range {v0 .. v5}, Landroid/app/ActivityThread;->getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZ)Landroid/app/LoadedApk;

    #@98
    move-result-object v0

    #@99
    return-object v0
.end method

.method public final getPackageInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;I)Landroid/app/LoadedApk;
    .registers 5
    .parameter "packageName"
    .parameter "compatInfo"
    .parameter "flags"

    #@0
    .prologue
    .line 1766
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/ActivityThread;->getPackageInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;II)Landroid/app/LoadedApk;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final getPackageInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;II)Landroid/app/LoadedApk;
    .registers 12
    .parameter "packageName"
    .parameter "compatInfo"
    .parameter "flags"
    .parameter "userId"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1771
    iget-object v5, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@3
    monitor-enter v5

    #@4
    .line 1773
    and-int/lit8 v3, p3, 0x1

    #@6
    if-eqz v3, :cond_6f

    #@8
    .line 1774
    :try_start_8
    iget-object v3, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@a
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@10
    .line 1778
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :goto_10
    if-eqz v2, :cond_78

    #@12
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    check-cast v3, Landroid/app/LoadedApk;

    #@18
    move-object v1, v3

    #@19
    .line 1782
    .local v1, packageInfo:Landroid/app/LoadedApk;
    :goto_19
    if-eqz v1, :cond_7c

    #@1b
    iget-object v3, v1, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@1d
    if-eqz v3, :cond_2b

    #@1f
    iget-object v3, v1, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@21
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Landroid/content/res/AssetManager;->isUpToDate()Z

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_7c

    #@2b
    .line 1784
    :cond_2b
    invoke-virtual {v1}, Landroid/app/LoadedApk;->isSecurityViolation()Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_7a

    #@31
    and-int/lit8 v3, p3, 0x2

    #@33
    if-nez v3, :cond_7a

    #@35
    .line 1786
    new-instance v3, Ljava/lang/SecurityException;

    #@37
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v6, "Requesting code from "

    #@3e
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    const-string v6, " to be run in process "

    #@48
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v4

    #@4c
    iget-object v6, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@4e
    iget-object v6, v6, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@50
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    const-string v6, "/"

    #@56
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    iget-object v6, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@5c
    iget-object v6, v6, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@5e
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@60
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v4

    #@68
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@6b
    throw v3

    #@6c
    .line 1794
    .end local v1           #packageInfo:Landroid/app/LoadedApk;
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :catchall_6c
    move-exception v3

    #@6d
    monitor-exit v5
    :try_end_6e
    .catchall {:try_start_8 .. :try_end_6e} :catchall_6c

    #@6e
    throw v3

    #@6f
    .line 1776
    :cond_6f
    :try_start_6f
    iget-object v3, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@71
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@74
    move-result-object v2

    #@75
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@77
    .restart local v2       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    goto :goto_10

    #@78
    :cond_78
    move-object v1, v4

    #@79
    .line 1778
    goto :goto_19

    #@7a
    .line 1792
    .restart local v1       #packageInfo:Landroid/app/LoadedApk;
    :cond_7a
    monitor-exit v5

    #@7b
    .line 1808
    .end local v1           #packageInfo:Landroid/app/LoadedApk;
    :goto_7b
    return-object v1

    #@7c
    .line 1794
    .restart local v1       #packageInfo:Landroid/app/LoadedApk;
    :cond_7c
    monitor-exit v5
    :try_end_7d
    .catchall {:try_start_6f .. :try_end_7d} :catchall_6c

    #@7d
    .line 1796
    const/4 v0, 0x0

    #@7e
    .line 1798
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_7e
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@81
    move-result-object v3

    #@82
    const/16 v5, 0x400

    #@84
    invoke-interface {v3, p1, v5, p4}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_87
    .catch Landroid/os/RemoteException; {:try_start_7e .. :try_end_87} :catch_91

    #@87
    move-result-object v0

    #@88
    .line 1804
    :goto_88
    if-eqz v0, :cond_8f

    #@8a
    .line 1805
    invoke-virtual {p0, v0, p2, p3}, Landroid/app/ActivityThread;->getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)Landroid/app/LoadedApk;

    #@8d
    move-result-object v1

    #@8e
    goto :goto_7b

    #@8f
    :cond_8f
    move-object v1, v4

    #@90
    .line 1808
    goto :goto_7b

    #@91
    .line 1800
    :catch_91
    move-exception v3

    #@92
    goto :goto_88
.end method

.method public final getPackageInfoNoCheck(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;
    .registers 9
    .parameter "ai"
    .parameter "compatInfo"

    #@0
    .prologue
    .line 1837
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v2, p2

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/app/ActivityThread;->getPackageInfo(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZ)Landroid/app/LoadedApk;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getProcessName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1920
    iget-object v0, p0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@2
    iget-object v0, v0, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@4
    return-object v0
.end method

.method public getProfileFilePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1908
    iget-object v0, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@2
    iget-object v0, v0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@4
    return-object v0
.end method

.method public getSystemContext()Landroid/app/ContextImpl;
    .registers 7

    #@0
    .prologue
    .line 1924
    monitor-enter p0

    #@1
    .line 1925
    :try_start_1
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@3
    if-nez v1, :cond_2c

    #@5
    .line 1926
    invoke-static {p0}, Landroid/app/ContextImpl;->createSystemContext(Landroid/app/ActivityThread;)Landroid/app/ContextImpl;

    #@8
    move-result-object v3

    #@9
    .line 1928
    .local v3, context:Landroid/app/ContextImpl;
    new-instance v0, Landroid/app/LoadedApk;

    #@b
    const-string v2, "android"

    #@d
    const/4 v4, 0x0

    #@e
    sget-object v5, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@10
    move-object v1, p0

    #@11
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk;-><init>(Landroid/app/ActivityThread;Ljava/lang/String;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V

    #@14
    .line 1930
    .local v0, info:Landroid/app/LoadedApk;
    const/4 v1, 0x0

    #@15
    invoke-virtual {v3, v0, v1, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@18
    .line 1931
    invoke-virtual {v3}, Landroid/app/ContextImpl;->getResources()Landroid/content/res/Resources;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getConfiguration()Landroid/content/res/Configuration;

    #@1f
    move-result-object v2

    #@20
    const/4 v4, 0x0

    #@21
    sget-object v5, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@23
    invoke-virtual {p0, v4, v5}, Landroid/app/ActivityThread;->getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    #@2a
    .line 1935
    sput-object v3, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@2c
    .line 1939
    .end local v0           #info:Landroid/app/LoadedApk;
    .end local v3           #context:Landroid/app/ContextImpl;
    :cond_2c
    monitor-exit p0
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_30

    #@2d
    .line 1940
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@2f
    return-object v1

    #@30
    .line 1939
    :catchall_30
    move-exception v1

    #@31
    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    #@32
    throw v1
.end method

.method getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/app/LoadedApk;)Landroid/content/res/Resources;
    .registers 6
    .parameter "resDir"
    .parameter "displayId"
    .parameter "overrideConfiguration"
    .parameter "pkgInfo"

    #@0
    .prologue
    .line 1756
    iget-object v0, p4, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@2
    invoke-virtual {v0}, Landroid/view/CompatibilityInfoHolder;->get()Landroid/content/res/CompatibilityInfo;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/ActivityThread;->getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;
    .registers 18
    .parameter "resDir"
    .parameter "displayId"
    .parameter "overrideConfiguration"
    .parameter "compInfo"

    #@0
    .prologue
    .line 1664
    new-instance v6, Landroid/app/ActivityThread$ResourcesKey;

    #@2
    move-object/from16 v0, p4

    #@4
    iget v10, v0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@6
    move-object/from16 v0, p3

    #@8
    invoke-direct {v6, p1, p2, v0, v10}, Landroid/app/ActivityThread$ResourcesKey;-><init>(Ljava/lang/String;ILandroid/content/res/Configuration;F)V

    #@b
    .line 1668
    .local v6, key:Landroid/app/ActivityThread$ResourcesKey;
    iget-object v11, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@d
    monitor-enter v11

    #@e
    .line 1674
    :try_start_e
    iget-object v10, p0, Landroid/app/ActivityThread;->mActiveResources:Ljava/util/HashMap;

    #@10
    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v9

    #@14
    check-cast v9, Ljava/lang/ref/WeakReference;

    #@16
    .line 1675
    .local v9, wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    if-eqz v9, :cond_2e

    #@18
    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@1b
    move-result-object v10

    #@1c
    check-cast v10, Landroid/content/res/Resources;

    #@1e
    move-object v8, v10

    #@1f
    .line 1677
    .local v8, r:Landroid/content/res/Resources;
    :goto_1f
    if-eqz v8, :cond_30

    #@21
    invoke-virtual {v8}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@24
    move-result-object v10

    #@25
    invoke-virtual {v10}, Landroid/content/res/AssetManager;->isUpToDate()Z

    #@28
    move-result v10

    #@29
    if-eqz v10, :cond_30

    #@2b
    .line 1682
    monitor-exit v11

    #@2c
    move-object v4, v8

    #@2d
    .line 1746
    :goto_2d
    return-object v4

    #@2e
    .line 1675
    .end local v8           #r:Landroid/content/res/Resources;
    :cond_2e
    const/4 v8, 0x0

    #@2f
    goto :goto_1f

    #@30
    .line 1684
    .restart local v8       #r:Landroid/content/res/Resources;
    :cond_30
    monitor-exit v11
    :try_end_31
    .catchall {:try_start_e .. :try_end_31} :catchall_3e

    #@31
    .line 1691
    new-instance v1, Landroid/content/res/AssetManager;

    #@33
    invoke-direct {v1}, Landroid/content/res/AssetManager;-><init>()V

    #@36
    .line 1692
    .local v1, assets:Landroid/content/res/AssetManager;
    invoke-virtual {v1, p1}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    #@39
    move-result v10

    #@3a
    if-nez v10, :cond_41

    #@3c
    .line 1693
    const/4 v4, 0x0

    #@3d
    goto :goto_2d

    #@3e
    .line 1684
    .end local v1           #assets:Landroid/content/res/AssetManager;
    .end local v8           #r:Landroid/content/res/Resources;
    .end local v9           #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    :catchall_3e
    move-exception v10

    #@3f
    :try_start_3f
    monitor-exit v11
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v10

    #@41
    .line 1698
    .restart local v1       #assets:Landroid/content/res/AssetManager;
    .restart local v8       #r:Landroid/content/res/Resources;
    .restart local v9       #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    :cond_41
    :try_start_41
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@44
    move-result-object v10

    #@45
    invoke-interface {v10, p1}, Landroid/content/pm/IPackageManager;->getOverlayPath(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    .line 1699
    .local v7, overlayPath:Ljava/lang/String;
    if-eqz v7, :cond_4e

    #@4b
    .line 1700
    invoke-virtual {v1, p1, v7}, Landroid/content/res/AssetManager;->addOverlayPath(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_4e} :catch_cf

    #@4e
    .line 1708
    .end local v7           #overlayPath:Ljava/lang/String;
    :cond_4e
    :goto_4e
    const/4 v10, 0x0

    #@4f
    invoke-virtual {p0, p2, v10}, Landroid/app/ActivityThread;->getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;

    #@52
    move-result-object v3

    #@53
    .line 1710
    .local v3, dm:Landroid/util/DisplayMetrics;
    if-nez p2, :cond_ac

    #@55
    const/4 v5, 0x1

    #@56
    .line 1711
    .local v5, isDefaultDisplay:Z
    :goto_56
    if-eqz v5, :cond_5e

    #@58
    invoke-static {v6}, Landroid/app/ActivityThread$ResourcesKey;->access$3000(Landroid/app/ActivityThread$ResourcesKey;)Landroid/content/res/Configuration;

    #@5b
    move-result-object v10

    #@5c
    if-eqz v10, :cond_ae

    #@5e
    .line 1712
    :cond_5e
    new-instance v2, Landroid/content/res/Configuration;

    #@60
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getConfiguration()Landroid/content/res/Configuration;

    #@63
    move-result-object v10

    #@64
    invoke-direct {v2, v10}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    #@67
    .line 1713
    .local v2, config:Landroid/content/res/Configuration;
    if-nez v5, :cond_6c

    #@69
    .line 1714
    invoke-virtual {p0, v3, v2}, Landroid/app/ActivityThread;->applyNonDefaultDisplayMetricsToConfigurationLocked(Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    #@6c
    .line 1716
    :cond_6c
    invoke-static {v6}, Landroid/app/ActivityThread$ResourcesKey;->access$3000(Landroid/app/ActivityThread$ResourcesKey;)Landroid/content/res/Configuration;

    #@6f
    move-result-object v10

    #@70
    if-eqz v10, :cond_79

    #@72
    .line 1717
    invoke-static {v6}, Landroid/app/ActivityThread$ResourcesKey;->access$3000(Landroid/app/ActivityThread$ResourcesKey;)Landroid/content/res/Configuration;

    #@75
    move-result-object v10

    #@76
    invoke-virtual {v2, v10}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@79
    .line 1722
    :cond_79
    :goto_79
    new-instance v8, Landroid/content/res/Resources;

    #@7b
    .end local v8           #r:Landroid/content/res/Resources;
    move-object/from16 v0, p4

    #@7d
    invoke-direct {v8, v1, v3, v2, v0}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    #@80
    .line 1729
    .restart local v8       #r:Landroid/content/res/Resources;
    iget-object v11, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@82
    monitor-enter v11

    #@83
    .line 1730
    :try_start_83
    iget-object v10, p0, Landroid/app/ActivityThread;->mActiveResources:Ljava/util/HashMap;

    #@85
    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@88
    move-result-object v9

    #@89
    .end local v9           #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    check-cast v9, Ljava/lang/ref/WeakReference;

    #@8b
    .line 1731
    .restart local v9       #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    if-eqz v9, :cond_b3

    #@8d
    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@90
    move-result-object v10

    #@91
    check-cast v10, Landroid/content/res/Resources;

    #@93
    move-object v4, v10

    #@94
    .line 1732
    .local v4, existing:Landroid/content/res/Resources;
    :goto_94
    if-eqz v4, :cond_b5

    #@96
    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@99
    move-result-object v10

    #@9a
    invoke-virtual {v10}, Landroid/content/res/AssetManager;->isUpToDate()Z

    #@9d
    move-result v10

    #@9e
    if-eqz v10, :cond_b5

    #@a0
    .line 1735
    invoke-virtual {v8}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v10}, Landroid/content/res/AssetManager;->close()V

    #@a7
    .line 1736
    monitor-exit v11

    #@a8
    goto :goto_2d

    #@a9
    .line 1747
    .end local v4           #existing:Landroid/content/res/Resources;
    .end local v9           #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    :catchall_a9
    move-exception v10

    #@aa
    monitor-exit v11
    :try_end_ab
    .catchall {:try_start_83 .. :try_end_ab} :catchall_a9

    #@ab
    throw v10

    #@ac
    .line 1710
    .end local v2           #config:Landroid/content/res/Configuration;
    .end local v5           #isDefaultDisplay:Z
    .restart local v9       #wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/content/res/Resources;>;"
    :cond_ac
    const/4 v5, 0x0

    #@ad
    goto :goto_56

    #@ae
    .line 1720
    .restart local v5       #isDefaultDisplay:Z
    :cond_ae
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getConfiguration()Landroid/content/res/Configuration;

    #@b1
    move-result-object v2

    #@b2
    .restart local v2       #config:Landroid/content/res/Configuration;
    goto :goto_79

    #@b3
    .line 1731
    :cond_b3
    const/4 v4, 0x0

    #@b4
    goto :goto_94

    #@b5
    .line 1739
    .restart local v4       #existing:Landroid/content/res/Resources;
    :cond_b5
    :try_start_b5
    iget-boolean v10, p0, Landroid/app/ActivityThread;->mThemeIconEnabled:Z

    #@b7
    if-eqz v10, :cond_c1

    #@b9
    .line 1740
    new-instance v10, Landroid/content/thm/ThemeIconManager;

    #@bb
    invoke-direct {v10, v8}, Landroid/content/thm/ThemeIconManager;-><init>(Landroid/content/res/Resources;)V

    #@be
    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->setThemeIconManager(Landroid/content/thm/ThemeIconManager;)V

    #@c1
    .line 1745
    :cond_c1
    iget-object v10, p0, Landroid/app/ActivityThread;->mActiveResources:Ljava/util/HashMap;

    #@c3
    new-instance v12, Ljava/lang/ref/WeakReference;

    #@c5
    invoke-direct {v12, v8}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@c8
    invoke-virtual {v10, v6, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@cb
    .line 1746
    monitor-exit v11
    :try_end_cc
    .catchall {:try_start_b5 .. :try_end_cc} :catchall_a9

    #@cc
    move-object v4, v8

    #@cd
    goto/16 :goto_2d

    #@cf
    .line 1702
    .end local v2           #config:Landroid/content/res/Configuration;
    .end local v3           #dm:Landroid/util/DisplayMetrics;
    .end local v4           #existing:Landroid/content/res/Resources;
    .end local v5           #isDefaultDisplay:Z
    :catch_cf
    move-exception v10

    #@d0
    goto/16 :goto_4e
.end method

.method final handleActivityConfigurationChanged(Landroid/os/IBinder;)V
    .registers 5
    .parameter "token"

    #@0
    .prologue
    .line 4038
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 4039
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v0, :cond_e

    #@a
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@c
    if-nez v1, :cond_f

    #@e
    .line 4055
    :cond_e
    :goto_e
    return-void

    #@f
    .line 4046
    :cond_f
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@11
    iget-object v2, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@13
    invoke-static {v1, v2}, Landroid/app/ActivityThread;->performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V

    #@16
    .line 4049
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@18
    if-eqz v1, :cond_27

    #@1a
    .line 4050
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1c
    iget-object v1, v1, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@1e
    iget-object v2, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@20
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@23
    move-result v1

    #@24
    invoke-virtual {p0, v1}, Landroid/app/ActivityThread;->updateFontConfigurationIfNeeded(I)V

    #@27
    .line 4054
    :cond_27
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@29
    iget-object v1, v1, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@2b
    iget-object v2, p0, Landroid/app/ActivityThread;->mCompatConfiguration:Landroid/content/res/Configuration;

    #@2d
    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@30
    move-result v1

    #@31
    invoke-virtual {p0, v1}, Landroid/app/ActivityThread;->freeTextLayoutCachesIfNeeded(I)V

    #@34
    goto :goto_e
.end method

.method final handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V
    .registers 9
    .parameter "config"
    .parameter "compat"

    #@0
    .prologue
    .line 3974
    const/4 v2, 0x0

    #@1
    .line 3976
    .local v2, configDiff:I
    iget-object v5, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@3
    monitor-enter v5

    #@4
    .line 3977
    :try_start_4
    iget-object v4, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@6
    if-eqz v4, :cond_1c

    #@8
    .line 3978
    iget-object v4, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@a
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_19

    #@10
    .line 3979
    iget-object p1, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@12
    .line 3980
    iget v4, p1, Landroid/content/res/Configuration;->densityDpi:I

    #@14
    iput v4, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@16
    .line 3981
    invoke-direct {p0}, Landroid/app/ActivityThread;->updateDefaultDensity()V

    #@19
    .line 3983
    :cond_19
    const/4 v4, 0x0

    #@1a
    iput-object v4, p0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@1c
    .line 3986
    :cond_1c
    if-nez p1, :cond_20

    #@1e
    .line 3987
    monitor-exit v5

    #@1f
    .line 4024
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 3993
    :cond_20
    invoke-virtual {p0, p1, p2}, Landroid/app/ActivityThread;->applyConfigurationToResourcesLocked(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z

    #@23
    .line 3995
    iget-object v4, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@25
    if-nez v4, :cond_2e

    #@27
    .line 3996
    new-instance v4, Landroid/content/res/Configuration;

    #@29
    invoke-direct {v4}, Landroid/content/res/Configuration;-><init>()V

    #@2c
    iput-object v4, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@2e
    .line 3998
    :cond_2e
    iget-object v4, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@30
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@33
    move-result v4

    #@34
    if-nez v4, :cond_3d

    #@36
    if-nez p2, :cond_3d

    #@38
    .line 3999
    monitor-exit v5

    #@39
    goto :goto_1f

    #@3a
    .line 4004
    :catchall_3a
    move-exception v4

    #@3b
    monitor-exit v5
    :try_end_3c
    .catchall {:try_start_4 .. :try_end_3c} :catchall_3a

    #@3c
    throw v4

    #@3d
    .line 4001
    :cond_3d
    :try_start_3d
    iget-object v4, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@3f
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@42
    move-result v2

    #@43
    .line 4002
    iget-object v4, p0, Landroid/app/ActivityThread;->mConfiguration:Landroid/content/res/Configuration;

    #@45
    invoke-virtual {v4, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    #@48
    .line 4003
    iget v4, p0, Landroid/app/ActivityThread;->mCurDefaultDisplayDpi:I

    #@4a
    invoke-virtual {p0, v4}, Landroid/app/ActivityThread;->applyCompatConfiguration(I)Landroid/content/res/Configuration;

    #@4d
    move-result-object p1

    #@4e
    .line 4004
    monitor-exit v5
    :try_end_4f
    .catchall {:try_start_3d .. :try_end_4f} :catchall_3a

    #@4f
    .line 4006
    const/4 v4, 0x0

    #@50
    invoke-virtual {p0, v4, p1}, Landroid/app/ActivityThread;->collectComponentCallbacks(ZLandroid/content/res/Configuration;)Ljava/util/ArrayList;

    #@53
    move-result-object v1

    #@54
    .line 4009
    .local v1, callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentCallbacks2;>;"
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v4}, Landroid/view/WindowManagerGlobal;->trimLocalMemory()V

    #@5b
    .line 4012
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@5d
    if-eqz v4, :cond_62

    #@5f
    .line 4013
    invoke-virtual {p0, v2}, Landroid/app/ActivityThread;->updateFontConfigurationIfNeeded(I)V

    #@62
    .line 4016
    :cond_62
    invoke-virtual {p0, v2}, Landroid/app/ActivityThread;->freeTextLayoutCachesIfNeeded(I)V

    #@65
    .line 4018
    if-eqz v1, :cond_1f

    #@67
    .line 4019
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@6a
    move-result v0

    #@6b
    .line 4020
    .local v0, N:I
    const/4 v3, 0x0

    #@6c
    .local v3, i:I
    :goto_6c
    if-ge v3, v0, :cond_1f

    #@6e
    .line 4021
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@71
    move-result-object v4

    #@72
    check-cast v4, Landroid/content/ComponentCallbacks2;

    #@74
    invoke-static {v4, p1}, Landroid/app/ActivityThread;->performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V

    #@77
    .line 4020
    add-int/lit8 v3, v3, 0x1

    #@79
    goto :goto_6c
.end method

.method final handleDispatchPackageBroadcast(I[Ljava/lang/String;)V
    .registers 8
    .parameter "cmd"
    .parameter "packages"

    #@0
    .prologue
    .line 4106
    const/4 v0, 0x0

    #@1
    .line 4107
    .local v0, hasPkgInfo:Z
    if-eqz p2, :cond_42

    #@3
    .line 4108
    array-length v3, p2

    #@4
    add-int/lit8 v1, v3, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_42

    #@8
    .line 4110
    if-nez v0, :cond_1d

    #@a
    .line 4112
    iget-object v3, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@c
    aget-object v4, p2, v1

    #@e
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@14
    .line 4113
    .local v2, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    if-eqz v2, :cond_2e

    #@16
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    if-eqz v3, :cond_2e

    #@1c
    .line 4114
    const/4 v0, 0x1

    #@1d
    .line 4122
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :cond_1d
    :goto_1d
    iget-object v3, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@1f
    aget-object v4, p2, v1

    #@21
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 4123
    iget-object v3, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@26
    aget-object v4, p2, v1

    #@28
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 4108
    add-int/lit8 v1, v1, -0x1

    #@2d
    goto :goto_6

    #@2e
    .line 4116
    .restart local v2       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :cond_2e
    iget-object v3, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@30
    aget-object v4, p2, v1

    #@32
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    move-result-object v2

    #@36
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@38
    .line 4117
    .restart local v2       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    if-eqz v2, :cond_1d

    #@3a
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@3d
    move-result-object v3

    #@3e
    if-eqz v3, :cond_1d

    #@40
    .line 4118
    const/4 v0, 0x1

    #@41
    goto :goto_1d

    #@42
    .line 4126
    .end local v1           #i:I
    .end local v2           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :cond_42
    invoke-static {p1, p2, v0}, Landroid/app/ApplicationPackageManager;->handlePackageBroadcast(I[Ljava/lang/String;Z)V

    #@45
    .line 4128
    return-void
.end method

.method final handleLowMemory()V
    .registers 7

    #@0
    .prologue
    .line 4131
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    invoke-virtual {p0, v4, v5}, Landroid/app/ActivityThread;->collectComponentCallbacks(ZLandroid/content/res/Configuration;)Ljava/util/ArrayList;

    #@5
    move-result-object v1

    #@6
    .line 4133
    .local v1, callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentCallbacks2;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    .line 4134
    .local v0, N:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    if-ge v2, v0, :cond_19

    #@d
    .line 4135
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Landroid/content/ComponentCallbacks2;

    #@13
    invoke-interface {v4}, Landroid/content/ComponentCallbacks2;->onLowMemory()V

    #@16
    .line 4134
    add-int/lit8 v2, v2, 0x1

    #@18
    goto :goto_b

    #@19
    .line 4139
    :cond_19
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@1c
    move-result v4

    #@1d
    const/16 v5, 0x3e8

    #@1f
    if-eq v4, v5, :cond_2b

    #@21
    .line 4140
    invoke-static {}, Landroid/database/sqlite/SQLiteDatabase;->releaseMemory()I

    #@24
    move-result v3

    #@25
    .line 4141
    .local v3, sqliteReleased:I
    const v4, 0x124fb

    #@28
    invoke-static {v4, v3}, Landroid/util/EventLog;->writeEvent(II)I

    #@2b
    .line 4145
    .end local v3           #sqliteReleased:I
    :cond_2b
    invoke-static {}, Landroid/graphics/Canvas;->freeCaches()V

    #@2e
    .line 4148
    invoke-static {}, Landroid/graphics/Canvas;->freeTextLayoutCaches()V

    #@31
    .line 4150
    const-string/jumbo v4, "mem"

    #@34
    invoke-static {v4}, Lcom/android/internal/os/BinderInternal;->forceGc(Ljava/lang/String;)V

    #@37
    .line 4151
    return-void
.end method

.method final handleProfilerControl(ZLandroid/app/ActivityThread$ProfilerControlData;I)V
    .registers 8
    .parameter "start"
    .parameter "pcd"
    .parameter "profileType"

    #@0
    .prologue
    .line 4058
    if-eqz p1, :cond_52

    #@2
    .line 4062
    :try_start_2
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@4
    iget-object v2, p2, Landroid/app/ActivityThread$ProfilerControlData;->path:Ljava/lang/String;

    #@6
    iget-object v3, p2, Landroid/app/ActivityThread$ProfilerControlData;->fd:Landroid/os/ParcelFileDescriptor;

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/app/ActivityThread$Profiler;->setProfiler(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    #@b
    .line 4063
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@d
    const/4 v2, 0x0

    #@e
    iput-boolean v2, v1, Landroid/app/ActivityThread$Profiler;->autoStopProfiler:Z

    #@10
    .line 4064
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@12
    invoke-virtual {v1}, Landroid/app/ActivityThread$Profiler;->startProfiling()V
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_4b
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_15} :catch_1b

    #@15
    .line 4072
    :try_start_15
    iget-object v1, p2, Landroid/app/ActivityThread$ProfilerControlData;->fd:Landroid/os/ParcelFileDescriptor;

    #@17
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1a} :catch_61

    #@1a
    .line 4084
    :goto_1a
    return-void

    #@1b
    .line 4067
    :catch_1b
    move-exception v0

    #@1c
    .line 4068
    .local v0, e:Ljava/lang/RuntimeException;
    :try_start_1c
    const-string v1, "ActivityThread"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "Profiling failed on path "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    iget-object v3, p2, Landroid/app/ActivityThread$ProfilerControlData;->path:Ljava/lang/String;

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " -- can the process access this path?"

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c
    .catchall {:try_start_1c .. :try_end_3c} :catchall_4b

    #@3c
    .line 4072
    :try_start_3c
    iget-object v1, p2, Landroid/app/ActivityThread$ProfilerControlData;->fd:Landroid/os/ParcelFileDescriptor;

    #@3e
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_41} :catch_42

    #@41
    goto :goto_1a

    #@42
    .line 4073
    :catch_42
    move-exception v0

    #@43
    .line 4074
    .local v0, e:Ljava/io/IOException;
    const-string v1, "ActivityThread"

    #@45
    const-string v2, "Failure closing profile fd"

    #@47
    :goto_47
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_1a

    #@4b
    .line 4071
    .end local v0           #e:Ljava/io/IOException;
    :catchall_4b
    move-exception v1

    #@4c
    .line 4072
    :try_start_4c
    iget-object v2, p2, Landroid/app/ActivityThread$ProfilerControlData;->fd:Landroid/os/ParcelFileDescriptor;

    #@4e
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_51} :catch_58

    #@51
    .line 4071
    :goto_51
    throw v1

    #@52
    .line 4080
    :cond_52
    iget-object v1, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@54
    invoke-virtual {v1}, Landroid/app/ActivityThread$Profiler;->stopProfiling()V

    #@57
    goto :goto_1a

    #@58
    .line 4073
    :catch_58
    move-exception v0

    #@59
    .line 4074
    .restart local v0       #e:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@5b
    const-string v3, "Failure closing profile fd"

    #@5d
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@60
    goto :goto_51

    #@61
    .line 4073
    .end local v0           #e:Ljava/io/IOException;
    :catch_61
    move-exception v0

    #@62
    .line 4074
    .restart local v0       #e:Ljava/io/IOException;
    const-string v1, "ActivityThread"

    #@64
    const-string v2, "Failure closing profile fd"

    #@66
    goto :goto_47
.end method

.method final handleResumeActivity(Landroid/os/IBinder;ZZZ)V
    .registers 15
    .parameter "token"
    .parameter "clearHide"
    .parameter "isForward"
    .parameter "reallyResume"

    #@0
    .prologue
    .line 2791
    invoke-virtual {p0}, Landroid/app/ActivityThread;->unscheduleGcIdler()V

    #@3
    .line 2793
    invoke-virtual {p0, p1, p2}, Landroid/app/ActivityThread;->performResumeActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@6
    move-result-object v4

    #@7
    .line 2795
    .local v4, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v4, :cond_10a

    #@9
    .line 2796
    iget-object v0, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b
    .line 2803
    .local v0, a:Landroid/app/Activity;
    if-eqz p3, :cond_fd

    #@d
    const/16 v2, 0x100

    #@f
    .line 2809
    .local v2, forwardBit:I
    :goto_f
    iget-boolean v7, v0, Landroid/app/Activity;->mStartedActivity:Z

    #@11
    if-nez v7, :cond_100

    #@13
    const/4 v5, 0x1

    #@14
    .line 2810
    .local v5, willBeVisible:Z
    :goto_14
    if-nez v5, :cond_22

    #@16
    .line 2812
    :try_start_16
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v0}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    #@1d
    move-result-object v8

    #@1e
    invoke-interface {v7, v8}, Landroid/app/IActivityManager;->willActivityBeVisible(Landroid/os/IBinder;)Z
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_21} :catch_118

    #@21
    move-result v5

    #@22
    .line 2817
    :cond_22
    :goto_22
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@24
    if-nez v7, :cond_103

    #@26
    iget-boolean v7, v0, Landroid/app/Activity;->mFinished:Z

    #@28
    if-nez v7, :cond_103

    #@2a
    if-eqz v5, :cond_103

    #@2c
    .line 2818
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2e
    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@31
    move-result-object v7

    #@32
    iput-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@34
    .line 2819
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@36
    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@39
    move-result-object v1

    #@3a
    .line 2820
    .local v1, decor:Landroid/view/View;
    const/4 v7, 0x4

    #@3b
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    #@3e
    .line 2821
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    #@41
    move-result-object v6

    #@42
    .line 2822
    .local v6, wm:Landroid/view/ViewManager;
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@44
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@47
    move-result-object v3

    #@48
    .line 2823
    .local v3, l:Landroid/view/WindowManager$LayoutParams;
    iput-object v1, v0, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@4a
    .line 2824
    const/4 v7, 0x1

    #@4b
    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4d
    .line 2825
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@4f
    or-int/2addr v7, v2

    #@50
    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@52
    .line 2826
    iget-boolean v7, v0, Landroid/app/Activity;->mVisibleFromClient:Z

    #@54
    if-eqz v7, :cond_5c

    #@56
    .line 2827
    const/4 v7, 0x1

    #@57
    iput-boolean v7, v0, Landroid/app/Activity;->mWindowAdded:Z

    #@59
    .line 2828
    invoke-interface {v6, v1, v3}, Landroid/view/ViewManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@5c
    .line 2841
    .end local v1           #decor:Landroid/view/View;
    .end local v3           #l:Landroid/view/WindowManager$LayoutParams;
    .end local v6           #wm:Landroid/view/ViewManager;
    :cond_5c
    :goto_5c
    invoke-static {v4}, Landroid/app/ActivityThread;->cleanUpPendingRemoveWindows(Landroid/app/ActivityThread$ActivityClientRecord;)V

    #@5f
    .line 2845
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@61
    iget-boolean v7, v7, Landroid/app/Activity;->mFinished:Z

    #@63
    if-nez v7, :cond_d9

    #@65
    if-eqz v5, :cond_d9

    #@67
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@69
    iget-object v7, v7, Landroid/app/Activity;->mDecor:Landroid/view/View;

    #@6b
    if-eqz v7, :cond_d9

    #@6d
    iget-boolean v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->hideForNow:Z

    #@6f
    if-nez v7, :cond_d9

    #@71
    .line 2847
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@73
    if-eqz v7, :cond_9d

    #@75
    .line 2850
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@77
    iget-object v8, v4, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@79
    invoke-static {v7, v8}, Landroid/app/ActivityThread;->performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V

    #@7c
    .line 2852
    sget-boolean v7, Lcom/lge/config/ConfigBuildFlags;->CAPP_FONTS:Z

    #@7e
    if-eqz v7, :cond_8d

    #@80
    .line 2853
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@82
    iget-object v7, v7, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@84
    iget-object v8, v4, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@86
    invoke-virtual {v7, v8}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@89
    move-result v7

    #@8a
    invoke-virtual {p0, v7}, Landroid/app/ActivityThread;->updateFontConfigurationIfNeeded(I)V

    #@8d
    .line 2856
    :cond_8d
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@8f
    iget-object v7, v7, Landroid/app/Activity;->mCurrentConfig:Landroid/content/res/Configuration;

    #@91
    iget-object v8, v4, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@93
    invoke-virtual {v7, v8}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    #@96
    move-result v7

    #@97
    invoke-virtual {p0, v7}, Landroid/app/ActivityThread;->freeTextLayoutCachesIfNeeded(I)V

    #@9a
    .line 2857
    const/4 v7, 0x0

    #@9b
    iput-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->newConfig:Landroid/content/res/Configuration;

    #@9d
    .line 2861
    :cond_9d
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@9f
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@a2
    move-result-object v3

    #@a3
    .line 2862
    .restart local v3       #l:Landroid/view/WindowManager$LayoutParams;
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@a5
    and-int/lit16 v7, v7, 0x100

    #@a7
    if-eq v7, v2, :cond_c3

    #@a9
    .line 2865
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@ab
    and-int/lit16 v7, v7, -0x101

    #@ad
    or-int/2addr v7, v2

    #@ae
    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@b0
    .line 2868
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@b2
    iget-boolean v7, v7, Landroid/app/Activity;->mVisibleFromClient:Z

    #@b4
    if-eqz v7, :cond_c3

    #@b6
    .line 2869
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    #@b9
    move-result-object v6

    #@ba
    .line 2870
    .restart local v6       #wm:Landroid/view/ViewManager;
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->window:Landroid/view/Window;

    #@bc
    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@bf
    move-result-object v1

    #@c0
    .line 2871
    .restart local v1       #decor:Landroid/view/View;
    invoke-interface {v6, v1, v3}, Landroid/view/ViewManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@c3
    .line 2874
    .end local v1           #decor:Landroid/view/View;
    .end local v6           #wm:Landroid/view/ViewManager;
    :cond_c3
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@c5
    const/4 v8, 0x1

    #@c6
    iput-boolean v8, v7, Landroid/app/Activity;->mVisibleFromServer:Z

    #@c8
    .line 2875
    iget v7, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@ca
    add-int/lit8 v7, v7, 0x1

    #@cc
    iput v7, p0, Landroid/app/ActivityThread;->mNumVisibleActivities:I

    #@ce
    .line 2876
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@d0
    iget-boolean v7, v7, Landroid/app/Activity;->mVisibleFromClient:Z

    #@d2
    if-eqz v7, :cond_d9

    #@d4
    .line 2877
    iget-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@d6
    invoke-virtual {v7}, Landroid/app/Activity;->makeVisible()V

    #@d9
    .line 2881
    .end local v3           #l:Landroid/view/WindowManager$LayoutParams;
    :cond_d9
    iget-boolean v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@db
    if-nez v7, :cond_f0

    #@dd
    .line 2882
    iget-object v7, p0, Landroid/app/ActivityThread;->mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

    #@df
    iput-object v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->nextIdle:Landroid/app/ActivityThread$ActivityClientRecord;

    #@e1
    .line 2883
    iput-object v4, p0, Landroid/app/ActivityThread;->mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

    #@e3
    .line 2886
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    #@e6
    move-result-object v7

    #@e7
    new-instance v8, Landroid/app/ActivityThread$Idler;

    #@e9
    const/4 v9, 0x0

    #@ea
    invoke-direct {v8, p0, v9}, Landroid/app/ActivityThread$Idler;-><init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V

    #@ed
    invoke-virtual {v7, v8}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@f0
    .line 2888
    :cond_f0
    const/4 v7, 0x0

    #@f1
    iput-boolean v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@f3
    .line 2891
    if-eqz p4, :cond_fc

    #@f5
    .line 2893
    :try_start_f5
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f8
    move-result-object v7

    #@f9
    invoke-interface {v7, p1}, Landroid/app/IActivityManager;->activityResumed(Landroid/os/IBinder;)V
    :try_end_fc
    .catch Landroid/os/RemoteException; {:try_start_f5 .. :try_end_fc} :catch_116

    #@fc
    .line 2907
    .end local v0           #a:Landroid/app/Activity;
    .end local v2           #forwardBit:I
    .end local v5           #willBeVisible:Z
    :cond_fc
    :goto_fc
    return-void

    #@fd
    .line 2803
    .restart local v0       #a:Landroid/app/Activity;
    :cond_fd
    const/4 v2, 0x0

    #@fe
    goto/16 :goto_f

    #@100
    .line 2809
    .restart local v2       #forwardBit:I
    :cond_100
    const/4 v5, 0x0

    #@101
    goto/16 :goto_14

    #@103
    .line 2834
    .restart local v5       #willBeVisible:Z
    :cond_103
    if-nez v5, :cond_5c

    #@105
    .line 2837
    const/4 v7, 0x1

    #@106
    iput-boolean v7, v4, Landroid/app/ActivityThread$ActivityClientRecord;->hideForNow:Z

    #@108
    goto/16 :goto_5c

    #@10a
    .line 2902
    .end local v0           #a:Landroid/app/Activity;
    .end local v2           #forwardBit:I
    .end local v5           #willBeVisible:Z
    :cond_10a
    :try_start_10a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@10d
    move-result-object v7

    #@10e
    const/4 v8, 0x0

    #@10f
    const/4 v9, 0x0

    #@110
    invoke-interface {v7, p1, v8, v9}, Landroid/app/IActivityManager;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Z
    :try_end_113
    .catch Landroid/os/RemoteException; {:try_start_10a .. :try_end_113} :catch_114

    #@113
    goto :goto_fc

    #@114
    .line 2904
    :catch_114
    move-exception v7

    #@115
    goto :goto_fc

    #@116
    .line 2894
    .restart local v0       #a:Landroid/app/Activity;
    .restart local v2       #forwardBit:I
    .restart local v5       #willBeVisible:Z
    :catch_116
    move-exception v7

    #@117
    goto :goto_fc

    #@118
    .line 2814
    :catch_118
    move-exception v7

    #@119
    goto/16 :goto_22
.end method

.method final handleTrimMemory(I)V
    .registers 8
    .parameter "level"

    #@0
    .prologue
    .line 4156
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@3
    move-result-object v3

    #@4
    .line 4157
    .local v3, windowManager:Landroid/view/WindowManagerGlobal;
    invoke-virtual {v3, p1}, Landroid/view/WindowManagerGlobal;->startTrimMemory(I)V

    #@7
    .line 4159
    const/4 v4, 0x1

    #@8
    const/4 v5, 0x0

    #@9
    invoke-virtual {p0, v4, v5}, Landroid/app/ActivityThread;->collectComponentCallbacks(ZLandroid/content/res/Configuration;)Ljava/util/ArrayList;

    #@c
    move-result-object v1

    #@d
    .line 4161
    .local v1, callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ComponentCallbacks2;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v0

    #@11
    .line 4162
    .local v0, N:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_20

    #@14
    .line 4163
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@17
    move-result-object v4

    #@18
    check-cast v4, Landroid/content/ComponentCallbacks2;

    #@1a
    invoke-interface {v4, p1}, Landroid/content/ComponentCallbacks2;->onTrimMemory(I)V

    #@1d
    .line 4162
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_12

    #@20
    .line 4166
    :cond_20
    invoke-virtual {v3}, Landroid/view/WindowManagerGlobal;->endTrimMemory()V

    #@23
    .line 4167
    return-void
.end method

.method final handleUnstableProviderDied(Landroid/os/IBinder;Z)V
    .registers 5
    .parameter "provider"
    .parameter "fromClient"

    #@0
    .prologue
    .line 4827
    iget-object v1, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 4828
    :try_start_3
    invoke-virtual {p0, p1, p2}, Landroid/app/ActivityThread;->handleUnstableProviderDiedLocked(Landroid/os/IBinder;Z)V

    #@6
    .line 4829
    monitor-exit v1

    #@7
    .line 4830
    return-void

    #@8
    .line 4829
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method final handleUnstableProviderDiedLocked(Landroid/os/IBinder;Z)V
    .registers 12
    .parameter "provider"
    .parameter "fromClient"

    #@0
    .prologue
    .line 4833
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v5

    #@6
    check-cast v5, Landroid/app/ActivityThread$ProviderRefCount;

    #@8
    .line 4834
    .local v5, prc:Landroid/app/ActivityThread$ProviderRefCount;
    if-eqz v5, :cond_62

    #@a
    .line 4837
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@c
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 4838
    iget-object v6, v5, Landroid/app/ActivityThread$ProviderRefCount;->client:Landroid/app/ActivityThread$ProviderClientRecord;

    #@11
    if-eqz v6, :cond_55

    #@13
    iget-object v6, v5, Landroid/app/ActivityThread$ProviderRefCount;->client:Landroid/app/ActivityThread$ProviderClientRecord;

    #@15
    iget-object v6, v6, Landroid/app/ActivityThread$ProviderClientRecord;->mNames:[Ljava/lang/String;

    #@17
    if-eqz v6, :cond_55

    #@19
    .line 4839
    iget-object v6, v5, Landroid/app/ActivityThread$ProviderRefCount;->client:Landroid/app/ActivityThread$ProviderClientRecord;

    #@1b
    iget-object v0, v6, Landroid/app/ActivityThread$ProviderClientRecord;->mNames:[Ljava/lang/String;

    #@1d
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@1e
    .local v2, len$:I
    const/4 v1, 0x0

    #@1f
    .local v1, i$:I
    :goto_1f
    if-ge v1, v2, :cond_55

    #@21
    aget-object v3, v0, v1

    #@23
    .line 4840
    .local v3, name:Ljava/lang/String;
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@25
    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Landroid/app/ActivityThread$ProviderClientRecord;

    #@2b
    .line 4841
    .local v4, pr:Landroid/app/ActivityThread$ProviderClientRecord;
    if-eqz v4, :cond_52

    #@2d
    iget-object v6, v4, Landroid/app/ActivityThread$ProviderClientRecord;->mProvider:Landroid/content/IContentProvider;

    #@2f
    invoke-interface {v6}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@32
    move-result-object v6

    #@33
    if-ne v6, p1, :cond_52

    #@35
    .line 4842
    const-string v6, "ActivityThread"

    #@37
    new-instance v7, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v8, "Removing dead content provider: "

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 4843
    iget-object v6, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@4f
    invoke-virtual {v6, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    .line 4839
    :cond_52
    add-int/lit8 v1, v1, 0x1

    #@54
    goto :goto_1f

    #@55
    .line 4847
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #pr:Landroid/app/ActivityThread$ProviderClientRecord;
    :cond_55
    if-eqz p2, :cond_62

    #@57
    .line 4854
    :try_start_57
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5a
    move-result-object v6

    #@5b
    iget-object v7, v5, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@5d
    iget-object v7, v7, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@5f
    invoke-interface {v6, v7}, Landroid/app/IActivityManager;->unstableProviderDied(Landroid/os/IBinder;)V
    :try_end_62
    .catch Landroid/os/RemoteException; {:try_start_57 .. :try_end_62} :catch_63

    #@62
    .line 4861
    :cond_62
    :goto_62
    return-void

    #@63
    .line 4856
    :catch_63
    move-exception v6

    #@64
    goto :goto_62
.end method

.method public installSystemApplicationInfo(Landroid/content/pm/ApplicationInfo;)V
    .registers 8
    .parameter "info"

    #@0
    .prologue
    .line 1944
    monitor-enter p0

    #@1
    .line 1945
    :try_start_1
    invoke-virtual {p0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    #@4
    move-result-object v3

    #@5
    .line 1946
    .local v3, context:Landroid/app/ContextImpl;
    new-instance v0, Landroid/app/LoadedApk;

    #@7
    const-string v2, "android"

    #@9
    sget-object v5, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@b
    move-object v1, p0

    #@c
    move-object v4, p1

    #@d
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk;-><init>(Landroid/app/ActivityThread;Ljava/lang/String;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V

    #@10
    const/4 v1, 0x0

    #@11
    invoke-virtual {v3, v0, v1, p0}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@14
    .line 1950
    new-instance v0, Landroid/app/ActivityThread$Profiler;

    #@16
    invoke-direct {v0}, Landroid/app/ActivityThread$Profiler;-><init>()V

    #@19
    iput-object v0, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@1b
    .line 1951
    monitor-exit p0

    #@1c
    .line 1952
    return-void

    #@1d
    .line 1951
    .end local v3           #context:Landroid/app/ContextImpl;
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public final installSystemProviders(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ProviderInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 5096
    .local p1, providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    if-eqz p1, :cond_7

    #@2
    .line 5097
    iget-object v0, p0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@4
    invoke-direct {p0, v0, p1}, Landroid/app/ActivityThread;->installContentProviders(Landroid/content/Context;Ljava/util/List;)V

    #@7
    .line 5099
    :cond_7
    return-void
.end method

.method public isProfiling()Z
    .registers 2

    #@0
    .prologue
    .line 1903
    iget-object v0, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@6
    iget-object v0, v0, Landroid/app/ActivityThread$Profiler;->profileFile:Ljava/lang/String;

    #@8
    if-eqz v0, :cond_12

    #@a
    iget-object v0, p0, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@c
    iget-object v0, v0, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@e
    if-nez v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public final peekPackageInfo(Ljava/lang/String;Z)Landroid/app/LoadedApk;
    .registers 6
    .parameter "packageName"
    .parameter "includeCode"

    #@0
    .prologue
    .line 1841
    iget-object v2, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 1843
    if-eqz p2, :cond_17

    #@5
    .line 1844
    :try_start_5
    iget-object v1, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@7
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Ljava/lang/ref/WeakReference;

    #@d
    .line 1848
    .local v0, ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :goto_d
    if-eqz v0, :cond_20

    #@f
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/app/LoadedApk;

    #@15
    :goto_15
    monitor-exit v2

    #@16
    return-object v1

    #@17
    .line 1846
    .end local v0           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :cond_17
    iget-object v1, p0, Landroid/app/ActivityThread;->mResourcePackages:Ljava/util/HashMap;

    #@19
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Ljava/lang/ref/WeakReference;

    #@1f
    .restart local v0       #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    goto :goto_d

    #@20
    .line 1848
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_15

    #@22
    .line 1849
    .end local v0           #ref:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/app/LoadedApk;>;"
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_5 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method

.method public final performDestroyActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;
    .registers 4
    .parameter "token"
    .parameter "finishing"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3403
    invoke-direct {p0, p1, p2, v0, v0}, Landroid/app/ActivityThread;->performDestroyActivity(Landroid/os/IBinder;ZIZ)Landroid/app/ActivityThread$ActivityClientRecord;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final performNewIntents(Landroid/os/IBinder;Ljava/util/List;)V
    .registers 8
    .parameter "token"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, intents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2327
    iget-object v4, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@4
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@a
    .line 2328
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v0, :cond_2c

    #@c
    .line 2329
    iget-boolean v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@e
    if-nez v4, :cond_2d

    #@10
    move v1, v2

    #@11
    .line 2330
    .local v1, resumed:Z
    :goto_11
    if-eqz v1, :cond_1e

    #@13
    .line 2331
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@15
    iput-boolean v2, v4, Landroid/app/Activity;->mTemporaryPause:Z

    #@17
    .line 2332
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@19
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1b
    invoke-virtual {v2, v4}, Landroid/app/Instrumentation;->callActivityOnPause(Landroid/app/Activity;)V

    #@1e
    .line 2334
    :cond_1e
    invoke-direct {p0, v0, p2}, Landroid/app/ActivityThread;->deliverNewIntents(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V

    #@21
    .line 2335
    if-eqz v1, :cond_2c

    #@23
    .line 2336
    iget-object v2, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@25
    invoke-virtual {v2}, Landroid/app/Activity;->performResume()V

    #@28
    .line 2337
    iget-object v2, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2a
    iput-boolean v3, v2, Landroid/app/Activity;->mTemporaryPause:Z

    #@2c
    .line 2340
    .end local v1           #resumed:Z
    :cond_2c
    return-void

    #@2d
    :cond_2d
    move v1, v3

    #@2e
    .line 2329
    goto :goto_11
.end method

.method final performPauseActivity(Landroid/app/ActivityThread$ActivityClientRecord;ZZ)Landroid/os/Bundle;
    .registers 15
    .parameter "r"
    .parameter "finished"
    .parameter "saveState"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3003
    iget-boolean v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@4
    if-eqz v6, :cond_39

    #@6
    .line 3004
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@8
    iget-boolean v6, v6, Landroid/app/Activity;->mFinished:Z

    #@a
    if-eqz v6, :cond_e

    #@c
    .line 3008
    const/4 v4, 0x0

    #@d
    .line 3061
    :cond_d
    return-object v4

    #@e
    .line 3010
    :cond_e
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    new-instance v6, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v7, "Performing pause of activity that is not resumed: "

    #@17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@1d
    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@20
    move-result-object v7

    #@21
    invoke-virtual {v7}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    invoke-direct {v0, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@30
    .line 3013
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v6, "ActivityThread"

    #@32
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    invoke-static {v6, v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@39
    .line 3015
    .end local v0           #e:Ljava/lang/RuntimeException;
    :cond_39
    const/4 v4, 0x0

    #@3a
    .line 3016
    .local v4, state:Landroid/os/Bundle;
    if-eqz p2, :cond_40

    #@3c
    .line 3017
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@3e
    iput-boolean v10, v6, Landroid/app/Activity;->mFinished:Z

    #@40
    .line 3021
    :cond_40
    :try_start_40
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@42
    iget-boolean v6, v6, Landroid/app/Activity;->mFinished:Z

    #@44
    if-nez v6, :cond_5b

    #@46
    if-eqz p3, :cond_5b

    #@48
    .line 3022
    new-instance v5, Landroid/os/Bundle;

    #@4a
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V
    :try_end_4d
    .catch Landroid/app/SuperNotCalledException; {:try_start_40 .. :try_end_4d} :catch_b6
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_4d} :catch_b8

    #@4d
    .line 3023
    .end local v4           #state:Landroid/os/Bundle;
    .local v5, state:Landroid/os/Bundle;
    const/4 v6, 0x0

    #@4e
    :try_start_4e
    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@51
    .line 3024
    iget-object v6, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@53
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@55
    invoke-virtual {v6, v7, v5}, Landroid/app/Instrumentation;->callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    #@58
    .line 3025
    iput-object v5, p1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;
    :try_end_5a
    .catch Landroid/app/SuperNotCalledException; {:try_start_4e .. :try_end_5a} :catch_121
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_5a} :catch_11e

    #@5a
    move-object v4, v5

    #@5b
    .line 3028
    .end local v5           #state:Landroid/os/Bundle;
    .restart local v4       #state:Landroid/os/Bundle;
    :cond_5b
    :try_start_5b
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@5d
    const/4 v7, 0x0

    #@5e
    iput-boolean v7, v6, Landroid/app/Activity;->mCalled:Z

    #@60
    .line 3029
    iget-object v6, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@62
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@64
    invoke-virtual {v6, v7}, Landroid/app/Instrumentation;->callActivityOnPause(Landroid/app/Activity;)V

    #@67
    .line 3030
    const/16 v6, 0x7545

    #@69
    const/4 v7, 0x2

    #@6a
    new-array v7, v7, [Ljava/lang/Object;

    #@6c
    const/4 v8, 0x0

    #@6d
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@70
    move-result v9

    #@71
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@74
    move-result-object v9

    #@75
    aput-object v9, v7, v8

    #@77
    const/4 v8, 0x1

    #@78
    iget-object v9, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@7a
    invoke-virtual {v9}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@7d
    move-result-object v9

    #@7e
    invoke-virtual {v9}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@81
    move-result-object v9

    #@82
    aput-object v9, v7, v8

    #@84
    invoke-static {v6, v7}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@87
    .line 3032
    iget-object v6, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@89
    iget-boolean v6, v6, Landroid/app/Activity;->mCalled:Z

    #@8b
    if-nez v6, :cond_f4

    #@8d
    .line 3033
    new-instance v6, Landroid/app/SuperNotCalledException;

    #@8f
    new-instance v7, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v8, "Activity "

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@9c
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@9f
    move-result-object v8

    #@a0
    invoke-virtual {v8}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@a3
    move-result-object v8

    #@a4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v7

    #@a8
    const-string v8, " did not call through to super.onPause()"

    #@aa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v7

    #@b2
    invoke-direct {v6, v7}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@b5
    throw v6
    :try_end_b6
    .catch Landroid/app/SuperNotCalledException; {:try_start_5b .. :try_end_b6} :catch_b6
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_b6} :catch_b8

    #@b6
    .line 3038
    :catch_b6
    move-exception v0

    #@b7
    .line 3039
    .local v0, e:Landroid/app/SuperNotCalledException;
    :goto_b7
    throw v0

    #@b8
    .line 3041
    .end local v0           #e:Landroid/app/SuperNotCalledException;
    :catch_b8
    move-exception v0

    #@b9
    .line 3042
    .local v0, e:Ljava/lang/Exception;
    :goto_b9
    iget-object v6, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@bb
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@bd
    invoke-virtual {v6, v7, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@c0
    move-result v6

    #@c1
    if-nez v6, :cond_f4

    #@c3
    .line 3043
    new-instance v6, Ljava/lang/RuntimeException;

    #@c5
    new-instance v7, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v8, "Unable to pause activity "

    #@cc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v7

    #@d0
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@d2
    invoke-virtual {v8}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@d5
    move-result-object v8

    #@d6
    invoke-virtual {v8}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@d9
    move-result-object v8

    #@da
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v7

    #@de
    const-string v8, ": "

    #@e0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v7

    #@e4
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@e7
    move-result-object v8

    #@e8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v7

    #@ec
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ef
    move-result-object v7

    #@f0
    invoke-direct {v6, v7, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@f3
    throw v6

    #@f4
    .line 3049
    .end local v0           #e:Ljava/lang/Exception;
    :cond_f4
    iput-boolean v10, p1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@f6
    .line 3053
    iget-object v7, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@f8
    monitor-enter v7

    #@f9
    .line 3054
    :try_start_f9
    iget-object v6, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@fb
    iget-object v8, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@fd
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@100
    move-result-object v2

    #@101
    check-cast v2, Ljava/util/ArrayList;

    #@103
    .line 3055
    .local v2, listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    monitor-exit v7
    :try_end_104
    .catchall {:try_start_f9 .. :try_end_104} :catchall_11b

    #@104
    .line 3056
    if-eqz v2, :cond_10a

    #@106
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@109
    move-result v3

    #@10a
    .line 3057
    .local v3, size:I
    :cond_10a
    const/4 v1, 0x0

    #@10b
    .local v1, i:I
    :goto_10b
    if-ge v1, v3, :cond_d

    #@10d
    .line 3058
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@110
    move-result-object v6

    #@111
    check-cast v6, Landroid/app/OnActivityPausedListener;

    #@113
    iget-object v7, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@115
    invoke-interface {v6, v7}, Landroid/app/OnActivityPausedListener;->onPaused(Landroid/app/Activity;)V

    #@118
    .line 3057
    add-int/lit8 v1, v1, 0x1

    #@11a
    goto :goto_10b

    #@11b
    .line 3055
    .end local v1           #i:I
    .end local v2           #listeners:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    .end local v3           #size:I
    :catchall_11b
    move-exception v6

    #@11c
    :try_start_11c
    monitor-exit v7
    :try_end_11d
    .catchall {:try_start_11c .. :try_end_11d} :catchall_11b

    #@11d
    throw v6

    #@11e
    .line 3041
    .end local v4           #state:Landroid/os/Bundle;
    .restart local v5       #state:Landroid/os/Bundle;
    :catch_11e
    move-exception v0

    #@11f
    move-object v4, v5

    #@120
    .end local v5           #state:Landroid/os/Bundle;
    .restart local v4       #state:Landroid/os/Bundle;
    goto :goto_b9

    #@121
    .line 3038
    .end local v4           #state:Landroid/os/Bundle;
    .restart local v5       #state:Landroid/os/Bundle;
    :catch_121
    move-exception v0

    #@122
    move-object v4, v5

    #@123
    .end local v5           #state:Landroid/os/Bundle;
    .restart local v4       #state:Landroid/os/Bundle;
    goto :goto_b7
.end method

.method final performPauseActivity(Landroid/os/IBinder;ZZ)Landroid/os/Bundle;
    .registers 6
    .parameter "token"
    .parameter "finished"
    .parameter "saveState"

    #@0
    .prologue
    .line 2997
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 2998
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v0, :cond_f

    #@a
    invoke-virtual {p0, v0, p2, p3}, Landroid/app/ActivityThread;->performPauseActivity(Landroid/app/ActivityThread$ActivityClientRecord;ZZ)Landroid/os/Bundle;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    const/4 v1, 0x0

    #@10
    goto :goto_e
.end method

.method final performRestartActivity(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 3244
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 3245
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-boolean v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@a
    if-eqz v1, :cond_14

    #@c
    .line 3246
    iget-object v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@e
    invoke-virtual {v1}, Landroid/app/Activity;->performRestart()V

    #@11
    .line 3247
    const/4 v1, 0x0

    #@12
    iput-boolean v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@14
    .line 3249
    :cond_14
    return-void
.end method

.method public final performResumeActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;
    .registers 9
    .parameter "token"
    .parameter "clearHide"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2736
    iget-object v2, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@3
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/app/ActivityThread$ActivityClientRecord;

    #@9
    .line 2739
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v1, :cond_66

    #@b
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@d
    iget-boolean v2, v2, Landroid/app/Activity;->mFinished:Z

    #@f
    if-nez v2, :cond_66

    #@11
    .line 2740
    if-eqz p2, :cond_19

    #@13
    .line 2741
    iput-boolean v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->hideForNow:Z

    #@15
    .line 2742
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@17
    iput-boolean v3, v2, Landroid/app/Activity;->mStartedActivity:Z

    #@19
    .line 2745
    :cond_19
    :try_start_19
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@1b
    iget-object v2, v2, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@1d
    invoke-virtual {v2}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@20
    .line 2746
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@22
    if-eqz v2, :cond_2c

    #@24
    .line 2747
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@26
    invoke-direct {p0, v1, v2}, Landroid/app/ActivityThread;->deliverNewIntents(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V

    #@29
    .line 2748
    const/4 v2, 0x0

    #@2a
    iput-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@2c
    .line 2750
    :cond_2c
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@2e
    if-eqz v2, :cond_38

    #@30
    .line 2751
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@32
    invoke-direct {p0, v1, v2}, Landroid/app/ActivityThread;->deliverResults(Landroid/app/ActivityThread$ActivityClientRecord;Ljava/util/List;)V

    #@35
    .line 2752
    const/4 v2, 0x0

    #@36
    iput-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@38
    .line 2754
    :cond_38
    iget-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@3a
    invoke-virtual {v2}, Landroid/app/Activity;->performResume()V

    #@3d
    .line 2756
    const/16 v2, 0x7546

    #@3f
    const/4 v3, 0x2

    #@40
    new-array v3, v3, [Ljava/lang/Object;

    #@42
    const/4 v4, 0x0

    #@43
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@46
    move-result v5

    #@47
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v5

    #@4b
    aput-object v5, v3, v4

    #@4d
    const/4 v4, 0x1

    #@4e
    iget-object v5, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@50
    invoke-virtual {v5}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@57
    move-result-object v5

    #@58
    aput-object v5, v3, v4

    #@5a
    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@5d
    .line 2759
    const/4 v2, 0x0

    #@5e
    iput-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@60
    .line 2760
    const/4 v2, 0x0

    #@61
    iput-boolean v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->stopped:Z

    #@63
    .line 2761
    const/4 v2, 0x0

    #@64
    iput-object v2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;
    :try_end_66
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_66} :catch_67

    #@66
    .line 2771
    :cond_66
    return-object v1

    #@67
    .line 2762
    :catch_67
    move-exception v0

    #@68
    .line 2763
    .local v0, e:Ljava/lang/Exception;
    iget-object v2, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@6a
    iget-object v3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@6c
    invoke-virtual {v2, v3, v0}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@6f
    move-result v2

    #@70
    if-nez v2, :cond_66

    #@72
    .line 2764
    new-instance v2, Ljava/lang/RuntimeException;

    #@74
    new-instance v3, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v4, "Unable to resume activity "

    #@7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    iget-object v4, v1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@81
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    const-string v4, ": "

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v3

    #@9f
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a2
    throw v2
.end method

.method final performStopActivity(Landroid/os/IBinder;Z)V
    .registers 6
    .parameter "token"
    .parameter "saveState"

    #@0
    .prologue
    .line 3065
    iget-object v1, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@8
    .line 3066
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    const/4 v1, 0x0

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {p0, v0, v1, v2, p2}, Landroid/app/ActivityThread;->performStopActivityInner(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/app/ActivityThread$StopInfo;ZZ)V

    #@d
    .line 3067
    return-void
.end method

.method final performUserLeavingActivity(Landroid/app/ActivityThread$ActivityClientRecord;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 2992
    iget-object v0, p0, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    iget-object v1, p1, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@4
    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->callActivityOnUserLeaving(Landroid/app/Activity;)V

    #@7
    .line 2993
    return-void
.end method

.method public registerOnActivityPausedListener(Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V
    .registers 6
    .parameter "activity"
    .parameter "listener"

    #@0
    .prologue
    .line 1990
    iget-object v2, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 1991
    :try_start_3
    iget-object v1, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/util/ArrayList;

    #@b
    .line 1992
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    if-nez v0, :cond_17

    #@d
    .line 1993
    new-instance v0, Ljava/util/ArrayList;

    #@f
    .end local v0           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 1994
    .restart local v0       #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    iget-object v1, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@14
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 1996
    :cond_17
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 1997
    monitor-exit v2

    #@1b
    .line 1998
    return-void

    #@1c
    .line 1997
    .end local v0           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit v2
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method public final releaseProvider(Landroid/content/IContentProvider;Z)Z
    .registers 14
    .parameter "provider"
    .parameter "stable"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 4695
    if-nez p1, :cond_5

    #@4
    .line 4783
    :goto_4
    return v6

    #@5
    .line 4699
    :cond_5
    invoke-interface {p1}, Landroid/content/IContentProvider;->asBinder()Landroid/os/IBinder;

    #@8
    move-result-object v1

    #@9
    .line 4700
    .local v1, jBinder:Landroid/os/IBinder;
    iget-object v7, p0, Landroid/app/ActivityThread;->mProviderMap:Ljava/util/HashMap;

    #@b
    monitor-enter v7

    #@c
    .line 4701
    :try_start_c
    iget-object v8, p0, Landroid/app/ActivityThread;->mProviderRefCountMap:Ljava/util/HashMap;

    #@e
    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Landroid/app/ActivityThread$ProviderRefCount;

    #@14
    .line 4702
    .local v4, prc:Landroid/app/ActivityThread$ProviderRefCount;
    if-nez v4, :cond_1b

    #@16
    .line 4704
    monitor-exit v7

    #@17
    goto :goto_4

    #@18
    .line 4784
    .end local v4           #prc:Landroid/app/ActivityThread$ProviderRefCount;
    :catchall_18
    move-exception v5

    #@19
    monitor-exit v7
    :try_end_1a
    .catchall {:try_start_c .. :try_end_1a} :catchall_18

    #@1a
    throw v5

    #@1b
    .line 4707
    .restart local v4       #prc:Landroid/app/ActivityThread$ProviderRefCount;
    :cond_1b
    const/4 v2, 0x0

    #@1c
    .line 4708
    .local v2, lastRef:Z
    if-eqz p2, :cond_78

    #@1e
    .line 4709
    :try_start_1e
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@20
    if-nez v8, :cond_24

    #@22
    .line 4712
    monitor-exit v7

    #@23
    goto :goto_4

    #@24
    .line 4714
    :cond_24
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@26
    add-int/lit8 v8, v8, -0x1

    #@28
    iput v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@2a
    .line 4715
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I

    #@2c
    if-nez v8, :cond_42

    #@2e
    .line 4722
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I
    :try_end_30
    .catchall {:try_start_1e .. :try_end_30} :catchall_18

    #@30
    if-nez v8, :cond_5b

    #@32
    move v2, v5

    #@33
    .line 4728
    :goto_33
    :try_start_33
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@36
    move-result-object v8

    #@37
    iget-object v9, v4, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@39
    iget-object v9, v9, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@3b
    const/4 v10, -0x1

    #@3c
    if-eqz v2, :cond_3f

    #@3e
    move v6, v5

    #@3f
    :cond_3f
    invoke-interface {v8, v9, v10, v6}, Landroid/app/IActivityManager;->refContentProvider(Landroid/os/IBinder;II)Z
    :try_end_42
    .catchall {:try_start_33 .. :try_end_42} :catchall_18
    .catch Landroid/os/RemoteException; {:try_start_33 .. :try_end_42} :catch_dc
    .catch Ljava/lang/IllegalStateException; {:try_start_33 .. :try_end_42} :catch_5d

    #@42
    .line 4765
    :cond_42
    :goto_42
    if-eqz v2, :cond_58

    #@44
    .line 4766
    :try_start_44
    iget-boolean v6, v4, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@46
    if-nez v6, :cond_bc

    #@48
    .line 4776
    const/4 v6, 0x1

    #@49
    iput-boolean v6, v4, Landroid/app/ActivityThread$ProviderRefCount;->removePending:Z

    #@4b
    .line 4777
    iget-object v6, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@4d
    const/16 v8, 0x83

    #@4f
    invoke-virtual {v6, v8, v4}, Landroid/app/ActivityThread$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@52
    move-result-object v3

    #@53
    .line 4778
    .local v3, msg:Landroid/os/Message;
    iget-object v6, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@55
    invoke-virtual {v6, v3}, Landroid/app/ActivityThread$H;->sendMessage(Landroid/os/Message;)Z

    #@58
    .line 4783
    .end local v3           #msg:Landroid/os/Message;
    :cond_58
    :goto_58
    monitor-exit v7

    #@59
    move v6, v5

    #@5a
    goto :goto_4

    #@5b
    :cond_5b
    move v2, v6

    #@5c
    .line 4722
    goto :goto_33

    #@5d
    .line 4732
    :catch_5d
    move-exception v0

    #@5e
    .line 4733
    .local v0, e:Ljava/lang/IllegalStateException;
    const-string v6, "ActivityThread"

    #@60
    new-instance v8, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string/jumbo v9, "state problem : "

    #@68
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v8

    #@70
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v8

    #@74
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_42

    #@78
    .line 4737
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_78
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@7a
    if-nez v8, :cond_7e

    #@7c
    .line 4740
    monitor-exit v7

    #@7d
    goto :goto_4

    #@7e
    .line 4742
    :cond_7e
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@80
    add-int/lit8 v8, v8, -0x1

    #@82
    iput v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@84
    .line 4743
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->unstableCount:I

    #@86
    if-nez v8, :cond_42

    #@88
    .line 4747
    iget v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->stableCount:I
    :try_end_8a
    .catchall {:try_start_44 .. :try_end_8a} :catchall_18

    #@8a
    if-nez v8, :cond_9f

    #@8c
    move v2, v5

    #@8d
    .line 4748
    :goto_8d
    if-nez v2, :cond_42

    #@8f
    .line 4754
    :try_start_8f
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@92
    move-result-object v6

    #@93
    iget-object v8, v4, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@95
    iget-object v8, v8, Landroid/app/IActivityManager$ContentProviderHolder;->connection:Landroid/os/IBinder;

    #@97
    const/4 v9, 0x0

    #@98
    const/4 v10, -0x1

    #@99
    invoke-interface {v6, v8, v9, v10}, Landroid/app/IActivityManager;->refContentProvider(Landroid/os/IBinder;II)Z
    :try_end_9c
    .catchall {:try_start_8f .. :try_end_9c} :catchall_18
    .catch Landroid/os/RemoteException; {:try_start_8f .. :try_end_9c} :catch_9d
    .catch Ljava/lang/IllegalStateException; {:try_start_8f .. :try_end_9c} :catch_a1

    #@9c
    goto :goto_42

    #@9d
    .line 4756
    :catch_9d
    move-exception v6

    #@9e
    goto :goto_42

    #@9f
    :cond_9f
    move v2, v6

    #@a0
    .line 4747
    goto :goto_8d

    #@a1
    .line 4758
    :catch_a1
    move-exception v0

    #@a2
    .line 4759
    .restart local v0       #e:Ljava/lang/IllegalStateException;
    :try_start_a2
    const-string v6, "ActivityThread"

    #@a4
    new-instance v8, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string/jumbo v9, "state problem : "

    #@ac
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v8

    #@b8
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    goto :goto_42

    #@bc
    .line 4780
    .end local v0           #e:Ljava/lang/IllegalStateException;
    :cond_bc
    const-string v6, "ActivityThread"

    #@be
    new-instance v8, Ljava/lang/StringBuilder;

    #@c0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c3
    const-string v9, "Duplicate remove pending of provider "

    #@c5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v8

    #@c9
    iget-object v9, v4, Landroid/app/ActivityThread$ProviderRefCount;->holder:Landroid/app/IActivityManager$ContentProviderHolder;

    #@cb
    iget-object v9, v9, Landroid/app/IActivityManager$ContentProviderHolder;->info:Landroid/content/pm/ProviderInfo;

    #@cd
    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v8

    #@d7
    invoke-static {v6, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_da
    .catchall {:try_start_a2 .. :try_end_da} :catchall_18

    #@da
    goto/16 :goto_58

    #@dc
    .line 4730
    :catch_dc
    move-exception v6

    #@dd
    goto/16 :goto_42
.end method

.method public final requestRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;Z)V
    .registers 15
    .parameter "token"
    .parameter
    .parameter
    .parameter "configChanges"
    .parameter "notResumed"
    .parameter "config"
    .parameter "fromServer"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ResultInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;IZ",
            "Landroid/content/res/Configuration;",
            "Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 3562
    .local p2, pendingResults:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .local p3, pendingNewIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    const/4 v3, 0x0

    #@1
    .line 3564
    .local v3, target:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v6, p0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@3
    monitor-enter v6

    #@4
    .line 3565
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    :try_start_5
    iget-object v5, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v5

    #@b
    if-ge v1, v5, :cond_7f

    #@d
    .line 3566
    iget-object v5, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/app/ActivityThread$ActivityClientRecord;

    #@15
    .line 3567
    .local v2, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@17
    if-ne v5, p1, :cond_77

    #@19
    .line 3568
    move-object v3, v2

    #@1a
    .line 3569
    if-eqz p2, :cond_25

    #@1c
    .line 3570
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@1e
    if-eqz v5, :cond_6d

    #@20
    .line 3571
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@22
    invoke-interface {v5, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    #@25
    .line 3576
    :cond_25
    :goto_25
    if-eqz p3, :cond_7f

    #@27
    .line 3577
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@29
    if-eqz v5, :cond_73

    #@2b
    .line 3578
    iget-object v5, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@2d
    invoke-interface {v5, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_30
    .catchall {:try_start_5 .. :try_end_30} :catchall_70

    #@30
    move-object v4, v3

    #@31
    .line 3587
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    .end local v3           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .local v4, target:Landroid/app/ActivityThread$ActivityClientRecord;
    :goto_31
    if-nez v4, :cond_7d

    #@33
    .line 3588
    :try_start_33
    new-instance v3, Landroid/app/ActivityThread$ActivityClientRecord;

    #@35
    invoke-direct {v3}, Landroid/app/ActivityThread$ActivityClientRecord;-><init>()V
    :try_end_38
    .catchall {:try_start_33 .. :try_end_38} :catchall_7a

    #@38
    .line 3589
    .end local v4           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v3       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    :try_start_38
    iput-object p1, v3, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@3a
    .line 3590
    iput-object p2, v3, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@3c
    .line 3591
    iput-object p3, v3, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@3e
    .line 3592
    if-nez p7, :cond_51

    #@40
    .line 3593
    iget-object v5, p0, Landroid/app/ActivityThread;->mActivities:Ljava/util/HashMap;

    #@42
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@45
    move-result-object v0

    #@46
    check-cast v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@48
    .line 3594
    .local v0, existing:Landroid/app/ActivityThread$ActivityClientRecord;
    if-eqz v0, :cond_4e

    #@4a
    .line 3595
    iget-boolean v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->paused:Z

    #@4c
    iput-boolean v5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@4e
    .line 3597
    :cond_4e
    const/4 v5, 0x1

    #@4f
    iput-boolean v5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@51
    .line 3599
    .end local v0           #existing:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_51
    iget-object v5, p0, Landroid/app/ActivityThread;->mRelaunchingActivities:Ljava/util/ArrayList;

    #@53
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@56
    .line 3600
    const/16 v5, 0x7e

    #@58
    invoke-direct {p0, v5, v3}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;)V

    #@5b
    .line 3603
    :goto_5b
    if-eqz p7, :cond_62

    #@5d
    .line 3604
    iput-boolean p5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@5f
    .line 3605
    const/4 v5, 0x0

    #@60
    iput-boolean v5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->onlyLocalRequest:Z

    #@62
    .line 3607
    :cond_62
    if-eqz p6, :cond_66

    #@64
    .line 3608
    iput-object p6, v3, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@66
    .line 3610
    :cond_66
    iget v5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->pendingConfigChanges:I

    #@68
    or-int/2addr v5, p4

    #@69
    iput v5, v3, Landroid/app/ActivityThread$ActivityClientRecord;->pendingConfigChanges:I

    #@6b
    .line 3611
    monitor-exit v6

    #@6c
    .line 3612
    return-void

    #@6d
    .line 3573
    .restart local v2       #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_6d
    iput-object p2, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@6f
    goto :goto_25

    #@70
    .line 3611
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :catchall_70
    move-exception v5

    #@71
    :goto_71
    monitor-exit v6
    :try_end_72
    .catchall {:try_start_38 .. :try_end_72} :catchall_70

    #@72
    throw v5

    #@73
    .line 3580
    .restart local v2       #r:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_73
    :try_start_73
    iput-object p3, v2, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;
    :try_end_75
    .catchall {:try_start_73 .. :try_end_75} :catchall_70

    #@75
    move-object v4, v3

    #@76
    .end local v3           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v4       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    goto :goto_31

    #@77
    .line 3565
    .end local v4           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v3       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_77
    add-int/lit8 v1, v1, 0x1

    #@79
    goto :goto_5

    #@7a
    .line 3611
    .end local v2           #r:Landroid/app/ActivityThread$ActivityClientRecord;
    .end local v3           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v4       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    :catchall_7a
    move-exception v5

    #@7b
    move-object v3, v4

    #@7c
    .end local v4           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v3       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    goto :goto_71

    #@7d
    .end local v3           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v4       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_7d
    move-object v3, v4

    #@7e
    .end local v4           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v3       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    goto :goto_5b

    #@7f
    :cond_7f
    move-object v4, v3

    #@80
    .end local v3           #target:Landroid/app/ActivityThread$ActivityClientRecord;
    .restart local v4       #target:Landroid/app/ActivityThread$ActivityClientRecord;
    goto :goto_31
.end method

.method public final resolveActivityInfo(Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 2011
    iget-object v1, p0, Landroid/app/ActivityThread;->mInitialApplication:Landroid/app/Application;

    #@2
    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v1

    #@6
    const/16 v2, 0x400

    #@8
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@b
    move-result-object v0

    #@c
    .line 2013
    .local v0, aInfo:Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_12

    #@e
    .line 2015
    const/4 v1, -0x2

    #@f
    invoke-static {v1, p1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V

    #@12
    .line 2018
    :cond_12
    return-object v0
.end method

.method final scheduleContextCleanup(Landroid/app/ContextImpl;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 2088
    new-instance v0, Landroid/app/ActivityThread$ContextCleanupInfo;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$ContextCleanupInfo;-><init>()V

    #@5
    .line 2089
    .local v0, cci:Landroid/app/ActivityThread$ContextCleanupInfo;
    iput-object p1, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->context:Landroid/app/ContextImpl;

    #@7
    .line 2090
    iput-object p2, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->who:Ljava/lang/String;

    #@9
    .line 2091
    iput-object p3, v0, Landroid/app/ActivityThread$ContextCleanupInfo;->what:Ljava/lang/String;

    #@b
    .line 2092
    const/16 v1, 0x77

    #@d
    invoke-direct {p0, v1, v0}, Landroid/app/ActivityThread;->queueOrSendMessage(ILjava/lang/Object;)V

    #@10
    .line 2093
    return-void
.end method

.method scheduleGcIdler()V
    .registers 3

    #@0
    .prologue
    .line 1962
    iget-boolean v0, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@2
    if-nez v0, :cond_10

    #@4
    .line 1963
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@7
    .line 1964
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/app/ActivityThread;->mGcIdler:Landroid/app/ActivityThread$GcIdler;

    #@d
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@10
    .line 1966
    :cond_10
    iget-object v0, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@12
    const/16 v1, 0x78

    #@14
    invoke-virtual {v0, v1}, Landroid/app/ActivityThread$H;->removeMessages(I)V

    #@17
    .line 1967
    return-void
.end method

.method public final sendActivityResult(Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/Intent;)V
    .registers 8
    .parameter "token"
    .parameter "id"
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 2057
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 2058
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ResultInfo;>;"
    new-instance v1, Landroid/app/ResultInfo;

    #@7
    invoke-direct {v1, p2, p3, p4, p5}, Landroid/app/ResultInfo;-><init>(Ljava/lang/String;IILandroid/content/Intent;)V

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 2059
    iget-object v1, p0, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@f
    invoke-virtual {v1, p1, v0}, Landroid/app/ActivityThread$ApplicationThread;->scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V

    #@12
    .line 2060
    return-void
.end method

.method public final startActivityNow(Landroid/app/Activity;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Landroid/os/Bundle;Landroid/app/Activity$NonConfigurationInstances;)Landroid/app/Activity;
    .registers 10
    .parameter "parent"
    .parameter "id"
    .parameter "intent"
    .parameter "activityInfo"
    .parameter "token"
    .parameter "state"
    .parameter "lastNonConfigurationInstances"

    #@0
    .prologue
    .line 2024
    new-instance v0, Landroid/app/ActivityThread$ActivityClientRecord;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$ActivityClientRecord;-><init>()V

    #@5
    .line 2025
    .local v0, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iput-object p5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@7
    .line 2026
    const/4 v1, 0x0

    #@8
    iput v1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->ident:I

    #@a
    .line 2027
    iput-object p3, v0, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@c
    .line 2028
    iput-object p6, v0, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@e
    .line 2029
    iput-object p1, v0, Landroid/app/ActivityThread$ActivityClientRecord;->parent:Landroid/app/Activity;

    #@10
    .line 2030
    iput-object p2, v0, Landroid/app/ActivityThread$ActivityClientRecord;->embeddedID:Ljava/lang/String;

    #@12
    .line 2031
    iput-object p4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@14
    .line 2032
    iput-object p7, v0, Landroid/app/ActivityThread$ActivityClientRecord;->lastNonConfigurationInstances:Landroid/app/Activity$NonConfigurationInstances;

    #@16
    .line 2045
    const/4 v1, 0x0

    #@17
    invoke-direct {p0, v0, v1}, Landroid/app/ActivityThread;->performLaunchActivity(Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/Intent;)Landroid/app/Activity;

    #@1a
    move-result-object v1

    #@1b
    return-object v1
.end method

.method public unregisterOnActivityPausedListener(Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V
    .registers 6
    .parameter "activity"
    .parameter "listener"

    #@0
    .prologue
    .line 2002
    iget-object v2, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 2003
    :try_start_3
    iget-object v1, p0, Landroid/app/ActivityThread;->mOnPauseListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/util/ArrayList;

    #@b
    .line 2004
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    if-eqz v0, :cond_10

    #@d
    .line 2005
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@10
    .line 2007
    :cond_10
    monitor-exit v2

    #@11
    .line 2008
    return-void

    #@12
    .line 2007
    .end local v0           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/OnActivityPausedListener;>;"
    :catchall_12
    move-exception v1

    #@13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method unscheduleGcIdler()V
    .registers 3

    #@0
    .prologue
    .line 1970
    iget-boolean v0, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 1971
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/app/ActivityThread;->mGcIdlerScheduled:Z

    #@7
    .line 1972
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/app/ActivityThread;->mGcIdler:Landroid/app/ActivityThread$GcIdler;

    #@d
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@10
    .line 1974
    :cond_10
    iget-object v0, p0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@12
    const/16 v1, 0x78

    #@14
    invoke-virtual {v0, v1}, Landroid/app/ActivityThread$H;->removeMessages(I)V

    #@17
    .line 1975
    return-void
.end method

.method final updateFontConfigurationIfNeeded(I)V
    .registers 3
    .parameter "configDiff"

    #@0
    .prologue
    .line 5175
    if-eqz p1, :cond_a

    #@2
    .line 5176
    const/high16 v0, 0x2000

    #@4
    and-int/2addr v0, p1

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 5177
    invoke-virtual {p0}, Landroid/app/ActivityThread;->updateFontConfigurationLocked()V

    #@a
    .line 5181
    :cond_a
    return-void
.end method

.method final updateFontConfigurationLocked()V
    .registers 1

    #@0
    .prologue
    .line 5184
    invoke-static {}, Landroid/graphics/Typeface;->updateFontConfiguration()V

    #@3
    .line 5185
    return-void
.end method
