.class final Landroid/app/SharedPreferencesImpl;
.super Ljava/lang/Object;
.source "SharedPreferencesImpl.java"

# interfaces
.implements Landroid/content/SharedPreferences;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/SharedPreferencesImpl$EditorImpl;,
        Landroid/app/SharedPreferencesImpl$MemoryCommitResult;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "SharedPreferencesImpl"

.field private static final mContent:Ljava/lang/Object;


# instance fields
.field private final mBackupFile:Ljava/io/File;

.field private mDiskWritesInFlight:I

.field private final mFile:Ljava/io/File;

.field private final mListeners:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaded:Z

.field private mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mMode:I

.field private mStatSize:J

.field private mStatTimestamp:J

.field private final mWritingToDiskLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/app/SharedPreferencesImpl;->mContent:Ljava/lang/Object;

    #@7
    return-void
.end method

.method constructor <init>(Ljava/io/File;I)V
    .registers 5
    .parameter "file"
    .parameter "mode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 65
    iput v1, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@6
    .line 66
    iput-boolean v1, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@8
    .line 70
    new-instance v0, Ljava/lang/Object;

    #@a
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@d
    iput-object v0, p0, Landroid/app/SharedPreferencesImpl;->mWritingToDiskLock:Ljava/lang/Object;

    #@f
    .line 72
    new-instance v0, Ljava/util/WeakHashMap;

    #@11
    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    #@14
    iput-object v0, p0, Landroid/app/SharedPreferencesImpl;->mListeners:Ljava/util/WeakHashMap;

    #@16
    .line 76
    iput-object p1, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@18
    .line 77
    invoke-static {p1}, Landroid/app/SharedPreferencesImpl;->makeBackupFile(Ljava/io/File;)Ljava/io/File;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@1e
    .line 78
    iput p2, p0, Landroid/app/SharedPreferencesImpl;->mMode:I

    #@20
    .line 79
    iput-boolean v1, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@22
    .line 80
    const/4 v0, 0x0

    #@23
    iput-object v0, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@25
    .line 81
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->startLoadFromDisk()V

    #@28
    .line 82
    return-void
.end method

.method static synthetic access$000(Landroid/app/SharedPreferencesImpl;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->loadFromDiskLocked()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/app/SharedPreferencesImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/app/SharedPreferencesImpl;->enqueueDiskWrite(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/app/SharedPreferencesImpl;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@2
    return v0
.end method

.method static synthetic access$308(Landroid/app/SharedPreferencesImpl;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@6
    return v0
.end method

.method static synthetic access$310(Landroid/app/SharedPreferencesImpl;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget v0, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@6
    return v0
.end method

.method static synthetic access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Landroid/app/SharedPreferencesImpl;Ljava/util/Map;)Ljava/util/Map;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    iput-object p1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Landroid/app/SharedPreferencesImpl;)Ljava/util/WeakHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mListeners:Ljava/util/WeakHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/app/SharedPreferencesImpl;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mWritingToDiskLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$800(Landroid/app/SharedPreferencesImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/app/SharedPreferencesImpl;->writeToFile(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@3
    return-void
.end method

.method private awaitLoadedLocked()V
    .registers 2

    #@0
    .prologue
    .line 199
    iget-boolean v0, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 203
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Ldalvik/system/BlockGuard$Policy;->onReadFromDisk()V

    #@b
    .line 205
    :cond_b
    :goto_b
    iget-boolean v0, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@d
    if-nez v0, :cond_15

    #@f
    .line 207
    :try_start_f
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_12} :catch_13

    #@12
    goto :goto_b

    #@13
    .line 208
    :catch_13
    move-exception v0

    #@14
    goto :goto_b

    #@15
    .line 211
    :cond_15
    return-void
.end method

.method private static createFileOutputStream(Ljava/io/File;)Ljava/io/FileOutputStream;
    .registers 9
    .parameter "file"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 541
    const/4 v3, 0x0

    #@2
    .line 543
    .local v3, str:Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v4, Ljava/io/FileOutputStream;

    #@4
    invoke-direct {v4, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_7} :catch_a

    #@7
    .end local v3           #str:Ljava/io/FileOutputStream;
    .local v4, str:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@8
    .end local v4           #str:Ljava/io/FileOutputStream;
    .restart local v3       #str:Ljava/io/FileOutputStream;
    :goto_8
    move-object v5, v3

    #@9
    .line 560
    :goto_9
    return-object v5

    #@a
    .line 544
    :catch_a
    move-exception v0

    #@b
    .line 545
    .local v0, e:Ljava/io/FileNotFoundException;
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@e
    move-result-object v2

    #@f
    .line 546
    .local v2, parent:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    #@12
    move-result v5

    #@13
    if-nez v5, :cond_2f

    #@15
    .line 547
    const-string v5, "SharedPreferencesImpl"

    #@17
    new-instance v6, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v7, "Couldn\'t create directory for SharedPreferences file "

    #@1e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 548
    const/4 v5, 0x0

    #@2e
    goto :goto_9

    #@2f
    .line 550
    :cond_2f
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@32
    move-result-object v5

    #@33
    const/16 v6, 0x1f9

    #@35
    invoke-static {v5, v6, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@38
    .line 555
    :try_start_38
    new-instance v4, Ljava/io/FileOutputStream;

    #@3a
    invoke-direct {v4, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_38 .. :try_end_3d} :catch_3f

    #@3d
    .end local v3           #str:Ljava/io/FileOutputStream;
    .restart local v4       #str:Ljava/io/FileOutputStream;
    move-object v3, v4

    #@3e
    .line 558
    .end local v4           #str:Ljava/io/FileOutputStream;
    .restart local v3       #str:Ljava/io/FileOutputStream;
    goto :goto_8

    #@3f
    .line 556
    :catch_3f
    move-exception v1

    #@40
    .line 557
    .local v1, e2:Ljava/io/FileNotFoundException;
    const-string v5, "SharedPreferencesImpl"

    #@42
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v7, "Couldn\'t create SharedPreferences file "

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@58
    goto :goto_8
.end method

.method private enqueueDiskWrite(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V
    .registers 9
    .parameter "mcr"
    .parameter "postWriteRunnable"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 508
    new-instance v2, Landroid/app/SharedPreferencesImpl$2;

    #@4
    invoke-direct {v2, p0, p1, p2}, Landroid/app/SharedPreferencesImpl$2;-><init>(Landroid/app/SharedPreferencesImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V

    #@7
    .line 522
    .local v2, writeToDiskRunnable:Ljava/lang/Runnable;
    if-nez p2, :cond_1a

    #@9
    move v0, v3

    #@a
    .line 526
    .local v0, isFromSyncCommit:Z
    :goto_a
    if-eqz v0, :cond_21

    #@c
    .line 527
    const/4 v1, 0x0

    #@d
    .line 528
    .local v1, wasEmpty:Z
    monitor-enter p0

    #@e
    .line 529
    :try_start_e
    iget v5, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@10
    if-ne v5, v3, :cond_1c

    #@12
    move v1, v3

    #@13
    .line 530
    :goto_13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_e .. :try_end_14} :catchall_1e

    #@14
    .line 531
    if-eqz v1, :cond_21

    #@16
    .line 532
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    #@19
    .line 538
    .end local v1           #wasEmpty:Z
    :goto_19
    return-void

    #@1a
    .end local v0           #isFromSyncCommit:Z
    :cond_1a
    move v0, v4

    #@1b
    .line 522
    goto :goto_a

    #@1c
    .restart local v0       #isFromSyncCommit:Z
    .restart local v1       #wasEmpty:Z
    :cond_1c
    move v1, v4

    #@1d
    .line 529
    goto :goto_13

    #@1e
    .line 530
    :catchall_1e
    move-exception v3

    #@1f
    :try_start_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    #@20
    throw v3

    #@21
    .line 537
    .end local v1           #wasEmpty:Z
    :cond_21
    invoke-static {}, Landroid/app/QueuedWork;->singleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    #@24
    move-result-object v3

    #@25
    invoke-interface {v3, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    #@28
    goto :goto_19
.end method

.method private hasFileChangedUnexpectedly()Z
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 161
    monitor-enter p0

    #@3
    .line 162
    :try_start_3
    iget v4, p0, Landroid/app/SharedPreferencesImpl;->mDiskWritesInFlight:I

    #@5
    if-lez v4, :cond_9

    #@7
    .line 165
    monitor-exit p0

    #@8
    .line 182
    :goto_8
    return v2

    #@9
    .line 167
    :cond_9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_34

    #@a
    .line 175
    :try_start_a
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@d
    move-result-object v4

    #@e
    invoke-interface {v4}, Ldalvik/system/BlockGuard$Policy;->onReadFromDisk()V

    #@11
    .line 176
    sget-object v4, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@13
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@15
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-interface {v4, v5}, Llibcore/io/Os;->stat(Ljava/lang/String;)Llibcore/io/StructStat;
    :try_end_1c
    .catch Llibcore/io/ErrnoException; {:try_start_a .. :try_end_1c} :catch_37

    #@1c
    move-result-object v1

    #@1d
    .line 181
    .local v1, stat:Llibcore/io/StructStat;
    monitor-enter p0

    #@1e
    .line 182
    :try_start_1e
    iget-wide v4, p0, Landroid/app/SharedPreferencesImpl;->mStatTimestamp:J

    #@20
    iget-wide v6, v1, Llibcore/io/StructStat;->st_mtime:J

    #@22
    cmp-long v4, v4, v6

    #@24
    if-nez v4, :cond_2e

    #@26
    iget-wide v4, p0, Landroid/app/SharedPreferencesImpl;->mStatSize:J

    #@28
    iget-wide v6, v1, Llibcore/io/StructStat;->st_size:J

    #@2a
    cmp-long v4, v4, v6

    #@2c
    if-eqz v4, :cond_2f

    #@2e
    :cond_2e
    move v2, v3

    #@2f
    :cond_2f
    monitor-exit p0

    #@30
    goto :goto_8

    #@31
    .line 183
    :catchall_31
    move-exception v2

    #@32
    monitor-exit p0
    :try_end_33
    .catchall {:try_start_1e .. :try_end_33} :catchall_31

    #@33
    throw v2

    #@34
    .line 167
    .end local v1           #stat:Llibcore/io/StructStat;
    :catchall_34
    move-exception v2

    #@35
    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    #@36
    throw v2

    #@37
    .line 177
    :catch_37
    move-exception v0

    #@38
    .local v0, e:Llibcore/io/ErrnoException;
    move v2, v3

    #@39
    .line 178
    goto :goto_8
.end method

.method private loadFromDiskLocked()V
    .registers 9

    #@0
    .prologue
    .line 98
    iget-boolean v5, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@2
    if-eqz v5, :cond_5

    #@4
    .line 142
    :goto_4
    return-void

    #@5
    .line 101
    :cond_5
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@7
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@a
    move-result v5

    #@b
    if-eqz v5, :cond_19

    #@d
    .line 102
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@f
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@12
    .line 103
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@14
    iget-object v6, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@16
    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@19
    .line 107
    :cond_19
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@1b
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_49

    #@21
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@23
    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    #@26
    move-result v5

    #@27
    if-nez v5, :cond_49

    #@29
    .line 108
    const-string v5, "SharedPreferencesImpl"

    #@2b
    new-instance v6, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v7, "Attempt to read preferences file "

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    iget-object v7, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    const-string v7, " without permission"

    #@3e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 111
    :cond_49
    const/4 v1, 0x0

    #@4a
    .line 112
    .local v1, map:Ljava/util/Map;
    const/4 v2, 0x0

    #@4b
    .line 114
    .local v2, stat:Llibcore/io/StructStat;
    :try_start_4b
    sget-object v5, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@4d
    iget-object v6, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@4f
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-interface {v5, v6}, Llibcore/io/Os;->stat(Ljava/lang/String;)Llibcore/io/StructStat;

    #@56
    move-result-object v2

    #@57
    .line 115
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@59
    invoke-virtual {v5}, Ljava/io/File;->canRead()Z
    :try_end_5c
    .catch Llibcore/io/ErrnoException; {:try_start_4b .. :try_end_5c} :catch_95

    #@5c
    move-result v5

    #@5d
    if-eqz v5, :cond_75

    #@5f
    .line 116
    const/4 v3, 0x0

    #@60
    .line 118
    .local v3, str:Ljava/io/BufferedInputStream;
    :try_start_60
    new-instance v4, Ljava/io/BufferedInputStream;

    #@62
    new-instance v5, Ljava/io/FileInputStream;

    #@64
    iget-object v6, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@66
    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@69
    const/16 v6, 0x4000

    #@6b
    invoke-direct {v4, v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_6e
    .catchall {:try_start_60 .. :try_end_6e} :catchall_af
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_60 .. :try_end_6e} :catch_89
    .catch Ljava/io/FileNotFoundException; {:try_start_60 .. :try_end_6e} :catch_97
    .catch Ljava/io/IOException; {:try_start_60 .. :try_end_6e} :catch_a3

    #@6e
    .line 120
    .end local v3           #str:Ljava/io/BufferedInputStream;
    .local v4, str:Ljava/io/BufferedInputStream;
    :try_start_6e
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->readMapXml(Ljava/io/InputStream;)Ljava/util/HashMap;
    :try_end_71
    .catchall {:try_start_6e .. :try_end_71} :catchall_bc
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6e .. :try_end_71} :catch_c5
    .catch Ljava/io/FileNotFoundException; {:try_start_6e .. :try_end_71} :catch_c2
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_bf

    #@71
    move-result-object v1

    #@72
    .line 128
    :try_start_72
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_75
    .catch Llibcore/io/ErrnoException; {:try_start_72 .. :try_end_75} :catch_95

    #@75
    .line 133
    .end local v4           #str:Ljava/io/BufferedInputStream;
    :cond_75
    :goto_75
    const/4 v5, 0x1

    #@76
    iput-boolean v5, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@78
    .line 134
    if-eqz v1, :cond_b4

    #@7a
    .line 135
    iput-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@7c
    .line 136
    iget-wide v5, v2, Llibcore/io/StructStat;->st_mtime:J

    #@7e
    iput-wide v5, p0, Landroid/app/SharedPreferencesImpl;->mStatTimestamp:J

    #@80
    .line 137
    iget-wide v5, v2, Llibcore/io/StructStat;->st_size:J

    #@82
    iput-wide v5, p0, Landroid/app/SharedPreferencesImpl;->mStatSize:J

    #@84
    .line 141
    :goto_84
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@87
    goto/16 :goto_4

    #@89
    .line 121
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    :catch_89
    move-exception v0

    #@8a
    .line 122
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_8a
    :try_start_8a
    const-string v5, "SharedPreferencesImpl"

    #@8c
    const-string v6, "getSharedPreferences"

    #@8e
    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_91
    .catchall {:try_start_8a .. :try_end_91} :catchall_af

    #@91
    .line 128
    :try_start_91
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_94
    .catch Llibcore/io/ErrnoException; {:try_start_91 .. :try_end_94} :catch_95

    #@94
    goto :goto_75

    #@95
    .line 131
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .end local v3           #str:Ljava/io/BufferedInputStream;
    :catch_95
    move-exception v5

    #@96
    goto :goto_75

    #@97
    .line 123
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    :catch_97
    move-exception v0

    #@98
    .line 124
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_98
    :try_start_98
    const-string v5, "SharedPreferencesImpl"

    #@9a
    const-string v6, "getSharedPreferences"

    #@9c
    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9f
    .catchall {:try_start_98 .. :try_end_9f} :catchall_af

    #@9f
    .line 128
    :try_start_9f
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V
    :try_end_a2
    .catch Llibcore/io/ErrnoException; {:try_start_9f .. :try_end_a2} :catch_95

    #@a2
    goto :goto_75

    #@a3
    .line 125
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_a3
    move-exception v0

    #@a4
    .line 126
    .local v0, e:Ljava/io/IOException;
    :goto_a4
    :try_start_a4
    const-string v5, "SharedPreferencesImpl"

    #@a6
    const-string v6, "getSharedPreferences"

    #@a8
    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ab
    .catchall {:try_start_a4 .. :try_end_ab} :catchall_af

    #@ab
    .line 128
    :try_start_ab
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@ae
    goto :goto_75

    #@af
    .end local v0           #e:Ljava/io/IOException;
    :catchall_af
    move-exception v5

    #@b0
    :goto_b0
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    #@b3
    throw v5
    :try_end_b4
    .catch Llibcore/io/ErrnoException; {:try_start_ab .. :try_end_b4} :catch_95

    #@b4
    .line 139
    .end local v3           #str:Ljava/io/BufferedInputStream;
    :cond_b4
    new-instance v5, Ljava/util/HashMap;

    #@b6
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    #@b9
    iput-object v5, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@bb
    goto :goto_84

    #@bc
    .line 128
    .restart local v4       #str:Ljava/io/BufferedInputStream;
    :catchall_bc
    move-exception v5

    #@bd
    move-object v3, v4

    #@be
    .end local v4           #str:Ljava/io/BufferedInputStream;
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    goto :goto_b0

    #@bf
    .line 125
    .end local v3           #str:Ljava/io/BufferedInputStream;
    .restart local v4       #str:Ljava/io/BufferedInputStream;
    :catch_bf
    move-exception v0

    #@c0
    move-object v3, v4

    #@c1
    .end local v4           #str:Ljava/io/BufferedInputStream;
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    goto :goto_a4

    #@c2
    .line 123
    .end local v3           #str:Ljava/io/BufferedInputStream;
    .restart local v4       #str:Ljava/io/BufferedInputStream;
    :catch_c2
    move-exception v0

    #@c3
    move-object v3, v4

    #@c4
    .end local v4           #str:Ljava/io/BufferedInputStream;
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    goto :goto_98

    #@c5
    .line 121
    .end local v3           #str:Ljava/io/BufferedInputStream;
    .restart local v4       #str:Ljava/io/BufferedInputStream;
    :catch_c5
    move-exception v0

    #@c6
    move-object v3, v4

    #@c7
    .end local v4           #str:Ljava/io/BufferedInputStream;
    .restart local v3       #str:Ljava/io/BufferedInputStream;
    goto :goto_8a
.end method

.method private static makeBackupFile(Ljava/io/File;)Ljava/io/File;
    .registers 4
    .parameter "prefsFile"

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/io/File;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ".bak"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    return-object v0
.end method

.method private startLoadFromDisk()V
    .registers 3

    #@0
    .prologue
    .line 85
    monitor-enter p0

    #@1
    .line 86
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/app/SharedPreferencesImpl;->mLoaded:Z

    #@4
    .line 87
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_10

    #@5
    .line 88
    new-instance v0, Landroid/app/SharedPreferencesImpl$1;

    #@7
    const-string v1, "SharedPreferencesImpl-load"

    #@9
    invoke-direct {v0, p0, v1}, Landroid/app/SharedPreferencesImpl$1;-><init>(Landroid/app/SharedPreferencesImpl;Ljava/lang/String;)V

    #@c
    invoke-virtual {v0}, Landroid/app/SharedPreferencesImpl$1;->start()V

    #@f
    .line 95
    return-void

    #@10
    .line 87
    :catchall_10
    move-exception v0

    #@11
    :try_start_11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method private writeToFile(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V
    .registers 9
    .parameter "mcr"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 566
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@4
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@7
    move-result v3

    #@8
    if-eqz v3, :cond_53

    #@a
    .line 567
    iget-boolean v3, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->changesMade:Z

    #@c
    if-nez v3, :cond_12

    #@e
    .line 572
    invoke-virtual {p1, v4}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->setDiskWriteResult(Z)V

    #@11
    .line 625
    :goto_11
    return-void

    #@12
    .line 575
    :cond_12
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@14
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_4e

    #@1a
    .line 576
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@1c
    iget-object v4, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@1e
    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_53

    #@24
    .line 577
    const-string v3, "SharedPreferencesImpl"

    #@26
    new-instance v4, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v5, "Couldn\'t rename file "

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, " to backup file "

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 579
    invoke-virtual {p1, v6}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->setDiskWriteResult(Z)V

    #@4d
    goto :goto_11

    #@4e
    .line 583
    :cond_4e
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@50
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@53
    .line 591
    :cond_53
    :try_start_53
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@55
    invoke-static {v3}, Landroid/app/SharedPreferencesImpl;->createFileOutputStream(Ljava/io/File;)Ljava/io/FileOutputStream;

    #@58
    move-result-object v2

    #@59
    .line 592
    .local v2, str:Ljava/io/FileOutputStream;
    if-nez v2, :cond_98

    #@5b
    .line 593
    const/4 v3, 0x0

    #@5c
    invoke-virtual {p1, v3}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->setDiskWriteResult(Z)V
    :try_end_5f
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_53 .. :try_end_5f} :catch_60
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_5f} :catch_d0

    #@5f
    goto :goto_11

    #@60
    .line 613
    .end local v2           #str:Ljava/io/FileOutputStream;
    :catch_60
    move-exception v0

    #@61
    .line 614
    .local v0, e:Lorg/xmlpull/v1/XmlPullParserException;
    const-string v3, "SharedPreferencesImpl"

    #@63
    const-string/jumbo v4, "writeToFile: Got exception:"

    #@66
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@69
    .line 619
    .end local v0           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_69
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@6b
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    #@6e
    move-result v3

    #@6f
    if-eqz v3, :cond_93

    #@71
    .line 620
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@73
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@76
    move-result v3

    #@77
    if-nez v3, :cond_93

    #@79
    .line 621
    const-string v3, "SharedPreferencesImpl"

    #@7b
    new-instance v4, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v5, "Couldn\'t clean up partially-written file "

    #@82
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v4

    #@86
    iget-object v5, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@88
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v4

    #@8c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v4

    #@90
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 624
    :cond_93
    invoke-virtual {p1, v6}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->setDiskWriteResult(Z)V

    #@96
    goto/16 :goto_11

    #@98
    .line 596
    .restart local v2       #str:Ljava/io/FileOutputStream;
    :cond_98
    :try_start_98
    iget-object v3, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->mapToWriteToDisk:Ljava/util/Map;

    #@9a
    invoke-static {v3, v2}, Lcom/android/internal/util/XmlUtils;->writeMapXml(Ljava/util/Map;Ljava/io/OutputStream;)V

    #@9d
    .line 597
    invoke-static {v2}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    #@a0
    .line 598
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    #@a3
    .line 599
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@a5
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    iget v4, p0, Landroid/app/SharedPreferencesImpl;->mMode:I

    #@ab
    const/4 v5, 0x0

    #@ac
    invoke-static {v3, v4, v5}, Landroid/app/ContextImpl;->setFilePermissionsFromMode(Ljava/lang/String;II)V
    :try_end_af
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_98 .. :try_end_af} :catch_60
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_af} :catch_d0

    #@af
    .line 601
    :try_start_af
    sget-object v3, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@b1
    iget-object v4, p0, Landroid/app/SharedPreferencesImpl;->mFile:Ljava/io/File;

    #@b3
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b6
    move-result-object v4

    #@b7
    invoke-interface {v3, v4}, Llibcore/io/Os;->stat(Ljava/lang/String;)Llibcore/io/StructStat;

    #@ba
    move-result-object v1

    #@bb
    .line 602
    .local v1, stat:Llibcore/io/StructStat;
    monitor-enter p0
    :try_end_bc
    .catch Llibcore/io/ErrnoException; {:try_start_af .. :try_end_bc} :catch_dd
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_af .. :try_end_bc} :catch_60
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_bc} :catch_d0

    #@bc
    .line 603
    :try_start_bc
    iget-wide v3, v1, Llibcore/io/StructStat;->st_mtime:J

    #@be
    iput-wide v3, p0, Landroid/app/SharedPreferencesImpl;->mStatTimestamp:J

    #@c0
    .line 604
    iget-wide v3, v1, Llibcore/io/StructStat;->st_size:J

    #@c2
    iput-wide v3, p0, Landroid/app/SharedPreferencesImpl;->mStatSize:J

    #@c4
    .line 605
    monitor-exit p0
    :try_end_c5
    .catchall {:try_start_bc .. :try_end_c5} :catchall_da

    #@c5
    .line 610
    .end local v1           #stat:Llibcore/io/StructStat;
    :goto_c5
    :try_start_c5
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl;->mBackupFile:Ljava/io/File;

    #@c7
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@ca
    .line 611
    const/4 v3, 0x1

    #@cb
    invoke-virtual {p1, v3}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->setDiskWriteResult(Z)V
    :try_end_ce
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_c5 .. :try_end_ce} :catch_60
    .catch Ljava/io/IOException; {:try_start_c5 .. :try_end_ce} :catch_d0

    #@ce
    goto/16 :goto_11

    #@d0
    .line 615
    .end local v2           #str:Ljava/io/FileOutputStream;
    :catch_d0
    move-exception v0

    #@d1
    .line 616
    .local v0, e:Ljava/io/IOException;
    const-string v3, "SharedPreferencesImpl"

    #@d3
    const-string/jumbo v4, "writeToFile: Got exception:"

    #@d6
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d9
    goto :goto_69

    #@da
    .line 605
    .end local v0           #e:Ljava/io/IOException;
    .restart local v1       #stat:Llibcore/io/StructStat;
    .restart local v2       #str:Ljava/io/FileOutputStream;
    :catchall_da
    move-exception v3

    #@db
    :try_start_db
    monitor-exit p0
    :try_end_dc
    .catchall {:try_start_db .. :try_end_dc} :catchall_da

    #@dc
    :try_start_dc
    throw v3
    :try_end_dd
    .catch Llibcore/io/ErrnoException; {:try_start_dc .. :try_end_dd} :catch_dd
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_dc .. :try_end_dd} :catch_60
    .catch Ljava/io/IOException; {:try_start_dc .. :try_end_dd} :catch_d0

    #@dd
    .line 606
    .end local v1           #stat:Llibcore/io/StructStat;
    :catch_dd
    move-exception v3

    #@de
    goto :goto_c5
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 267
    monitor-enter p0

    #@1
    .line 268
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 269
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    monitor-exit p0

    #@b
    return v0

    #@c
    .line 270
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public edit()Landroid/content/SharedPreferences$Editor;
    .registers 2

    #@0
    .prologue
    .line 281
    monitor-enter p0

    #@1
    .line 282
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 283
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_b

    #@5
    .line 285
    new-instance v0, Landroid/app/SharedPreferencesImpl$EditorImpl;

    #@7
    invoke-direct {v0, p0}, Landroid/app/SharedPreferencesImpl$EditorImpl;-><init>(Landroid/app/SharedPreferencesImpl;)V

    #@a
    return-object v0

    #@b
    .line 283
    :catchall_b
    move-exception v0

    #@c
    :try_start_c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_c .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public getAll()Ljava/util/Map;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    #@0
    .prologue
    .line 214
    monitor-enter p0

    #@1
    .line 215
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 217
    new-instance v0, Ljava/util/HashMap;

    #@6
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@8
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@b
    monitor-exit p0

    #@c
    return-object v0

    #@d
    .line 218
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .registers 5
    .parameter "key"
    .parameter "defValue"

    #@0
    .prologue
    .line 259
    monitor-enter p0

    #@1
    .line 260
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 261
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Boolean;

    #@c
    .line 262
    .local v0, v:Ljava/lang/Boolean;
    if-eqz v0, :cond_12

    #@e
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    #@11
    move-result p2

    #@12
    .end local p2
    :cond_12
    monitor-exit p0

    #@13
    return p2

    #@14
    .line 263
    .end local v0           #v:Ljava/lang/Boolean;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public getFloat(Ljava/lang/String;F)F
    .registers 5
    .parameter "key"
    .parameter "defValue"

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    .line 253
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 254
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Float;

    #@c
    .line 255
    .local v0, v:Ljava/lang/Float;
    if-eqz v0, :cond_12

    #@e
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@11
    move-result p2

    #@12
    .end local p2
    :cond_12
    monitor-exit p0

    #@13
    return p2

    #@14
    .line 256
    .end local v0           #v:Ljava/lang/Float;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public getInt(Ljava/lang/String;I)I
    .registers 5
    .parameter "key"
    .parameter "defValue"

    #@0
    .prologue
    .line 238
    monitor-enter p0

    #@1
    .line 239
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 240
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Integer;

    #@c
    .line 241
    .local v0, v:Ljava/lang/Integer;
    if-eqz v0, :cond_12

    #@e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@11
    move-result p2

    #@12
    .end local p2
    :cond_12
    monitor-exit p0

    #@13
    return p2

    #@14
    .line 242
    .end local v0           #v:Ljava/lang/Integer;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public getLong(Ljava/lang/String;J)J
    .registers 6
    .parameter "key"
    .parameter "defValue"

    #@0
    .prologue
    .line 245
    monitor-enter p0

    #@1
    .line 246
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 247
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Long;

    #@c
    .line 248
    .local v0, v:Ljava/lang/Long;
    if-eqz v0, :cond_12

    #@e
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@11
    move-result-wide p2

    #@12
    .end local p2
    :cond_12
    monitor-exit p0

    #@13
    return-wide p2

    #@14
    .line 249
    .end local v0           #v:Ljava/lang/Long;
    :catchall_14
    move-exception v1

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "key"
    .parameter "defValue"

    #@0
    .prologue
    .line 222
    monitor-enter p0

    #@1
    .line 223
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 224
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 225
    .local v0, v:Ljava/lang/String;
    if-eqz v0, :cond_10

    #@e
    .end local v0           #v:Ljava/lang/String;
    :goto_e
    monitor-exit p0

    #@f
    return-object v0

    #@10
    .restart local v0       #v:Ljava/lang/String;
    :cond_10
    move-object v0, p2

    #@11
    goto :goto_e

    #@12
    .line 226
    .end local v0           #v:Ljava/lang/String;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 5
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 230
    .local p2, defValues:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    monitor-enter p0

    #@1
    .line 231
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->awaitLoadedLocked()V

    #@4
    .line 232
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/util/Set;

    #@c
    .line 233
    .local v0, v:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_10

    #@e
    .end local v0           #v:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :goto_e
    monitor-exit p0

    #@f
    return-object v0

    #@10
    .restart local v0       #v:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_10
    move-object v0, p2

    #@11
    goto :goto_e

    #@12
    .line 234
    .end local v0           #v:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    #@14
    throw v1
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 187
    monitor-enter p0

    #@1
    .line 188
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mListeners:Ljava/util/WeakHashMap;

    #@3
    sget-object v1, Landroid/app/SharedPreferencesImpl;->mContent:Ljava/lang/Object;

    #@5
    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 189
    monitor-exit p0

    #@9
    .line 190
    return-void

    #@a
    .line 189
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method startReloadIfChangedUnexpectedly()V
    .registers 2

    #@0
    .prologue
    .line 149
    monitor-enter p0

    #@1
    .line 151
    :try_start_1
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->hasFileChangedUnexpectedly()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_9

    #@7
    .line 152
    monitor-exit p0

    #@8
    .line 156
    :goto_8
    return-void

    #@9
    .line 154
    :cond_9
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl;->startLoadFromDisk()V

    #@c
    .line 155
    monitor-exit p0

    #@d
    goto :goto_8

    #@e
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 193
    monitor-enter p0

    #@1
    .line 194
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl;->mListeners:Ljava/util/WeakHashMap;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 195
    monitor-exit p0

    #@7
    .line 196
    return-void

    #@8
    .line 195
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
