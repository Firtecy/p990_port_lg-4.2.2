.class Landroid/app/ContextImpl;
.super Landroid/content/Context;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/ContextImpl$ApplicationContentResolver;,
        Landroid/app/ContextImpl$StaticServiceFetcher;,
        Landroid/app/ContextImpl$ServiceFetcher;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final EMPTY_FILE_LIST:[Ljava/lang/String; = null

.field private static final SYSTEM_SERVICE_MAP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/ContextImpl$ServiceFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ContextImpl"

.field private static WALLPAPER_FETCHER:Landroid/app/ContextImpl$ServiceFetcher;

.field private static sNextPerContextServiceCacheIndex:I

.field private static final sSharedPrefs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/SharedPreferencesImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityToken:Landroid/os/IBinder;

.field private mBasePackageName:Ljava/lang/String;

.field private mCacheDir:Ljava/io/File;

.field private mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

.field private mDatabasesDir:Ljava/io/File;

.field private mDisplay:Landroid/view/Display;

.field private mExternalCacheDir:Ljava/io/File;

.field private mExternalFilesDir:Ljava/io/File;

.field private mFilesDir:Ljava/io/File;

.field mMainThread:Landroid/app/ActivityThread;

.field private mObbDir:Ljava/io/File;

.field private mOuterContext:Landroid/content/Context;

.field mPackageInfo:Landroid/app/LoadedApk;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPreferencesDir:Ljava/io/File;

.field private mReceiverRestrictedContext:Landroid/content/Context;

.field private mResources:Landroid/content/res/Resources;

.field private mRestricted:Z

.field final mServiceCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mSync:Ljava/lang/Object;

.field private mTheme:Landroid/content/res/Resources$Theme;

.field private mThemeResource:I

.field private mUser:Landroid/os/UserHandle;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 200
    new-instance v0, Ljava/util/HashMap;

    #@3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6
    sput-object v0, Landroid/app/ContextImpl;->sSharedPrefs:Ljava/util/HashMap;

    #@8
    .line 228
    new-array v0, v1, [Ljava/lang/String;

    #@a
    sput-object v0, Landroid/app/ContextImpl;->EMPTY_FILE_LIST:[Ljava/lang/String;

    #@c
    .line 293
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    sput-object v0, Landroid/app/ContextImpl;->SYSTEM_SERVICE_MAP:Ljava/util/HashMap;

    #@13
    .line 296
    sput v1, Landroid/app/ContextImpl;->sNextPerContextServiceCacheIndex:I

    #@15
    .line 307
    new-instance v0, Landroid/app/ContextImpl$1;

    #@17
    invoke-direct {v0}, Landroid/app/ContextImpl$1;-><init>()V

    #@1a
    sput-object v0, Landroid/app/ContextImpl;->WALLPAPER_FETCHER:Landroid/app/ContextImpl$ServiceFetcher;

    #@1c
    .line 314
    const-string v0, "accessibility"

    #@1e
    new-instance v1, Landroid/app/ContextImpl$2;

    #@20
    invoke-direct {v1}, Landroid/app/ContextImpl$2;-><init>()V

    #@23
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@26
    .line 319
    const-string v0, "account"

    #@28
    new-instance v1, Landroid/app/ContextImpl$3;

    #@2a
    invoke-direct {v1}, Landroid/app/ContextImpl$3;-><init>()V

    #@2d
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@30
    .line 326
    const-string v0, "activity"

    #@32
    new-instance v1, Landroid/app/ContextImpl$4;

    #@34
    invoke-direct {v1}, Landroid/app/ContextImpl$4;-><init>()V

    #@37
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@3a
    .line 331
    const-string v0, "alarm"

    #@3c
    new-instance v1, Landroid/app/ContextImpl$5;

    #@3e
    invoke-direct {v1}, Landroid/app/ContextImpl$5;-><init>()V

    #@41
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@44
    .line 338
    const-string v0, "audio"

    #@46
    new-instance v1, Landroid/app/ContextImpl$6;

    #@48
    invoke-direct {v1}, Landroid/app/ContextImpl$6;-><init>()V

    #@4b
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@4e
    .line 343
    const-string/jumbo v0, "media_router"

    #@51
    new-instance v1, Landroid/app/ContextImpl$7;

    #@53
    invoke-direct {v1}, Landroid/app/ContextImpl$7;-><init>()V

    #@56
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@59
    .line 348
    const-string v0, "bluetooth"

    #@5b
    new-instance v1, Landroid/app/ContextImpl$8;

    #@5d
    invoke-direct {v1}, Landroid/app/ContextImpl$8;-><init>()V

    #@60
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@63
    .line 353
    const-string v0, "clipboard"

    #@65
    new-instance v1, Landroid/app/ContextImpl$9;

    #@67
    invoke-direct {v1}, Landroid/app/ContextImpl$9;-><init>()V

    #@6a
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@6d
    .line 359
    const-string v0, "connectivity"

    #@6f
    new-instance v1, Landroid/app/ContextImpl$10;

    #@71
    invoke-direct {v1}, Landroid/app/ContextImpl$10;-><init>()V

    #@74
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@77
    .line 365
    const-string v0, "country_detector"

    #@79
    new-instance v1, Landroid/app/ContextImpl$11;

    #@7b
    invoke-direct {v1}, Landroid/app/ContextImpl$11;-><init>()V

    #@7e
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@81
    .line 371
    const-string v0, "device_policy"

    #@83
    new-instance v1, Landroid/app/ContextImpl$12;

    #@85
    invoke-direct {v1}, Landroid/app/ContextImpl$12;-><init>()V

    #@88
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@8b
    .line 376
    const-string v0, "download"

    #@8d
    new-instance v1, Landroid/app/ContextImpl$13;

    #@8f
    invoke-direct {v1}, Landroid/app/ContextImpl$13;-><init>()V

    #@92
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@95
    .line 381
    const-string/jumbo v0, "nfc"

    #@98
    new-instance v1, Landroid/app/ContextImpl$14;

    #@9a
    invoke-direct {v1}, Landroid/app/ContextImpl$14;-><init>()V

    #@9d
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@a0
    .line 386
    const-string v0, "dropbox"

    #@a2
    new-instance v1, Landroid/app/ContextImpl$15;

    #@a4
    invoke-direct {v1}, Landroid/app/ContextImpl$15;-><init>()V

    #@a7
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@aa
    .line 391
    const-string v0, "input"

    #@ac
    new-instance v1, Landroid/app/ContextImpl$16;

    #@ae
    invoke-direct {v1}, Landroid/app/ContextImpl$16;-><init>()V

    #@b1
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@b4
    .line 396
    const-string v0, "display"

    #@b6
    new-instance v1, Landroid/app/ContextImpl$17;

    #@b8
    invoke-direct {v1}, Landroid/app/ContextImpl$17;-><init>()V

    #@bb
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@be
    .line 402
    const-string v0, "input_method"

    #@c0
    new-instance v1, Landroid/app/ContextImpl$18;

    #@c2
    invoke-direct {v1}, Landroid/app/ContextImpl$18;-><init>()V

    #@c5
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@c8
    .line 407
    const-string/jumbo v0, "textservices"

    #@cb
    new-instance v1, Landroid/app/ContextImpl$19;

    #@cd
    invoke-direct {v1}, Landroid/app/ContextImpl$19;-><init>()V

    #@d0
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@d3
    .line 412
    const-string/jumbo v0, "keyguard"

    #@d6
    new-instance v1, Landroid/app/ContextImpl$20;

    #@d8
    invoke-direct {v1}, Landroid/app/ContextImpl$20;-><init>()V

    #@db
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@de
    .line 421
    const-string/jumbo v0, "layout_inflater"

    #@e1
    new-instance v1, Landroid/app/ContextImpl$21;

    #@e3
    invoke-direct {v1}, Landroid/app/ContextImpl$21;-><init>()V

    #@e6
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@e9
    .line 426
    const-string/jumbo v0, "location"

    #@ec
    new-instance v1, Landroid/app/ContextImpl$22;

    #@ee
    invoke-direct {v1}, Landroid/app/ContextImpl$22;-><init>()V

    #@f1
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@f4
    .line 432
    const-string/jumbo v0, "netpolicy"

    #@f7
    new-instance v1, Landroid/app/ContextImpl$23;

    #@f9
    invoke-direct {v1}, Landroid/app/ContextImpl$23;-><init>()V

    #@fc
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@ff
    .line 440
    const-string/jumbo v0, "notification"

    #@102
    new-instance v1, Landroid/app/ContextImpl$24;

    #@104
    invoke-direct {v1}, Landroid/app/ContextImpl$24;-><init>()V

    #@107
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@10a
    .line 453
    const-string/jumbo v0, "servicediscovery"

    #@10d
    new-instance v1, Landroid/app/ContextImpl$25;

    #@10f
    invoke-direct {v1}, Landroid/app/ContextImpl$25;-><init>()V

    #@112
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@115
    .line 464
    const-string/jumbo v0, "power"

    #@118
    new-instance v1, Landroid/app/ContextImpl$26;

    #@11a
    invoke-direct {v1}, Landroid/app/ContextImpl$26;-><init>()V

    #@11d
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@120
    .line 472
    const-string/jumbo v0, "search"

    #@123
    new-instance v1, Landroid/app/ContextImpl$27;

    #@125
    invoke-direct {v1}, Landroid/app/ContextImpl$27;-><init>()V

    #@128
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@12b
    .line 478
    const-string/jumbo v0, "sensor"

    #@12e
    new-instance v1, Landroid/app/ContextImpl$28;

    #@130
    invoke-direct {v1}, Landroid/app/ContextImpl$28;-><init>()V

    #@133
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@136
    .line 483
    const-string/jumbo v0, "statusbar"

    #@139
    new-instance v1, Landroid/app/ContextImpl$29;

    #@13b
    invoke-direct {v1}, Landroid/app/ContextImpl$29;-><init>()V

    #@13e
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@141
    .line 488
    const-string/jumbo v0, "storage"

    #@144
    new-instance v1, Landroid/app/ContextImpl$30;

    #@146
    invoke-direct {v1}, Landroid/app/ContextImpl$30;-><init>()V

    #@149
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@14c
    .line 498
    const-string/jumbo v0, "phone"

    #@14f
    new-instance v1, Landroid/app/ContextImpl$31;

    #@151
    invoke-direct {v1}, Landroid/app/ContextImpl$31;-><init>()V

    #@154
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@157
    .line 503
    invoke-static {}, Landroid/telephony/MSimTelephonyManager;->getDefault()Landroid/telephony/MSimTelephonyManager;

    #@15a
    move-result-object v0

    #@15b
    invoke-virtual {v0}, Landroid/telephony/MSimTelephonyManager;->isMultiSimEnabled()Z

    #@15e
    move-result v0

    #@15f
    if-eqz v0, :cond_16c

    #@161
    .line 504
    const-string/jumbo v0, "phone_msim"

    #@164
    new-instance v1, Landroid/app/ContextImpl$32;

    #@166
    invoke-direct {v1}, Landroid/app/ContextImpl$32;-><init>()V

    #@169
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@16c
    .line 510
    :cond_16c
    const-string/jumbo v0, "throttle"

    #@16f
    new-instance v1, Landroid/app/ContextImpl$33;

    #@171
    invoke-direct {v1}, Landroid/app/ContextImpl$33;-><init>()V

    #@174
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@177
    .line 516
    const-string/jumbo v0, "uimode"

    #@17a
    new-instance v1, Landroid/app/ContextImpl$34;

    #@17c
    invoke-direct {v1}, Landroid/app/ContextImpl$34;-><init>()V

    #@17f
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@182
    .line 521
    const-string/jumbo v0, "usb"

    #@185
    new-instance v1, Landroid/app/ContextImpl$35;

    #@187
    invoke-direct {v1}, Landroid/app/ContextImpl$35;-><init>()V

    #@18a
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@18d
    .line 527
    const-string/jumbo v0, "serial"

    #@190
    new-instance v1, Landroid/app/ContextImpl$36;

    #@192
    invoke-direct {v1}, Landroid/app/ContextImpl$36;-><init>()V

    #@195
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@198
    .line 533
    const-string/jumbo v0, "vibrator"

    #@19b
    new-instance v1, Landroid/app/ContextImpl$37;

    #@19d
    invoke-direct {v1}, Landroid/app/ContextImpl$37;-><init>()V

    #@1a0
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1a3
    .line 538
    const-string/jumbo v0, "wallpaper"

    #@1a6
    sget-object v1, Landroid/app/ContextImpl;->WALLPAPER_FETCHER:Landroid/app/ContextImpl$ServiceFetcher;

    #@1a8
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1ab
    .line 540
    const-string/jumbo v0, "wifi"

    #@1ae
    new-instance v1, Landroid/app/ContextImpl$38;

    #@1b0
    invoke-direct {v1}, Landroid/app/ContextImpl$38;-><init>()V

    #@1b3
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1b6
    .line 547
    const-string/jumbo v0, "wifip2p"

    #@1b9
    new-instance v1, Landroid/app/ContextImpl$39;

    #@1bb
    invoke-direct {v1}, Landroid/app/ContextImpl$39;-><init>()V

    #@1be
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1c1
    .line 554
    const-string/jumbo v0, "window"

    #@1c4
    new-instance v1, Landroid/app/ContextImpl$40;

    #@1c6
    invoke-direct {v1}, Landroid/app/ContextImpl$40;-><init>()V

    #@1c9
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1cc
    .line 566
    const-string v0, "VZW"

    #@1ce
    const-string/jumbo v1, "ro.build.target_operator"

    #@1d1
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1d4
    move-result-object v1

    #@1d5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d8
    move-result v0

    #@1d9
    if-eqz v0, :cond_1e6

    #@1db
    .line 567
    const-string/jumbo v0, "vzwconnectivity"

    #@1de
    new-instance v1, Landroid/app/ContextImpl$41;

    #@1e0
    invoke-direct {v1}, Landroid/app/ContextImpl$41;-><init>()V

    #@1e3
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1e6
    .line 578
    :cond_1e6
    const-string v0, "VZW"

    #@1e8
    const-string/jumbo v1, "ro.build.target_operator"

    #@1eb
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@1ee
    move-result-object v1

    #@1ef
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f2
    move-result v0

    #@1f3
    if-eqz v0, :cond_1ff

    #@1f5
    .line 580
    const-string v0, "VZW_LOCATION_SERVICE"

    #@1f7
    new-instance v1, Landroid/app/ContextImpl$42;

    #@1f9
    invoke-direct {v1}, Landroid/app/ContextImpl$42;-><init>()V

    #@1fc
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@1ff
    .line 589
    :cond_1ff
    const-string/jumbo v0, "ktuca"

    #@202
    new-instance v1, Landroid/app/ContextImpl$43;

    #@204
    invoke-direct {v1}, Landroid/app/ContextImpl$43;-><init>()V

    #@207
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@20a
    .line 597
    const-string/jumbo v0, "user"

    #@20d
    new-instance v1, Landroid/app/ContextImpl$44;

    #@20f
    invoke-direct {v1}, Landroid/app/ContextImpl$44;-><init>()V

    #@212
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@215
    .line 605
    const-string v0, "SPR"

    #@217
    const-string/jumbo v1, "ro.build.target_operator"

    #@21a
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@21d
    move-result-object v1

    #@21e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@221
    move-result v0

    #@222
    if-eqz v0, :cond_244

    #@224
    .line 607
    const-string/jumbo v0, "lte"

    #@227
    new-instance v1, Landroid/app/ContextImpl$45;

    #@229
    invoke-direct {v1}, Landroid/app/ContextImpl$45;-><init>()V

    #@22c
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@22f
    .line 613
    const-string v0, "cdma"

    #@231
    new-instance v1, Landroid/app/ContextImpl$46;

    #@233
    invoke-direct {v1}, Landroid/app/ContextImpl$46;-><init>()V

    #@236
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@239
    .line 619
    const-string/jumbo v0, "wimax"

    #@23c
    new-instance v1, Landroid/app/ContextImpl$47;

    #@23e
    invoke-direct {v1}, Landroid/app/ContextImpl$47;-><init>()V

    #@241
    invoke-static {v0, v1}, Landroid/app/ContextImpl;->registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V

    #@244
    .line 627
    :cond_244
    return-void
.end method

.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1925
    invoke-direct {p0}, Landroid/content/Context;-><init>()V

    #@4
    .line 208
    iput-object v1, p0, Landroid/app/ContextImpl;->mActivityToken:Landroid/os/IBinder;

    #@6
    .line 210
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@9
    .line 211
    iput-object v1, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@b
    .line 214
    iput-object v1, p0, Landroid/app/ContextImpl;->mReceiverRestrictedContext:Landroid/content/Context;

    #@d
    .line 218
    new-instance v0, Ljava/lang/Object;

    #@f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v0, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@14
    .line 641
    new-instance v0, Ljava/util/ArrayList;

    #@16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v0, p0, Landroid/app/ContextImpl;->mServiceCache:Ljava/util/ArrayList;

    #@1b
    .line 1926
    iput-object p0, p0, Landroid/app/ContextImpl;->mOuterContext:Landroid/content/Context;

    #@1d
    .line 1927
    return-void
.end method

.method public constructor <init>(Landroid/app/ContextImpl;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1935
    invoke-direct {p0}, Landroid/content/Context;-><init>()V

    #@4
    .line 208
    iput-object v1, p0, Landroid/app/ContextImpl;->mActivityToken:Landroid/os/IBinder;

    #@6
    .line 210
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@9
    .line 211
    iput-object v1, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@b
    .line 214
    iput-object v1, p0, Landroid/app/ContextImpl;->mReceiverRestrictedContext:Landroid/content/Context;

    #@d
    .line 218
    new-instance v0, Ljava/lang/Object;

    #@f
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@12
    iput-object v0, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@14
    .line 641
    new-instance v0, Ljava/util/ArrayList;

    #@16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@19
    iput-object v0, p0, Landroid/app/ContextImpl;->mServiceCache:Ljava/util/ArrayList;

    #@1b
    .line 1936
    iget-object v0, p1, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@1d
    iput-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@1f
    .line 1937
    iget-object v0, p1, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@21
    iput-object v0, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@23
    .line 1938
    iget-object v0, p1, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@25
    iput-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@27
    .line 1939
    iget-object v0, p1, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@29
    iput-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@2b
    .line 1940
    iget-object v0, p1, Landroid/app/ContextImpl;->mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

    #@2d
    iput-object v0, p0, Landroid/app/ContextImpl;->mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

    #@2f
    .line 1941
    iget-object v0, p1, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@31
    iput-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@33
    .line 1942
    iget-object v0, p1, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@35
    iput-object v0, p0, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@37
    .line 1943
    iput-object p0, p0, Landroid/app/ContextImpl;->mOuterContext:Landroid/content/Context;

    #@39
    .line 1944
    return-void
.end method

.method static synthetic access$000()I
    .registers 1

    #@0
    .prologue
    .line 196
    sget v0, Landroid/app/ContextImpl;->sNextPerContextServiceCacheIndex:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/app/ContextImpl;)Landroid/view/Display;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 196
    iget-object v0, p0, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@2
    return-object v0
.end method

.method static createDropBoxManager()Landroid/os/DropBoxManager;
    .registers 3

    #@0
    .prologue
    .line 1577
    const-string v2, "dropbox"

    #@2
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    .line 1578
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/os/IDropBoxManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/os/IDropBoxManagerService;

    #@9
    move-result-object v1

    #@a
    .line 1579
    .local v1, service:Lcom/android/internal/os/IDropBoxManagerService;
    if-nez v1, :cond_e

    #@c
    .line 1584
    const/4 v2, 0x0

    #@d
    .line 1586
    :goto_d
    return-object v2

    #@e
    :cond_e
    new-instance v2, Landroid/os/DropBoxManager;

    #@10
    invoke-direct {v2, v1}, Landroid/os/DropBoxManager;-><init>(Lcom/android/internal/os/IDropBoxManagerService;)V

    #@13
    goto :goto_d
.end method

.method static createSystemContext(Landroid/app/ActivityThread;)Landroid/app/ContextImpl;
    .registers 4
    .parameter "mainThread"

    #@0
    .prologue
    .line 1920
    new-instance v0, Landroid/app/ContextImpl;

    #@2
    invoke-direct {v0}, Landroid/app/ContextImpl;-><init>()V

    #@5
    .line 1921
    .local v0, context:Landroid/app/ContextImpl;
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@8
    move-result-object v1

    #@9
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, p0, v2}, Landroid/app/ContextImpl;->init(Landroid/content/res/Resources;Landroid/app/ActivityThread;Landroid/os/UserHandle;)V

    #@10
    .line 1922
    return-object v0
.end method

.method private enforce(Ljava/lang/String;IZILjava/lang/String;)V
    .registers 10
    .parameter "permission"
    .parameter "resultOfCheck"
    .parameter "selfToo"
    .parameter "uid"
    .parameter "message"

    #@0
    .prologue
    .line 1629
    if-eqz p2, :cond_71

    #@2
    .line 1630
    new-instance v1, Ljava/lang/SecurityException;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    if-eqz p5, :cond_53

    #@b
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v3, ": "

    #@16
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    :goto_1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    if-eqz p3, :cond_56

    #@24
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Neither user "

    #@2b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v3, " nor current process has "

    #@35
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    :goto_3d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    const-string v2, "."

    #@47
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@52
    throw v1

    #@53
    :cond_53
    const-string v0, ""

    #@55
    goto :goto_1e

    #@56
    :cond_56
    new-instance v0, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string/jumbo v3, "uid "

    #@5e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    const-string v3, " does not have "

    #@68
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v0

    #@6c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    goto :goto_3d

    #@71
    .line 1638
    :cond_71
    return-void
.end method

.method private enforceForUri(IIZILandroid/net/Uri;Ljava/lang/String;)V
    .registers 11
    .parameter "modeFlags"
    .parameter "resultOfCheck"
    .parameter "selfToo"
    .parameter "uid"
    .parameter "uri"
    .parameter "message"

    #@0
    .prologue
    .line 1755
    if-eqz p2, :cond_7e

    #@2
    .line 1756
    new-instance v1, Ljava/lang/SecurityException;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    if-eqz p6, :cond_61

    #@b
    new-instance v0, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v3, ": "

    #@16
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    :goto_1e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    if-eqz p3, :cond_64

    #@24
    new-instance v0, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Neither user "

    #@2b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    const-string v3, " nor current process has "

    #@35
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    :goto_3d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-direct {p0, p1}, Landroid/app/ContextImpl;->uriModeFlagToString(I)Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    const-string v2, " permission on "

    #@4b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v2, "."

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@60
    throw v1

    #@61
    :cond_61
    const-string v0, ""

    #@63
    goto :goto_1e

    #@64
    :cond_64
    new-instance v0, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "User "

    #@6b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v0

    #@6f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v0

    #@73
    const-string v3, " does not have "

    #@75
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    goto :goto_3d

    #@7e
    .line 1766
    :cond_7e
    return-void
.end method

.method private getDataDirFile()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 1896
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1897
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getDataDirFile()Ljava/io/File;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    .line 1899
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    const-string v1, "Not supported in system context"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method private getDatabasesDir()Ljava/io/File;
    .registers 5

    #@0
    .prologue
    .line 971
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 972
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mDatabasesDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 973
    new-instance v0, Ljava/io/File;

    #@9
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDataDirFile()Ljava/io/File;

    #@c
    move-result-object v2

    #@d
    const-string v3, "databases"

    #@f
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@12
    iput-object v0, p0, Landroid/app/ContextImpl;->mDatabasesDir:Ljava/io/File;

    #@14
    .line 975
    :cond_14
    iget-object v0, p0, Landroid/app/ContextImpl;->mDatabasesDir:Ljava/io/File;

    #@16
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    const-string v2, "databases"

    #@1c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_2b

    #@22
    .line 976
    new-instance v0, Ljava/io/File;

    #@24
    const-string v2, "/data/system"

    #@26
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@29
    iput-object v0, p0, Landroid/app/ContextImpl;->mDatabasesDir:Ljava/io/File;

    #@2b
    .line 978
    :cond_2b
    iget-object v0, p0, Landroid/app/ContextImpl;->mDatabasesDir:Ljava/io/File;

    #@2d
    monitor-exit v1

    #@2e
    return-object v0

    #@2f
    .line 979
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    #@31
    throw v0
.end method

.method private getDisplayId()I
    .registers 2

    #@0
    .prologue
    .line 1882
    iget-object v0, p0, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@6
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method static getImpl(Landroid/content/Context;)Landroid/app/ContextImpl;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 632
    :goto_0
    instance-of v1, p0, Landroid/content/ContextWrapper;

    #@2
    if-eqz v1, :cond_f

    #@4
    move-object v1, p0

    #@5
    check-cast v1, Landroid/content/ContextWrapper;

    #@7
    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    #@a
    move-result-object v0

    #@b
    .local v0, nextContext:Landroid/content/Context;
    if-eqz v0, :cond_f

    #@d
    .line 633
    move-object p0, v0

    #@e
    goto :goto_0

    #@f
    .line 635
    .end local v0           #nextContext:Landroid/content/Context;
    :cond_f
    check-cast p0, Landroid/app/ContextImpl;

    #@11
    .end local p0
    return-object p0
.end method

.method private getPreferencesDir()Ljava/io/File;
    .registers 5

    #@0
    .prologue
    .line 770
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 771
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mPreferencesDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_15

    #@7
    .line 772
    new-instance v0, Ljava/io/File;

    #@9
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDataDirFile()Ljava/io/File;

    #@c
    move-result-object v2

    #@d
    const-string/jumbo v3, "shared_prefs"

    #@10
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@13
    iput-object v0, p0, Landroid/app/ContextImpl;->mPreferencesDir:Ljava/io/File;

    #@15
    .line 774
    :cond_15
    iget-object v0, p0, Landroid/app/ContextImpl;->mPreferencesDir:Ljava/io/File;

    #@17
    monitor-exit v1

    #@18
    return-object v0

    #@19
    .line 775
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method private getWallpaperManager()Landroid/app/WallpaperManager;
    .registers 2

    #@0
    .prologue
    .line 1573
    sget-object v0, Landroid/app/ContextImpl;->WALLPAPER_FETCHER:Landroid/app/ContextImpl$ServiceFetcher;

    #@2
    invoke-virtual {v0, p0}, Landroid/app/ContextImpl$ServiceFetcher;->getService(Landroid/app/ContextImpl;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/WallpaperManager;

    #@8
    return-object v0
.end method

.method private makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .registers 6
    .parameter "base"
    .parameter "name"

    #@0
    .prologue
    .line 2052
    sget-char v0, Ljava/io/File;->separatorChar:C

    #@2
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    #@5
    move-result v0

    #@6
    if-gez v0, :cond_e

    #@8
    .line 2053
    new-instance v0, Ljava/io/File;

    #@a
    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@d
    return-object v0

    #@e
    .line 2055
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "File "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " contains a path separator"

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0
.end method

.method private registerReceiverInternal(Landroid/content/BroadcastReceiver;ILandroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)Landroid/content/Intent;
    .registers 16
    .parameter "receiver"
    .parameter "userId"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 1399
    const/4 v3, 0x0

    #@3
    .line 1400
    .local v3, rd:Landroid/content/IIntentReceiver;
    if-eqz p1, :cond_22

    #@5
    .line 1401
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@7
    if-eqz v0, :cond_36

    #@9
    if-eqz p6, :cond_36

    #@b
    .line 1402
    if-nez p5, :cond_13

    #@d
    .line 1403
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@f
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@12
    move-result-object p5

    #@13
    .line 1405
    :cond_13
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@15
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1a
    move-result-object v4

    #@1b
    move-object v1, p1

    #@1c
    move-object v2, p6

    #@1d
    move-object v3, p5

    #@1e
    invoke-virtual/range {v0 .. v5}, Landroid/app/LoadedApk;->getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;

    #@21
    .end local v3           #rd:Landroid/content/IIntentReceiver;
    move-result-object v3

    #@22
    .line 1417
    .restart local v3       #rd:Landroid/content/IIntentReceiver;
    :cond_22
    :goto_22
    :try_start_22
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@25
    move-result-object v0

    #@26
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@28
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@2b
    move-result-object v1

    #@2c
    iget-object v2, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@2e
    move-object v4, p3

    #@2f
    move-object v5, p4

    #@30
    move v6, p2

    #@31
    invoke-interface/range {v0 .. v6}, Landroid/app/IActivityManager;->registerReceiver(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/IIntentReceiver;Landroid/content/IntentFilter;Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_34} :catch_4c

    #@34
    move-result-object v0

    #@35
    .line 1421
    :goto_35
    return-object v0

    #@36
    .line 1409
    :cond_36
    if-nez p5, :cond_3e

    #@38
    .line 1410
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@3a
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@3d
    move-result-object p5

    #@3e
    .line 1412
    :cond_3e
    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@40
    move-object v1, p1

    #@41
    move-object v2, p6

    #@42
    move-object v3, p5

    #@43
    move-object v4, v8

    #@44
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V

    #@47
    .end local v3           #rd:Landroid/content/IIntentReceiver;
    invoke-virtual {v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@4a
    move-result-object v3

    #@4b
    .restart local v3       #rd:Landroid/content/IIntentReceiver;
    goto :goto_22

    #@4c
    .line 1420
    :catch_4c
    move-exception v7

    #@4d
    .local v7, e:Landroid/os/RemoteException;
    move-object v0, v8

    #@4e
    .line 1421
    goto :goto_35
.end method

.method private static registerService(Ljava/lang/String;Landroid/app/ContextImpl$ServiceFetcher;)V
    .registers 4
    .parameter "serviceName"
    .parameter "fetcher"

    #@0
    .prologue
    .line 298
    instance-of v0, p1, Landroid/app/ContextImpl$StaticServiceFetcher;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 299
    sget v0, Landroid/app/ContextImpl;->sNextPerContextServiceCacheIndex:I

    #@6
    add-int/lit8 v1, v0, 0x1

    #@8
    sput v1, Landroid/app/ContextImpl;->sNextPerContextServiceCacheIndex:I

    #@a
    iput v0, p1, Landroid/app/ContextImpl$ServiceFetcher;->mContextCacheIndex:I

    #@c
    .line 301
    :cond_c
    sget-object v0, Landroid/app/ContextImpl;->SYSTEM_SERVICE_MAP:Ljava/util/HashMap;

    #@e
    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    .line 302
    return-void
.end method

.method static setFilePermissionsFromMode(Ljava/lang/String;II)V
    .registers 6
    .parameter "name"
    .parameter "mode"
    .parameter "extraPermissions"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 2012
    or-int/lit16 v0, p2, 0x1b0

    #@3
    .line 2015
    .local v0, perms:I
    and-int/lit8 v1, p1, 0x1

    #@5
    if-eqz v1, :cond_9

    #@7
    .line 2016
    or-int/lit8 v0, v0, 0x4

    #@9
    .line 2018
    :cond_9
    and-int/lit8 v1, p1, 0x2

    #@b
    if-eqz v1, :cond_f

    #@d
    .line 2019
    or-int/lit8 v0, v0, 0x2

    #@f
    .line 2025
    :cond_f
    invoke-static {p0, v0, v2, v2}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@12
    .line 2026
    return-void
.end method

.method private uriModeFlagToString(I)Ljava/lang/String;
    .registers 5
    .parameter "uriModeFlags"

    #@0
    .prologue
    .line 1739
    packed-switch p1, :pswitch_data_28

    #@3
    .line 1748
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown permission mode flags: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 1742
    :pswitch_1c
    const-string/jumbo v0, "read and write"

    #@1f
    .line 1746
    :goto_1f
    return-object v0

    #@20
    .line 1744
    :pswitch_20
    const-string/jumbo v0, "read"

    #@23
    goto :goto_1f

    #@24
    .line 1746
    :pswitch_24
    const-string/jumbo v0, "write"

    #@27
    goto :goto_1f

    #@28
    .line 1739
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_20
        :pswitch_24
        :pswitch_1c
    .end packed-switch
.end method

.method private validateFilePath(Ljava/lang/String;Z)Ljava/io/File;
    .registers 10
    .parameter "name"
    .parameter "createDirectory"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, -0x1

    #@2
    .line 2032
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v3

    #@6
    sget-char v4, Ljava/io/File;->separatorChar:C

    #@8
    if-ne v3, v4, :cond_40

    #@a
    .line 2033
    sget-char v3, Ljava/io/File;->separatorChar:C

    #@c
    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@f
    move-result v3

    #@10
    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 2034
    .local v1, dirPath:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@16
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@19
    .line 2035
    .local v0, dir:Ljava/io/File;
    sget-char v3, Ljava/io/File;->separatorChar:C

    #@1b
    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@1e
    move-result v3

    #@1f
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@22
    move-result-object p1

    #@23
    .line 2036
    new-instance v2, Ljava/io/File;

    #@25
    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@28
    .line 2042
    .end local v1           #dirPath:Ljava/lang/String;
    .local v2, f:Ljava/io/File;
    :goto_28
    if-eqz p2, :cond_3f

    #@2a
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_3f

    #@30
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_3f

    #@36
    .line 2043
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    const/16 v4, 0x1f9

    #@3c
    invoke-static {v3, v4, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@3f
    .line 2048
    :cond_3f
    return-object v2

    #@40
    .line 2038
    .end local v0           #dir:Ljava/io/File;
    .end local v2           #f:Ljava/io/File;
    :cond_40
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDatabasesDir()Ljava/io/File;

    #@43
    move-result-object v0

    #@44
    .line 2039
    .restart local v0       #dir:Ljava/io/File;
    invoke-direct {p0, v0, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@47
    move-result-object v2

    #@48
    .restart local v2       #f:Ljava/io/File;
    goto :goto_28
.end method

.method private warnIfCallingFromSystemProcess()V
    .registers 4

    #@0
    .prologue
    .line 1805
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x3e8

    #@6
    if-ne v0, v1, :cond_25

    #@8
    .line 1806
    const-string v0, "ContextImpl"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Calling a method in the system process without a qualified user: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const/4 v2, 0x5

    #@16
    invoke-static {v2}, Landroid/os/Debug;->getCallers(I)Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1809
    :cond_25
    return-void
.end method


# virtual methods
.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .registers 5
    .parameter "service"
    .parameter "conn"
    .parameter "flags"

    #@0
    .prologue
    .line 1495
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1496
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@6
    move-result v0

    #@7
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@a
    move-result v0

    #@b
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/ContextImpl;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@e
    move-result v0

    #@f
    return v0
.end method

.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    .registers 16
    .parameter "service"
    .parameter "conn"
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 1503
    if-nez p2, :cond_a

    #@2
    .line 1504
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "connection is null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 1506
    :cond_a
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@c
    if-eqz v0, :cond_79

    #@e
    .line 1507
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@10
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@13
    move-result-object v1

    #@14
    iget-object v2, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@16
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/app/LoadedApk;->getServiceDispatcher(Landroid/content/ServiceConnection;Landroid/content/Context;Landroid/os/Handler;I)Landroid/app/IServiceConnection;

    #@1d
    move-result-object v5

    #@1e
    .line 1513
    .local v5, sd:Landroid/app/IServiceConnection;
    :try_start_1e
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getActivityToken()Landroid/os/IBinder;

    #@21
    move-result-object v10

    #@22
    .line 1514
    .local v10, token:Landroid/os/IBinder;
    if-nez v10, :cond_3a

    #@24
    and-int/lit8 v0, p3, 0x1

    #@26
    if-nez v0, :cond_3a

    #@28
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2a
    if-eqz v0, :cond_3a

    #@2c
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2e
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@31
    move-result-object v0

    #@32
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@34
    const/16 v1, 0xe

    #@36
    if-ge v0, v1, :cond_3a

    #@38
    .line 1517
    or-int/lit8 p3, p3, 0x20

    #@3a
    .line 1519
    :cond_3a
    const/4 v0, 0x0

    #@3b
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@3e
    .line 1520
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@41
    move-result-object v0

    #@42
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@44
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getActivityToken()Landroid/os/IBinder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p1, v3}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@53
    move-result-object v4

    #@54
    move-object v3, p1

    #@55
    move v6, p3

    #@56
    move v7, p4

    #@57
    invoke-interface/range {v0 .. v7}, Landroid/app/IActivityManager;->bindService(Landroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I

    #@5a
    move-result v9

    #@5b
    .line 1524
    .local v9, res:I
    if-gez v9, :cond_81

    #@5d
    .line 1525
    new-instance v0, Ljava/lang/SecurityException;

    #@5f
    new-instance v1, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v2, "Not allowed to bind to service "

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v1

    #@6e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v1

    #@72
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@75
    throw v0
    :try_end_76
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_76} :catch_76

    #@76
    .line 1529
    .end local v9           #res:I
    .end local v10           #token:Landroid/os/IBinder;
    :catch_76
    move-exception v8

    #@77
    .line 1530
    .local v8, e:Landroid/os/RemoteException;
    const/4 v0, 0x0

    #@78
    .end local v8           #e:Landroid/os/RemoteException;
    :goto_78
    return v0

    #@79
    .line 1510
    .end local v5           #sd:Landroid/app/IServiceConnection;
    :cond_79
    new-instance v0, Ljava/lang/RuntimeException;

    #@7b
    const-string v1, "Not supported in system context"

    #@7d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@80
    throw v0

    #@81
    .line 1528
    .restart local v5       #sd:Landroid/app/IServiceConnection;
    .restart local v9       #res:I
    .restart local v10       #token:Landroid/os/IBinder;
    :cond_81
    if-eqz v9, :cond_85

    #@83
    const/4 v0, 0x1

    #@84
    goto :goto_78

    #@85
    :cond_85
    const/4 v0, 0x0

    #@86
    goto :goto_78
.end method

.method public checkCallingOrSelfPermission(Ljava/lang/String;)I
    .registers 4
    .parameter "permission"

    #@0
    .prologue
    .line 1618
    if-nez p1, :cond_b

    #@2
    .line 1619
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "permission is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 1622
    :cond_b
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@e
    move-result v0

    #@f
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@12
    move-result v1

    #@13
    invoke-virtual {p0, p1, v0, v1}, Landroid/app/ContextImpl;->checkPermission(Ljava/lang/String;II)I

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I
    .registers 5
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 1708
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/app/ContextImpl;->checkUriPermission(Landroid/net/Uri;III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public checkCallingPermission(Ljava/lang/String;)I
    .registers 5
    .parameter "permission"

    #@0
    .prologue
    .line 1605
    if-nez p1, :cond_b

    #@2
    .line 1606
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "permission is null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1609
    :cond_b
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@e
    move-result v0

    #@f
    .line 1610
    .local v0, pid:I
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@12
    move-result v1

    #@13
    if-eq v0, v1, :cond_1e

    #@15
    .line 1611
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@18
    move-result v1

    #@19
    invoke-virtual {p0, p1, v0, v1}, Landroid/app/ContextImpl;->checkPermission(Ljava/lang/String;II)I

    #@1c
    move-result v1

    #@1d
    .line 1613
    :goto_1d
    return v1

    #@1e
    :cond_1e
    const/4 v1, -0x1

    #@1f
    goto :goto_1d
.end method

.method public checkCallingUriPermission(Landroid/net/Uri;I)I
    .registers 5
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 1698
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@3
    move-result v0

    #@4
    .line 1699
    .local v0, pid:I
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@7
    move-result v1

    #@8
    if-eq v0, v1, :cond_13

    #@a
    .line 1700
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/app/ContextImpl;->checkUriPermission(Landroid/net/Uri;III)I

    #@11
    move-result v1

    #@12
    .line 1703
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, -0x1

    #@14
    goto :goto_12
.end method

.method public checkPermission(Ljava/lang/String;II)I
    .registers 7
    .parameter "permission"
    .parameter "pid"
    .parameter "uid"

    #@0
    .prologue
    .line 1591
    if-nez p1, :cond_b

    #@2
    .line 1592
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "permission is null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1596
    :cond_b
    :try_start_b
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@e
    move-result-object v1

    #@f
    invoke-interface {v1, p1, p2, p3}, Landroid/app/IActivityManager;->checkPermission(Ljava/lang/String;II)I
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_12} :catch_14

    #@12
    move-result v1

    #@13
    .line 1599
    :goto_13
    return v1

    #@14
    .line 1598
    :catch_14
    move-exception v0

    #@15
    .line 1599
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@16
    goto :goto_13
.end method

.method public checkUriPermission(Landroid/net/Uri;III)I
    .registers 7
    .parameter "uri"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 1689
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/IActivityManager;->checkUriPermission(Landroid/net/Uri;III)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 1692
    :goto_8
    return v1

    #@9
    .line 1691
    :catch_9
    move-exception v0

    #@a
    .line 1692
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@b
    goto :goto_8
.end method

.method public checkUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;III)I
    .registers 9
    .parameter "uri"
    .parameter "readPermission"
    .parameter "writePermission"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1720
    and-int/lit8 v1, p6, 0x1

    #@3
    if-eqz v1, :cond_e

    #@5
    .line 1721
    if-eqz p2, :cond_d

    #@7
    invoke-virtual {p0, p2, p4, p5}, Landroid/app/ContextImpl;->checkPermission(Ljava/lang/String;II)I

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_e

    #@d
    .line 1734
    :cond_d
    :goto_d
    return v0

    #@e
    .line 1727
    :cond_e
    and-int/lit8 v1, p6, 0x2

    #@10
    if-eqz v1, :cond_1a

    #@12
    .line 1728
    if-eqz p3, :cond_d

    #@14
    invoke-virtual {p0, p3, p4, p5}, Landroid/app/ContextImpl;->checkPermission(Ljava/lang/String;II)I

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_d

    #@1a
    .line 1734
    :cond_1a
    if-eqz p1, :cond_21

    #@1c
    invoke-virtual {p0, p1, p4, p5, p6}, Landroid/app/ContextImpl;->checkUriPermission(Landroid/net/Uri;III)I

    #@1f
    move-result v0

    #@20
    goto :goto_d

    #@21
    :cond_21
    const/4 v0, -0x1

    #@22
    goto :goto_d
.end method

.method public clearWallpaper()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1014
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->clear()V

    #@7
    .line 1015
    return-void
.end method

.method public createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;
    .registers 7
    .parameter "overrideConfiguration"

    #@0
    .prologue
    .line 1847
    if-nez p1, :cond_b

    #@2
    .line 1848
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "overrideConfiguration must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1851
    :cond_b
    new-instance v0, Landroid/app/ContextImpl;

    #@d
    invoke-direct {v0}, Landroid/app/ContextImpl;-><init>()V

    #@10
    .line 1852
    .local v0, c:Landroid/app/ContextImpl;
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@12
    const/4 v2, 0x0

    #@13
    iget-object v3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@18
    .line 1853
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@1a
    iget-object v2, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@1c
    invoke-virtual {v2}, Landroid/app/LoadedApk;->getResDir()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDisplayId()I

    #@23
    move-result v3

    #@24
    iget-object v4, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@26
    invoke-virtual {v4}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v1, v2, v3, p1, v4}, Landroid/app/ActivityThread;->getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, v0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@30
    .line 1857
    return-object v0
.end method

.method public createDisplayContext(Landroid/view/Display;)Landroid/content/Context;
    .registers 9
    .parameter "display"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1862
    if-nez p1, :cond_b

    #@3
    .line 1863
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@5
    const-string v5, "display must not be null"

    #@7
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v4

    #@b
    .line 1866
    :cond_b
    invoke-virtual {p1}, Landroid/view/Display;->getDisplayId()I

    #@e
    move-result v3

    #@f
    .line 1867
    .local v3, displayId:I
    sget-object v0, Landroid/content/res/CompatibilityInfo;->DEFAULT_COMPATIBILITY_INFO:Landroid/content/res/CompatibilityInfo;

    #@11
    .line 1868
    .local v0, ci:Landroid/content/res/CompatibilityInfo;
    invoke-virtual {p0, v3}, Landroid/app/ContextImpl;->getCompatibilityInfo(I)Landroid/view/CompatibilityInfoHolder;

    #@14
    move-result-object v1

    #@15
    .line 1869
    .local v1, cih:Landroid/view/CompatibilityInfoHolder;
    if-eqz v1, :cond_1b

    #@17
    .line 1870
    invoke-virtual {v1}, Landroid/view/CompatibilityInfoHolder;->get()Landroid/content/res/CompatibilityInfo;

    #@1a
    move-result-object v0

    #@1b
    .line 1873
    :cond_1b
    new-instance v2, Landroid/app/ContextImpl;

    #@1d
    invoke-direct {v2}, Landroid/app/ContextImpl;-><init>()V

    #@20
    .line 1874
    .local v2, context:Landroid/app/ContextImpl;
    iget-object v4, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@22
    iget-object v5, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@24
    invoke-virtual {v2, v4, v6, v5}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@27
    .line 1875
    iput-object p1, v2, Landroid/app/ContextImpl;->mDisplay:Landroid/view/Display;

    #@29
    .line 1876
    iget-object v4, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@2b
    iget-object v5, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2d
    invoke-virtual {v5}, Landroid/app/LoadedApk;->getResDir()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v4, v5, v3, v6, v0}, Landroid/app/ActivityThread;->getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;

    #@34
    move-result-object v4

    #@35
    iput-object v4, v2, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@37
    .line 1878
    return-object v2
.end method

.method public createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    .registers 4
    .parameter "packageName"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1814
    iget-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@6
    :goto_6
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/ContextImpl;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    :cond_b
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@e
    move-result-object v0

    #@f
    goto :goto_6
.end method

.method public createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    .registers 14
    .parameter "packageName"
    .parameter "flags"
    .parameter "user"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v9, 0x4

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v5, 0x0

    #@4
    .line 1821
    const-string/jumbo v6, "system"

    #@7
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v6

    #@b
    if-nez v6, :cond_15

    #@d
    const-string v6, "android"

    #@f
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v6

    #@13
    if-eqz v6, :cond_36

    #@15
    .line 1822
    :cond_15
    new-instance v0, Landroid/app/ContextImpl;

    #@17
    iget-object v6, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@19
    invoke-virtual {v6}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    #@1c
    move-result-object v6

    #@1d
    invoke-direct {v0, v6}, Landroid/app/ContextImpl;-><init>(Landroid/app/ContextImpl;)V

    #@20
    .line 1823
    .local v0, context:Landroid/app/ContextImpl;
    and-int/lit8 v6, p2, 0x4

    #@22
    if-ne v6, v9, :cond_34

    #@24
    :goto_24
    iput-boolean v1, v0, Landroid/app/ContextImpl;->mRestricted:Z

    #@26
    .line 1824
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@28
    iget-object v3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@2a
    iget-object v4, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@2c
    iget-object v5, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@2e
    move-object v6, p3

    #@2f
    invoke-virtual/range {v0 .. v6}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;Landroid/content/res/Resources;Ljava/lang/String;Landroid/os/UserHandle;)V

    #@32
    move-object v3, v0

    #@33
    .line 1836
    .end local v0           #context:Landroid/app/ContextImpl;
    :cond_33
    return-object v3

    #@34
    .restart local v0       #context:Landroid/app/ContextImpl;
    :cond_34
    move v1, v5

    #@35
    .line 1823
    goto :goto_24

    #@36
    .line 1828
    .end local v0           #context:Landroid/app/ContextImpl;
    :cond_36
    iget-object v6, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@38
    iget-object v7, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@3a
    invoke-virtual {v7}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {p3}, Landroid/os/UserHandle;->getIdentifier()I

    #@41
    move-result v8

    #@42
    invoke-virtual {v6, p1, v7, p2, v8}, Landroid/app/ActivityThread;->getPackageInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;II)Landroid/app/LoadedApk;

    #@45
    move-result-object v4

    #@46
    .line 1831
    .local v4, pi:Landroid/app/LoadedApk;
    if-eqz v4, :cond_62

    #@48
    .line 1832
    new-instance v3, Landroid/app/ContextImpl;

    #@4a
    invoke-direct {v3}, Landroid/app/ContextImpl;-><init>()V

    #@4d
    .line 1833
    .local v3, c:Landroid/app/ContextImpl;
    and-int/lit8 v6, p2, 0x4

    #@4f
    if-ne v6, v9, :cond_81

    #@51
    :goto_51
    iput-boolean v1, v3, Landroid/app/ContextImpl;->mRestricted:Z

    #@53
    .line 1834
    iget-object v6, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@55
    iget-object v7, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@57
    iget-object v8, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@59
    move-object v5, v2

    #@5a
    move-object v9, p3

    #@5b
    invoke-virtual/range {v3 .. v9}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;Landroid/content/res/Resources;Ljava/lang/String;Landroid/os/UserHandle;)V

    #@5e
    .line 1835
    iget-object v1, v3, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@60
    if-nez v1, :cond_33

    #@62
    .line 1841
    .end local v3           #c:Landroid/app/ContextImpl;
    :cond_62
    new-instance v1, Landroid/content/pm/PackageManager$NameNotFoundException;

    #@64
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v5, "Application package "

    #@6b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v2

    #@73
    const-string v5, " not found"

    #@75
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    invoke-direct {v1, v2}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>(Ljava/lang/String;)V

    #@80
    throw v1

    #@81
    .restart local v3       #c:Landroid/app/ContextImpl;
    :cond_81
    move v1, v5

    #@82
    .line 1833
    goto :goto_51
.end method

.method public databaseList()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 965
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDatabasesDir()Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 966
    .local v0, list:[Ljava/lang/String;
    if-eqz v0, :cond_b

    #@a
    .end local v0           #list:[Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #list:[Ljava/lang/String;
    :cond_b
    sget-object v0, Landroid/app/ContextImpl;->EMPTY_FILE_LIST:[Ljava/lang/String;

    #@d
    goto :goto_a
.end method

.method public deleteDatabase(Ljava/lang/String;)Z
    .registers 5
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 951
    const/4 v2, 0x0

    #@2
    :try_start_2
    invoke-direct {p0, p1, v2}, Landroid/app/ContextImpl;->validateFilePath(Ljava/lang/String;Z)Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    .line 952
    .local v0, f:Ljava/io/File;
    invoke-static {v0}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 955
    .end local v0           #f:Ljava/io/File;
    :goto_a
    return v1

    #@b
    .line 953
    :catch_b
    move-exception v2

    #@c
    goto :goto_a
.end method

.method public deleteFile(Ljava/lang/String;)Z
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 810
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getFilesDir()Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0, v1, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@7
    move-result-object v0

    #@8
    .line 811
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@b
    move-result v1

    #@c
    return v1
.end method

.method public enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "permission"
    .parameter "message"

    #@0
    .prologue
    .line 1659
    invoke-virtual {p0, p1}, Landroid/app/ContextImpl;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x1

    #@5
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8
    move-result v4

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v5, p2

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/app/ContextImpl;->enforce(Ljava/lang/String;IZILjava/lang/String;)V

    #@f
    .line 1664
    return-void
.end method

.method public enforceCallingOrSelfUriPermission(Landroid/net/Uri;ILjava/lang/String;)V
    .registers 11
    .parameter "uri"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 1785
    invoke-virtual {p0, p1, p2}, Landroid/app/ContextImpl;->checkCallingOrSelfUriPermission(Landroid/net/Uri;I)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x1

    #@5
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8
    move-result v4

    #@9
    move-object v0, p0

    #@a
    move v1, p2

    #@b
    move-object v5, p1

    #@c
    move-object v6, p3

    #@d
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->enforceForUri(IIZILandroid/net/Uri;Ljava/lang/String;)V

    #@10
    .line 1789
    return-void
.end method

.method public enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "permission"
    .parameter "message"

    #@0
    .prologue
    .line 1650
    invoke-virtual {p0, p1}, Landroid/app/ContextImpl;->checkCallingPermission(Ljava/lang/String;)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x0

    #@5
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8
    move-result v4

    #@9
    move-object v0, p0

    #@a
    move-object v1, p1

    #@b
    move-object v5, p2

    #@c
    invoke-direct/range {v0 .. v5}, Landroid/app/ContextImpl;->enforce(Ljava/lang/String;IZILjava/lang/String;)V

    #@f
    .line 1655
    return-void
.end method

.method public enforceCallingUriPermission(Landroid/net/Uri;ILjava/lang/String;)V
    .registers 11
    .parameter "uri"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 1777
    invoke-virtual {p0, p1, p2}, Landroid/app/ContextImpl;->checkCallingUriPermission(Landroid/net/Uri;I)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x0

    #@5
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@8
    move-result v4

    #@9
    move-object v0, p0

    #@a
    move v1, p2

    #@b
    move-object v5, p1

    #@c
    move-object v6, p3

    #@d
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->enforceForUri(IIZILandroid/net/Uri;Ljava/lang/String;)V

    #@10
    .line 1781
    return-void
.end method

.method public enforcePermission(Ljava/lang/String;IILjava/lang/String;)V
    .registers 11
    .parameter "permission"
    .parameter "pid"
    .parameter "uid"
    .parameter "message"

    #@0
    .prologue
    .line 1642
    invoke-virtual {p0, p1, p2, p3}, Landroid/app/ContextImpl;->checkPermission(Ljava/lang/String;II)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x0

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move v4, p3

    #@8
    move-object v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Landroid/app/ContextImpl;->enforce(Ljava/lang/String;IZILjava/lang/String;)V

    #@c
    .line 1647
    return-void
.end method

.method public enforceUriPermission(Landroid/net/Uri;IIILjava/lang/String;)V
    .registers 13
    .parameter "uri"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 1770
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/app/ContextImpl;->checkUriPermission(Landroid/net/Uri;III)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x0

    #@5
    move-object v0, p0

    #@6
    move v1, p4

    #@7
    move v4, p3

    #@8
    move-object v5, p1

    #@9
    move-object v6, p5

    #@a
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->enforceForUri(IIZILandroid/net/Uri;Ljava/lang/String;)V

    #@d
    .line 1773
    return-void
.end method

.method public enforceUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .registers 15
    .parameter "uri"
    .parameter "readPermission"
    .parameter "writePermission"
    .parameter "pid"
    .parameter "uid"
    .parameter "modeFlags"
    .parameter "message"

    #@0
    .prologue
    .line 1794
    invoke-virtual/range {p0 .. p6}, Landroid/app/ContextImpl;->checkUriPermission(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;III)I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x0

    #@5
    move-object v0, p0

    #@6
    move v1, p6

    #@7
    move v4, p5

    #@8
    move-object v5, p1

    #@9
    move-object v6, p7

    #@a
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->enforceForUri(IIZILandroid/net/Uri;Ljava/lang/String;)V

    #@d
    .line 1802
    return-void
.end method

.method public fileList()[Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 926
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getFilesDir()Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 927
    .local v0, list:[Ljava/lang/String;
    if-eqz v0, :cond_b

    #@a
    .end local v0           #list:[Ljava/lang/String;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #list:[Ljava/lang/String;
    :cond_b
    sget-object v0, Landroid/app/ContextImpl;->EMPTY_FILE_LIST:[Ljava/lang/String;

    #@d
    goto :goto_a
.end method

.method final getActivityToken()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 2007
    iget-object v0, p0, Landroid/app/ContextImpl;->mActivityToken:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 680
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getApplication()Landroid/app/Application;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@d
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    #@10
    move-result-object v0

    #@11
    goto :goto_a
.end method

.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .registers 3

    #@0
    .prologue
    .line 721
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 722
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    .line 724
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    const-string v1, "Not supported in system context"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method public getAssets()Landroid/content/res/AssetManager;
    .registers 2

    #@0
    .prologue
    .line 645
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getCacheDir()Ljava/io/File;
    .registers 6

    #@0
    .prologue
    .line 879
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 880
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 881
    new-instance v0, Ljava/io/File;

    #@9
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDataDirFile()Ljava/io/File;

    #@c
    move-result-object v2

    #@d
    const-string v3, "cache"

    #@f
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@12
    iput-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@14
    .line 883
    :cond_14
    iget-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@16
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_52

    #@1c
    .line 884
    iget-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@1e
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_45

    #@24
    .line 885
    const-string v0, "ContextImpl"

    #@26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "Unable to create cache directory "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v3, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@33
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 886
    const/4 v0, 0x0

    #@43
    monitor-exit v1

    #@44
    .line 894
    :goto_44
    return-object v0

    #@45
    .line 888
    :cond_45
    iget-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@47
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    const/16 v2, 0x1f9

    #@4d
    const/4 v3, -0x1

    #@4e
    const/4 v4, -0x1

    #@4f
    invoke-static {v0, v2, v3, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@52
    .line 893
    :cond_52
    monitor-exit v1
    :try_end_53
    .catchall {:try_start_3 .. :try_end_53} :catchall_56

    #@53
    .line 894
    iget-object v0, p0, Landroid/app/ContextImpl;->mCacheDir:Ljava/io/File;

    #@55
    goto :goto_44

    #@56
    .line 893
    :catchall_56
    move-exception v0

    #@57
    :try_start_57
    monitor-exit v1
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_56

    #@58
    throw v0
.end method

.method public getClassLoader()Ljava/lang/ClassLoader;
    .registers 2

    #@0
    .prologue
    .line 707
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method public getCompatibilityInfo(I)Landroid/view/CompatibilityInfoHolder;
    .registers 3
    .parameter "displayId"

    #@0
    .prologue
    .line 1892
    if-nez p1, :cond_7

    #@2
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@4
    iget-object v0, v0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .registers 2

    #@0
    .prologue
    .line 670
    iget-object v0, p0, Landroid/app/ContextImpl;->mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

    #@2
    return-object v0
.end method

.method public getDatabasePath(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 960
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/app/ContextImpl;->validateFilePath(Ljava/lang/String;Z)Ljava/io/File;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getDir(Ljava/lang/String;I)Ljava/io/File;
    .registers 6
    .parameter "name"
    .parameter "mode"

    #@0
    .prologue
    .line 1904
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "app_"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object p1

    #@13
    .line 1905
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDataDirFile()Ljava/io/File;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v1, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@1a
    move-result-object v0

    #@1b
    .line 1906
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_2d

    #@21
    .line 1907
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    #@24
    .line 1908
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    const/16 v2, 0x1f9

    #@2a
    invoke-static {v1, p2, v2}, Landroid/app/ContextImpl;->setFilePermissionsFromMode(Ljava/lang/String;II)V

    #@2d
    .line 1911
    :cond_2d
    return-object v0
.end method

.method public getExternalCacheDir()Ljava/io/File;
    .registers 5

    #@0
    .prologue
    .line 899
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 900
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mExternalCacheDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_11

    #@7
    .line 901
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getPackageName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/os/Environment;->getExternalStorageAppCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/app/ContextImpl;->mExternalCacheDir:Ljava/io/File;

    #@11
    .line 904
    :cond_11
    iget-object v0, p0, Landroid/app/ContextImpl;->mExternalCacheDir:Ljava/io/File;

    #@13
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_3d

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_39

    #@19
    .line 906
    :try_start_19
    new-instance v0, Ljava/io/File;

    #@1b
    invoke-static {}, Landroid/os/Environment;->getExternalStorageAndroidDataDir()Ljava/io/File;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ".nomedia"

    #@21
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@24
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_27
    .catchall {:try_start_19 .. :try_end_27} :catchall_3d
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_27} :catch_40

    #@27
    .line 910
    :goto_27
    :try_start_27
    iget-object v0, p0, Landroid/app/ContextImpl;->mExternalCacheDir:Ljava/io/File;

    #@29
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@2c
    move-result v0

    #@2d
    if-nez v0, :cond_39

    #@2f
    .line 911
    const-string v0, "ContextImpl"

    #@31
    const-string v2, "Unable to create external cache directory"

    #@33
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 912
    const/4 v0, 0x0

    #@37
    monitor-exit v1

    #@38
    .line 915
    :goto_38
    return-object v0

    #@39
    :cond_39
    iget-object v0, p0, Landroid/app/ContextImpl;->mExternalCacheDir:Ljava/io/File;

    #@3b
    monitor-exit v1

    #@3c
    goto :goto_38

    #@3d
    .line 916
    :catchall_3d
    move-exception v0

    #@3e
    monitor-exit v1
    :try_end_3f
    .catchall {:try_start_27 .. :try_end_3f} :catchall_3d

    #@3f
    throw v0

    #@40
    .line 908
    :catch_40
    move-exception v0

    #@41
    goto :goto_27
.end method

.method public getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;
    .registers 8
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 836
    iget-object v2, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 837
    :try_start_4
    iget-object v3, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@6
    if-nez v3, :cond_12

    #@8
    .line 838
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getPackageName()Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    invoke-static {v3}, Landroid/os/Environment;->getExternalStorageAppFilesDirectory(Ljava/lang/String;)Ljava/io/File;

    #@f
    move-result-object v3

    #@10
    iput-object v3, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@12
    .line 841
    :cond_12
    iget-object v3, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@14
    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_40

    #@17
    move-result v3

    #@18
    if-nez v3, :cond_3a

    #@1a
    .line 843
    :try_start_1a
    new-instance v3, Ljava/io/File;

    #@1c
    invoke-static {}, Landroid/os/Environment;->getExternalStorageAndroidDataDir()Ljava/io/File;

    #@1f
    move-result-object v4

    #@20
    const-string v5, ".nomedia"

    #@22
    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@25
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_28
    .catchall {:try_start_1a .. :try_end_28} :catchall_40
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_28} :catch_73

    #@28
    .line 847
    :goto_28
    :try_start_28
    iget-object v3, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@2a
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_3a

    #@30
    .line 848
    const-string v3, "ContextImpl"

    #@32
    const-string v4, "Unable to create external files directory"

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 849
    monitor-exit v2

    #@38
    move-object v0, v1

    #@39
    .line 862
    :goto_39
    return-object v0

    #@3a
    .line 852
    :cond_3a
    if-nez p1, :cond_43

    #@3c
    .line 853
    iget-object v0, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@3e
    monitor-exit v2

    #@3f
    goto :goto_39

    #@40
    .line 863
    :catchall_40
    move-exception v1

    #@41
    monitor-exit v2
    :try_end_42
    .catchall {:try_start_28 .. :try_end_42} :catchall_40

    #@42
    throw v1

    #@43
    .line 855
    :cond_43
    :try_start_43
    new-instance v0, Ljava/io/File;

    #@45
    iget-object v3, p0, Landroid/app/ContextImpl;->mExternalFilesDir:Ljava/io/File;

    #@47
    invoke-direct {v0, v3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@4a
    .line 856
    .local v0, dir:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@4d
    move-result v3

    #@4e
    if-nez v3, :cond_71

    #@50
    .line 857
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@53
    move-result v3

    #@54
    if-nez v3, :cond_71

    #@56
    .line 858
    const-string v3, "ContextImpl"

    #@58
    new-instance v4, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v5, "Unable to create external media directory "

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 859
    monitor-exit v2

    #@6f
    move-object v0, v1

    #@70
    goto :goto_39

    #@71
    .line 862
    :cond_71
    monitor-exit v2
    :try_end_72
    .catchall {:try_start_43 .. :try_end_72} :catchall_40

    #@72
    goto :goto_39

    #@73
    .line 845
    .end local v0           #dir:Ljava/io/File;
    :catch_73
    move-exception v3

    #@74
    goto :goto_28
.end method

.method public getFileStreamPath(Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 921
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getFilesDir()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getFilesDir()Ljava/io/File;
    .registers 6

    #@0
    .prologue
    .line 816
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 817
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_14

    #@7
    .line 818
    new-instance v0, Ljava/io/File;

    #@9
    invoke-direct {p0}, Landroid/app/ContextImpl;->getDataDirFile()Ljava/io/File;

    #@c
    move-result-object v2

    #@d
    const-string v3, "files"

    #@f
    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@12
    iput-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@14
    .line 820
    :cond_14
    iget-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@16
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_52

    #@1c
    .line 821
    iget-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@1e
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_45

    #@24
    .line 822
    const-string v0, "ContextImpl"

    #@26
    new-instance v2, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v3, "Unable to create files directory "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-object v3, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@33
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 823
    const/4 v0, 0x0

    #@43
    monitor-exit v1

    #@44
    .line 830
    :goto_44
    return-object v0

    #@45
    .line 825
    :cond_45
    iget-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@47
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    const/16 v2, 0x1f9

    #@4d
    const/4 v3, -0x1

    #@4e
    const/4 v4, -0x1

    #@4f
    invoke-static {v0, v2, v3, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@52
    .line 830
    :cond_52
    iget-object v0, p0, Landroid/app/ContextImpl;->mFilesDir:Ljava/io/File;

    #@54
    monitor-exit v1

    #@55
    goto :goto_44

    #@56
    .line 831
    :catchall_56
    move-exception v0

    #@57
    monitor-exit v1
    :try_end_58
    .catchall {:try_start_3 .. :try_end_58} :catchall_56

    #@58
    throw v0
.end method

.method public getMainLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 675
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@2
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getObbDir()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 868
    iget-object v1, p0, Landroid/app/ContextImpl;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 869
    :try_start_3
    iget-object v0, p0, Landroid/app/ContextImpl;->mObbDir:Ljava/io/File;

    #@5
    if-nez v0, :cond_11

    #@7
    .line 870
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getPackageName()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/os/Environment;->getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/app/ContextImpl;->mObbDir:Ljava/io/File;

    #@11
    .line 873
    :cond_11
    iget-object v0, p0, Landroid/app/ContextImpl;->mObbDir:Ljava/io/File;

    #@13
    monitor-exit v1

    #@14
    return-object v0

    #@15
    .line 874
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method final getOuterContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 2003
    iget-object v0, p0, Landroid/app/ContextImpl;->mOuterContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getPackageCodePath()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 737
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 738
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getAppDir()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    .line 740
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    const-string v1, "Not supported in system context"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method public getPackageManager()Landroid/content/pm/PackageManager;
    .registers 3

    #@0
    .prologue
    .line 655
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageManager:Landroid/content/pm/PackageManager;

    #@2
    if-eqz v1, :cond_7

    #@4
    .line 656
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageManager:Landroid/content/pm/PackageManager;

    #@6
    .line 665
    :goto_6
    return-object v1

    #@7
    .line 659
    :cond_7
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@a
    move-result-object v0

    #@b
    .line 660
    .local v0, pm:Landroid/content/pm/IPackageManager;
    if-eqz v0, :cond_15

    #@d
    .line 662
    new-instance v1, Landroid/app/ApplicationPackageManager;

    #@f
    invoke-direct {v1, p0, v0}, Landroid/app/ApplicationPackageManager;-><init>(Landroid/app/ContextImpl;Landroid/content/pm/IPackageManager;)V

    #@12
    iput-object v1, p0, Landroid/app/ContextImpl;->mPackageManager:Landroid/content/pm/PackageManager;

    #@14
    goto :goto_6

    #@15
    .line 665
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_6
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 713
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 714
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    .line 716
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    const-string v1, "Not supported in system context"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method public getPackageResourcePath()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 729
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 730
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getResDir()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0

    #@b
    .line 732
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    #@d
    const-string v1, "Not supported in system context"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0
.end method

.method final getReceiverRestrictedContext()Landroid/content/Context;
    .registers 3

    #@0
    .prologue
    .line 1992
    iget-object v0, p0, Landroid/app/ContextImpl;->mReceiverRestrictedContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1993
    iget-object v0, p0, Landroid/app/ContextImpl;->mReceiverRestrictedContext:Landroid/content/Context;

    #@6
    .line 1995
    :goto_6
    return-object v0

    #@7
    :cond_7
    new-instance v0, Landroid/app/ReceiverRestrictedContext;

    #@9
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@c
    move-result-object v1

    #@d
    invoke-direct {v0, v1}, Landroid/app/ReceiverRestrictedContext;-><init>(Landroid/content/Context;)V

    #@10
    iput-object v0, p0, Landroid/app/ContextImpl;->mReceiverRestrictedContext:Landroid/content/Context;

    #@12
    goto :goto_6
.end method

.method public getResources()Landroid/content/res/Resources;
    .registers 2

    #@0
    .prologue
    .line 650
    iget-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@2
    return-object v0
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .registers 8
    .parameter "name"
    .parameter "mode"

    #@0
    .prologue
    .line 750
    sget-object v4, Landroid/app/ContextImpl;->sSharedPrefs:Ljava/util/HashMap;

    #@2
    monitor-enter v4

    #@3
    .line 751
    :try_start_3
    sget-object v3, Landroid/app/ContextImpl;->sSharedPrefs:Ljava/util/HashMap;

    #@5
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Landroid/app/SharedPreferencesImpl;

    #@b
    .line 752
    .local v1, sp:Landroid/app/SharedPreferencesImpl;
    if-nez v1, :cond_1e

    #@d
    .line 753
    invoke-virtual {p0, p1}, Landroid/app/ContextImpl;->getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;

    #@10
    move-result-object v0

    #@11
    .line 754
    .local v0, prefsFile:Ljava/io/File;
    new-instance v1, Landroid/app/SharedPreferencesImpl;

    #@13
    .end local v1           #sp:Landroid/app/SharedPreferencesImpl;
    invoke-direct {v1, v0, p2}, Landroid/app/SharedPreferencesImpl;-><init>(Ljava/io/File;I)V

    #@16
    .line 755
    .restart local v1       #sp:Landroid/app/SharedPreferencesImpl;
    sget-object v3, Landroid/app/ContextImpl;->sSharedPrefs:Ljava/util/HashMap;

    #@18
    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 756
    monitor-exit v4

    #@1c
    move-object v2, v1

    #@1d
    .line 766
    .end local v0           #prefsFile:Ljava/io/File;
    .end local v1           #sp:Landroid/app/SharedPreferencesImpl;
    .local v2, sp:Ljava/lang/Object;
    :goto_1d
    return-object v2

    #@1e
    .line 758
    .end local v2           #sp:Ljava/lang/Object;
    .restart local v1       #sp:Landroid/app/SharedPreferencesImpl;
    :cond_1e
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_32

    #@1f
    .line 759
    and-int/lit8 v3, p2, 0x4

    #@21
    if-nez v3, :cond_2d

    #@23
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@26
    move-result-object v3

    #@27
    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@29
    const/16 v4, 0xb

    #@2b
    if-ge v3, v4, :cond_30

    #@2d
    .line 764
    :cond_2d
    invoke-virtual {v1}, Landroid/app/SharedPreferencesImpl;->startReloadIfChangedUnexpectedly()V

    #@30
    :cond_30
    move-object v2, v1

    #@31
    .line 766
    .restart local v2       #sp:Ljava/lang/Object;
    goto :goto_1d

    #@32
    .line 758
    .end local v1           #sp:Landroid/app/SharedPreferencesImpl;
    .end local v2           #sp:Ljava/lang/Object;
    :catchall_32
    move-exception v3

    #@33
    :try_start_33
    monitor-exit v4
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    #@34
    throw v3
.end method

.method public getSharedPrefsFile(Ljava/lang/String;)Ljava/io/File;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 744
    invoke-direct {p0}, Landroid/app/ContextImpl;->getPreferencesDir()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, ".xml"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v0, v1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1568
    sget-object v1, Landroid/app/ContextImpl;->SYSTEM_SERVICE_MAP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/ContextImpl$ServiceFetcher;

    #@8
    .line 1569
    .local v0, fetcher:Landroid/app/ContextImpl$ServiceFetcher;
    if-nez v0, :cond_c

    #@a
    const/4 v1, 0x0

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0, p0}, Landroid/app/ContextImpl$ServiceFetcher;->getService(Landroid/app/ContextImpl;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method public getTheme()Landroid/content/res/Resources$Theme;
    .registers 4

    #@0
    .prologue
    .line 696
    iget-object v0, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@2
    if-nez v0, :cond_26

    #@4
    .line 697
    iget v0, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@6
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@d
    move-result-object v1

    #@e
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@10
    invoke-static {v0, v1}, Landroid/content/res/Resources;->selectDefaultTheme(II)I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@16
    .line 699
    iget-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@18
    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@1e
    .line 700
    iget-object v0, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@20
    iget v1, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@22
    const/4 v2, 0x1

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    #@26
    .line 702
    :cond_26
    iget-object v0, p0, Landroid/app/ContextImpl;->mTheme:Landroid/content/res/Resources$Theme;

    #@28
    return-object v0
.end method

.method public getThemeResId()I
    .registers 2

    #@0
    .prologue
    .line 691
    iget v0, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@2
    return v0
.end method

.method public getUserId()I
    .registers 2

    #@0
    .prologue
    .line 1916
    iget-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@2
    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getWallpaper()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 984
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getWallpaperDesiredMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 999
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDesiredMinimumHeight()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getWallpaperDesiredMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 994
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getDesiredMinimumWidth()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    .registers 6
    .parameter "toPackage"
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 1669
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@6
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v0, v1, p1, p2, p3}, Landroid/app/IActivityManager;->grantUriPermission(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/net/Uri;I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 1674
    :goto_d
    return-void

    #@e
    .line 1672
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method

.method final init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V
    .registers 11
    .parameter "packageInfo"
    .parameter "activityToken"
    .parameter "mainThread"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1947
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@4
    move-result-object v6

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v2, p2

    #@8
    move-object v3, p3

    #@9
    move-object v5, v4

    #@a
    invoke-virtual/range {v0 .. v6}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;Landroid/content/res/Resources;Ljava/lang/String;Landroid/os/UserHandle;)V

    #@d
    .line 1948
    return-void
.end method

.method final init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;Landroid/content/res/Resources;Ljava/lang/String;Landroid/os/UserHandle;)V
    .registers 11
    .parameter "packageInfo"
    .parameter "activityToken"
    .parameter "mainThread"
    .parameter "container"
    .parameter "basePackageName"
    .parameter "user"

    #@0
    .prologue
    .line 1952
    iput-object p1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    .line 1953
    if-eqz p5, :cond_46

    #@4
    .end local p5
    :goto_4
    iput-object p5, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@6
    .line 1954
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@8
    invoke-virtual {v0, p3}, Landroid/app/LoadedApk;->getResources(Landroid/app/ActivityThread;)Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@e
    .line 1956
    iget-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@10
    if-eqz v0, :cond_38

    #@12
    if-eqz p4, :cond_38

    #@14
    invoke-virtual {p4}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@17
    move-result-object v0

    #@18
    iget v0, v0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@1a
    iget-object v1, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@1c
    invoke-virtual {v1}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@1f
    move-result-object v1

    #@20
    iget v1, v1, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    #@22
    cmpl-float v0, v0, v1

    #@24
    if-eqz v0, :cond_38

    #@26
    .line 1963
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@28
    invoke-virtual {v0}, Landroid/app/LoadedApk;->getResDir()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    const/4 v1, 0x0

    #@2d
    const/4 v2, 0x0

    #@2e
    invoke-virtual {p4}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {p3, v0, v1, v2, v3}, Landroid/app/ActivityThread;->getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@38
    .line 1967
    :cond_38
    iput-object p3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@3a
    .line 1968
    iput-object p2, p0, Landroid/app/ContextImpl;->mActivityToken:Landroid/os/IBinder;

    #@3c
    .line 1969
    new-instance v0, Landroid/app/ContextImpl$ApplicationContentResolver;

    #@3e
    invoke-direct {v0, p0, p3, p6}, Landroid/app/ContextImpl$ApplicationContentResolver;-><init>(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/os/UserHandle;)V

    #@41
    iput-object v0, p0, Landroid/app/ContextImpl;->mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

    #@43
    .line 1970
    iput-object p6, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@45
    .line 1971
    return-void

    #@46
    .line 1953
    .restart local p5
    :cond_46
    iget-object p5, p1, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@48
    goto :goto_4
.end method

.method final init(Landroid/content/res/Resources;Landroid/app/ActivityThread;Landroid/os/UserHandle;)V
    .registers 5
    .parameter "resources"
    .parameter "mainThread"
    .parameter "user"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1974
    iput-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@3
    .line 1975
    iput-object v0, p0, Landroid/app/ContextImpl;->mBasePackageName:Ljava/lang/String;

    #@5
    .line 1976
    iput-object p1, p0, Landroid/app/ContextImpl;->mResources:Landroid/content/res/Resources;

    #@7
    .line 1977
    iput-object p2, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@9
    .line 1978
    new-instance v0, Landroid/app/ContextImpl$ApplicationContentResolver;

    #@b
    invoke-direct {v0, p0, p2, p3}, Landroid/app/ContextImpl$ApplicationContentResolver;-><init>(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/os/UserHandle;)V

    #@e
    iput-object v0, p0, Landroid/app/ContextImpl;->mContentResolver:Landroid/app/ContextImpl$ApplicationContentResolver;

    #@10
    .line 1979
    iput-object p3, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@12
    .line 1980
    return-void
.end method

.method public isRestricted()Z
    .registers 2

    #@0
    .prologue
    .line 1887
    iget-boolean v0, p0, Landroid/app/ContextImpl;->mRestricted:Z

    #@2
    return v0
.end method

.method public openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    .registers 4
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 781
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getFilesDir()Ljava/io/File;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0, v1, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@7
    move-result-object v0

    #@8
    .line 782
    .local v0, f:Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    #@a
    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@d
    return-object v1
.end method

.method public openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    .registers 11
    .parameter "name"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 788
    const v5, 0x8000

    #@5
    and-int/2addr v5, p2

    #@6
    if-eqz v5, :cond_1f

    #@8
    const/4 v0, 0x1

    #@9
    .line 789
    .local v0, append:Z
    :goto_9
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getFilesDir()Ljava/io/File;

    #@c
    move-result-object v5

    #@d
    invoke-direct {p0, v5, p1}, Landroid/app/ContextImpl;->makeFilename(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    #@10
    move-result-object v1

    #@11
    .line 791
    .local v1, f:Ljava/io/File;
    :try_start_11
    new-instance v2, Ljava/io/FileOutputStream;

    #@13
    invoke-direct {v2, v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@16
    .line 792
    .local v2, fos:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    const/4 v6, 0x0

    #@1b
    invoke-static {v5, p2, v6}, Landroid/app/ContextImpl;->setFilePermissionsFromMode(Ljava/lang/String;II)V
    :try_end_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_1e} :catch_21

    #@1e
    .line 805
    :goto_1e
    return-object v2

    #@1f
    .end local v0           #append:Z
    .end local v1           #f:Ljava/io/File;
    .end local v2           #fos:Ljava/io/FileOutputStream;
    :cond_1f
    move v0, v4

    #@20
    .line 788
    goto :goto_9

    #@21
    .line 794
    .restart local v0       #append:Z
    .restart local v1       #f:Ljava/io/File;
    :catch_21
    move-exception v5

    #@22
    .line 797
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@25
    move-result-object v3

    #@26
    .line 798
    .local v3, parent:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    #@29
    .line 799
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    const/16 v6, 0x1f9

    #@2f
    invoke-static {v5, v6, v7, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    #@32
    .line 803
    new-instance v2, Ljava/io/FileOutputStream;

    #@34
    invoke-direct {v2, v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    #@37
    .line 804
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v5, p2, v4}, Landroid/app/ContextImpl;->setFilePermissionsFromMode(Ljava/lang/String;II)V

    #@3e
    goto :goto_1e
.end method

.method public openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 5
    .parameter "name"
    .parameter "mode"
    .parameter "factory"

    #@0
    .prologue
    .line 932
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/app/ContextImpl;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;Landroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;Landroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;
    .registers 10
    .parameter "name"
    .parameter "mode"
    .parameter "factory"
    .parameter "errorHandler"

    #@0
    .prologue
    .line 938
    const/4 v3, 0x1

    #@1
    invoke-direct {p0, p1, v3}, Landroid/app/ContextImpl;->validateFilePath(Ljava/lang/String;Z)Ljava/io/File;

    #@4
    move-result-object v1

    #@5
    .line 939
    .local v1, f:Ljava/io/File;
    const/high16 v2, 0x1000

    #@7
    .line 940
    .local v2, flags:I
    and-int/lit8 v3, p2, 0x8

    #@9
    if-eqz v3, :cond_e

    #@b
    .line 941
    const/high16 v3, 0x2000

    #@d
    or-int/2addr v2, v3

    #@e
    .line 943
    :cond_e
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-static {v3, p3, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;

    #@15
    move-result-object v0

    #@16
    .line 944
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-static {v3, p2, v4}, Landroid/app/ContextImpl;->setFilePermissionsFromMode(Ljava/lang/String;II)V

    #@1e
    .line 945
    return-object v0
.end method

.method public peekWallpaper()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 989
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->peekDrawable()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method final performFinalCleanup(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 1988
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p1, p2}, Landroid/app/LoadedApk;->removeContextRegistrations(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 1989
    return-void
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    .registers 4
    .parameter "receiver"
    .parameter "filter"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1379
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/app/ContextImpl;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 12
    .parameter "receiver"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 1385
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@3
    move-result v2

    #@4
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@7
    move-result-object v6

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v3, p2

    #@b
    move-object v4, p3

    #@c
    move-object v5, p4

    #@d
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->registerReceiverInternal(Landroid/content/BroadcastReceiver;ILandroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 13
    .parameter "receiver"
    .parameter "user"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 1392
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@3
    move-result v2

    #@4
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@7
    move-result-object v6

    #@8
    move-object v0, p0

    #@9
    move-object v1, p1

    #@a
    move-object v3, p3

    #@b
    move-object v4, p4

    #@c
    move-object v5, p5

    #@d
    invoke-direct/range {v0 .. v6}, Landroid/app/ContextImpl;->registerReceiverInternal(Landroid/content/BroadcastReceiver;ILandroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)Landroid/content/Intent;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public removeStickyBroadcast(Landroid/content/Intent;)V
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    .line 1304
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {p1, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 1305
    .local v1, resolvedType:Ljava/lang/String;
    if-eqz v1, :cond_17

    #@a
    .line 1306
    new-instance v0, Landroid/content/Intent;

    #@c
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@f
    .line 1307
    .end local p1
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@16
    move-object p1, v0

    #@17
    .line 1310
    .end local v0           #intent:Landroid/content/Intent;
    .restart local p1
    :cond_17
    const/4 v2, 0x0

    #@18
    :try_start_18
    invoke-virtual {p1, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@1b
    .line 1311
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1e
    move-result-object v2

    #@1f
    iget-object v3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@21
    invoke-virtual {v3}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@28
    move-result v4

    #@29
    invoke-interface {v2, v3, p1, v4}, Landroid/app/IActivityManager;->unbroadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;I)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_2c} :catch_2d

    #@2c
    .line 1315
    :goto_2c
    return-void

    #@2d
    .line 1313
    :catch_2d
    move-exception v2

    #@2e
    goto :goto_2c
.end method

.method public removeStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 8
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 1364
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {p1, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    .line 1365
    .local v1, resolvedType:Ljava/lang/String;
    if-eqz v1, :cond_17

    #@a
    .line 1366
    new-instance v0, Landroid/content/Intent;

    #@c
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@f
    .line 1367
    .end local p1
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@16
    move-object p1, v0

    #@17
    .line 1370
    .end local v0           #intent:Landroid/content/Intent;
    .restart local p1
    :cond_17
    const/4 v2, 0x0

    #@18
    :try_start_18
    invoke-virtual {p1, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@1b
    .line 1371
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1e
    move-result-object v2

    #@1f
    iget-object v3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@21
    invoke-virtual {v3}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@28
    move-result v4

    #@29
    invoke-interface {v2, v3, p1, v4}, Landroid/app/IActivityManager;->unbroadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;I)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_2c} :catch_2d

    #@2c
    .line 1375
    :goto_2c
    return-void

    #@2d
    .line 1373
    :catch_2d
    move-exception v2

    #@2e
    goto :goto_2c
.end method

.method public revokeUriPermission(Landroid/net/Uri;I)V
    .registers 5
    .parameter "uri"
    .parameter "modeFlags"

    #@0
    .prologue
    .line 1679
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@6
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@9
    move-result-object v1

    #@a
    invoke-interface {v0, v1, p1, p2}, Landroid/app/IActivityManager;->revokeUriPermission(Landroid/app/IApplicationThread;Landroid/net/Uri;I)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_d} :catch_e

    #@d
    .line 1684
    :goto_d
    return-void

    #@e
    .line 1682
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method

.method final scheduleFinalCleanup(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 1983
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Landroid/app/ActivityThread;->scheduleContextCleanup(Landroid/app/ContextImpl;Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 1984
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .registers 14
    .parameter "intent"

    #@0
    .prologue
    .line 1121
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1122
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 1124
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@c
    :try_start_c
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@f
    .line 1125
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@18
    move-result-object v1

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, -0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    const/4 v7, 0x0

    #@1d
    const/4 v8, 0x0

    #@1e
    const/4 v9, 0x0

    #@1f
    const/4 v10, 0x0

    #@20
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@23
    move-result v11

    #@24
    move-object v2, p1

    #@25
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_28} :catch_29

    #@28
    .line 1131
    :goto_28
    return-void

    #@29
    .line 1129
    :catch_29
    move-exception v0

    #@2a
    goto :goto_28
.end method

.method public sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 15
    .parameter "intent"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 1135
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1136
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 1138
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@c
    :try_start_c
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@f
    .line 1139
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@18
    move-result-object v1

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, -0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    const/4 v7, 0x0

    #@1d
    const/4 v9, 0x0

    #@1e
    const/4 v10, 0x0

    #@1f
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@22
    move-result v11

    #@23
    move-object v2, p1

    #@24
    move-object v8, p2

    #@25
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_28} :catch_29

    #@28
    .line 1145
    :goto_28
    return-void

    #@29
    .line 1143
    :catch_29
    move-exception v0

    #@2a
    goto :goto_28
.end method

.method public sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 15
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 1198
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1200
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    :try_start_9
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@c
    .line 1201
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@12
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@15
    move-result-object v1

    #@16
    const/4 v4, 0x0

    #@17
    const/4 v5, -0x1

    #@18
    const/4 v6, 0x0

    #@19
    const/4 v7, 0x0

    #@1a
    const/4 v8, 0x0

    #@1b
    const/4 v9, 0x0

    #@1c
    const/4 v10, 0x0

    #@1d
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@20
    move-result v11

    #@21
    move-object v2, p1

    #@22
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 1206
    :goto_25
    return-void

    #@26
    .line 1204
    :catch_26
    move-exception v0

    #@27
    goto :goto_25
.end method

.method public sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
    .registers 16
    .parameter "intent"
    .parameter "user"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 1211
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1213
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    :try_start_9
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@c
    .line 1214
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@12
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@15
    move-result-object v1

    #@16
    const/4 v4, 0x0

    #@17
    const/4 v5, -0x1

    #@18
    const/4 v6, 0x0

    #@19
    const/4 v7, 0x0

    #@1a
    const/4 v9, 0x0

    #@1b
    const/4 v10, 0x0

    #@1c
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@1f
    move-result v11

    #@20
    move-object v2, p1

    #@21
    move-object v8, p3

    #@22
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 1220
    :goto_25
    return-void

    #@26
    .line 1218
    :catch_26
    move-exception v0

    #@27
    goto :goto_25
.end method

.method public sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    .registers 15
    .parameter "intent"
    .parameter "receiverPermission"

    #@0
    .prologue
    .line 1150
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1151
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 1153
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@c
    :try_start_c
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@f
    .line 1154
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@18
    move-result-object v1

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, -0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    const/4 v7, 0x0

    #@1d
    const/4 v9, 0x1

    #@1e
    const/4 v10, 0x0

    #@1f
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@22
    move-result v11

    #@23
    move-object v2, p1

    #@24
    move-object v8, p2

    #@25
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_28} :catch_29

    #@28
    .line 1160
    :goto_28
    return-void

    #@29
    .line 1158
    :catch_29
    move-exception v0

    #@2a
    goto :goto_28
.end method

.method public sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 20
    .parameter "intent"
    .parameter "receiverPermission"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 1167
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1168
    const/4 v4, 0x0

    #@4
    .line 1169
    .local v4, rd:Landroid/content/IIntentReceiver;
    if-eqz p3, :cond_26

    #@6
    .line 1170
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@8
    if-eqz v0, :cond_4e

    #@a
    .line 1171
    if-nez p4, :cond_12

    #@c
    .line 1172
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@e
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@11
    move-result-object p4

    #@12
    .line 1174
    :cond_12
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@14
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@17
    move-result-object v2

    #@18
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@1a
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1d
    move-result-object v4

    #@1e
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    const/4 v5, 0x0

    #@1f
    move-object v1, p3

    #@20
    move-object/from16 v3, p4

    #@22
    invoke-virtual/range {v0 .. v5}, Landroid/app/LoadedApk;->getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;

    #@25
    move-result-object v4

    #@26
    .line 1185
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    :cond_26
    :goto_26
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    .line 1187
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@2f
    :try_start_2f
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@32
    .line 1188
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@35
    move-result-object v0

    #@36
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@38
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@3b
    move-result-object v1

    #@3c
    const/4 v9, 0x1

    #@3d
    const/4 v10, 0x0

    #@3e
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@41
    move-result v11

    #@42
    move-object v2, p1

    #@43
    move/from16 v5, p5

    #@45
    move-object/from16 v6, p6

    #@47
    move-object/from16 v7, p7

    #@49
    move-object v8, p2

    #@4a
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_4d
    .catch Landroid/os/RemoteException; {:try_start_2f .. :try_end_4d} :catch_69

    #@4d
    .line 1194
    :goto_4d
    return-void

    #@4e
    .line 1178
    .end local v3           #resolvedType:Ljava/lang/String;
    :cond_4e
    if-nez p4, :cond_56

    #@50
    .line 1179
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@52
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@55
    move-result-object p4

    #@56
    .line 1181
    :cond_56
    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@58
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@5b
    move-result-object v2

    #@5c
    const/4 v4, 0x0

    #@5d
    const/4 v5, 0x0

    #@5e
    move-object v1, p3

    #@5f
    move-object/from16 v3, p4

    #@61
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V

    #@64
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    invoke-virtual {v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@67
    move-result-object v4

    #@68
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    goto :goto_26

    #@69
    .line 1192
    .restart local v3       #resolvedType:Ljava/lang/String;
    :catch_69
    move-exception v0

    #@6a
    goto :goto_4d
.end method

.method public sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 21
    .parameter "intent"
    .parameter "user"
    .parameter "receiverPermission"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 1226
    const/4 v4, 0x0

    #@1
    .line 1227
    .local v4, rd:Landroid/content/IIntentReceiver;
    if-eqz p4, :cond_24

    #@3
    .line 1228
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@5
    if-eqz v0, :cond_4c

    #@7
    .line 1229
    if-nez p5, :cond_f

    #@9
    .line 1230
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@b
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@e
    move-result-object p5

    #@f
    .line 1232
    :cond_f
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@11
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@14
    move-result-object v2

    #@15
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1a
    move-result-object v4

    #@1b
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    const/4 v5, 0x0

    #@1c
    move-object/from16 v1, p4

    #@1e
    move-object/from16 v3, p5

    #@20
    invoke-virtual/range {v0 .. v5}, Landroid/app/LoadedApk;->getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;

    #@23
    move-result-object v4

    #@24
    .line 1243
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    :cond_24
    :goto_24
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    .line 1245
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@30
    .line 1246
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@33
    move-result-object v0

    #@34
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@36
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@39
    move-result-object v1

    #@3a
    const/4 v9, 0x1

    #@3b
    const/4 v10, 0x0

    #@3c
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@3f
    move-result v11

    #@40
    move-object v2, p1

    #@41
    move/from16 v5, p6

    #@43
    move-object/from16 v6, p7

    #@45
    move-object/from16 v7, p8

    #@47
    move-object v8, p3

    #@48
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_4b
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_4b} :catch_68

    #@4b
    .line 1252
    :goto_4b
    return-void

    #@4c
    .line 1236
    .end local v3           #resolvedType:Ljava/lang/String;
    :cond_4c
    if-nez p5, :cond_54

    #@4e
    .line 1237
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@50
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@53
    move-result-object p5

    #@54
    .line 1239
    :cond_54
    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@56
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@59
    move-result-object v2

    #@5a
    const/4 v4, 0x0

    #@5b
    const/4 v5, 0x0

    #@5c
    move-object/from16 v1, p4

    #@5e
    move-object/from16 v3, p5

    #@60
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V

    #@63
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    invoke-virtual {v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@66
    move-result-object v4

    #@67
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    goto :goto_24

    #@68
    .line 1250
    .restart local v3       #resolvedType:Ljava/lang/String;
    :catch_68
    move-exception v0

    #@69
    goto :goto_4b
.end method

.method public sendStickyBroadcast(Landroid/content/Intent;)V
    .registers 14
    .parameter "intent"

    #@0
    .prologue
    .line 1256
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1257
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 1259
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@c
    :try_start_c
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@f
    .line 1260
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@18
    move-result-object v1

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, -0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    const/4 v7, 0x0

    #@1d
    const/4 v8, 0x0

    #@1e
    const/4 v9, 0x0

    #@1f
    const/4 v10, 0x1

    #@20
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@23
    move-result v11

    #@24
    move-object v2, p1

    #@25
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_28} :catch_29

    #@28
    .line 1266
    :goto_28
    return-void

    #@29
    .line 1264
    :catch_29
    move-exception v0

    #@2a
    goto :goto_28
.end method

.method public sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 15
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 1319
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1321
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@9
    :try_start_9
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@c
    .line 1322
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@12
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@15
    move-result-object v1

    #@16
    const/4 v4, 0x0

    #@17
    const/4 v5, -0x1

    #@18
    const/4 v6, 0x0

    #@19
    const/4 v7, 0x0

    #@1a
    const/4 v8, 0x0

    #@1b
    const/4 v9, 0x0

    #@1c
    const/4 v10, 0x1

    #@1d
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@20
    move-result v11

    #@21
    move-object v2, p1

    #@22
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_25} :catch_26

    #@25
    .line 1327
    :goto_25
    return-void

    #@26
    .line 1325
    :catch_26
    move-exception v0

    #@27
    goto :goto_25
.end method

.method public sendStickyOrderedBroadcast(Landroid/content/Intent;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 19
    .parameter "intent"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 1273
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1274
    const/4 v4, 0x0

    #@4
    .line 1275
    .local v4, rd:Landroid/content/IIntentReceiver;
    if-eqz p2, :cond_25

    #@6
    .line 1276
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@8
    if-eqz v0, :cond_4d

    #@a
    .line 1277
    if-nez p3, :cond_12

    #@c
    .line 1278
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@e
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@11
    move-result-object p3

    #@12
    .line 1280
    :cond_12
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@14
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@17
    move-result-object v2

    #@18
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@1a
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1d
    move-result-object v4

    #@1e
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    const/4 v5, 0x0

    #@1f
    move-object v1, p2

    #@20
    move-object v3, p3

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/app/LoadedApk;->getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;

    #@24
    move-result-object v4

    #@25
    .line 1291
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    :cond_25
    :goto_25
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    .line 1293
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@31
    .line 1294
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@34
    move-result-object v0

    #@35
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@37
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@3a
    move-result-object v1

    #@3b
    const/4 v8, 0x0

    #@3c
    const/4 v9, 0x1

    #@3d
    const/4 v10, 0x1

    #@3e
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@41
    move-result v11

    #@42
    move-object v2, p1

    #@43
    move/from16 v5, p4

    #@45
    move-object/from16 v6, p5

    #@47
    move-object/from16 v7, p6

    #@49
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_4c} :catch_67

    #@4c
    .line 1300
    :goto_4c
    return-void

    #@4d
    .line 1284
    .end local v3           #resolvedType:Ljava/lang/String;
    :cond_4d
    if-nez p3, :cond_55

    #@4f
    .line 1285
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@51
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@54
    move-result-object p3

    #@55
    .line 1287
    :cond_55
    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@57
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@5a
    move-result-object v2

    #@5b
    const/4 v4, 0x0

    #@5c
    const/4 v5, 0x0

    #@5d
    move-object v1, p2

    #@5e
    move-object v3, p3

    #@5f
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V

    #@62
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    invoke-virtual {v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@65
    move-result-object v4

    #@66
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    goto :goto_25

    #@67
    .line 1298
    .restart local v3       #resolvedType:Ljava/lang/String;
    :catch_67
    move-exception v0

    #@68
    goto :goto_4c
.end method

.method public sendStickyOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
    .registers 20
    .parameter "intent"
    .parameter "user"
    .parameter "resultReceiver"
    .parameter "scheduler"
    .parameter "initialCode"
    .parameter "initialData"
    .parameter "initialExtras"

    #@0
    .prologue
    .line 1334
    const/4 v4, 0x0

    #@1
    .line 1335
    .local v4, rd:Landroid/content/IIntentReceiver;
    if-eqz p3, :cond_23

    #@3
    .line 1336
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@5
    if-eqz v0, :cond_4b

    #@7
    .line 1337
    if-nez p4, :cond_f

    #@9
    .line 1338
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@b
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@e
    move-result-object p4

    #@f
    .line 1340
    :cond_f
    iget-object v0, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@11
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@14
    move-result-object v2

    #@15
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1a
    move-result-object v4

    #@1b
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    const/4 v5, 0x0

    #@1c
    move-object v1, p3

    #@1d
    move-object/from16 v3, p4

    #@1f
    invoke-virtual/range {v0 .. v5}, Landroid/app/LoadedApk;->getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;

    #@22
    move-result-object v4

    #@23
    .line 1351
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    :cond_23
    :goto_23
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 1353
    .local v3, resolvedType:Ljava/lang/String;
    const/4 v0, 0x0

    #@2c
    :try_start_2c
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@2f
    .line 1354
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@32
    move-result-object v0

    #@33
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@35
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@38
    move-result-object v1

    #@39
    const/4 v8, 0x0

    #@3a
    const/4 v9, 0x1

    #@3b
    const/4 v10, 0x1

    #@3c
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@3f
    move-result v11

    #@40
    move-object v2, p1

    #@41
    move/from16 v5, p5

    #@43
    move-object/from16 v6, p6

    #@45
    move-object/from16 v7, p7

    #@47
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_4a
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_4a} :catch_66

    #@4a
    .line 1360
    :goto_4a
    return-void

    #@4b
    .line 1344
    .end local v3           #resolvedType:Ljava/lang/String;
    :cond_4b
    if-nez p4, :cond_53

    #@4d
    .line 1345
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@4f
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@52
    move-result-object p4

    #@53
    .line 1347
    :cond_53
    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@55
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@58
    move-result-object v2

    #@59
    const/4 v4, 0x0

    #@5a
    const/4 v5, 0x0

    #@5b
    move-object v1, p3

    #@5c
    move-object/from16 v3, p4

    #@5e
    invoke-direct/range {v0 .. v5}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V

    #@61
    .end local v4           #rd:Landroid/content/IIntentReceiver;
    invoke-virtual {v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@64
    move-result-object v4

    #@65
    .restart local v4       #rd:Landroid/content/IIntentReceiver;
    goto :goto_23

    #@66
    .line 1358
    .restart local v3       #resolvedType:Ljava/lang/String;
    :catch_66
    move-exception v0

    #@67
    goto :goto_4a
.end method

.method final setOuterContext(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 1999
    iput-object p1, p0, Landroid/app/ContextImpl;->mOuterContext:Landroid/content/Context;

    #@2
    .line 2000
    return-void
.end method

.method public setTheme(I)V
    .registers 2
    .parameter "resid"

    #@0
    .prologue
    .line 686
    iput p1, p0, Landroid/app/ContextImpl;->mThemeResource:I

    #@2
    .line 687
    return-void
.end method

.method public setWallpaper(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter "bitmap"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1004
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    #@7
    .line 1005
    return-void
.end method

.method public setWallpaper(Ljava/io/InputStream;)V
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1009
    invoke-direct {p0}, Landroid/app/ContextImpl;->getWallpaperManager()Landroid/app/WallpaperManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    #@7
    .line 1010
    return-void
.end method

.method public startActivities([Landroid/content/Intent;)V
    .registers 3
    .parameter "intents"

    #@0
    .prologue
    .line 1058
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1059
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/ContextImpl;->startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V

    #@7
    .line 1060
    return-void
.end method

.method public startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 10
    .parameter "intents"
    .parameter "options"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1078
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@4
    .line 1079
    const/4 v0, 0x0

    #@5
    aget-object v0, p1, v0

    #@7
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@a
    move-result v0

    #@b
    const/high16 v1, 0x1000

    #@d
    and-int/2addr v0, v1

    #@e
    if-nez v0, :cond_18

    #@10
    .line 1080
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@12
    const-string v1, "Calling startActivities() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag on first Intent. Is this really what you want?"

    #@14
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 1085
    :cond_18
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@1a
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@24
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@27
    move-result-object v2

    #@28
    move-object v4, v3

    #@29
    check-cast v4, Landroid/app/Activity;

    #@2b
    move-object v5, p1

    #@2c
    move-object v6, p2

    #@2d
    invoke-virtual/range {v0 .. v6}, Landroid/app/Instrumentation;->execStartActivities(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;)V

    #@30
    .line 1088
    return-void
.end method

.method public startActivitiesAsUser([Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 12
    .parameter "intents"
    .parameter "options"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1065
    const/4 v0, 0x0

    #@2
    aget-object v0, p1, v0

    #@4
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    #@7
    move-result v0

    #@8
    const/high16 v1, 0x1000

    #@a
    and-int/2addr v0, v1

    #@b
    if-nez v0, :cond_15

    #@d
    .line 1066
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@f
    const-string v1, "Calling startActivities() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag on first Intent. Is this really what you want?"

    #@11
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 1071
    :cond_15
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@21
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@24
    move-result-object v2

    #@25
    move-object v4, v3

    #@26
    check-cast v4, Landroid/app/Activity;

    #@28
    invoke-virtual {p3}, Landroid/os/UserHandle;->getIdentifier()I

    #@2b
    move-result v7

    #@2c
    move-object v5, p1

    #@2d
    move-object v6, p2

    #@2e
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivitiesAsUser(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;I)V

    #@31
    .line 1074
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 1019
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1020
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/ContextImpl;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    #@7
    .line 1021
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 11
    .parameter "intent"
    .parameter "options"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1031
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@4
    .line 1032
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    #@7
    move-result v0

    #@8
    const/high16 v1, 0x1000

    #@a
    and-int/2addr v0, v1

    #@b
    if-nez v0, :cond_15

    #@d
    .line 1033
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@f
    const-string v1, "Calling startActivity() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag. Is this really what you want?"

    #@11
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 1038
    :cond_15
    iget-object v0, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@17
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getInstrumentation()Landroid/app/Instrumentation;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@21
    invoke-virtual {v2}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@24
    move-result-object v2

    #@25
    move-object v4, v3

    #@26
    check-cast v4, Landroid/app/Activity;

    #@28
    const/4 v6, -0x1

    #@29
    move-object v5, p1

    #@2a
    move-object v7, p2

    #@2b
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;

    #@2e
    .line 1041
    return-void
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    .registers 16
    .parameter "intent"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 1047
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@6
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {p1, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    const/4 v4, 0x0

    #@13
    const/4 v5, 0x0

    #@14
    const/4 v6, 0x0

    #@15
    const/high16 v7, 0x1000

    #@17
    const/4 v8, 0x0

    #@18
    const/4 v9, 0x0

    #@19
    invoke-virtual {p3}, Landroid/os/UserHandle;->getIdentifier()I

    #@1c
    move-result v11

    #@1d
    move-object v2, p1

    #@1e
    move-object v10, p2

    #@1f
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->startActivityAsUser(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)I
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_22} :catch_23

    #@22
    .line 1054
    :goto_22
    return-void

    #@23
    .line 1052
    :catch_23
    move-exception v0

    #@24
    goto :goto_22
.end method

.method public startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .registers 4
    .parameter "intent"
    .parameter "user"

    #@0
    .prologue
    .line 1026
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0, p2}, Landroid/app/ContextImpl;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    #@4
    .line 1027
    return-void
.end method

.method public startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)Z
    .registers 12
    .parameter "className"
    .parameter "profileFile"
    .parameter "arguments"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1555
    if-eqz p3, :cond_7

    #@3
    .line 1556
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->setAllowFds(Z)Z

    #@7
    .line 1558
    :cond_7
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@a
    move-result-object v0

    #@b
    const/4 v3, 0x0

    #@c
    const/4 v5, 0x0

    #@d
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getUserId()I

    #@10
    move-result v6

    #@11
    move-object v1, p1

    #@12
    move-object v2, p2

    #@13
    move-object v4, p3

    #@14
    invoke-interface/range {v0 .. v6}, Landroid/app/IActivityManager;->startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;I)Z
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_17} :catch_19

    #@17
    move-result v0

    #@18
    .line 1563
    :goto_18
    return v0

    #@19
    .line 1560
    :catch_19
    move-exception v0

    #@1a
    move v0, v7

    #@1b
    .line 1563
    goto :goto_18
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    .registers 13
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 1094
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/ContextImpl;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V

    #@a
    .line 1095
    return-void
.end method

.method public startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V
    .registers 19
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "extraFlags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    #@0
    .prologue
    .line 1102
    const/4 v4, 0x0

    #@1
    .line 1103
    .local v4, resolvedType:Ljava/lang/String;
    if-eqz p2, :cond_f

    #@3
    .line 1104
    const/4 v0, 0x0

    #@4
    :try_start_4
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@7
    .line 1105
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p2, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    .line 1107
    :cond_f
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@12
    move-result-object v0

    #@13
    iget-object v1, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@15
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@18
    move-result-object v1

    #@19
    const/4 v5, 0x0

    #@1a
    const/4 v6, 0x0

    #@1b
    const/4 v7, 0x0

    #@1c
    move-object v2, p1

    #@1d
    move-object v3, p2

    #@1e
    move v8, p3

    #@1f
    move/from16 v9, p4

    #@21
    move-object/from16 v10, p6

    #@23
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->startActivityIntentSender(Landroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I

    #@26
    move-result v11

    #@27
    .line 1111
    .local v11, result:I
    const/4 v0, -0x6

    #@28
    if-ne v11, v0, :cond_32

    #@2a
    .line 1112
    new-instance v0, Landroid/content/IntentSender$SendIntentException;

    #@2c
    invoke-direct {v0}, Landroid/content/IntentSender$SendIntentException;-><init>()V

    #@2f
    throw v0

    #@30
    .line 1115
    .end local v11           #result:I
    :catch_30
    move-exception v0

    #@31
    .line 1117
    :goto_31
    return-void

    #@32
    .line 1114
    .restart local v11       #result:I
    :cond_32
    const/4 v0, 0x0

    #@33
    invoke-static {v11, v0}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_36} :catch_30

    #@36
    goto :goto_31
.end method

.method public startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 1441
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1442
    iget-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@5
    invoke-virtual {p0, p1, v0}, Landroid/app/ContextImpl;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    .registers 9
    .parameter "service"
    .parameter "user"

    #@0
    .prologue
    .line 1454
    const/4 v2, 0x0

    #@1
    :try_start_1
    invoke-virtual {p1, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@4
    .line 1455
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v2

    #@8
    iget-object v3, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@a
    invoke-virtual {v3}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {p1, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@19
    move-result v5

    #@1a
    invoke-interface {v2, v3, p1, v4, v5}, Landroid/app/IActivityManager;->startService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/ComponentName;

    #@1d
    move-result-object v0

    #@1e
    .line 1458
    .local v0, cn:Landroid/content/ComponentName;
    if-eqz v0, :cond_55

    #@20
    .line 1459
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    const-string v3, "!"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_56

    #@2c
    .line 1460
    new-instance v2, Ljava/lang/SecurityException;

    #@2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v4, "Not allowed to start service "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " without permission "

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@52
    throw v2

    #@53
    .line 1470
    .end local v0           #cn:Landroid/content/ComponentName;
    :catch_53
    move-exception v1

    #@54
    .line 1471
    .local v1, e:Landroid/os/RemoteException;
    const/4 v0, 0x0

    #@55
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_55
    return-object v0

    #@56
    .line 1463
    .restart local v0       #cn:Landroid/content/ComponentName;
    :cond_56
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    const-string v3, "!!"

    #@5c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v2

    #@60
    if-eqz v2, :cond_55

    #@62
    .line 1464
    new-instance v2, Ljava/lang/SecurityException;

    #@64
    new-instance v3, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v4, "Unable to start service "

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    const-string v4, ": "

    #@75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@88
    throw v2
    :try_end_89
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_89} :catch_53
.end method

.method public stopService(Landroid/content/Intent;)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 1447
    invoke-direct {p0}, Landroid/app/ContextImpl;->warnIfCallingFromSystemProcess()V

    #@3
    .line 1448
    iget-object v0, p0, Landroid/app/ContextImpl;->mUser:Landroid/os/UserHandle;

    #@5
    invoke-virtual {p0, p1, v0}, Landroid/app/ContextImpl;->stopServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public stopServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Z
    .registers 10
    .parameter "service"
    .parameter "user"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1478
    const/4 v3, 0x0

    #@2
    :try_start_2
    invoke-virtual {p1, v3}, Landroid/content/Intent;->setAllowFds(Z)V

    #@5
    .line 1479
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@8
    move-result-object v3

    #@9
    iget-object v4, p0, Landroid/app/ContextImpl;->mMainThread:Landroid/app/ActivityThread;

    #@b
    invoke-virtual {v4}, Landroid/app/ActivityThread;->getApplicationThread()Landroid/app/ActivityThread$ApplicationThread;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {p1, v5}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {p2}, Landroid/os/UserHandle;->getIdentifier()I

    #@1a
    move-result v6

    #@1b
    invoke-interface {v3, v4, p1, v5, v6}, Landroid/app/IActivityManager;->stopService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)I

    #@1e
    move-result v1

    #@1f
    .line 1482
    .local v1, res:I
    if-gez v1, :cond_3c

    #@21
    .line 1483
    new-instance v3, Ljava/lang/SecurityException;

    #@23
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v5, "Not allowed to stop service "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@39
    throw v3
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_3a} :catch_3a

    #@3a
    .line 1487
    .end local v1           #res:I
    :catch_3a
    move-exception v0

    #@3b
    .line 1488
    :cond_3b
    :goto_3b
    return v2

    #@3c
    .line 1486
    .restart local v1       #res:I
    :cond_3c
    if-eqz v1, :cond_3b

    #@3e
    const/4 v2, 0x1

    #@3f
    goto :goto_3b
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .registers 5
    .parameter "conn"

    #@0
    .prologue
    .line 1536
    if-nez p1, :cond_a

    #@2
    .line 1537
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "connection is null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 1539
    :cond_a
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@c
    if-eqz v1, :cond_20

    #@e
    .line 1540
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@10
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v1, v2, p1}, Landroid/app/LoadedApk;->forgetServiceDispatcher(Landroid/content/Context;Landroid/content/ServiceConnection;)Landroid/app/IServiceConnection;

    #@17
    move-result-object v0

    #@18
    .line 1543
    .local v0, sd:Landroid/app/IServiceConnection;
    :try_start_18
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1b
    move-result-object v1

    #@1c
    invoke-interface {v1, v0}, Landroid/app/IActivityManager;->unbindService(Landroid/app/IServiceConnection;)Z
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1f} :catch_28

    #@1f
    .line 1549
    :goto_1f
    return-void

    #@20
    .line 1547
    .end local v0           #sd:Landroid/app/IServiceConnection;
    :cond_20
    new-instance v1, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "Not supported in system context"

    #@24
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 1544
    .restart local v0       #sd:Landroid/app/IServiceConnection;
    :catch_28
    move-exception v1

    #@29
    goto :goto_1f
.end method

.method public unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .registers 5
    .parameter "receiver"

    #@0
    .prologue
    .line 1427
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@2
    if-eqz v1, :cond_16

    #@4
    .line 1428
    iget-object v1, p0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    #@6
    invoke-virtual {p0}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v1, v2, p1}, Landroid/app/LoadedApk;->forgetReceiverDispatcher(Landroid/content/Context;Landroid/content/BroadcastReceiver;)Landroid/content/IIntentReceiver;

    #@d
    move-result-object v0

    #@e
    .line 1431
    .local v0, rd:Landroid/content/IIntentReceiver;
    :try_start_e
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v1, v0}, Landroid/app/IActivityManager;->unregisterReceiver(Landroid/content/IIntentReceiver;)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_15} :catch_1e

    #@15
    .line 1437
    :goto_15
    return-void

    #@16
    .line 1435
    .end local v0           #rd:Landroid/content/IIntentReceiver;
    :cond_16
    new-instance v1, Ljava/lang/RuntimeException;

    #@18
    const-string v2, "Not supported in system context"

    #@1a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v1

    #@1e
    .line 1432
    .restart local v0       #rd:Landroid/content/IIntentReceiver;
    :catch_1e
    move-exception v1

    #@1f
    goto :goto_15
.end method
