.class public Landroid/app/DownloadManager$Query;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Query"
.end annotation


# static fields
.field public static final ORDER_ASCENDING:I = 0x1

.field public static final ORDER_DESCENDING:I = 0x2


# instance fields
.field private mIds:[J

.field private mOnlyIncludeVisibleInDownloadsUi:Z

.field private mOrderByColumn:Ljava/lang/String;

.field private mOrderDirection:I

.field private mStatusFlags:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 728
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 741
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    #@6
    .line 742
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@8
    .line 743
    const-string/jumbo v0, "lastmod"

    #@b
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    #@d
    .line 744
    const/4 v0, 0x2

    #@e
    iput v0, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    #@10
    .line 745
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    #@13
    return-void
.end method

.method private joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;
    .registers 8
    .parameter "joiner"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 859
    .local p2, parts:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 860
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    #@6
    .line 861
    .local v1, first:Z
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_20

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/String;

    #@16
    .line 862
    .local v3, part:Ljava/lang/String;
    if-nez v1, :cond_1b

    #@18
    .line 863
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 865
    :cond_1b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 866
    const/4 v1, 0x0

    #@1f
    goto :goto_a

    #@20
    .line 868
    .end local v3           #part:Ljava/lang/String;
    :cond_20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    return-object v4
.end method

.method private statusClause(Ljava/lang/String;I)Ljava/lang/String;
    .registers 5
    .parameter "operator"
    .parameter "value"

    #@0
    .prologue
    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "status"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    const-string v1, "\'"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, "\'"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    return-object v0
.end method


# virtual methods
.method public orderBy(Ljava/lang/String;I)Landroid/app/DownloadManager$Query;
    .registers 6
    .parameter "column"
    .parameter "direction"

    #@0
    .prologue
    .line 790
    const/4 v0, 0x1

    #@1
    if-eq p2, v0, :cond_1f

    #@3
    const/4 v0, 0x2

    #@4
    if-eq p2, v0, :cond_1f

    #@6
    .line 791
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Invalid direction: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 794
    :cond_1f
    const-string/jumbo v0, "last_modified_timestamp"

    #@22
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_30

    #@28
    .line 795
    const-string/jumbo v0, "lastmod"

    #@2b
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    #@2d
    .line 801
    :goto_2d
    iput p2, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    #@2f
    .line 802
    return-object p0

    #@30
    .line 796
    :cond_30
    const-string/jumbo v0, "total_size"

    #@33
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_3f

    #@39
    .line 797
    const-string/jumbo v0, "total_bytes"

    #@3c
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    #@3e
    goto :goto_2d

    #@3f
    .line 799
    :cond_3f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@41
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, "Cannot order by "

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0
.end method

.method runQuery(Landroid/content/ContentResolver;[Ljava/lang/String;Landroid/net/Uri;)Landroid/database/Cursor;
    .registers 14
    .parameter "resolver"
    .parameter "projection"
    .parameter "baseUri"

    #@0
    .prologue
    .line 811
    move-object v1, p3

    #@1
    .line 812
    .local v1, uri:Landroid/net/Uri;
    new-instance v8, Ljava/util/ArrayList;

    #@3
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 813
    .local v8, selectionParts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    #@7
    .line 815
    .local v4, selectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    #@9
    if-eqz v0, :cond_1a

    #@b
    .line 816
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    #@d
    invoke-static {v0}, Landroid/app/DownloadManager;->getWhereClauseForIds([J)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@14
    .line 817
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    #@16
    invoke-static {v0}, Landroid/app/DownloadManager;->getWhereArgsForIds([J)[Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    .line 820
    :cond_1a
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@1c
    if-eqz v0, :cond_e1

    #@1e
    .line 821
    new-instance v7, Ljava/util/ArrayList;

    #@20
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@23
    .line 822
    .local v7, parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@28
    move-result v0

    #@29
    and-int/lit8 v0, v0, 0x1

    #@2b
    if-eqz v0, :cond_38

    #@2d
    .line 823
    const-string v0, "="

    #@2f
    const/16 v2, 0xbe

    #@31
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@34
    move-result-object v0

    #@35
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@38
    .line 825
    :cond_38
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@3a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@3d
    move-result v0

    #@3e
    and-int/lit8 v0, v0, 0x2

    #@40
    if-eqz v0, :cond_4d

    #@42
    .line 826
    const-string v0, "="

    #@44
    const/16 v2, 0xc0

    #@46
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@4d
    .line 828
    :cond_4d
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@4f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@52
    move-result v0

    #@53
    and-int/lit8 v0, v0, 0x4

    #@55
    if-eqz v0, :cond_83

    #@57
    .line 829
    const-string v0, "="

    #@59
    const/16 v2, 0xc1

    #@5b
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@62
    .line 830
    const-string v0, "="

    #@64
    const/16 v2, 0xc2

    #@66
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@6d
    .line 831
    const-string v0, "="

    #@6f
    const/16 v2, 0xc3

    #@71
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@78
    .line 832
    const-string v0, "="

    #@7a
    const/16 v2, 0xc4

    #@7c
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@83
    .line 834
    :cond_83
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@85
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@88
    move-result v0

    #@89
    and-int/lit8 v0, v0, 0x8

    #@8b
    if-eqz v0, :cond_98

    #@8d
    .line 835
    const-string v0, "="

    #@8f
    const/16 v2, 0xc8

    #@91
    invoke-direct {p0, v0, v2}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@98
    .line 837
    :cond_98
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@9a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@9d
    move-result v0

    #@9e
    and-int/lit8 v0, v0, 0x10

    #@a0
    if-eqz v0, :cond_d8

    #@a2
    .line 838
    new-instance v0, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v2, "("

    #@a9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    const-string v2, ">="

    #@af
    const/16 v9, 0x190

    #@b1
    invoke-direct {p0, v2, v9}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@b4
    move-result-object v2

    #@b5
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v0

    #@b9
    const-string v2, " AND "

    #@bb
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v0

    #@bf
    const-string v2, "<"

    #@c1
    const/16 v9, 0x258

    #@c3
    invoke-direct {p0, v2, v9}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    #@c6
    move-result-object v2

    #@c7
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v0

    #@cb
    const-string v2, ")"

    #@cd
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v0

    #@d1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v0

    #@d5
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d8
    .line 841
    :cond_d8
    const-string v0, " OR "

    #@da
    invoke-direct {p0, v0, v7}, Landroid/app/DownloadManager$Query;->joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    #@dd
    move-result-object v0

    #@de
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@e1
    .line 844
    .end local v7           #parts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_e1
    iget-boolean v0, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    #@e3
    if-eqz v0, :cond_eb

    #@e5
    .line 845
    const-string/jumbo v0, "is_visible_in_downloads_ui != \'0\'"

    #@e8
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@eb
    .line 849
    :cond_eb
    const-string v0, "deleted != \'1\'"

    #@ed
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f0
    .line 851
    const-string v0, " AND "

    #@f2
    invoke-direct {p0, v0, v8}, Landroid/app/DownloadManager$Query;->joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    #@f5
    move-result-object v3

    #@f6
    .line 852
    .local v3, selection:Ljava/lang/String;
    iget v0, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    #@f8
    const/4 v2, 0x1

    #@f9
    if-ne v0, v2, :cond_11d

    #@fb
    const-string v6, "ASC"

    #@fd
    .line 853
    .local v6, orderDirection:Ljava/lang/String;
    :goto_fd
    new-instance v0, Ljava/lang/StringBuilder;

    #@ff
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@102
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    #@104
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v0

    #@108
    const-string v2, " "

    #@10a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v0

    #@10e
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v0

    #@112
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    .local v5, orderBy:Ljava/lang/String;
    move-object v0, p1

    #@117
    move-object v2, p2

    #@118
    .line 855
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@11b
    move-result-object v0

    #@11c
    return-object v0

    #@11d
    .line 852
    .end local v5           #orderBy:Ljava/lang/String;
    .end local v6           #orderDirection:Ljava/lang/String;
    :cond_11d
    const-string v6, "DESC"

    #@11f
    goto :goto_fd
.end method

.method public varargs setFilterById([J)Landroid/app/DownloadManager$Query;
    .registers 2
    .parameter "ids"

    #@0
    .prologue
    .line 752
    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    #@2
    .line 753
    return-object p0
.end method

.method public setFilterByStatus(I)Landroid/app/DownloadManager$Query;
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 762
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    #@6
    .line 763
    return-object p0
.end method

.method public setOnlyIncludeVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Query;
    .registers 2
    .parameter "value"

    #@0
    .prologue
    .line 775
    iput-boolean p1, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    #@2
    .line 776
    return-object p0
.end method
