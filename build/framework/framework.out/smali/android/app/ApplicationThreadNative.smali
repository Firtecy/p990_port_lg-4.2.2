.class public abstract Landroid/app/ApplicationThreadNative;
.super Landroid/os/Binder;
.source "ApplicationThreadNative.java"

# interfaces
.implements Landroid/app/IApplicationThread;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 64
    const-string v0, "android.app.IApplicationThread"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/ApplicationThreadNative;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 65
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 51
    if-nez p0, :cond_4

    #@2
    .line 52
    const/4 v0, 0x0

    #@3
    .line 60
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 54
    :cond_4
    const-string v1, "android.app.IApplicationThread"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/IApplicationThread;

    #@c
    .line 56
    .local v0, in:Landroid/app/IApplicationThread;
    if-nez v0, :cond_3

    #@e
    .line 60
    new-instance v0, Landroid/app/ApplicationThreadProxy;

    #@10
    .end local v0           #in:Landroid/app/IApplicationThread;
    invoke-direct {v0, p0}, Landroid/app/ApplicationThreadProxy;-><init>(Landroid/os/IBinder;)V

    #@13
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 597
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 106
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    packed-switch p1, :pswitch_data_75c

    #@3
    .line 592
    :pswitch_3
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v5

    #@7
    :goto_7
    return v5

    #@8
    .line 73
    :pswitch_8
    const-string v5, "android.app.IApplicationThread"

    #@a
    move-object/from16 v0, p2

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f
    .line 74
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@12
    move-result-object v7

    #@13
    .line 75
    .local v7, b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v5

    #@17
    if-eqz v5, :cond_34

    #@19
    const/16 v79, 0x1

    #@1b
    .line 76
    .local v79, finished:Z
    :goto_1b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_37

    #@21
    const/16 v100, 0x1

    #@23
    .line 77
    .local v100, userLeaving:Z
    :goto_23
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@26
    move-result v24

    #@27
    .line 78
    .local v24, configChanges:I
    move-object/from16 v0, p0

    #@29
    move/from16 v1, v79

    #@2b
    move/from16 v2, v100

    #@2d
    move/from16 v3, v24

    #@2f
    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/app/ApplicationThreadNative;->schedulePauseActivity(Landroid/os/IBinder;ZZI)V

    #@32
    .line 79
    const/4 v5, 0x1

    #@33
    goto :goto_7

    #@34
    .line 75
    .end local v24           #configChanges:I
    .end local v79           #finished:Z
    .end local v100           #userLeaving:Z
    :cond_34
    const/16 v79, 0x0

    #@36
    goto :goto_1b

    #@37
    .line 76
    .restart local v79       #finished:Z
    :cond_37
    const/16 v100, 0x0

    #@39
    goto :goto_23

    #@3a
    .line 84
    .end local v7           #b:Landroid/os/IBinder;
    .end local v79           #finished:Z
    :pswitch_3a
    const-string v5, "android.app.IApplicationThread"

    #@3c
    move-object/from16 v0, p2

    #@3e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 85
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@44
    move-result-object v7

    #@45
    .line 86
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@48
    move-result v5

    #@49
    if-eqz v5, :cond_5c

    #@4b
    const/16 v97, 0x1

    #@4d
    .line 87
    .local v97, show:Z
    :goto_4d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v24

    #@51
    .line 88
    .restart local v24       #configChanges:I
    move-object/from16 v0, p0

    #@53
    move/from16 v1, v97

    #@55
    move/from16 v2, v24

    #@57
    invoke-virtual {v0, v7, v1, v2}, Landroid/app/ApplicationThreadNative;->scheduleStopActivity(Landroid/os/IBinder;ZI)V

    #@5a
    .line 89
    const/4 v5, 0x1

    #@5b
    goto :goto_7

    #@5c
    .line 86
    .end local v24           #configChanges:I
    .end local v97           #show:Z
    :cond_5c
    const/16 v97, 0x0

    #@5e
    goto :goto_4d

    #@5f
    .line 94
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_5f
    const-string v5, "android.app.IApplicationThread"

    #@61
    move-object/from16 v0, p2

    #@63
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66
    .line 95
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@69
    move-result-object v7

    #@6a
    .line 96
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v5

    #@6e
    if-eqz v5, :cond_7b

    #@70
    const/16 v97, 0x1

    #@72
    .line 97
    .restart local v97       #show:Z
    :goto_72
    move-object/from16 v0, p0

    #@74
    move/from16 v1, v97

    #@76
    invoke-virtual {v0, v7, v1}, Landroid/app/ApplicationThreadNative;->scheduleWindowVisibility(Landroid/os/IBinder;Z)V

    #@79
    .line 98
    const/4 v5, 0x1

    #@7a
    goto :goto_7

    #@7b
    .line 96
    .end local v97           #show:Z
    :cond_7b
    const/16 v97, 0x0

    #@7d
    goto :goto_72

    #@7e
    .line 103
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_7e
    const-string v5, "android.app.IApplicationThread"

    #@80
    move-object/from16 v0, p2

    #@82
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 104
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@88
    move-result-object v7

    #@89
    .line 105
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8c
    move-result v5

    #@8d
    if-eqz v5, :cond_9b

    #@8f
    const/16 v98, 0x1

    #@91
    .line 106
    .local v98, sleeping:Z
    :goto_91
    move-object/from16 v0, p0

    #@93
    move/from16 v1, v98

    #@95
    invoke-virtual {v0, v7, v1}, Landroid/app/ApplicationThreadNative;->scheduleSleeping(Landroid/os/IBinder;Z)V

    #@98
    .line 107
    const/4 v5, 0x1

    #@99
    goto/16 :goto_7

    #@9b
    .line 105
    .end local v98           #sleeping:Z
    :cond_9b
    const/16 v98, 0x0

    #@9d
    goto :goto_91

    #@9e
    .line 112
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_9e
    const-string v5, "android.app.IApplicationThread"

    #@a0
    move-object/from16 v0, p2

    #@a2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a5
    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a8
    move-result-object v7

    #@a9
    .line 114
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ac
    move-result v5

    #@ad
    if-eqz v5, :cond_bb

    #@af
    const/16 v16, 0x1

    #@b1
    .line 115
    .local v16, isForward:Z
    :goto_b1
    move-object/from16 v0, p0

    #@b3
    move/from16 v1, v16

    #@b5
    invoke-virtual {v0, v7, v1}, Landroid/app/ApplicationThreadNative;->scheduleResumeActivity(Landroid/os/IBinder;Z)V

    #@b8
    .line 116
    const/4 v5, 0x1

    #@b9
    goto/16 :goto_7

    #@bb
    .line 114
    .end local v16           #isForward:Z
    :cond_bb
    const/16 v16, 0x0

    #@bd
    goto :goto_b1

    #@be
    .line 121
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_be
    const-string v5, "android.app.IApplicationThread"

    #@c0
    move-object/from16 v0, p2

    #@c2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c5
    .line 122
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c8
    move-result-object v7

    #@c9
    .line 123
    .restart local v7       #b:Landroid/os/IBinder;
    sget-object v5, Landroid/app/ResultInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@d0
    move-result-object v13

    #@d1
    .line 124
    .local v13, ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    move-object/from16 v0, p0

    #@d3
    invoke-virtual {v0, v7, v13}, Landroid/app/ApplicationThreadNative;->scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V

    #@d6
    .line 125
    const/4 v5, 0x1

    #@d7
    goto/16 :goto_7

    #@d9
    .line 130
    .end local v7           #b:Landroid/os/IBinder;
    .end local v13           #ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    :pswitch_d9
    const-string v5, "android.app.IApplicationThread"

    #@db
    move-object/from16 v0, p2

    #@dd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e0
    .line 131
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e2
    move-object/from16 v0, p2

    #@e4
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7
    move-result-object v6

    #@e8
    check-cast v6, Landroid/content/Intent;

    #@ea
    .line 132
    .local v6, intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ed
    move-result-object v7

    #@ee
    .line 133
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v8

    #@f2
    .line 134
    .local v8, ident:I
    sget-object v5, Landroid/content/pm/ActivityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f4
    move-object/from16 v0, p2

    #@f6
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f9
    move-result-object v9

    #@fa
    check-cast v9, Landroid/content/pm/ActivityInfo;

    #@fc
    .line 135
    .local v9, info:Landroid/content/pm/ActivityInfo;
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@fe
    move-object/from16 v0, p2

    #@100
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@103
    move-result-object v10

    #@104
    check-cast v10, Landroid/content/res/Configuration;

    #@106
    .line 136
    .local v10, curConfig:Landroid/content/res/Configuration;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@108
    move-object/from16 v0, p2

    #@10a
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10d
    move-result-object v11

    #@10e
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@110
    .line 137
    .local v11, compatInfo:Landroid/content/res/CompatibilityInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@113
    move-result-object v12

    #@114
    .line 138
    .local v12, state:Landroid/os/Bundle;
    sget-object v5, Landroid/app/ResultInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@116
    move-object/from16 v0, p2

    #@118
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@11b
    move-result-object v13

    #@11c
    .line 139
    .restart local v13       #ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11e
    move-object/from16 v0, p2

    #@120
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@123
    move-result-object v14

    #@124
    .line 140
    .local v14, pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@127
    move-result v5

    #@128
    if-eqz v5, :cond_151

    #@12a
    const/4 v15, 0x1

    #@12b
    .line 141
    .local v15, notResumed:Z
    :goto_12b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v5

    #@12f
    if-eqz v5, :cond_153

    #@131
    const/16 v16, 0x1

    #@133
    .line 142
    .restart local v16       #isForward:Z
    :goto_133
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@136
    move-result-object v17

    #@137
    .line 143
    .local v17, profileName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13a
    move-result v5

    #@13b
    if-eqz v5, :cond_156

    #@13d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@140
    move-result-object v18

    #@141
    .line 145
    .local v18, profileFd:Landroid/os/ParcelFileDescriptor;
    :goto_141
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@144
    move-result v5

    #@145
    if-eqz v5, :cond_159

    #@147
    const/16 v19, 0x1

    #@149
    .local v19, autoStopProfiler:Z
    :goto_149
    move-object/from16 v5, p0

    #@14b
    .line 146
    invoke-virtual/range {v5 .. v19}, Landroid/app/ApplicationThreadNative;->scheduleLaunchActivity(Landroid/content/Intent;Landroid/os/IBinder;ILandroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Landroid/os/Bundle;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Z)V

    #@14e
    .line 148
    const/4 v5, 0x1

    #@14f
    goto/16 :goto_7

    #@151
    .line 140
    .end local v15           #notResumed:Z
    .end local v16           #isForward:Z
    .end local v17           #profileName:Ljava/lang/String;
    .end local v18           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v19           #autoStopProfiler:Z
    :cond_151
    const/4 v15, 0x0

    #@152
    goto :goto_12b

    #@153
    .line 141
    .restart local v15       #notResumed:Z
    :cond_153
    const/16 v16, 0x0

    #@155
    goto :goto_133

    #@156
    .line 143
    .restart local v16       #isForward:Z
    .restart local v17       #profileName:Ljava/lang/String;
    :cond_156
    const/16 v18, 0x0

    #@158
    goto :goto_141

    #@159
    .line 145
    .restart local v18       #profileFd:Landroid/os/ParcelFileDescriptor;
    :cond_159
    const/16 v19, 0x0

    #@15b
    goto :goto_149

    #@15c
    .line 153
    .end local v6           #intent:Landroid/content/Intent;
    .end local v7           #b:Landroid/os/IBinder;
    .end local v8           #ident:I
    .end local v9           #info:Landroid/content/pm/ActivityInfo;
    .end local v10           #curConfig:Landroid/content/res/Configuration;
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v12           #state:Landroid/os/Bundle;
    .end local v13           #ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .end local v14           #pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .end local v15           #notResumed:Z
    .end local v16           #isForward:Z
    .end local v17           #profileName:Ljava/lang/String;
    .end local v18           #profileFd:Landroid/os/ParcelFileDescriptor;
    :pswitch_15c
    const-string v5, "android.app.IApplicationThread"

    #@15e
    move-object/from16 v0, p2

    #@160
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@163
    .line 154
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@166
    move-result-object v7

    #@167
    .line 155
    .restart local v7       #b:Landroid/os/IBinder;
    sget-object v5, Landroid/app/ResultInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@169
    move-object/from16 v0, p2

    #@16b
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@16e
    move-result-object v13

    #@16f
    .line 156
    .restart local v13       #ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@171
    move-object/from16 v0, p2

    #@173
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@176
    move-result-object v14

    #@177
    .line 157
    .restart local v14       #pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17a
    move-result v24

    #@17b
    .line 158
    .restart local v24       #configChanges:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@17e
    move-result v5

    #@17f
    if-eqz v5, :cond_1a4

    #@181
    const/4 v15, 0x1

    #@182
    .line 159
    .restart local v15       #notResumed:Z
    :goto_182
    const/16 v26, 0x0

    #@184
    .line 160
    .local v26, config:Landroid/content/res/Configuration;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@187
    move-result v5

    #@188
    if-eqz v5, :cond_194

    #@18a
    .line 161
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18c
    move-object/from16 v0, p2

    #@18e
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@191
    move-result-object v26

    #@192
    .end local v26           #config:Landroid/content/res/Configuration;
    check-cast v26, Landroid/content/res/Configuration;

    #@194
    .restart local v26       #config:Landroid/content/res/Configuration;
    :cond_194
    move-object/from16 v20, p0

    #@196
    move-object/from16 v21, v7

    #@198
    move-object/from16 v22, v13

    #@19a
    move-object/from16 v23, v14

    #@19c
    move/from16 v25, v15

    #@19e
    .line 163
    invoke-virtual/range {v20 .. v26}, Landroid/app/ApplicationThreadNative;->scheduleRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;)V

    #@1a1
    .line 164
    const/4 v5, 0x1

    #@1a2
    goto/16 :goto_7

    #@1a4
    .line 158
    .end local v15           #notResumed:Z
    .end local v26           #config:Landroid/content/res/Configuration;
    :cond_1a4
    const/4 v15, 0x0

    #@1a5
    goto :goto_182

    #@1a6
    .line 169
    .end local v7           #b:Landroid/os/IBinder;
    .end local v13           #ri:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .end local v14           #pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    .end local v24           #configChanges:I
    :pswitch_1a6
    const-string v5, "android.app.IApplicationThread"

    #@1a8
    move-object/from16 v0, p2

    #@1aa
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ad
    .line 170
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1af
    move-object/from16 v0, p2

    #@1b1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@1b4
    move-result-object v14

    #@1b5
    .line 171
    .restart local v14       #pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b8
    move-result-object v7

    #@1b9
    .line 172
    .restart local v7       #b:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@1bb
    invoke-virtual {v0, v14, v7}, Landroid/app/ApplicationThreadNative;->scheduleNewIntent(Ljava/util/List;Landroid/os/IBinder;)V

    #@1be
    .line 173
    const/4 v5, 0x1

    #@1bf
    goto/16 :goto_7

    #@1c1
    .line 178
    .end local v7           #b:Landroid/os/IBinder;
    .end local v14           #pi:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    :pswitch_1c1
    const-string v5, "android.app.IApplicationThread"

    #@1c3
    move-object/from16 v0, p2

    #@1c5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c8
    .line 179
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1cb
    move-result-object v7

    #@1cc
    .line 180
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1cf
    move-result v5

    #@1d0
    if-eqz v5, :cond_1e4

    #@1d2
    const/16 v80, 0x1

    #@1d4
    .line 181
    .local v80, finishing:Z
    :goto_1d4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1d7
    move-result v24

    #@1d8
    .line 182
    .restart local v24       #configChanges:I
    move-object/from16 v0, p0

    #@1da
    move/from16 v1, v80

    #@1dc
    move/from16 v2, v24

    #@1de
    invoke-virtual {v0, v7, v1, v2}, Landroid/app/ApplicationThreadNative;->scheduleDestroyActivity(Landroid/os/IBinder;ZI)V

    #@1e1
    .line 183
    const/4 v5, 0x1

    #@1e2
    goto/16 :goto_7

    #@1e4
    .line 180
    .end local v24           #configChanges:I
    .end local v80           #finishing:Z
    :cond_1e4
    const/16 v80, 0x0

    #@1e6
    goto :goto_1d4

    #@1e7
    .line 188
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_1e7
    const-string v5, "android.app.IApplicationThread"

    #@1e9
    move-object/from16 v0, p2

    #@1eb
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ee
    .line 189
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f0
    move-object/from16 v0, p2

    #@1f2
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f5
    move-result-object v6

    #@1f6
    check-cast v6, Landroid/content/Intent;

    #@1f8
    .line 190
    .restart local v6       #intent:Landroid/content/Intent;
    sget-object v5, Landroid/content/pm/ActivityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1fa
    move-object/from16 v0, p2

    #@1fc
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1ff
    move-result-object v9

    #@200
    check-cast v9, Landroid/content/pm/ActivityInfo;

    #@202
    .line 191
    .restart local v9       #info:Landroid/content/pm/ActivityInfo;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@204
    move-object/from16 v0, p2

    #@206
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@209
    move-result-object v11

    #@20a
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@20c
    .line 192
    .restart local v11       #compatInfo:Landroid/content/res/CompatibilityInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@20f
    move-result v31

    #@210
    .line 193
    .local v31, resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@213
    move-result-object v32

    #@214
    .line 194
    .local v32, resultData:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@217
    move-result-object v33

    #@218
    .line 195
    .local v33, resultExtras:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@21b
    move-result v5

    #@21c
    if-eqz v5, :cond_232

    #@21e
    const/16 v34, 0x1

    #@220
    .line 196
    .local v34, sync:Z
    :goto_220
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@223
    move-result v35

    #@224
    .local v35, sendingUser:I
    move-object/from16 v27, p0

    #@226
    move-object/from16 v28, v6

    #@228
    move-object/from16 v29, v9

    #@22a
    move-object/from16 v30, v11

    #@22c
    .line 197
    invoke-virtual/range {v27 .. v35}, Landroid/app/ApplicationThreadNative;->scheduleReceiver(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/content/res/CompatibilityInfo;ILjava/lang/String;Landroid/os/Bundle;ZI)V

    #@22f
    .line 199
    const/4 v5, 0x1

    #@230
    goto/16 :goto_7

    #@232
    .line 195
    .end local v34           #sync:Z
    .end local v35           #sendingUser:I
    :cond_232
    const/16 v34, 0x0

    #@234
    goto :goto_220

    #@235
    .line 203
    .end local v6           #intent:Landroid/content/Intent;
    .end local v9           #info:Landroid/content/pm/ActivityInfo;
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v31           #resultCode:I
    .end local v32           #resultData:Ljava/lang/String;
    .end local v33           #resultExtras:Landroid/os/Bundle;
    :pswitch_235
    const-string v5, "android.app.IApplicationThread"

    #@237
    move-object/from16 v0, p2

    #@239
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@23c
    .line 204
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@23f
    move-result-object v37

    #@240
    .line 205
    .local v37, token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/pm/ServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@242
    move-object/from16 v0, p2

    #@244
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@247
    move-result-object v9

    #@248
    check-cast v9, Landroid/content/pm/ServiceInfo;

    #@24a
    .line 206
    .local v9, info:Landroid/content/pm/ServiceInfo;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24c
    move-object/from16 v0, p2

    #@24e
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@251
    move-result-object v11

    #@252
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@254
    .line 207
    .restart local v11       #compatInfo:Landroid/content/res/CompatibilityInfo;
    move-object/from16 v0, p0

    #@256
    move-object/from16 v1, v37

    #@258
    invoke-virtual {v0, v1, v9, v11}, Landroid/app/ApplicationThreadNative;->scheduleCreateService(Landroid/os/IBinder;Landroid/content/pm/ServiceInfo;Landroid/content/res/CompatibilityInfo;)V

    #@25b
    .line 208
    const/4 v5, 0x1

    #@25c
    goto/16 :goto_7

    #@25e
    .line 212
    .end local v9           #info:Landroid/content/pm/ServiceInfo;
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v37           #token:Landroid/os/IBinder;
    :pswitch_25e
    const-string v5, "android.app.IApplicationThread"

    #@260
    move-object/from16 v0, p2

    #@262
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@265
    .line 213
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@268
    move-result-object v37

    #@269
    .line 214
    .restart local v37       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@26b
    move-object/from16 v0, p2

    #@26d
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@270
    move-result-object v6

    #@271
    check-cast v6, Landroid/content/Intent;

    #@273
    .line 215
    .restart local v6       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@276
    move-result v5

    #@277
    if-eqz v5, :cond_287

    #@279
    const/16 v94, 0x1

    #@27b
    .line 216
    .local v94, rebind:Z
    :goto_27b
    move-object/from16 v0, p0

    #@27d
    move-object/from16 v1, v37

    #@27f
    move/from16 v2, v94

    #@281
    invoke-virtual {v0, v1, v6, v2}, Landroid/app/ApplicationThreadNative;->scheduleBindService(Landroid/os/IBinder;Landroid/content/Intent;Z)V

    #@284
    .line 217
    const/4 v5, 0x1

    #@285
    goto/16 :goto_7

    #@287
    .line 215
    .end local v94           #rebind:Z
    :cond_287
    const/16 v94, 0x0

    #@289
    goto :goto_27b

    #@28a
    .line 221
    .end local v6           #intent:Landroid/content/Intent;
    .end local v37           #token:Landroid/os/IBinder;
    :pswitch_28a
    const-string v5, "android.app.IApplicationThread"

    #@28c
    move-object/from16 v0, p2

    #@28e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@291
    .line 222
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@294
    move-result-object v37

    #@295
    .line 223
    .restart local v37       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@297
    move-object/from16 v0, p2

    #@299
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@29c
    move-result-object v6

    #@29d
    check-cast v6, Landroid/content/Intent;

    #@29f
    .line 224
    .restart local v6       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@2a1
    move-object/from16 v1, v37

    #@2a3
    invoke-virtual {v0, v1, v6}, Landroid/app/ApplicationThreadNative;->scheduleUnbindService(Landroid/os/IBinder;Landroid/content/Intent;)V

    #@2a6
    .line 225
    const/4 v5, 0x1

    #@2a7
    goto/16 :goto_7

    #@2a9
    .line 230
    .end local v6           #intent:Landroid/content/Intent;
    .end local v37           #token:Landroid/os/IBinder;
    :pswitch_2a9
    const-string v5, "android.app.IApplicationThread"

    #@2ab
    move-object/from16 v0, p2

    #@2ad
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b0
    .line 231
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2b3
    move-result-object v37

    #@2b4
    .line 232
    .restart local v37       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b7
    move-result v5

    #@2b8
    if-eqz v5, :cond_2dc

    #@2ba
    const/16 v38, 0x1

    #@2bc
    .line 233
    .local v38, taskRemoved:Z
    :goto_2bc
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2bf
    move-result v39

    #@2c0
    .line 234
    .local v39, startId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c3
    move-result v40

    #@2c4
    .line 236
    .local v40, fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2c7
    move-result v5

    #@2c8
    if-eqz v5, :cond_2df

    #@2ca
    .line 237
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2cc
    move-object/from16 v0, p2

    #@2ce
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d1
    move-result-object v41

    #@2d2
    check-cast v41, Landroid/content/Intent;

    #@2d4
    .local v41, args:Landroid/content/Intent;
    :goto_2d4
    move-object/from16 v36, p0

    #@2d6
    .line 241
    invoke-virtual/range {v36 .. v41}, Landroid/app/ApplicationThreadNative;->scheduleServiceArgs(Landroid/os/IBinder;ZIILandroid/content/Intent;)V

    #@2d9
    .line 242
    const/4 v5, 0x1

    #@2da
    goto/16 :goto_7

    #@2dc
    .line 232
    .end local v38           #taskRemoved:Z
    .end local v39           #startId:I
    .end local v40           #fl:I
    .end local v41           #args:Landroid/content/Intent;
    :cond_2dc
    const/16 v38, 0x0

    #@2de
    goto :goto_2bc

    #@2df
    .line 239
    .restart local v38       #taskRemoved:Z
    .restart local v39       #startId:I
    .restart local v40       #fl:I
    :cond_2df
    const/16 v41, 0x0

    #@2e1
    .restart local v41       #args:Landroid/content/Intent;
    goto :goto_2d4

    #@2e2
    .line 247
    .end local v37           #token:Landroid/os/IBinder;
    .end local v38           #taskRemoved:Z
    .end local v39           #startId:I
    .end local v40           #fl:I
    .end local v41           #args:Landroid/content/Intent;
    :pswitch_2e2
    const-string v5, "android.app.IApplicationThread"

    #@2e4
    move-object/from16 v0, p2

    #@2e6
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e9
    .line 248
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2ec
    move-result-object v37

    #@2ed
    .line 249
    .restart local v37       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@2ef
    move-object/from16 v1, v37

    #@2f1
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->scheduleStopService(Landroid/os/IBinder;)V

    #@2f4
    .line 250
    const/4 v5, 0x1

    #@2f5
    goto/16 :goto_7

    #@2f7
    .line 255
    .end local v37           #token:Landroid/os/IBinder;
    :pswitch_2f7
    const-string v5, "android.app.IApplicationThread"

    #@2f9
    move-object/from16 v0, p2

    #@2fb
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2fe
    .line 256
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@301
    move-result-object v43

    #@302
    .line 257
    .local v43, packageName:Ljava/lang/String;
    sget-object v5, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@304
    move-object/from16 v0, p2

    #@306
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@309
    move-result-object v9

    #@30a
    check-cast v9, Landroid/content/pm/ApplicationInfo;

    #@30c
    .line 259
    .local v9, info:Landroid/content/pm/ApplicationInfo;
    sget-object v5, Landroid/content/pm/ProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@30e
    move-object/from16 v0, p2

    #@310
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@313
    move-result-object v45

    #@314
    .line 261
    .local v45, providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@317
    move-result v5

    #@318
    if-eqz v5, :cond_394

    #@31a
    new-instance v46, Landroid/content/ComponentName;

    #@31c
    move-object/from16 v0, v46

    #@31e
    move-object/from16 v1, p2

    #@320
    invoke-direct {v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/os/Parcel;)V

    #@323
    .line 263
    .local v46, testName:Landroid/content/ComponentName;
    :goto_323
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@326
    move-result-object v17

    #@327
    .line 264
    .restart local v17       #profileName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@32a
    move-result v5

    #@32b
    if-eqz v5, :cond_397

    #@32d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@330
    move-result-object v18

    #@331
    .line 266
    .restart local v18       #profileFd:Landroid/os/ParcelFileDescriptor;
    :goto_331
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@334
    move-result v5

    #@335
    if-eqz v5, :cond_39a

    #@337
    const/16 v19, 0x1

    #@339
    .line 267
    .restart local v19       #autoStopProfiler:Z
    :goto_339
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@33c
    move-result-object v50

    #@33d
    .line 268
    .local v50, testArgs:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@340
    move-result-object v73

    #@341
    .line 269
    .local v73, binder:Landroid/os/IBinder;
    invoke-static/range {v73 .. v73}, Landroid/app/IInstrumentationWatcher$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IInstrumentationWatcher;

    #@344
    move-result-object v51

    #@345
    .line 270
    .local v51, testWatcher:Landroid/app/IInstrumentationWatcher;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@348
    move-result v52

    #@349
    .line 271
    .local v52, testMode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34c
    move-result v5

    #@34d
    if-eqz v5, :cond_39d

    #@34f
    const/16 v53, 0x1

    #@351
    .line 272
    .local v53, openGlTrace:Z
    :goto_351
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@354
    move-result v5

    #@355
    if-eqz v5, :cond_3a0

    #@357
    const/16 v54, 0x1

    #@359
    .line 273
    .local v54, restrictedBackupMode:Z
    :goto_359
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@35c
    move-result v5

    #@35d
    if-eqz v5, :cond_3a3

    #@35f
    const/16 v55, 0x1

    #@361
    .line 274
    .local v55, persistent:Z
    :goto_361
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@363
    move-object/from16 v0, p2

    #@365
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@368
    move-result-object v26

    #@369
    check-cast v26, Landroid/content/res/Configuration;

    #@36b
    .line 275
    .restart local v26       #config:Landroid/content/res/Configuration;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@36d
    move-object/from16 v0, p2

    #@36f
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@372
    move-result-object v11

    #@373
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@375
    .line 276
    .restart local v11       #compatInfo:Landroid/content/res/CompatibilityInfo;
    const/4 v5, 0x0

    #@376
    move-object/from16 v0, p2

    #@378
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@37b
    move-result-object v58

    #@37c
    .line 277
    .local v58, services:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/IBinder;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@37f
    move-result-object v59

    #@380
    .local v59, coreSettings:Landroid/os/Bundle;
    move-object/from16 v42, p0

    #@382
    move-object/from16 v44, v9

    #@384
    move-object/from16 v47, v17

    #@386
    move-object/from16 v48, v18

    #@388
    move/from16 v49, v19

    #@38a
    move-object/from16 v56, v26

    #@38c
    move-object/from16 v57, v11

    #@38e
    .line 278
    invoke-virtual/range {v42 .. v59}, Landroid/app/ApplicationThreadNative;->bindApplication(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/util/List;Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ZLandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;IZZZLandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Ljava/util/Map;Landroid/os/Bundle;)V

    #@391
    .line 282
    const/4 v5, 0x1

    #@392
    goto/16 :goto_7

    #@394
    .line 261
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v17           #profileName:Ljava/lang/String;
    .end local v18           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v19           #autoStopProfiler:Z
    .end local v26           #config:Landroid/content/res/Configuration;
    .end local v46           #testName:Landroid/content/ComponentName;
    .end local v50           #testArgs:Landroid/os/Bundle;
    .end local v51           #testWatcher:Landroid/app/IInstrumentationWatcher;
    .end local v52           #testMode:I
    .end local v53           #openGlTrace:Z
    .end local v54           #restrictedBackupMode:Z
    .end local v55           #persistent:Z
    .end local v58           #services:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/IBinder;>;"
    .end local v59           #coreSettings:Landroid/os/Bundle;
    .end local v73           #binder:Landroid/os/IBinder;
    :cond_394
    const/16 v46, 0x0

    #@396
    goto :goto_323

    #@397
    .line 264
    .restart local v17       #profileName:Ljava/lang/String;
    .restart local v46       #testName:Landroid/content/ComponentName;
    :cond_397
    const/16 v18, 0x0

    #@399
    goto :goto_331

    #@39a
    .line 266
    .restart local v18       #profileFd:Landroid/os/ParcelFileDescriptor;
    :cond_39a
    const/16 v19, 0x0

    #@39c
    goto :goto_339

    #@39d
    .line 271
    .restart local v19       #autoStopProfiler:Z
    .restart local v50       #testArgs:Landroid/os/Bundle;
    .restart local v51       #testWatcher:Landroid/app/IInstrumentationWatcher;
    .restart local v52       #testMode:I
    .restart local v73       #binder:Landroid/os/IBinder;
    :cond_39d
    const/16 v53, 0x0

    #@39f
    goto :goto_351

    #@3a0
    .line 272
    .restart local v53       #openGlTrace:Z
    :cond_3a0
    const/16 v54, 0x0

    #@3a2
    goto :goto_359

    #@3a3
    .line 273
    .restart local v54       #restrictedBackupMode:Z
    :cond_3a3
    const/16 v55, 0x0

    #@3a5
    goto :goto_361

    #@3a6
    .line 287
    .end local v9           #info:Landroid/content/pm/ApplicationInfo;
    .end local v17           #profileName:Ljava/lang/String;
    .end local v18           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v19           #autoStopProfiler:Z
    .end local v43           #packageName:Ljava/lang/String;
    .end local v45           #providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    .end local v46           #testName:Landroid/content/ComponentName;
    .end local v50           #testArgs:Landroid/os/Bundle;
    .end local v51           #testWatcher:Landroid/app/IInstrumentationWatcher;
    .end local v52           #testMode:I
    .end local v53           #openGlTrace:Z
    .end local v54           #restrictedBackupMode:Z
    .end local v73           #binder:Landroid/os/IBinder;
    :pswitch_3a6
    const-string v5, "android.app.IApplicationThread"

    #@3a8
    move-object/from16 v0, p2

    #@3aa
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ad
    .line 288
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->scheduleExit()V

    #@3b0
    .line 289
    const/4 v5, 0x1

    #@3b1
    goto/16 :goto_7

    #@3b3
    .line 294
    :pswitch_3b3
    const-string v5, "android.app.IApplicationThread"

    #@3b5
    move-object/from16 v0, p2

    #@3b7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ba
    .line 295
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->scheduleSuicide()V

    #@3bd
    .line 296
    const/4 v5, 0x1

    #@3be
    goto/16 :goto_7

    #@3c0
    .line 301
    :pswitch_3c0
    const-string v5, "android.app.IApplicationThread"

    #@3c2
    move-object/from16 v0, p2

    #@3c4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c7
    .line 302
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3ca
    move-result-object v7

    #@3cb
    .line 303
    .restart local v7       #b:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@3cd
    invoke-virtual {v0, v7}, Landroid/app/ApplicationThreadNative;->requestThumbnail(Landroid/os/IBinder;)V

    #@3d0
    .line 304
    const/4 v5, 0x1

    #@3d1
    goto/16 :goto_7

    #@3d3
    .line 309
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_3d3
    const-string v5, "android.app.IApplicationThread"

    #@3d5
    move-object/from16 v0, p2

    #@3d7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3da
    .line 310
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3dc
    move-object/from16 v0, p2

    #@3de
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3e1
    move-result-object v26

    #@3e2
    check-cast v26, Landroid/content/res/Configuration;

    #@3e4
    .line 311
    .restart local v26       #config:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@3e6
    move-object/from16 v1, v26

    #@3e8
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->scheduleConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3eb
    .line 312
    const/4 v5, 0x1

    #@3ec
    goto/16 :goto_7

    #@3ee
    .line 316
    .end local v26           #config:Landroid/content/res/Configuration;
    :pswitch_3ee
    const-string v5, "android.app.IApplicationThread"

    #@3f0
    move-object/from16 v0, p2

    #@3f2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f5
    .line 317
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->updateTimeZone()V

    #@3f8
    .line 318
    const/4 v5, 0x1

    #@3f9
    goto/16 :goto_7

    #@3fb
    .line 322
    :pswitch_3fb
    const-string v5, "android.app.IApplicationThread"

    #@3fd
    move-object/from16 v0, p2

    #@3ff
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@402
    .line 323
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->clearDnsCache()V

    #@405
    .line 324
    const/4 v5, 0x1

    #@406
    goto/16 :goto_7

    #@408
    .line 328
    :pswitch_408
    const-string v5, "android.app.IApplicationThread"

    #@40a
    move-object/from16 v0, p2

    #@40c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40f
    .line 329
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@412
    move-result-object v93

    #@413
    .line 330
    .local v93, proxy:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@416
    move-result-object v89

    #@417
    .line 331
    .local v89, port:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41a
    move-result-object v77

    #@41b
    .line 332
    .local v77, exclList:Ljava/lang/String;
    move-object/from16 v0, p0

    #@41d
    move-object/from16 v1, v93

    #@41f
    move-object/from16 v2, v89

    #@421
    move-object/from16 v3, v77

    #@423
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ApplicationThreadNative;->setHttpProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@426
    .line 333
    const/4 v5, 0x1

    #@427
    goto/16 :goto_7

    #@429
    .line 337
    .end local v77           #exclList:Ljava/lang/String;
    .end local v89           #port:Ljava/lang/String;
    .end local v93           #proxy:Ljava/lang/String;
    :pswitch_429
    const-string v5, "android.app.IApplicationThread"

    #@42b
    move-object/from16 v0, p2

    #@42d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@430
    .line 338
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->processInBackground()V

    #@433
    .line 339
    const/4 v5, 0x1

    #@434
    goto/16 :goto_7

    #@436
    .line 343
    :pswitch_436
    const-string v5, "android.app.IApplicationThread"

    #@438
    move-object/from16 v0, p2

    #@43a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43d
    .line 344
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@440
    move-result-object v78

    #@441
    .line 345
    .local v78, fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@444
    move-result-object v95

    #@445
    .line 346
    .local v95, service:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@448
    move-result-object v41

    #@449
    .line 347
    .local v41, args:[Ljava/lang/String;
    if-eqz v78, :cond_45b

    #@44b
    .line 348
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@44e
    move-result-object v5

    #@44f
    move-object/from16 v0, p0

    #@451
    move-object/from16 v1, v95

    #@453
    move-object/from16 v2, v41

    #@455
    invoke-virtual {v0, v5, v1, v2}, Landroid/app/ApplicationThreadNative;->dumpService(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V

    #@458
    .line 350
    :try_start_458
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_45b
    .catch Ljava/io/IOException; {:try_start_458 .. :try_end_45b} :catch_745

    #@45b
    .line 354
    :cond_45b
    :goto_45b
    const/4 v5, 0x1

    #@45c
    goto/16 :goto_7

    #@45e
    .line 358
    .end local v41           #args:[Ljava/lang/String;
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v95           #service:Landroid/os/IBinder;
    :pswitch_45e
    const-string v5, "android.app.IApplicationThread"

    #@460
    move-object/from16 v0, p2

    #@462
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@465
    .line 359
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@468
    move-result-object v78

    #@469
    .line 360
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@46c
    move-result-object v95

    #@46d
    .line 361
    .restart local v95       #service:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@470
    move-result-object v41

    #@471
    .line 362
    .restart local v41       #args:[Ljava/lang/String;
    if-eqz v78, :cond_483

    #@473
    .line 363
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@476
    move-result-object v5

    #@477
    move-object/from16 v0, p0

    #@479
    move-object/from16 v1, v95

    #@47b
    move-object/from16 v2, v41

    #@47d
    invoke-virtual {v0, v5, v1, v2}, Landroid/app/ApplicationThreadNative;->dumpProvider(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V

    #@480
    .line 365
    :try_start_480
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_483
    .catch Ljava/io/IOException; {:try_start_480 .. :try_end_483} :catch_748

    #@483
    .line 369
    :cond_483
    :goto_483
    const/4 v5, 0x1

    #@484
    goto/16 :goto_7

    #@486
    .line 373
    .end local v41           #args:[Ljava/lang/String;
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v95           #service:Landroid/os/IBinder;
    :pswitch_486
    const-string v5, "android.app.IApplicationThread"

    #@488
    move-object/from16 v0, p2

    #@48a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@48d
    .line 374
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@490
    move-result-object v5

    #@491
    invoke-static {v5}, Landroid/content/IIntentReceiver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;

    #@494
    move-result-object v61

    #@495
    .line 376
    .local v61, receiver:Landroid/content/IIntentReceiver;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@497
    move-object/from16 v0, p2

    #@499
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@49c
    move-result-object v6

    #@49d
    check-cast v6, Landroid/content/Intent;

    #@49f
    .line 377
    .restart local v6       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4a2
    move-result v31

    #@4a3
    .line 378
    .restart local v31       #resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a6
    move-result-object v64

    #@4a7
    .line 379
    .local v64, dataStr:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@4aa
    move-result-object v65

    #@4ab
    .line 380
    .local v65, extras:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4ae
    move-result v5

    #@4af
    if-eqz v5, :cond_4cd

    #@4b1
    const/16 v66, 0x1

    #@4b3
    .line 381
    .local v66, ordered:Z
    :goto_4b3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4b6
    move-result v5

    #@4b7
    if-eqz v5, :cond_4d0

    #@4b9
    const/16 v67, 0x1

    #@4bb
    .line 382
    .local v67, sticky:Z
    :goto_4bb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4be
    move-result v35

    #@4bf
    .restart local v35       #sendingUser:I
    move-object/from16 v60, p0

    #@4c1
    move-object/from16 v62, v6

    #@4c3
    move/from16 v63, v31

    #@4c5
    move/from16 v68, v35

    #@4c7
    .line 383
    invoke-virtual/range {v60 .. v68}, Landroid/app/ApplicationThreadNative;->scheduleRegisteredReceiver(Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@4ca
    .line 385
    const/4 v5, 0x1

    #@4cb
    goto/16 :goto_7

    #@4cd
    .line 380
    .end local v35           #sendingUser:I
    .end local v66           #ordered:Z
    .end local v67           #sticky:Z
    :cond_4cd
    const/16 v66, 0x0

    #@4cf
    goto :goto_4b3

    #@4d0
    .line 381
    .restart local v66       #ordered:Z
    :cond_4d0
    const/16 v67, 0x0

    #@4d2
    goto :goto_4bb

    #@4d3
    .line 390
    .end local v6           #intent:Landroid/content/Intent;
    .end local v31           #resultCode:I
    .end local v61           #receiver:Landroid/content/IIntentReceiver;
    .end local v64           #dataStr:Ljava/lang/String;
    .end local v65           #extras:Landroid/os/Bundle;
    .end local v66           #ordered:Z
    :pswitch_4d3
    const-string v5, "android.app.IApplicationThread"

    #@4d5
    move-object/from16 v0, p2

    #@4d7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4da
    .line 391
    invoke-virtual/range {p0 .. p0}, Landroid/app/ApplicationThreadNative;->scheduleLowMemory()V

    #@4dd
    .line 392
    const/4 v5, 0x1

    #@4de
    goto/16 :goto_7

    #@4e0
    .line 397
    :pswitch_4e0
    const-string v5, "android.app.IApplicationThread"

    #@4e2
    move-object/from16 v0, p2

    #@4e4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e7
    .line 398
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4ea
    move-result-object v7

    #@4eb
    .line 399
    .restart local v7       #b:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@4ed
    invoke-virtual {v0, v7}, Landroid/app/ApplicationThreadNative;->scheduleActivityConfigurationChanged(Landroid/os/IBinder;)V

    #@4f0
    .line 400
    const/4 v5, 0x1

    #@4f1
    goto/16 :goto_7

    #@4f3
    .line 405
    .end local v7           #b:Landroid/os/IBinder;
    :pswitch_4f3
    const-string v5, "android.app.IApplicationThread"

    #@4f5
    move-object/from16 v0, p2

    #@4f7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4fa
    .line 406
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4fd
    move-result v5

    #@4fe
    if-eqz v5, :cond_524

    #@500
    const/16 v99, 0x1

    #@502
    .line 407
    .local v99, start:Z
    :goto_502
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@505
    move-result v91

    #@506
    .line 408
    .local v91, profileType:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@509
    move-result-object v87

    #@50a
    .line 409
    .local v87, path:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@50d
    move-result v5

    #@50e
    if-eqz v5, :cond_527

    #@510
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@513
    move-result-object v78

    #@514
    .line 411
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    :goto_514
    move-object/from16 v0, p0

    #@516
    move/from16 v1, v99

    #@518
    move-object/from16 v2, v87

    #@51a
    move-object/from16 v3, v78

    #@51c
    move/from16 v4, v91

    #@51e
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ApplicationThreadNative;->profilerControl(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)V

    #@521
    .line 412
    const/4 v5, 0x1

    #@522
    goto/16 :goto_7

    #@524
    .line 406
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v87           #path:Ljava/lang/String;
    .end local v91           #profileType:I
    .end local v99           #start:Z
    :cond_524
    const/16 v99, 0x0

    #@526
    goto :goto_502

    #@527
    .line 409
    .restart local v87       #path:Ljava/lang/String;
    .restart local v91       #profileType:I
    .restart local v99       #start:Z
    :cond_527
    const/16 v78, 0x0

    #@529
    goto :goto_514

    #@52a
    .line 417
    .end local v87           #path:Ljava/lang/String;
    .end local v91           #profileType:I
    .end local v99           #start:Z
    :pswitch_52a
    const-string v5, "android.app.IApplicationThread"

    #@52c
    move-object/from16 v0, p2

    #@52e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@531
    .line 418
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@534
    move-result v81

    #@535
    .line 419
    .local v81, group:I
    move-object/from16 v0, p0

    #@537
    move/from16 v1, v81

    #@539
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->setSchedulingGroup(I)V

    #@53c
    .line 420
    const/4 v5, 0x1

    #@53d
    goto/16 :goto_7

    #@53f
    .line 425
    .end local v81           #group:I
    :pswitch_53f
    const-string v5, "android.app.IApplicationThread"

    #@541
    move-object/from16 v0, p2

    #@543
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@546
    .line 426
    sget-object v5, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@548
    move-object/from16 v0, p2

    #@54a
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@54d
    move-result-object v71

    #@54e
    check-cast v71, Landroid/content/pm/ApplicationInfo;

    #@550
    .line 427
    .local v71, appInfo:Landroid/content/pm/ApplicationInfo;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@552
    move-object/from16 v0, p2

    #@554
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@557
    move-result-object v11

    #@558
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@55a
    .line 428
    .restart local v11       #compatInfo:Landroid/content/res/CompatibilityInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@55d
    move-result v72

    #@55e
    .line 429
    .local v72, backupMode:I
    move-object/from16 v0, p0

    #@560
    move-object/from16 v1, v71

    #@562
    move/from16 v2, v72

    #@564
    invoke-virtual {v0, v1, v11, v2}, Landroid/app/ApplicationThreadNative;->scheduleCreateBackupAgent(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)V

    #@567
    .line 430
    const/4 v5, 0x1

    #@568
    goto/16 :goto_7

    #@56a
    .line 435
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v71           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v72           #backupMode:I
    :pswitch_56a
    const-string v5, "android.app.IApplicationThread"

    #@56c
    move-object/from16 v0, p2

    #@56e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@571
    .line 436
    sget-object v5, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@573
    move-object/from16 v0, p2

    #@575
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@578
    move-result-object v71

    #@579
    check-cast v71, Landroid/content/pm/ApplicationInfo;

    #@57b
    .line 437
    .restart local v71       #appInfo:Landroid/content/pm/ApplicationInfo;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@57d
    move-object/from16 v0, p2

    #@57f
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@582
    move-result-object v11

    #@583
    check-cast v11, Landroid/content/res/CompatibilityInfo;

    #@585
    .line 438
    .restart local v11       #compatInfo:Landroid/content/res/CompatibilityInfo;
    move-object/from16 v0, p0

    #@587
    move-object/from16 v1, v71

    #@589
    invoke-virtual {v0, v1, v11}, Landroid/app/ApplicationThreadNative;->scheduleDestroyBackupAgent(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V

    #@58c
    .line 439
    const/4 v5, 0x1

    #@58d
    goto/16 :goto_7

    #@58f
    .line 444
    .end local v11           #compatInfo:Landroid/content/res/CompatibilityInfo;
    .end local v71           #appInfo:Landroid/content/pm/ApplicationInfo;
    :pswitch_58f
    const-string v5, "android.app.IApplicationThread"

    #@591
    move-object/from16 v0, p2

    #@593
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@596
    .line 445
    new-instance v84, Landroid/os/Debug$MemoryInfo;

    #@598
    invoke-direct/range {v84 .. v84}, Landroid/os/Debug$MemoryInfo;-><init>()V

    #@59b
    .line 446
    .local v84, mi:Landroid/os/Debug$MemoryInfo;
    move-object/from16 v0, p0

    #@59d
    move-object/from16 v1, v84

    #@59f
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    #@5a2
    .line 447
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a5
    .line 448
    const/4 v5, 0x0

    #@5a6
    move-object/from16 v0, v84

    #@5a8
    move-object/from16 v1, p3

    #@5aa
    invoke-virtual {v0, v1, v5}, Landroid/os/Debug$MemoryInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5ad
    .line 449
    const/4 v5, 0x1

    #@5ae
    goto/16 :goto_7

    #@5b0
    .line 454
    .end local v84           #mi:Landroid/os/Debug$MemoryInfo;
    :pswitch_5b0
    const-string v5, "android.app.IApplicationThread"

    #@5b2
    move-object/from16 v0, p2

    #@5b4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5b7
    .line 455
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5ba
    move-result v75

    #@5bb
    .line 456
    .local v75, cmd:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@5be
    move-result-object v86

    #@5bf
    .line 457
    .local v86, packages:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@5c1
    move/from16 v1, v75

    #@5c3
    move-object/from16 v2, v86

    #@5c5
    invoke-virtual {v0, v1, v2}, Landroid/app/ApplicationThreadNative;->dispatchPackageBroadcast(I[Ljava/lang/String;)V

    #@5c8
    .line 458
    const/4 v5, 0x1

    #@5c9
    goto/16 :goto_7

    #@5cb
    .line 463
    .end local v75           #cmd:I
    .end local v86           #packages:[Ljava/lang/String;
    :pswitch_5cb
    const-string v5, "android.app.IApplicationThread"

    #@5cd
    move-object/from16 v0, p2

    #@5cf
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5d2
    .line 464
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5d5
    move-result-object v85

    #@5d6
    .line 465
    .local v85, msg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5d8
    move-object/from16 v1, v85

    #@5da
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->scheduleCrash(Ljava/lang/String;)V

    #@5dd
    .line 466
    const/4 v5, 0x1

    #@5de
    goto/16 :goto_7

    #@5e0
    .line 471
    .end local v85           #msg:Ljava/lang/String;
    :pswitch_5e0
    const-string v5, "android.app.IApplicationThread"

    #@5e2
    move-object/from16 v0, p2

    #@5e4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5e7
    .line 472
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5ea
    move-result v5

    #@5eb
    if-eqz v5, :cond_60b

    #@5ed
    const/16 v83, 0x1

    #@5ef
    .line 473
    .local v83, managed:Z
    :goto_5ef
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f2
    move-result-object v87

    #@5f3
    .line 474
    .restart local v87       #path:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5f6
    move-result v5

    #@5f7
    if-eqz v5, :cond_60e

    #@5f9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@5fc
    move-result-object v78

    #@5fd
    .line 476
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    :goto_5fd
    move-object/from16 v0, p0

    #@5ff
    move/from16 v1, v83

    #@601
    move-object/from16 v2, v87

    #@603
    move-object/from16 v3, v78

    #@605
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ApplicationThreadNative;->dumpHeap(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)V

    #@608
    .line 477
    const/4 v5, 0x1

    #@609
    goto/16 :goto_7

    #@60b
    .line 472
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v83           #managed:Z
    .end local v87           #path:Ljava/lang/String;
    :cond_60b
    const/16 v83, 0x0

    #@60d
    goto :goto_5ef

    #@60e
    .line 474
    .restart local v83       #managed:Z
    .restart local v87       #path:Ljava/lang/String;
    :cond_60e
    const/16 v78, 0x0

    #@610
    goto :goto_5fd

    #@611
    .line 481
    .end local v83           #managed:Z
    .end local v87           #path:Ljava/lang/String;
    :pswitch_611
    const-string v5, "android.app.IApplicationThread"

    #@613
    move-object/from16 v0, p2

    #@615
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@618
    .line 482
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@61b
    move-result-object v78

    #@61c
    .line 483
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@61f
    move-result-object v69

    #@620
    .line 484
    .local v69, activity:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@623
    move-result-object v90

    #@624
    .line 485
    .local v90, prefix:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@627
    move-result-object v41

    #@628
    .line 486
    .restart local v41       #args:[Ljava/lang/String;
    if-eqz v78, :cond_63c

    #@62a
    .line 487
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@62d
    move-result-object v5

    #@62e
    move-object/from16 v0, p0

    #@630
    move-object/from16 v1, v69

    #@632
    move-object/from16 v2, v90

    #@634
    move-object/from16 v3, v41

    #@636
    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/app/ApplicationThreadNative;->dumpActivity(Ljava/io/FileDescriptor;Landroid/os/IBinder;Ljava/lang/String;[Ljava/lang/String;)V

    #@639
    .line 489
    :try_start_639
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_63c
    .catch Ljava/io/IOException; {:try_start_639 .. :try_end_63c} :catch_74b

    #@63c
    .line 493
    :cond_63c
    :goto_63c
    const/4 v5, 0x1

    #@63d
    goto/16 :goto_7

    #@63f
    .line 497
    .end local v41           #args:[Ljava/lang/String;
    .end local v69           #activity:Landroid/os/IBinder;
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v90           #prefix:Ljava/lang/String;
    :pswitch_63f
    const-string v5, "android.app.IApplicationThread"

    #@641
    move-object/from16 v0, p2

    #@643
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@646
    .line 498
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@649
    move-result-object v96

    #@64a
    .line 499
    .local v96, settings:Landroid/os/Bundle;
    move-object/from16 v0, p0

    #@64c
    move-object/from16 v1, v96

    #@64e
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->setCoreSettings(Landroid/os/Bundle;)V

    #@651
    .line 500
    const/4 v5, 0x1

    #@652
    goto/16 :goto_7

    #@654
    .line 504
    .end local v96           #settings:Landroid/os/Bundle;
    :pswitch_654
    const-string v5, "android.app.IApplicationThread"

    #@656
    move-object/from16 v0, p2

    #@658
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65b
    .line 505
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@65e
    move-result-object v88

    #@65f
    .line 506
    .local v88, pkg:Ljava/lang/String;
    sget-object v5, Landroid/content/res/CompatibilityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@661
    move-object/from16 v0, p2

    #@663
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@666
    move-result-object v76

    #@667
    check-cast v76, Landroid/content/res/CompatibilityInfo;

    #@669
    .line 507
    .local v76, compat:Landroid/content/res/CompatibilityInfo;
    move-object/from16 v0, p0

    #@66b
    move-object/from16 v1, v88

    #@66d
    move-object/from16 v2, v76

    #@66f
    invoke-virtual {v0, v1, v2}, Landroid/app/ApplicationThreadNative;->updatePackageCompatibilityInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;)V

    #@672
    .line 508
    const/4 v5, 0x1

    #@673
    goto/16 :goto_7

    #@675
    .line 512
    .end local v76           #compat:Landroid/content/res/CompatibilityInfo;
    .end local v88           #pkg:Ljava/lang/String;
    :pswitch_675
    const-string v5, "android.app.IApplicationThread"

    #@677
    move-object/from16 v0, p2

    #@679
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67c
    .line 513
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@67f
    move-result v82

    #@680
    .line 514
    .local v82, level:I
    move-object/from16 v0, p0

    #@682
    move/from16 v1, v82

    #@684
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->scheduleTrimMemory(I)V

    #@687
    .line 515
    const/4 v5, 0x1

    #@688
    goto/16 :goto_7

    #@68a
    .line 520
    .end local v82           #level:I
    :pswitch_68a
    const-string v5, "android.app.IApplicationThread"

    #@68c
    move-object/from16 v0, p2

    #@68e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@691
    .line 521
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@694
    move-result-object v78

    #@695
    .line 522
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@698
    move-result v5

    #@699
    if-eqz v5, :cond_6ce

    #@69b
    const/16 v74, 0x1

    #@69d
    .line 523
    .local v74, checkin:Z
    :goto_69d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6a0
    move-result v5

    #@6a1
    if-eqz v5, :cond_6d1

    #@6a3
    const/16 v70, 0x1

    #@6a5
    .line 524
    .local v70, all:Z
    :goto_6a5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@6a8
    move-result-object v41

    #@6a9
    .line 525
    .restart local v41       #args:[Ljava/lang/String;
    const/16 v84, 0x0

    #@6ab
    .line 526
    .restart local v84       #mi:Landroid/os/Debug$MemoryInfo;
    if-eqz v78, :cond_6c0

    #@6ad
    .line 528
    :try_start_6ad
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@6b0
    move-result-object v5

    #@6b1
    move-object/from16 v0, p0

    #@6b3
    move/from16 v1, v74

    #@6b5
    move/from16 v2, v70

    #@6b7
    move-object/from16 v3, v41

    #@6b9
    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/app/ApplicationThreadNative;->dumpMemInfo(Ljava/io/FileDescriptor;ZZ[Ljava/lang/String;)Landroid/os/Debug$MemoryInfo;
    :try_end_6bc
    .catchall {:try_start_6ad .. :try_end_6bc} :catchall_6d4

    #@6bc
    move-result-object v84

    #@6bd
    .line 531
    :try_start_6bd
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6c0
    .catch Ljava/io/IOException; {:try_start_6bd .. :try_end_6c0} :catch_74e

    #@6c0
    .line 537
    :cond_6c0
    :goto_6c0
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c3
    .line 538
    const/4 v5, 0x0

    #@6c4
    move-object/from16 v0, v84

    #@6c6
    move-object/from16 v1, p3

    #@6c8
    invoke-virtual {v0, v1, v5}, Landroid/os/Debug$MemoryInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@6cb
    .line 539
    const/4 v5, 0x1

    #@6cc
    goto/16 :goto_7

    #@6ce
    .line 522
    .end local v41           #args:[Ljava/lang/String;
    .end local v70           #all:Z
    .end local v74           #checkin:Z
    .end local v84           #mi:Landroid/os/Debug$MemoryInfo;
    :cond_6ce
    const/16 v74, 0x0

    #@6d0
    goto :goto_69d

    #@6d1
    .line 523
    .restart local v74       #checkin:Z
    :cond_6d1
    const/16 v70, 0x0

    #@6d3
    goto :goto_6a5

    #@6d4
    .line 530
    .restart local v41       #args:[Ljava/lang/String;
    .restart local v70       #all:Z
    .restart local v84       #mi:Landroid/os/Debug$MemoryInfo;
    :catchall_6d4
    move-exception v5

    #@6d5
    .line 531
    :try_start_6d5
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6d8
    .catch Ljava/io/IOException; {:try_start_6d5 .. :try_end_6d8} :catch_751

    #@6d8
    .line 534
    :goto_6d8
    throw v5

    #@6d9
    .line 544
    .end local v41           #args:[Ljava/lang/String;
    .end local v70           #all:Z
    .end local v74           #checkin:Z
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v84           #mi:Landroid/os/Debug$MemoryInfo;
    :pswitch_6d9
    const-string v5, "android.app.IApplicationThread"

    #@6db
    move-object/from16 v0, p2

    #@6dd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6e0
    .line 545
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@6e3
    move-result-object v78

    #@6e4
    .line 546
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@6e7
    move-result-object v41

    #@6e8
    .line 547
    .restart local v41       #args:[Ljava/lang/String;
    if-eqz v78, :cond_6f8

    #@6ea
    .line 549
    :try_start_6ea
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@6ed
    move-result-object v5

    #@6ee
    move-object/from16 v0, p0

    #@6f0
    move-object/from16 v1, v41

    #@6f2
    invoke-virtual {v0, v5, v1}, Landroid/app/ApplicationThreadNative;->dumpGfxInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_6f5
    .catchall {:try_start_6ea .. :try_end_6f5} :catchall_6fe

    #@6f5
    .line 552
    :try_start_6f5
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6f8
    .catch Ljava/io/IOException; {:try_start_6f5 .. :try_end_6f8} :catch_753

    #@6f8
    .line 558
    :cond_6f8
    :goto_6f8
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6fb
    .line 559
    const/4 v5, 0x1

    #@6fc
    goto/16 :goto_7

    #@6fe
    .line 551
    :catchall_6fe
    move-exception v5

    #@6ff
    .line 552
    :try_start_6ff
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_702
    .catch Ljava/io/IOException; {:try_start_6ff .. :try_end_702} :catch_755

    #@702
    .line 555
    :goto_702
    throw v5

    #@703
    .line 564
    .end local v41           #args:[Ljava/lang/String;
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    :pswitch_703
    const-string v5, "android.app.IApplicationThread"

    #@705
    move-object/from16 v0, p2

    #@707
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70a
    .line 565
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@70d
    move-result-object v78

    #@70e
    .line 566
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@711
    move-result-object v41

    #@712
    .line 567
    .restart local v41       #args:[Ljava/lang/String;
    if-eqz v78, :cond_722

    #@714
    .line 569
    :try_start_714
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@717
    move-result-object v5

    #@718
    move-object/from16 v0, p0

    #@71a
    move-object/from16 v1, v41

    #@71c
    invoke-virtual {v0, v5, v1}, Landroid/app/ApplicationThreadNative;->dumpDbInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_71f
    .catchall {:try_start_714 .. :try_end_71f} :catchall_728

    #@71f
    .line 572
    :try_start_71f
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_722
    .catch Ljava/io/IOException; {:try_start_71f .. :try_end_722} :catch_757

    #@722
    .line 578
    :cond_722
    :goto_722
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@725
    .line 579
    const/4 v5, 0x1

    #@726
    goto/16 :goto_7

    #@728
    .line 571
    :catchall_728
    move-exception v5

    #@729
    .line 572
    :try_start_729
    invoke-virtual/range {v78 .. v78}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_72c
    .catch Ljava/io/IOException; {:try_start_729 .. :try_end_72c} :catch_759

    #@72c
    .line 575
    :goto_72c
    throw v5

    #@72d
    .line 584
    .end local v41           #args:[Ljava/lang/String;
    .end local v78           #fd:Landroid/os/ParcelFileDescriptor;
    :pswitch_72d
    const-string v5, "android.app.IApplicationThread"

    #@72f
    move-object/from16 v0, p2

    #@731
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@734
    .line 585
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@737
    move-result-object v92

    #@738
    .line 586
    .local v92, provider:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@73a
    move-object/from16 v1, v92

    #@73c
    invoke-virtual {v0, v1}, Landroid/app/ApplicationThreadNative;->unstableProviderDied(Landroid/os/IBinder;)V

    #@73f
    .line 587
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@742
    .line 588
    const/4 v5, 0x1

    #@743
    goto/16 :goto_7

    #@745
    .line 351
    .end local v92           #provider:Landroid/os/IBinder;
    .restart local v41       #args:[Ljava/lang/String;
    .restart local v78       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v95       #service:Landroid/os/IBinder;
    :catch_745
    move-exception v5

    #@746
    goto/16 :goto_45b

    #@748
    .line 366
    :catch_748
    move-exception v5

    #@749
    goto/16 :goto_483

    #@74b
    .line 490
    .end local v95           #service:Landroid/os/IBinder;
    .restart local v69       #activity:Landroid/os/IBinder;
    .restart local v90       #prefix:Ljava/lang/String;
    :catch_74b
    move-exception v5

    #@74c
    goto/16 :goto_63c

    #@74e
    .line 532
    .end local v69           #activity:Landroid/os/IBinder;
    .end local v90           #prefix:Ljava/lang/String;
    .restart local v70       #all:Z
    .restart local v74       #checkin:Z
    .restart local v84       #mi:Landroid/os/Debug$MemoryInfo;
    :catch_74e
    move-exception v5

    #@74f
    goto/16 :goto_6c0

    #@751
    :catch_751
    move-exception v20

    #@752
    goto :goto_6d8

    #@753
    .line 553
    .end local v70           #all:Z
    .end local v74           #checkin:Z
    .end local v84           #mi:Landroid/os/Debug$MemoryInfo;
    :catch_753
    move-exception v5

    #@754
    goto :goto_6f8

    #@755
    :catch_755
    move-exception v20

    #@756
    goto :goto_702

    #@757
    .line 573
    :catch_757
    move-exception v5

    #@758
    goto :goto_722

    #@759
    :catch_759
    move-exception v20

    #@75a
    goto :goto_72c

    #@75b
    .line 70
    nop

    #@75c
    :pswitch_data_75c
    .packed-switch 0x1
        :pswitch_8
        :pswitch_3
        :pswitch_3a
        :pswitch_5f
        :pswitch_9e
        :pswitch_be
        :pswitch_d9
        :pswitch_1a6
        :pswitch_1c1
        :pswitch_1e7
        :pswitch_235
        :pswitch_2e2
        :pswitch_2f7
        :pswitch_3a6
        :pswitch_3c0
        :pswitch_3d3
        :pswitch_2a9
        :pswitch_3ee
        :pswitch_429
        :pswitch_25e
        :pswitch_28a
        :pswitch_436
        :pswitch_486
        :pswitch_4d3
        :pswitch_4e0
        :pswitch_15c
        :pswitch_7e
        :pswitch_4f3
        :pswitch_52a
        :pswitch_53f
        :pswitch_56a
        :pswitch_58f
        :pswitch_3b3
        :pswitch_5b0
        :pswitch_5cb
        :pswitch_5e0
        :pswitch_611
        :pswitch_3fb
        :pswitch_408
        :pswitch_63f
        :pswitch_654
        :pswitch_675
        :pswitch_68a
        :pswitch_6d9
        :pswitch_45e
        :pswitch_703
        :pswitch_72d
    .end packed-switch
.end method
