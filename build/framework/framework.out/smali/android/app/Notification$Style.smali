.class public abstract Landroid/app/Notification$Style;
.super Ljava/lang/Object;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Style"
.end annotation


# instance fields
.field private mBigContentTitle:Ljava/lang/CharSequence;

.field protected mBuilder:Landroid/app/Notification$Builder;

.field private mSummaryText:Ljava/lang/CharSequence;

.field private mSummaryTextSet:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1684
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1687
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/app/Notification$Style;->mSummaryText:Ljava/lang/CharSequence;

    #@6
    .line 1688
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/app/Notification$Style;->mSummaryTextSet:Z

    #@9
    return-void
.end method


# virtual methods
.method public abstract build()Landroid/app/Notification;
.end method

.method protected checkBuilder()V
    .registers 3

    #@0
    .prologue
    .line 1718
    iget-object v0, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 1719
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v1, "Style requires a valid Builder object"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1721
    :cond_c
    return-void
.end method

.method protected getStandardView(I)Landroid/widget/RemoteViews;
    .registers 11
    .parameter "layoutId"

    #@0
    .prologue
    const v8, 0x102033e

    #@3
    const v7, 0x1020339

    #@6
    const v6, 0x1020337

    #@9
    const/16 v5, 0x8

    #@b
    const/4 v4, 0x0

    #@c
    .line 1724
    invoke-virtual {p0}, Landroid/app/Notification$Style;->checkBuilder()V

    #@f
    .line 1726
    iget-object v2, p0, Landroid/app/Notification$Style;->mBigContentTitle:Ljava/lang/CharSequence;

    #@11
    if-eqz v2, :cond_1a

    #@13
    .line 1727
    iget-object v2, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@15
    iget-object v3, p0, Landroid/app/Notification$Style;->mBigContentTitle:Ljava/lang/CharSequence;

    #@17
    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@1a
    .line 1730
    :cond_1a
    iget-object v2, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@1c
    invoke-static {v2, p1}, Landroid/app/Notification$Builder;->access$300(Landroid/app/Notification$Builder;I)Landroid/widget/RemoteViews;

    #@1f
    move-result-object v0

    #@20
    .line 1732
    .local v0, contentView:Landroid/widget/RemoteViews;
    iget-object v2, p0, Landroid/app/Notification$Style;->mBigContentTitle:Ljava/lang/CharSequence;

    #@22
    if-eqz v2, :cond_46

    #@24
    iget-object v2, p0, Landroid/app/Notification$Style;->mBigContentTitle:Ljava/lang/CharSequence;

    #@26
    const-string v3, ""

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_46

    #@2e
    .line 1733
    invoke-virtual {v0, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@31
    .line 1739
    :goto_31
    iget-boolean v2, p0, Landroid/app/Notification$Style;->mSummaryTextSet:Z

    #@33
    if-eqz v2, :cond_4a

    #@35
    iget-object v1, p0, Landroid/app/Notification$Style;->mSummaryText:Ljava/lang/CharSequence;

    #@37
    .line 1742
    .local v1, overflowText:Ljava/lang/CharSequence;
    :goto_37
    if-eqz v1, :cond_51

    #@39
    .line 1743
    const v2, 0x1020046

    #@3c
    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@3f
    .line 1744
    invoke-virtual {v0, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@42
    .line 1745
    invoke-virtual {v0, v7, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@45
    .line 1751
    :goto_45
    return-object v0

    #@46
    .line 1735
    .end local v1           #overflowText:Ljava/lang/CharSequence;
    :cond_46
    invoke-virtual {v0, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@49
    goto :goto_31

    #@4a
    .line 1739
    :cond_4a
    iget-object v2, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@4c
    invoke-static {v2}, Landroid/app/Notification$Builder;->access$400(Landroid/app/Notification$Builder;)Ljava/lang/CharSequence;

    #@4f
    move-result-object v1

    #@50
    goto :goto_37

    #@51
    .line 1747
    .restart local v1       #overflowText:Ljava/lang/CharSequence;
    :cond_51
    invoke-virtual {v0, v8, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@54
    .line 1748
    invoke-virtual {v0, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@57
    goto :goto_45
.end method

.method protected internalSetBigContentTitle(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 1697
    iput-object p1, p0, Landroid/app/Notification$Style;->mBigContentTitle:Ljava/lang/CharSequence;

    #@2
    .line 1698
    return-void
.end method

.method protected internalSetSummaryText(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "cs"

    #@0
    .prologue
    .line 1704
    iput-object p1, p0, Landroid/app/Notification$Style;->mSummaryText:Ljava/lang/CharSequence;

    #@2
    .line 1705
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/app/Notification$Style;->mSummaryTextSet:Z

    #@5
    .line 1706
    return-void
.end method

.method public setBuilder(Landroid/app/Notification$Builder;)V
    .registers 3
    .parameter "builder"

    #@0
    .prologue
    .line 1709
    iget-object v0, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@2
    if-eq v0, p1, :cond_f

    #@4
    .line 1710
    iput-object p1, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@6
    .line 1711
    iget-object v0, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1712
    iget-object v0, p0, Landroid/app/Notification$Style;->mBuilder:Landroid/app/Notification$Builder;

    #@c
    invoke-virtual {v0, p0}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    #@f
    .line 1715
    :cond_f
    return-void
.end method
