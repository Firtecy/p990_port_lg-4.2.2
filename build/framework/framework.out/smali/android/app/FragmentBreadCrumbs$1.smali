.class Landroid/app/FragmentBreadCrumbs$1;
.super Ljava/lang/Object;
.source "FragmentBreadCrumbs.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/FragmentBreadCrumbs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/app/FragmentBreadCrumbs;


# direct methods
.method constructor <init>(Landroid/app/FragmentBreadCrumbs;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 338
    iput-object p1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter "v"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 340
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@4
    move-result-object v1

    #@5
    instance-of v1, v1, Landroid/app/FragmentManager$BackStackEntry;

    #@7
    if-eqz v1, :cond_26

    #@9
    .line 341
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/app/FragmentManager$BackStackEntry;

    #@f
    .line 342
    .local v0, bse:Landroid/app/FragmentManager$BackStackEntry;
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@11
    iget-object v1, v1, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    #@13
    if-ne v0, v1, :cond_27

    #@15
    .line 343
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@17
    invoke-static {v1}, Landroid/app/FragmentBreadCrumbs;->access$000(Landroid/app/FragmentBreadCrumbs;)Landroid/view/View$OnClickListener;

    #@1a
    move-result-object v1

    #@1b
    if-eqz v1, :cond_26

    #@1d
    .line 344
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@1f
    invoke-static {v1}, Landroid/app/FragmentBreadCrumbs;->access$000(Landroid/app/FragmentBreadCrumbs;)Landroid/view/View$OnClickListener;

    #@22
    move-result-object v1

    #@23
    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    #@26
    .line 361
    .end local v0           #bse:Landroid/app/FragmentManager$BackStackEntry;
    :cond_26
    :goto_26
    return-void

    #@27
    .line 347
    .restart local v0       #bse:Landroid/app/FragmentManager$BackStackEntry;
    :cond_27
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@29
    invoke-static {v1}, Landroid/app/FragmentBreadCrumbs;->access$100(Landroid/app/FragmentBreadCrumbs;)Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    #@2c
    move-result-object v1

    #@2d
    if-eqz v1, :cond_42

    #@2f
    .line 348
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@31
    invoke-static {v1}, Landroid/app/FragmentBreadCrumbs;->access$100(Landroid/app/FragmentBreadCrumbs;)Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    #@34
    move-result-object v2

    #@35
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@37
    iget-object v1, v1, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@39
    if-ne v0, v1, :cond_54

    #@3b
    const/4 v1, 0x0

    #@3c
    :goto_3c
    invoke-interface {v2, v1, v3}, Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;->onBreadCrumbClick(Landroid/app/FragmentManager$BackStackEntry;I)Z

    #@3f
    move-result v1

    #@40
    if-nez v1, :cond_26

    #@42
    .line 353
    :cond_42
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@44
    iget-object v1, v1, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    #@46
    if-ne v0, v1, :cond_56

    #@48
    .line 355
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@4a
    iget-object v1, v1, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    #@4c
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    #@53
    goto :goto_26

    #@54
    :cond_54
    move-object v1, v0

    #@55
    .line 348
    goto :goto_3c

    #@56
    .line 357
    :cond_56
    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs$1;->this$0:Landroid/app/FragmentBreadCrumbs;

    #@58
    iget-object v1, v1, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    #@5a
    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    #@5d
    move-result-object v1

    #@5e
    invoke-interface {v0}, Landroid/app/FragmentManager$BackStackEntry;->getId()I

    #@61
    move-result v2

    #@62
    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentManager;->popBackStack(II)V

    #@65
    goto :goto_26
.end method
