.class public Landroid/app/DialogFragment;
.super Landroid/app/Fragment;
.source "DialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static final SAVED_BACK_STACK_ID:Ljava/lang/String; = "android:backStackId"

.field private static final SAVED_CANCELABLE:Ljava/lang/String; = "android:cancelable"

.field private static final SAVED_DIALOG_STATE_TAG:Ljava/lang/String; = "android:savedDialogState"

.field private static final SAVED_SHOWS_DIALOG:Ljava/lang/String; = "android:showsDialog"

.field private static final SAVED_STYLE:Ljava/lang/String; = "android:style"

.field private static final SAVED_THEME:Ljava/lang/String; = "android:theme"

.field public static final STYLE_NORMAL:I = 0x0

.field public static final STYLE_NO_FRAME:I = 0x2

.field public static final STYLE_NO_INPUT:I = 0x3

.field public static final STYLE_NO_TITLE:I = 0x1


# instance fields
.field mBackStackId:I

.field mCancelable:Z

.field mDialog:Landroid/app/Dialog;

.field mDismissed:Z

.field mShownByMe:Z

.field mShowsDialog:Z

.field mStyle:I

.field mTheme:I

.field mViewDestroyed:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 188
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    #@5
    .line 177
    iput v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@7
    .line 178
    iput v0, p0, Landroid/app/DialogFragment;->mTheme:I

    #@9
    .line 179
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@b
    .line 180
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@d
    .line 181
    const/4 v0, -0x1

    #@e
    iput v0, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@10
    .line 189
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    #@0
    .prologue
    .line 259
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/app/DialogFragment;->dismissInternal(Z)V

    #@4
    .line 260
    return-void
.end method

.method public dismissAllowingStateLoss()V
    .registers 2

    #@0
    .prologue
    .line 269
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/app/DialogFragment;->dismissInternal(Z)V

    #@4
    .line 270
    return-void
.end method

.method dismissInternal(Z)V
    .registers 6
    .parameter "allowStateLoss"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 273
    iget-boolean v1, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 296
    :goto_5
    return-void

    #@6
    .line 276
    :cond_6
    iput-boolean v3, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@8
    .line 277
    const/4 v1, 0x0

    #@9
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@b
    .line 278
    iget-object v1, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@d
    if-eqz v1, :cond_17

    #@f
    .line 279
    iget-object v1, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@11
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    #@14
    .line 280
    const/4 v1, 0x0

    #@15
    iput-object v1, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@17
    .line 282
    :cond_17
    iput-boolean v3, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@19
    .line 283
    iget v1, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@1b
    if-ltz v1, :cond_2a

    #@1d
    .line 284
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    #@20
    move-result-object v1

    #@21
    iget v2, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@23
    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentManager;->popBackStack(II)V

    #@26
    .line 286
    const/4 v1, -0x1

    #@27
    iput v1, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@29
    goto :goto_5

    #@2a
    .line 288
    :cond_2a
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@31
    move-result-object v0

    #@32
    .line 289
    .local v0, ft:Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    #@35
    .line 290
    if-eqz p1, :cond_3b

    #@37
    .line 291
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    #@3a
    goto :goto_5

    #@3b
    .line 293
    :cond_3b
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    #@3e
    goto :goto_5
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 552
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Fragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@3
    .line 553
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6
    const-string v0, "DialogFragment:"

    #@8
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b
    .line 554
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e
    const-string v0, "  mStyle="

    #@10
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    iget v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@15
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@18
    .line 555
    const-string v0, " mTheme=0x"

    #@1a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d
    iget v0, p0, Landroid/app/DialogFragment;->mTheme:I

    #@1f
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@26
    .line 556
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    const-string v0, "  mCancelable="

    #@2b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@30
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@33
    .line 557
    const-string v0, " mShowsDialog="

    #@35
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@3a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@3d
    .line 558
    const-string v0, " mBackStackId="

    #@3f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42
    iget v0, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@44
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@47
    .line 559
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    const-string v0, "  mDialog="

    #@4c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4f
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@51
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@54
    .line 560
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@57
    const-string v0, "  mViewDestroyed="

    #@59
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5c
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@5e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@61
    .line 561
    const-string v0, " mDismissed="

    #@63
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@66
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@68
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@6b
    .line 562
    const-string v0, " mShownByMe="

    #@6d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@70
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@72
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@75
    .line 563
    return-void
.end method

.method public getDialog()Landroid/app/Dialog;
    .registers 2

    #@0
    .prologue
    .line 299
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@2
    return-object v0
.end method

.method public getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 395
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@2
    if-nez v0, :cond_9

    #@4
    .line 396
    invoke-super {p0, p1}, Landroid/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    .line 414
    :goto_8
    return-object v0

    #@9
    .line 399
    :cond_9
    invoke-virtual {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@f
    .line 400
    iget v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@11
    packed-switch v0, :pswitch_data_46

    #@14
    .line 410
    :goto_14
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@16
    if-eqz v0, :cond_3a

    #@18
    .line 411
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@1a
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    #@1d
    move-result-object v0

    #@1e
    const-string/jumbo v1, "layout_inflater"

    #@21
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Landroid/view/LayoutInflater;

    #@27
    goto :goto_8

    #@28
    .line 402
    :pswitch_28
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@2a
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@2d
    move-result-object v0

    #@2e
    const/16 v1, 0x18

    #@30
    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    #@33
    .line 408
    :pswitch_33
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@35
    const/4 v1, 0x1

    #@36
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    #@39
    goto :goto_14

    #@3a
    .line 414
    :cond_3a
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@3c
    const-string/jumbo v1, "layout_inflater"

    #@3f
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@42
    move-result-object v0

    #@43
    check-cast v0, Landroid/view/LayoutInflater;

    #@45
    goto :goto_8

    #@46
    .line 400
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_33
        :pswitch_33
        :pswitch_28
    .end packed-switch
.end method

.method public getShowsDialog()Z
    .registers 2

    #@0
    .prologue
    .line 352
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@2
    return v0
.end method

.method public getTheme()I
    .registers 2

    #@0
    .prologue
    .line 303
    iget v0, p0, Landroid/app/DialogFragment;->mTheme:I

    #@2
    return v0
.end method

.method public isCancelable()Z
    .registers 2

    #@0
    .prologue
    .line 324
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@2
    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 459
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    #@3
    .line 461
    iget-boolean v2, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@5
    if-nez v2, :cond_8

    #@7
    .line 489
    :cond_7
    :goto_7
    return-void

    #@8
    .line 465
    :cond_8
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@a
    if-nez v2, :cond_15

    #@c
    .line 466
    const-string v2, "DialogFragment"

    #@e
    const-string/jumbo v3, "mDialog is null"

    #@11
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    goto :goto_7

    #@15
    .line 470
    :cond_15
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getView()Landroid/view/View;

    #@18
    move-result-object v1

    #@19
    .line 471
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_2e

    #@1b
    .line 472
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    #@1e
    move-result-object v2

    #@1f
    if-eqz v2, :cond_29

    #@21
    .line 473
    new-instance v2, Ljava/lang/IllegalStateException;

    #@23
    const-string v3, "DialogFragment can not be attached to a container view"

    #@25
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@28
    throw v2

    #@29
    .line 475
    :cond_29
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@2b
    invoke-virtual {v2, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    #@2e
    .line 477
    :cond_2e
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@30
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getActivity()Landroid/app/Activity;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    #@37
    .line 478
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@39
    iget-boolean v3, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@3b
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    #@3e
    .line 479
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@40
    const-string v3, "DialogFragment"

    #@42
    invoke-virtual {v2, v3, p0, p0}, Landroid/app/Dialog;->takeCancelAndDismissListeners(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnDismissListener;)Z

    #@45
    move-result v2

    #@46
    if-nez v2, :cond_50

    #@48
    .line 480
    new-instance v2, Ljava/lang/IllegalStateException;

    #@4a
    const-string v3, "You can not set Dialog\'s OnCancelListener or OnDismissListener"

    #@4c
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v2

    #@50
    .line 483
    :cond_50
    if-eqz p1, :cond_7

    #@52
    .line 484
    const-string v2, "android:savedDialogState"

    #@54
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@57
    move-result-object v0

    #@58
    .line 485
    .local v0, dialogState:Landroid/os/Bundle;
    if-eqz v0, :cond_7

    #@5a
    .line 486
    iget-object v2, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@5c
    invoke-virtual {v2, v0}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@5f
    goto :goto_7
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 3
    .parameter "activity"

    #@0
    .prologue
    .line 357
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    #@3
    .line 358
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@5
    if-nez v0, :cond_a

    #@7
    .line 361
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@a
    .line 363
    :cond_a
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter "dialog"

    #@0
    .prologue
    .line 445
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 378
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    #@5
    .line 380
    iget v0, p0, Landroid/app/Fragment;->mContainerId:I

    #@7
    if-nez v0, :cond_3a

    #@9
    move v0, v1

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@c
    .line 382
    if-eqz p1, :cond_39

    #@e
    .line 383
    const-string v0, "android:style"

    #@10
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@16
    .line 384
    const-string v0, "android:theme"

    #@18
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/app/DialogFragment;->mTheme:I

    #@1e
    .line 385
    const-string v0, "android:cancelable"

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@23
    move-result v0

    #@24
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@26
    .line 386
    const-string v0, "android:showsDialog"

    #@28
    iget-boolean v1, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@2a
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@2d
    move-result v0

    #@2e
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@30
    .line 387
    const-string v0, "android:backStackId"

    #@32
    const/4 v1, -0x1

    #@33
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@39
    .line 390
    :cond_39
    return-void

    #@3a
    :cond_3a
    move v0, v2

    #@3b
    .line 380
    goto :goto_a
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 441
    new-instance v0, Landroid/app/Dialog;

    #@2
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getActivity()Landroid/app/Activity;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {p0}, Landroid/app/DialogFragment;->getTheme()I

    #@9
    move-result v2

    #@a
    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    #@d
    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    #@0
    .prologue
    .line 539
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    #@3
    .line 540
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 544
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@a
    .line 545
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@c
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    #@f
    .line 546
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@12
    .line 548
    :cond_12
    return-void
.end method

.method public onDetach()V
    .registers 2

    #@0
    .prologue
    .line 367
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    #@3
    .line 368
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@5
    if-nez v0, :cond_e

    #@7
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@9
    if-nez v0, :cond_e

    #@b
    .line 372
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@e
    .line 374
    :cond_e
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "dialog"

    #@0
    .prologue
    .line 448
    iget-boolean v0, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@2
    if-nez v0, :cond_8

    #@4
    .line 453
    const/4 v0, 0x1

    #@5
    invoke-virtual {p0, v0}, Landroid/app/DialogFragment;->dismissInternal(Z)V

    #@8
    .line 455
    :cond_8
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    #@0
    .prologue
    .line 502
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 503
    iget-object v1, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@5
    if-eqz v1, :cond_14

    #@7
    .line 504
    iget-object v1, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@9
    invoke-virtual {v1}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@c
    move-result-object v0

    #@d
    .line 505
    .local v0, dialogState:Landroid/os/Bundle;
    if-eqz v0, :cond_14

    #@f
    .line 506
    const-string v1, "android:savedDialogState"

    #@11
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@14
    .line 509
    .end local v0           #dialogState:Landroid/os/Bundle;
    :cond_14
    iget v1, p0, Landroid/app/DialogFragment;->mStyle:I

    #@16
    if-eqz v1, :cond_1f

    #@18
    .line 510
    const-string v1, "android:style"

    #@1a
    iget v2, p0, Landroid/app/DialogFragment;->mStyle:I

    #@1c
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1f
    .line 512
    :cond_1f
    iget v1, p0, Landroid/app/DialogFragment;->mTheme:I

    #@21
    if-eqz v1, :cond_2a

    #@23
    .line 513
    const-string v1, "android:theme"

    #@25
    iget v2, p0, Landroid/app/DialogFragment;->mTheme:I

    #@27
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2a
    .line 515
    :cond_2a
    iget-boolean v1, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@2c
    if-nez v1, :cond_35

    #@2e
    .line 516
    const-string v1, "android:cancelable"

    #@30
    iget-boolean v2, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@32
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@35
    .line 518
    :cond_35
    iget-boolean v1, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@37
    if-nez v1, :cond_40

    #@39
    .line 519
    const-string v1, "android:showsDialog"

    #@3b
    iget-boolean v2, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@3d
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@40
    .line 521
    :cond_40
    iget v1, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@42
    const/4 v2, -0x1

    #@43
    if-eq v1, v2, :cond_4c

    #@45
    .line 522
    const-string v1, "android:backStackId"

    #@47
    iget v2, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@49
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@4c
    .line 524
    :cond_4c
    return-void
.end method

.method public onStart()V
    .registers 2

    #@0
    .prologue
    .line 493
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    #@3
    .line 494
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 495
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@a
    .line 496
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@c
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@f
    .line 498
    :cond_f
    return-void
.end method

.method public onStop()V
    .registers 2

    #@0
    .prologue
    .line 528
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    #@3
    .line 529
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 530
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@9
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    #@c
    .line 532
    :cond_c
    return-void
.end method

.method public setCancelable(Z)V
    .registers 3
    .parameter "cancelable"

    #@0
    .prologue
    .line 316
    iput-boolean p1, p0, Landroid/app/DialogFragment;->mCancelable:Z

    #@2
    .line 317
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/app/DialogFragment;->mDialog:Landroid/app/Dialog;

    #@8
    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    #@b
    .line 318
    :cond_b
    return-void
.end method

.method public setShowsDialog(Z)V
    .registers 2
    .parameter "showsDialog"

    #@0
    .prologue
    .line 345
    iput-boolean p1, p0, Landroid/app/DialogFragment;->mShowsDialog:Z

    #@2
    .line 346
    return-void
.end method

.method public setStyle(II)V
    .registers 5
    .parameter "style"
    .parameter "theme"

    #@0
    .prologue
    .line 206
    iput p1, p0, Landroid/app/DialogFragment;->mStyle:I

    #@2
    .line 207
    iget v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@4
    const/4 v1, 0x2

    #@5
    if-eq v0, v1, :cond_c

    #@7
    iget v0, p0, Landroid/app/DialogFragment;->mStyle:I

    #@9
    const/4 v1, 0x3

    #@a
    if-ne v0, v1, :cond_11

    #@c
    .line 208
    :cond_c
    const v0, 0x1030312

    #@f
    iput v0, p0, Landroid/app/DialogFragment;->mTheme:I

    #@11
    .line 210
    :cond_11
    if-eqz p2, :cond_15

    #@13
    .line 211
    iput p2, p0, Landroid/app/DialogFragment;->mTheme:I

    #@15
    .line 213
    :cond_15
    return-void
.end method

.method public show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    .registers 5
    .parameter "transaction"
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 244
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@3
    .line 245
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@6
    .line 246
    invoke-virtual {p1, p0, p2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    #@9
    .line 247
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mViewDestroyed:Z

    #@b
    .line 248
    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commit()I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@11
    .line 249
    iget v0, p0, Landroid/app/DialogFragment;->mBackStackId:I

    #@13
    return v0
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .registers 5
    .parameter "manager"
    .parameter "tag"

    #@0
    .prologue
    .line 227
    const/4 v1, 0x0

    #@1
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mDismissed:Z

    #@3
    .line 228
    const/4 v1, 0x1

    #@4
    iput-boolean v1, p0, Landroid/app/DialogFragment;->mShownByMe:Z

    #@6
    .line 229
    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    #@9
    move-result-object v0

    #@a
    .line 230
    .local v0, ft:Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p0, p2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    #@d
    .line 231
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    #@10
    .line 232
    return-void
.end method
