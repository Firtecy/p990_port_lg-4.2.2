.class final Landroid/app/BackStackRecord;
.super Landroid/app/FragmentTransaction;
.source "BackStackRecord.java"

# interfaces
.implements Landroid/app/FragmentManager$BackStackEntry;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/BackStackRecord$Op;
    }
.end annotation


# static fields
.field static final OP_ADD:I = 0x1

.field static final OP_ATTACH:I = 0x7

.field static final OP_DETACH:I = 0x6

.field static final OP_HIDE:I = 0x4

.field static final OP_NULL:I = 0x0

.field static final OP_REMOVE:I = 0x3

.field static final OP_REPLACE:I = 0x2

.field static final OP_SHOW:I = 0x5

.field static final TAG:Ljava/lang/String; = "FragmentManager"


# instance fields
.field mAddToBackStack:Z

.field mAllowAddToBackStack:Z

.field mBreadCrumbShortTitleRes:I

.field mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

.field mBreadCrumbTitleRes:I

.field mBreadCrumbTitleText:Ljava/lang/CharSequence;

.field mCommitted:Z

.field mEnterAnim:I

.field mExitAnim:I

.field mHead:Landroid/app/BackStackRecord$Op;

.field mIndex:I

.field final mManager:Landroid/app/FragmentManagerImpl;

.field mName:Ljava/lang/String;

.field mNumOp:I

.field mPopEnterAnim:I

.field mPopExitAnim:I

.field mTail:Landroid/app/BackStackRecord$Op;

.field mTransition:I

.field mTransitionStyle:I


# direct methods
.method public constructor <init>(Landroid/app/FragmentManagerImpl;)V
    .registers 3
    .parameter "manager"

    #@0
    .prologue
    .line 333
    invoke-direct {p0}, Landroid/app/FragmentTransaction;-><init>()V

    #@3
    .line 209
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    #@6
    .line 212
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@9
    .line 334
    iput-object p1, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@b
    .line 335
    return-void
.end method

.method private doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V
    .registers 9
    .parameter "containerViewId"
    .parameter "fragment"
    .parameter "tag"
    .parameter "opcmd"

    #@0
    .prologue
    .line 394
    iget-object v1, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@2
    iput-object v1, p2, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@4
    .line 396
    if-eqz p3, :cond_43

    #@6
    .line 397
    iget-object v1, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@8
    if-eqz v1, :cond_41

    #@a
    iget-object v1, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@c
    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_41

    #@12
    .line 398
    new-instance v1, Ljava/lang/IllegalStateException;

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Can\'t change tag of fragment "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, ": was "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    iget-object v3, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " now "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@40
    throw v1

    #@41
    .line 402
    :cond_41
    iput-object p3, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@43
    .line 405
    :cond_43
    if-eqz p1, :cond_80

    #@45
    .line 406
    iget v1, p2, Landroid/app/Fragment;->mFragmentId:I

    #@47
    if-eqz v1, :cond_7c

    #@49
    iget v1, p2, Landroid/app/Fragment;->mFragmentId:I

    #@4b
    if-eq v1, p1, :cond_7c

    #@4d
    .line 407
    new-instance v1, Ljava/lang/IllegalStateException;

    #@4f
    new-instance v2, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "Can\'t change container ID of fragment "

    #@56
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    const-string v3, ": was "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    iget v3, p2, Landroid/app/Fragment;->mFragmentId:I

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    const-string v3, " now "

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v2

    #@70
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@7b
    throw v1

    #@7c
    .line 411
    :cond_7c
    iput p1, p2, Landroid/app/Fragment;->mFragmentId:I

    #@7e
    iput p1, p2, Landroid/app/Fragment;->mContainerId:I

    #@80
    .line 414
    :cond_80
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@82
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@85
    .line 415
    .local v0, op:Landroid/app/BackStackRecord$Op;
    iput p4, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@87
    .line 416
    iput-object p2, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@89
    .line 417
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@8c
    .line 418
    return-void
.end method


# virtual methods
.method public add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 5
    .parameter "containerViewId"
    .parameter "fragment"

    #@0
    .prologue
    .line 384
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    #@5
    .line 385
    return-object p0
.end method

.method public add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .registers 5
    .parameter "containerViewId"
    .parameter "fragment"
    .parameter "tag"

    #@0
    .prologue
    .line 389
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    #@4
    .line 390
    return-object p0
.end method

.method public add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .registers 5
    .parameter "fragment"
    .parameter "tag"

    #@0
    .prologue
    .line 379
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, v0, p1, p2, v1}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    #@5
    .line 380
    return-object p0
.end method

.method addOp(Landroid/app/BackStackRecord$Op;)V
    .registers 3
    .parameter "op"

    #@0
    .prologue
    .line 364
    iget-object v0, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@2
    if-nez v0, :cond_1f

    #@4
    .line 365
    iput-object p1, p0, Landroid/app/BackStackRecord;->mTail:Landroid/app/BackStackRecord$Op;

    #@6
    iput-object p1, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@8
    .line 371
    :goto_8
    iget v0, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    #@a
    iput v0, p1, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@c
    .line 372
    iget v0, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    #@e
    iput v0, p1, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@10
    .line 373
    iget v0, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    #@12
    iput v0, p1, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@14
    .line 374
    iget v0, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    #@16
    iput v0, p1, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@18
    .line 375
    iget v0, p0, Landroid/app/BackStackRecord;->mNumOp:I

    #@1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    iput v0, p0, Landroid/app/BackStackRecord;->mNumOp:I

    #@1e
    .line 376
    return-void

    #@1f
    .line 367
    :cond_1f
    iget-object v0, p0, Landroid/app/BackStackRecord;->mTail:Landroid/app/BackStackRecord$Op;

    #@21
    iput-object v0, p1, Landroid/app/BackStackRecord$Op;->prev:Landroid/app/BackStackRecord$Op;

    #@23
    .line 368
    iget-object v0, p0, Landroid/app/BackStackRecord;->mTail:Landroid/app/BackStackRecord$Op;

    #@25
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@27
    .line 369
    iput-object p1, p0, Landroid/app/BackStackRecord;->mTail:Landroid/app/BackStackRecord$Op;

    #@29
    goto :goto_8
.end method

.method public addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 502
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 503
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 506
    :cond_c
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@f
    .line 507
    iput-object p1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@11
    .line 508
    return-object p0
.end method

.method public attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "fragment"

    #@0
    .prologue
    .line 470
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@5
    .line 471
    .local v0, op:Landroid/app/BackStackRecord$Op;
    const/4 v1, 0x7

    #@6
    iput v1, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@8
    .line 472
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a
    .line 473
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@d
    .line 475
    return-object p0
.end method

.method bumpBackStackNesting(I)V
    .registers 8
    .parameter "amt"

    #@0
    .prologue
    .line 549
    iget-boolean v3, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@2
    if-nez v3, :cond_5

    #@4
    .line 571
    :cond_4
    return-void

    #@5
    .line 552
    :cond_5
    sget-boolean v3, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@7
    if-eqz v3, :cond_2b

    #@9
    const-string v3, "FragmentManager"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "Bump nesting in "

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, " by "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 554
    :cond_2b
    iget-object v1, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@2d
    .line 555
    .local v1, op:Landroid/app/BackStackRecord$Op;
    :goto_2d
    if-eqz v1, :cond_4

    #@2f
    .line 556
    iget-object v3, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@31
    if-eqz v3, :cond_66

    #@33
    .line 557
    iget-object v3, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@35
    iget v4, v3, Landroid/app/Fragment;->mBackStackNesting:I

    #@37
    add-int/2addr v4, p1

    #@38
    iput v4, v3, Landroid/app/Fragment;->mBackStackNesting:I

    #@3a
    .line 558
    sget-boolean v3, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@3c
    if-eqz v3, :cond_66

    #@3e
    const-string v3, "FragmentManager"

    #@40
    new-instance v4, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v5, "Bump nesting of "

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    iget-object v5, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    const-string v5, " to "

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    iget-object v5, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@59
    iget v5, v5, Landroid/app/Fragment;->mBackStackNesting:I

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 561
    :cond_66
    iget-object v3, v1, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@68
    if-eqz v3, :cond_ac

    #@6a
    .line 562
    iget-object v3, v1, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@6c
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@6f
    move-result v3

    #@70
    add-int/lit8 v0, v3, -0x1

    #@72
    .local v0, i:I
    :goto_72
    if-ltz v0, :cond_ac

    #@74
    .line 563
    iget-object v3, v1, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@76
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@79
    move-result-object v2

    #@7a
    check-cast v2, Landroid/app/Fragment;

    #@7c
    .line 564
    .local v2, r:Landroid/app/Fragment;
    iget v3, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@7e
    add-int/2addr v3, p1

    #@7f
    iput v3, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@81
    .line 565
    sget-boolean v3, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@83
    if-eqz v3, :cond_a9

    #@85
    const-string v3, "FragmentManager"

    #@87
    new-instance v4, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v5, "Bump nesting of "

    #@8e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    const-string v5, " to "

    #@98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    iget v5, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@9e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v4

    #@a6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 562
    :cond_a9
    add-int/lit8 v0, v0, -0x1

    #@ab
    goto :goto_72

    #@ac
    .line 569
    .end local v0           #i:I
    .end local v2           #r:Landroid/app/Fragment;
    :cond_ac
    iget-object v1, v1, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@ae
    goto/16 :goto_2d
.end method

.method public commit()I
    .registers 2

    #@0
    .prologue
    .line 574
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->commitInternal(Z)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public commitAllowingStateLoss()I
    .registers 2

    #@0
    .prologue
    .line 578
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->commitInternal(Z)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method commitInternal(Z)I
    .registers 8
    .parameter "allowStateLoss"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 582
    iget-boolean v2, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    #@3
    if-eqz v2, :cond_d

    #@5
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    const-string v3, "commit already called"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 583
    :cond_d
    sget-boolean v2, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@f
    if-eqz v2, :cond_3b

    #@11
    .line 584
    const-string v2, "FragmentManager"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Commit: "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 585
    new-instance v0, Landroid/util/LogWriter;

    #@2b
    const/4 v2, 0x2

    #@2c
    const-string v3, "FragmentManager"

    #@2e
    invoke-direct {v0, v2, v3}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    #@31
    .line 586
    .local v0, logw:Landroid/util/LogWriter;
    new-instance v1, Ljava/io/PrintWriter;

    #@33
    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@36
    .line 587
    .local v1, pw:Ljava/io/PrintWriter;
    const-string v2, "  "

    #@38
    invoke-virtual {p0, v2, v5, v1, v5}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@3b
    .line 589
    .end local v0           #logw:Landroid/util/LogWriter;
    .end local v1           #pw:Ljava/io/PrintWriter;
    :cond_3b
    const/4 v2, 0x1

    #@3c
    iput-boolean v2, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    #@3e
    .line 590
    iget-boolean v2, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@40
    if-eqz v2, :cond_52

    #@42
    .line 591
    iget-object v2, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@44
    invoke-virtual {v2, p0}, Landroid/app/FragmentManagerImpl;->allocBackStackIndex(Landroid/app/BackStackRecord;)I

    #@47
    move-result v2

    #@48
    iput v2, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@4a
    .line 595
    :goto_4a
    iget-object v2, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@4c
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Ljava/lang/Runnable;Z)V

    #@4f
    .line 596
    iget v2, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@51
    return v2

    #@52
    .line 593
    :cond_52
    const/4 v2, -0x1

    #@53
    iput v2, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@55
    goto :goto_4a
.end method

.method public detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "fragment"

    #@0
    .prologue
    .line 461
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@5
    .line 462
    .local v0, op:Landroid/app/BackStackRecord$Op;
    const/4 v1, 0x6

    #@6
    iput v1, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@8
    .line 463
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a
    .line 464
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@d
    .line 466
    return-object p0
.end method

.method public disallowAddToBackStack()Landroid/app/FragmentTransaction;
    .registers 3

    #@0
    .prologue
    .line 516
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 517
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "This transaction is already being added to the back stack"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 520
    :cond_c
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    #@f
    .line 521
    return-object p0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 237
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, p3, v0}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    #@4
    .line 238
    return-void
.end method

.method dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .registers 11
    .parameter "prefix"
    .parameter "writer"
    .parameter "full"

    #@0
    .prologue
    .line 241
    if-eqz p3, :cond_e0

    #@2
    .line 242
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    const-string/jumbo v5, "mName="

    #@8
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b
    iget-object v5, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@d
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10
    .line 243
    const-string v5, " mIndex="

    #@12
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15
    iget v5, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@17
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(I)V

    #@1a
    .line 244
    const-string v5, " mCommitted="

    #@1c
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f
    iget-boolean v5, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    #@21
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Z)V

    #@24
    .line 245
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@26
    if-eqz v5, :cond_48

    #@28
    .line 246
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    const-string/jumbo v5, "mTransition=#"

    #@2e
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31
    .line 247
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@33
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a
    .line 248
    const-string v5, " mTransitionStyle=#"

    #@3c
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f
    .line 249
    iget v5, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@41
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@48
    .line 251
    :cond_48
    iget v5, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    #@4a
    if-nez v5, :cond_50

    #@4c
    iget v5, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    #@4e
    if-eqz v5, :cond_70

    #@50
    .line 252
    :cond_50
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@53
    const-string/jumbo v5, "mEnterAnim=#"

    #@56
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@59
    .line 253
    iget v5, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    #@5b
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@62
    .line 254
    const-string v5, " mExitAnim=#"

    #@64
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@67
    .line 255
    iget v5, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    #@69
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@70
    .line 257
    :cond_70
    iget v5, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    #@72
    if-nez v5, :cond_78

    #@74
    iget v5, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    #@76
    if-eqz v5, :cond_98

    #@78
    .line 258
    :cond_78
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7b
    const-string/jumbo v5, "mPopEnterAnim=#"

    #@7e
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@81
    .line 259
    iget v5, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    #@83
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8a
    .line 260
    const-string v5, " mPopExitAnim=#"

    #@8c
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    .line 261
    iget v5, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    #@91
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@98
    .line 263
    :cond_98
    iget v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@9a
    if-nez v5, :cond_a0

    #@9c
    iget-object v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@9e
    if-eqz v5, :cond_bc

    #@a0
    .line 264
    :cond_a0
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a3
    const-string/jumbo v5, "mBreadCrumbTitleRes=#"

    #@a6
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a9
    .line 265
    iget v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@ab
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ae
    move-result-object v5

    #@af
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b2
    .line 266
    const-string v5, " mBreadCrumbTitleText="

    #@b4
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b7
    .line 267
    iget-object v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@b9
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@bc
    .line 269
    :cond_bc
    iget v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@be
    if-nez v5, :cond_c4

    #@c0
    iget-object v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@c2
    if-eqz v5, :cond_e0

    #@c4
    .line 270
    :cond_c4
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c7
    const-string/jumbo v5, "mBreadCrumbShortTitleRes=#"

    #@ca
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cd
    .line 271
    iget v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@cf
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d2
    move-result-object v5

    #@d3
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d6
    .line 272
    const-string v5, " mBreadCrumbShortTitleText="

    #@d8
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@db
    .line 273
    iget-object v5, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@dd
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@e0
    .line 277
    :cond_e0
    iget-object v5, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@e2
    if-eqz v5, :cond_1fc

    #@e4
    .line 278
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e7
    const-string v5, "Operations:"

    #@e9
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ec
    .line 279
    new-instance v5, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v5

    #@f5
    const-string v6, "    "

    #@f7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v5

    #@fb
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v2

    #@ff
    .line 280
    .local v2, innerPrefix:Ljava/lang/String;
    iget-object v4, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@101
    .line 281
    .local v4, op:Landroid/app/BackStackRecord$Op;
    const/4 v3, 0x0

    #@102
    .line 282
    .local v3, num:I
    :goto_102
    if-eqz v4, :cond_1fc

    #@104
    .line 284
    iget v5, v4, Landroid/app/BackStackRecord$Op;->cmd:I

    #@106
    packed-switch v5, :pswitch_data_1fe

    #@109
    .line 293
    new-instance v5, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v6, "cmd="

    #@110
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v5

    #@114
    iget v6, v4, Landroid/app/BackStackRecord$Op;->cmd:I

    #@116
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@119
    move-result-object v5

    #@11a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11d
    move-result-object v0

    #@11e
    .line 295
    .local v0, cmdStr:Ljava/lang/String;
    :goto_11e
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@121
    const-string v5, "  Op #"

    #@123
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@126
    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(I)V

    #@129
    .line 296
    const-string v5, ": "

    #@12b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@131
    .line 297
    const-string v5, " "

    #@133
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@136
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@138
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@13b
    .line 298
    if-eqz p3, :cond_18c

    #@13d
    .line 299
    iget v5, v4, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@13f
    if-nez v5, :cond_145

    #@141
    iget v5, v4, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@143
    if-eqz v5, :cond_164

    #@145
    .line 300
    :cond_145
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@148
    const-string v5, "enterAnim=#"

    #@14a
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14d
    .line 301
    iget v5, v4, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@14f
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@152
    move-result-object v5

    #@153
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@156
    .line 302
    const-string v5, " exitAnim=#"

    #@158
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15b
    .line 303
    iget v5, v4, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@15d
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@160
    move-result-object v5

    #@161
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@164
    .line 305
    :cond_164
    iget v5, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@166
    if-nez v5, :cond_16c

    #@168
    iget v5, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@16a
    if-eqz v5, :cond_18c

    #@16c
    .line 306
    :cond_16c
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16f
    const-string/jumbo v5, "popEnterAnim=#"

    #@172
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@175
    .line 307
    iget v5, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@177
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@17a
    move-result-object v5

    #@17b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17e
    .line 308
    const-string v5, " popExitAnim=#"

    #@180
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@183
    .line 309
    iget v5, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@185
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@188
    move-result-object v5

    #@189
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@18c
    .line 312
    :cond_18c
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@18e
    if-eqz v5, :cond_1f6

    #@190
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@192
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@195
    move-result v5

    #@196
    if-lez v5, :cond_1f6

    #@198
    .line 313
    const/4 v1, 0x0

    #@199
    .local v1, i:I
    :goto_199
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@19b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@19e
    move-result v5

    #@19f
    if-ge v1, v5, :cond_1f6

    #@1a1
    .line 314
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a4
    .line 315
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@1a6
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@1a9
    move-result v5

    #@1aa
    const/4 v6, 0x1

    #@1ab
    if-ne v5, v6, :cond_1de

    #@1ad
    .line 316
    const-string v5, "Removed: "

    #@1af
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b2
    .line 324
    :goto_1b2
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@1b4
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b7
    move-result-object v5

    #@1b8
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1bb
    .line 313
    add-int/lit8 v1, v1, 0x1

    #@1bd
    goto :goto_199

    #@1be
    .line 285
    .end local v0           #cmdStr:Ljava/lang/String;
    .end local v1           #i:I
    :pswitch_1be
    const-string v0, "NULL"

    #@1c0
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1c2
    .line 286
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1c2
    const-string v0, "ADD"

    #@1c4
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1c6
    .line 287
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1c6
    const-string v0, "REPLACE"

    #@1c8
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1ca
    .line 288
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1ca
    const-string v0, "REMOVE"

    #@1cc
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1ce
    .line 289
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1ce
    const-string v0, "HIDE"

    #@1d0
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1d2
    .line 290
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1d2
    const-string v0, "SHOW"

    #@1d4
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1d6
    .line 291
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1d6
    const-string v0, "DETACH"

    #@1d8
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1da
    .line 292
    .end local v0           #cmdStr:Ljava/lang/String;
    :pswitch_1da
    const-string v0, "ATTACH"

    #@1dc
    .restart local v0       #cmdStr:Ljava/lang/String;
    goto/16 :goto_11e

    #@1de
    .line 318
    .restart local v1       #i:I
    :cond_1de
    if-nez v1, :cond_1e5

    #@1e0
    .line 319
    const-string v5, "Removed:"

    #@1e2
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1e5
    .line 321
    :cond_1e5
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e8
    const-string v5, "  #"

    #@1ea
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ed
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1f0
    .line 322
    const-string v5, ": "

    #@1f2
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f5
    goto :goto_1b2

    #@1f6
    .line 327
    .end local v1           #i:I
    :cond_1f6
    iget-object v4, v4, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@1f8
    .line 328
    add-int/lit8 v3, v3, 0x1

    #@1fa
    .line 329
    goto/16 :goto_102

    #@1fc
    .line 331
    .end local v0           #cmdStr:Ljava/lang/String;
    .end local v2           #innerPrefix:Ljava/lang/String;
    .end local v3           #num:I
    .end local v4           #op:Landroid/app/BackStackRecord$Op;
    :cond_1fc
    return-void

    #@1fd
    .line 284
    nop

    #@1fe
    :pswitch_data_1fe
    .packed-switch 0x0
        :pswitch_1be
        :pswitch_1c2
        :pswitch_1c6
        :pswitch_1ca
        :pswitch_1ce
        :pswitch_1d2
        :pswitch_1d6
        :pswitch_1da
    .end packed-switch
.end method

.method public getBreadCrumbShortTitle()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 357
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 358
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@6
    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@8
    iget v1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@a
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    #@d
    move-result-object v0

    #@e
    .line 360
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@11
    goto :goto_e
.end method

.method public getBreadCrumbShortTitleRes()I
    .registers 2

    #@0
    .prologue
    .line 346
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@2
    return v0
.end method

.method public getBreadCrumbTitle()Ljava/lang/CharSequence;
    .registers 3

    #@0
    .prologue
    .line 350
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 351
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@6
    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@8
    iget v1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@a
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    #@d
    move-result-object v0

    #@e
    .line 353
    :goto_e
    return-object v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@11
    goto :goto_e
.end method

.method public getBreadCrumbTitleRes()I
    .registers 2

    #@0
    .prologue
    .line 342
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@2
    return v0
.end method

.method public getId()I
    .registers 2

    #@0
    .prologue
    .line 338
    iget v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@2
    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 775
    iget-object v0, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTransition()I
    .registers 2

    #@0
    .prologue
    .line 779
    iget v0, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@2
    return v0
.end method

.method public getTransitionStyle()I
    .registers 2

    #@0
    .prologue
    .line 783
    iget v0, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@2
    return v0
.end method

.method public hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "fragment"

    #@0
    .prologue
    .line 443
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@5
    .line 444
    .local v0, op:Landroid/app/BackStackRecord$Op;
    const/4 v1, 0x4

    #@6
    iput v1, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@8
    .line 445
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a
    .line 446
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@d
    .line 448
    return-object p0
.end method

.method public isAddToBackStackAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 512
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    #@2
    return v0
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 787
    iget v0, p0, Landroid/app/BackStackRecord;->mNumOp:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public popFromBackStack(Z)V
    .registers 14
    .parameter "doStateMove"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v11, -0x1

    #@3
    .line 691
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@5
    if-eqz v6, :cond_32

    #@7
    .line 692
    const-string v6, "FragmentManager"

    #@9
    new-instance v7, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v8, "popFromBackStack: "

    #@11
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v7

    #@15
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v7

    #@1d
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 693
    new-instance v2, Landroid/util/LogWriter;

    #@22
    const/4 v6, 0x2

    #@23
    const-string v7, "FragmentManager"

    #@25
    invoke-direct {v2, v6, v7}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    #@28
    .line 694
    .local v2, logw:Landroid/util/LogWriter;
    new-instance v5, Ljava/io/PrintWriter;

    #@2a
    invoke-direct {v5, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@2d
    .line 695
    .local v5, pw:Ljava/io/PrintWriter;
    const-string v6, "  "

    #@2f
    invoke-virtual {p0, v6, v10, v5, v10}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@32
    .line 698
    .end local v2           #logw:Landroid/util/LogWriter;
    .end local v5           #pw:Ljava/io/PrintWriter;
    :cond_32
    invoke-virtual {p0, v11}, Landroid/app/BackStackRecord;->bumpBackStackNesting(I)V

    #@35
    .line 700
    iget-object v4, p0, Landroid/app/BackStackRecord;->mTail:Landroid/app/BackStackRecord$Op;

    #@37
    .line 701
    .local v4, op:Landroid/app/BackStackRecord$Op;
    :goto_37
    if-eqz v4, :cond_102

    #@39
    .line 702
    iget v6, v4, Landroid/app/BackStackRecord$Op;->cmd:I

    #@3b
    packed-switch v6, :pswitch_data_124

    #@3e
    .line 756
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@40
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "Unknown cmd: "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    iget v8, v4, Landroid/app/BackStackRecord$Op;->cmd:I

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v7

    #@55
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@58
    throw v6

    #@59
    .line 704
    :pswitch_59
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@5b
    .line 705
    .local v0, f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@5d
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@5f
    .line 706
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@61
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@63
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@66
    move-result v7

    #@67
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@69
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;II)V

    #@6c
    .line 760
    :cond_6c
    :goto_6c
    iget-object v4, v4, Landroid/app/BackStackRecord$Op;->prev:Landroid/app/BackStackRecord$Op;

    #@6e
    goto :goto_37

    #@6f
    .line 711
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_6f
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@71
    .line 712
    .restart local v0       #f:Landroid/app/Fragment;
    if-eqz v0, :cond_84

    #@73
    .line 713
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@75
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@77
    .line 714
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@79
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@7b
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@7e
    move-result v7

    #@7f
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@81
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;II)V

    #@84
    .line 718
    :cond_84
    iget-object v6, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@86
    if-eqz v6, :cond_6c

    #@88
    .line 719
    const/4 v1, 0x0

    #@89
    .local v1, i:I
    :goto_89
    iget-object v6, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@8b
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@8e
    move-result v6

    #@8f
    if-ge v1, v6, :cond_6c

    #@91
    .line 720
    iget-object v6, v4, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@93
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@96
    move-result-object v3

    #@97
    check-cast v3, Landroid/app/Fragment;

    #@99
    .line 721
    .local v3, old:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@9b
    iput v6, v3, Landroid/app/Fragment;->mNextAnim:I

    #@9d
    .line 722
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@9f
    invoke-virtual {v6, v3, v9}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    #@a2
    .line 719
    add-int/lit8 v1, v1, 0x1

    #@a4
    goto :goto_89

    #@a5
    .line 727
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    .end local v3           #old:Landroid/app/Fragment;
    :pswitch_a5
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a7
    .line 728
    .restart local v0       #f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@a9
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@ab
    .line 729
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@ad
    invoke-virtual {v6, v0, v9}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    #@b0
    goto :goto_6c

    #@b1
    .line 732
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_b1
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@b3
    .line 733
    .restart local v0       #f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@b5
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@b7
    .line 734
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@b9
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@bb
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@be
    move-result v7

    #@bf
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@c1
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->showFragment(Landroid/app/Fragment;II)V

    #@c4
    goto :goto_6c

    #@c5
    .line 738
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_c5
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@c7
    .line 739
    .restart local v0       #f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@c9
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@cb
    .line 740
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@cd
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@cf
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@d2
    move-result v7

    #@d3
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@d5
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->hideFragment(Landroid/app/Fragment;II)V

    #@d8
    goto :goto_6c

    #@d9
    .line 744
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_d9
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@db
    .line 745
    .restart local v0       #f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    #@dd
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@df
    .line 746
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@e1
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@e3
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@e6
    move-result v7

    #@e7
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@e9
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->attachFragment(Landroid/app/Fragment;II)V

    #@ec
    goto :goto_6c

    #@ed
    .line 750
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_ed
    iget-object v0, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@ef
    .line 751
    .restart local v0       #f:Landroid/app/Fragment;
    iget v6, v4, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    #@f1
    iput v6, v0, Landroid/app/Fragment;->mNextAnim:I

    #@f3
    .line 752
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@f5
    iget v7, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@f7
    invoke-static {v7}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@fa
    move-result v7

    #@fb
    iget v8, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@fd
    invoke-virtual {v6, v0, v7, v8}, Landroid/app/FragmentManagerImpl;->detachFragment(Landroid/app/Fragment;II)V

    #@100
    goto/16 :goto_6c

    #@102
    .line 763
    .end local v0           #f:Landroid/app/Fragment;
    :cond_102
    if-eqz p1, :cond_116

    #@104
    .line 764
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@106
    iget-object v7, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@108
    iget v7, v7, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@10a
    iget v8, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@10c
    invoke-static {v8}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    #@10f
    move-result v8

    #@110
    iget v9, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@112
    const/4 v10, 0x1

    #@113
    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/app/FragmentManagerImpl;->moveToState(IIIZ)V

    #@116
    .line 768
    :cond_116
    iget v6, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@118
    if-ltz v6, :cond_123

    #@11a
    .line 769
    iget-object v6, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@11c
    iget v7, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@11e
    invoke-virtual {v6, v7}, Landroid/app/FragmentManagerImpl;->freeBackStackIndex(I)V

    #@121
    .line 770
    iput v11, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@123
    .line 772
    :cond_123
    return-void

    #@124
    .line 702
    :pswitch_data_124
    .packed-switch 0x1
        :pswitch_59
        :pswitch_6f
        :pswitch_a5
        :pswitch_b1
        :pswitch_c5
        :pswitch_d9
        :pswitch_ed
    .end packed-switch
.end method

.method public remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "fragment"

    #@0
    .prologue
    .line 434
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@5
    .line 435
    .local v0, op:Landroid/app/BackStackRecord$Op;
    const/4 v1, 0x3

    #@6
    iput v1, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@8
    .line 436
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a
    .line 437
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@d
    .line 439
    return-object p0
.end method

.method public replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "containerViewId"
    .parameter "fragment"

    #@0
    .prologue
    .line 421
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/BackStackRecord;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .registers 6
    .parameter "containerViewId"
    .parameter "fragment"
    .parameter "tag"

    #@0
    .prologue
    .line 425
    if-nez p1, :cond_a

    #@2
    .line 426
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Must use non-zero containerViewId"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 429
    :cond_a
    const/4 v0, 0x2

    #@b
    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    #@e
    .line 430
    return-object p0
.end method

.method public run()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 600
    sget-boolean v4, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4
    if-eqz v4, :cond_1e

    #@6
    const-string v4, "FragmentManager"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "Run: "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 602
    :cond_1e
    iget-boolean v4, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@20
    if-eqz v4, :cond_2e

    #@22
    .line 603
    iget v4, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@24
    if-gez v4, :cond_2e

    #@26
    .line 604
    new-instance v4, Ljava/lang/IllegalStateException;

    #@28
    const-string v5, "addToBackStack() called after commit()"

    #@2a
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v4

    #@2e
    .line 608
    :cond_2e
    invoke-virtual {p0, v8}, Landroid/app/BackStackRecord;->bumpBackStackNesting(I)V

    #@31
    .line 610
    iget-object v3, p0, Landroid/app/BackStackRecord;->mHead:Landroid/app/BackStackRecord$Op;

    #@33
    .line 611
    .local v3, op:Landroid/app/BackStackRecord$Op;
    :goto_33
    if-eqz v3, :cond_168

    #@35
    .line 612
    iget v4, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    #@37
    packed-switch v4, :pswitch_data_180

    #@3a
    .line 675
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "Unknown cmd: "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    iget v6, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@54
    throw v4

    #@55
    .line 614
    :pswitch_55
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@57
    .line 615
    .local v0, f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@59
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@5b
    .line 616
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@5d
    invoke-virtual {v4, v0, v7}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    #@60
    .line 679
    :cond_60
    :goto_60
    iget-object v3, v3, Landroid/app/BackStackRecord$Op;->next:Landroid/app/BackStackRecord$Op;

    #@62
    goto :goto_33

    #@63
    .line 619
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_63
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@65
    .line 620
    .restart local v0       #f:Landroid/app/Fragment;
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@67
    iget-object v4, v4, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@69
    if-eqz v4, :cond_106

    #@6b
    .line 621
    const/4 v1, 0x0

    #@6c
    .local v1, i:I
    :goto_6c
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@6e
    iget-object v4, v4, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@73
    move-result v4

    #@74
    if-ge v1, v4, :cond_106

    #@76
    .line 622
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@78
    iget-object v4, v4, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7d
    move-result-object v2

    #@7e
    check-cast v2, Landroid/app/Fragment;

    #@80
    .line 623
    .local v2, old:Landroid/app/Fragment;
    sget-boolean v4, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@82
    if-eqz v4, :cond_a6

    #@84
    const-string v4, "FragmentManager"

    #@86
    new-instance v5, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v6, "OP_REPLACE: adding="

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v5

    #@95
    const-string v6, " old="

    #@97
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v5

    #@9b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 625
    :cond_a6
    if-eqz v0, :cond_ae

    #@a8
    iget v4, v2, Landroid/app/Fragment;->mContainerId:I

    #@aa
    iget v5, v0, Landroid/app/Fragment;->mContainerId:I

    #@ac
    if-ne v4, v5, :cond_b3

    #@ae
    .line 626
    :cond_ae
    if-ne v2, v0, :cond_b6

    #@b0
    .line 627
    const/4 v0, 0x0

    #@b1
    iput-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@b3
    .line 621
    :cond_b3
    :goto_b3
    add-int/lit8 v1, v1, 0x1

    #@b5
    goto :goto_6c

    #@b6
    .line 629
    :cond_b6
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@b8
    if-nez v4, :cond_c1

    #@ba
    .line 630
    new-instance v4, Ljava/util/ArrayList;

    #@bc
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@bf
    iput-object v4, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@c1
    .line 632
    :cond_c1
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;

    #@c3
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c6
    .line 633
    iget v4, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@c8
    iput v4, v2, Landroid/app/Fragment;->mNextAnim:I

    #@ca
    .line 634
    iget-boolean v4, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@cc
    if-eqz v4, :cond_fc

    #@ce
    .line 635
    iget v4, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@d0
    add-int/lit8 v4, v4, 0x1

    #@d2
    iput v4, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@d4
    .line 636
    sget-boolean v4, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@d6
    if-eqz v4, :cond_fc

    #@d8
    const-string v4, "FragmentManager"

    #@da
    new-instance v5, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v6, "Bump nesting of "

    #@e1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v5

    #@e5
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v5

    #@e9
    const-string v6, " to "

    #@eb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    iget v6, v2, Landroid/app/Fragment;->mBackStackNesting:I

    #@f1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v5

    #@f5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f8
    move-result-object v5

    #@f9
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    .line 639
    :cond_fc
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@fe
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@100
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@102
    invoke-virtual {v4, v2, v5, v6}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;II)V

    #@105
    goto :goto_b3

    #@106
    .line 644
    .end local v1           #i:I
    .end local v2           #old:Landroid/app/Fragment;
    :cond_106
    if-eqz v0, :cond_60

    #@108
    .line 645
    iget v4, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@10a
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@10c
    .line 646
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@10e
    invoke-virtual {v4, v0, v7}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    #@111
    goto/16 :goto_60

    #@113
    .line 650
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_113
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@115
    .line 651
    .restart local v0       #f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@117
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@119
    .line 652
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@11b
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@11d
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@11f
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;II)V

    #@122
    goto/16 :goto_60

    #@124
    .line 655
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_124
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@126
    .line 656
    .restart local v0       #f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@128
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@12a
    .line 657
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@12c
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@12e
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@130
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/FragmentManagerImpl;->hideFragment(Landroid/app/Fragment;II)V

    #@133
    goto/16 :goto_60

    #@135
    .line 660
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_135
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@137
    .line 661
    .restart local v0       #f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@139
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@13b
    .line 662
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@13d
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@13f
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@141
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/FragmentManagerImpl;->showFragment(Landroid/app/Fragment;II)V

    #@144
    goto/16 :goto_60

    #@146
    .line 665
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_146
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@148
    .line 666
    .restart local v0       #f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    #@14a
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@14c
    .line 667
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@14e
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@150
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@152
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/FragmentManagerImpl;->detachFragment(Landroid/app/Fragment;II)V

    #@155
    goto/16 :goto_60

    #@157
    .line 670
    .end local v0           #f:Landroid/app/Fragment;
    :pswitch_157
    iget-object v0, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@159
    .line 671
    .restart local v0       #f:Landroid/app/Fragment;
    iget v4, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    #@15b
    iput v4, v0, Landroid/app/Fragment;->mNextAnim:I

    #@15d
    .line 672
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@15f
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@161
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@163
    invoke-virtual {v4, v0, v5, v6}, Landroid/app/FragmentManagerImpl;->attachFragment(Landroid/app/Fragment;II)V

    #@166
    goto/16 :goto_60

    #@168
    .line 682
    .end local v0           #f:Landroid/app/Fragment;
    :cond_168
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@16a
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@16c
    iget v5, v5, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@16e
    iget v6, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@170
    iget v7, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@172
    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/app/FragmentManagerImpl;->moveToState(IIIZ)V

    #@175
    .line 685
    iget-boolean v4, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    #@177
    if-eqz v4, :cond_17e

    #@179
    .line 686
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    #@17b
    invoke-virtual {v4, p0}, Landroid/app/FragmentManagerImpl;->addBackStackState(Landroid/app/BackStackRecord;)V

    #@17e
    .line 688
    :cond_17e
    return-void

    #@17f
    .line 612
    nop

    #@180
    :pswitch_data_180
    .packed-switch 0x1
        :pswitch_55
        :pswitch_63
        :pswitch_113
        :pswitch_124
        :pswitch_135
        :pswitch_146
        :pswitch_157
    .end packed-switch
.end method

.method public setBreadCrumbShortTitle(I)Landroid/app/FragmentTransaction;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 537
    iput p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@2
    .line 538
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@5
    .line 539
    return-object p0
.end method

.method public setBreadCrumbShortTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 543
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    #@3
    .line 544
    iput-object p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    #@5
    .line 545
    return-object p0
.end method

.method public setBreadCrumbTitle(I)Landroid/app/FragmentTransaction;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 525
    iput p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@2
    .line 526
    const/4 v0, 0x0

    #@3
    iput-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@5
    .line 527
    return-object p0
.end method

.method public setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 531
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    #@3
    .line 532
    iput-object p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    #@5
    .line 533
    return-object p0
.end method

.method public setCustomAnimations(II)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "enter"
    .parameter "exit"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 479
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/app/BackStackRecord;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public setCustomAnimations(IIII)Landroid/app/FragmentTransaction;
    .registers 5
    .parameter "enter"
    .parameter "exit"
    .parameter "popEnter"
    .parameter "popExit"

    #@0
    .prologue
    .line 484
    iput p1, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    #@2
    .line 485
    iput p2, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    #@4
    .line 486
    iput p3, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    #@6
    .line 487
    iput p4, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    #@8
    .line 488
    return-object p0
.end method

.method public setTransition(I)Landroid/app/FragmentTransaction;
    .registers 2
    .parameter "transition"

    #@0
    .prologue
    .line 492
    iput p1, p0, Landroid/app/BackStackRecord;->mTransition:I

    #@2
    .line 493
    return-object p0
.end method

.method public setTransitionStyle(I)Landroid/app/FragmentTransaction;
    .registers 2
    .parameter "styleRes"

    #@0
    .prologue
    .line 497
    iput p1, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    #@2
    .line 498
    return-object p0
.end method

.method public show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .registers 4
    .parameter "fragment"

    #@0
    .prologue
    .line 452
    new-instance v0, Landroid/app/BackStackRecord$Op;

    #@2
    invoke-direct {v0}, Landroid/app/BackStackRecord$Op;-><init>()V

    #@5
    .line 453
    .local v0, op:Landroid/app/BackStackRecord$Op;
    const/4 v1, 0x5

    #@6
    iput v1, v0, Landroid/app/BackStackRecord$Op;->cmd:I

    #@8
    .line 454
    iput-object p1, v0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    #@a
    .line 455
    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    #@d
    .line 457
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 222
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "BackStackEntry{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 223
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v1

    #@10
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 224
    iget v1, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@19
    if-ltz v1, :cond_25

    #@1b
    .line 225
    const-string v1, " #"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 226
    iget v1, p0, Landroid/app/BackStackRecord;->mIndex:I

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    .line 228
    :cond_25
    iget-object v1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@27
    if-eqz v1, :cond_33

    #@29
    .line 229
    const-string v1, " "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    .line 230
    iget-object v1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 232
    :cond_33
    const-string/jumbo v1, "}"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    .line 233
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    return-object v1
.end method
