.class public Landroid/app/ActivityManager;
.super Ljava/lang/Object;
.source "ActivityManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/ActivityManager$1;,
        Landroid/app/ActivityManager$RunningAppProcessInfo;,
        Landroid/app/ActivityManager$ProcessErrorStateInfo;,
        Landroid/app/ActivityManager$MemoryInfo;,
        Landroid/app/ActivityManager$RunningServiceInfo;,
        Landroid/app/ActivityManager$TaskThumbnails;,
        Landroid/app/ActivityManager$RunningTaskInfo;,
        Landroid/app/ActivityManager$RecentTaskInfo;
    }
.end annotation


# static fields
.field public static final BROADCAST_STICKY_CANT_HAVE_PERMISSION:I = -0x1

.field public static final BROADCAST_SUCCESS:I = 0x0

.field public static final COMPAT_MODE_ALWAYS:I = -0x1

.field public static final COMPAT_MODE_DISABLED:I = 0x0

.field public static final COMPAT_MODE_ENABLED:I = 0x1

.field public static final COMPAT_MODE_ENABLED_WVGA:I = 0x63

.field public static final COMPAT_MODE_NEVER:I = -0x2

.field public static final COMPAT_MODE_TOGGLE:I = 0x2

.field public static final COMPAT_MODE_UNKNOWN:I = -0x3

.field public static final INTENT_SENDER_ACTIVITY:I = 0x2

.field public static final INTENT_SENDER_ACTIVITY_RESULT:I = 0x3

.field public static final INTENT_SENDER_BROADCAST:I = 0x1

.field public static final INTENT_SENDER_SERVICE:I = 0x4

.field public static final MOVE_TASK_NO_USER_ACTION:I = 0x2

.field public static final MOVE_TASK_WITH_HOME:I = 0x1

.field public static final RECENT_IGNORE_UNAVAILABLE:I = 0x2

.field public static final RECENT_WITH_EXCLUDED:I = 0x1

.field public static final REMOVE_TASK_KILL_PROCESS:I = 0x1

.field public static final START_CANCELED:I = -0x6

.field public static final START_CLASS_NOT_FOUND:I = -0x2

.field public static final START_DELIVERED_TO_TOP:I = 0x3

.field public static final START_FLAG_AUTO_STOP_PROFILER:I = 0x8

.field public static final START_FLAG_DEBUG:I = 0x2

.field public static final START_FLAG_ONLY_IF_NEEDED:I = 0x1

.field public static final START_FLAG_OPENGL_TRACES:I = 0x4

.field public static final START_FORWARD_AND_REQUEST_CONFLICT:I = -0x3

.field public static final START_INTENT_NOT_RESOLVED:I = -0x1

.field public static final START_NOT_ACTIVITY:I = -0x5

.field public static final START_PERMISSION_DENIED:I = -0x4

.field public static final START_RETURN_INTENT_TO_CALLER:I = 0x1

.field public static final START_SUCCESS:I = 0x0

.field public static final START_SWITCHES_CANCELED:I = 0x4

.field public static final START_TASK_TO_FRONT:I = 0x2

.field private static TAG:Ljava/lang/String; = null

.field public static final USER_OP_IS_CURRENT:I = -0x2

.field public static final USER_OP_SUCCESS:I = 0x0

.field public static final USER_OP_UNKNOWN_USER:I = -0x1

.field private static localLOGV:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    const-string v0, "ActivityManager"

    #@2
    sput-object v0, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@4
    .line 62
    const/4 v0, 0x0

    #@5
    sput-boolean v0, Landroid/app/ActivityManager;->localLOGV:Z

    #@7
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 225
    iput-object p1, p0, Landroid/app/ActivityManager;->mContext:Landroid/content/Context;

    #@5
    .line 226
    iput-object p2, p0, Landroid/app/ActivityManager;->mHandler:Landroid/os/Handler;

    #@7
    .line 227
    return-void
.end method

.method public static checkComponentPermission(Ljava/lang/String;IIZ)I
    .registers 9
    .parameter "permission"
    .parameter "uid"
    .parameter "owningUid"
    .parameter "exported"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 1896
    if-eqz p1, :cond_8

    #@4
    const/16 v3, 0x3e8

    #@6
    if-ne p1, v3, :cond_a

    #@8
    :cond_8
    move v1, v2

    #@9
    .line 1923
    :cond_9
    :goto_9
    return v1

    #@a
    .line 1900
    :cond_a
    invoke-static {p1}, Landroid/os/UserHandle;->isIsolated(I)Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_9

    #@10
    .line 1905
    if-ltz p2, :cond_1a

    #@12
    invoke-static {p1, p2}, Landroid/os/UserHandle;->isSameApp(II)Z

    #@15
    move-result v3

    #@16
    if-eqz v3, :cond_1a

    #@18
    move v1, v2

    #@19
    .line 1906
    goto :goto_9

    #@1a
    .line 1909
    :cond_1a
    if-nez p3, :cond_35

    #@1c
    .line 1910
    sget-object v2, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "Permission denied: checkComponentPermission() owningUid="

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_9

    #@35
    .line 1913
    :cond_35
    if-nez p0, :cond_39

    #@37
    move v1, v2

    #@38
    .line 1914
    goto :goto_9

    #@39
    .line 1917
    :cond_39
    :try_start_39
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@3c
    move-result-object v2

    #@3d
    invoke-interface {v2, p0, p1}, Landroid/content/pm/IPackageManager;->checkUidPermission(Ljava/lang/String;I)I
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_40} :catch_42

    #@40
    move-result v1

    #@41
    goto :goto_9

    #@42
    .line 1919
    :catch_42
    move-exception v0

    #@43
    .line 1921
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@45
    const-string v3, "PackageManager is dead?!?"

    #@47
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_9
.end method

.method public static checkUidPermission(Ljava/lang/String;I)I
    .registers 5
    .parameter "permission"
    .parameter "uid"

    #@0
    .prologue
    .line 1929
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1}, Landroid/content/pm/IPackageManager;->checkUidPermission(Ljava/lang/String;I)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 1935
    :goto_8
    return v1

    #@9
    .line 1931
    :catch_9
    move-exception v0

    #@a
    .line 1933
    .local v0, e:Landroid/os/RemoteException;
    sget-object v1, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@c
    const-string v2, "PackageManager is dead?!?"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 1935
    const/4 v1, -0x1

    #@12
    goto :goto_8
.end method

.method public static getCurrentUser()I
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1956
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@8
    move-result-object v1

    #@9
    .line 1957
    .local v1, ui:Landroid/content/pm/UserInfo;
    if-eqz v1, :cond_d

    #@b
    iget v2, v1, Landroid/content/pm/UserInfo;->id:I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_d} :catch_e

    #@d
    .line 1959
    :cond_d
    :goto_d
    return v2

    #@e
    .line 1958
    :catch_e
    move-exception v0

    #@f
    .line 1959
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_d
.end method

.method public static getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
    .registers 2
    .parameter "outState"

    #@0
    .prologue
    .line 1675
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p0}, Landroid/app/IActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1678
    :goto_7
    return-void

    #@8
    .line 1676
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public static handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I
    .registers 16
    .parameter "callingPid"
    .parameter "callingUid"
    .parameter "userId"
    .parameter "allowAll"
    .parameter "requireFull"
    .parameter "name"
    .parameter "callerPackage"

    #@0
    .prologue
    .line 1941
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    #@3
    move-result v0

    #@4
    if-ne v0, p2, :cond_7

    #@6
    .line 1945
    .end local p2
    :goto_6
    return p2

    #@7
    .restart local p2
    :cond_7
    :try_start_7
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@a
    move-result-object v0

    #@b
    move v1, p0

    #@c
    move v2, p1

    #@d
    move v3, p2

    #@e
    move v4, p3

    #@f
    move v5, p4

    #@10
    move-object v6, p5

    #@11
    move-object v7, p6

    #@12
    invoke-interface/range {v0 .. v7}, Landroid/app/IActivityManager;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_15} :catch_17

    #@15
    move-result p2

    #@16
    goto :goto_6

    #@17
    .line 1947
    :catch_17
    move-exception v8

    #@18
    .line 1948
    .local v8, e:Landroid/os/RemoteException;
    new-instance v0, Ljava/lang/SecurityException;

    #@1a
    const-string v1, "Failed calling activity manager"

    #@1c
    invoke-direct {v0, v1, v8}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1f
    throw v0
.end method

.method public static isHighEndGfx()Z
    .registers 10

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 390
    new-instance v3, Lcom/android/internal/util/MemInfoReader;

    #@4
    invoke-direct {v3}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    #@7
    .line 391
    .local v3, reader:Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v3}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    #@a
    .line 392
    invoke-virtual {v3}, Lcom/android/internal/util/MemInfoReader;->getTotalSize()J

    #@d
    move-result-wide v6

    #@e
    const-wide/32 v8, 0x10000000

    #@11
    cmp-long v6, v6, v8

    #@13
    if-ltz v6, :cond_16

    #@15
    .line 408
    :cond_15
    :goto_15
    return v4

    #@16
    .line 398
    :cond_16
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v6, v5}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    #@1d
    move-result-object v0

    #@1e
    .line 400
    .local v0, display:Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    #@20
    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    #@23
    .line 401
    .local v1, p:Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    #@26
    .line 402
    iget v6, v1, Landroid/graphics/Point;->x:I

    #@28
    iget v7, v1, Landroid/graphics/Point;->y:I

    #@2a
    mul-int v2, v6, v7

    #@2c
    .line 403
    .local v2, pixels:I
    const v6, 0x96000

    #@2f
    if-ge v2, v6, :cond_15

    #@31
    move v4, v5

    #@32
    .line 408
    goto :goto_15
.end method

.method public static isLargeRAM()Z
    .registers 5

    #@0
    .prologue
    .line 419
    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    #@2
    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    #@5
    .line 420
    .local v0, reader:Lcom/android/internal/util/MemInfoReader;
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    #@8
    .line 421
    invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getTotalSize()J

    #@b
    move-result-wide v1

    #@c
    const-wide/32 v3, 0x28000000

    #@f
    cmp-long v1, v1, v3

    #@11
    if-ltz v1, :cond_15

    #@13
    .line 424
    const/4 v1, 0x1

    #@14
    .line 426
    :goto_14
    return v1

    #@15
    :cond_15
    const/4 v1, 0x0

    #@16
    goto :goto_14
.end method

.method public static isRunningInTestHarness()Z
    .registers 2

    #@0
    .prologue
    .line 1859
    const-string/jumbo v0, "ro.test_harness"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static isUserAMonkey()Z
    .registers 1

    #@0
    .prologue
    .line 1849
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/app/IActivityManager;->isUserAMonkey()Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v0

    #@8
    .line 1852
    :goto_8
    return v0

    #@9
    .line 1850
    :catch_9
    move-exception v0

    #@a
    .line 1852
    const/4 v0, 0x0

    #@b
    goto :goto_8
.end method

.method public static staticGetLargeMemoryClass()I
    .registers 3

    #@0
    .prologue
    .line 379
    const-string v1, "dalvik.vm.heapsize"

    #@2
    const-string v2, "16m"

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 380
    .local v0, vmHeapSize:Ljava/lang/String;
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@c
    move-result v2

    #@d
    add-int/lit8 v2, v2, -0x1

    #@f
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@16
    move-result v1

    #@17
    return v1
.end method

.method public static staticGetMemoryClass()I
    .registers 3

    #@0
    .prologue
    .line 351
    const-string v1, "dalvik.vm.heapgrowthlimit"

    #@2
    const-string v2, ""

    #@4
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 352
    .local v0, vmHeapSize:Ljava/lang/String;
    if-eqz v0, :cond_22

    #@a
    const-string v1, ""

    #@c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_22

    #@12
    .line 353
    const/4 v1, 0x0

    #@13
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@16
    move-result v2

    #@17
    add-int/lit8 v2, v2, -0x1

    #@19
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@20
    move-result v1

    #@21
    .line 355
    :goto_21
    return v1

    #@22
    :cond_22
    invoke-static {}, Landroid/app/ActivityManager;->staticGetLargeMemoryClass()I

    #@25
    move-result v1

    #@26
    goto :goto_21
.end method


# virtual methods
.method public bootAniEnd()V
    .registers 2

    #@0
    .prologue
    .line 2016
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/app/IActivityManager;->bootAniEnd()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 2019
    :goto_7
    return-void

    #@8
    .line 2017
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;)Z
    .registers 6
    .parameter "packageName"
    .parameter "observer"

    #@0
    .prologue
    .line 1258
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, v2}, Landroid/app/IActivityManager;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 1261
    :goto_c
    return v1

    #@d
    .line 1260
    :catch_d
    move-exception v0

    #@e
    .line 1261
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method public forceStopPackage(Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 1752
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v1

    #@8
    invoke-interface {v0, p1, v1}, Landroid/app/IActivityManager;->forceStopPackage(Ljava/lang/String;I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 1756
    :goto_b
    return-void

    #@c
    .line 1754
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public getAllPackageLaunchCounts()Ljava/util/Map;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1869
    :try_start_0
    const-string/jumbo v8, "usagestats"

    #@3
    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v8

    #@7
    invoke-static {v8}, Lcom/android/internal/app/IUsageStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IUsageStats;

    #@a
    move-result-object v7

    #@b
    .line 1871
    .local v7, usageStatsService:Lcom/android/internal/app/IUsageStats;
    if-nez v7, :cond_13

    #@d
    .line 1872
    new-instance v4, Ljava/util/HashMap;

    #@f
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@12
    .line 1888
    .end local v7           #usageStatsService:Lcom/android/internal/app/IUsageStats;
    :cond_12
    :goto_12
    return-object v4

    #@13
    .line 1875
    .restart local v7       #usageStatsService:Lcom/android/internal/app/IUsageStats;
    :cond_13
    invoke-interface {v7}, Lcom/android/internal/app/IUsageStats;->getAllPkgUsageStats()[Lcom/android/internal/os/PkgUsageStats;

    #@16
    move-result-object v0

    #@17
    .line 1876
    .local v0, allPkgUsageStats:[Lcom/android/internal/os/PkgUsageStats;
    if-nez v0, :cond_2d

    #@19
    .line 1877
    new-instance v4, Ljava/util/HashMap;

    #@1b
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_1e} :catch_1f

    #@1e
    goto :goto_12

    #@1f
    .line 1886
    .end local v0           #allPkgUsageStats:[Lcom/android/internal/os/PkgUsageStats;
    .end local v7           #usageStatsService:Lcom/android/internal/app/IUsageStats;
    :catch_1f
    move-exception v2

    #@20
    .line 1887
    .local v2, e:Landroid/os/RemoteException;
    sget-object v8, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@22
    const-string v9, "Could not query launch counts"

    #@24
    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    .line 1888
    new-instance v4, Ljava/util/HashMap;

    #@29
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@2c
    goto :goto_12

    #@2d
    .line 1880
    .end local v2           #e:Landroid/os/RemoteException;
    .restart local v0       #allPkgUsageStats:[Lcom/android/internal/os/PkgUsageStats;
    .restart local v7       #usageStatsService:Lcom/android/internal/app/IUsageStats;
    :cond_2d
    :try_start_2d
    new-instance v4, Ljava/util/HashMap;

    #@2f
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@32
    .line 1881
    .local v4, launchCounts:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object v1, v0

    #@33
    .local v1, arr$:[Lcom/android/internal/os/PkgUsageStats;
    array-length v5, v1

    #@34
    .local v5, len$:I
    const/4 v3, 0x0

    #@35
    .local v3, i$:I
    :goto_35
    if-ge v3, v5, :cond_12

    #@37
    aget-object v6, v1, v3

    #@39
    .line 1882
    .local v6, pkgUsageStats:Lcom/android/internal/os/PkgUsageStats;
    iget-object v8, v6, Lcom/android/internal/os/PkgUsageStats;->packageName:Ljava/lang/String;

    #@3b
    iget v9, v6, Lcom/android/internal/os/PkgUsageStats;->launchCount:I

    #@3d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@40
    move-result-object v9

    #@41
    invoke-interface {v4, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_2d .. :try_end_44} :catch_1f

    #@44
    .line 1881
    add-int/lit8 v3, v3, 0x1

    #@46
    goto :goto_35
.end method

.method public getAllPackageUsageStats()[Lcom/android/internal/os/PkgUsageStats;
    .registers 5

    #@0
    .prologue
    .line 1970
    :try_start_0
    const-string/jumbo v2, "usagestats"

    #@3
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v2

    #@7
    invoke-static {v2}, Lcom/android/internal/app/IUsageStats$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IUsageStats;

    #@a
    move-result-object v1

    #@b
    .line 1972
    .local v1, usageStatsService:Lcom/android/internal/app/IUsageStats;
    if-eqz v1, :cond_1a

    #@d
    .line 1973
    invoke-interface {v1}, Lcom/android/internal/app/IUsageStats;->getAllPkgUsageStats()[Lcom/android/internal/os/PkgUsageStats;
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_10} :catch_12

    #@10
    move-result-object v2

    #@11
    .line 1978
    .end local v1           #usageStatsService:Lcom/android/internal/app/IUsageStats;
    :goto_11
    return-object v2

    #@12
    .line 1975
    :catch_12
    move-exception v0

    #@13
    .line 1976
    .local v0, e:Landroid/os/RemoteException;
    sget-object v2, Landroid/app/ActivityManager;->TAG:Ljava/lang/String;

    #@15
    const-string v3, "Could not query usage stats"

    #@17
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 1978
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1a
    const/4 v2, 0x0

    #@1b
    new-array v2, v2, [Lcom/android/internal/os/PkgUsageStats;

    #@1d
    goto :goto_11
.end method

.method public getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;
    .registers 2

    #@0
    .prologue
    .line 1763
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Landroid/app/IActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v0

    #@8
    .line 1766
    :goto_8
    return-object v0

    #@9
    .line 1764
    :catch_9
    move-exception v0

    #@a
    .line 1766
    const/4 v0, 0x0

    #@b
    goto :goto_8
.end method

.method public getFrontActivityScreenCompatMode()I
    .registers 3

    #@0
    .prologue
    .line 281
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/app/IActivityManager;->getFrontActivityScreenCompatMode()I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 284
    :goto_8
    return v1

    #@9
    .line 282
    :catch_9
    move-exception v0

    #@a
    .line 284
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getLargeMemoryClass()I
    .registers 2

    #@0
    .prologue
    .line 372
    invoke-static {}, Landroid/app/ActivityManager;->staticGetLargeMemoryClass()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getLauncherLargeIconDensity()I
    .registers 6

    #@0
    .prologue
    const/16 v3, 0x140

    #@2
    .line 1776
    iget-object v4, p0, Landroid/app/ActivityManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@7
    move-result-object v1

    #@8
    .line 1777
    .local v1, res:Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@b
    move-result-object v4

    #@c
    iget v0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    #@e
    .line 1778
    .local v0, density:I
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@11
    move-result-object v4

    #@12
    iget v2, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@14
    .line 1780
    .local v2, sw:I
    const/16 v4, 0x258

    #@16
    if-ge v2, v4, :cond_19

    #@18
    .line 1801
    .end local v0           #density:I
    :goto_18
    return v0

    #@19
    .line 1785
    .restart local v0       #density:I
    :cond_19
    sparse-switch v0, :sswitch_data_36

    #@1c
    .line 1801
    int-to-float v3, v0

    #@1d
    const/high16 v4, 0x3fc0

    #@1f
    mul-float/2addr v3, v4

    #@20
    const/high16 v4, 0x3f00

    #@22
    add-float/2addr v3, v4

    #@23
    float-to-int v0, v3

    #@24
    goto :goto_18

    #@25
    .line 1787
    :sswitch_25
    const/16 v0, 0xa0

    #@27
    goto :goto_18

    #@28
    .line 1789
    :sswitch_28
    const/16 v0, 0xf0

    #@2a
    goto :goto_18

    #@2b
    :sswitch_2b
    move v0, v3

    #@2c
    .line 1791
    goto :goto_18

    #@2d
    :sswitch_2d
    move v0, v3

    #@2e
    .line 1793
    goto :goto_18

    #@2f
    .line 1795
    :sswitch_2f
    const/16 v0, 0x1e0

    #@31
    goto :goto_18

    #@32
    .line 1797
    :sswitch_32
    const/16 v0, 0x280

    #@34
    goto :goto_18

    #@35
    .line 1785
    nop

    #@36
    :sswitch_data_36
    .sparse-switch
        0x78 -> :sswitch_25
        0xa0 -> :sswitch_28
        0xd5 -> :sswitch_2b
        0xf0 -> :sswitch_2d
        0x140 -> :sswitch_2f
        0x1e0 -> :sswitch_32
    .end sparse-switch
.end method

.method public getLauncherLargeIconSize()I
    .registers 7

    #@0
    .prologue
    .line 1812
    iget-object v4, p0, Landroid/app/ActivityManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    .line 1813
    .local v1, res:Landroid/content/res/Resources;
    const/high16 v4, 0x105

    #@8
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@b
    move-result v2

    #@c
    .line 1814
    .local v2, size:I
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@f
    move-result-object v4

    #@10
    iget v3, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    #@12
    .line 1816
    .local v3, sw:I
    const/16 v4, 0x258

    #@14
    if-ge v3, v4, :cond_17

    #@16
    .line 1839
    .end local v2           #size:I
    :goto_16
    return v2

    #@17
    .line 1821
    .restart local v2       #size:I
    :cond_17
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1a
    move-result-object v4

    #@1b
    iget v0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    #@1d
    .line 1823
    .local v0, density:I
    sparse-switch v0, :sswitch_data_4a

    #@20
    .line 1839
    int-to-float v4, v2

    #@21
    const/high16 v5, 0x3fc0

    #@23
    mul-float/2addr v4, v5

    #@24
    const/high16 v5, 0x3f00

    #@26
    add-float/2addr v4, v5

    #@27
    float-to-int v2, v4

    #@28
    goto :goto_16

    #@29
    .line 1825
    :sswitch_29
    mul-int/lit16 v4, v2, 0xa0

    #@2b
    div-int/lit8 v2, v4, 0x78

    #@2d
    goto :goto_16

    #@2e
    .line 1827
    :sswitch_2e
    mul-int/lit16 v4, v2, 0xf0

    #@30
    div-int/lit16 v2, v4, 0xa0

    #@32
    goto :goto_16

    #@33
    .line 1829
    :sswitch_33
    mul-int/lit16 v4, v2, 0x140

    #@35
    div-int/lit16 v2, v4, 0xf0

    #@37
    goto :goto_16

    #@38
    .line 1831
    :sswitch_38
    mul-int/lit16 v4, v2, 0x140

    #@3a
    div-int/lit16 v2, v4, 0xf0

    #@3c
    goto :goto_16

    #@3d
    .line 1833
    :sswitch_3d
    mul-int/lit16 v4, v2, 0x1e0

    #@3f
    div-int/lit16 v2, v4, 0x140

    #@41
    goto :goto_16

    #@42
    .line 1835
    :sswitch_42
    mul-int/lit16 v4, v2, 0x140

    #@44
    mul-int/lit8 v4, v4, 0x2

    #@46
    div-int/lit16 v2, v4, 0x1e0

    #@48
    goto :goto_16

    #@49
    .line 1823
    nop

    #@4a
    :sswitch_data_4a
    .sparse-switch
        0x78 -> :sswitch_29
        0xa0 -> :sswitch_2e
        0xd5 -> :sswitch_33
        0xf0 -> :sswitch_38
        0x140 -> :sswitch_3d
        0x1e0 -> :sswitch_42
    .end sparse-switch
.end method

.method public getMemoryClass()I
    .registers 2

    #@0
    .prologue
    .line 344
    invoke-static {}, Landroid/app/ActivityManager;->staticGetMemoryClass()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V
    .registers 3
    .parameter "outInfo"

    #@0
    .prologue
    .line 1248
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1}, Landroid/app/IActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 1251
    :goto_7
    return-void

    #@8
    .line 1249
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public getPackageAskScreenCompat(Ljava/lang/String;)Z
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 319
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getPackageAskScreenCompat(Ljava/lang/String;)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 322
    :goto_8
    return v1

    #@9
    .line 320
    :catch_9
    move-exception v0

    #@a
    .line 322
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getPackageScreenCompatMode(Ljava/lang/String;)I
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 300
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getPackageScreenCompatMode(Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 303
    :goto_8
    return v1

    #@9
    .line 301
    :catch_9
    move-exception v0

    #@a
    .line 303
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;
    .registers 4
    .parameter "pids"

    #@0
    .prologue
    .line 1693
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 1695
    :goto_8
    return-object v1

    #@9
    .line 1694
    :catch_9
    move-exception v0

    #@a
    .line 1695
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getProcessesInErrorState()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$ProcessErrorStateInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1377
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/app/IActivityManager;->getProcessesInErrorState()Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 1379
    :goto_8
    return-object v1

    #@9
    .line 1378
    :catch_9
    move-exception v0

    #@a
    .line 1379
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRecentTasks(II)Ljava/util/List;
    .registers 6
    .parameter "maxNum"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RecentTaskInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 554
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v2

    #@8
    invoke-interface {v1, p1, p2, v2}, Landroid/app/IActivityManager;->getRecentTasks(III)Ljava/util/List;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result-object v1

    #@c
    .line 558
    :goto_c
    return-object v1

    #@d
    .line 556
    :catch_d
    move-exception v0

    #@e
    .line 558
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@f
    goto :goto_c
.end method

.method public getRecentTasksForUser(III)Ljava/util/List;
    .registers 6
    .parameter "maxNum"
    .parameter "flags"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RecentTaskInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 583
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2, p3}, Landroid/app/IActivityManager;->getRecentTasks(III)Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 587
    :goto_8
    return-object v1

    #@9
    .line 585
    :catch_9
    move-exception v0

    #@a
    .line 587
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRunningAppProcesses()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningAppProcessInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1656
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 1658
    :goto_8
    return-object v1

    #@9
    .line 1657
    :catch_9
    move-exception v0

    #@a
    .line 1658
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRunningExternalApplications()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1638
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/app/IActivityManager;->getRunningExternalApplications()Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 1640
    :goto_8
    return-object v1

    #@9
    .line 1639
    :catch_9
    move-exception v0

    #@a
    .line 1640
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRunningServiceControlPanel(Landroid/content/ComponentName;)Landroid/app/PendingIntent;
    .registers 4
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 1142
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getRunningServiceControlPanel(Landroid/content/ComponentName;)Landroid/app/PendingIntent;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 1146
    :goto_8
    return-object v1

    #@9
    .line 1144
    :catch_9
    move-exception v0

    #@a
    .line 1146
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getRunningServices(I)Ljava/util/List;
    .registers 5
    .parameter "maxNum"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningServiceInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 1126
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x0

    #@5
    invoke-interface {v1, p1, v2}, Landroid/app/IActivityManager;->getServices(II)Ljava/util/List;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_8} :catch_a

    #@8
    move-result-object v1

    #@9
    .line 1130
    :goto_9
    return-object v1

    #@a
    .line 1128
    :catch_a
    move-exception v0

    #@b
    .line 1130
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@c
    goto :goto_9
.end method

.method public getRunningTasks(I)Ljava/util/List;
    .registers 4
    .parameter "maxNum"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 755
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getRunningTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;
    .registers 6
    .parameter "maxNum"
    .parameter "flags"
    .parameter "receiver"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/app/IThumbnailReceiver;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 719
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2, p3}, Landroid/app/IActivityManager;->getTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 722
    :goto_8
    return-object v1

    #@9
    .line 720
    :catch_9
    move-exception v0

    #@a
    .line 722
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getTaskScreenShot(I)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 890
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getTaskScreenShot(I)Landroid/app/ActivityManager$TaskThumbnails;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 893
    :goto_8
    return-object v1

    #@9
    .line 891
    :catch_9
    move-exception v0

    #@a
    .line 893
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getTaskThumbnails(I)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 869
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getTaskThumbnails(I)Landroid/app/ActivityManager$TaskThumbnails;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 872
    :goto_8
    return-object v1

    #@9
    .line 870
    :catch_9
    move-exception v0

    #@a
    .line 872
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getTaskTopThumbnail(I)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 879
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getTaskTopThumbnail(I)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 882
    :goto_8
    return-object v1

    #@9
    .line 880
    :catch_9
    move-exception v0

    #@a
    .line 882
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public getTopRunningTaskInfo(I)Landroid/app/ActivityManager$RunningTaskInfo;
    .registers 4
    .parameter "screenZone"

    #@0
    .prologue
    .line 2029
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->getTopRunningTaskInfo(I)Landroid/app/ActivityManager$RunningTaskInfo;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 2031
    :goto_8
    return-object v1

    #@9
    .line 2030
    :catch_9
    move-exception v0

    #@a
    .line 2031
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public isUserRunning(I)Z
    .registers 6
    .parameter "userid"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2004
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v2

    #@5
    const/4 v3, 0x0

    #@6
    invoke-interface {v2, p1, v3}, Landroid/app/IActivityManager;->isUserRunning(IZ)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 2006
    :goto_a
    return v1

    #@b
    .line 2005
    :catch_b
    move-exception v0

    #@c
    .line 2006
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_a
.end method

.method public killBackgroundProcesses(Ljava/lang/String;)V
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 1726
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v1

    #@8
    invoke-interface {v0, p1, v1}, Landroid/app/IActivityManager;->killBackgroundProcesses(Ljava/lang/String;I)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 1730
    :goto_b
    return-void

    #@c
    .line 1728
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public moveTaskToFront(II)V
    .registers 4
    .parameter "taskId"
    .parameter "flags"

    #@0
    .prologue
    .line 922
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/ActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V

    #@4
    .line 923
    return-void
.end method

.method public moveTaskToFront(IILandroid/os/Bundle;)V
    .registers 5
    .parameter "taskId"
    .parameter "flags"
    .parameter "options"

    #@0
    .prologue
    .line 941
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1, p2, p3}, Landroid/app/IActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 945
    :goto_7
    return-void

    #@8
    .line 942
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public removeSubTask(II)Z
    .registers 5
    .parameter "taskId"
    .parameter "subTaskIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 773
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2}, Landroid/app/IActivityManager;->removeSubTask(II)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 776
    :goto_8
    return v1

    #@9
    .line 774
    :catch_9
    move-exception v0

    #@a
    .line 776
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public removeTask(II)Z
    .registers 5
    .parameter "taskId"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 800
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1, p2}, Landroid/app/IActivityManager;->removeTask(II)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 803
    :goto_8
    return v1

    #@9
    .line 801
    :catch_9
    move-exception v0

    #@a
    .line 803
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public restartPackage(Ljava/lang/String;)V
    .registers 2
    .parameter "packageName"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1708
    invoke-virtual {p0, p1}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    #@3
    .line 1709
    return-void
.end method

.method public setFrontActivityScreenCompatMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 291
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1}, Landroid/app/IActivityManager;->setFrontActivityScreenCompatMode(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 295
    :goto_7
    return-void

    #@8
    .line 292
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public setPackageAskScreenCompat(Ljava/lang/String;Z)V
    .registers 4
    .parameter "packageName"
    .parameter "ask"

    #@0
    .prologue
    .line 329
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1, p2}, Landroid/app/IActivityManager;->setPackageAskScreenCompat(Ljava/lang/String;Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 333
    :goto_7
    return-void

    #@8
    .line 330
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public setPackageScreenCompatMode(Ljava/lang/String;I)V
    .registers 4
    .parameter "packageName"
    .parameter "mode"

    #@0
    .prologue
    .line 310
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0, p1, p2}, Landroid/app/IActivityManager;->setPackageScreenCompatMode(Ljava/lang/String;I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 314
    :goto_7
    return-void

    #@8
    .line 311
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public switchUser(I)Z
    .registers 4
    .parameter "userid"

    #@0
    .prologue
    .line 1987
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p1}, Landroid/app/IActivityManager;->switchUser(I)Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result v1

    #@8
    .line 1989
    :goto_8
    return v1

    #@9
    .line 1988
    :catch_9
    move-exception v0

    #@a
    .line 1989
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method
