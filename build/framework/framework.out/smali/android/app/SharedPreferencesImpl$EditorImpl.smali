.class public final Landroid/app/SharedPreferencesImpl$EditorImpl;
.super Ljava/lang/Object;
.source "SharedPreferencesImpl.java"

# interfaces
.implements Landroid/content/SharedPreferences$Editor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/SharedPreferencesImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "EditorImpl"
.end annotation


# instance fields
.field private mClear:Z

.field private final mModified:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/app/SharedPreferencesImpl;


# direct methods
.method public constructor <init>(Landroid/app/SharedPreferencesImpl;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 303
    iput-object p1, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 304
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@b
    .line 305
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mClear:Z

    #@e
    return-void
.end method

.method static synthetic access$600(Landroid/app/SharedPreferencesImpl$EditorImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 303
    invoke-direct {p0, p1}, Landroid/app/SharedPreferencesImpl$EditorImpl;->notifyListeners(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@3
    return-void
.end method

.method private commitToMemory()Landroid/app/SharedPreferencesImpl$MemoryCommitResult;
    .registers 14

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 390
    new-instance v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;

    #@4
    const/4 v9, 0x0

    #@5
    invoke-direct {v6, v9}, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;-><init>(Landroid/app/SharedPreferencesImpl$1;)V

    #@8
    .line 391
    .local v6, mcr:Landroid/app/SharedPreferencesImpl$MemoryCommitResult;
    iget-object v9, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@a
    monitor-enter v9

    #@b
    .line 395
    :try_start_b
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@d
    invoke-static {v10}, Landroid/app/SharedPreferencesImpl;->access$300(Landroid/app/SharedPreferencesImpl;)I

    #@10
    move-result v10

    #@11
    if-lez v10, :cond_23

    #@13
    .line 400
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@15
    new-instance v11, Ljava/util/HashMap;

    #@17
    iget-object v12, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@19
    invoke-static {v12}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@1c
    move-result-object v12

    #@1d
    invoke-direct {v11, v12}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@20
    invoke-static {v10, v11}, Landroid/app/SharedPreferencesImpl;->access$402(Landroid/app/SharedPreferencesImpl;Ljava/util/Map;)Ljava/util/Map;

    #@23
    .line 402
    :cond_23
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@25
    invoke-static {v10}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@28
    move-result-object v10

    #@29
    iput-object v10, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->mapToWriteToDisk:Ljava/util/Map;

    #@2b
    .line 403
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@2d
    invoke-static {v10}, Landroid/app/SharedPreferencesImpl;->access$308(Landroid/app/SharedPreferencesImpl;)I

    #@30
    .line 405
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@32
    invoke-static {v10}, Landroid/app/SharedPreferencesImpl;->access$500(Landroid/app/SharedPreferencesImpl;)Ljava/util/WeakHashMap;

    #@35
    move-result-object v10

    #@36
    invoke-virtual {v10}, Ljava/util/WeakHashMap;->size()I

    #@39
    move-result v10

    #@3a
    if-lez v10, :cond_be

    #@3c
    .line 406
    .local v2, hasListeners:Z
    :goto_3c
    if-eqz v2, :cond_56

    #@3e
    .line 407
    new-instance v8, Ljava/util/ArrayList;

    #@40
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@43
    iput-object v8, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@45
    .line 408
    new-instance v8, Ljava/util/HashSet;

    #@47
    iget-object v10, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@49
    invoke-static {v10}, Landroid/app/SharedPreferencesImpl;->access$500(Landroid/app/SharedPreferencesImpl;)Ljava/util/WeakHashMap;

    #@4c
    move-result-object v10

    #@4d
    invoke-virtual {v10}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    #@50
    move-result-object v10

    #@51
    invoke-direct {v8, v10}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@54
    iput-object v8, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->listeners:Ljava/util/Set;

    #@56
    .line 412
    :cond_56
    monitor-enter p0
    :try_end_57
    .catchall {:try_start_b .. :try_end_57} :catchall_bb

    #@57
    .line 413
    :try_start_57
    iget-boolean v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mClear:Z

    #@59
    if-eqz v8, :cond_76

    #@5b
    .line 414
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@5d
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@60
    move-result-object v8

    #@61
    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    #@64
    move-result v8

    #@65
    if-nez v8, :cond_73

    #@67
    .line 415
    const/4 v8, 0x1

    #@68
    iput-boolean v8, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->changesMade:Z

    #@6a
    .line 416
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@6c
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@6f
    move-result-object v8

    #@70
    invoke-interface {v8}, Ljava/util/Map;->clear()V

    #@73
    .line 418
    :cond_73
    const/4 v8, 0x0

    #@74
    iput-boolean v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mClear:Z

    #@76
    .line 421
    :cond_76
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@78
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@7b
    move-result-object v8

    #@7c
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7f
    move-result-object v3

    #@80
    .local v3, i$:Ljava/util/Iterator;
    :cond_80
    :goto_80
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@83
    move-result v8

    #@84
    if-eqz v8, :cond_ea

    #@86
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@89
    move-result-object v0

    #@8a
    check-cast v0, Ljava/util/Map$Entry;

    #@8c
    .line 422
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@8f
    move-result-object v5

    #@90
    check-cast v5, Ljava/lang/String;

    #@92
    .line 423
    .local v5, k:Ljava/lang/String;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@95
    move-result-object v7

    #@96
    .line 424
    .local v7, v:Ljava/lang/Object;
    if-ne v7, p0, :cond_c1

    #@98
    .line 425
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@9a
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@9d
    move-result-object v8

    #@9e
    invoke-interface {v8, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@a1
    move-result v8

    #@a2
    if-eqz v8, :cond_80

    #@a4
    .line 428
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@a6
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@a9
    move-result-object v8

    #@aa
    invoke-interface {v8, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@ad
    .line 440
    :goto_ad
    const/4 v8, 0x1

    #@ae
    iput-boolean v8, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->changesMade:Z

    #@b0
    .line 441
    if-eqz v2, :cond_80

    #@b2
    .line 442
    iget-object v8, v6, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@b4
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@b7
    goto :goto_80

    #@b8
    .line 447
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v5           #k:Ljava/lang/String;
    .end local v7           #v:Ljava/lang/Object;
    :catchall_b8
    move-exception v8

    #@b9
    monitor-exit p0
    :try_end_ba
    .catchall {:try_start_57 .. :try_end_ba} :catchall_b8

    #@ba
    :try_start_ba
    throw v8

    #@bb
    .line 448
    .end local v2           #hasListeners:Z
    :catchall_bb
    move-exception v8

    #@bc
    monitor-exit v9
    :try_end_bd
    .catchall {:try_start_ba .. :try_end_bd} :catchall_bb

    #@bd
    throw v8

    #@be
    :cond_be
    move v2, v8

    #@bf
    .line 405
    goto/16 :goto_3c

    #@c1
    .line 430
    .restart local v0       #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v2       #hasListeners:Z
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v5       #k:Ljava/lang/String;
    .restart local v7       #v:Ljava/lang/Object;
    :cond_c1
    const/4 v4, 0x0

    #@c2
    .line 431
    .local v4, isSame:Z
    :try_start_c2
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@c4
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@c7
    move-result-object v8

    #@c8
    invoke-interface {v8, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@cb
    move-result v8

    #@cc
    if-eqz v8, :cond_e0

    #@ce
    .line 432
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@d0
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@d3
    move-result-object v8

    #@d4
    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d7
    move-result-object v1

    #@d8
    .line 433
    .local v1, existingValue:Ljava/lang/Object;
    if-eqz v1, :cond_e0

    #@da
    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@dd
    move-result v8

    #@de
    if-nez v8, :cond_80

    #@e0
    .line 437
    .end local v1           #existingValue:Ljava/lang/Object;
    :cond_e0
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@e2
    invoke-static {v8}, Landroid/app/SharedPreferencesImpl;->access$400(Landroid/app/SharedPreferencesImpl;)Ljava/util/Map;

    #@e5
    move-result-object v8

    #@e6
    invoke-interface {v8, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e9
    goto :goto_ad

    #@ea
    .line 446
    .end local v0           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4           #isSame:Z
    .end local v5           #k:Ljava/lang/String;
    .end local v7           #v:Ljava/lang/Object;
    :cond_ea
    iget-object v8, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@ec
    invoke-interface {v8}, Ljava/util/Map;->clear()V

    #@ef
    .line 447
    monitor-exit p0
    :try_end_f0
    .catchall {:try_start_c2 .. :try_end_f0} :catchall_b8

    #@f0
    .line 448
    :try_start_f0
    monitor-exit v9
    :try_end_f1
    .catchall {:try_start_f0 .. :try_end_f1} :catchall_bb

    #@f1
    .line 449
    return-object v6
.end method

.method private notifyListeners(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V
    .registers 8
    .parameter "mcr"

    #@0
    .prologue
    .line 466
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->listeners:Ljava/util/Set;

    #@2
    if-eqz v4, :cond_10

    #@4
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@6
    if-eqz v4, :cond_10

    #@8
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@a
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@d
    move-result v4

    #@e
    if-nez v4, :cond_11

    #@10
    .line 487
    :cond_10
    :goto_10
    return-void

    #@11
    .line 470
    :cond_11
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@14
    move-result-object v4

    #@15
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@18
    move-result-object v5

    #@19
    if-ne v4, v5, :cond_4a

    #@1b
    .line 471
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@1d
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@20
    move-result v4

    #@21
    add-int/lit8 v0, v4, -0x1

    #@23
    .local v0, i:I
    :goto_23
    if-ltz v0, :cond_10

    #@25
    .line 472
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->keysModified:Ljava/util/List;

    #@27
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Ljava/lang/String;

    #@2d
    .line 473
    .local v2, key:Ljava/lang/String;
    iget-object v4, p1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->listeners:Ljava/util/Set;

    #@2f
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@32
    move-result-object v1

    #@33
    .local v1, i$:Ljava/util/Iterator;
    :cond_33
    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_47

    #@39
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v3

    #@3d
    check-cast v3, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    #@3f
    .line 474
    .local v3, listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    if-eqz v3, :cond_33

    #@41
    .line 475
    iget-object v4, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@43
    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    #@46
    goto :goto_33

    #@47
    .line 471
    .end local v3           #listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
    :cond_47
    add-int/lit8 v0, v0, -0x1

    #@49
    goto :goto_23

    #@4a
    .line 481
    .end local v0           #i:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #key:Ljava/lang/String;
    :cond_4a
    sget-object v4, Landroid/app/ActivityThread;->sMainThreadHandler:Landroid/os/Handler;

    #@4c
    new-instance v5, Landroid/app/SharedPreferencesImpl$EditorImpl$3;

    #@4e
    invoke-direct {v5, p0, p1}, Landroid/app/SharedPreferencesImpl$EditorImpl$3;-><init>(Landroid/app/SharedPreferencesImpl$EditorImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@51
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@54
    goto :goto_10
.end method


# virtual methods
.method public apply()V
    .registers 5

    #@0
    .prologue
    .line 360
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl$EditorImpl;->commitToMemory()Landroid/app/SharedPreferencesImpl$MemoryCommitResult;

    #@3
    move-result-object v1

    #@4
    .line 361
    .local v1, mcr:Landroid/app/SharedPreferencesImpl$MemoryCommitResult;
    new-instance v0, Landroid/app/SharedPreferencesImpl$EditorImpl$1;

    #@6
    invoke-direct {v0, p0, v1}, Landroid/app/SharedPreferencesImpl$EditorImpl$1;-><init>(Landroid/app/SharedPreferencesImpl$EditorImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@9
    .line 370
    .local v0, awaitCommit:Ljava/lang/Runnable;
    invoke-static {v0}, Landroid/app/QueuedWork;->add(Ljava/lang/Runnable;)V

    #@c
    .line 372
    new-instance v2, Landroid/app/SharedPreferencesImpl$EditorImpl$2;

    #@e
    invoke-direct {v2, p0, v0}, Landroid/app/SharedPreferencesImpl$EditorImpl$2;-><init>(Landroid/app/SharedPreferencesImpl$EditorImpl;Ljava/lang/Runnable;)V

    #@11
    .line 379
    .local v2, postWriteRunnable:Ljava/lang/Runnable;
    iget-object v3, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@13
    invoke-static {v3, v1, v2}, Landroid/app/SharedPreferencesImpl;->access$100(Landroid/app/SharedPreferencesImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V

    #@16
    .line 385
    invoke-direct {p0, v1}, Landroid/app/SharedPreferencesImpl$EditorImpl;->notifyListeners(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@19
    .line 386
    return-void
.end method

.method public clear()Landroid/content/SharedPreferences$Editor;
    .registers 2

    #@0
    .prologue
    .line 353
    monitor-enter p0

    #@1
    .line 354
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mClear:Z

    #@4
    .line 355
    monitor-exit p0

    #@5
    return-object p0

    #@6
    .line 356
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_6

    #@8
    throw v0
.end method

.method public commit()Z
    .registers 5

    #@0
    .prologue
    .line 453
    invoke-direct {p0}, Landroid/app/SharedPreferencesImpl$EditorImpl;->commitToMemory()Landroid/app/SharedPreferencesImpl$MemoryCommitResult;

    #@3
    move-result-object v1

    #@4
    .line 454
    .local v1, mcr:Landroid/app/SharedPreferencesImpl$MemoryCommitResult;
    iget-object v2, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->this$0:Landroid/app/SharedPreferencesImpl;

    #@6
    const/4 v3, 0x0

    #@7
    invoke-static {v2, v1, v3}, Landroid/app/SharedPreferencesImpl;->access$100(Landroid/app/SharedPreferencesImpl;Landroid/app/SharedPreferencesImpl$MemoryCommitResult;Ljava/lang/Runnable;)V

    #@a
    .line 457
    :try_start_a
    iget-object v2, v1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->writtenToDiskLatch:Ljava/util/concurrent/CountDownLatch;

    #@c
    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_f} :catch_15

    #@f
    .line 461
    invoke-direct {p0, v1}, Landroid/app/SharedPreferencesImpl$EditorImpl;->notifyListeners(Landroid/app/SharedPreferencesImpl$MemoryCommitResult;)V

    #@12
    .line 462
    iget-boolean v2, v1, Landroid/app/SharedPreferencesImpl$MemoryCommitResult;->writeToDiskResult:Z

    #@14
    :goto_14
    return v2

    #@15
    .line 458
    :catch_15
    move-exception v0

    #@16
    .line 459
    .local v0, e:Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    #@17
    goto :goto_14
.end method

.method public putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 339
    monitor-enter p0

    #@1
    .line 340
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@6
    move-result-object v1

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 341
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 342
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 333
    monitor-enter p0

    #@1
    .line 334
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@6
    move-result-object v1

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 335
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 336
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 321
    monitor-enter p0

    #@1
    .line 322
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v1

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 323
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 324
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .registers 6
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 327
    monitor-enter p0

    #@1
    .line 328
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6
    move-result-object v1

    #@7
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 329
    monitor-exit p0

    #@b
    return-object p0

    #@c
    .line 330
    :catchall_c
    move-exception v0

    #@d
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 308
    monitor-enter p0

    #@1
    .line 309
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 310
    monitor-exit p0

    #@7
    return-object p0

    #@8
    .line 311
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .registers 5
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/SharedPreferences$Editor;"
        }
    .end annotation

    #@0
    .prologue
    .line 314
    .local p2, values:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    monitor-enter p0

    #@1
    .line 315
    :try_start_1
    iget-object v1, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    if-nez p2, :cond_b

    #@5
    const/4 v0, 0x0

    #@6
    :goto_6
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 317
    monitor-exit p0

    #@a
    return-object p0

    #@b
    .line 315
    :cond_b
    new-instance v0, Ljava/util/HashSet;

    #@d
    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    #@10
    goto :goto_6

    #@11
    .line 318
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 346
    monitor-enter p0

    #@1
    .line 347
    :try_start_1
    iget-object v0, p0, Landroid/app/SharedPreferencesImpl$EditorImpl;->mModified:Ljava/util/Map;

    #@3
    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 348
    monitor-exit p0

    #@7
    return-object p0

    #@8
    .line 349
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method
