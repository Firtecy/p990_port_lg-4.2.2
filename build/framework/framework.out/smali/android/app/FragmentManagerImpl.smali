.class final Landroid/app/FragmentManagerImpl;
.super Landroid/app/FragmentManager;
.source "FragmentManager.java"


# static fields
.field static DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "FragmentManager"

.field static final TARGET_REQUEST_CODE_STATE_TAG:Ljava/lang/String; = "android:target_req_state"

.field static final TARGET_STATE_TAG:Ljava/lang/String; = "android:target_state"

.field static final USER_VISIBLE_HINT_TAG:Ljava/lang/String; = "android:user_visible_hint"

.field static final VIEW_STATE_TAG:Ljava/lang/String; = "android:view_state"


# instance fields
.field mActive:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mActivity:Landroid/app/Activity;

.field mAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mAvailBackStackIndices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mAvailIndices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mBackStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field mBackStackChangeListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/FragmentManager$OnBackStackChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field mBackStackIndices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field mContainer:Landroid/app/FragmentContainer;

.field mCreatedMenus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mCurState:I

.field mDestroyed:Z

.field mExecCommit:Ljava/lang/Runnable;

.field mExecutingActions:Z

.field mHavePendingDeferredStart:Z

.field mNeedMenuInvalidate:Z

.field mNoTransactionsBecause:Ljava/lang/String;

.field mParent:Landroid/app/Fragment;

.field mPendingActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field mStateArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field mStateBundle:Landroid/os/Bundle;

.field mStateSaved:Z

.field mTmpActions:[Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 399
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@3
    return-void
.end method

.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 398
    invoke-direct {p0}, Landroid/app/FragmentManager;-><init>()V

    #@4
    .line 423
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@7
    .line 435
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@9
    .line 436
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@b
    .line 438
    new-instance v0, Landroid/app/FragmentManagerImpl$1;

    #@d
    invoke-direct {v0, p0}, Landroid/app/FragmentManagerImpl$1;-><init>(Landroid/app/FragmentManagerImpl;)V

    #@10
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    #@12
    return-void
.end method

.method private checkStateLoss()V
    .registers 4

    #@0
    .prologue
    .line 1317
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 1318
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Can not perform this action after onSaveInstanceState"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1321
    :cond_c
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    #@e
    if-eqz v0, :cond_2b

    #@10
    .line 1322
    new-instance v0, Ljava/lang/IllegalStateException;

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Can not perform this action inside of "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 1325
    :cond_2b
    return-void
.end method

.method public static reverseTransit(I)I
    .registers 2
    .parameter "transit"

    #@0
    .prologue
    .line 2003
    const/4 v0, 0x0

    #@1
    .line 2004
    .local v0, rev:I
    sparse-switch p0, :sswitch_data_e

    #@4
    .line 2015
    :goto_4
    return v0

    #@5
    .line 2006
    :sswitch_5
    const/16 v0, 0x2002

    #@7
    .line 2007
    goto :goto_4

    #@8
    .line 2009
    :sswitch_8
    const/16 v0, 0x1001

    #@a
    .line 2010
    goto :goto_4

    #@b
    .line 2012
    :sswitch_b
    const/16 v0, 0x1003

    #@d
    goto :goto_4

    #@e
    .line 2004
    :sswitch_data_e
    .sparse-switch
        0x1001 -> :sswitch_5
        0x1003 -> :sswitch_b
        0x2002 -> :sswitch_8
    .end sparse-switch
.end method

.method private throwException(Ljava/lang/RuntimeException;)V
    .registers 9
    .parameter "ex"

    #@0
    .prologue
    .line 446
    const-string v3, "FragmentManager"

    #@2
    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 447
    new-instance v1, Landroid/util/LogWriter;

    #@b
    const/4 v3, 0x6

    #@c
    const-string v4, "FragmentManager"

    #@e
    invoke-direct {v1, v3, v4}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    #@11
    .line 448
    .local v1, logw:Landroid/util/LogWriter;
    new-instance v2, Ljava/io/PrintWriter;

    #@13
    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@16
    .line 449
    .local v2, pw:Ljava/io/PrintWriter;
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@18
    if-eqz v3, :cond_36

    #@1a
    .line 450
    const-string v3, "FragmentManager"

    #@1c
    const-string v4, "Activity state:"

    #@1e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 452
    :try_start_21
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@23
    const-string v4, "  "

    #@25
    const/4 v5, 0x0

    #@26
    const/4 v6, 0x0

    #@27
    new-array v6, v6, [Ljava/lang/String;

    #@29
    invoke-virtual {v3, v4, v5, v2, v6}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_2c} :catch_2d

    #@2c
    .line 464
    :goto_2c
    throw p1

    #@2d
    .line 453
    :catch_2d
    move-exception v0

    #@2e
    .line 454
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "FragmentManager"

    #@30
    const-string v4, "Failed dumping state"

    #@32
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35
    goto :goto_2c

    #@36
    .line 457
    .end local v0           #e:Ljava/lang/Exception;
    :cond_36
    const-string v3, "FragmentManager"

    #@38
    const-string v4, "Fragment manager state:"

    #@3a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 459
    :try_start_3d
    const-string v3, "  "

    #@3f
    const/4 v4, 0x0

    #@40
    const/4 v5, 0x0

    #@41
    new-array v5, v5, [Ljava/lang/String;

    #@43
    invoke-virtual {p0, v3, v4, v2, v5}, Landroid/app/FragmentManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_46} :catch_47

    #@46
    goto :goto_2c

    #@47
    .line 460
    :catch_47
    move-exception v0

    #@48
    .line 461
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v3, "FragmentManager"

    #@4a
    const-string v4, "Failed dumping state"

    #@4c
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4f
    goto :goto_2c
.end method

.method public static transitToStyleIndex(IZ)I
    .registers 3
    .parameter "transit"
    .parameter "enter"

    #@0
    .prologue
    .line 2020
    const/4 v0, -0x1

    #@1
    .line 2021
    .local v0, animAttr:I
    sparse-switch p0, :sswitch_data_18

    #@4
    .line 2038
    :goto_4
    return v0

    #@5
    .line 2023
    :sswitch_5
    if-eqz p1, :cond_9

    #@7
    const/4 v0, 0x0

    #@8
    .line 2026
    :goto_8
    goto :goto_4

    #@9
    .line 2023
    :cond_9
    const/4 v0, 0x1

    #@a
    goto :goto_8

    #@b
    .line 2028
    :sswitch_b
    if-eqz p1, :cond_f

    #@d
    const/4 v0, 0x2

    #@e
    .line 2031
    :goto_e
    goto :goto_4

    #@f
    .line 2028
    :cond_f
    const/4 v0, 0x3

    #@10
    goto :goto_e

    #@11
    .line 2033
    :sswitch_11
    if-eqz p1, :cond_15

    #@13
    const/4 v0, 0x4

    #@14
    :goto_14
    goto :goto_4

    #@15
    :cond_15
    const/4 v0, 0x5

    #@16
    goto :goto_14

    #@17
    .line 2021
    nop

    #@18
    :sswitch_data_18
    .sparse-switch
        0x1001 -> :sswitch_5
        0x1003 -> :sswitch_11
        0x2002 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method addBackStackState(Landroid/app/BackStackRecord;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 1467
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1468
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@b
    .line 1470
    :cond_b
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 1471
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->reportBackStackChanged()V

    #@13
    .line 1472
    return-void
.end method

.method public addFragment(Landroid/app/Fragment;Z)V
    .registers 7
    .parameter "fragment"
    .parameter "moveToStateNow"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1121
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 1122
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@c
    .line 1124
    :cond_c
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@e
    if-eqz v0, :cond_28

    #@10
    const-string v0, "FragmentManager"

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "add: "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1125
    :cond_28
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->makeActive(Landroid/app/Fragment;)V

    #@2b
    .line 1126
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    #@2d
    if-nez v0, :cond_69

    #@2f
    .line 1127
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@34
    move-result v0

    #@35
    if-eqz v0, :cond_50

    #@37
    .line 1128
    new-instance v0, Ljava/lang/IllegalStateException;

    #@39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "Fragment already added: "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v0

    #@50
    .line 1130
    :cond_50
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@52
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@55
    .line 1131
    iput-boolean v3, p1, Landroid/app/Fragment;->mAdded:Z

    #@57
    .line 1132
    const/4 v0, 0x0

    #@58
    iput-boolean v0, p1, Landroid/app/Fragment;->mRemoving:Z

    #@5a
    .line 1133
    iget-boolean v0, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@5c
    if-eqz v0, :cond_64

    #@5e
    iget-boolean v0, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@60
    if-eqz v0, :cond_64

    #@62
    .line 1134
    iput-boolean v3, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@64
    .line 1136
    :cond_64
    if-eqz p2, :cond_69

    #@66
    .line 1137
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;)V

    #@69
    .line 1140
    :cond_69
    return-void
.end method

.method public addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 543
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 544
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@b
    .line 546
    :cond_b
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 547
    return-void
.end method

.method public allocBackStackIndex(Landroid/app/BackStackRecord;)I
    .registers 7
    .parameter "bse"

    #@0
    .prologue
    .line 1347
    monitor-enter p0

    #@1
    .line 1348
    :try_start_1
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@3
    if-eqz v2, :cond_d

    #@5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-gtz v2, :cond_4c

    #@d
    .line 1349
    :cond_d
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@f
    if-nez v2, :cond_18

    #@11
    .line 1350
    new-instance v2, Ljava/util/ArrayList;

    #@13
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@16
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@18
    .line 1352
    :cond_18
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@1d
    move-result v0

    #@1e
    .line 1353
    .local v0, index:I
    sget-boolean v2, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@20
    if-eqz v2, :cond_44

    #@22
    const-string v2, "FragmentManager"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "Setting back stack index "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, " to "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1354
    :cond_44
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@46
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 1355
    monitor-exit p0

    #@4a
    move v1, v0

    #@4b
    .line 1361
    .end local v0           #index:I
    .local v1, index:I
    :goto_4b
    return v1

    #@4c
    .line 1358
    .end local v1           #index:I
    :cond_4c
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@4e
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v3

    #@54
    add-int/lit8 v3, v3, -0x1

    #@56
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@59
    move-result-object v2

    #@5a
    check-cast v2, Ljava/lang/Integer;

    #@5c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@5f
    move-result v0

    #@60
    .line 1359
    .restart local v0       #index:I
    sget-boolean v2, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@62
    if-eqz v2, :cond_86

    #@64
    const-string v2, "FragmentManager"

    #@66
    new-instance v3, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v4, "Adding back stack index "

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    const-string v4, " with "

    #@77
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v3

    #@7f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1360
    :cond_86
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@88
    invoke-virtual {v2, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@8b
    .line 1361
    monitor-exit p0

    #@8c
    move v1, v0

    #@8d
    .end local v0           #index:I
    .restart local v1       #index:I
    goto :goto_4b

    #@8e
    .line 1363
    .end local v1           #index:I
    :catchall_8e
    move-exception v2

    #@8f
    monitor-exit p0
    :try_end_90
    .catchall {:try_start_1 .. :try_end_90} :catchall_8e

    #@90
    throw v2
.end method

.method public attachActivity(Landroid/app/Activity;Landroid/app/FragmentContainer;Landroid/app/Fragment;)V
    .registers 6
    .parameter "activity"
    .parameter "container"
    .parameter "parent"

    #@0
    .prologue
    .line 1823
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_c

    #@4
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Already attached"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1824
    :cond_c
    iput-object p1, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@e
    .line 1825
    iput-object p2, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    #@10
    .line 1826
    iput-object p3, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@12
    .line 1827
    return-void
.end method

.method public attachFragment(Landroid/app/Fragment;II)V
    .registers 10
    .parameter "fragment"
    .parameter "transition"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1239
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4
    if-eqz v0, :cond_1e

    #@6
    const-string v0, "FragmentManager"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "attach: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1240
    :cond_1e
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    #@20
    if-eqz v0, :cond_8a

    #@22
    .line 1241
    iput-boolean v5, p1, Landroid/app/Fragment;->mDetached:Z

    #@24
    .line 1242
    iget-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    #@26
    if-nez v0, :cond_8a

    #@28
    .line 1243
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2a
    if-nez v0, :cond_33

    #@2c
    .line 1244
    new-instance v0, Ljava/util/ArrayList;

    #@2e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@31
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@33
    .line 1246
    :cond_33
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@35
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@38
    move-result v0

    #@39
    if-eqz v0, :cond_54

    #@3b
    .line 1247
    new-instance v0, Ljava/lang/IllegalStateException;

    #@3d
    new-instance v1, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v2, "Fragment already added: "

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@53
    throw v0

    #@54
    .line 1249
    :cond_54
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@56
    if-eqz v0, :cond_70

    #@58
    const-string v0, "FragmentManager"

    #@5a
    new-instance v1, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v2, "add from attach: "

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1250
    :cond_70
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@72
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    .line 1251
    iput-boolean v3, p1, Landroid/app/Fragment;->mAdded:Z

    #@77
    .line 1252
    iget-boolean v0, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@79
    if-eqz v0, :cond_81

    #@7b
    iget-boolean v0, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@7d
    if-eqz v0, :cond_81

    #@7f
    .line 1253
    iput-boolean v3, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@81
    .line 1255
    :cond_81
    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@83
    move-object v0, p0

    #@84
    move-object v1, p1

    #@85
    move v3, p2

    #@86
    move v4, p3

    #@87
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@8a
    .line 1258
    :cond_8a
    return-void
.end method

.method public beginTransaction()Landroid/app/FragmentTransaction;
    .registers 2

    #@0
    .prologue
    .line 469
    new-instance v0, Landroid/app/BackStackRecord;

    #@2
    invoke-direct {v0, p0}, Landroid/app/BackStackRecord;-><init>(Landroid/app/FragmentManagerImpl;)V

    #@5
    return-object v0
.end method

.method public detachFragment(Landroid/app/Fragment;II)V
    .registers 10
    .parameter "fragment"
    .parameter "transition"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 1220
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4
    if-eqz v0, :cond_1e

    #@6
    const-string v0, "FragmentManager"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "detach: "

    #@f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1221
    :cond_1e
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    #@20
    if-nez v0, :cond_61

    #@22
    .line 1222
    iput-boolean v2, p1, Landroid/app/Fragment;->mDetached:Z

    #@24
    .line 1223
    iget-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    #@26
    if-eqz v0, :cond_61

    #@28
    .line 1225
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2a
    if-eqz v0, :cond_4e

    #@2c
    .line 1226
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@2e
    if-eqz v0, :cond_49

    #@30
    const-string v0, "FragmentManager"

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string/jumbo v3, "remove from detach: "

    #@3a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 1227
    :cond_49
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@4b
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@4e
    .line 1229
    :cond_4e
    iget-boolean v0, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@50
    if-eqz v0, :cond_58

    #@52
    iget-boolean v0, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@54
    if-eqz v0, :cond_58

    #@56
    .line 1230
    iput-boolean v2, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@58
    .line 1232
    :cond_58
    iput-boolean v5, p1, Landroid/app/Fragment;->mAdded:Z

    #@5a
    move-object v0, p0

    #@5b
    move-object v1, p1

    #@5c
    move v3, p2

    #@5d
    move v4, p3

    #@5e
    .line 1233
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@61
    .line 1236
    :cond_61
    return-void
.end method

.method public dispatchActivityCreated()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1839
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@3
    .line 1840
    const/4 v0, 0x2

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@7
    .line 1841
    return-void
.end method

.method public dispatchConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter "newConfig"

    #@0
    .prologue
    .line 1875
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_1d

    #@4
    .line 1876
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_1d

    #@d
    .line 1877
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1878
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1a

    #@17
    .line 1879
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performConfigurationChanged(Landroid/content/res/Configuration;)V

    #@1a
    .line 1876
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 1883
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_1d
    return-void
.end method

.method public dispatchContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    #@0
    .prologue
    .line 1969
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_22

    #@4
    .line 1970
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_22

    #@d
    .line 1971
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1972
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1f

    #@17
    .line 1973
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performContextItemSelected(Landroid/view/MenuItem;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_1f

    #@1d
    .line 1974
    const/4 v2, 0x1

    #@1e
    .line 1979
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :goto_1e
    return v2

    #@1f
    .line 1970
    .restart local v0       #f:Landroid/app/Fragment;
    .restart local v1       #i:I
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_5

    #@22
    .line 1979
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_1e
.end method

.method public dispatchCreate()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1834
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@3
    .line 1835
    const/4 v0, 0x1

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@7
    .line 1836
    return-void
.end method

.method public dispatchCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 8
    .parameter "menu"
    .parameter "inflater"

    #@0
    .prologue
    .line 1908
    const/4 v3, 0x0

    #@1
    .line 1909
    .local v3, show:Z
    const/4 v2, 0x0

    #@2
    .line 1910
    .local v2, newMenus:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@4
    if-eqz v4, :cond_2d

    #@6
    .line 1911
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v4

    #@d
    if-ge v1, v4, :cond_2d

    #@f
    .line 1912
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/app/Fragment;

    #@17
    .line 1913
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_2a

    #@19
    .line 1914
    invoke-virtual {v0, p1, p2}, Landroid/app/Fragment;->performCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    #@1c
    move-result v4

    #@1d
    if-eqz v4, :cond_2a

    #@1f
    .line 1915
    const/4 v3, 0x1

    #@20
    .line 1916
    if-nez v2, :cond_27

    #@22
    .line 1917
    new-instance v2, Ljava/util/ArrayList;

    #@24
    .end local v2           #newMenus:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@27
    .line 1919
    .restart local v2       #newMenus:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    :cond_27
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 1911
    :cond_2a
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_7

    #@2d
    .line 1925
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_2d
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@2f
    if-eqz v4, :cond_50

    #@31
    .line 1926
    const/4 v1, 0x0

    #@32
    .restart local v1       #i:I
    :goto_32
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@37
    move-result v4

    #@38
    if-ge v1, v4, :cond_50

    #@3a
    .line 1927
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v0

    #@40
    check-cast v0, Landroid/app/Fragment;

    #@42
    .line 1928
    .restart local v0       #f:Landroid/app/Fragment;
    if-eqz v2, :cond_4a

    #@44
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@47
    move-result v4

    #@48
    if-nez v4, :cond_4d

    #@4a
    .line 1929
    :cond_4a
    invoke-virtual {v0}, Landroid/app/Fragment;->onDestroyOptionsMenu()V

    #@4d
    .line 1926
    :cond_4d
    add-int/lit8 v1, v1, 0x1

    #@4f
    goto :goto_32

    #@50
    .line 1934
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_50
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@52
    .line 1936
    return v3
.end method

.method public dispatchDestroy()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1866
    const/4 v0, 0x1

    #@3
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    #@5
    .line 1867
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@8
    .line 1868
    invoke-virtual {p0, v2, v2}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@b
    .line 1869
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@d
    .line 1870
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    #@f
    .line 1871
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@11
    .line 1872
    return-void
.end method

.method public dispatchDestroyView()V
    .registers 3

    #@0
    .prologue
    .line 1862
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@5
    .line 1863
    return-void
.end method

.method public dispatchLowMemory()V
    .registers 4

    #@0
    .prologue
    .line 1886
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_1d

    #@4
    .line 1887
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_1d

    #@d
    .line 1888
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1889
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1a

    #@17
    .line 1890
    invoke-virtual {v0}, Landroid/app/Fragment;->performLowMemory()V

    #@1a
    .line 1887
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 1894
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_1d
    return-void
.end method

.method public dispatchOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    #@0
    .prologue
    .line 1955
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_22

    #@4
    .line 1956
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_22

    #@d
    .line 1957
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1958
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1f

    #@17
    .line 1959
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@1a
    move-result v2

    #@1b
    if-eqz v2, :cond_1f

    #@1d
    .line 1960
    const/4 v2, 0x1

    #@1e
    .line 1965
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :goto_1e
    return v2

    #@1f
    .line 1956
    .restart local v0       #f:Landroid/app/Fragment;
    .restart local v1       #i:I
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_5

    #@22
    .line 1965
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_22
    const/4 v2, 0x0

    #@23
    goto :goto_1e
.end method

.method public dispatchOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 5
    .parameter "menu"

    #@0
    .prologue
    .line 1983
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_1d

    #@4
    .line 1984
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_1d

    #@d
    .line 1985
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1986
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1a

    #@17
    .line 1987
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performOptionsMenuClosed(Landroid/view/Menu;)V

    #@1a
    .line 1984
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 1991
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_1d
    return-void
.end method

.method public dispatchPause()V
    .registers 3

    #@0
    .prologue
    .line 1854
    const/4 v0, 0x4

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@5
    .line 1855
    return-void
.end method

.method public dispatchPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .parameter "menu"

    #@0
    .prologue
    .line 1940
    const/4 v2, 0x0

    #@1
    .line 1941
    .local v2, show:Z
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@3
    if-eqz v3, :cond_22

    #@5
    .line 1942
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v1, v3, :cond_22

    #@e
    .line 1943
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/app/Fragment;

    #@16
    .line 1944
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1f

    #@18
    .line 1945
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    .line 1946
    const/4 v2, 0x1

    #@1f
    .line 1942
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_6

    #@22
    .line 1951
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_22
    return v2
.end method

.method public dispatchResume()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1849
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@3
    .line 1850
    const/4 v0, 0x5

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@7
    .line 1851
    return-void
.end method

.method public dispatchStart()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1844
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@3
    .line 1845
    const/4 v0, 0x4

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@7
    .line 1846
    return-void
.end method

.method public dispatchStop()V
    .registers 3

    #@0
    .prologue
    .line 1858
    const/4 v0, 0x3

    #@1
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    #@5
    .line 1859
    return-void
.end method

.method public dispatchTrimMemory(I)V
    .registers 5
    .parameter "level"

    #@0
    .prologue
    .line 1897
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_1d

    #@4
    .line 1898
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v2

    #@b
    if-ge v1, v2, :cond_1d

    #@d
    .line 1899
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/app/Fragment;

    #@15
    .line 1900
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1a

    #@17
    .line 1901
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->performTrimMemory(I)V

    #@1a
    .line 1898
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_5

    #@1d
    .line 1905
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_1d
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 13
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 618
    new-instance v6, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v6

    #@9
    const-string v7, "    "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 621
    .local v4, innerPrefix:Ljava/lang/String;
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@15
    if-eqz v6, :cond_5d

    #@17
    .line 622
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@1c
    move-result v0

    #@1d
    .line 623
    .local v0, N:I
    if-lez v0, :cond_5d

    #@1f
    .line 624
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    const-string v6, "Active Fragments in "

    #@24
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27
    .line 625
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@2a
    move-result v6

    #@2b
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@32
    .line 626
    const-string v6, ":"

    #@34
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@37
    .line 627
    const/4 v3, 0x0

    #@38
    .local v3, i:I
    :goto_38
    if-ge v3, v0, :cond_5d

    #@3a
    .line 628
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3f
    move-result-object v2

    #@40
    check-cast v2, Landroid/app/Fragment;

    #@42
    .line 629
    .local v2, f:Landroid/app/Fragment;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@45
    const-string v6, "  #"

    #@47
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@4d
    .line 630
    const-string v6, ": "

    #@4f
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@52
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@55
    .line 631
    if-eqz v2, :cond_5a

    #@57
    .line 632
    invoke-virtual {v2, v4, p2, p3, p4}, Landroid/app/Fragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@5a
    .line 627
    :cond_5a
    add-int/lit8 v3, v3, 0x1

    #@5c
    goto :goto_38

    #@5d
    .line 638
    .end local v0           #N:I
    .end local v2           #f:Landroid/app/Fragment;
    .end local v3           #i:I
    :cond_5d
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@5f
    if-eqz v6, :cond_96

    #@61
    .line 639
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@63
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@66
    move-result v0

    #@67
    .line 640
    .restart local v0       #N:I
    if-lez v0, :cond_96

    #@69
    .line 641
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6c
    const-string v6, "Added Fragments:"

    #@6e
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@71
    .line 642
    const/4 v3, 0x0

    #@72
    .restart local v3       #i:I
    :goto_72
    if-ge v3, v0, :cond_96

    #@74
    .line 643
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@76
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@79
    move-result-object v2

    #@7a
    check-cast v2, Landroid/app/Fragment;

    #@7c
    .line 644
    .restart local v2       #f:Landroid/app/Fragment;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7f
    const-string v6, "  #"

    #@81
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@84
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@87
    .line 645
    const-string v6, ": "

    #@89
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8c
    invoke-virtual {v2}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    #@8f
    move-result-object v6

    #@90
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@93
    .line 642
    add-int/lit8 v3, v3, 0x1

    #@95
    goto :goto_72

    #@96
    .line 650
    .end local v0           #N:I
    .end local v2           #f:Landroid/app/Fragment;
    .end local v3           #i:I
    :cond_96
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@98
    if-eqz v6, :cond_cf

    #@9a
    .line 651
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@9c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v0

    #@a0
    .line 652
    .restart local v0       #N:I
    if-lez v0, :cond_cf

    #@a2
    .line 653
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    const-string v6, "Fragments Created Menus:"

    #@a7
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@aa
    .line 654
    const/4 v3, 0x0

    #@ab
    .restart local v3       #i:I
    :goto_ab
    if-ge v3, v0, :cond_cf

    #@ad
    .line 655
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    #@af
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@b2
    move-result-object v2

    #@b3
    check-cast v2, Landroid/app/Fragment;

    #@b5
    .line 656
    .restart local v2       #f:Landroid/app/Fragment;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b8
    const-string v6, "  #"

    #@ba
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bd
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@c0
    .line 657
    const-string v6, ": "

    #@c2
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c5
    invoke-virtual {v2}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@cc
    .line 654
    add-int/lit8 v3, v3, 0x1

    #@ce
    goto :goto_ab

    #@cf
    .line 662
    .end local v0           #N:I
    .end local v2           #f:Landroid/app/Fragment;
    .end local v3           #i:I
    :cond_cf
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@d1
    if-eqz v6, :cond_10b

    #@d3
    .line 663
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@d5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@d8
    move-result v0

    #@d9
    .line 664
    .restart local v0       #N:I
    if-lez v0, :cond_10b

    #@db
    .line 665
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@de
    const-string v6, "Back Stack:"

    #@e0
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@e3
    .line 666
    const/4 v3, 0x0

    #@e4
    .restart local v3       #i:I
    :goto_e4
    if-ge v3, v0, :cond_10b

    #@e6
    .line 667
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@e8
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@eb
    move-result-object v1

    #@ec
    check-cast v1, Landroid/app/BackStackRecord;

    #@ee
    .line 668
    .local v1, bs:Landroid/app/BackStackRecord;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f1
    const-string v6, "  #"

    #@f3
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f6
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@f9
    .line 669
    const-string v6, ": "

    #@fb
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fe
    invoke-virtual {v1}, Landroid/app/BackStackRecord;->toString()Ljava/lang/String;

    #@101
    move-result-object v6

    #@102
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@105
    .line 670
    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@108
    .line 666
    add-int/lit8 v3, v3, 0x1

    #@10a
    goto :goto_e4

    #@10b
    .line 675
    .end local v0           #N:I
    .end local v1           #bs:Landroid/app/BackStackRecord;
    .end local v3           #i:I
    :cond_10b
    monitor-enter p0

    #@10c
    .line 676
    :try_start_10c
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@10e
    if-eqz v6, :cond_141

    #@110
    .line 677
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@112
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@115
    move-result v0

    #@116
    .line 678
    .restart local v0       #N:I
    if-lez v0, :cond_141

    #@118
    .line 679
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11b
    const-string v6, "Back Stack Indices:"

    #@11d
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@120
    .line 680
    const/4 v3, 0x0

    #@121
    .restart local v3       #i:I
    :goto_121
    if-ge v3, v0, :cond_141

    #@123
    .line 681
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@125
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@128
    move-result-object v1

    #@129
    check-cast v1, Landroid/app/BackStackRecord;

    #@12b
    .line 682
    .restart local v1       #bs:Landroid/app/BackStackRecord;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12e
    const-string v6, "  #"

    #@130
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@133
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@136
    .line 683
    const-string v6, ": "

    #@138
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13b
    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@13e
    .line 680
    add-int/lit8 v3, v3, 0x1

    #@140
    goto :goto_121

    #@141
    .line 688
    .end local v0           #N:I
    .end local v1           #bs:Landroid/app/BackStackRecord;
    .end local v3           #i:I
    :cond_141
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@143
    if-eqz v6, :cond_163

    #@145
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@147
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@14a
    move-result v6

    #@14b
    if-lez v6, :cond_163

    #@14d
    .line 689
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@150
    const-string/jumbo v6, "mAvailBackStackIndices: "

    #@153
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@156
    .line 690
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@158
    invoke-virtual {v6}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@15b
    move-result-object v6

    #@15c
    invoke-static {v6}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@15f
    move-result-object v6

    #@160
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@163
    .line 692
    :cond_163
    monitor-exit p0
    :try_end_164
    .catchall {:try_start_10c .. :try_end_164} :catchall_199

    #@164
    .line 694
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@166
    if-eqz v6, :cond_19c

    #@168
    .line 695
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@16a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@16d
    move-result v0

    #@16e
    .line 696
    .restart local v0       #N:I
    if-lez v0, :cond_19c

    #@170
    .line 697
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@173
    const-string v6, "Pending Actions:"

    #@175
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@178
    .line 698
    const/4 v3, 0x0

    #@179
    .restart local v3       #i:I
    :goto_179
    if-ge v3, v0, :cond_19c

    #@17b
    .line 699
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@17d
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@180
    move-result-object v5

    #@181
    check-cast v5, Ljava/lang/Runnable;

    #@183
    .line 700
    .local v5, r:Ljava/lang/Runnable;
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@186
    const-string v6, "  #"

    #@188
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18b
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    #@18e
    .line 701
    const-string v6, ": "

    #@190
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@193
    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@196
    .line 698
    add-int/lit8 v3, v3, 0x1

    #@198
    goto :goto_179

    #@199
    .line 692
    .end local v0           #N:I
    .end local v3           #i:I
    .end local v5           #r:Ljava/lang/Runnable;
    :catchall_199
    move-exception v6

    #@19a
    :try_start_19a
    monitor-exit p0
    :try_end_19b
    .catchall {:try_start_19a .. :try_end_19b} :catchall_199

    #@19b
    throw v6

    #@19c
    .line 706
    :cond_19c
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19f
    const-string v6, "FragmentManager misc state:"

    #@1a1
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1a4
    .line 707
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a7
    const-string v6, "  mActivity="

    #@1a9
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ac
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@1ae
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1b1
    .line 708
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b4
    const-string v6, "  mContainer="

    #@1b6
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b9
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    #@1bb
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1be
    .line 709
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@1c0
    if-eqz v6, :cond_1cf

    #@1c2
    .line 710
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c5
    const-string v6, "  mParent="

    #@1c7
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ca
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@1cc
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1cf
    .line 712
    :cond_1cf
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d2
    const-string v6, "  mCurState="

    #@1d4
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d7
    iget v6, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@1d9
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(I)V

    #@1dc
    .line 713
    const-string v6, " mStateSaved="

    #@1de
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e1
    iget-boolean v6, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@1e3
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Z)V

    #@1e6
    .line 714
    const-string v6, " mDestroyed="

    #@1e8
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1eb
    iget-boolean v6, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    #@1ed
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Z)V

    #@1f0
    .line 715
    iget-boolean v6, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@1f2
    if-eqz v6, :cond_201

    #@1f4
    .line 716
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f7
    const-string v6, "  mNeedMenuInvalidate="

    #@1f9
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1fc
    .line 717
    iget-boolean v6, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@1fe
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Z)V

    #@201
    .line 719
    :cond_201
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    #@203
    if-eqz v6, :cond_212

    #@205
    .line 720
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@208
    const-string v6, "  mNoTransactionsBecause="

    #@20a
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20d
    .line 721
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    #@20f
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@212
    .line 723
    :cond_212
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@214
    if-eqz v6, :cond_233

    #@216
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@218
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@21b
    move-result v6

    #@21c
    if-lez v6, :cond_233

    #@21e
    .line 724
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@221
    const-string v6, "  mAvailIndices: "

    #@223
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@226
    .line 725
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@228
    invoke-virtual {v6}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@22b
    move-result-object v6

    #@22c
    invoke-static {v6}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@22f
    move-result-object v6

    #@230
    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@233
    .line 727
    :cond_233
    return-void
.end method

.method public enqueueAction(Ljava/lang/Runnable;Z)V
    .registers 5
    .parameter "action"
    .parameter "allowStateLoss"

    #@0
    .prologue
    .line 1328
    if-nez p2, :cond_5

    #@2
    .line 1329
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    #@5
    .line 1331
    :cond_5
    monitor-enter p0

    #@6
    .line 1332
    :try_start_6
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@8
    if-nez v0, :cond_15

    #@a
    .line 1333
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    const-string v1, "Activity has been destroyed"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1343
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_6 .. :try_end_14} :catchall_12

    #@14
    throw v0

    #@15
    .line 1335
    :cond_15
    :try_start_15
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@17
    if-nez v0, :cond_20

    #@19
    .line 1336
    new-instance v0, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@20
    .line 1338
    :cond_20
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    .line 1339
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@27
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@2a
    move-result v0

    #@2b
    const/4 v1, 0x1

    #@2c
    if-ne v0, v1, :cond_40

    #@2e
    .line 1340
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@30
    iget-object v0, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@32
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    #@34
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@37
    .line 1341
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@39
    iget-object v0, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@3b
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    #@3d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@40
    .line 1343
    :cond_40
    monitor-exit p0
    :try_end_41
    .catchall {:try_start_15 .. :try_end_41} :catchall_12

    #@41
    .line 1344
    return-void
.end method

.method public execPendingActions()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1406
    iget-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    #@3
    if-eqz v5, :cond_d

    #@5
    .line 1407
    new-instance v5, Ljava/lang/IllegalStateException;

    #@7
    const-string v6, "Recursive entry to executePendingTransactions"

    #@9
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v5

    #@d
    .line 1410
    :cond_d
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@10
    move-result-object v5

    #@11
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@13
    iget-object v6, v6, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@15
    invoke-virtual {v6}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@18
    move-result-object v6

    #@19
    if-eq v5, v6, :cond_23

    #@1b
    .line 1411
    new-instance v5, Ljava/lang/IllegalStateException;

    #@1d
    const-string v6, "Must be called from main thread of process"

    #@1f
    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v5

    #@23
    .line 1414
    :cond_23
    const/4 v0, 0x0

    #@24
    .line 1419
    .local v0, didSomething:Z
    :goto_24
    monitor-enter p0

    #@25
    .line 1420
    :try_start_25
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@27
    if-eqz v5, :cond_31

    #@29
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v5

    #@2f
    if-nez v5, :cond_58

    #@31
    .line 1421
    :cond_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_25 .. :try_end_32} :catchall_96

    #@32
    .line 1442
    iget-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    #@34
    if-eqz v5, :cond_a4

    #@36
    .line 1443
    const/4 v3, 0x0

    #@37
    .line 1444
    .local v3, loadersRunning:Z
    const/4 v2, 0x0

    #@38
    .local v2, i:I
    :goto_38
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@3d
    move-result v5

    #@3e
    if-ge v2, v5, :cond_9d

    #@40
    .line 1445
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@45
    move-result-object v1

    #@46
    check-cast v1, Landroid/app/Fragment;

    #@48
    .line 1446
    .local v1, f:Landroid/app/Fragment;
    if-eqz v1, :cond_55

    #@4a
    iget-object v5, v1, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@4c
    if-eqz v5, :cond_55

    #@4e
    .line 1447
    iget-object v5, v1, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@50
    invoke-virtual {v5}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    #@53
    move-result v5

    #@54
    or-int/2addr v3, v5

    #@55
    .line 1444
    :cond_55
    add-int/lit8 v2, v2, 0x1

    #@57
    goto :goto_38

    #@58
    .line 1424
    .end local v1           #f:Landroid/app/Fragment;
    .end local v2           #i:I
    .end local v3           #loadersRunning:Z
    :cond_58
    :try_start_58
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@5a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@5d
    move-result v4

    #@5e
    .line 1425
    .local v4, numActions:I
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@60
    if-eqz v5, :cond_67

    #@62
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@64
    array-length v5, v5

    #@65
    if-ge v5, v4, :cond_6b

    #@67
    .line 1426
    :cond_67
    new-array v5, v4, [Ljava/lang/Runnable;

    #@69
    iput-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@6b
    .line 1428
    :cond_6b
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@6d
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@6f
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@72
    .line 1429
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    #@74
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@77
    .line 1430
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@79
    iget-object v5, v5, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@7b
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    #@7d
    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@80
    .line 1431
    monitor-exit p0
    :try_end_81
    .catchall {:try_start_58 .. :try_end_81} :catchall_96

    #@81
    .line 1433
    const/4 v5, 0x1

    #@82
    iput-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    #@84
    .line 1434
    const/4 v2, 0x0

    #@85
    .restart local v2       #i:I
    :goto_85
    if-ge v2, v4, :cond_99

    #@87
    .line 1435
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@89
    aget-object v5, v5, v2

    #@8b
    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    #@8e
    .line 1436
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpActions:[Ljava/lang/Runnable;

    #@90
    const/4 v6, 0x0

    #@91
    aput-object v6, v5, v2

    #@93
    .line 1434
    add-int/lit8 v2, v2, 0x1

    #@95
    goto :goto_85

    #@96
    .line 1431
    .end local v2           #i:I
    .end local v4           #numActions:I
    :catchall_96
    move-exception v5

    #@97
    :try_start_97
    monitor-exit p0
    :try_end_98
    .catchall {:try_start_97 .. :try_end_98} :catchall_96

    #@98
    throw v5

    #@99
    .line 1438
    .restart local v2       #i:I
    .restart local v4       #numActions:I
    :cond_99
    iput-boolean v7, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    #@9b
    .line 1439
    const/4 v0, 0x1

    #@9c
    .line 1440
    goto :goto_24

    #@9d
    .line 1450
    .end local v4           #numActions:I
    .restart local v3       #loadersRunning:Z
    :cond_9d
    if-nez v3, :cond_a4

    #@9f
    .line 1451
    iput-boolean v7, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    #@a1
    .line 1452
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->startPendingDeferredFragments()V

    #@a4
    .line 1455
    .end local v2           #i:I
    .end local v3           #loadersRunning:Z
    :cond_a4
    return v0
.end method

.method public executePendingTransactions()Z
    .registers 2

    #@0
    .prologue
    .line 474
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public findFragmentById(I)Landroid/app/Fragment;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 1261
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_20

    #@4
    .line 1263
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v2

    #@a
    add-int/lit8 v1, v2, -0x1

    #@c
    .local v1, i:I
    :goto_c
    if-ltz v1, :cond_20

    #@e
    .line 1264
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/app/Fragment;

    #@16
    .line 1265
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1d

    #@18
    iget v2, v0, Landroid/app/Fragment;->mFragmentId:I

    #@1a
    if-ne v2, p1, :cond_1d

    #@1c
    .line 1279
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_1c
    :goto_1c
    return-object v0

    #@1d
    .line 1263
    .restart local v0       #f:Landroid/app/Fragment;
    .restart local v1       #i:I
    :cond_1d
    add-int/lit8 v1, v1, -0x1

    #@1f
    goto :goto_c

    #@20
    .line 1270
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_20
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@22
    if-eqz v2, :cond_3f

    #@24
    .line 1272
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@26
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@29
    move-result v2

    #@2a
    add-int/lit8 v1, v2, -0x1

    #@2c
    .restart local v1       #i:I
    :goto_2c
    if-ltz v1, :cond_3f

    #@2e
    .line 1273
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@33
    move-result-object v0

    #@34
    check-cast v0, Landroid/app/Fragment;

    #@36
    .line 1274
    .restart local v0       #f:Landroid/app/Fragment;
    if-eqz v0, :cond_3c

    #@38
    iget v2, v0, Landroid/app/Fragment;->mFragmentId:I

    #@3a
    if-eq v2, p1, :cond_1c

    #@3c
    .line 1272
    :cond_3c
    add-int/lit8 v1, v1, -0x1

    #@3e
    goto :goto_2c

    #@3f
    .line 1279
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_1c
.end method

.method public findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
    .registers 5
    .parameter "tag"

    #@0
    .prologue
    .line 1283
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_26

    #@4
    if-eqz p1, :cond_26

    #@6
    .line 1285
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v2

    #@c
    add-int/lit8 v1, v2, -0x1

    #@e
    .local v1, i:I
    :goto_e
    if-ltz v1, :cond_26

    #@10
    .line 1286
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/app/Fragment;

    #@18
    .line 1287
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_23

    #@1a
    iget-object v2, v0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_23

    #@22
    .line 1301
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_22
    :goto_22
    return-object v0

    #@23
    .line 1285
    .restart local v0       #f:Landroid/app/Fragment;
    .restart local v1       #i:I
    :cond_23
    add-int/lit8 v1, v1, -0x1

    #@25
    goto :goto_e

    #@26
    .line 1292
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_26
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@28
    if-eqz v2, :cond_4b

    #@2a
    if-eqz p1, :cond_4b

    #@2c
    .line 1294
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v2

    #@32
    add-int/lit8 v1, v2, -0x1

    #@34
    .restart local v1       #i:I
    :goto_34
    if-ltz v1, :cond_4b

    #@36
    .line 1295
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/app/Fragment;

    #@3e
    .line 1296
    .restart local v0       #f:Landroid/app/Fragment;
    if-eqz v0, :cond_48

    #@40
    iget-object v2, v0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@42
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v2

    #@46
    if-nez v2, :cond_22

    #@48
    .line 1294
    :cond_48
    add-int/lit8 v1, v1, -0x1

    #@4a
    goto :goto_34

    #@4b
    .line 1301
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_4b
    const/4 v0, 0x0

    #@4c
    goto :goto_22
.end method

.method public findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;
    .registers 5
    .parameter "who"

    #@0
    .prologue
    .line 1305
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@2
    if-eqz v2, :cond_24

    #@4
    if-eqz p1, :cond_24

    #@6
    .line 1306
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v2

    #@c
    add-int/lit8 v1, v2, -0x1

    #@e
    .local v1, i:I
    :goto_e
    if-ltz v1, :cond_24

    #@10
    .line 1307
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, Landroid/app/Fragment;

    #@18
    .line 1308
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_21

    #@1a
    invoke-virtual {v0, p1}, Landroid/app/Fragment;->findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;

    #@1d
    move-result-object v0

    #@1e
    if-eqz v0, :cond_21

    #@20
    .line 1313
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :goto_20
    return-object v0

    #@21
    .line 1306
    .restart local v0       #f:Landroid/app/Fragment;
    .restart local v1       #i:I
    :cond_21
    add-int/lit8 v1, v1, -0x1

    #@23
    goto :goto_e

    #@24
    .line 1313
    .end local v0           #f:Landroid/app/Fragment;
    .end local v1           #i:I
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_20
.end method

.method public freeBackStackIndex(I)V
    .registers 5
    .parameter "index"

    #@0
    .prologue
    .line 1392
    monitor-enter p0

    #@1
    .line 1393
    :try_start_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@7
    .line 1394
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@9
    if-nez v0, :cond_12

    #@b
    .line 1395
    new-instance v0, Ljava/util/ArrayList;

    #@d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@12
    .line 1397
    :cond_12
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@14
    if-eqz v0, :cond_2e

    #@16
    const-string v0, "FragmentManager"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "Freeing back stack index "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1398
    :cond_2e
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 1399
    monitor-exit p0

    #@38
    .line 1400
    return-void

    #@39
    .line 1399
    :catchall_39
    move-exception v0

    #@3a
    monitor-exit p0
    :try_end_3b
    .catchall {:try_start_1 .. :try_end_3b} :catchall_39

    #@3b
    throw v0
.end method

.method public getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 538
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/FragmentManager$BackStackEntry;

    #@8
    return-object v0
.end method

.method public getBackStackEntryCount()I
    .registers 2

    #@0
    .prologue
    .line 533
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;
    .registers 8
    .parameter "bundle"
    .parameter "key"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 567
    invoke-virtual {p1, p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@4
    move-result v1

    #@5
    .line 568
    .local v1, index:I
    if-ne v1, v2, :cond_9

    #@7
    .line 569
    const/4 v0, 0x0

    #@8
    .line 580
    :cond_8
    :goto_8
    return-object v0

    #@9
    .line 571
    :cond_9
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v2

    #@f
    if-lt v1, v2, :cond_36

    #@11
    .line 572
    new-instance v2, Ljava/lang/IllegalStateException;

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "Fragement no longer exists for key "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    const-string v4, ": index "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@33
    invoke-direct {p0, v2}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@36
    .line 575
    :cond_36
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/app/Fragment;

    #@3e
    .line 576
    .local v0, f:Landroid/app/Fragment;
    if-nez v0, :cond_8

    #@40
    .line 577
    new-instance v2, Ljava/lang/IllegalStateException;

    #@42
    new-instance v3, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v4, "Fragement no longer exists for key "

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    const-string v4, ": index "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@62
    invoke-direct {p0, v2}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@65
    goto :goto_8
.end method

.method public hideFragment(Landroid/app/Fragment;II)V
    .registers 10
    .parameter "fragment"
    .parameter "transition"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1168
    sget-boolean v2, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@3
    if-eqz v2, :cond_1d

    #@5
    const-string v2, "FragmentManager"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "hide: "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1169
    :cond_1d
    iget-boolean v2, p1, Landroid/app/Fragment;->mHidden:Z

    #@1f
    if-nez v2, :cond_4f

    #@21
    .line 1170
    iput-boolean v5, p1, Landroid/app/Fragment;->mHidden:Z

    #@23
    .line 1171
    iget-object v2, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@25
    if-eqz v2, :cond_3e

    #@27
    .line 1172
    invoke-virtual {p0, p1, p2, v5, p3}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    #@2a
    move-result-object v0

    #@2b
    .line 1174
    .local v0, anim:Landroid/animation/Animator;
    if-eqz v0, :cond_50

    #@2d
    .line 1175
    iget-object v2, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@2f
    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@32
    .line 1178
    move-object v1, p1

    #@33
    .line 1179
    .local v1, finalFragment:Landroid/app/Fragment;
    new-instance v2, Landroid/app/FragmentManagerImpl$6;

    #@35
    invoke-direct {v2, p0, v1}, Landroid/app/FragmentManagerImpl$6;-><init>(Landroid/app/FragmentManagerImpl;Landroid/app/Fragment;)V

    #@38
    invoke-virtual {v0, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@3b
    .line 1187
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@3e
    .line 1192
    .end local v0           #anim:Landroid/animation/Animator;
    .end local v1           #finalFragment:Landroid/app/Fragment;
    :cond_3e
    :goto_3e
    iget-boolean v2, p1, Landroid/app/Fragment;->mAdded:Z

    #@40
    if-eqz v2, :cond_4c

    #@42
    iget-boolean v2, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@44
    if-eqz v2, :cond_4c

    #@46
    iget-boolean v2, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@48
    if-eqz v2, :cond_4c

    #@4a
    .line 1193
    iput-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@4c
    .line 1195
    :cond_4c
    invoke-virtual {p1, v5}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    #@4f
    .line 1197
    :cond_4f
    return-void

    #@50
    .line 1189
    .restart local v0       #anim:Landroid/animation/Animator;
    :cond_50
    iget-object v2, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@52
    const/16 v3, 0x8

    #@54
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@57
    goto :goto_3e
.end method

.method public invalidateOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 1995
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@6
    const/4 v1, 0x5

    #@7
    if-ne v0, v1, :cond_f

    #@9
    .line 1996
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@b
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    #@e
    .line 2000
    :goto_e
    return-void

    #@f
    .line 1998
    :cond_f
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@12
    goto :goto_e
.end method

.method public isDestroyed()Z
    .registers 2

    #@0
    .prologue
    .line 598
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    #@2
    return v0
.end method

.method loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;
    .registers 12
    .parameter "fragment"
    .parameter "transit"
    .parameter "enter"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 731
    iget v5, p1, Landroid/app/Fragment;->mNextAnim:I

    #@3
    invoke-virtual {p1, p2, p3, v5}, Landroid/app/Fragment;->onCreateAnimator(IZI)Landroid/animation/Animator;

    #@6
    move-result-object v1

    #@7
    .line 733
    .local v1, animObj:Landroid/animation/Animator;
    if-eqz v1, :cond_a

    #@9
    .line 769
    .end local v1           #animObj:Landroid/animation/Animator;
    :goto_9
    return-object v1

    #@a
    .line 737
    .restart local v1       #animObj:Landroid/animation/Animator;
    :cond_a
    iget v5, p1, Landroid/app/Fragment;->mNextAnim:I

    #@c
    if-eqz v5, :cond_1a

    #@e
    .line 738
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@10
    iget v6, p1, Landroid/app/Fragment;->mNextAnim:I

    #@12
    invoke-static {v5, v6}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    #@15
    move-result-object v0

    #@16
    .line 739
    .local v0, anim:Landroid/animation/Animator;
    if-eqz v0, :cond_1a

    #@18
    move-object v1, v0

    #@19
    .line 740
    goto :goto_9

    #@1a
    .line 744
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_1a
    if-nez p2, :cond_1e

    #@1c
    move-object v1, v4

    #@1d
    .line 745
    goto :goto_9

    #@1e
    .line 748
    :cond_1e
    invoke-static {p2, p3}, Landroid/app/FragmentManagerImpl;->transitToStyleIndex(IZ)I

    #@21
    move-result v3

    #@22
    .line 749
    .local v3, styleIndex:I
    if-gez v3, :cond_26

    #@24
    move-object v1, v4

    #@25
    .line 750
    goto :goto_9

    #@26
    .line 753
    :cond_26
    if-nez p4, :cond_3c

    #@28
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@2a
    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@2d
    move-result-object v5

    #@2e
    if-eqz v5, :cond_3c

    #@30
    .line 754
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@32
    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@39
    move-result-object v5

    #@3a
    iget p4, v5, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@3c
    .line 756
    :cond_3c
    if-nez p4, :cond_40

    #@3e
    move-object v1, v4

    #@3f
    .line 757
    goto :goto_9

    #@40
    .line 760
    :cond_40
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@42
    sget-object v6, Lcom/android/internal/R$styleable;->FragmentAnimation:[I

    #@44
    invoke-virtual {v5, p4, v6}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@47
    move-result-object v2

    #@48
    .line 762
    .local v2, attrs:Landroid/content/res/TypedArray;
    const/4 v5, 0x0

    #@49
    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@4c
    move-result v0

    #@4d
    .line 763
    .local v0, anim:I
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    #@50
    .line 765
    if-nez v0, :cond_54

    #@52
    move-object v1, v4

    #@53
    .line 766
    goto :goto_9

    #@54
    .line 769
    :cond_54
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@56
    invoke-static {v4, v0}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    #@59
    move-result-object v1

    #@5a
    goto :goto_9
.end method

.method makeActive(Landroid/app/Fragment;)V
    .registers 5
    .parameter "f"

    #@0
    .prologue
    .line 1087
    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    #@2
    if-ltz v0, :cond_5

    #@4
    .line 1103
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1091
    :cond_5
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@7
    if-eqz v0, :cond_11

    #@9
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@e
    move-result v0

    #@f
    if-gtz v0, :cond_49

    #@11
    .line 1092
    :cond_11
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@13
    if-nez v0, :cond_1c

    #@15
    .line 1093
    new-instance v0, Ljava/util/ArrayList;

    #@17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1a
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@1c
    .line 1095
    :cond_1c
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@21
    move-result v0

    #@22
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@24
    invoke-virtual {p1, v0, v1}, Landroid/app/Fragment;->setIndex(ILandroid/app/Fragment;)V

    #@27
    .line 1096
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@29
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    .line 1102
    :goto_2c
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@2e
    if-eqz v0, :cond_4

    #@30
    const-string v0, "FragmentManager"

    #@32
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "Allocated fragment index "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_4

    #@49
    .line 1099
    :cond_49
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@4b
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@4d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@50
    move-result v1

    #@51
    add-int/lit8 v1, v1, -0x1

    #@53
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@56
    move-result-object v0

    #@57
    check-cast v0, Ljava/lang/Integer;

    #@59
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@5c
    move-result v0

    #@5d
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@5f
    invoke-virtual {p1, v0, v1}, Landroid/app/Fragment;->setIndex(ILandroid/app/Fragment;)V

    #@62
    .line 1100
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@64
    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    #@66
    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@69
    goto :goto_2c
.end method

.method makeInactive(Landroid/app/Fragment;)V
    .registers 5
    .parameter "f"

    #@0
    .prologue
    .line 1106
    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    #@2
    if-gez v0, :cond_5

    #@4
    .line 1118
    :goto_4
    return-void

    #@5
    .line 1110
    :cond_5
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@7
    if-eqz v0, :cond_21

    #@9
    const-string v0, "FragmentManager"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Freeing fragment index "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1111
    :cond_21
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@23
    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    #@25
    const/4 v2, 0x0

    #@26
    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 1112
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@2b
    if-nez v0, :cond_34

    #@2d
    .line 1113
    new-instance v0, Ljava/util/ArrayList;

    #@2f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@32
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@34
    .line 1115
    :cond_34
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@36
    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    #@38
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@3f
    .line 1116
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@41
    iget-object v1, p1, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@43
    invoke-virtual {v0, v1}, Landroid/app/Activity;->invalidateFragment(Ljava/lang/String;)V

    #@46
    .line 1117
    invoke-virtual {p1}, Landroid/app/Fragment;->initState()V

    #@49
    goto :goto_4
.end method

.method moveToState(IIIZ)V
    .registers 13
    .parameter "newState"
    .parameter "transit"
    .parameter "transitStyle"
    .parameter "always"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1043
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@3
    if-nez v0, :cond_f

    #@5
    if-eqz p1, :cond_f

    #@7
    .line 1044
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v2, "No activity"

    #@b
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 1047
    :cond_f
    if-nez p4, :cond_16

    #@11
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@13
    if-ne v0, p1, :cond_16

    #@15
    .line 1073
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1051
    :cond_16
    iput p1, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@18
    .line 1052
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@1a
    if-eqz v0, :cond_15

    #@1c
    .line 1053
    const/4 v7, 0x0

    #@1d
    .line 1054
    .local v7, loadersRunning:Z
    const/4 v6, 0x0

    #@1e
    .local v6, i:I
    :goto_1e
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v0

    #@24
    if-ge v6, v0, :cond_45

    #@26
    .line 1055
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@28
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v1

    #@2c
    check-cast v1, Landroid/app/Fragment;

    #@2e
    .line 1056
    .local v1, f:Landroid/app/Fragment;
    if-eqz v1, :cond_42

    #@30
    move-object v0, p0

    #@31
    move v2, p1

    #@32
    move v3, p2

    #@33
    move v4, p3

    #@34
    .line 1057
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@37
    .line 1058
    iget-object v0, v1, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@39
    if-eqz v0, :cond_42

    #@3b
    .line 1059
    iget-object v0, v1, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@3d
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    #@40
    move-result v0

    #@41
    or-int/2addr v7, v0

    #@42
    .line 1054
    :cond_42
    add-int/lit8 v6, v6, 0x1

    #@44
    goto :goto_1e

    #@45
    .line 1064
    .end local v1           #f:Landroid/app/Fragment;
    :cond_45
    if-nez v7, :cond_4a

    #@47
    .line 1065
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->startPendingDeferredFragments()V

    #@4a
    .line 1068
    :cond_4a
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@4c
    if-eqz v0, :cond_15

    #@4e
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@50
    if-eqz v0, :cond_15

    #@52
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@54
    const/4 v2, 0x5

    #@55
    if-ne v0, v2, :cond_15

    #@57
    .line 1069
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@59
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    #@5c
    .line 1070
    iput-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@5e
    goto :goto_15
.end method

.method moveToState(IZ)V
    .registers 4
    .parameter "newState"
    .parameter "always"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1039
    invoke-virtual {p0, p1, v0, v0, p2}, Landroid/app/FragmentManagerImpl;->moveToState(IIIZ)V

    #@4
    .line 1040
    return-void
.end method

.method moveToState(Landroid/app/Fragment;)V
    .registers 8
    .parameter "f"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1035
    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move v4, v3

    #@6
    move v5, v3

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@a
    .line 1036
    return-void
.end method

.method moveToState(Landroid/app/Fragment;IIIZ)V
    .registers 16
    .parameter "f"
    .parameter "newState"
    .parameter "transit"
    .parameter "transitionStyle"
    .parameter "keepActive"

    #@0
    .prologue
    .line 786
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@2
    if-eqz v0, :cond_4

    #@4
    .line 791
    :cond_4
    iget-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    #@6
    if-eqz v0, :cond_c

    #@8
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    #@a
    if-eqz v0, :cond_10

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    if-le p2, v0, :cond_10

    #@f
    .line 792
    const/4 p2, 0x1

    #@10
    .line 794
    :cond_10
    iget-boolean v0, p1, Landroid/app/Fragment;->mRemoving:Z

    #@12
    if-eqz v0, :cond_1a

    #@14
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@16
    if-le p2, v0, :cond_1a

    #@18
    .line 796
    iget p2, p1, Landroid/app/Fragment;->mState:I

    #@1a
    .line 800
    :cond_1a
    iget-boolean v0, p1, Landroid/app/Fragment;->mDeferStart:Z

    #@1c
    if-eqz v0, :cond_27

    #@1e
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@20
    const/4 v1, 0x4

    #@21
    if-ge v0, v1, :cond_27

    #@23
    const/4 v0, 0x3

    #@24
    if-le p2, v0, :cond_27

    #@26
    .line 801
    const/4 p2, 0x3

    #@27
    .line 803
    :cond_27
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@29
    if-ge v0, p2, :cond_249

    #@2b
    .line 807
    iget-boolean v0, p1, Landroid/app/Fragment;->mFromLayout:Z

    #@2d
    if-eqz v0, :cond_34

    #@2f
    iget-boolean v0, p1, Landroid/app/Fragment;->mInLayout:Z

    #@31
    if-nez v0, :cond_34

    #@33
    .line 1032
    :goto_33
    return-void

    #@34
    .line 810
    :cond_34
    iget-object v0, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@36
    if-eqz v0, :cond_45

    #@38
    .line 815
    const/4 v0, 0x0

    #@39
    iput-object v0, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@3b
    .line 816
    iget v2, p1, Landroid/app/Fragment;->mStateAfterAnimating:I

    #@3d
    const/4 v3, 0x0

    #@3e
    const/4 v4, 0x0

    #@3f
    const/4 v5, 0x1

    #@40
    move-object v0, p0

    #@41
    move-object v1, p1

    #@42
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@45
    .line 818
    :cond_45
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@47
    packed-switch v0, :pswitch_data_396

    #@4a
    .line 1031
    :cond_4a
    :goto_4a
    iput p2, p1, Landroid/app/Fragment;->mState:I

    #@4c
    goto :goto_33

    #@4d
    .line 820
    :pswitch_4d
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4f
    if-eqz v0, :cond_6a

    #@51
    const-string v0, "FragmentManager"

    #@53
    new-instance v1, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string/jumbo v2, "moveto CREATED: "

    #@5b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v1

    #@5f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v1

    #@67
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 821
    :cond_6a
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@6c
    if-eqz v0, :cond_a7

    #@6e
    .line 822
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@70
    const-string v1, "android:view_state"

    #@72
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@75
    move-result-object v0

    #@76
    iput-object v0, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@78
    .line 824
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@7a
    const-string v1, "android:target_state"

    #@7c
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    #@7f
    move-result-object v0

    #@80
    iput-object v0, p1, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@82
    .line 826
    iget-object v0, p1, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@84
    if-eqz v0, :cond_91

    #@86
    .line 827
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@88
    const-string v1, "android:target_req_state"

    #@8a
    const/4 v2, 0x0

    #@8b
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@8e
    move-result v0

    #@8f
    iput v0, p1, Landroid/app/Fragment;->mTargetRequestCode:I

    #@91
    .line 830
    :cond_91
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@93
    const-string v1, "android:user_visible_hint"

    #@95
    const/4 v2, 0x1

    #@96
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@99
    move-result v0

    #@9a
    iput-boolean v0, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@9c
    .line 832
    iget-boolean v0, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@9e
    if-nez v0, :cond_a7

    #@a0
    .line 833
    const/4 v0, 0x1

    #@a1
    iput-boolean v0, p1, Landroid/app/Fragment;->mDeferStart:Z

    #@a3
    .line 834
    const/4 v0, 0x3

    #@a4
    if-le p2, v0, :cond_a7

    #@a6
    .line 835
    const/4 p2, 0x3

    #@a7
    .line 839
    :cond_a7
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@a9
    iput-object v0, p1, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@ab
    .line 840
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@ad
    iput-object v0, p1, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@af
    .line 841
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@b1
    if-eqz v0, :cond_e4

    #@b3
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@b5
    iget-object v0, v0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@b7
    :goto_b7
    iput-object v0, p1, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@b9
    .line 843
    const/4 v0, 0x0

    #@ba
    iput-boolean v0, p1, Landroid/app/Fragment;->mCalled:Z

    #@bc
    .line 844
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@be
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    #@c1
    .line 845
    iget-boolean v0, p1, Landroid/app/Fragment;->mCalled:Z

    #@c3
    if-nez v0, :cond_e9

    #@c5
    .line 846
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@c7
    new-instance v1, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v2, "Fragment "

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v1

    #@d2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v1

    #@d6
    const-string v2, " did not call through to super.onAttach()"

    #@d8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v1

    #@dc
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v1

    #@e0
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@e3
    throw v0

    #@e4
    .line 841
    :cond_e4
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@e6
    iget-object v0, v0, Landroid/app/Activity;->mFragments:Landroid/app/FragmentManagerImpl;

    #@e8
    goto :goto_b7

    #@e9
    .line 849
    :cond_e9
    iget-object v0, p1, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@eb
    if-nez v0, :cond_f2

    #@ed
    .line 850
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@ef
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onAttachFragment(Landroid/app/Fragment;)V

    #@f2
    .line 853
    :cond_f2
    iget-boolean v0, p1, Landroid/app/Fragment;->mRetaining:Z

    #@f4
    if-nez v0, :cond_fb

    #@f6
    .line 854
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@f8
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->performCreate(Landroid/os/Bundle;)V

    #@fb
    .line 856
    :cond_fb
    const/4 v0, 0x0

    #@fc
    iput-boolean v0, p1, Landroid/app/Fragment;->mRetaining:Z

    #@fe
    .line 857
    iget-boolean v0, p1, Landroid/app/Fragment;->mFromLayout:Z

    #@100
    if-eqz v0, :cond_12d

    #@102
    .line 861
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@104
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    #@107
    move-result-object v0

    #@108
    const/4 v1, 0x0

    #@109
    iget-object v2, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@10b
    invoke-virtual {p1, v0, v1, v2}, Landroid/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    #@10e
    move-result-object v0

    #@10f
    iput-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@111
    .line 863
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@113
    if-eqz v0, :cond_12d

    #@115
    .line 864
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@117
    const/4 v1, 0x0

    #@118
    invoke-virtual {v0, v1}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    #@11b
    .line 865
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    #@11d
    if-eqz v0, :cond_126

    #@11f
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@121
    const/16 v1, 0x8

    #@123
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@126
    .line 866
    :cond_126
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@128
    iget-object v1, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@12a
    invoke-virtual {p1, v0, v1}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    #@12d
    .line 870
    :cond_12d
    :pswitch_12d
    const/4 v0, 0x1

    #@12e
    if-le p2, v0, :cond_1f8

    #@130
    .line 871
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@132
    if-eqz v0, :cond_14d

    #@134
    const-string v0, "FragmentManager"

    #@136
    new-instance v1, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string/jumbo v2, "moveto ACTIVITY_CREATED: "

    #@13e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v1

    #@142
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v1

    #@146
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v1

    #@14a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14d
    .line 872
    :cond_14d
    iget-boolean v0, p1, Landroid/app/Fragment;->mFromLayout:Z

    #@14f
    if-nez v0, :cond_1e7

    #@151
    .line 873
    const/4 v7, 0x0

    #@152
    .line 874
    .local v7, container:Landroid/view/ViewGroup;
    iget v0, p1, Landroid/app/Fragment;->mContainerId:I

    #@154
    if-eqz v0, :cond_1a5

    #@156
    .line 875
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    #@158
    iget v1, p1, Landroid/app/Fragment;->mContainerId:I

    #@15a
    invoke-interface {v0, v1}, Landroid/app/FragmentContainer;->findViewById(I)Landroid/view/View;

    #@15d
    move-result-object v7

    #@15e
    .end local v7           #container:Landroid/view/ViewGroup;
    check-cast v7, Landroid/view/ViewGroup;

    #@160
    .line 876
    .restart local v7       #container:Landroid/view/ViewGroup;
    if-nez v7, :cond_1a5

    #@162
    iget-boolean v0, p1, Landroid/app/Fragment;->mRestored:Z

    #@164
    if-nez v0, :cond_1a5

    #@166
    .line 877
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@168
    new-instance v1, Ljava/lang/StringBuilder;

    #@16a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16d
    const-string v2, "No view found for id 0x"

    #@16f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v1

    #@173
    iget v2, p1, Landroid/app/Fragment;->mContainerId:I

    #@175
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@178
    move-result-object v2

    #@179
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v1

    #@17d
    const-string v2, " ("

    #@17f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v1

    #@183
    invoke-virtual {p1}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    #@186
    move-result-object v2

    #@187
    iget v3, p1, Landroid/app/Fragment;->mContainerId:I

    #@189
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    #@18c
    move-result-object v2

    #@18d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v1

    #@191
    const-string v2, ") for fragment "

    #@193
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v1

    #@197
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v1

    #@19b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v1

    #@19f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a2
    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@1a5
    .line 884
    :cond_1a5
    iput-object v7, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@1a7
    .line 885
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1a9
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    #@1ac
    move-result-object v0

    #@1ad
    iget-object v1, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1af
    invoke-virtual {p1, v0, v7, v1}, Landroid/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    #@1b2
    move-result-object v0

    #@1b3
    iput-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1b5
    .line 887
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1b7
    if-eqz v0, :cond_1e7

    #@1b9
    .line 888
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1bb
    const/4 v1, 0x0

    #@1bc
    invoke-virtual {v0, v1}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    #@1bf
    .line 889
    if-eqz v7, :cond_1d5

    #@1c1
    .line 890
    const/4 v0, 0x1

    #@1c2
    invoke-virtual {p0, p1, p3, v0, p4}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    #@1c5
    move-result-object v6

    #@1c6
    .line 892
    .local v6, anim:Landroid/animation/Animator;
    if-eqz v6, :cond_1d0

    #@1c8
    .line 893
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1ca
    invoke-virtual {v6, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@1cd
    .line 894
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    #@1d0
    .line 896
    :cond_1d0
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1d2
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    #@1d5
    .line 898
    .end local v6           #anim:Landroid/animation/Animator;
    :cond_1d5
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    #@1d7
    if-eqz v0, :cond_1e0

    #@1d9
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1db
    const/16 v1, 0x8

    #@1dd
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@1e0
    .line 899
    :cond_1e0
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1e2
    iget-object v1, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1e4
    invoke-virtual {p1, v0, v1}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    #@1e7
    .line 903
    .end local v7           #container:Landroid/view/ViewGroup;
    :cond_1e7
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1e9
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->performActivityCreated(Landroid/os/Bundle;)V

    #@1ec
    .line 904
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1ee
    if-eqz v0, :cond_1f5

    #@1f0
    .line 905
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1f2
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->restoreViewState(Landroid/os/Bundle;)V

    #@1f5
    .line 907
    :cond_1f5
    const/4 v0, 0x0

    #@1f6
    iput-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@1f8
    .line 911
    :cond_1f8
    :pswitch_1f8
    const/4 v0, 0x3

    #@1f9
    if-le p2, v0, :cond_21b

    #@1fb
    .line 912
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@1fd
    if-eqz v0, :cond_218

    #@1ff
    const-string v0, "FragmentManager"

    #@201
    new-instance v1, Ljava/lang/StringBuilder;

    #@203
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@206
    const-string/jumbo v2, "moveto STARTED: "

    #@209
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v1

    #@20d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v1

    #@211
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@214
    move-result-object v1

    #@215
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@218
    .line 913
    :cond_218
    invoke-virtual {p1}, Landroid/app/Fragment;->performStart()V

    #@21b
    .line 916
    :cond_21b
    :pswitch_21b
    const/4 v0, 0x4

    #@21c
    if-le p2, v0, :cond_4a

    #@21e
    .line 917
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@220
    if-eqz v0, :cond_23b

    #@222
    const-string v0, "FragmentManager"

    #@224
    new-instance v1, Ljava/lang/StringBuilder;

    #@226
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string/jumbo v2, "moveto RESUMED: "

    #@22c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22f
    move-result-object v1

    #@230
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v1

    #@234
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v1

    #@238
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23b
    .line 918
    :cond_23b
    const/4 v0, 0x1

    #@23c
    iput-boolean v0, p1, Landroid/app/Fragment;->mResumed:Z

    #@23e
    .line 919
    invoke-virtual {p1}, Landroid/app/Fragment;->performResume()V

    #@241
    .line 921
    const/4 v0, 0x0

    #@242
    iput-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@244
    .line 922
    const/4 v0, 0x0

    #@245
    iput-object v0, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@247
    goto/16 :goto_4a

    #@249
    .line 925
    :cond_249
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@24b
    if-le v0, p2, :cond_4a

    #@24d
    .line 926
    iget v0, p1, Landroid/app/Fragment;->mState:I

    #@24f
    packed-switch v0, :pswitch_data_3a4

    #@252
    goto/16 :goto_4a

    #@254
    .line 984
    :cond_254
    :goto_254
    :pswitch_254
    const/4 v0, 0x1

    #@255
    if-ge p2, v0, :cond_4a

    #@257
    .line 985
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    #@259
    if-eqz v0, :cond_267

    #@25b
    .line 986
    iget-object v0, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@25d
    if-eqz v0, :cond_267

    #@25f
    .line 993
    iget-object v6, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@261
    .line 994
    .restart local v6       #anim:Landroid/animation/Animator;
    const/4 v0, 0x0

    #@262
    iput-object v0, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@264
    .line 995
    invoke-virtual {v6}, Landroid/animation/Animator;->cancel()V

    #@267
    .line 998
    .end local v6           #anim:Landroid/animation/Animator;
    :cond_267
    iget-object v0, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@269
    if-eqz v0, :cond_332

    #@26b
    .line 1003
    iput p2, p1, Landroid/app/Fragment;->mStateAfterAnimating:I

    #@26d
    .line 1004
    const/4 p2, 0x1

    #@26e
    goto/16 :goto_4a

    #@270
    .line 928
    :pswitch_270
    const/4 v0, 0x5

    #@271
    if-ge p2, v0, :cond_296

    #@273
    .line 929
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@275
    if-eqz v0, :cond_290

    #@277
    const-string v0, "FragmentManager"

    #@279
    new-instance v1, Ljava/lang/StringBuilder;

    #@27b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@27e
    const-string/jumbo v2, "movefrom RESUMED: "

    #@281
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@284
    move-result-object v1

    #@285
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v1

    #@289
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28c
    move-result-object v1

    #@28d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@290
    .line 930
    :cond_290
    invoke-virtual {p1}, Landroid/app/Fragment;->performPause()V

    #@293
    .line 931
    const/4 v0, 0x0

    #@294
    iput-boolean v0, p1, Landroid/app/Fragment;->mResumed:Z

    #@296
    .line 934
    :cond_296
    :pswitch_296
    const/4 v0, 0x4

    #@297
    if-ge p2, v0, :cond_2b9

    #@299
    .line 935
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@29b
    if-eqz v0, :cond_2b6

    #@29d
    const-string v0, "FragmentManager"

    #@29f
    new-instance v1, Ljava/lang/StringBuilder;

    #@2a1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a4
    const-string/jumbo v2, "movefrom STARTED: "

    #@2a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2aa
    move-result-object v1

    #@2ab
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2ae
    move-result-object v1

    #@2af
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b2
    move-result-object v1

    #@2b3
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b6
    .line 936
    :cond_2b6
    invoke-virtual {p1}, Landroid/app/Fragment;->performStop()V

    #@2b9
    .line 940
    :cond_2b9
    :pswitch_2b9
    const/4 v0, 0x2

    #@2ba
    if-ge p2, v0, :cond_254

    #@2bc
    .line 941
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@2be
    if-eqz v0, :cond_2d9

    #@2c0
    const-string v0, "FragmentManager"

    #@2c2
    new-instance v1, Ljava/lang/StringBuilder;

    #@2c4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2c7
    const-string/jumbo v2, "movefrom ACTIVITY_CREATED: "

    #@2ca
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v1

    #@2ce
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d1
    move-result-object v1

    #@2d2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d5
    move-result-object v1

    #@2d6
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d9
    .line 942
    :cond_2d9
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@2db
    if-eqz v0, :cond_2ec

    #@2dd
    .line 945
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@2df
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    #@2e2
    move-result v0

    #@2e3
    if-nez v0, :cond_2ec

    #@2e5
    iget-object v0, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@2e7
    if-nez v0, :cond_2ec

    #@2e9
    .line 946
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->saveFragmentViewState(Landroid/app/Fragment;)V

    #@2ec
    .line 949
    :cond_2ec
    invoke-virtual {p1}, Landroid/app/Fragment;->performDestroyView()V

    #@2ef
    .line 950
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@2f1
    if-eqz v0, :cond_32a

    #@2f3
    iget-object v0, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@2f5
    if-eqz v0, :cond_32a

    #@2f7
    .line 951
    const/4 v6, 0x0

    #@2f8
    .line 952
    .restart local v6       #anim:Landroid/animation/Animator;
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@2fa
    if-lez v0, :cond_305

    #@2fc
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    #@2fe
    if-nez v0, :cond_305

    #@300
    .line 953
    const/4 v0, 0x0

    #@301
    invoke-virtual {p0, p1, p3, v0, p4}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    #@304
    move-result-object v6

    #@305
    .line 956
    :cond_305
    if-eqz v6, :cond_323

    #@307
    .line 957
    iget-object v7, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@309
    .line 958
    .restart local v7       #container:Landroid/view/ViewGroup;
    iget-object v9, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@30b
    .line 959
    .local v9, view:Landroid/view/View;
    move-object v8, p1

    #@30c
    .line 960
    .local v8, fragment:Landroid/app/Fragment;
    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    #@30f
    .line 961
    iput-object v6, p1, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@311
    .line 962
    iput p2, p1, Landroid/app/Fragment;->mStateAfterAnimating:I

    #@313
    .line 963
    new-instance v0, Landroid/app/FragmentManagerImpl$5;

    #@315
    invoke-direct {v0, p0, v7, v9, v8}, Landroid/app/FragmentManagerImpl$5;-><init>(Landroid/app/FragmentManagerImpl;Landroid/view/ViewGroup;Landroid/view/View;Landroid/app/Fragment;)V

    #@318
    invoke-virtual {v6, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@31b
    .line 974
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@31d
    invoke-virtual {v6, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@320
    .line 975
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    #@323
    .line 978
    .end local v7           #container:Landroid/view/ViewGroup;
    .end local v8           #fragment:Landroid/app/Fragment;
    .end local v9           #view:Landroid/view/View;
    :cond_323
    iget-object v0, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@325
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@327
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    #@32a
    .line 980
    .end local v6           #anim:Landroid/animation/Animator;
    :cond_32a
    const/4 v0, 0x0

    #@32b
    iput-object v0, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@32d
    .line 981
    const/4 v0, 0x0

    #@32e
    iput-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@330
    goto/16 :goto_254

    #@332
    .line 1006
    :cond_332
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@334
    if-eqz v0, :cond_34f

    #@336
    const-string v0, "FragmentManager"

    #@338
    new-instance v1, Ljava/lang/StringBuilder;

    #@33a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33d
    const-string/jumbo v2, "movefrom CREATED: "

    #@340
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@343
    move-result-object v1

    #@344
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@347
    move-result-object v1

    #@348
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34b
    move-result-object v1

    #@34c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34f
    .line 1007
    :cond_34f
    iget-boolean v0, p1, Landroid/app/Fragment;->mRetaining:Z

    #@351
    if-nez v0, :cond_356

    #@353
    .line 1008
    invoke-virtual {p1}, Landroid/app/Fragment;->performDestroy()V

    #@356
    .line 1011
    :cond_356
    const/4 v0, 0x0

    #@357
    iput-boolean v0, p1, Landroid/app/Fragment;->mCalled:Z

    #@359
    .line 1012
    invoke-virtual {p1}, Landroid/app/Fragment;->onDetach()V

    #@35c
    .line 1013
    iget-boolean v0, p1, Landroid/app/Fragment;->mCalled:Z

    #@35e
    if-nez v0, :cond_37f

    #@360
    .line 1014
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@362
    new-instance v1, Ljava/lang/StringBuilder;

    #@364
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@367
    const-string v2, "Fragment "

    #@369
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36c
    move-result-object v1

    #@36d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@370
    move-result-object v1

    #@371
    const-string v2, " did not call through to super.onDetach()"

    #@373
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@376
    move-result-object v1

    #@377
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37a
    move-result-object v1

    #@37b
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@37e
    throw v0

    #@37f
    .line 1017
    :cond_37f
    if-nez p5, :cond_4a

    #@381
    .line 1018
    iget-boolean v0, p1, Landroid/app/Fragment;->mRetaining:Z

    #@383
    if-nez v0, :cond_38a

    #@385
    .line 1019
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->makeInactive(Landroid/app/Fragment;)V

    #@388
    goto/16 :goto_4a

    #@38a
    .line 1021
    :cond_38a
    const/4 v0, 0x0

    #@38b
    iput-object v0, p1, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@38d
    .line 1022
    const/4 v0, 0x0

    #@38e
    iput-object v0, p1, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@390
    .line 1023
    const/4 v0, 0x0

    #@391
    iput-object v0, p1, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@393
    goto/16 :goto_4a

    #@395
    .line 818
    nop

    #@396
    :pswitch_data_396
    .packed-switch 0x0
        :pswitch_4d
        :pswitch_12d
        :pswitch_1f8
        :pswitch_1f8
        :pswitch_21b
    .end packed-switch

    #@3a4
    .line 926
    :pswitch_data_3a4
    .packed-switch 0x1
        :pswitch_254
        :pswitch_2b9
        :pswitch_2b9
        :pswitch_296
        :pswitch_270
    .end packed-switch
.end method

.method public noteStateNotSaved()V
    .registers 2

    #@0
    .prologue
    .line 1830
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@3
    .line 1831
    return-void
.end method

.method public performPendingDeferredStart(Landroid/app/Fragment;)V
    .registers 8
    .parameter "f"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 773
    iget-boolean v0, p1, Landroid/app/Fragment;->mDeferStart:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 774
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 776
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    #@c
    .line 782
    :cond_c
    :goto_c
    return-void

    #@d
    .line 779
    :cond_d
    iput-boolean v3, p1, Landroid/app/Fragment;->mDeferStart:Z

    #@f
    .line 780
    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    #@11
    move-object v0, p0

    #@12
    move-object v1, p1

    #@13
    move v4, v3

    #@14
    move v5, v3

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@18
    goto :goto_c
.end method

.method public popBackStack()V
    .registers 3

    #@0
    .prologue
    .line 479
    new-instance v0, Landroid/app/FragmentManagerImpl$2;

    #@2
    invoke-direct {v0, p0}, Landroid/app/FragmentManagerImpl$2;-><init>(Landroid/app/FragmentManagerImpl;)V

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Ljava/lang/Runnable;Z)V

    #@9
    .line 484
    return-void
.end method

.method public popBackStack(II)V
    .registers 6
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    .line 511
    if-gez p1, :cond_1b

    #@2
    .line 512
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "Bad id: "

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 514
    :cond_1b
    new-instance v0, Landroid/app/FragmentManagerImpl$4;

    #@1d
    invoke-direct {v0, p0, p1, p2}, Landroid/app/FragmentManagerImpl$4;-><init>(Landroid/app/FragmentManagerImpl;II)V

    #@20
    const/4 v1, 0x0

    #@21
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Ljava/lang/Runnable;Z)V

    #@24
    .line 519
    return-void
.end method

.method public popBackStack(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "flags"

    #@0
    .prologue
    .line 495
    new-instance v0, Landroid/app/FragmentManagerImpl$3;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/app/FragmentManagerImpl$3;-><init>(Landroid/app/FragmentManagerImpl;Ljava/lang/String;I)V

    #@5
    const/4 v1, 0x0

    #@6
    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Ljava/lang/Runnable;Z)V

    #@9
    .line 500
    return-void
.end method

.method public popBackStackImmediate()Z
    .registers 5

    #@0
    .prologue
    .line 488
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    #@3
    .line 489
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->executePendingTransactions()Z

    #@6
    .line 490
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@8
    iget-object v0, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@a
    const/4 v1, 0x0

    #@b
    const/4 v2, -0x1

    #@c
    const/4 v3, 0x0

    #@d
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/app/FragmentManagerImpl;->popBackStackState(Landroid/os/Handler;Ljava/lang/String;II)Z

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public popBackStackImmediate(II)Z
    .registers 6
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    .line 523
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    #@3
    .line 524
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->executePendingTransactions()Z

    #@6
    .line 525
    if-gez p1, :cond_21

    #@8
    .line 526
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Bad id: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 528
    :cond_21
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@23
    iget-object v0, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@25
    const/4 v1, 0x0

    #@26
    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/app/FragmentManagerImpl;->popBackStackState(Landroid/os/Handler;Ljava/lang/String;II)Z

    #@29
    move-result v0

    #@2a
    return v0
.end method

.method public popBackStackImmediate(Ljava/lang/String;I)Z
    .registers 5
    .parameter "name"
    .parameter "flags"

    #@0
    .prologue
    .line 504
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    #@3
    .line 505
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->executePendingTransactions()Z

    #@6
    .line 506
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@8
    iget-object v0, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    #@a
    const/4 v1, -0x1

    #@b
    invoke-virtual {p0, v0, p1, v1, p2}, Landroid/app/FragmentManagerImpl;->popBackStackState(Landroid/os/Handler;Ljava/lang/String;II)Z

    #@e
    move-result v0

    #@f
    return v0
.end method

.method popBackStackState(Landroid/os/Handler;Ljava/lang/String;II)Z
    .registers 16
    .parameter "handler"
    .parameter "name"
    .parameter "id"
    .parameter "flags"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1475
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@4
    if-nez v6, :cond_7

    #@6
    .line 1534
    :cond_6
    :goto_6
    return v9

    #@7
    .line 1478
    :cond_7
    if-nez p2, :cond_29

    #@9
    if-gez p3, :cond_29

    #@b
    and-int/lit8 v6, p4, 0x1

    #@d
    if-nez v6, :cond_29

    #@f
    .line 1479
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v6

    #@15
    add-int/lit8 v4, v6, -0x1

    #@17
    .line 1480
    .local v4, last:I
    if-ltz v4, :cond_6

    #@19
    .line 1483
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@1b
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, Landroid/app/BackStackRecord;

    #@21
    .line 1484
    .local v1, bss:Landroid/app/BackStackRecord;
    invoke-virtual {v1, v8}, Landroid/app/BackStackRecord;->popFromBackStack(Z)V

    #@24
    .line 1485
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->reportBackStackChanged()V

    #@27
    .end local v1           #bss:Landroid/app/BackStackRecord;
    .end local v4           #last:I
    :goto_27
    move v9, v8

    #@28
    .line 1534
    goto :goto_6

    #@29
    .line 1487
    :cond_29
    const/4 v3, -0x1

    #@2a
    .line 1488
    .local v3, index:I
    if-nez p2, :cond_2e

    #@2c
    if-ltz p3, :cond_7c

    #@2e
    .line 1491
    :cond_2e
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@33
    move-result v6

    #@34
    add-int/lit8 v3, v6, -0x1

    #@36
    .line 1492
    :goto_36
    if-ltz v3, :cond_4c

    #@38
    .line 1493
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@3a
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3d
    move-result-object v1

    #@3e
    check-cast v1, Landroid/app/BackStackRecord;

    #@40
    .line 1494
    .restart local v1       #bss:Landroid/app/BackStackRecord;
    if-eqz p2, :cond_73

    #@42
    invoke-virtual {v1}, Landroid/app/BackStackRecord;->getName()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@49
    move-result v6

    #@4a
    if-eqz v6, :cond_73

    #@4c
    .line 1502
    .end local v1           #bss:Landroid/app/BackStackRecord;
    :cond_4c
    if-ltz v3, :cond_6

    #@4e
    .line 1505
    and-int/lit8 v6, p4, 0x1

    #@50
    if-eqz v6, :cond_7c

    #@52
    .line 1506
    add-int/lit8 v3, v3, -0x1

    #@54
    .line 1508
    :goto_54
    if-ltz v3, :cond_7c

    #@56
    .line 1509
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@58
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5b
    move-result-object v1

    #@5c
    check-cast v1, Landroid/app/BackStackRecord;

    #@5e
    .line 1510
    .restart local v1       #bss:Landroid/app/BackStackRecord;
    if-eqz p2, :cond_6a

    #@60
    invoke-virtual {v1}, Landroid/app/BackStackRecord;->getName()Ljava/lang/String;

    #@63
    move-result-object v6

    #@64
    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v6

    #@68
    if-nez v6, :cond_70

    #@6a
    :cond_6a
    if-ltz p3, :cond_7c

    #@6c
    iget v6, v1, Landroid/app/BackStackRecord;->mIndex:I

    #@6e
    if-ne p3, v6, :cond_7c

    #@70
    .line 1512
    :cond_70
    add-int/lit8 v3, v3, -0x1

    #@72
    .line 1513
    goto :goto_54

    #@73
    .line 1497
    :cond_73
    if-ltz p3, :cond_79

    #@75
    iget v6, v1, Landroid/app/BackStackRecord;->mIndex:I

    #@77
    if-eq p3, v6, :cond_4c

    #@79
    .line 1500
    :cond_79
    add-int/lit8 v3, v3, -0x1

    #@7b
    .line 1501
    goto :goto_36

    #@7c
    .line 1519
    .end local v1           #bss:Landroid/app/BackStackRecord;
    :cond_7c
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@7e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@81
    move-result v6

    #@82
    add-int/lit8 v6, v6, -0x1

    #@84
    if-eq v3, v6, :cond_6

    #@86
    .line 1522
    new-instance v5, Ljava/util/ArrayList;

    #@88
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@8b
    .line 1524
    .local v5, states:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/BackStackRecord;>;"
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@8d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@90
    move-result v6

    #@91
    add-int/lit8 v2, v6, -0x1

    #@93
    .local v2, i:I
    :goto_93
    if-le v2, v3, :cond_a1

    #@95
    .line 1525
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@97
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9e
    .line 1524
    add-int/lit8 v2, v2, -0x1

    #@a0
    goto :goto_93

    #@a1
    .line 1527
    :cond_a1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@a4
    move-result v6

    #@a5
    add-int/lit8 v0, v6, -0x1

    #@a7
    .line 1528
    .local v0, LAST:I
    const/4 v2, 0x0

    #@a8
    :goto_a8
    if-gt v2, v0, :cond_db

    #@aa
    .line 1529
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@ac
    if-eqz v6, :cond_ca

    #@ae
    const-string v6, "FragmentManager"

    #@b0
    new-instance v7, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v10, "Popping back stack state: "

    #@b7
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@be
    move-result-object v10

    #@bf
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v7

    #@c3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v7

    #@c7
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 1530
    :cond_ca
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@cd
    move-result-object v6

    #@ce
    check-cast v6, Landroid/app/BackStackRecord;

    #@d0
    if-ne v2, v0, :cond_d9

    #@d2
    move v7, v8

    #@d3
    :goto_d3
    invoke-virtual {v6, v7}, Landroid/app/BackStackRecord;->popFromBackStack(Z)V

    #@d6
    .line 1528
    add-int/lit8 v2, v2, 0x1

    #@d8
    goto :goto_a8

    #@d9
    :cond_d9
    move v7, v9

    #@da
    .line 1530
    goto :goto_d3

    #@db
    .line 1532
    :cond_db
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->reportBackStackChanged()V

    #@de
    goto/16 :goto_27
.end method

.method public putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V
    .registers 7
    .parameter "bundle"
    .parameter "key"
    .parameter "fragment"

    #@0
    .prologue
    .line 558
    iget v0, p3, Landroid/app/Fragment;->mIndex:I

    #@2
    if-gez v0, :cond_25

    #@4
    .line 559
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Fragment "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " is not currently in the FragmentManager"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@25
    .line 562
    :cond_25
    iget v0, p3, Landroid/app/Fragment;->mIndex:I

    #@27
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2a
    .line 563
    return-void
.end method

.method public removeFragment(Landroid/app/Fragment;II)V
    .registers 11
    .parameter "fragment"
    .parameter "transition"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1143
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4
    if-eqz v1, :cond_2b

    #@6
    const-string v1, "FragmentManager"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v3, "remove: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, " nesting="

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    iget v3, p1, Landroid/app/Fragment;->mBackStackNesting:I

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1144
    :cond_2b
    invoke-virtual {p1}, Landroid/app/Fragment;->isInBackStack()Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_5a

    #@31
    move v6, v0

    #@32
    .line 1145
    .local v6, inactive:Z
    :goto_32
    iget-boolean v1, p1, Landroid/app/Fragment;->mDetached:Z

    #@34
    if-eqz v1, :cond_38

    #@36
    if-eqz v6, :cond_59

    #@38
    .line 1154
    :cond_38
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@3a
    if-eqz v1, :cond_41

    #@3c
    .line 1155
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@3e
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@41
    .line 1157
    :cond_41
    iget-boolean v1, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@43
    if-eqz v1, :cond_4b

    #@45
    iget-boolean v1, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@47
    if-eqz v1, :cond_4b

    #@49
    .line 1158
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@4b
    .line 1160
    :cond_4b
    iput-boolean v5, p1, Landroid/app/Fragment;->mAdded:Z

    #@4d
    .line 1161
    iput-boolean v0, p1, Landroid/app/Fragment;->mRemoving:Z

    #@4f
    .line 1162
    if-eqz v6, :cond_5c

    #@51
    move v2, v5

    #@52
    :goto_52
    move-object v0, p0

    #@53
    move-object v1, p1

    #@54
    move v3, p2

    #@55
    move v4, p3

    #@56
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    #@59
    .line 1165
    :cond_59
    return-void

    #@5a
    .end local v6           #inactive:Z
    :cond_5a
    move v6, v5

    #@5b
    .line 1144
    goto :goto_32

    #@5c
    .restart local v6       #inactive:Z
    :cond_5c
    move v2, v0

    #@5d
    .line 1162
    goto :goto_52
.end method

.method public removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 551
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 552
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@9
    .line 554
    :cond_9
    return-void
.end method

.method reportBackStackChanged()V
    .registers 3

    #@0
    .prologue
    .line 1459
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@2
    if-eqz v1, :cond_1b

    #@4
    .line 1460
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v1

    #@b
    if-ge v0, v1, :cond_1b

    #@d
    .line 1461
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v1

    #@13
    check-cast v1, Landroid/app/FragmentManager$OnBackStackChangedListener;

    #@15
    invoke-interface {v1}, Landroid/app/FragmentManager$OnBackStackChangedListener;->onBackStackChanged()V

    #@18
    .line 1460
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_5

    #@1b
    .line 1464
    .end local v0           #i:I
    :cond_1b
    return-void
.end method

.method restoreAllState(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .registers 15
    .parameter "state"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, nonConfig:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    .line 1713
    if-nez p1, :cond_5

    #@4
    .line 1820
    :cond_4
    :goto_4
    return-void

    #@5
    :cond_5
    move-object v2, p1

    #@6
    .line 1714
    check-cast v2, Landroid/app/FragmentManagerState;

    #@8
    .line 1715
    .local v2, fms:Landroid/app/FragmentManagerState;
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@a
    if-eqz v7, :cond_4

    #@c
    .line 1719
    if-eqz p2, :cond_66

    #@e
    .line 1720
    const/4 v4, 0x0

    #@f
    .local v4, i:I
    :goto_f
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v7

    #@13
    if-ge v4, v7, :cond_66

    #@15
    .line 1721
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/app/Fragment;

    #@1b
    .line 1722
    .local v1, f:Landroid/app/Fragment;
    sget-boolean v7, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@1d
    if-eqz v7, :cond_38

    #@1f
    const-string v7, "FragmentManager"

    #@21
    new-instance v8, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string/jumbo v9, "restoreAllState: re-attaching retained "

    #@29
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v8

    #@35
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1723
    :cond_38
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@3a
    iget v8, v1, Landroid/app/Fragment;->mIndex:I

    #@3c
    aget-object v3, v7, v8

    #@3e
    .line 1724
    .local v3, fs:Landroid/app/FragmentState;
    iput-object v1, v3, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@40
    .line 1725
    iput-object v10, v1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@42
    .line 1726
    iput v11, v1, Landroid/app/Fragment;->mBackStackNesting:I

    #@44
    .line 1727
    iput-boolean v11, v1, Landroid/app/Fragment;->mInLayout:Z

    #@46
    .line 1728
    iput-boolean v11, v1, Landroid/app/Fragment;->mAdded:Z

    #@48
    .line 1729
    iput-object v10, v1, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@4a
    .line 1730
    iget-object v7, v3, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@4c
    if-eqz v7, :cond_63

    #@4e
    .line 1731
    iget-object v7, v3, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@50
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@52
    invoke-virtual {v8}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v7, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@59
    .line 1732
    iget-object v7, v3, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@5b
    const-string v8, "android:view_state"

    #@5d
    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    #@60
    move-result-object v7

    #@61
    iput-object v7, v1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@63
    .line 1720
    :cond_63
    add-int/lit8 v4, v4, 0x1

    #@65
    goto :goto_f

    #@66
    .line 1740
    .end local v1           #f:Landroid/app/Fragment;
    .end local v3           #fs:Landroid/app/FragmentState;
    .end local v4           #i:I
    :cond_66
    new-instance v7, Ljava/util/ArrayList;

    #@68
    iget-object v8, v2, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@6a
    array-length v8, v8

    #@6b
    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@6e
    iput-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@70
    .line 1741
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@72
    if-eqz v7, :cond_79

    #@74
    .line 1742
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@76
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    #@79
    .line 1744
    :cond_79
    const/4 v4, 0x0

    #@7a
    .restart local v4       #i:I
    :goto_7a
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@7c
    array-length v7, v7

    #@7d
    if-ge v4, v7, :cond_f5

    #@7f
    .line 1745
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@81
    aget-object v3, v7, v4

    #@83
    .line 1746
    .restart local v3       #fs:Landroid/app/FragmentState;
    if-eqz v3, :cond_be

    #@85
    .line 1747
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@87
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@89
    invoke-virtual {v3, v7, v8}, Landroid/app/FragmentState;->instantiate(Landroid/app/Activity;Landroid/app/Fragment;)Landroid/app/Fragment;

    #@8c
    move-result-object v1

    #@8d
    .line 1748
    .restart local v1       #f:Landroid/app/Fragment;
    sget-boolean v7, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@8f
    if-eqz v7, :cond_b4

    #@91
    const-string v7, "FragmentManager"

    #@93
    new-instance v8, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string/jumbo v9, "restoreAllState: active #"

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    const-string v9, ": "

    #@a5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v8

    #@a9
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 1749
    :cond_b4
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@b6
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b9
    .line 1753
    iput-object v10, v3, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    #@bb
    .line 1744
    .end local v1           #f:Landroid/app/Fragment;
    :goto_bb
    add-int/lit8 v4, v4, 0x1

    #@bd
    goto :goto_7a

    #@be
    .line 1755
    :cond_be
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@c0
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c3
    .line 1756
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@c5
    if-nez v7, :cond_ce

    #@c7
    .line 1757
    new-instance v7, Ljava/util/ArrayList;

    #@c9
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@cc
    iput-object v7, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@ce
    .line 1759
    :cond_ce
    sget-boolean v7, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@d0
    if-eqz v7, :cond_eb

    #@d2
    const-string v7, "FragmentManager"

    #@d4
    new-instance v8, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string/jumbo v9, "restoreAllState: avail #"

    #@dc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v8

    #@e0
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v8

    #@e4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v8

    #@e8
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 1760
    :cond_eb
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAvailIndices:Ljava/util/ArrayList;

    #@ed
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f0
    move-result-object v8

    #@f1
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f4
    goto :goto_bb

    #@f5
    .line 1765
    .end local v3           #fs:Landroid/app/FragmentState;
    :cond_f5
    if-eqz p2, :cond_148

    #@f7
    .line 1766
    const/4 v4, 0x0

    #@f8
    :goto_f8
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@fb
    move-result v7

    #@fc
    if-ge v4, v7, :cond_148

    #@fe
    .line 1767
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@101
    move-result-object v1

    #@102
    check-cast v1, Landroid/app/Fragment;

    #@104
    .line 1768
    .restart local v1       #f:Landroid/app/Fragment;
    iget v7, v1, Landroid/app/Fragment;->mTargetIndex:I

    #@106
    if-ltz v7, :cond_11e

    #@108
    .line 1769
    iget v7, v1, Landroid/app/Fragment;->mTargetIndex:I

    #@10a
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@10c
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@10f
    move-result v8

    #@110
    if-ge v7, v8, :cond_121

    #@112
    .line 1770
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@114
    iget v8, v1, Landroid/app/Fragment;->mTargetIndex:I

    #@116
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@119
    move-result-object v7

    #@11a
    check-cast v7, Landroid/app/Fragment;

    #@11c
    iput-object v7, v1, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@11e
    .line 1766
    :cond_11e
    :goto_11e
    add-int/lit8 v4, v4, 0x1

    #@120
    goto :goto_f8

    #@121
    .line 1772
    :cond_121
    const-string v7, "FragmentManager"

    #@123
    new-instance v8, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v9, "Re-attaching retained fragment "

    #@12a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v8

    #@12e
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v8

    #@132
    const-string v9, " target no longer exists: "

    #@134
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v8

    #@138
    iget v9, v1, Landroid/app/Fragment;->mTargetIndex:I

    #@13a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v8

    #@13e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v8

    #@142
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@145
    .line 1774
    iput-object v10, v1, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@147
    goto :goto_11e

    #@148
    .line 1781
    .end local v1           #f:Landroid/app/Fragment;
    :cond_148
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mAdded:[I

    #@14a
    if-eqz v7, :cond_1cb

    #@14c
    .line 1782
    new-instance v7, Ljava/util/ArrayList;

    #@14e
    iget-object v8, v2, Landroid/app/FragmentManagerState;->mAdded:[I

    #@150
    array-length v8, v8

    #@151
    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@154
    iput-object v7, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@156
    .line 1783
    const/4 v4, 0x0

    #@157
    :goto_157
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mAdded:[I

    #@159
    array-length v7, v7

    #@15a
    if-ge v4, v7, :cond_1cd

    #@15c
    .line 1784
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@15e
    iget-object v8, v2, Landroid/app/FragmentManagerState;->mAdded:[I

    #@160
    aget v8, v8, v4

    #@162
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@165
    move-result-object v1

    #@166
    check-cast v1, Landroid/app/Fragment;

    #@168
    .line 1785
    .restart local v1       #f:Landroid/app/Fragment;
    if-nez v1, :cond_189

    #@16a
    .line 1786
    new-instance v7, Ljava/lang/IllegalStateException;

    #@16c
    new-instance v8, Ljava/lang/StringBuilder;

    #@16e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@171
    const-string v9, "No instantiated fragment for index #"

    #@173
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@176
    move-result-object v8

    #@177
    iget-object v9, v2, Landroid/app/FragmentManagerState;->mAdded:[I

    #@179
    aget v9, v9, v4

    #@17b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v8

    #@17f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@182
    move-result-object v8

    #@183
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@186
    invoke-direct {p0, v7}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@189
    .line 1789
    :cond_189
    const/4 v7, 0x1

    #@18a
    iput-boolean v7, v1, Landroid/app/Fragment;->mAdded:Z

    #@18c
    .line 1790
    sget-boolean v7, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@18e
    if-eqz v7, :cond_1b3

    #@190
    const-string v7, "FragmentManager"

    #@192
    new-instance v8, Ljava/lang/StringBuilder;

    #@194
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@197
    const-string/jumbo v9, "restoreAllState: added #"

    #@19a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19d
    move-result-object v8

    #@19e
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v8

    #@1a2
    const-string v9, ": "

    #@1a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v8

    #@1a8
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v8

    #@1ac
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1af
    move-result-object v8

    #@1b0
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b3
    .line 1791
    :cond_1b3
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@1b5
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1b8
    move-result v7

    #@1b9
    if-eqz v7, :cond_1c3

    #@1bb
    .line 1792
    new-instance v7, Ljava/lang/IllegalStateException;

    #@1bd
    const-string v8, "Already added!"

    #@1bf
    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1c2
    throw v7

    #@1c3
    .line 1794
    :cond_1c3
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@1c5
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c8
    .line 1783
    add-int/lit8 v4, v4, 0x1

    #@1ca
    goto :goto_157

    #@1cb
    .line 1797
    .end local v1           #f:Landroid/app/Fragment;
    :cond_1cb
    iput-object v10, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@1cd
    .line 1801
    :cond_1cd
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    #@1cf
    if-eqz v7, :cond_23f

    #@1d1
    .line 1802
    new-instance v7, Ljava/util/ArrayList;

    #@1d3
    iget-object v8, v2, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    #@1d5
    array-length v8, v8

    #@1d6
    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@1d9
    iput-object v7, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@1db
    .line 1803
    const/4 v4, 0x0

    #@1dc
    :goto_1dc
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    #@1de
    array-length v7, v7

    #@1df
    if-ge v4, v7, :cond_4

    #@1e1
    .line 1804
    iget-object v7, v2, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    #@1e3
    aget-object v7, v7, v4

    #@1e5
    invoke-virtual {v7, p0}, Landroid/app/BackStackState;->instantiate(Landroid/app/FragmentManagerImpl;)Landroid/app/BackStackRecord;

    #@1e8
    move-result-object v0

    #@1e9
    .line 1805
    .local v0, bse:Landroid/app/BackStackRecord;
    sget-boolean v7, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@1eb
    if-eqz v7, :cond_22e

    #@1ed
    .line 1806
    const-string v7, "FragmentManager"

    #@1ef
    new-instance v8, Ljava/lang/StringBuilder;

    #@1f1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f4
    const-string/jumbo v9, "restoreAllState: back stack #"

    #@1f7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fa
    move-result-object v8

    #@1fb
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v8

    #@1ff
    const-string v9, " (index "

    #@201
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@204
    move-result-object v8

    #@205
    iget v9, v0, Landroid/app/BackStackRecord;->mIndex:I

    #@207
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20a
    move-result-object v8

    #@20b
    const-string v9, "): "

    #@20d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v8

    #@211
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@214
    move-result-object v8

    #@215
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@218
    move-result-object v8

    #@219
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21c
    .line 1808
    new-instance v5, Landroid/util/LogWriter;

    #@21e
    const/4 v7, 0x2

    #@21f
    const-string v8, "FragmentManager"

    #@221
    invoke-direct {v5, v7, v8}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    #@224
    .line 1809
    .local v5, logw:Landroid/util/LogWriter;
    new-instance v6, Ljava/io/PrintWriter;

    #@226
    invoke-direct {v6, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@229
    .line 1810
    .local v6, pw:Ljava/io/PrintWriter;
    const-string v7, "  "

    #@22b
    invoke-virtual {v0, v7, v6, v11}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    #@22e
    .line 1812
    .end local v5           #logw:Landroid/util/LogWriter;
    .end local v6           #pw:Ljava/io/PrintWriter;
    :cond_22e
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@230
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@233
    .line 1813
    iget v7, v0, Landroid/app/BackStackRecord;->mIndex:I

    #@235
    if-ltz v7, :cond_23c

    #@237
    .line 1814
    iget v7, v0, Landroid/app/BackStackRecord;->mIndex:I

    #@239
    invoke-virtual {p0, v7, v0}, Landroid/app/FragmentManagerImpl;->setBackStackIndex(ILandroid/app/BackStackRecord;)V

    #@23c
    .line 1803
    :cond_23c
    add-int/lit8 v4, v4, 0x1

    #@23e
    goto :goto_1dc

    #@23f
    .line 1818
    .end local v0           #bse:Landroid/app/BackStackRecord;
    :cond_23f
    iput-object v10, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@241
    goto/16 :goto_4
.end method

.method retainNonConfig()Ljava/util/ArrayList;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1538
    const/4 v1, 0x0

    #@1
    .line 1539
    .local v1, fragments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@3
    if-eqz v3, :cond_55

    #@5
    .line 1540
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v2, v3, :cond_55

    #@e
    .line 1541
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/app/Fragment;

    #@16
    .line 1542
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_50

    #@18
    iget-boolean v3, v0, Landroid/app/Fragment;->mRetainInstance:Z

    #@1a
    if-eqz v3, :cond_50

    #@1c
    .line 1543
    if-nez v1, :cond_23

    #@1e
    .line 1544
    new-instance v1, Ljava/util/ArrayList;

    #@20
    .end local v1           #fragments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@23
    .line 1546
    .restart local v1       #fragments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/Fragment;>;"
    :cond_23
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26
    .line 1547
    const/4 v3, 0x1

    #@27
    iput-boolean v3, v0, Landroid/app/Fragment;->mRetaining:Z

    #@29
    .line 1548
    iget-object v3, v0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@2b
    if-eqz v3, :cond_53

    #@2d
    iget-object v3, v0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@2f
    iget v3, v3, Landroid/app/Fragment;->mIndex:I

    #@31
    :goto_31
    iput v3, v0, Landroid/app/Fragment;->mTargetIndex:I

    #@33
    .line 1549
    sget-boolean v3, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@35
    if-eqz v3, :cond_50

    #@37
    const-string v3, "FragmentManager"

    #@39
    new-instance v4, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string/jumbo v5, "retainNonConfig: keeping retained "

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1540
    :cond_50
    add-int/lit8 v2, v2, 0x1

    #@52
    goto :goto_6

    #@53
    .line 1548
    :cond_53
    const/4 v3, -0x1

    #@54
    goto :goto_31

    #@55
    .line 1553
    .end local v0           #f:Landroid/app/Fragment;
    .end local v2           #i:I
    :cond_55
    return-object v1
.end method

.method saveAllState()Landroid/os/Parcelable;
    .registers 13

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1608
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@4
    .line 1610
    const/4 v9, 0x1

    #@5
    iput-boolean v9, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    #@7
    .line 1612
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@9
    if-eqz v9, :cond_13

    #@b
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@10
    move-result v9

    #@11
    if-gtz v9, :cond_14

    #@13
    .line 1707
    :cond_13
    :goto_13
    return-object v5

    #@14
    .line 1617
    :cond_14
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v0

    #@1a
    .line 1618
    .local v0, N:I
    new-array v1, v0, [Landroid/app/FragmentState;

    #@1c
    .line 1619
    .local v1, active:[Landroid/app/FragmentState;
    const/4 v7, 0x0

    #@1d
    .line 1620
    .local v7, haveFragments:Z
    const/4 v8, 0x0

    #@1e
    .local v8, i:I
    :goto_1e
    if-ge v8, v0, :cond_ee

    #@20
    .line 1621
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v4

    #@26
    check-cast v4, Landroid/app/Fragment;

    #@28
    .line 1622
    .local v4, f:Landroid/app/Fragment;
    if-eqz v4, :cond_e5

    #@2a
    .line 1623
    iget v9, v4, Landroid/app/Fragment;->mIndex:I

    #@2c
    if-gez v9, :cond_55

    #@2e
    .line 1624
    new-instance v9, Ljava/lang/IllegalStateException;

    #@30
    new-instance v10, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v11, "Failure saving state: active "

    #@37
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v10

    #@3f
    const-string v11, " has cleared index: "

    #@41
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v10

    #@45
    iget v11, v4, Landroid/app/Fragment;->mIndex:I

    #@47
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v10

    #@4b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v10

    #@4f
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@52
    invoke-direct {p0, v9}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@55
    .line 1629
    :cond_55
    const/4 v7, 0x1

    #@56
    .line 1631
    new-instance v6, Landroid/app/FragmentState;

    #@58
    invoke-direct {v6, v4}, Landroid/app/FragmentState;-><init>(Landroid/app/Fragment;)V

    #@5b
    .line 1632
    .local v6, fs:Landroid/app/FragmentState;
    aput-object v6, v1, v8

    #@5d
    .line 1634
    iget v9, v4, Landroid/app/Fragment;->mState:I

    #@5f
    if-lez v9, :cond_e9

    #@61
    iget-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@63
    if-nez v9, :cond_e9

    #@65
    .line 1635
    invoke-virtual {p0, v4}, Landroid/app/FragmentManagerImpl;->saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;

    #@68
    move-result-object v9

    #@69
    iput-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@6b
    .line 1637
    iget-object v9, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@6d
    if-eqz v9, :cond_bd

    #@6f
    .line 1638
    iget-object v9, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@71
    iget v9, v9, Landroid/app/Fragment;->mIndex:I

    #@73
    if-gez v9, :cond_9c

    #@75
    .line 1639
    new-instance v9, Ljava/lang/IllegalStateException;

    #@77
    new-instance v10, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v11, "Failure saving state: "

    #@7e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v10

    #@82
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v10

    #@86
    const-string v11, " has target not in fragment manager: "

    #@88
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v10

    #@8c
    iget-object v11, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@8e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v10

    #@92
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@99
    invoke-direct {p0, v9}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@9c
    .line 1643
    :cond_9c
    iget-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@9e
    if-nez v9, :cond_a7

    #@a0
    .line 1644
    new-instance v9, Landroid/os/Bundle;

    #@a2
    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    #@a5
    iput-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@a7
    .line 1646
    :cond_a7
    iget-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@a9
    const-string v10, "android:target_state"

    #@ab
    iget-object v11, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@ad
    invoke-virtual {p0, v9, v10, v11}, Landroid/app/FragmentManagerImpl;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    #@b0
    .line 1648
    iget v9, v4, Landroid/app/Fragment;->mTargetRequestCode:I

    #@b2
    if-eqz v9, :cond_bd

    #@b4
    .line 1649
    iget-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@b6
    const-string v10, "android:target_req_state"

    #@b8
    iget v11, v4, Landroid/app/Fragment;->mTargetRequestCode:I

    #@ba
    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@bd
    .line 1659
    :cond_bd
    :goto_bd
    sget-boolean v9, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@bf
    if-eqz v9, :cond_e5

    #@c1
    const-string v9, "FragmentManager"

    #@c3
    new-instance v10, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v11, "Saved state of "

    #@ca
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v10

    #@ce
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v10

    #@d2
    const-string v11, ": "

    #@d4
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v10

    #@d8
    iget-object v11, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@da
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v10

    #@de
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v10

    #@e2
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 1620
    .end local v6           #fs:Landroid/app/FragmentState;
    :cond_e5
    add-int/lit8 v8, v8, 0x1

    #@e7
    goto/16 :goto_1e

    #@e9
    .line 1656
    .restart local v6       #fs:Landroid/app/FragmentState;
    :cond_e9
    iget-object v9, v4, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@eb
    iput-object v9, v6, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    #@ed
    goto :goto_bd

    #@ee
    .line 1664
    .end local v4           #f:Landroid/app/Fragment;
    .end local v6           #fs:Landroid/app/FragmentState;
    :cond_ee
    if-nez v7, :cond_fe

    #@f0
    .line 1665
    sget-boolean v9, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@f2
    if-eqz v9, :cond_13

    #@f4
    const-string v9, "FragmentManager"

    #@f6
    const-string/jumbo v10, "saveAllState: no fragments!"

    #@f9
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@fc
    goto/16 :goto_13

    #@fe
    .line 1669
    :cond_fe
    const/4 v2, 0x0

    #@ff
    .line 1670
    .local v2, added:[I
    const/4 v3, 0x0

    #@100
    .line 1673
    .local v3, backStack:[Landroid/app/BackStackState;
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@102
    if-eqz v9, :cond_17e

    #@104
    .line 1674
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@106
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@109
    move-result v0

    #@10a
    .line 1675
    if-lez v0, :cond_17e

    #@10c
    .line 1676
    new-array v2, v0, [I

    #@10e
    .line 1677
    const/4 v8, 0x0

    #@10f
    :goto_10f
    if-ge v8, v0, :cond_17e

    #@111
    .line 1678
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@113
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@116
    move-result-object v9

    #@117
    check-cast v9, Landroid/app/Fragment;

    #@119
    iget v9, v9, Landroid/app/Fragment;->mIndex:I

    #@11b
    aput v9, v2, v8

    #@11d
    .line 1679
    aget v9, v2, v8

    #@11f
    if-gez v9, :cond_14e

    #@121
    .line 1680
    new-instance v9, Ljava/lang/IllegalStateException;

    #@123
    new-instance v10, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v11, "Failure saving state: active "

    #@12a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v10

    #@12e
    iget-object v11, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@130
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@133
    move-result-object v11

    #@134
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v10

    #@138
    const-string v11, " has cleared index: "

    #@13a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v10

    #@13e
    aget v11, v2, v8

    #@140
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@143
    move-result-object v10

    #@144
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@147
    move-result-object v10

    #@148
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@14b
    invoke-direct {p0, v9}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@14e
    .line 1684
    :cond_14e
    sget-boolean v9, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@150
    if-eqz v9, :cond_17b

    #@152
    const-string v9, "FragmentManager"

    #@154
    new-instance v10, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    const-string/jumbo v11, "saveAllState: adding fragment #"

    #@15c
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v10

    #@160
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@163
    move-result-object v10

    #@164
    const-string v11, ": "

    #@166
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v10

    #@16a
    iget-object v11, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    #@16c
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16f
    move-result-object v11

    #@170
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@173
    move-result-object v10

    #@174
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@177
    move-result-object v10

    #@178
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 1677
    :cond_17b
    add-int/lit8 v8, v8, 0x1

    #@17d
    goto :goto_10f

    #@17e
    .line 1691
    :cond_17e
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@180
    if-eqz v9, :cond_1ce

    #@182
    .line 1692
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@184
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@187
    move-result v0

    #@188
    .line 1693
    if-lez v0, :cond_1ce

    #@18a
    .line 1694
    new-array v3, v0, [Landroid/app/BackStackState;

    #@18c
    .line 1695
    const/4 v8, 0x0

    #@18d
    :goto_18d
    if-ge v8, v0, :cond_1ce

    #@18f
    .line 1696
    new-instance v10, Landroid/app/BackStackState;

    #@191
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@193
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@196
    move-result-object v9

    #@197
    check-cast v9, Landroid/app/BackStackRecord;

    #@199
    invoke-direct {v10, p0, v9}, Landroid/app/BackStackState;-><init>(Landroid/app/FragmentManagerImpl;Landroid/app/BackStackRecord;)V

    #@19c
    aput-object v10, v3, v8

    #@19e
    .line 1697
    sget-boolean v9, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@1a0
    if-eqz v9, :cond_1cb

    #@1a2
    const-string v9, "FragmentManager"

    #@1a4
    new-instance v10, Ljava/lang/StringBuilder;

    #@1a6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9
    const-string/jumbo v11, "saveAllState: adding back stack #"

    #@1ac
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1af
    move-result-object v10

    #@1b0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v10

    #@1b4
    const-string v11, ": "

    #@1b6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v10

    #@1ba
    iget-object v11, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    #@1bc
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1bf
    move-result-object v11

    #@1c0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c3
    move-result-object v10

    #@1c4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c7
    move-result-object v10

    #@1c8
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 1695
    :cond_1cb
    add-int/lit8 v8, v8, 0x1

    #@1cd
    goto :goto_18d

    #@1ce
    .line 1703
    :cond_1ce
    new-instance v5, Landroid/app/FragmentManagerState;

    #@1d0
    invoke-direct {v5}, Landroid/app/FragmentManagerState;-><init>()V

    #@1d3
    .line 1704
    .local v5, fms:Landroid/app/FragmentManagerState;
    iput-object v1, v5, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    #@1d5
    .line 1705
    iput-object v2, v5, Landroid/app/FragmentManagerState;->mAdded:[I

    #@1d7
    .line 1706
    iput-object v3, v5, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    #@1d9
    goto/16 :goto_13
.end method

.method saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;
    .registers 5
    .parameter "f"

    #@0
    .prologue
    .line 1573
    const/4 v0, 0x0

    #@1
    .line 1575
    .local v0, result:Landroid/os/Bundle;
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@3
    if-nez v1, :cond_c

    #@5
    .line 1576
    new-instance v1, Landroid/os/Bundle;

    #@7
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@a
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@c
    .line 1578
    :cond_c
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@e
    invoke-virtual {p1, v1}, Landroid/app/Fragment;->performSaveInstanceState(Landroid/os/Bundle;)V

    #@11
    .line 1579
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@13
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1e

    #@19
    .line 1580
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@1b
    .line 1581
    const/4 v1, 0x0

    #@1c
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    #@1e
    .line 1584
    :cond_1e
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@20
    if-eqz v1, :cond_25

    #@22
    .line 1585
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->saveFragmentViewState(Landroid/app/Fragment;)V

    #@25
    .line 1587
    :cond_25
    iget-object v1, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@27
    if-eqz v1, :cond_37

    #@29
    .line 1588
    if-nez v0, :cond_30

    #@2b
    .line 1589
    new-instance v0, Landroid/os/Bundle;

    #@2d
    .end local v0           #result:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@30
    .line 1591
    .restart local v0       #result:Landroid/os/Bundle;
    :cond_30
    const-string v1, "android:view_state"

    #@32
    iget-object v2, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@34
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    #@37
    .line 1594
    :cond_37
    iget-boolean v1, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@39
    if-nez v1, :cond_49

    #@3b
    .line 1595
    if-nez v0, :cond_42

    #@3d
    .line 1596
    new-instance v0, Landroid/os/Bundle;

    #@3f
    .end local v0           #result:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@42
    .line 1599
    .restart local v0       #result:Landroid/os/Bundle;
    :cond_42
    const-string v1, "android:user_visible_hint"

    #@44
    iget-boolean v2, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@46
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@49
    .line 1602
    :cond_49
    return-object v0
.end method

.method public saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;
    .registers 7
    .parameter "fragment"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 585
    iget v2, p1, Landroid/app/Fragment;->mIndex:I

    #@3
    if-gez v2, :cond_26

    #@5
    .line 586
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "Fragment "

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, " is not currently in the FragmentManager"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    invoke-direct {p0, v2}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    #@26
    .line 589
    :cond_26
    iget v2, p1, Landroid/app/Fragment;->mState:I

    #@28
    if-lez v2, :cond_35

    #@2a
    .line 590
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;

    #@2d
    move-result-object v0

    #@2e
    .line 591
    .local v0, result:Landroid/os/Bundle;
    if-eqz v0, :cond_35

    #@30
    new-instance v1, Landroid/app/Fragment$SavedState;

    #@32
    invoke-direct {v1, v0}, Landroid/app/Fragment$SavedState;-><init>(Landroid/os/Bundle;)V

    #@35
    .line 593
    .end local v0           #result:Landroid/os/Bundle;
    :cond_35
    return-object v1
.end method

.method saveFragmentViewState(Landroid/app/Fragment;)V
    .registers 4
    .parameter "f"

    #@0
    .prologue
    .line 1557
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 1570
    :cond_4
    :goto_4
    return-void

    #@5
    .line 1560
    :cond_5
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@7
    if-nez v0, :cond_27

    #@9
    .line 1561
    new-instance v0, Landroid/util/SparseArray;

    #@b
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@e
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@10
    .line 1565
    :goto_10
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@12
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@14
    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    #@17
    .line 1566
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@19
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    #@1c
    move-result v0

    #@1d
    if-lez v0, :cond_4

    #@1f
    .line 1567
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@21
    iput-object v0, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@23
    .line 1568
    const/4 v0, 0x0

    #@24
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@26
    goto :goto_4

    #@27
    .line 1563
    :cond_27
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    #@29
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    #@2c
    goto :goto_10
.end method

.method public setBackStackIndex(ILandroid/app/BackStackRecord;)V
    .registers 7
    .parameter "index"
    .parameter "bse"

    #@0
    .prologue
    .line 1367
    monitor-enter p0

    #@1
    .line 1368
    :try_start_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@3
    if-nez v1, :cond_c

    #@5
    .line 1369
    new-instance v1, Ljava/util/ArrayList;

    #@7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@c
    .line 1371
    :cond_c
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v0

    #@12
    .line 1372
    .local v0, N:I
    if-ge p1, v0, :cond_41

    #@14
    .line 1373
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@16
    if-eqz v1, :cond_3a

    #@18
    const-string v1, "FragmentManager"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "Setting back stack index "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " to "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1374
    :cond_3a
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v1, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 1388
    :goto_3f
    monitor-exit p0

    #@40
    .line 1389
    return-void

    #@41
    .line 1376
    :cond_41
    :goto_41
    if-ge v0, p1, :cond_7c

    #@43
    .line 1377
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@45
    const/4 v2, 0x0

    #@46
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@49
    .line 1378
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@4b
    if-nez v1, :cond_54

    #@4d
    .line 1379
    new-instance v1, Ljava/util/ArrayList;

    #@4f
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@52
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@54
    .line 1381
    :cond_54
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@56
    if-eqz v1, :cond_70

    #@58
    const-string v1, "FragmentManager"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "Adding available back stack index "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 1382
    :cond_70
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    #@72
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@79
    .line 1383
    add-int/lit8 v0, v0, 0x1

    #@7b
    goto :goto_41

    #@7c
    .line 1385
    :cond_7c
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@7e
    if-eqz v1, :cond_a2

    #@80
    const-string v1, "FragmentManager"

    #@82
    new-instance v2, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v3, "Adding back stack index "

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    const-string v3, " with "

    #@93
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v2

    #@97
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v2

    #@9b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 1386
    :cond_a2
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a7
    goto :goto_3f

    #@a8
    .line 1388
    .end local v0           #N:I
    :catchall_a8
    move-exception v1

    #@a9
    monitor-exit p0
    :try_end_aa
    .catchall {:try_start_1 .. :try_end_aa} :catchall_a8

    #@aa
    throw v1
.end method

.method public showFragment(Landroid/app/Fragment;II)V
    .registers 10
    .parameter "fragment"
    .parameter "transition"
    .parameter "transitionStyle"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1200
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    #@4
    if-eqz v1, :cond_1f

    #@6
    const-string v1, "FragmentManager"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v3, "show: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1201
    :cond_1f
    iget-boolean v1, p1, Landroid/app/Fragment;->mHidden:Z

    #@21
    if-eqz v1, :cond_4d

    #@23
    .line 1202
    iput-boolean v4, p1, Landroid/app/Fragment;->mHidden:Z

    #@25
    .line 1203
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@27
    if-eqz v1, :cond_3c

    #@29
    .line 1204
    invoke-virtual {p0, p1, p2, v5, p3}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    #@2c
    move-result-object v0

    #@2d
    .line 1206
    .local v0, anim:Landroid/animation/Animator;
    if-eqz v0, :cond_37

    #@2f
    .line 1207
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@31
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    #@34
    .line 1208
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    #@37
    .line 1210
    :cond_37
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@39
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    #@3c
    .line 1212
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_3c
    iget-boolean v1, p1, Landroid/app/Fragment;->mAdded:Z

    #@3e
    if-eqz v1, :cond_4a

    #@40
    iget-boolean v1, p1, Landroid/app/Fragment;->mHasMenu:Z

    #@42
    if-eqz v1, :cond_4a

    #@44
    iget-boolean v1, p1, Landroid/app/Fragment;->mMenuVisible:Z

    #@46
    if-eqz v1, :cond_4a

    #@48
    .line 1213
    iput-boolean v5, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    #@4a
    .line 1215
    :cond_4a
    invoke-virtual {p1, v4}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    #@4d
    .line 1217
    :cond_4d
    return-void
.end method

.method startPendingDeferredFragments()V
    .registers 4

    #@0
    .prologue
    .line 1076
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@2
    if-nez v2, :cond_5

    #@4
    .line 1084
    :cond_4
    return-void

    #@5
    .line 1078
    :cond_5
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v2

    #@c
    if-ge v1, v2, :cond_4

    #@e
    .line 1079
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/app/Fragment;

    #@16
    .line 1080
    .local v0, f:Landroid/app/Fragment;
    if-eqz v0, :cond_1b

    #@18
    .line 1081
    invoke-virtual {p0, v0}, Landroid/app/FragmentManagerImpl;->performPendingDeferredStart(Landroid/app/Fragment;)V

    #@1b
    .line 1078
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 604
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "FragmentManager{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 605
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v1

    #@10
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 606
    const-string v1, " in "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 607
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@1e
    if-eqz v1, :cond_30

    #@20
    .line 608
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    #@22
    invoke-static {v1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@25
    .line 612
    :goto_25
    const-string/jumbo v1, "}}"

    #@28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 613
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    return-object v1

    #@30
    .line 610
    :cond_30
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActivity:Landroid/app/Activity;

    #@32
    invoke-static {v1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@35
    goto :goto_25
.end method
