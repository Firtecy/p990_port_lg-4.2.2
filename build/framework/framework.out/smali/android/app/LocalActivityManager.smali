.class public Landroid/app/LocalActivityManager;
.super Ljava/lang/Object;
.source "LocalActivityManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/LocalActivityManager$LocalActivityRecord;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final CREATED:I = 0x2

.field static final DESTROYED:I = 0x5

.field static final INITIALIZING:I = 0x1

.field static final RESTORED:I = 0x0

.field static final RESUMED:I = 0x4

.field static final STARTED:I = 0x3

.field private static final TAG:Ljava/lang/String; = "LocalActivityManager"

.field private static final localLOGV:Z


# instance fields
.field private final mActivities:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/app/LocalActivityManager$LocalActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/LocalActivityManager$LocalActivityRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mActivityThread:Landroid/app/ActivityThread;

.field private mCurState:I

.field private mFinishing:Z

.field private final mParent:Landroid/app/Activity;

.field private mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

.field private mSingleMode:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .registers 4
    .parameter "parent"
    .parameter "singleMode"

    #@0
    .prologue
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@a
    .line 80
    new-instance v0, Ljava/util/ArrayList;

    #@c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@11
    .line 90
    const/4 v0, 0x1

    #@12
    iput v0, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@14
    .line 105
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@1a
    .line 106
    iput-object p1, p0, Landroid/app/LocalActivityManager;->mParent:Landroid/app/Activity;

    #@1c
    .line 107
    iput-boolean p2, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@1e
    .line 108
    return-void
.end method

.method private moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V
    .registers 16
    .parameter "r"
    .parameter "desiredState"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x2

    #@2
    const/4 v12, 0x1

    #@3
    const/4 v11, 0x3

    #@4
    const/4 v10, 0x4

    #@5
    .line 111
    iget v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@7
    if-eqz v0, :cond_e

    #@9
    iget v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@b
    const/4 v1, 0x5

    #@c
    if-ne v0, v1, :cond_f

    #@e
    .line 204
    :cond_e
    :goto_e
    return-void

    #@f
    .line 116
    :cond_f
    iget v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@11
    if-ne v0, v12, :cond_68

    #@13
    .line 118
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mParent:Landroid/app/Activity;

    #@15
    invoke-virtual {v0}, Landroid/app/Activity;->getLastNonConfigurationChildInstances()Ljava/util/HashMap;

    #@18
    move-result-object v9

    #@19
    .line 120
    .local v9, lastNonConfigurationInstances:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v8, 0x0

    #@1a
    .line 121
    .local v8, instanceObj:Ljava/lang/Object;
    if-eqz v9, :cond_22

    #@1c
    .line 122
    iget-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->id:Ljava/lang/String;

    #@1e
    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v8

    #@22
    .line 124
    .end local v8           #instanceObj:Ljava/lang/Object;
    :cond_22
    const/4 v7, 0x0

    #@23
    .line 125
    .local v7, instance:Landroid/app/Activity$NonConfigurationInstances;
    if-eqz v8, :cond_2c

    #@25
    .line 126
    new-instance v7, Landroid/app/Activity$NonConfigurationInstances;

    #@27
    .end local v7           #instance:Landroid/app/Activity$NonConfigurationInstances;
    invoke-direct {v7}, Landroid/app/Activity$NonConfigurationInstances;-><init>()V

    #@2a
    .line 127
    .restart local v7       #instance:Landroid/app/Activity$NonConfigurationInstances;
    iput-object v8, v7, Landroid/app/Activity$NonConfigurationInstances;->activity:Ljava/lang/Object;

    #@2c
    .line 132
    :cond_2c
    iget-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2e
    if-nez v0, :cond_3a

    #@30
    .line 133
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@32
    iget-object v1, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@34
    invoke-virtual {v0, v1}, Landroid/app/ActivityThread;->resolveActivityInfo(Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;

    #@37
    move-result-object v0

    #@38
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@3a
    .line 135
    :cond_3a
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@3c
    iget-object v1, p0, Landroid/app/LocalActivityManager;->mParent:Landroid/app/Activity;

    #@3e
    iget-object v2, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->id:Ljava/lang/String;

    #@40
    iget-object v3, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@42
    iget-object v4, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@44
    iget-object v6, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@46
    move-object v5, p1

    #@47
    invoke-virtual/range {v0 .. v7}, Landroid/app/ActivityThread;->startActivityNow(Landroid/app/Activity;Ljava/lang/String;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Landroid/os/Bundle;Landroid/app/Activity$NonConfigurationInstances;)Landroid/app/Activity;

    #@4a
    move-result-object v0

    #@4b
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@4d
    .line 137
    iget-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@4f
    if-eqz v0, :cond_e

    #@51
    .line 140
    iget-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@53
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    #@56
    move-result-object v0

    #@57
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@59
    .line 141
    const/4 v0, 0x0

    #@5a
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@5c
    .line 142
    iput v11, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@5e
    .line 144
    if-ne p2, v10, :cond_e

    #@60
    .line 146
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@62
    invoke-virtual {v0, p1, v12}, Landroid/app/ActivityThread;->performResumeActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@65
    .line 147
    iput v10, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@67
    goto :goto_e

    #@68
    .line 159
    .end local v7           #instance:Landroid/app/Activity$NonConfigurationInstances;
    .end local v9           #lastNonConfigurationInstances:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_68
    iget v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@6a
    packed-switch v0, :pswitch_data_b6

    #@6d
    goto :goto_e

    #@6e
    .line 161
    :pswitch_6e
    if-ne p2, v11, :cond_77

    #@70
    .line 163
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@72
    invoke-virtual {v0, p1}, Landroid/app/ActivityThread;->performRestartActivity(Landroid/os/IBinder;)V

    #@75
    .line 164
    iput v11, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@77
    .line 166
    :cond_77
    if-ne p2, v10, :cond_e

    #@79
    .line 168
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@7b
    invoke-virtual {v0, p1}, Landroid/app/ActivityThread;->performRestartActivity(Landroid/os/IBinder;)V

    #@7e
    .line 169
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@80
    invoke-virtual {v0, p1, v12}, Landroid/app/ActivityThread;->performResumeActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@83
    .line 170
    iput v10, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@85
    goto :goto_e

    #@86
    .line 175
    :pswitch_86
    if-ne p2, v10, :cond_92

    #@88
    .line 178
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@8a
    invoke-virtual {v0, p1, v12}, Landroid/app/ActivityThread;->performResumeActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@8d
    .line 179
    const/4 v0, 0x0

    #@8e
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@90
    .line 180
    iput v10, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@92
    .line 182
    :cond_92
    if-ne p2, v2, :cond_e

    #@94
    .line 184
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@96
    invoke-virtual {v0, p1, v3}, Landroid/app/ActivityThread;->performStopActivity(Landroid/os/IBinder;Z)V

    #@99
    .line 185
    iput v2, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@9b
    goto/16 :goto_e

    #@9d
    .line 190
    :pswitch_9d
    if-ne p2, v11, :cond_a6

    #@9f
    .line 192
    iget-boolean v0, p0, Landroid/app/LocalActivityManager;->mFinishing:Z

    #@a1
    invoke-direct {p0, p1, v0}, Landroid/app/LocalActivityManager;->performPause(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)V

    #@a4
    .line 193
    iput v11, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@a6
    .line 195
    :cond_a6
    if-ne p2, v2, :cond_e

    #@a8
    .line 197
    iget-boolean v0, p0, Landroid/app/LocalActivityManager;->mFinishing:Z

    #@aa
    invoke-direct {p0, p1, v0}, Landroid/app/LocalActivityManager;->performPause(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)V

    #@ad
    .line 199
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@af
    invoke-virtual {v0, p1, v3}, Landroid/app/ActivityThread;->performStopActivity(Landroid/os/IBinder;Z)V

    #@b2
    .line 200
    iput v2, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@b4
    goto/16 :goto_e

    #@b6
    .line 159
    :pswitch_data_b6
    .packed-switch 0x2
        :pswitch_6e
        :pswitch_86
        :pswitch_9d
    .end packed-switch
.end method

.method private performDestroy(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)Landroid/view/Window;
    .registers 7
    .parameter "r"
    .parameter "finish"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 358
    iget-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@3
    .line 359
    .local v0, win:Landroid/view/Window;
    iget v1, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@5
    const/4 v2, 0x4

    #@6
    if-ne v1, v2, :cond_d

    #@8
    if-nez p2, :cond_d

    #@a
    .line 360
    invoke-direct {p0, p1, p2}, Landroid/app/LocalActivityManager;->performPause(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)V

    #@d
    .line 363
    :cond_d
    iget-object v1, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@f
    invoke-virtual {v1, p1, p2}, Landroid/app/ActivityThread;->performDestroyActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@12
    .line 364
    iput-object v3, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@14
    .line 365
    iput-object v3, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@16
    .line 366
    if-eqz p2, :cond_1a

    #@18
    .line 367
    iput-object v3, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@1a
    .line 369
    :cond_1a
    const/4 v1, 0x5

    #@1b
    iput v1, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@1d
    .line 370
    return-object v0
.end method

.method private performPause(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)V
    .registers 6
    .parameter "r"
    .parameter "finishing"

    #@0
    .prologue
    .line 207
    iget-object v2, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@2
    if-nez v2, :cond_10

    #@4
    const/4 v1, 0x1

    #@5
    .line 208
    .local v1, needState:Z
    :goto_5
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@7
    invoke-virtual {v2, p1, p2, v1}, Landroid/app/ActivityThread;->performPauseActivity(Landroid/os/IBinder;ZZ)Landroid/os/Bundle;

    #@a
    move-result-object v0

    #@b
    .line 210
    .local v0, instanceState:Landroid/os/Bundle;
    if-eqz v1, :cond_f

    #@d
    .line 211
    iput-object v0, p1, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@f
    .line 213
    :cond_f
    return-void

    #@10
    .line 207
    .end local v0           #instanceState:Landroid/os/Bundle;
    .end local v1           #needState:Z
    :cond_10
    const/4 v1, 0x0

    #@11
    goto :goto_5
.end method


# virtual methods
.method public destroyActivity(Ljava/lang/String;Z)Landroid/view/Window;
    .registers 6
    .parameter "id"
    .parameter "finish"

    #@0
    .prologue
    .line 386
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@2
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@8
    .line 387
    .local v0, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    const/4 v1, 0x0

    #@9
    .line 388
    .local v1, win:Landroid/view/Window;
    if-eqz v0, :cond_1b

    #@b
    .line 389
    invoke-direct {p0, v0, p2}, Landroid/app/LocalActivityManager;->performDestroy(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)Landroid/view/Window;

    #@e
    move-result-object v1

    #@f
    .line 390
    if-eqz p2, :cond_1b

    #@11
    .line 391
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@13
    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    .line 392
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@1b
    .line 395
    :cond_1b
    return-object v1
.end method

.method public dispatchCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "state"

    #@0
    .prologue
    .line 452
    if-eqz p1, :cond_43

    #@2
    .line 453
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@5
    move-result-object v5

    #@6
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v2

    #@a
    .local v2, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v5

    #@e
    if-eqz v5, :cond_43

    #@10
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v3

    #@14
    check-cast v3, Ljava/lang/String;

    #@16
    .line 455
    .local v3, id:Ljava/lang/String;
    :try_start_16
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@19
    move-result-object v0

    #@1a
    .line 456
    .local v0, astate:Landroid/os/Bundle;
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@1c
    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@22
    .line 457
    .local v4, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-eqz v4, :cond_30

    #@24
    .line 458
    iput-object v0, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_26} :catch_27

    #@26
    goto :goto_a

    #@27
    .line 465
    .end local v0           #astate:Landroid/os/Bundle;
    .end local v4           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :catch_27
    move-exception v1

    #@28
    .line 467
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "LocalActivityManager"

    #@2a
    const-string v6, "Exception thrown when restoring LocalActivityManager state"

    #@2c
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    goto :goto_a

    #@30
    .line 460
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #astate:Landroid/os/Bundle;
    .restart local v4       #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_30
    :try_start_30
    new-instance v4, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@32
    .end local v4           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    const/4 v5, 0x0

    #@33
    invoke-direct {v4, v3, v5}, Landroid/app/LocalActivityManager$LocalActivityRecord;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    #@36
    .line 461
    .restart local v4       #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    iput-object v0, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@38
    .line 462
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@3a
    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 463
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_42} :catch_27

    #@42
    goto :goto_a

    #@43
    .line 472
    .end local v0           #astate:Landroid/os/Bundle;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #id:Ljava/lang/String;
    .end local v4           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_43
    const/4 v5, 0x2

    #@44
    iput v5, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@46
    .line 473
    return-void
.end method

.method public dispatchDestroy(Z)V
    .registers 6
    .parameter "finishing"

    #@0
    .prologue
    .line 623
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    .line 624
    .local v0, N:I
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_19

    #@9
    .line 625
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    check-cast v2, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@11
    .line 627
    .local v2, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@13
    invoke-virtual {v3, v2, p1}, Landroid/app/ActivityThread;->performDestroyActivity(Landroid/os/IBinder;Z)Landroid/app/ActivityThread$ActivityClientRecord;

    #@16
    .line 624
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_7

    #@19
    .line 629
    .end local v2           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_19
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@1b
    invoke-interface {v3}, Ljava/util/Map;->clear()V

    #@1e
    .line 630
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    #@23
    .line 631
    return-void
.end method

.method public dispatchPause(Z)V
    .registers 8
    .parameter "finishing"

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    .line 546
    if-eqz p1, :cond_6

    #@3
    .line 547
    const/4 v3, 0x1

    #@4
    iput-boolean v3, p0, Landroid/app/LocalActivityManager;->mFinishing:Z

    #@6
    .line 549
    :cond_6
    iput v5, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@8
    .line 550
    iget-boolean v3, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@a
    if-eqz v3, :cond_16

    #@c
    .line 551
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@e
    if-eqz v3, :cond_15

    #@10
    .line 552
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@12
    invoke-direct {p0, v3, v5}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@15
    .line 563
    :cond_15
    return-void

    #@16
    .line 555
    :cond_16
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@1b
    move-result v0

    #@1c
    .line 556
    .local v0, N:I
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v0, :cond_15

    #@1f
    .line 557
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@27
    .line 558
    .local v2, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    iget v3, v2, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@29
    const/4 v4, 0x4

    #@2a
    if-ne v3, v4, :cond_2f

    #@2c
    .line 559
    invoke-direct {p0, v2, v5}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@2f
    .line 556
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_1d
.end method

.method public dispatchResume()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 520
    iput v3, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@3
    .line 521
    iget-boolean v2, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@5
    if-eqz v2, :cond_11

    #@7
    .line 522
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@9
    if-eqz v2, :cond_10

    #@b
    .line 523
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@d
    invoke-direct {p0, v2, v3}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@10
    .line 531
    :cond_10
    return-void

    #@11
    .line 526
    :cond_11
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@16
    move-result v0

    #@17
    .line 527
    .local v0, N:I
    const/4 v1, 0x0

    #@18
    .local v1, i:I
    :goto_18
    if-ge v1, v0, :cond_10

    #@1a
    .line 528
    iget-object v2, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v2

    #@20
    check-cast v2, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@22
    invoke-direct {p0, v2, v3}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@25
    .line 527
    add-int/lit8 v1, v1, 0x1

    #@27
    goto :goto_18
.end method

.method public dispatchRetainNonConfigurationInstance()Ljava/util/HashMap;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 589
    const/4 v3, 0x0

    #@1
    .line 591
    .local v3, instanceMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 592
    .local v0, N:I
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_2f

    #@a
    .line 593
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v4

    #@10
    check-cast v4, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@12
    .line 594
    .local v4, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-eqz v4, :cond_2c

    #@14
    iget-object v5, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@16
    if-eqz v5, :cond_2c

    #@18
    .line 595
    iget-object v5, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@1a
    invoke-virtual {v5}, Landroid/app/Activity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    .line 596
    .local v2, instance:Ljava/lang/Object;
    if-eqz v2, :cond_2c

    #@20
    .line 597
    if-nez v3, :cond_27

    #@22
    .line 598
    new-instance v3, Ljava/util/HashMap;

    #@24
    .end local v3           #instanceMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    #@27
    .line 600
    .restart local v3       #instanceMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_27
    iget-object v5, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->id:Ljava/lang/String;

    #@29
    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 592
    .end local v2           #instance:Ljava/lang/Object;
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_8

    #@2f
    .line 604
    .end local v4           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_2f
    return-object v3
.end method

.method public dispatchStop()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 573
    iput v4, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@3
    .line 574
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    .line 575
    .local v0, N:I
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v0, :cond_1a

    #@c
    .line 576
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@14
    .line 577
    .local v2, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    invoke-direct {p0, v2, v4}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@17
    .line 575
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_a

    #@1a
    .line 579
    .end local v2           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_1a
    return-void
.end method

.method public getActivity(Ljava/lang/String;)Landroid/app/Activity;
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 433
    iget-object v1, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@2
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@8
    .line 434
    .local v0, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-eqz v0, :cond_d

    #@a
    iget-object v1, v0, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@c
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 408
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@6
    iget-object v0, v0, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public getCurrentId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 421
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@6
    iget-object v0, v0, Landroid/app/LocalActivityManager$LocalActivityRecord;->id:Ljava/lang/String;

    #@8
    :goto_8
    return-object v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public removeAllActivities()V
    .registers 2

    #@0
    .prologue
    .line 612
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/app/LocalActivityManager;->dispatchDestroy(Z)V

    #@4
    .line 613
    return-void
.end method

.method public saveInstanceState()Landroid/os/Bundle;
    .registers 8

    #@0
    .prologue
    .line 486
    const/4 v4, 0x0

    #@1
    .line 490
    .local v4, state:Landroid/os/Bundle;
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v0

    #@7
    .line 491
    .local v0, N:I
    const/4 v2, 0x0

    #@8
    .local v2, i:I
    :goto_8
    if-ge v2, v0, :cond_40

    #@a
    .line 492
    iget-object v5, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@12
    .line 493
    .local v3, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-nez v4, :cond_19

    #@14
    .line 494
    new-instance v4, Landroid/os/Bundle;

    #@16
    .end local v4           #state:Landroid/os/Bundle;
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    #@19
    .line 496
    .restart local v4       #state:Landroid/os/Bundle;
    :cond_19
    iget-object v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@1b
    if-nez v5, :cond_22

    #@1d
    iget v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@1f
    const/4 v6, 0x4

    #@20
    if-ne v5, v6, :cond_32

    #@22
    :cond_22
    iget-object v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@24
    if-eqz v5, :cond_32

    #@26
    .line 500
    new-instance v1, Landroid/os/Bundle;

    #@28
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@2b
    .line 501
    .local v1, childState:Landroid/os/Bundle;
    iget-object v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->activity:Landroid/app/Activity;

    #@2d
    invoke-virtual {v5, v1}, Landroid/app/Activity;->performSaveInstanceState(Landroid/os/Bundle;)V

    #@30
    .line 502
    iput-object v1, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@32
    .line 504
    .end local v1           #childState:Landroid/os/Bundle;
    :cond_32
    iget-object v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@34
    if-eqz v5, :cond_3d

    #@36
    .line 505
    iget-object v5, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->id:Ljava/lang/String;

    #@38
    iget-object v6, v3, Landroid/app/LocalActivityManager$LocalActivityRecord;->instanceState:Landroid/os/Bundle;

    #@3a
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@3d
    .line 491
    :cond_3d
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_8

    #@40
    .line 509
    .end local v3           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_40
    return-object v4
.end method

.method public startActivity(Ljava/lang/String;Landroid/content/Intent;)Landroid/view/Window;
    .registers 12
    .parameter "id"
    .parameter "intent"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 261
    iget v6, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@3
    if-ne v6, v8, :cond_d

    #@5
    .line 262
    new-instance v6, Ljava/lang/IllegalStateException;

    #@7
    const-string v7, "Activities can\'t be added until the containing group has been created."

    #@9
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v6

    #@d
    .line 266
    :cond_d
    const/4 v1, 0x0

    #@e
    .line 267
    .local v1, adding:Z
    const/4 v5, 0x0

    #@f
    .line 269
    .local v5, sameIntent:Z
    const/4 v0, 0x0

    #@10
    .line 272
    .local v0, aInfo:Landroid/content/pm/ActivityInfo;
    iget-object v6, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@12
    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v4

    #@16
    check-cast v4, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@18
    .line 273
    .local v4, r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-nez v4, :cond_5b

    #@1a
    .line 275
    new-instance v4, Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@1c
    .end local v4           #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    invoke-direct {v4, p1, p2}, Landroid/app/LocalActivityManager$LocalActivityRecord;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    #@1f
    .line 276
    .restart local v4       #r:Landroid/app/LocalActivityManager$LocalActivityRecord;
    const/4 v1, 0x1

    #@20
    .line 284
    :cond_20
    :goto_20
    if-nez v0, :cond_28

    #@22
    .line 285
    iget-object v6, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@24
    invoke-virtual {v6, p2}, Landroid/app/ActivityThread;->resolveActivityInfo(Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;

    #@27
    move-result-object v0

    #@28
    .line 290
    :cond_28
    iget-boolean v6, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@2a
    if-eqz v6, :cond_3b

    #@2c
    .line 291
    iget-object v3, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@2e
    .line 295
    .local v3, old:Landroid/app/LocalActivityManager$LocalActivityRecord;
    if-eqz v3, :cond_3b

    #@30
    if-eq v3, v4, :cond_3b

    #@32
    iget v6, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@34
    const/4 v7, 0x4

    #@35
    if-ne v6, v7, :cond_3b

    #@37
    .line 296
    const/4 v6, 0x3

    #@38
    invoke-direct {p0, v3, v6}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@3b
    .line 300
    .end local v3           #old:Landroid/app/LocalActivityManager$LocalActivityRecord;
    :cond_3b
    if-eqz v1, :cond_6a

    #@3d
    .line 302
    iget-object v6, p0, Landroid/app/LocalActivityManager;->mActivities:Ljava/util/Map;

    #@3f
    invoke-interface {v6, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    .line 303
    iget-object v6, p0, Landroid/app/LocalActivityManager;->mActivityArray:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@47
    .line 343
    :cond_47
    :goto_47
    iput-object p2, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@49
    .line 344
    iput v8, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->curState:I

    #@4b
    .line 345
    iput-object v0, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4d
    .line 347
    iget v6, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@4f
    invoke-direct {p0, v4, v6}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@52
    .line 350
    iget-boolean v6, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@54
    if-eqz v6, :cond_58

    #@56
    .line 351
    iput-object v4, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@58
    .line 353
    :cond_58
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@5a
    :goto_5a
    return-object v6

    #@5b
    .line 277
    :cond_5b
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@5d
    if-eqz v6, :cond_20

    #@5f
    .line 278
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@61
    invoke-virtual {v6, p2}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@64
    move-result v5

    #@65
    .line 279
    if-eqz v5, :cond_20

    #@67
    .line 281
    iget-object v0, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@69
    goto :goto_20

    #@6a
    .line 304
    :cond_6a
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@6c
    if-eqz v6, :cond_47

    #@6e
    .line 307
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@70
    if-eq v0, v6, :cond_8a

    #@72
    iget-object v6, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@74
    iget-object v7, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@76
    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7b
    move-result v6

    #@7c
    if-eqz v6, :cond_cf

    #@7e
    iget-object v6, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@80
    iget-object v7, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@82
    iget-object v7, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v6

    #@88
    if-eqz v6, :cond_cf

    #@8a
    .line 310
    :cond_8a
    iget v6, v0, Landroid/content/pm/ActivityInfo;->launchMode:I

    #@8c
    if-nez v6, :cond_97

    #@8e
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    #@91
    move-result v6

    #@92
    const/high16 v7, 0x2000

    #@94
    and-int/2addr v6, v7

    #@95
    if-eqz v6, :cond_b4

    #@97
    .line 313
    :cond_97
    new-instance v2, Ljava/util/ArrayList;

    #@99
    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    #@9c
    .line 314
    .local v2, intents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/Intent;>;"
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9f
    .line 316
    iget-object v6, p0, Landroid/app/LocalActivityManager;->mActivityThread:Landroid/app/ActivityThread;

    #@a1
    invoke-virtual {v6, v4, v2}, Landroid/app/ActivityThread;->performNewIntents(Landroid/os/IBinder;Ljava/util/List;)V

    #@a4
    .line 317
    iput-object p2, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@a6
    .line 318
    iget v6, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@a8
    invoke-direct {p0, v4, v6}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@ab
    .line 319
    iget-boolean v6, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@ad
    if-eqz v6, :cond_b1

    #@af
    .line 320
    iput-object v4, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@b1
    .line 322
    :cond_b1
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@b3
    goto :goto_5a

    #@b4
    .line 324
    .end local v2           #intents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/Intent;>;"
    :cond_b4
    if-eqz v5, :cond_cf

    #@b6
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    #@b9
    move-result v6

    #@ba
    const/high16 v7, 0x400

    #@bc
    and-int/2addr v6, v7

    #@bd
    if-nez v6, :cond_cf

    #@bf
    .line 328
    iput-object p2, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->intent:Landroid/content/Intent;

    #@c1
    .line 329
    iget v6, p0, Landroid/app/LocalActivityManager;->mCurState:I

    #@c3
    invoke-direct {p0, v4, v6}, Landroid/app/LocalActivityManager;->moveToState(Landroid/app/LocalActivityManager$LocalActivityRecord;I)V

    #@c6
    .line 330
    iget-boolean v6, p0, Landroid/app/LocalActivityManager;->mSingleMode:Z

    #@c8
    if-eqz v6, :cond_cc

    #@ca
    .line 331
    iput-object v4, p0, Landroid/app/LocalActivityManager;->mResumed:Landroid/app/LocalActivityManager$LocalActivityRecord;

    #@cc
    .line 333
    :cond_cc
    iget-object v6, v4, Landroid/app/LocalActivityManager$LocalActivityRecord;->window:Landroid/view/Window;

    #@ce
    goto :goto_5a

    #@cf
    .line 340
    :cond_cf
    invoke-direct {p0, v4, v8}, Landroid/app/LocalActivityManager;->performDestroy(Landroid/app/LocalActivityManager$LocalActivityRecord;Z)Landroid/view/Window;

    #@d2
    goto/16 :goto_47
.end method
