.class public Landroid/app/Instrumentation;
.super Ljava/lang/Object;
.source "Instrumentation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Instrumentation$1;,
        Landroid/app/Instrumentation$Idler;,
        Landroid/app/Instrumentation$ActivityGoing;,
        Landroid/app/Instrumentation$ActivityWaiter;,
        Landroid/app/Instrumentation$SyncRunnable;,
        Landroid/app/Instrumentation$EmptyRunnable;,
        Landroid/app/Instrumentation$InstrumentationThread;,
        Landroid/app/Instrumentation$ActivityResult;,
        Landroid/app/Instrumentation$ActivityMonitor;
    }
.end annotation


# static fields
.field public static final REPORT_KEY_IDENTIFIER:Ljava/lang/String; = "id"

.field public static final REPORT_KEY_STREAMRESULT:Ljava/lang/String; = "stream"

.field private static final TAG:Ljava/lang/String; = "Instrumentation"


# instance fields
.field private mActivityMonitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Instrumentation$ActivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mAutomaticPerformanceSnapshots:Z

.field private mComponent:Landroid/content/ComponentName;

.field private mInstrContext:Landroid/content/Context;

.field private mMessageQueue:Landroid/os/MessageQueue;

.field private mPerfMetrics:Landroid/os/Bundle;

.field private mPerformanceCollector:Landroid/os/PerformanceCollector;

.field private mRunner:Ljava/lang/Thread;

.field private final mSync:Ljava/lang/Object;

.field private mThread:Landroid/app/ActivityThread;

.field private mWaitingActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Instrumentation$ActivityWaiter;",
            ">;"
        }
    .end annotation
.end field

.field private mWatcher:Landroid/app/IInstrumentationWatcher;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 92
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 78
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@b
    .line 79
    iput-object v1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@d
    .line 80
    iput-object v1, p0, Landroid/app/Instrumentation;->mMessageQueue:Landroid/os/MessageQueue;

    #@f
    .line 88
    const/4 v0, 0x0

    #@10
    iput-boolean v0, p0, Landroid/app/Instrumentation;->mAutomaticPerformanceSnapshots:Z

    #@12
    .line 90
    new-instance v0, Landroid/os/Bundle;

    #@14
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@17
    iput-object v0, p0, Landroid/app/Instrumentation;->mPerfMetrics:Landroid/os/Bundle;

    #@19
    .line 93
    return-void
.end method

.method static synthetic access$100(Landroid/app/Instrumentation;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Landroid/app/Instrumentation;->mAutomaticPerformanceSnapshots:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/app/Instrumentation;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/app/Instrumentation;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@2
    return-object v0
.end method

.method private addValue(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 7
    .parameter "key"
    .parameter "value"
    .parameter "results"

    #@0
    .prologue
    .line 1273
    invoke-virtual {p3, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_14

    #@6
    .line 1274
    invoke-virtual {p3, p1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@9
    move-result-object v1

    #@a
    .line 1275
    .local v1, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_13

    #@c
    .line 1276
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v2

    #@10
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@13
    .line 1283
    .end local v1           #list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_13
    :goto_13
    return-void

    #@14
    .line 1279
    :cond_14
    new-instance v0, Ljava/util/ArrayList;

    #@16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@19
    .line 1280
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@20
    .line 1281
    invoke-virtual {p3, p1, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@23
    goto :goto_13
.end method

.method static checkStartActivityResult(ILjava/lang/Object;)V
    .registers 5
    .parameter "res"
    .parameter "intent"

    #@0
    .prologue
    .line 1610
    if-ltz p0, :cond_3

    #@2
    .line 1611
    return-void

    #@3
    .line 1614
    :cond_3
    packed-switch p0, :pswitch_data_a2

    #@6
    .line 1634
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Unknown error code "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " when starting "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 1617
    :pswitch_29
    instance-of v0, p1, Landroid/content/Intent;

    #@2b
    if-eqz v0, :cond_5f

    #@2d
    move-object v0, p1

    #@2e
    check-cast v0, Landroid/content/Intent;

    #@30
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@33
    move-result-object v0

    #@34
    if-eqz v0, :cond_5f

    #@36
    .line 1618
    new-instance v0, Landroid/content/ActivityNotFoundException;

    #@38
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v2, "Unable to find explicit activity class "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    check-cast p1, Landroid/content/Intent;

    #@45
    .end local p1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    const-string v2, "; have you declared this activity in your AndroidManifest.xml?"

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-direct {v0, v1}, Landroid/content/ActivityNotFoundException;-><init>(Ljava/lang/String;)V

    #@5e
    throw v0

    #@5f
    .line 1622
    .restart local p1
    :cond_5f
    new-instance v0, Landroid/content/ActivityNotFoundException;

    #@61
    new-instance v1, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v2, "No Activity found to handle "

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-direct {v0, v1}, Landroid/content/ActivityNotFoundException;-><init>(Ljava/lang/String;)V

    #@77
    throw v0

    #@78
    .line 1625
    :pswitch_78
    new-instance v0, Ljava/lang/SecurityException;

    #@7a
    new-instance v1, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v2, "Not allowed to start activity "

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v1

    #@8d
    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@90
    throw v0

    #@91
    .line 1628
    :pswitch_91
    new-instance v0, Landroid/util/AndroidRuntimeException;

    #@93
    const-string v1, "FORWARD_RESULT_FLAG used while also requesting a result"

    #@95
    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@98
    throw v0

    #@99
    .line 1631
    :pswitch_99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9b
    const-string v1, "PendingIntent is not an activity"

    #@9d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a0
    throw v0

    #@a1
    .line 1614
    nop

    #@a2
    :pswitch_data_a2
    .packed-switch -0x5
        :pswitch_99
        :pswitch_78
        :pswitch_91
        :pswitch_29
        :pswitch_29
    .end packed-switch
.end method

.method public static newApplication(Ljava/lang/Class;Landroid/content/Context;)Landroid/app/Application;
    .registers 3
    .parameter
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/content/Context;",
            ")",
            "Landroid/app/Application;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 983
    .local p0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/app/Application;

    #@6
    .line 984
    .local v0, app:Landroid/app/Application;
    invoke-virtual {v0, p1}, Landroid/app/Application;->attach(Landroid/content/Context;)V

    #@9
    .line 985
    return-object v0
.end method

.method private final validateNotAppThread()V
    .registers 3

    #@0
    .prologue
    .line 1640
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 1641
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "This method can not be called from the main application thread"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1644
    :cond_e
    return-void
.end method


# virtual methods
.method public addMonitor(Landroid/content/IntentFilter;Landroid/app/Instrumentation$ActivityResult;Z)Landroid/app/Instrumentation$ActivityMonitor;
    .registers 5
    .parameter "filter"
    .parameter "result"
    .parameter "block"

    #@0
    .prologue
    .line 625
    new-instance v0, Landroid/app/Instrumentation$ActivityMonitor;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/app/Instrumentation$ActivityMonitor;-><init>(Landroid/content/IntentFilter;Landroid/app/Instrumentation$ActivityResult;Z)V

    #@5
    .line 626
    .local v0, am:Landroid/app/Instrumentation$ActivityMonitor;
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->addMonitor(Landroid/app/Instrumentation$ActivityMonitor;)V

    #@8
    .line 627
    return-object v0
.end method

.method public addMonitor(Ljava/lang/String;Landroid/app/Instrumentation$ActivityResult;Z)Landroid/app/Instrumentation$ActivityMonitor;
    .registers 5
    .parameter "cls"
    .parameter "result"
    .parameter "block"

    #@0
    .prologue
    .line 648
    new-instance v0, Landroid/app/Instrumentation$ActivityMonitor;

    #@2
    invoke-direct {v0, p1, p2, p3}, Landroid/app/Instrumentation$ActivityMonitor;-><init>(Ljava/lang/String;Landroid/app/Instrumentation$ActivityResult;Z)V

    #@5
    .line 649
    .local v0, am:Landroid/app/Instrumentation$ActivityMonitor;
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->addMonitor(Landroid/app/Instrumentation$ActivityMonitor;)V

    #@8
    .line 650
    return-object v0
.end method

.method public addMonitor(Landroid/app/Instrumentation$ActivityMonitor;)V
    .registers 4
    .parameter "monitor"

    #@0
    .prologue
    .line 598
    iget-object v1, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 599
    :try_start_3
    iget-object v0, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 600
    new-instance v0, Ljava/util/ArrayList;

    #@9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@c
    iput-object v0, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@e
    .line 602
    :cond_e
    iget-object v0, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@10
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@13
    .line 603
    monitor-exit v1

    #@14
    .line 604
    return-void

    #@15
    .line 603
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method public callActivityOnCreate(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 11
    .parameter "activity"
    .parameter "icicle"

    #@0
    .prologue
    .line 1066
    iget-object v5, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@2
    if-eqz v5, :cond_34

    #@4
    .line 1067
    iget-object v6, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@6
    monitor-enter v6

    #@7
    .line 1068
    :try_start_7
    iget-object v5, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@9
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@c
    move-result v0

    #@d
    .line 1069
    .local v0, N:I
    const/4 v3, 0x0

    #@e
    .local v3, i:I
    :goto_e
    if-ge v3, v0, :cond_33

    #@10
    .line 1070
    iget-object v5, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@12
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/app/Instrumentation$ActivityWaiter;

    #@18
    .line 1071
    .local v2, aw:Landroid/app/Instrumentation$ActivityWaiter;
    iget-object v4, v2, Landroid/app/Instrumentation$ActivityWaiter;->intent:Landroid/content/Intent;

    #@1a
    .line 1072
    .local v4, intent:Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v4, v5}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    #@21
    move-result v5

    #@22
    if-eqz v5, :cond_30

    #@24
    .line 1073
    iput-object p1, v2, Landroid/app/Instrumentation$ActivityWaiter;->activity:Landroid/app/Activity;

    #@26
    .line 1074
    iget-object v5, p0, Landroid/app/Instrumentation;->mMessageQueue:Landroid/os/MessageQueue;

    #@28
    new-instance v7, Landroid/app/Instrumentation$ActivityGoing;

    #@2a
    invoke-direct {v7, p0, v2}, Landroid/app/Instrumentation$ActivityGoing;-><init>(Landroid/app/Instrumentation;Landroid/app/Instrumentation$ActivityWaiter;)V

    #@2d
    invoke-virtual {v5, v7}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@30
    .line 1069
    :cond_30
    add-int/lit8 v3, v3, 0x1

    #@32
    goto :goto_e

    #@33
    .line 1077
    .end local v2           #aw:Landroid/app/Instrumentation$ActivityWaiter;
    .end local v4           #intent:Landroid/content/Intent;
    :cond_33
    monitor-exit v6
    :try_end_34
    .catchall {:try_start_7 .. :try_end_34} :catchall_59

    #@34
    .line 1080
    .end local v0           #N:I
    .end local v3           #i:I
    :cond_34
    invoke-virtual {p1, p2}, Landroid/app/Activity;->performCreate(Landroid/os/Bundle;)V

    #@37
    .line 1082
    iget-object v5, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@39
    if-eqz v5, :cond_5d

    #@3b
    .line 1083
    iget-object v6, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@3d
    monitor-enter v6

    #@3e
    .line 1084
    :try_start_3e
    iget-object v5, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@40
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@43
    move-result v0

    #@44
    .line 1085
    .restart local v0       #N:I
    const/4 v3, 0x0

    #@45
    .restart local v3       #i:I
    :goto_45
    if-ge v3, v0, :cond_5c

    #@47
    .line 1086
    iget-object v5, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@49
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4c
    move-result-object v1

    #@4d
    check-cast v1, Landroid/app/Instrumentation$ActivityMonitor;

    #@4f
    .line 1087
    .local v1, am:Landroid/app/Instrumentation$ActivityMonitor;
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v1, p1, p1, v5}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z
    :try_end_56
    .catchall {:try_start_3e .. :try_end_56} :catchall_5e

    #@56
    .line 1085
    add-int/lit8 v3, v3, 0x1

    #@58
    goto :goto_45

    #@59
    .line 1077
    .end local v0           #N:I
    .end local v1           #am:Landroid/app/Instrumentation$ActivityMonitor;
    .end local v3           #i:I
    :catchall_59
    move-exception v5

    #@5a
    :try_start_5a
    monitor-exit v6
    :try_end_5b
    .catchall {:try_start_5a .. :try_end_5b} :catchall_59

    #@5b
    throw v5

    #@5c
    .line 1089
    .restart local v0       #N:I
    .restart local v3       #i:I
    :cond_5c
    :try_start_5c
    monitor-exit v6

    #@5d
    .line 1091
    .end local v0           #N:I
    .end local v3           #i:I
    :cond_5d
    return-void

    #@5e
    .line 1089
    :catchall_5e
    move-exception v5

    #@5f
    monitor-exit v6
    :try_end_60
    .catchall {:try_start_5c .. :try_end_60} :catchall_5e

    #@60
    throw v5
.end method

.method public callActivityOnDestroy(Landroid/app/Activity;)V
    .registers 7
    .parameter "activity"

    #@0
    .prologue
    .line 1110
    invoke-virtual {p1}, Landroid/app/Activity;->performDestroy()V

    #@3
    .line 1112
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@5
    if-eqz v3, :cond_26

    #@7
    .line 1113
    iget-object v4, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@9
    monitor-enter v4

    #@a
    .line 1114
    :try_start_a
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@c
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@f
    move-result v0

    #@10
    .line 1115
    .local v0, N:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_25

    #@13
    .line 1116
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@15
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/app/Instrumentation$ActivityMonitor;

    #@1b
    .line 1117
    .local v1, am:Landroid/app/Instrumentation$ActivityMonitor;
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, p1, p1, v3}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@22
    .line 1115
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_11

    #@25
    .line 1119
    .end local v1           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_25
    monitor-exit v4

    #@26
    .line 1121
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_26
    return-void

    #@27
    .line 1119
    :catchall_27
    move-exception v3

    #@28
    monitor-exit v4
    :try_end_29
    .catchall {:try_start_a .. :try_end_29} :catchall_27

    #@29
    throw v3
.end method

.method public callActivityOnNewIntent(Landroid/app/Activity;Landroid/content/Intent;)V
    .registers 3
    .parameter "activity"
    .parameter "intent"

    #@0
    .prologue
    .line 1154
    invoke-virtual {p1, p2}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    #@3
    .line 1155
    return-void
.end method

.method public callActivityOnPause(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 1226
    invoke-virtual {p1}, Landroid/app/Activity;->performPause()V

    #@3
    .line 1227
    return-void
.end method

.method public callActivityOnPostCreate(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .parameter "activity"
    .parameter "icicle"

    #@0
    .prologue
    .line 1143
    invoke-virtual {p1, p2}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    #@3
    .line 1144
    return-void
.end method

.method public callActivityOnRestart(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 1174
    invoke-virtual {p1}, Landroid/app/Activity;->onRestart()V

    #@3
    .line 1175
    return-void
.end method

.method public callActivityOnRestoreInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .parameter "activity"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1131
    invoke-virtual {p1, p2}, Landroid/app/Activity;->performRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1132
    return-void
.end method

.method public callActivityOnResume(Landroid/app/Activity;)V
    .registers 7
    .parameter "activity"

    #@0
    .prologue
    .line 1184
    const/4 v3, 0x1

    #@1
    iput-boolean v3, p1, Landroid/app/Activity;->mResumed:Z

    #@3
    .line 1185
    invoke-virtual {p1}, Landroid/app/Activity;->onResume()V

    #@6
    .line 1187
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@8
    if-eqz v3, :cond_29

    #@a
    .line 1188
    iget-object v4, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@c
    monitor-enter v4

    #@d
    .line 1189
    :try_start_d
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@f
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@12
    move-result v0

    #@13
    .line 1190
    .local v0, N:I
    const/4 v2, 0x0

    #@14
    .local v2, i:I
    :goto_14
    if-ge v2, v0, :cond_28

    #@16
    .line 1191
    iget-object v3, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@18
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v1

    #@1c
    check-cast v1, Landroid/app/Instrumentation$ActivityMonitor;

    #@1e
    .line 1192
    .local v1, am:Landroid/app/Instrumentation$ActivityMonitor;
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v1, p1, p1, v3}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@25
    .line 1190
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_14

    #@28
    .line 1194
    .end local v1           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_28
    monitor-exit v4

    #@29
    .line 1196
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_29
    return-void

    #@2a
    .line 1194
    :catchall_2a
    move-exception v3

    #@2b
    monitor-exit v4
    :try_end_2c
    .catchall {:try_start_d .. :try_end_2c} :catchall_2a

    #@2c
    throw v3
.end method

.method public callActivityOnSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .parameter "activity"
    .parameter "outState"

    #@0
    .prologue
    .line 1216
    invoke-virtual {p1, p2}, Landroid/app/Activity;->performSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1217
    return-void
.end method

.method public callActivityOnStart(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 1164
    invoke-virtual {p1}, Landroid/app/Activity;->onStart()V

    #@3
    .line 1165
    return-void
.end method

.method public callActivityOnStop(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 1205
    invoke-virtual {p1}, Landroid/app/Activity;->onStop()V

    #@3
    .line 1206
    return-void
.end method

.method public callActivityOnUserLeaving(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 1236
    invoke-virtual {p1}, Landroid/app/Activity;->performUserLeaving()V

    #@3
    .line 1237
    return-void
.end method

.method public callApplicationOnCreate(Landroid/app/Application;)V
    .registers 2
    .parameter "app"

    #@0
    .prologue
    .line 1000
    invoke-virtual {p1}, Landroid/app/Application;->onCreate()V

    #@3
    .line 1001
    return-void
.end method

.method public checkMonitorHit(Landroid/app/Instrumentation$ActivityMonitor;I)Z
    .registers 5
    .parameter "monitor"
    .parameter "minHits"

    #@0
    .prologue
    .line 667
    invoke-virtual {p0}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@3
    .line 668
    iget-object v1, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 669
    :try_start_6
    invoke-virtual {p1}, Landroid/app/Instrumentation$ActivityMonitor;->getHits()I

    #@9
    move-result v0

    #@a
    if-ge v0, p2, :cond_f

    #@c
    .line 670
    const/4 v0, 0x0

    #@d
    monitor-exit v1

    #@e
    .line 674
    :goto_e
    return v0

    #@f
    .line 672
    :cond_f
    iget-object v0, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@11
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@14
    .line 673
    monitor-exit v1

    #@15
    .line 674
    const/4 v0, 0x1

    #@16
    goto :goto_e

    #@17
    .line 673
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_6 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public endPerformanceSnapshot()V
    .registers 2

    #@0
    .prologue
    .line 204
    invoke-virtual {p0}, Landroid/app/Instrumentation;->isProfiling()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 205
    iget-object v0, p0, Landroid/app/Instrumentation;->mPerformanceCollector:Landroid/os/PerformanceCollector;

    #@8
    invoke-virtual {v0}, Landroid/os/PerformanceCollector;->endSnapshot()Landroid/os/Bundle;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/app/Instrumentation;->mPerfMetrics:Landroid/os/Bundle;

    #@e
    .line 207
    :cond_e
    return-void
.end method

.method public execStartActivities(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 15
    .parameter "who"
    .parameter "contextThread"
    .parameter "token"
    .parameter "target"
    .parameter "intents"
    .parameter "options"

    #@0
    .prologue
    .line 1433
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v7

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v2, p2

    #@7
    move-object v3, p3

    #@8
    move-object v4, p4

    #@9
    move-object v5, p5

    #@a
    move-object v6, p6

    #@b
    invoke-virtual/range {v0 .. v7}, Landroid/app/Instrumentation;->execStartActivitiesAsUser(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;I)V

    #@e
    .line 1435
    return-void
.end method

.method public execStartActivitiesAsUser(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;[Landroid/content/Intent;Landroid/os/Bundle;I)V
    .registers 20
    .parameter "who"
    .parameter "contextThread"
    .parameter "token"
    .parameter "target"
    .parameter "intents"
    .parameter "options"
    .parameter "userId"

    #@0
    .prologue
    .line 1448
    move-object v2, p2

    #@1
    check-cast v2, Landroid/app/IApplicationThread;

    #@3
    .line 1449
    .local v2, whoThread:Landroid/app/IApplicationThread;
    iget-object v1, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@5
    if-eqz v1, :cond_37

    #@7
    .line 1450
    iget-object v3, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@9
    monitor-enter v3

    #@a
    .line 1451
    :try_start_a
    iget-object v1, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@c
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@f
    move-result v8

    #@10
    .line 1452
    .local v8, N:I
    const/4 v10, 0x0

    #@11
    .local v10, i:I
    :goto_11
    if-ge v10, v8, :cond_36

    #@13
    .line 1453
    iget-object v1, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@15
    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v9

    #@19
    check-cast v9, Landroid/app/Instrumentation$ActivityMonitor;

    #@1b
    .line 1454
    .local v9, am:Landroid/app/Instrumentation$ActivityMonitor;
    const/4 v1, 0x0

    #@1c
    const/4 v5, 0x0

    #@1d
    aget-object v5, p5, v5

    #@1f
    invoke-virtual {v9, p1, v1, v5}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_33

    #@25
    .line 1455
    iget v1, v9, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@27
    add-int/lit8 v1, v1, 0x1

    #@29
    iput v1, v9, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@2b
    .line 1456
    invoke-virtual {v9}, Landroid/app/Instrumentation$ActivityMonitor;->isBlocking()Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_36

    #@31
    .line 1457
    monitor-exit v3

    #@32
    .line 1476
    .end local v8           #N:I
    .end local v9           #am:Landroid/app/Instrumentation$ActivityMonitor;
    .end local v10           #i:I
    :goto_32
    return-void

    #@33
    .line 1452
    .restart local v8       #N:I
    .restart local v9       #am:Landroid/app/Instrumentation$ActivityMonitor;
    .restart local v10       #i:I
    :cond_33
    add-int/lit8 v10, v10, 0x1

    #@35
    goto :goto_11

    #@36
    .line 1462
    .end local v9           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_36
    monitor-exit v3
    :try_end_37
    .catchall {:try_start_a .. :try_end_37} :catchall_57

    #@37
    .line 1465
    .end local v8           #N:I
    .end local v10           #i:I
    :cond_37
    :try_start_37
    move-object/from16 v0, p5

    #@39
    array-length v1, v0

    #@3a
    new-array v4, v1, [Ljava/lang/String;

    #@3c
    .line 1466
    .local v4, resolvedTypes:[Ljava/lang/String;
    const/4 v10, 0x0

    #@3d
    .restart local v10       #i:I
    :goto_3d
    move-object/from16 v0, p5

    #@3f
    array-length v1, v0

    #@40
    if-ge v10, v1, :cond_5a

    #@42
    .line 1467
    aget-object v1, p5, v10

    #@44
    const/4 v3, 0x0

    #@45
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAllowFds(Z)V

    #@48
    .line 1468
    aget-object v1, p5, v10

    #@4a
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v1, v3}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    aput-object v1, v4, v10
    :try_end_54
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_54} :catch_70

    #@54
    .line 1466
    add-int/lit8 v10, v10, 0x1

    #@56
    goto :goto_3d

    #@57
    .line 1462
    .end local v4           #resolvedTypes:[Ljava/lang/String;
    .end local v10           #i:I
    :catchall_57
    move-exception v1

    #@58
    :try_start_58
    monitor-exit v3
    :try_end_59
    .catchall {:try_start_58 .. :try_end_59} :catchall_57

    #@59
    throw v1

    #@5a
    .line 1470
    .restart local v4       #resolvedTypes:[Ljava/lang/String;
    .restart local v10       #i:I
    :cond_5a
    :try_start_5a
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5d
    move-result-object v1

    #@5e
    move-object/from16 v3, p5

    #@60
    move-object v5, p3

    #@61
    move-object/from16 v6, p6

    #@63
    move/from16 v7, p7

    #@65
    invoke-interface/range {v1 .. v7}, Landroid/app/IActivityManager;->startActivities(Landroid/app/IApplicationThread;[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I

    #@68
    move-result v11

    #@69
    .line 1473
    .local v11, result:I
    const/4 v1, 0x0

    #@6a
    aget-object v1, p5, v1

    #@6c
    invoke-static {v11, v1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_6f
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_6f} :catch_70

    #@6f
    goto :goto_32

    #@70
    .line 1474
    .end local v4           #resolvedTypes:[Ljava/lang/String;
    .end local v10           #i:I
    .end local v11           #result:I
    :catch_70
    move-exception v1

    #@71
    goto :goto_32
.end method

.method public execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;
    .registers 25
    .parameter "who"
    .parameter "contextThread"
    .parameter "token"
    .parameter "target"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 1393
    move-object/from16 v3, p2

    #@2
    check-cast v3, Landroid/app/IApplicationThread;

    #@4
    .line 1394
    .local v3, whoThread:Landroid/app/IApplicationThread;
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@8
    if-eqz v2, :cond_49

    #@a
    .line 1395
    move-object/from16 v0, p0

    #@c
    iget-object v4, v0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@e
    monitor-enter v4

    #@f
    .line 1396
    :try_start_f
    move-object/from16 v0, p0

    #@11
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@13
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@16
    move-result v13

    #@17
    .line 1397
    .local v13, N:I
    const/4 v15, 0x0

    #@18
    .local v15, i:I
    :goto_18
    if-ge v15, v13, :cond_48

    #@1a
    .line 1398
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@1e
    invoke-interface {v2, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v14

    #@22
    check-cast v14, Landroid/app/Instrumentation$ActivityMonitor;

    #@24
    .line 1399
    .local v14, am:Landroid/app/Instrumentation$ActivityMonitor;
    const/4 v2, 0x0

    #@25
    move-object/from16 v0, p1

    #@27
    move-object/from16 v1, p5

    #@29
    invoke-virtual {v14, v0, v2, v1}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_45

    #@2f
    .line 1400
    iget v2, v14, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@31
    add-int/lit8 v2, v2, 0x1

    #@33
    iput v2, v14, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@35
    .line 1401
    invoke-virtual {v14}, Landroid/app/Instrumentation$ActivityMonitor;->isBlocking()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_48

    #@3b
    .line 1402
    if-ltz p6, :cond_43

    #@3d
    invoke-virtual {v14}, Landroid/app/Instrumentation$ActivityMonitor;->getResult()Landroid/app/Instrumentation$ActivityResult;

    #@40
    move-result-object v2

    #@41
    :goto_41
    monitor-exit v4

    #@42
    .line 1420
    .end local v13           #N:I
    .end local v14           #am:Landroid/app/Instrumentation$ActivityMonitor;
    .end local v15           #i:I
    :goto_42
    return-object v2

    #@43
    .line 1402
    .restart local v13       #N:I
    .restart local v14       #am:Landroid/app/Instrumentation$ActivityMonitor;
    .restart local v15       #i:I
    :cond_43
    const/4 v2, 0x0

    #@44
    goto :goto_41

    #@45
    .line 1397
    :cond_45
    add-int/lit8 v15, v15, 0x1

    #@47
    goto :goto_18

    #@48
    .line 1407
    .end local v14           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_48
    monitor-exit v4
    :try_end_49
    .catchall {:try_start_f .. :try_end_49} :catchall_7e

    #@49
    .line 1410
    .end local v13           #N:I
    .end local v15           #i:I
    :cond_49
    const/4 v2, 0x0

    #@4a
    :try_start_4a
    move-object/from16 v0, p5

    #@4c
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@4f
    .line 1411
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->migrateExtraStreamToClipData()Z

    #@52
    .line 1412
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@55
    move-result-object v2

    #@56
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@59
    move-result-object v4

    #@5a
    move-object/from16 v0, p5

    #@5c
    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    if-eqz p4, :cond_81

    #@62
    move-object/from16 v0, p4

    #@64
    iget-object v7, v0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@66
    :goto_66
    const/4 v9, 0x0

    #@67
    const/4 v10, 0x0

    #@68
    const/4 v11, 0x0

    #@69
    move-object/from16 v4, p5

    #@6b
    move-object/from16 v6, p3

    #@6d
    move/from16 v8, p6

    #@6f
    move-object/from16 v12, p7

    #@71
    invoke-interface/range {v2 .. v12}, Landroid/app/IActivityManager;->startActivity(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I

    #@74
    move-result v16

    #@75
    .line 1417
    .local v16, result:I
    move/from16 v0, v16

    #@77
    move-object/from16 v1, p5

    #@79
    invoke-static {v0, v1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_7c
    .catch Landroid/os/RemoteException; {:try_start_4a .. :try_end_7c} :catch_83

    #@7c
    .line 1420
    .end local v16           #result:I
    :goto_7c
    const/4 v2, 0x0

    #@7d
    goto :goto_42

    #@7e
    .line 1407
    :catchall_7e
    move-exception v2

    #@7f
    :try_start_7f
    monitor-exit v4
    :try_end_80
    .catchall {:try_start_7f .. :try_end_80} :catchall_7e

    #@80
    throw v2

    #@81
    .line 1412
    :cond_81
    const/4 v7, 0x0

    #@82
    goto :goto_66

    #@83
    .line 1418
    :catch_83
    move-exception v2

    #@84
    goto :goto_7c
.end method

.method public execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/Instrumentation$ActivityResult;
    .registers 27
    .parameter "who"
    .parameter "contextThread"
    .parameter "token"
    .parameter "target"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 1568
    move-object/from16 v3, p2

    #@2
    check-cast v3, Landroid/app/IApplicationThread;

    #@4
    .line 1569
    .local v3, whoThread:Landroid/app/IApplicationThread;
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@8
    if-eqz v2, :cond_4e

    #@a
    .line 1570
    move-object/from16 v0, p0

    #@c
    iget-object v4, v0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@e
    monitor-enter v4

    #@f
    .line 1571
    :try_start_f
    move-object/from16 v0, p0

    #@11
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@13
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@16
    move-result v14

    #@17
    .line 1572
    .local v14, N:I
    const/16 v16, 0x0

    #@19
    .local v16, i:I
    :goto_19
    move/from16 v0, v16

    #@1b
    if-ge v0, v14, :cond_4d

    #@1d
    .line 1573
    move-object/from16 v0, p0

    #@1f
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@21
    move/from16 v0, v16

    #@23
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v15

    #@27
    check-cast v15, Landroid/app/Instrumentation$ActivityMonitor;

    #@29
    .line 1574
    .local v15, am:Landroid/app/Instrumentation$ActivityMonitor;
    const/4 v2, 0x0

    #@2a
    move-object/from16 v0, p1

    #@2c
    move-object/from16 v1, p5

    #@2e
    invoke-virtual {v15, v0, v2, v1}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@31
    move-result v2

    #@32
    if-eqz v2, :cond_4a

    #@34
    .line 1575
    iget v2, v15, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@36
    add-int/lit8 v2, v2, 0x1

    #@38
    iput v2, v15, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@3a
    .line 1576
    invoke-virtual {v15}, Landroid/app/Instrumentation$ActivityMonitor;->isBlocking()Z

    #@3d
    move-result v2

    #@3e
    if-eqz v2, :cond_4d

    #@40
    .line 1577
    if-ltz p6, :cond_48

    #@42
    invoke-virtual {v15}, Landroid/app/Instrumentation$ActivityMonitor;->getResult()Landroid/app/Instrumentation$ActivityResult;

    #@45
    move-result-object v2

    #@46
    :goto_46
    monitor-exit v4

    #@47
    .line 1595
    .end local v14           #N:I
    .end local v15           #am:Landroid/app/Instrumentation$ActivityMonitor;
    .end local v16           #i:I
    :goto_47
    return-object v2

    #@48
    .line 1577
    .restart local v14       #N:I
    .restart local v15       #am:Landroid/app/Instrumentation$ActivityMonitor;
    .restart local v16       #i:I
    :cond_48
    const/4 v2, 0x0

    #@49
    goto :goto_46

    #@4a
    .line 1572
    :cond_4a
    add-int/lit8 v16, v16, 0x1

    #@4c
    goto :goto_19

    #@4d
    .line 1582
    .end local v15           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_4d
    monitor-exit v4
    :try_end_4e
    .catchall {:try_start_f .. :try_end_4e} :catchall_87

    #@4e
    .line 1585
    .end local v14           #N:I
    .end local v16           #i:I
    :cond_4e
    const/4 v2, 0x0

    #@4f
    :try_start_4f
    move-object/from16 v0, p5

    #@51
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@54
    .line 1586
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->migrateExtraStreamToClipData()Z

    #@57
    .line 1587
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5e
    move-result-object v4

    #@5f
    move-object/from16 v0, p5

    #@61
    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@64
    move-result-object v5

    #@65
    if-eqz p4, :cond_8a

    #@67
    move-object/from16 v0, p4

    #@69
    iget-object v7, v0, Landroid/app/Activity;->mEmbeddedID:Ljava/lang/String;

    #@6b
    :goto_6b
    const/4 v9, 0x0

    #@6c
    const/4 v10, 0x0

    #@6d
    const/4 v11, 0x0

    #@6e
    invoke-virtual/range {p8 .. p8}, Landroid/os/UserHandle;->getIdentifier()I

    #@71
    move-result v13

    #@72
    move-object/from16 v4, p5

    #@74
    move-object/from16 v6, p3

    #@76
    move/from16 v8, p6

    #@78
    move-object/from16 v12, p7

    #@7a
    invoke-interface/range {v2 .. v13}, Landroid/app/IActivityManager;->startActivityAsUser(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)I

    #@7d
    move-result v17

    #@7e
    .line 1592
    .local v17, result:I
    move/from16 v0, v17

    #@80
    move-object/from16 v1, p5

    #@82
    invoke-static {v0, v1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_85
    .catch Landroid/os/RemoteException; {:try_start_4f .. :try_end_85} :catch_8c

    #@85
    .line 1595
    .end local v17           #result:I
    :goto_85
    const/4 v2, 0x0

    #@86
    goto :goto_47

    #@87
    .line 1582
    :catchall_87
    move-exception v2

    #@88
    :try_start_88
    monitor-exit v4
    :try_end_89
    .catchall {:try_start_88 .. :try_end_89} :catchall_87

    #@89
    throw v2

    #@8a
    .line 1587
    :cond_8a
    const/4 v7, 0x0

    #@8b
    goto :goto_6b

    #@8c
    .line 1593
    :catch_8c
    move-exception v2

    #@8d
    goto :goto_85
.end method

.method public execStartActivity(Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;
    .registers 25
    .parameter "who"
    .parameter "contextThread"
    .parameter "token"
    .parameter "target"
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 1508
    move-object/from16 v3, p2

    #@2
    check-cast v3, Landroid/app/IApplicationThread;

    #@4
    .line 1509
    .local v3, whoThread:Landroid/app/IApplicationThread;
    move-object/from16 v0, p0

    #@6
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@8
    if-eqz v2, :cond_49

    #@a
    .line 1510
    move-object/from16 v0, p0

    #@c
    iget-object v4, v0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@e
    monitor-enter v4

    #@f
    .line 1511
    :try_start_f
    move-object/from16 v0, p0

    #@11
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@13
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@16
    move-result v13

    #@17
    .line 1512
    .local v13, N:I
    const/4 v15, 0x0

    #@18
    .local v15, i:I
    :goto_18
    if-ge v15, v13, :cond_48

    #@1a
    .line 1513
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@1e
    invoke-interface {v2, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v14

    #@22
    check-cast v14, Landroid/app/Instrumentation$ActivityMonitor;

    #@24
    .line 1514
    .local v14, am:Landroid/app/Instrumentation$ActivityMonitor;
    const/4 v2, 0x0

    #@25
    move-object/from16 v0, p1

    #@27
    move-object/from16 v1, p5

    #@29
    invoke-virtual {v14, v0, v2, v1}, Landroid/app/Instrumentation$ActivityMonitor;->match(Landroid/content/Context;Landroid/app/Activity;Landroid/content/Intent;)Z

    #@2c
    move-result v2

    #@2d
    if-eqz v2, :cond_45

    #@2f
    .line 1515
    iget v2, v14, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@31
    add-int/lit8 v2, v2, 0x1

    #@33
    iput v2, v14, Landroid/app/Instrumentation$ActivityMonitor;->mHits:I

    #@35
    .line 1516
    invoke-virtual {v14}, Landroid/app/Instrumentation$ActivityMonitor;->isBlocking()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_48

    #@3b
    .line 1517
    if-ltz p6, :cond_43

    #@3d
    invoke-virtual {v14}, Landroid/app/Instrumentation$ActivityMonitor;->getResult()Landroid/app/Instrumentation$ActivityResult;

    #@40
    move-result-object v2

    #@41
    :goto_41
    monitor-exit v4

    #@42
    .line 1535
    .end local v13           #N:I
    .end local v14           #am:Landroid/app/Instrumentation$ActivityMonitor;
    .end local v15           #i:I
    :goto_42
    return-object v2

    #@43
    .line 1517
    .restart local v13       #N:I
    .restart local v14       #am:Landroid/app/Instrumentation$ActivityMonitor;
    .restart local v15       #i:I
    :cond_43
    const/4 v2, 0x0

    #@44
    goto :goto_41

    #@45
    .line 1512
    :cond_45
    add-int/lit8 v15, v15, 0x1

    #@47
    goto :goto_18

    #@48
    .line 1522
    .end local v14           #am:Landroid/app/Instrumentation$ActivityMonitor;
    :cond_48
    monitor-exit v4
    :try_end_49
    .catchall {:try_start_f .. :try_end_49} :catchall_7e

    #@49
    .line 1525
    .end local v13           #N:I
    .end local v15           #i:I
    :cond_49
    const/4 v2, 0x0

    #@4a
    :try_start_4a
    move-object/from16 v0, p5

    #@4c
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAllowFds(Z)V

    #@4f
    .line 1526
    invoke-virtual/range {p5 .. p5}, Landroid/content/Intent;->migrateExtraStreamToClipData()Z

    #@52
    .line 1527
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@55
    move-result-object v2

    #@56
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@59
    move-result-object v4

    #@5a
    move-object/from16 v0, p5

    #@5c
    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    if-eqz p4, :cond_81

    #@62
    move-object/from16 v0, p4

    #@64
    iget-object v7, v0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@66
    :goto_66
    const/4 v9, 0x0

    #@67
    const/4 v10, 0x0

    #@68
    const/4 v11, 0x0

    #@69
    move-object/from16 v4, p5

    #@6b
    move-object/from16 v6, p3

    #@6d
    move/from16 v8, p6

    #@6f
    move-object/from16 v12, p7

    #@71
    invoke-interface/range {v2 .. v12}, Landroid/app/IActivityManager;->startActivity(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I

    #@74
    move-result v16

    #@75
    .line 1532
    .local v16, result:I
    move/from16 v0, v16

    #@77
    move-object/from16 v1, p5

    #@79
    invoke-static {v0, v1}, Landroid/app/Instrumentation;->checkStartActivityResult(ILjava/lang/Object;)V
    :try_end_7c
    .catch Landroid/os/RemoteException; {:try_start_4a .. :try_end_7c} :catch_83

    #@7c
    .line 1535
    .end local v16           #result:I
    :goto_7c
    const/4 v2, 0x0

    #@7d
    goto :goto_42

    #@7e
    .line 1522
    :catchall_7e
    move-exception v2

    #@7f
    :try_start_7f
    monitor-exit v4
    :try_end_80
    .catchall {:try_start_7f .. :try_end_80} :catchall_7e

    #@80
    throw v2

    #@81
    .line 1527
    :cond_81
    const/4 v7, 0x0

    #@82
    goto :goto_66

    #@83
    .line 1533
    :catch_83
    move-exception v2

    #@84
    goto :goto_7c
.end method

.method public finish(ILandroid/os/Bundle;)V
    .registers 4
    .parameter "resultCode"
    .parameter "results"

    #@0
    .prologue
    .line 183
    iget-boolean v0, p0, Landroid/app/Instrumentation;->mAutomaticPerformanceSnapshots:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 184
    invoke-virtual {p0}, Landroid/app/Instrumentation;->endPerformanceSnapshot()V

    #@7
    .line 186
    :cond_7
    iget-object v0, p0, Landroid/app/Instrumentation;->mPerfMetrics:Landroid/os/Bundle;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 187
    iget-object v0, p0, Landroid/app/Instrumentation;->mPerfMetrics:Landroid/os/Bundle;

    #@d
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    #@10
    .line 189
    :cond_10
    iget-object v0, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@12
    invoke-virtual {v0, p1, p2}, Landroid/app/ActivityThread;->finishInstrumentation(ILandroid/os/Bundle;)V

    #@15
    .line 190
    return-void
.end method

.method public getAllocCounts()Landroid/os/Bundle;
    .registers 5

    #@0
    .prologue
    .line 1289
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 1290
    .local v0, results:Landroid/os/Bundle;
    const-string v1, "global_alloc_count"

    #@7
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocCount()I

    #@a
    move-result v2

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@f
    .line 1291
    const-string v1, "global_alloc_size"

    #@11
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    #@14
    move-result v2

    #@15
    int-to-long v2, v2

    #@16
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@19
    .line 1292
    const-string v1, "global_freed_count"

    #@1b
    invoke-static {}, Landroid/os/Debug;->getGlobalFreedCount()I

    #@1e
    move-result v2

    #@1f
    int-to-long v2, v2

    #@20
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@23
    .line 1293
    const-string v1, "global_freed_size"

    #@25
    invoke-static {}, Landroid/os/Debug;->getGlobalFreedSize()I

    #@28
    move-result v2

    #@29
    int-to-long v2, v2

    #@2a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@2d
    .line 1294
    const-string v1, "gc_invocation_count"

    #@2f
    invoke-static {}, Landroid/os/Debug;->getGlobalGcInvocationCount()I

    #@32
    move-result v2

    #@33
    int-to-long v2, v2

    #@34
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@37
    .line 1295
    return-object v0
.end method

.method public getBinderCounts()Landroid/os/Bundle;
    .registers 5

    #@0
    .prologue
    .line 1303
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 1304
    .local v0, results:Landroid/os/Bundle;
    const-string/jumbo v1, "sent_transactions"

    #@8
    invoke-static {}, Landroid/os/Debug;->getBinderSentTransactions()I

    #@b
    move-result v2

    #@c
    int-to-long v2, v2

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@10
    .line 1305
    const-string/jumbo v1, "received_transactions"

    #@13
    invoke-static {}, Landroid/os/Debug;->getBinderReceivedTransactions()I

    #@16
    move-result v2

    #@17
    int-to-long v2, v2

    #@18
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1b
    .line 1306
    return-object v0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 238
    iget-object v0, p0, Landroid/app/Instrumentation;->mComponent:Landroid/content/ComponentName;

    #@2
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 229
    iget-object v0, p0, Landroid/app/Instrumentation;->mInstrContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getTargetContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 253
    iget-object v0, p0, Landroid/app/Instrumentation;->mAppContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method final init(Landroid/app/ActivityThread;Landroid/content/Context;Landroid/content/Context;Landroid/content/ComponentName;Landroid/app/IInstrumentationWatcher;)V
    .registers 7
    .parameter "thread"
    .parameter "instrContext"
    .parameter "appContext"
    .parameter "component"
    .parameter "watcher"

    #@0
    .prologue
    .line 1601
    iput-object p1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@2
    .line 1602
    iget-object v0, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@4
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getLooper()Landroid/os/Looper;

    #@7
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/app/Instrumentation;->mMessageQueue:Landroid/os/MessageQueue;

    #@d
    .line 1603
    iput-object p2, p0, Landroid/app/Instrumentation;->mInstrContext:Landroid/content/Context;

    #@f
    .line 1604
    iput-object p3, p0, Landroid/app/Instrumentation;->mAppContext:Landroid/content/Context;

    #@11
    .line 1605
    iput-object p4, p0, Landroid/app/Instrumentation;->mComponent:Landroid/content/ComponentName;

    #@13
    .line 1606
    iput-object p5, p0, Landroid/app/Instrumentation;->mWatcher:Landroid/app/IInstrumentationWatcher;

    #@15
    .line 1607
    return-void
.end method

.method public invokeContextMenuAction(Landroid/app/Activity;II)Z
    .registers 12
    .parameter "targetActivity"
    .parameter "id"
    .parameter "flag"

    #@0
    .prologue
    const/16 v7, 0x17

    #@2
    const/4 v4, 0x0

    #@3
    .line 777
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@6
    .line 783
    new-instance v1, Landroid/view/KeyEvent;

    #@8
    invoke-direct {v1, v4, v7}, Landroid/view/KeyEvent;-><init>(II)V

    #@b
    .line 784
    .local v1, downEvent:Landroid/view/KeyEvent;
    invoke-virtual {p0, v1}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@e
    .line 787
    invoke-virtual {p0}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@11
    .line 789
    :try_start_11
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    #@14
    move-result v5

    #@15
    int-to-long v5, v5

    #@16
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_19} :catch_30

    #@19
    .line 795
    new-instance v3, Landroid/view/KeyEvent;

    #@1b
    const/4 v4, 0x1

    #@1c
    invoke-direct {v3, v4, v7}, Landroid/view/KeyEvent;-><init>(II)V

    #@1f
    .line 796
    .local v3, upEvent:Landroid/view/KeyEvent;
    invoke-virtual {p0, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@22
    .line 799
    invoke-virtual {p0}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@25
    .line 823
    new-instance v0, Landroid/app/Instrumentation$1ContextMenuRunnable;

    #@27
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/app/Instrumentation$1ContextMenuRunnable;-><init>(Landroid/app/Instrumentation;Landroid/app/Activity;II)V

    #@2a
    .line 824
    .local v0, cmr:Landroid/app/Instrumentation$1ContextMenuRunnable;
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    #@2d
    .line 825
    iget-boolean v4, v0, Landroid/app/Instrumentation$1ContextMenuRunnable;->returnValue:Z

    #@2f
    .end local v0           #cmr:Landroid/app/Instrumentation$1ContextMenuRunnable;
    .end local v3           #upEvent:Landroid/view/KeyEvent;
    :goto_2f
    return v4

    #@30
    .line 790
    :catch_30
    move-exception v2

    #@31
    .line 791
    .local v2, e:Ljava/lang/InterruptedException;
    const-string v5, "Instrumentation"

    #@33
    const-string v6, "Could not sleep for long press timeout"

    #@35
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    goto :goto_2f
.end method

.method public invokeMenuActionSync(Landroid/app/Activity;II)Z
    .registers 6
    .parameter "targetActivity"
    .parameter "id"
    .parameter "flag"

    #@0
    .prologue
    .line 761
    new-instance v0, Landroid/app/Instrumentation$1MenuRunnable;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/app/Instrumentation$1MenuRunnable;-><init>(Landroid/app/Instrumentation;Landroid/app/Activity;II)V

    #@5
    .line 762
    .local v0, mr:Landroid/app/Instrumentation$1MenuRunnable;
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    #@8
    .line 763
    iget-boolean v1, v0, Landroid/app/Instrumentation$1MenuRunnable;->returnValue:Z

    #@a
    return v1
.end method

.method public isProfiling()Z
    .registers 2

    #@0
    .prologue
    .line 262
    iget-object v0, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@2
    invoke-virtual {v0}, Landroid/app/ActivityThread;->isProfiling()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public newActivity(Ljava/lang/Class;Landroid/content/Context;Landroid/os/IBinder;Landroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)Landroid/app/Activity;
    .registers 24
    .parameter
    .parameter "context"
    .parameter "token"
    .parameter "application"
    .parameter "intent"
    .parameter "info"
    .parameter "title"
    .parameter "parent"
    .parameter "id"
    .parameter "lastNonConfigurationInstance"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/content/Context;",
            "Landroid/os/IBinder;",
            "Landroid/app/Application;",
            "Landroid/content/Intent;",
            "Landroid/content/pm/ActivityInfo;",
            "Ljava/lang/CharSequence;",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Landroid/app/Activity;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    #@0
    .prologue
    .line 1029
    .local p1, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    check-cast v0, Landroid/app/Activity;

    #@6
    .line 1030
    .local v0, activity:Landroid/app/Activity;
    const/4 v2, 0x0

    #@7
    .local v2, aThread:Landroid/app/ActivityThread;
    move-object/from16 v11, p10

    #@9
    .line 1031
    check-cast v11, Landroid/app/Activity$NonConfigurationInstances;

    #@b
    new-instance v12, Landroid/content/res/Configuration;

    #@d
    invoke-direct {v12}, Landroid/content/res/Configuration;-><init>()V

    #@10
    move-object v1, p2

    #@11
    move-object v3, p0

    #@12
    move-object/from16 v4, p3

    #@14
    move-object/from16 v5, p4

    #@16
    move-object/from16 v6, p5

    #@18
    move-object/from16 v7, p6

    #@1a
    move-object/from16 v8, p7

    #@1c
    move-object/from16 v9, p8

    #@1e
    move-object/from16 v10, p9

    #@20
    invoke-virtual/range {v0 .. v12}, Landroid/app/Activity;->attach(Landroid/content/Context;Landroid/app/ActivityThread;Landroid/app/Instrumentation;Landroid/os/IBinder;Landroid/app/Application;Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Ljava/lang/CharSequence;Landroid/app/Activity;Ljava/lang/String;Landroid/app/Activity$NonConfigurationInstances;Landroid/content/res/Configuration;)V

    #@23
    .line 1035
    return-object v0
.end method

.method public newActivity(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Intent;)Landroid/app/Activity;
    .registers 5
    .parameter "cl"
    .parameter "className"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1054
    invoke-virtual {p1, p2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/app/Activity;

    #@a
    return-object v0
.end method

.method public newApplication(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Context;)Landroid/app/Application;
    .registers 5
    .parameter "cl"
    .parameter "className"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 968
    invoke-virtual {p1, p2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0, p3}, Landroid/app/Instrumentation;->newApplication(Ljava/lang/Class;Landroid/content/Context;)Landroid/app/Application;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter "arguments"

    #@0
    .prologue
    .line 111
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 214
    return-void
.end method

.method public onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z
    .registers 4
    .parameter "obj"
    .parameter "e"

    #@0
    .prologue
    .line 153
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onStart()V
    .registers 1

    #@0
    .prologue
    .line 136
    return-void
.end method

.method public removeMonitor(Landroid/app/Instrumentation$ActivityMonitor;)V
    .registers 4
    .parameter "monitor"

    #@0
    .prologue
    .line 722
    iget-object v1, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 723
    :try_start_3
    iget-object v0, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@5
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@8
    .line 724
    monitor-exit v1

    #@9
    .line 725
    return-void

    #@a
    .line 724
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public runOnMainSync(Ljava/lang/Runnable;)V
    .registers 4
    .parameter "runner"

    #@0
    .prologue
    .line 337
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@3
    .line 338
    new-instance v0, Landroid/app/Instrumentation$SyncRunnable;

    #@5
    invoke-direct {v0, p1}, Landroid/app/Instrumentation$SyncRunnable;-><init>(Ljava/lang/Runnable;)V

    #@8
    .line 339
    .local v0, sr:Landroid/app/Instrumentation$SyncRunnable;
    iget-object v1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@a
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@11
    .line 340
    invoke-virtual {v0}, Landroid/app/Instrumentation$SyncRunnable;->waitForComplete()V

    #@14
    .line 341
    return-void
.end method

.method public sendCharacterSync(I)V
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 910
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@6
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@9
    .line 911
    new-instance v0, Landroid/view/KeyEvent;

    #@b
    const/4 v1, 0x1

    #@c
    invoke-direct {v0, v1, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@f
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@12
    .line 912
    return-void
.end method

.method public sendKeyDownUpSync(I)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 897
    new-instance v0, Landroid/view/KeyEvent;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@6
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@9
    .line 898
    new-instance v0, Landroid/view/KeyEvent;

    #@b
    const/4 v1, 0x1

    #@c
    invoke-direct {v0, v1, p1}, Landroid/view/KeyEvent;-><init>(II)V

    #@f
    invoke-virtual {p0, v0}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@12
    .line 899
    return-void
.end method

.method public sendKeySync(Landroid/view/KeyEvent;)V
    .registers 18
    .parameter "event"

    #@0
    .prologue
    .line 864
    invoke-direct/range {p0 .. p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@3
    .line 866
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@6
    move-result-wide v1

    #@7
    .line 867
    .local v1, downTime:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getEventTime()J

    #@a
    move-result-wide v3

    #@b
    .line 868
    .local v3, eventTime:J
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    #@e
    move-result v5

    #@f
    .line 869
    .local v5, action:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@12
    move-result v6

    #@13
    .line 870
    .local v6, code:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@16
    move-result v7

    #@17
    .line 871
    .local v7, repeatCount:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getMetaState()I

    #@1a
    move-result v8

    #@1b
    .line 872
    .local v8, metaState:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDeviceId()I

    #@1e
    move-result v9

    #@1f
    .line 873
    .local v9, deviceId:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getScanCode()I

    #@22
    move-result v10

    #@23
    .line 874
    .local v10, scancode:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getSource()I

    #@26
    move-result v12

    #@27
    .line 875
    .local v12, source:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@2a
    move-result v13

    #@2b
    .line 876
    .local v13, flags:I
    if-nez v12, :cond_2f

    #@2d
    .line 877
    const/16 v12, 0x101

    #@2f
    .line 879
    :cond_2f
    const-wide/16 v14, 0x0

    #@31
    cmp-long v11, v3, v14

    #@33
    if-nez v11, :cond_39

    #@35
    .line 880
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@38
    move-result-wide v3

    #@39
    .line 882
    :cond_39
    const-wide/16 v14, 0x0

    #@3b
    cmp-long v11, v1, v14

    #@3d
    if-nez v11, :cond_40

    #@3f
    .line 883
    move-wide v1, v3

    #@40
    .line 885
    :cond_40
    new-instance v0, Landroid/view/KeyEvent;

    #@42
    or-int/lit8 v11, v13, 0x8

    #@44
    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@47
    .line 887
    .local v0, newEvent:Landroid/view/KeyEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@4a
    move-result-object v11

    #@4b
    const/4 v14, 0x2

    #@4c
    invoke-virtual {v11, v0, v14}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@4f
    .line 889
    return-void
.end method

.method public sendPointerSync(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 926
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@3
    .line 927
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@6
    move-result v0

    #@7
    and-int/lit8 v0, v0, 0x2

    #@9
    if-nez v0, :cond_10

    #@b
    .line 928
    const/16 v0, 0x1002

    #@d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setSource(I)V

    #@10
    .line 930
    :cond_10
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@13
    move-result-object v0

    #@14
    const/4 v1, 0x2

    #@15
    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@18
    .line 932
    return-void
.end method

.method public sendStatus(ILandroid/os/Bundle;)V
    .registers 6
    .parameter "resultCode"
    .parameter "results"

    #@0
    .prologue
    .line 163
    iget-object v1, p0, Landroid/app/Instrumentation;->mWatcher:Landroid/app/IInstrumentationWatcher;

    #@2
    if-eqz v1, :cond_b

    #@4
    .line 165
    :try_start_4
    iget-object v1, p0, Landroid/app/Instrumentation;->mWatcher:Landroid/app/IInstrumentationWatcher;

    #@6
    iget-object v2, p0, Landroid/app/Instrumentation;->mComponent:Landroid/content/ComponentName;

    #@8
    invoke-interface {v1, v2, p1, p2}, Landroid/app/IInstrumentationWatcher;->instrumentationStatus(Landroid/content/ComponentName;ILandroid/os/Bundle;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_b} :catch_c

    #@b
    .line 171
    :cond_b
    :goto_b
    return-void

    #@c
    .line 167
    :catch_c
    move-exception v0

    #@d
    .line 168
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@e
    iput-object v1, p0, Landroid/app/Instrumentation;->mWatcher:Landroid/app/IInstrumentationWatcher;

    #@10
    goto :goto_b
.end method

.method public sendStringSync(Ljava/lang/String;)V
    .registers 9
    .parameter "text"

    #@0
    .prologue
    .line 835
    if-nez p1, :cond_3

    #@2
    .line 852
    :cond_2
    return-void

    #@3
    .line 838
    :cond_3
    const/4 v3, -0x1

    #@4
    invoke-static {v3}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    #@7
    move-result-object v2

    #@8
    .line 840
    .local v2, keyCharacterMap:Landroid/view/KeyCharacterMap;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v2, v3}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    #@f
    move-result-object v0

    #@10
    .line 842
    .local v0, events:[Landroid/view/KeyEvent;
    if-eqz v0, :cond_2

    #@12
    .line 843
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    array-length v3, v0

    #@14
    if-ge v1, v3, :cond_2

    #@16
    .line 849
    aget-object v3, v0, v1

    #@18
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1b
    move-result-wide v4

    #@1c
    const/4 v6, 0x0

    #@1d
    invoke-static {v3, v4, v5, v6}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JI)Landroid/view/KeyEvent;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {p0, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    #@24
    .line 843
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_13
.end method

.method public sendTrackballEventSync(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 946
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@3
    .line 947
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    #@6
    move-result v0

    #@7
    and-int/lit8 v0, v0, 0x4

    #@9
    if-nez v0, :cond_11

    #@b
    .line 948
    const v0, 0x10004

    #@e
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setSource(I)V

    #@11
    .line 950
    :cond_11
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@14
    move-result-object v0

    #@15
    const/4 v1, 0x2

    #@16
    invoke-virtual {v0, p1, v1}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@19
    .line 952
    return-void
.end method

.method public setAutomaticPerformanceSnapshots()V
    .registers 2

    #@0
    .prologue
    .line 193
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Instrumentation;->mAutomaticPerformanceSnapshots:Z

    #@3
    .line 194
    new-instance v0, Landroid/os/PerformanceCollector;

    #@5
    invoke-direct {v0}, Landroid/os/PerformanceCollector;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/Instrumentation;->mPerformanceCollector:Landroid/os/PerformanceCollector;

    #@a
    .line 195
    return-void
.end method

.method public setInTouchMode(Z)V
    .registers 3
    .parameter "inTouch"

    #@0
    .prologue
    .line 297
    :try_start_0
    const-string/jumbo v0, "window"

    #@3
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    #@a
    move-result-object v0

    #@b
    invoke-interface {v0, p1}, Landroid/view/IWindowManager;->setInTouchMode(Z)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 302
    :goto_e
    return-void

    #@f
    .line 299
    :catch_f
    move-exception v0

    #@10
    goto :goto_e
.end method

.method public start()V
    .registers 4

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/app/Instrumentation;->mRunner:Ljava/lang/Thread;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "Instrumentation already started"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 122
    :cond_c
    new-instance v0, Landroid/app/Instrumentation$InstrumentationThread;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Instr: "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, p0, v1}, Landroid/app/Instrumentation$InstrumentationThread;-><init>(Landroid/app/Instrumentation;Ljava/lang/String;)V

    #@2c
    iput-object v0, p0, Landroid/app/Instrumentation;->mRunner:Ljava/lang/Thread;

    #@2e
    .line 123
    iget-object v0, p0, Landroid/app/Instrumentation;->mRunner:Ljava/lang/Thread;

    #@30
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@33
    .line 124
    return-void
.end method

.method public startActivitySync(Landroid/content/Intent;)Landroid/app/Activity;
    .registers 10
    .parameter "intent"

    #@0
    .prologue
    .line 364
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@3
    .line 366
    iget-object v5, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@5
    monitor-enter v5

    #@6
    .line 367
    :try_start_6
    new-instance v2, Landroid/content/Intent;

    #@8
    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_af

    #@b
    .line 369
    .end local p1
    .local v2, intent:Landroid/content/Intent;
    :try_start_b
    invoke-virtual {p0}, Landroid/app/Instrumentation;->getTargetContext()Landroid/content/Context;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@12
    move-result-object v4

    #@13
    const/4 v6, 0x0

    #@14
    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@17
    move-result-object v0

    #@18
    .line 371
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_37

    #@1a
    .line 372
    new-instance v4, Ljava/lang/RuntimeException;

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "Unable to resolve activity for: "

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-direct {v4, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v4
    :try_end_33
    .catchall {:try_start_b .. :try_end_33} :catchall_33

    #@33
    .line 402
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    :catchall_33
    move-exception v4

    #@34
    move-object p1, v2

    #@35
    .end local v2           #intent:Landroid/content/Intent;
    .restart local p1
    :goto_35
    :try_start_35
    monitor-exit v5
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_af

    #@36
    throw v4

    #@37
    .line 374
    .end local p1
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_37
    :try_start_37
    iget-object v4, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@39
    invoke-virtual {v4}, Landroid/app/ActivityThread;->getProcessName()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    .line 375
    .local v3, myProc:Ljava/lang/String;
    iget-object v4, v0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@3f
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@42
    move-result v4

    #@43
    if-nez v4, :cond_74

    #@45
    .line 378
    new-instance v4, Ljava/lang/RuntimeException;

    #@47
    new-instance v6, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v7, "Intent in process "

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v6

    #@56
    const-string v7, " resolved to different process "

    #@58
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    iget-object v7, v0, Landroid/content/pm/ComponentInfo;->processName:Ljava/lang/String;

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    const-string v7, ": "

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v6

    #@70
    invoke-direct {v4, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@73
    throw v4

    #@74
    .line 383
    :cond_74
    new-instance v4, Landroid/content/ComponentName;

    #@76
    iget-object v6, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@78
    iget-object v6, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@7a
    iget-object v7, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@7c
    invoke-direct {v4, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@82
    .line 385
    new-instance v1, Landroid/app/Instrumentation$ActivityWaiter;

    #@84
    invoke-direct {v1, v2}, Landroid/app/Instrumentation$ActivityWaiter;-><init>(Landroid/content/Intent;)V

    #@87
    .line 387
    .local v1, aw:Landroid/app/Instrumentation$ActivityWaiter;
    iget-object v4, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@89
    if-nez v4, :cond_92

    #@8b
    .line 388
    new-instance v4, Ljava/util/ArrayList;

    #@8d
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@90
    iput-object v4, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@92
    .line 390
    :cond_92
    iget-object v4, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@94
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@97
    .line 392
    invoke-virtual {p0}, Landroid/app/Instrumentation;->getTargetContext()Landroid/content/Context;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_9e
    .catchall {:try_start_37 .. :try_end_9e} :catchall_33

    #@9e
    .line 396
    :cond_9e
    :try_start_9e
    iget-object v4, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@a0
    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_a3
    .catchall {:try_start_9e .. :try_end_a3} :catchall_33
    .catch Ljava/lang/InterruptedException; {:try_start_9e .. :try_end_a3} :catch_b1

    #@a3
    .line 399
    :goto_a3
    :try_start_a3
    iget-object v4, p0, Landroid/app/Instrumentation;->mWaitingActivities:Ljava/util/List;

    #@a5
    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@a8
    move-result v4

    #@a9
    if-nez v4, :cond_9e

    #@ab
    .line 401
    iget-object v4, v1, Landroid/app/Instrumentation$ActivityWaiter;->activity:Landroid/app/Activity;

    #@ad
    monitor-exit v5
    :try_end_ae
    .catchall {:try_start_a3 .. :try_end_ae} :catchall_33

    #@ae
    return-object v4

    #@af
    .line 402
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v1           #aw:Landroid/app/Instrumentation$ActivityWaiter;
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #myProc:Ljava/lang/String;
    .restart local p1
    :catchall_af
    move-exception v4

    #@b0
    goto :goto_35

    #@b1
    .line 397
    .end local p1
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v1       #aw:Landroid/app/Instrumentation$ActivityWaiter;
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v3       #myProc:Ljava/lang/String;
    :catch_b1
    move-exception v4

    #@b2
    goto :goto_a3
.end method

.method public startAllocCounting()V
    .registers 2

    #@0
    .prologue
    .line 1247
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@7
    .line 1248
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    #@e
    .line 1249
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@15
    .line 1251
    invoke-static {}, Landroid/os/Debug;->resetAllCounts()V

    #@18
    .line 1254
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    #@1b
    .line 1255
    return-void
.end method

.method public startPerformanceSnapshot()V
    .registers 3

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/app/Instrumentation;->isProfiling()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    .line 199
    iget-object v0, p0, Landroid/app/Instrumentation;->mPerformanceCollector:Landroid/os/PerformanceCollector;

    #@8
    const/4 v1, 0x0

    #@9
    invoke-virtual {v0, v1}, Landroid/os/PerformanceCollector;->beginSnapshot(Ljava/lang/String;)V

    #@c
    .line 201
    :cond_c
    return-void
.end method

.method public startProfiling()V
    .registers 4

    #@0
    .prologue
    .line 271
    iget-object v1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@2
    invoke-virtual {v1}, Landroid/app/ActivityThread;->isProfiling()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_23

    #@8
    .line 272
    new-instance v0, Ljava/io/File;

    #@a
    iget-object v1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@c
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getProfileFilePath()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@13
    .line 273
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    #@1a
    .line 274
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    const/high16 v2, 0x80

    #@20
    invoke-static {v1, v2}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;I)V

    #@23
    .line 276
    .end local v0           #file:Ljava/io/File;
    :cond_23
    return-void
.end method

.method public stopAllocCounting()V
    .registers 2

    #@0
    .prologue
    .line 1261
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@7
    .line 1262
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    #@e
    .line 1263
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@15
    .line 1264
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@18
    .line 1265
    return-void
.end method

.method public stopProfiling()V
    .registers 2

    #@0
    .prologue
    .line 282
    iget-object v0, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@2
    invoke-virtual {v0}, Landroid/app/ActivityThread;->isProfiling()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 283
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    #@b
    .line 285
    :cond_b
    return-void
.end method

.method public waitForIdle(Ljava/lang/Runnable;)V
    .registers 5
    .parameter "recipient"

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Landroid/app/Instrumentation;->mMessageQueue:Landroid/os/MessageQueue;

    #@2
    new-instance v1, Landroid/app/Instrumentation$Idler;

    #@4
    invoke-direct {v1, p1}, Landroid/app/Instrumentation$Idler;-><init>(Ljava/lang/Runnable;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@a
    .line 313
    iget-object v0, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@c
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@f
    move-result-object v0

    #@10
    new-instance v1, Landroid/app/Instrumentation$EmptyRunnable;

    #@12
    const/4 v2, 0x0

    #@13
    invoke-direct {v1, v2}, Landroid/app/Instrumentation$EmptyRunnable;-><init>(Landroid/app/Instrumentation$1;)V

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@19
    .line 314
    return-void
.end method

.method public waitForIdleSync()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 322
    invoke-direct {p0}, Landroid/app/Instrumentation;->validateNotAppThread()V

    #@4
    .line 323
    new-instance v0, Landroid/app/Instrumentation$Idler;

    #@6
    invoke-direct {v0, v3}, Landroid/app/Instrumentation$Idler;-><init>(Ljava/lang/Runnable;)V

    #@9
    .line 324
    .local v0, idler:Landroid/app/Instrumentation$Idler;
    iget-object v1, p0, Landroid/app/Instrumentation;->mMessageQueue:Landroid/os/MessageQueue;

    #@b
    invoke-virtual {v1, v0}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@e
    .line 325
    iget-object v1, p0, Landroid/app/Instrumentation;->mThread:Landroid/app/ActivityThread;

    #@10
    invoke-virtual {v1}, Landroid/app/ActivityThread;->getHandler()Landroid/os/Handler;

    #@13
    move-result-object v1

    #@14
    new-instance v2, Landroid/app/Instrumentation$EmptyRunnable;

    #@16
    invoke-direct {v2, v3}, Landroid/app/Instrumentation$EmptyRunnable;-><init>(Landroid/app/Instrumentation$1;)V

    #@19
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1c
    .line 326
    invoke-virtual {v0}, Landroid/app/Instrumentation$Idler;->waitForIdle()V

    #@1f
    .line 327
    return-void
.end method

.method public waitForMonitor(Landroid/app/Instrumentation$ActivityMonitor;)Landroid/app/Activity;
    .registers 5
    .parameter "monitor"

    #@0
    .prologue
    .line 687
    invoke-virtual {p1}, Landroid/app/Instrumentation$ActivityMonitor;->waitForActivity()Landroid/app/Activity;

    #@3
    move-result-object v0

    #@4
    .line 688
    .local v0, activity:Landroid/app/Activity;
    iget-object v2, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 689
    :try_start_7
    iget-object v1, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@9
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@c
    .line 690
    monitor-exit v2

    #@d
    .line 691
    return-object v0

    #@e
    .line 690
    :catchall_e
    move-exception v1

    #@f
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_e

    #@10
    throw v1
.end method

.method public waitForMonitorWithTimeout(Landroid/app/Instrumentation$ActivityMonitor;J)Landroid/app/Activity;
    .registers 7
    .parameter "monitor"
    .parameter "timeOut"

    #@0
    .prologue
    .line 706
    invoke-virtual {p1, p2, p3}, Landroid/app/Instrumentation$ActivityMonitor;->waitForActivityWithTimeout(J)Landroid/app/Activity;

    #@3
    move-result-object v0

    #@4
    .line 707
    .local v0, activity:Landroid/app/Activity;
    iget-object v2, p0, Landroid/app/Instrumentation;->mSync:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 708
    :try_start_7
    iget-object v1, p0, Landroid/app/Instrumentation;->mActivityMonitors:Ljava/util/List;

    #@9
    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@c
    .line 709
    monitor-exit v2

    #@d
    .line 710
    return-object v0

    #@e
    .line 709
    :catchall_e
    move-exception v1

    #@f
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_e

    #@10
    throw v1
.end method
