.class public Landroid/app/NativeActivity;
.super Landroid/app/Activity;
.source "NativeActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback2;
.implements Landroid/view/InputQueue$Callback;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/NativeActivity$InputMethodCallback;,
        Landroid/app/NativeActivity$NativeContentView;
    }
.end annotation


# static fields
.field private static final KEY_NATIVE_SAVED_STATE:Ljava/lang/String; = "android:native_state"

.field public static final META_DATA_FUNC_NAME:Ljava/lang/String; = "android.app.func_name"

.field public static final META_DATA_LIB_NAME:Ljava/lang/String; = "android.app.lib_name"


# instance fields
.field private mCurInputQueue:Landroid/view/InputQueue;

.field private mCurSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mDestroyed:Z

.field private mDispatchingUnhandledKey:Z

.field private mIMM:Landroid/view/inputmethod/InputMethodManager;

.field private mInputMethodCallback:Landroid/app/NativeActivity$InputMethodCallback;

.field mLastContentHeight:I

.field mLastContentWidth:I

.field mLastContentX:I

.field mLastContentY:I

.field final mLocation:[I

.field private mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

.field private mNativeHandle:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 75
    const/4 v0, 0x2

    #@4
    new-array v0, v0, [I

    #@6
    iput-object v0, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@8
    .line 121
    return-void
.end method

.method static synthetic access$000(Landroid/app/NativeActivity;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 47
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/app/NativeActivity;IIZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/app/NativeActivity;->finishPreDispatchKeyEventNative(IIZ)V

    #@3
    return-void
.end method

.method private native dispatchKeyEventNative(ILandroid/view/KeyEvent;)V
.end method

.method private native finishPreDispatchKeyEventNative(IIZ)V
.end method

.method private native loadNativeCode(Ljava/lang/String;Ljava/lang/String;Landroid/os/MessageQueue;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/res/AssetManager;[B)I
.end method

.method private native onConfigurationChangedNative(I)V
.end method

.method private native onContentRectChangedNative(IIIII)V
.end method

.method private native onInputChannelCreatedNative(ILandroid/view/InputChannel;)V
.end method

.method private native onInputChannelDestroyedNative(ILandroid/view/InputChannel;)V
.end method

.method private native onLowMemoryNative(I)V
.end method

.method private native onPauseNative(I)V
.end method

.method private native onResumeNative(I)V
.end method

.method private native onSaveInstanceStateNative(I)[B
.end method

.method private native onStartNative(I)V
.end method

.method private native onStopNative(I)V
.end method

.method private native onSurfaceChangedNative(ILandroid/view/Surface;III)V
.end method

.method private native onSurfaceCreatedNative(ILandroid/view/Surface;)V
.end method

.method private native onSurfaceDestroyedNative(I)V
.end method

.method private native onSurfaceRedrawNeededNative(ILandroid/view/Surface;)V
.end method

.method private native onWindowFocusChangedNative(IZ)V
.end method

.method private native unloadNativeCode(I)V
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 272
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDispatchingUnhandledKey:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 273
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@7
    move-result v0

    #@8
    .line 278
    :goto_8
    return v0

    #@9
    .line 277
    :cond_9
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@b
    invoke-direct {p0, v0, p1}, Landroid/app/NativeActivity;->dispatchKeyEventNative(ILandroid/view/KeyEvent;)V

    #@e
    .line 278
    const/4 v0, 0x1

    #@f
    goto :goto_8
.end method

.method dispatchUnhandledKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 343
    const/4 v1, 0x1

    #@2
    :try_start_2
    iput-boolean v1, p0, Landroid/app/NativeActivity;->mDispatchingUnhandledKey:Z

    #@4
    .line 344
    invoke-virtual {p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@b
    move-result-object v0

    #@c
    .line 345
    .local v0, decor:Landroid/view/View;
    if-eqz v0, :cond_15

    #@e
    .line 346
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    :try_end_11
    .catchall {:try_start_2 .. :try_end_11} :catchall_19

    #@11
    move-result v1

    #@12
    .line 351
    iput-boolean v2, p0, Landroid/app/NativeActivity;->mDispatchingUnhandledKey:Z

    #@14
    .line 348
    :goto_14
    return v1

    #@15
    .line 351
    :cond_15
    iput-boolean v2, p0, Landroid/app/NativeActivity;->mDispatchingUnhandledKey:Z

    #@17
    move v1, v2

    #@18
    .line 348
    goto :goto_14

    #@19
    .line 351
    .end local v0           #decor:Landroid/view/View;
    :catchall_19
    move-exception v1

    #@1a
    iput-boolean v2, p0, Landroid/app/NativeActivity;->mDispatchingUnhandledKey:Z

    #@1c
    throw v1
.end method

.method hideIme(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 373
    iget-object v0, p0, Landroid/app/NativeActivity;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@4
    invoke-virtual {v1}, Landroid/app/NativeActivity$NativeContentView;->getWindowToken()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1, p1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    #@b
    .line 374
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 248
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 249
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 250
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@9
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onConfigurationChangedNative(I)V

    #@c
    .line 252
    :cond_c
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 18
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 139
    const-string/jumbo v13, "main"

    #@3
    .line 140
    .local v13, libname:Ljava/lang/String;
    const-string v3, "ANativeActivity_onCreate"

    #@5
    .line 143
    .local v3, funcname:Ljava/lang/String;
    const-string v1, "input_method"

    #@7
    move-object/from16 v0, p0

    #@9
    invoke-virtual {v0, v1}, Landroid/app/NativeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    #@f
    move-object/from16 v0, p0

    #@11
    iput-object v1, v0, Landroid/app/NativeActivity;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@13
    .line 144
    new-instance v1, Landroid/app/NativeActivity$InputMethodCallback;

    #@15
    move-object/from16 v0, p0

    #@17
    invoke-direct {v1, v0}, Landroid/app/NativeActivity$InputMethodCallback;-><init>(Landroid/app/NativeActivity;)V

    #@1a
    move-object/from16 v0, p0

    #@1c
    iput-object v1, v0, Landroid/app/NativeActivity;->mInputMethodCallback:Landroid/app/NativeActivity$InputMethodCallback;

    #@1e
    .line 146
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@21
    move-result-object v1

    #@22
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v1, v0}, Landroid/view/Window;->takeSurface(Landroid/view/SurfaceHolder$Callback2;)V

    #@27
    .line 147
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@2a
    move-result-object v1

    #@2b
    move-object/from16 v0, p0

    #@2d
    invoke-virtual {v1, v0}, Landroid/view/Window;->takeInputQueue(Landroid/view/InputQueue$Callback;)V

    #@30
    .line 148
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@33
    move-result-object v1

    #@34
    const/4 v4, 0x4

    #@35
    invoke-virtual {v1, v4}, Landroid/view/Window;->setFormat(I)V

    #@38
    .line 149
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@3b
    move-result-object v1

    #@3c
    const/16 v4, 0x10

    #@3e
    invoke-virtual {v1, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    #@41
    .line 153
    new-instance v1, Landroid/app/NativeActivity$NativeContentView;

    #@43
    move-object/from16 v0, p0

    #@45
    invoke-direct {v1, v0}, Landroid/app/NativeActivity$NativeContentView;-><init>(Landroid/content/Context;)V

    #@48
    move-object/from16 v0, p0

    #@4a
    iput-object v1, v0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@4c
    .line 154
    move-object/from16 v0, p0

    #@4e
    iget-object v1, v0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@50
    move-object/from16 v0, p0

    #@52
    iput-object v0, v1, Landroid/app/NativeActivity$NativeContentView;->mActivity:Landroid/app/NativeActivity;

    #@54
    .line 155
    move-object/from16 v0, p0

    #@56
    iget-object v1, v0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@58
    move-object/from16 v0, p0

    #@5a
    invoke-virtual {v0, v1}, Landroid/app/NativeActivity;->setContentView(Landroid/view/View;)V

    #@5d
    .line 156
    move-object/from16 v0, p0

    #@5f
    iget-object v1, v0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@61
    invoke-virtual {v1}, Landroid/app/NativeActivity$NativeContentView;->requestFocus()Z

    #@64
    .line 157
    move-object/from16 v0, p0

    #@66
    iget-object v1, v0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@68
    invoke-virtual {v1}, Landroid/app/NativeActivity$NativeContentView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    #@6b
    move-result-object v1

    #@6c
    move-object/from16 v0, p0

    #@6e
    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    #@71
    .line 160
    :try_start_71
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@74
    move-result-object v1

    #@75
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getIntent()Landroid/content/Intent;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    #@7c
    move-result-object v4

    #@7d
    const/16 v5, 0x80

    #@7f
    invoke-virtual {v1, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@82
    move-result-object v11

    #@83
    .line 162
    .local v11, ai:Landroid/content/pm/ActivityInfo;
    iget-object v1, v11, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@85
    if-eqz v1, :cond_9d

    #@87
    .line 163
    iget-object v1, v11, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@89
    const-string v4, "android.app.lib_name"

    #@8b
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@8e
    move-result-object v15

    #@8f
    .line 164
    .local v15, ln:Ljava/lang/String;
    if-eqz v15, :cond_92

    #@91
    move-object v13, v15

    #@92
    .line 165
    :cond_92
    iget-object v1, v11, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@94
    const-string v4, "android.app.func_name"

    #@96
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_99
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_71 .. :try_end_99} :catch_d0

    #@99
    move-result-object v15

    #@9a
    .line 166
    if-eqz v15, :cond_9d

    #@9c
    move-object v3, v15

    #@9d
    .line 172
    .end local v15           #ln:Ljava/lang/String;
    :cond_9d
    const/4 v2, 0x0

    #@9e
    .line 174
    .local v2, path:Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    #@a0
    iget-object v1, v11, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a2
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@a4
    invoke-static {v13}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    #@a7
    move-result-object v4

    #@a8
    invoke-direct {v14, v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@ab
    .line 176
    .local v14, libraryFile:Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    #@ae
    move-result v1

    #@af
    if-eqz v1, :cond_b5

    #@b1
    .line 177
    invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b4
    move-result-object v2

    #@b5
    .line 180
    :cond_b5
    if-nez v2, :cond_d9

    #@b7
    .line 181
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@b9
    new-instance v4, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v5, "Unable to find native library: "

    #@c0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v4

    #@c4
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v4

    #@cc
    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@cf
    throw v1

    #@d0
    .line 168
    .end local v2           #path:Ljava/lang/String;
    .end local v11           #ai:Landroid/content/pm/ActivityInfo;
    .end local v14           #libraryFile:Ljava/io/File;
    :catch_d0
    move-exception v12

    #@d1
    .line 169
    .local v12, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@d3
    const-string v4, "Error getting activity info"

    #@d5
    invoke-direct {v1, v4, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@d8
    throw v1

    #@d9
    .line 184
    .end local v12           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2       #path:Ljava/lang/String;
    .restart local v11       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v14       #libraryFile:Ljava/io/File;
    :cond_d9
    if-eqz p1, :cond_130

    #@db
    const-string v1, "android:native_state"

    #@dd
    move-object/from16 v0, p1

    #@df
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@e2
    move-result-object v10

    #@e3
    .line 187
    .local v10, nativeSavedState:[B
    :goto_e3
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    #@e6
    move-result-object v4

    #@e7
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getFilesDir()Ljava/io/File;

    #@ea
    move-result-object v1

    #@eb
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getObbDir()Ljava/io/File;

    #@f2
    move-result-object v1

    #@f3
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    #@f6
    move-result-object v6

    #@f7
    iget-object v1, v11, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@f9
    invoke-static {v1}, Landroid/os/Environment;->getExternalStorageAppFilesDirectory(Ljava/lang/String;)Ljava/io/File;

    #@fc
    move-result-object v1

    #@fd
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    #@100
    move-result-object v7

    #@101
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    #@103
    invoke-virtual/range {p0 .. p0}, Landroid/app/NativeActivity;->getAssets()Landroid/content/res/AssetManager;

    #@106
    move-result-object v9

    #@107
    move-object/from16 v1, p0

    #@109
    invoke-direct/range {v1 .. v10}, Landroid/app/NativeActivity;->loadNativeCode(Ljava/lang/String;Ljava/lang/String;Landroid/os/MessageQueue;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/res/AssetManager;[B)I

    #@10c
    move-result v1

    #@10d
    move-object/from16 v0, p0

    #@10f
    iput v1, v0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@111
    .line 192
    move-object/from16 v0, p0

    #@113
    iget v1, v0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@115
    if-nez v1, :cond_132

    #@117
    .line 193
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@119
    new-instance v4, Ljava/lang/StringBuilder;

    #@11b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11e
    const-string v5, "Unable to load native library: "

    #@120
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@123
    move-result-object v4

    #@124
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v4

    #@128
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12b
    move-result-object v4

    #@12c
    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@12f
    throw v1

    #@130
    .line 184
    .end local v10           #nativeSavedState:[B
    :cond_130
    const/4 v10, 0x0

    #@131
    goto :goto_e3

    #@132
    .line 195
    .restart local v10       #nativeSavedState:[B
    :cond_132
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@135
    .line 196
    return-void
.end method

.method protected onDestroy()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 200
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@4
    .line 201
    iget-object v0, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 202
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@a
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onSurfaceDestroyedNative(I)V

    #@d
    .line 203
    iput-object v2, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@f
    .line 205
    :cond_f
    iget-object v0, p0, Landroid/app/NativeActivity;->mCurInputQueue:Landroid/view/InputQueue;

    #@11
    if-eqz v0, :cond_20

    #@13
    .line 206
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@15
    iget-object v1, p0, Landroid/app/NativeActivity;->mCurInputQueue:Landroid/view/InputQueue;

    #@17
    invoke-virtual {v1}, Landroid/view/InputQueue;->getInputChannel()Landroid/view/InputChannel;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {p0, v0, v1}, Landroid/app/NativeActivity;->onInputChannelDestroyedNative(ILandroid/view/InputChannel;)V

    #@1e
    .line 207
    iput-object v2, p0, Landroid/app/NativeActivity;->mCurInputQueue:Landroid/view/InputQueue;

    #@20
    .line 209
    :cond_20
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@22
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->unloadNativeCode(I)V

    #@25
    .line 210
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    #@28
    .line 211
    return-void
.end method

.method public onGlobalLayout()V
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 325
    iget-object v0, p0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@4
    iget-object v1, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@6
    invoke-virtual {v0, v1}, Landroid/app/NativeActivity$NativeContentView;->getLocationInWindow([I)V

    #@9
    .line 326
    iget-object v0, p0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@b
    invoke-virtual {v0}, Landroid/app/NativeActivity$NativeContentView;->getWidth()I

    #@e
    move-result v7

    #@f
    .line 327
    .local v7, w:I
    iget-object v0, p0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@11
    invoke-virtual {v0}, Landroid/app/NativeActivity$NativeContentView;->getHeight()I

    #@14
    move-result v6

    #@15
    .line 328
    .local v6, h:I
    iget-object v0, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@17
    aget v0, v0, v2

    #@19
    iget v1, p0, Landroid/app/NativeActivity;->mLastContentX:I

    #@1b
    if-ne v0, v1, :cond_2d

    #@1d
    iget-object v0, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@1f
    aget v0, v0, v3

    #@21
    iget v1, p0, Landroid/app/NativeActivity;->mLastContentY:I

    #@23
    if-ne v0, v1, :cond_2d

    #@25
    iget v0, p0, Landroid/app/NativeActivity;->mLastContentWidth:I

    #@27
    if-ne v7, v0, :cond_2d

    #@29
    iget v0, p0, Landroid/app/NativeActivity;->mLastContentHeight:I

    #@2b
    if-eq v6, v0, :cond_4f

    #@2d
    .line 330
    :cond_2d
    iget-object v0, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@2f
    aget v0, v0, v2

    #@31
    iput v0, p0, Landroid/app/NativeActivity;->mLastContentX:I

    #@33
    .line 331
    iget-object v0, p0, Landroid/app/NativeActivity;->mLocation:[I

    #@35
    aget v0, v0, v3

    #@37
    iput v0, p0, Landroid/app/NativeActivity;->mLastContentY:I

    #@39
    .line 332
    iput v7, p0, Landroid/app/NativeActivity;->mLastContentWidth:I

    #@3b
    .line 333
    iput v6, p0, Landroid/app/NativeActivity;->mLastContentHeight:I

    #@3d
    .line 334
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@3f
    if-nez v0, :cond_4f

    #@41
    .line 335
    iget v1, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@43
    iget v2, p0, Landroid/app/NativeActivity;->mLastContentX:I

    #@45
    iget v3, p0, Landroid/app/NativeActivity;->mLastContentY:I

    #@47
    iget v4, p0, Landroid/app/NativeActivity;->mLastContentWidth:I

    #@49
    iget v5, p0, Landroid/app/NativeActivity;->mLastContentHeight:I

    #@4b
    move-object v0, p0

    #@4c
    invoke-direct/range {v0 .. v5}, Landroid/app/NativeActivity;->onContentRectChangedNative(IIIII)V

    #@4f
    .line 339
    :cond_4f
    return-void
.end method

.method public onInputQueueCreated(Landroid/view/InputQueue;)V
    .registers 4
    .parameter "queue"

    #@0
    .prologue
    .line 311
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@2
    if-nez v0, :cond_f

    #@4
    .line 312
    iput-object p1, p0, Landroid/app/NativeActivity;->mCurInputQueue:Landroid/view/InputQueue;

    #@6
    .line 313
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@8
    invoke-virtual {p1}, Landroid/view/InputQueue;->getInputChannel()Landroid/view/InputChannel;

    #@b
    move-result-object v1

    #@c
    invoke-direct {p0, v0, v1}, Landroid/app/NativeActivity;->onInputChannelCreatedNative(ILandroid/view/InputChannel;)V

    #@f
    .line 315
    :cond_f
    return-void
.end method

.method public onInputQueueDestroyed(Landroid/view/InputQueue;)V
    .registers 4
    .parameter "queue"

    #@0
    .prologue
    .line 318
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/app/NativeActivity;->mCurInputQueue:Landroid/view/InputQueue;

    #@3
    .line 319
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@5
    if-nez v0, :cond_10

    #@7
    .line 320
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@9
    invoke-virtual {p1}, Landroid/view/InputQueue;->getInputChannel()Landroid/view/InputChannel;

    #@c
    move-result-object v1

    #@d
    invoke-direct {p0, v0, v1}, Landroid/app/NativeActivity;->onInputChannelDestroyedNative(ILandroid/view/InputChannel;)V

    #@10
    .line 322
    :cond_10
    return-void
.end method

.method public onLowMemory()V
    .registers 2

    #@0
    .prologue
    .line 256
    invoke-super {p0}, Landroid/app/Activity;->onLowMemory()V

    #@3
    .line 257
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 258
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@9
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onLowMemoryNative(I)V

    #@c
    .line 260
    :cond_c
    return-void
.end method

.method protected onPause()V
    .registers 2

    #@0
    .prologue
    .line 215
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    #@3
    .line 216
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@5
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onPauseNative(I)V

    #@8
    .line 217
    return-void
.end method

.method protected onResume()V
    .registers 2

    #@0
    .prologue
    .line 221
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    #@3
    .line 222
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@5
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onResumeNative(I)V

    #@8
    .line 223
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    #@0
    .prologue
    .line 227
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 228
    iget v1, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@5
    invoke-direct {p0, v1}, Landroid/app/NativeActivity;->onSaveInstanceStateNative(I)[B

    #@8
    move-result-object v0

    #@9
    .line 229
    .local v0, state:[B
    if-eqz v0, :cond_10

    #@b
    .line 230
    const-string v1, "android:native_state"

    #@d
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    #@10
    .line 232
    :cond_10
    return-void
.end method

.method protected onStart()V
    .registers 2

    #@0
    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    #@3
    .line 237
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@5
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onStartNative(I)V

    #@8
    .line 238
    return-void
.end method

.method protected onStop()V
    .registers 2

    #@0
    .prologue
    .line 242
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    #@3
    .line 243
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@5
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onStopNative(I)V

    #@8
    .line 244
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3
    .parameter "hasFocus"

    #@0
    .prologue
    .line 264
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    #@3
    .line 265
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 266
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@9
    invoke-direct {p0, v0, p1}, Landroid/app/NativeActivity;->onWindowFocusChangedNative(IZ)V

    #@c
    .line 268
    :cond_c
    return-void
.end method

.method preDispatchKeyEvent(Landroid/view/KeyEvent;I)V
    .registers 5
    .parameter "event"
    .parameter "seq"

    #@0
    .prologue
    .line 356
    iget-object v0, p0, Landroid/app/NativeActivity;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/app/NativeActivity;->mInputMethodCallback:Landroid/app/NativeActivity$InputMethodCallback;

    #@4
    invoke-virtual {v0, p0, p2, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->dispatchKeyEvent(Landroid/content/Context;ILandroid/view/KeyEvent;Landroid/view/inputmethod/InputMethodManager$FinishedEventCallback;)V

    #@7
    .line 358
    return-void
.end method

.method setWindowFlags(II)V
    .registers 4
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 361
    invoke-virtual {p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFlags(II)V

    #@7
    .line 362
    return-void
.end method

.method setWindowFormat(I)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 365
    invoke-virtual {p0}, Landroid/app/NativeActivity;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setFormat(I)V

    #@7
    .line 366
    return-void
.end method

.method showIme(I)V
    .registers 4
    .parameter "mode"

    #@0
    .prologue
    .line 369
    iget-object v0, p0, Landroid/app/NativeActivity;->mIMM:Landroid/view/inputmethod/InputMethodManager;

    #@2
    iget-object v1, p0, Landroid/app/NativeActivity;->mNativeContentView:Landroid/app/NativeActivity$NativeContentView;

    #@4
    invoke-virtual {v0, v1, p1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    #@7
    .line 370
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 11
    .parameter "holder"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 290
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@2
    if-nez v0, :cond_13

    #@4
    .line 291
    iput-object p1, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@6
    .line 292
    iget v1, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@8
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@b
    move-result-object v2

    #@c
    move-object v0, p0

    #@d
    move v3, p2

    #@e
    move v4, p3

    #@f
    move v5, p4

    #@10
    invoke-direct/range {v0 .. v5}, Landroid/app/NativeActivity;->onSurfaceChangedNative(ILandroid/view/Surface;III)V

    #@13
    .line 294
    :cond_13
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 4
    .parameter "holder"

    #@0
    .prologue
    .line 283
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@2
    if-nez v0, :cond_f

    #@4
    .line 284
    iput-object p1, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@6
    .line 285
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@8
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@b
    move-result-object v1

    #@c
    invoke-direct {p0, v0, v1}, Landroid/app/NativeActivity;->onSurfaceCreatedNative(ILandroid/view/Surface;)V

    #@f
    .line 287
    :cond_f
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 304
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@3
    .line 305
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@5
    if-nez v0, :cond_c

    #@7
    .line 306
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@9
    invoke-direct {p0, v0}, Landroid/app/NativeActivity;->onSurfaceDestroyedNative(I)V

    #@c
    .line 308
    :cond_c
    return-void
.end method

.method public surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    .registers 4
    .parameter "holder"

    #@0
    .prologue
    .line 297
    iget-boolean v0, p0, Landroid/app/NativeActivity;->mDestroyed:Z

    #@2
    if-nez v0, :cond_f

    #@4
    .line 298
    iput-object p1, p0, Landroid/app/NativeActivity;->mCurSurfaceHolder:Landroid/view/SurfaceHolder;

    #@6
    .line 299
    iget v0, p0, Landroid/app/NativeActivity;->mNativeHandle:I

    #@8
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@b
    move-result-object v1

    #@c
    invoke-direct {p0, v0, v1}, Landroid/app/NativeActivity;->onSurfaceRedrawNeededNative(ILandroid/view/Surface;)V

    #@f
    .line 301
    :cond_f
    return-void
.end method
