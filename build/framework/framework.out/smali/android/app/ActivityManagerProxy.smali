.class Landroid/app/ActivityManagerProxy;
.super Ljava/lang/Object;
.source "ActivityManagerNative.java"

# interfaces
.implements Landroid/app/IActivityManager;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 1912
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1913
    iput-object p1, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 1914
    return-void
.end method


# virtual methods
.method public activityDestroyed(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2342
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2343
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2344
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2345
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2346
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x3e

    #@14
    const/4 v4, 0x1

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2347
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2349
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2350
    return-void
.end method

.method public activityIdle(Landroid/os/IBinder;Landroid/content/res/Configuration;Z)V
    .registers 9
    .parameter "token"
    .parameter "config"
    .parameter "stopProfiling"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 2271
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2272
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2273
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2274
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2275
    if-eqz p2, :cond_31

    #@14
    .line 2276
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2277
    invoke-virtual {p2, v0, v2}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2281
    :goto_1a
    if-eqz p3, :cond_1d

    #@1c
    move v2, v3

    #@1d
    :cond_1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 2282
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v4, 0x12

    #@24
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 2283
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 2284
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2285
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2286
    return-void

    #@31
    .line 2279
    :cond_31
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    goto :goto_1a
.end method

.method public activityPaused(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2300
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2301
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2302
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2303
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2304
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x13

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2305
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2306
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2307
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2308
    return-void
.end method

.method public activityResumed(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2290
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2291
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2292
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2293
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x27

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2294
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2296
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2297
    return-void
.end method

.method public activitySlept(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2331
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2332
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2333
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2334
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2335
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x7b

    #@14
    const/4 v4, 0x1

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2336
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2337
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2338
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2339
    return-void
.end method

.method public activityStopped(Landroid/os/IBinder;Landroid/os/Bundle;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .registers 10
    .parameter "token"
    .parameter "state"
    .parameter "thumbnail"
    .parameter "description"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2312
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2313
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2314
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2315
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2316
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@15
    .line 2317
    if-eqz p3, :cond_31

    #@17
    .line 2318
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2319
    invoke-virtual {p3, v0, v3}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 2323
    :goto_1d
    invoke-static {p4, v0, v3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@20
    .line 2324
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v3, 0x14

    #@24
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 2325
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 2326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2327
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2328
    return-void

    #@31
    .line 2321
    :cond_31
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    goto :goto_1d
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 1918
    iget-object v0, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public attachApplication(Landroid/app/IApplicationThread;)V
    .registers 7
    .parameter "app"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2260
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2261
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2262
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 2263
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0x11

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 2264
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 2265
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2266
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2267
    return-void
.end method

.method public backupAgentCreated(Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 8
    .parameter "packageName"
    .parameter "agent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2901
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2902
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2903
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2904
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 2905
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 2906
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x5b

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2907
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2908
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2909
    return-void
.end method

.method public bindBackupAgent(Landroid/content/pm/ApplicationInfo;I)Z
    .registers 8
    .parameter "app"
    .parameter "backupRestoreMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2878
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2879
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2880
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2881
    invoke-virtual {p1, v0, v2}, Landroid/content/pm/ApplicationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 2882
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 2883
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x5a

    #@18
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2884
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 2885
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 2886
    .local v2, success:Z
    :cond_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 2887
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 2888
    return v2
.end method

.method public bindService(Landroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I
    .registers 14
    .parameter "caller"
    .parameter "token"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "connection"
    .parameter "flags"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2788
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2789
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2790
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2791
    if-eqz p1, :cond_42

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2792
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1a
    .line 2793
    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 2794
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@20
    .line 2795
    invoke-interface {p5}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@27
    .line 2796
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 2797
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 2798
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/16 v4, 0x24

    #@31
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@34
    .line 2799
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@37
    .line 2800
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v2

    #@3b
    .line 2801
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 2802
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 2803
    return v2

    #@42
    .line 2791
    .end local v2           #res:I
    :cond_42
    const/4 v3, 0x0

    #@43
    goto :goto_14
.end method

.method public bootAniEnd()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4224
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4225
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4226
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4227
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xa2

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 4228
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 4229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 4230
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 4231
    return-void
.end method

.method public broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    .registers 19
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "map"
    .parameter "requiredPermission"
    .parameter "serialized"
    .parameter "sticky"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2207
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2208
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2209
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2210
    if-eqz p1, :cond_59

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2211
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2212
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 2213
    if-eqz p4, :cond_5b

    #@1f
    invoke-interface {p4}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@22
    move-result-object v4

    #@23
    :goto_23
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@26
    .line 2214
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 2215
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2c
    .line 2216
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@2f
    .line 2217
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 2218
    if-eqz p9, :cond_5d

    #@34
    const/4 v4, 0x1

    #@35
    :goto_35
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 2219
    if-eqz p10, :cond_5f

    #@3a
    const/4 v4, 0x1

    #@3b
    :goto_3b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 2220
    move/from16 v0, p11

    #@40
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 2221
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@45
    const/16 v5, 0xe

    #@47
    const/4 v6, 0x0

    #@48
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@4b
    .line 2222
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@4e
    .line 2223
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v3

    #@52
    .line 2224
    .local v3, res:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@55
    .line 2225
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@58
    .line 2226
    return v3

    #@59
    .line 2210
    .end local v3           #res:I
    :cond_59
    const/4 v4, 0x0

    #@5a
    goto :goto_13

    #@5b
    .line 2213
    :cond_5b
    const/4 v4, 0x0

    #@5c
    goto :goto_23

    #@5d
    .line 2218
    :cond_5d
    const/4 v4, 0x0

    #@5e
    goto :goto_35

    #@5f
    .line 2219
    :cond_5f
    const/4 v4, 0x0

    #@60
    goto :goto_3b
.end method

.method public callOnActivityResultToDroppedActivity(ILandroid/content/ClipData;)Z
    .registers 8
    .parameter "screenId"
    .parameter "clip"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4278
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4279
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4280
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4281
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 4282
    invoke-virtual {p2, v0, v2}, Landroid/content/ClipData;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 4283
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0xae

    #@18
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 4285
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 4286
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 4287
    .local v2, result:Z
    :cond_25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 4288
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 4289
    return v2
.end method

.method public cancelIntentSender(Landroid/content/IIntentSender;)V
    .registers 7
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3064
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3065
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3066
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3067
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 3068
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0x40

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 3069
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 3070
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3071
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3072
    return-void
.end method

.method public checkGrantUriPermission(ILjava/lang/String;Landroid/net/Uri;I)I
    .registers 11
    .parameter "callingUid"
    .parameter "targetPkg"
    .parameter "uri"
    .parameter "modeFlags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3769
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3770
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3771
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3772
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 3773
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 3774
    invoke-virtual {p3, v0, v5}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 3775
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 3776
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v4, 0x77

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 3777
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 3778
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v2

    #@28
    .line 3779
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3780
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 3781
    return v2
.end method

.method public checkPermission(Ljava/lang/String;II)I
    .registers 10
    .parameter "permission"
    .parameter "pid"
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3154
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3155
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3156
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3157
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3158
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3159
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 3160
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0x35

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3161
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3162
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v2

    #@25
    .line 3163
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3164
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3165
    return v2
.end method

.method public checkUriPermission(Landroid/net/Uri;III)I
    .registers 11
    .parameter "uri"
    .parameter "pid"
    .parameter "uid"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3184
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3185
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3186
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3187
    invoke-virtual {p1, v0, v5}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 3188
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3189
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 3190
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 3191
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v4, 0x36

    #@1e
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 3192
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 3193
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v2

    #@28
    .line 3194
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3195
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 3196
    return v2
.end method

.method public clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z
    .registers 9
    .parameter "packageName"
    .parameter "observer"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3169
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3170
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3171
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3172
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 3173
    invoke-interface {p2}, Landroid/content/pm/IPackageDataObserver;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 3174
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3175
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v4, 0x4e

    #@1f
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 3176
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@25
    .line 3177
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v3

    #@29
    if-eqz v3, :cond_2c

    #@2b
    const/4 v2, 0x1

    #@2c
    .line 3178
    .local v2, res:Z
    :cond_2c
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 3179
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 3180
    return v2
.end method

.method public clearPendingBackup()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2892
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2893
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2894
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2895
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0xa0

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 2896
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@18
    .line 2897
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 2898
    return-void
.end method

.method public closeSystemDialogs(Ljava/lang/String;)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3576
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3577
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3578
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3579
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3580
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x61

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3581
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3582
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3583
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3584
    return-void
.end method

.method public crashApplication(IILjava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "uid"
    .parameter "initialPid"
    .parameter "packageName"
    .parameter "message"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3691
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3692
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3693
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3694
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3695
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3696
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 3697
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 3698
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v3, 0x72

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 3699
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 3700
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3701
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3702
    return-void
.end method

.method public dismissKeyguardOnNextActivity()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4100
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4101
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4102
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4103
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x8b

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 4104
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 4105
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 4106
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 4107
    return-void
.end method

.method public dumpHeap(Ljava/lang/String;IZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
    .registers 13
    .parameter "process"
    .parameter "userId"
    .parameter "managed"
    .parameter "path"
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 3786
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3787
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3788
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3789
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 3790
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 3791
    if-eqz p3, :cond_3e

    #@17
    move v3, v4

    #@18
    :goto_18
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3792
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1e
    .line 3793
    if-eqz p5, :cond_40

    #@20
    .line 3794
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 3795
    invoke-virtual {p5, v0, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@26
    .line 3799
    :goto_26
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/16 v6, 0x78

    #@2a
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 3800
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 3801
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_44

    #@36
    move v2, v4

    #@37
    .line 3802
    .local v2, res:Z
    :goto_37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 3803
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 3804
    return v2

    #@3e
    .end local v2           #res:Z
    :cond_3e
    move v3, v5

    #@3f
    .line 3791
    goto :goto_18

    #@40
    .line 3797
    :cond_40
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    goto :goto_26

    #@44
    :cond_44
    move v2, v5

    #@45
    .line 3801
    goto :goto_37
.end method

.method public enterSafeMode()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3330
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3331
    .local v0, data:Landroid/os/Parcel;
    const-string v1, "android.app.IActivityManager"

    #@6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 3332
    iget-object v1, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@b
    const/16 v2, 0x42

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x0

    #@f
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@12
    .line 3333
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@15
    .line 3334
    return-void
.end method

.method public exitSplitWindow(Z)Z
    .registers 9
    .parameter "bRemoveTop"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 4236
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 4237
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 4238
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 4239
    if-eqz p1, :cond_2d

    #@11
    move v3, v4

    #@12
    :goto_12
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 4240
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v6, 0xab

    #@19
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 4241
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 4242
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_2f

    #@25
    move v2, v4

    #@26
    .line 4243
    .local v2, result:Z
    :goto_26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 4244
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 4245
    return v2

    #@2d
    .end local v2           #result:Z
    :cond_2d
    move v3, v5

    #@2e
    .line 4239
    goto :goto_12

    #@2f
    :cond_2f
    move v2, v5

    #@30
    .line 4242
    goto :goto_26
.end method

.method public finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Z
    .registers 10
    .parameter "token"
    .parameter "resultCode"
    .parameter "resultData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2112
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2113
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2114
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2115
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 2116
    if-eqz p3, :cond_34

    #@17
    .line 2117
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2118
    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 2122
    :goto_1d
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v5, 0xb

    #@21
    invoke-interface {v4, v5, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2123
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2124
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_38

    #@2d
    .line 2125
    .local v2, res:Z
    :goto_2d
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2126
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 2127
    return v2

    #@34
    .line 2120
    .end local v2           #res:Z
    :cond_34
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    goto :goto_1d

    #@38
    :cond_38
    move v2, v3

    #@39
    .line 2124
    goto :goto_2d
.end method

.method public finishActivityAffinity(Landroid/os/IBinder;)Z
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2143
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2144
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2145
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2146
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2147
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x95

    #@15
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2148
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2149
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 2150
    .local v2, res:Z
    :cond_22
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2151
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 2152
    return v2
.end method

.method public finishHeavyWeightApp()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3640
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3641
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3642
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3643
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x6d

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3644
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3645
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3646
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3647
    return-void
.end method

.method public finishInstrumentation(Landroid/app/IApplicationThread;ILandroid/os/Bundle;)V
    .registers 9
    .parameter "target"
    .parameter "resultCode"
    .parameter "results"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2944
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2945
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2946
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2947
    if-eqz p1, :cond_2e

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2948
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 2949
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@1c
    .line 2950
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v3, 0x2d

    #@20
    const/4 v4, 0x0

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2951
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2952
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2953
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2954
    return-void

    #@2e
    .line 2947
    :cond_2e
    const/4 v2, 0x0

    #@2f
    goto :goto_13
.end method

.method public finishReceiver(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;Z)V
    .registers 11
    .parameter "who"
    .parameter "resultCode"
    .parameter "resultData"
    .parameter "map"
    .parameter "abortBroadcast"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2244
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2245
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2246
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2247
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2248
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 2249
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 2250
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@1a
    .line 2251
    if-eqz p5, :cond_31

    #@1c
    move v2, v3

    #@1d
    :goto_1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 2252
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v4, 0x10

    #@24
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 2253
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 2254
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2255
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2256
    return-void

    #@31
    .line 2251
    :cond_31
    const/4 v2, 0x0

    #@32
    goto :goto_1d
.end method

.method public finishSubActivity(Landroid/os/IBinder;Ljava/lang/String;I)V
    .registers 9
    .parameter "token"
    .parameter "resultWho"
    .parameter "requestCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2131
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2132
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2133
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2134
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2135
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 2136
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 2137
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x20

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2138
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2139
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2140
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2141
    return-void
.end method

.method public forceStopPackage(Ljava/lang/String;I)V
    .registers 8
    .parameter "packageName"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3467
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3468
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3469
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3470
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3471
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3472
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x4f

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3473
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3474
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3475
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3476
    return-void
.end method

.method public getActivityClassForToken(Landroid/os/IBinder;)Landroid/content/ComponentName;
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3004
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3005
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3006
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3007
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 3008
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x31

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3009
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3010
    invoke-static {v1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@1e
    move-result-object v2

    #@1f
    .line 3011
    .local v2, res:Landroid/content/ComponentName;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3012
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3013
    return-object v2
.end method

.method public getCallingActivity(Landroid/os/IBinder;)Landroid/content/ComponentName;
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2366
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2367
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2368
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2369
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2370
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x16

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2371
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2372
    invoke-static {v1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@1e
    move-result-object v2

    #@1f
    .line 2373
    .local v2, res:Landroid/content/ComponentName;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2374
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2375
    return-object v2
.end method

.method public getCallingPackage(Landroid/os/IBinder;)Ljava/lang/String;
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2353
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2354
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2355
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2356
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2357
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x15

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2358
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2359
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 2360
    .local v2, res:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2361
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2362
    return-object v2
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2957
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2958
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2959
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2960
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x2e

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 2961
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 2962
    sget-object v3, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/content/res/Configuration;

    #@20
    .line 2963
    .local v2, res:Landroid/content/res/Configuration;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 2964
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 2965
    return-object v2
.end method

.method public getContentProvider(Landroid/app/IApplicationThread;Ljava/lang/String;IZ)Landroid/app/IActivityManager$ContentProviderHolder;
    .registers 12
    .parameter "caller"
    .parameter "name"
    .parameter "userId"
    .parameter "stable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2601
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v1

    #@5
    .line 2602
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v2

    #@9
    .line 2603
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2604
    if-eqz p1, :cond_43

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v4

    #@14
    :goto_14
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2605
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 2606
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 2607
    if-eqz p4, :cond_45

    #@1f
    const/4 v4, 0x1

    #@20
    :goto_20
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 2608
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/16 v6, 0x1d

    #@27
    invoke-interface {v4, v6, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 2609
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 2610
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v3

    #@31
    .line 2611
    .local v3, res:I
    const/4 v0, 0x0

    #@32
    .line 2612
    .local v0, cph:Landroid/app/IActivityManager$ContentProviderHolder;
    if-eqz v3, :cond_3c

    #@34
    .line 2613
    sget-object v4, Landroid/app/IActivityManager$ContentProviderHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    #@36
    invoke-interface {v4, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@39
    move-result-object v0

    #@3a
    .end local v0           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    check-cast v0, Landroid/app/IActivityManager$ContentProviderHolder;

    #@3c
    .line 2615
    .restart local v0       #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    :cond_3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 2616
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@42
    .line 2617
    return-object v0

    #@43
    .line 2604
    .end local v0           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    .end local v3           #res:I
    :cond_43
    const/4 v4, 0x0

    #@44
    goto :goto_14

    #@45
    :cond_45
    move v4, v5

    #@46
    .line 2607
    goto :goto_20
.end method

.method public getContentProviderExternal(Ljava/lang/String;ILandroid/os/IBinder;)Landroid/app/IActivityManager$ContentProviderHolder;
    .registers 11
    .parameter "name"
    .parameter "userId"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2621
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2622
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2623
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2624
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 2625
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2626
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2627
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v5, 0x8d

    #@1a
    const/4 v6, 0x0

    #@1b
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2628
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2629
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    .line 2630
    .local v3, res:I
    const/4 v0, 0x0

    #@26
    .line 2631
    .local v0, cph:Landroid/app/IActivityManager$ContentProviderHolder;
    if-eqz v3, :cond_30

    #@28
    .line 2632
    sget-object v4, Landroid/app/IActivityManager$ContentProviderHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-interface {v4, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d
    move-result-object v0

    #@2e
    .end local v0           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    check-cast v0, Landroid/app/IActivityManager$ContentProviderHolder;

    #@30
    .line 2634
    .restart local v0       #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    :cond_30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 2635
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 2636
    return-object v0
.end method

.method public getCurrentUser()Landroid/content/pm/UserInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3935
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3936
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3937
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3938
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x91

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3939
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3940
    sget-object v3, Landroid/content/pm/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/content/pm/UserInfo;

    #@20
    .line 3941
    .local v2, userInfo:Landroid/content/pm/UserInfo;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 3942
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3943
    return-object v2
.end method

.method public getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3493
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3494
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3495
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3496
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x54

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3497
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3498
    sget-object v3, Landroid/content/pm/ConfigurationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/content/pm/ConfigurationInfo;

    #@20
    .line 3499
    .local v2, res:Landroid/content/pm/ConfigurationInfo;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 3500
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3501
    return-object v2
.end method

.method public getFrontActivityScreenCompatMode()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3833
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3834
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 3835
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3836
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x7c

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3837
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3838
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 3839
    .local v1, mode:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 3840
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3841
    return v1
.end method

.method public getIntentForIntentSender(Landroid/content/IIntentSender;)Landroid/content/Intent;
    .registers 8
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4049
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4050
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4051
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4052
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 4053
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0xa1

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 4054
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 4055
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_35

    #@25
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a
    move-result-object v3

    #@2b
    check-cast v3, Landroid/content/Intent;

    #@2d
    move-object v2, v3

    #@2e
    .line 4057
    .local v2, res:Landroid/content/Intent;
    :goto_2e
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 4058
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 4059
    return-object v2

    #@35
    .line 4055
    .end local v2           #res:Landroid/content/Intent;
    :cond_35
    const/4 v2, 0x0

    #@36
    goto :goto_2e
.end method

.method public getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;
    .registers 18
    .parameter "type"
    .parameter "packageName"
    .parameter "token"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "intents"
    .parameter "resolvedTypes"
    .parameter "flags"
    .parameter "options"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3032
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 3033
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 3034
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3035
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3036
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 3037
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 3038
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 3039
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 3040
    if-eqz p6, :cond_57

    #@1e
    .line 3041
    const/4 v4, 0x1

    #@1f
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 3042
    const/4 v4, 0x0

    #@23
    invoke-virtual {v1, p6, v4}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@26
    .line 3043
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@29
    .line 3047
    :goto_29
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 3048
    if-eqz p9, :cond_5c

    #@2e
    .line 3049
    const/4 v4, 0x1

    #@2f
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 3050
    const/4 v4, 0x0

    #@33
    move-object/from16 v0, p9

    #@35
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@38
    .line 3054
    :goto_38
    move/from16 v0, p10

    #@3a
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 3055
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@3f
    const/16 v5, 0x3f

    #@41
    const/4 v6, 0x0

    #@42
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@45
    .line 3056
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@48
    .line 3057
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v4}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@4f
    move-result-object v3

    #@50
    .line 3059
    .local v3, res:Landroid/content/IIntentSender;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@53
    .line 3060
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@56
    .line 3061
    return-object v3

    #@57
    .line 3045
    .end local v3           #res:Landroid/content/IIntentSender;
    :cond_57
    const/4 v4, 0x0

    #@58
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_29

    #@5c
    .line 3052
    :cond_5c
    const/4 v4, 0x0

    #@5d
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    goto :goto_38
.end method

.method public getLaunchedFromUid(Landroid/os/IBinder;)I
    .registers 8
    .parameter "activityToken"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4148
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4149
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4150
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 4151
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x96

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4152
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4153
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v2

    #@1f
    .line 4154
    .local v2, result:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 4155
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 4156
    return v2
.end method

.method public getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V
    .registers 7
    .parameter "outInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3238
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3239
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3240
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3241
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x4c

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3242
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3243
    invoke-virtual {p1, v1}, Landroid/app/ActivityManager$MemoryInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@1b
    .line 3244
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3245
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3246
    return-void
.end method

.method public getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
    .registers 7
    .parameter "outInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3481
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3482
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3483
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3484
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x8f

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3485
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3486
    invoke-virtual {p1, v1}, Landroid/app/ActivityManager$RunningAppProcessInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@1b
    .line 3487
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3488
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3489
    return-void
.end method

.method public getPackageAskScreenCompat(Ljava/lang/String;)Z
    .registers 7
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 3882
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v1

    #@5
    .line 3883
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v2

    #@9
    .line 3884
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3885
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 3886
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x80

    #@15
    invoke-interface {v3, v4, v1, v2, v0}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3887
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3888
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v0, 0x1

    #@22
    .line 3889
    .local v0, ask:Z
    :cond_22
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3890
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3891
    return v0
.end method

.method public getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;
    .registers 8
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3074
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3075
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3076
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3077
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 3078
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x41

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 3079
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 3080
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    .line 3081
    .local v2, res:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3082
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 3083
    return-object v2
.end method

.method public getPackageForToken(Landroid/os/IBinder;)Ljava/lang/String;
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3017
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3018
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3019
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3020
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 3021
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x32

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3022
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3023
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    .line 3024
    .local v2, res:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3025
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3026
    return-object v2
.end method

.method public getPackageScreenCompatMode(Ljava/lang/String;)I
    .registers 8
    .parameter "packageName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3856
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3857
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 3858
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3859
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3860
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x7e

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3861
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3862
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v1

    #@1f
    .line 3863
    .local v1, mode:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3864
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3865
    return v1
.end method

.method public getProcessLimit()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3129
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3130
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3131
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3132
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x34

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3133
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3134
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 3135
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 3136
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3137
    return v2
.end method

.method public getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;
    .registers 8
    .parameter "pids"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3588
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3589
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3590
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3591
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 3592
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x62

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3593
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3594
    sget-object v3, Landroid/os/Debug$MemoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, [Landroid/os/Debug$MemoryInfo;

    #@23
    .line 3595
    .local v2, res:[Landroid/os/Debug$MemoryInfo;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3596
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 3597
    return-object v2
.end method

.method public getProcessPss([I)[J
    .registers 8
    .parameter "pids"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4075
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4076
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4077
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4078
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 4079
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x89

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4080
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4081
    invoke-virtual {v1}, Landroid/os/Parcel;->createLongArray()[J

    #@1e
    move-result-object v2

    #@1f
    .line 4082
    .local v2, res:[J
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 4083
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 4084
    return-object v2
.end method

.method public getProcessesInErrorState()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$ProcessErrorStateInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2475
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2476
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2477
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2478
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x4d

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 2479
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@18
    .line 2480
    sget-object v3, Landroid/app/ActivityManager$ProcessErrorStateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@1d
    move-result-object v1

    #@1e
    .line 2482
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ActivityManager$ProcessErrorStateInfo;>;"
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2483
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2484
    return-object v1
.end method

.method public getProviderMimeType(Landroid/net/Uri;I)Ljava/lang/String;
    .registers 9
    .parameter "uri"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3705
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3706
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3707
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3708
    invoke-virtual {p1, v0, v5}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 3709
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3710
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x73

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3711
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3712
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    .line 3713
    .local v2, res:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3714
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3715
    return-object v2
.end method

.method public getRecentTasks(III)Ljava/util/List;
    .registers 10
    .parameter "maxNum"
    .parameter "flags"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RecentTaskInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2405
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2406
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2407
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2408
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2409
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2410
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 2411
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v4, 0x3c

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2412
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2413
    sget-object v3, Landroid/app/ActivityManager$RecentTaskInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@23
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@26
    move-result-object v1

    #@27
    .line 2415
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2416
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2417
    return-object v1
.end method

.method public getRequestedOrientation(Landroid/os/IBinder;)I
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2991
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2992
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2993
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2994
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2995
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x47

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2996
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2997
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v2

    #@1f
    .line 2998
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2999
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3000
    return v2
.end method

.method public getRunningAppProcesses()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningAppProcessInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2488
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2489
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2490
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2491
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x53

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 2492
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@18
    .line 2493
    sget-object v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@1d
    move-result-object v1

    #@1e
    .line 2495
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2496
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2497
    return-object v1
.end method

.method public getRunningExternalApplications()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2501
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2502
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2503
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2504
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x6c

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 2505
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@18
    .line 2506
    sget-object v3, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@1d
    move-result-object v1

    #@1e
    .line 2508
    .local v1, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2509
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2510
    return-object v1
.end method

.method public getRunningServiceControlPanel(Landroid/content/ComponentName;)Landroid/app/PendingIntent;
    .registers 8
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2704
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2705
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2706
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2707
    invoke-virtual {p1, v0, v5}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 2708
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x21

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2709
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2710
    invoke-static {v1}, Landroid/app/PendingIntent;->readPendingIntentOrNullFromParcel(Landroid/os/Parcel;)Landroid/app/PendingIntent;

    #@1e
    move-result-object v2

    #@1f
    .line 2711
    .local v2, res:Landroid/app/PendingIntent;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2712
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2713
    return-object v2
.end method

.method public getRunningUserIds()[I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3961
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3962
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3963
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3964
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x9d

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3965
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3966
    invoke-virtual {v1}, Landroid/os/Parcel;->createIntArray()[I

    #@1b
    move-result-object v2

    #@1c
    .line 3967
    .local v2, result:[I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 3968
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3969
    return-object v2
.end method

.method public getScreenId(Landroid/os/IBinder;)I
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4264
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4265
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4266
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4267
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 4268
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xad

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4269
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4270
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v2

    #@1f
    .line 4271
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 4272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 4273
    return v2
.end method

.method public getServices(II)Ljava/util/List;
    .registers 11
    .parameter "maxNum"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2450
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2451
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v4

    #@8
    .line 2452
    .local v4, reply:Landroid/os/Parcel;
    const-string v5, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2453
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2454
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2455
    iget-object v5, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v6, 0x51

    #@17
    const/4 v7, 0x0

    #@18
    invoke-interface {v5, v6, v1, v4, v7}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2456
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 2457
    const/4 v3, 0x0

    #@1f
    .line 2458
    .local v3, list:Ljava/util/ArrayList;
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v0

    #@23
    .line 2459
    .local v0, N:I
    if-ltz v0, :cond_3a

    #@25
    .line 2460
    new-instance v3, Ljava/util/ArrayList;

    #@27
    .end local v3           #list:Ljava/util/ArrayList;
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@2a
    .line 2461
    .restart local v3       #list:Ljava/util/ArrayList;
    :goto_2a
    if-lez v0, :cond_3a

    #@2c
    .line 2462
    sget-object v5, Landroid/app/ActivityManager$RunningServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2e
    invoke-interface {v5, v4}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, Landroid/app/ActivityManager$RunningServiceInfo;

    #@34
    .line 2465
    .local v2, info:Landroid/app/ActivityManager$RunningServiceInfo;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 2466
    add-int/lit8 v0, v0, -0x1

    #@39
    .line 2467
    goto :goto_2a

    #@3a
    .line 2469
    .end local v2           #info:Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 2470
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 2471
    return-object v3
.end method

.method public getTaskForActivity(Landroid/os/IBinder;Z)I
    .registers 9
    .parameter "token"
    .parameter "onlyRoot"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2568
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2569
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2570
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2571
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2572
    if-eqz p2, :cond_2c

    #@13
    const/4 v3, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2573
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v5, 0x1b

    #@1b
    invoke-interface {v3, v5, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2574
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2575
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v2

    #@25
    .line 2576
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 2577
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 2578
    return v2

    #@2c
    .end local v2           #res:I
    :cond_2c
    move v3, v4

    #@2d
    .line 2572
    goto :goto_14
.end method

.method public getTaskScreenShot(I)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 8
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 4184
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 4185
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4186
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 4187
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xa7

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4188
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4189
    const/4 v0, 0x0

    #@1c
    .line 4190
    .local v0, bm:Landroid/app/ActivityManager$TaskThumbnails;
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_2a

    #@22
    .line 4191
    sget-object v3, Landroid/app/ActivityManager$TaskThumbnails;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v3, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    .end local v0           #bm:Landroid/app/ActivityManager$TaskThumbnails;
    check-cast v0, Landroid/app/ActivityManager$TaskThumbnails;

    #@2a
    .line 4193
    .restart local v0       #bm:Landroid/app/ActivityManager$TaskThumbnails;
    :cond_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 4194
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 4195
    return-object v0
.end method

.method public getTaskThumbnails(I)Landroid/app/ActivityManager$TaskThumbnails;
    .registers 8
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2420
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2421
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2422
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2423
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2424
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x52

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2425
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2426
    const/4 v0, 0x0

    #@1c
    .line 2427
    .local v0, bm:Landroid/app/ActivityManager$TaskThumbnails;
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_2a

    #@22
    .line 2428
    sget-object v3, Landroid/app/ActivityManager$TaskThumbnails;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v3, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    .end local v0           #bm:Landroid/app/ActivityManager$TaskThumbnails;
    check-cast v0, Landroid/app/ActivityManager$TaskThumbnails;

    #@2a
    .line 2430
    .restart local v0       #bm:Landroid/app/ActivityManager$TaskThumbnails;
    :cond_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2431
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2432
    return-object v0
.end method

.method public getTaskTopThumbnail(I)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2435
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2436
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2437
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2438
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2439
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x5f

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2440
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2441
    const/4 v0, 0x0

    #@1c
    .line 2442
    .local v0, bm:Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_2a

    #@22
    .line 2443
    sget-object v3, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v3, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    .end local v0           #bm:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    #@2a
    .line 2445
    .restart local v0       #bm:Landroid/graphics/Bitmap;
    :cond_2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2446
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 2447
    return-object v0
.end method

.method public getTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;
    .registers 12
    .parameter "maxNum"
    .parameter "flags"
    .parameter "receiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2379
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2380
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v4

    #@8
    .line 2381
    .local v4, reply:Landroid/os/Parcel;
    const-string v5, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2382
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2383
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2384
    if-eqz p3, :cond_43

    #@15
    invoke-interface {p3}, Landroid/app/IThumbnailReceiver;->asBinder()Landroid/os/IBinder;

    #@18
    move-result-object v5

    #@19
    :goto_19
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1c
    .line 2385
    iget-object v5, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v6, 0x17

    #@20
    const/4 v7, 0x0

    #@21
    invoke-interface {v5, v6, v1, v4, v7}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2386
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2387
    const/4 v3, 0x0

    #@28
    .line 2388
    .local v3, list:Ljava/util/ArrayList;
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    .line 2389
    .local v0, N:I
    if-ltz v0, :cond_45

    #@2e
    .line 2390
    new-instance v3, Ljava/util/ArrayList;

    #@30
    .end local v3           #list:Ljava/util/ArrayList;
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@33
    .line 2391
    .restart local v3       #list:Ljava/util/ArrayList;
    :goto_33
    if-lez v0, :cond_45

    #@35
    .line 2392
    sget-object v5, Landroid/app/ActivityManager$RunningTaskInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@37
    invoke-interface {v5, v4}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3a
    move-result-object v2

    #@3b
    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    #@3d
    .line 2395
    .local v2, info:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@40
    .line 2396
    add-int/lit8 v0, v0, -0x1

    #@42
    .line 2397
    goto :goto_33

    #@43
    .line 2384
    .end local v0           #N:I
    .end local v2           #info:Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v3           #list:Ljava/util/ArrayList;
    :cond_43
    const/4 v5, 0x0

    #@44
    goto :goto_19

    #@45
    .line 2399
    .restart local v0       #N:I
    .restart local v3       #list:Ljava/util/ArrayList;
    :cond_45
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 2400
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@4b
    .line 2401
    return-object v3
.end method

.method public getTopRunningTaskInfo(I)Landroid/app/ActivityManager$RunningTaskInfo;
    .registers 8
    .parameter "screenZone"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4250
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4251
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4252
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 4253
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0xac

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4254
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4255
    sget-object v3, Landroid/app/ActivityManager$RunningTaskInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    #@23
    .line 4257
    .local v2, res:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 4258
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 4259
    return-object v2
.end method

.method public getUidForIntentSender(Landroid/content/IIntentSender;)I
    .registers 8
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3086
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3087
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3088
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3089
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 3090
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x5d

    #@18
    const/4 v5, 0x0

    #@19
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 3091
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 3092
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v2

    #@23
    .line 3093
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3094
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 3095
    return v2
.end method

.method public goingToSleep()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3274
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3275
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3276
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3277
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x28

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3278
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3279
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3280
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3281
    return-void
.end method

.method public grantUriPermission(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/net/Uri;I)V
    .registers 10
    .parameter "caller"
    .parameter "targetPkg"
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3200
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3201
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3202
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3203
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 3204
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@18
    .line 3205
    invoke-virtual {p3, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@1b
    .line 3206
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 3207
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@20
    const/16 v3, 0x37

    #@22
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 3208
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@28
    .line 3209
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3210
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 3211
    return-void
.end method

.method public grantUriPermissionFromOwner(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;I)V
    .registers 11
    .parameter "owner"
    .parameter "fromUid"
    .parameter "targetPkg"
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3734
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3735
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3736
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3737
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3738
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3739
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 3740
    invoke-virtual {p4, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 3741
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 3742
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x37

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 3743
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 3744
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3745
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 3746
    return-void
.end method

.method public handleApplicationCrash(Landroid/os/IBinder;Landroid/app/ApplicationErrorReport$CrashInfo;)V
    .registers 8
    .parameter "app"
    .parameter "crashInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3389
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3390
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3391
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3392
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3393
    invoke-virtual {p2, v0, v4}, Landroid/app/ApplicationErrorReport$CrashInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 3394
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v3, 0x2

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 3395
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1d
    .line 3396
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 3397
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 3398
    return-void
.end method

.method public handleApplicationStrictModeViolation(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V
    .registers 9
    .parameter "app"
    .parameter "violationMask"
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3421
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3422
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3423
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3424
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3425
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3426
    invoke-virtual {p3, v0, v4}, Landroid/os/StrictMode$ViolationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 3427
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x6e

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3428
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3429
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3430
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3431
    return-void
.end method

.method public handleApplicationWtf(Landroid/os/IBinder;Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)Z
    .registers 9
    .parameter "app"
    .parameter "tag"
    .parameter "crashInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3403
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3404
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3405
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3406
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3407
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 3408
    invoke-virtual {p3, v0, v2}, Landroid/app/ApplicationErrorReport$CrashInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 3409
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x66

    #@1b
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3410
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3411
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 3412
    .local v2, res:Z
    :cond_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 3414
    return v2
.end method

.method public handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I
    .registers 14
    .parameter "callingPid"
    .parameter "callingUid"
    .parameter "userId"
    .parameter "allowAll"
    .parameter "requireFull"
    .parameter "name"
    .parameter "callerPackage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 3099
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3100
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3101
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3102
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 3103
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 3104
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 3105
    if-eqz p4, :cond_3e

    #@1a
    move v3, v4

    #@1b
    :goto_1b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 3106
    if-eqz p5, :cond_40

    #@20
    :goto_20
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 3107
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@26
    .line 3108
    invoke-virtual {v0, p7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29
    .line 3109
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2b
    const/16 v4, 0x5e

    #@2d
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@30
    .line 3110
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@33
    .line 3111
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v2

    #@37
    .line 3112
    .local v2, res:I
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 3113
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 3114
    return v2

    #@3e
    .end local v2           #res:I
    :cond_3e
    move v3, v5

    #@3f
    .line 3105
    goto :goto_1b

    #@40
    :cond_40
    move v4, v5

    #@41
    .line 3106
    goto :goto_20
.end method

.method public inputDispatchingTimedOut(IZ)J
    .registers 10
    .parameter "pid"
    .parameter "aboveSystem"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 4209
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4210
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4211
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4212
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 4213
    if-eqz p2, :cond_2d

    #@13
    const/4 v4, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 4214
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v6, 0x9f

    #@1b
    invoke-interface {v4, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4215
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4216
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v4

    #@25
    int-to-long v2, v4

    #@26
    .line 4217
    .local v2, res:J
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 4218
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 4219
    return-wide v2

    #@2d
    .end local v2           #res:J
    :cond_2d
    move v4, v5

    #@2e
    .line 4213
    goto :goto_14
.end method

.method public isImmersive(Landroid/os/IBinder;)Z
    .registers 8
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3664
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3665
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3666
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3667
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 3668
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@14
    const/16 v5, 0x6f

    #@16
    invoke-interface {v4, v5, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 3669
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 3670
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v4

    #@20
    if-ne v4, v2, :cond_29

    #@22
    .line 3671
    .local v2, res:Z
    :goto_22
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3672
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3673
    return v2

    #@29
    .end local v2           #res:Z
    :cond_29
    move v2, v3

    #@2a
    .line 3670
    goto :goto_22
.end method

.method public isIntentSenderAnActivity(Landroid/content/IIntentSender;)Z
    .registers 7
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4036
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4037
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4038
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4039
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 4040
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v4, 0x98

    #@19
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 4041
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 4042
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 4043
    .local v2, res:Z
    :cond_26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 4044
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 4045
    return v2
.end method

.method public isIntentSenderTargetedToPackage(Landroid/content/IIntentSender;)Z
    .registers 7
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4023
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4024
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4025
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4026
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 4027
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v4, 0x87

    #@19
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 4028
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 4029
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 4030
    .local v2, res:Z
    :cond_26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 4031
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 4032
    return v2
.end method

.method public isTopActivityImmersive()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 3678
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3679
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3680
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3681
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@11
    const/16 v5, 0x71

    #@13
    invoke-interface {v4, v5, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 3682
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@19
    .line 3683
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v4

    #@1d
    if-ne v4, v2, :cond_26

    #@1f
    .line 3684
    .local v2, res:Z
    :goto_1f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3685
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3686
    return v2

    #@26
    .end local v2           #res:Z
    :cond_26
    move v2, v3

    #@27
    .line 3683
    goto :goto_1f
.end method

.method public isUserAMonkey()Z
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3628
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3629
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3630
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3631
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/16 v4, 0x68

    #@12
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3632
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3633
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 3634
    .local v2, res:Z
    :cond_1f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3635
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3636
    return v2
.end method

.method public isUserRunning(IZ)Z
    .registers 10
    .parameter "userid"
    .parameter "orStopping"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 3947
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3948
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3949
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3950
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 3951
    if-eqz p2, :cond_30

    #@14
    move v3, v4

    #@15
    :goto_15
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 3952
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v6, 0x7a

    #@1c
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 3953
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 3954
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_32

    #@28
    move v2, v4

    #@29
    .line 3955
    .local v2, result:Z
    :goto_29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 3956
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 3957
    return v2

    #@30
    .end local v2           #result:Z
    :cond_30
    move v3, v5

    #@31
    .line 3951
    goto :goto_15

    #@32
    :cond_32
    move v2, v5

    #@33
    .line 3954
    goto :goto_29
.end method

.method public killAllBackgroundProcesses()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3457
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3458
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3459
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3460
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x8c

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3461
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3462
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3463
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3464
    return-void
.end method

.method public killApplicationProcess(Ljava/lang/String;I)V
    .registers 8
    .parameter "processName"
    .parameter "uid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3601
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3602
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3603
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3604
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3605
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3606
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x63

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3607
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3608
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3609
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3610
    return-void
.end method

.method public killApplicationWithAppId(Ljava/lang/String;I)V
    .registers 8
    .parameter "pkg"
    .parameter "appid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3564
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3565
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3566
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3567
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3568
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3569
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x60

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3570
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3571
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3572
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3573
    return-void
.end method

.method public killBackgroundProcesses(Ljava/lang/String;I)V
    .registers 8
    .parameter "packageName"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3445
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3446
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3447
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3448
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3449
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3450
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x67

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3451
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3452
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3453
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3454
    return-void
.end method

.method public killPids([ILjava/lang/String;Z)Z
    .registers 11
    .parameter "pids"
    .parameter "reason"
    .parameter "secure"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 3343
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3344
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3345
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3346
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@12
    .line 3347
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    .line 3348
    if-eqz p3, :cond_30

    #@17
    move v3, v4

    #@18
    :goto_18
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3349
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v6, 0x50

    #@1f
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 3350
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_32

    #@28
    move v2, v4

    #@29
    .line 3351
    .local v2, res:Z
    :goto_29
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 3352
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 3353
    return v2

    #@30
    .end local v2           #res:Z
    :cond_30
    move v3, v5

    #@31
    .line 3348
    goto :goto_18

    #@32
    :cond_32
    move v2, v5

    #@33
    .line 3350
    goto :goto_29
.end method

.method public killProcessesBelowForeground(Ljava/lang/String;)Z
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3357
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3358
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3359
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3360
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 3361
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x90

    #@15
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3362
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 3363
    .local v2, res:Z
    :cond_1f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3364
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3365
    return v2
.end method

.method public moveActivityTaskToBack(Landroid/os/IBinder;Z)Z
    .registers 10
    .parameter "token"
    .parameter "nonRoot"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2543
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2544
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2545
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2546
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2547
    if-eqz p2, :cond_30

    #@14
    move v3, v4

    #@15
    :goto_15
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 2548
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v6, 0x4b

    #@1c
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 2549
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 2550
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_32

    #@28
    move v2, v4

    #@29
    .line 2551
    .local v2, res:Z
    :goto_29
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 2552
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 2553
    return v2

    #@30
    .end local v2           #res:Z
    :cond_30
    move v3, v5

    #@31
    .line 2547
    goto :goto_15

    #@32
    :cond_32
    move v2, v5

    #@33
    .line 2550
    goto :goto_29
.end method

.method public moveTaskBackwards(I)V
    .registers 7
    .parameter "task"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2557
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2558
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2559
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2560
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2561
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x1a

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2562
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2563
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2564
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2565
    return-void
.end method

.method public moveTaskToBack(I)V
    .registers 7
    .parameter "task"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2532
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2533
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2534
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2535
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 2536
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x19

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2537
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2538
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2539
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2540
    return-void
.end method

.method public moveTaskToFront(IILandroid/os/Bundle;)V
    .registers 9
    .parameter "task"
    .parameter "flags"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2514
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2515
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2516
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2517
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 2518
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 2519
    if-eqz p3, :cond_2e

    #@16
    .line 2520
    const/4 v2, 0x1

    #@17
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2521
    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 2525
    :goto_1d
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x18

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2526
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2527
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2528
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2529
    return-void

    #@2e
    .line 2523
    :cond_2e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    goto :goto_1d
.end method

.method public navigateUpTo(Landroid/os/IBinder;Landroid/content/Intent;ILandroid/content/Intent;)Z
    .registers 11
    .parameter "token"
    .parameter "target"
    .parameter "resultCode"
    .parameter "resultData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 4126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 4127
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 4128
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 4129
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 4130
    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 4131
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 4132
    if-eqz p4, :cond_37

    #@1a
    .line 4133
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 4134
    invoke-virtual {p4, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 4138
    :goto_20
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v5, 0x93

    #@24
    invoke-interface {v4, v5, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 4139
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 4140
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v4

    #@2e
    if-eqz v4, :cond_3b

    #@30
    .line 4141
    .local v2, result:Z
    :goto_30
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 4142
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 4143
    return v2

    #@37
    .line 4136
    .end local v2           #result:Z
    :cond_37
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3a
    goto :goto_20

    #@3b
    :cond_3b
    move v2, v3

    #@3c
    .line 4140
    goto :goto_30
.end method

.method public newUriPermissionOwner(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 8
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3720
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3721
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3722
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3723
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3724
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v4, 0x74

    #@14
    const/4 v5, 0x0

    #@15
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3725
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3726
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    .line 3727
    .local v2, res:Landroid/os/IBinder;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 3728
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3729
    return-object v2
.end method

.method public noteWakeupAlarm(Landroid/content/IIntentSender;)V
    .registers 7
    .parameter "sender"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3336
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3337
    .local v0, data:Landroid/os/Parcel;
    invoke-interface {p1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@b
    .line 3338
    const-string v1, "android.app.IActivityManager"

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@10
    .line 3339
    iget-object v1, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v2, 0x44

    #@14
    const/4 v3, 0x0

    #@15
    const/4 v4, 0x0

    #@16
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 3340
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 3341
    return-void
.end method

.method public openContentUri(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .registers 8
    .parameter "uri"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3260
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 3261
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3262
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 3263
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@17
    .line 3264
    const/4 v1, 0x0

    #@18
    .line 3265
    .local v1, pfd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_26

    #@1e
    .line 3266
    sget-object v3, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    .end local v1           #pfd:Landroid/os/ParcelFileDescriptor;
    check-cast v1, Landroid/os/ParcelFileDescriptor;

    #@26
    .line 3268
    .restart local v1       #pfd:Landroid/os/ParcelFileDescriptor;
    :cond_26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 3269
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 3270
    return-object v1
.end method

.method public overridePendingTransition(Landroid/os/IBinder;Ljava/lang/String;II)V
    .registers 10
    .parameter "token"
    .parameter "packageName"
    .parameter "enterAnim"
    .parameter "exitAnim"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3614
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3615
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3616
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3617
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 3618
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 3619
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 3620
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 3621
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v3, 0x65

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 3622
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 3623
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3624
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3625
    return-void
.end method

.method public peekService(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;
    .registers 9
    .parameter "service"
    .parameter "resolvedType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2863
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v1

    #@5
    .line 2864
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v2

    #@9
    .line 2865
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2866
    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 2867
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 2868
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x55

    #@18
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2869
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 2870
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@21
    move-result-object v0

    #@22
    .line 2871
    .local v0, binder:Landroid/os/IBinder;
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2872
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 2873
    return-object v0
.end method

.method public profileControl(Ljava/lang/String;IZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Z
    .registers 14
    .parameter "process"
    .parameter "userId"
    .parameter "start"
    .parameter "path"
    .parameter "fd"
    .parameter "profileType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 3507
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3508
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3509
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3510
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 3511
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 3512
    if-eqz p3, :cond_41

    #@17
    move v3, v4

    #@18
    :goto_18
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3513
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 3514
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@21
    .line 3515
    if-eqz p5, :cond_43

    #@23
    .line 3516
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 3517
    invoke-virtual {p5, v0, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@29
    .line 3521
    :goto_29
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2b
    const/16 v6, 0x56

    #@2d
    invoke-interface {v3, v6, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@30
    .line 3522
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@33
    .line 3523
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@36
    move-result v3

    #@37
    if-eqz v3, :cond_47

    #@39
    move v2, v4

    #@3a
    .line 3524
    .local v2, res:Z
    :goto_3a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 3525
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 3526
    return v2

    #@41
    .end local v2           #res:Z
    :cond_41
    move v3, v5

    #@42
    .line 3512
    goto :goto_18

    #@43
    .line 3519
    :cond_43
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@46
    goto :goto_29

    #@47
    :cond_47
    move v2, v5

    #@48
    .line 3523
    goto :goto_3a
.end method

.method public publishContentProviders(Landroid/app/IApplicationThread;Ljava/util/List;)V
    .registers 8
    .parameter "caller"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/IApplicationThread;",
            "Ljava/util/List",
            "<",
            "Landroid/app/IActivityManager$ContentProviderHolder;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2641
    .local p2, providers:Ljava/util/List;,"Ljava/util/List<Landroid/app/IActivityManager$ContentProviderHolder;>;"
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2642
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2643
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2644
    if-eqz p1, :cond_2b

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2645
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@19
    .line 2646
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v3, 0x1e

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 2647
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 2648
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2649
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2650
    return-void

    #@2b
    .line 2644
    :cond_2b
    const/4 v2, 0x0

    #@2c
    goto :goto_13
.end method

.method public publishService(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/IBinder;)V
    .registers 9
    .parameter "token"
    .parameter "intent"
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2821
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2822
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2823
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2824
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2825
    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 2826
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2827
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v3, 0x26

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2828
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2829
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2830
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2831
    return-void
.end method

.method public refContentProvider(Landroid/os/IBinder;II)Z
    .registers 9
    .parameter "connection"
    .parameter "stable"
    .parameter "unstable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2653
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2654
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2655
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2656
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2657
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 2658
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2659
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x1f

    #@1b
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2660
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2661
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 2662
    .local v2, res:Z
    :cond_28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 2663
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 2664
    return v2
.end method

.method public registerProcessObserver(Landroid/app/IProcessObserver;)V
    .registers 7
    .parameter "observer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4001
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4002
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4003
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4004
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/app/IProcessObserver;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 4005
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x85

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4006
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4007
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 4008
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4009
    return-void

    #@28
    .line 4004
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13
.end method

.method public registerReceiver(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/IIntentReceiver;Landroid/content/IntentFilter;Ljava/lang/String;I)Landroid/content/Intent;
    .registers 14
    .parameter "caller"
    .parameter "packageName"
    .parameter "receiver"
    .parameter "filter"
    .parameter "perm"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    .line 2170
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2171
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v3

    #@a
    .line 2172
    .local v3, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2173
    if-eqz p1, :cond_4d

    #@11
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 2174
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 2175
    if-eqz p3, :cond_21

    #@1d
    invoke-interface {p3}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@20
    move-result-object v5

    #@21
    :cond_21
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@24
    .line 2176
    invoke-virtual {p4, v0, v6}, Landroid/content/IntentFilter;->writeToParcel(Landroid/os/Parcel;I)V

    #@27
    .line 2177
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2a
    .line 2178
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 2179
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/16 v5, 0xc

    #@31
    invoke-interface {v4, v5, v0, v3, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@34
    .line 2180
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    #@37
    .line 2181
    const/4 v2, 0x0

    #@38
    .line 2182
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v1

    #@3c
    .line 2183
    .local v1, haveIntent:I
    if-eqz v1, :cond_46

    #@3e
    .line 2184
    sget-object v4, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40
    invoke-interface {v4, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@43
    move-result-object v2

    #@44
    .end local v2           #intent:Landroid/content/Intent;
    check-cast v2, Landroid/content/Intent;

    #@46
    .line 2186
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_46
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@49
    .line 2187
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4c
    .line 2188
    return-object v2

    #@4d
    .end local v1           #haveIntent:I
    .end local v2           #intent:Landroid/content/Intent;
    :cond_4d
    move-object v4, v5

    #@4e
    .line 2173
    goto :goto_15
.end method

.method public registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V
    .registers 7
    .parameter "observer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4160
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4161
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4162
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4163
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/app/IUserSwitchObserver;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 4164
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x9b

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4165
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4166
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 4167
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4168
    return-void

    #@28
    .line 4163
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13
.end method

.method public removeContentProvider(Landroid/os/IBinder;Z)V
    .registers 8
    .parameter "connection"
    .parameter "stable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2678
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2679
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2680
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2681
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2682
    if-eqz p2, :cond_28

    #@13
    const/4 v2, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2683
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x45

    #@1b
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2684
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2685
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2686
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2687
    return-void

    #@28
    :cond_28
    move v2, v3

    #@29
    .line 2682
    goto :goto_14
.end method

.method public removeContentProviderExternal(Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 8
    .parameter "name"
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2690
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2691
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2692
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2693
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 2694
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@13
    .line 2695
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x8e

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2696
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 2697
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2698
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2699
    return-void
.end method

.method public removeSubTask(II)Z
    .registers 8
    .parameter "taskId"
    .parameter "subTaskIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3973
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3974
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3975
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3976
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 3977
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3978
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x83

    #@18
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3979
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3980
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 3981
    .local v2, result:Z
    :cond_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3982
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3983
    return v2
.end method

.method public removeTask(II)Z
    .registers 8
    .parameter "taskId"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3987
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3988
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3989
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3990
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 3991
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3992
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x84

    #@18
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3993
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3994
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 3995
    .local v2, result:Z
    :cond_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3996
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3997
    return v2
.end method

.method public reportThumbnail(Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V
    .registers 9
    .parameter "token"
    .parameter "thumbnail"
    .parameter "description"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2583
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2584
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2585
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2586
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2587
    if-eqz p2, :cond_2e

    #@14
    .line 2588
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2589
    invoke-virtual {p2, v0, v3}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2593
    :goto_1a
    invoke-static {p3, v0, v3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@1d
    .line 2594
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x1c

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2595
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2596
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2597
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2598
    return-void

    #@2e
    .line 2591
    :cond_2e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    goto :goto_1a
.end method

.method public requestBugReport()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4199
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4200
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4201
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4202
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x9e

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 4203
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 4204
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 4205
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 4206
    return-void
.end method

.method public resumeAppSwitches()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3554
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3555
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3556
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3557
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x59

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3558
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3559
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3560
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3561
    return-void
.end method

.method public revokeUriPermission(Landroid/app/IApplicationThread;Landroid/net/Uri;I)V
    .registers 9
    .parameter "caller"
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3214
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3215
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3216
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3217
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 3218
    invoke-virtual {p2, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@18
    .line 3219
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3220
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v3, 0x38

    #@1f
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 3221
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@25
    .line 3222
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3223
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3224
    return-void
.end method

.method public revokeUriPermissionFromOwner(Landroid/os/IBinder;Landroid/net/Uri;I)V
    .registers 9
    .parameter "owner"
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 3750
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3751
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3752
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3753
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3754
    if-eqz p2, :cond_2e

    #@13
    .line 3755
    const/4 v2, 0x1

    #@14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 3756
    invoke-virtual {p2, v0, v4}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 3760
    :goto_1a
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 3761
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x38

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 3762
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 3763
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3764
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 3765
    return-void

    #@2e
    .line 3758
    :cond_2e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    goto :goto_1a
.end method

.method public serviceDoneExecuting(Landroid/os/IBinder;III)V
    .registers 10
    .parameter "token"
    .parameter "type"
    .parameter "startId"
    .parameter "res"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2849
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2850
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2851
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2852
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2853
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2854
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 2855
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 2856
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/16 v3, 0x3d

    #@1d
    const/4 v4, 0x1

    #@1e
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 2857
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 2858
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2859
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2860
    return-void
.end method

.method public setActivityController(Landroid/app/IActivityController;)V
    .registers 7
    .parameter "watcher"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3320
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3321
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3322
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3323
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/app/IActivityController;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 3324
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x39

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3325
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3326
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3327
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3328
    return-void

    #@28
    .line 3323
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13
.end method

.method public setAlwaysFinish(Z)V
    .registers 7
    .parameter "enabled"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3309
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3310
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3311
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3312
    if-eqz p1, :cond_25

    #@10
    const/4 v2, 0x1

    #@11
    :goto_11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3313
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x2b

    #@18
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3314
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3315
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3316
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3317
    return-void

    #@25
    :cond_25
    move v2, v3

    #@26
    .line 3312
    goto :goto_11
.end method

.method public setDebugApp(Ljava/lang/String;ZZ)V
    .registers 9
    .parameter "packageName"
    .parameter "waitForDebugger"
    .parameter "persistent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3296
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 3297
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 3298
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 3299
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 3300
    if-eqz p2, :cond_2e

    #@14
    move v2, v3

    #@15
    :goto_15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 3301
    if-eqz p3, :cond_30

    #@1a
    :goto_1a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 3302
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0x2a

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 3303
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 3304
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3305
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 3306
    return-void

    #@2e
    :cond_2e
    move v2, v4

    #@2f
    .line 3300
    goto :goto_15

    #@30
    :cond_30
    move v3, v4

    #@31
    .line 3301
    goto :goto_1a
.end method

.method public setFrontActivityScreenCompatMode(I)V
    .registers 7
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3845
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3846
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3847
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3848
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3849
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x7d

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3850
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3851
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3852
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3853
    return-void
.end method

.method public setImmersive(Landroid/os/IBinder;Z)V
    .registers 8
    .parameter "token"
    .parameter "immersive"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3651
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3652
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3653
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3654
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3655
    if-eqz p2, :cond_28

    #@13
    const/4 v2, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 3656
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x70

    #@1b
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3657
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3658
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3659
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3660
    return-void

    #@28
    :cond_28
    move v2, v3

    #@29
    .line 3655
    goto :goto_14
.end method

.method public setPackageAskScreenCompat(Ljava/lang/String;Z)V
    .registers 8
    .parameter "packageName"
    .parameter "ask"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3896
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3897
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3898
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3899
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 3900
    if-eqz p2, :cond_28

    #@13
    const/4 v2, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 3901
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x81

    #@1b
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 3902
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 3903
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3904
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3905
    return-void

    #@28
    :cond_28
    move v2, v3

    #@29
    .line 3900
    goto :goto_14
.end method

.method public setPackageScreenCompatMode(Ljava/lang/String;I)V
    .registers 8
    .parameter "packageName"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3870
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3871
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3872
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3873
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3874
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 3875
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x7f

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3876
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3877
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3878
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 3879
    return-void
.end method

.method public setProcessForeground(Landroid/os/IBinder;IZ)V
    .registers 9
    .parameter "token"
    .parameter "pid"
    .parameter "isForeground"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3141
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3142
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3143
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3144
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 3145
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 3146
    if-eqz p3, :cond_2b

    #@16
    const/4 v2, 0x1

    #@17
    :goto_17
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 3147
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v4, 0x49

    #@1e
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 3148
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 3149
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 3150
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 3151
    return-void

    #@2b
    :cond_2b
    move v2, v3

    #@2c
    .line 3146
    goto :goto_17
.end method

.method public setProcessLimit(I)V
    .registers 7
    .parameter "max"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3118
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3119
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3120
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3121
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3122
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x33

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3123
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3124
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3125
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3126
    return-void
.end method

.method public setRequestedOrientation(Landroid/os/IBinder;I)V
    .registers 8
    .parameter "token"
    .parameter "requestedOrientation"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2980
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2981
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2982
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2983
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2984
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 2985
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x46

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 2986
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 2987
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2988
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 2989
    return-void
.end method

.method public setServiceForeground(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V
    .registers 11
    .parameter "className"
    .parameter "token"
    .parameter "id"
    .parameter "notification"
    .parameter "removeNotification"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 2767
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2768
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2769
    .local v1, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2770
    invoke-static {p1, v0}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@12
    .line 2771
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 2772
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 2773
    if-eqz p4, :cond_36

    #@1a
    .line 2774
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 2775
    invoke-virtual {p4, v0, v3}, Landroid/app/Notification;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 2779
    :goto_20
    if-eqz p5, :cond_3a

    #@22
    :goto_22
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 2780
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/16 v4, 0x4a

    #@29
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 2781
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 2782
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 2783
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 2784
    return-void

    #@36
    .line 2777
    :cond_36
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    goto :goto_20

    #@3a
    :cond_3a
    move v2, v3

    #@3b
    .line 2779
    goto :goto_22
.end method

.method public showBootMessage(Ljava/lang/CharSequence;Z)V
    .registers 8
    .parameter "msg"
    .parameter "always"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4088
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4089
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4090
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4091
    invoke-static {p1, v0, v3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@11
    .line 4092
    if-eqz p2, :cond_28

    #@13
    const/4 v2, 0x1

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 4093
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x8a

    #@1b
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4094
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4095
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 4096
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4097
    return-void

    #@28
    :cond_28
    move v2, v3

    #@29
    .line 4092
    goto :goto_14
.end method

.method public showWaitingForDebugger(Landroid/app/IApplicationThread;Z)V
    .registers 8
    .parameter "who"
    .parameter "waiting"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3227
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3228
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3229
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3230
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 3231
    if-eqz p2, :cond_2c

    #@17
    const/4 v2, 0x1

    #@18
    :goto_18
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 3232
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1d
    const/16 v4, 0x3a

    #@1f
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@22
    .line 3233
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@25
    .line 3234
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3235
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 3236
    return-void

    #@2c
    :cond_2c
    move v2, v3

    #@2d
    .line 3231
    goto :goto_18
.end method

.method public shutdown(I)Z
    .registers 7
    .parameter "timeout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3531
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3532
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3533
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3534
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 3535
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x57

    #@15
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3536
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3537
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 3538
    .local v2, res:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3539
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3540
    return v2
.end method

.method public signalPersistentProcesses(I)V
    .registers 7
    .parameter "sig"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3434
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3435
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3436
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3437
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3438
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x3b

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3439
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3440
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3441
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 3442
    return-void
.end method

.method public startActivities(Landroid/app/IApplicationThread;[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I
    .registers 13
    .parameter "caller"
    .parameter "intents"
    .parameter "resolvedTypes"
    .parameter "resultTo"
    .parameter "options"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 3810
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3811
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3812
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3813
    if-eqz p1, :cond_41

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 3814
    invoke-virtual {v0, p2, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@1a
    .line 3815
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@1d
    .line 3816
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 3817
    if-eqz p5, :cond_43

    #@22
    .line 3818
    const/4 v3, 0x1

    #@23
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 3819
    invoke-virtual {p5, v0, v5}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@29
    .line 3823
    :goto_29
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 3824
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2e
    const/16 v4, 0x79

    #@30
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@33
    .line 3825
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@36
    .line 3826
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v2

    #@3a
    .line 3827
    .local v2, result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 3828
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 3829
    return v2

    #@41
    .line 3813
    .end local v2           #result:I
    :cond_41
    const/4 v3, 0x0

    #@42
    goto :goto_14

    #@43
    .line 3821
    :cond_43
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@46
    goto :goto_29
.end method

.method public startActivity(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I
    .registers 18
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "startFlags"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1925
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 1926
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 1927
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1928
    if-eqz p1, :cond_59

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1929
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 1930
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 1931
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 1932
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 1933
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 1934
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 1935
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2c
    .line 1936
    if-eqz p9, :cond_5b

    #@2e
    .line 1937
    const/4 v4, 0x1

    #@2f
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 1938
    const/4 v4, 0x1

    #@33
    move-object/from16 v0, p9

    #@35
    invoke-virtual {v0, v1, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@38
    .line 1942
    :goto_38
    if-eqz p10, :cond_60

    #@3a
    .line 1943
    const/4 v4, 0x1

    #@3b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 1944
    const/4 v4, 0x0

    #@3f
    move-object/from16 v0, p10

    #@41
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    .line 1948
    :goto_44
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@46
    const/4 v5, 0x3

    #@47
    const/4 v6, 0x0

    #@48
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@4b
    .line 1949
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@4e
    .line 1950
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v3

    #@52
    .line 1951
    .local v3, result:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@55
    .line 1952
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@58
    .line 1953
    return v3

    #@59
    .line 1928
    .end local v3           #result:I
    :cond_59
    const/4 v4, 0x0

    #@5a
    goto :goto_13

    #@5b
    .line 1940
    :cond_5b
    const/4 v4, 0x0

    #@5c
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_38

    #@60
    .line 1946
    :cond_60
    const/4 v4, 0x0

    #@61
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    goto :goto_44
.end method

.method public startActivityAndWait(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)Landroid/app/IActivityManager$WaitResult;
    .registers 19
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "startFlags"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "options"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1995
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 1996
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 1997
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1998
    if-eqz p1, :cond_63

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1999
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2000
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 2001
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 2002
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 2003
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 2004
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 2005
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2c
    .line 2006
    if-eqz p9, :cond_65

    #@2e
    .line 2007
    const/4 v4, 0x1

    #@2f
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 2008
    const/4 v4, 0x1

    #@33
    move-object/from16 v0, p9

    #@35
    invoke-virtual {v0, v1, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@38
    .line 2012
    :goto_38
    if-eqz p10, :cond_6a

    #@3a
    .line 2013
    const/4 v4, 0x1

    #@3b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 2014
    const/4 v4, 0x0

    #@3f
    move-object/from16 v0, p10

    #@41
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    .line 2018
    :goto_44
    move/from16 v0, p11

    #@46
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 2019
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@4b
    const/16 v5, 0x69

    #@4d
    const/4 v6, 0x0

    #@4e
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@51
    .line 2020
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@54
    .line 2021
    sget-object v4, Landroid/app/IActivityManager$WaitResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@56
    invoke-interface {v4, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@59
    move-result-object v3

    #@5a
    check-cast v3, Landroid/app/IActivityManager$WaitResult;

    #@5c
    .line 2022
    .local v3, result:Landroid/app/IActivityManager$WaitResult;
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@5f
    .line 2023
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@62
    .line 2024
    return-object v3

    #@63
    .line 1998
    .end local v3           #result:Landroid/app/IActivityManager$WaitResult;
    :cond_63
    const/4 v4, 0x0

    #@64
    goto :goto_13

    #@65
    .line 2010
    :cond_65
    const/4 v4, 0x0

    #@66
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@69
    goto :goto_38

    #@6a
    .line 2016
    :cond_6a
    const/4 v4, 0x0

    #@6b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6e
    goto :goto_44
.end method

.method public startActivityAsUser(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)I
    .registers 19
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "startFlags"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "options"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1960
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 1961
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 1962
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1963
    if-eqz p1, :cond_5f

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 1964
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 1965
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 1966
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 1967
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 1968
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 1969
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 1970
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2c
    .line 1971
    if-eqz p9, :cond_61

    #@2e
    .line 1972
    const/4 v4, 0x1

    #@2f
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 1973
    const/4 v4, 0x1

    #@33
    move-object/from16 v0, p9

    #@35
    invoke-virtual {v0, v1, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@38
    .line 1977
    :goto_38
    if-eqz p10, :cond_66

    #@3a
    .line 1978
    const/4 v4, 0x1

    #@3b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 1979
    const/4 v4, 0x0

    #@3f
    move-object/from16 v0, p10

    #@41
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    .line 1983
    :goto_44
    move/from16 v0, p11

    #@46
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@49
    .line 1984
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@4b
    const/16 v5, 0x99

    #@4d
    const/4 v6, 0x0

    #@4e
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@51
    .line 1985
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@54
    .line 1986
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v3

    #@58
    .line 1987
    .local v3, result:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@5b
    .line 1988
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@5e
    .line 1989
    return v3

    #@5f
    .line 1963
    .end local v3           #result:I
    :cond_5f
    const/4 v4, 0x0

    #@60
    goto :goto_13

    #@61
    .line 1975
    :cond_61
    const/4 v4, 0x0

    #@62
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    goto :goto_38

    #@66
    .line 1981
    :cond_66
    const/4 v4, 0x0

    #@67
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    goto :goto_44
.end method

.method public startActivityIntentSender(Landroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I
    .registers 18
    .parameter "caller"
    .parameter "intent"
    .parameter "fillInIntent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "flagsMask"
    .parameter "flagsValues"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2059
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2060
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2061
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2062
    if-eqz p1, :cond_5a

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2063
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/IntentSender;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2064
    if-eqz p3, :cond_5c

    #@1c
    .line 2065
    const/4 v4, 0x1

    #@1d
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 2066
    const/4 v4, 0x0

    #@21
    invoke-virtual {p3, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 2070
    :goto_24
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@27
    .line 2071
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2a
    .line 2072
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 2073
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    .line 2074
    invoke-virtual {v1, p8}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 2075
    move/from16 v0, p9

    #@35
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 2076
    if-eqz p10, :cond_61

    #@3a
    .line 2077
    const/4 v4, 0x1

    #@3b
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 2078
    const/4 v4, 0x0

    #@3f
    move-object/from16 v0, p10

    #@41
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    .line 2082
    :goto_44
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@46
    const/16 v5, 0x64

    #@48
    const/4 v6, 0x0

    #@49
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@4c
    .line 2083
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@4f
    .line 2084
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@52
    move-result v3

    #@53
    .line 2085
    .local v3, result:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@56
    .line 2086
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@59
    .line 2087
    return v3

    #@5a
    .line 2062
    .end local v3           #result:I
    :cond_5a
    const/4 v4, 0x0

    #@5b
    goto :goto_13

    #@5c
    .line 2068
    :cond_5c
    const/4 v4, 0x0

    #@5d
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    goto :goto_24

    #@61
    .line 2080
    :cond_61
    const/4 v4, 0x0

    #@62
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@65
    goto :goto_44
.end method

.method public startActivityWithConfig(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/res/Configuration;Landroid/os/Bundle;I)I
    .registers 18
    .parameter "caller"
    .parameter "intent"
    .parameter "resolvedType"
    .parameter "resultTo"
    .parameter "resultWho"
    .parameter "requestCode"
    .parameter "startFlags"
    .parameter "config"
    .parameter "options"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2030
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 2031
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 2032
    .local v2, reply:Landroid/os/Parcel;
    const-string v4, "android.app.IActivityManager"

    #@a
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2033
    if-eqz p1, :cond_53

    #@f
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v4

    #@13
    :goto_13
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 2034
    const/4 v4, 0x0

    #@17
    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2035
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 2036
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 2037
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 2038
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 2039
    invoke-virtual {v1, p7}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 2040
    const/4 v4, 0x0

    #@2a
    invoke-virtual {p8, v1, v4}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@2d
    .line 2041
    if-eqz p9, :cond_55

    #@2f
    .line 2042
    const/4 v4, 0x1

    #@30
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 2043
    const/4 v4, 0x0

    #@34
    move-object/from16 v0, p9

    #@36
    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@39
    .line 2047
    :goto_39
    move/from16 v0, p10

    #@3b
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3e
    .line 2048
    iget-object v4, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@40
    const/4 v5, 0x3

    #@41
    const/4 v6, 0x0

    #@42
    invoke-interface {v4, v5, v1, v2, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@45
    .line 2049
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    #@48
    .line 2050
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    #@4b
    move-result v3

    #@4c
    .line 2051
    .local v3, result:I
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@4f
    .line 2052
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@52
    .line 2053
    return v3

    #@53
    .line 2033
    .end local v3           #result:I
    :cond_53
    const/4 v4, 0x0

    #@54
    goto :goto_13

    #@55
    .line 2045
    :cond_55
    const/4 v4, 0x0

    #@56
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@59
    goto :goto_39
.end method

.method public startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;I)Z
    .registers 12
    .parameter "className"
    .parameter "profileFile"
    .parameter "flags"
    .parameter "arguments"
    .parameter "watcher"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2925
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2926
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2927
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2928
    invoke-static {p1, v0}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@11
    .line 2929
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 2930
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2931
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@1a
    .line 2932
    if-eqz p5, :cond_3e

    #@1c
    invoke-interface {p5}, Landroid/app/IInstrumentationWatcher;->asBinder()Landroid/os/IBinder;

    #@1f
    move-result-object v3

    #@20
    :goto_20
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@23
    .line 2933
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    .line 2934
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@28
    const/16 v4, 0x2c

    #@2a
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2d
    .line 2935
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@30
    .line 2936
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v3

    #@34
    if-eqz v3, :cond_37

    #@36
    const/4 v2, 0x1

    #@37
    .line 2937
    .local v2, res:Z
    :cond_37
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 2938
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 2939
    return v2

    #@3e
    .line 2932
    .end local v2           #res:Z
    :cond_3e
    const/4 v3, 0x0

    #@3f
    goto :goto_20
.end method

.method public startNextMatchingActivity(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)Z
    .registers 11
    .parameter "callingActivity"
    .parameter "intent"
    .parameter "options"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2091
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 2092
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 2093
    .local v1, reply:Landroid/os/Parcel;
    const-string v5, "android.app.IActivityManager"

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 2094
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@12
    .line 2095
    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    .line 2096
    if-eqz p3, :cond_34

    #@17
    .line 2097
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2098
    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 2102
    :goto_1d
    iget-object v5, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v6, 0x43

    #@21
    invoke-interface {v5, v6, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2103
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2104
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2a
    move-result v2

    #@2b
    .line 2105
    .local v2, result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 2106
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 2107
    if-eqz v2, :cond_38

    #@33
    :goto_33
    return v3

    #@34
    .line 2100
    .end local v2           #result:I
    :cond_34
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    goto :goto_1d

    #@38
    .restart local v2       #result:I
    :cond_38
    move v3, v4

    #@39
    .line 2107
    goto :goto_33
.end method

.method public startRunning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "pkg"
    .parameter "cls"
    .parameter "action"
    .parameter "indata"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3369
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3370
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3371
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3372
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 3373
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 3374
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 3375
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 3376
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v3, 0x1

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 3377
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 3378
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 3379
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 3380
    return-void
.end method

.method public startService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/ComponentName;
    .registers 11
    .parameter "caller"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2719
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2720
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2721
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2722
    if-eqz p1, :cond_35

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2723
    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2724
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 2725
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 2726
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v4, 0x22

    #@24
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 2727
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 2728
    invoke-static {v1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@2d
    move-result-object v2

    #@2e
    .line 2729
    .local v2, res:Landroid/content/ComponentName;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 2730
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 2731
    return-object v2

    #@35
    .line 2722
    .end local v2           #res:Landroid/content/ComponentName;
    :cond_35
    const/4 v3, 0x0

    #@36
    goto :goto_14
.end method

.method public stopAppSwitches()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3544
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3545
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3546
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3547
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x58

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3548
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3549
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3550
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3551
    return-void
.end method

.method public stopService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)I
    .registers 11
    .parameter "caller"
    .parameter "service"
    .parameter "resolvedType"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 2736
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2737
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2738
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2739
    if-eqz p1, :cond_35

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v3

    #@14
    :goto_14
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2740
    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2741
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 2742
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 2743
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/16 v4, 0x23

    #@24
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 2744
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 2745
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v2

    #@2e
    .line 2746
    .local v2, res:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 2747
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 2748
    return v2

    #@35
    .line 2739
    .end local v2           #res:I
    :cond_35
    const/4 v3, 0x0

    #@36
    goto :goto_14
.end method

.method public stopServiceToken(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z
    .registers 9
    .parameter "className"
    .parameter "token"
    .parameter "startId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2752
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2753
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2754
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2755
    invoke-static {p1, v0}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@11
    .line 2756
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 2757
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 2758
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/16 v4, 0x30

    #@1b
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 2759
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 2760
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_28

    #@27
    const/4 v2, 0x1

    #@28
    .line 2761
    .local v2, res:Z
    :cond_28
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 2762
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 2763
    return v2
.end method

.method public stopUser(ILandroid/app/IStopUserCallback;)I
    .registers 9
    .parameter "userid"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3921
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3922
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3923
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3924
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 3925
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V

    #@13
    .line 3926
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v4, 0x9a

    #@17
    const/4 v5, 0x0

    #@18
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 3927
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 3928
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v2

    #@22
    .line 3929
    .local v2, result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3930
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3931
    return v2
.end method

.method public switchUser(I)Z
    .registers 7
    .parameter "userid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 3908
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 3909
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 3910
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 3911
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 3912
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x82

    #@15
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 3913
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 3914
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 3915
    .local v2, result:Z
    :cond_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 3916
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 3917
    return v2
.end method

.method public targetTaskAffinityMatchesActivity(Landroid/os/IBinder;Ljava/lang/String;)Z
    .registers 8
    .parameter "token"
    .parameter "destAffinity"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 4111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4112
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4113
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4114
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 4115
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 4116
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v4, 0x92

    #@18
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 4117
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 4118
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_25

    #@24
    const/4 v2, 0x1

    #@25
    .line 4119
    .local v2, result:Z
    :cond_25
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 4120
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 4121
    return v2
.end method

.method public testIsSystemReady()Z
    .registers 2

    #@0
    .prologue
    .line 3384
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V
    .registers 7
    .parameter "app"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2912
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2913
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2914
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2915
    invoke-virtual {p1, v0, v4}, Landroid/content/pm/ApplicationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 2916
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v3, 0x5c

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2917
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2918
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2919
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2920
    return-void
.end method

.method public unbindFinished(Landroid/os/IBinder;Landroid/content/Intent;Z)V
    .registers 9
    .parameter "token"
    .parameter "intent"
    .parameter "doRebind"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2835
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2836
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2837
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2838
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2839
    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 2840
    if-eqz p3, :cond_2b

    #@16
    const/4 v2, 0x1

    #@17
    :goto_17
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 2841
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/16 v4, 0x48

    #@1e
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@21
    .line 2842
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@24
    .line 2843
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 2844
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2845
    return-void

    #@2b
    :cond_2b
    move v2, v3

    #@2c
    .line 2840
    goto :goto_17
.end method

.method public unbindService(Landroid/app/IServiceConnection;)Z
    .registers 7
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2807
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2808
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2809
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2810
    invoke-interface {p1}, Landroid/app/IServiceConnection;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 2811
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@17
    const/16 v4, 0x25

    #@19
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 2812
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 2813
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@22
    move-result v3

    #@23
    if-eqz v3, :cond_26

    #@25
    const/4 v2, 0x1

    #@26
    .line 2814
    .local v2, res:Z
    :cond_26
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 2815
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 2816
    return v2
.end method

.method public unbroadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;I)V
    .registers 9
    .parameter "caller"
    .parameter "intent"
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2232
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2233
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2234
    if-eqz p1, :cond_2e

    #@10
    invoke-interface {p1}, Landroid/app/IApplicationThread;->asBinder()Landroid/os/IBinder;

    #@13
    move-result-object v2

    #@14
    :goto_14
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    .line 2235
    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 2236
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 2237
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1f
    const/16 v3, 0xf

    #@21
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 2238
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 2239
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 2240
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 2241
    return-void

    #@2e
    .line 2234
    :cond_2e
    const/4 v2, 0x0

    #@2f
    goto :goto_14
.end method

.method public unhandledBack()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3250
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3251
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3252
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x4

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 3253
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 3254
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 3255
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 3256
    return-void
.end method

.method public unregisterProcessObserver(Landroid/app/IProcessObserver;)V
    .registers 7
    .parameter "observer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4012
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4013
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4014
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4015
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/app/IProcessObserver;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 4016
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x86

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4017
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4018
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 4019
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4020
    return-void

    #@28
    .line 4015
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13
.end method

.method public unregisterReceiver(Landroid/content/IIntentReceiver;)V
    .registers 7
    .parameter "receiver"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2192
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2193
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2194
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2195
    invoke-interface {p1}, Landroid/content/IIntentReceiver;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 2196
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/16 v3, 0xd

    #@18
    const/4 v4, 0x0

    #@19
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1c
    .line 2197
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1f
    .line 2198
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 2199
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2200
    return-void
.end method

.method public unregisterUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V
    .registers 7
    .parameter "observer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 4171
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 4172
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 4173
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 4174
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Landroid/app/IUserSwitchObserver;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 4175
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x9c

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 4176
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 4177
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 4178
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 4179
    return-void

    #@28
    .line 4174
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13
.end method

.method public unstableProviderDied(Landroid/os/IBinder;)V
    .registers 7
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2667
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 2668
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 2669
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 2670
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 2671
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x97

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2672
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2673
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2674
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2675
    return-void
.end method

.method public updateConfiguration(Landroid/content/res/Configuration;)V
    .registers 7
    .parameter "values"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2969
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2970
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2971
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2972
    invoke-virtual {p1, v0, v4}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 2973
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v3, 0x2f

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2974
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2975
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 2976
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 2977
    return-void
.end method

.method public updatePersistentConfiguration(Landroid/content/res/Configuration;)V
    .registers 7
    .parameter "values"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4064
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 4065
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 4066
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 4067
    invoke-virtual {p1, v0, v4}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@11
    .line 4068
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v3, 0x88

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 4069
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 4070
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 4071
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 4072
    return-void
.end method

.method public wakingUp()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 3284
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 3285
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 3286
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.app.IActivityManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 3287
    iget-object v2, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x29

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 3288
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 3289
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 3290
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 3291
    return-void
.end method

.method public willActivityBeVisible(Landroid/os/IBinder;)Z
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2155
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 2156
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 2157
    .local v1, reply:Landroid/os/Parcel;
    const-string v3, "android.app.IActivityManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 2158
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@11
    .line 2159
    iget-object v3, p0, Landroid/app/ActivityManagerProxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x6a

    #@15
    invoke-interface {v3, v4, v0, v1, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 2160
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 2161
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v3

    #@1f
    if-eqz v3, :cond_22

    #@21
    const/4 v2, 0x1

    #@22
    .line 2162
    .local v2, res:Z
    :cond_22
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 2163
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 2164
    return v2
.end method
