.class Landroid/app/WallpaperManager$FastBitmapDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "WallpaperManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/WallpaperManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FastBitmapDrawable"
.end annotation


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private mDrawLeft:I

.field private mDrawTop:I

.field private final mHeight:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mWidth:I


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "bitmap"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 159
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@4
    .line 160
    iput-object p1, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@6
    .line 161
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mWidth:I

    #@c
    .line 162
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mHeight:I

    #@12
    .line 164
    iget v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mWidth:I

    #@14
    iget v1, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mHeight:I

    #@16
    invoke-virtual {p0, v2, v2, v0, v1}, Landroid/app/WallpaperManager$FastBitmapDrawable;->setBounds(IIII)V

    #@19
    .line 166
    new-instance v0, Landroid/graphics/Paint;

    #@1b
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    #@20
    .line 167
    iget-object v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    #@22
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    #@24
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    #@26
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@29
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@2c
    .line 168
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Bitmap;Landroid/app/WallpaperManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 151
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    #@3
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 172
    iget-object v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    iget v1, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mDrawLeft:I

    #@4
    int-to-float v1, v1

    #@5
    iget v2, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mDrawTop:I

    #@7
    int-to-float v2, v2

    #@8
    iget-object v3, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    #@a
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@d
    .line 173
    return-void
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 213
    iget v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mHeight:I

    #@2
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 208
    iget v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mWidth:I

    #@2
    return v0
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 223
    iget v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mHeight:I

    #@2
    return v0
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 218
    iget v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mWidth:I

    #@2
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 177
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public setAlpha(I)V
    .registers 4
    .parameter "alpha"

    #@0
    .prologue
    .line 188
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported with this drawable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setBounds(IIII)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 182
    sub-int v0, p3, p1

    #@2
    iget v1, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mWidth:I

    #@4
    sub-int/2addr v0, v1

    #@5
    div-int/lit8 v0, v0, 0x2

    #@7
    add-int/2addr v0, p1

    #@8
    iput v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mDrawLeft:I

    #@a
    .line 183
    sub-int v0, p4, p2

    #@c
    iget v1, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mHeight:I

    #@e
    sub-int/2addr v0, v1

    #@f
    div-int/lit8 v0, v0, 0x2

    #@11
    add-int/2addr v0, p2

    #@12
    iput v0, p0, Landroid/app/WallpaperManager$FastBitmapDrawable;->mDrawTop:I

    #@14
    .line 184
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 4
    .parameter "cf"

    #@0
    .prologue
    .line 193
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported with this drawable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setDither(Z)V
    .registers 4
    .parameter "dither"

    #@0
    .prologue
    .line 198
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported with this drawable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setFilterBitmap(Z)V
    .registers 4
    .parameter "filter"

    #@0
    .prologue
    .line 203
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported with this drawable"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
