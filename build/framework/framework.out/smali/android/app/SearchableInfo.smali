.class public final Landroid/app/SearchableInfo;
.super Ljava/lang/Object;
.source "SearchableInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/SearchableInfo$ActionKeyInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final DBG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "SearchableInfo"

.field private static final MD_LABEL_SEARCHABLE:Ljava/lang/String; = "android.app.searchable"

.field private static final MD_XML_ELEMENT_SEARCHABLE:Ljava/lang/String; = "searchable"

.field private static final MD_XML_ELEMENT_SEARCHABLE_ACTION_KEY:Ljava/lang/String; = "actionkey"

.field private static final SEARCH_MODE_BADGE_ICON:I = 0x8

.field private static final SEARCH_MODE_BADGE_LABEL:I = 0x4

.field private static final SEARCH_MODE_QUERY_REWRITE_FROM_DATA:I = 0x10

.field private static final SEARCH_MODE_QUERY_REWRITE_FROM_TEXT:I = 0x20

.field private static final VOICE_SEARCH_LAUNCH_RECOGNIZER:I = 0x4

.field private static final VOICE_SEARCH_LAUNCH_WEB_SEARCH:I = 0x2

.field private static final VOICE_SEARCH_SHOW_BUTTON:I = 0x1


# instance fields
.field private mActionKeys:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/app/SearchableInfo$ActionKeyInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mAutoUrlDetect:Z

.field private final mHintId:I

.field private final mIconId:I

.field private final mIncludeInGlobalSearch:Z

.field private final mLabelId:I

.field private final mQueryAfterZeroResults:Z

.field private final mSearchActivity:Landroid/content/ComponentName;

.field private final mSearchButtonText:I

.field private final mSearchImeOptions:I

.field private final mSearchInputType:I

.field private final mSearchMode:I

.field private final mSettingsDescriptionId:I

.field private final mSuggestAuthority:Ljava/lang/String;

.field private final mSuggestIntentAction:Ljava/lang/String;

.field private final mSuggestIntentData:Ljava/lang/String;

.field private final mSuggestPath:Ljava/lang/String;

.field private final mSuggestProviderPackage:Ljava/lang/String;

.field private final mSuggestSelection:Ljava/lang/String;

.field private final mSuggestThreshold:I

.field private final mVoiceLanguageId:I

.field private final mVoiceLanguageModeId:I

.field private final mVoiceMaxResults:I

.field private final mVoicePromptTextId:I

.field private final mVoiceSearchMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 788
    new-instance v0, Landroid/app/SearchableInfo$1;

    #@2
    invoke-direct {v0}, Landroid/app/SearchableInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/SearchableInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/ComponentName;)V
    .registers 12
    .parameter "activityContext"
    .parameter "attr"
    .parameter "cName"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 309
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 93
    const/4 v4, 0x0

    #@7
    iput-object v4, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@9
    .line 310
    iput-object p3, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@b
    .line 312
    sget-object v4, Lcom/android/internal/R$styleable;->Searchable:[I

    #@d
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v0

    #@11
    .line 314
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x3

    #@12
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@15
    move-result v4

    #@16
    iput v4, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@18
    .line 315
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@1b
    move-result v4

    #@1c
    iput v4, p0, Landroid/app/SearchableInfo;->mLabelId:I

    #@1e
    .line 316
    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@21
    move-result v4

    #@22
    iput v4, p0, Landroid/app/SearchableInfo;->mHintId:I

    #@24
    .line 317
    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@27
    move-result v4

    #@28
    iput v4, p0, Landroid/app/SearchableInfo;->mIconId:I

    #@2a
    .line 318
    const/16 v4, 0x9

    #@2c
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@2f
    move-result v4

    #@30
    iput v4, p0, Landroid/app/SearchableInfo;->mSearchButtonText:I

    #@32
    .line 320
    const/16 v4, 0xa

    #@34
    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    #@37
    move-result v4

    #@38
    iput v4, p0, Landroid/app/SearchableInfo;->mSearchInputType:I

    #@3a
    .line 323
    const/16 v4, 0x10

    #@3c
    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3f
    move-result v4

    #@40
    iput v4, p0, Landroid/app/SearchableInfo;->mSearchImeOptions:I

    #@42
    .line 325
    const/16 v4, 0x12

    #@44
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@47
    move-result v4

    #@48
    iput-boolean v4, p0, Landroid/app/SearchableInfo;->mIncludeInGlobalSearch:Z

    #@4a
    .line 327
    const/16 v4, 0x13

    #@4c
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@4f
    move-result v4

    #@50
    iput-boolean v4, p0, Landroid/app/SearchableInfo;->mQueryAfterZeroResults:Z

    #@52
    .line 329
    const/16 v4, 0x15

    #@54
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@57
    move-result v4

    #@58
    iput-boolean v4, p0, Landroid/app/SearchableInfo;->mAutoUrlDetect:Z

    #@5a
    .line 332
    const/16 v4, 0x14

    #@5c
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5f
    move-result v4

    #@60
    iput v4, p0, Landroid/app/SearchableInfo;->mSettingsDescriptionId:I

    #@62
    .line 334
    const/4 v4, 0x4

    #@63
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@66
    move-result-object v4

    #@67
    iput-object v4, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@69
    .line 336
    const/4 v4, 0x5

    #@6a
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v4

    #@6e
    iput-object v4, p0, Landroid/app/SearchableInfo;->mSuggestPath:Ljava/lang/String;

    #@70
    .line 338
    const/4 v4, 0x6

    #@71
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    iput-object v4, p0, Landroid/app/SearchableInfo;->mSuggestSelection:Ljava/lang/String;

    #@77
    .line 340
    const/4 v4, 0x7

    #@78
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    iput-object v4, p0, Landroid/app/SearchableInfo;->mSuggestIntentAction:Ljava/lang/String;

    #@7e
    .line 342
    const/16 v4, 0x8

    #@80
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@83
    move-result-object v4

    #@84
    iput-object v4, p0, Landroid/app/SearchableInfo;->mSuggestIntentData:Ljava/lang/String;

    #@86
    .line 344
    const/16 v4, 0x11

    #@88
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8b
    move-result v4

    #@8c
    iput v4, p0, Landroid/app/SearchableInfo;->mSuggestThreshold:I

    #@8e
    .line 347
    const/16 v4, 0xb

    #@90
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@93
    move-result v4

    #@94
    iput v4, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@96
    .line 350
    const/16 v4, 0xc

    #@98
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@9b
    move-result v4

    #@9c
    iput v4, p0, Landroid/app/SearchableInfo;->mVoiceLanguageModeId:I

    #@9e
    .line 352
    const/16 v4, 0xd

    #@a0
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@a3
    move-result v4

    #@a4
    iput v4, p0, Landroid/app/SearchableInfo;->mVoicePromptTextId:I

    #@a6
    .line 354
    const/16 v4, 0xe

    #@a8
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@ab
    move-result v4

    #@ac
    iput v4, p0, Landroid/app/SearchableInfo;->mVoiceLanguageId:I

    #@ae
    .line 356
    const/16 v4, 0xf

    #@b0
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@b3
    move-result v4

    #@b4
    iput v4, p0, Landroid/app/SearchableInfo;->mVoiceMaxResults:I

    #@b6
    .line 359
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@b9
    .line 362
    const/4 v3, 0x0

    #@ba
    .line 363
    .local v3, suggestProviderPackage:Ljava/lang/String;
    iget-object v4, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@bc
    if-eqz v4, :cond_cc

    #@be
    .line 364
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@c1
    move-result-object v2

    #@c2
    .line 365
    .local v2, pm:Landroid/content/pm/PackageManager;
    iget-object v4, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@c4
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    #@c7
    move-result-object v1

    #@c8
    .line 366
    .local v1, pi:Landroid/content/pm/ProviderInfo;
    if-eqz v1, :cond_cc

    #@ca
    .line 367
    iget-object v3, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@cc
    .line 370
    .end local v1           #pi:Landroid/content/pm/ProviderInfo;
    .end local v2           #pm:Landroid/content/pm/PackageManager;
    :cond_cc
    iput-object v3, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@ce
    .line 373
    iget v4, p0, Landroid/app/SearchableInfo;->mLabelId:I

    #@d0
    if-nez v4, :cond_da

    #@d2
    .line 374
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@d4
    const-string v5, "Search label must be a resource reference."

    #@d6
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d9
    throw v4

    #@da
    .line 376
    :cond_da
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 7
    .parameter "in"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 806
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 93
    iput-object v4, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@8
    .line 807
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v1

    #@c
    iput v1, p0, Landroid/app/SearchableInfo;->mLabelId:I

    #@e
    .line 808
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@14
    .line 809
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v1

    #@18
    iput v1, p0, Landroid/app/SearchableInfo;->mHintId:I

    #@1a
    .line 810
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v1

    #@1e
    iput v1, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@20
    .line 811
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v1

    #@24
    iput v1, p0, Landroid/app/SearchableInfo;->mIconId:I

    #@26
    .line 812
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v1

    #@2a
    iput v1, p0, Landroid/app/SearchableInfo;->mSearchButtonText:I

    #@2c
    .line 813
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v1

    #@30
    iput v1, p0, Landroid/app/SearchableInfo;->mSearchInputType:I

    #@32
    .line 814
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v1

    #@36
    iput v1, p0, Landroid/app/SearchableInfo;->mSearchImeOptions:I

    #@38
    .line 815
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_8d

    #@3e
    move v1, v2

    #@3f
    :goto_3f
    iput-boolean v1, p0, Landroid/app/SearchableInfo;->mIncludeInGlobalSearch:Z

    #@41
    .line 816
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v1

    #@45
    if-eqz v1, :cond_8f

    #@47
    move v1, v2

    #@48
    :goto_48
    iput-boolean v1, p0, Landroid/app/SearchableInfo;->mQueryAfterZeroResults:Z

    #@4a
    .line 817
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v1

    #@4e
    if-eqz v1, :cond_91

    #@50
    :goto_50
    iput-boolean v2, p0, Landroid/app/SearchableInfo;->mAutoUrlDetect:Z

    #@52
    .line 819
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v1

    #@56
    iput v1, p0, Landroid/app/SearchableInfo;->mSettingsDescriptionId:I

    #@58
    .line 820
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@5e
    .line 821
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestPath:Ljava/lang/String;

    #@64
    .line 822
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestSelection:Ljava/lang/String;

    #@6a
    .line 823
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestIntentAction:Ljava/lang/String;

    #@70
    .line 824
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestIntentData:Ljava/lang/String;

    #@76
    .line 825
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@79
    move-result v1

    #@7a
    iput v1, p0, Landroid/app/SearchableInfo;->mSuggestThreshold:I

    #@7c
    .line 827
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v0

    #@80
    .local v0, count:I
    :goto_80
    if-lez v0, :cond_93

    #@82
    .line 828
    new-instance v1, Landroid/app/SearchableInfo$ActionKeyInfo;

    #@84
    invoke-direct {v1, p1, v4}, Landroid/app/SearchableInfo$ActionKeyInfo;-><init>(Landroid/os/Parcel;Landroid/app/SearchableInfo$1;)V

    #@87
    invoke-direct {p0, v1}, Landroid/app/SearchableInfo;->addActionKey(Landroid/app/SearchableInfo$ActionKeyInfo;)V

    #@8a
    .line 827
    add-int/lit8 v0, v0, -0x1

    #@8c
    goto :goto_80

    #@8d
    .end local v0           #count:I
    :cond_8d
    move v1, v3

    #@8e
    .line 815
    goto :goto_3f

    #@8f
    :cond_8f
    move v1, v3

    #@90
    .line 816
    goto :goto_48

    #@91
    :cond_91
    move v2, v3

    #@92
    .line 817
    goto :goto_50

    #@93
    .line 831
    .restart local v0       #count:I
    :cond_93
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@96
    move-result-object v1

    #@97
    iput-object v1, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@99
    .line 833
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v1

    #@9d
    iput v1, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@9f
    .line 834
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v1

    #@a3
    iput v1, p0, Landroid/app/SearchableInfo;->mVoiceLanguageModeId:I

    #@a5
    .line 835
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v1

    #@a9
    iput v1, p0, Landroid/app/SearchableInfo;->mVoicePromptTextId:I

    #@ab
    .line 836
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@ae
    move-result v1

    #@af
    iput v1, p0, Landroid/app/SearchableInfo;->mVoiceLanguageId:I

    #@b1
    .line 837
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b4
    move-result v1

    #@b5
    iput v1, p0, Landroid/app/SearchableInfo;->mVoiceMaxResults:I

    #@b7
    .line 838
    return-void
.end method

.method private addActionKey(Landroid/app/SearchableInfo$ActionKeyInfo;)V
    .registers 4
    .parameter "keyInfo"

    #@0
    .prologue
    .line 499
    iget-object v0, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 500
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@b
    .line 502
    :cond_b
    iget-object v0, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@d
    invoke-virtual {p1}, Landroid/app/SearchableInfo$ActionKeyInfo;->getKeyCode()I

    #@10
    move-result v1

    #@11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 503
    return-void
.end method

.method private static createActivityContext(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Context;
    .registers 7
    .parameter "context"
    .parameter "activity"

    #@0
    .prologue
    .line 259
    const/4 v1, 0x0

    #@1
    .line 261
    .local v1, theirContext:Landroid/content/Context;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    const/4 v3, 0x0

    #@6
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_9
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_9} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_9} :catch_29

    #@9
    move-result-object v1

    #@a
    .line 268
    :goto_a
    return-object v1

    #@b
    .line 262
    :catch_b
    move-exception v0

    #@c
    .line 263
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "SearchableInfo"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Package not found "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_a

    #@29
    .line 264
    .end local v0           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_29
    move-exception v0

    #@2a
    .line 265
    .local v0, e:Ljava/lang/SecurityException;
    const-string v2, "SearchableInfo"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Can\'t make context for "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@46
    goto :goto_a
.end method

.method public static getActivityMetaData(Landroid/content/Context;Landroid/content/pm/ActivityInfo;I)Landroid/app/SearchableInfo;
    .registers 11
    .parameter "context"
    .parameter "activityInfo"
    .parameter "userId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 517
    const/4 v3, 0x0

    #@2
    .line 519
    .local v3, userContext:Landroid/content/Context;
    :try_start_2
    const-string/jumbo v5, "system"

    #@5
    const/4 v6, 0x0

    #@6
    new-instance v7, Landroid/os/UserHandle;

    #@8
    invoke-direct {v7, p2}, Landroid/os/UserHandle;-><init>(I)V

    #@b
    invoke-virtual {p0, v5, v6, v7}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_e} :catch_1c

    #@e
    move-result-object v3

    #@f
    .line 526
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@12
    move-result-object v5

    #@13
    const-string v6, "android.app.searchable"

    #@15
    invoke-virtual {p1, v5, v6}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@18
    move-result-object v4

    #@19
    .line 528
    .local v4, xml:Landroid/content/res/XmlResourceParser;
    if-nez v4, :cond_36

    #@1b
    .line 550
    .end local v4           #xml:Landroid/content/res/XmlResourceParser;
    :goto_1b
    return-object v2

    #@1c
    .line 521
    :catch_1c
    move-exception v1

    #@1d
    .line 522
    .local v1, nnfe:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "SearchableInfo"

    #@1f
    new-instance v6, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v7, "Couldn\'t create package context for user "

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1b

    #@36
    .line 531
    .end local v1           #nnfe:Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4       #xml:Landroid/content/res/XmlResourceParser;
    :cond_36
    new-instance v0, Landroid/content/ComponentName;

    #@38
    iget-object v5, p1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@3a
    iget-object v6, p1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@3c
    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    .line 533
    .local v0, cName:Landroid/content/ComponentName;
    invoke-static {v3, v4, v0}, Landroid/app/SearchableInfo;->getActivityMetaData(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    #@42
    move-result-object v2

    #@43
    .line 534
    .local v2, searchable:Landroid/app/SearchableInfo;
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    #@46
    goto :goto_1b
.end method

.method private static getActivityMetaData(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
    .registers 14
    .parameter "context"
    .parameter "xml"
    .parameter "cName"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 564
    const/4 v4, 0x0

    #@2
    .line 565
    .local v4, result:Landroid/app/SearchableInfo;
    invoke-static {p0, p2}, Landroid/app/SearchableInfo;->createActivityContext(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    .line 566
    .local v0, activityContext:Landroid/content/Context;
    if-nez v0, :cond_a

    #@8
    move-object v5, v7

    #@9
    .line 612
    :goto_9
    return-object v5

    #@a
    .line 571
    :cond_a
    :try_start_a
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_d
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a .. :try_end_d} :catch_b1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_d} :catch_d1

    #@d
    move-result v6

    #@e
    .local v6, tagType:I
    move-object v5, v4

    #@f
    .line 572
    .end local v4           #result:Landroid/app/SearchableInfo;
    .local v5, result:Landroid/app/SearchableInfo;
    :goto_f
    const/4 v8, 0x1

    #@10
    if-eq v6, v8, :cond_f1

    #@12
    .line 573
    const/4 v8, 0x2

    #@13
    if-ne v6, v8, :cond_fa

    #@15
    .line 574
    :try_start_15
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@18
    move-result-object v8

    #@19
    const-string/jumbo v9, "searchable"

    #@1c
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v8

    #@20
    if-eqz v8, :cond_61

    #@22
    .line 575
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    :try_end_25
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_15 .. :try_end_25} :catch_f7
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_25} :catch_f4

    #@25
    move-result-object v1

    #@26
    .line 576
    .local v1, attr:Landroid/util/AttributeSet;
    if-eqz v1, :cond_fa

    #@28
    .line 578
    :try_start_28
    new-instance v4, Landroid/app/SearchableInfo;

    #@2a
    invoke-direct {v4, v0, v1, p2}, Landroid/app/SearchableInfo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/ComponentName;)V
    :try_end_2d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_28 .. :try_end_2d} :catch_33
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_28 .. :try_end_2d} :catch_f7
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2d} :catch_f4

    #@2d
    .line 602
    .end local v1           #attr:Landroid/util/AttributeSet;
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    :goto_2d
    :try_start_2d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_30
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2d .. :try_end_30} :catch_b1
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_d1

    #@30
    move-result v6

    #@31
    move-object v5, v4

    #@32
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    goto :goto_f

    #@33
    .line 579
    .restart local v1       #attr:Landroid/util/AttributeSet;
    :catch_33
    move-exception v3

    #@34
    .line 580
    .local v3, ex:Ljava/lang/IllegalArgumentException;
    :try_start_34
    const-string v8, "SearchableInfo"

    #@36
    new-instance v9, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v10, "Invalid searchable metadata for "

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@44
    move-result-object v10

    #@45
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    const-string v10, ": "

    #@4b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v9

    #@4f
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@52
    move-result-object v10

    #@53
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v9

    #@5b
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    move-object v4, v5

    #@5f
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    move-object v5, v7

    #@60
    .line 582
    goto :goto_9

    #@61
    .line 585
    .end local v1           #attr:Landroid/util/AttributeSet;
    .end local v3           #ex:Ljava/lang/IllegalArgumentException;
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :cond_61
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    const-string v9, "actionkey"

    #@67
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v8

    #@6b
    if-eqz v8, :cond_fa

    #@6d
    .line 586
    if-nez v5, :cond_72

    #@6f
    move-object v4, v5

    #@70
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    move-object v5, v7

    #@71
    .line 588
    goto :goto_9

    #@72
    .line 590
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :cond_72
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
    :try_end_75
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_34 .. :try_end_75} :catch_f7
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_75} :catch_f4

    #@75
    move-result-object v1

    #@76
    .line 591
    .restart local v1       #attr:Landroid/util/AttributeSet;
    if-eqz v1, :cond_fa

    #@78
    .line 593
    :try_start_78
    new-instance v8, Landroid/app/SearchableInfo$ActionKeyInfo;

    #@7a
    invoke-direct {v8, v0, v1}, Landroid/app/SearchableInfo$ActionKeyInfo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@7d
    invoke-direct {v5, v8}, Landroid/app/SearchableInfo;->addActionKey(Landroid/app/SearchableInfo$ActionKeyInfo;)V
    :try_end_80
    .catch Ljava/lang/IllegalArgumentException; {:try_start_78 .. :try_end_80} :catch_82
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_78 .. :try_end_80} :catch_f7
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_80} :catch_f4

    #@80
    move-object v4, v5

    #@81
    .line 598
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    goto :goto_2d

    #@82
    .line 594
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :catch_82
    move-exception v3

    #@83
    .line 595
    .restart local v3       #ex:Ljava/lang/IllegalArgumentException;
    :try_start_83
    const-string v8, "SearchableInfo"

    #@85
    new-instance v9, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v10, "Invalid action key for "

    #@8c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v9

    #@90
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@93
    move-result-object v10

    #@94
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v9

    #@98
    const-string v10, ": "

    #@9a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v9

    #@9e
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v9

    #@a6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v9

    #@aa
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ad
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_83 .. :try_end_ad} :catch_f7
    .catch Ljava/io/IOException; {:try_start_83 .. :try_end_ad} :catch_f4

    #@ad
    move-object v4, v5

    #@ae
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    move-object v5, v7

    #@af
    .line 597
    goto/16 :goto_9

    #@b1
    .line 604
    .end local v1           #attr:Landroid/util/AttributeSet;
    .end local v3           #ex:Ljava/lang/IllegalArgumentException;
    .end local v6           #tagType:I
    :catch_b1
    move-exception v2

    #@b2
    .line 605
    .local v2, e:Lorg/xmlpull/v1/XmlPullParserException;
    :goto_b2
    const-string v8, "SearchableInfo"

    #@b4
    new-instance v9, Ljava/lang/StringBuilder;

    #@b6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b9
    const-string v10, "Reading searchable metadata for "

    #@bb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v9

    #@bf
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@c2
    move-result-object v10

    #@c3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v9

    #@c7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v9

    #@cb
    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ce
    move-object v5, v7

    #@cf
    .line 606
    goto/16 :goto_9

    #@d1
    .line 607
    .end local v2           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_d1
    move-exception v2

    #@d2
    .line 608
    .local v2, e:Ljava/io/IOException;
    :goto_d2
    const-string v8, "SearchableInfo"

    #@d4
    new-instance v9, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v10, "Reading searchable metadata for "

    #@db
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v9

    #@df
    invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@e2
    move-result-object v10

    #@e3
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v9

    #@e7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v9

    #@eb
    invoke-static {v8, v9, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ee
    move-object v5, v7

    #@ef
    .line 609
    goto/16 :goto_9

    #@f1
    .end local v2           #e:Ljava/io/IOException;
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    .restart local v6       #tagType:I
    :cond_f1
    move-object v4, v5

    #@f2
    .line 612
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    goto/16 :goto_9

    #@f4
    .line 607
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :catch_f4
    move-exception v2

    #@f5
    move-object v4, v5

    #@f6
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    goto :goto_d2

    #@f7
    .line 604
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :catch_f7
    move-exception v2

    #@f8
    move-object v4, v5

    #@f9
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    goto :goto_b2

    #@fa
    .end local v4           #result:Landroid/app/SearchableInfo;
    .restart local v5       #result:Landroid/app/SearchableInfo;
    :cond_fa
    move-object v4, v5

    #@fb
    .end local v5           #result:Landroid/app/SearchableInfo;
    .restart local v4       #result:Landroid/app/SearchableInfo;
    goto/16 :goto_2d
.end method


# virtual methods
.method public autoUrlDetect()Z
    .registers 2

    #@0
    .prologue
    .line 782
    iget-boolean v0, p0, Landroid/app/SearchableInfo;->mAutoUrlDetect:Z

    #@2
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 841
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public findActionKey(I)Landroid/app/SearchableInfo$ActionKeyInfo;
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 492
    iget-object v0, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 493
    const/4 v0, 0x0

    #@5
    .line 495
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/app/SearchableInfo$ActionKeyInfo;

    #@12
    goto :goto_5
.end method

.method public getActivityContext(Landroid/content/Context;)Landroid/content/Context;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 252
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@2
    invoke-static {p1, v0}, Landroid/app/SearchableInfo;->createActivityContext(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Context;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getHintId()I
    .registers 2

    #@0
    .prologue
    .line 636
    iget v0, p0, Landroid/app/SearchableInfo;->mHintId:I

    #@2
    return v0
.end method

.method public getIconId()I
    .registers 2

    #@0
    .prologue
    .line 649
    iget v0, p0, Landroid/app/SearchableInfo;->mIconId:I

    #@2
    return v0
.end method

.method public getImeOptions()I
    .registers 2

    #@0
    .prologue
    .line 752
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchImeOptions:I

    #@2
    return v0
.end method

.method public getInputType()I
    .registers 2

    #@0
    .prologue
    .line 740
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchInputType:I

    #@2
    return v0
.end method

.method public getLabelId()I
    .registers 2

    #@0
    .prologue
    .line 625
    iget v0, p0, Landroid/app/SearchableInfo;->mLabelId:I

    #@2
    return v0
.end method

.method public getProviderContext(Landroid/content/Context;Landroid/content/Context;)Landroid/content/Context;
    .registers 6
    .parameter "context"
    .parameter "activityContext"

    #@0
    .prologue
    .line 281
    const/4 v0, 0x0

    #@1
    .line 282
    .local v0, theirContext:Landroid/content/Context;
    iget-object v1, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@3
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_10

    #@f
    .line 294
    .end local p2
    :goto_f
    return-object p2

    #@10
    .line 285
    .restart local p2
    :cond_10
    iget-object v1, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@12
    if-eqz v1, :cond_1b

    #@14
    .line 287
    :try_start_14
    iget-object v1, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@16
    const/4 v2, 0x0

    #@17
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_1a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14 .. :try_end_1a} :catch_1f
    .catch Ljava/lang/SecurityException; {:try_start_14 .. :try_end_1a} :catch_1d

    #@1a
    move-result-object v0

    #@1b
    :cond_1b
    :goto_1b
    move-object p2, v0

    #@1c
    .line 294
    goto :goto_f

    #@1d
    .line 290
    :catch_1d
    move-exception v1

    #@1e
    goto :goto_1b

    #@1f
    .line 288
    :catch_1f
    move-exception v1

    #@20
    goto :goto_1b
.end method

.method public getSearchActivity()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@2
    return-object v0
.end method

.method public getSearchButtonText()I
    .registers 2

    #@0
    .prologue
    .line 728
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchButtonText:I

    #@2
    return v0
.end method

.method public getSettingsDescriptionId()I
    .registers 2

    #@0
    .prologue
    .line 180
    iget v0, p0, Landroid/app/SearchableInfo;->mSettingsDescriptionId:I

    #@2
    return v0
.end method

.method public getSuggestAuthority()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestIntentAction()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestIntentAction:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestIntentData()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestIntentData:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestPackage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestSelection()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 199
    iget-object v0, p0, Landroid/app/SearchableInfo;->mSuggestSelection:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSuggestThreshold()I
    .registers 2

    #@0
    .prologue
    .line 241
    iget v0, p0, Landroid/app/SearchableInfo;->mSuggestThreshold:I

    #@2
    return v0
.end method

.method public getVoiceLanguageId()I
    .registers 2

    #@0
    .prologue
    .line 706
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceLanguageId:I

    #@2
    return v0
.end method

.method public getVoiceLanguageModeId()I
    .registers 2

    #@0
    .prologue
    .line 686
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceLanguageModeId:I

    #@2
    return v0
.end method

.method public getVoiceMaxResults()I
    .registers 2

    #@0
    .prologue
    .line 717
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceMaxResults:I

    #@2
    return v0
.end method

.method public getVoicePromptTextId()I
    .registers 2

    #@0
    .prologue
    .line 696
    iget v0, p0, Landroid/app/SearchableInfo;->mVoicePromptTextId:I

    #@2
    return v0
.end method

.method public getVoiceSearchEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 658
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getVoiceSearchLaunchRecognizer()Z
    .registers 2

    #@0
    .prologue
    .line 676
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getVoiceSearchLaunchWebSearch()Z
    .registers 2

    #@0
    .prologue
    .line 667
    iget v0, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public queryAfterZeroResults()Z
    .registers 2

    #@0
    .prologue
    .line 773
    iget-boolean v0, p0, Landroid/app/SearchableInfo;->mQueryAfterZeroResults:Z

    #@2
    return v0
.end method

.method public shouldIncludeInGlobalSearch()Z
    .registers 2

    #@0
    .prologue
    .line 763
    iget-boolean v0, p0, Landroid/app/SearchableInfo;->mIncludeInGlobalSearch:Z

    #@2
    return v0
.end method

.method public shouldRewriteQueryFromData()Z
    .registers 2

    #@0
    .prologue
    .line 161
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x10

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public shouldRewriteQueryFromText()Z
    .registers 2

    #@0
    .prologue
    .line 170
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x20

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public useBadgeIcon()Z
    .registers 2

    #@0
    .prologue
    .line 152
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x8

    #@4
    if-eqz v0, :cond_c

    #@6
    iget v0, p0, Landroid/app/SearchableInfo;->mIconId:I

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public useBadgeLabel()Z
    .registers 2

    #@0
    .prologue
    .line 141
    iget v0, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@2
    and-int/lit8 v0, v0, 0x4

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 845
    iget v2, p0, Landroid/app/SearchableInfo;->mLabelId:I

    #@4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 846
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSearchActivity:Landroid/content/ComponentName;

    #@9
    invoke-virtual {v2, p1, p2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@c
    .line 847
    iget v2, p0, Landroid/app/SearchableInfo;->mHintId:I

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 848
    iget v2, p0, Landroid/app/SearchableInfo;->mSearchMode:I

    #@13
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 849
    iget v2, p0, Landroid/app/SearchableInfo;->mIconId:I

    #@18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 850
    iget v2, p0, Landroid/app/SearchableInfo;->mSearchButtonText:I

    #@1d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 851
    iget v2, p0, Landroid/app/SearchableInfo;->mSearchInputType:I

    #@22
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 852
    iget v2, p0, Landroid/app/SearchableInfo;->mSearchImeOptions:I

    #@27
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 853
    iget-boolean v2, p0, Landroid/app/SearchableInfo;->mIncludeInGlobalSearch:Z

    #@2c
    if-eqz v2, :cond_8a

    #@2e
    move v2, v3

    #@2f
    :goto_2f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 854
    iget-boolean v2, p0, Landroid/app/SearchableInfo;->mQueryAfterZeroResults:Z

    #@34
    if-eqz v2, :cond_8c

    #@36
    move v2, v3

    #@37
    :goto_37
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@3a
    .line 855
    iget-boolean v2, p0, Landroid/app/SearchableInfo;->mAutoUrlDetect:Z

    #@3c
    if-eqz v2, :cond_8e

    #@3e
    :goto_3e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@41
    .line 857
    iget v2, p0, Landroid/app/SearchableInfo;->mSettingsDescriptionId:I

    #@43
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@46
    .line 858
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestAuthority:Ljava/lang/String;

    #@48
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4b
    .line 859
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestPath:Ljava/lang/String;

    #@4d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@50
    .line 860
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestSelection:Ljava/lang/String;

    #@52
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@55
    .line 861
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestIntentAction:Ljava/lang/String;

    #@57
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5a
    .line 862
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestIntentData:Ljava/lang/String;

    #@5c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5f
    .line 863
    iget v2, p0, Landroid/app/SearchableInfo;->mSuggestThreshold:I

    #@61
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    .line 865
    iget-object v2, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@66
    if-nez v2, :cond_90

    #@68
    .line 866
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6b
    .line 874
    :cond_6b
    iget-object v2, p0, Landroid/app/SearchableInfo;->mSuggestProviderPackage:Ljava/lang/String;

    #@6d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@70
    .line 876
    iget v2, p0, Landroid/app/SearchableInfo;->mVoiceSearchMode:I

    #@72
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@75
    .line 877
    iget v2, p0, Landroid/app/SearchableInfo;->mVoiceLanguageModeId:I

    #@77
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    .line 878
    iget v2, p0, Landroid/app/SearchableInfo;->mVoicePromptTextId:I

    #@7c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    .line 879
    iget v2, p0, Landroid/app/SearchableInfo;->mVoiceLanguageId:I

    #@81
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@84
    .line 880
    iget v2, p0, Landroid/app/SearchableInfo;->mVoiceMaxResults:I

    #@86
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@89
    .line 881
    return-void

    #@8a
    :cond_8a
    move v2, v4

    #@8b
    .line 853
    goto :goto_2f

    #@8c
    :cond_8c
    move v2, v4

    #@8d
    .line 854
    goto :goto_37

    #@8e
    :cond_8e
    move v3, v4

    #@8f
    .line 855
    goto :goto_3e

    #@90
    .line 868
    :cond_90
    iget-object v2, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@92
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@95
    move-result v2

    #@96
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@99
    .line 869
    iget-object v2, p0, Landroid/app/SearchableInfo;->mActionKeys:Ljava/util/HashMap;

    #@9b
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@9e
    move-result-object v2

    #@9f
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a2
    move-result-object v1

    #@a3
    .local v1, i$:Ljava/util/Iterator;
    :goto_a3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a6
    move-result v2

    #@a7
    if-eqz v2, :cond_6b

    #@a9
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ac
    move-result-object v0

    #@ad
    check-cast v0, Landroid/app/SearchableInfo$ActionKeyInfo;

    #@af
    .line 870
    .local v0, actionKey:Landroid/app/SearchableInfo$ActionKeyInfo;
    invoke-virtual {v0, p1, p2}, Landroid/app/SearchableInfo$ActionKeyInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@b2
    goto :goto_a3
.end method
