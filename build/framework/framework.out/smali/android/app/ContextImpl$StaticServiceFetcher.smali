.class abstract Landroid/app/ContextImpl$StaticServiceFetcher;
.super Landroid/app/ContextImpl$ServiceFetcher;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "StaticServiceFetcher"
.end annotation


# instance fields
.field private mCachedInstance:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 276
    invoke-direct {p0}, Landroid/app/ContextImpl$ServiceFetcher;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public abstract createStaticService()Ljava/lang/Object;
.end method

.method public final getService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 4
    .parameter "unused"

    #@0
    .prologue
    .line 281
    monitor-enter p0

    #@1
    .line 282
    :try_start_1
    iget-object v0, p0, Landroid/app/ContextImpl$StaticServiceFetcher;->mCachedInstance:Ljava/lang/Object;

    #@3
    .line 283
    .local v0, service:Ljava/lang/Object;
    if-eqz v0, :cond_7

    #@5
    .line 284
    monitor-exit p0

    #@6
    .line 286
    .end local v0           #service:Ljava/lang/Object;
    :goto_6
    return-object v0

    #@7
    .restart local v0       #service:Ljava/lang/Object;
    :cond_7
    invoke-virtual {p0}, Landroid/app/ContextImpl$StaticServiceFetcher;->createStaticService()Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    .end local v0           #service:Ljava/lang/Object;
    iput-object v0, p0, Landroid/app/ContextImpl$StaticServiceFetcher;->mCachedInstance:Ljava/lang/Object;

    #@d
    monitor-exit p0

    #@e
    goto :goto_6

    #@f
    .line 287
    :catchall_f
    move-exception v1

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v1
.end method
