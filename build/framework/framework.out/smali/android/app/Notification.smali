.class public Landroid/app/Notification;
.super Ljava/lang/Object;
.source "Notification.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Notification$InboxStyle;,
        Landroid/app/Notification$BigTextStyle;,
        Landroid/app/Notification$BigPictureStyle;,
        Landroid/app/Notification$Style;,
        Landroid/app/Notification$Builder;,
        Landroid/app/Notification$Action;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALL:I = -0x1

.field public static final DEFAULT_LIGHTS:I = 0x4

.field public static final DEFAULT_SOUND:I = 0x1

.field public static final DEFAULT_VIBRATE:I = 0x2

.field public static final EXTRA_PEOPLE:Ljava/lang/String; = "android.people"

.field public static final FLAG_AUTO_CANCEL:I = 0x10

.field public static final FLAG_CALL_BGCOLOR:I = 0x200

.field public static final FLAG_FOREGROUND_SERVICE:I = 0x40

.field public static final FLAG_HIGH_PRIORITY:I = 0x80

.field public static final FLAG_INSISTENT:I = 0x4

.field public static final FLAG_NO_CLEAR:I = 0x20

.field public static final FLAG_NO_DISPLAY_ON_GOING_EVENT:I = 0x100

.field public static final FLAG_ONGOING_EVENT:I = 0x2

.field public static final FLAG_ONLY_ALERT_ONCE:I = 0x8

.field public static final FLAG_RINGING_BGCOLOR:I = 0x2000

.field public static final FLAG_SHOW_LIGHTS:I = 0x1

.field public static final FLAG_VOICERECORDER_BGCOLOR:I = 0x400

.field public static final FLAG_WIFI_SCREEN_BGCOLOR:I = 0x800

.field public static final FLAG_WIFI_SHARE_BGCOLOR:I = 0x1000

.field public static final KIND_CALL:Ljava/lang/String; = "android.call"

.field public static final KIND_EMAIL:Ljava/lang/String; = "android.email"

.field public static final KIND_EVENT:Ljava/lang/String; = "android.event"

.field public static final KIND_MESSAGE:Ljava/lang/String; = "android.message"

.field public static final KIND_PROMO:Ljava/lang/String; = "android.promo"

.field public static final PRIORITY_DEFAULT:I = 0x0

.field public static final PRIORITY_HIGH:I = 0x1

.field public static final PRIORITY_LOW:I = -0x1

.field public static final PRIORITY_MAX:I = 0x2

.field public static final PRIORITY_MIN:I = -0x2

.field public static final STREAM_DEFAULT:I = -0x1


# instance fields
.field private actions:[Landroid/app/Notification$Action;

.field public audioStreamType:I

.field public bigContentView:Landroid/widget/RemoteViews;

.field public contentIntent:Landroid/app/PendingIntent;

.field public contentView:Landroid/widget/RemoteViews;

.field public defaults:I

.field public deleteIntent:Landroid/app/PendingIntent;

.field private extras:Landroid/os/Bundle;

.field public flags:I

.field public fullScreenIntent:Landroid/app/PendingIntent;

.field public icon:I

.field public iconLevel:I

.field public kind:[Ljava/lang/String;

.field public largeIcon:Landroid/graphics/Bitmap;

.field public ledARGB:I

.field public ledOffMS:I

.field public ledOnMS:I

.field public number:I

.field public priority:I

.field public sound:Landroid/net/Uri;

.field public tickerText:Ljava/lang/CharSequence;

.field public tickerView:Landroid/widget/RemoteViews;

.field public vibrate:[J

.field public when:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 800
    new-instance v0, Landroid/app/Notification$1;

    #@2
    invoke-direct {v0}, Landroid/app/Notification$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/Notification;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 537
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 230
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/app/Notification;->audioStreamType:I

    #@6
    .line 538
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@9
    move-result-wide v0

    #@a
    iput-wide v0, p0, Landroid/app/Notification;->when:J

    #@c
    .line 539
    const/4 v0, 0x0

    #@d
    iput v0, p0, Landroid/app/Notification;->priority:I

    #@f
    .line 540
    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;J)V
    .registers 6
    .parameter "icon"
    .parameter "tickerText"
    .parameter "when"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 569
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 230
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/app/Notification;->audioStreamType:I

    #@6
    .line 570
    iput p1, p0, Landroid/app/Notification;->icon:I

    #@8
    .line 571
    iput-object p2, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@a
    .line 572
    iput-wide p3, p0, Landroid/app/Notification;->when:J

    #@c
    .line 573
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/CharSequence;JLjava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "icon"
    .parameter "tickerText"
    .parameter "when"
    .parameter "contentTitle"
    .parameter "contentText"
    .parameter "contentIntent"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 547
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 230
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/app/Notification;->audioStreamType:I

    #@7
    .line 548
    iput-wide p4, p0, Landroid/app/Notification;->when:J

    #@9
    .line 549
    iput p2, p0, Landroid/app/Notification;->icon:I

    #@b
    .line 550
    iput-object p3, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@d
    .line 551
    invoke-static {p1, v1, p8, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {p0, p1, p6, p7, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@14
    .line 553
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "parcel"

    #@0
    .prologue
    .line 579
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 230
    const/4 v1, -0x1

    #@4
    iput v1, p0, Landroid/app/Notification;->audioStreamType:I

    #@6
    .line 580
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    .line 582
    .local v0, version:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@d
    move-result-wide v1

    #@e
    iput-wide v1, p0, Landroid/app/Notification;->when:J

    #@10
    .line 583
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v1

    #@14
    iput v1, p0, Landroid/app/Notification;->icon:I

    #@16
    .line 584
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Landroid/app/Notification;->number:I

    #@1c
    .line 585
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2c

    #@22
    .line 586
    sget-object v1, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Landroid/app/PendingIntent;

    #@2a
    iput-object v1, p0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@2c
    .line 588
    :cond_2c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_3c

    #@32
    .line 589
    sget-object v1, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@34
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@37
    move-result-object v1

    #@38
    check-cast v1, Landroid/app/PendingIntent;

    #@3a
    iput-object v1, p0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@3c
    .line 591
    :cond_3c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v1

    #@40
    if-eqz v1, :cond_4c

    #@42
    .line 592
    sget-object v1, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@44
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@47
    move-result-object v1

    #@48
    check-cast v1, Ljava/lang/CharSequence;

    #@4a
    iput-object v1, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@4c
    .line 594
    :cond_4c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_5c

    #@52
    .line 595
    sget-object v1, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@54
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@57
    move-result-object v1

    #@58
    check-cast v1, Landroid/widget/RemoteViews;

    #@5a
    iput-object v1, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@5c
    .line 597
    :cond_5c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v1

    #@60
    if-eqz v1, :cond_6c

    #@62
    .line 598
    sget-object v1, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@64
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@67
    move-result-object v1

    #@68
    check-cast v1, Landroid/widget/RemoteViews;

    #@6a
    iput-object v1, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@6c
    .line 600
    :cond_6c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v1

    #@70
    if-eqz v1, :cond_7c

    #@72
    .line 601
    sget-object v1, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@74
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@77
    move-result-object v1

    #@78
    check-cast v1, Landroid/graphics/Bitmap;

    #@7a
    iput-object v1, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@7c
    .line 603
    :cond_7c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v1

    #@80
    iput v1, p0, Landroid/app/Notification;->defaults:I

    #@82
    .line 604
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v1

    #@86
    iput v1, p0, Landroid/app/Notification;->flags:I

    #@88
    .line 605
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8b
    move-result v1

    #@8c
    if-eqz v1, :cond_98

    #@8e
    .line 606
    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@90
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@93
    move-result-object v1

    #@94
    check-cast v1, Landroid/net/Uri;

    #@96
    iput-object v1, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@98
    .line 609
    :cond_98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9b
    move-result v1

    #@9c
    iput v1, p0, Landroid/app/Notification;->audioStreamType:I

    #@9e
    .line 610
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    #@a1
    move-result-object v1

    #@a2
    iput-object v1, p0, Landroid/app/Notification;->vibrate:[J

    #@a4
    .line 611
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a7
    move-result v1

    #@a8
    iput v1, p0, Landroid/app/Notification;->ledARGB:I

    #@aa
    .line 612
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@ad
    move-result v1

    #@ae
    iput v1, p0, Landroid/app/Notification;->ledOnMS:I

    #@b0
    .line 613
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b3
    move-result v1

    #@b4
    iput v1, p0, Landroid/app/Notification;->ledOffMS:I

    #@b6
    .line 614
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v1

    #@ba
    iput v1, p0, Landroid/app/Notification;->iconLevel:I

    #@bc
    .line 616
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@bf
    move-result v1

    #@c0
    if-eqz v1, :cond_cc

    #@c2
    .line 617
    sget-object v1, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c4
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c7
    move-result-object v1

    #@c8
    check-cast v1, Landroid/app/PendingIntent;

    #@ca
    iput-object v1, p0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@cc
    .line 620
    :cond_cc
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@cf
    move-result v1

    #@d0
    iput v1, p0, Landroid/app/Notification;->priority:I

    #@d2
    .line 622
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@d5
    move-result-object v1

    #@d6
    iput-object v1, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@d8
    .line 624
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@db
    move-result v1

    #@dc
    if-eqz v1, :cond_e4

    #@de
    .line 625
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@e1
    move-result-object v1

    #@e2
    iput-object v1, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@e4
    .line 628
    :cond_e4
    sget-object v1, Landroid/app/Notification$Action;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@e9
    move-result-object v1

    #@ea
    check-cast v1, [Landroid/app/Notification$Action;

    #@ec
    iput-object v1, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@ee
    .line 629
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v1

    #@f2
    if-eqz v1, :cond_fe

    #@f4
    .line 630
    sget-object v1, Landroid/widget/RemoteViews;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f6
    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f9
    move-result-object v1

    #@fa
    check-cast v1, Landroid/widget/RemoteViews;

    #@fc
    iput-object v1, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@fe
    .line 632
    :cond_fe
    return-void
.end method

.method static synthetic access$102(Landroid/app/Notification;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/app/Notification;)[Landroid/app/Notification$Action;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@2
    return-object v0
.end method

.method static synthetic access$202(Landroid/app/Notification;[Landroid/app/Notification$Action;)[Landroid/app/Notification$Action;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@2
    return-object p1
.end method


# virtual methods
.method public clone()Landroid/app/Notification;
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 636
    new-instance v2, Landroid/app/Notification;

    #@3
    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    #@6
    .line 638
    .local v2, that:Landroid/app/Notification;
    iget-wide v7, p0, Landroid/app/Notification;->when:J

    #@8
    iput-wide v7, v2, Landroid/app/Notification;->when:J

    #@a
    .line 639
    iget v7, p0, Landroid/app/Notification;->icon:I

    #@c
    iput v7, v2, Landroid/app/Notification;->icon:I

    #@e
    .line 640
    iget v7, p0, Landroid/app/Notification;->number:I

    #@10
    iput v7, v2, Landroid/app/Notification;->number:I

    #@12
    .line 643
    iget-object v7, p0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@14
    iput-object v7, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@16
    .line 644
    iget-object v7, p0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@18
    iput-object v7, v2, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@1a
    .line 645
    iget-object v7, p0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@1c
    iput-object v7, v2, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@1e
    .line 647
    iget-object v7, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@20
    if-eqz v7, :cond_2a

    #@22
    .line 648
    iget-object v7, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@24
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@27
    move-result-object v7

    #@28
    iput-object v7, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@2a
    .line 650
    :cond_2a
    iget-object v7, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@2c
    if-eqz v7, :cond_36

    #@2e
    .line 651
    iget-object v7, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@30
    invoke-virtual {v7}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@33
    move-result-object v7

    #@34
    iput-object v7, v2, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@36
    .line 653
    :cond_36
    iget-object v7, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@38
    if-eqz v7, :cond_42

    #@3a
    .line 654
    iget-object v7, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@3c
    invoke-virtual {v7}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@3f
    move-result-object v7

    #@40
    iput-object v7, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@42
    .line 656
    :cond_42
    iget-object v7, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@44
    if-eqz v7, :cond_4e

    #@46
    .line 657
    iget-object v7, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@48
    invoke-static {v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@4b
    move-result-object v7

    #@4c
    iput-object v7, v2, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@4e
    .line 659
    :cond_4e
    iget v7, p0, Landroid/app/Notification;->iconLevel:I

    #@50
    iput v7, v2, Landroid/app/Notification;->iconLevel:I

    #@52
    .line 660
    iget-object v7, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@54
    iput-object v7, v2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@56
    .line 661
    iget v7, p0, Landroid/app/Notification;->audioStreamType:I

    #@58
    iput v7, v2, Landroid/app/Notification;->audioStreamType:I

    #@5a
    .line 663
    iget-object v6, p0, Landroid/app/Notification;->vibrate:[J

    #@5c
    .line 664
    .local v6, vibrate:[J
    if-eqz v6, :cond_66

    #@5e
    .line 665
    array-length v0, v6

    #@5f
    .line 666
    .local v0, N:I
    new-array v5, v0, [J

    #@61
    iput-object v5, v2, Landroid/app/Notification;->vibrate:[J

    #@63
    .line 667
    .local v5, vib:[J
    invoke-static {v6, v9, v5, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@66
    .line 670
    .end local v0           #N:I
    .end local v5           #vib:[J
    :cond_66
    iget v7, p0, Landroid/app/Notification;->ledARGB:I

    #@68
    iput v7, v2, Landroid/app/Notification;->ledARGB:I

    #@6a
    .line 671
    iget v7, p0, Landroid/app/Notification;->ledOnMS:I

    #@6c
    iput v7, v2, Landroid/app/Notification;->ledOnMS:I

    #@6e
    .line 672
    iget v7, p0, Landroid/app/Notification;->ledOffMS:I

    #@70
    iput v7, v2, Landroid/app/Notification;->ledOffMS:I

    #@72
    .line 673
    iget v7, p0, Landroid/app/Notification;->defaults:I

    #@74
    iput v7, v2, Landroid/app/Notification;->defaults:I

    #@76
    .line 675
    iget v7, p0, Landroid/app/Notification;->flags:I

    #@78
    iput v7, v2, Landroid/app/Notification;->flags:I

    #@7a
    .line 677
    iget v7, p0, Landroid/app/Notification;->priority:I

    #@7c
    iput v7, v2, Landroid/app/Notification;->priority:I

    #@7e
    .line 679
    iget-object v4, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@80
    .line 680
    .local v4, thiskind:[Ljava/lang/String;
    if-eqz v4, :cond_8a

    #@82
    .line 681
    array-length v0, v4

    #@83
    .line 682
    .restart local v0       #N:I
    new-array v3, v0, [Ljava/lang/String;

    #@85
    iput-object v3, v2, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@87
    .line 683
    .local v3, thatkind:[Ljava/lang/String;
    invoke-static {v4, v9, v3, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8a
    .line 686
    .end local v0           #N:I
    .end local v3           #thatkind:[Ljava/lang/String;
    :cond_8a
    iget-object v7, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@8c
    if-eqz v7, :cond_97

    #@8e
    .line 687
    new-instance v7, Landroid/os/Bundle;

    #@90
    iget-object v8, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@92
    invoke-direct {v7, v8}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@95
    iput-object v7, v2, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@97
    .line 691
    :cond_97
    iget-object v7, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@99
    array-length v7, v7

    #@9a
    new-array v7, v7, [Landroid/app/Notification$Action;

    #@9c
    iput-object v7, v2, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@9e
    .line 692
    const/4 v1, 0x0

    #@9f
    .local v1, i:I
    :goto_9f
    iget-object v7, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@a1
    array-length v7, v7

    #@a2
    if-ge v1, v7, :cond_b3

    #@a4
    .line 693
    iget-object v7, v2, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@a6
    iget-object v8, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@a8
    aget-object v8, v8, v1

    #@aa
    invoke-virtual {v8}, Landroid/app/Notification$Action;->clone()Landroid/app/Notification$Action;

    #@ad
    move-result-object v8

    #@ae
    aput-object v8, v7, v1

    #@b0
    .line 692
    add-int/lit8 v1, v1, 0x1

    #@b2
    goto :goto_9f

    #@b3
    .line 695
    :cond_b3
    iget-object v7, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@b5
    if-eqz v7, :cond_bf

    #@b7
    .line 696
    iget-object v7, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@b9
    invoke-virtual {v7}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@bc
    move-result-object v7

    #@bd
    iput-object v7, v2, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@bf
    .line 699
    :cond_bf
    return-object v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 59
    invoke-virtual {p0}, Landroid/app/Notification;->clone()Landroid/app/Notification;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 703
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .registers 12
    .parameter "context"
    .parameter "contentTitle"
    .parameter "contentText"
    .parameter "contentIntent"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const v6, 0x1020064

    #@3
    const v4, 0x1020006

    #@6
    .line 836
    new-instance v0, Landroid/widget/RemoteViews;

    #@8
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    const v3, 0x1090092

    #@f
    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@12
    .line 838
    .local v0, contentView:Landroid/widget/RemoteViews;
    iget v2, p0, Landroid/app/Notification;->icon:I

    #@14
    if-eqz v2, :cond_1b

    #@16
    .line 839
    iget v2, p0, Landroid/app/Notification;->icon:I

    #@18
    invoke-virtual {v0, v4, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    #@1b
    .line 841
    :cond_1b
    iget v2, p0, Landroid/app/Notification;->priority:I

    #@1d
    const/4 v3, -0x1

    #@1e
    if-ge v2, v3, :cond_35

    #@20
    .line 842
    const-string/jumbo v2, "setBackgroundResource"

    #@23
    const v3, 0x1080624

    #@26
    invoke-virtual {v0, v4, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@29
    .line 844
    const v2, 0x1020336

    #@2c
    const-string/jumbo v3, "setBackgroundResource"

    #@2f
    const v4, 0x1080426

    #@32
    invoke-virtual {v0, v2, v3, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@35
    .line 847
    :cond_35
    if-eqz p2, :cond_3d

    #@37
    .line 848
    const v2, 0x1020016

    #@3a
    invoke-virtual {v0, v2, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@3d
    .line 850
    :cond_3d
    if-eqz p3, :cond_45

    #@3f
    .line 851
    const v2, 0x1020046

    #@42
    invoke-virtual {v0, v2, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@45
    .line 853
    :cond_45
    iget-wide v2, p0, Landroid/app/Notification;->when:J

    #@47
    const-wide/16 v4, 0x0

    #@49
    cmp-long v2, v2, v4

    #@4b
    if-eqz v2, :cond_59

    #@4d
    .line 854
    const/4 v2, 0x0

    #@4e
    invoke-virtual {v0, v6, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@51
    .line 855
    const-string/jumbo v2, "setTime"

    #@54
    iget-wide v3, p0, Landroid/app/Notification;->when:J

    #@56
    invoke-virtual {v0, v6, v2, v3, v4}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    #@59
    .line 857
    :cond_59
    iget v2, p0, Landroid/app/Notification;->number:I

    #@5b
    if-eqz v2, :cond_6e

    #@5d
    .line 858
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    #@60
    move-result-object v1

    #@61
    .line 859
    .local v1, f:Ljava/text/NumberFormat;
    const v2, 0x102033a

    #@64
    iget v3, p0, Landroid/app/Notification;->number:I

    #@66
    int-to-long v3, v3

    #@67
    invoke-virtual {v1, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@6e
    .line 862
    .end local v1           #f:Ljava/text/NumberFormat;
    :cond_6e
    iput-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@70
    .line 863
    iput-object p4, p0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@72
    .line 864
    return-void
.end method

.method public setUser(Landroid/os/UserHandle;)V
    .registers 4
    .parameter "user"

    #@0
    .prologue
    .line 931
    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    #@3
    move-result v0

    #@4
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    .line 932
    sget-object p1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@9
    .line 934
    :cond_9
    iget-object v0, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 935
    iget-object v0, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@f
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@12
    .line 937
    :cond_12
    iget-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 938
    iget-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@18
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@1b
    .line 940
    :cond_1b
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 941
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@21
    invoke-virtual {v0, p1}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@24
    .line 943
    :cond_24
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 868
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 869
    .local v2, sb:Ljava/lang/StringBuilder;
    const-string v3, "Notification(pri="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 870
    iget v3, p0, Landroid/app/Notification;->priority:I

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    .line 871
    const-string v3, " contentView="

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 872
    iget-object v3, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@16
    if-eqz v3, :cond_ad

    #@18
    .line 873
    iget-object v3, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@1a
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 874
    const-string v3, "/0x"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 875
    iget-object v3, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@28
    invoke-virtual {v3}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@2b
    move-result v3

    #@2c
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 880
    :goto_33
    const-string v3, " vibrate="

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 881
    iget v3, p0, Landroid/app/Notification;->defaults:I

    #@3a
    and-int/lit8 v3, v3, 0x2

    #@3c
    if-eqz v3, :cond_b4

    #@3e
    .line 882
    const-string v3, "default"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 897
    :goto_43
    const-string v3, " sound="

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    .line 898
    iget v3, p0, Landroid/app/Notification;->defaults:I

    #@4a
    and-int/lit8 v3, v3, 0x1

    #@4c
    if-eqz v3, :cond_ed

    #@4e
    .line 899
    const-string v3, "default"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 905
    :goto_53
    const-string v3, " defaults=0x"

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 906
    iget v3, p0, Landroid/app/Notification;->defaults:I

    #@5a
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    .line 907
    const-string v3, " flags=0x"

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    .line 908
    iget v3, p0, Landroid/app/Notification;->flags:I

    #@68
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    .line 909
    const-string v3, " kind=["

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    .line 910
    iget-object v3, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@76
    if-nez v3, :cond_104

    #@78
    .line 911
    const-string/jumbo v3, "null"

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 918
    :cond_7e
    const-string v3, "]"

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    .line 919
    iget-object v3, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@85
    if-eqz v3, :cond_a3

    #@87
    .line 920
    const-string v3, " "

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    .line 921
    iget-object v3, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@8e
    array-length v3, v3

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@92
    .line 922
    const-string v3, " action"

    #@94
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    .line 923
    iget-object v3, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@99
    array-length v3, v3

    #@9a
    const/4 v4, 0x1

    #@9b
    if-le v3, v4, :cond_a3

    #@9d
    const-string/jumbo v3, "s"

    #@a0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    .line 925
    :cond_a3
    const-string v3, ")"

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    .line 926
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v3

    #@ac
    return-object v3

    #@ad
    .line 877
    :cond_ad
    const-string/jumbo v3, "null"

    #@b0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    goto :goto_33

    #@b4
    .line 883
    :cond_b4
    iget-object v3, p0, Landroid/app/Notification;->vibrate:[J

    #@b6
    if-eqz v3, :cond_e5

    #@b8
    .line 884
    iget-object v3, p0, Landroid/app/Notification;->vibrate:[J

    #@ba
    array-length v3, v3

    #@bb
    add-int/lit8 v0, v3, -0x1

    #@bd
    .line 885
    .local v0, N:I
    const-string v3, "["

    #@bf
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    .line 886
    const/4 v1, 0x0

    #@c3
    .local v1, i:I
    :goto_c3
    if-ge v1, v0, :cond_d4

    #@c5
    .line 887
    iget-object v3, p0, Landroid/app/Notification;->vibrate:[J

    #@c7
    aget-wide v3, v3, v1

    #@c9
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@cc
    .line 888
    const/16 v3, 0x2c

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@d1
    .line 886
    add-int/lit8 v1, v1, 0x1

    #@d3
    goto :goto_c3

    #@d4
    .line 890
    :cond_d4
    const/4 v3, -0x1

    #@d5
    if-eq v0, v3, :cond_de

    #@d7
    .line 891
    iget-object v3, p0, Landroid/app/Notification;->vibrate:[J

    #@d9
    aget-wide v3, v3, v0

    #@db
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@de
    .line 893
    :cond_de
    const-string v3, "]"

    #@e0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    goto/16 :goto_43

    #@e5
    .line 895
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_e5
    const-string/jumbo v3, "null"

    #@e8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    goto/16 :goto_43

    #@ed
    .line 900
    :cond_ed
    iget-object v3, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@ef
    if-eqz v3, :cond_fc

    #@f1
    .line 901
    iget-object v3, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@f3
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@f6
    move-result-object v3

    #@f7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    goto/16 :goto_53

    #@fc
    .line 903
    :cond_fc
    const-string/jumbo v3, "null"

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    goto/16 :goto_53

    #@104
    .line 913
    :cond_104
    const/4 v1, 0x0

    #@105
    .restart local v1       #i:I
    :goto_105
    iget-object v3, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@107
    array-length v3, v3

    #@108
    if-ge v1, v3, :cond_7e

    #@10a
    .line 914
    if-lez v1, :cond_111

    #@10c
    const-string v3, ","

    #@10e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    .line 915
    :cond_111
    iget-object v3, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@113
    aget-object v3, v3, v1

    #@115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    .line 913
    add-int/lit8 v1, v1, 0x1

    #@11a
    goto :goto_105
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 711
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 713
    iget-wide v0, p0, Landroid/app/Notification;->when:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 714
    iget v0, p0, Landroid/app/Notification;->icon:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 715
    iget v0, p0, Landroid/app/Notification;->number:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 716
    iget-object v0, p0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@16
    if-eqz v0, :cond_c4

    #@18
    .line 717
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 718
    iget-object v0, p0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@1d
    invoke-virtual {v0, p1, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 722
    :goto_20
    iget-object v0, p0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@22
    if-eqz v0, :cond_c9

    #@24
    .line 723
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 724
    iget-object v0, p0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@29
    invoke-virtual {v0, p1, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    .line 728
    :goto_2c
    iget-object v0, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@2e
    if-eqz v0, :cond_ce

    #@30
    .line 729
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 730
    iget-object v0, p0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@35
    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@38
    .line 734
    :goto_38
    iget-object v0, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@3a
    if-eqz v0, :cond_d3

    #@3c
    .line 735
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3f
    .line 736
    iget-object v0, p0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@41
    invoke-virtual {v0, p1, v2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@44
    .line 740
    :goto_44
    iget-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@46
    if-eqz v0, :cond_d8

    #@48
    .line 741
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4b
    .line 742
    iget-object v0, p0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@4d
    invoke-virtual {v0, p1, v2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@50
    .line 746
    :goto_50
    iget-object v0, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@52
    if-eqz v0, :cond_dd

    #@54
    .line 747
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@57
    .line 748
    iget-object v0, p0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@59
    invoke-virtual {v0, p1, v2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@5c
    .line 753
    :goto_5c
    iget v0, p0, Landroid/app/Notification;->defaults:I

    #@5e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@61
    .line 754
    iget v0, p0, Landroid/app/Notification;->flags:I

    #@63
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@66
    .line 756
    iget-object v0, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@68
    if-eqz v0, :cond_e2

    #@6a
    .line 757
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6d
    .line 758
    iget-object v0, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@6f
    invoke-virtual {v0, p1, v2}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    #@72
    .line 762
    :goto_72
    iget v0, p0, Landroid/app/Notification;->audioStreamType:I

    #@74
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@77
    .line 763
    iget-object v0, p0, Landroid/app/Notification;->vibrate:[J

    #@79
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    #@7c
    .line 764
    iget v0, p0, Landroid/app/Notification;->ledARGB:I

    #@7e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@81
    .line 765
    iget v0, p0, Landroid/app/Notification;->ledOnMS:I

    #@83
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@86
    .line 766
    iget v0, p0, Landroid/app/Notification;->ledOffMS:I

    #@88
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8b
    .line 767
    iget v0, p0, Landroid/app/Notification;->iconLevel:I

    #@8d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@90
    .line 769
    iget-object v0, p0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@92
    if-eqz v0, :cond_e6

    #@94
    .line 770
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@97
    .line 771
    iget-object v0, p0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@99
    invoke-virtual {v0, p1, v2}, Landroid/app/PendingIntent;->writeToParcel(Landroid/os/Parcel;I)V

    #@9c
    .line 776
    :goto_9c
    iget v0, p0, Landroid/app/Notification;->priority:I

    #@9e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a1
    .line 778
    iget-object v0, p0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@a3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@a6
    .line 780
    iget-object v0, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@a8
    if-eqz v0, :cond_ea

    #@aa
    .line 781
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ad
    .line 782
    iget-object v0, p0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    #@af
    invoke-virtual {v0, p1, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@b2
    .line 787
    :goto_b2
    iget-object v0, p0, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    #@b4
    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@b7
    .line 789
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@b9
    if-eqz v0, :cond_ee

    #@bb
    .line 790
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@be
    .line 791
    iget-object v0, p0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@c0
    invoke-virtual {v0, p1, v2}, Landroid/widget/RemoteViews;->writeToParcel(Landroid/os/Parcel;I)V

    #@c3
    .line 795
    :goto_c3
    return-void

    #@c4
    .line 720
    :cond_c4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c7
    goto/16 :goto_20

    #@c9
    .line 726
    :cond_c9
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@cc
    goto/16 :goto_2c

    #@ce
    .line 732
    :cond_ce
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d1
    goto/16 :goto_38

    #@d3
    .line 738
    :cond_d3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@d6
    goto/16 :goto_44

    #@d8
    .line 744
    :cond_d8
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@db
    goto/16 :goto_50

    #@dd
    .line 750
    :cond_dd
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e0
    goto/16 :goto_5c

    #@e2
    .line 760
    :cond_e2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e5
    goto :goto_72

    #@e6
    .line 773
    :cond_e6
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e9
    goto :goto_9c

    #@ea
    .line 784
    :cond_ea
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@ed
    goto :goto_b2

    #@ee
    .line 793
    :cond_ee
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@f1
    goto :goto_c3
.end method
