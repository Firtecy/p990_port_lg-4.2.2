.class Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
.super Ljava/lang/Object;
.source "WallpaperManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/WallpaperManager$Globals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SprintChameleonAttributes"
.end annotation


# instance fields
.field public networkCode:Ljava/lang/String;

.field public resourceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "init"

    #@0
    .prologue
    .line 482
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 483
    const-string v1, ","

    #@5
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 484
    .local v0, fragments:[Ljava/lang/String;
    const/4 v1, 0x0

    #@a
    aget-object v1, v0, v1

    #@c
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    iput-object v1, p0, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->networkCode:Ljava/lang/String;

    #@12
    .line 485
    const/4 v1, 0x3

    #@13
    aget-object v1, v0, v1

    #@15
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->resourceName:Ljava/lang/String;

    #@1b
    .line 486
    return-void
.end method
