.class public abstract Landroid/app/Service;
.super Landroid/content/ContextWrapper;
.source "Service.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# static fields
.field public static final START_CONTINUATION_MASK:I = 0xf

.field public static final START_FLAG_REDELIVERY:I = 0x1

.field public static final START_FLAG_RETRY:I = 0x2

.field public static final START_NOT_STICKY:I = 0x2

.field public static final START_REDELIVER_INTENT:I = 0x3

.field public static final START_STICKY:I = 0x1

.field public static final START_STICKY_COMPATIBILITY:I = 0x0

.field public static final START_TASK_REMOVED_COMPLETE:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "Service"


# instance fields
.field private mActivityManager:Landroid/app/IActivityManager;

.field private mApplication:Landroid/app/Application;

.field private mClassName:Ljava/lang/String;

.field private mStartCompatibility:Z

.field private mThread:Landroid/app/ActivityThread;

.field private mToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 295
    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@4
    .line 707
    iput-object v0, p0, Landroid/app/Service;->mThread:Landroid/app/ActivityThread;

    #@6
    .line 708
    iput-object v0, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@8
    .line 709
    iput-object v0, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@a
    .line 710
    iput-object v0, p0, Landroid/app/Service;->mApplication:Landroid/app/Application;

    #@c
    .line 711
    iput-object v0, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@e
    .line 712
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/app/Service;->mStartCompatibility:Z

    #@11
    .line 296
    return-void
.end method


# virtual methods
.method public final attach(Landroid/content/Context;Landroid/app/ActivityThread;Ljava/lang/String;Landroid/os/IBinder;Landroid/app/Application;Ljava/lang/Object;)V
    .registers 9
    .parameter "context"
    .parameter "thread"
    .parameter "className"
    .parameter "token"
    .parameter "application"
    .parameter "activityManager"

    #@0
    .prologue
    .line 692
    invoke-virtual {p0, p1}, Landroid/app/Service;->attachBaseContext(Landroid/content/Context;)V

    #@3
    .line 693
    iput-object p2, p0, Landroid/app/Service;->mThread:Landroid/app/ActivityThread;

    #@5
    .line 694
    iput-object p3, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@7
    .line 695
    iput-object p4, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@9
    .line 696
    iput-object p5, p0, Landroid/app/Service;->mApplication:Landroid/app/Application;

    #@b
    .line 697
    check-cast p6, Landroid/app/IActivityManager;

    #@d
    .end local p6
    iput-object p6, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@f
    .line 698
    invoke-virtual {p0}, Landroid/app/Service;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@12
    move-result-object v0

    #@13
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@15
    const/4 v1, 0x5

    #@16
    if-ge v0, v1, :cond_1c

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    iput-boolean v0, p0, Landroid/app/Service;->mStartCompatibility:Z

    #@1b
    .line 700
    return-void

    #@1c
    .line 698
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_19
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 680
    const-string/jumbo v0, "nothing to dump"

    #@3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6
    .line 681
    return-void
.end method

.method public final getApplication()Landroid/app/Application;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/app/Service;->mApplication:Landroid/app/Application;

    #@2
    return-object v0
.end method

.method final getClassName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public abstract onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 464
    return-void
.end method

.method public onCreate()V
    .registers 1

    #@0
    .prologue
    .line 307
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 461
    return-void
.end method

.method public onLowMemory()V
    .registers 1

    #@0
    .prologue
    .line 467
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 524
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 3
    .parameter "intent"
    .parameter "startId"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 314
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 450
    invoke-virtual {p0, p1, p3}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    #@3
    .line 451
    iget-boolean v0, p0, Landroid/app/Service;->mStartCompatibility:Z

    #@5
    if-eqz v0, :cond_9

    #@7
    const/4 v0, 0x0

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x1

    #@a
    goto :goto_8
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .registers 2
    .parameter "rootIntent"

    #@0
    .prologue
    .line 537
    return-void
.end method

.method public onTrimMemory(I)V
    .registers 2
    .parameter "level"

    #@0
    .prologue
    .line 470
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 509
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final setForeground(Z)V
    .registers 5
    .parameter "isForeground"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 614
    const-string v0, "Service"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setForeground: ignoring old API call on "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 615
    return-void
.end method

.method public final startForeground(ILandroid/app/Notification;)V
    .registers 9
    .parameter "id"
    .parameter "notification"

    #@0
    .prologue
    .line 643
    :try_start_0
    iget-object v0, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@2
    new-instance v1, Landroid/content/ComponentName;

    #@4
    iget-object v2, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@6
    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@9
    iget-object v2, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@b
    const/4 v5, 0x1

    #@c
    move v3, p1

    #@d
    move-object v4, p2

    #@e
    invoke-interface/range {v0 .. v5}, Landroid/app/IActivityManager;->setServiceForeground(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 648
    :goto_11
    return-void

    #@12
    .line 646
    :catch_12
    move-exception v0

    #@13
    goto :goto_11
.end method

.method public final stopForeground(Z)V
    .registers 8
    .parameter "removeNotification"

    #@0
    .prologue
    .line 660
    :try_start_0
    iget-object v0, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@2
    new-instance v1, Landroid/content/ComponentName;

    #@4
    iget-object v2, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@6
    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@9
    iget-object v2, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@b
    const/4 v3, 0x0

    #@c
    const/4 v4, 0x0

    #@d
    move v5, p1

    #@e
    invoke-interface/range {v0 .. v5}, Landroid/app/IActivityManager;->setServiceForeground(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_12

    #@11
    .line 665
    :goto_11
    return-void

    #@12
    .line 663
    :catch_12
    move-exception v0

    #@13
    goto :goto_11
.end method

.method public final stopSelf()V
    .registers 2

    #@0
    .prologue
    .line 546
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/app/Service;->stopSelf(I)V

    #@4
    .line 547
    return-void
.end method

.method public final stopSelf(I)V
    .registers 5
    .parameter "startId"

    #@0
    .prologue
    .line 555
    iget-object v0, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 563
    :goto_4
    return-void

    #@5
    .line 559
    :cond_5
    :try_start_5
    iget-object v0, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@7
    new-instance v1, Landroid/content/ComponentName;

    #@9
    iget-object v2, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@b
    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@e
    iget-object v2, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@10
    invoke-interface {v0, v1, v2, p1}, Landroid/app/IActivityManager;->stopServiceToken(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_13} :catch_14

    #@13
    goto :goto_4

    #@14
    .line 561
    :catch_14
    move-exception v0

    #@15
    goto :goto_4
.end method

.method public final stopSelfResult(I)Z
    .registers 6
    .parameter "startId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 587
    iget-object v1, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 595
    :goto_5
    return v0

    #@6
    .line 591
    :cond_6
    :try_start_6
    iget-object v1, p0, Landroid/app/Service;->mActivityManager:Landroid/app/IActivityManager;

    #@8
    new-instance v2, Landroid/content/ComponentName;

    #@a
    iget-object v3, p0, Landroid/app/Service;->mClassName:Ljava/lang/String;

    #@c
    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    #@f
    iget-object v3, p0, Landroid/app/Service;->mToken:Landroid/os/IBinder;

    #@11
    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->stopServiceToken(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_14} :catch_16

    #@14
    move-result v0

    #@15
    goto :goto_5

    #@16
    .line 593
    :catch_16
    move-exception v1

    #@17
    goto :goto_5
.end method
