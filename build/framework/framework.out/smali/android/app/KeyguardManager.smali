.class public Landroid/app/KeyguardManager;
.super Ljava/lang/Object;
.source "KeyguardManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/KeyguardManager$OnKeyguardExitResult;,
        Landroid/app/KeyguardManager$KeyguardLock;
    }
.end annotation


# instance fields
.field private mWM:Landroid/view/IWindowManager;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 118
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@9
    .line 119
    return-void
.end method

.method static synthetic access$000(Landroid/app/KeyguardManager;)Landroid/view/IWindowManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@2
    return-object v0
.end method


# virtual methods
.method public exitKeyguardSecurely(Landroid/app/KeyguardManager$OnKeyguardExitResult;)V
    .registers 4
    .parameter "callback"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 211
    :try_start_0
    iget-object v0, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@2
    new-instance v1, Landroid/app/KeyguardManager$1;

    #@4
    invoke-direct {v1, p0, p1}, Landroid/app/KeyguardManager$1;-><init>(Landroid/app/KeyguardManager;Landroid/app/KeyguardManager$OnKeyguardExitResult;)V

    #@7
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->exitKeyguardSecurely(Landroid/view/IOnKeyguardExitResult;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_a} :catch_b

    #@a
    .line 219
    :goto_a
    return-void

    #@b
    .line 216
    :catch_b
    move-exception v0

    #@c
    goto :goto_a
.end method

.method public inKeyguardRestrictedInputMode()Z
    .registers 3

    #@0
    .prologue
    .line 179
    :try_start_0
    iget-object v1, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@2
    invoke-interface {v1}, Landroid/view/IWindowManager;->inKeyguardRestrictedInputMode()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 181
    :goto_6
    return v1

    #@7
    .line 180
    :catch_7
    move-exception v0

    #@8
    .line 181
    .local v0, ex:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isKeyguardLocked()Z
    .registers 3

    #@0
    .prologue
    .line 149
    :try_start_0
    iget-object v1, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@2
    invoke-interface {v1}, Landroid/view/IWindowManager;->isKeyguardLocked()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 151
    :goto_6
    return v1

    #@7
    .line 150
    :catch_7
    move-exception v0

    #@8
    .line 151
    .local v0, ex:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isKeyguardSecure()Z
    .registers 3

    #@0
    .prologue
    .line 162
    :try_start_0
    iget-object v1, p0, Landroid/app/KeyguardManager;->mWM:Landroid/view/IWindowManager;

    #@2
    invoke-interface {v1}, Landroid/view/IWindowManager;->isKeyguardSecure()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 164
    :goto_6
    return v1

    #@7
    .line 163
    :catch_7
    move-exception v0

    #@8
    .line 164
    .local v0, ex:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;
    .registers 3
    .parameter "tag"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 139
    new-instance v0, Landroid/app/KeyguardManager$KeyguardLock;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/app/KeyguardManager$KeyguardLock;-><init>(Landroid/app/KeyguardManager;Ljava/lang/String;)V

    #@5
    return-object v0
.end method
