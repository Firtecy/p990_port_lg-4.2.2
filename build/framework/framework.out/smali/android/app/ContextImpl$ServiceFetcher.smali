.class Landroid/app/ContextImpl$ServiceFetcher;
.super Ljava/lang/Object;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceFetcher"
.end annotation


# instance fields
.field mContextCacheIndex:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 234
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 235
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/app/ContextImpl$ServiceFetcher;->mContextCacheIndex:I

    #@6
    return-void
.end method


# virtual methods
.method public createService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 4
    .parameter "ctx"

    #@0
    .prologue
    .line 269
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Not implemented"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 7
    .parameter "ctx"

    #@0
    .prologue
    .line 241
    iget-object v0, p1, Landroid/app/ContextImpl;->mServiceCache:Ljava/util/ArrayList;

    #@2
    .line 243
    .local v0, cache:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    monitor-enter v0

    #@3
    .line 244
    :try_start_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v4

    #@7
    if-nez v4, :cond_17

    #@9
    .line 249
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    invoke-static {}, Landroid/app/ContextImpl;->access$000()I

    #@d
    move-result v4

    #@e
    if-ge v1, v4, :cond_22

    #@10
    .line 250
    const/4 v4, 0x0

    #@11
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 249
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_a

    #@17
    .line 253
    .end local v1           #i:I
    :cond_17
    iget v4, p0, Landroid/app/ContextImpl$ServiceFetcher;->mContextCacheIndex:I

    #@19
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    .line 254
    .local v2, service:Ljava/lang/Object;
    if-eqz v2, :cond_22

    #@1f
    .line 255
    monitor-exit v0

    #@20
    move-object v3, v2

    #@21
    .line 260
    .end local v2           #service:Ljava/lang/Object;
    .local v3, service:Ljava/lang/Object;
    :goto_21
    return-object v3

    #@22
    .line 258
    .end local v3           #service:Ljava/lang/Object;
    :cond_22
    invoke-virtual {p0, p1}, Landroid/app/ContextImpl$ServiceFetcher;->createService(Landroid/app/ContextImpl;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    .line 259
    .restart local v2       #service:Ljava/lang/Object;
    iget v4, p0, Landroid/app/ContextImpl$ServiceFetcher;->mContextCacheIndex:I

    #@28
    invoke-virtual {v0, v4, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 260
    monitor-exit v0

    #@2c
    move-object v3, v2

    #@2d
    .end local v2           #service:Ljava/lang/Object;
    .restart local v3       #service:Ljava/lang/Object;
    goto :goto_21

    #@2e
    .line 261
    .end local v3           #service:Ljava/lang/Object;
    :catchall_2e
    move-exception v4

    #@2f
    monitor-exit v0
    :try_end_30
    .catchall {:try_start_3 .. :try_end_30} :catchall_2e

    #@30
    throw v4
.end method
