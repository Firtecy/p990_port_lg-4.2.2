.class public Landroid/app/SearchManager;
.super Ljava/lang/Object;
.source "SearchManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/SearchManager$OnCancelListener;,
        Landroid/app/SearchManager$OnDismissListener;
    }
.end annotation


# static fields
.field public static final ACTION_KEY:Ljava/lang/String; = "action_key"

.field public static final ACTION_MSG:Ljava/lang/String; = "action_msg"

.field public static final APP_DATA:Ljava/lang/String; = "app_data"

.field public static final CONTEXT_IS_VOICE:Ljava/lang/String; = "android.search.CONTEXT_IS_VOICE"

.field public static final CURSOR_EXTRA_KEY_IN_PROGRESS:Ljava/lang/String; = "in_progress"

.field private static final DBG:Z = false

.field public static final DISABLE_VOICE_SEARCH:Ljava/lang/String; = "android.search.DISABLE_VOICE_SEARCH"

.field public static final EXTRA_DATA_KEY:Ljava/lang/String; = "intent_extra_data_key"

.field public static final EXTRA_NEW_SEARCH:Ljava/lang/String; = "new_search"

.field public static final EXTRA_SELECT_QUERY:Ljava/lang/String; = "select_query"

.field public static final EXTRA_WEB_SEARCH_PENDINGINTENT:Ljava/lang/String; = "web_search_pendingintent"

.field public static final FLAG_QUERY_REFINEMENT:I = 0x1

.field public static final INTENT_ACTION_GLOBAL_SEARCH:Ljava/lang/String; = "android.search.action.GLOBAL_SEARCH"

.field public static final INTENT_ACTION_SEARCHABLES_CHANGED:Ljava/lang/String; = "android.search.action.SEARCHABLES_CHANGED"

.field public static final INTENT_ACTION_SEARCH_SETTINGS:Ljava/lang/String; = "android.search.action.SEARCH_SETTINGS"

.field public static final INTENT_ACTION_SEARCH_SETTINGS_CHANGED:Ljava/lang/String; = "android.search.action.SETTINGS_CHANGED"

.field public static final INTENT_ACTION_WEB_SEARCH_SETTINGS:Ljava/lang/String; = "android.search.action.WEB_SEARCH_SETTINGS"

.field public static final INTENT_GLOBAL_SEARCH_ACTIVITY_CHANGED:Ljava/lang/String; = "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

.field public static final MENU_KEY:C = 's'

.field public static final MENU_KEYCODE:I = 0x2f

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final SEARCH_MODE:Ljava/lang/String; = "search_mode"

.field public static final SHORTCUT_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.android.search.suggest"

.field public static final SUGGEST_COLUMN_FLAGS:Ljava/lang/String; = "suggest_flags"

.field public static final SUGGEST_COLUMN_FORMAT:Ljava/lang/String; = "suggest_format"

.field public static final SUGGEST_COLUMN_ICON_1:Ljava/lang/String; = "suggest_icon_1"

.field public static final SUGGEST_COLUMN_ICON_2:Ljava/lang/String; = "suggest_icon_2"

.field public static final SUGGEST_COLUMN_INTENT_ACTION:Ljava/lang/String; = "suggest_intent_action"

.field public static final SUGGEST_COLUMN_INTENT_DATA:Ljava/lang/String; = "suggest_intent_data"

.field public static final SUGGEST_COLUMN_INTENT_DATA_ID:Ljava/lang/String; = "suggest_intent_data_id"

.field public static final SUGGEST_COLUMN_INTENT_EXTRA_DATA:Ljava/lang/String; = "suggest_intent_extra_data"

.field public static final SUGGEST_COLUMN_LAST_ACCESS_HINT:Ljava/lang/String; = "suggest_last_access_hint"

.field public static final SUGGEST_COLUMN_QUERY:Ljava/lang/String; = "suggest_intent_query"

.field public static final SUGGEST_COLUMN_SHORTCUT_ID:Ljava/lang/String; = "suggest_shortcut_id"

.field public static final SUGGEST_COLUMN_SPINNER_WHILE_REFRESHING:Ljava/lang/String; = "suggest_spinner_while_refreshing"

.field public static final SUGGEST_COLUMN_TEXT_1:Ljava/lang/String; = "suggest_text_1"

.field public static final SUGGEST_COLUMN_TEXT_2:Ljava/lang/String; = "suggest_text_2"

.field public static final SUGGEST_COLUMN_TEXT_2_URL:Ljava/lang/String; = "suggest_text_2_url"

.field public static final SUGGEST_MIME_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.android.search.suggest"

.field public static final SUGGEST_NEVER_MAKE_SHORTCUT:Ljava/lang/String; = "_-1"

.field public static final SUGGEST_PARAMETER_LIMIT:Ljava/lang/String; = "limit"

.field public static final SUGGEST_URI_PATH_QUERY:Ljava/lang/String; = "search_suggest_query"

.field public static final SUGGEST_URI_PATH_SHORTCUT:Ljava/lang/String; = "search_suggest_shortcut"

.field private static final TAG:Ljava/lang/String; = "SearchManager"

.field public static final USER_QUERY:Ljava/lang/String; = "user_query"

.field private static mService:Landroid/app/ISearchManager;


# instance fields
.field private mAssociatedPackage:Ljava/lang/String;

.field mCancelListener:Landroid/app/SearchManager$OnCancelListener;

.field private final mContext:Landroid/content/Context;

.field mDismissListener:Landroid/app/SearchManager$OnDismissListener;

.field final mHandler:Landroid/os/Handler;

.field private mSearchDialog:Landroid/app/SearchDialog;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 450
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 445
    iput-object v0, p0, Landroid/app/SearchManager;->mDismissListener:Landroid/app/SearchManager$OnDismissListener;

    #@6
    .line 446
    iput-object v0, p0, Landroid/app/SearchManager;->mCancelListener:Landroid/app/SearchManager$OnCancelListener;

    #@8
    .line 451
    iput-object p1, p0, Landroid/app/SearchManager;->mContext:Landroid/content/Context;

    #@a
    .line 452
    iput-object p2, p0, Landroid/app/SearchManager;->mHandler:Landroid/os/Handler;

    #@c
    .line 453
    const-string/jumbo v0, "search"

    #@f
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Landroid/app/ISearchManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/ISearchManager;

    #@16
    move-result-object v0

    #@17
    sput-object v0, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@19
    .line 455
    return-void
.end method

.method private ensureSearchDialog()V
    .registers 3

    #@0
    .prologue
    .line 529
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@2
    if-nez v0, :cond_17

    #@4
    .line 530
    new-instance v0, Landroid/app/SearchDialog;

    #@6
    iget-object v1, p0, Landroid/app/SearchManager;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1, p0}, Landroid/app/SearchDialog;-><init>(Landroid/content/Context;Landroid/app/SearchManager;)V

    #@b
    iput-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@d
    .line 531
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@f
    invoke-virtual {v0, p0}, Landroid/app/SearchDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@12
    .line 532
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@14
    invoke-virtual {v0, p0}, Landroid/app/SearchDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@17
    .line 534
    :cond_17
    return-void
.end method


# virtual methods
.method public getAssistIntent(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 850
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;
    .registers 10
    .parameter "context"
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 861
    :try_start_1
    sget-object v4, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@3
    if-nez v4, :cond_7

    #@5
    move-object v1, v3

    #@6
    .line 873
    :goto_6
    return-object v1

    #@7
    .line 864
    :cond_7
    sget-object v4, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@9
    invoke-interface {v4, p2}, Landroid/app/ISearchManager;->getAssistIntent(I)Landroid/content/ComponentName;

    #@c
    move-result-object v0

    #@d
    .line 865
    .local v0, comp:Landroid/content/ComponentName;
    if-nez v0, :cond_11

    #@f
    move-object v1, v3

    #@10
    .line 866
    goto :goto_6

    #@11
    .line 868
    :cond_11
    new-instance v1, Landroid/content/Intent;

    #@13
    const-string v4, "android.intent.action.ASSIST"

    #@15
    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    .line 869
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1b} :catch_1c

    #@1b
    goto :goto_6

    #@1c
    .line 871
    .end local v0           #comp:Landroid/content/ComponentName;
    .end local v1           #intent:Landroid/content/Intent;
    :catch_1c
    move-exception v2

    #@1d
    .line 872
    .local v2, re:Landroid/os/RemoteException;
    const-string v4, "SearchManager"

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "getAssistIntent() failed: "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    move-object v1, v3

    #@36
    .line 873
    goto :goto_6
.end method

.method public getGlobalSearchActivities()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 583
    :try_start_0
    sget-object v1, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@2
    invoke-interface {v1}, Landroid/app/ISearchManager;->getGlobalSearchActivities()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 586
    :goto_6
    return-object v1

    #@7
    .line 584
    :catch_7
    move-exception v0

    #@8
    .line 585
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "SearchManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getGlobalSearchActivities() failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 586
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public getGlobalSearchActivity()Landroid/content/ComponentName;
    .registers 5

    #@0
    .prologue
    .line 595
    :try_start_0
    sget-object v1, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@2
    invoke-interface {v1}, Landroid/app/ISearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 598
    :goto_6
    return-object v1

    #@7
    .line 596
    :catch_7
    move-exception v0

    #@8
    .line 597
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "SearchManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getGlobalSearchActivity() failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 598
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
    .registers 6
    .parameter "componentName"

    #@0
    .prologue
    .line 751
    :try_start_0
    sget-object v1, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@2
    invoke-interface {v1, p1}, Landroid/app/ISearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 754
    :goto_6
    return-object v1

    #@7
    .line 752
    :catch_7
    move-exception v0

    #@8
    .line 753
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "SearchManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getSearchableInfo() failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 754
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public getSearchablesInGlobalSearch()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 836
    :try_start_0
    sget-object v1, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@2
    invoke-interface {v1}, Landroid/app/ISearchManager;->getSearchablesInGlobalSearch()Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 839
    :goto_6
    return-object v1

    #@7
    .line 837
    :catch_7
    move-exception v0

    #@8
    .line 838
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SearchManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getSearchablesInGlobalSearch() failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 839
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public getSuggestions(Landroid/app/SearchableInfo;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 4
    .parameter "searchable"
    .parameter "query"

    #@0
    .prologue
    .line 768
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/SearchManager;->getSuggestions(Landroid/app/SearchableInfo;Ljava/lang/String;I)Landroid/database/Cursor;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public getSuggestions(Landroid/app/SearchableInfo;Ljava/lang/String;I)Landroid/database/Cursor;
    .registers 13
    .parameter "searchable"
    .parameter "query"
    .parameter "limit"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 783
    if-nez p1, :cond_4

    #@3
    .line 824
    :cond_3
    :goto_3
    return-object v2

    #@4
    .line 787
    :cond_4
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    #@7
    move-result-object v6

    #@8
    .line 788
    .local v6, authority:Ljava/lang/String;
    if-eqz v6, :cond_3

    #@a
    .line 792
    new-instance v0, Landroid/net/Uri$Builder;

    #@c
    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    #@f
    const-string v5, "content"

    #@11
    invoke-virtual {v0, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@18
    move-result-object v0

    #@19
    const-string v5, ""

    #@1b
    invoke-virtual {v0, v5}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@1e
    move-result-object v0

    #@1f
    const-string v5, ""

    #@21
    invoke-virtual {v0, v5}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@24
    move-result-object v8

    #@25
    .line 799
    .local v8, uriBuilder:Landroid/net/Uri$Builder;
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestPath()Ljava/lang/String;

    #@28
    move-result-object v7

    #@29
    .line 800
    .local v7, contentPath:Ljava/lang/String;
    if-eqz v7, :cond_2e

    #@2b
    .line 801
    invoke-virtual {v8, v7}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@2e
    .line 805
    :cond_2e
    const-string/jumbo v0, "search_suggest_query"

    #@31
    invoke-virtual {v8, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@34
    .line 808
    invoke-virtual {p1}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    .line 810
    .local v3, selection:Ljava/lang/String;
    const/4 v4, 0x0

    #@39
    .line 811
    .local v4, selArgs:[Ljava/lang/String;
    if-eqz v3, :cond_5d

    #@3b
    .line 812
    const/4 v0, 0x1

    #@3c
    new-array v4, v0, [Ljava/lang/String;

    #@3e
    .end local v4           #selArgs:[Ljava/lang/String;
    const/4 v0, 0x0

    #@3f
    aput-object p2, v4, v0

    #@41
    .line 817
    .restart local v4       #selArgs:[Ljava/lang/String;
    :goto_41
    if-lez p3, :cond_4d

    #@43
    .line 818
    const-string/jumbo v0, "limit"

    #@46
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v8, v0, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@4d
    .line 821
    :cond_4d
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@50
    move-result-object v1

    #@51
    .line 824
    .local v1, uri:Landroid/net/Uri;
    iget-object v0, p0, Landroid/app/SearchManager;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@56
    move-result-object v0

    #@57
    move-object v5, v2

    #@58
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@5b
    move-result-object v2

    #@5c
    goto :goto_3

    #@5d
    .line 814
    .end local v1           #uri:Landroid/net/Uri;
    :cond_5d
    invoke-virtual {v8, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@60
    goto :goto_41
.end method

.method public getWebSearchActivity()Landroid/content/ComponentName;
    .registers 5

    #@0
    .prologue
    .line 613
    :try_start_0
    sget-object v1, Landroid/app/SearchManager;->mService:Landroid/app/ISearchManager;

    #@2
    invoke-interface {v1}, Landroid/app/ISearchManager;->getWebSearchActivity()Landroid/content/ComponentName;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 616
    :goto_6
    return-object v1

    #@7
    .line 614
    :catch_7
    move-exception v0

    #@8
    .line 615
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "SearchManager"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "getWebSearchActivity() failed: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 616
    const/4 v1, 0x0

    #@21
    goto :goto_6
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 675
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@8
    invoke-virtual {v0}, Landroid/app/SearchDialog;->isShowing()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "dialog"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 727
    iget-object v0, p0, Landroid/app/SearchManager;->mCancelListener:Landroid/app/SearchManager$OnCancelListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 728
    iget-object v0, p0, Landroid/app/SearchManager;->mCancelListener:Landroid/app/SearchManager$OnCancelListener;

    #@6
    invoke-interface {v0}, Landroid/app/SearchManager$OnCancelListener;->onCancel()V

    #@9
    .line 730
    :cond_9
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter "dialog"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 737
    iget-object v0, p0, Landroid/app/SearchManager;->mDismissListener:Landroid/app/SearchManager$OnDismissListener;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 738
    iget-object v0, p0, Landroid/app/SearchManager;->mDismissListener:Landroid/app/SearchManager$OnDismissListener;

    #@6
    invoke-interface {v0}, Landroid/app/SearchManager$OnDismissListener;->onDismiss()V

    #@9
    .line 740
    :cond_9
    return-void
.end method

.method public setOnCancelListener(Landroid/app/SearchManager$OnCancelListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 719
    iput-object p1, p0, Landroid/app/SearchManager;->mCancelListener:Landroid/app/SearchManager$OnCancelListener;

    #@2
    .line 720
    return-void
.end method

.method public setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 710
    iput-object p1, p0, Landroid/app/SearchManager;->mDismissListener:Landroid/app/SearchManager$OnDismissListener;

    #@2
    .line 711
    return-void
.end method

.method startGlobalSearch(Ljava/lang/String;ZLandroid/os/Bundle;Landroid/graphics/Rect;)V
    .registers 12
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "appSearchData"
    .parameter "sourceBounds"

    #@0
    .prologue
    .line 541
    invoke-virtual {p0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    #@3
    move-result-object v2

    #@4
    .line 542
    .local v2, globalSearchActivity:Landroid/content/ComponentName;
    if-nez v2, :cond_e

    #@6
    .line 543
    const-string v4, "SearchManager"

    #@8
    const-string v5, "No global search activity found."

    #@a
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 573
    :goto_d
    return-void

    #@e
    .line 546
    :cond_e
    new-instance v3, Landroid/content/Intent;

    #@10
    const-string v4, "android.search.action.GLOBAL_SEARCH"

    #@12
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@15
    .line 547
    .local v3, intent:Landroid/content/Intent;
    const/high16 v4, 0x1000

    #@17
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1a
    .line 548
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@1d
    .line 550
    if-nez p3, :cond_75

    #@1f
    .line 551
    new-instance p3, Landroid/os/Bundle;

    #@21
    .end local p3
    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    #@24
    .line 556
    .restart local p3
    :goto_24
    const-string/jumbo v4, "source"

    #@27
    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@2a
    move-result v4

    #@2b
    if-nez v4, :cond_39

    #@2d
    .line 557
    const-string/jumbo v4, "source"

    #@30
    iget-object v5, p0, Landroid/app/SearchManager;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {p3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 559
    :cond_39
    const-string v4, "app_data"

    #@3b
    invoke-virtual {v3, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    #@3e
    .line 560
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@41
    move-result v4

    #@42
    if-nez v4, :cond_4a

    #@44
    .line 561
    const-string/jumbo v4, "query"

    #@47
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4a
    .line 563
    :cond_4a
    if-eqz p2, :cond_52

    #@4c
    .line 564
    const-string/jumbo v4, "select_query"

    #@4f
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@52
    .line 566
    :cond_52
    invoke-virtual {v3, p4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    #@55
    .line 569
    :try_start_55
    iget-object v4, p0, Landroid/app/SearchManager;->mContext:Landroid/content/Context;

    #@57
    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_5a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_55 .. :try_end_5a} :catch_5b

    #@5a
    goto :goto_d

    #@5b
    .line 570
    :catch_5b
    move-exception v1

    #@5c
    .line 571
    .local v1, ex:Landroid/content/ActivityNotFoundException;
    const-string v4, "SearchManager"

    #@5e
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "Global search activity not found: "

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v5

    #@71
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    goto :goto_d

    #@75
    .line 553
    .end local v1           #ex:Landroid/content/ActivityNotFoundException;
    :cond_75
    new-instance v0, Landroid/os/Bundle;

    #@77
    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@7a
    .end local p3
    .local v0, appSearchData:Landroid/os/Bundle;
    move-object p3, v0

    #@7b
    .end local v0           #appSearchData:Landroid/os/Bundle;
    .restart local p3
    goto :goto_24
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V
    .registers 13
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "launchActivity"
    .parameter "appSearchData"
    .parameter "globalSearch"

    #@0
    .prologue
    .line 502
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;ZLandroid/graphics/Rect;)V

    #@a
    .line 504
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;ZLandroid/graphics/Rect;)V
    .registers 8
    .parameter "initialQuery"
    .parameter "selectInitialQuery"
    .parameter "launchActivity"
    .parameter "appSearchData"
    .parameter "globalSearch"
    .parameter "sourceBounds"

    #@0
    .prologue
    .line 518
    if-eqz p5, :cond_6

    #@2
    .line 519
    invoke-virtual {p0, p1, p2, p4, p6}, Landroid/app/SearchManager;->startGlobalSearch(Ljava/lang/String;ZLandroid/os/Bundle;Landroid/graphics/Rect;)V

    #@5
    .line 526
    :goto_5
    return-void

    #@6
    .line 523
    :cond_6
    invoke-direct {p0}, Landroid/app/SearchManager;->ensureSearchDialog()V

    #@9
    .line 525
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@b
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/SearchDialog;->show(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;)Z

    #@e
    goto :goto_5
.end method

.method public stopSearch()V
    .registers 2

    #@0
    .prologue
    .line 660
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 661
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/SearchDialog;->cancel()V

    #@9
    .line 663
    :cond_9
    return-void
.end method

.method public triggerSearch(Ljava/lang/String;Landroid/content/ComponentName;Landroid/os/Bundle;)V
    .registers 10
    .parameter "query"
    .parameter "launchActivity"
    .parameter "appSearchData"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 636
    iget-object v0, p0, Landroid/app/SearchManager;->mAssociatedPackage:Ljava/lang/String;

    #@3
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_15

    #@d
    .line 637
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v1, "invoking app search on a different package not associated with this search manager"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 640
    :cond_15
    if-eqz p1, :cond_1d

    #@17
    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    #@1a
    move-result v0

    #@1b
    if-nez v0, :cond_26

    #@1d
    .line 641
    :cond_1d
    const-string v0, "SearchManager"

    #@1f
    const-string/jumbo v1, "triggerSearch called with empty query, ignoring."

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 646
    :goto_25
    return-void

    #@26
    :cond_26
    move-object v0, p0

    #@27
    move-object v1, p1

    #@28
    move-object v3, p2

    #@29
    move-object v4, p3

    #@2a
    move v5, v2

    #@2b
    .line 644
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    #@2e
    .line 645
    iget-object v0, p0, Landroid/app/SearchManager;->mSearchDialog:Landroid/app/SearchDialog;

    #@30
    invoke-virtual {v0}, Landroid/app/SearchDialog;->launchQuerySearch()V

    #@33
    goto :goto_25
.end method
