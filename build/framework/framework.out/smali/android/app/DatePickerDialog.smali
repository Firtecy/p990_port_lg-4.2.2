.class public Landroid/app/DatePickerDialog;
.super Landroid/app/AlertDialog;
.source "DatePickerDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/DatePicker$OnDateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/DatePickerDialog$OnDateSetListener;
    }
.end annotation


# static fields
.field private static final DAY:Ljava/lang/String; = "day"

.field private static final MONTH:Ljava/lang/String; = "month"

.field private static final YEAR:Ljava/lang/String; = "year"


# instance fields
.field private final mCalendar:Ljava/util/Calendar;

.field private final mCallBack:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private final mDatePicker:Landroid/widget/DatePicker;

.field private mTitleNeedsUpdate:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V
    .registers 12
    .parameter "context"
    .parameter "theme"
    .parameter "callBack"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    #@3
    .line 50
    const/4 v3, 0x1

    #@4
    iput-boolean v3, p0, Landroid/app/DatePickerDialog;->mTitleNeedsUpdate:Z

    #@6
    .line 98
    iput-object p3, p0, Landroid/app/DatePickerDialog;->mCallBack:Landroid/app/DatePickerDialog$OnDateSetListener;

    #@8
    .line 100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@b
    move-result-object v3

    #@c
    iput-object v3, p0, Landroid/app/DatePickerDialog;->mCalendar:Ljava/util/Calendar;

    #@e
    .line 102
    invoke-virtual {p0}, Landroid/app/DatePickerDialog;->getContext()Landroid/content/Context;

    #@11
    move-result-object v1

    #@12
    .line 103
    .local v1, themeContext:Landroid/content/Context;
    const/4 v3, -0x1

    #@13
    const v4, 0x104045a

    #@16
    invoke-virtual {v1, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {p0, v3, v4, p0}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@1d
    .line 104
    const/4 v3, 0x0

    #@1e
    invoke-virtual {p0, v3}, Landroid/app/DatePickerDialog;->setIcon(I)V

    #@21
    .line 106
    const-string/jumbo v3, "layout_inflater"

    #@24
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Landroid/view/LayoutInflater;

    #@2a
    .line 108
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v3, 0x1090038

    #@2d
    const/4 v4, 0x0

    #@2e
    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@31
    move-result-object v2

    #@32
    .line 109
    .local v2, view:Landroid/view/View;
    invoke-virtual {p0, v2}, Landroid/app/DatePickerDialog;->setView(Landroid/view/View;)V

    #@35
    .line 110
    const v3, 0x102028d

    #@38
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3b
    move-result-object v3

    #@3c
    check-cast v3, Landroid/widget/DatePicker;

    #@3e
    iput-object v3, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@40
    .line 111
    iget-object v3, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@42
    invoke-virtual {v3, p4, p5, p6, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    #@45
    .line 112
    invoke-direct {p0, p4, p5, p6}, Landroid/app/DatePickerDialog;->updateTitle(III)V

    #@48
    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V
    .registers 13
    .parameter "context"
    .parameter "callBack"
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 79
    const/4 v2, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    move v6, p5

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    #@a
    .line 80
    return-void
.end method

.method private tryNotifyDateSet()V
    .registers 6

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mCallBack:Landroid/app/DatePickerDialog$OnDateSetListener;

    #@2
    if-eqz v0, :cond_22

    #@4
    .line 147
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@6
    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    #@9
    .line 148
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mCallBack:Landroid/app/DatePickerDialog$OnDateSetListener;

    #@b
    iget-object v1, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@d
    iget-object v2, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@f
    invoke-virtual {v2}, Landroid/widget/DatePicker;->getYear()I

    #@12
    move-result v2

    #@13
    iget-object v3, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@15
    invoke-virtual {v3}, Landroid/widget/DatePicker;->getMonth()I

    #@18
    move-result v3

    #@19
    iget-object v4, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@1b
    invoke-virtual {v4}, Landroid/widget/DatePicker;->getDayOfMonth()I

    #@1e
    move-result v4

    #@1f
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/app/DatePickerDialog$OnDateSetListener;->onDateSet(Landroid/widget/DatePicker;III)V

    #@22
    .line 151
    :cond_22
    return-void
.end method

.method private updateTitle(III)V
    .registers 10
    .parameter "year"
    .parameter "month"
    .parameter "day"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 160
    iget-object v1, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@3
    invoke-virtual {v1}, Landroid/widget/DatePicker;->getCalendarViewShown()Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_2f

    #@9
    .line 161
    iget-object v1, p0, Landroid/app/DatePickerDialog;->mCalendar:Ljava/util/Calendar;

    #@b
    invoke-virtual {v1, v5, p1}, Ljava/util/Calendar;->set(II)V

    #@e
    .line 162
    iget-object v1, p0, Landroid/app/DatePickerDialog;->mCalendar:Ljava/util/Calendar;

    #@10
    const/4 v2, 0x2

    #@11
    invoke-virtual {v1, v2, p2}, Ljava/util/Calendar;->set(II)V

    #@14
    .line 163
    iget-object v1, p0, Landroid/app/DatePickerDialog;->mCalendar:Ljava/util/Calendar;

    #@16
    const/4 v2, 0x5

    #@17
    invoke-virtual {v1, v2, p3}, Ljava/util/Calendar;->set(II)V

    #@1a
    .line 164
    iget-object v1, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@1c
    iget-object v2, p0, Landroid/app/DatePickerDialog;->mCalendar:Ljava/util/Calendar;

    #@1e
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    #@21
    move-result-wide v2

    #@22
    const v4, 0x18016

    #@25
    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 171
    .local v0, title:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/DatePickerDialog;->setTitle(Ljava/lang/CharSequence;)V

    #@2c
    .line 172
    iput-boolean v5, p0, Landroid/app/DatePickerDialog;->mTitleNeedsUpdate:Z

    #@2e
    .line 179
    .end local v0           #title:Ljava/lang/String;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 174
    :cond_2f
    iget-boolean v1, p0, Landroid/app/DatePickerDialog;->mTitleNeedsUpdate:Z

    #@31
    if-eqz v1, :cond_2e

    #@33
    .line 175
    const/4 v1, 0x0

    #@34
    iput-boolean v1, p0, Landroid/app/DatePickerDialog;->mTitleNeedsUpdate:Z

    #@36
    .line 176
    const v1, 0x1040458

    #@39
    invoke-virtual {p0, v1}, Landroid/app/DatePickerDialog;->setTitle(I)V

    #@3c
    goto :goto_2e
.end method


# virtual methods
.method public getDatePicker()Landroid/widget/DatePicker;
    .registers 2

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@2
    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 116
    invoke-direct {p0}, Landroid/app/DatePickerDialog;->tryNotifyDateSet()V

    #@3
    .line 117
    return-void
.end method

.method public onDateChanged(Landroid/widget/DatePicker;III)V
    .registers 6
    .parameter "view"
    .parameter "year"
    .parameter "month"
    .parameter "day"

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@2
    invoke-virtual {v0, p2, p3, p4, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    #@5
    .line 122
    invoke-direct {p0, p2, p3, p4}, Landroid/app/DatePickerDialog;->updateTitle(III)V

    #@8
    .line 123
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 192
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 193
    const-string/jumbo v3, "year"

    #@6
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@9
    move-result v2

    #@a
    .line 194
    .local v2, year:I
    const-string/jumbo v3, "month"

    #@d
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10
    move-result v1

    #@11
    .line 195
    .local v1, month:I
    const-string v3, "day"

    #@13
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@16
    move-result v0

    #@17
    .line 196
    .local v0, day:I
    iget-object v3, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@19
    invoke-virtual {v3, v2, v1, v0, p0}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    #@1c
    .line 197
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, state:Landroid/os/Bundle;
    const-string/jumbo v1, "year"

    #@7
    iget-object v2, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@9
    invoke-virtual {v2}, Landroid/widget/DatePicker;->getYear()I

    #@c
    move-result v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@10
    .line 185
    const-string/jumbo v1, "month"

    #@13
    iget-object v2, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@15
    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    #@18
    move-result v2

    #@19
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1c
    .line 186
    const-string v1, "day"

    #@1e
    iget-object v2, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@20
    invoke-virtual {v2}, Landroid/widget/DatePicker;->getDayOfMonth()I

    #@23
    move-result v2

    #@24
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@27
    .line 187
    return-object v0
.end method

.method protected onStop()V
    .registers 1

    #@0
    .prologue
    .line 155
    invoke-direct {p0}, Landroid/app/DatePickerDialog;->tryNotifyDateSet()V

    #@3
    .line 156
    invoke-super {p0}, Landroid/app/AlertDialog;->onStop()V

    #@6
    .line 157
    return-void
.end method

.method public updateDate(III)V
    .registers 5
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Landroid/app/DatePickerDialog;->mDatePicker:Landroid/widget/DatePicker;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/DatePicker;->updateDate(III)V

    #@5
    .line 143
    return-void
.end method
