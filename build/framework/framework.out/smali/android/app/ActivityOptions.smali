.class public Landroid/app/ActivityOptions;
.super Ljava/lang/Object;
.source "ActivityOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/ActivityOptions$OnAnimationStartedListener;
    }
.end annotation


# static fields
.field public static final ANIM_CUSTOM:I = 0x1

.field public static final ANIM_NONE:I = 0x0

.field public static final ANIM_SCALE_UP:I = 0x2

.field public static final ANIM_THUMBNAIL_SCALE_DOWN:I = 0x4

.field public static final ANIM_THUMBNAIL_SCALE_UP:I = 0x3

.field public static final KEY_ANIM_ENTER_RES_ID:Ljava/lang/String; = "android:animEnterRes"

.field public static final KEY_ANIM_EXIT_RES_ID:Ljava/lang/String; = "android:animExitRes"

.field public static final KEY_ANIM_START_HEIGHT:Ljava/lang/String; = "android:animStartHeight"

.field public static final KEY_ANIM_START_LISTENER:Ljava/lang/String; = "android:animStartListener"

.field public static final KEY_ANIM_START_WIDTH:Ljava/lang/String; = "android:animStartWidth"

.field public static final KEY_ANIM_START_X:Ljava/lang/String; = "android:animStartX"

.field public static final KEY_ANIM_START_Y:Ljava/lang/String; = "android:animStartY"

.field public static final KEY_ANIM_THUMBNAIL:Ljava/lang/String; = "android:animThumbnail"

.field public static final KEY_ANIM_TYPE:Ljava/lang/String; = "android:animType"

.field public static final KEY_PACKAGE_NAME:Ljava/lang/String; = "android:packageName"


# instance fields
.field private mAnimationStartedListener:Landroid/os/IRemoteCallback;

.field private mAnimationType:I

.field private mCustomEnterResId:I

.field private mCustomExitResId:I

.field private mPackageName:Ljava/lang/String;

.field private mStartHeight:I

.field private mStartWidth:I

.field private mStartX:I

.field private mStartY:I

.field private mThumbnail:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 305
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 105
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@6
    .line 306
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .registers 5
    .parameter "opts"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 309
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 105
    iput v2, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@6
    .line 310
    const-string v0, "android:packageName"

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@e
    .line 311
    const-string v0, "android:animType"

    #@10
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@16
    .line 312
    iget v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@18
    const/4 v1, 0x1

    #@19
    if-ne v0, v1, :cond_38

    #@1b
    .line 313
    const-string v0, "android:animEnterRes"

    #@1d
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@20
    move-result v0

    #@21
    iput v0, p0, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@23
    .line 314
    const-string v0, "android:animExitRes"

    #@25
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@28
    move-result v0

    #@29
    iput v0, p0, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@2b
    .line 315
    const-string v0, "android:animStartListener"

    #@2d
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIBinder(Ljava/lang/String;)Landroid/os/IBinder;

    #@30
    move-result-object v0

    #@31
    invoke-static {v0}, Landroid/os/IRemoteCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IRemoteCallback;

    #@34
    move-result-object v0

    #@35
    iput-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@37
    .line 330
    :cond_37
    :goto_37
    return-void

    #@38
    .line 317
    :cond_38
    iget v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@3a
    const/4 v1, 0x2

    #@3b
    if-ne v0, v1, :cond_5e

    #@3d
    .line 318
    const-string v0, "android:animStartX"

    #@3f
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@42
    move-result v0

    #@43
    iput v0, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@45
    .line 319
    const-string v0, "android:animStartY"

    #@47
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@4a
    move-result v0

    #@4b
    iput v0, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@4d
    .line 320
    const-string v0, "android:animStartWidth"

    #@4f
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@52
    move-result v0

    #@53
    iput v0, p0, Landroid/app/ActivityOptions;->mStartWidth:I

    #@55
    .line 321
    const-string v0, "android:animStartHeight"

    #@57
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@5a
    move-result v0

    #@5b
    iput v0, p0, Landroid/app/ActivityOptions;->mStartHeight:I

    #@5d
    goto :goto_37

    #@5e
    .line 322
    :cond_5e
    iget v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@60
    const/4 v1, 0x3

    #@61
    if-eq v0, v1, :cond_68

    #@63
    iget v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@65
    const/4 v1, 0x4

    #@66
    if-ne v0, v1, :cond_37

    #@68
    .line 324
    :cond_68
    const-string v0, "android:animThumbnail"

    #@6a
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@6d
    move-result-object v0

    #@6e
    check-cast v0, Landroid/graphics/Bitmap;

    #@70
    iput-object v0, p0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@72
    .line 325
    const-string v0, "android:animStartX"

    #@74
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@77
    move-result v0

    #@78
    iput v0, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@7a
    .line 326
    const-string v0, "android:animStartY"

    #@7c
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@7f
    move-result v0

    #@80
    iput v0, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@82
    .line 327
    const-string v0, "android:animStartListener"

    #@84
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIBinder(Ljava/lang/String;)Landroid/os/IBinder;

    #@87
    move-result-object v0

    #@88
    invoke-static {v0}, Landroid/os/IRemoteCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IRemoteCallback;

    #@8b
    move-result-object v0

    #@8c
    iput-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@8e
    goto :goto_37
.end method

.method public static abort(Landroid/os/Bundle;)V
    .registers 2
    .parameter "options"

    #@0
    .prologue
    .line 394
    if-eqz p0, :cond_a

    #@2
    .line 395
    new-instance v0, Landroid/app/ActivityOptions;

    #@4
    invoke-direct {v0, p0}, Landroid/app/ActivityOptions;-><init>(Landroid/os/Bundle;)V

    #@7
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->abort()V

    #@a
    .line 397
    :cond_a
    return-void
.end method

.method public static makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
    .registers 4
    .parameter "context"
    .parameter "enterResId"
    .parameter "exitResId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 130
    invoke-static {p0, p1, p2, v0, v0}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;
    .registers 7
    .parameter "context"
    .parameter "enterResId"
    .parameter "exitResId"
    .parameter "handler"
    .parameter "listener"

    #@0
    .prologue
    .line 154
    new-instance v0, Landroid/app/ActivityOptions;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityOptions;-><init>()V

    #@5
    .line 155
    .local v0, opts:Landroid/app/ActivityOptions;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@8
    move-result-object v1

    #@9
    iput-object v1, v0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@b
    .line 156
    const/4 v1, 0x1

    #@c
    iput v1, v0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@e
    .line 157
    iput p1, v0, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@10
    .line 158
    iput p2, v0, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@12
    .line 159
    invoke-direct {v0, p3, p4}, Landroid/app/ActivityOptions;->setListener(Landroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)V

    #@15
    .line 160
    return-object v0
.end method

.method public static makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;
    .registers 9
    .parameter "source"
    .parameter "startX"
    .parameter "startY"
    .parameter "startWidth"
    .parameter "startHeight"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 209
    new-instance v0, Landroid/app/ActivityOptions;

    #@3
    invoke-direct {v0}, Landroid/app/ActivityOptions;-><init>()V

    #@6
    .line 210
    .local v0, opts:Landroid/app/ActivityOptions;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    iput-object v2, v0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@10
    .line 211
    iput v3, v0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@12
    .line 212
    new-array v1, v3, [I

    #@14
    .line 213
    .local v1, pts:[I
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    #@17
    .line 214
    const/4 v2, 0x0

    #@18
    aget v2, v1, v2

    #@1a
    add-int/2addr v2, p1

    #@1b
    iput v2, v0, Landroid/app/ActivityOptions;->mStartX:I

    #@1d
    .line 215
    const/4 v2, 0x1

    #@1e
    aget v2, v1, v2

    #@20
    add-int/2addr v2, p2

    #@21
    iput v2, v0, Landroid/app/ActivityOptions;->mStartY:I

    #@23
    .line 216
    iput p3, v0, Landroid/app/ActivityOptions;->mStartWidth:I

    #@25
    .line 217
    iput p4, v0, Landroid/app/ActivityOptions;->mStartHeight:I

    #@27
    .line 218
    return-object v0
.end method

.method private static makeThumbnailAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;Z)Landroid/app/ActivityOptions;
    .registers 9
    .parameter "source"
    .parameter "thumbnail"
    .parameter "startX"
    .parameter "startY"
    .parameter "listener"
    .parameter "scaleUp"

    #@0
    .prologue
    .line 293
    new-instance v0, Landroid/app/ActivityOptions;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityOptions;-><init>()V

    #@5
    .line 294
    .local v0, opts:Landroid/app/ActivityOptions;
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    iput-object v2, v0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@f
    .line 295
    if-eqz p5, :cond_30

    #@11
    const/4 v2, 0x3

    #@12
    :goto_12
    iput v2, v0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@14
    .line 296
    iput-object p1, v0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@16
    .line 297
    const/4 v2, 0x2

    #@17
    new-array v1, v2, [I

    #@19
    .line 298
    .local v1, pts:[I
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    #@1c
    .line 299
    const/4 v2, 0x0

    #@1d
    aget v2, v1, v2

    #@1f
    add-int/2addr v2, p2

    #@20
    iput v2, v0, Landroid/app/ActivityOptions;->mStartX:I

    #@22
    .line 300
    const/4 v2, 0x1

    #@23
    aget v2, v1, v2

    #@25
    add-int/2addr v2, p3

    #@26
    iput v2, v0, Landroid/app/ActivityOptions;->mStartY:I

    #@28
    .line 301
    invoke-virtual {p0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    #@2b
    move-result-object v2

    #@2c
    invoke-direct {v0, v2, p4}, Landroid/app/ActivityOptions;->setListener(Landroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)V

    #@2f
    .line 302
    return-object v0

    #@30
    .line 295
    .end local v1           #pts:[I
    :cond_30
    const/4 v2, 0x4

    #@31
    goto :goto_12
.end method

.method public static makeThumbnailScaleDownAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;
    .registers 11
    .parameter "source"
    .parameter "thumbnail"
    .parameter "startX"
    .parameter "startY"
    .parameter "listener"

    #@0
    .prologue
    .line 287
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-static/range {v0 .. v5}, Landroid/app/ActivityOptions;->makeThumbnailAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;Z)Landroid/app/ActivityOptions;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/app/ActivityOptions;
    .registers 5
    .parameter "source"
    .parameter "thumbnail"
    .parameter "startX"
    .parameter "startY"

    #@0
    .prologue
    .line 242
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Landroid/app/ActivityOptions;->makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;
    .registers 11
    .parameter "source"
    .parameter "thumbnail"
    .parameter "startX"
    .parameter "startY"
    .parameter "listener"

    #@0
    .prologue
    .line 265
    const/4 v5, 0x1

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    invoke-static/range {v0 .. v5}, Landroid/app/ActivityOptions;->makeThumbnailAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IILandroid/app/ActivityOptions$OnAnimationStartedListener;Z)Landroid/app/ActivityOptions;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private setListener(Landroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)V
    .registers 6
    .parameter "handler"
    .parameter "listener"

    #@0
    .prologue
    .line 164
    if-eqz p2, :cond_b

    #@2
    .line 165
    move-object v1, p1

    #@3
    .line 166
    .local v1, h:Landroid/os/Handler;
    move-object v0, p2

    #@4
    .line 167
    .local v0, finalListener:Landroid/app/ActivityOptions$OnAnimationStartedListener;
    new-instance v2, Landroid/app/ActivityOptions$1;

    #@6
    invoke-direct {v2, p0, v1, v0}, Landroid/app/ActivityOptions$1;-><init>(Landroid/app/ActivityOptions;Landroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)V

    #@9
    iput-object v2, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@b
    .line 177
    .end local v0           #finalListener:Landroid/app/ActivityOptions$OnAnimationStartedListener;
    .end local v1           #h:Landroid/os/Handler;
    :cond_b
    return-void
.end method


# virtual methods
.method public abort()V
    .registers 3

    #@0
    .prologue
    .line 384
    iget-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 386
    :try_start_4
    iget-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-interface {v0, v1}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_a} :catch_b

    #@a
    .line 390
    :cond_a
    :goto_a
    return-void

    #@b
    .line 387
    :catch_b
    move-exception v0

    #@c
    goto :goto_a
.end method

.method public getAnimationType()I
    .registers 2

    #@0
    .prologue
    .line 339
    iget v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@2
    return v0
.end method

.method public getCustomEnterResId()I
    .registers 2

    #@0
    .prologue
    .line 344
    iget v0, p0, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@2
    return v0
.end method

.method public getCustomExitResId()I
    .registers 2

    #@0
    .prologue
    .line 349
    iget v0, p0, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@2
    return v0
.end method

.method public getOnAnimationStartListener()Landroid/os/IRemoteCallback;
    .registers 2

    #@0
    .prologue
    .line 379
    iget-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@2
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 334
    iget-object v0, p0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getStartHeight()I
    .registers 2

    #@0
    .prologue
    .line 374
    iget v0, p0, Landroid/app/ActivityOptions;->mStartHeight:I

    #@2
    return v0
.end method

.method public getStartWidth()I
    .registers 2

    #@0
    .prologue
    .line 369
    iget v0, p0, Landroid/app/ActivityOptions;->mStartWidth:I

    #@2
    return v0
.end method

.method public getStartX()I
    .registers 2

    #@0
    .prologue
    .line 359
    iget v0, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@2
    return v0
.end method

.method public getStartY()I
    .registers 2

    #@0
    .prologue
    .line 364
    iget v0, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@2
    return v0
.end method

.method public getThumbnail()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 354
    iget-object v0, p0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@2
    return-object v0
.end method

.method public toBundle()Landroid/os/Bundle;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 462
    new-instance v0, Landroid/os/Bundle;

    #@3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@6
    .line 463
    .local v0, b:Landroid/os/Bundle;
    iget-object v2, p0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@8
    if-eqz v2, :cond_11

    #@a
    .line 464
    const-string v2, "android:packageName"

    #@c
    iget-object v3, p0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@e
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 466
    :cond_11
    iget v2, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@13
    packed-switch v2, :pswitch_data_8c

    #@16
    .line 491
    :goto_16
    return-object v0

    #@17
    .line 468
    :pswitch_17
    const-string v2, "android:animType"

    #@19
    iget v3, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@1b
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@1e
    .line 469
    const-string v2, "android:animEnterRes"

    #@20
    iget v3, p0, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@22
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@25
    .line 470
    const-string v2, "android:animExitRes"

    #@27
    iget v3, p0, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@29
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2c
    .line 471
    const-string v2, "android:animStartListener"

    #@2e
    iget-object v3, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@30
    if-eqz v3, :cond_38

    #@32
    iget-object v1, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@34
    invoke-interface {v1}, Landroid/os/IRemoteCallback;->asBinder()Landroid/os/IBinder;

    #@37
    move-result-object v1

    #@38
    :cond_38
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    #@3b
    goto :goto_16

    #@3c
    .line 475
    :pswitch_3c
    const-string v1, "android:animType"

    #@3e
    iget v2, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@40
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@43
    .line 476
    const-string v1, "android:animStartX"

    #@45
    iget v2, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@47
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@4a
    .line 477
    const-string v1, "android:animStartY"

    #@4c
    iget v2, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@4e
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@51
    .line 478
    const-string v1, "android:animStartWidth"

    #@53
    iget v2, p0, Landroid/app/ActivityOptions;->mStartWidth:I

    #@55
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@58
    .line 479
    const-string v1, "android:animStartHeight"

    #@5a
    iget v2, p0, Landroid/app/ActivityOptions;->mStartHeight:I

    #@5c
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@5f
    goto :goto_16

    #@60
    .line 483
    :pswitch_60
    const-string v2, "android:animType"

    #@62
    iget v3, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@64
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@67
    .line 484
    const-string v2, "android:animThumbnail"

    #@69
    iget-object v3, p0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@6b
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@6e
    .line 485
    const-string v2, "android:animStartX"

    #@70
    iget v3, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@72
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@75
    .line 486
    const-string v2, "android:animStartY"

    #@77
    iget v3, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@79
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@7c
    .line 487
    const-string v2, "android:animStartListener"

    #@7e
    iget-object v3, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@80
    if-eqz v3, :cond_88

    #@82
    iget-object v1, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@84
    invoke-interface {v1}, Landroid/os/IRemoteCallback;->asBinder()Landroid/os/IBinder;

    #@87
    move-result-object v1

    #@88
    :cond_88
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    #@8b
    goto :goto_16

    #@8c
    .line 466
    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_17
        :pswitch_3c
        :pswitch_60
        :pswitch_60
    .end packed-switch
.end method

.method public update(Landroid/app/ActivityOptions;)V
    .registers 5
    .parameter "otherOptions"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 405
    iget-object v0, p1, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@3
    if-eqz v0, :cond_9

    #@5
    .line 406
    iget-object v0, p1, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@7
    iput-object v0, p0, Landroid/app/ActivityOptions;->mPackageName:Ljava/lang/String;

    #@9
    .line 408
    :cond_9
    iget v0, p1, Landroid/app/ActivityOptions;->mAnimationType:I

    #@b
    packed-switch v0, :pswitch_data_72

    #@e
    .line 451
    :goto_e
    return-void

    #@f
    .line 410
    :pswitch_f
    iget v0, p1, Landroid/app/ActivityOptions;->mAnimationType:I

    #@11
    iput v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@13
    .line 411
    iget v0, p1, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@15
    iput v0, p0, Landroid/app/ActivityOptions;->mCustomEnterResId:I

    #@17
    .line 412
    iget v0, p1, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@19
    iput v0, p0, Landroid/app/ActivityOptions;->mCustomExitResId:I

    #@1b
    .line 413
    iput-object v2, p0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@1d
    .line 414
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@1f
    if-eqz v0, :cond_27

    #@21
    .line 416
    :try_start_21
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-interface {v0, v1}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_27} :catch_70

    #@27
    .line 420
    :cond_27
    :goto_27
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@29
    iput-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@2b
    goto :goto_e

    #@2c
    .line 423
    :pswitch_2c
    iget v0, p1, Landroid/app/ActivityOptions;->mAnimationType:I

    #@2e
    iput v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@30
    .line 424
    iget v0, p1, Landroid/app/ActivityOptions;->mStartX:I

    #@32
    iput v0, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@34
    .line 425
    iget v0, p1, Landroid/app/ActivityOptions;->mStartY:I

    #@36
    iput v0, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@38
    .line 426
    iget v0, p1, Landroid/app/ActivityOptions;->mStartWidth:I

    #@3a
    iput v0, p0, Landroid/app/ActivityOptions;->mStartWidth:I

    #@3c
    .line 427
    iget v0, p1, Landroid/app/ActivityOptions;->mStartHeight:I

    #@3e
    iput v0, p0, Landroid/app/ActivityOptions;->mStartHeight:I

    #@40
    .line 428
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@42
    if-eqz v0, :cond_4a

    #@44
    .line 430
    :try_start_44
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@46
    const/4 v1, 0x0

    #@47
    invoke-interface {v0, v1}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_4a
    .catch Landroid/os/RemoteException; {:try_start_44 .. :try_end_4a} :catch_6e

    #@4a
    .line 434
    :cond_4a
    :goto_4a
    iput-object v2, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@4c
    goto :goto_e

    #@4d
    .line 438
    :pswitch_4d
    iget v0, p1, Landroid/app/ActivityOptions;->mAnimationType:I

    #@4f
    iput v0, p0, Landroid/app/ActivityOptions;->mAnimationType:I

    #@51
    .line 439
    iget-object v0, p1, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@53
    iput-object v0, p0, Landroid/app/ActivityOptions;->mThumbnail:Landroid/graphics/Bitmap;

    #@55
    .line 440
    iget v0, p1, Landroid/app/ActivityOptions;->mStartX:I

    #@57
    iput v0, p0, Landroid/app/ActivityOptions;->mStartX:I

    #@59
    .line 441
    iget v0, p1, Landroid/app/ActivityOptions;->mStartY:I

    #@5b
    iput v0, p0, Landroid/app/ActivityOptions;->mStartY:I

    #@5d
    .line 442
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@5f
    if-eqz v0, :cond_67

    #@61
    .line 444
    :try_start_61
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@63
    const/4 v1, 0x0

    #@64
    invoke-interface {v0, v1}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_67
    .catch Landroid/os/RemoteException; {:try_start_61 .. :try_end_67} :catch_6c

    #@67
    .line 448
    :cond_67
    :goto_67
    iget-object v0, p1, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@69
    iput-object v0, p0, Landroid/app/ActivityOptions;->mAnimationStartedListener:Landroid/os/IRemoteCallback;

    #@6b
    goto :goto_e

    #@6c
    .line 445
    :catch_6c
    move-exception v0

    #@6d
    goto :goto_67

    #@6e
    .line 431
    :catch_6e
    move-exception v0

    #@6f
    goto :goto_4a

    #@70
    .line 417
    :catch_70
    move-exception v0

    #@71
    goto :goto_27

    #@72
    .line 408
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_f
        :pswitch_2c
        :pswitch_4d
        :pswitch_4d
    .end packed-switch
.end method
