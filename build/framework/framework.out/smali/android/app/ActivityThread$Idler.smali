.class Landroid/app/ActivityThread$Idler;
.super Ljava/lang/Object;
.source "ActivityThread.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Idler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/app/ActivityThread;


# direct methods
.method private constructor <init>(Landroid/app/ActivityThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1469
    iput-object p1, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1469
    invoke-direct {p0, p1}, Landroid/app/ActivityThread$Idler;-><init>(Landroid/app/ActivityThread;)V

    #@3
    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1471
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@3
    iget-object v0, v4, Landroid/app/ActivityThread;->mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

    #@5
    .line 1472
    .local v0, a:Landroid/app/ActivityThread$ActivityClientRecord;
    const/4 v3, 0x0

    #@6
    .line 1473
    .local v3, stopProfiling:Z
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@8
    iget-object v4, v4, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@a
    if-eqz v4, :cond_1d

    #@c
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@e
    iget-object v4, v4, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@10
    iget-object v4, v4, Landroid/app/ActivityThread$Profiler;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@12
    if-eqz v4, :cond_1d

    #@14
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@16
    iget-object v4, v4, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@18
    iget-boolean v4, v4, Landroid/app/ActivityThread$Profiler;->autoStopProfiler:Z

    #@1a
    if-eqz v4, :cond_1d

    #@1c
    .line 1475
    const/4 v3, 0x1

    #@1d
    .line 1477
    :cond_1d
    if-eqz v0, :cond_42

    #@1f
    .line 1478
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@21
    iput-object v6, v4, Landroid/app/ActivityThread;->mNewActivities:Landroid/app/ActivityThread$ActivityClientRecord;

    #@23
    .line 1479
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@26
    move-result-object v1

    #@27
    .line 1486
    .local v1, am:Landroid/app/IActivityManager;
    :cond_27
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@29
    if-eqz v4, :cond_3b

    #@2b
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    #@2d
    iget-boolean v4, v4, Landroid/app/Activity;->mFinished:Z

    #@2f
    if-nez v4, :cond_3b

    #@31
    .line 1488
    :try_start_31
    iget-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@33
    iget-object v5, v0, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;

    #@35
    invoke-interface {v1, v4, v5, v3}, Landroid/app/IActivityManager;->activityIdle(Landroid/os/IBinder;Landroid/content/res/Configuration;Z)V

    #@38
    .line 1489
    const/4 v4, 0x0

    #@39
    iput-object v4, v0, Landroid/app/ActivityThread$ActivityClientRecord;->createdConfig:Landroid/content/res/Configuration;
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_31 .. :try_end_3b} :catch_52

    #@3b
    .line 1494
    :cond_3b
    :goto_3b
    move-object v2, v0

    #@3c
    .line 1495
    .local v2, prev:Landroid/app/ActivityThread$ActivityClientRecord;
    iget-object v0, v0, Landroid/app/ActivityThread$ActivityClientRecord;->nextIdle:Landroid/app/ActivityThread$ActivityClientRecord;

    #@3e
    .line 1496
    iput-object v6, v2, Landroid/app/ActivityThread$ActivityClientRecord;->nextIdle:Landroid/app/ActivityThread$ActivityClientRecord;

    #@40
    .line 1497
    if-nez v0, :cond_27

    #@42
    .line 1499
    .end local v1           #am:Landroid/app/IActivityManager;
    .end local v2           #prev:Landroid/app/ActivityThread$ActivityClientRecord;
    :cond_42
    if-eqz v3, :cond_4b

    #@44
    .line 1500
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@46
    iget-object v4, v4, Landroid/app/ActivityThread;->mProfiler:Landroid/app/ActivityThread$Profiler;

    #@48
    invoke-virtual {v4}, Landroid/app/ActivityThread$Profiler;->stopProfiling()V

    #@4b
    .line 1502
    :cond_4b
    iget-object v4, p0, Landroid/app/ActivityThread$Idler;->this$0:Landroid/app/ActivityThread;

    #@4d
    invoke-virtual {v4}, Landroid/app/ActivityThread;->ensureJitEnabled()V

    #@50
    .line 1503
    const/4 v4, 0x0

    #@51
    return v4

    #@52
    .line 1490
    .restart local v1       #am:Landroid/app/IActivityManager;
    :catch_52
    move-exception v4

    #@53
    goto :goto_3b
.end method
