.class public abstract Landroid/app/ActivityManagerNative;
.super Landroid/os/Binder;
.source "ActivityManagerNative.java"

# interfaces
.implements Landroid/app/IActivityManager;


# static fields
.field private static final gDefault:Landroid/util/Singleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Singleton",
            "<",
            "Landroid/app/IActivityManager;",
            ">;"
        }
    .end annotation
.end field

.field static sSystemReady:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 90
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/app/ActivityManagerNative;->sSystemReady:Z

    #@3
    .line 1894
    new-instance v0, Landroid/app/ActivityManagerNative$1;

    #@5
    invoke-direct {v0}, Landroid/app/ActivityManagerNative$1;-><init>()V

    #@8
    sput-object v0, Landroid/app/ActivityManagerNative;->gDefault:Landroid/util/Singleton;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 112
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 113
    const-string v0, "android.app.IActivityManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/ActivityManagerNative;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 114
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IActivityManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 62
    if-nez p0, :cond_4

    #@2
    .line 63
    const/4 v0, 0x0

    #@3
    .line 71
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 65
    :cond_4
    const-string v1, "android.app.IActivityManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/IActivityManager;

    #@c
    .line 67
    .local v0, in:Landroid/app/IActivityManager;
    if-nez v0, :cond_3

    #@e
    .line 71
    new-instance v0, Landroid/app/ActivityManagerProxy;

    #@10
    .end local v0           #in:Landroid/app/IActivityManager;
    invoke-direct {v0, p0}, Landroid/app/ActivityManagerProxy;-><init>(Landroid/os/IBinder;)V

    #@13
    goto :goto_3
.end method

.method public static broadcastStickyIntent(Landroid/content/Intent;Ljava/lang/String;I)V
    .registers 15
    .parameter "intent"
    .parameter "permission"
    .parameter "userId"

    #@0
    .prologue
    .line 98
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x0

    #@5
    const/4 v3, 0x0

    #@6
    const/4 v4, 0x0

    #@7
    const/4 v5, -0x1

    #@8
    const/4 v6, 0x0

    #@9
    const/4 v7, 0x0

    #@a
    const/4 v8, 0x0

    #@b
    const/4 v9, 0x0

    #@c
    const/4 v10, 0x1

    #@d
    move-object v2, p0

    #@e
    move v11, p2

    #@f
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_12} :catch_13

    #@12
    .line 103
    :goto_12
    return-void

    #@13
    .line 101
    :catch_13
    move-exception v0

    #@14
    goto :goto_12
.end method

.method public static getDefault()Landroid/app/IActivityManager;
    .registers 1

    #@0
    .prologue
    .line 78
    sget-object v0, Landroid/app/ActivityManagerNative;->gDefault:Landroid/util/Singleton;

    #@2
    invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/IActivityManager;

    #@8
    return-object v0
.end method

.method public static isSystemReady()Z
    .registers 1

    #@0
    .prologue
    .line 85
    sget-boolean v0, Landroid/app/ActivityManagerNative;->sSystemReady:Z

    #@2
    if-nez v0, :cond_e

    #@4
    .line 86
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Landroid/app/IActivityManager;->testIsSystemReady()Z

    #@b
    move-result v0

    #@c
    sput-boolean v0, Landroid/app/ActivityManagerNative;->sSystemReady:Z

    #@e
    .line 88
    :cond_e
    sget-boolean v0, Landroid/app/ActivityManagerNative;->sSystemReady:Z

    #@10
    return v0
.end method

.method public static noteWakeupAlarm(Landroid/app/PendingIntent;)V
    .registers 3
    .parameter "ps"

    #@0
    .prologue
    .line 107
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0}, Landroid/app/PendingIntent;->getTarget()Landroid/content/IIntentSender;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->noteWakeupAlarm(Landroid/content/IIntentSender;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 110
    :goto_b
    return-void

    #@c
    .line 108
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 1891
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 206
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    packed-switch p1, :pswitch_data_1a58

    #@3
    .line 1887
    :pswitch_3
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v5

    #@7
    :goto_7
    return v5

    #@8
    .line 121
    :pswitch_8
    const-string v5, "android.app.IActivityManager"

    #@a
    move-object/from16 v0, p2

    #@c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f
    .line 122
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@12
    move-result-object v114

    #@13
    .line 123
    .local v114, b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@16
    move-result-object v6

    #@17
    .line 124
    .local v6, app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    move-object/from16 v0, p2

    #@1b
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e
    move-result-object v7

    #@1f
    check-cast v7, Landroid/content/Intent;

    #@21
    .line 125
    .local v7, intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v8

    #@25
    .line 126
    .local v8, resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@28
    move-result-object v9

    #@29
    .line 127
    .local v9, resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v10

    #@2d
    .line 128
    .local v10, resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v11

    #@31
    .line 129
    .local v11, requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v12

    #@35
    .line 130
    .local v12, startFlags:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38
    move-result-object v13

    #@39
    .line 131
    .local v13, profileFile:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v5

    #@3d
    if-eqz v5, :cond_66

    #@3f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@42
    move-result-object v14

    #@43
    .line 133
    .local v14, profileFd:Landroid/os/ParcelFileDescriptor;
    :goto_43
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v5

    #@47
    if-eqz v5, :cond_68

    #@49
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4b
    move-object/from16 v0, p2

    #@4d
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@50
    move-result-object v5

    #@51
    check-cast v5, Landroid/os/Bundle;

    #@53
    move-object v15, v5

    #@54
    .local v15, options:Landroid/os/Bundle;
    :goto_54
    move-object/from16 v5, p0

    #@56
    .line 135
    invoke-virtual/range {v5 .. v15}, Landroid/app/ActivityManagerNative;->startActivity(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)I

    #@59
    move-result v176

    #@5a
    .line 138
    .local v176, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d
    .line 139
    move-object/from16 v0, p3

    #@5f
    move/from16 v1, v176

    #@61
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    .line 140
    const/4 v5, 0x1

    #@65
    goto :goto_7

    #@66
    .line 131
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v15           #options:Landroid/os/Bundle;
    .end local v176           #result:I
    :cond_66
    const/4 v14, 0x0

    #@67
    goto :goto_43

    #@68
    .line 133
    .restart local v14       #profileFd:Landroid/os/ParcelFileDescriptor;
    :cond_68
    const/4 v15, 0x0

    #@69
    goto :goto_54

    #@6a
    .line 145
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v12           #startFlags:I
    .end local v13           #profileFile:Ljava/lang/String;
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_6a
    const-string v5, "android.app.IActivityManager"

    #@6c
    move-object/from16 v0, p2

    #@6e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 146
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@74
    move-result-object v114

    #@75
    .line 147
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@78
    move-result-object v6

    #@79
    .line 148
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7b
    move-object/from16 v0, p2

    #@7d
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@80
    move-result-object v7

    #@81
    check-cast v7, Landroid/content/Intent;

    #@83
    .line 149
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@86
    move-result-object v8

    #@87
    .line 150
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8a
    move-result-object v9

    #@8b
    .line 151
    .restart local v9       #resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8e
    move-result-object v10

    #@8f
    .line 152
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@92
    move-result v11

    #@93
    .line 153
    .restart local v11       #requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@96
    move-result v12

    #@97
    .line 154
    .restart local v12       #startFlags:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9a
    move-result-object v13

    #@9b
    .line 155
    .restart local v13       #profileFile:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9e
    move-result v5

    #@9f
    if-eqz v5, :cond_cd

    #@a1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@a4
    move-result-object v14

    #@a5
    .line 157
    .restart local v14       #profileFd:Landroid/os/ParcelFileDescriptor;
    :goto_a5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v5

    #@a9
    if-eqz v5, :cond_cf

    #@ab
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ad
    move-object/from16 v0, p2

    #@af
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b2
    move-result-object v5

    #@b3
    check-cast v5, Landroid/os/Bundle;

    #@b5
    move-object v15, v5

    #@b6
    .line 159
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_b6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b9
    move-result v16

    #@ba
    .local v16, userId:I
    move-object/from16 v5, p0

    #@bc
    .line 160
    invoke-virtual/range {v5 .. v16}, Landroid/app/ActivityManagerNative;->startActivityAsUser(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)I

    #@bf
    move-result v176

    #@c0
    .line 163
    .restart local v176       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c3
    .line 164
    move-object/from16 v0, p3

    #@c5
    move/from16 v1, v176

    #@c7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    .line 165
    const/4 v5, 0x1

    #@cb
    goto/16 :goto_7

    #@cd
    .line 155
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v176           #result:I
    :cond_cd
    const/4 v14, 0x0

    #@ce
    goto :goto_a5

    #@cf
    .line 157
    .restart local v14       #profileFd:Landroid/os/ParcelFileDescriptor;
    :cond_cf
    const/4 v15, 0x0

    #@d0
    goto :goto_b6

    #@d1
    .line 170
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v12           #startFlags:I
    .end local v13           #profileFile:Ljava/lang/String;
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_d1
    const-string v5, "android.app.IActivityManager"

    #@d3
    move-object/from16 v0, p2

    #@d5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d8
    .line 171
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@db
    move-result-object v114

    #@dc
    .line 172
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@df
    move-result-object v6

    #@e0
    .line 173
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e2
    move-object/from16 v0, p2

    #@e4
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7
    move-result-object v7

    #@e8
    check-cast v7, Landroid/content/Intent;

    #@ea
    .line 174
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ed
    move-result-object v8

    #@ee
    .line 175
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@f1
    move-result-object v9

    #@f2
    .line 176
    .restart local v9       #resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f5
    move-result-object v10

    #@f6
    .line 177
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f9
    move-result v11

    #@fa
    .line 178
    .restart local v11       #requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@fd
    move-result v12

    #@fe
    .line 179
    .restart local v12       #startFlags:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@101
    move-result-object v13

    #@102
    .line 180
    .restart local v13       #profileFile:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@105
    move-result v5

    #@106
    if-eqz v5, :cond_135

    #@108
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@10b
    move-result-object v14

    #@10c
    .line 182
    .restart local v14       #profileFd:Landroid/os/ParcelFileDescriptor;
    :goto_10c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@10f
    move-result v5

    #@110
    if-eqz v5, :cond_137

    #@112
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@114
    move-object/from16 v0, p2

    #@116
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@119
    move-result-object v5

    #@11a
    check-cast v5, Landroid/os/Bundle;

    #@11c
    move-object v15, v5

    #@11d
    .line 184
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_11d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@120
    move-result v16

    #@121
    .restart local v16       #userId:I
    move-object/from16 v5, p0

    #@123
    .line 185
    invoke-virtual/range {v5 .. v16}, Landroid/app/ActivityManagerNative;->startActivityAndWait(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)Landroid/app/IActivityManager$WaitResult;

    #@126
    move-result-object v176

    #@127
    .line 188
    .local v176, result:Landroid/app/IActivityManager$WaitResult;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12a
    .line 189
    const/4 v5, 0x0

    #@12b
    move-object/from16 v0, v176

    #@12d
    move-object/from16 v1, p3

    #@12f
    invoke-virtual {v0, v1, v5}, Landroid/app/IActivityManager$WaitResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@132
    .line 190
    const/4 v5, 0x1

    #@133
    goto/16 :goto_7

    #@135
    .line 180
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v176           #result:Landroid/app/IActivityManager$WaitResult;
    :cond_135
    const/4 v14, 0x0

    #@136
    goto :goto_10c

    #@137
    .line 182
    .restart local v14       #profileFd:Landroid/os/ParcelFileDescriptor;
    :cond_137
    const/4 v15, 0x0

    #@138
    goto :goto_11d

    #@139
    .line 195
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v12           #startFlags:I
    .end local v13           #profileFile:Ljava/lang/String;
    .end local v14           #profileFd:Landroid/os/ParcelFileDescriptor;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_139
    const-string v5, "android.app.IActivityManager"

    #@13b
    move-object/from16 v0, p2

    #@13d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@140
    .line 196
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@143
    move-result-object v114

    #@144
    .line 197
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@147
    move-result-object v6

    #@148
    .line 198
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14a
    move-object/from16 v0, p2

    #@14c
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14f
    move-result-object v7

    #@150
    check-cast v7, Landroid/content/Intent;

    #@152
    .line 199
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@155
    move-result-object v8

    #@156
    .line 200
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@159
    move-result-object v9

    #@15a
    .line 201
    .restart local v9       #resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15d
    move-result-object v10

    #@15e
    .line 202
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@161
    move-result v11

    #@162
    .line 203
    .restart local v11       #requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@165
    move-result v12

    #@166
    .line 204
    .restart local v12       #startFlags:I
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@168
    move-object/from16 v0, p2

    #@16a
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@16d
    move-result-object v25

    #@16e
    check-cast v25, Landroid/content/res/Configuration;

    #@170
    .line 205
    .local v25, config:Landroid/content/res/Configuration;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@173
    move-result v5

    #@174
    if-eqz v5, :cond_1aa

    #@176
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@178
    move-object/from16 v0, p2

    #@17a
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17d
    move-result-object v5

    #@17e
    check-cast v5, Landroid/os/Bundle;

    #@180
    move-object v15, v5

    #@181
    .line 207
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_181
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@184
    move-result v16

    #@185
    .restart local v16       #userId:I
    move-object/from16 v17, p0

    #@187
    move-object/from16 v18, v6

    #@189
    move-object/from16 v19, v7

    #@18b
    move-object/from16 v20, v8

    #@18d
    move-object/from16 v21, v9

    #@18f
    move-object/from16 v22, v10

    #@191
    move/from16 v23, v11

    #@193
    move/from16 v24, v12

    #@195
    move-object/from16 v26, v15

    #@197
    move/from16 v27, v16

    #@199
    .line 208
    invoke-virtual/range {v17 .. v27}, Landroid/app/ActivityManagerNative;->startActivityWithConfig(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/content/res/Configuration;Landroid/os/Bundle;I)I

    #@19c
    move-result v176

    #@19d
    .line 210
    .local v176, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a0
    .line 211
    move-object/from16 v0, p3

    #@1a2
    move/from16 v1, v176

    #@1a4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1a7
    .line 212
    const/4 v5, 0x1

    #@1a8
    goto/16 :goto_7

    #@1aa
    .line 205
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v176           #result:I
    :cond_1aa
    const/4 v15, 0x0

    #@1ab
    goto :goto_181

    #@1ac
    .line 217
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v12           #startFlags:I
    .end local v25           #config:Landroid/content/res/Configuration;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_1ac
    const-string v5, "android.app.IActivityManager"

    #@1ae
    move-object/from16 v0, p2

    #@1b0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b3
    .line 218
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b6
    move-result-object v114

    #@1b7
    .line 219
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@1ba
    move-result-object v6

    #@1bb
    .line 220
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/IntentSender;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1bd
    move-object/from16 v0, p2

    #@1bf
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1c2
    move-result-object v7

    #@1c3
    check-cast v7, Landroid/content/IntentSender;

    #@1c5
    .line 221
    .local v7, intent:Landroid/content/IntentSender;
    const/16 v29, 0x0

    #@1c7
    .line 222
    .local v29, fillInIntent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ca
    move-result v5

    #@1cb
    if-eqz v5, :cond_1d7

    #@1cd
    .line 223
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1cf
    move-object/from16 v0, p2

    #@1d1
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d4
    move-result-object v29

    #@1d5
    .end local v29           #fillInIntent:Landroid/content/Intent;
    check-cast v29, Landroid/content/Intent;

    #@1d7
    .line 225
    .restart local v29       #fillInIntent:Landroid/content/Intent;
    :cond_1d7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1da
    move-result-object v8

    #@1db
    .line 226
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1de
    move-result-object v9

    #@1df
    .line 227
    .restart local v9       #resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e2
    move-result-object v10

    #@1e3
    .line 228
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e6
    move-result v11

    #@1e7
    .line 229
    .restart local v11       #requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ea
    move-result v34

    #@1eb
    .line 230
    .local v34, flagsMask:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ee
    move-result v35

    #@1ef
    .line 231
    .local v35, flagsValues:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f2
    move-result v5

    #@1f3
    if-eqz v5, :cond_221

    #@1f5
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f7
    move-object/from16 v0, p2

    #@1f9
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1fc
    move-result-object v5

    #@1fd
    check-cast v5, Landroid/os/Bundle;

    #@1ff
    move-object v15, v5

    #@200
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_200
    move-object/from16 v26, p0

    #@202
    move-object/from16 v27, v6

    #@204
    move-object/from16 v28, v7

    #@206
    move-object/from16 v30, v8

    #@208
    move-object/from16 v31, v9

    #@20a
    move-object/from16 v32, v10

    #@20c
    move/from16 v33, v11

    #@20e
    move-object/from16 v36, v15

    #@210
    .line 233
    invoke-virtual/range {v26 .. v36}, Landroid/app/ActivityManagerNative;->startActivityIntentSender(Landroid/app/IApplicationThread;Landroid/content/IntentSender;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I

    #@213
    move-result v176

    #@214
    .line 236
    .restart local v176       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@217
    .line 237
    move-object/from16 v0, p3

    #@219
    move/from16 v1, v176

    #@21b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@21e
    .line 238
    const/4 v5, 0x1

    #@21f
    goto/16 :goto_7

    #@221
    .line 231
    .end local v15           #options:Landroid/os/Bundle;
    .end local v176           #result:I
    :cond_221
    const/4 v15, 0x0

    #@222
    goto :goto_200

    #@223
    .line 243
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/IntentSender;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v29           #fillInIntent:Landroid/content/Intent;
    .end local v34           #flagsMask:I
    .end local v35           #flagsValues:I
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_223
    const-string v5, "android.app.IActivityManager"

    #@225
    move-object/from16 v0, p2

    #@227
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22a
    .line 244
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@22d
    move-result-object v120

    #@22e
    .line 245
    .local v120, callingActivity:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@230
    move-object/from16 v0, p2

    #@232
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@235
    move-result-object v7

    #@236
    check-cast v7, Landroid/content/Intent;

    #@238
    .line 246
    .local v7, intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@23b
    move-result v5

    #@23c
    if-eqz v5, :cond_25f

    #@23e
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@240
    move-object/from16 v0, p2

    #@242
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@245
    move-result-object v5

    #@246
    check-cast v5, Landroid/os/Bundle;

    #@248
    move-object v15, v5

    #@249
    .line 248
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_249
    move-object/from16 v0, p0

    #@24b
    move-object/from16 v1, v120

    #@24d
    invoke-virtual {v0, v1, v7, v15}, Landroid/app/ActivityManagerNative;->startNextMatchingActivity(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)Z

    #@250
    move-result v176

    #@251
    .line 249
    .local v176, result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@254
    .line 250
    if-eqz v176, :cond_261

    #@256
    const/4 v5, 0x1

    #@257
    :goto_257
    move-object/from16 v0, p3

    #@259
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@25c
    .line 251
    const/4 v5, 0x1

    #@25d
    goto/16 :goto_7

    #@25f
    .line 246
    .end local v15           #options:Landroid/os/Bundle;
    .end local v176           #result:Z
    :cond_25f
    const/4 v15, 0x0

    #@260
    goto :goto_249

    #@261
    .line 250
    .restart local v15       #options:Landroid/os/Bundle;
    .restart local v176       #result:Z
    :cond_261
    const/4 v5, 0x0

    #@262
    goto :goto_257

    #@263
    .line 255
    .end local v7           #intent:Landroid/content/Intent;
    .end local v15           #options:Landroid/os/Bundle;
    .end local v120           #callingActivity:Landroid/os/IBinder;
    .end local v176           #result:Z
    :pswitch_263
    const-string v5, "android.app.IActivityManager"

    #@265
    move-object/from16 v0, p2

    #@267
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26a
    .line 256
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@26d
    move-result-object v49

    #@26e
    .line 257
    .local v49, token:Landroid/os/IBinder;
    const/16 v42, 0x0

    #@270
    .line 258
    .local v42, resultData:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@273
    move-result v41

    #@274
    .line 259
    .local v41, resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@277
    move-result v5

    #@278
    if-eqz v5, :cond_284

    #@27a
    .line 260
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27c
    move-object/from16 v0, p2

    #@27e
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@281
    move-result-object v42

    #@282
    .end local v42           #resultData:Landroid/content/Intent;
    check-cast v42, Landroid/content/Intent;

    #@284
    .line 262
    .restart local v42       #resultData:Landroid/content/Intent;
    :cond_284
    move-object/from16 v0, p0

    #@286
    move-object/from16 v1, v49

    #@288
    move/from16 v2, v41

    #@28a
    move-object/from16 v3, v42

    #@28c
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->finishActivity(Landroid/os/IBinder;ILandroid/content/Intent;)Z

    #@28f
    move-result v174

    #@290
    .line 263
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@293
    .line 264
    if-eqz v174, :cond_29e

    #@295
    const/4 v5, 0x1

    #@296
    :goto_296
    move-object/from16 v0, p3

    #@298
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@29b
    .line 265
    const/4 v5, 0x1

    #@29c
    goto/16 :goto_7

    #@29e
    .line 264
    :cond_29e
    const/4 v5, 0x0

    #@29f
    goto :goto_296

    #@2a0
    .line 269
    .end local v41           #resultCode:I
    .end local v42           #resultData:Landroid/content/Intent;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:Z
    :pswitch_2a0
    const-string v5, "android.app.IActivityManager"

    #@2a2
    move-object/from16 v0, p2

    #@2a4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a7
    .line 270
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2aa
    move-result-object v49

    #@2ab
    .line 271
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2ae
    move-result-object v10

    #@2af
    .line 272
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2b2
    move-result v11

    #@2b3
    .line 273
    .restart local v11       #requestCode:I
    move-object/from16 v0, p0

    #@2b5
    move-object/from16 v1, v49

    #@2b7
    invoke-virtual {v0, v1, v10, v11}, Landroid/app/ActivityManagerNative;->finishSubActivity(Landroid/os/IBinder;Ljava/lang/String;I)V

    #@2ba
    .line 274
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2bd
    .line 275
    const/4 v5, 0x1

    #@2be
    goto/16 :goto_7

    #@2c0
    .line 279
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_2c0
    const-string v5, "android.app.IActivityManager"

    #@2c2
    move-object/from16 v0, p2

    #@2c4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c7
    .line 280
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2ca
    move-result-object v49

    #@2cb
    .line 281
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@2cd
    move-object/from16 v1, v49

    #@2cf
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->finishActivityAffinity(Landroid/os/IBinder;)Z

    #@2d2
    move-result v174

    #@2d3
    .line 282
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d6
    .line 283
    if-eqz v174, :cond_2e1

    #@2d8
    const/4 v5, 0x1

    #@2d9
    :goto_2d9
    move-object/from16 v0, p3

    #@2db
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@2de
    .line 284
    const/4 v5, 0x1

    #@2df
    goto/16 :goto_7

    #@2e1
    .line 283
    :cond_2e1
    const/4 v5, 0x0

    #@2e2
    goto :goto_2d9

    #@2e3
    .line 288
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:Z
    :pswitch_2e3
    const-string v5, "android.app.IActivityManager"

    #@2e5
    move-object/from16 v0, p2

    #@2e7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2ea
    .line 289
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2ed
    move-result-object v49

    #@2ee
    .line 290
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@2f0
    move-object/from16 v1, v49

    #@2f2
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->willActivityBeVisible(Landroid/os/IBinder;)Z

    #@2f5
    move-result v174

    #@2f6
    .line 291
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f9
    .line 292
    if-eqz v174, :cond_304

    #@2fb
    const/4 v5, 0x1

    #@2fc
    :goto_2fc
    move-object/from16 v0, p3

    #@2fe
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@301
    .line 293
    const/4 v5, 0x1

    #@302
    goto/16 :goto_7

    #@304
    .line 292
    :cond_304
    const/4 v5, 0x0

    #@305
    goto :goto_2fc

    #@306
    .line 298
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:Z
    :pswitch_306
    const-string v5, "android.app.IActivityManager"

    #@308
    move-object/from16 v0, p2

    #@30a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30d
    .line 299
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@310
    move-result-object v114

    #@311
    .line 300
    .restart local v114       #b:Landroid/os/IBinder;
    if-eqz v114, :cond_355

    #@313
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@316
    move-result-object v6

    #@317
    .line 302
    .restart local v6       #app:Landroid/app/IApplicationThread;
    :goto_317
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31a
    move-result-object v19

    #@31b
    .line 303
    .local v19, packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@31e
    move-result-object v114

    #@31f
    .line 304
    if-eqz v114, :cond_357

    #@321
    invoke-static/range {v114 .. v114}, Landroid/content/IIntentReceiver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;

    #@324
    move-result-object v20

    #@325
    .line 306
    .local v20, rec:Landroid/content/IIntentReceiver;
    :goto_325
    sget-object v5, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@327
    move-object/from16 v0, p2

    #@329
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@32c
    move-result-object v21

    #@32d
    check-cast v21, Landroid/content/IntentFilter;

    #@32f
    .line 307
    .local v21, filter:Landroid/content/IntentFilter;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@332
    move-result-object v22

    #@333
    .line 308
    .local v22, perm:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@336
    move-result v16

    #@337
    .restart local v16       #userId:I
    move-object/from16 v17, p0

    #@339
    move-object/from16 v18, v6

    #@33b
    move/from16 v23, v16

    #@33d
    .line 309
    invoke-virtual/range {v17 .. v23}, Landroid/app/ActivityManagerNative;->registerReceiver(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/IIntentReceiver;Landroid/content/IntentFilter;Ljava/lang/String;I)Landroid/content/Intent;

    #@340
    move-result-object v7

    #@341
    .line 310
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@344
    .line 311
    if-eqz v7, :cond_35a

    #@346
    .line 312
    const/4 v5, 0x1

    #@347
    move-object/from16 v0, p3

    #@349
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@34c
    .line 313
    const/4 v5, 0x0

    #@34d
    move-object/from16 v0, p3

    #@34f
    invoke-virtual {v7, v0, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@352
    .line 317
    :goto_352
    const/4 v5, 0x1

    #@353
    goto/16 :goto_7

    #@355
    .line 300
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    .end local v20           #rec:Landroid/content/IIntentReceiver;
    .end local v21           #filter:Landroid/content/IntentFilter;
    .end local v22           #perm:Ljava/lang/String;
    :cond_355
    const/4 v6, 0x0

    #@356
    goto :goto_317

    #@357
    .line 304
    .restart local v6       #app:Landroid/app/IApplicationThread;
    .restart local v19       #packageName:Ljava/lang/String;
    :cond_357
    const/16 v20, 0x0

    #@359
    goto :goto_325

    #@35a
    .line 315
    .restart local v7       #intent:Landroid/content/Intent;
    .restart local v16       #userId:I
    .restart local v20       #rec:Landroid/content/IIntentReceiver;
    .restart local v21       #filter:Landroid/content/IntentFilter;
    .restart local v22       #perm:Ljava/lang/String;
    :cond_35a
    const/4 v5, 0x0

    #@35b
    move-object/from16 v0, p3

    #@35d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@360
    goto :goto_352

    #@361
    .line 322
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    .end local v20           #rec:Landroid/content/IIntentReceiver;
    .end local v21           #filter:Landroid/content/IntentFilter;
    .end local v22           #perm:Ljava/lang/String;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_361
    const-string v5, "android.app.IActivityManager"

    #@363
    move-object/from16 v0, p2

    #@365
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@368
    .line 323
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@36b
    move-result-object v114

    #@36c
    .line 324
    .restart local v114       #b:Landroid/os/IBinder;
    if-nez v114, :cond_371

    #@36e
    .line 325
    const/4 v5, 0x1

    #@36f
    goto/16 :goto_7

    #@371
    .line 327
    :cond_371
    invoke-static/range {v114 .. v114}, Landroid/content/IIntentReceiver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;

    #@374
    move-result-object v20

    #@375
    .line 328
    .restart local v20       #rec:Landroid/content/IIntentReceiver;
    move-object/from16 v0, p0

    #@377
    move-object/from16 v1, v20

    #@379
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unregisterReceiver(Landroid/content/IIntentReceiver;)V

    #@37c
    .line 329
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@37f
    .line 330
    const/4 v5, 0x1

    #@380
    goto/16 :goto_7

    #@382
    .line 335
    .end local v20           #rec:Landroid/content/IIntentReceiver;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_382
    const-string v5, "android.app.IActivityManager"

    #@384
    move-object/from16 v0, p2

    #@386
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@389
    .line 336
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@38c
    move-result-object v114

    #@38d
    .line 337
    .restart local v114       #b:Landroid/os/IBinder;
    if-eqz v114, :cond_3ee

    #@38f
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@392
    move-result-object v6

    #@393
    .line 339
    .restart local v6       #app:Landroid/app/IApplicationThread;
    :goto_393
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@395
    move-object/from16 v0, p2

    #@397
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@39a
    move-result-object v7

    #@39b
    check-cast v7, Landroid/content/Intent;

    #@39d
    .line 340
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3a0
    move-result-object v8

    #@3a1
    .line 341
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3a4
    move-result-object v114

    #@3a5
    .line 342
    if-eqz v114, :cond_3f0

    #@3a7
    invoke-static/range {v114 .. v114}, Landroid/content/IIntentReceiver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentReceiver;

    #@3aa
    move-result-object v9

    #@3ab
    .line 344
    .local v9, resultTo:Landroid/content/IIntentReceiver;
    :goto_3ab
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ae
    move-result v41

    #@3af
    .line 345
    .restart local v41       #resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b2
    move-result-object v42

    #@3b3
    .line 346
    .local v42, resultData:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@3b6
    move-result-object v43

    #@3b7
    .line 347
    .local v43, resultExtras:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ba
    move-result-object v22

    #@3bb
    .line 348
    .restart local v22       #perm:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3be
    move-result v5

    #@3bf
    if-eqz v5, :cond_3f2

    #@3c1
    const/16 v45, 0x1

    #@3c3
    .line 349
    .local v45, serialized:Z
    :goto_3c3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3c6
    move-result v5

    #@3c7
    if-eqz v5, :cond_3f5

    #@3c9
    const/16 v46, 0x1

    #@3cb
    .line 350
    .local v46, sticky:Z
    :goto_3cb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3ce
    move-result v16

    #@3cf
    .restart local v16       #userId:I
    move-object/from16 v36, p0

    #@3d1
    move-object/from16 v37, v6

    #@3d3
    move-object/from16 v38, v7

    #@3d5
    move-object/from16 v39, v8

    #@3d7
    move-object/from16 v40, v9

    #@3d9
    move-object/from16 v44, v22

    #@3db
    move/from16 v47, v16

    #@3dd
    .line 351
    invoke-virtual/range {v36 .. v47}, Landroid/app/ActivityManagerNative;->broadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;ZZI)I

    #@3e0
    move-result v174

    #@3e1
    .line 354
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e4
    .line 355
    move-object/from16 v0, p3

    #@3e6
    move/from16 v1, v174

    #@3e8
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3eb
    .line 356
    const/4 v5, 0x1

    #@3ec
    goto/16 :goto_7

    #@3ee
    .line 337
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/content/IIntentReceiver;
    .end local v16           #userId:I
    .end local v22           #perm:Ljava/lang/String;
    .end local v41           #resultCode:I
    .end local v42           #resultData:Ljava/lang/String;
    .end local v43           #resultExtras:Landroid/os/Bundle;
    .end local v45           #serialized:Z
    .end local v46           #sticky:Z
    .end local v174           #res:I
    :cond_3ee
    const/4 v6, 0x0

    #@3ef
    goto :goto_393

    #@3f0
    .line 342
    .restart local v6       #app:Landroid/app/IApplicationThread;
    .restart local v7       #intent:Landroid/content/Intent;
    .restart local v8       #resolvedType:Ljava/lang/String;
    :cond_3f0
    const/4 v9, 0x0

    #@3f1
    goto :goto_3ab

    #@3f2
    .line 348
    .restart local v9       #resultTo:Landroid/content/IIntentReceiver;
    .restart local v22       #perm:Ljava/lang/String;
    .restart local v41       #resultCode:I
    .restart local v42       #resultData:Ljava/lang/String;
    .restart local v43       #resultExtras:Landroid/os/Bundle;
    :cond_3f2
    const/16 v45, 0x0

    #@3f4
    goto :goto_3c3

    #@3f5
    .line 349
    .restart local v45       #serialized:Z
    :cond_3f5
    const/16 v46, 0x0

    #@3f7
    goto :goto_3cb

    #@3f8
    .line 361
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v9           #resultTo:Landroid/content/IIntentReceiver;
    .end local v22           #perm:Ljava/lang/String;
    .end local v41           #resultCode:I
    .end local v42           #resultData:Ljava/lang/String;
    .end local v43           #resultExtras:Landroid/os/Bundle;
    .end local v45           #serialized:Z
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_3f8
    const-string v5, "android.app.IActivityManager"

    #@3fa
    move-object/from16 v0, p2

    #@3fc
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ff
    .line 362
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@402
    move-result-object v114

    #@403
    .line 363
    .restart local v114       #b:Landroid/os/IBinder;
    if-eqz v114, :cond_424

    #@405
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@408
    move-result-object v6

    #@409
    .line 364
    .restart local v6       #app:Landroid/app/IApplicationThread;
    :goto_409
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@40b
    move-object/from16 v0, p2

    #@40d
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@410
    move-result-object v7

    #@411
    check-cast v7, Landroid/content/Intent;

    #@413
    .line 365
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@416
    move-result v16

    #@417
    .line 366
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@419
    move/from16 v1, v16

    #@41b
    invoke-virtual {v0, v6, v7, v1}, Landroid/app/ActivityManagerNative;->unbroadcastIntent(Landroid/app/IApplicationThread;Landroid/content/Intent;I)V

    #@41e
    .line 367
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@421
    .line 368
    const/4 v5, 0x1

    #@422
    goto/16 :goto_7

    #@424
    .line 363
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v16           #userId:I
    :cond_424
    const/4 v6, 0x0

    #@425
    goto :goto_409

    #@426
    .line 372
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_426
    const-string v5, "android.app.IActivityManager"

    #@428
    move-object/from16 v0, p2

    #@42a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42d
    .line 373
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@430
    move-result-object v40

    #@431
    .line 374
    .local v40, who:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@434
    move-result v41

    #@435
    .line 375
    .restart local v41       #resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@438
    move-result-object v42

    #@439
    .line 376
    .restart local v42       #resultData:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@43c
    move-result-object v43

    #@43d
    .line 377
    .restart local v43       #resultExtras:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@440
    move-result v5

    #@441
    if-eqz v5, :cond_452

    #@443
    const/16 v44, 0x1

    #@445
    .line 378
    .local v44, resultAbort:Z
    :goto_445
    if-eqz v40, :cond_44c

    #@447
    move-object/from16 v39, p0

    #@449
    .line 379
    invoke-virtual/range {v39 .. v44}, Landroid/app/ActivityManagerNative;->finishReceiver(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;Z)V

    #@44c
    .line 381
    :cond_44c
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@44f
    .line 382
    const/4 v5, 0x1

    #@450
    goto/16 :goto_7

    #@452
    .line 377
    .end local v44           #resultAbort:Z
    :cond_452
    const/16 v44, 0x0

    #@454
    goto :goto_445

    #@455
    .line 386
    .end local v40           #who:Landroid/os/IBinder;
    .end local v41           #resultCode:I
    .end local v42           #resultData:Ljava/lang/String;
    .end local v43           #resultExtras:Landroid/os/Bundle;
    :pswitch_455
    const-string v5, "android.app.IActivityManager"

    #@457
    move-object/from16 v0, p2

    #@459
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45c
    .line 387
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@45f
    move-result-object v5

    #@460
    invoke-static {v5}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@463
    move-result-object v6

    #@464
    .line 389
    .restart local v6       #app:Landroid/app/IApplicationThread;
    if-eqz v6, :cond_46b

    #@466
    .line 390
    move-object/from16 v0, p0

    #@468
    invoke-virtual {v0, v6}, Landroid/app/ActivityManagerNative;->attachApplication(Landroid/app/IApplicationThread;)V

    #@46b
    .line 392
    :cond_46b
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@46e
    .line 393
    const/4 v5, 0x1

    #@46f
    goto/16 :goto_7

    #@471
    .line 397
    .end local v6           #app:Landroid/app/IApplicationThread;
    :pswitch_471
    const-string v5, "android.app.IActivityManager"

    #@473
    move-object/from16 v0, p2

    #@475
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@478
    .line 398
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@47b
    move-result-object v49

    #@47c
    .line 399
    .restart local v49       #token:Landroid/os/IBinder;
    const/16 v25, 0x0

    #@47e
    .line 400
    .restart local v25       #config:Landroid/content/res/Configuration;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@481
    move-result v5

    #@482
    if-eqz v5, :cond_48e

    #@484
    .line 401
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@486
    move-object/from16 v0, p2

    #@488
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@48b
    move-result-object v25

    #@48c
    .end local v25           #config:Landroid/content/res/Configuration;
    check-cast v25, Landroid/content/res/Configuration;

    #@48e
    .line 403
    .restart local v25       #config:Landroid/content/res/Configuration;
    :cond_48e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@491
    move-result v5

    #@492
    if-eqz v5, :cond_4a9

    #@494
    const/16 v185, 0x1

    #@496
    .line 404
    .local v185, stopProfiling:Z
    :goto_496
    if-eqz v49, :cond_4a3

    #@498
    .line 405
    move-object/from16 v0, p0

    #@49a
    move-object/from16 v1, v49

    #@49c
    move-object/from16 v2, v25

    #@49e
    move/from16 v3, v185

    #@4a0
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->activityIdle(Landroid/os/IBinder;Landroid/content/res/Configuration;Z)V

    #@4a3
    .line 407
    :cond_4a3
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a6
    .line 408
    const/4 v5, 0x1

    #@4a7
    goto/16 :goto_7

    #@4a9
    .line 403
    .end local v185           #stopProfiling:Z
    :cond_4a9
    const/16 v185, 0x0

    #@4ab
    goto :goto_496

    #@4ac
    .line 412
    .end local v25           #config:Landroid/content/res/Configuration;
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_4ac
    const-string v5, "android.app.IActivityManager"

    #@4ae
    move-object/from16 v0, p2

    #@4b0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b3
    .line 413
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4b6
    move-result-object v49

    #@4b7
    .line 414
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@4b9
    move-object/from16 v1, v49

    #@4bb
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->activityResumed(Landroid/os/IBinder;)V

    #@4be
    .line 415
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c1
    .line 416
    const/4 v5, 0x1

    #@4c2
    goto/16 :goto_7

    #@4c4
    .line 420
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_4c4
    const-string v5, "android.app.IActivityManager"

    #@4c6
    move-object/from16 v0, p2

    #@4c8
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4cb
    .line 421
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4ce
    move-result-object v49

    #@4cf
    .line 422
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@4d1
    move-object/from16 v1, v49

    #@4d3
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->activityPaused(Landroid/os/IBinder;)V

    #@4d6
    .line 423
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d9
    .line 424
    const/4 v5, 0x1

    #@4da
    goto/16 :goto_7

    #@4dc
    .line 428
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_4dc
    const-string v5, "android.app.IActivityManager"

    #@4de
    move-object/from16 v0, p2

    #@4e0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e3
    .line 429
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4e6
    move-result-object v49

    #@4e7
    .line 430
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@4ea
    move-result-object v147

    #@4eb
    .line 431
    .local v147, map:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4ee
    move-result v5

    #@4ef
    if-eqz v5, :cond_51a

    #@4f1
    sget-object v5, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4f3
    move-object/from16 v0, p2

    #@4f5
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4f8
    move-result-object v5

    #@4f9
    check-cast v5, Landroid/graphics/Bitmap;

    #@4fb
    move-object/from16 v192, v5

    #@4fd
    .line 433
    .local v192, thumbnail:Landroid/graphics/Bitmap;
    :goto_4fd
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@4ff
    move-object/from16 v0, p2

    #@501
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@504
    move-result-object v127

    #@505
    check-cast v127, Ljava/lang/CharSequence;

    #@507
    .line 434
    .local v127, description:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@509
    move-object/from16 v1, v49

    #@50b
    move-object/from16 v2, v147

    #@50d
    move-object/from16 v3, v192

    #@50f
    move-object/from16 v4, v127

    #@511
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->activityStopped(Landroid/os/IBinder;Landroid/os/Bundle;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@514
    .line 435
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@517
    .line 436
    const/4 v5, 0x1

    #@518
    goto/16 :goto_7

    #@51a
    .line 431
    .end local v127           #description:Ljava/lang/CharSequence;
    .end local v192           #thumbnail:Landroid/graphics/Bitmap;
    :cond_51a
    const/16 v192, 0x0

    #@51c
    goto :goto_4fd

    #@51d
    .line 440
    .end local v49           #token:Landroid/os/IBinder;
    .end local v147           #map:Landroid/os/Bundle;
    :pswitch_51d
    const-string v5, "android.app.IActivityManager"

    #@51f
    move-object/from16 v0, p2

    #@521
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@524
    .line 441
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@527
    move-result-object v49

    #@528
    .line 442
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@52a
    move-object/from16 v1, v49

    #@52c
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->activitySlept(Landroid/os/IBinder;)V

    #@52f
    .line 443
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@532
    .line 444
    const/4 v5, 0x1

    #@533
    goto/16 :goto_7

    #@535
    .line 448
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_535
    const-string v5, "android.app.IActivityManager"

    #@537
    move-object/from16 v0, p2

    #@539
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53c
    .line 449
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@53f
    move-result-object v49

    #@540
    .line 450
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@542
    move-object/from16 v1, v49

    #@544
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->activityDestroyed(Landroid/os/IBinder;)V

    #@547
    .line 451
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@54a
    .line 452
    const/4 v5, 0x1

    #@54b
    goto/16 :goto_7

    #@54d
    .line 456
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_54d
    const-string v5, "android.app.IActivityManager"

    #@54f
    move-object/from16 v0, p2

    #@551
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@554
    .line 457
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@557
    move-result-object v49

    #@558
    .line 458
    .restart local v49       #token:Landroid/os/IBinder;
    if-eqz v49, :cond_56f

    #@55a
    move-object/from16 v0, p0

    #@55c
    move-object/from16 v1, v49

    #@55e
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getCallingPackage(Landroid/os/IBinder;)Ljava/lang/String;

    #@561
    move-result-object v174

    #@562
    .line 459
    .local v174, res:Ljava/lang/String;
    :goto_562
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@565
    .line 460
    move-object/from16 v0, p3

    #@567
    move-object/from16 v1, v174

    #@569
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@56c
    .line 461
    const/4 v5, 0x1

    #@56d
    goto/16 :goto_7

    #@56f
    .line 458
    .end local v174           #res:Ljava/lang/String;
    :cond_56f
    const/16 v174, 0x0

    #@571
    goto :goto_562

    #@572
    .line 465
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_572
    const-string v5, "android.app.IActivityManager"

    #@574
    move-object/from16 v0, p2

    #@576
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@579
    .line 466
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@57c
    move-result-object v49

    #@57d
    .line 467
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@57f
    move-object/from16 v1, v49

    #@581
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getCallingActivity(Landroid/os/IBinder;)Landroid/content/ComponentName;

    #@584
    move-result-object v124

    #@585
    .line 468
    .local v124, cn:Landroid/content/ComponentName;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@588
    .line 469
    move-object/from16 v0, v124

    #@58a
    move-object/from16 v1, p3

    #@58c
    invoke-static {v0, v1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@58f
    .line 470
    const/4 v5, 0x1

    #@590
    goto/16 :goto_7

    #@592
    .line 474
    .end local v49           #token:Landroid/os/IBinder;
    .end local v124           #cn:Landroid/content/ComponentName;
    :pswitch_592
    const-string v5, "android.app.IActivityManager"

    #@594
    move-object/from16 v0, p2

    #@596
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@599
    .line 475
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@59c
    move-result v149

    #@59d
    .line 476
    .local v149, maxNum:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5a0
    move-result v59

    #@5a1
    .line 477
    .local v59, fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5a4
    move-result-object v171

    #@5a5
    .line 478
    .local v171, receiverBinder:Landroid/os/IBinder;
    if-eqz v171, :cond_5e4

    #@5a7
    invoke-static/range {v171 .. v171}, Landroid/app/IThumbnailReceiver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IThumbnailReceiver;

    #@5aa
    move-result-object v170

    #@5ab
    .line 481
    .local v170, receiver:Landroid/app/IThumbnailReceiver;
    :goto_5ab
    move-object/from16 v0, p0

    #@5ad
    move/from16 v1, v149

    #@5af
    move/from16 v2, v59

    #@5b1
    move-object/from16 v3, v170

    #@5b3
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->getTasks(IILandroid/app/IThumbnailReceiver;)Ljava/util/List;

    #@5b6
    move-result-object v142

    #@5b7
    .line 482
    .local v142, list:Ljava/util/List;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5ba
    .line 483
    if-eqz v142, :cond_5e7

    #@5bc
    invoke-interface/range {v142 .. v142}, Ljava/util/List;->size()I

    #@5bf
    move-result v106

    #@5c0
    .line 484
    .local v106, N:I
    :goto_5c0
    move-object/from16 v0, p3

    #@5c2
    move/from16 v1, v106

    #@5c4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5c7
    .line 486
    const/16 v133, 0x0

    #@5c9
    .local v133, i:I
    :goto_5c9
    move/from16 v0, v133

    #@5cb
    move/from16 v1, v106

    #@5cd
    if-ge v0, v1, :cond_5ea

    #@5cf
    .line 487
    move-object/from16 v0, v142

    #@5d1
    move/from16 v1, v133

    #@5d3
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5d6
    move-result-object v136

    #@5d7
    check-cast v136, Landroid/app/ActivityManager$RunningTaskInfo;

    #@5d9
    .line 489
    .local v136, info:Landroid/app/ActivityManager$RunningTaskInfo;
    const/4 v5, 0x0

    #@5da
    move-object/from16 v0, v136

    #@5dc
    move-object/from16 v1, p3

    #@5de
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$RunningTaskInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5e1
    .line 486
    add-int/lit8 v133, v133, 0x1

    #@5e3
    goto :goto_5c9

    #@5e4
    .line 478
    .end local v106           #N:I
    .end local v133           #i:I
    .end local v136           #info:Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v142           #list:Ljava/util/List;
    .end local v170           #receiver:Landroid/app/IThumbnailReceiver;
    :cond_5e4
    const/16 v170, 0x0

    #@5e6
    goto :goto_5ab

    #@5e7
    .line 483
    .restart local v142       #list:Ljava/util/List;
    .restart local v170       #receiver:Landroid/app/IThumbnailReceiver;
    :cond_5e7
    const/16 v106, -0x1

    #@5e9
    goto :goto_5c0

    #@5ea
    .line 491
    .restart local v106       #N:I
    .restart local v133       #i:I
    :cond_5ea
    const/4 v5, 0x1

    #@5eb
    goto/16 :goto_7

    #@5ed
    .line 495
    .end local v59           #fl:I
    .end local v106           #N:I
    .end local v133           #i:I
    .end local v142           #list:Ljava/util/List;
    .end local v149           #maxNum:I
    .end local v170           #receiver:Landroid/app/IThumbnailReceiver;
    .end local v171           #receiverBinder:Landroid/os/IBinder;
    :pswitch_5ed
    const-string v5, "android.app.IActivityManager"

    #@5ef
    move-object/from16 v0, p2

    #@5f1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f4
    .line 496
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5f7
    move-result v149

    #@5f8
    .line 497
    .restart local v149       #maxNum:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5fb
    move-result v59

    #@5fc
    .line 498
    .restart local v59       #fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5ff
    move-result v16

    #@600
    .line 499
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@602
    move/from16 v1, v149

    #@604
    move/from16 v2, v59

    #@606
    move/from16 v3, v16

    #@608
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->getRecentTasks(III)Ljava/util/List;

    #@60b
    move-result-object v144

    #@60c
    .line 501
    .local v144, list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@60f
    .line 502
    move-object/from16 v0, p3

    #@611
    move-object/from16 v1, v144

    #@613
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@616
    .line 503
    const/4 v5, 0x1

    #@617
    goto/16 :goto_7

    #@619
    .line 507
    .end local v16           #userId:I
    .end local v59           #fl:I
    .end local v144           #list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    .end local v149           #maxNum:I
    :pswitch_619
    const-string v5, "android.app.IActivityManager"

    #@61b
    move-object/from16 v0, p2

    #@61d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@620
    .line 508
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@623
    move-result v50

    #@624
    .line 509
    .local v50, id:I
    move-object/from16 v0, p0

    #@626
    move/from16 v1, v50

    #@628
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getTaskThumbnails(I)Landroid/app/ActivityManager$TaskThumbnails;

    #@62b
    move-result-object v118

    #@62c
    .line 510
    .local v118, bm:Landroid/app/ActivityManager$TaskThumbnails;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@62f
    .line 511
    if-eqz v118, :cond_642

    #@631
    .line 512
    const/4 v5, 0x1

    #@632
    move-object/from16 v0, p3

    #@634
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@637
    .line 513
    const/4 v5, 0x0

    #@638
    move-object/from16 v0, v118

    #@63a
    move-object/from16 v1, p3

    #@63c
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$TaskThumbnails;->writeToParcel(Landroid/os/Parcel;I)V

    #@63f
    .line 517
    :goto_63f
    const/4 v5, 0x1

    #@640
    goto/16 :goto_7

    #@642
    .line 515
    :cond_642
    const/4 v5, 0x0

    #@643
    move-object/from16 v0, p3

    #@645
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@648
    goto :goto_63f

    #@649
    .line 521
    .end local v50           #id:I
    .end local v118           #bm:Landroid/app/ActivityManager$TaskThumbnails;
    :pswitch_649
    const-string v5, "android.app.IActivityManager"

    #@64b
    move-object/from16 v0, p2

    #@64d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@650
    .line 522
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@653
    move-result v50

    #@654
    .line 523
    .restart local v50       #id:I
    move-object/from16 v0, p0

    #@656
    move/from16 v1, v50

    #@658
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getTaskTopThumbnail(I)Landroid/graphics/Bitmap;

    #@65b
    move-result-object v118

    #@65c
    .line 524
    .local v118, bm:Landroid/graphics/Bitmap;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@65f
    .line 525
    if-eqz v118, :cond_672

    #@661
    .line 526
    const/4 v5, 0x1

    #@662
    move-object/from16 v0, p3

    #@664
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@667
    .line 527
    const/4 v5, 0x0

    #@668
    move-object/from16 v0, v118

    #@66a
    move-object/from16 v1, p3

    #@66c
    invoke-virtual {v0, v1, v5}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@66f
    .line 531
    :goto_66f
    const/4 v5, 0x1

    #@670
    goto/16 :goto_7

    #@672
    .line 529
    :cond_672
    const/4 v5, 0x0

    #@673
    move-object/from16 v0, p3

    #@675
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@678
    goto :goto_66f

    #@679
    .line 535
    .end local v50           #id:I
    .end local v118           #bm:Landroid/graphics/Bitmap;
    :pswitch_679
    const-string v5, "android.app.IActivityManager"

    #@67b
    move-object/from16 v0, p2

    #@67d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@680
    .line 536
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@683
    move-result v149

    #@684
    .line 537
    .restart local v149       #maxNum:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@687
    move-result v59

    #@688
    .line 538
    .restart local v59       #fl:I
    move-object/from16 v0, p0

    #@68a
    move/from16 v1, v149

    #@68c
    move/from16 v2, v59

    #@68e
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->getServices(II)Ljava/util/List;

    #@691
    move-result-object v142

    #@692
    .line 539
    .restart local v142       #list:Ljava/util/List;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@695
    .line 540
    if-eqz v142, :cond_6bf

    #@697
    invoke-interface/range {v142 .. v142}, Ljava/util/List;->size()I

    #@69a
    move-result v106

    #@69b
    .line 541
    .restart local v106       #N:I
    :goto_69b
    move-object/from16 v0, p3

    #@69d
    move/from16 v1, v106

    #@69f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6a2
    .line 543
    const/16 v133, 0x0

    #@6a4
    .restart local v133       #i:I
    :goto_6a4
    move/from16 v0, v133

    #@6a6
    move/from16 v1, v106

    #@6a8
    if-ge v0, v1, :cond_6c2

    #@6aa
    .line 544
    move-object/from16 v0, v142

    #@6ac
    move/from16 v1, v133

    #@6ae
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6b1
    move-result-object v136

    #@6b2
    check-cast v136, Landroid/app/ActivityManager$RunningServiceInfo;

    #@6b4
    .line 546
    .local v136, info:Landroid/app/ActivityManager$RunningServiceInfo;
    const/4 v5, 0x0

    #@6b5
    move-object/from16 v0, v136

    #@6b7
    move-object/from16 v1, p3

    #@6b9
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$RunningServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@6bc
    .line 543
    add-int/lit8 v133, v133, 0x1

    #@6be
    goto :goto_6a4

    #@6bf
    .line 540
    .end local v106           #N:I
    .end local v133           #i:I
    .end local v136           #info:Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_6bf
    const/16 v106, -0x1

    #@6c1
    goto :goto_69b

    #@6c2
    .line 548
    .restart local v106       #N:I
    .restart local v133       #i:I
    :cond_6c2
    const/4 v5, 0x1

    #@6c3
    goto/16 :goto_7

    #@6c5
    .line 552
    .end local v59           #fl:I
    .end local v106           #N:I
    .end local v133           #i:I
    .end local v142           #list:Ljava/util/List;
    .end local v149           #maxNum:I
    :pswitch_6c5
    const-string v5, "android.app.IActivityManager"

    #@6c7
    move-object/from16 v0, p2

    #@6c9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6cc
    .line 553
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getProcessesInErrorState()Ljava/util/List;

    #@6cf
    move-result-object v143

    #@6d0
    .line 554
    .local v143, list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$ProcessErrorStateInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6d3
    .line 555
    move-object/from16 v0, p3

    #@6d5
    move-object/from16 v1, v143

    #@6d7
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@6da
    .line 556
    const/4 v5, 0x1

    #@6db
    goto/16 :goto_7

    #@6dd
    .line 560
    .end local v143           #list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$ProcessErrorStateInfo;>;"
    :pswitch_6dd
    const-string v5, "android.app.IActivityManager"

    #@6df
    move-object/from16 v0, p2

    #@6e1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6e4
    .line 561
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getRunningAppProcesses()Ljava/util/List;

    #@6e7
    move-result-object v145

    #@6e8
    .line 562
    .local v145, list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6eb
    .line 563
    move-object/from16 v0, p3

    #@6ed
    move-object/from16 v1, v145

    #@6ef
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@6f2
    .line 564
    const/4 v5, 0x1

    #@6f3
    goto/16 :goto_7

    #@6f5
    .line 568
    .end local v145           #list:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :pswitch_6f5
    const-string v5, "android.app.IActivityManager"

    #@6f7
    move-object/from16 v0, p2

    #@6f9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6fc
    .line 569
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getRunningExternalApplications()Ljava/util/List;

    #@6ff
    move-result-object v146

    #@700
    .line 570
    .local v146, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@703
    .line 571
    move-object/from16 v0, p3

    #@705
    move-object/from16 v1, v146

    #@707
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@70a
    .line 572
    const/4 v5, 0x1

    #@70b
    goto/16 :goto_7

    #@70d
    .line 576
    .end local v146           #list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    :pswitch_70d
    const-string v5, "android.app.IActivityManager"

    #@70f
    move-object/from16 v0, p2

    #@711
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@714
    .line 577
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@717
    move-result v190

    #@718
    .line 578
    .local v190, task:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@71b
    move-result v59

    #@71c
    .line 579
    .restart local v59       #fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@71f
    move-result v5

    #@720
    if-eqz v5, :cond_73c

    #@722
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@724
    move-object/from16 v0, p2

    #@726
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@729
    move-result-object v5

    #@72a
    check-cast v5, Landroid/os/Bundle;

    #@72c
    move-object v15, v5

    #@72d
    .line 581
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_72d
    move-object/from16 v0, p0

    #@72f
    move/from16 v1, v190

    #@731
    move/from16 v2, v59

    #@733
    invoke-virtual {v0, v1, v2, v15}, Landroid/app/ActivityManagerNative;->moveTaskToFront(IILandroid/os/Bundle;)V

    #@736
    .line 582
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@739
    .line 583
    const/4 v5, 0x1

    #@73a
    goto/16 :goto_7

    #@73c
    .line 579
    .end local v15           #options:Landroid/os/Bundle;
    :cond_73c
    const/4 v15, 0x0

    #@73d
    goto :goto_72d

    #@73e
    .line 587
    .end local v59           #fl:I
    .end local v190           #task:I
    :pswitch_73e
    const-string v5, "android.app.IActivityManager"

    #@740
    move-object/from16 v0, p2

    #@742
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@745
    .line 588
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@748
    move-result v190

    #@749
    .line 589
    .restart local v190       #task:I
    move-object/from16 v0, p0

    #@74b
    move/from16 v1, v190

    #@74d
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->moveTaskToBack(I)V

    #@750
    .line 590
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@753
    .line 591
    const/4 v5, 0x1

    #@754
    goto/16 :goto_7

    #@756
    .line 595
    .end local v190           #task:I
    :pswitch_756
    const-string v5, "android.app.IActivityManager"

    #@758
    move-object/from16 v0, p2

    #@75a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@75d
    .line 596
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@760
    move-result-object v49

    #@761
    .line 597
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@764
    move-result v5

    #@765
    if-eqz v5, :cond_781

    #@767
    const/16 v154, 0x1

    #@769
    .line 598
    .local v154, nonRoot:Z
    :goto_769
    move-object/from16 v0, p0

    #@76b
    move-object/from16 v1, v49

    #@76d
    move/from16 v2, v154

    #@76f
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->moveActivityTaskToBack(Landroid/os/IBinder;Z)Z

    #@772
    move-result v174

    #@773
    .line 599
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@776
    .line 600
    if-eqz v174, :cond_784

    #@778
    const/4 v5, 0x1

    #@779
    :goto_779
    move-object/from16 v0, p3

    #@77b
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@77e
    .line 601
    const/4 v5, 0x1

    #@77f
    goto/16 :goto_7

    #@781
    .line 597
    .end local v154           #nonRoot:Z
    .end local v174           #res:Z
    :cond_781
    const/16 v154, 0x0

    #@783
    goto :goto_769

    #@784
    .line 600
    .restart local v154       #nonRoot:Z
    .restart local v174       #res:Z
    :cond_784
    const/4 v5, 0x0

    #@785
    goto :goto_779

    #@786
    .line 605
    .end local v49           #token:Landroid/os/IBinder;
    .end local v154           #nonRoot:Z
    .end local v174           #res:Z
    :pswitch_786
    const-string v5, "android.app.IActivityManager"

    #@788
    move-object/from16 v0, p2

    #@78a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@78d
    .line 606
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@790
    move-result v190

    #@791
    .line 607
    .restart local v190       #task:I
    move-object/from16 v0, p0

    #@793
    move/from16 v1, v190

    #@795
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->moveTaskBackwards(I)V

    #@798
    .line 608
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@79b
    .line 609
    const/4 v5, 0x1

    #@79c
    goto/16 :goto_7

    #@79e
    .line 613
    .end local v190           #task:I
    :pswitch_79e
    const-string v5, "android.app.IActivityManager"

    #@7a0
    move-object/from16 v0, p2

    #@7a2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a5
    .line 614
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7a8
    move-result-object v49

    #@7a9
    .line 615
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7ac
    move-result v5

    #@7ad
    if-eqz v5, :cond_7ca

    #@7af
    const/16 v156, 0x1

    #@7b1
    .line 616
    .local v156, onlyRoot:Z
    :goto_7b1
    if-eqz v49, :cond_7cd

    #@7b3
    move-object/from16 v0, p0

    #@7b5
    move-object/from16 v1, v49

    #@7b7
    move/from16 v2, v156

    #@7b9
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->getTaskForActivity(Landroid/os/IBinder;Z)I

    #@7bc
    move-result v174

    #@7bd
    .line 618
    .local v174, res:I
    :goto_7bd
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c0
    .line 619
    move-object/from16 v0, p3

    #@7c2
    move/from16 v1, v174

    #@7c4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@7c7
    .line 620
    const/4 v5, 0x1

    #@7c8
    goto/16 :goto_7

    #@7ca
    .line 615
    .end local v156           #onlyRoot:Z
    .end local v174           #res:I
    :cond_7ca
    const/16 v156, 0x0

    #@7cc
    goto :goto_7b1

    #@7cd
    .line 616
    .restart local v156       #onlyRoot:Z
    :cond_7cd
    const/16 v174, -0x1

    #@7cf
    goto :goto_7bd

    #@7d0
    .line 624
    .end local v49           #token:Landroid/os/IBinder;
    .end local v156           #onlyRoot:Z
    :pswitch_7d0
    const-string v5, "android.app.IActivityManager"

    #@7d2
    move-object/from16 v0, p2

    #@7d4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7d7
    .line 625
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@7da
    move-result-object v49

    #@7db
    .line 626
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7de
    move-result v5

    #@7df
    if-eqz v5, :cond_808

    #@7e1
    sget-object v5, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7e3
    move-object/from16 v0, p2

    #@7e5
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7e8
    move-result-object v5

    #@7e9
    check-cast v5, Landroid/graphics/Bitmap;

    #@7eb
    move-object/from16 v192, v5

    #@7ed
    .line 628
    .restart local v192       #thumbnail:Landroid/graphics/Bitmap;
    :goto_7ed
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@7ef
    move-object/from16 v0, p2

    #@7f1
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7f4
    move-result-object v127

    #@7f5
    check-cast v127, Ljava/lang/CharSequence;

    #@7f7
    .line 629
    .restart local v127       #description:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@7f9
    move-object/from16 v1, v49

    #@7fb
    move-object/from16 v2, v192

    #@7fd
    move-object/from16 v3, v127

    #@7ff
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->reportThumbnail(Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;)V

    #@802
    .line 630
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@805
    .line 631
    const/4 v5, 0x1

    #@806
    goto/16 :goto_7

    #@808
    .line 626
    .end local v127           #description:Ljava/lang/CharSequence;
    .end local v192           #thumbnail:Landroid/graphics/Bitmap;
    :cond_808
    const/16 v192, 0x0

    #@80a
    goto :goto_7ed

    #@80b
    .line 635
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_80b
    const-string v5, "android.app.IActivityManager"

    #@80d
    move-object/from16 v0, p2

    #@80f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@812
    .line 636
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@815
    move-result-object v114

    #@816
    .line 637
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@819
    move-result-object v6

    #@81a
    .line 638
    .restart local v6       #app:Landroid/app/IApplicationThread;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@81d
    move-result-object v80

    #@81e
    .line 639
    .local v80, name:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@821
    move-result v16

    #@822
    .line 640
    .restart local v16       #userId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@825
    move-result v5

    #@826
    if-eqz v5, :cond_84c

    #@828
    const/16 v183, 0x1

    #@82a
    .line 641
    .local v183, stable:Z
    :goto_82a
    move-object/from16 v0, p0

    #@82c
    move-object/from16 v1, v80

    #@82e
    move/from16 v2, v16

    #@830
    move/from16 v3, v183

    #@832
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/app/ActivityManagerNative;->getContentProvider(Landroid/app/IApplicationThread;Ljava/lang/String;IZ)Landroid/app/IActivityManager$ContentProviderHolder;

    #@835
    move-result-object v126

    #@836
    .line 642
    .local v126, cph:Landroid/app/IActivityManager$ContentProviderHolder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@839
    .line 643
    if-eqz v126, :cond_84f

    #@83b
    .line 644
    const/4 v5, 0x1

    #@83c
    move-object/from16 v0, p3

    #@83e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@841
    .line 645
    const/4 v5, 0x0

    #@842
    move-object/from16 v0, v126

    #@844
    move-object/from16 v1, p3

    #@846
    invoke-virtual {v0, v1, v5}, Landroid/app/IActivityManager$ContentProviderHolder;->writeToParcel(Landroid/os/Parcel;I)V

    #@849
    .line 649
    :goto_849
    const/4 v5, 0x1

    #@84a
    goto/16 :goto_7

    #@84c
    .line 640
    .end local v126           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    .end local v183           #stable:Z
    :cond_84c
    const/16 v183, 0x0

    #@84e
    goto :goto_82a

    #@84f
    .line 647
    .restart local v126       #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    .restart local v183       #stable:Z
    :cond_84f
    const/4 v5, 0x0

    #@850
    move-object/from16 v0, p3

    #@852
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@855
    goto :goto_849

    #@856
    .line 653
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v16           #userId:I
    .end local v80           #name:Ljava/lang/String;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v126           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    .end local v183           #stable:Z
    :pswitch_856
    const-string v5, "android.app.IActivityManager"

    #@858
    move-object/from16 v0, p2

    #@85a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85d
    .line 654
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@860
    move-result-object v80

    #@861
    .line 655
    .restart local v80       #name:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@864
    move-result v16

    #@865
    .line 656
    .restart local v16       #userId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@868
    move-result-object v49

    #@869
    .line 657
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@86b
    move-object/from16 v1, v80

    #@86d
    move/from16 v2, v16

    #@86f
    move-object/from16 v3, v49

    #@871
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->getContentProviderExternal(Ljava/lang/String;ILandroid/os/IBinder;)Landroid/app/IActivityManager$ContentProviderHolder;

    #@874
    move-result-object v126

    #@875
    .line 658
    .restart local v126       #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@878
    .line 659
    if-eqz v126, :cond_88b

    #@87a
    .line 660
    const/4 v5, 0x1

    #@87b
    move-object/from16 v0, p3

    #@87d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@880
    .line 661
    const/4 v5, 0x0

    #@881
    move-object/from16 v0, v126

    #@883
    move-object/from16 v1, p3

    #@885
    invoke-virtual {v0, v1, v5}, Landroid/app/IActivityManager$ContentProviderHolder;->writeToParcel(Landroid/os/Parcel;I)V

    #@888
    .line 665
    :goto_888
    const/4 v5, 0x1

    #@889
    goto/16 :goto_7

    #@88b
    .line 663
    :cond_88b
    const/4 v5, 0x0

    #@88c
    move-object/from16 v0, p3

    #@88e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@891
    goto :goto_888

    #@892
    .line 669
    .end local v16           #userId:I
    .end local v49           #token:Landroid/os/IBinder;
    .end local v80           #name:Ljava/lang/String;
    .end local v126           #cph:Landroid/app/IActivityManager$ContentProviderHolder;
    :pswitch_892
    const-string v5, "android.app.IActivityManager"

    #@894
    move-object/from16 v0, p2

    #@896
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@899
    .line 670
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@89c
    move-result-object v114

    #@89d
    .line 671
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@8a0
    move-result-object v6

    #@8a1
    .line 672
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/app/IActivityManager$ContentProviderHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    #@8a3
    move-object/from16 v0, p2

    #@8a5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    #@8a8
    move-result-object v166

    #@8a9
    .line 674
    .local v166, providers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/IActivityManager$ContentProviderHolder;>;"
    move-object/from16 v0, p0

    #@8ab
    move-object/from16 v1, v166

    #@8ad
    invoke-virtual {v0, v6, v1}, Landroid/app/ActivityManagerNative;->publishContentProviders(Landroid/app/IApplicationThread;Ljava/util/List;)V

    #@8b0
    .line 675
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b3
    .line 676
    const/4 v5, 0x1

    #@8b4
    goto/16 :goto_7

    #@8b6
    .line 680
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v166           #providers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/IActivityManager$ContentProviderHolder;>;"
    :pswitch_8b6
    const-string v5, "android.app.IActivityManager"

    #@8b8
    move-object/from16 v0, p2

    #@8ba
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8bd
    .line 681
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8c0
    move-result-object v114

    #@8c1
    .line 682
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8c4
    move-result v183

    #@8c5
    .line 683
    .local v183, stable:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8c8
    move-result v194

    #@8c9
    .line 684
    .local v194, unstable:I
    move-object/from16 v0, p0

    #@8cb
    move-object/from16 v1, v114

    #@8cd
    move/from16 v2, v183

    #@8cf
    move/from16 v3, v194

    #@8d1
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->refContentProvider(Landroid/os/IBinder;II)Z

    #@8d4
    move-result v174

    #@8d5
    .line 685
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d8
    .line 686
    if-eqz v174, :cond_8e3

    #@8da
    const/4 v5, 0x1

    #@8db
    :goto_8db
    move-object/from16 v0, p3

    #@8dd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@8e0
    .line 687
    const/4 v5, 0x1

    #@8e1
    goto/16 :goto_7

    #@8e3
    .line 686
    :cond_8e3
    const/4 v5, 0x0

    #@8e4
    goto :goto_8db

    #@8e5
    .line 691
    .end local v114           #b:Landroid/os/IBinder;
    .end local v174           #res:Z
    .end local v183           #stable:I
    .end local v194           #unstable:I
    :pswitch_8e5
    const-string v5, "android.app.IActivityManager"

    #@8e7
    move-object/from16 v0, p2

    #@8e9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8ec
    .line 692
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@8ef
    move-result-object v114

    #@8f0
    .line 693
    .restart local v114       #b:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@8f2
    move-object/from16 v1, v114

    #@8f4
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unstableProviderDied(Landroid/os/IBinder;)V

    #@8f7
    .line 694
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8fa
    .line 695
    const/4 v5, 0x1

    #@8fb
    goto/16 :goto_7

    #@8fd
    .line 699
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_8fd
    const-string v5, "android.app.IActivityManager"

    #@8ff
    move-object/from16 v0, p2

    #@901
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@904
    .line 700
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@907
    move-result-object v114

    #@908
    .line 701
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@90b
    move-result v5

    #@90c
    if-eqz v5, :cond_91f

    #@90e
    const/16 v183, 0x1

    #@910
    .line 702
    .local v183, stable:Z
    :goto_910
    move-object/from16 v0, p0

    #@912
    move-object/from16 v1, v114

    #@914
    move/from16 v2, v183

    #@916
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->removeContentProvider(Landroid/os/IBinder;Z)V

    #@919
    .line 703
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@91c
    .line 704
    const/4 v5, 0x1

    #@91d
    goto/16 :goto_7

    #@91f
    .line 701
    .end local v183           #stable:Z
    :cond_91f
    const/16 v183, 0x0

    #@921
    goto :goto_910

    #@922
    .line 708
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_922
    const-string v5, "android.app.IActivityManager"

    #@924
    move-object/from16 v0, p2

    #@926
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@929
    .line 709
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@92c
    move-result-object v80

    #@92d
    .line 710
    .restart local v80       #name:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@930
    move-result-object v49

    #@931
    .line 711
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@933
    move-object/from16 v1, v80

    #@935
    move-object/from16 v2, v49

    #@937
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->removeContentProviderExternal(Ljava/lang/String;Landroid/os/IBinder;)V

    #@93a
    .line 712
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@93d
    .line 713
    const/4 v5, 0x1

    #@93e
    goto/16 :goto_7

    #@940
    .line 717
    .end local v49           #token:Landroid/os/IBinder;
    .end local v80           #name:Ljava/lang/String;
    :pswitch_940
    const-string v5, "android.app.IActivityManager"

    #@942
    move-object/from16 v0, p2

    #@944
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@947
    .line 718
    sget-object v5, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@949
    move-object/from16 v0, p2

    #@94b
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@94e
    move-result-object v125

    #@94f
    check-cast v125, Landroid/content/ComponentName;

    #@951
    .line 719
    .local v125, comp:Landroid/content/ComponentName;
    move-object/from16 v0, p0

    #@953
    move-object/from16 v1, v125

    #@955
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getRunningServiceControlPanel(Landroid/content/ComponentName;)Landroid/app/PendingIntent;

    #@958
    move-result-object v160

    #@959
    .line 720
    .local v160, pi:Landroid/app/PendingIntent;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@95c
    .line 721
    move-object/from16 v0, v160

    #@95e
    move-object/from16 v1, p3

    #@960
    invoke-static {v0, v1}, Landroid/app/PendingIntent;->writePendingIntentOrNullToParcel(Landroid/app/PendingIntent;Landroid/os/Parcel;)V

    #@963
    .line 722
    const/4 v5, 0x1

    #@964
    goto/16 :goto_7

    #@966
    .line 726
    .end local v125           #comp:Landroid/content/ComponentName;
    .end local v160           #pi:Landroid/app/PendingIntent;
    :pswitch_966
    const-string v5, "android.app.IActivityManager"

    #@968
    move-object/from16 v0, p2

    #@96a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@96d
    .line 727
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@970
    move-result-object v114

    #@971
    .line 728
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@974
    move-result-object v6

    #@975
    .line 729
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@977
    move-object/from16 v0, p2

    #@979
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@97c
    move-result-object v56

    #@97d
    check-cast v56, Landroid/content/Intent;

    #@97f
    .line 730
    .local v56, service:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@982
    move-result-object v8

    #@983
    .line 731
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@986
    move-result v16

    #@987
    .line 732
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@989
    move-object/from16 v1, v56

    #@98b
    move/from16 v2, v16

    #@98d
    invoke-virtual {v0, v6, v1, v8, v2}, Landroid/app/ActivityManagerNative;->startService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/ComponentName;

    #@990
    move-result-object v124

    #@991
    .line 733
    .restart local v124       #cn:Landroid/content/ComponentName;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@994
    .line 734
    move-object/from16 v0, v124

    #@996
    move-object/from16 v1, p3

    #@998
    invoke-static {v0, v1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@99b
    .line 735
    const/4 v5, 0x1

    #@99c
    goto/16 :goto_7

    #@99e
    .line 739
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v16           #userId:I
    .end local v56           #service:Landroid/content/Intent;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v124           #cn:Landroid/content/ComponentName;
    :pswitch_99e
    const-string v5, "android.app.IActivityManager"

    #@9a0
    move-object/from16 v0, p2

    #@9a2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a5
    .line 740
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9a8
    move-result-object v114

    #@9a9
    .line 741
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@9ac
    move-result-object v6

    #@9ad
    .line 742
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@9af
    move-object/from16 v0, p2

    #@9b1
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@9b4
    move-result-object v56

    #@9b5
    check-cast v56, Landroid/content/Intent;

    #@9b7
    .line 743
    .restart local v56       #service:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9ba
    move-result-object v8

    #@9bb
    .line 744
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9be
    move-result v16

    #@9bf
    .line 745
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@9c1
    move-object/from16 v1, v56

    #@9c3
    move/from16 v2, v16

    #@9c5
    invoke-virtual {v0, v6, v1, v8, v2}, Landroid/app/ActivityManagerNative;->stopService(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;I)I

    #@9c8
    move-result v174

    #@9c9
    .line 746
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9cc
    .line 747
    move-object/from16 v0, p3

    #@9ce
    move/from16 v1, v174

    #@9d0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@9d3
    .line 748
    const/4 v5, 0x1

    #@9d4
    goto/16 :goto_7

    #@9d6
    .line 752
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v16           #userId:I
    .end local v56           #service:Landroid/content/Intent;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v174           #res:I
    :pswitch_9d6
    const-string v5, "android.app.IActivityManager"

    #@9d8
    move-object/from16 v0, p2

    #@9da
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9dd
    .line 753
    invoke-static/range {p2 .. p2}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@9e0
    move-result-object v48

    #@9e1
    .line 754
    .local v48, className:Landroid/content/ComponentName;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9e4
    move-result-object v49

    #@9e5
    .line 755
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@9e8
    move-result v184

    #@9e9
    .line 756
    .local v184, startId:I
    move-object/from16 v0, p0

    #@9eb
    move-object/from16 v1, v48

    #@9ed
    move-object/from16 v2, v49

    #@9ef
    move/from16 v3, v184

    #@9f1
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->stopServiceToken(Landroid/content/ComponentName;Landroid/os/IBinder;I)Z

    #@9f4
    move-result v174

    #@9f5
    .line 757
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9f8
    .line 758
    if-eqz v174, :cond_a03

    #@9fa
    const/4 v5, 0x1

    #@9fb
    :goto_9fb
    move-object/from16 v0, p3

    #@9fd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@a00
    .line 759
    const/4 v5, 0x1

    #@a01
    goto/16 :goto_7

    #@a03
    .line 758
    :cond_a03
    const/4 v5, 0x0

    #@a04
    goto :goto_9fb

    #@a05
    .line 763
    .end local v48           #className:Landroid/content/ComponentName;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:Z
    .end local v184           #startId:I
    :pswitch_a05
    const-string v5, "android.app.IActivityManager"

    #@a07
    move-object/from16 v0, p2

    #@a09
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0c
    .line 764
    invoke-static/range {p2 .. p2}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@a0f
    move-result-object v48

    #@a10
    .line 765
    .restart local v48       #className:Landroid/content/ComponentName;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a13
    move-result-object v49

    #@a14
    .line 766
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a17
    move-result v50

    #@a18
    .line 767
    .restart local v50       #id:I
    const/16 v51, 0x0

    #@a1a
    .line 768
    .local v51, notification:Landroid/app/Notification;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a1d
    move-result v5

    #@a1e
    if-eqz v5, :cond_a2a

    #@a20
    .line 769
    sget-object v5, Landroid/app/Notification;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a22
    move-object/from16 v0, p2

    #@a24
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a27
    move-result-object v51

    #@a28
    .end local v51           #notification:Landroid/app/Notification;
    check-cast v51, Landroid/app/Notification;

    #@a2a
    .line 771
    .restart local v51       #notification:Landroid/app/Notification;
    :cond_a2a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a2d
    move-result v5

    #@a2e
    if-eqz v5, :cond_a3d

    #@a30
    const/16 v52, 0x1

    #@a32
    .local v52, removeNotification:Z
    :goto_a32
    move-object/from16 v47, p0

    #@a34
    .line 772
    invoke-virtual/range {v47 .. v52}, Landroid/app/ActivityManagerNative;->setServiceForeground(Landroid/content/ComponentName;Landroid/os/IBinder;ILandroid/app/Notification;Z)V

    #@a37
    .line 773
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a3a
    .line 774
    const/4 v5, 0x1

    #@a3b
    goto/16 :goto_7

    #@a3d
    .line 771
    .end local v52           #removeNotification:Z
    :cond_a3d
    const/16 v52, 0x0

    #@a3f
    goto :goto_a32

    #@a40
    .line 778
    .end local v48           #className:Landroid/content/ComponentName;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v50           #id:I
    .end local v51           #notification:Landroid/app/Notification;
    :pswitch_a40
    const-string v5, "android.app.IActivityManager"

    #@a42
    move-object/from16 v0, p2

    #@a44
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a47
    .line 779
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a4a
    move-result-object v114

    #@a4b
    .line 780
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@a4e
    move-result-object v6

    #@a4f
    .line 781
    .restart local v6       #app:Landroid/app/IApplicationThread;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a52
    move-result-object v49

    #@a53
    .line 782
    .restart local v49       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a55
    move-object/from16 v0, p2

    #@a57
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a5a
    move-result-object v56

    #@a5b
    check-cast v56, Landroid/content/Intent;

    #@a5d
    .line 783
    .restart local v56       #service:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a60
    move-result-object v8

    #@a61
    .line 784
    .restart local v8       #resolvedType:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a64
    move-result-object v114

    #@a65
    .line 785
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a68
    move-result v59

    #@a69
    .line 786
    .restart local v59       #fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a6c
    move-result v16

    #@a6d
    .line 787
    .restart local v16       #userId:I
    invoke-static/range {v114 .. v114}, Landroid/app/IServiceConnection$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IServiceConnection;

    #@a70
    move-result-object v58

    #@a71
    .local v58, conn:Landroid/app/IServiceConnection;
    move-object/from16 v53, p0

    #@a73
    move-object/from16 v54, v6

    #@a75
    move-object/from16 v55, v49

    #@a77
    move-object/from16 v57, v8

    #@a79
    move/from16 v60, v16

    #@a7b
    .line 788
    invoke-virtual/range {v53 .. v60}, Landroid/app/ActivityManagerNative;->bindService(Landroid/app/IApplicationThread;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/app/IServiceConnection;II)I

    #@a7e
    move-result v174

    #@a7f
    .line 789
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a82
    .line 790
    move-object/from16 v0, p3

    #@a84
    move/from16 v1, v174

    #@a86
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@a89
    .line 791
    const/4 v5, 0x1

    #@a8a
    goto/16 :goto_7

    #@a8c
    .line 795
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v16           #userId:I
    .end local v49           #token:Landroid/os/IBinder;
    .end local v56           #service:Landroid/content/Intent;
    .end local v58           #conn:Landroid/app/IServiceConnection;
    .end local v59           #fl:I
    .end local v114           #b:Landroid/os/IBinder;
    .end local v174           #res:I
    :pswitch_a8c
    const-string v5, "android.app.IActivityManager"

    #@a8e
    move-object/from16 v0, p2

    #@a90
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a93
    .line 796
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a96
    move-result-object v114

    #@a97
    .line 797
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/IServiceConnection$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IServiceConnection;

    #@a9a
    move-result-object v58

    #@a9b
    .line 798
    .restart local v58       #conn:Landroid/app/IServiceConnection;
    move-object/from16 v0, p0

    #@a9d
    move-object/from16 v1, v58

    #@a9f
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unbindService(Landroid/app/IServiceConnection;)Z

    #@aa2
    move-result v174

    #@aa3
    .line 799
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@aa6
    .line 800
    if-eqz v174, :cond_ab1

    #@aa8
    const/4 v5, 0x1

    #@aa9
    :goto_aa9
    move-object/from16 v0, p3

    #@aab
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@aae
    .line 801
    const/4 v5, 0x1

    #@aaf
    goto/16 :goto_7

    #@ab1
    .line 800
    :cond_ab1
    const/4 v5, 0x0

    #@ab2
    goto :goto_aa9

    #@ab3
    .line 805
    .end local v58           #conn:Landroid/app/IServiceConnection;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v174           #res:Z
    :pswitch_ab3
    const-string v5, "android.app.IActivityManager"

    #@ab5
    move-object/from16 v0, p2

    #@ab7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@aba
    .line 806
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@abd
    move-result-object v49

    #@abe
    .line 807
    .restart local v49       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ac0
    move-object/from16 v0, p2

    #@ac2
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ac5
    move-result-object v7

    #@ac6
    check-cast v7, Landroid/content/Intent;

    #@ac8
    .line 808
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@acb
    move-result-object v56

    #@acc
    .line 809
    .local v56, service:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@ace
    move-object/from16 v1, v49

    #@ad0
    move-object/from16 v2, v56

    #@ad2
    invoke-virtual {v0, v1, v7, v2}, Landroid/app/ActivityManagerNative;->publishService(Landroid/os/IBinder;Landroid/content/Intent;Landroid/os/IBinder;)V

    #@ad5
    .line 810
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad8
    .line 811
    const/4 v5, 0x1

    #@ad9
    goto/16 :goto_7

    #@adb
    .line 815
    .end local v7           #intent:Landroid/content/Intent;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v56           #service:Landroid/os/IBinder;
    :pswitch_adb
    const-string v5, "android.app.IActivityManager"

    #@add
    move-object/from16 v0, p2

    #@adf
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae2
    .line 816
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ae5
    move-result-object v49

    #@ae6
    .line 817
    .restart local v49       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ae8
    move-object/from16 v0, p2

    #@aea
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@aed
    move-result-object v7

    #@aee
    check-cast v7, Landroid/content/Intent;

    #@af0
    .line 818
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@af3
    move-result v5

    #@af4
    if-eqz v5, :cond_b07

    #@af6
    const/16 v129, 0x1

    #@af8
    .line 819
    .local v129, doRebind:Z
    :goto_af8
    move-object/from16 v0, p0

    #@afa
    move-object/from16 v1, v49

    #@afc
    move/from16 v2, v129

    #@afe
    invoke-virtual {v0, v1, v7, v2}, Landroid/app/ActivityManagerNative;->unbindFinished(Landroid/os/IBinder;Landroid/content/Intent;Z)V

    #@b01
    .line 820
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b04
    .line 821
    const/4 v5, 0x1

    #@b05
    goto/16 :goto_7

    #@b07
    .line 818
    .end local v129           #doRebind:Z
    :cond_b07
    const/16 v129, 0x0

    #@b09
    goto :goto_af8

    #@b0a
    .line 825
    .end local v7           #intent:Landroid/content/Intent;
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_b0a
    const-string v5, "android.app.IActivityManager"

    #@b0c
    move-object/from16 v0, p2

    #@b0e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b11
    .line 826
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b14
    move-result-object v49

    #@b15
    .line 827
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b18
    move-result v67

    #@b19
    .line 828
    .local v67, type:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b1c
    move-result v184

    #@b1d
    .line 829
    .restart local v184       #startId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b20
    move-result v174

    #@b21
    .line 830
    .local v174, res:I
    move-object/from16 v0, p0

    #@b23
    move-object/from16 v1, v49

    #@b25
    move/from16 v2, v67

    #@b27
    move/from16 v3, v184

    #@b29
    move/from16 v4, v174

    #@b2b
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->serviceDoneExecuting(Landroid/os/IBinder;III)V

    #@b2e
    .line 831
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b31
    .line 832
    const/4 v5, 0x1

    #@b32
    goto/16 :goto_7

    #@b34
    .line 836
    .end local v49           #token:Landroid/os/IBinder;
    .end local v67           #type:I
    .end local v174           #res:I
    .end local v184           #startId:I
    :pswitch_b34
    const-string v5, "android.app.IActivityManager"

    #@b36
    move-object/from16 v0, p2

    #@b38
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b3b
    .line 837
    invoke-static/range {p2 .. p2}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@b3e
    move-result-object v48

    #@b3f
    .line 838
    .restart local v48       #className:Landroid/content/ComponentName;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b42
    move-result-object v13

    #@b43
    .line 839
    .restart local v13       #profileFile:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b46
    move-result v59

    #@b47
    .line 840
    .restart local v59       #fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@b4a
    move-result-object v64

    #@b4b
    .line 841
    .local v64, arguments:Landroid/os/Bundle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b4e
    move-result-object v114

    #@b4f
    .line 842
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/IInstrumentationWatcher$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IInstrumentationWatcher;

    #@b52
    move-result-object v65

    #@b53
    .line 843
    .local v65, w:Landroid/app/IInstrumentationWatcher;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b56
    move-result v16

    #@b57
    .restart local v16       #userId:I
    move-object/from16 v60, p0

    #@b59
    move-object/from16 v61, v48

    #@b5b
    move-object/from16 v62, v13

    #@b5d
    move/from16 v63, v59

    #@b5f
    move/from16 v66, v16

    #@b61
    .line 844
    invoke-virtual/range {v60 .. v66}, Landroid/app/ActivityManagerNative;->startInstrumentation(Landroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;I)Z

    #@b64
    move-result v174

    #@b65
    .line 845
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b68
    .line 846
    if-eqz v174, :cond_b73

    #@b6a
    const/4 v5, 0x1

    #@b6b
    :goto_b6b
    move-object/from16 v0, p3

    #@b6d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b70
    .line 847
    const/4 v5, 0x1

    #@b71
    goto/16 :goto_7

    #@b73
    .line 846
    :cond_b73
    const/4 v5, 0x0

    #@b74
    goto :goto_b6b

    #@b75
    .line 852
    .end local v13           #profileFile:Ljava/lang/String;
    .end local v16           #userId:I
    .end local v48           #className:Landroid/content/ComponentName;
    .end local v59           #fl:I
    .end local v64           #arguments:Landroid/os/Bundle;
    .end local v65           #w:Landroid/app/IInstrumentationWatcher;
    .end local v114           #b:Landroid/os/IBinder;
    .end local v174           #res:Z
    :pswitch_b75
    const-string v5, "android.app.IActivityManager"

    #@b77
    move-object/from16 v0, p2

    #@b79
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b7c
    .line 853
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b7f
    move-result-object v114

    #@b80
    .line 854
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@b83
    move-result-object v6

    #@b84
    .line 855
    .restart local v6       #app:Landroid/app/IApplicationThread;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b87
    move-result v41

    #@b88
    .line 856
    .restart local v41       #resultCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@b8b
    move-result-object v177

    #@b8c
    .line 857
    .local v177, results:Landroid/os/Bundle;
    move-object/from16 v0, p0

    #@b8e
    move/from16 v1, v41

    #@b90
    move-object/from16 v2, v177

    #@b92
    invoke-virtual {v0, v6, v1, v2}, Landroid/app/ActivityManagerNative;->finishInstrumentation(Landroid/app/IApplicationThread;ILandroid/os/Bundle;)V

    #@b95
    .line 858
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b98
    .line 859
    const/4 v5, 0x1

    #@b99
    goto/16 :goto_7

    #@b9b
    .line 863
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v41           #resultCode:I
    .end local v114           #b:Landroid/os/IBinder;
    .end local v177           #results:Landroid/os/Bundle;
    :pswitch_b9b
    const-string v5, "android.app.IActivityManager"

    #@b9d
    move-object/from16 v0, p2

    #@b9f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ba2
    .line 864
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getConfiguration()Landroid/content/res/Configuration;

    #@ba5
    move-result-object v25

    #@ba6
    .line 865
    .restart local v25       #config:Landroid/content/res/Configuration;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ba9
    .line 866
    const/4 v5, 0x0

    #@baa
    move-object/from16 v0, v25

    #@bac
    move-object/from16 v1, p3

    #@bae
    invoke-virtual {v0, v1, v5}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    #@bb1
    .line 867
    const/4 v5, 0x1

    #@bb2
    goto/16 :goto_7

    #@bb4
    .line 871
    .end local v25           #config:Landroid/content/res/Configuration;
    :pswitch_bb4
    const-string v5, "android.app.IActivityManager"

    #@bb6
    move-object/from16 v0, p2

    #@bb8
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bbb
    .line 872
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bbd
    move-object/from16 v0, p2

    #@bbf
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bc2
    move-result-object v25

    #@bc3
    check-cast v25, Landroid/content/res/Configuration;

    #@bc5
    .line 873
    .restart local v25       #config:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@bc7
    move-object/from16 v1, v25

    #@bc9
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->updateConfiguration(Landroid/content/res/Configuration;)V

    #@bcc
    .line 874
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bcf
    .line 875
    const/4 v5, 0x1

    #@bd0
    goto/16 :goto_7

    #@bd2
    .line 879
    .end local v25           #config:Landroid/content/res/Configuration;
    :pswitch_bd2
    const-string v5, "android.app.IActivityManager"

    #@bd4
    move-object/from16 v0, p2

    #@bd6
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bd9
    .line 880
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@bdc
    move-result-object v49

    #@bdd
    .line 881
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@be0
    move-result v173

    #@be1
    .line 882
    .local v173, requestedOrientation:I
    move-object/from16 v0, p0

    #@be3
    move-object/from16 v1, v49

    #@be5
    move/from16 v2, v173

    #@be7
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->setRequestedOrientation(Landroid/os/IBinder;I)V

    #@bea
    .line 883
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bed
    .line 884
    const/4 v5, 0x1

    #@bee
    goto/16 :goto_7

    #@bf0
    .line 888
    .end local v49           #token:Landroid/os/IBinder;
    .end local v173           #requestedOrientation:I
    :pswitch_bf0
    const-string v5, "android.app.IActivityManager"

    #@bf2
    move-object/from16 v0, p2

    #@bf4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf7
    .line 889
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@bfa
    move-result-object v49

    #@bfb
    .line 890
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@bfd
    move-object/from16 v1, v49

    #@bff
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getRequestedOrientation(Landroid/os/IBinder;)I

    #@c02
    move-result v172

    #@c03
    .line 891
    .local v172, req:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c06
    .line 892
    move-object/from16 v0, p3

    #@c08
    move/from16 v1, v172

    #@c0a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@c0d
    .line 893
    const/4 v5, 0x1

    #@c0e
    goto/16 :goto_7

    #@c10
    .line 897
    .end local v49           #token:Landroid/os/IBinder;
    .end local v172           #req:I
    :pswitch_c10
    const-string v5, "android.app.IActivityManager"

    #@c12
    move-object/from16 v0, p2

    #@c14
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c17
    .line 898
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c1a
    move-result-object v49

    #@c1b
    .line 899
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@c1d
    move-object/from16 v1, v49

    #@c1f
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getActivityClassForToken(Landroid/os/IBinder;)Landroid/content/ComponentName;

    #@c22
    move-result-object v124

    #@c23
    .line 900
    .restart local v124       #cn:Landroid/content/ComponentName;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c26
    .line 901
    move-object/from16 v0, v124

    #@c28
    move-object/from16 v1, p3

    #@c2a
    invoke-static {v0, v1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@c2d
    .line 902
    const/4 v5, 0x1

    #@c2e
    goto/16 :goto_7

    #@c30
    .line 906
    .end local v49           #token:Landroid/os/IBinder;
    .end local v124           #cn:Landroid/content/ComponentName;
    :pswitch_c30
    const-string v5, "android.app.IActivityManager"

    #@c32
    move-object/from16 v0, p2

    #@c34
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c37
    .line 907
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c3a
    move-result-object v49

    #@c3b
    .line 908
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c3e
    .line 909
    move-object/from16 v0, p0

    #@c40
    move-object/from16 v1, v49

    #@c42
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getPackageForToken(Landroid/os/IBinder;)Ljava/lang/String;

    #@c45
    move-result-object v5

    #@c46
    move-object/from16 v0, p3

    #@c48
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c4b
    .line 910
    const/4 v5, 0x1

    #@c4c
    goto/16 :goto_7

    #@c4e
    .line 914
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_c4e
    const-string v5, "android.app.IActivityManager"

    #@c50
    move-object/from16 v0, p2

    #@c52
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c55
    .line 915
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c58
    move-result v67

    #@c59
    .line 916
    .restart local v67       #type:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c5c
    move-result-object v19

    #@c5d
    .line 917
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c60
    move-result-object v49

    #@c61
    .line 918
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c64
    move-result-object v10

    #@c65
    .line 919
    .restart local v10       #resultWho:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c68
    move-result v11

    #@c69
    .line 922
    .restart local v11       #requestCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c6c
    move-result v5

    #@c6d
    if-eqz v5, :cond_cbb

    #@c6f
    .line 923
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c71
    move-object/from16 v0, p2

    #@c73
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@c76
    move-result-object v72

    #@c77
    check-cast v72, [Landroid/content/Intent;

    #@c79
    .line 924
    .local v72, requestIntents:[Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@c7c
    move-result-object v73

    #@c7d
    .line 929
    .local v73, requestResolvedTypes:[Ljava/lang/String;
    :goto_c7d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c80
    move-result v59

    #@c81
    .line 930
    .restart local v59       #fl:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c84
    move-result v5

    #@c85
    if-eqz v5, :cond_cc0

    #@c87
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c89
    move-object/from16 v0, p2

    #@c8b
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c8e
    move-result-object v5

    #@c8f
    check-cast v5, Landroid/os/Bundle;

    #@c91
    move-object v15, v5

    #@c92
    .line 932
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_c92
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c95
    move-result v16

    #@c96
    .restart local v16       #userId:I
    move-object/from16 v66, p0

    #@c98
    move-object/from16 v68, v19

    #@c9a
    move-object/from16 v69, v49

    #@c9c
    move-object/from16 v70, v10

    #@c9e
    move/from16 v71, v11

    #@ca0
    move/from16 v74, v59

    #@ca2
    move-object/from16 v75, v15

    #@ca4
    move/from16 v76, v16

    #@ca6
    .line 933
    invoke-virtual/range {v66 .. v76}, Landroid/app/ActivityManagerNative;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@ca9
    move-result-object v174

    #@caa
    .line 936
    .local v174, res:Landroid/content/IIntentSender;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cad
    .line 937
    if-eqz v174, :cond_cc2

    #@caf
    invoke-interface/range {v174 .. v174}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@cb2
    move-result-object v5

    #@cb3
    :goto_cb3
    move-object/from16 v0, p3

    #@cb5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@cb8
    .line 938
    const/4 v5, 0x1

    #@cb9
    goto/16 :goto_7

    #@cbb
    .line 926
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v59           #fl:I
    .end local v72           #requestIntents:[Landroid/content/Intent;
    .end local v73           #requestResolvedTypes:[Ljava/lang/String;
    .end local v174           #res:Landroid/content/IIntentSender;
    :cond_cbb
    const/16 v72, 0x0

    #@cbd
    .line 927
    .restart local v72       #requestIntents:[Landroid/content/Intent;
    const/16 v73, 0x0

    #@cbf
    .restart local v73       #requestResolvedTypes:[Ljava/lang/String;
    goto :goto_c7d

    #@cc0
    .line 930
    .restart local v59       #fl:I
    :cond_cc0
    const/4 v15, 0x0

    #@cc1
    goto :goto_c92

    #@cc2
    .line 937
    .restart local v15       #options:Landroid/os/Bundle;
    .restart local v16       #userId:I
    .restart local v174       #res:Landroid/content/IIntentSender;
    :cond_cc2
    const/4 v5, 0x0

    #@cc3
    goto :goto_cb3

    #@cc4
    .line 942
    .end local v10           #resultWho:Ljava/lang/String;
    .end local v11           #requestCode:I
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v59           #fl:I
    .end local v67           #type:I
    .end local v72           #requestIntents:[Landroid/content/Intent;
    .end local v73           #requestResolvedTypes:[Ljava/lang/String;
    .end local v174           #res:Landroid/content/IIntentSender;
    :pswitch_cc4
    const-string v5, "android.app.IActivityManager"

    #@cc6
    move-object/from16 v0, p2

    #@cc8
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ccb
    .line 943
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@cce
    move-result-object v5

    #@ccf
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@cd2
    move-result-object v168

    #@cd3
    .line 945
    .local v168, r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@cd5
    move-object/from16 v1, v168

    #@cd7
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->cancelIntentSender(Landroid/content/IIntentSender;)V

    #@cda
    .line 946
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cdd
    .line 947
    const/4 v5, 0x1

    #@cde
    goto/16 :goto_7

    #@ce0
    .line 951
    .end local v168           #r:Landroid/content/IIntentSender;
    :pswitch_ce0
    const-string v5, "android.app.IActivityManager"

    #@ce2
    move-object/from16 v0, p2

    #@ce4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ce7
    .line 952
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@cea
    move-result-object v5

    #@ceb
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@cee
    move-result-object v168

    #@cef
    .line 954
    .restart local v168       #r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@cf1
    move-object/from16 v1, v168

    #@cf3
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;

    #@cf6
    move-result-object v174

    #@cf7
    .line 955
    .local v174, res:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cfa
    .line 956
    move-object/from16 v0, p3

    #@cfc
    move-object/from16 v1, v174

    #@cfe
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d01
    .line 957
    const/4 v5, 0x1

    #@d02
    goto/16 :goto_7

    #@d04
    .line 961
    .end local v168           #r:Landroid/content/IIntentSender;
    .end local v174           #res:Ljava/lang/String;
    :pswitch_d04
    const-string v5, "android.app.IActivityManager"

    #@d06
    move-object/from16 v0, p2

    #@d08
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d0b
    .line 962
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@d0e
    move-result-object v5

    #@d0f
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@d12
    move-result-object v168

    #@d13
    .line 964
    .restart local v168       #r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@d15
    move-object/from16 v1, v168

    #@d17
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getUidForIntentSender(Landroid/content/IIntentSender;)I

    #@d1a
    move-result v174

    #@d1b
    .line 965
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1e
    .line 966
    move-object/from16 v0, p3

    #@d20
    move/from16 v1, v174

    #@d22
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@d25
    .line 967
    const/4 v5, 0x1

    #@d26
    goto/16 :goto_7

    #@d28
    .line 971
    .end local v168           #r:Landroid/content/IIntentSender;
    .end local v174           #res:I
    :pswitch_d28
    const-string v5, "android.app.IActivityManager"

    #@d2a
    move-object/from16 v0, p2

    #@d2c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d2f
    .line 972
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d32
    move-result v75

    #@d33
    .line 973
    .local v75, callingPid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d36
    move-result v76

    #@d37
    .line 974
    .local v76, callingUid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d3a
    move-result v16

    #@d3b
    .line 975
    .restart local v16       #userId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d3e
    move-result v5

    #@d3f
    if-eqz v5, :cond_d68

    #@d41
    const/16 v78, 0x1

    #@d43
    .line 976
    .local v78, allowAll:Z
    :goto_d43
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d46
    move-result v5

    #@d47
    if-eqz v5, :cond_d6b

    #@d49
    const/16 v79, 0x1

    #@d4b
    .line 977
    .local v79, requireFull:Z
    :goto_d4b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d4e
    move-result-object v80

    #@d4f
    .line 978
    .restart local v80       #name:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d52
    move-result-object v81

    #@d53
    .local v81, callerPackage:Ljava/lang/String;
    move-object/from16 v74, p0

    #@d55
    move/from16 v77, v16

    #@d57
    .line 979
    invoke-virtual/range {v74 .. v81}, Landroid/app/ActivityManagerNative;->handleIncomingUser(IIIZZLjava/lang/String;Ljava/lang/String;)I

    #@d5a
    move-result v174

    #@d5b
    .line 981
    .restart local v174       #res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d5e
    .line 982
    move-object/from16 v0, p3

    #@d60
    move/from16 v1, v174

    #@d62
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@d65
    .line 983
    const/4 v5, 0x1

    #@d66
    goto/16 :goto_7

    #@d68
    .line 975
    .end local v78           #allowAll:Z
    .end local v79           #requireFull:Z
    .end local v80           #name:Ljava/lang/String;
    .end local v81           #callerPackage:Ljava/lang/String;
    .end local v174           #res:I
    :cond_d68
    const/16 v78, 0x0

    #@d6a
    goto :goto_d43

    #@d6b
    .line 976
    .restart local v78       #allowAll:Z
    :cond_d6b
    const/16 v79, 0x0

    #@d6d
    goto :goto_d4b

    #@d6e
    .line 987
    .end local v16           #userId:I
    .end local v75           #callingPid:I
    .end local v76           #callingUid:I
    .end local v78           #allowAll:Z
    :pswitch_d6e
    const-string v5, "android.app.IActivityManager"

    #@d70
    move-object/from16 v0, p2

    #@d72
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d75
    .line 988
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d78
    move-result v148

    #@d79
    .line 989
    .local v148, max:I
    move-object/from16 v0, p0

    #@d7b
    move/from16 v1, v148

    #@d7d
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->setProcessLimit(I)V

    #@d80
    .line 990
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d83
    .line 991
    const/4 v5, 0x1

    #@d84
    goto/16 :goto_7

    #@d86
    .line 995
    .end local v148           #max:I
    :pswitch_d86
    const-string v5, "android.app.IActivityManager"

    #@d88
    move-object/from16 v0, p2

    #@d8a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d8d
    .line 996
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getProcessLimit()I

    #@d90
    move-result v141

    #@d91
    .line 997
    .local v141, limit:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@d94
    .line 998
    move-object/from16 v0, p3

    #@d96
    move/from16 v1, v141

    #@d98
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@d9b
    .line 999
    const/4 v5, 0x1

    #@d9c
    goto/16 :goto_7

    #@d9e
    .line 1003
    .end local v141           #limit:I
    :pswitch_d9e
    const-string v5, "android.app.IActivityManager"

    #@da0
    move-object/from16 v0, p2

    #@da2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@da5
    .line 1004
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@da8
    move-result-object v49

    #@da9
    .line 1005
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@dac
    move-result v161

    #@dad
    .line 1006
    .local v161, pid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@db0
    move-result v5

    #@db1
    if-eqz v5, :cond_dc6

    #@db3
    const/16 v139, 0x1

    #@db5
    .line 1007
    .local v139, isForeground:Z
    :goto_db5
    move-object/from16 v0, p0

    #@db7
    move-object/from16 v1, v49

    #@db9
    move/from16 v2, v161

    #@dbb
    move/from16 v3, v139

    #@dbd
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->setProcessForeground(Landroid/os/IBinder;IZ)V

    #@dc0
    .line 1008
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@dc3
    .line 1009
    const/4 v5, 0x1

    #@dc4
    goto/16 :goto_7

    #@dc6
    .line 1006
    .end local v139           #isForeground:Z
    :cond_dc6
    const/16 v139, 0x0

    #@dc8
    goto :goto_db5

    #@dc9
    .line 1013
    .end local v49           #token:Landroid/os/IBinder;
    .end local v161           #pid:I
    :pswitch_dc9
    const-string v5, "android.app.IActivityManager"

    #@dcb
    move-object/from16 v0, p2

    #@dcd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dd0
    .line 1014
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@dd3
    move-result-object v22

    #@dd4
    .line 1015
    .restart local v22       #perm:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@dd7
    move-result v161

    #@dd8
    .line 1016
    .restart local v161       #pid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ddb
    move-result v193

    #@ddc
    .line 1017
    .local v193, uid:I
    move-object/from16 v0, p0

    #@dde
    move-object/from16 v1, v22

    #@de0
    move/from16 v2, v161

    #@de2
    move/from16 v3, v193

    #@de4
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->checkPermission(Ljava/lang/String;II)I

    #@de7
    move-result v174

    #@de8
    .line 1018
    .restart local v174       #res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@deb
    .line 1019
    move-object/from16 v0, p3

    #@ded
    move/from16 v1, v174

    #@def
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@df2
    .line 1020
    const/4 v5, 0x1

    #@df3
    goto/16 :goto_7

    #@df5
    .line 1024
    .end local v22           #perm:Ljava/lang/String;
    .end local v161           #pid:I
    .end local v174           #res:I
    .end local v193           #uid:I
    :pswitch_df5
    const-string v5, "android.app.IActivityManager"

    #@df7
    move-object/from16 v0, p2

    #@df9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dfc
    .line 1025
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@dfe
    move-object/from16 v0, p2

    #@e00
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e03
    move-result-object v93

    #@e04
    check-cast v93, Landroid/net/Uri;

    #@e06
    .line 1026
    .local v93, uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e09
    move-result v161

    #@e0a
    .line 1027
    .restart local v161       #pid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e0d
    move-result v193

    #@e0e
    .line 1028
    .restart local v193       #uid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e11
    move-result v94

    #@e12
    .line 1029
    .local v94, mode:I
    move-object/from16 v0, p0

    #@e14
    move-object/from16 v1, v93

    #@e16
    move/from16 v2, v161

    #@e18
    move/from16 v3, v193

    #@e1a
    move/from16 v4, v94

    #@e1c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->checkUriPermission(Landroid/net/Uri;III)I

    #@e1f
    move-result v174

    #@e20
    .line 1030
    .restart local v174       #res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e23
    .line 1031
    move-object/from16 v0, p3

    #@e25
    move/from16 v1, v174

    #@e27
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e2a
    .line 1032
    const/4 v5, 0x1

    #@e2b
    goto/16 :goto_7

    #@e2d
    .line 1036
    .end local v93           #uri:Landroid/net/Uri;
    .end local v94           #mode:I
    .end local v161           #pid:I
    .end local v174           #res:I
    .end local v193           #uid:I
    :pswitch_e2d
    const-string v5, "android.app.IActivityManager"

    #@e2f
    move-object/from16 v0, p2

    #@e31
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e34
    .line 1037
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e37
    move-result-object v19

    #@e38
    .line 1038
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@e3b
    move-result-object v5

    #@e3c
    invoke-static {v5}, Landroid/content/pm/IPackageDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDataObserver;

    #@e3f
    move-result-object v155

    #@e40
    .line 1040
    .local v155, observer:Landroid/content/pm/IPackageDataObserver;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e43
    move-result v16

    #@e44
    .line 1041
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@e46
    move-object/from16 v1, v19

    #@e48
    move-object/from16 v2, v155

    #@e4a
    move/from16 v3, v16

    #@e4c
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->clearApplicationUserData(Ljava/lang/String;Landroid/content/pm/IPackageDataObserver;I)Z

    #@e4f
    move-result v174

    #@e50
    .line 1042
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e53
    .line 1043
    if-eqz v174, :cond_e5e

    #@e55
    const/4 v5, 0x1

    #@e56
    :goto_e56
    move-object/from16 v0, p3

    #@e58
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@e5b
    .line 1044
    const/4 v5, 0x1

    #@e5c
    goto/16 :goto_7

    #@e5e
    .line 1043
    :cond_e5e
    const/4 v5, 0x0

    #@e5f
    goto :goto_e56

    #@e60
    .line 1048
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    .end local v155           #observer:Landroid/content/pm/IPackageDataObserver;
    .end local v174           #res:Z
    :pswitch_e60
    const-string v5, "android.app.IActivityManager"

    #@e62
    move-object/from16 v0, p2

    #@e64
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e67
    .line 1049
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@e6a
    move-result-object v114

    #@e6b
    .line 1050
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@e6e
    move-result-object v6

    #@e6f
    .line 1051
    .restart local v6       #app:Landroid/app/IApplicationThread;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e72
    move-result-object v92

    #@e73
    .line 1052
    .local v92, targetPkg:Ljava/lang/String;
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e75
    move-object/from16 v0, p2

    #@e77
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e7a
    move-result-object v93

    #@e7b
    check-cast v93, Landroid/net/Uri;

    #@e7d
    .line 1053
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e80
    move-result v94

    #@e81
    .line 1054
    .restart local v94       #mode:I
    move-object/from16 v0, p0

    #@e83
    move-object/from16 v1, v92

    #@e85
    move-object/from16 v2, v93

    #@e87
    move/from16 v3, v94

    #@e89
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/app/ActivityManagerNative;->grantUriPermission(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/net/Uri;I)V

    #@e8c
    .line 1055
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e8f
    .line 1056
    const/4 v5, 0x1

    #@e90
    goto/16 :goto_7

    #@e92
    .line 1060
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v92           #targetPkg:Ljava/lang/String;
    .end local v93           #uri:Landroid/net/Uri;
    .end local v94           #mode:I
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_e92
    const-string v5, "android.app.IActivityManager"

    #@e94
    move-object/from16 v0, p2

    #@e96
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e99
    .line 1061
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@e9c
    move-result-object v114

    #@e9d
    .line 1062
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@ea0
    move-result-object v6

    #@ea1
    .line 1063
    .restart local v6       #app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ea3
    move-object/from16 v0, p2

    #@ea5
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ea8
    move-result-object v93

    #@ea9
    check-cast v93, Landroid/net/Uri;

    #@eab
    .line 1064
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@eae
    move-result v94

    #@eaf
    .line 1065
    .restart local v94       #mode:I
    move-object/from16 v0, p0

    #@eb1
    move-object/from16 v1, v93

    #@eb3
    move/from16 v2, v94

    #@eb5
    invoke-virtual {v0, v6, v1, v2}, Landroid/app/ActivityManagerNative;->revokeUriPermission(Landroid/app/IApplicationThread;Landroid/net/Uri;I)V

    #@eb8
    .line 1066
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ebb
    .line 1067
    const/4 v5, 0x1

    #@ebc
    goto/16 :goto_7

    #@ebe
    .line 1071
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v93           #uri:Landroid/net/Uri;
    .end local v94           #mode:I
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_ebe
    const-string v5, "android.app.IActivityManager"

    #@ec0
    move-object/from16 v0, p2

    #@ec2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ec5
    .line 1072
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ec8
    move-result-object v114

    #@ec9
    .line 1073
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@ecc
    move-result-object v6

    #@ecd
    .line 1074
    .restart local v6       #app:Landroid/app/IApplicationThread;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@ed0
    move-result v5

    #@ed1
    if-eqz v5, :cond_ee2

    #@ed3
    const/16 v198, 0x1

    #@ed5
    .line 1075
    .local v198, waiting:Z
    :goto_ed5
    move-object/from16 v0, p0

    #@ed7
    move/from16 v1, v198

    #@ed9
    invoke-virtual {v0, v6, v1}, Landroid/app/ActivityManagerNative;->showWaitingForDebugger(Landroid/app/IApplicationThread;Z)V

    #@edc
    .line 1076
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@edf
    .line 1077
    const/4 v5, 0x1

    #@ee0
    goto/16 :goto_7

    #@ee2
    .line 1074
    .end local v198           #waiting:Z
    :cond_ee2
    const/16 v198, 0x0

    #@ee4
    goto :goto_ed5

    #@ee5
    .line 1081
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_ee5
    const-string v5, "android.app.IActivityManager"

    #@ee7
    move-object/from16 v0, p2

    #@ee9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@eec
    .line 1082
    new-instance v151, Landroid/app/ActivityManager$MemoryInfo;

    #@eee
    invoke-direct/range {v151 .. v151}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    #@ef1
    .line 1083
    .local v151, mi:Landroid/app/ActivityManager$MemoryInfo;
    move-object/from16 v0, p0

    #@ef3
    move-object/from16 v1, v151

    #@ef5
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    #@ef8
    .line 1084
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@efb
    .line 1085
    const/4 v5, 0x0

    #@efc
    move-object/from16 v0, v151

    #@efe
    move-object/from16 v1, p3

    #@f00
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$MemoryInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@f03
    .line 1086
    const/4 v5, 0x1

    #@f04
    goto/16 :goto_7

    #@f06
    .line 1090
    .end local v151           #mi:Landroid/app/ActivityManager$MemoryInfo;
    :pswitch_f06
    const-string v5, "android.app.IActivityManager"

    #@f08
    move-object/from16 v0, p2

    #@f0a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f0d
    .line 1091
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->unhandledBack()V

    #@f10
    .line 1092
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f13
    .line 1093
    const/4 v5, 0x1

    #@f14
    goto/16 :goto_7

    #@f16
    .line 1097
    :pswitch_f16
    const-string v5, "android.app.IActivityManager"

    #@f18
    move-object/from16 v0, p2

    #@f1a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f1d
    .line 1098
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f20
    move-result-object v5

    #@f21
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@f24
    move-result-object v93

    #@f25
    .line 1099
    .restart local v93       #uri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@f27
    move-object/from16 v1, v93

    #@f29
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->openContentUri(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;

    #@f2c
    move-result-object v159

    #@f2d
    .line 1100
    .local v159, pfd:Landroid/os/ParcelFileDescriptor;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f30
    .line 1101
    if-eqz v159, :cond_f43

    #@f32
    .line 1102
    const/4 v5, 0x1

    #@f33
    move-object/from16 v0, p3

    #@f35
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@f38
    .line 1103
    const/4 v5, 0x1

    #@f39
    move-object/from16 v0, v159

    #@f3b
    move-object/from16 v1, p3

    #@f3d
    invoke-virtual {v0, v1, v5}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@f40
    .line 1107
    :goto_f40
    const/4 v5, 0x1

    #@f41
    goto/16 :goto_7

    #@f43
    .line 1105
    :cond_f43
    const/4 v5, 0x0

    #@f44
    move-object/from16 v0, p3

    #@f46
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@f49
    goto :goto_f40

    #@f4a
    .line 1111
    .end local v93           #uri:Landroid/net/Uri;
    .end local v159           #pfd:Landroid/os/ParcelFileDescriptor;
    :pswitch_f4a
    const-string v5, "android.app.IActivityManager"

    #@f4c
    move-object/from16 v0, p2

    #@f4e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f51
    .line 1112
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->goingToSleep()V

    #@f54
    .line 1113
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f57
    .line 1114
    const/4 v5, 0x1

    #@f58
    goto/16 :goto_7

    #@f5a
    .line 1118
    :pswitch_f5a
    const-string v5, "android.app.IActivityManager"

    #@f5c
    move-object/from16 v0, p2

    #@f5e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f61
    .line 1119
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->wakingUp()V

    #@f64
    .line 1120
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f67
    .line 1121
    const/4 v5, 0x1

    #@f68
    goto/16 :goto_7

    #@f6a
    .line 1125
    :pswitch_f6a
    const-string v5, "android.app.IActivityManager"

    #@f6c
    move-object/from16 v0, p2

    #@f6e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f71
    .line 1126
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f74
    move-result-object v164

    #@f75
    .line 1127
    .local v164, pn:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f78
    move-result v5

    #@f79
    if-eqz v5, :cond_f96

    #@f7b
    const/16 v200, 0x1

    #@f7d
    .line 1128
    .local v200, wfd:Z
    :goto_f7d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@f80
    move-result v5

    #@f81
    if-eqz v5, :cond_f99

    #@f83
    const/16 v158, 0x1

    #@f85
    .line 1129
    .local v158, per:Z
    :goto_f85
    move-object/from16 v0, p0

    #@f87
    move-object/from16 v1, v164

    #@f89
    move/from16 v2, v200

    #@f8b
    move/from16 v3, v158

    #@f8d
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->setDebugApp(Ljava/lang/String;ZZ)V

    #@f90
    .line 1130
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f93
    .line 1131
    const/4 v5, 0x1

    #@f94
    goto/16 :goto_7

    #@f96
    .line 1127
    .end local v158           #per:Z
    .end local v200           #wfd:Z
    :cond_f96
    const/16 v200, 0x0

    #@f98
    goto :goto_f7d

    #@f99
    .line 1128
    .restart local v200       #wfd:Z
    :cond_f99
    const/16 v158, 0x0

    #@f9b
    goto :goto_f85

    #@f9c
    .line 1135
    .end local v164           #pn:Ljava/lang/String;
    .end local v200           #wfd:Z
    :pswitch_f9c
    const-string v5, "android.app.IActivityManager"

    #@f9e
    move-object/from16 v0, p2

    #@fa0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fa3
    .line 1136
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@fa6
    move-result v5

    #@fa7
    if-eqz v5, :cond_fb8

    #@fa9
    const/16 v130, 0x1

    #@fab
    .line 1137
    .local v130, enabled:Z
    :goto_fab
    move-object/from16 v0, p0

    #@fad
    move/from16 v1, v130

    #@faf
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->setAlwaysFinish(Z)V

    #@fb2
    .line 1138
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@fb5
    .line 1139
    const/4 v5, 0x1

    #@fb6
    goto/16 :goto_7

    #@fb8
    .line 1136
    .end local v130           #enabled:Z
    :cond_fb8
    const/16 v130, 0x0

    #@fba
    goto :goto_fab

    #@fbb
    .line 1143
    :pswitch_fbb
    const-string v5, "android.app.IActivityManager"

    #@fbd
    move-object/from16 v0, p2

    #@fbf
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fc2
    .line 1144
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@fc5
    move-result-object v5

    #@fc6
    invoke-static {v5}, Landroid/app/IActivityController$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IActivityController;

    #@fc9
    move-result-object v199

    #@fca
    .line 1146
    .local v199, watcher:Landroid/app/IActivityController;
    move-object/from16 v0, p0

    #@fcc
    move-object/from16 v1, v199

    #@fce
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->setActivityController(Landroid/app/IActivityController;)V

    #@fd1
    .line 1149
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd4
    .line 1151
    const/4 v5, 0x1

    #@fd5
    goto/16 :goto_7

    #@fd7
    .line 1155
    .end local v199           #watcher:Landroid/app/IActivityController;
    :pswitch_fd7
    const-string v5, "android.app.IActivityManager"

    #@fd9
    move-object/from16 v0, p2

    #@fdb
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fde
    .line 1156
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->enterSafeMode()V

    #@fe1
    .line 1157
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@fe4
    .line 1158
    const/4 v5, 0x1

    #@fe5
    goto/16 :goto_7

    #@fe7
    .line 1162
    :pswitch_fe7
    const-string v5, "android.app.IActivityManager"

    #@fe9
    move-object/from16 v0, p2

    #@feb
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fee
    .line 1163
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ff1
    move-result-object v5

    #@ff2
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@ff5
    move-result-object v138

    #@ff6
    .line 1165
    .local v138, is:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@ff8
    move-object/from16 v1, v138

    #@ffa
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->noteWakeupAlarm(Landroid/content/IIntentSender;)V

    #@ffd
    .line 1166
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1000
    .line 1167
    const/4 v5, 0x1

    #@1001
    goto/16 :goto_7

    #@1003
    .line 1171
    .end local v138           #is:Landroid/content/IIntentSender;
    :pswitch_1003
    const-string v5, "android.app.IActivityManager"

    #@1005
    move-object/from16 v0, p2

    #@1007
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@100a
    .line 1172
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    #@100d
    move-result-object v162

    #@100e
    .line 1173
    .local v162, pids:[I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1011
    move-result-object v169

    #@1012
    .line 1174
    .local v169, reason:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1015
    move-result v5

    #@1016
    if-eqz v5, :cond_1034

    #@1018
    const/16 v181, 0x1

    #@101a
    .line 1175
    .local v181, secure:Z
    :goto_101a
    move-object/from16 v0, p0

    #@101c
    move-object/from16 v1, v162

    #@101e
    move-object/from16 v2, v169

    #@1020
    move/from16 v3, v181

    #@1022
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->killPids([ILjava/lang/String;Z)Z

    #@1025
    move-result v174

    #@1026
    .line 1176
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1029
    .line 1177
    if-eqz v174, :cond_1037

    #@102b
    const/4 v5, 0x1

    #@102c
    :goto_102c
    move-object/from16 v0, p3

    #@102e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1031
    .line 1178
    const/4 v5, 0x1

    #@1032
    goto/16 :goto_7

    #@1034
    .line 1174
    .end local v174           #res:Z
    .end local v181           #secure:Z
    :cond_1034
    const/16 v181, 0x0

    #@1036
    goto :goto_101a

    #@1037
    .line 1177
    .restart local v174       #res:Z
    .restart local v181       #secure:Z
    :cond_1037
    const/4 v5, 0x0

    #@1038
    goto :goto_102c

    #@1039
    .line 1182
    .end local v162           #pids:[I
    .end local v169           #reason:Ljava/lang/String;
    .end local v174           #res:Z
    .end local v181           #secure:Z
    :pswitch_1039
    const-string v5, "android.app.IActivityManager"

    #@103b
    move-object/from16 v0, p2

    #@103d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1040
    .line 1183
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1043
    move-result-object v169

    #@1044
    .line 1184
    .restart local v169       #reason:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1046
    move-object/from16 v1, v169

    #@1048
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->killProcessesBelowForeground(Ljava/lang/String;)Z

    #@104b
    move-result v174

    #@104c
    .line 1185
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@104f
    .line 1186
    if-eqz v174, :cond_105a

    #@1051
    const/4 v5, 0x1

    #@1052
    :goto_1052
    move-object/from16 v0, p3

    #@1054
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1057
    .line 1187
    const/4 v5, 0x1

    #@1058
    goto/16 :goto_7

    #@105a
    .line 1186
    :cond_105a
    const/4 v5, 0x0

    #@105b
    goto :goto_1052

    #@105c
    .line 1191
    .end local v169           #reason:Ljava/lang/String;
    .end local v174           #res:Z
    :pswitch_105c
    const-string v5, "android.app.IActivityManager"

    #@105e
    move-object/from16 v0, p2

    #@1060
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1063
    .line 1192
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1066
    move-result-object v163

    #@1067
    .line 1193
    .local v163, pkg:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@106a
    move-result-object v123

    #@106b
    .line 1194
    .local v123, cls:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@106e
    move-result-object v108

    #@106f
    .line 1195
    .local v108, action:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1072
    move-result-object v135

    #@1073
    .line 1196
    .local v135, indata:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1075
    move-object/from16 v1, v163

    #@1077
    move-object/from16 v2, v123

    #@1079
    move-object/from16 v3, v108

    #@107b
    move-object/from16 v4, v135

    #@107d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->startRunning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@1080
    .line 1197
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1083
    .line 1198
    const/4 v5, 0x1

    #@1084
    goto/16 :goto_7

    #@1086
    .line 1202
    .end local v108           #action:Ljava/lang/String;
    .end local v123           #cls:Ljava/lang/String;
    .end local v135           #indata:Ljava/lang/String;
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_1086
    const-string v5, "android.app.IActivityManager"

    #@1088
    move-object/from16 v0, p2

    #@108a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@108d
    .line 1203
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1090
    move-result-object v6

    #@1091
    .line 1204
    .local v6, app:Landroid/os/IBinder;
    new-instance v121, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@1093
    move-object/from16 v0, v121

    #@1095
    move-object/from16 v1, p2

    #@1097
    invoke-direct {v0, v1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Landroid/os/Parcel;)V

    #@109a
    .line 1205
    .local v121, ci:Landroid/app/ApplicationErrorReport$CrashInfo;
    move-object/from16 v0, p0

    #@109c
    move-object/from16 v1, v121

    #@109e
    invoke-virtual {v0, v6, v1}, Landroid/app/ActivityManagerNative;->handleApplicationCrash(Landroid/os/IBinder;Landroid/app/ApplicationErrorReport$CrashInfo;)V

    #@10a1
    .line 1206
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10a4
    .line 1207
    const/4 v5, 0x1

    #@10a5
    goto/16 :goto_7

    #@10a7
    .line 1211
    .end local v6           #app:Landroid/os/IBinder;
    .end local v121           #ci:Landroid/app/ApplicationErrorReport$CrashInfo;
    :pswitch_10a7
    const-string v5, "android.app.IActivityManager"

    #@10a9
    move-object/from16 v0, p2

    #@10ab
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10ae
    .line 1212
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@10b1
    move-result-object v6

    #@10b2
    .line 1213
    .restart local v6       #app:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@10b5
    move-result-object v188

    #@10b6
    .line 1214
    .local v188, tag:Ljava/lang/String;
    new-instance v121, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@10b8
    move-object/from16 v0, v121

    #@10ba
    move-object/from16 v1, p2

    #@10bc
    invoke-direct {v0, v1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Landroid/os/Parcel;)V

    #@10bf
    .line 1215
    .restart local v121       #ci:Landroid/app/ApplicationErrorReport$CrashInfo;
    move-object/from16 v0, p0

    #@10c1
    move-object/from16 v1, v188

    #@10c3
    move-object/from16 v2, v121

    #@10c5
    invoke-virtual {v0, v6, v1, v2}, Landroid/app/ActivityManagerNative;->handleApplicationWtf(Landroid/os/IBinder;Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)Z

    #@10c8
    move-result v174

    #@10c9
    .line 1216
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10cc
    .line 1217
    if-eqz v174, :cond_10d7

    #@10ce
    const/4 v5, 0x1

    #@10cf
    :goto_10cf
    move-object/from16 v0, p3

    #@10d1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@10d4
    .line 1218
    const/4 v5, 0x1

    #@10d5
    goto/16 :goto_7

    #@10d7
    .line 1217
    :cond_10d7
    const/4 v5, 0x0

    #@10d8
    goto :goto_10cf

    #@10d9
    .line 1222
    .end local v6           #app:Landroid/os/IBinder;
    .end local v121           #ci:Landroid/app/ApplicationErrorReport$CrashInfo;
    .end local v174           #res:Z
    .end local v188           #tag:Ljava/lang/String;
    :pswitch_10d9
    const-string v5, "android.app.IActivityManager"

    #@10db
    move-object/from16 v0, p2

    #@10dd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10e0
    .line 1223
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@10e3
    move-result-object v6

    #@10e4
    .line 1224
    .restart local v6       #app:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@10e7
    move-result v197

    #@10e8
    .line 1225
    .local v197, violationMask:I
    new-instance v136, Landroid/os/StrictMode$ViolationInfo;

    #@10ea
    move-object/from16 v0, v136

    #@10ec
    move-object/from16 v1, p2

    #@10ee
    invoke-direct {v0, v1}, Landroid/os/StrictMode$ViolationInfo;-><init>(Landroid/os/Parcel;)V

    #@10f1
    .line 1226
    .local v136, info:Landroid/os/StrictMode$ViolationInfo;
    move-object/from16 v0, p0

    #@10f3
    move/from16 v1, v197

    #@10f5
    move-object/from16 v2, v136

    #@10f7
    invoke-virtual {v0, v6, v1, v2}, Landroid/app/ActivityManagerNative;->handleApplicationStrictModeViolation(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V

    #@10fa
    .line 1227
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10fd
    .line 1228
    const/4 v5, 0x1

    #@10fe
    goto/16 :goto_7

    #@1100
    .line 1232
    .end local v6           #app:Landroid/os/IBinder;
    .end local v136           #info:Landroid/os/StrictMode$ViolationInfo;
    .end local v197           #violationMask:I
    :pswitch_1100
    const-string v5, "android.app.IActivityManager"

    #@1102
    move-object/from16 v0, p2

    #@1104
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1107
    .line 1233
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@110a
    move-result v182

    #@110b
    .line 1234
    .local v182, sig:I
    move-object/from16 v0, p0

    #@110d
    move/from16 v1, v182

    #@110f
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->signalPersistentProcesses(I)V

    #@1112
    .line 1235
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1115
    .line 1236
    const/4 v5, 0x1

    #@1116
    goto/16 :goto_7

    #@1118
    .line 1240
    .end local v182           #sig:I
    :pswitch_1118
    const-string v5, "android.app.IActivityManager"

    #@111a
    move-object/from16 v0, p2

    #@111c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@111f
    .line 1241
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1122
    move-result-object v19

    #@1123
    .line 1242
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1126
    move-result v16

    #@1127
    .line 1243
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@1129
    move-object/from16 v1, v19

    #@112b
    move/from16 v2, v16

    #@112d
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->killBackgroundProcesses(Ljava/lang/String;I)V

    #@1130
    .line 1244
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1133
    .line 1245
    const/4 v5, 0x1

    #@1134
    goto/16 :goto_7

    #@1136
    .line 1249
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    :pswitch_1136
    const-string v5, "android.app.IActivityManager"

    #@1138
    move-object/from16 v0, p2

    #@113a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@113d
    .line 1250
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->killAllBackgroundProcesses()V

    #@1140
    .line 1251
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1143
    .line 1252
    const/4 v5, 0x1

    #@1144
    goto/16 :goto_7

    #@1146
    .line 1256
    :pswitch_1146
    const-string v5, "android.app.IActivityManager"

    #@1148
    move-object/from16 v0, p2

    #@114a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@114d
    .line 1257
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1150
    move-result-object v19

    #@1151
    .line 1258
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1154
    move-result v16

    #@1155
    .line 1259
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@1157
    move-object/from16 v1, v19

    #@1159
    move/from16 v2, v16

    #@115b
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->forceStopPackage(Ljava/lang/String;I)V

    #@115e
    .line 1260
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1161
    .line 1261
    const/4 v5, 0x1

    #@1162
    goto/16 :goto_7

    #@1164
    .line 1265
    .end local v16           #userId:I
    .end local v19           #packageName:Ljava/lang/String;
    :pswitch_1164
    const-string v5, "android.app.IActivityManager"

    #@1166
    move-object/from16 v0, p2

    #@1168
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@116b
    .line 1266
    new-instance v136, Landroid/app/ActivityManager$RunningAppProcessInfo;

    #@116d
    invoke-direct/range {v136 .. v136}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    #@1170
    .line 1268
    .local v136, info:Landroid/app/ActivityManager$RunningAppProcessInfo;
    move-object/from16 v0, p0

    #@1172
    move-object/from16 v1, v136

    #@1174
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    #@1177
    .line 1269
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@117a
    .line 1270
    const/4 v5, 0x0

    #@117b
    move-object/from16 v0, v136

    #@117d
    move-object/from16 v1, p3

    #@117f
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$RunningAppProcessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1182
    .line 1271
    const/4 v5, 0x1

    #@1183
    goto/16 :goto_7

    #@1185
    .line 1275
    .end local v136           #info:Landroid/app/ActivityManager$RunningAppProcessInfo;
    :pswitch_1185
    const-string v5, "android.app.IActivityManager"

    #@1187
    move-object/from16 v0, p2

    #@1189
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@118c
    .line 1276
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    #@118f
    move-result-object v25

    #@1190
    .line 1277
    .local v25, config:Landroid/content/pm/ConfigurationInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1193
    .line 1278
    const/4 v5, 0x0

    #@1194
    move-object/from16 v0, v25

    #@1196
    move-object/from16 v1, p3

    #@1198
    invoke-virtual {v0, v1, v5}, Landroid/content/pm/ConfigurationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@119b
    .line 1279
    const/4 v5, 0x1

    #@119c
    goto/16 :goto_7

    #@119e
    .line 1283
    .end local v25           #config:Landroid/content/pm/ConfigurationInfo;
    :pswitch_119e
    const-string v5, "android.app.IActivityManager"

    #@11a0
    move-object/from16 v0, p2

    #@11a2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11a5
    .line 1284
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11a8
    move-result-object v83

    #@11a9
    .line 1285
    .local v83, process:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11ac
    move-result v16

    #@11ad
    .line 1286
    .restart local v16       #userId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11b0
    move-result v5

    #@11b1
    if-eqz v5, :cond_11dd

    #@11b3
    const/16 v85, 0x1

    #@11b5
    .line 1287
    .local v85, start:Z
    :goto_11b5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11b8
    move-result v88

    #@11b9
    .line 1288
    .local v88, profileType:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11bc
    move-result-object v86

    #@11bd
    .line 1289
    .local v86, path:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11c0
    move-result v5

    #@11c1
    if-eqz v5, :cond_11e0

    #@11c3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@11c6
    move-result-object v87

    #@11c7
    .local v87, fd:Landroid/os/ParcelFileDescriptor;
    :goto_11c7
    move-object/from16 v82, p0

    #@11c9
    move/from16 v84, v16

    #@11cb
    .line 1291
    invoke-virtual/range {v82 .. v88}, Landroid/app/ActivityManagerNative;->profileControl(Ljava/lang/String;IZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Z

    #@11ce
    move-result v174

    #@11cf
    .line 1292
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@11d2
    .line 1293
    if-eqz v174, :cond_11e3

    #@11d4
    const/4 v5, 0x1

    #@11d5
    :goto_11d5
    move-object/from16 v0, p3

    #@11d7
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@11da
    .line 1294
    const/4 v5, 0x1

    #@11db
    goto/16 :goto_7

    #@11dd
    .line 1286
    .end local v85           #start:Z
    .end local v86           #path:Ljava/lang/String;
    .end local v87           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v88           #profileType:I
    .end local v174           #res:Z
    :cond_11dd
    const/16 v85, 0x0

    #@11df
    goto :goto_11b5

    #@11e0
    .line 1289
    .restart local v85       #start:Z
    .restart local v86       #path:Ljava/lang/String;
    .restart local v88       #profileType:I
    :cond_11e0
    const/16 v87, 0x0

    #@11e2
    goto :goto_11c7

    #@11e3
    .line 1293
    .restart local v87       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v174       #res:Z
    :cond_11e3
    const/4 v5, 0x0

    #@11e4
    goto :goto_11d5

    #@11e5
    .line 1298
    .end local v16           #userId:I
    .end local v83           #process:Ljava/lang/String;
    .end local v85           #start:Z
    .end local v86           #path:Ljava/lang/String;
    .end local v87           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v88           #profileType:I
    .end local v174           #res:Z
    :pswitch_11e5
    const-string v5, "android.app.IActivityManager"

    #@11e7
    move-object/from16 v0, p2

    #@11e9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11ec
    .line 1299
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11ef
    move-result v5

    #@11f0
    move-object/from16 v0, p0

    #@11f2
    invoke-virtual {v0, v5}, Landroid/app/ActivityManagerNative;->shutdown(I)Z

    #@11f5
    move-result v174

    #@11f6
    .line 1300
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f9
    .line 1301
    if-eqz v174, :cond_1204

    #@11fb
    const/4 v5, 0x1

    #@11fc
    :goto_11fc
    move-object/from16 v0, p3

    #@11fe
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1201
    .line 1302
    const/4 v5, 0x1

    #@1202
    goto/16 :goto_7

    #@1204
    .line 1301
    :cond_1204
    const/4 v5, 0x0

    #@1205
    goto :goto_11fc

    #@1206
    .line 1306
    .end local v174           #res:Z
    :pswitch_1206
    const-string v5, "android.app.IActivityManager"

    #@1208
    move-object/from16 v0, p2

    #@120a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@120d
    .line 1307
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->stopAppSwitches()V

    #@1210
    .line 1308
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1213
    .line 1309
    const/4 v5, 0x1

    #@1214
    goto/16 :goto_7

    #@1216
    .line 1313
    :pswitch_1216
    const-string v5, "android.app.IActivityManager"

    #@1218
    move-object/from16 v0, p2

    #@121a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@121d
    .line 1314
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->resumeAppSwitches()V

    #@1220
    .line 1315
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1223
    .line 1316
    const/4 v5, 0x1

    #@1224
    goto/16 :goto_7

    #@1226
    .line 1320
    :pswitch_1226
    const-string v5, "android.app.IActivityManager"

    #@1228
    move-object/from16 v0, p2

    #@122a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@122d
    .line 1321
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@122f
    move-object/from16 v0, p2

    #@1231
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1234
    move-result-object v56

    #@1235
    check-cast v56, Landroid/content/Intent;

    #@1237
    .line 1322
    .local v56, service:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@123a
    move-result-object v8

    #@123b
    .line 1323
    .restart local v8       #resolvedType:Ljava/lang/String;
    move-object/from16 v0, p0

    #@123d
    move-object/from16 v1, v56

    #@123f
    invoke-virtual {v0, v1, v8}, Landroid/app/ActivityManagerNative;->peekService(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;

    #@1242
    move-result-object v117

    #@1243
    .line 1324
    .local v117, binder:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1246
    .line 1325
    move-object/from16 v0, p3

    #@1248
    move-object/from16 v1, v117

    #@124a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@124d
    .line 1326
    const/4 v5, 0x1

    #@124e
    goto/16 :goto_7

    #@1250
    .line 1330
    .end local v8           #resolvedType:Ljava/lang/String;
    .end local v56           #service:Landroid/content/Intent;
    .end local v117           #binder:Landroid/os/IBinder;
    :pswitch_1250
    const-string v5, "android.app.IActivityManager"

    #@1252
    move-object/from16 v0, p2

    #@1254
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1257
    .line 1331
    sget-object v5, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1259
    move-object/from16 v0, p2

    #@125b
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@125e
    move-result-object v136

    #@125f
    check-cast v136, Landroid/content/pm/ApplicationInfo;

    #@1261
    .line 1332
    .local v136, info:Landroid/content/pm/ApplicationInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1264
    move-result v116

    #@1265
    .line 1333
    .local v116, backupRestoreMode:I
    move-object/from16 v0, p0

    #@1267
    move-object/from16 v1, v136

    #@1269
    move/from16 v2, v116

    #@126b
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->bindBackupAgent(Landroid/content/pm/ApplicationInfo;I)Z

    #@126e
    move-result v187

    #@126f
    .line 1334
    .local v187, success:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1272
    .line 1335
    if-eqz v187, :cond_127d

    #@1274
    const/4 v5, 0x1

    #@1275
    :goto_1275
    move-object/from16 v0, p3

    #@1277
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@127a
    .line 1336
    const/4 v5, 0x1

    #@127b
    goto/16 :goto_7

    #@127d
    .line 1335
    :cond_127d
    const/4 v5, 0x0

    #@127e
    goto :goto_1275

    #@127f
    .line 1340
    .end local v116           #backupRestoreMode:I
    .end local v136           #info:Landroid/content/pm/ApplicationInfo;
    .end local v187           #success:Z
    :pswitch_127f
    const-string v5, "android.app.IActivityManager"

    #@1281
    move-object/from16 v0, p2

    #@1283
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1286
    .line 1341
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1289
    move-result-object v19

    #@128a
    .line 1342
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@128d
    move-result-object v109

    #@128e
    .line 1343
    .local v109, agent:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@1290
    move-object/from16 v1, v19

    #@1292
    move-object/from16 v2, v109

    #@1294
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->backupAgentCreated(Ljava/lang/String;Landroid/os/IBinder;)V

    #@1297
    .line 1344
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@129a
    .line 1345
    const/4 v5, 0x1

    #@129b
    goto/16 :goto_7

    #@129d
    .line 1349
    .end local v19           #packageName:Ljava/lang/String;
    .end local v109           #agent:Landroid/os/IBinder;
    :pswitch_129d
    const-string v5, "android.app.IActivityManager"

    #@129f
    move-object/from16 v0, p2

    #@12a1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12a4
    .line 1350
    sget-object v5, Landroid/content/pm/ApplicationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12a6
    move-object/from16 v0, p2

    #@12a8
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@12ab
    move-result-object v136

    #@12ac
    check-cast v136, Landroid/content/pm/ApplicationInfo;

    #@12ae
    .line 1351
    .restart local v136       #info:Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p0

    #@12b0
    move-object/from16 v1, v136

    #@12b2
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unbindBackupAgent(Landroid/content/pm/ApplicationInfo;)V

    #@12b5
    .line 1352
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12b8
    .line 1353
    const/4 v5, 0x1

    #@12b9
    goto/16 :goto_7

    #@12bb
    .line 1357
    .end local v136           #info:Landroid/content/pm/ApplicationInfo;
    :pswitch_12bb
    const-string v5, "android.app.IActivityManager"

    #@12bd
    move-object/from16 v0, p2

    #@12bf
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12c2
    .line 1358
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12c5
    move-result-object v163

    #@12c6
    .line 1359
    .restart local v163       #pkg:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@12c9
    move-result v111

    #@12ca
    .line 1360
    .local v111, appid:I
    move-object/from16 v0, p0

    #@12cc
    move-object/from16 v1, v163

    #@12ce
    move/from16 v2, v111

    #@12d0
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->killApplicationWithAppId(Ljava/lang/String;I)V

    #@12d3
    .line 1361
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12d6
    .line 1362
    const/4 v5, 0x1

    #@12d7
    goto/16 :goto_7

    #@12d9
    .line 1366
    .end local v111           #appid:I
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_12d9
    const-string v5, "android.app.IActivityManager"

    #@12db
    move-object/from16 v0, p2

    #@12dd
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12e0
    .line 1367
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12e3
    move-result-object v169

    #@12e4
    .line 1368
    .restart local v169       #reason:Ljava/lang/String;
    move-object/from16 v0, p0

    #@12e6
    move-object/from16 v1, v169

    #@12e8
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->closeSystemDialogs(Ljava/lang/String;)V

    #@12eb
    .line 1369
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@12ee
    .line 1370
    const/4 v5, 0x1

    #@12ef
    goto/16 :goto_7

    #@12f1
    .line 1374
    .end local v169           #reason:Ljava/lang/String;
    :pswitch_12f1
    const-string v5, "android.app.IActivityManager"

    #@12f3
    move-object/from16 v0, p2

    #@12f5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12f8
    .line 1375
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    #@12fb
    move-result-object v162

    #@12fc
    .line 1376
    .restart local v162       #pids:[I
    move-object/from16 v0, p0

    #@12fe
    move-object/from16 v1, v162

    #@1300
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    #@1303
    move-result-object v174

    #@1304
    .line 1377
    .local v174, res:[Landroid/os/Debug$MemoryInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1307
    .line 1378
    const/4 v5, 0x1

    #@1308
    move-object/from16 v0, p3

    #@130a
    move-object/from16 v1, v174

    #@130c
    invoke-virtual {v0, v1, v5}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@130f
    .line 1379
    const/4 v5, 0x1

    #@1310
    goto/16 :goto_7

    #@1312
    .line 1383
    .end local v162           #pids:[I
    .end local v174           #res:[Landroid/os/Debug$MemoryInfo;
    :pswitch_1312
    const-string v5, "android.app.IActivityManager"

    #@1314
    move-object/from16 v0, p2

    #@1316
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1319
    .line 1384
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@131c
    move-result-object v165

    #@131d
    .line 1385
    .local v165, processName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1320
    move-result v193

    #@1321
    .line 1386
    .restart local v193       #uid:I
    move-object/from16 v0, p0

    #@1323
    move-object/from16 v1, v165

    #@1325
    move/from16 v2, v193

    #@1327
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->killApplicationProcess(Ljava/lang/String;I)V

    #@132a
    .line 1387
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@132d
    .line 1388
    const/4 v5, 0x1

    #@132e
    goto/16 :goto_7

    #@1330
    .line 1392
    .end local v165           #processName:Ljava/lang/String;
    .end local v193           #uid:I
    :pswitch_1330
    const-string v5, "android.app.IActivityManager"

    #@1332
    move-object/from16 v0, p2

    #@1334
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1337
    .line 1393
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@133a
    move-result-object v49

    #@133b
    .line 1394
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@133e
    move-result-object v19

    #@133f
    .line 1395
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1342
    move-result v131

    #@1343
    .line 1396
    .local v131, enterAnim:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1346
    move-result v132

    #@1347
    .line 1397
    .local v132, exitAnim:I
    move-object/from16 v0, p0

    #@1349
    move-object/from16 v1, v49

    #@134b
    move-object/from16 v2, v19

    #@134d
    move/from16 v3, v131

    #@134f
    move/from16 v4, v132

    #@1351
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->overridePendingTransition(Landroid/os/IBinder;Ljava/lang/String;II)V

    #@1354
    .line 1398
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1357
    .line 1399
    const/4 v5, 0x1

    #@1358
    goto/16 :goto_7

    #@135a
    .line 1403
    .end local v19           #packageName:Ljava/lang/String;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v131           #enterAnim:I
    .end local v132           #exitAnim:I
    :pswitch_135a
    const-string v5, "android.app.IActivityManager"

    #@135c
    move-object/from16 v0, p2

    #@135e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1361
    .line 1404
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->isUserAMonkey()Z

    #@1364
    move-result v112

    #@1365
    .line 1405
    .local v112, areThey:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1368
    .line 1406
    if-eqz v112, :cond_1373

    #@136a
    const/4 v5, 0x1

    #@136b
    :goto_136b
    move-object/from16 v0, p3

    #@136d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1370
    .line 1407
    const/4 v5, 0x1

    #@1371
    goto/16 :goto_7

    #@1373
    .line 1406
    :cond_1373
    const/4 v5, 0x0

    #@1374
    goto :goto_136b

    #@1375
    .line 1411
    .end local v112           #areThey:Z
    :pswitch_1375
    const-string v5, "android.app.IActivityManager"

    #@1377
    move-object/from16 v0, p2

    #@1379
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@137c
    .line 1412
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->finishHeavyWeightApp()V

    #@137f
    .line 1413
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1382
    .line 1414
    const/4 v5, 0x1

    #@1383
    goto/16 :goto_7

    #@1385
    .line 1418
    :pswitch_1385
    const-string v5, "android.app.IActivityManager"

    #@1387
    move-object/from16 v0, p2

    #@1389
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@138c
    .line 1419
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@138f
    move-result-object v49

    #@1390
    .line 1420
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@1392
    move-object/from16 v1, v49

    #@1394
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->isImmersive(Landroid/os/IBinder;)Z

    #@1397
    move-result v140

    #@1398
    .line 1421
    .local v140, isit:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@139b
    .line 1422
    if-eqz v140, :cond_13a6

    #@139d
    const/4 v5, 0x1

    #@139e
    :goto_139e
    move-object/from16 v0, p3

    #@13a0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@13a3
    .line 1423
    const/4 v5, 0x1

    #@13a4
    goto/16 :goto_7

    #@13a6
    .line 1422
    :cond_13a6
    const/4 v5, 0x0

    #@13a7
    goto :goto_139e

    #@13a8
    .line 1427
    .end local v49           #token:Landroid/os/IBinder;
    .end local v140           #isit:Z
    :pswitch_13a8
    const-string v5, "android.app.IActivityManager"

    #@13aa
    move-object/from16 v0, p2

    #@13ac
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13af
    .line 1428
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@13b2
    move-result-object v49

    #@13b3
    .line 1429
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13b6
    move-result v5

    #@13b7
    const/16 v17, 0x1

    #@13b9
    move/from16 v0, v17

    #@13bb
    if-ne v5, v0, :cond_13ce

    #@13bd
    const/16 v134, 0x1

    #@13bf
    .line 1430
    .local v134, imm:Z
    :goto_13bf
    move-object/from16 v0, p0

    #@13c1
    move-object/from16 v1, v49

    #@13c3
    move/from16 v2, v134

    #@13c5
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->setImmersive(Landroid/os/IBinder;Z)V

    #@13c8
    .line 1431
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@13cb
    .line 1432
    const/4 v5, 0x1

    #@13cc
    goto/16 :goto_7

    #@13ce
    .line 1429
    .end local v134           #imm:Z
    :cond_13ce
    const/16 v134, 0x0

    #@13d0
    goto :goto_13bf

    #@13d1
    .line 1436
    .end local v49           #token:Landroid/os/IBinder;
    :pswitch_13d1
    const-string v5, "android.app.IActivityManager"

    #@13d3
    move-object/from16 v0, p2

    #@13d5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13d8
    .line 1437
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->isTopActivityImmersive()Z

    #@13db
    move-result v140

    #@13dc
    .line 1438
    .restart local v140       #isit:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@13df
    .line 1439
    if-eqz v140, :cond_13ea

    #@13e1
    const/4 v5, 0x1

    #@13e2
    :goto_13e2
    move-object/from16 v0, p3

    #@13e4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@13e7
    .line 1440
    const/4 v5, 0x1

    #@13e8
    goto/16 :goto_7

    #@13ea
    .line 1439
    :cond_13ea
    const/4 v5, 0x0

    #@13eb
    goto :goto_13e2

    #@13ec
    .line 1444
    .end local v140           #isit:Z
    :pswitch_13ec
    const-string v5, "android.app.IActivityManager"

    #@13ee
    move-object/from16 v0, p2

    #@13f0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@13f3
    .line 1445
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13f6
    move-result v193

    #@13f7
    .line 1446
    .restart local v193       #uid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@13fa
    move-result v137

    #@13fb
    .line 1447
    .local v137, initialPid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@13fe
    move-result-object v19

    #@13ff
    .line 1448
    .restart local v19       #packageName:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1402
    move-result-object v150

    #@1403
    .line 1449
    .local v150, message:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1405
    move/from16 v1, v193

    #@1407
    move/from16 v2, v137

    #@1409
    move-object/from16 v3, v19

    #@140b
    move-object/from16 v4, v150

    #@140d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->crashApplication(IILjava/lang/String;Ljava/lang/String;)V

    #@1410
    .line 1450
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1413
    .line 1451
    const/4 v5, 0x1

    #@1414
    goto/16 :goto_7

    #@1416
    .line 1455
    .end local v19           #packageName:Ljava/lang/String;
    .end local v137           #initialPid:I
    .end local v150           #message:Ljava/lang/String;
    .end local v193           #uid:I
    :pswitch_1416
    const-string v5, "android.app.IActivityManager"

    #@1418
    move-object/from16 v0, p2

    #@141a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@141d
    .line 1456
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@141f
    move-object/from16 v0, p2

    #@1421
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1424
    move-result-object v93

    #@1425
    check-cast v93, Landroid/net/Uri;

    #@1427
    .line 1457
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@142a
    move-result v16

    #@142b
    .line 1458
    .restart local v16       #userId:I
    move-object/from16 v0, p0

    #@142d
    move-object/from16 v1, v93

    #@142f
    move/from16 v2, v16

    #@1431
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->getProviderMimeType(Landroid/net/Uri;I)Ljava/lang/String;

    #@1434
    move-result-object v67

    #@1435
    .line 1459
    .local v67, type:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1438
    .line 1460
    move-object/from16 v0, p3

    #@143a
    move-object/from16 v1, v67

    #@143c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@143f
    .line 1461
    const/4 v5, 0x1

    #@1440
    goto/16 :goto_7

    #@1442
    .line 1465
    .end local v16           #userId:I
    .end local v67           #type:Ljava/lang/String;
    .end local v93           #uri:Landroid/net/Uri;
    :pswitch_1442
    const-string v5, "android.app.IActivityManager"

    #@1444
    move-object/from16 v0, p2

    #@1446
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1449
    .line 1466
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@144c
    move-result-object v80

    #@144d
    .line 1467
    .restart local v80       #name:Ljava/lang/String;
    move-object/from16 v0, p0

    #@144f
    move-object/from16 v1, v80

    #@1451
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->newUriPermissionOwner(Ljava/lang/String;)Landroid/os/IBinder;

    #@1454
    move-result-object v22

    #@1455
    .line 1468
    .local v22, perm:Landroid/os/IBinder;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1458
    .line 1469
    move-object/from16 v0, p3

    #@145a
    move-object/from16 v1, v22

    #@145c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@145f
    .line 1470
    const/4 v5, 0x1

    #@1460
    goto/16 :goto_7

    #@1462
    .line 1474
    .end local v22           #perm:Landroid/os/IBinder;
    .end local v80           #name:Ljava/lang/String;
    :pswitch_1462
    const-string v5, "android.app.IActivityManager"

    #@1464
    move-object/from16 v0, p2

    #@1466
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1469
    .line 1475
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@146c
    move-result-object v90

    #@146d
    .line 1476
    .local v90, owner:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1470
    move-result v91

    #@1471
    .line 1477
    .local v91, fromUid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1474
    move-result-object v92

    #@1475
    .line 1478
    .restart local v92       #targetPkg:Ljava/lang/String;
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1477
    move-object/from16 v0, p2

    #@1479
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@147c
    move-result-object v93

    #@147d
    check-cast v93, Landroid/net/Uri;

    #@147f
    .line 1479
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1482
    move-result v94

    #@1483
    .restart local v94       #mode:I
    move-object/from16 v89, p0

    #@1485
    .line 1480
    invoke-virtual/range {v89 .. v94}, Landroid/app/ActivityManagerNative;->grantUriPermissionFromOwner(Landroid/os/IBinder;ILjava/lang/String;Landroid/net/Uri;I)V

    #@1488
    .line 1481
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@148b
    .line 1482
    const/4 v5, 0x1

    #@148c
    goto/16 :goto_7

    #@148e
    .line 1486
    .end local v90           #owner:Landroid/os/IBinder;
    .end local v91           #fromUid:I
    .end local v92           #targetPkg:Ljava/lang/String;
    .end local v93           #uri:Landroid/net/Uri;
    .end local v94           #mode:I
    :pswitch_148e
    const-string v5, "android.app.IActivityManager"

    #@1490
    move-object/from16 v0, p2

    #@1492
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1495
    .line 1487
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1498
    move-result-object v90

    #@1499
    .line 1488
    .restart local v90       #owner:Landroid/os/IBinder;
    const/16 v93, 0x0

    #@149b
    .line 1489
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@149e
    move-result v5

    #@149f
    if-eqz v5, :cond_14a8

    #@14a1
    .line 1490
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14a3
    move-object/from16 v0, p2

    #@14a5
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14a8
    .line 1492
    :cond_14a8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@14ab
    move-result v94

    #@14ac
    .line 1493
    .restart local v94       #mode:I
    move-object/from16 v0, p0

    #@14ae
    move-object/from16 v1, v90

    #@14b0
    move-object/from16 v2, v93

    #@14b2
    move/from16 v3, v94

    #@14b4
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ActivityManagerNative;->revokeUriPermissionFromOwner(Landroid/os/IBinder;Landroid/net/Uri;I)V

    #@14b7
    .line 1494
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@14ba
    .line 1495
    const/4 v5, 0x1

    #@14bb
    goto/16 :goto_7

    #@14bd
    .line 1499
    .end local v90           #owner:Landroid/os/IBinder;
    .end local v93           #uri:Landroid/net/Uri;
    .end local v94           #mode:I
    :pswitch_14bd
    const-string v5, "android.app.IActivityManager"

    #@14bf
    move-object/from16 v0, p2

    #@14c1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14c4
    .line 1500
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@14c7
    move-result v76

    #@14c8
    .line 1501
    .restart local v76       #callingUid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14cb
    move-result-object v92

    #@14cc
    .line 1502
    .restart local v92       #targetPkg:Ljava/lang/String;
    sget-object v5, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@14ce
    move-object/from16 v0, p2

    #@14d0
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@14d3
    move-result-object v93

    #@14d4
    check-cast v93, Landroid/net/Uri;

    #@14d6
    .line 1503
    .restart local v93       #uri:Landroid/net/Uri;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@14d9
    move-result v152

    #@14da
    .line 1504
    .local v152, modeFlags:I
    move-object/from16 v0, p0

    #@14dc
    move/from16 v1, v76

    #@14de
    move-object/from16 v2, v92

    #@14e0
    move-object/from16 v3, v93

    #@14e2
    move/from16 v4, v152

    #@14e4
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->checkGrantUriPermission(ILjava/lang/String;Landroid/net/Uri;I)I

    #@14e7
    move-result v174

    #@14e8
    .line 1505
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@14eb
    .line 1506
    move-object/from16 v0, p3

    #@14ed
    move/from16 v1, v174

    #@14ef
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@14f2
    .line 1507
    const/4 v5, 0x1

    #@14f3
    goto/16 :goto_7

    #@14f5
    .line 1511
    .end local v76           #callingUid:I
    .end local v92           #targetPkg:Ljava/lang/String;
    .end local v93           #uri:Landroid/net/Uri;
    .end local v152           #modeFlags:I
    .end local v174           #res:I
    :pswitch_14f5
    const-string v5, "android.app.IActivityManager"

    #@14f7
    move-object/from16 v0, p2

    #@14f9
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14fc
    .line 1512
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14ff
    move-result-object v83

    #@1500
    .line 1513
    .restart local v83       #process:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1503
    move-result v16

    #@1504
    .line 1514
    .restart local v16       #userId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1507
    move-result v5

    #@1508
    if-eqz v5, :cond_1536

    #@150a
    const/16 v98, 0x1

    #@150c
    .line 1515
    .local v98, managed:Z
    :goto_150c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@150f
    move-result-object v86

    #@1510
    .line 1516
    .restart local v86       #path:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1513
    move-result v5

    #@1514
    if-eqz v5, :cond_1539

    #@1516
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@1519
    move-result-object v87

    #@151a
    .restart local v87       #fd:Landroid/os/ParcelFileDescriptor;
    :goto_151a
    move-object/from16 v95, p0

    #@151c
    move-object/from16 v96, v83

    #@151e
    move/from16 v97, v16

    #@1520
    move-object/from16 v99, v86

    #@1522
    move-object/from16 v100, v87

    #@1524
    .line 1518
    invoke-virtual/range {v95 .. v100}, Landroid/app/ActivityManagerNative;->dumpHeap(Ljava/lang/String;IZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z

    #@1527
    move-result v174

    #@1528
    .line 1519
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@152b
    .line 1520
    if-eqz v174, :cond_153c

    #@152d
    const/4 v5, 0x1

    #@152e
    :goto_152e
    move-object/from16 v0, p3

    #@1530
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1533
    .line 1521
    const/4 v5, 0x1

    #@1534
    goto/16 :goto_7

    #@1536
    .line 1514
    .end local v86           #path:Ljava/lang/String;
    .end local v87           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v98           #managed:Z
    .end local v174           #res:Z
    :cond_1536
    const/16 v98, 0x0

    #@1538
    goto :goto_150c

    #@1539
    .line 1516
    .restart local v86       #path:Ljava/lang/String;
    .restart local v98       #managed:Z
    :cond_1539
    const/16 v87, 0x0

    #@153b
    goto :goto_151a

    #@153c
    .line 1520
    .restart local v87       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v174       #res:Z
    :cond_153c
    const/4 v5, 0x0

    #@153d
    goto :goto_152e

    #@153e
    .line 1526
    .end local v16           #userId:I
    .end local v83           #process:Ljava/lang/String;
    .end local v86           #path:Ljava/lang/String;
    .end local v87           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v98           #managed:Z
    .end local v174           #res:Z
    :pswitch_153e
    const-string v5, "android.app.IActivityManager"

    #@1540
    move-object/from16 v0, p2

    #@1542
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1545
    .line 1527
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1548
    move-result-object v114

    #@1549
    .line 1528
    .restart local v114       #b:Landroid/os/IBinder;
    invoke-static/range {v114 .. v114}, Landroid/app/ApplicationThreadNative;->asInterface(Landroid/os/IBinder;)Landroid/app/IApplicationThread;

    #@154c
    move-result-object v6

    #@154d
    .line 1529
    .local v6, app:Landroid/app/IApplicationThread;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@154f
    move-object/from16 v0, p2

    #@1551
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1554
    move-result-object v101

    #@1555
    check-cast v101, [Landroid/content/Intent;

    #@1557
    .line 1530
    .local v101, intents:[Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@155a
    move-result-object v102

    #@155b
    .line 1531
    .local v102, resolvedTypes:[Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@155e
    move-result-object v9

    #@155f
    .line 1532
    .local v9, resultTo:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1562
    move-result v5

    #@1563
    if-eqz v5, :cond_158f

    #@1565
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1567
    move-object/from16 v0, p2

    #@1569
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@156c
    move-result-object v5

    #@156d
    check-cast v5, Landroid/os/Bundle;

    #@156f
    move-object v15, v5

    #@1570
    .line 1534
    .restart local v15       #options:Landroid/os/Bundle;
    :goto_1570
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1573
    move-result v16

    #@1574
    .restart local v16       #userId:I
    move-object/from16 v99, p0

    #@1576
    move-object/from16 v100, v6

    #@1578
    move-object/from16 v103, v9

    #@157a
    move-object/from16 v104, v15

    #@157c
    move/from16 v105, v16

    #@157e
    .line 1535
    invoke-virtual/range {v99 .. v105}, Landroid/app/ActivityManagerNative;->startActivities(Landroid/app/IApplicationThread;[Landroid/content/Intent;[Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;I)I

    #@1581
    move-result v176

    #@1582
    .line 1537
    .local v176, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1585
    .line 1538
    move-object/from16 v0, p3

    #@1587
    move/from16 v1, v176

    #@1589
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@158c
    .line 1539
    const/4 v5, 0x1

    #@158d
    goto/16 :goto_7

    #@158f
    .line 1532
    .end local v15           #options:Landroid/os/Bundle;
    .end local v16           #userId:I
    .end local v176           #result:I
    :cond_158f
    const/4 v15, 0x0

    #@1590
    goto :goto_1570

    #@1591
    .line 1544
    .end local v6           #app:Landroid/app/IApplicationThread;
    .end local v9           #resultTo:Landroid/os/IBinder;
    .end local v101           #intents:[Landroid/content/Intent;
    .end local v102           #resolvedTypes:[Ljava/lang/String;
    .end local v114           #b:Landroid/os/IBinder;
    :pswitch_1591
    const-string v5, "android.app.IActivityManager"

    #@1593
    move-object/from16 v0, p2

    #@1595
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1598
    .line 1545
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getFrontActivityScreenCompatMode()I

    #@159b
    move-result v94

    #@159c
    .line 1546
    .restart local v94       #mode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@159f
    .line 1547
    move-object/from16 v0, p3

    #@15a1
    move/from16 v1, v94

    #@15a3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15a6
    .line 1548
    const/4 v5, 0x1

    #@15a7
    goto/16 :goto_7

    #@15a9
    .line 1553
    .end local v94           #mode:I
    :pswitch_15a9
    const-string v5, "android.app.IActivityManager"

    #@15ab
    move-object/from16 v0, p2

    #@15ad
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15b0
    .line 1554
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15b3
    move-result v94

    #@15b4
    .line 1555
    .restart local v94       #mode:I
    move-object/from16 v0, p0

    #@15b6
    move/from16 v1, v94

    #@15b8
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->setFrontActivityScreenCompatMode(I)V

    #@15bb
    .line 1556
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@15be
    .line 1557
    move-object/from16 v0, p3

    #@15c0
    move/from16 v1, v94

    #@15c2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15c5
    .line 1558
    const/4 v5, 0x1

    #@15c6
    goto/16 :goto_7

    #@15c8
    .line 1563
    .end local v94           #mode:I
    :pswitch_15c8
    const-string v5, "android.app.IActivityManager"

    #@15ca
    move-object/from16 v0, p2

    #@15cc
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15cf
    .line 1564
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15d2
    move-result-object v163

    #@15d3
    .line 1565
    .restart local v163       #pkg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@15d5
    move-object/from16 v1, v163

    #@15d7
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getPackageScreenCompatMode(Ljava/lang/String;)I

    #@15da
    move-result v94

    #@15db
    .line 1566
    .restart local v94       #mode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@15de
    .line 1567
    move-object/from16 v0, p3

    #@15e0
    move/from16 v1, v94

    #@15e2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@15e5
    .line 1568
    const/4 v5, 0x1

    #@15e6
    goto/16 :goto_7

    #@15e8
    .line 1573
    .end local v94           #mode:I
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_15e8
    const-string v5, "android.app.IActivityManager"

    #@15ea
    move-object/from16 v0, p2

    #@15ec
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15ef
    .line 1574
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15f2
    move-result-object v163

    #@15f3
    .line 1575
    .restart local v163       #pkg:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15f6
    move-result v94

    #@15f7
    .line 1576
    .restart local v94       #mode:I
    move-object/from16 v0, p0

    #@15f9
    move-object/from16 v1, v163

    #@15fb
    move/from16 v2, v94

    #@15fd
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->setPackageScreenCompatMode(Ljava/lang/String;I)V

    #@1600
    .line 1577
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1603
    .line 1578
    const/4 v5, 0x1

    #@1604
    goto/16 :goto_7

    #@1606
    .line 1582
    .end local v94           #mode:I
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_1606
    const-string v5, "android.app.IActivityManager"

    #@1608
    move-object/from16 v0, p2

    #@160a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@160d
    .line 1583
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1610
    move-result v196

    #@1611
    .line 1584
    .local v196, userid:I
    move-object/from16 v0, p0

    #@1613
    move/from16 v1, v196

    #@1615
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->switchUser(I)Z

    #@1618
    move-result v176

    #@1619
    .line 1585
    .local v176, result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@161c
    .line 1586
    if-eqz v176, :cond_1627

    #@161e
    const/4 v5, 0x1

    #@161f
    :goto_161f
    move-object/from16 v0, p3

    #@1621
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1624
    .line 1587
    const/4 v5, 0x1

    #@1625
    goto/16 :goto_7

    #@1627
    .line 1586
    :cond_1627
    const/4 v5, 0x0

    #@1628
    goto :goto_161f

    #@1629
    .line 1591
    .end local v176           #result:Z
    .end local v196           #userid:I
    :pswitch_1629
    const-string v5, "android.app.IActivityManager"

    #@162b
    move-object/from16 v0, p2

    #@162d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1630
    .line 1592
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1633
    move-result v196

    #@1634
    .line 1593
    .restart local v196       #userid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1637
    move-result-object v5

    #@1638
    invoke-static {v5}, Landroid/app/IStopUserCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IStopUserCallback;

    #@163b
    move-result-object v119

    #@163c
    .line 1595
    .local v119, callback:Landroid/app/IStopUserCallback;
    move-object/from16 v0, p0

    #@163e
    move/from16 v1, v196

    #@1640
    move-object/from16 v2, v119

    #@1642
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->stopUser(ILandroid/app/IStopUserCallback;)I

    #@1645
    move-result v176

    #@1646
    .line 1596
    .local v176, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1649
    .line 1597
    move-object/from16 v0, p3

    #@164b
    move/from16 v1, v176

    #@164d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1650
    .line 1598
    const/4 v5, 0x1

    #@1651
    goto/16 :goto_7

    #@1653
    .line 1602
    .end local v119           #callback:Landroid/app/IStopUserCallback;
    .end local v176           #result:I
    .end local v196           #userid:I
    :pswitch_1653
    const-string v5, "android.app.IActivityManager"

    #@1655
    move-object/from16 v0, p2

    #@1657
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@165a
    .line 1603
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@165d
    move-result-object v195

    #@165e
    .line 1604
    .local v195, userInfo:Landroid/content/pm/UserInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1661
    .line 1605
    const/4 v5, 0x0

    #@1662
    move-object/from16 v0, v195

    #@1664
    move-object/from16 v1, p3

    #@1666
    invoke-virtual {v0, v1, v5}, Landroid/content/pm/UserInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1669
    .line 1606
    const/4 v5, 0x1

    #@166a
    goto/16 :goto_7

    #@166c
    .line 1610
    .end local v195           #userInfo:Landroid/content/pm/UserInfo;
    :pswitch_166c
    const-string v5, "android.app.IActivityManager"

    #@166e
    move-object/from16 v0, p2

    #@1670
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1673
    .line 1611
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1676
    move-result v196

    #@1677
    .line 1612
    .restart local v196       #userid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@167a
    move-result v5

    #@167b
    if-eqz v5, :cond_1697

    #@167d
    const/16 v157, 0x1

    #@167f
    .line 1613
    .local v157, orStopping:Z
    :goto_167f
    move-object/from16 v0, p0

    #@1681
    move/from16 v1, v196

    #@1683
    move/from16 v2, v157

    #@1685
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->isUserRunning(IZ)Z

    #@1688
    move-result v176

    #@1689
    .line 1614
    .local v176, result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@168c
    .line 1615
    if-eqz v176, :cond_169a

    #@168e
    const/4 v5, 0x1

    #@168f
    :goto_168f
    move-object/from16 v0, p3

    #@1691
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1694
    .line 1616
    const/4 v5, 0x1

    #@1695
    goto/16 :goto_7

    #@1697
    .line 1612
    .end local v157           #orStopping:Z
    .end local v176           #result:Z
    :cond_1697
    const/16 v157, 0x0

    #@1699
    goto :goto_167f

    #@169a
    .line 1615
    .restart local v157       #orStopping:Z
    .restart local v176       #result:Z
    :cond_169a
    const/4 v5, 0x0

    #@169b
    goto :goto_168f

    #@169c
    .line 1620
    .end local v157           #orStopping:Z
    .end local v176           #result:Z
    .end local v196           #userid:I
    :pswitch_169c
    const-string v5, "android.app.IActivityManager"

    #@169e
    move-object/from16 v0, p2

    #@16a0
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16a3
    .line 1621
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->getRunningUserIds()[I

    #@16a6
    move-result-object v176

    #@16a7
    .line 1622
    .local v176, result:[I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@16aa
    .line 1623
    move-object/from16 v0, p3

    #@16ac
    move-object/from16 v1, v176

    #@16ae
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@16b1
    .line 1624
    const/4 v5, 0x1

    #@16b2
    goto/16 :goto_7

    #@16b4
    .line 1629
    .end local v176           #result:[I
    :pswitch_16b4
    const-string v5, "android.app.IActivityManager"

    #@16b6
    move-object/from16 v0, p2

    #@16b8
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16bb
    .line 1630
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16be
    move-result v191

    #@16bf
    .line 1631
    .local v191, taskId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16c2
    move-result v186

    #@16c3
    .line 1632
    .local v186, subTaskIndex:I
    move-object/from16 v0, p0

    #@16c5
    move/from16 v1, v191

    #@16c7
    move/from16 v2, v186

    #@16c9
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->removeSubTask(II)Z

    #@16cc
    move-result v176

    #@16cd
    .line 1633
    .local v176, result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@16d0
    .line 1634
    if-eqz v176, :cond_16db

    #@16d2
    const/4 v5, 0x1

    #@16d3
    :goto_16d3
    move-object/from16 v0, p3

    #@16d5
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@16d8
    .line 1635
    const/4 v5, 0x1

    #@16d9
    goto/16 :goto_7

    #@16db
    .line 1634
    :cond_16db
    const/4 v5, 0x0

    #@16dc
    goto :goto_16d3

    #@16dd
    .line 1640
    .end local v176           #result:Z
    .end local v186           #subTaskIndex:I
    .end local v191           #taskId:I
    :pswitch_16dd
    const-string v5, "android.app.IActivityManager"

    #@16df
    move-object/from16 v0, p2

    #@16e1
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16e4
    .line 1641
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16e7
    move-result v191

    #@16e8
    .line 1642
    .restart local v191       #taskId:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16eb
    move-result v59

    #@16ec
    .line 1643
    .restart local v59       #fl:I
    move-object/from16 v0, p0

    #@16ee
    move/from16 v1, v191

    #@16f0
    move/from16 v2, v59

    #@16f2
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->removeTask(II)Z

    #@16f5
    move-result v176

    #@16f6
    .line 1644
    .restart local v176       #result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@16f9
    .line 1645
    if-eqz v176, :cond_1704

    #@16fb
    const/4 v5, 0x1

    #@16fc
    :goto_16fc
    move-object/from16 v0, p3

    #@16fe
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1701
    .line 1646
    const/4 v5, 0x1

    #@1702
    goto/16 :goto_7

    #@1704
    .line 1645
    :cond_1704
    const/4 v5, 0x0

    #@1705
    goto :goto_16fc

    #@1706
    .line 1650
    .end local v59           #fl:I
    .end local v176           #result:Z
    .end local v191           #taskId:I
    :pswitch_1706
    const-string v5, "android.app.IActivityManager"

    #@1708
    move-object/from16 v0, p2

    #@170a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@170d
    .line 1651
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1710
    move-result-object v5

    #@1711
    invoke-static {v5}, Landroid/app/IProcessObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IProcessObserver;

    #@1714
    move-result-object v155

    #@1715
    .line 1653
    .local v155, observer:Landroid/app/IProcessObserver;
    move-object/from16 v0, p0

    #@1717
    move-object/from16 v1, v155

    #@1719
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    #@171c
    .line 1654
    const/4 v5, 0x1

    #@171d
    goto/16 :goto_7

    #@171f
    .line 1658
    .end local v155           #observer:Landroid/app/IProcessObserver;
    :pswitch_171f
    const-string v5, "android.app.IActivityManager"

    #@1721
    move-object/from16 v0, p2

    #@1723
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1726
    .line 1659
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1729
    move-result-object v5

    #@172a
    invoke-static {v5}, Landroid/app/IProcessObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IProcessObserver;

    #@172d
    move-result-object v155

    #@172e
    .line 1661
    .restart local v155       #observer:Landroid/app/IProcessObserver;
    move-object/from16 v0, p0

    #@1730
    move-object/from16 v1, v155

    #@1732
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unregisterProcessObserver(Landroid/app/IProcessObserver;)V

    #@1735
    .line 1662
    const/4 v5, 0x1

    #@1736
    goto/16 :goto_7

    #@1738
    .line 1667
    .end local v155           #observer:Landroid/app/IProcessObserver;
    :pswitch_1738
    const-string v5, "android.app.IActivityManager"

    #@173a
    move-object/from16 v0, p2

    #@173c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173f
    .line 1668
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1742
    move-result-object v163

    #@1743
    .line 1669
    .restart local v163       #pkg:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1745
    move-object/from16 v1, v163

    #@1747
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getPackageAskScreenCompat(Ljava/lang/String;)Z

    #@174a
    move-result v113

    #@174b
    .line 1670
    .local v113, ask:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@174e
    .line 1671
    if-eqz v113, :cond_1759

    #@1750
    const/4 v5, 0x1

    #@1751
    :goto_1751
    move-object/from16 v0, p3

    #@1753
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1756
    .line 1672
    const/4 v5, 0x1

    #@1757
    goto/16 :goto_7

    #@1759
    .line 1671
    :cond_1759
    const/4 v5, 0x0

    #@175a
    goto :goto_1751

    #@175b
    .line 1677
    .end local v113           #ask:Z
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_175b
    const-string v5, "android.app.IActivityManager"

    #@175d
    move-object/from16 v0, p2

    #@175f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1762
    .line 1678
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1765
    move-result-object v163

    #@1766
    .line 1679
    .restart local v163       #pkg:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1769
    move-result v5

    #@176a
    if-eqz v5, :cond_177d

    #@176c
    const/16 v113, 0x1

    #@176e
    .line 1680
    .restart local v113       #ask:Z
    :goto_176e
    move-object/from16 v0, p0

    #@1770
    move-object/from16 v1, v163

    #@1772
    move/from16 v2, v113

    #@1774
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->setPackageAskScreenCompat(Ljava/lang/String;Z)V

    #@1777
    .line 1681
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@177a
    .line 1682
    const/4 v5, 0x1

    #@177b
    goto/16 :goto_7

    #@177d
    .line 1679
    .end local v113           #ask:Z
    :cond_177d
    const/16 v113, 0x0

    #@177f
    goto :goto_176e

    #@1780
    .line 1686
    .end local v163           #pkg:Ljava/lang/String;
    :pswitch_1780
    const-string v5, "android.app.IActivityManager"

    #@1782
    move-object/from16 v0, p2

    #@1784
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1787
    .line 1687
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@178a
    move-result-object v5

    #@178b
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@178e
    move-result-object v168

    #@178f
    .line 1689
    .restart local v168       #r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@1791
    move-object/from16 v1, v168

    #@1793
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->isIntentSenderTargetedToPackage(Landroid/content/IIntentSender;)Z

    #@1796
    move-result v174

    #@1797
    .line 1690
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@179a
    .line 1691
    if-eqz v174, :cond_17a5

    #@179c
    const/4 v5, 0x1

    #@179d
    :goto_179d
    move-object/from16 v0, p3

    #@179f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@17a2
    .line 1692
    const/4 v5, 0x1

    #@17a3
    goto/16 :goto_7

    #@17a5
    .line 1691
    :cond_17a5
    const/4 v5, 0x0

    #@17a6
    goto :goto_179d

    #@17a7
    .line 1696
    .end local v168           #r:Landroid/content/IIntentSender;
    .end local v174           #res:Z
    :pswitch_17a7
    const-string v5, "android.app.IActivityManager"

    #@17a9
    move-object/from16 v0, p2

    #@17ab
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17ae
    .line 1697
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17b1
    move-result-object v5

    #@17b2
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@17b5
    move-result-object v168

    #@17b6
    .line 1699
    .restart local v168       #r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@17b8
    move-object/from16 v1, v168

    #@17ba
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->isIntentSenderAnActivity(Landroid/content/IIntentSender;)Z

    #@17bd
    move-result v174

    #@17be
    .line 1700
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@17c1
    .line 1701
    if-eqz v174, :cond_17cc

    #@17c3
    const/4 v5, 0x1

    #@17c4
    :goto_17c4
    move-object/from16 v0, p3

    #@17c6
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@17c9
    .line 1702
    const/4 v5, 0x1

    #@17ca
    goto/16 :goto_7

    #@17cc
    .line 1701
    :cond_17cc
    const/4 v5, 0x0

    #@17cd
    goto :goto_17c4

    #@17ce
    .line 1706
    .end local v168           #r:Landroid/content/IIntentSender;
    .end local v174           #res:Z
    :pswitch_17ce
    const-string v5, "android.app.IActivityManager"

    #@17d0
    move-object/from16 v0, p2

    #@17d2
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17d5
    .line 1707
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17d8
    move-result-object v5

    #@17d9
    invoke-static {v5}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@17dc
    move-result-object v168

    #@17dd
    .line 1709
    .restart local v168       #r:Landroid/content/IIntentSender;
    move-object/from16 v0, p0

    #@17df
    move-object/from16 v1, v168

    #@17e1
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getIntentForIntentSender(Landroid/content/IIntentSender;)Landroid/content/Intent;

    #@17e4
    move-result-object v7

    #@17e5
    .line 1710
    .restart local v7       #intent:Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@17e8
    .line 1711
    if-eqz v7, :cond_17f9

    #@17ea
    .line 1712
    const/4 v5, 0x1

    #@17eb
    move-object/from16 v0, p3

    #@17ed
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@17f0
    .line 1713
    const/4 v5, 0x1

    #@17f1
    move-object/from16 v0, p3

    #@17f3
    invoke-virtual {v7, v0, v5}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    #@17f6
    .line 1717
    :goto_17f6
    const/4 v5, 0x1

    #@17f7
    goto/16 :goto_7

    #@17f9
    .line 1715
    :cond_17f9
    const/4 v5, 0x0

    #@17fa
    move-object/from16 v0, p3

    #@17fc
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@17ff
    goto :goto_17f6

    #@1800
    .line 1721
    .end local v7           #intent:Landroid/content/Intent;
    .end local v168           #r:Landroid/content/IIntentSender;
    :pswitch_1800
    const-string v5, "android.app.IActivityManager"

    #@1802
    move-object/from16 v0, p2

    #@1804
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1807
    .line 1722
    sget-object v5, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1809
    move-object/from16 v0, p2

    #@180b
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@180e
    move-result-object v25

    #@180f
    check-cast v25, Landroid/content/res/Configuration;

    #@1811
    .line 1723
    .local v25, config:Landroid/content/res/Configuration;
    move-object/from16 v0, p0

    #@1813
    move-object/from16 v1, v25

    #@1815
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->updatePersistentConfiguration(Landroid/content/res/Configuration;)V

    #@1818
    .line 1724
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@181b
    .line 1725
    const/4 v5, 0x1

    #@181c
    goto/16 :goto_7

    #@181e
    .line 1729
    .end local v25           #config:Landroid/content/res/Configuration;
    :pswitch_181e
    const-string v5, "android.app.IActivityManager"

    #@1820
    move-object/from16 v0, p2

    #@1822
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1825
    .line 1730
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    #@1828
    move-result-object v162

    #@1829
    .line 1731
    .restart local v162       #pids:[I
    move-object/from16 v0, p0

    #@182b
    move-object/from16 v1, v162

    #@182d
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getProcessPss([I)[J

    #@1830
    move-result-object v167

    #@1831
    .line 1732
    .local v167, pss:[J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1834
    .line 1733
    move-object/from16 v0, p3

    #@1836
    move-object/from16 v1, v167

    #@1838
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeLongArray([J)V

    #@183b
    .line 1734
    const/4 v5, 0x1

    #@183c
    goto/16 :goto_7

    #@183e
    .line 1738
    .end local v162           #pids:[I
    .end local v167           #pss:[J
    :pswitch_183e
    const-string v5, "android.app.IActivityManager"

    #@1840
    move-object/from16 v0, p2

    #@1842
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1845
    .line 1739
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@1847
    move-object/from16 v0, p2

    #@1849
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@184c
    move-result-object v153

    #@184d
    check-cast v153, Ljava/lang/CharSequence;

    #@184f
    .line 1740
    .local v153, msg:Ljava/lang/CharSequence;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1852
    move-result v5

    #@1853
    if-eqz v5, :cond_1866

    #@1855
    const/16 v110, 0x1

    #@1857
    .line 1741
    .local v110, always:Z
    :goto_1857
    move-object/from16 v0, p0

    #@1859
    move-object/from16 v1, v153

    #@185b
    move/from16 v2, v110

    #@185d
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->showBootMessage(Ljava/lang/CharSequence;Z)V

    #@1860
    .line 1742
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1863
    .line 1743
    const/4 v5, 0x1

    #@1864
    goto/16 :goto_7

    #@1866
    .line 1740
    .end local v110           #always:Z
    :cond_1866
    const/16 v110, 0x0

    #@1868
    goto :goto_1857

    #@1869
    .line 1747
    .end local v153           #msg:Ljava/lang/CharSequence;
    :pswitch_1869
    const-string v5, "android.app.IActivityManager"

    #@186b
    move-object/from16 v0, p2

    #@186d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1870
    .line 1748
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->dismissKeyguardOnNextActivity()V

    #@1873
    .line 1749
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1876
    .line 1750
    const/4 v5, 0x1

    #@1877
    goto/16 :goto_7

    #@1879
    .line 1754
    :pswitch_1879
    const-string v5, "android.app.IActivityManager"

    #@187b
    move-object/from16 v0, p2

    #@187d
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1880
    .line 1755
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1883
    move-result-object v49

    #@1884
    .line 1756
    .restart local v49       #token:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1887
    move-result-object v128

    #@1888
    .line 1757
    .local v128, destAffinity:Ljava/lang/String;
    move-object/from16 v0, p0

    #@188a
    move-object/from16 v1, v49

    #@188c
    move-object/from16 v2, v128

    #@188e
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->targetTaskAffinityMatchesActivity(Landroid/os/IBinder;Ljava/lang/String;)Z

    #@1891
    move-result v174

    #@1892
    .line 1758
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1895
    .line 1759
    if-eqz v174, :cond_18a0

    #@1897
    const/4 v5, 0x1

    #@1898
    :goto_1898
    move-object/from16 v0, p3

    #@189a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@189d
    .line 1760
    const/4 v5, 0x1

    #@189e
    goto/16 :goto_7

    #@18a0
    .line 1759
    :cond_18a0
    const/4 v5, 0x0

    #@18a1
    goto :goto_1898

    #@18a2
    .line 1764
    .end local v49           #token:Landroid/os/IBinder;
    .end local v128           #destAffinity:Ljava/lang/String;
    .end local v174           #res:Z
    :pswitch_18a2
    const-string v5, "android.app.IActivityManager"

    #@18a4
    move-object/from16 v0, p2

    #@18a6
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18a9
    .line 1765
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18ac
    move-result-object v49

    #@18ad
    .line 1766
    .restart local v49       #token:Landroid/os/IBinder;
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18af
    move-object/from16 v0, p2

    #@18b1
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@18b4
    move-result-object v189

    #@18b5
    check-cast v189, Landroid/content/Intent;

    #@18b7
    .line 1767
    .local v189, target:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@18ba
    move-result v41

    #@18bb
    .line 1768
    .restart local v41       #resultCode:I
    const/16 v42, 0x0

    #@18bd
    .line 1769
    .local v42, resultData:Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@18c0
    move-result v5

    #@18c1
    if-eqz v5, :cond_18cd

    #@18c3
    .line 1770
    sget-object v5, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@18c5
    move-object/from16 v0, p2

    #@18c7
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@18ca
    move-result-object v42

    #@18cb
    .end local v42           #resultData:Landroid/content/Intent;
    check-cast v42, Landroid/content/Intent;

    #@18cd
    .line 1772
    .restart local v42       #resultData:Landroid/content/Intent;
    :cond_18cd
    move-object/from16 v0, p0

    #@18cf
    move-object/from16 v1, v49

    #@18d1
    move-object/from16 v2, v189

    #@18d3
    move/from16 v3, v41

    #@18d5
    move-object/from16 v4, v42

    #@18d7
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/ActivityManagerNative;->navigateUpTo(Landroid/os/IBinder;Landroid/content/Intent;ILandroid/content/Intent;)Z

    #@18da
    move-result v174

    #@18db
    .line 1773
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@18de
    .line 1774
    if-eqz v174, :cond_18e9

    #@18e0
    const/4 v5, 0x1

    #@18e1
    :goto_18e1
    move-object/from16 v0, p3

    #@18e3
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@18e6
    .line 1775
    const/4 v5, 0x1

    #@18e7
    goto/16 :goto_7

    #@18e9
    .line 1774
    :cond_18e9
    const/4 v5, 0x0

    #@18ea
    goto :goto_18e1

    #@18eb
    .line 1779
    .end local v41           #resultCode:I
    .end local v42           #resultData:Landroid/content/Intent;
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:Z
    .end local v189           #target:Landroid/content/Intent;
    :pswitch_18eb
    const-string v5, "android.app.IActivityManager"

    #@18ed
    move-object/from16 v0, p2

    #@18ef
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18f2
    .line 1780
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@18f5
    move-result-object v49

    #@18f6
    .line 1781
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@18f8
    move-object/from16 v1, v49

    #@18fa
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getLaunchedFromUid(Landroid/os/IBinder;)I

    #@18fd
    move-result v174

    #@18fe
    .line 1782
    .local v174, res:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1901
    .line 1783
    move-object/from16 v0, p3

    #@1903
    move/from16 v1, v174

    #@1905
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1908
    .line 1784
    const/4 v5, 0x1

    #@1909
    goto/16 :goto_7

    #@190b
    .line 1788
    .end local v49           #token:Landroid/os/IBinder;
    .end local v174           #res:I
    :pswitch_190b
    const-string v5, "android.app.IActivityManager"

    #@190d
    move-object/from16 v0, p2

    #@190f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1912
    .line 1789
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1915
    move-result-object v5

    #@1916
    invoke-static {v5}, Landroid/app/IUserSwitchObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IUserSwitchObserver;

    #@1919
    move-result-object v155

    #@191a
    .line 1791
    .local v155, observer:Landroid/app/IUserSwitchObserver;
    move-object/from16 v0, p0

    #@191c
    move-object/from16 v1, v155

    #@191e
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V

    #@1921
    .line 1792
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1924
    .line 1793
    const/4 v5, 0x1

    #@1925
    goto/16 :goto_7

    #@1927
    .line 1797
    .end local v155           #observer:Landroid/app/IUserSwitchObserver;
    :pswitch_1927
    const-string v5, "android.app.IActivityManager"

    #@1929
    move-object/from16 v0, p2

    #@192b
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@192e
    .line 1798
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1931
    move-result-object v5

    #@1932
    invoke-static {v5}, Landroid/app/IUserSwitchObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IUserSwitchObserver;

    #@1935
    move-result-object v155

    #@1936
    .line 1800
    .restart local v155       #observer:Landroid/app/IUserSwitchObserver;
    move-object/from16 v0, p0

    #@1938
    move-object/from16 v1, v155

    #@193a
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->unregisterUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V

    #@193d
    .line 1801
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1940
    .line 1802
    const/4 v5, 0x1

    #@1941
    goto/16 :goto_7

    #@1943
    .line 1806
    .end local v155           #observer:Landroid/app/IUserSwitchObserver;
    :pswitch_1943
    const-string v5, "android.app.IActivityManager"

    #@1945
    move-object/from16 v0, p2

    #@1947
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@194a
    .line 1807
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->requestBugReport()V

    #@194d
    .line 1808
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1950
    .line 1809
    const/4 v5, 0x1

    #@1951
    goto/16 :goto_7

    #@1953
    .line 1813
    :pswitch_1953
    const-string v5, "android.app.IActivityManager"

    #@1955
    move-object/from16 v0, p2

    #@1957
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@195a
    .line 1814
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@195d
    move-result v161

    #@195e
    .line 1815
    .restart local v161       #pid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1961
    move-result v5

    #@1962
    if-eqz v5, :cond_197d

    #@1964
    const/16 v107, 0x1

    #@1966
    .line 1816
    .local v107, aboveSystem:Z
    :goto_1966
    move-object/from16 v0, p0

    #@1968
    move/from16 v1, v161

    #@196a
    move/from16 v2, v107

    #@196c
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->inputDispatchingTimedOut(IZ)J

    #@196f
    move-result-wide v174

    #@1970
    .line 1817
    .local v174, res:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1973
    .line 1818
    move-object/from16 v0, p3

    #@1975
    move-wide/from16 v1, v174

    #@1977
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@197a
    .line 1819
    const/4 v5, 0x1

    #@197b
    goto/16 :goto_7

    #@197d
    .line 1815
    .end local v107           #aboveSystem:Z
    .end local v174           #res:J
    :cond_197d
    const/16 v107, 0x0

    #@197f
    goto :goto_1966

    #@1980
    .line 1823
    .end local v161           #pid:I
    :pswitch_1980
    const-string v5, "android.app.IActivityManager"

    #@1982
    move-object/from16 v0, p2

    #@1984
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1987
    .line 1824
    invoke-virtual/range {p0 .. p0}, Landroid/app/ActivityManagerNative;->bootAniEnd()V

    #@198a
    .line 1825
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@198d
    .line 1826
    const/4 v5, 0x1

    #@198e
    goto/16 :goto_7

    #@1990
    .line 1832
    :pswitch_1990
    const-string v5, "android.app.IActivityManager"

    #@1992
    move-object/from16 v0, p2

    #@1994
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1997
    .line 1833
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@199a
    move-result v50

    #@199b
    .line 1834
    .restart local v50       #id:I
    move-object/from16 v0, p0

    #@199d
    move/from16 v1, v50

    #@199f
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getTaskScreenShot(I)Landroid/app/ActivityManager$TaskThumbnails;

    #@19a2
    move-result-object v118

    #@19a3
    .line 1835
    .local v118, bm:Landroid/app/ActivityManager$TaskThumbnails;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@19a6
    .line 1836
    if-eqz v118, :cond_19b9

    #@19a8
    .line 1837
    const/4 v5, 0x1

    #@19a9
    move-object/from16 v0, p3

    #@19ab
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@19ae
    .line 1838
    const/4 v5, 0x0

    #@19af
    move-object/from16 v0, v118

    #@19b1
    move-object/from16 v1, p3

    #@19b3
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$TaskThumbnails;->writeToParcel(Landroid/os/Parcel;I)V

    #@19b6
    .line 1842
    :goto_19b6
    const/4 v5, 0x1

    #@19b7
    goto/16 :goto_7

    #@19b9
    .line 1840
    :cond_19b9
    const/4 v5, 0x0

    #@19ba
    move-object/from16 v0, p3

    #@19bc
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@19bf
    goto :goto_19b6

    #@19c0
    .line 1847
    .end local v50           #id:I
    .end local v118           #bm:Landroid/app/ActivityManager$TaskThumbnails;
    :pswitch_19c0
    const-string v5, "android.app.IActivityManager"

    #@19c2
    move-object/from16 v0, p2

    #@19c4
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c7
    .line 1849
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19ca
    move-result v5

    #@19cb
    if-eqz v5, :cond_19e5

    #@19cd
    const/16 v115, 0x1

    #@19cf
    .line 1850
    .local v115, bRemoveTop:Z
    :goto_19cf
    move-object/from16 v0, p0

    #@19d1
    move/from16 v1, v115

    #@19d3
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->exitSplitWindow(Z)Z

    #@19d6
    move-result v174

    #@19d7
    .line 1851
    .local v174, res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@19da
    .line 1852
    if-eqz v174, :cond_19e8

    #@19dc
    const/4 v5, 0x1

    #@19dd
    :goto_19dd
    move-object/from16 v0, p3

    #@19df
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@19e2
    .line 1853
    const/4 v5, 0x1

    #@19e3
    goto/16 :goto_7

    #@19e5
    .line 1849
    .end local v115           #bRemoveTop:Z
    .end local v174           #res:Z
    :cond_19e5
    const/16 v115, 0x0

    #@19e7
    goto :goto_19cf

    #@19e8
    .line 1852
    .restart local v115       #bRemoveTop:Z
    .restart local v174       #res:Z
    :cond_19e8
    const/4 v5, 0x0

    #@19e9
    goto :goto_19dd

    #@19ea
    .line 1858
    .end local v115           #bRemoveTop:Z
    .end local v174           #res:Z
    :pswitch_19ea
    const-string v5, "android.app.IActivityManager"

    #@19ec
    move-object/from16 v0, p2

    #@19ee
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19f1
    .line 1859
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19f4
    move-result v180

    #@19f5
    .line 1860
    .local v180, screenZone:I
    move-object/from16 v0, p0

    #@19f7
    move/from16 v1, v180

    #@19f9
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getTopRunningTaskInfo(I)Landroid/app/ActivityManager$RunningTaskInfo;

    #@19fc
    move-result-object v178

    #@19fd
    .line 1861
    .local v178, runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a00
    .line 1862
    const/4 v5, 0x0

    #@1a01
    move-object/from16 v0, v178

    #@1a03
    move-object/from16 v1, p3

    #@1a05
    invoke-virtual {v0, v1, v5}, Landroid/app/ActivityManager$RunningTaskInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a08
    .line 1863
    const/4 v5, 0x1

    #@1a09
    goto/16 :goto_7

    #@1a0b
    .line 1868
    .end local v178           #runningTaskInfo:Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v180           #screenZone:I
    :pswitch_1a0b
    const-string v5, "android.app.IActivityManager"

    #@1a0d
    move-object/from16 v0, p2

    #@1a0f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a12
    .line 1869
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1a15
    move-result-object v49

    #@1a16
    .line 1870
    .restart local v49       #token:Landroid/os/IBinder;
    move-object/from16 v0, p0

    #@1a18
    move-object/from16 v1, v49

    #@1a1a
    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerNative;->getScreenId(Landroid/os/IBinder;)I

    #@1a1d
    move-result v179

    #@1a1e
    .line 1871
    .local v179, screenId:I
    move-object/from16 v0, p3

    #@1a20
    move/from16 v1, v179

    #@1a22
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1a25
    .line 1872
    const/4 v5, 0x1

    #@1a26
    goto/16 :goto_7

    #@1a28
    .line 1876
    .end local v49           #token:Landroid/os/IBinder;
    .end local v179           #screenId:I
    :pswitch_1a28
    const-string v5, "android.app.IActivityManager"

    #@1a2a
    move-object/from16 v0, p2

    #@1a2c
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a2f
    .line 1877
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1a32
    move-result v179

    #@1a33
    .line 1878
    .restart local v179       #screenId:I
    sget-object v5, Landroid/content/ClipData;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a35
    move-object/from16 v0, p2

    #@1a37
    invoke-interface {v5, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a3a
    move-result-object v122

    #@1a3b
    check-cast v122, Landroid/content/ClipData;

    #@1a3d
    .line 1879
    .local v122, clip:Landroid/content/ClipData;
    move-object/from16 v0, p0

    #@1a3f
    move/from16 v1, v179

    #@1a41
    move-object/from16 v2, v122

    #@1a43
    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManagerNative;->callOnActivityResultToDroppedActivity(ILandroid/content/ClipData;)Z

    #@1a46
    move-result v174

    #@1a47
    .line 1880
    .restart local v174       #res:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a4a
    .line 1881
    if-eqz v174, :cond_1a55

    #@1a4c
    const/4 v5, 0x1

    #@1a4d
    :goto_1a4d
    move-object/from16 v0, p3

    #@1a4f
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@1a52
    .line 1882
    const/4 v5, 0x1

    #@1a53
    goto/16 :goto_7

    #@1a55
    .line 1881
    :cond_1a55
    const/4 v5, 0x0

    #@1a56
    goto :goto_1a4d

    #@1a57
    .line 118
    nop

    #@1a58
    :pswitch_data_1a58
    .packed-switch 0x1
        :pswitch_105c
        :pswitch_1086
        :pswitch_8
        :pswitch_f06
        :pswitch_f16
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_263
        :pswitch_306
        :pswitch_361
        :pswitch_382
        :pswitch_3f8
        :pswitch_426
        :pswitch_455
        :pswitch_471
        :pswitch_4c4
        :pswitch_4dc
        :pswitch_54d
        :pswitch_572
        :pswitch_592
        :pswitch_70d
        :pswitch_73e
        :pswitch_786
        :pswitch_79e
        :pswitch_7d0
        :pswitch_80b
        :pswitch_892
        :pswitch_8b6
        :pswitch_2a0
        :pswitch_940
        :pswitch_966
        :pswitch_99e
        :pswitch_a40
        :pswitch_a8c
        :pswitch_ab3
        :pswitch_4ac
        :pswitch_f4a
        :pswitch_f5a
        :pswitch_f6a
        :pswitch_f9c
        :pswitch_b34
        :pswitch_b75
        :pswitch_b9b
        :pswitch_bb4
        :pswitch_9d6
        :pswitch_c10
        :pswitch_c30
        :pswitch_d6e
        :pswitch_d86
        :pswitch_dc9
        :pswitch_df5
        :pswitch_e60
        :pswitch_e92
        :pswitch_fbb
        :pswitch_ebe
        :pswitch_1100
        :pswitch_5ed
        :pswitch_b0a
        :pswitch_535
        :pswitch_c4e
        :pswitch_cc4
        :pswitch_ce0
        :pswitch_fd7
        :pswitch_223
        :pswitch_fe7
        :pswitch_8fd
        :pswitch_bd2
        :pswitch_bf0
        :pswitch_adb
        :pswitch_d9e
        :pswitch_a05
        :pswitch_756
        :pswitch_ee5
        :pswitch_6c5
        :pswitch_e2d
        :pswitch_1146
        :pswitch_1003
        :pswitch_679
        :pswitch_619
        :pswitch_6dd
        :pswitch_1185
        :pswitch_1226
        :pswitch_119e
        :pswitch_11e5
        :pswitch_1206
        :pswitch_1216
        :pswitch_1250
        :pswitch_127f
        :pswitch_129d
        :pswitch_d04
        :pswitch_d28
        :pswitch_649
        :pswitch_12bb
        :pswitch_12d9
        :pswitch_12f1
        :pswitch_1312
        :pswitch_1ac
        :pswitch_1330
        :pswitch_10a7
        :pswitch_1118
        :pswitch_135a
        :pswitch_d1
        :pswitch_2e3
        :pswitch_139
        :pswitch_6f5
        :pswitch_1375
        :pswitch_10d9
        :pswitch_1385
        :pswitch_13a8
        :pswitch_13d1
        :pswitch_13ec
        :pswitch_1416
        :pswitch_1442
        :pswitch_1462
        :pswitch_148e
        :pswitch_14bd
        :pswitch_14f5
        :pswitch_153e
        :pswitch_166c
        :pswitch_51d
        :pswitch_1591
        :pswitch_15a9
        :pswitch_15c8
        :pswitch_15e8
        :pswitch_1738
        :pswitch_175b
        :pswitch_1606
        :pswitch_16b4
        :pswitch_16dd
        :pswitch_1706
        :pswitch_171f
        :pswitch_1780
        :pswitch_1800
        :pswitch_181e
        :pswitch_183e
        :pswitch_1869
        :pswitch_1136
        :pswitch_856
        :pswitch_922
        :pswitch_1164
        :pswitch_1039
        :pswitch_1653
        :pswitch_1879
        :pswitch_18a2
        :pswitch_3
        :pswitch_2c0
        :pswitch_18eb
        :pswitch_8e5
        :pswitch_17a7
        :pswitch_6a
        :pswitch_1629
        :pswitch_190b
        :pswitch_1927
        :pswitch_169c
        :pswitch_1943
        :pswitch_1953
        :pswitch_3
        :pswitch_17ce
        :pswitch_1980
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1990
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_19c0
        :pswitch_19ea
        :pswitch_1a0b
        :pswitch_1a28
    .end packed-switch
.end method
