.class final Landroid/app/Instrumentation$SyncRunnable;
.super Ljava/lang/Object;
.source "Instrumentation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Instrumentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SyncRunnable"
.end annotation


# instance fields
.field private mComplete:Z

.field private final mTarget:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 1674
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1675
    iput-object p1, p0, Landroid/app/Instrumentation$SyncRunnable;->mTarget:Ljava/lang/Runnable;

    #@5
    .line 1676
    return-void
.end method


# virtual methods
.method public run()V
    .registers 2

    #@0
    .prologue
    .line 1679
    iget-object v0, p0, Landroid/app/Instrumentation$SyncRunnable;->mTarget:Ljava/lang/Runnable;

    #@2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@5
    .line 1680
    monitor-enter p0

    #@6
    .line 1681
    const/4 v0, 0x1

    #@7
    :try_start_7
    iput-boolean v0, p0, Landroid/app/Instrumentation$SyncRunnable;->mComplete:Z

    #@9
    .line 1682
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@c
    .line 1683
    monitor-exit p0

    #@d
    .line 1684
    return-void

    #@e
    .line 1683
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public waitForComplete()V
    .registers 2

    #@0
    .prologue
    .line 1687
    monitor-enter p0

    #@1
    .line 1688
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Landroid/app/Instrumentation$SyncRunnable;->mComplete:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    #@3
    if-nez v0, :cond_b

    #@5
    .line 1690
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 1691
    :catch_9
    move-exception v0

    #@a
    goto :goto_1

    #@b
    .line 1694
    :cond_b
    :try_start_b
    monitor-exit p0

    #@c
    .line 1695
    return-void

    #@d
    .line 1694
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_b .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method
