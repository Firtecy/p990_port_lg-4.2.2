.class public final Landroid/app/PendingIntent;
.super Ljava/lang/Object;
.source "PendingIntent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/PendingIntent$FinishedDispatcher;,
        Landroid/app/PendingIntent$OnFinished;,
        Landroid/app/PendingIntent$CanceledException;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_CANCEL_CURRENT:I = 0x10000000

.field public static final FLAG_NO_CREATE:I = 0x20000000

.field public static final FLAG_ONE_SHOT:I = 0x40000000

.field public static final FLAG_UPDATE_CURRENT:I = 0x8000000


# instance fields
.field private final mTarget:Landroid/content/IIntentSender;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 858
    new-instance v0, Landroid/app/PendingIntent$1;

    #@2
    invoke-direct {v0}, Landroid/app/PendingIntent$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method constructor <init>(Landroid/content/IIntentSender;)V
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 899
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 900
    iput-object p1, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@5
    .line 901
    return-void
.end method

.method constructor <init>(Landroid/os/IBinder;)V
    .registers 3
    .parameter "target"

    #@0
    .prologue
    .line 903
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 904
    invoke-static {p1}, Landroid/content/IIntentSender$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/IIntentSender;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@9
    .line 905
    return-void
.end method

.method public static getActivities(Landroid/content/Context;I[Landroid/content/Intent;I)Landroid/app/PendingIntent;
    .registers 5
    .parameter "context"
    .parameter "requestCode"
    .parameter "intents"
    .parameter "flags"

    #@0
    .prologue
    .line 335
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Landroid/app/PendingIntent;->getActivities(Landroid/content/Context;I[Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/PendingIntent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getActivities(Landroid/content/Context;I[Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/PendingIntent;
    .registers 18
    .parameter "context"
    .parameter "requestCode"
    .parameter "intents"
    .parameter "flags"
    .parameter "options"

    #@0
    .prologue
    .line 381
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 382
    .local v2, packageName:Ljava/lang/String;
    if-nez p2, :cond_8

    #@6
    .line 383
    const/4 v0, 0x0

    #@7
    .line 399
    :goto_7
    return-object v0

    #@8
    .line 385
    :cond_8
    array-length v0, p2

    #@9
    new-array v7, v0, [Ljava/lang/String;

    #@b
    .line 386
    .local v7, resolvedTypes:[Ljava/lang/String;
    const/4 v11, 0x0

    #@c
    .local v11, i:I
    :goto_c
    array-length v0, p2

    #@d
    if-ge v11, v0, :cond_24

    #@f
    .line 387
    aget-object v0, p2, v11

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@15
    .line 388
    aget-object v0, p2, v11

    #@17
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    aput-object v0, v7, v11

    #@21
    .line 386
    add-int/lit8 v11, v11, 0x1

    #@23
    goto :goto_c

    #@24
    .line 391
    :cond_24
    :try_start_24
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@27
    move-result-object v0

    #@28
    const/4 v1, 0x2

    #@29
    const/4 v3, 0x0

    #@2a
    const/4 v4, 0x0

    #@2b
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@2e
    move-result v10

    #@2f
    move v5, p1

    #@30
    move-object v6, p2

    #@31
    move/from16 v8, p3

    #@33
    move-object/from16 v9, p4

    #@35
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@38
    move-result-object v12

    #@39
    .line 396
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_44

    #@3b
    new-instance v0, Landroid/app/PendingIntent;

    #@3d
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_40} :catch_41

    #@40
    goto :goto_7

    #@41
    .line 397
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_41
    move-exception v0

    #@42
    .line 399
    const/4 v0, 0x0

    #@43
    goto :goto_7

    #@44
    .line 396
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_44
    const/4 v0, 0x0

    #@45
    goto :goto_7
.end method

.method public static getActivitiesAsUser(Landroid/content/Context;I[Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;
    .registers 19
    .parameter "context"
    .parameter "requestCode"
    .parameter "intents"
    .parameter "flags"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 409
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 410
    .local v2, packageName:Ljava/lang/String;
    if-nez p2, :cond_8

    #@6
    .line 411
    const/4 v0, 0x0

    #@7
    .line 427
    :goto_7
    return-object v0

    #@8
    .line 413
    :cond_8
    array-length v0, p2

    #@9
    new-array v7, v0, [Ljava/lang/String;

    #@b
    .line 414
    .local v7, resolvedTypes:[Ljava/lang/String;
    const/4 v11, 0x0

    #@c
    .local v11, i:I
    :goto_c
    array-length v0, p2

    #@d
    if-ge v11, v0, :cond_24

    #@f
    .line 415
    aget-object v0, p2, v11

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAllowFds(Z)V

    #@15
    .line 416
    aget-object v0, p2, v11

    #@17
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    aput-object v0, v7, v11

    #@21
    .line 414
    add-int/lit8 v11, v11, 0x1

    #@23
    goto :goto_c

    #@24
    .line 419
    :cond_24
    :try_start_24
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@27
    move-result-object v0

    #@28
    const/4 v1, 0x2

    #@29
    const/4 v3, 0x0

    #@2a
    const/4 v4, 0x0

    #@2b
    invoke-virtual/range {p5 .. p5}, Landroid/os/UserHandle;->getIdentifier()I

    #@2e
    move-result v10

    #@2f
    move v5, p1

    #@30
    move-object v6, p2

    #@31
    move/from16 v8, p3

    #@33
    move-object/from16 v9, p4

    #@35
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@38
    move-result-object v12

    #@39
    .line 424
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_44

    #@3b
    new-instance v0, Landroid/app/PendingIntent;

    #@3d
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_40} :catch_41

    #@40
    goto :goto_7

    #@41
    .line 425
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_41
    move-exception v0

    #@42
    .line 427
    const/4 v0, 0x0

    #@43
    goto :goto_7

    #@44
    .line 424
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_44
    const/4 v0, 0x0

    #@45
    goto :goto_7
.end method

.method public static getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .registers 5
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"

    #@0
    .prologue
    .line 220
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/PendingIntent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getActivity(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/PendingIntent;
    .registers 18
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"
    .parameter "options"

    #@0
    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 250
    .local v2, packageName:Ljava/lang/String;
    if-eqz p2, :cond_3c

    #@6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p2, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@d
    move-result-object v11

    #@e
    .line 253
    .local v11, resolvedType:Ljava/lang/String;
    :goto_e
    const/4 v0, 0x0

    #@f
    :try_start_f
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@12
    .line 254
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x2

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x1

    #@1a
    new-array v6, v5, [Landroid/content/Intent;

    #@1c
    const/4 v5, 0x0

    #@1d
    aput-object p2, v6, v5

    #@1f
    if-eqz v11, :cond_3e

    #@21
    const/4 v5, 0x1

    #@22
    new-array v7, v5, [Ljava/lang/String;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v11, v7, v5

    #@27
    :goto_27
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@2a
    move-result v10

    #@2b
    move v5, p1

    #@2c
    move/from16 v8, p3

    #@2e
    move-object/from16 v9, p4

    #@30
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@33
    move-result-object v12

    #@34
    .line 260
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_40

    #@36
    new-instance v0, Landroid/app/PendingIntent;

    #@38
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_3b} :catch_45
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_3b} :catch_42

    #@3b
    .line 264
    .end local v12           #target:Landroid/content/IIntentSender;
    :goto_3b
    return-object v0

    #@3c
    .line 250
    .end local v11           #resolvedType:Ljava/lang/String;
    :cond_3c
    const/4 v11, 0x0

    #@3d
    goto :goto_e

    #@3e
    .line 254
    .restart local v11       #resolvedType:Ljava/lang/String;
    :cond_3e
    const/4 v7, 0x0

    #@3f
    goto :goto_27

    #@40
    .line 260
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_3b

    #@42
    .line 262
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_42
    move-exception v0

    #@43
    .line 264
    :goto_43
    const/4 v0, 0x0

    #@44
    goto :goto_3b

    #@45
    .line 261
    :catch_45
    move-exception v0

    #@46
    goto :goto_43
.end method

.method public static getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;
    .registers 19
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"
    .parameter "options"
    .parameter "user"

    #@0
    .prologue
    .line 274
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 275
    .local v2, packageName:Ljava/lang/String;
    if-eqz p2, :cond_3c

    #@6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p2, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@d
    move-result-object v11

    #@e
    .line 278
    .local v11, resolvedType:Ljava/lang/String;
    :goto_e
    const/4 v0, 0x0

    #@f
    :try_start_f
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@12
    .line 279
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x2

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x1

    #@1a
    new-array v6, v5, [Landroid/content/Intent;

    #@1c
    const/4 v5, 0x0

    #@1d
    aput-object p2, v6, v5

    #@1f
    if-eqz v11, :cond_3e

    #@21
    const/4 v5, 0x1

    #@22
    new-array v7, v5, [Ljava/lang/String;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v11, v7, v5

    #@27
    :goto_27
    invoke-virtual/range {p5 .. p5}, Landroid/os/UserHandle;->getIdentifier()I

    #@2a
    move-result v10

    #@2b
    move v5, p1

    #@2c
    move/from16 v8, p3

    #@2e
    move-object/from16 v9, p4

    #@30
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@33
    move-result-object v12

    #@34
    .line 285
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_40

    #@36
    new-instance v0, Landroid/app/PendingIntent;

    #@38
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_3b} :catch_45
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_3b} :catch_42

    #@3b
    .line 289
    .end local v12           #target:Landroid/content/IIntentSender;
    :goto_3b
    return-object v0

    #@3c
    .line 275
    .end local v11           #resolvedType:Ljava/lang/String;
    :cond_3c
    const/4 v11, 0x0

    #@3d
    goto :goto_e

    #@3e
    .line 279
    .restart local v11       #resolvedType:Ljava/lang/String;
    :cond_3e
    const/4 v7, 0x0

    #@3f
    goto :goto_27

    #@40
    .line 285
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_40
    const/4 v0, 0x0

    #@41
    goto :goto_3b

    #@42
    .line 287
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_42
    move-exception v0

    #@43
    .line 289
    :goto_43
    const/4 v0, 0x0

    #@44
    goto :goto_3b

    #@45
    .line 286
    :catch_45
    move-exception v0

    #@46
    goto :goto_43
.end method

.method public static getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .registers 6
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"

    #@0
    .prologue
    .line 451
    new-instance v0, Landroid/os/UserHandle;

    #@2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@9
    invoke-static {p0, p1, p2, p3, v0}, Landroid/app/PendingIntent;->getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public static getBroadcastAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/UserHandle;)Landroid/app/PendingIntent;
    .registers 18
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"
    .parameter "userHandle"

    #@0
    .prologue
    .line 462
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 463
    .local v2, packageName:Ljava/lang/String;
    if-eqz p2, :cond_3b

    #@6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p2, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@d
    move-result-object v11

    #@e
    .line 466
    .local v11, resolvedType:Ljava/lang/String;
    :goto_e
    const/4 v0, 0x0

    #@f
    :try_start_f
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@12
    .line 467
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x1

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x1

    #@1a
    new-array v6, v5, [Landroid/content/Intent;

    #@1c
    const/4 v5, 0x0

    #@1d
    aput-object p2, v6, v5

    #@1f
    if-eqz v11, :cond_3d

    #@21
    const/4 v5, 0x1

    #@22
    new-array v7, v5, [Ljava/lang/String;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v11, v7, v5

    #@27
    :goto_27
    const/4 v9, 0x0

    #@28
    invoke-virtual/range {p4 .. p4}, Landroid/os/UserHandle;->getIdentifier()I

    #@2b
    move-result v10

    #@2c
    move v5, p1

    #@2d
    move/from16 v8, p3

    #@2f
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@32
    move-result-object v12

    #@33
    .line 473
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_3f

    #@35
    new-instance v0, Landroid/app/PendingIntent;

    #@37
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_3a} :catch_44
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_3a} :catch_41

    #@3a
    .line 477
    .end local v12           #target:Landroid/content/IIntentSender;
    :goto_3a
    return-object v0

    #@3b
    .line 463
    .end local v11           #resolvedType:Ljava/lang/String;
    :cond_3b
    const/4 v11, 0x0

    #@3c
    goto :goto_e

    #@3d
    .line 467
    .restart local v11       #resolvedType:Ljava/lang/String;
    :cond_3d
    const/4 v7, 0x0

    #@3e
    goto :goto_27

    #@3f
    .line 473
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_3a

    #@41
    .line 475
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_41
    move-exception v0

    #@42
    .line 477
    :goto_42
    const/4 v0, 0x0

    #@43
    goto :goto_3a

    #@44
    .line 474
    :catch_44
    move-exception v0

    #@45
    goto :goto_42
.end method

.method public static getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    .registers 17
    .parameter "context"
    .parameter "requestCode"
    .parameter "intent"
    .parameter "flags"

    #@0
    .prologue
    .line 502
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    .line 503
    .local v2, packageName:Ljava/lang/String;
    if-eqz p2, :cond_3b

    #@6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {p2, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@d
    move-result-object v11

    #@e
    .line 506
    .local v11, resolvedType:Ljava/lang/String;
    :goto_e
    const/4 v0, 0x0

    #@f
    :try_start_f
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAllowFds(Z)V

    #@12
    .line 507
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x4

    #@17
    const/4 v3, 0x0

    #@18
    const/4 v4, 0x0

    #@19
    const/4 v5, 0x1

    #@1a
    new-array v6, v5, [Landroid/content/Intent;

    #@1c
    const/4 v5, 0x0

    #@1d
    aput-object p2, v6, v5

    #@1f
    if-eqz v11, :cond_3d

    #@21
    const/4 v5, 0x1

    #@22
    new-array v7, v5, [Ljava/lang/String;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v11, v7, v5

    #@27
    :goto_27
    const/4 v9, 0x0

    #@28
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@2b
    move-result v10

    #@2c
    move v5, p1

    #@2d
    move/from16 v8, p3

    #@2f
    invoke-interface/range {v0 .. v10}, Landroid/app/IActivityManager;->getIntentSender(ILjava/lang/String;Landroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;I)Landroid/content/IIntentSender;

    #@32
    move-result-object v12

    #@33
    .line 513
    .local v12, target:Landroid/content/IIntentSender;
    if-eqz v12, :cond_3f

    #@35
    new-instance v0, Landroid/app/PendingIntent;

    #@37
    invoke-direct {v0, v12}, Landroid/app/PendingIntent;-><init>(Landroid/content/IIntentSender;)V
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_3a} :catch_44
    .catch Ljava/lang/NullPointerException; {:try_start_f .. :try_end_3a} :catch_41

    #@3a
    .line 517
    .end local v12           #target:Landroid/content/IIntentSender;
    :goto_3a
    return-object v0

    #@3b
    .line 503
    .end local v11           #resolvedType:Ljava/lang/String;
    :cond_3b
    const/4 v11, 0x0

    #@3c
    goto :goto_e

    #@3d
    .line 507
    .restart local v11       #resolvedType:Ljava/lang/String;
    :cond_3d
    const/4 v7, 0x0

    #@3e
    goto :goto_27

    #@3f
    .line 513
    .restart local v12       #target:Landroid/content/IIntentSender;
    :cond_3f
    const/4 v0, 0x0

    #@40
    goto :goto_3a

    #@41
    .line 515
    .end local v12           #target:Landroid/content/IIntentSender;
    :catch_41
    move-exception v0

    #@42
    .line 517
    :goto_42
    const/4 v0, 0x0

    #@43
    goto :goto_3a

    #@44
    .line 514
    :catch_44
    move-exception v0

    #@45
    goto :goto_42
.end method

.method public static readPendingIntentOrNullFromParcel(Landroid/os/Parcel;)Landroid/app/PendingIntent;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 895
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@3
    move-result-object v0

    #@4
    .line 896
    .local v0, b:Landroid/os/IBinder;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/app/PendingIntent;

    #@8
    invoke-direct {v1, v0}, Landroid/app/PendingIntent;-><init>(Landroid/os/IBinder;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public static writePendingIntentOrNullToParcel(Landroid/app/PendingIntent;Landroid/os/Parcel;)V
    .registers 3
    .parameter "sender"
    .parameter "out"

    #@0
    .prologue
    .line 880
    if-eqz p0, :cond_c

    #@2
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@4
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v0

    #@8
    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@b
    .line 882
    return-void

    #@c
    .line 880
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_8
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 536
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->cancelIntentSender(Landroid/content/IIntentSender;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 539
    :goto_9
    return-void

    #@a
    .line 537
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 851
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "otherObj"

    #@0
    .prologue
    .line 827
    instance-of v0, p1, Landroid/app/PendingIntent;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 828
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    check-cast p1, Landroid/app/PendingIntent;

    #@c
    .end local p1
    iget-object v1, p1, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@e
    invoke-interface {v1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    .line 831
    :goto_16
    return v0

    #@17
    .restart local p1
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public getCreatorPackage()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 725
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 729
    :goto_a
    return-object v1

    #@b
    .line 727
    :catch_b
    move-exception v0

    #@c
    .line 729
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public getCreatorUid()I
    .registers 4

    #@0
    .prologue
    .line 744
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getUidForIntentSender(Landroid/content/IIntentSender;)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 748
    :goto_a
    return v1

    #@b
    .line 746
    :catch_b
    move-exception v0

    #@c
    .line 748
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, -0x1

    #@d
    goto :goto_a
.end method

.method public getCreatorUserHandle()Landroid/os/UserHandle;
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 765
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v2

    #@5
    iget-object v4, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@7
    invoke-interface {v2, v4}, Landroid/app/IActivityManager;->getUidForIntentSender(Landroid/content/IIntentSender;)I

    #@a
    move-result v1

    #@b
    .line 767
    .local v1, uid:I
    if-lez v1, :cond_17

    #@d
    new-instance v2, Landroid/os/UserHandle;

    #@f
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@12
    move-result v4

    #@13
    invoke-direct {v2, v4}, Landroid/os/UserHandle;-><init>(I)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_16} :catch_19

    #@16
    .line 770
    .end local v1           #uid:I
    :goto_16
    return-object v2

    #@17
    .restart local v1       #uid:I
    :cond_17
    move-object v2, v3

    #@18
    .line 767
    goto :goto_16

    #@19
    .line 768
    .end local v1           #uid:I
    :catch_19
    move-exception v0

    #@1a
    .local v0, e:Landroid/os/RemoteException;
    move-object v2, v3

    #@1b
    .line 770
    goto :goto_16
.end method

.method public getIntent()Landroid/content/Intent;
    .registers 4

    #@0
    .prologue
    .line 808
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getIntentForIntentSender(Landroid/content/IIntentSender;)Landroid/content/Intent;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 812
    :goto_a
    return-object v1

    #@b
    .line 810
    :catch_b
    move-exception v0

    #@c
    .line 812
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public getIntentSender()Landroid/content/IntentSender;
    .registers 3

    #@0
    .prologue
    .line 527
    new-instance v0, Landroid/content/IntentSender;

    #@2
    iget-object v1, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@4
    invoke-direct {v0, v1}, Landroid/content/IntentSender;-><init>(Landroid/content/IIntentSender;)V

    #@7
    return-object v0
.end method

.method public getTarget()Landroid/content/IIntentSender;
    .registers 2

    #@0
    .prologue
    .line 909
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@2
    return-object v0
.end method

.method public getTargetPackage()Ljava/lang/String;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 706
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->getPackageForIntentSender(Landroid/content/IIntentSender;)Ljava/lang/String;
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result-object v1

    #@a
    .line 710
    :goto_a
    return-object v1

    #@b
    .line 708
    :catch_b
    move-exception v0

    #@c
    .line 710
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 836
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@2
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public isActivity()Z
    .registers 4

    #@0
    .prologue
    .line 794
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->isIntentSenderAnActivity(Landroid/content/IIntentSender;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 798
    :goto_a
    return v1

    #@b
    .line 796
    :catch_b
    move-exception v0

    #@c
    .line 798
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public isTargetedToPackage()Z
    .registers 4

    #@0
    .prologue
    .line 780
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@6
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->isIntentSenderTargetedToPackage(Landroid/content/IIntentSender;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 784
    :goto_a
    return v1

    #@b
    .line 782
    :catch_b
    move-exception v0

    #@c
    .line 784
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public send()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 550
    const/4 v2, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v3, v1

    #@4
    move-object v4, v1

    #@5
    move-object v5, v1

    #@6
    move-object v6, v1

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 551
    return-void
.end method

.method public send(I)V
    .registers 9
    .parameter "code"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 564
    move-object v0, p0

    #@2
    move v2, p1

    #@3
    move-object v3, v1

    #@4
    move-object v4, v1

    #@5
    move-object v5, v1

    #@6
    move-object v6, v1

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 565
    return-void
.end method

.method public send(ILandroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    .registers 11
    .parameter "code"
    .parameter "onFinished"
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 605
    move-object v0, p0

    #@2
    move v2, p1

    #@3
    move-object v3, v1

    #@4
    move-object v4, p2

    #@5
    move-object v5, p3

    #@6
    move-object v6, v1

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 606
    return-void
.end method

.method public send(Landroid/content/Context;ILandroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "code"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 584
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v5, v4

    #@6
    move-object v6, v4

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 585
    return-void
.end method

.method public send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    .registers 13
    .parameter "context"
    .parameter "code"
    .parameter "intent"
    .parameter "onFinished"
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    .line 641
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move-object v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V

    #@a
    .line 642
    return-void
.end method

.method public send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "code"
    .parameter "intent"
    .parameter "onFinished"
    .parameter "handler"
    .parameter "requiredPermission"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/app/PendingIntent$CanceledException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 684
    if-eqz p3, :cond_2a

    #@3
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {p3, v0}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 687
    .local v3, resolvedType:Ljava/lang/String;
    :goto_b
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@d
    if-eqz p4, :cond_14

    #@f
    new-instance v4, Landroid/app/PendingIntent$FinishedDispatcher;

    #@11
    invoke-direct {v4, p0, p4, p5}, Landroid/app/PendingIntent$FinishedDispatcher;-><init>(Landroid/app/PendingIntent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V

    #@14
    :cond_14
    move v1, p2

    #@15
    move-object v2, p3

    #@16
    move-object v5, p6

    #@17
    invoke-interface/range {v0 .. v5}, Landroid/content/IIntentSender;->send(ILandroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;Ljava/lang/String;)I

    #@1a
    move-result v7

    #@1b
    .line 692
    .local v7, res:I
    if-gez v7, :cond_2c

    #@1d
    .line 693
    new-instance v0, Landroid/app/PendingIntent$CanceledException;

    #@1f
    invoke-direct {v0}, Landroid/app/PendingIntent$CanceledException;-><init>()V

    #@22
    throw v0
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_23} :catch_23

    #@23
    .line 695
    .end local v3           #resolvedType:Ljava/lang/String;
    .end local v7           #res:I
    :catch_23
    move-exception v6

    #@24
    .line 696
    .local v6, e:Landroid/os/RemoteException;
    new-instance v0, Landroid/app/PendingIntent$CanceledException;

    #@26
    invoke-direct {v0, v6}, Landroid/app/PendingIntent$CanceledException;-><init>(Ljava/lang/Exception;)V

    #@29
    throw v0

    #@2a
    .end local v6           #e:Landroid/os/RemoteException;
    :cond_2a
    move-object v3, v4

    #@2b
    .line 684
    goto :goto_b

    #@2c
    .line 698
    .restart local v3       #resolvedType:Ljava/lang/String;
    .restart local v7       #res:I
    :cond_2c
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 841
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 842
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "PendingIntent{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 843
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@f
    move-result v1

    #@10
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 844
    const-string v1, ": "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 845
    iget-object v1, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@1e
    if-eqz v1, :cond_33

    #@20
    iget-object v1, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@22
    invoke-interface {v1}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@25
    move-result-object v1

    #@26
    :goto_26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    .line 846
    const/16 v1, 0x7d

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2e
    .line 847
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    return-object v1

    #@33
    .line 845
    :cond_33
    const/4 v1, 0x0

    #@34
    goto :goto_26
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 855
    iget-object v0, p0, Landroid/app/PendingIntent;->mTarget:Landroid/content/IIntentSender;

    #@2
    invoke-interface {v0}, Landroid/content/IIntentSender;->asBinder()Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@9
    .line 856
    return-void
.end method
