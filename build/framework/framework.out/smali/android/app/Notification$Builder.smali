.class public Landroid/app/Notification$Builder;
.super Ljava/lang/Object;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static final MAX_ACTION_BUTTONS:I = 0x3


# instance fields
.field private mActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Notification$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioStreamType:I

.field private mContentInfo:Ljava/lang/CharSequence;

.field private mContentIntent:Landroid/app/PendingIntent;

.field private mContentText:Ljava/lang/CharSequence;

.field private mContentTitle:Ljava/lang/CharSequence;

.field private mContentView:Landroid/widget/RemoteViews;

.field private mContext:Landroid/content/Context;

.field private mDefaults:I

.field private mDeleteIntent:Landroid/app/PendingIntent;

.field private mExtras:Landroid/os/Bundle;

.field private mFlags:I

.field private mFullScreenIntent:Landroid/app/PendingIntent;

.field private mKindList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLargeIcon:Landroid/graphics/Bitmap;

.field private mLedArgb:I

.field private mLedOffMs:I

.field private mLedOnMs:I

.field private mNumber:I

.field private mPriority:I

.field private mProgress:I

.field private mProgressIndeterminate:Z

.field private mProgressMax:I

.field private mShowWhen:Z

.field private mSmallIcon:I

.field private mSmallIconLevel:I

.field private mSound:Landroid/net/Uri;

.field private mStyle:Landroid/app/Notification$Style;

.field private mSubText:Ljava/lang/CharSequence;

.field private mTickerText:Ljava/lang/CharSequence;

.field private mTickerView:Landroid/widget/RemoteViews;

.field private mUseChronometer:Z

.field private mVibrate:[J

.field private mWhen:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1024
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 997
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@9
    iput-object v0, p0, Landroid/app/Notification$Builder;->mKindList:Ljava/util/ArrayList;

    #@b
    .line 1000
    new-instance v0, Ljava/util/ArrayList;

    #@d
    const/4 v1, 0x3

    #@e
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@11
    iput-object v0, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@13
    .line 1003
    iput-boolean v2, p0, Landroid/app/Notification$Builder;->mShowWhen:Z

    #@15
    .line 1025
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@17
    .line 1028
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1a
    move-result-wide v0

    #@1b
    iput-wide v0, p0, Landroid/app/Notification$Builder;->mWhen:J

    #@1d
    .line 1029
    const/4 v0, -0x1

    #@1e
    iput v0, p0, Landroid/app/Notification$Builder;->mAudioStreamType:I

    #@20
    .line 1030
    const/4 v0, 0x0

    #@21
    iput v0, p0, Landroid/app/Notification$Builder;->mPriority:I

    #@23
    .line 1031
    return-void
.end method

.method static synthetic access$300(Landroid/app/Notification$Builder;I)Landroid/widget/RemoteViews;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 966
    invoke-direct {p0, p1}, Landroid/app/Notification$Builder;->applyStandardTemplateWithActions(I)Landroid/widget/RemoteViews;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Landroid/app/Notification$Builder;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 966
    iget-object v0, p0, Landroid/app/Notification$Builder;->mSubText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/app/Notification$Builder;)Landroid/app/Notification;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 966
    invoke-direct {p0}, Landroid/app/Notification$Builder;->buildUnstyled()Landroid/app/Notification;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$602(Landroid/app/Notification$Builder;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 966
    iput-object p1, p0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Landroid/app/Notification$Builder;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 966
    iget-object v0, p0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@2
    return-object v0
.end method

.method static synthetic access$702(Landroid/app/Notification$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 966
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@2
    return-object p1
.end method

.method private applyStandardTemplate(IZ)Landroid/widget/RemoteViews;
    .registers 21
    .parameter "resId"
    .parameter "fitIn1U"

    #@0
    .prologue
    .line 1452
    new-instance v1, Landroid/widget/RemoteViews;

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    move/from16 v0, p1

    #@c
    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@f
    .line 1453
    .local v1, contentView:Landroid/widget/RemoteViews;
    const/4 v10, 0x0

    #@10
    .line 1454
    .local v10, showLine3:Z
    const/4 v9, 0x0

    #@11
    .line 1455
    .local v9, showLine2:Z
    const v11, 0x1020006

    #@14
    .line 1456
    .local v11, smallIconImageViewId:I
    move-object/from16 v0, p0

    #@16
    iget-object v2, v0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@18
    if-eqz v2, :cond_27

    #@1a
    .line 1457
    const v2, 0x1020006

    #@1d
    move-object/from16 v0, p0

    #@1f
    iget-object v3, v0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@21
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    #@24
    .line 1458
    const v11, 0x102024d

    #@27
    .line 1460
    :cond_27
    move-object/from16 v0, p0

    #@29
    iget v2, v0, Landroid/app/Notification$Builder;->mPriority:I

    #@2b
    const/4 v3, -0x1

    #@2c
    if-ge v2, v3, :cond_46

    #@2e
    .line 1461
    const v2, 0x1020006

    #@31
    const-string/jumbo v3, "setBackgroundResource"

    #@34
    const v4, 0x1080624

    #@37
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@3a
    .line 1463
    const v2, 0x1020336

    #@3d
    const-string/jumbo v3, "setBackgroundResource"

    #@40
    const v4, 0x1080426

    #@43
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    #@46
    .line 1466
    :cond_46
    move-object/from16 v0, p0

    #@48
    iget v2, v0, Landroid/app/Notification$Builder;->mSmallIcon:I

    #@4a
    if-eqz v2, :cond_12f

    #@4c
    .line 1467
    move-object/from16 v0, p0

    #@4e
    iget v2, v0, Landroid/app/Notification$Builder;->mSmallIcon:I

    #@50
    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    #@53
    .line 1468
    const/4 v2, 0x0

    #@54
    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@57
    .line 1472
    :goto_57
    move-object/from16 v0, p0

    #@59
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContentTitle:Ljava/lang/CharSequence;

    #@5b
    if-eqz v2, :cond_67

    #@5d
    .line 1473
    const v2, 0x1020016

    #@60
    move-object/from16 v0, p0

    #@62
    iget-object v3, v0, Landroid/app/Notification$Builder;->mContentTitle:Ljava/lang/CharSequence;

    #@64
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@67
    .line 1475
    :cond_67
    move-object/from16 v0, p0

    #@69
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@6b
    if-eqz v2, :cond_78

    #@6d
    .line 1476
    const v2, 0x1020046

    #@70
    move-object/from16 v0, p0

    #@72
    iget-object v3, v0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@74
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@77
    .line 1477
    const/4 v10, 0x1

    #@78
    .line 1479
    :cond_78
    move-object/from16 v0, p0

    #@7a
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContentInfo:Ljava/lang/CharSequence;

    #@7c
    if-eqz v2, :cond_136

    #@7e
    .line 1480
    const v2, 0x102033a

    #@81
    move-object/from16 v0, p0

    #@83
    iget-object v3, v0, Landroid/app/Notification$Builder;->mContentInfo:Ljava/lang/CharSequence;

    #@85
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@88
    .line 1481
    const v2, 0x102033a

    #@8b
    const/4 v3, 0x0

    #@8c
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@8f
    .line 1482
    const/4 v10, 0x1

    #@90
    .line 1500
    :goto_90
    move-object/from16 v0, p0

    #@92
    iget-object v2, v0, Landroid/app/Notification$Builder;->mSubText:Ljava/lang/CharSequence;

    #@94
    if-eqz v2, :cond_198

    #@96
    .line 1501
    const v2, 0x1020046

    #@99
    move-object/from16 v0, p0

    #@9b
    iget-object v3, v0, Landroid/app/Notification$Builder;->mSubText:Ljava/lang/CharSequence;

    #@9d
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@a0
    .line 1502
    move-object/from16 v0, p0

    #@a2
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@a4
    if-eqz v2, :cond_18e

    #@a6
    .line 1503
    const v2, 0x1020015

    #@a9
    move-object/from16 v0, p0

    #@ab
    iget-object v3, v0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@ad
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@b0
    .line 1504
    const v2, 0x1020015

    #@b3
    const/4 v3, 0x0

    #@b4
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@b7
    .line 1505
    const/4 v9, 0x1

    #@b8
    .line 1520
    :goto_b8
    if-eqz v9, :cond_dd

    #@ba
    .line 1521
    if-eqz p2, :cond_d3

    #@bc
    .line 1523
    move-object/from16 v0, p0

    #@be
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@c0
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@c3
    move-result-object v8

    #@c4
    .line 1524
    .local v8, res:Landroid/content/res/Resources;
    const v2, 0x1050059

    #@c7
    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@ca
    move-result v2

    #@cb
    int-to-float v12, v2

    #@cc
    .line 1526
    .local v12, subTextSize:F
    const v2, 0x1020046

    #@cf
    const/4 v3, 0x0

    #@d0
    invoke-virtual {v1, v2, v3, v12}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    #@d3
    .line 1529
    .end local v8           #res:Landroid/content/res/Resources;
    .end local v12           #subTextSize:F
    :cond_d3
    const v2, 0x1020337

    #@d6
    const/4 v3, 0x0

    #@d7
    const/4 v4, 0x0

    #@d8
    const/4 v5, 0x0

    #@d9
    const/4 v6, 0x0

    #@da
    invoke-virtual/range {v1 .. v6}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    #@dd
    .line 1532
    :cond_dd
    move-object/from16 v0, p0

    #@df
    iget-wide v2, v0, Landroid/app/Notification$Builder;->mWhen:J

    #@e1
    const-wide/16 v4, 0x0

    #@e3
    cmp-long v2, v2, v4

    #@e5
    if-eqz v2, :cond_1e8

    #@e7
    move-object/from16 v0, p0

    #@e9
    iget-boolean v2, v0, Landroid/app/Notification$Builder;->mShowWhen:Z

    #@eb
    if-eqz v2, :cond_1e8

    #@ed
    .line 1533
    move-object/from16 v0, p0

    #@ef
    iget-boolean v2, v0, Landroid/app/Notification$Builder;->mUseChronometer:Z

    #@f1
    if-eqz v2, :cond_1d2

    #@f3
    .line 1534
    const v2, 0x1020338

    #@f6
    const/4 v3, 0x0

    #@f7
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@fa
    .line 1535
    const v2, 0x1020338

    #@fd
    const-string/jumbo v3, "setBase"

    #@100
    move-object/from16 v0, p0

    #@102
    iget-wide v4, v0, Landroid/app/Notification$Builder;->mWhen:J

    #@104
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@107
    move-result-wide v14

    #@108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10b
    move-result-wide v16

    #@10c
    sub-long v14, v14, v16

    #@10e
    add-long/2addr v4, v14

    #@10f
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    #@112
    .line 1537
    const v2, 0x1020338

    #@115
    const-string/jumbo v3, "setStarted"

    #@118
    const/4 v4, 0x1

    #@119
    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    #@11c
    .line 1546
    :goto_11c
    const v3, 0x1020339

    #@11f
    if-eqz v10, :cond_1f2

    #@121
    const/4 v2, 0x0

    #@122
    :goto_122
    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@125
    .line 1547
    const v3, 0x102033e

    #@128
    if-eqz v10, :cond_1f6

    #@12a
    const/4 v2, 0x0

    #@12b
    :goto_12b
    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@12e
    .line 1548
    return-object v1

    #@12f
    .line 1470
    :cond_12f
    const/16 v2, 0x8

    #@131
    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@134
    goto/16 :goto_57

    #@136
    .line 1483
    :cond_136
    move-object/from16 v0, p0

    #@138
    iget v2, v0, Landroid/app/Notification$Builder;->mNumber:I

    #@13a
    if-lez v2, :cond_184

    #@13c
    .line 1484
    move-object/from16 v0, p0

    #@13e
    iget-object v2, v0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@140
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@143
    move-result-object v2

    #@144
    const v3, 0x10e0003

    #@147
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    #@14a
    move-result v13

    #@14b
    .line 1486
    .local v13, tooBig:I
    move-object/from16 v0, p0

    #@14d
    iget v2, v0, Landroid/app/Notification$Builder;->mNumber:I

    #@14f
    if-le v2, v13, :cond_170

    #@151
    .line 1487
    const v2, 0x102033a

    #@154
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@158
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15b
    move-result-object v3

    #@15c
    const v4, 0x1040017

    #@15f
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@162
    move-result-object v3

    #@163
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@166
    .line 1493
    :goto_166
    const v2, 0x102033a

    #@169
    const/4 v3, 0x0

    #@16a
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@16d
    .line 1494
    const/4 v10, 0x1

    #@16e
    .line 1495
    goto/16 :goto_90

    #@170
    .line 1490
    :cond_170
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    #@173
    move-result-object v7

    #@174
    .line 1491
    .local v7, f:Ljava/text/NumberFormat;
    const v2, 0x102033a

    #@177
    move-object/from16 v0, p0

    #@179
    iget v3, v0, Landroid/app/Notification$Builder;->mNumber:I

    #@17b
    int-to-long v3, v3

    #@17c
    invoke-virtual {v7, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    #@17f
    move-result-object v3

    #@180
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@183
    goto :goto_166

    #@184
    .line 1496
    .end local v7           #f:Ljava/text/NumberFormat;
    .end local v13           #tooBig:I
    :cond_184
    const v2, 0x102033a

    #@187
    const/16 v3, 0x8

    #@189
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@18c
    goto/16 :goto_90

    #@18e
    .line 1507
    :cond_18e
    const v2, 0x1020015

    #@191
    const/16 v3, 0x8

    #@193
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@196
    goto/16 :goto_b8

    #@198
    .line 1510
    :cond_198
    const v2, 0x1020015

    #@19b
    const/16 v3, 0x8

    #@19d
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@1a0
    .line 1511
    move-object/from16 v0, p0

    #@1a2
    iget v2, v0, Landroid/app/Notification$Builder;->mProgressMax:I

    #@1a4
    if-nez v2, :cond_1ac

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    iget-boolean v2, v0, Landroid/app/Notification$Builder;->mProgressIndeterminate:Z

    #@1aa
    if-eqz v2, :cond_1c8

    #@1ac
    .line 1512
    :cond_1ac
    const v2, 0x102000d

    #@1af
    move-object/from16 v0, p0

    #@1b1
    iget v3, v0, Landroid/app/Notification$Builder;->mProgressMax:I

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iget v4, v0, Landroid/app/Notification$Builder;->mProgress:I

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget-boolean v5, v0, Landroid/app/Notification$Builder;->mProgressIndeterminate:Z

    #@1bb
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    #@1be
    .line 1514
    const v2, 0x102000d

    #@1c1
    const/4 v3, 0x0

    #@1c2
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@1c5
    .line 1515
    const/4 v9, 0x1

    #@1c6
    goto/16 :goto_b8

    #@1c8
    .line 1517
    :cond_1c8
    const v2, 0x102000d

    #@1cb
    const/16 v3, 0x8

    #@1cd
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@1d0
    goto/16 :goto_b8

    #@1d2
    .line 1539
    :cond_1d2
    const v2, 0x1020064

    #@1d5
    const/4 v3, 0x0

    #@1d6
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@1d9
    .line 1540
    const v2, 0x1020064

    #@1dc
    const-string/jumbo v3, "setTime"

    #@1df
    move-object/from16 v0, p0

    #@1e1
    iget-wide v4, v0, Landroid/app/Notification$Builder;->mWhen:J

    #@1e3
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RemoteViews;->setLong(ILjava/lang/String;J)V

    #@1e6
    goto/16 :goto_11c

    #@1e8
    .line 1543
    :cond_1e8
    const v2, 0x1020064

    #@1eb
    const/16 v3, 0x8

    #@1ed
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@1f0
    goto/16 :goto_11c

    #@1f2
    .line 1546
    :cond_1f2
    const/16 v2, 0x8

    #@1f4
    goto/16 :goto_122

    #@1f6
    .line 1547
    :cond_1f6
    const/16 v2, 0x8

    #@1f8
    goto/16 :goto_12b
.end method

.method private applyStandardTemplateWithActions(I)Landroid/widget/RemoteViews;
    .registers 9
    .parameter "layoutId"

    #@0
    .prologue
    const v6, 0x1020333

    #@3
    const/4 v5, 0x0

    #@4
    .line 1552
    invoke-direct {p0, p1, v5}, Landroid/app/Notification$Builder;->applyStandardTemplate(IZ)Landroid/widget/RemoteViews;

    #@7
    move-result-object v1

    #@8
    .line 1554
    .local v1, big:Landroid/widget/RemoteViews;
    iget-object v4, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@a
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@d
    move-result v0

    #@e
    .line 1555
    .local v0, N:I
    if-lez v0, :cond_35

    #@10
    .line 1557
    invoke-virtual {v1, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@13
    .line 1558
    const v4, 0x102033c

    #@16
    invoke-virtual {v1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    #@19
    .line 1559
    const/4 v4, 0x3

    #@1a
    if-le v0, v4, :cond_1d

    #@1c
    const/4 v0, 0x3

    #@1d
    .line 1560
    :cond_1d
    invoke-virtual {v1, v6}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    #@20
    .line 1561
    const/4 v3, 0x0

    #@21
    .local v3, i:I
    :goto_21
    if-ge v3, v0, :cond_35

    #@23
    .line 1562
    iget-object v4, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Landroid/app/Notification$Action;

    #@2b
    invoke-direct {p0, v4}, Landroid/app/Notification$Builder;->generateActionButton(Landroid/app/Notification$Action;)Landroid/widget/RemoteViews;

    #@2e
    move-result-object v2

    #@2f
    .line 1564
    .local v2, button:Landroid/widget/RemoteViews;
    invoke-virtual {v1, v6, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    #@32
    .line 1561
    add-int/lit8 v3, v3, 0x1

    #@34
    goto :goto_21

    #@35
    .line 1567
    .end local v2           #button:Landroid/widget/RemoteViews;
    .end local v3           #i:I
    :cond_35
    return-object v1
.end method

.method private buildUnstyled()Landroid/app/Notification;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1616
    new-instance v0, Landroid/app/Notification;

    #@3
    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    #@6
    .line 1617
    .local v0, n:Landroid/app/Notification;
    iget-wide v2, p0, Landroid/app/Notification$Builder;->mWhen:J

    #@8
    iput-wide v2, v0, Landroid/app/Notification;->when:J

    #@a
    .line 1618
    iget v2, p0, Landroid/app/Notification$Builder;->mSmallIcon:I

    #@c
    iput v2, v0, Landroid/app/Notification;->icon:I

    #@e
    .line 1619
    iget v2, p0, Landroid/app/Notification$Builder;->mSmallIconLevel:I

    #@10
    iput v2, v0, Landroid/app/Notification;->iconLevel:I

    #@12
    .line 1620
    iget v2, p0, Landroid/app/Notification$Builder;->mNumber:I

    #@14
    iput v2, v0, Landroid/app/Notification;->number:I

    #@16
    .line 1621
    invoke-direct {p0}, Landroid/app/Notification$Builder;->makeContentView()Landroid/widget/RemoteViews;

    #@19
    move-result-object v2

    #@1a
    iput-object v2, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    #@1c
    .line 1622
    iget-object v2, p0, Landroid/app/Notification$Builder;->mContentIntent:Landroid/app/PendingIntent;

    #@1e
    iput-object v2, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    #@20
    .line 1623
    iget-object v2, p0, Landroid/app/Notification$Builder;->mDeleteIntent:Landroid/app/PendingIntent;

    #@22
    iput-object v2, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@24
    .line 1624
    iget-object v2, p0, Landroid/app/Notification$Builder;->mFullScreenIntent:Landroid/app/PendingIntent;

    #@26
    iput-object v2, v0, Landroid/app/Notification;->fullScreenIntent:Landroid/app/PendingIntent;

    #@28
    .line 1625
    iget-object v2, p0, Landroid/app/Notification$Builder;->mTickerText:Ljava/lang/CharSequence;

    #@2a
    iput-object v2, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    #@2c
    .line 1626
    invoke-direct {p0}, Landroid/app/Notification$Builder;->makeTickerView()Landroid/widget/RemoteViews;

    #@2f
    move-result-object v2

    #@30
    iput-object v2, v0, Landroid/app/Notification;->tickerView:Landroid/widget/RemoteViews;

    #@32
    .line 1627
    iget-object v2, p0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@34
    iput-object v2, v0, Landroid/app/Notification;->largeIcon:Landroid/graphics/Bitmap;

    #@36
    .line 1628
    iget-object v2, p0, Landroid/app/Notification$Builder;->mSound:Landroid/net/Uri;

    #@38
    iput-object v2, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    #@3a
    .line 1629
    iget v2, p0, Landroid/app/Notification$Builder;->mAudioStreamType:I

    #@3c
    iput v2, v0, Landroid/app/Notification;->audioStreamType:I

    #@3e
    .line 1630
    iget-object v2, p0, Landroid/app/Notification$Builder;->mVibrate:[J

    #@40
    iput-object v2, v0, Landroid/app/Notification;->vibrate:[J

    #@42
    .line 1631
    iget v2, p0, Landroid/app/Notification$Builder;->mLedArgb:I

    #@44
    iput v2, v0, Landroid/app/Notification;->ledARGB:I

    #@46
    .line 1632
    iget v2, p0, Landroid/app/Notification$Builder;->mLedOnMs:I

    #@48
    iput v2, v0, Landroid/app/Notification;->ledOnMS:I

    #@4a
    .line 1633
    iget v2, p0, Landroid/app/Notification$Builder;->mLedOffMs:I

    #@4c
    iput v2, v0, Landroid/app/Notification;->ledOffMS:I

    #@4e
    .line 1634
    iget v2, p0, Landroid/app/Notification$Builder;->mDefaults:I

    #@50
    iput v2, v0, Landroid/app/Notification;->defaults:I

    #@52
    .line 1635
    iget v2, p0, Landroid/app/Notification$Builder;->mFlags:I

    #@54
    iput v2, v0, Landroid/app/Notification;->flags:I

    #@56
    .line 1636
    invoke-direct {p0}, Landroid/app/Notification$Builder;->makeBigContentView()Landroid/widget/RemoteViews;

    #@59
    move-result-object v2

    #@5a
    iput-object v2, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    #@5c
    .line 1637
    iget v2, p0, Landroid/app/Notification$Builder;->mLedOnMs:I

    #@5e
    if-eqz v2, :cond_6a

    #@60
    iget v2, p0, Landroid/app/Notification$Builder;->mLedOffMs:I

    #@62
    if-eqz v2, :cond_6a

    #@64
    .line 1638
    iget v2, v0, Landroid/app/Notification;->flags:I

    #@66
    or-int/lit8 v2, v2, 0x1

    #@68
    iput v2, v0, Landroid/app/Notification;->flags:I

    #@6a
    .line 1640
    :cond_6a
    iget v2, p0, Landroid/app/Notification$Builder;->mDefaults:I

    #@6c
    and-int/lit8 v2, v2, 0x4

    #@6e
    if-eqz v2, :cond_76

    #@70
    .line 1641
    iget v2, v0, Landroid/app/Notification;->flags:I

    #@72
    or-int/lit8 v2, v2, 0x1

    #@74
    iput v2, v0, Landroid/app/Notification;->flags:I

    #@76
    .line 1643
    :cond_76
    iget-object v2, p0, Landroid/app/Notification$Builder;->mKindList:Ljava/util/ArrayList;

    #@78
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@7b
    move-result v2

    #@7c
    if-lez v2, :cond_be

    #@7e
    .line 1644
    iget-object v2, p0, Landroid/app/Notification$Builder;->mKindList:Ljava/util/ArrayList;

    #@80
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@83
    move-result v2

    #@84
    new-array v2, v2, [Ljava/lang/String;

    #@86
    iput-object v2, v0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@88
    .line 1645
    iget-object v2, p0, Landroid/app/Notification$Builder;->mKindList:Ljava/util/ArrayList;

    #@8a
    iget-object v3, v0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@8c
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@8f
    .line 1649
    :goto_8f
    iget v2, p0, Landroid/app/Notification$Builder;->mPriority:I

    #@91
    iput v2, v0, Landroid/app/Notification;->priority:I

    #@93
    .line 1650
    iget-object v2, p0, Landroid/app/Notification$Builder;->mExtras:Landroid/os/Bundle;

    #@95
    if-eqz v2, :cond_9e

    #@97
    new-instance v1, Landroid/os/Bundle;

    #@99
    iget-object v2, p0, Landroid/app/Notification$Builder;->mExtras:Landroid/os/Bundle;

    #@9b
    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@9e
    :cond_9e
    invoke-static {v0, v1}, Landroid/app/Notification;->access$102(Landroid/app/Notification;Landroid/os/Bundle;)Landroid/os/Bundle;

    #@a1
    .line 1651
    iget-object v1, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@a3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@a6
    move-result v1

    #@a7
    if-lez v1, :cond_bd

    #@a9
    .line 1652
    iget-object v1, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@ab
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@ae
    move-result v1

    #@af
    new-array v1, v1, [Landroid/app/Notification$Action;

    #@b1
    invoke-static {v0, v1}, Landroid/app/Notification;->access$202(Landroid/app/Notification;[Landroid/app/Notification$Action;)[Landroid/app/Notification$Action;

    #@b4
    .line 1653
    iget-object v1, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@b6
    invoke-static {v0}, Landroid/app/Notification;->access$200(Landroid/app/Notification;)[Landroid/app/Notification$Action;

    #@b9
    move-result-object v2

    #@ba
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@bd
    .line 1655
    :cond_bd
    return-object v0

    #@be
    .line 1647
    :cond_be
    iput-object v1, v0, Landroid/app/Notification;->kind:[Ljava/lang/String;

    #@c0
    goto :goto_8f
.end method

.method private generateActionButton(Landroid/app/Notification$Action;)Landroid/widget/RemoteViews;
    .registers 9
    .parameter "action"

    #@0
    .prologue
    const v1, 0x1020332

    #@3
    const/4 v3, 0x0

    #@4
    .line 1599
    iget-object v2, p1, Landroid/app/Notification$Action;->actionIntent:Landroid/app/PendingIntent;

    #@6
    if-nez v2, :cond_32

    #@8
    const/4 v6, 0x1

    #@9
    .line 1600
    .local v6, tombstone:Z
    :goto_9
    new-instance v0, Landroid/widget/RemoteViews;

    #@b
    iget-object v2, p0, Landroid/app/Notification$Builder;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@10
    move-result-object v4

    #@11
    if-eqz v6, :cond_34

    #@13
    const v2, 0x1090090

    #@16
    :goto_16
    invoke-direct {v0, v4, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    #@19
    .line 1603
    .local v0, button:Landroid/widget/RemoteViews;
    iget v2, p1, Landroid/app/Notification$Action;->icon:I

    #@1b
    move v4, v3

    #@1c
    move v5, v3

    #@1d
    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawablesRelative(IIIII)V

    #@20
    .line 1604
    iget-object v2, p1, Landroid/app/Notification$Action;->title:Ljava/lang/CharSequence;

    #@22
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    #@25
    .line 1605
    if-nez v6, :cond_2c

    #@27
    .line 1606
    iget-object v2, p1, Landroid/app/Notification$Action;->actionIntent:Landroid/app/PendingIntent;

    #@29
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    #@2c
    .line 1608
    :cond_2c
    iget-object v2, p1, Landroid/app/Notification$Action;->title:Ljava/lang/CharSequence;

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    #@31
    .line 1609
    return-object v0

    #@32
    .end local v0           #button:Landroid/widget/RemoteViews;
    .end local v6           #tombstone:Z
    :cond_32
    move v6, v3

    #@33
    .line 1599
    goto :goto_9

    #@34
    .line 1600
    .restart local v6       #tombstone:Z
    :cond_34
    const v2, 0x109008e

    #@37
    goto :goto_16
.end method

.method private makeBigContentView()Landroid/widget/RemoteViews;
    .registers 2

    #@0
    .prologue
    .line 1593
    iget-object v0, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    .line 1595
    :goto_9
    return-object v0

    #@a
    :cond_a
    const v0, 0x1090093

    #@d
    invoke-direct {p0, v0}, Landroid/app/Notification$Builder;->applyStandardTemplateWithActions(I)Landroid/widget/RemoteViews;

    #@10
    move-result-object v0

    #@11
    goto :goto_9
.end method

.method private makeContentView()Landroid/widget/RemoteViews;
    .registers 3

    #@0
    .prologue
    .line 1571
    iget-object v0, p0, Landroid/app/Notification$Builder;->mContentView:Landroid/widget/RemoteViews;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1572
    iget-object v0, p0, Landroid/app/Notification$Builder;->mContentView:Landroid/widget/RemoteViews;

    #@6
    .line 1574
    :goto_6
    return-object v0

    #@7
    :cond_7
    const v0, 0x1090092

    #@a
    const/4 v1, 0x1

    #@b
    invoke-direct {p0, v0, v1}, Landroid/app/Notification$Builder;->applyStandardTemplate(IZ)Landroid/widget/RemoteViews;

    #@e
    move-result-object v0

    #@f
    goto :goto_6
.end method

.method private makeTickerView()Landroid/widget/RemoteViews;
    .registers 3

    #@0
    .prologue
    .line 1579
    iget-object v0, p0, Landroid/app/Notification$Builder;->mTickerView:Landroid/widget/RemoteViews;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 1580
    iget-object v0, p0, Landroid/app/Notification$Builder;->mTickerView:Landroid/widget/RemoteViews;

    #@6
    .line 1587
    :goto_6
    return-object v0

    #@7
    .line 1582
    :cond_7
    iget-object v0, p0, Landroid/app/Notification$Builder;->mContentView:Landroid/widget/RemoteViews;

    #@9
    if-nez v0, :cond_1c

    #@b
    .line 1583
    iget-object v0, p0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@d
    if-nez v0, :cond_18

    #@f
    const v0, 0x10900d1

    #@12
    :goto_12
    const/4 v1, 0x1

    #@13
    invoke-direct {p0, v0, v1}, Landroid/app/Notification$Builder;->applyStandardTemplate(IZ)Landroid/widget/RemoteViews;

    #@16
    move-result-object v0

    #@17
    goto :goto_6

    #@18
    :cond_18
    const v0, 0x10900d2

    #@1b
    goto :goto_12

    #@1c
    .line 1587
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_6
.end method

.method private setFlag(IZ)V
    .registers 5
    .parameter "mask"
    .parameter "value"

    #@0
    .prologue
    .line 1444
    if-eqz p2, :cond_8

    #@2
    .line 1445
    iget v0, p0, Landroid/app/Notification$Builder;->mFlags:I

    #@4
    or-int/2addr v0, p1

    #@5
    iput v0, p0, Landroid/app/Notification$Builder;->mFlags:I

    #@7
    .line 1449
    :goto_7
    return-void

    #@8
    .line 1447
    :cond_8
    iget v0, p0, Landroid/app/Notification$Builder;->mFlags:I

    #@a
    xor-int/lit8 v1, p1, -0x1

    #@c
    and-int/2addr v0, v1

    #@d
    iput v0, p0, Landroid/app/Notification$Builder;->mFlags:I

    #@f
    goto :goto_7
.end method


# virtual methods
.method public addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
    .registers 6
    .parameter "icon"
    .parameter "title"
    .parameter "intent"

    #@0
    .prologue
    .line 1424
    iget-object v0, p0, Landroid/app/Notification$Builder;->mActions:Ljava/util/ArrayList;

    #@2
    new-instance v1, Landroid/app/Notification$Action;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/app/Notification$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a
    .line 1425
    return-object p0
.end method

.method public addKind(Ljava/lang/String;)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "k"

    #@0
    .prologue
    .line 1394
    iget-object v0, p0, Landroid/app/Notification$Builder;->mKindList:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5
    .line 1395
    return-object p0
.end method

.method public build()Landroid/app/Notification;
    .registers 2

    #@0
    .prologue
    .line 1671
    iget-object v0, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1672
    iget-object v0, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@6
    invoke-virtual {v0}, Landroid/app/Notification$Style;->build()Landroid/app/Notification;

    #@9
    move-result-object v0

    #@a
    .line 1674
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-direct {p0}, Landroid/app/Notification$Builder;->buildUnstyled()Landroid/app/Notification;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method public getNotification()Landroid/app/Notification;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1663
    invoke-virtual {p0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public setAutoCancel(Z)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "autoCancel"

    #@0
    .prologue
    .line 1358
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0, p1}, Landroid/app/Notification$Builder;->setFlag(IZ)V

    #@5
    .line 1359
    return-object p0
.end method

.method public setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "views"

    #@0
    .prologue
    .line 1170
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentView:Landroid/widget/RemoteViews;

    #@2
    .line 1171
    return-object p0
.end method

.method public setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "info"

    #@0
    .prologue
    .line 1148
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentInfo:Ljava/lang/CharSequence;

    #@2
    .line 1149
    return-object p0
.end method

.method public setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 1186
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentIntent:Landroid/app/PendingIntent;

    #@2
    .line 1187
    return-object p0
.end method

.method public setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 1118
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentText:Ljava/lang/CharSequence;

    #@2
    .line 1119
    return-object p0
.end method

.method public setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 1110
    iput-object p1, p0, Landroid/app/Notification$Builder;->mContentTitle:Ljava/lang/CharSequence;

    #@2
    .line 1111
    return-object p0
.end method

.method public setDefaults(I)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "defaults"

    #@0
    .prologue
    .line 1372
    iput p1, p0, Landroid/app/Notification$Builder;->mDefaults:I

    #@2
    .line 1373
    return-object p0
.end method

.method public setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "intent"

    #@0
    .prologue
    .line 1196
    iput-object p1, p0, Landroid/app/Notification$Builder;->mDeleteIntent:Landroid/app/PendingIntent;

    #@2
    .line 1197
    return-object p0
.end method

.method public setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "bag"

    #@0
    .prologue
    .line 1409
    iput-object p1, p0, Landroid/app/Notification$Builder;->mExtras:Landroid/os/Bundle;

    #@2
    .line 1410
    return-object p0
.end method

.method public setFullScreenIntent(Landroid/app/PendingIntent;Z)Landroid/app/Notification$Builder;
    .registers 4
    .parameter "intent"
    .parameter "highPriority"

    #@0
    .prologue
    .line 1216
    iput-object p1, p0, Landroid/app/Notification$Builder;->mFullScreenIntent:Landroid/app/PendingIntent;

    #@2
    .line 1217
    const/16 v0, 0x80

    #@4
    invoke-direct {p0, v0, p2}, Landroid/app/Notification$Builder;->setFlag(IZ)V

    #@7
    .line 1218
    return-object p0
.end method

.method public setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "icon"

    #@0
    .prologue
    .line 1255
    iput-object p1, p0, Landroid/app/Notification$Builder;->mLargeIcon:Landroid/graphics/Bitmap;

    #@2
    .line 1256
    return-object p0
.end method

.method public setLights(III)Landroid/app/Notification$Builder;
    .registers 4
    .parameter "argb"
    .parameter "onMs"
    .parameter "offMs"

    #@0
    .prologue
    .line 1313
    iput p1, p0, Landroid/app/Notification$Builder;->mLedArgb:I

    #@2
    .line 1314
    iput p2, p0, Landroid/app/Notification$Builder;->mLedOnMs:I

    #@4
    .line 1315
    iput p3, p0, Landroid/app/Notification$Builder;->mLedOffMs:I

    #@6
    .line 1316
    return-object p0
.end method

.method public setNumber(I)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 1137
    iput p1, p0, Landroid/app/Notification$Builder;->mNumber:I

    #@2
    .line 1138
    return-object p0
.end method

.method public setOngoing(Z)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "ongoing"

    #@0
    .prologue
    .line 1336
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0, p1}, Landroid/app/Notification$Builder;->setFlag(IZ)V

    #@4
    .line 1337
    return-object p0
.end method

.method public setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "onlyAlertOnce"

    #@0
    .prologue
    .line 1347
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0, p1}, Landroid/app/Notification$Builder;->setFlag(IZ)V

    #@5
    .line 1348
    return-object p0
.end method

.method public setPriority(I)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "pri"

    #@0
    .prologue
    .line 1382
    iput p1, p0, Landroid/app/Notification$Builder;->mPriority:I

    #@2
    .line 1383
    return-object p0
.end method

.method public setProgress(IIZ)Landroid/app/Notification$Builder;
    .registers 4
    .parameter "max"
    .parameter "progress"
    .parameter "indeterminate"

    #@0
    .prologue
    .line 1158
    iput p1, p0, Landroid/app/Notification$Builder;->mProgressMax:I

    #@2
    .line 1159
    iput p2, p0, Landroid/app/Notification$Builder;->mProgress:I

    #@4
    .line 1160
    iput-boolean p3, p0, Landroid/app/Notification$Builder;->mProgressIndeterminate:Z

    #@6
    .line 1161
    return-object p0
.end method

.method public setShowWhen(Z)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "show"

    #@0
    .prologue
    .line 1050
    iput-boolean p1, p0, Landroid/app/Notification$Builder;->mShowWhen:Z

    #@2
    .line 1051
    return-object p0
.end method

.method public setSmallIcon(I)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "icon"

    #@0
    .prologue
    .line 1085
    iput p1, p0, Landroid/app/Notification$Builder;->mSmallIcon:I

    #@2
    .line 1086
    return-object p0
.end method

.method public setSmallIcon(II)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "icon"
    .parameter "level"

    #@0
    .prologue
    .line 1101
    iput p1, p0, Landroid/app/Notification$Builder;->mSmallIcon:I

    #@2
    .line 1102
    iput p2, p0, Landroid/app/Notification$Builder;->mSmallIconLevel:I

    #@4
    .line 1103
    return-object p0
.end method

.method public setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "sound"

    #@0
    .prologue
    .line 1267
    iput-object p1, p0, Landroid/app/Notification$Builder;->mSound:Landroid/net/Uri;

    #@2
    .line 1268
    const/4 v0, -0x1

    #@3
    iput v0, p0, Landroid/app/Notification$Builder;->mAudioStreamType:I

    #@5
    .line 1269
    return-object p0
.end method

.method public setSound(Landroid/net/Uri;I)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "sound"
    .parameter "streamType"

    #@0
    .prologue
    .line 1280
    iput-object p1, p0, Landroid/app/Notification$Builder;->mSound:Landroid/net/Uri;

    #@2
    .line 1281
    iput p2, p0, Landroid/app/Notification$Builder;->mAudioStreamType:I

    #@4
    .line 1282
    return-object p0
.end method

.method public setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "style"

    #@0
    .prologue
    .line 1434
    iget-object v0, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@2
    if-eq v0, p1, :cond_f

    #@4
    .line 1435
    iput-object p1, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@6
    .line 1436
    iget-object v0, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1437
    iget-object v0, p0, Landroid/app/Notification$Builder;->mStyle:Landroid/app/Notification$Style;

    #@c
    invoke-virtual {v0, p0}, Landroid/app/Notification$Style;->setBuilder(Landroid/app/Notification$Builder;)V

    #@f
    .line 1440
    :cond_f
    return-object p0
.end method

.method public setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 1127
    iput-object p1, p0, Landroid/app/Notification$Builder;->mSubText:Ljava/lang/CharSequence;

    #@2
    .line 1128
    return-object p0
.end method

.method public setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "tickerText"

    #@0
    .prologue
    .line 1228
    iput-object p1, p0, Landroid/app/Notification$Builder;->mTickerText:Ljava/lang/CharSequence;

    #@2
    .line 1229
    return-object p0
.end method

.method public setTicker(Ljava/lang/CharSequence;Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "tickerText"
    .parameter "views"

    #@0
    .prologue
    .line 1241
    iput-object p1, p0, Landroid/app/Notification$Builder;->mTickerText:Ljava/lang/CharSequence;

    #@2
    .line 1242
    iput-object p2, p0, Landroid/app/Notification$Builder;->mTickerView:Landroid/widget/RemoteViews;

    #@4
    .line 1243
    return-object p0
.end method

.method public setUsesChronometer(Z)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 1066
    iput-boolean p1, p0, Landroid/app/Notification$Builder;->mUseChronometer:Z

    #@2
    .line 1067
    return-object p0
.end method

.method public setVibrate([J)Landroid/app/Notification$Builder;
    .registers 2
    .parameter "pattern"

    #@0
    .prologue
    .line 1296
    iput-object p1, p0, Landroid/app/Notification$Builder;->mVibrate:[J

    #@2
    .line 1297
    return-object p0
.end method

.method public setWhen(J)Landroid/app/Notification$Builder;
    .registers 3
    .parameter "when"

    #@0
    .prologue
    .line 1041
    iput-wide p1, p0, Landroid/app/Notification$Builder;->mWhen:J

    #@2
    .line 1042
    return-object p0
.end method
