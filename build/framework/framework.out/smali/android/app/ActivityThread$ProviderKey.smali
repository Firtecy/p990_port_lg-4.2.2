.class final Landroid/app/ActivityThread$ProviderKey;
.super Ljava/lang/Object;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProviderKey"
.end annotation


# instance fields
.field final authority:Ljava/lang/String;

.field final userId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter "authority"
    .parameter "userId"

    #@0
    .prologue
    .line 230
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 231
    iput-object p1, p0, Landroid/app/ActivityThread$ProviderKey;->authority:Ljava/lang/String;

    #@5
    .line 232
    iput p2, p0, Landroid/app/ActivityThread$ProviderKey;->userId:I

    #@7
    .line 233
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 237
    instance-of v2, p1, Landroid/app/ActivityThread$ProviderKey;

    #@3
    if-eqz v2, :cond_19

    #@5
    move-object v0, p1

    #@6
    .line 238
    check-cast v0, Landroid/app/ActivityThread$ProviderKey;

    #@8
    .line 239
    .local v0, other:Landroid/app/ActivityThread$ProviderKey;
    iget-object v2, p0, Landroid/app/ActivityThread$ProviderKey;->authority:Ljava/lang/String;

    #@a
    iget-object v3, v0, Landroid/app/ActivityThread$ProviderKey;->authority:Ljava/lang/String;

    #@c
    invoke-static {v2, v3}, Lcom/android/internal/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_19

    #@12
    iget v2, p0, Landroid/app/ActivityThread$ProviderKey;->userId:I

    #@14
    iget v3, v0, Landroid/app/ActivityThread$ProviderKey;->userId:I

    #@16
    if-ne v2, v3, :cond_19

    #@18
    const/4 v1, 0x1

    #@19
    .line 241
    .end local v0           #other:Landroid/app/ActivityThread$ProviderKey;
    :cond_19
    return v1
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Landroid/app/ActivityThread$ProviderKey;->authority:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_e

    #@4
    iget-object v0, p0, Landroid/app/ActivityThread$ProviderKey;->authority:Ljava/lang/String;

    #@6
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    #@9
    move-result v0

    #@a
    :goto_a
    iget v1, p0, Landroid/app/ActivityThread$ProviderKey;->userId:I

    #@c
    xor-int/2addr v0, v1

    #@d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_a
.end method
