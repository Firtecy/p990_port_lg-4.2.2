.class public Landroid/app/IActivityManager$WaitResult;
.super Ljava/lang/Object;
.source "IActivityManager.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IActivityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WaitResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/IActivityManager$WaitResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public result:I

.field public thisTime:J

.field public timeout:Z

.field public totalTime:J

.field public who:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 465
    new-instance v0, Landroid/app/IActivityManager$WaitResult$1;

    #@2
    invoke-direct {v0}, Landroid/app/IActivityManager$WaitResult$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/IActivityManager$WaitResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 450
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 451
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 476
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 477
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/app/IActivityManager$WaitResult;->result:I

    #@9
    .line 478
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_25

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    iput-boolean v0, p0, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@12
    .line 479
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@18
    .line 480
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@1b
    move-result-wide v0

    #@1c
    iput-wide v0, p0, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@1e
    .line 481
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@21
    move-result-wide v0

    #@22
    iput-wide v0, p0, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@24
    .line 482
    return-void

    #@25
    .line 478
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_10
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/IActivityManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 443
    invoke-direct {p0, p1}, Landroid/app/IActivityManager$WaitResult;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 454
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 458
    iget v0, p0, Landroid/app/IActivityManager$WaitResult;->result:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 459
    iget-boolean v0, p0, Landroid/app/IActivityManager$WaitResult;->timeout:Z

    #@7
    if-eqz v0, :cond_1d

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 460
    iget-object v0, p0, Landroid/app/IActivityManager$WaitResult;->who:Landroid/content/ComponentName;

    #@f
    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    #@12
    .line 461
    iget-wide v0, p0, Landroid/app/IActivityManager$WaitResult;->thisTime:J

    #@14
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@17
    .line 462
    iget-wide v0, p0, Landroid/app/IActivityManager$WaitResult;->totalTime:J

    #@19
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@1c
    .line 463
    return-void

    #@1d
    .line 459
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_a
.end method
