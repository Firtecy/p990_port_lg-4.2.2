.class final Landroid/app/ContextImpl$40;
.super Landroid/app/ContextImpl$ServiceFetcher;
.source "ContextImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ContextImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 554
    invoke-direct {p0}, Landroid/app/ContextImpl$ServiceFetcher;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getService(Landroid/app/ContextImpl;)Ljava/lang/Object;
    .registers 6
    .parameter "ctx"

    #@0
    .prologue
    .line 556
    invoke-static {p1}, Landroid/app/ContextImpl;->access$100(Landroid/app/ContextImpl;)Landroid/view/Display;

    #@3
    move-result-object v0

    #@4
    .line 557
    .local v0, display:Landroid/view/Display;
    if-nez v0, :cond_17

    #@6
    .line 558
    invoke-virtual {p1}, Landroid/app/ContextImpl;->getOuterContext()Landroid/content/Context;

    #@9
    move-result-object v2

    #@a
    const-string v3, "display"

    #@c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/hardware/display/DisplayManager;

    #@12
    .line 560
    .local v1, dm:Landroid/hardware/display/DisplayManager;
    const/4 v2, 0x0

    #@13
    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    #@16
    move-result-object v0

    #@17
    .line 562
    .end local v1           #dm:Landroid/hardware/display/DisplayManager;
    :cond_17
    new-instance v2, Landroid/view/WindowManagerImpl;

    #@19
    invoke-direct {v2, v0}, Landroid/view/WindowManagerImpl;-><init>(Landroid/view/Display;)V

    #@1c
    return-object v2
.end method
