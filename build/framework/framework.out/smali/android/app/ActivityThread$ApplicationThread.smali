.class Landroid/app/ActivityThread$ApplicationThread;
.super Landroid/app/ApplicationThreadNative;
.source "ActivityThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplicationThread"
.end annotation


# static fields
.field private static final ACTIVITY_THREAD_CHECKIN_VERSION:I = 0x1

.field private static final DB_INFO_FORMAT:Ljava/lang/String; = "  %8s %8s %14s %14s  %s"

.field private static final HEAP_COLUMN:Ljava/lang/String; = "%13s %8s %8s %8s %8s %8s %8s"

.field private static final ONE_COUNT_COLUMN:Ljava/lang/String; = "%21s %8d"

.field private static final TWO_COUNT_COLUMNS:Ljava/lang/String; = "%21s %8d %21s %8d"


# instance fields
.field final synthetic this$0:Landroid/app/ActivityThread;


# direct methods
.method private constructor <init>(Landroid/app/ActivityThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 544
    iput-object p1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    invoke-direct {p0}, Landroid/app/ApplicationThreadNative;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/ActivityThread;Landroid/app/ActivityThread$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 544
    invoke-direct {p0, p1}, Landroid/app/ActivityThread$ApplicationThread;-><init>(Landroid/app/ActivityThread;)V

    #@3
    return-void
.end method

.method private dumpMemInfo(Ljava/io/PrintWriter;ZZ)Landroid/os/Debug$MemoryInfo;
    .registers 54
    .parameter "pw"
    .parameter "checkin"
    .parameter "all"

    #@0
    .prologue
    .line 906
    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    #@3
    move-result-wide v42

    #@4
    const-wide/16 v44, 0x400

    #@6
    div-long v28, v42, v44

    #@8
    .line 907
    .local v28, nativeMax:J
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    #@b
    move-result-wide v42

    #@c
    const-wide/16 v44, 0x400

    #@e
    div-long v24, v42, v44

    #@10
    .line 908
    .local v24, nativeAllocated:J
    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    #@13
    move-result-wide v42

    #@14
    const-wide/16 v44, 0x400

    #@16
    div-long v26, v42, v44

    #@18
    .line 910
    .local v26, nativeFree:J
    new-instance v23, Landroid/os/Debug$MemoryInfo;

    #@1a
    invoke-direct/range {v23 .. v23}, Landroid/os/Debug$MemoryInfo;-><init>()V

    #@1d
    .line 911
    .local v23, memInfo:Landroid/os/Debug$MemoryInfo;
    invoke-static/range {v23 .. v23}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    #@20
    .line 913
    if-nez p3, :cond_23

    #@22
    .line 1094
    :cond_22
    :goto_22
    return-object v23

    #@23
    .line 917
    :cond_23
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@26
    move-result-object v36

    #@27
    .line 919
    .local v36, runtime:Ljava/lang/Runtime;
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Runtime;->totalMemory()J

    #@2a
    move-result-wide v42

    #@2b
    const-wide/16 v44, 0x400

    #@2d
    div-long v17, v42, v44

    #@2f
    .line 920
    .local v17, dalvikMax:J
    invoke-virtual/range {v36 .. v36}, Ljava/lang/Runtime;->freeMemory()J

    #@32
    move-result-wide v42

    #@33
    const-wide/16 v44, 0x400

    #@35
    div-long v15, v42, v44

    #@37
    .line 921
    .local v15, dalvikFree:J
    sub-long v13, v17, v15

    #@39
    .line 922
    .local v13, dalvikAllocated:J
    invoke-static {}, Landroid/view/ViewDebug;->getViewInstanceCount()J

    #@3c
    move-result-wide v38

    #@3d
    .line 923
    .local v38, viewInstanceCount:J
    invoke-static {}, Landroid/view/ViewDebug;->getViewRootImplCount()J

    #@40
    move-result-wide v40

    #@41
    .line 924
    .local v40, viewRootInstanceCount:J
    const-class v42, Landroid/app/ContextImpl;

    #@43
    invoke-static/range {v42 .. v42}, Landroid/os/Debug;->countInstancesOfClass(Ljava/lang/Class;)J

    #@46
    move-result-wide v7

    #@47
    .line 925
    .local v7, appContextInstanceCount:J
    const-class v42, Landroid/app/Activity;

    #@49
    invoke-static/range {v42 .. v42}, Landroid/os/Debug;->countInstancesOfClass(Ljava/lang/Class;)J

    #@4c
    move-result-wide v5

    #@4d
    .line 926
    .local v5, activityInstanceCount:J
    invoke-static {}, Landroid/content/res/AssetManager;->getGlobalAssetCount()I

    #@50
    move-result v20

    #@51
    .line 927
    .local v20, globalAssetCount:I
    invoke-static {}, Landroid/content/res/AssetManager;->getGlobalAssetManagerCount()I

    #@54
    move-result v21

    #@55
    .line 928
    .local v21, globalAssetManagerCount:I
    invoke-static {}, Landroid/os/Debug;->getBinderLocalObjectCount()I

    #@58
    move-result v11

    #@59
    .line 929
    .local v11, binderLocalObjectCount:I
    invoke-static {}, Landroid/os/Debug;->getBinderProxyObjectCount()I

    #@5c
    move-result v12

    #@5d
    .line 930
    .local v12, binderProxyObjectCount:I
    invoke-static {}, Landroid/os/Debug;->getBinderDeathObjectCount()I

    #@60
    move-result v10

    #@61
    .line 931
    .local v10, binderDeathObjectCount:I
    const-class v42, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSocketImpl;

    #@63
    invoke-static/range {v42 .. v42}, Landroid/os/Debug;->countInstancesOfClass(Ljava/lang/Class;)J

    #@66
    move-result-wide v30

    #@67
    .line 932
    .local v30, openSslSocketCount:J
    invoke-static {}, Landroid/database/sqlite/SQLiteDebug;->getDatabaseInfo()Landroid/database/sqlite/SQLiteDebug$PagerStats;

    #@6a
    move-result-object v37

    #@6b
    .line 935
    .local v37, stats:Landroid/database/sqlite/SQLiteDebug$PagerStats;
    if-eqz p2, :cond_45a

    #@6d
    .line 938
    move-object/from16 v0, p0

    #@6f
    iget-object v0, v0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@71
    move-object/from16 v42, v0

    #@73
    move-object/from16 v0, v42

    #@75
    iget-object v0, v0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@77
    move-object/from16 v42, v0

    #@79
    if-eqz v42, :cond_450

    #@7b
    move-object/from16 v0, p0

    #@7d
    iget-object v0, v0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@7f
    move-object/from16 v42, v0

    #@81
    move-object/from16 v0, v42

    #@83
    iget-object v0, v0, Landroid/app/ActivityThread;->mBoundApplication:Landroid/app/ActivityThread$AppBindData;

    #@85
    move-object/from16 v42, v0

    #@87
    move-object/from16 v0, v42

    #@89
    iget-object v0, v0, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@8b
    move-object/from16 v35, v0

    #@8d
    .line 942
    .local v35, processName:Ljava/lang/String;
    :goto_8d
    const/16 v42, 0x1

    #@8f
    move-object/from16 v0, p1

    #@91
    move/from16 v1, v42

    #@93
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@96
    const/16 v42, 0x2c

    #@98
    move-object/from16 v0, p1

    #@9a
    move/from16 v1, v42

    #@9c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@9f
    .line 943
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@a2
    move-result v42

    #@a3
    move-object/from16 v0, p1

    #@a5
    move/from16 v1, v42

    #@a7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@aa
    const/16 v42, 0x2c

    #@ac
    move-object/from16 v0, p1

    #@ae
    move/from16 v1, v42

    #@b0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@b3
    .line 944
    move-object/from16 v0, p1

    #@b5
    move-object/from16 v1, v35

    #@b7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ba
    const/16 v42, 0x2c

    #@bc
    move-object/from16 v0, p1

    #@be
    move/from16 v1, v42

    #@c0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@c3
    .line 947
    move-object/from16 v0, p1

    #@c5
    move-wide/from16 v1, v28

    #@c7
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@ca
    const/16 v42, 0x2c

    #@cc
    move-object/from16 v0, p1

    #@ce
    move/from16 v1, v42

    #@d0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@d3
    .line 948
    move-object/from16 v0, p1

    #@d5
    move-wide/from16 v1, v17

    #@d7
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@da
    const/16 v42, 0x2c

    #@dc
    move-object/from16 v0, p1

    #@de
    move/from16 v1, v42

    #@e0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@e3
    .line 949
    const-string v42, "N/A,"

    #@e5
    move-object/from16 v0, p1

    #@e7
    move-object/from16 v1, v42

    #@e9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ec
    .line 950
    add-long v42, v28, v17

    #@ee
    move-object/from16 v0, p1

    #@f0
    move-wide/from16 v1, v42

    #@f2
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@f5
    const/16 v42, 0x2c

    #@f7
    move-object/from16 v0, p1

    #@f9
    move/from16 v1, v42

    #@fb
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@fe
    .line 953
    move-object/from16 v0, p1

    #@100
    move-wide/from16 v1, v24

    #@102
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@105
    const/16 v42, 0x2c

    #@107
    move-object/from16 v0, p1

    #@109
    move/from16 v1, v42

    #@10b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@10e
    .line 954
    move-object/from16 v0, p1

    #@110
    invoke-virtual {v0, v13, v14}, Ljava/io/PrintWriter;->print(J)V

    #@113
    const/16 v42, 0x2c

    #@115
    move-object/from16 v0, p1

    #@117
    move/from16 v1, v42

    #@119
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@11c
    .line 955
    const-string v42, "N/A,"

    #@11e
    move-object/from16 v0, p1

    #@120
    move-object/from16 v1, v42

    #@122
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@125
    .line 956
    add-long v42, v24, v13

    #@127
    move-object/from16 v0, p1

    #@129
    move-wide/from16 v1, v42

    #@12b
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@12e
    const/16 v42, 0x2c

    #@130
    move-object/from16 v0, p1

    #@132
    move/from16 v1, v42

    #@134
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@137
    .line 959
    move-object/from16 v0, p1

    #@139
    move-wide/from16 v1, v26

    #@13b
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@13e
    const/16 v42, 0x2c

    #@140
    move-object/from16 v0, p1

    #@142
    move/from16 v1, v42

    #@144
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@147
    .line 960
    move-object/from16 v0, p1

    #@149
    move-wide v1, v15

    #@14a
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@14d
    const/16 v42, 0x2c

    #@14f
    move-object/from16 v0, p1

    #@151
    move/from16 v1, v42

    #@153
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@156
    .line 961
    const-string v42, "N/A,"

    #@158
    move-object/from16 v0, p1

    #@15a
    move-object/from16 v1, v42

    #@15c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15f
    .line 962
    add-long v42, v26, v15

    #@161
    move-object/from16 v0, p1

    #@163
    move-wide/from16 v1, v42

    #@165
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@168
    const/16 v42, 0x2c

    #@16a
    move-object/from16 v0, p1

    #@16c
    move/from16 v1, v42

    #@16e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@171
    .line 965
    move-object/from16 v0, v23

    #@173
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@175
    move/from16 v42, v0

    #@177
    move-object/from16 v0, p1

    #@179
    move/from16 v1, v42

    #@17b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@17e
    const/16 v42, 0x2c

    #@180
    move-object/from16 v0, p1

    #@182
    move/from16 v1, v42

    #@184
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@187
    .line 966
    move-object/from16 v0, v23

    #@189
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@18b
    move/from16 v42, v0

    #@18d
    move-object/from16 v0, p1

    #@18f
    move/from16 v1, v42

    #@191
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@194
    const/16 v42, 0x2c

    #@196
    move-object/from16 v0, p1

    #@198
    move/from16 v1, v42

    #@19a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@19d
    .line 967
    move-object/from16 v0, v23

    #@19f
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@1a1
    move/from16 v42, v0

    #@1a3
    move-object/from16 v0, p1

    #@1a5
    move/from16 v1, v42

    #@1a7
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1aa
    const/16 v42, 0x2c

    #@1ac
    move-object/from16 v0, p1

    #@1ae
    move/from16 v1, v42

    #@1b0
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@1b3
    .line 968
    move-object/from16 v0, v23

    #@1b5
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@1b7
    move/from16 v42, v0

    #@1b9
    move-object/from16 v0, v23

    #@1bb
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@1bd
    move/from16 v43, v0

    #@1bf
    add-int v42, v42, v43

    #@1c1
    move-object/from16 v0, v23

    #@1c3
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@1c5
    move/from16 v43, v0

    #@1c7
    add-int v42, v42, v43

    #@1c9
    move-object/from16 v0, p1

    #@1cb
    move/from16 v1, v42

    #@1cd
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1d0
    const/16 v42, 0x2c

    #@1d2
    move-object/from16 v0, p1

    #@1d4
    move/from16 v1, v42

    #@1d6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@1d9
    .line 971
    move-object/from16 v0, v23

    #@1db
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@1dd
    move/from16 v42, v0

    #@1df
    move-object/from16 v0, p1

    #@1e1
    move/from16 v1, v42

    #@1e3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1e6
    const/16 v42, 0x2c

    #@1e8
    move-object/from16 v0, p1

    #@1ea
    move/from16 v1, v42

    #@1ec
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@1ef
    .line 972
    move-object/from16 v0, v23

    #@1f1
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@1f3
    move/from16 v42, v0

    #@1f5
    move-object/from16 v0, p1

    #@1f7
    move/from16 v1, v42

    #@1f9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@1fc
    const/16 v42, 0x2c

    #@1fe
    move-object/from16 v0, p1

    #@200
    move/from16 v1, v42

    #@202
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@205
    .line 973
    move-object/from16 v0, v23

    #@207
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@209
    move/from16 v42, v0

    #@20b
    move-object/from16 v0, p1

    #@20d
    move/from16 v1, v42

    #@20f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@212
    const/16 v42, 0x2c

    #@214
    move-object/from16 v0, p1

    #@216
    move/from16 v1, v42

    #@218
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@21b
    .line 974
    move-object/from16 v0, v23

    #@21d
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@21f
    move/from16 v42, v0

    #@221
    move-object/from16 v0, v23

    #@223
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@225
    move/from16 v43, v0

    #@227
    add-int v42, v42, v43

    #@229
    move-object/from16 v0, v23

    #@22b
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@22d
    move/from16 v43, v0

    #@22f
    add-int v42, v42, v43

    #@231
    move-object/from16 v0, p1

    #@233
    move/from16 v1, v42

    #@235
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@238
    .line 975
    const/16 v42, 0x2c

    #@23a
    move-object/from16 v0, p1

    #@23c
    move/from16 v1, v42

    #@23e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@241
    .line 978
    move-object/from16 v0, v23

    #@243
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@245
    move/from16 v42, v0

    #@247
    move-object/from16 v0, p1

    #@249
    move/from16 v1, v42

    #@24b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@24e
    const/16 v42, 0x2c

    #@250
    move-object/from16 v0, p1

    #@252
    move/from16 v1, v42

    #@254
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@257
    .line 979
    move-object/from16 v0, v23

    #@259
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@25b
    move/from16 v42, v0

    #@25d
    move-object/from16 v0, p1

    #@25f
    move/from16 v1, v42

    #@261
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@264
    const/16 v42, 0x2c

    #@266
    move-object/from16 v0, p1

    #@268
    move/from16 v1, v42

    #@26a
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@26d
    .line 980
    move-object/from16 v0, v23

    #@26f
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@271
    move/from16 v42, v0

    #@273
    move-object/from16 v0, p1

    #@275
    move/from16 v1, v42

    #@277
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@27a
    const/16 v42, 0x2c

    #@27c
    move-object/from16 v0, p1

    #@27e
    move/from16 v1, v42

    #@280
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@283
    .line 981
    move-object/from16 v0, v23

    #@285
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@287
    move/from16 v42, v0

    #@289
    move-object/from16 v0, v23

    #@28b
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@28d
    move/from16 v43, v0

    #@28f
    add-int v42, v42, v43

    #@291
    move-object/from16 v0, v23

    #@293
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@295
    move/from16 v43, v0

    #@297
    add-int v42, v42, v43

    #@299
    move-object/from16 v0, p1

    #@29b
    move/from16 v1, v42

    #@29d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2a0
    .line 982
    const/16 v42, 0x2c

    #@2a2
    move-object/from16 v0, p1

    #@2a4
    move/from16 v1, v42

    #@2a6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2a9
    .line 985
    move-object/from16 v0, p1

    #@2ab
    move-wide/from16 v1, v38

    #@2ad
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@2b0
    const/16 v42, 0x2c

    #@2b2
    move-object/from16 v0, p1

    #@2b4
    move/from16 v1, v42

    #@2b6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2b9
    .line 986
    move-object/from16 v0, p1

    #@2bb
    move-wide/from16 v1, v40

    #@2bd
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@2c0
    const/16 v42, 0x2c

    #@2c2
    move-object/from16 v0, p1

    #@2c4
    move/from16 v1, v42

    #@2c6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2c9
    .line 987
    move-object/from16 v0, p1

    #@2cb
    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->print(J)V

    #@2ce
    const/16 v42, 0x2c

    #@2d0
    move-object/from16 v0, p1

    #@2d2
    move/from16 v1, v42

    #@2d4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2d7
    .line 988
    move-object/from16 v0, p1

    #@2d9
    invoke-virtual {v0, v5, v6}, Ljava/io/PrintWriter;->print(J)V

    #@2dc
    const/16 v42, 0x2c

    #@2de
    move-object/from16 v0, p1

    #@2e0
    move/from16 v1, v42

    #@2e2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2e5
    .line 990
    move-object/from16 v0, p1

    #@2e7
    move/from16 v1, v20

    #@2e9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2ec
    const/16 v42, 0x2c

    #@2ee
    move-object/from16 v0, p1

    #@2f0
    move/from16 v1, v42

    #@2f2
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@2f5
    .line 991
    move-object/from16 v0, p1

    #@2f7
    move/from16 v1, v21

    #@2f9
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@2fc
    const/16 v42, 0x2c

    #@2fe
    move-object/from16 v0, p1

    #@300
    move/from16 v1, v42

    #@302
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@305
    .line 992
    move-object/from16 v0, p1

    #@307
    invoke-virtual {v0, v11}, Ljava/io/PrintWriter;->print(I)V

    #@30a
    const/16 v42, 0x2c

    #@30c
    move-object/from16 v0, p1

    #@30e
    move/from16 v1, v42

    #@310
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@313
    .line 993
    move-object/from16 v0, p1

    #@315
    invoke-virtual {v0, v12}, Ljava/io/PrintWriter;->print(I)V

    #@318
    const/16 v42, 0x2c

    #@31a
    move-object/from16 v0, p1

    #@31c
    move/from16 v1, v42

    #@31e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@321
    .line 995
    move-object/from16 v0, p1

    #@323
    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->print(I)V

    #@326
    const/16 v42, 0x2c

    #@328
    move-object/from16 v0, p1

    #@32a
    move/from16 v1, v42

    #@32c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@32f
    .line 996
    move-object/from16 v0, p1

    #@331
    move-wide/from16 v1, v30

    #@333
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@336
    const/16 v42, 0x2c

    #@338
    move-object/from16 v0, p1

    #@33a
    move/from16 v1, v42

    #@33c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@33f
    .line 999
    move-object/from16 v0, v37

    #@341
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->memoryUsed:I

    #@343
    move/from16 v42, v0

    #@345
    move/from16 v0, v42

    #@347
    div-int/lit16 v0, v0, 0x400

    #@349
    move/from16 v42, v0

    #@34b
    move-object/from16 v0, p1

    #@34d
    move/from16 v1, v42

    #@34f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@352
    const/16 v42, 0x2c

    #@354
    move-object/from16 v0, p1

    #@356
    move/from16 v1, v42

    #@358
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@35b
    .line 1000
    move-object/from16 v0, v37

    #@35d
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->memoryUsed:I

    #@35f
    move/from16 v42, v0

    #@361
    move/from16 v0, v42

    #@363
    div-int/lit16 v0, v0, 0x400

    #@365
    move/from16 v42, v0

    #@367
    move-object/from16 v0, p1

    #@369
    move/from16 v1, v42

    #@36b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@36e
    const/16 v42, 0x2c

    #@370
    move-object/from16 v0, p1

    #@372
    move/from16 v1, v42

    #@374
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@377
    .line 1001
    move-object/from16 v0, v37

    #@379
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->pageCacheOverflow:I

    #@37b
    move/from16 v42, v0

    #@37d
    move/from16 v0, v42

    #@37f
    div-int/lit16 v0, v0, 0x400

    #@381
    move/from16 v42, v0

    #@383
    move-object/from16 v0, p1

    #@385
    move/from16 v1, v42

    #@387
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@38a
    const/16 v42, 0x2c

    #@38c
    move-object/from16 v0, p1

    #@38e
    move/from16 v1, v42

    #@390
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@393
    .line 1002
    move-object/from16 v0, v37

    #@395
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->largestMemAlloc:I

    #@397
    move/from16 v42, v0

    #@399
    move/from16 v0, v42

    #@39b
    div-int/lit16 v0, v0, 0x400

    #@39d
    move/from16 v42, v0

    #@39f
    move-object/from16 v0, p1

    #@3a1
    move/from16 v1, v42

    #@3a3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@3a6
    .line 1003
    const/16 v22, 0x0

    #@3a8
    .local v22, i:I
    :goto_3a8
    move-object/from16 v0, v37

    #@3aa
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->dbStats:Ljava/util/ArrayList;

    #@3ac
    move-object/from16 v42, v0

    #@3ae
    invoke-virtual/range {v42 .. v42}, Ljava/util/ArrayList;->size()I

    #@3b1
    move-result v42

    #@3b2
    move/from16 v0, v22

    #@3b4
    move/from16 v1, v42

    #@3b6
    if-ge v0, v1, :cond_455

    #@3b8
    .line 1004
    move-object/from16 v0, v37

    #@3ba
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->dbStats:Ljava/util/ArrayList;

    #@3bc
    move-object/from16 v42, v0

    #@3be
    move-object/from16 v0, v42

    #@3c0
    move/from16 v1, v22

    #@3c2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3c5
    move-result-object v19

    #@3c6
    check-cast v19, Landroid/database/sqlite/SQLiteDebug$DbStats;

    #@3c8
    .line 1005
    .local v19, dbStats:Landroid/database/sqlite/SQLiteDebug$DbStats;
    const/16 v42, 0x2c

    #@3ca
    move-object/from16 v0, p1

    #@3cc
    move/from16 v1, v42

    #@3ce
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@3d1
    move-object/from16 v0, v19

    #@3d3
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->dbName:Ljava/lang/String;

    #@3d5
    move-object/from16 v42, v0

    #@3d7
    move-object/from16 v0, p1

    #@3d9
    move-object/from16 v1, v42

    #@3db
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3de
    .line 1006
    const/16 v42, 0x2c

    #@3e0
    move-object/from16 v0, p1

    #@3e2
    move/from16 v1, v42

    #@3e4
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@3e7
    move-object/from16 v0, v19

    #@3e9
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->pageSize:J

    #@3eb
    move-wide/from16 v42, v0

    #@3ed
    move-object/from16 v0, p1

    #@3ef
    move-wide/from16 v1, v42

    #@3f1
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@3f4
    .line 1007
    const/16 v42, 0x2c

    #@3f6
    move-object/from16 v0, p1

    #@3f8
    move/from16 v1, v42

    #@3fa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@3fd
    move-object/from16 v0, v19

    #@3ff
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->dbSize:J

    #@401
    move-wide/from16 v42, v0

    #@403
    move-object/from16 v0, p1

    #@405
    move-wide/from16 v1, v42

    #@407
    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->print(J)V

    #@40a
    .line 1008
    const/16 v42, 0x2c

    #@40c
    move-object/from16 v0, p1

    #@40e
    move/from16 v1, v42

    #@410
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@413
    move-object/from16 v0, v19

    #@415
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->lookaside:I

    #@417
    move/from16 v42, v0

    #@419
    move-object/from16 v0, p1

    #@41b
    move/from16 v1, v42

    #@41d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@420
    .line 1009
    const/16 v42, 0x2c

    #@422
    move-object/from16 v0, p1

    #@424
    move/from16 v1, v42

    #@426
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@429
    move-object/from16 v0, v19

    #@42b
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->cache:Ljava/lang/String;

    #@42d
    move-object/from16 v42, v0

    #@42f
    move-object/from16 v0, p1

    #@431
    move-object/from16 v1, v42

    #@433
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@436
    .line 1010
    const/16 v42, 0x2c

    #@438
    move-object/from16 v0, p1

    #@43a
    move/from16 v1, v42

    #@43c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(C)V

    #@43f
    move-object/from16 v0, v19

    #@441
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->cache:Ljava/lang/String;

    #@443
    move-object/from16 v42, v0

    #@445
    move-object/from16 v0, p1

    #@447
    move-object/from16 v1, v42

    #@449
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@44c
    .line 1003
    add-int/lit8 v22, v22, 0x1

    #@44e
    goto/16 :goto_3a8

    #@450
    .line 938
    .end local v19           #dbStats:Landroid/database/sqlite/SQLiteDebug$DbStats;
    .end local v22           #i:I
    .end local v35           #processName:Ljava/lang/String;
    :cond_450
    const-string/jumbo v35, "unknown"

    #@453
    goto/16 :goto_8d

    #@455
    .line 1012
    .restart local v22       #i:I
    .restart local v35       #processName:Ljava/lang/String;
    :cond_455
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@458
    goto/16 :goto_22

    #@45a
    .line 1018
    .end local v22           #i:I
    .end local v35           #processName:Ljava/lang/String;
    :cond_45a
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@45c
    const/16 v43, 0x7

    #@45e
    move/from16 v0, v43

    #@460
    new-array v0, v0, [Ljava/lang/Object;

    #@462
    move-object/from16 v43, v0

    #@464
    const/16 v44, 0x0

    #@466
    const-string v45, ""

    #@468
    aput-object v45, v43, v44

    #@46a
    const/16 v44, 0x1

    #@46c
    const-string v45, ""

    #@46e
    aput-object v45, v43, v44

    #@470
    const/16 v44, 0x2

    #@472
    const-string v45, "Shared"

    #@474
    aput-object v45, v43, v44

    #@476
    const/16 v44, 0x3

    #@478
    const-string v45, "Private"

    #@47a
    aput-object v45, v43, v44

    #@47c
    const/16 v44, 0x4

    #@47e
    const-string v45, "Heap"

    #@480
    aput-object v45, v43, v44

    #@482
    const/16 v44, 0x5

    #@484
    const-string v45, "Heap"

    #@486
    aput-object v45, v43, v44

    #@488
    const/16 v44, 0x6

    #@48a
    const-string v45, "Heap"

    #@48c
    aput-object v45, v43, v44

    #@48e
    move-object/from16 v0, p0

    #@490
    move-object/from16 v1, p1

    #@492
    move-object/from16 v2, v42

    #@494
    move-object/from16 v3, v43

    #@496
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@499
    .line 1019
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@49b
    const/16 v43, 0x7

    #@49d
    move/from16 v0, v43

    #@49f
    new-array v0, v0, [Ljava/lang/Object;

    #@4a1
    move-object/from16 v43, v0

    #@4a3
    const/16 v44, 0x0

    #@4a5
    const-string v45, ""

    #@4a7
    aput-object v45, v43, v44

    #@4a9
    const/16 v44, 0x1

    #@4ab
    const-string v45, "Pss"

    #@4ad
    aput-object v45, v43, v44

    #@4af
    const/16 v44, 0x2

    #@4b1
    const-string v45, "Dirty"

    #@4b3
    aput-object v45, v43, v44

    #@4b5
    const/16 v44, 0x3

    #@4b7
    const-string v45, "Dirty"

    #@4b9
    aput-object v45, v43, v44

    #@4bb
    const/16 v44, 0x4

    #@4bd
    const-string v45, "Size"

    #@4bf
    aput-object v45, v43, v44

    #@4c1
    const/16 v44, 0x5

    #@4c3
    const-string v45, "Alloc"

    #@4c5
    aput-object v45, v43, v44

    #@4c7
    const/16 v44, 0x6

    #@4c9
    const-string v45, "Free"

    #@4cb
    aput-object v45, v43, v44

    #@4cd
    move-object/from16 v0, p0

    #@4cf
    move-object/from16 v1, p1

    #@4d1
    move-object/from16 v2, v42

    #@4d3
    move-object/from16 v3, v43

    #@4d5
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@4d8
    .line 1020
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@4da
    const/16 v43, 0x7

    #@4dc
    move/from16 v0, v43

    #@4de
    new-array v0, v0, [Ljava/lang/Object;

    #@4e0
    move-object/from16 v43, v0

    #@4e2
    const/16 v44, 0x0

    #@4e4
    const-string v45, ""

    #@4e6
    aput-object v45, v43, v44

    #@4e8
    const/16 v44, 0x1

    #@4ea
    const-string v45, "------"

    #@4ec
    aput-object v45, v43, v44

    #@4ee
    const/16 v44, 0x2

    #@4f0
    const-string v45, "------"

    #@4f2
    aput-object v45, v43, v44

    #@4f4
    const/16 v44, 0x3

    #@4f6
    const-string v45, "------"

    #@4f8
    aput-object v45, v43, v44

    #@4fa
    const/16 v44, 0x4

    #@4fc
    const-string v45, "------"

    #@4fe
    aput-object v45, v43, v44

    #@500
    const/16 v44, 0x5

    #@502
    const-string v45, "------"

    #@504
    aput-object v45, v43, v44

    #@506
    const/16 v44, 0x6

    #@508
    const-string v45, "------"

    #@50a
    aput-object v45, v43, v44

    #@50c
    move-object/from16 v0, p0

    #@50e
    move-object/from16 v1, p1

    #@510
    move-object/from16 v2, v42

    #@512
    move-object/from16 v3, v43

    #@514
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@517
    .line 1022
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@519
    const/16 v43, 0x7

    #@51b
    move/from16 v0, v43

    #@51d
    new-array v0, v0, [Ljava/lang/Object;

    #@51f
    move-object/from16 v43, v0

    #@521
    const/16 v44, 0x0

    #@523
    const-string v45, "Native"

    #@525
    aput-object v45, v43, v44

    #@527
    const/16 v44, 0x1

    #@529
    move-object/from16 v0, v23

    #@52b
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@52d
    move/from16 v45, v0

    #@52f
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@532
    move-result-object v45

    #@533
    aput-object v45, v43, v44

    #@535
    const/16 v44, 0x2

    #@537
    move-object/from16 v0, v23

    #@539
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@53b
    move/from16 v45, v0

    #@53d
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@540
    move-result-object v45

    #@541
    aput-object v45, v43, v44

    #@543
    const/16 v44, 0x3

    #@545
    move-object/from16 v0, v23

    #@547
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@549
    move/from16 v45, v0

    #@54b
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@54e
    move-result-object v45

    #@54f
    aput-object v45, v43, v44

    #@551
    const/16 v44, 0x4

    #@553
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@556
    move-result-object v45

    #@557
    aput-object v45, v43, v44

    #@559
    const/16 v44, 0x5

    #@55b
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@55e
    move-result-object v45

    #@55f
    aput-object v45, v43, v44

    #@561
    const/16 v44, 0x6

    #@563
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@566
    move-result-object v45

    #@567
    aput-object v45, v43, v44

    #@569
    move-object/from16 v0, p0

    #@56b
    move-object/from16 v1, p1

    #@56d
    move-object/from16 v2, v42

    #@56f
    move-object/from16 v3, v43

    #@571
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@574
    .line 1024
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@576
    const/16 v43, 0x7

    #@578
    move/from16 v0, v43

    #@57a
    new-array v0, v0, [Ljava/lang/Object;

    #@57c
    move-object/from16 v43, v0

    #@57e
    const/16 v44, 0x0

    #@580
    const-string v45, "Dalvik"

    #@582
    aput-object v45, v43, v44

    #@584
    const/16 v44, 0x1

    #@586
    move-object/from16 v0, v23

    #@588
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@58a
    move/from16 v45, v0

    #@58c
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@58f
    move-result-object v45

    #@590
    aput-object v45, v43, v44

    #@592
    const/16 v44, 0x2

    #@594
    move-object/from16 v0, v23

    #@596
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@598
    move/from16 v45, v0

    #@59a
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59d
    move-result-object v45

    #@59e
    aput-object v45, v43, v44

    #@5a0
    const/16 v44, 0x3

    #@5a2
    move-object/from16 v0, v23

    #@5a4
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@5a6
    move/from16 v45, v0

    #@5a8
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5ab
    move-result-object v45

    #@5ac
    aput-object v45, v43, v44

    #@5ae
    const/16 v44, 0x4

    #@5b0
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5b3
    move-result-object v45

    #@5b4
    aput-object v45, v43, v44

    #@5b6
    const/16 v44, 0x5

    #@5b8
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5bb
    move-result-object v45

    #@5bc
    aput-object v45, v43, v44

    #@5be
    const/16 v44, 0x6

    #@5c0
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5c3
    move-result-object v45

    #@5c4
    aput-object v45, v43, v44

    #@5c6
    move-object/from16 v0, p0

    #@5c8
    move-object/from16 v1, p1

    #@5ca
    move-object/from16 v2, v42

    #@5cc
    move-object/from16 v3, v43

    #@5ce
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@5d1
    .line 1027
    move-object/from16 v0, v23

    #@5d3
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@5d5
    move/from16 v33, v0

    #@5d7
    .line 1028
    .local v33, otherPss:I
    move-object/from16 v0, v23

    #@5d9
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@5db
    move/from16 v34, v0

    #@5dd
    .line 1029
    .local v34, otherSharedDirty:I
    move-object/from16 v0, v23

    #@5df
    iget v0, v0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@5e1
    move/from16 v32, v0

    #@5e3
    .line 1031
    .local v32, otherPrivateDirty:I
    const/16 v22, 0x0

    #@5e5
    .restart local v22       #i:I
    :goto_5e5
    const/16 v42, 0x9

    #@5e7
    move/from16 v0, v22

    #@5e9
    move/from16 v1, v42

    #@5eb
    if-ge v0, v1, :cond_66e

    #@5ed
    .line 1032
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@5ef
    const/16 v43, 0x7

    #@5f1
    move/from16 v0, v43

    #@5f3
    new-array v0, v0, [Ljava/lang/Object;

    #@5f5
    move-object/from16 v43, v0

    #@5f7
    const/16 v44, 0x0

    #@5f9
    invoke-static/range {v22 .. v22}, Landroid/os/Debug$MemoryInfo;->getOtherLabel(I)Ljava/lang/String;

    #@5fc
    move-result-object v45

    #@5fd
    aput-object v45, v43, v44

    #@5ff
    const/16 v44, 0x1

    #@601
    move-object/from16 v0, v23

    #@603
    move/from16 v1, v22

    #@605
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherPss(I)I

    #@608
    move-result v45

    #@609
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60c
    move-result-object v45

    #@60d
    aput-object v45, v43, v44

    #@60f
    const/16 v44, 0x2

    #@611
    move-object/from16 v0, v23

    #@613
    move/from16 v1, v22

    #@615
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherSharedDirty(I)I

    #@618
    move-result v45

    #@619
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61c
    move-result-object v45

    #@61d
    aput-object v45, v43, v44

    #@61f
    const/16 v44, 0x3

    #@621
    move-object/from16 v0, v23

    #@623
    move/from16 v1, v22

    #@625
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherPrivateDirty(I)I

    #@628
    move-result v45

    #@629
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@62c
    move-result-object v45

    #@62d
    aput-object v45, v43, v44

    #@62f
    const/16 v44, 0x4

    #@631
    const-string v45, ""

    #@633
    aput-object v45, v43, v44

    #@635
    const/16 v44, 0x5

    #@637
    const-string v45, ""

    #@639
    aput-object v45, v43, v44

    #@63b
    const/16 v44, 0x6

    #@63d
    const-string v45, ""

    #@63f
    aput-object v45, v43, v44

    #@641
    move-object/from16 v0, p0

    #@643
    move-object/from16 v1, p1

    #@645
    move-object/from16 v2, v42

    #@647
    move-object/from16 v3, v43

    #@649
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@64c
    .line 1035
    move-object/from16 v0, v23

    #@64e
    move/from16 v1, v22

    #@650
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherPss(I)I

    #@653
    move-result v42

    #@654
    sub-int v33, v33, v42

    #@656
    .line 1036
    move-object/from16 v0, v23

    #@658
    move/from16 v1, v22

    #@65a
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherSharedDirty(I)I

    #@65d
    move-result v42

    #@65e
    sub-int v34, v34, v42

    #@660
    .line 1037
    move-object/from16 v0, v23

    #@662
    move/from16 v1, v22

    #@664
    invoke-virtual {v0, v1}, Landroid/os/Debug$MemoryInfo;->getOtherPrivateDirty(I)I

    #@667
    move-result v42

    #@668
    sub-int v32, v32, v42

    #@66a
    .line 1031
    add-int/lit8 v22, v22, 0x1

    #@66c
    goto/16 :goto_5e5

    #@66e
    .line 1040
    :cond_66e
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@670
    const/16 v43, 0x7

    #@672
    move/from16 v0, v43

    #@674
    new-array v0, v0, [Ljava/lang/Object;

    #@676
    move-object/from16 v43, v0

    #@678
    const/16 v44, 0x0

    #@67a
    const-string v45, "Unknown"

    #@67c
    aput-object v45, v43, v44

    #@67e
    const/16 v44, 0x1

    #@680
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@683
    move-result-object v45

    #@684
    aput-object v45, v43, v44

    #@686
    const/16 v44, 0x2

    #@688
    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68b
    move-result-object v45

    #@68c
    aput-object v45, v43, v44

    #@68e
    const/16 v44, 0x3

    #@690
    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@693
    move-result-object v45

    #@694
    aput-object v45, v43, v44

    #@696
    const/16 v44, 0x4

    #@698
    const-string v45, ""

    #@69a
    aput-object v45, v43, v44

    #@69c
    const/16 v44, 0x5

    #@69e
    const-string v45, ""

    #@6a0
    aput-object v45, v43, v44

    #@6a2
    const/16 v44, 0x6

    #@6a4
    const-string v45, ""

    #@6a6
    aput-object v45, v43, v44

    #@6a8
    move-object/from16 v0, p0

    #@6aa
    move-object/from16 v1, p1

    #@6ac
    move-object/from16 v2, v42

    #@6ae
    move-object/from16 v3, v43

    #@6b0
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@6b3
    .line 1042
    const-string v42, "%13s %8s %8s %8s %8s %8s %8s"

    #@6b5
    const/16 v43, 0x7

    #@6b7
    move/from16 v0, v43

    #@6b9
    new-array v0, v0, [Ljava/lang/Object;

    #@6bb
    move-object/from16 v43, v0

    #@6bd
    const/16 v44, 0x0

    #@6bf
    const-string v45, "TOTAL"

    #@6c1
    aput-object v45, v43, v44

    #@6c3
    const/16 v44, 0x1

    #@6c5
    invoke-virtual/range {v23 .. v23}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    #@6c8
    move-result v45

    #@6c9
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6cc
    move-result-object v45

    #@6cd
    aput-object v45, v43, v44

    #@6cf
    const/16 v44, 0x2

    #@6d1
    invoke-virtual/range {v23 .. v23}, Landroid/os/Debug$MemoryInfo;->getTotalSharedDirty()I

    #@6d4
    move-result v45

    #@6d5
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d8
    move-result-object v45

    #@6d9
    aput-object v45, v43, v44

    #@6db
    const/16 v44, 0x3

    #@6dd
    invoke-virtual/range {v23 .. v23}, Landroid/os/Debug$MemoryInfo;->getTotalPrivateDirty()I

    #@6e0
    move-result v45

    #@6e1
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e4
    move-result-object v45

    #@6e5
    aput-object v45, v43, v44

    #@6e7
    const/16 v44, 0x4

    #@6e9
    add-long v45, v28, v17

    #@6eb
    invoke-static/range {v45 .. v46}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6ee
    move-result-object v45

    #@6ef
    aput-object v45, v43, v44

    #@6f1
    const/16 v44, 0x5

    #@6f3
    add-long v45, v24, v13

    #@6f5
    invoke-static/range {v45 .. v46}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6f8
    move-result-object v45

    #@6f9
    aput-object v45, v43, v44

    #@6fb
    const/16 v44, 0x6

    #@6fd
    add-long v45, v26, v15

    #@6ff
    invoke-static/range {v45 .. v46}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@702
    move-result-object v45

    #@703
    aput-object v45, v43, v44

    #@705
    move-object/from16 v0, p0

    #@707
    move-object/from16 v1, p1

    #@709
    move-object/from16 v2, v42

    #@70b
    move-object/from16 v3, v43

    #@70d
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@710
    .line 1047
    const-string v42, " "

    #@712
    move-object/from16 v0, p1

    #@714
    move-object/from16 v1, v42

    #@716
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@719
    .line 1048
    const-string v42, " Objects"

    #@71b
    move-object/from16 v0, p1

    #@71d
    move-object/from16 v1, v42

    #@71f
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@722
    .line 1049
    const-string v42, "%21s %8d %21s %8d"

    #@724
    const/16 v43, 0x4

    #@726
    move/from16 v0, v43

    #@728
    new-array v0, v0, [Ljava/lang/Object;

    #@72a
    move-object/from16 v43, v0

    #@72c
    const/16 v44, 0x0

    #@72e
    const-string v45, "Views:"

    #@730
    aput-object v45, v43, v44

    #@732
    const/16 v44, 0x1

    #@734
    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@737
    move-result-object v45

    #@738
    aput-object v45, v43, v44

    #@73a
    const/16 v44, 0x2

    #@73c
    const-string v45, "ViewRootImpl:"

    #@73e
    aput-object v45, v43, v44

    #@740
    const/16 v44, 0x3

    #@742
    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@745
    move-result-object v45

    #@746
    aput-object v45, v43, v44

    #@748
    move-object/from16 v0, p0

    #@74a
    move-object/from16 v1, p1

    #@74c
    move-object/from16 v2, v42

    #@74e
    move-object/from16 v3, v43

    #@750
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@753
    .line 1052
    const-string v42, "%21s %8d %21s %8d"

    #@755
    const/16 v43, 0x4

    #@757
    move/from16 v0, v43

    #@759
    new-array v0, v0, [Ljava/lang/Object;

    #@75b
    move-object/from16 v43, v0

    #@75d
    const/16 v44, 0x0

    #@75f
    const-string v45, "AppContexts:"

    #@761
    aput-object v45, v43, v44

    #@763
    const/16 v44, 0x1

    #@765
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@768
    move-result-object v45

    #@769
    aput-object v45, v43, v44

    #@76b
    const/16 v44, 0x2

    #@76d
    const-string v45, "Activities:"

    #@76f
    aput-object v45, v43, v44

    #@771
    const/16 v44, 0x3

    #@773
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@776
    move-result-object v45

    #@777
    aput-object v45, v43, v44

    #@779
    move-object/from16 v0, p0

    #@77b
    move-object/from16 v1, p1

    #@77d
    move-object/from16 v2, v42

    #@77f
    move-object/from16 v3, v43

    #@781
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@784
    .line 1055
    const-string v42, "%21s %8d %21s %8d"

    #@786
    const/16 v43, 0x4

    #@788
    move/from16 v0, v43

    #@78a
    new-array v0, v0, [Ljava/lang/Object;

    #@78c
    move-object/from16 v43, v0

    #@78e
    const/16 v44, 0x0

    #@790
    const-string v45, "Assets:"

    #@792
    aput-object v45, v43, v44

    #@794
    const/16 v44, 0x1

    #@796
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@799
    move-result-object v45

    #@79a
    aput-object v45, v43, v44

    #@79c
    const/16 v44, 0x2

    #@79e
    const-string v45, "AssetManagers:"

    #@7a0
    aput-object v45, v43, v44

    #@7a2
    const/16 v44, 0x3

    #@7a4
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a7
    move-result-object v45

    #@7a8
    aput-object v45, v43, v44

    #@7aa
    move-object/from16 v0, p0

    #@7ac
    move-object/from16 v1, p1

    #@7ae
    move-object/from16 v2, v42

    #@7b0
    move-object/from16 v3, v43

    #@7b2
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@7b5
    .line 1058
    const-string v42, "%21s %8d %21s %8d"

    #@7b7
    const/16 v43, 0x4

    #@7b9
    move/from16 v0, v43

    #@7bb
    new-array v0, v0, [Ljava/lang/Object;

    #@7bd
    move-object/from16 v43, v0

    #@7bf
    const/16 v44, 0x0

    #@7c1
    const-string v45, "Local Binders:"

    #@7c3
    aput-object v45, v43, v44

    #@7c5
    const/16 v44, 0x1

    #@7c7
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7ca
    move-result-object v45

    #@7cb
    aput-object v45, v43, v44

    #@7cd
    const/16 v44, 0x2

    #@7cf
    const-string v45, "Proxy Binders:"

    #@7d1
    aput-object v45, v43, v44

    #@7d3
    const/16 v44, 0x3

    #@7d5
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7d8
    move-result-object v45

    #@7d9
    aput-object v45, v43, v44

    #@7db
    move-object/from16 v0, p0

    #@7dd
    move-object/from16 v1, p1

    #@7df
    move-object/from16 v2, v42

    #@7e1
    move-object/from16 v3, v43

    #@7e3
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@7e6
    .line 1060
    const-string v42, "%21s %8d"

    #@7e8
    const/16 v43, 0x2

    #@7ea
    move/from16 v0, v43

    #@7ec
    new-array v0, v0, [Ljava/lang/Object;

    #@7ee
    move-object/from16 v43, v0

    #@7f0
    const/16 v44, 0x0

    #@7f2
    const-string v45, "Death Recipients:"

    #@7f4
    aput-object v45, v43, v44

    #@7f6
    const/16 v44, 0x1

    #@7f8
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7fb
    move-result-object v45

    #@7fc
    aput-object v45, v43, v44

    #@7fe
    move-object/from16 v0, p0

    #@800
    move-object/from16 v1, p1

    #@802
    move-object/from16 v2, v42

    #@804
    move-object/from16 v3, v43

    #@806
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@809
    .line 1062
    const-string v42, "%21s %8d"

    #@80b
    const/16 v43, 0x2

    #@80d
    move/from16 v0, v43

    #@80f
    new-array v0, v0, [Ljava/lang/Object;

    #@811
    move-object/from16 v43, v0

    #@813
    const/16 v44, 0x0

    #@815
    const-string v45, "OpenSSL Sockets:"

    #@817
    aput-object v45, v43, v44

    #@819
    const/16 v44, 0x1

    #@81b
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@81e
    move-result-object v45

    #@81f
    aput-object v45, v43, v44

    #@821
    move-object/from16 v0, p0

    #@823
    move-object/from16 v1, p1

    #@825
    move-object/from16 v2, v42

    #@827
    move-object/from16 v3, v43

    #@829
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@82c
    .line 1065
    const-string v42, " "

    #@82e
    move-object/from16 v0, p1

    #@830
    move-object/from16 v1, v42

    #@832
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@835
    .line 1066
    const-string v42, " SQL"

    #@837
    move-object/from16 v0, p1

    #@839
    move-object/from16 v1, v42

    #@83b
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@83e
    .line 1067
    const-string v42, "%21s %8d"

    #@840
    const/16 v43, 0x2

    #@842
    move/from16 v0, v43

    #@844
    new-array v0, v0, [Ljava/lang/Object;

    #@846
    move-object/from16 v43, v0

    #@848
    const/16 v44, 0x0

    #@84a
    const-string v45, "MEMORY_USED:"

    #@84c
    aput-object v45, v43, v44

    #@84e
    const/16 v44, 0x1

    #@850
    move-object/from16 v0, v37

    #@852
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->memoryUsed:I

    #@854
    move/from16 v45, v0

    #@856
    move/from16 v0, v45

    #@858
    div-int/lit16 v0, v0, 0x400

    #@85a
    move/from16 v45, v0

    #@85c
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85f
    move-result-object v45

    #@860
    aput-object v45, v43, v44

    #@862
    move-object/from16 v0, p0

    #@864
    move-object/from16 v1, p1

    #@866
    move-object/from16 v2, v42

    #@868
    move-object/from16 v3, v43

    #@86a
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@86d
    .line 1068
    const-string v42, "%21s %8d %21s %8d"

    #@86f
    const/16 v43, 0x4

    #@871
    move/from16 v0, v43

    #@873
    new-array v0, v0, [Ljava/lang/Object;

    #@875
    move-object/from16 v43, v0

    #@877
    const/16 v44, 0x0

    #@879
    const-string v45, "PAGECACHE_OVERFLOW:"

    #@87b
    aput-object v45, v43, v44

    #@87d
    const/16 v44, 0x1

    #@87f
    move-object/from16 v0, v37

    #@881
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->pageCacheOverflow:I

    #@883
    move/from16 v45, v0

    #@885
    move/from16 v0, v45

    #@887
    div-int/lit16 v0, v0, 0x400

    #@889
    move/from16 v45, v0

    #@88b
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@88e
    move-result-object v45

    #@88f
    aput-object v45, v43, v44

    #@891
    const/16 v44, 0x2

    #@893
    const-string v45, "MALLOC_SIZE:"

    #@895
    aput-object v45, v43, v44

    #@897
    const/16 v44, 0x3

    #@899
    move-object/from16 v0, v37

    #@89b
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->largestMemAlloc:I

    #@89d
    move/from16 v45, v0

    #@89f
    move/from16 v0, v45

    #@8a1
    div-int/lit16 v0, v0, 0x400

    #@8a3
    move/from16 v45, v0

    #@8a5
    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a8
    move-result-object v45

    #@8a9
    aput-object v45, v43, v44

    #@8ab
    move-object/from16 v0, p0

    #@8ad
    move-object/from16 v1, p1

    #@8af
    move-object/from16 v2, v42

    #@8b1
    move-object/from16 v3, v43

    #@8b3
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@8b6
    .line 1070
    const-string v42, " "

    #@8b8
    move-object/from16 v0, p1

    #@8ba
    move-object/from16 v1, v42

    #@8bc
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8bf
    .line 1071
    move-object/from16 v0, v37

    #@8c1
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->dbStats:Ljava/util/ArrayList;

    #@8c3
    move-object/from16 v42, v0

    #@8c5
    invoke-virtual/range {v42 .. v42}, Ljava/util/ArrayList;->size()I

    #@8c8
    move-result v4

    #@8c9
    .line 1072
    .local v4, N:I
    if-lez v4, :cond_99e

    #@8cb
    .line 1073
    const-string v42, " DATABASES"

    #@8cd
    move-object/from16 v0, p1

    #@8cf
    move-object/from16 v1, v42

    #@8d1
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8d4
    .line 1074
    const-string v42, "  %8s %8s %14s %14s  %s"

    #@8d6
    const/16 v43, 0x5

    #@8d8
    move/from16 v0, v43

    #@8da
    new-array v0, v0, [Ljava/lang/Object;

    #@8dc
    move-object/from16 v43, v0

    #@8de
    const/16 v44, 0x0

    #@8e0
    const-string/jumbo v45, "pgsz"

    #@8e3
    aput-object v45, v43, v44

    #@8e5
    const/16 v44, 0x1

    #@8e7
    const-string v45, "dbsz"

    #@8e9
    aput-object v45, v43, v44

    #@8eb
    const/16 v44, 0x2

    #@8ed
    const-string v45, "Lookaside(b)"

    #@8ef
    aput-object v45, v43, v44

    #@8f1
    const/16 v44, 0x3

    #@8f3
    const-string v45, "cache"

    #@8f5
    aput-object v45, v43, v44

    #@8f7
    const/16 v44, 0x4

    #@8f9
    const-string v45, "Dbname"

    #@8fb
    aput-object v45, v43, v44

    #@8fd
    move-object/from16 v0, p0

    #@8ff
    move-object/from16 v1, p1

    #@901
    move-object/from16 v2, v42

    #@903
    move-object/from16 v3, v43

    #@905
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@908
    .line 1076
    const/16 v22, 0x0

    #@90a
    :goto_90a
    move/from16 v0, v22

    #@90c
    if-ge v0, v4, :cond_99e

    #@90e
    .line 1077
    move-object/from16 v0, v37

    #@910
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$PagerStats;->dbStats:Ljava/util/ArrayList;

    #@912
    move-object/from16 v42, v0

    #@914
    move-object/from16 v0, v42

    #@916
    move/from16 v1, v22

    #@918
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@91b
    move-result-object v19

    #@91c
    check-cast v19, Landroid/database/sqlite/SQLiteDebug$DbStats;

    #@91e
    .line 1078
    .restart local v19       #dbStats:Landroid/database/sqlite/SQLiteDebug$DbStats;
    const-string v43, "  %8s %8s %14s %14s  %s"

    #@920
    const/16 v42, 0x5

    #@922
    move/from16 v0, v42

    #@924
    new-array v0, v0, [Ljava/lang/Object;

    #@926
    move-object/from16 v44, v0

    #@928
    const/16 v45, 0x0

    #@92a
    move-object/from16 v0, v19

    #@92c
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->pageSize:J

    #@92e
    move-wide/from16 v46, v0

    #@930
    const-wide/16 v48, 0x0

    #@932
    cmp-long v42, v46, v48

    #@934
    if-lez v42, :cond_995

    #@936
    move-object/from16 v0, v19

    #@938
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->pageSize:J

    #@93a
    move-wide/from16 v46, v0

    #@93c
    invoke-static/range {v46 .. v47}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@93f
    move-result-object v42

    #@940
    :goto_940
    aput-object v42, v44, v45

    #@942
    const/16 v45, 0x1

    #@944
    move-object/from16 v0, v19

    #@946
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->dbSize:J

    #@948
    move-wide/from16 v46, v0

    #@94a
    const-wide/16 v48, 0x0

    #@94c
    cmp-long v42, v46, v48

    #@94e
    if-lez v42, :cond_998

    #@950
    move-object/from16 v0, v19

    #@952
    iget-wide v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->dbSize:J

    #@954
    move-wide/from16 v46, v0

    #@956
    invoke-static/range {v46 .. v47}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@959
    move-result-object v42

    #@95a
    :goto_95a
    aput-object v42, v44, v45

    #@95c
    const/16 v45, 0x2

    #@95e
    move-object/from16 v0, v19

    #@960
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->lookaside:I

    #@962
    move/from16 v42, v0

    #@964
    if-lez v42, :cond_99b

    #@966
    move-object/from16 v0, v19

    #@968
    iget v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->lookaside:I

    #@96a
    move/from16 v42, v0

    #@96c
    invoke-static/range {v42 .. v42}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@96f
    move-result-object v42

    #@970
    :goto_970
    aput-object v42, v44, v45

    #@972
    const/16 v42, 0x3

    #@974
    move-object/from16 v0, v19

    #@976
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->cache:Ljava/lang/String;

    #@978
    move-object/from16 v45, v0

    #@97a
    aput-object v45, v44, v42

    #@97c
    const/16 v42, 0x4

    #@97e
    move-object/from16 v0, v19

    #@980
    iget-object v0, v0, Landroid/database/sqlite/SQLiteDebug$DbStats;->dbName:Ljava/lang/String;

    #@982
    move-object/from16 v45, v0

    #@984
    aput-object v45, v44, v42

    #@986
    move-object/from16 v0, p0

    #@988
    move-object/from16 v1, p1

    #@98a
    move-object/from16 v2, v43

    #@98c
    move-object/from16 v3, v44

    #@98e
    invoke-direct {v0, v1, v2, v3}, Landroid/app/ActivityThread$ApplicationThread;->printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V

    #@991
    .line 1076
    add-int/lit8 v22, v22, 0x1

    #@993
    goto/16 :goto_90a

    #@995
    .line 1078
    :cond_995
    const-string v42, " "

    #@997
    goto :goto_940

    #@998
    :cond_998
    const-string v42, " "

    #@99a
    goto :goto_95a

    #@99b
    :cond_99b
    const-string v42, " "

    #@99d
    goto :goto_970

    #@99e
    .line 1087
    .end local v19           #dbStats:Landroid/database/sqlite/SQLiteDebug$DbStats;
    :cond_99e
    invoke-static {}, Landroid/content/res/AssetManager;->getAssetAllocations()Ljava/lang/String;

    #@9a1
    move-result-object v9

    #@9a2
    .line 1088
    .local v9, assetAlloc:Ljava/lang/String;
    if-eqz v9, :cond_22

    #@9a4
    .line 1089
    const-string v42, " "

    #@9a6
    move-object/from16 v0, p1

    #@9a8
    move-object/from16 v1, v42

    #@9aa
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9ad
    .line 1090
    const-string v42, " Asset Allocations"

    #@9af
    move-object/from16 v0, p1

    #@9b1
    move-object/from16 v1, v42

    #@9b3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@9b6
    .line 1091
    move-object/from16 v0, p1

    #@9b8
    invoke-virtual {v0, v9}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9bb
    goto/16 :goto_22
.end method

.method private varargs printRow(Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 5
    .parameter "pw"
    .parameter "format"
    .parameter "objs"

    #@0
    .prologue
    .line 1117
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7
    .line 1118
    return-void
.end method

.method private updatePendingConfiguration(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 554
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    iget-object v1, v0, Landroid/app/ActivityThread;->mPackages:Ljava/util/HashMap;

    #@4
    monitor-enter v1

    #@5
    .line 555
    :try_start_5
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@7
    iget-object v0, v0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@9
    if-eqz v0, :cond_15

    #@b
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@d
    iget-object v0, v0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@f
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    #@12
    move-result v0

    #@13
    if-eqz v0, :cond_19

    #@15
    .line 557
    :cond_15
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@17
    iput-object p1, v0, Landroid/app/ActivityThread;->mPendingConfiguration:Landroid/content/res/Configuration;

    #@19
    .line 559
    :cond_19
    monitor-exit v1

    #@1a
    .line 560
    return-void

    #@1b
    .line 559
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method


# virtual methods
.method public final bindApplication(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/util/List;Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ZLandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;IZZZLandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Ljava/util/Map;Landroid/os/Bundle;)V
    .registers 22
    .parameter "processName"
    .parameter "appInfo"
    .parameter
    .parameter "instrumentationName"
    .parameter "profileFile"
    .parameter "profileFd"
    .parameter "autoStopProfiler"
    .parameter "instrumentationArgs"
    .parameter "instrumentationWatcher"
    .parameter "debugMode"
    .parameter "enableOpenGlTrace"
    .parameter "isRestrictedBackupMode"
    .parameter "persistent"
    .parameter "config"
    .parameter "compatInfo"
    .parameter
    .parameter "coreSettings"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/pm/ApplicationInfo;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ProviderInfo;",
            ">;",
            "Landroid/content/ComponentName;",
            "Ljava/lang/String;",
            "Landroid/os/ParcelFileDescriptor;",
            "Z",
            "Landroid/os/Bundle;",
            "Landroid/app/IInstrumentationWatcher;",
            "IZZZ",
            "Landroid/content/res/Configuration;",
            "Landroid/content/res/CompatibilityInfo;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/IBinder;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 736
    .local p3, providers:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ProviderInfo;>;"
    .local p16, services:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/os/IBinder;>;"
    if-eqz p16, :cond_5

    #@2
    .line 738
    invoke-static/range {p16 .. p16}, Landroid/os/ServiceManager;->initServiceCache(Ljava/util/Map;)V

    #@5
    .line 741
    :cond_5
    move-object/from16 v0, p17

    #@7
    invoke-virtual {p0, v0}, Landroid/app/ActivityThread$ApplicationThread;->setCoreSettings(Landroid/os/Bundle;)V

    #@a
    .line 743
    new-instance v1, Landroid/app/ActivityThread$AppBindData;

    #@c
    invoke-direct {v1}, Landroid/app/ActivityThread$AppBindData;-><init>()V

    #@f
    .line 744
    .local v1, data:Landroid/app/ActivityThread$AppBindData;
    iput-object p1, v1, Landroid/app/ActivityThread$AppBindData;->processName:Ljava/lang/String;

    #@11
    .line 745
    iput-object p2, v1, Landroid/app/ActivityThread$AppBindData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    .line 746
    iput-object p3, v1, Landroid/app/ActivityThread$AppBindData;->providers:Ljava/util/List;

    #@15
    .line 747
    iput-object p4, v1, Landroid/app/ActivityThread$AppBindData;->instrumentationName:Landroid/content/ComponentName;

    #@17
    .line 748
    iput-object p8, v1, Landroid/app/ActivityThread$AppBindData;->instrumentationArgs:Landroid/os/Bundle;

    #@19
    .line 749
    iput-object p9, v1, Landroid/app/ActivityThread$AppBindData;->instrumentationWatcher:Landroid/app/IInstrumentationWatcher;

    #@1b
    .line 750
    iput p10, v1, Landroid/app/ActivityThread$AppBindData;->debugMode:I

    #@1d
    .line 751
    iput-boolean p11, v1, Landroid/app/ActivityThread$AppBindData;->enableOpenGlTrace:Z

    #@1f
    .line 752
    move/from16 v0, p12

    #@21
    iput-boolean v0, v1, Landroid/app/ActivityThread$AppBindData;->restrictedBackupMode:Z

    #@23
    .line 753
    move/from16 v0, p13

    #@25
    iput-boolean v0, v1, Landroid/app/ActivityThread$AppBindData;->persistent:Z

    #@27
    .line 754
    move-object/from16 v0, p14

    #@29
    iput-object v0, v1, Landroid/app/ActivityThread$AppBindData;->config:Landroid/content/res/Configuration;

    #@2b
    .line 755
    move-object/from16 v0, p15

    #@2d
    iput-object v0, v1, Landroid/app/ActivityThread$AppBindData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@2f
    .line 756
    iput-object p5, v1, Landroid/app/ActivityThread$AppBindData;->initProfileFile:Ljava/lang/String;

    #@31
    .line 757
    iput-object p6, v1, Landroid/app/ActivityThread$AppBindData;->initProfileFd:Landroid/os/ParcelFileDescriptor;

    #@33
    .line 758
    const/4 v2, 0x0

    #@34
    iput-boolean v2, v1, Landroid/app/ActivityThread$AppBindData;->initAutoStopProfiler:Z

    #@36
    .line 759
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@38
    const/16 v3, 0x6e

    #@3a
    invoke-static {v2, v3, v1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@3d
    .line 760
    return-void
.end method

.method public clearDnsCache()V
    .registers 1

    #@0
    .prologue
    .line 785
    invoke-static {}, Ljava/net/InetAddress;->clearDnsCache()V

    #@3
    .line 786
    return-void
.end method

.method public dispatchPackageBroadcast(I[Ljava/lang/String;)V
    .registers 5
    .parameter "cmd"
    .parameter "packages"

    #@0
    .prologue
    .line 859
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x85

    #@4
    invoke-static {v0, v1, p2, p1}, Landroid/app/ActivityThread;->access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V

    #@7
    .line 860
    return-void
.end method

.method public dumpActivity(Ljava/io/FileDescriptor;Landroid/os/IBinder;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "activitytoken"
    .parameter "prefix"
    .parameter "args"

    #@0
    .prologue
    .line 868
    new-instance v0, Landroid/app/ActivityThread$DumpComponentInfo;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$DumpComponentInfo;-><init>()V

    #@5
    .line 870
    .local v0, data:Landroid/app/ActivityThread$DumpComponentInfo;
    :try_start_5
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@8
    move-result-object v2

    #@9
    iput-object v2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@b
    .line 871
    iput-object p2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@d
    .line 872
    iput-object p3, v0, Landroid/app/ActivityThread$DumpComponentInfo;->prefix:Ljava/lang/String;

    #@f
    .line 873
    iput-object p4, v0, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@11
    .line 874
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@13
    const/16 v3, 0x88

    #@15
    invoke-static {v2, v3, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_18} :catch_19

    #@18
    .line 878
    :goto_18
    return-void

    #@19
    .line 875
    :catch_19
    move-exception v1

    #@1a
    .line 876
    .local v1, e:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@1c
    const-string v3, "dumpActivity failed"

    #@1e
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_18
.end method

.method public dumpDbInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "args"

    #@0
    .prologue
    .line 1105
    new-instance v1, Ljava/io/PrintWriter;

    #@2
    new-instance v2, Ljava/io/FileOutputStream;

    #@4
    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@7
    invoke-direct {v1, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@a
    .line 1106
    .local v1, pw:Ljava/io/PrintWriter;
    new-instance v0, Landroid/util/PrintWriterPrinter;

    #@c
    invoke-direct {v0, v1}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    #@f
    .line 1107
    .local v0, printer:Landroid/util/PrintWriterPrinter;
    invoke-static {v0, p2}, Landroid/database/sqlite/SQLiteDebug;->dump(Landroid/util/Printer;[Ljava/lang/String;)V

    #@12
    .line 1108
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    #@15
    .line 1109
    return-void
.end method

.method public dumpGfxInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 4
    .parameter "fd"
    .parameter "args"

    #@0
    .prologue
    .line 1099
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    invoke-static {v0, p1}, Landroid/app/ActivityThread;->access$500(Landroid/app/ActivityThread;Ljava/io/FileDescriptor;)V

    #@5
    .line 1100
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0, p1}, Landroid/view/WindowManagerGlobal;->dumpGfxInfo(Ljava/io/FileDescriptor;)V

    #@c
    .line 1101
    return-void
.end method

.method public dumpHeap(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;)V
    .registers 8
    .parameter "managed"
    .parameter "path"
    .parameter "fd"

    #@0
    .prologue
    .line 836
    new-instance v0, Landroid/app/ActivityThread$DumpHeapData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$DumpHeapData;-><init>()V

    #@5
    .line 837
    .local v0, dhd:Landroid/app/ActivityThread$DumpHeapData;
    iput-object p2, v0, Landroid/app/ActivityThread$DumpHeapData;->path:Ljava/lang/String;

    #@7
    .line 838
    iput-object p3, v0, Landroid/app/ActivityThread$DumpHeapData;->fd:Landroid/os/ParcelFileDescriptor;

    #@9
    .line 839
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v3, 0x87

    #@d
    if-eqz p1, :cond_14

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    invoke-static {v2, v3, v0, v1}, Landroid/app/ActivityThread;->access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V

    #@13
    .line 840
    return-void

    #@14
    .line 839
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_10
.end method

.method public dumpMemInfo(Ljava/io/FileDescriptor;ZZ[Ljava/lang/String;)Landroid/os/Debug$MemoryInfo;
    .registers 8
    .parameter "fd"
    .parameter "checkin"
    .parameter "all"
    .parameter "args"

    #@0
    .prologue
    .line 896
    new-instance v0, Ljava/io/FileOutputStream;

    #@2
    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@5
    .line 897
    .local v0, fout:Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/PrintWriter;

    #@7
    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    #@a
    .line 899
    .local v1, pw:Ljava/io/PrintWriter;
    :try_start_a
    invoke-direct {p0, v1, p2, p3}, Landroid/app/ActivityThread$ApplicationThread;->dumpMemInfo(Ljava/io/PrintWriter;ZZ)Landroid/os/Debug$MemoryInfo;
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_12

    #@d
    move-result-object v2

    #@e
    .line 901
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    #@11
    .line 899
    return-object v2

    #@12
    .line 901
    :catchall_12
    move-exception v2

    #@13
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    #@16
    throw v2
.end method

.method public dumpProvider(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "providertoken"
    .parameter "args"

    #@0
    .prologue
    .line 882
    new-instance v0, Landroid/app/ActivityThread$DumpComponentInfo;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$DumpComponentInfo;-><init>()V

    #@5
    .line 884
    .local v0, data:Landroid/app/ActivityThread$DumpComponentInfo;
    :try_start_5
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@8
    move-result-object v2

    #@9
    iput-object v2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@b
    .line 885
    iput-object p2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@d
    .line 886
    iput-object p3, v0, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@f
    .line 887
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@11
    const/16 v3, 0x8d

    #@13
    invoke-static {v2, v3, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_16} :catch_17

    #@16
    .line 891
    :goto_16
    return-void

    #@17
    .line 888
    :catch_17
    move-exception v1

    #@18
    .line 889
    .local v1, e:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@1a
    const-string v3, "dumpProvider failed"

    #@1c
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_16
.end method

.method public dumpService(Ljava/io/FileDescriptor;Landroid/os/IBinder;[Ljava/lang/String;)V
    .registers 8
    .parameter "fd"
    .parameter "servicetoken"
    .parameter "args"

    #@0
    .prologue
    .line 798
    new-instance v0, Landroid/app/ActivityThread$DumpComponentInfo;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$DumpComponentInfo;-><init>()V

    #@5
    .line 800
    .local v0, data:Landroid/app/ActivityThread$DumpComponentInfo;
    :try_start_5
    invoke-static {p1}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@8
    move-result-object v2

    #@9
    iput-object v2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->fd:Landroid/os/ParcelFileDescriptor;

    #@b
    .line 801
    iput-object p2, v0, Landroid/app/ActivityThread$DumpComponentInfo;->token:Landroid/os/IBinder;

    #@d
    .line 802
    iput-object p3, v0, Landroid/app/ActivityThread$DumpComponentInfo;->args:[Ljava/lang/String;

    #@f
    .line 803
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@11
    const/16 v3, 0x7b

    #@13
    invoke-static {v2, v3, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_16} :catch_17

    #@16
    .line 807
    :goto_16
    return-void

    #@17
    .line 804
    :catch_17
    move-exception v1

    #@18
    .line 805
    .local v1, e:Ljava/io/IOException;
    const-string v2, "ActivityThread"

    #@1a
    const-string v3, "dumpService failed"

    #@1c
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_16
.end method

.method public getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V
    .registers 2
    .parameter "outInfo"

    #@0
    .prologue
    .line 855
    invoke-static {p1}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    #@3
    .line 856
    return-void
.end method

.method public processInBackground()V
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x78

    #@2
    .line 793
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@4
    iget-object v0, v0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@6
    invoke-virtual {v0, v2}, Landroid/app/ActivityThread$H;->removeMessages(I)V

    #@9
    .line 794
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    iget-object v0, v0, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@d
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@f
    iget-object v1, v1, Landroid/app/ActivityThread;->mH:Landroid/app/ActivityThread$H;

    #@11
    invoke-virtual {v1, v2}, Landroid/app/ActivityThread$H;->obtainMessage(I)Landroid/os/Message;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/app/ActivityThread$H;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 795
    return-void
.end method

.method public profilerControl(ZLjava/lang/String;Landroid/os/ParcelFileDescriptor;I)V
    .registers 9
    .parameter "start"
    .parameter "path"
    .parameter "fd"
    .parameter "profileType"

    #@0
    .prologue
    .line 829
    new-instance v0, Landroid/app/ActivityThread$ProfilerControlData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$ProfilerControlData;-><init>()V

    #@5
    .line 830
    .local v0, pcd:Landroid/app/ActivityThread$ProfilerControlData;
    iput-object p2, v0, Landroid/app/ActivityThread$ProfilerControlData;->path:Ljava/lang/String;

    #@7
    .line 831
    iput-object p3, v0, Landroid/app/ActivityThread$ProfilerControlData;->fd:Landroid/os/ParcelFileDescriptor;

    #@9
    .line 832
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v3, 0x7f

    #@d
    if-eqz p1, :cond_14

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    invoke-static {v2, v3, v0, v1, p4}, Landroid/app/ActivityThread;->access$200(Landroid/app/ActivityThread;ILjava/lang/Object;II)V

    #@13
    .line 833
    return-void

    #@14
    .line 832
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_10
.end method

.method public requestThumbnail(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 771
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x75

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 772
    return-void
.end method

.method public scheduleActivityConfigurationChanged(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 824
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x7d

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 825
    return-void
.end method

.method public final scheduleBindService(Landroid/os/IBinder;Landroid/content/Intent;Z)V
    .registers 7
    .parameter "token"
    .parameter "intent"
    .parameter "rebind"

    #@0
    .prologue
    .line 692
    new-instance v0, Landroid/app/ActivityThread$BindServiceData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$BindServiceData;-><init>()V

    #@5
    .line 693
    .local v0, s:Landroid/app/ActivityThread$BindServiceData;
    iput-object p1, v0, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@7
    .line 694
    iput-object p2, v0, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@9
    .line 695
    iput-boolean p3, v0, Landroid/app/ActivityThread$BindServiceData;->rebind:Z

    #@b
    .line 700
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@d
    const/16 v2, 0x79

    #@f
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@12
    .line 701
    return-void
.end method

.method public scheduleConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 775
    invoke-direct {p0, p1}, Landroid/app/ActivityThread$ApplicationThread;->updatePendingConfiguration(Landroid/content/res/Configuration;)V

    #@3
    .line 776
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@5
    const/16 v1, 0x76

    #@7
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@a
    .line 777
    return-void
.end method

.method public scheduleCrash(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 863
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x86

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 864
    return-void
.end method

.method public final scheduleCreateBackupAgent(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)V
    .registers 7
    .parameter "app"
    .parameter "compatInfo"
    .parameter "backupMode"

    #@0
    .prologue
    .line 663
    new-instance v0, Landroid/app/ActivityThread$CreateBackupAgentData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$CreateBackupAgentData;-><init>()V

    #@5
    .line 664
    .local v0, d:Landroid/app/ActivityThread$CreateBackupAgentData;
    iput-object p1, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@7
    .line 665
    iput-object p2, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@9
    .line 666
    iput p3, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->backupMode:I

    #@b
    .line 668
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@d
    const/16 v2, 0x80

    #@f
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@12
    .line 669
    return-void
.end method

.method public final scheduleCreateService(Landroid/os/IBinder;Landroid/content/pm/ServiceInfo;Landroid/content/res/CompatibilityInfo;)V
    .registers 7
    .parameter "token"
    .parameter "info"
    .parameter "compatInfo"

    #@0
    .prologue
    .line 682
    new-instance v0, Landroid/app/ActivityThread$CreateServiceData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$CreateServiceData;-><init>()V

    #@5
    .line 683
    .local v0, s:Landroid/app/ActivityThread$CreateServiceData;
    iput-object p1, v0, Landroid/app/ActivityThread$CreateServiceData;->token:Landroid/os/IBinder;

    #@7
    .line 684
    iput-object p2, v0, Landroid/app/ActivityThread$CreateServiceData;->info:Landroid/content/pm/ServiceInfo;

    #@9
    .line 685
    iput-object p3, v0, Landroid/app/ActivityThread$CreateServiceData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@b
    .line 687
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@d
    const/16 v2, 0x72

    #@f
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@12
    .line 688
    return-void
.end method

.method public final scheduleDestroyActivity(Landroid/os/IBinder;ZI)V
    .registers 7
    .parameter "token"
    .parameter "finishing"
    .parameter "configChanges"

    #@0
    .prologue
    .line 647
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v2, 0x6d

    #@4
    if-eqz p2, :cond_b

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-static {v1, v2, p1, v0, p3}, Landroid/app/ActivityThread;->access$200(Landroid/app/ActivityThread;ILjava/lang/Object;II)V

    #@a
    .line 649
    return-void

    #@b
    .line 647
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7
.end method

.method public final scheduleDestroyBackupAgent(Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V
    .registers 6
    .parameter "app"
    .parameter "compatInfo"

    #@0
    .prologue
    .line 673
    new-instance v0, Landroid/app/ActivityThread$CreateBackupAgentData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$CreateBackupAgentData;-><init>()V

    #@5
    .line 674
    .local v0, d:Landroid/app/ActivityThread$CreateBackupAgentData;
    iput-object p1, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->appInfo:Landroid/content/pm/ApplicationInfo;

    #@7
    .line 675
    iput-object p2, v0, Landroid/app/ActivityThread$CreateBackupAgentData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@9
    .line 677
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v2, 0x81

    #@d
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@10
    .line 678
    return-void
.end method

.method public final scheduleExit()V
    .registers 4

    #@0
    .prologue
    .line 763
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x6f

    #@4
    const/4 v2, 0x0

    #@5
    invoke-static {v0, v1, v2}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@8
    .line 764
    return-void
.end method

.method public final scheduleLaunchActivity(Landroid/content/Intent;Landroid/os/IBinder;ILandroid/content/pm/ActivityInfo;Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Landroid/os/Bundle;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Landroid/os/ParcelFileDescriptor;Z)V
    .registers 19
    .parameter "intent"
    .parameter "token"
    .parameter "ident"
    .parameter "info"
    .parameter "curConfig"
    .parameter "compatInfo"
    .parameter "state"
    .parameter
    .parameter
    .parameter "notResumed"
    .parameter "isForward"
    .parameter "profileName"
    .parameter "profileFd"
    .parameter "autoStopProfiler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Landroid/os/IBinder;",
            "I",
            "Landroid/content/pm/ActivityInfo;",
            "Landroid/content/res/Configuration;",
            "Landroid/content/res/CompatibilityInfo;",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ResultInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Landroid/os/ParcelFileDescriptor;",
            "Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 606
    .local p8, pendingResults:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .local p9, pendingNewIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v1, Landroid/app/ActivityThread$ActivityClientRecord;

    #@2
    invoke-direct {v1}, Landroid/app/ActivityThread$ActivityClientRecord;-><init>()V

    #@5
    .line 608
    .local v1, r:Landroid/app/ActivityThread$ActivityClientRecord;
    iput-object p2, v1, Landroid/app/ActivityThread$ActivityClientRecord;->token:Landroid/os/IBinder;

    #@7
    .line 609
    iput p3, v1, Landroid/app/ActivityThread$ActivityClientRecord;->ident:I

    #@9
    .line 610
    iput-object p1, v1, Landroid/app/ActivityThread$ActivityClientRecord;->intent:Landroid/content/Intent;

    #@b
    .line 611
    iput-object p4, v1, Landroid/app/ActivityThread$ActivityClientRecord;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@d
    .line 612
    iput-object p6, v1, Landroid/app/ActivityThread$ActivityClientRecord;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@f
    .line 613
    iput-object p7, v1, Landroid/app/ActivityThread$ActivityClientRecord;->state:Landroid/os/Bundle;

    #@11
    .line 615
    iput-object p8, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingResults:Ljava/util/List;

    #@13
    .line 616
    iput-object p9, v1, Landroid/app/ActivityThread$ActivityClientRecord;->pendingIntents:Ljava/util/List;

    #@15
    .line 618
    iput-boolean p10, v1, Landroid/app/ActivityThread$ActivityClientRecord;->startsNotResumed:Z

    #@17
    .line 619
    iput-boolean p11, v1, Landroid/app/ActivityThread$ActivityClientRecord;->isForward:Z

    #@19
    .line 621
    move-object/from16 v0, p12

    #@1b
    iput-object v0, v1, Landroid/app/ActivityThread$ActivityClientRecord;->profileFile:Ljava/lang/String;

    #@1d
    .line 622
    move-object/from16 v0, p13

    #@1f
    iput-object v0, v1, Landroid/app/ActivityThread$ActivityClientRecord;->profileFd:Landroid/os/ParcelFileDescriptor;

    #@21
    .line 623
    move/from16 v0, p14

    #@23
    iput-boolean v0, v1, Landroid/app/ActivityThread$ActivityClientRecord;->autoStopProfiler:Z

    #@25
    .line 625
    invoke-direct {p0, p5}, Landroid/app/ActivityThread$ApplicationThread;->updatePendingConfiguration(Landroid/content/res/Configuration;)V

    #@28
    .line 627
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2a
    const/16 v3, 0x64

    #@2c
    invoke-static {v2, v3, v1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@2f
    .line 628
    return-void
.end method

.method public scheduleLowMemory()V
    .registers 4

    #@0
    .prologue
    .line 820
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x7c

    #@4
    const/4 v2, 0x0

    #@5
    invoke-static {v0, v1, v2}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@8
    .line 821
    return-void
.end method

.method public final scheduleNewIntent(Ljava/util/List;Landroid/os/IBinder;)V
    .registers 6
    .parameter
    .parameter "token"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/os/IBinder;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 638
    .local p1, intents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    new-instance v0, Landroid/app/ActivityThread$NewIntentData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$NewIntentData;-><init>()V

    #@5
    .line 639
    .local v0, data:Landroid/app/ActivityThread$NewIntentData;
    iput-object p1, v0, Landroid/app/ActivityThread$NewIntentData;->intents:Ljava/util/List;

    #@7
    .line 640
    iput-object p2, v0, Landroid/app/ActivityThread$NewIntentData;->token:Landroid/os/IBinder;

    #@9
    .line 642
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v2, 0x70

    #@d
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@10
    .line 643
    return-void
.end method

.method public final schedulePauseActivity(Landroid/os/IBinder;ZZI)V
    .registers 8
    .parameter "token"
    .parameter "finished"
    .parameter "userLeaving"
    .parameter "configChanges"

    #@0
    .prologue
    .line 564
    iget-object v2, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    if-eqz p2, :cond_e

    #@4
    const/16 v0, 0x66

    #@6
    move v1, v0

    #@7
    :goto_7
    if-eqz p3, :cond_12

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    invoke-static {v2, v1, p1, v0, p4}, Landroid/app/ActivityThread;->access$200(Landroid/app/ActivityThread;ILjava/lang/Object;II)V

    #@d
    .line 569
    return-void

    #@e
    .line 564
    :cond_e
    const/16 v0, 0x65

    #@10
    move v1, v0

    #@11
    goto :goto_7

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_a
.end method

.method public final scheduleReceiver(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Landroid/content/res/CompatibilityInfo;ILjava/lang/String;Landroid/os/Bundle;ZI)V
    .registers 18
    .parameter "intent"
    .parameter "info"
    .parameter "compatInfo"
    .parameter "resultCode"
    .parameter "data"
    .parameter "extras"
    .parameter "sync"
    .parameter "sendingUser"

    #@0
    .prologue
    .line 654
    new-instance v0, Landroid/app/ActivityThread$ReceiverData;

    #@2
    const/4 v6, 0x0

    #@3
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@5
    iget-object v1, v1, Landroid/app/ActivityThread;->mAppThread:Landroid/app/ActivityThread$ApplicationThread;

    #@7
    invoke-virtual {v1}, Landroid/app/ActivityThread$ApplicationThread;->asBinder()Landroid/os/IBinder;

    #@a
    move-result-object v7

    #@b
    move-object v1, p1

    #@c
    move v2, p4

    #@d
    move-object v3, p5

    #@e
    move-object v4, p6

    #@f
    move/from16 v5, p7

    #@11
    move/from16 v8, p8

    #@13
    invoke-direct/range {v0 .. v8}, Landroid/app/ActivityThread$ReceiverData;-><init>(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZLandroid/os/IBinder;I)V

    #@16
    .line 656
    .local v0, r:Landroid/app/ActivityThread$ReceiverData;
    iput-object p2, v0, Landroid/app/ActivityThread$ReceiverData;->info:Landroid/content/pm/ActivityInfo;

    #@18
    .line 657
    iput-object p3, v0, Landroid/app/ActivityThread$ReceiverData;->compatInfo:Landroid/content/res/CompatibilityInfo;

    #@1a
    .line 658
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@1c
    const/16 v2, 0x71

    #@1e
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@21
    .line 659
    return-void
.end method

.method public scheduleRegisteredReceiver(Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    .registers 9
    .parameter "receiver"
    .parameter "intent"
    .parameter "resultCode"
    .parameter "dataStr"
    .parameter "extras"
    .parameter "ordered"
    .parameter "sticky"
    .parameter "sendingUser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 815
    invoke-interface/range {p1 .. p8}, Landroid/content/IIntentReceiver;->performReceive(Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V

    #@3
    .line 817
    return-void
.end method

.method public final scheduleRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;)V
    .registers 15
    .parameter "token"
    .parameter
    .parameter
    .parameter "configChanges"
    .parameter "notResumed"
    .parameter "config"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ResultInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;IZ",
            "Landroid/content/res/Configuration;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 633
    .local p2, pendingResults:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    .local p3, pendingNewIntents:Ljava/util/List;,"Ljava/util/List<Landroid/content/Intent;>;"
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/4 v7, 0x1

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move v4, p4

    #@7
    move v5, p5

    #@8
    move-object v6, p6

    #@9
    invoke-virtual/range {v0 .. v7}, Landroid/app/ActivityThread;->requestRelaunchActivity(Landroid/os/IBinder;Ljava/util/List;Ljava/util/List;IZLandroid/content/res/Configuration;Z)V

    #@c
    .line 635
    return-void
.end method

.method public final scheduleResumeActivity(Landroid/os/IBinder;Z)V
    .registers 6
    .parameter "token"
    .parameter "isForward"

    #@0
    .prologue
    .line 589
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v2, 0x6b

    #@4
    if-eqz p2, :cond_b

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-static {v1, v2, p1, v0}, Landroid/app/ActivityThread;->access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V

    #@a
    .line 590
    return-void

    #@b
    .line 589
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7
.end method

.method public final scheduleSendResult(Landroid/os/IBinder;Ljava/util/List;)V
    .registers 6
    .parameter "token"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ResultInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 593
    .local p2, results:Ljava/util/List;,"Ljava/util/List<Landroid/app/ResultInfo;>;"
    new-instance v0, Landroid/app/ActivityThread$ResultData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$ResultData;-><init>()V

    #@5
    .line 594
    .local v0, res:Landroid/app/ActivityThread$ResultData;
    iput-object p1, v0, Landroid/app/ActivityThread$ResultData;->token:Landroid/os/IBinder;

    #@7
    .line 595
    iput-object p2, v0, Landroid/app/ActivityThread$ResultData;->results:Ljava/util/List;

    #@9
    .line 596
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v2, 0x6c

    #@d
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@10
    .line 597
    return-void
.end method

.method public final scheduleServiceArgs(Landroid/os/IBinder;ZIILandroid/content/Intent;)V
    .registers 9
    .parameter "token"
    .parameter "taskRemoved"
    .parameter "startId"
    .parameter "flags"
    .parameter "args"

    #@0
    .prologue
    .line 713
    new-instance v0, Landroid/app/ActivityThread$ServiceArgsData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$ServiceArgsData;-><init>()V

    #@5
    .line 714
    .local v0, s:Landroid/app/ActivityThread$ServiceArgsData;
    iput-object p1, v0, Landroid/app/ActivityThread$ServiceArgsData;->token:Landroid/os/IBinder;

    #@7
    .line 715
    iput-boolean p2, v0, Landroid/app/ActivityThread$ServiceArgsData;->taskRemoved:Z

    #@9
    .line 716
    iput p3, v0, Landroid/app/ActivityThread$ServiceArgsData;->startId:I

    #@b
    .line 717
    iput p4, v0, Landroid/app/ActivityThread$ServiceArgsData;->flags:I

    #@d
    .line 718
    iput-object p5, v0, Landroid/app/ActivityThread$ServiceArgsData;->args:Landroid/content/Intent;

    #@f
    .line 720
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@11
    const/16 v2, 0x73

    #@13
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@16
    .line 721
    return-void
.end method

.method public final scheduleSleeping(Landroid/os/IBinder;Z)V
    .registers 6
    .parameter "token"
    .parameter "sleeping"

    #@0
    .prologue
    .line 585
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v2, 0x89

    #@4
    if-eqz p2, :cond_b

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    invoke-static {v1, v2, p1, v0}, Landroid/app/ActivityThread;->access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V

    #@a
    .line 586
    return-void

    #@b
    .line 585
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_7
.end method

.method public final scheduleStopActivity(Landroid/os/IBinder;ZI)V
    .registers 7
    .parameter "token"
    .parameter "showWindow"
    .parameter "configChanges"

    #@0
    .prologue
    .line 573
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    if-eqz p2, :cond_b

    #@4
    const/16 v0, 0x67

    #@6
    :goto_6
    const/4 v2, 0x0

    #@7
    invoke-static {v1, v0, p1, v2, p3}, Landroid/app/ActivityThread;->access$200(Landroid/app/ActivityThread;ILjava/lang/Object;II)V

    #@a
    .line 576
    return-void

    #@b
    .line 573
    :cond_b
    const/16 v0, 0x68

    #@d
    goto :goto_6
.end method

.method public final scheduleStopService(Landroid/os/IBinder;)V
    .registers 4
    .parameter "token"

    #@0
    .prologue
    .line 724
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x74

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 725
    return-void
.end method

.method public final scheduleSuicide()V
    .registers 4

    #@0
    .prologue
    .line 767
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x82

    #@4
    const/4 v2, 0x0

    #@5
    invoke-static {v0, v1, v2}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@8
    .line 768
    return-void
.end method

.method public scheduleTrimMemory(I)V
    .registers 5
    .parameter "level"

    #@0
    .prologue
    .line 1132
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x8c

    #@4
    const/4 v2, 0x0

    #@5
    invoke-static {v0, v1, v2, p1}, Landroid/app/ActivityThread;->access$400(Landroid/app/ActivityThread;ILjava/lang/Object;I)V

    #@8
    .line 1133
    return-void
.end method

.method public final scheduleUnbindService(Landroid/os/IBinder;Landroid/content/Intent;)V
    .registers 6
    .parameter "token"
    .parameter "intent"

    #@0
    .prologue
    .line 704
    new-instance v0, Landroid/app/ActivityThread$BindServiceData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$BindServiceData;-><init>()V

    #@5
    .line 705
    .local v0, s:Landroid/app/ActivityThread$BindServiceData;
    iput-object p1, v0, Landroid/app/ActivityThread$BindServiceData;->token:Landroid/os/IBinder;

    #@7
    .line 706
    iput-object p2, v0, Landroid/app/ActivityThread$BindServiceData;->intent:Landroid/content/Intent;

    #@9
    .line 708
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v2, 0x7a

    #@d
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@10
    .line 709
    return-void
.end method

.method public final scheduleWindowVisibility(Landroid/os/IBinder;Z)V
    .registers 5
    .parameter "token"
    .parameter "showWindow"

    #@0
    .prologue
    .line 579
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    if-eqz p2, :cond_a

    #@4
    const/16 v0, 0x69

    #@6
    :goto_6
    invoke-static {v1, v0, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@9
    .line 582
    return-void

    #@a
    .line 579
    :cond_a
    const/16 v0, 0x6a

    #@c
    goto :goto_6
.end method

.method public setCoreSettings(Landroid/os/Bundle;)V
    .registers 4
    .parameter "coreSettings"

    #@0
    .prologue
    .line 1121
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x8a

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 1122
    return-void
.end method

.method public setHttpProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "host"
    .parameter "port"
    .parameter "exclList"

    #@0
    .prologue
    .line 789
    invoke-static {p1, p2, p3}, Landroid/net/Proxy;->setHttpProxySystemProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@3
    .line 790
    return-void
.end method

.method public setSchedulingGroup(I)V
    .registers 6
    .parameter "group"

    #@0
    .prologue
    .line 848
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@3
    move-result v1

    #@4
    invoke-static {v1, p1}, Landroid/os/Process;->setProcessGroup(II)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 852
    :goto_7
    return-void

    #@8
    .line 849
    :catch_8
    move-exception v0

    #@9
    .line 850
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "ActivityThread"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Failed setting process group to "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_7
.end method

.method public unstableProviderDied(Landroid/os/IBinder;)V
    .registers 4
    .parameter "provider"

    #@0
    .prologue
    .line 1113
    iget-object v0, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@2
    const/16 v1, 0x8e

    #@4
    invoke-static {v0, v1, p1}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@7
    .line 1114
    return-void
.end method

.method public updatePackageCompatibilityInfo(Ljava/lang/String;Landroid/content/res/CompatibilityInfo;)V
    .registers 6
    .parameter "pkg"
    .parameter "info"

    #@0
    .prologue
    .line 1125
    new-instance v0, Landroid/app/ActivityThread$UpdateCompatibilityData;

    #@2
    invoke-direct {v0}, Landroid/app/ActivityThread$UpdateCompatibilityData;-><init>()V

    #@5
    .line 1126
    .local v0, ucd:Landroid/app/ActivityThread$UpdateCompatibilityData;
    iput-object p1, v0, Landroid/app/ActivityThread$UpdateCompatibilityData;->pkg:Ljava/lang/String;

    #@7
    .line 1127
    iput-object p2, v0, Landroid/app/ActivityThread$UpdateCompatibilityData;->info:Landroid/content/res/CompatibilityInfo;

    #@9
    .line 1128
    iget-object v1, p0, Landroid/app/ActivityThread$ApplicationThread;->this$0:Landroid/app/ActivityThread;

    #@b
    const/16 v2, 0x8b

    #@d
    invoke-static {v1, v2, v0}, Landroid/app/ActivityThread;->access$300(Landroid/app/ActivityThread;ILjava/lang/Object;)V

    #@10
    .line 1129
    return-void
.end method

.method public updateTimeZone()V
    .registers 2

    #@0
    .prologue
    .line 780
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Ljava/util/TimeZone;->setDefault(Ljava/util/TimeZone;)V

    #@4
    .line 781
    return-void
.end method
