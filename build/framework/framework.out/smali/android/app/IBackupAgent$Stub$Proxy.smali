.class Landroid/app/IBackupAgent$Stub$Proxy;
.super Ljava/lang/Object;
.source "IBackupAgent.java"

# interfaces
.implements Landroid/app/IBackupAgent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IBackupAgent$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 162
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 163
    iput-object p1, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 164
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public doBackup(Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 11
    .parameter "oldState"
    .parameter "data"
    .parameter "newState"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 196
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 198
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.app.IBackupAgent"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 199
    if-eqz p1, :cond_40

    #@c
    .line 200
    const/4 v2, 0x1

    #@d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 201
    const/4 v2, 0x0

    #@11
    invoke-virtual {p1, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 206
    :goto_14
    if-eqz p2, :cond_4a

    #@16
    .line 207
    const/4 v2, 0x1

    #@17
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 208
    const/4 v2, 0x0

    #@1b
    invoke-virtual {p2, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@1e
    .line 213
    :goto_1e
    if-eqz p3, :cond_4f

    #@20
    .line 214
    const/4 v2, 0x1

    #@21
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 215
    const/4 v2, 0x0

    #@25
    invoke-virtual {p3, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    .line 220
    :goto_28
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    .line 221
    if-eqz p5, :cond_31

    #@2d
    invoke-interface {p5}, Landroid/app/backup/IBackupManager;->asBinder()Landroid/os/IBinder;

    #@30
    move-result-object v1

    #@31
    :cond_31
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@34
    .line 222
    iget-object v1, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@36
    const/4 v2, 0x1

    #@37
    const/4 v3, 0x0

    #@38
    const/4 v4, 0x1

    #@39
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3c
    .catchall {:try_start_5 .. :try_end_3c} :catchall_45

    #@3c
    .line 225
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 227
    return-void

    #@40
    .line 204
    :cond_40
    const/4 v2, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_45

    #@44
    goto :goto_14

    #@45
    .line 225
    :catchall_45
    move-exception v1

    #@46
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    throw v1

    #@4a
    .line 211
    :cond_4a
    const/4 v2, 0x0

    #@4b
    :try_start_4b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@4e
    goto :goto_1e

    #@4f
    .line 218
    :cond_4f
    const/4 v2, 0x0

    #@50
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_53
    .catchall {:try_start_4b .. :try_end_53} :catchall_45

    #@53
    goto :goto_28
.end method

.method public doFullBackup(Landroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 9
    .parameter "data"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 298
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 300
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.app.IBackupAgent"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 301
    if-eqz p1, :cond_2c

    #@c
    .line 302
    const/4 v2, 0x1

    #@d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 303
    const/4 v2, 0x0

    #@11
    invoke-virtual {p1, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 308
    :goto_14
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 309
    if-eqz p3, :cond_1d

    #@19
    invoke-interface {p3}, Landroid/app/backup/IBackupManager;->asBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v1

    #@1d
    :cond_1d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@20
    .line 310
    iget-object v1, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@22
    const/4 v2, 0x3

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x1

    #@25
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_28
    .catchall {:try_start_5 .. :try_end_28} :catchall_31

    #@28
    .line 313
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 315
    return-void

    #@2c
    .line 306
    :cond_2c
    const/4 v2, 0x0

    #@2d
    :try_start_2d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_31

    #@30
    goto :goto_14

    #@31
    .line 313
    :catchall_31
    move-exception v1

    #@32
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v1
.end method

.method public doRestore(Landroid/os/ParcelFileDescriptor;ILandroid/os/ParcelFileDescriptor;ILandroid/app/backup/IBackupManager;)V
    .registers 11
    .parameter "data"
    .parameter "appVersionCode"
    .parameter "newState"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 253
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 255
    .local v0, _data:Landroid/os/Parcel;
    :try_start_5
    const-string v2, "android.app.IBackupAgent"

    #@7
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@a
    .line 256
    if-eqz p1, :cond_39

    #@c
    .line 257
    const/4 v2, 0x1

    #@d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 258
    const/4 v2, 0x0

    #@11
    invoke-virtual {p1, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 263
    :goto_14
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 264
    if-eqz p3, :cond_43

    #@19
    .line 265
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 266
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p3, v0, v2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 271
    :goto_21
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 272
    if-eqz p5, :cond_2a

    #@26
    invoke-interface {p5}, Landroid/app/backup/IBackupManager;->asBinder()Landroid/os/IBinder;

    #@29
    move-result-object v1

    #@2a
    :cond_2a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2d
    .line 273
    iget-object v1, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2f
    const/4 v2, 0x2

    #@30
    const/4 v3, 0x0

    #@31
    const/4 v4, 0x1

    #@32
    invoke-interface {v1, v2, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_35
    .catchall {:try_start_5 .. :try_end_35} :catchall_3e

    #@35
    .line 276
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 278
    return-void

    #@39
    .line 261
    :cond_39
    const/4 v2, 0x0

    #@3a
    :try_start_3a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3d
    .catchall {:try_start_3a .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_14

    #@3e
    .line 276
    :catchall_3e
    move-exception v1

    #@3f
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v1

    #@43
    .line 269
    :cond_43
    const/4 v2, 0x0

    #@44
    :try_start_44
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_3e

    #@47
    goto :goto_21
.end method

.method public doRestoreFile(Landroid/os/ParcelFileDescriptor;JILjava/lang/String;Ljava/lang/String;JJILandroid/app/backup/IBackupManager;)V
    .registers 20
    .parameter "data"
    .parameter "size"
    .parameter "type"
    .parameter "domain"
    .parameter "path"
    .parameter "mode"
    .parameter "mtime"
    .parameter "token"
    .parameter "callbackBinder"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 333
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v2

    #@4
    .line 335
    .local v2, _data:Landroid/os/Parcel;
    :try_start_4
    const-string v3, "android.app.IBackupAgent"

    #@6
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@9
    .line 336
    if-eqz p1, :cond_41

    #@b
    .line 337
    const/4 v3, 0x1

    #@c
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 338
    const/4 v3, 0x0

    #@10
    invoke-virtual {p1, v2, v3}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@13
    .line 343
    :goto_13
    invoke-virtual {v2, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@16
    .line 344
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 345
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1c
    .line 346
    invoke-virtual {v2, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 347
    invoke-virtual {v2, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@22
    .line 348
    move-wide/from16 v0, p9

    #@24
    invoke-virtual {v2, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@27
    .line 349
    move/from16 v0, p11

    #@29
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 350
    if-eqz p12, :cond_4b

    #@2e
    invoke-interface/range {p12 .. p12}, Landroid/app/backup/IBackupManager;->asBinder()Landroid/os/IBinder;

    #@31
    move-result-object v3

    #@32
    :goto_32
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@35
    .line 351
    iget-object v3, p0, Landroid/app/IBackupAgent$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@37
    const/4 v4, 0x4

    #@38
    const/4 v5, 0x0

    #@39
    const/4 v6, 0x1

    #@3a
    invoke-interface {v3, v4, v2, v5, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_3d
    .catchall {:try_start_4 .. :try_end_3d} :catchall_46

    #@3d
    .line 354
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 356
    return-void

    #@41
    .line 341
    :cond_41
    const/4 v3, 0x0

    #@42
    :try_start_42
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_46

    #@45
    goto :goto_13

    #@46
    .line 354
    :catchall_46
    move-exception v3

    #@47
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v3

    #@4b
    .line 350
    :cond_4b
    const/4 v3, 0x0

    #@4c
    goto :goto_32
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 171
    const-string v0, "android.app.IBackupAgent"

    #@2
    return-object v0
.end method
