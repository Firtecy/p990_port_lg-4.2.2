.class public Landroid/app/TimePickerDialog;
.super Landroid/app/AlertDialog;
.source "TimePickerDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/TimePickerDialog$OnTimeSetListener;
    }
.end annotation


# static fields
.field private static final HOUR:Ljava/lang/String; = "hour"

.field private static final IS_24_HOUR:Ljava/lang/String; = "is24hour"

.field private static final MINUTE:Ljava/lang/String; = "minute"


# instance fields
.field private final mCallback:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field mInitialHourOfDay:I

.field mInitialMinute:I

.field mIs24HourView:Z

.field private final mTimePicker:Landroid/widget/TimePicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V
    .registers 12
    .parameter "context"
    .parameter "theme"
    .parameter "callBack"
    .parameter "hourOfDay"
    .parameter "minute"
    .parameter "is24HourView"

    #@0
    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    #@3
    .line 90
    iput-object p3, p0, Landroid/app/TimePickerDialog;->mCallback:Landroid/app/TimePickerDialog$OnTimeSetListener;

    #@5
    .line 91
    iput p4, p0, Landroid/app/TimePickerDialog;->mInitialHourOfDay:I

    #@7
    .line 92
    iput p5, p0, Landroid/app/TimePickerDialog;->mInitialMinute:I

    #@9
    .line 93
    iput-boolean p6, p0, Landroid/app/TimePickerDialog;->mIs24HourView:Z

    #@b
    .line 95
    const/4 v3, 0x0

    #@c
    invoke-virtual {p0, v3}, Landroid/app/TimePickerDialog;->setIcon(I)V

    #@f
    .line 96
    const v3, 0x1040457

    #@12
    invoke-virtual {p0, v3}, Landroid/app/TimePickerDialog;->setTitle(I)V

    #@15
    .line 98
    invoke-virtual {p0}, Landroid/app/TimePickerDialog;->getContext()Landroid/content/Context;

    #@18
    move-result-object v1

    #@19
    .line 99
    .local v1, themeContext:Landroid/content/Context;
    const/4 v3, -0x1

    #@1a
    const v4, 0x104045a

    #@1d
    invoke-virtual {v1, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {p0, v3, v4, p0}, Landroid/app/TimePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    #@24
    .line 101
    const-string/jumbo v3, "layout_inflater"

    #@27
    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v0

    #@2b
    check-cast v0, Landroid/view/LayoutInflater;

    #@2d
    .line 103
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v3, 0x10900e0

    #@30
    const/4 v4, 0x0

    #@31
    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@34
    move-result-object v2

    #@35
    .line 104
    .local v2, view:Landroid/view/View;
    invoke-virtual {p0, v2}, Landroid/app/TimePickerDialog;->setView(Landroid/view/View;)V

    #@38
    .line 105
    const v3, 0x10203a6

    #@3b
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Landroid/widget/TimePicker;

    #@41
    iput-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@43
    .line 108
    iget-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@45
    iget-boolean v4, p0, Landroid/app/TimePickerDialog;->mIs24HourView:Z

    #@47
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v3, v4}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    #@4e
    .line 109
    iget-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@50
    iget v4, p0, Landroid/app/TimePickerDialog;->mInitialHourOfDay:I

    #@52
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v3, v4}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@59
    .line 110
    iget-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@5b
    iget v4, p0, Landroid/app/TimePickerDialog;->mInitialMinute:I

    #@5d
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v3, v4}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    #@64
    .line 111
    iget-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@66
    invoke-virtual {v3, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    #@69
    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V
    .registers 13
    .parameter "context"
    .parameter "callBack"
    .parameter "hourOfDay"
    .parameter "minute"
    .parameter "is24HourView"

    #@0
    .prologue
    .line 74
    const/4 v2, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    move v6, p5

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;ILandroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    #@a
    .line 75
    return-void
.end method

.method private tryNotifyTimeSet()V
    .registers 5

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/app/TimePickerDialog;->mCallback:Landroid/app/TimePickerDialog$OnTimeSetListener;

    #@2
    if-eqz v0, :cond_24

    #@4
    .line 129
    iget-object v0, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@6
    invoke-virtual {v0}, Landroid/widget/TimePicker;->clearFocus()V

    #@9
    .line 130
    iget-object v0, p0, Landroid/app/TimePickerDialog;->mCallback:Landroid/app/TimePickerDialog$OnTimeSetListener;

    #@b
    iget-object v1, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@d
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@f
    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@16
    move-result v2

    #@17
    iget-object v3, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@19
    invoke-virtual {v3}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@20
    move-result v3

    #@21
    invoke-interface {v0, v1, v2, v3}, Landroid/app/TimePickerDialog$OnTimeSetListener;->onTimeSet(Landroid/widget/TimePicker;II)V

    #@24
    .line 133
    :cond_24
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 115
    invoke-direct {p0}, Landroid/app/TimePickerDialog;->tryNotifyTimeSet()V

    #@3
    .line 116
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 153
    const-string v2, "hour"

    #@5
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@8
    move-result v0

    #@9
    .line 154
    .local v0, hour:I
    const-string/jumbo v2, "minute"

    #@c
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@f
    move-result v1

    #@10
    .line 155
    .local v1, minute:I
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@12
    const-string v3, "is24hour"

    #@14
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@17
    move-result v3

    #@18
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v2, v3}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    #@1f
    .line 156
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v2, v3}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@28
    .line 157
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@2a
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v2, v3}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    #@31
    .line 158
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 143
    invoke-super {p0}, Landroid/app/AlertDialog;->onSaveInstanceState()Landroid/os/Bundle;

    #@3
    move-result-object v0

    #@4
    .line 144
    .local v0, state:Landroid/os/Bundle;
    const-string v1, "hour"

    #@6
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@8
    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@f
    move-result v2

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 145
    const-string/jumbo v1, "minute"

    #@16
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@18
    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@1f
    move-result v2

    #@20
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@23
    .line 146
    const-string v1, "is24hour"

    #@25
    iget-object v2, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@27
    invoke-virtual {v2}, Landroid/widget/TimePicker;->is24HourView()Z

    #@2a
    move-result v2

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@2e
    .line 147
    return-object v0
.end method

.method protected onStop()V
    .registers 1

    #@0
    .prologue
    .line 137
    invoke-direct {p0}, Landroid/app/TimePickerDialog;->tryNotifyTimeSet()V

    #@3
    .line 138
    invoke-super {p0}, Landroid/app/AlertDialog;->onStop()V

    #@6
    .line 139
    return-void
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .registers 4
    .parameter "view"
    .parameter "hourOfDay"
    .parameter "minute"

    #@0
    .prologue
    .line 125
    return-void
.end method

.method public updateTime(II)V
    .registers 5
    .parameter "hourOfDay"
    .parameter "minutOfHour"

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    #@9
    .line 120
    iget-object v0, p0, Landroid/app/TimePickerDialog;->mTimePicker:Landroid/widget/TimePicker;

    #@b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    #@12
    .line 121
    return-void
.end method
