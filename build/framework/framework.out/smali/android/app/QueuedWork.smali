.class public Landroid/app/QueuedWork;
.super Ljava/lang/Object;
.source "QueuedWork.java"


# static fields
.field private static final sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    #@5
    sput-object v0, Landroid/app/QueuedWork;->sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@7
    .line 41
    const/4 v0, 0x0

    #@8
    sput-object v0, Landroid/app/QueuedWork;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static add(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "finisher"

    #@0
    .prologue
    .line 70
    sget-object v0, Landroid/app/QueuedWork;->sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    #@5
    .line 71
    return-void
.end method

.method public static hasPendingWork()Z
    .registers 1

    #@0
    .prologue
    .line 98
    sget-object v0, Landroid/app/QueuedWork;->sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static remove(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "finisher"

    #@0
    .prologue
    .line 74
    sget-object v0, Landroid/app/QueuedWork;->sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2
    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    #@5
    .line 75
    return-void
.end method

.method public static singleThreadExecutor()Ljava/util/concurrent/ExecutorService;
    .registers 2

    #@0
    .prologue
    .line 48
    const-class v1, Landroid/app/QueuedWork;

    #@2
    monitor-enter v1

    #@3
    .line 49
    :try_start_3
    sget-object v0, Landroid/app/QueuedWork;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    #@5
    if-nez v0, :cond_d

    #@7
    .line 51
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    #@a
    move-result-object v0

    #@b
    sput-object v0, Landroid/app/QueuedWork;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    #@d
    .line 53
    :cond_d
    sget-object v0, Landroid/app/QueuedWork;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    #@f
    monitor-exit v1

    #@10
    return-object v0

    #@11
    .line 54
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public static waitToFinish()V
    .registers 2

    #@0
    .prologue
    .line 87
    .local v0, toFinish:Ljava/lang/Runnable;
    :goto_0
    sget-object v1, Landroid/app/QueuedWork;->sPendingWorkFinishers:Ljava/util/concurrent/ConcurrentLinkedQueue;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .end local v0           #toFinish:Ljava/lang/Runnable;
    check-cast v0, Ljava/lang/Runnable;

    #@8
    .restart local v0       #toFinish:Ljava/lang/Runnable;
    if-eqz v0, :cond_e

    #@a
    .line 88
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@d
    goto :goto_0

    #@e
    .line 90
    :cond_e
    return-void
.end method
