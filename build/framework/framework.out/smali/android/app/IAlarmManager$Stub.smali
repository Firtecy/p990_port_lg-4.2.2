.class public abstract Landroid/app/IAlarmManager$Stub;
.super Landroid/os/Binder;
.source "IAlarmManager.java"

# interfaces
.implements Landroid/app/IAlarmManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IAlarmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/IAlarmManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.IAlarmManager"

.field static final TRANSACTION_remove:I = 0x6

.field static final TRANSACTION_set:I = 0x1

.field static final TRANSACTION_setInexactRepeating:I = 0x3

.field static final TRANSACTION_setRepeating:I = 0x2

.field static final TRANSACTION_setTime:I = 0x4

.field static final TRANSACTION_setTimeZone:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.app.IAlarmManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/IAlarmManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IAlarmManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.app.IAlarmManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/IAlarmManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/app/IAlarmManager;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/app/IAlarmManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/IAlarmManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 15
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_cc

    #@4
    .line 141
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 47
    :sswitch_9
    const-string v0, "android.app.IAlarmManager"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v0, v9

    #@f
    .line 48
    goto :goto_8

    #@10
    .line 52
    :sswitch_10
    const-string v0, "android.app.IAlarmManager"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v1

    #@19
    .line 56
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@1c
    move-result-wide v2

    #@1d
    .line 58
    .local v2, _arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_33

    #@23
    .line 59
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@28
    move-result-object v4

    #@29
    check-cast v4, Landroid/app/PendingIntent;

    #@2b
    .line 64
    .local v4, _arg2:Landroid/app/PendingIntent;
    :goto_2b
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/app/IAlarmManager$Stub;->set(IJLandroid/app/PendingIntent;)V

    #@2e
    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    move v0, v9

    #@32
    .line 66
    goto :goto_8

    #@33
    .line 62
    .end local v4           #_arg2:Landroid/app/PendingIntent;
    :cond_33
    const/4 v4, 0x0

    #@34
    .restart local v4       #_arg2:Landroid/app/PendingIntent;
    goto :goto_2b

    #@35
    .line 70
    .end local v1           #_arg0:I
    .end local v2           #_arg1:J
    .end local v4           #_arg2:Landroid/app/PendingIntent;
    :sswitch_35
    const-string v0, "android.app.IAlarmManager"

    #@37
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a
    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v1

    #@3e
    .line 74
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@41
    move-result-wide v2

    #@42
    .line 76
    .restart local v2       #_arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@45
    move-result-wide v4

    #@46
    .line 78
    .local v4, _arg2:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@49
    move-result v0

    #@4a
    if-eqz v0, :cond_5d

    #@4c
    .line 79
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4e
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@51
    move-result-object v6

    #@52
    check-cast v6, Landroid/app/PendingIntent;

    #@54
    .local v6, _arg3:Landroid/app/PendingIntent;
    :goto_54
    move-object v0, p0

    #@55
    .line 84
    invoke-virtual/range {v0 .. v6}, Landroid/app/IAlarmManager$Stub;->setRepeating(IJJLandroid/app/PendingIntent;)V

    #@58
    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5b
    move v0, v9

    #@5c
    .line 86
    goto :goto_8

    #@5d
    .line 82
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    :cond_5d
    const/4 v6, 0x0

    #@5e
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_54

    #@5f
    .line 90
    .end local v1           #_arg0:I
    .end local v2           #_arg1:J
    .end local v4           #_arg2:J
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    :sswitch_5f
    const-string v0, "android.app.IAlarmManager"

    #@61
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@64
    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@67
    move-result v1

    #@68
    .line 94
    .restart local v1       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@6b
    move-result-wide v2

    #@6c
    .line 96
    .restart local v2       #_arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@6f
    move-result-wide v4

    #@70
    .line 98
    .restart local v4       #_arg2:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_87

    #@76
    .line 99
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@78
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7b
    move-result-object v6

    #@7c
    check-cast v6, Landroid/app/PendingIntent;

    #@7e
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    :goto_7e
    move-object v0, p0

    #@7f
    .line 104
    invoke-virtual/range {v0 .. v6}, Landroid/app/IAlarmManager$Stub;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    #@82
    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@85
    move v0, v9

    #@86
    .line 106
    goto :goto_8

    #@87
    .line 102
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    :cond_87
    const/4 v6, 0x0

    #@88
    .restart local v6       #_arg3:Landroid/app/PendingIntent;
    goto :goto_7e

    #@89
    .line 110
    .end local v1           #_arg0:I
    .end local v2           #_arg1:J
    .end local v4           #_arg2:J
    .end local v6           #_arg3:Landroid/app/PendingIntent;
    :sswitch_89
    const-string v0, "android.app.IAlarmManager"

    #@8b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8e
    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@91
    move-result-wide v7

    #@92
    .line 113
    .local v7, _arg0:J
    invoke-virtual {p0, v7, v8}, Landroid/app/IAlarmManager$Stub;->setTime(J)V

    #@95
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@98
    move v0, v9

    #@99
    .line 115
    goto/16 :goto_8

    #@9b
    .line 119
    .end local v7           #_arg0:J
    :sswitch_9b
    const-string v0, "android.app.IAlarmManager"

    #@9d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    .line 122
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/IAlarmManager$Stub;->setTimeZone(Ljava/lang/String;)V

    #@a7
    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@aa
    move v0, v9

    #@ab
    .line 124
    goto/16 :goto_8

    #@ad
    .line 128
    .end local v1           #_arg0:Ljava/lang/String;
    :sswitch_ad
    const-string v0, "android.app.IAlarmManager"

    #@af
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b2
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v0

    #@b6
    if-eqz v0, :cond_c9

    #@b8
    .line 131
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ba
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bd
    move-result-object v1

    #@be
    check-cast v1, Landroid/app/PendingIntent;

    #@c0
    .line 136
    .local v1, _arg0:Landroid/app/PendingIntent;
    :goto_c0
    invoke-virtual {p0, v1}, Landroid/app/IAlarmManager$Stub;->remove(Landroid/app/PendingIntent;)V

    #@c3
    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c6
    move v0, v9

    #@c7
    .line 138
    goto/16 :goto_8

    #@c9
    .line 134
    .end local v1           #_arg0:Landroid/app/PendingIntent;
    :cond_c9
    const/4 v1, 0x0

    #@ca
    .restart local v1       #_arg0:Landroid/app/PendingIntent;
    goto :goto_c0

    #@cb
    .line 43
    nop

    #@cc
    :sswitch_data_cc
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_35
        0x3 -> :sswitch_5f
        0x4 -> :sswitch_89
        0x5 -> :sswitch_9b
        0x6 -> :sswitch_ad
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
