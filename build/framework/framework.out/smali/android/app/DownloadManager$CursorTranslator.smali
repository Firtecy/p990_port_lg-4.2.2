.class Landroid/app/DownloadManager$CursorTranslator;
.super Landroid/database/CursorWrapper;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CursorTranslator"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBaseUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1308
    const-class v0, Landroid/app/DownloadManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/app/DownloadManager$CursorTranslator;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/database/Cursor;Landroid/net/Uri;)V
    .registers 3
    .parameter "cursor"
    .parameter "baseUri"

    #@0
    .prologue
    .line 1312
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    #@3
    .line 1313
    iput-object p2, p0, Landroid/app/DownloadManager$CursorTranslator;->mBaseUri:Landroid/net/Uri;

    #@5
    .line 1314
    return-void
.end method

.method private getErrorCode(I)J
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 1385
    const/16 v0, 0x190

    #@2
    if-gt v0, p1, :cond_8

    #@4
    const/16 v0, 0x1e8

    #@6
    if-lt p1, v0, :cond_10

    #@8
    :cond_8
    const/16 v0, 0x1f4

    #@a
    if-gt v0, p1, :cond_12

    #@c
    const/16 v0, 0x258

    #@e
    if-ge p1, v0, :cond_12

    #@10
    .line 1388
    :cond_10
    int-to-long v0, p1

    #@11
    .line 1418
    :goto_11
    return-wide v0

    #@12
    .line 1391
    :cond_12
    sparse-switch p1, :sswitch_data_30

    #@15
    .line 1418
    const-wide/16 v0, 0x3e8

    #@17
    goto :goto_11

    #@18
    .line 1393
    :sswitch_18
    const-wide/16 v0, 0x3e9

    #@1a
    goto :goto_11

    #@1b
    .line 1397
    :sswitch_1b
    const-wide/16 v0, 0x3ea

    #@1d
    goto :goto_11

    #@1e
    .line 1400
    :sswitch_1e
    const-wide/16 v0, 0x3ec

    #@20
    goto :goto_11

    #@21
    .line 1403
    :sswitch_21
    const-wide/16 v0, 0x3ed

    #@23
    goto :goto_11

    #@24
    .line 1406
    :sswitch_24
    const-wide/16 v0, 0x3ee

    #@26
    goto :goto_11

    #@27
    .line 1409
    :sswitch_27
    const-wide/16 v0, 0x3ef

    #@29
    goto :goto_11

    #@2a
    .line 1412
    :sswitch_2a
    const-wide/16 v0, 0x3f0

    #@2c
    goto :goto_11

    #@2d
    .line 1415
    :sswitch_2d
    const-wide/16 v0, 0x3f1

    #@2f
    goto :goto_11

    #@30
    .line 1391
    :sswitch_data_30
    .sparse-switch
        0xc6 -> :sswitch_24
        0xc7 -> :sswitch_27
        0x1e8 -> :sswitch_2d
        0x1e9 -> :sswitch_2a
        0x1ec -> :sswitch_18
        0x1ed -> :sswitch_1b
        0x1ee -> :sswitch_1b
        0x1ef -> :sswitch_1e
        0x1f1 -> :sswitch_21
    .end sparse-switch
.end method

.method private getLocalUri()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 1339
    const-string v5, "destination"

    #@2
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getColumnIndex(Ljava/lang/String;)I

    #@5
    move-result v5

    #@6
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getLong(I)J

    #@9
    move-result-wide v0

    #@a
    .line 1340
    .local v0, destinationType:J
    const-wide/16 v5, 0x4

    #@c
    cmp-long v5, v0, v5

    #@e
    if-eqz v5, :cond_1c

    #@10
    const-wide/16 v5, 0x0

    #@12
    cmp-long v5, v0, v5

    #@14
    if-eqz v5, :cond_1c

    #@16
    const-wide/16 v5, 0x6

    #@18
    cmp-long v5, v0, v5

    #@1a
    if-nez v5, :cond_39

    #@1c
    .line 1343
    :cond_1c
    const-string/jumbo v5, "local_filename"

    #@1f
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getColumnIndex(Ljava/lang/String;)I

    #@22
    move-result v5

    #@23
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 1344
    .local v4, localPath:Ljava/lang/String;
    if-nez v4, :cond_2b

    #@29
    .line 1345
    const/4 v5, 0x0

    #@2a
    .line 1352
    .end local v4           #localPath:Ljava/lang/String;
    :goto_2a
    return-object v5

    #@2b
    .line 1347
    .restart local v4       #localPath:Ljava/lang/String;
    :cond_2b
    new-instance v5, Ljava/io/File;

    #@2d
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@30
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    goto :goto_2a

    #@39
    .line 1351
    .end local v4           #localPath:Ljava/lang/String;
    :cond_39
    const-string v5, "_id"

    #@3b
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getColumnIndex(Ljava/lang/String;)I

    #@3e
    move-result v5

    #@3f
    invoke-virtual {p0, v5}, Landroid/app/DownloadManager$CursorTranslator;->getLong(I)J

    #@42
    move-result-wide v2

    #@43
    .line 1352
    .local v2, downloadId:J
    iget-object v5, p0, Landroid/app/DownloadManager$CursorTranslator;->mBaseUri:Landroid/net/Uri;

    #@45
    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    goto :goto_2a
.end method

.method private getPausedReason(I)J
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 1369
    packed-switch p1, :pswitch_data_10

    #@3
    .line 1380
    const-wide/16 v0, 0x4

    #@5
    :goto_5
    return-wide v0

    #@6
    .line 1371
    :pswitch_6
    const-wide/16 v0, 0x1

    #@8
    goto :goto_5

    #@9
    .line 1374
    :pswitch_9
    const-wide/16 v0, 0x2

    #@b
    goto :goto_5

    #@c
    .line 1377
    :pswitch_c
    const-wide/16 v0, 0x3

    #@e
    goto :goto_5

    #@f
    .line 1369
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0xc2
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method private getReason(I)J
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 1356
    invoke-direct {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->translateStatus(I)I

    #@3
    move-result v0

    #@4
    sparse-switch v0, :sswitch_data_14

    #@7
    .line 1364
    const-wide/16 v0, 0x0

    #@9
    :goto_9
    return-wide v0

    #@a
    .line 1358
    :sswitch_a
    invoke-direct {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getErrorCode(I)J

    #@d
    move-result-wide v0

    #@e
    goto :goto_9

    #@f
    .line 1361
    :sswitch_f
    invoke-direct {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getPausedReason(I)J

    #@12
    move-result-wide v0

    #@13
    goto :goto_9

    #@14
    .line 1356
    :sswitch_data_14
    .sparse-switch
        0x4 -> :sswitch_f
        0x10 -> :sswitch_a
    .end sparse-switch
.end method

.method private translateStatus(I)I
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 1423
    packed-switch p1, :pswitch_data_20

    #@3
    .line 1440
    :pswitch_3
    sget-boolean v0, Landroid/app/DownloadManager$CursorTranslator;->$assertionsDisabled:Z

    #@5
    if-nez v0, :cond_1c

    #@7
    invoke-static {p1}, Landroid/provider/Downloads$Impl;->isStatusError(I)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_1c

    #@d
    new-instance v0, Ljava/lang/AssertionError;

    #@f
    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    #@12
    throw v0

    #@13
    .line 1425
    :pswitch_13
    const/4 v0, 0x1

    #@14
    .line 1441
    :goto_14
    return v0

    #@15
    .line 1428
    :pswitch_15
    const/4 v0, 0x2

    #@16
    goto :goto_14

    #@17
    .line 1434
    :pswitch_17
    const/4 v0, 0x4

    #@18
    goto :goto_14

    #@19
    .line 1437
    :pswitch_19
    const/16 v0, 0x8

    #@1b
    goto :goto_14

    #@1c
    .line 1441
    :cond_1c
    const/16 v0, 0x10

    #@1e
    goto :goto_14

    #@1f
    .line 1423
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0xbe
        :pswitch_13
        :pswitch_3
        :pswitch_15
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_19
    .end packed-switch
.end method


# virtual methods
.method public getInt(I)I
    .registers 4
    .parameter "columnIndex"

    #@0
    .prologue
    .line 1318
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getLong(I)J

    #@3
    move-result-wide v0

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public getLong(I)J
    .registers 4
    .parameter "columnIndex"

    #@0
    .prologue
    .line 1323
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getColumnName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string/jumbo v1, "reason"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_1d

    #@d
    .line 1324
    const-string/jumbo v0, "status"

    #@10
    invoke-virtual {p0, v0}, Landroid/app/DownloadManager$CursorTranslator;->getColumnIndex(Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->getInt(I)I

    #@17
    move-result v0

    #@18
    invoke-direct {p0, v0}, Landroid/app/DownloadManager$CursorTranslator;->getReason(I)J

    #@1b
    move-result-wide v0

    #@1c
    .line 1328
    :goto_1c
    return-wide v0

    #@1d
    .line 1325
    :cond_1d
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getColumnName(I)Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    const-string/jumbo v1, "status"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    if-eqz v0, :cond_3b

    #@2a
    .line 1326
    const-string/jumbo v0, "status"

    #@2d
    invoke-virtual {p0, v0}, Landroid/app/DownloadManager$CursorTranslator;->getColumnIndex(Ljava/lang/String;)I

    #@30
    move-result v0

    #@31
    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->getInt(I)I

    #@34
    move-result v0

    #@35
    invoke-direct {p0, v0}, Landroid/app/DownloadManager$CursorTranslator;->translateStatus(I)I

    #@38
    move-result v0

    #@39
    int-to-long v0, v0

    #@3a
    goto :goto_1c

    #@3b
    .line 1328
    :cond_3b
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getLong(I)J

    #@3e
    move-result-wide v0

    #@3f
    goto :goto_1c
.end method

.method public getString(I)Ljava/lang/String;
    .registers 4
    .parameter "columnIndex"

    #@0
    .prologue
    .line 1334
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager$CursorTranslator;->getColumnName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string/jumbo v1, "local_uri"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_12

    #@d
    invoke-direct {p0}, Landroid/app/DownloadManager$CursorTranslator;->getLocalUri()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    :goto_11
    return-object v0

    #@12
    :cond_12
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    goto :goto_11
.end method
