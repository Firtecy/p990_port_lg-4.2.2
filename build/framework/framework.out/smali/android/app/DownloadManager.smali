.class public Landroid/app/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/DownloadManager$CursorTranslator;,
        Landroid/app/DownloadManager$Query;,
        Landroid/app/DownloadManager$Request;
    }
.end annotation


# static fields
.field public static final ACTION_DOWNLOAD_COMPLETE:Ljava/lang/String; = "android.intent.action.DOWNLOAD_COMPLETE"

.field public static final ACTION_NOTIFICATION_CLICKED:Ljava/lang/String; = "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

.field public static final ACTION_VIEW_DOWNLOADS:Ljava/lang/String; = "android.intent.action.VIEW_DOWNLOADS"

.field public static final COLUMN_BYTES_DOWNLOADED_SO_FAR:Ljava/lang/String; = "bytes_so_far"

.field public static final COLUMN_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_LAST_MODIFIED_TIMESTAMP:Ljava/lang/String; = "last_modified_timestamp"

.field public static final COLUMN_LOCAL_FILENAME:Ljava/lang/String; = "local_filename"

.field public static final COLUMN_LOCAL_URI:Ljava/lang/String; = "local_uri"

.field public static final COLUMN_MEDIAPROVIDER_URI:Ljava/lang/String; = "mediaprovider_uri"

.field public static final COLUMN_MEDIA_TYPE:Ljava/lang/String; = "media_type"

.field public static final COLUMN_REASON:Ljava/lang/String; = "reason"

.field public static final COLUMN_STATUS:Ljava/lang/String; = "status"

.field public static final COLUMN_TITLE:Ljava/lang/String; = "title"

.field public static final COLUMN_TOTAL_SIZE_BYTES:Ljava/lang/String; = "total_size"

.field public static final COLUMN_URI:Ljava/lang/String; = "uri"

.field public static final ERROR_BLOCKED:I = 0x3f2

.field public static final ERROR_CANNOT_RESUME:I = 0x3f0

.field public static final ERROR_DEVICE_NOT_FOUND:I = 0x3ef

.field public static final ERROR_FILE_ALREADY_EXISTS:I = 0x3f1

.field public static final ERROR_FILE_ERROR:I = 0x3e9

.field public static final ERROR_HTTP_DATA_ERROR:I = 0x3ec

.field public static final ERROR_INSUFFICIENT_SPACE:I = 0x3ee

.field public static final ERROR_TOO_MANY_REDIRECTS:I = 0x3ed

.field public static final ERROR_UNHANDLED_HTTP_CODE:I = 0x3ea

.field public static final ERROR_UNKNOWN:I = 0x3e8

.field public static final EXTRA_DOWNLOAD_ID:Ljava/lang/String; = "extra_download_id"

.field public static final EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS:Ljava/lang/String; = "extra_click_download_ids"

.field public static final INTENT_EXTRAS_SORT_BY_SIZE:Ljava/lang/String; = "android.app.DownloadManager.extra_sortBySize"

.field private static final NON_DOWNLOADMANAGER_DOWNLOAD:Ljava/lang/String; = "non-dwnldmngr-download-dont-retry2download"

.field public static final PAUSED_QUEUED_FOR_WIFI:I = 0x3

.field public static final PAUSED_UNKNOWN:I = 0x4

.field public static final PAUSED_WAITING_FOR_NETWORK:I = 0x2

.field public static final PAUSED_WAITING_TO_RETRY:I = 0x1

.field public static final STATUS_FAILED:I = 0x10

.field public static final STATUS_PAUSED:I = 0x4

.field public static final STATUS_PENDING:I = 0x1

.field public static final STATUS_RUNNING:I = 0x2

.field public static final STATUS_SUCCESSFUL:I = 0x8

.field public static final UNDERLYING_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mBaseUri:Landroid/net/Uri;

.field private mClearBaseUri:Landroid/net/Uri;

.field private mPackageName:Ljava/lang/String;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 304
    const/16 v0, 0xf

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    const-string v2, "_id"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string v2, "_data AS local_filename"

    #@c
    aput-object v2, v0, v1

    #@e
    const/4 v1, 0x2

    #@f
    const-string/jumbo v2, "mediaprovider_uri"

    #@12
    aput-object v2, v0, v1

    #@14
    const/4 v1, 0x3

    #@15
    const-string v2, "destination"

    #@17
    aput-object v2, v0, v1

    #@19
    const/4 v1, 0x4

    #@1a
    const-string/jumbo v2, "title"

    #@1d
    aput-object v2, v0, v1

    #@1f
    const/4 v1, 0x5

    #@20
    const-string v2, "description"

    #@22
    aput-object v2, v0, v1

    #@24
    const/4 v1, 0x6

    #@25
    const-string/jumbo v2, "uri"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x7

    #@2b
    const-string/jumbo v2, "status"

    #@2e
    aput-object v2, v0, v1

    #@30
    const/16 v1, 0x8

    #@32
    const-string v2, "hint"

    #@34
    aput-object v2, v0, v1

    #@36
    const/16 v1, 0x9

    #@38
    const-string/jumbo v2, "mimetype AS media_type"

    #@3b
    aput-object v2, v0, v1

    #@3d
    const/16 v1, 0xa

    #@3f
    const-string/jumbo v2, "total_bytes AS total_size"

    #@42
    aput-object v2, v0, v1

    #@44
    const/16 v1, 0xb

    #@46
    const-string/jumbo v2, "lastmod AS last_modified_timestamp"

    #@49
    aput-object v2, v0, v1

    #@4b
    const/16 v1, 0xc

    #@4d
    const-string v2, "current_bytes AS bytes_so_far"

    #@4f
    aput-object v2, v0, v1

    #@51
    const/16 v1, 0xd

    #@53
    const-string v2, "\'placeholder\' AS local_uri"

    #@55
    aput-object v2, v0, v1

    #@57
    const/16 v1, 0xe

    #@59
    const-string v2, "\'placeholder\' AS reason"

    #@5b
    aput-object v2, v0, v1

    #@5d
    sput-object v0, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@5f
    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .registers 4
    .parameter "resolver"
    .parameter "packageName"

    #@0
    .prologue
    .line 887
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 878
    sget-object v0, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    #@5
    iput-object v0, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@7
    .line 881
    const-string v0, "content://downloads/clear_download"

    #@9
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/app/DownloadManager;->mClearBaseUri:Landroid/net/Uri;

    #@f
    .line 888
    iput-object p1, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@11
    .line 889
    iput-object p2, p0, Landroid/app/DownloadManager;->mPackageName:Ljava/lang/String;

    #@13
    .line 890
    return-void
.end method

.method public static getActiveNetworkWarningBytes(Landroid/content/Context;)J
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 1199
    const-wide/16 v0, -0x1

    #@2
    return-wide v0
.end method

.method public static getMaxBytesOverMobile(Landroid/content/Context;)Ljava/lang/Long;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 1165
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "download_manager_max_bytes_over_mobile"

    #@6
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    #@9
    move-result-wide v1

    #@a
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 1168
    :goto_e
    return-object v1

    #@f
    .line 1167
    :catch_f
    move-exception v0

    #@10
    .line 1168
    .local v0, exc:Landroid/provider/Settings$SettingNotFoundException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method public static getRecommendedMaxBytesOverMobile(Landroid/content/Context;)Ljava/lang/Long;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 1183
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "download_manager_recommended_max_bytes_over_mobile"

    #@6
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    #@9
    move-result-wide v1

    #@a
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_d
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    .line 1186
    :goto_e
    return-object v1

    #@f
    .line 1185
    :catch_f
    move-exception v0

    #@10
    .line 1186
    .local v0, exc:Landroid/provider/Settings$SettingNotFoundException;
    const/4 v1, 0x0

    #@11
    goto :goto_e
.end method

.method static getWhereArgsForIds([J)[Ljava/lang/String;
    .registers 5
    .parameter "ids"

    #@0
    .prologue
    .line 1295
    array-length v2, p0

    #@1
    new-array v1, v2, [Ljava/lang/String;

    #@3
    .line 1296
    .local v1, whereArgs:[Ljava/lang/String;
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    array-length v2, p0

    #@5
    if-ge v0, v2, :cond_12

    #@7
    .line 1297
    aget-wide v2, p0, v0

    #@9
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    aput-object v2, v1, v0

    #@f
    .line 1296
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_4

    #@12
    .line 1299
    :cond_12
    return-object v1
.end method

.method static getWhereClauseForIds([J)Ljava/lang/String;
    .registers 4
    .parameter "ids"

    #@0
    .prologue
    .line 1278
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1279
    .local v1, whereClause:Ljava/lang/StringBuilder;
    const-string v2, "("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 1280
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    array-length v2, p0

    #@c
    if-ge v0, v2, :cond_22

    #@e
    .line 1281
    if-lez v0, :cond_15

    #@10
    .line 1282
    const-string v2, "OR "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 1284
    :cond_15
    const-string v2, "_id"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 1285
    const-string v2, " = ? "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 1280
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_b

    #@22
    .line 1287
    :cond_22
    const-string v2, ")"

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 1288
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    return-object v2
.end method

.method public static isActiveNetworkExpensive(Landroid/content/Context;)Z
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 1193
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method private static validateArgumentIsNonEmpty(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "paramName"
    .parameter "val"

    #@0
    .prologue
    .line 1262
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1f

    #@6
    .line 1263
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " can\'t be null"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 1265
    :cond_1f
    return-void
.end method


# virtual methods
.method public addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    .registers 16
    .parameter "title"
    .parameter "description"
    .parameter "isMediaScannerScannable"
    .parameter "mimeType"
    .parameter "path"
    .parameter "length"
    .parameter "showNotification"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 1228
    const-string/jumbo v3, "title"

    #@4
    invoke-static {v3, p1}, Landroid/app/DownloadManager;->validateArgumentIsNonEmpty(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1229
    const-string v3, "description"

    #@9
    invoke-static {v3, p2}, Landroid/app/DownloadManager;->validateArgumentIsNonEmpty(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 1230
    const-string/jumbo v3, "path"

    #@f
    invoke-static {v3, p5}, Landroid/app/DownloadManager;->validateArgumentIsNonEmpty(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 1231
    const-string/jumbo v3, "mimeType"

    #@15
    invoke-static {v3, p4}, Landroid/app/DownloadManager;->validateArgumentIsNonEmpty(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1232
    const-wide/16 v5, 0x0

    #@1a
    cmp-long v3, p6, v5

    #@1c
    if-gez v3, :cond_26

    #@1e
    .line 1233
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@20
    const-string v4, " invalid value for param: totalBytes"

    #@22
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v3

    #@26
    .line 1237
    :cond_26
    new-instance v3, Landroid/app/DownloadManager$Request;

    #@28
    const-string/jumbo v5, "non-dwnldmngr-download-dont-retry2download"

    #@2b
    invoke-direct {v3, v5}, Landroid/app/DownloadManager$Request;-><init>(Ljava/lang/String;)V

    #@2e
    invoke-virtual {v3, p1}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p2}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, p4}, Landroid/app/DownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    #@39
    move-result-object v1

    #@3a
    .line 1241
    .local v1, request:Landroid/app/DownloadManager$Request;
    const/4 v3, 0x0

    #@3b
    invoke-virtual {v1, v3}, Landroid/app/DownloadManager$Request;->toContentValues(Ljava/lang/String;)Landroid/content/ContentValues;

    #@3e
    move-result-object v2

    #@3f
    .line 1242
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "destination"

    #@41
    const/4 v5, 0x6

    #@42
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@49
    .line 1244
    const-string v3, "_data"

    #@4b
    invoke-virtual {v2, v3, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 1245
    const-string/jumbo v3, "status"

    #@51
    const/16 v5, 0xc8

    #@53
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5a
    .line 1246
    const-string/jumbo v3, "total_bytes"

    #@5d
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@64
    .line 1247
    const-string/jumbo v5, "scanned"

    #@67
    if-eqz p3, :cond_8b

    #@69
    const/4 v3, 0x0

    #@6a
    :goto_6a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@71
    .line 1250
    const-string/jumbo v3, "visibility"

    #@74
    if-eqz p8, :cond_77

    #@76
    const/4 v4, 0x3

    #@77
    :cond_77
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7e
    .line 1252
    iget-object v3, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@80
    sget-object v4, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    #@82
    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@85
    move-result-object v0

    #@86
    .line 1253
    .local v0, downloadUri:Landroid/net/Uri;
    if-nez v0, :cond_8d

    #@88
    .line 1254
    const-wide/16 v3, -0x1

    #@8a
    .line 1256
    :goto_8a
    return-wide v3

    #@8b
    .end local v0           #downloadUri:Landroid/net/Uri;
    :cond_8b
    move v3, v4

    #@8c
    .line 1247
    goto :goto_6a

    #@8d
    .line 1256
    .restart local v0       #downloadUri:Landroid/net/Uri;
    :cond_8d
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@94
    move-result-wide v3

    #@95
    goto :goto_8a
.end method

.method public enqueue(Landroid/app/DownloadManager$Request;)J
    .registers 8
    .parameter "request"

    #@0
    .prologue
    .line 914
    iget-object v4, p0, Landroid/app/DownloadManager;->mPackageName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v4}, Landroid/app/DownloadManager$Request;->toContentValues(Ljava/lang/String;)Landroid/content/ContentValues;

    #@5
    move-result-object v3

    #@6
    .line 915
    .local v3, values:Landroid/content/ContentValues;
    iget-object v4, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@8
    sget-object v5, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    #@a
    invoke-virtual {v4, v5, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    .line 918
    .local v0, downloadUri:Landroid/net/Uri;
    if-nez v0, :cond_13

    #@10
    .line 919
    const-wide/16 v1, -0x1

    #@12
    .line 922
    :goto_12
    return-wide v1

    #@13
    .line 921
    :cond_13
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@16
    move-result-object v4

    #@17
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@1a
    move-result-wide v1

    #@1b
    .line 922
    .local v1, id:J
    goto :goto_12
.end method

.method getDownloadUri(J)Landroid/net/Uri;
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 1271
    iget-object v0, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@2
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getMimeTypeForDownloadedFile(J)Ljava/lang/String;
    .registers 9
    .parameter "id"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1100
    new-instance v3, Landroid/app/DownloadManager$Query;

    #@3
    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    #@6
    const/4 v4, 0x1

    #@7
    new-array v4, v4, [J

    #@9
    const/4 v5, 0x0

    #@a
    aput-wide p1, v4, v5

    #@c
    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    #@f
    move-result-object v1

    #@10
    .line 1101
    .local v1, query:Landroid/app/DownloadManager$Query;
    const/4 v0, 0x0

    #@11
    .line 1103
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_11
    invoke-virtual {p0, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_31

    #@14
    move-result-object v0

    #@15
    .line 1104
    if-nez v0, :cond_1d

    #@17
    .line 1111
    if-eqz v0, :cond_1c

    #@19
    .line 1112
    :goto_19
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@1c
    .line 1116
    :cond_1c
    return-object v2

    #@1d
    .line 1107
    :cond_1d
    :try_start_1d
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@20
    move-result v3

    #@21
    if-eqz v3, :cond_38

    #@23
    .line 1108
    const-string/jumbo v2, "media_type"

    #@26
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@29
    move-result v2

    #@2a
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2d
    .catchall {:try_start_1d .. :try_end_2d} :catchall_31

    #@2d
    move-result-object v2

    #@2e
    .line 1111
    if-eqz v0, :cond_1c

    #@30
    goto :goto_19

    #@31
    :catchall_31
    move-exception v2

    #@32
    if-eqz v0, :cond_37

    #@34
    .line 1112
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@37
    .line 1111
    :cond_37
    throw v2

    #@38
    :cond_38
    if-eqz v0, :cond_1c

    #@3a
    goto :goto_19
.end method

.method public getUriForDownloadedFile(J)Landroid/net/Uri;
    .registers 14
    .parameter "id"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 1048
    new-instance v7, Landroid/app/DownloadManager$Query;

    #@4
    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    #@7
    new-array v8, v10, [J

    #@9
    const/4 v9, 0x0

    #@a
    aput-wide p1, v8, v9

    #@c
    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    #@f
    move-result-object v4

    #@10
    .line 1049
    .local v4, query:Landroid/app/DownloadManager$Query;
    const/4 v0, 0x0

    #@11
    .line 1051
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_11
    invoke-virtual {p0, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_67

    #@14
    move-result-object v0

    #@15
    .line 1052
    if-nez v0, :cond_1d

    #@17
    .line 1079
    if-eqz v0, :cond_1c

    #@19
    .line 1080
    :goto_19
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@1c
    .line 1084
    :cond_1c
    return-object v6

    #@1d
    .line 1055
    :cond_1d
    :try_start_1d
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@20
    move-result v7

    #@21
    if-eqz v7, :cond_6e

    #@23
    .line 1056
    const-string/jumbo v7, "status"

    #@26
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@29
    move-result v7

    #@2a
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    #@2d
    move-result v5

    #@2e
    .line 1057
    .local v5, status:I
    const/16 v7, 0x8

    #@30
    if-ne v7, v5, :cond_6e

    #@32
    .line 1058
    const-string v6, "destination"

    #@34
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@37
    move-result v2

    #@38
    .line 1060
    .local v2, indx:I
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    #@3b
    move-result v1

    #@3c
    .line 1064
    .local v1, destination:I
    if-eq v1, v10, :cond_47

    #@3e
    const/4 v6, 0x5

    #@3f
    if-eq v1, v6, :cond_47

    #@41
    const/4 v6, 0x3

    #@42
    if-eq v1, v6, :cond_47

    #@44
    const/4 v6, 0x2

    #@45
    if-ne v1, v6, :cond_50

    #@47
    .line 1069
    :cond_47
    sget-object v6, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    #@49
    invoke-static {v6, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@4c
    move-result-object v6

    #@4d
    .line 1079
    if-eqz v0, :cond_1c

    #@4f
    goto :goto_19

    #@50
    .line 1072
    :cond_50
    const-string/jumbo v6, "local_filename"

    #@53
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@56
    move-result v6

    #@57
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    .line 1074
    .local v3, path:Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    #@5d
    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@60
    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_63
    .catchall {:try_start_1d .. :try_end_63} :catchall_67

    #@63
    move-result-object v6

    #@64
    .line 1079
    if-eqz v0, :cond_1c

    #@66
    goto :goto_19

    #@67
    .end local v1           #destination:I
    .end local v2           #indx:I
    .end local v3           #path:Ljava/lang/String;
    .end local v5           #status:I
    :catchall_67
    move-exception v6

    #@68
    if-eqz v0, :cond_6d

    #@6a
    .line 1080
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@6d
    .line 1079
    :cond_6d
    throw v6

    #@6e
    :cond_6e
    if-eqz v0, :cond_1c

    #@70
    goto :goto_19
.end method

.method public varargs markRowCleared([J)I
    .registers 7
    .parameter "ids"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 963
    if-eqz p1, :cond_6

    #@3
    array-length v0, p1

    #@4
    if-nez v0, :cond_e

    #@6
    .line 964
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v1, "input param \'ids\' can\'t be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 967
    :cond_e
    array-length v0, p1

    #@f
    const/4 v1, 0x1

    #@10
    if-ne v0, v1, :cond_22

    #@12
    .line 968
    iget-object v0, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@14
    iget-object v1, p0, Landroid/app/DownloadManager;->mClearBaseUri:Landroid/net/Uri;

    #@16
    const/4 v2, 0x0

    #@17
    aget-wide v2, p1, v2

    #@19
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@20
    move-result v0

    #@21
    .line 970
    :goto_21
    return v0

    #@22
    :cond_22
    iget-object v0, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@24
    iget-object v1, p0, Landroid/app/DownloadManager;->mClearBaseUri:Landroid/net/Uri;

    #@26
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereClauseForIds([J)Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereArgsForIds([J)[Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@31
    move-result v0

    #@32
    goto :goto_21
.end method

.method public varargs markRowDeleted([J)I
    .registers 8
    .parameter "ids"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 935
    if-eqz p1, :cond_7

    #@4
    array-length v1, p1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 937
    :cond_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v2, "input param \'ids\' can\'t be null"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 939
    :cond_f
    new-instance v0, Landroid/content/ContentValues;

    #@11
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@14
    .line 940
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "deleted"

    #@16
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1d
    .line 943
    array-length v1, p1

    #@1e
    if-ne v1, v3, :cond_30

    #@20
    .line 944
    iget-object v1, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@22
    iget-object v2, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@24
    const/4 v3, 0x0

    #@25
    aget-wide v3, p1, v3

    #@27
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v1, v2, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2e
    move-result v1

    #@2f
    .line 947
    :goto_2f
    return v1

    #@30
    :cond_30
    iget-object v1, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@32
    iget-object v2, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@34
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereClauseForIds([J)Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereArgsForIds([J)[Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3f
    move-result v1

    #@40
    goto :goto_2f
.end method

.method public openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;
    .registers 6
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 1031
    iget-object v0, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@2
    invoke-virtual {p0, p1, p2}, Landroid/app/DownloadManager;->getDownloadUri(J)Landroid/net/Uri;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "r"

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    .registers 6
    .parameter "query"

    #@0
    .prologue
    .line 993
    iget-object v1, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@2
    sget-object v2, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@4
    iget-object v3, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@6
    invoke-virtual {p1, v1, v2, v3}, Landroid/app/DownloadManager$Query;->runQuery(Landroid/content/ContentResolver;[Ljava/lang/String;Landroid/net/Uri;)Landroid/database/Cursor;

    #@9
    move-result-object v0

    #@a
    .line 994
    .local v0, underlyingCursor:Landroid/database/Cursor;
    if-nez v0, :cond_e

    #@c
    .line 995
    const/4 v1, 0x0

    #@d
    .line 997
    :goto_d
    return-object v1

    #@e
    :cond_e
    new-instance v1, Landroid/app/DownloadManager$CursorTranslator;

    #@10
    iget-object v2, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@12
    invoke-direct {v1, v0, v2}, Landroid/app/DownloadManager$CursorTranslator;-><init>(Landroid/database/Cursor;Landroid/net/Uri;)V

    #@15
    goto :goto_d
.end method

.method public query(Landroid/app/DownloadManager$Query;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "query"
    .parameter "additionalColumns"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1009
    if-eqz p2, :cond_30

    #@3
    array-length v2, p2

    #@4
    if-lez v2, :cond_30

    #@6
    .line 1010
    sget-object v2, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@8
    array-length v2, v2

    #@9
    array-length v3, p2

    #@a
    add-int/2addr v2, v3

    #@b
    new-array v0, v2, [Ljava/lang/String;

    #@d
    .line 1011
    .local v0, projection:[Ljava/lang/String;
    sget-object v2, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@f
    sget-object v3, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@11
    array-length v3, v3

    #@12
    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@15
    .line 1012
    sget-object v2, Landroid/app/DownloadManager;->UNDERLYING_COLUMNS:[Ljava/lang/String;

    #@17
    array-length v2, v2

    #@18
    array-length v3, p2

    #@19
    invoke-static {p2, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 1015
    iget-object v2, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@1e
    iget-object v3, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@20
    invoke-virtual {p1, v2, v0, v3}, Landroid/app/DownloadManager$Query;->runQuery(Landroid/content/ContentResolver;[Ljava/lang/String;Landroid/net/Uri;)Landroid/database/Cursor;

    #@23
    move-result-object v1

    #@24
    .line 1016
    .local v1, underlyingCursor:Landroid/database/Cursor;
    if-nez v1, :cond_28

    #@26
    .line 1017
    const/4 v2, 0x0

    #@27
    .line 1021
    .end local v0           #projection:[Ljava/lang/String;
    .end local v1           #underlyingCursor:Landroid/database/Cursor;
    :goto_27
    return-object v2

    #@28
    .line 1019
    .restart local v0       #projection:[Ljava/lang/String;
    .restart local v1       #underlyingCursor:Landroid/database/Cursor;
    :cond_28
    new-instance v2, Landroid/app/DownloadManager$CursorTranslator;

    #@2a
    iget-object v3, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@2c
    invoke-direct {v2, v1, v3}, Landroid/app/DownloadManager$CursorTranslator;-><init>(Landroid/database/Cursor;Landroid/net/Uri;)V

    #@2f
    goto :goto_27

    #@30
    .line 1021
    .end local v0           #projection:[Ljava/lang/String;
    .end local v1           #underlyingCursor:Landroid/database/Cursor;
    :cond_30
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    #@33
    move-result-object v2

    #@34
    goto :goto_27
.end method

.method public varargs remove([J)I
    .registers 3
    .parameter "ids"

    #@0
    .prologue
    .line 983
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager;->markRowDeleted([J)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public varargs restartDownload([J)V
    .registers 9
    .parameter "ids"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1126
    new-instance v3, Landroid/app/DownloadManager$Query;

    #@3
    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    #@6
    invoke-virtual {v3, p1}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {p0, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    #@d
    move-result-object v0

    #@e
    .line 1128
    .local v0, cursor:Landroid/database/Cursor;
    :try_start_e
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@11
    :goto_11
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    #@14
    move-result v3

    #@15
    if-nez v3, :cond_56

    #@17
    .line 1129
    const-string/jumbo v3, "status"

    #@1a
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1d
    move-result v3

    #@1e
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    #@21
    move-result v1

    #@22
    .line 1130
    .local v1, status:I
    const/16 v3, 0x8

    #@24
    if-eq v1, v3, :cond_52

    #@26
    const/16 v3, 0x10

    #@28
    if-eq v1, v3, :cond_52

    #@2a
    .line 1131
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2c
    new-instance v4, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "Cannot restart incomplete download: "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "_id"

    #@39
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3c
    move-result v5

    #@3d
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    #@40
    move-result-wide v5

    #@41
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4c
    throw v3
    :try_end_4d
    .catchall {:try_start_e .. :try_end_4d} :catchall_4d

    #@4d
    .line 1136
    .end local v1           #status:I
    :catchall_4d
    move-exception v3

    #@4e
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@51
    throw v3

    #@52
    .line 1128
    .restart local v1       #status:I
    :cond_52
    :try_start_52
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_55
    .catchall {:try_start_52 .. :try_end_55} :catchall_4d

    #@55
    goto :goto_11

    #@56
    .line 1136
    .end local v1           #status:I
    :cond_56
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@59
    .line 1139
    new-instance v2, Landroid/content/ContentValues;

    #@5b
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@5e
    .line 1140
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "current_bytes"

    #@60
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@67
    .line 1141
    const-string/jumbo v3, "total_bytes"

    #@6a
    const/4 v4, -0x1

    #@6b
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@72
    .line 1142
    const-string v3, "_data"

    #@74
    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    #@77
    .line 1143
    const-string/jumbo v3, "status"

    #@7a
    const/16 v4, 0xbe

    #@7c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7f
    move-result-object v4

    #@80
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@83
    .line 1146
    const-string/jumbo v3, "visibility"

    #@86
    const/4 v4, 0x1

    #@87
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@8e
    .line 1150
    const-string/jumbo v3, "scanned"

    #@91
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@98
    .line 1152
    iget-object v3, p0, Landroid/app/DownloadManager;->mResolver:Landroid/content/ContentResolver;

    #@9a
    iget-object v4, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@9c
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereClauseForIds([J)Ljava/lang/String;

    #@9f
    move-result-object v5

    #@a0
    invoke-static {p1}, Landroid/app/DownloadManager;->getWhereArgsForIds([J)[Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@a7
    .line 1153
    return-void
.end method

.method public setAccessAllDownloads(Z)V
    .registers 3
    .parameter "accessAllDownloads"

    #@0
    .prologue
    .line 898
    if-eqz p1, :cond_7

    #@2
    .line 899
    sget-object v0, Landroid/provider/Downloads$Impl;->ALL_DOWNLOADS_CONTENT_URI:Landroid/net/Uri;

    #@4
    iput-object v0, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@6
    .line 903
    :goto_6
    return-void

    #@7
    .line 901
    :cond_7
    sget-object v0, Landroid/provider/Downloads$Impl;->CONTENT_URI:Landroid/net/Uri;

    #@9
    iput-object v0, p0, Landroid/app/DownloadManager;->mBaseUri:Landroid/net/Uri;

    #@b
    goto :goto_6
.end method
