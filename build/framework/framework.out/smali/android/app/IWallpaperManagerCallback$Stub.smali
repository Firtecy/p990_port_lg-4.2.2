.class public abstract Landroid/app/IWallpaperManagerCallback$Stub;
.super Landroid/os/Binder;
.source "IWallpaperManagerCallback.java"

# interfaces
.implements Landroid/app/IWallpaperManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IWallpaperManagerCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/IWallpaperManagerCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.IWallpaperManagerCallback"

.field static final TRANSACTION_onWallpaperChanged:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 22
    const-string v0, "android.app.IWallpaperManagerCallback"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/IWallpaperManagerCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IWallpaperManagerCallback;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 30
    if-nez p0, :cond_4

    #@2
    .line 31
    const/4 v0, 0x0

    #@3
    .line 37
    :goto_3
    return-object v0

    #@4
    .line 33
    :cond_4
    const-string v1, "android.app.IWallpaperManagerCallback"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 34
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/IWallpaperManagerCallback;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 35
    check-cast v0, Landroid/app/IWallpaperManagerCallback;

    #@12
    goto :goto_3

    #@13
    .line 37
    :cond_13
    new-instance v0, Landroid/app/IWallpaperManagerCallback$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/IWallpaperManagerCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 7
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 45
    sparse-switch p1, :sswitch_data_18

    #@4
    .line 59
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v0

    #@8
    :goto_8
    return v0

    #@9
    .line 49
    :sswitch_9
    const-string v1, "android.app.IWallpaperManagerCallback"

    #@b
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 54
    :sswitch_f
    const-string v1, "android.app.IWallpaperManagerCallback"

    #@11
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 55
    invoke-virtual {p0}, Landroid/app/IWallpaperManagerCallback$Stub;->onWallpaperChanged()V

    #@17
    goto :goto_8

    #@18
    .line 45
    :sswitch_data_18
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
