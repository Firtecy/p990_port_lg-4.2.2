.class Landroid/app/ReceiverRestrictedContext;
.super Landroid/content/ContextWrapper;
.source "ContextImpl.java"


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "base"

    #@0
    .prologue
    .line 151
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    #@3
    .line 152
    return-void
.end method


# virtual methods
.method public bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    .registers 6
    .parameter "service"
    .parameter "conn"
    .parameter "flags"

    #@0
    .prologue
    .line 187
    new-instance v0, Landroid/content/ReceiverCallNotAllowedException;

    #@2
    const-string v1, "BroadcastReceiver components are not allowed to bind to services"

    #@4
    invoke-direct {v0, v1}, Landroid/content/ReceiverCallNotAllowedException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    .registers 4
    .parameter "receiver"
    .parameter "filter"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 156
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/app/ReceiverRestrictedContext;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 7
    .parameter "receiver"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 162
    if-nez p1, :cond_8

    #@2
    .line 165
    const/4 v0, 0x0

    #@3
    invoke-super {p0, v0, p2, p3, p4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@6
    move-result-object v0

    #@7
    return-object v0

    #@8
    .line 167
    :cond_8
    new-instance v0, Landroid/content/ReceiverCallNotAllowedException;

    #@a
    const-string v1, "BroadcastReceiver components are not allowed to register to receive intents"

    #@c
    invoke-direct {v0, v1}, Landroid/content/ReceiverCallNotAllowedException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0
.end method

.method public registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
    .registers 12
    .parameter "receiver"
    .parameter "user"
    .parameter "filter"
    .parameter "broadcastPermission"
    .parameter "scheduler"

    #@0
    .prologue
    .line 175
    if-nez p1, :cond_d

    #@2
    .line 178
    const/4 v1, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v4, p4

    #@7
    move-object v5, p5

    #@8
    invoke-super/range {v0 .. v5}, Landroid/content/ContextWrapper;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@b
    move-result-object v0

    #@c
    return-object v0

    #@d
    .line 180
    :cond_d
    new-instance v0, Landroid/content/ReceiverCallNotAllowedException;

    #@f
    const-string v1, "BroadcastReceiver components are not allowed to register to receive intents"

    #@11
    invoke-direct {v0, v1}, Landroid/content/ReceiverCallNotAllowedException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0
.end method
