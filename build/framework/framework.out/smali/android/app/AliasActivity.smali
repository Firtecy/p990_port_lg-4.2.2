.class public Landroid/app/AliasActivity;
.super Landroid/app/Activity;
.source "AliasActivity.java"


# instance fields
.field public final ALIAS_META_DATA:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 50
    const-string v0, "android.app.alias"

    #@5
    iput-object v0, p0, Landroid/app/AliasActivity;->ALIAS_META_DATA:Ljava/lang/String;

    #@7
    return-void
.end method

.method private parseAlias(Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/Intent;
    .registers 11
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v7, 0x1

    #@2
    .line 89
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@5
    move-result-object v0

    #@6
    .line 91
    .local v0, attrs:Landroid/util/AttributeSet;
    const/4 v2, 0x0

    #@7
    .line 95
    .local v2, intent:Landroid/content/Intent;
    :cond_7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@a
    move-result v5

    #@b
    .local v5, type:I
    if-eq v5, v7, :cond_10

    #@d
    const/4 v6, 0x2

    #@e
    if-ne v5, v6, :cond_7

    #@10
    .line 98
    :cond_10
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 99
    .local v3, nodeName:Ljava/lang/String;
    const-string v6, "alias"

    #@16
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v6

    #@1a
    if-nez v6, :cond_43

    #@1c
    .line 100
    new-instance v6, Ljava/lang/RuntimeException;

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "Alias meta-data must start with <alias> tag; found"

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    const-string v8, " at "

    #@2f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v7

    #@33
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@36
    move-result-object v8

    #@37
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v7

    #@3b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v7

    #@3f
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42
    throw v6

    #@43
    .line 105
    :cond_43
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@46
    move-result v4

    #@47
    .line 107
    .local v4, outerDepth:I
    :cond_47
    :goto_47
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@4a
    move-result v5

    #@4b
    if-eq v5, v7, :cond_76

    #@4d
    if-ne v5, v8, :cond_55

    #@4f
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@52
    move-result v6

    #@53
    if-le v6, v4, :cond_76

    #@55
    .line 108
    :cond_55
    if-eq v5, v8, :cond_47

    #@57
    const/4 v6, 0x4

    #@58
    if-eq v5, v6, :cond_47

    #@5a
    .line 112
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 113
    const-string v6, "intent"

    #@60
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@63
    move-result v6

    #@64
    if-eqz v6, :cond_72

    #@66
    .line 114
    invoke-virtual {p0}, Landroid/app/AliasActivity;->getResources()Landroid/content/res/Resources;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v6, p1, v0}, Landroid/content/Intent;->parseIntent(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;

    #@6d
    move-result-object v1

    #@6e
    .line 115
    .local v1, gotIntent:Landroid/content/Intent;
    if-nez v2, :cond_47

    #@70
    move-object v2, v1

    #@71
    goto :goto_47

    #@72
    .line 117
    .end local v1           #gotIntent:Landroid/content/Intent;
    :cond_72
    invoke-static {p1}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    #@75
    goto :goto_47

    #@76
    .line 121
    :cond_76
    return-object v2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 56
    const/4 v3, 0x0

    #@4
    .line 58
    .local v3, parser:Landroid/content/res/XmlResourceParser;
    :try_start_4
    invoke-virtual {p0}, Landroid/app/AliasActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {p0}, Landroid/app/AliasActivity;->getComponentName()Landroid/content/ComponentName;

    #@b
    move-result-object v5

    #@c
    const/16 v6, 0x80

    #@e
    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    #@11
    move-result-object v0

    #@12
    .line 60
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    invoke-virtual {p0}, Landroid/app/AliasActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    #@15
    move-result-object v4

    #@16
    const-string v5, "android.app.alias"

    #@18
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/ActivityInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@1b
    move-result-object v3

    #@1c
    .line 62
    if-nez v3, :cond_36

    #@1e
    .line 63
    new-instance v4, Ljava/lang/RuntimeException;

    #@20
    const-string v5, "Alias requires a meta-data field android.app.alias"

    #@22
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v4
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_2f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_26} :catch_26
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_26} :catch_44
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_26} :catch_59

    #@26
    .line 76
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    :catch_26
    move-exception v1

    #@27
    .line 77
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_27
    new-instance v4, Ljava/lang/RuntimeException;

    #@29
    const-string v5, "Error parsing alias"

    #@2b
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2e
    throw v4
    :try_end_2f
    .catchall {:try_start_27 .. :try_end_2f} :catchall_2f

    #@2f
    .line 83
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_2f
    move-exception v4

    #@30
    if-eqz v3, :cond_35

    #@32
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@35
    :cond_35
    throw v4

    #@36
    .line 67
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    :cond_36
    :try_start_36
    invoke-direct {p0, v3}, Landroid/app/AliasActivity;->parseAlias(Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/Intent;

    #@39
    move-result-object v2

    #@3a
    .line 68
    .local v2, intent:Landroid/content/Intent;
    if-nez v2, :cond_4d

    #@3c
    .line 69
    new-instance v4, Ljava/lang/RuntimeException;

    #@3e
    const-string v5, "No <intent> tag found in alias description"

    #@40
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@43
    throw v4
    :try_end_44
    .catchall {:try_start_36 .. :try_end_44} :catchall_2f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_36 .. :try_end_44} :catch_26
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_36 .. :try_end_44} :catch_44
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_44} :catch_59

    #@44
    .line 78
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #intent:Landroid/content/Intent;
    :catch_44
    move-exception v1

    #@45
    .line 79
    .local v1, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_45
    new-instance v4, Ljava/lang/RuntimeException;

    #@47
    const-string v5, "Error parsing alias"

    #@49
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4c
    throw v4
    :try_end_4d
    .catchall {:try_start_45 .. :try_end_4d} :catchall_2f

    #@4d
    .line 73
    .end local v1           #e:Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_4d
    :try_start_4d
    invoke-virtual {p0, v2}, Landroid/app/AliasActivity;->startActivity(Landroid/content/Intent;)V

    #@50
    .line 74
    invoke-virtual {p0}, Landroid/app/AliasActivity;->finish()V
    :try_end_53
    .catchall {:try_start_4d .. :try_end_53} :catchall_2f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4d .. :try_end_53} :catch_26
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4d .. :try_end_53} :catch_44
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_53} :catch_59

    #@53
    .line 83
    if-eqz v3, :cond_58

    #@55
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@58
    .line 85
    :cond_58
    return-void

    #@59
    .line 80
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #intent:Landroid/content/Intent;
    :catch_59
    move-exception v1

    #@5a
    .line 81
    .local v1, e:Ljava/io/IOException;
    :try_start_5a
    new-instance v4, Ljava/lang/RuntimeException;

    #@5c
    const-string v5, "Error parsing alias"

    #@5e
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@61
    throw v4
    :try_end_62
    .catchall {:try_start_5a .. :try_end_62} :catchall_2f
.end method
