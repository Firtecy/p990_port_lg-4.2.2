.class Landroid/app/IThumbnailRetriever$Stub$Proxy;
.super Ljava/lang/Object;
.source "IThumbnailRetriever.java"

# interfaces
.implements Landroid/app/IThumbnailRetriever;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IThumbnailRetriever$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    iput-object p1, p0, Landroid/app/IThumbnailRetriever$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 74
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Landroid/app/IThumbnailRetriever$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 81
    const-string v0, "android.app.IThumbnailRetriever"

    #@2
    return-object v0
.end method

.method public getThumbnail(I)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "index"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 85
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 86
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 89
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.app.IThumbnailRetriever"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 90
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 91
    iget-object v3, p0, Landroid/app/IThumbnailRetriever$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 92
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 93
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_2f

    #@20
    .line 94
    sget-object v3, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@22
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/graphics/Bitmap;
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_31

    #@28
    .line 101
    .local v2, _result:Landroid/graphics/Bitmap;
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 102
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 104
    return-object v2

    #@2f
    .line 97
    .end local v2           #_result:Landroid/graphics/Bitmap;
    :cond_2f
    const/4 v2, 0x0

    #@30
    .restart local v2       #_result:Landroid/graphics/Bitmap;
    goto :goto_28

    #@31
    .line 101
    .end local v2           #_result:Landroid/graphics/Bitmap;
    :catchall_31
    move-exception v3

    #@32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 102
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@38
    throw v3
.end method
