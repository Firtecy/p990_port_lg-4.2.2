.class Landroid/app/WallpaperManager$Globals;
.super Landroid/app/IWallpaperManagerCallback$Stub;
.source "WallpaperManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/WallpaperManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Globals"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    }
.end annotation


# static fields
.field private static final MSG_CLEAR_WALLPAPER:I = 0x1


# instance fields
.field private mDefaultWallpaper:Landroid/graphics/Bitmap;

.field private final mHandler:Landroid/os/Handler;

.field private mService:Landroid/app/IWallpaperManager;

.field private mWallpaper:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .registers 4
    .parameter "looper"

    #@0
    .prologue
    .line 236
    invoke-direct {p0}, Landroid/app/IWallpaperManagerCallback$Stub;-><init>()V

    #@3
    .line 237
    const-string/jumbo v1, "wallpaper"

    #@6
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@9
    move-result-object v0

    #@a
    .line 238
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Landroid/app/IWallpaperManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IWallpaperManager;

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@10
    .line 239
    new-instance v1, Landroid/app/WallpaperManager$Globals$1;

    #@12
    invoke-direct {v1, p0, p1}, Landroid/app/WallpaperManager$Globals$1;-><init>(Landroid/app/WallpaperManager$Globals;Landroid/os/Looper;)V

    #@15
    iput-object v1, p0, Landroid/app/WallpaperManager$Globals;->mHandler:Landroid/os/Handler;

    #@17
    .line 252
    return-void
.end method

.method static synthetic access$002(Landroid/app/WallpaperManager$Globals;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@2
    return-object p1
.end method

.method static synthetic access$102(Landroid/app/WallpaperManager$Globals;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@2
    return-object v0
.end method

.method private getCurrentWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 16
    .parameter "context"

    #@0
    .prologue
    .line 322
    :try_start_0
    new-instance v8, Landroid/os/Bundle;

    #@2
    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 323
    .local v8, params:Landroid/os/Bundle;
    iget-object v12, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@7
    invoke-interface {v12, p0, v8}, Landroid/app/IWallpaperManager;->getWallpaper(Landroid/app/IWallpaperManagerCallback;Landroid/os/Bundle;)Landroid/os/ParcelFileDescriptor;

    #@a
    move-result-object v3

    #@b
    .line 324
    .local v3, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v3, :cond_90

    #@d
    .line 325
    const-string/jumbo v12, "width"

    #@10
    const/4 v13, 0x0

    #@11
    invoke-virtual {v8, v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@14
    move-result v11

    #@15
    .line 326
    .local v11, width:I
    const-string v12, "height"

    #@17
    const/4 v13, 0x0

    #@18
    invoke-virtual {v8, v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@1b
    move-result v4

    #@1c
    .line 329
    .local v4, height:I
    sget-boolean v12, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@1e
    if-eqz v12, :cond_92

    #@20
    .line 330
    const-string v12, "drmKey"

    #@22
    invoke-virtual {v8, v12}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_25} :catch_8f

    #@25
    move-result-object v1

    #@26
    .line 331
    .local v1, decInfo:[B
    if-eqz v1, :cond_92

    #@28
    .line 332
    const/4 v6, 0x0

    #@29
    .line 334
    .local v6, is:Ljava/io/InputStream;
    :try_start_29
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@2c
    move-result-object v12

    #@2d
    invoke-static {v12, v1}, Lcom/lge/lgdrm/DrmContentSession;->openDrmStream(Ljava/io/FileDescriptor;[B)Lcom/lge/lgdrm/DrmStream;
    :try_end_30
    .catchall {:try_start_29 .. :try_end_30} :catchall_85
    .catch Ljava/lang/OutOfMemoryError; {:try_start_29 .. :try_end_30} :catch_71
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_30} :catch_bf

    #@30
    move-result-object v6

    #@31
    .line 335
    if-nez v6, :cond_3d

    #@33
    .line 336
    const/4 v12, 0x0

    #@34
    .line 366
    :try_start_34
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@37
    .line 367
    if-eqz v6, :cond_3c

    #@39
    .line 368
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_3c} :catch_6f
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_3c} :catch_8f

    #@3c
    .line 396
    .end local v1           #decInfo:[B
    .end local v3           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v4           #height:I
    .end local v6           #is:Ljava/io/InputStream;
    .end local v8           #params:Landroid/os/Bundle;
    .end local v11           #width:I
    :cond_3c
    :goto_3c
    return-object v12

    #@3d
    .line 339
    .restart local v1       #decInfo:[B
    .restart local v3       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v4       #height:I
    .restart local v6       #is:Ljava/io/InputStream;
    .restart local v8       #params:Landroid/os/Bundle;
    .restart local v11       #width:I
    :cond_3d
    const/4 v9, 0x0

    #@3e
    .line 340
    .local v9, sampleSize:I
    :try_start_3e
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->available()I

    #@41
    move-result v10

    #@42
    .line 345
    .local v10, size:I
    const v12, 0x4b000

    #@45
    if-lt v10, v12, :cond_54

    #@47
    .line 346
    const/4 v5, 0x0

    #@48
    .line 347
    .local v5, i:I
    const/4 v5, 0x2

    #@49
    :goto_49
    div-int v12, v10, v5

    #@4b
    const v13, 0x4b000

    #@4e
    if-le v12, v13, :cond_53

    #@50
    .line 349
    mul-int/lit8 v5, v5, 0x2

    #@52
    goto :goto_49

    #@53
    .line 351
    :cond_53
    move v9, v5

    #@54
    .line 354
    .end local v5           #i:I
    :cond_54
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    #@56
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@59
    .line 355
    .local v7, options:Landroid/graphics/BitmapFactory$Options;
    if-eqz v9, :cond_5d

    #@5b
    .line 356
    iput v9, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    #@5d
    .line 358
    :cond_5d
    const/4 v12, 0x0

    #@5e
    invoke-static {v6, v12, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@61
    move-result-object v0

    #@62
    .line 360
    .local v0, bm:Landroid/graphics/Bitmap;
    invoke-static {p1, v0, v11, v4}, Landroid/app/WallpaperManager;->generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_65
    .catchall {:try_start_3e .. :try_end_65} :catchall_85
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3e .. :try_end_65} :catch_71
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_65} :catch_bf

    #@65
    move-result-object v12

    #@66
    .line 366
    :try_start_66
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@69
    .line 367
    if-eqz v6, :cond_3c

    #@6b
    .line 368
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_6e} :catch_6f
    .catch Landroid/os/RemoteException; {:try_start_66 .. :try_end_6e} :catch_8f

    #@6e
    goto :goto_3c

    #@6f
    .line 370
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v9           #sampleSize:I
    .end local v10           #size:I
    :catch_6f
    move-exception v13

    #@70
    goto :goto_3c

    #@71
    .line 361
    :catch_71
    move-exception v2

    #@72
    .line 362
    .local v2, e:Ljava/lang/OutOfMemoryError;
    :try_start_72
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@75
    move-result-object v12

    #@76
    const-string v13, "Can\'t decode file"

    #@78
    invoke-static {v12, v13, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7b
    .catchall {:try_start_72 .. :try_end_7b} :catchall_85

    #@7b
    .line 366
    :try_start_7b
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@7e
    .line 367
    if-eqz v6, :cond_83

    #@80
    .line 368
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->close()V
    :try_end_83
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_83} :catch_c9
    .catch Landroid/os/RemoteException; {:try_start_7b .. :try_end_83} :catch_8f

    #@83
    .line 373
    .end local v2           #e:Ljava/lang/OutOfMemoryError;
    :cond_83
    :goto_83
    const/4 v12, 0x0

    #@84
    goto :goto_3c

    #@85
    .line 365
    :catchall_85
    move-exception v12

    #@86
    .line 366
    :try_start_86
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@89
    .line 367
    if-eqz v6, :cond_8e

    #@8b
    .line 368
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_8e} :catch_cd
    .catch Landroid/os/RemoteException; {:try_start_86 .. :try_end_8e} :catch_8f

    #@8e
    .line 365
    :cond_8e
    :goto_8e
    :try_start_8e
    throw v12
    :try_end_8f
    .catch Landroid/os/RemoteException; {:try_start_8e .. :try_end_8f} :catch_8f

    #@8f
    .line 393
    .end local v1           #decInfo:[B
    .end local v3           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v4           #height:I
    .end local v6           #is:Ljava/io/InputStream;
    .end local v8           #params:Landroid/os/Bundle;
    .end local v11           #width:I
    :catch_8f
    move-exception v12

    #@90
    .line 396
    :cond_90
    :goto_90
    const/4 v12, 0x0

    #@91
    goto :goto_3c

    #@92
    .line 379
    .restart local v3       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v4       #height:I
    .restart local v8       #params:Landroid/os/Bundle;
    .restart local v11       #width:I
    :cond_92
    :try_start_92
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    #@94
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@97
    .line 380
    .restart local v7       #options:Landroid/graphics/BitmapFactory$Options;
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@9a
    move-result-object v12

    #@9b
    const/4 v13, 0x0

    #@9c
    invoke-static {v12, v13, v7}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@9f
    move-result-object v0

    #@a0
    .line 382
    .restart local v0       #bm:Landroid/graphics/Bitmap;
    invoke-static {p1, v0, v11, v4}, Landroid/app/WallpaperManager;->generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_a3
    .catchall {:try_start_92 .. :try_end_a3} :catchall_ba
    .catch Ljava/lang/OutOfMemoryError; {:try_start_92 .. :try_end_a3} :catch_aa

    #@a3
    move-result-object v12

    #@a4
    .line 387
    :try_start_a4
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_a7
    .catch Ljava/io/IOException; {:try_start_a4 .. :try_end_a7} :catch_a8
    .catch Landroid/os/RemoteException; {:try_start_a4 .. :try_end_a7} :catch_8f

    #@a7
    goto :goto_3c

    #@a8
    .line 388
    :catch_a8
    move-exception v13

    #@a9
    goto :goto_3c

    #@aa
    .line 383
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    :catch_aa
    move-exception v2

    #@ab
    .line 384
    .restart local v2       #e:Ljava/lang/OutOfMemoryError;
    :try_start_ab
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@ae
    move-result-object v12

    #@af
    const-string v13, "Can\'t decode file"

    #@b1
    invoke-static {v12, v13, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b4
    .catchall {:try_start_ab .. :try_end_b4} :catchall_ba

    #@b4
    .line 387
    :try_start_b4
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_b7
    .catch Ljava/io/IOException; {:try_start_b4 .. :try_end_b7} :catch_b8
    .catch Landroid/os/RemoteException; {:try_start_b4 .. :try_end_b7} :catch_8f

    #@b7
    goto :goto_90

    #@b8
    .line 388
    :catch_b8
    move-exception v12

    #@b9
    goto :goto_90

    #@ba
    .line 386
    .end local v2           #e:Ljava/lang/OutOfMemoryError;
    :catchall_ba
    move-exception v12

    #@bb
    .line 387
    :try_start_bb
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_be
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_be} :catch_cb
    .catch Landroid/os/RemoteException; {:try_start_bb .. :try_end_be} :catch_8f

    #@be
    .line 386
    :goto_be
    :try_start_be
    throw v12
    :try_end_bf
    .catch Landroid/os/RemoteException; {:try_start_be .. :try_end_bf} :catch_8f

    #@bf
    .line 363
    .restart local v1       #decInfo:[B
    .restart local v6       #is:Ljava/io/InputStream;
    :catch_bf
    move-exception v12

    #@c0
    .line 366
    :try_start_c0
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    #@c3
    .line 367
    if-eqz v6, :cond_83

    #@c5
    .line 368
    invoke-virtual {v6}, Lcom/lge/lgdrm/DrmStream;->close()V
    :try_end_c8
    .catch Ljava/io/IOException; {:try_start_c0 .. :try_end_c8} :catch_c9
    .catch Landroid/os/RemoteException; {:try_start_c0 .. :try_end_c8} :catch_8f

    #@c8
    goto :goto_83

    #@c9
    .line 370
    :catch_c9
    move-exception v12

    #@ca
    goto :goto_83

    #@cb
    .line 388
    .end local v1           #decInfo:[B
    .end local v6           #is:Ljava/io/InputStream;
    :catch_cb
    move-exception v13

    #@cc
    goto :goto_be

    #@cd
    .line 370
    .restart local v1       #decInfo:[B
    .restart local v6       #is:Ljava/io/InputStream;
    :catch_cd
    move-exception v13

    #@ce
    goto :goto_8e
.end method

.method private getDefaultWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 11
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 574
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4
    move-result-object v6

    #@5
    const v8, 0x1080222

    #@8
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@b
    move-result-object v3

    #@c
    .line 576
    .local v3, is:Ljava/io/InputStream;
    if-eqz v3, :cond_39

    #@e
    .line 577
    iget-object v6, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@10
    invoke-interface {v6}, Landroid/app/IWallpaperManager;->getWidthHint()I

    #@13
    move-result v5

    #@14
    .line 578
    .local v5, width:I
    iget-object v6, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@16
    invoke-interface {v6}, Landroid/app/IWallpaperManager;->getHeightHint()I
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_19} :catch_40

    #@19
    move-result v2

    #@1a
    .line 581
    .local v2, height:I
    :try_start_1a
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    #@1c
    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@1f
    .line 582
    .local v4, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v6, 0x0

    #@20
    invoke-static {v3, v6, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@23
    move-result-object v0

    #@24
    .line 583
    .local v0, bm:Landroid/graphics/Bitmap;
    invoke-static {p1, v0, v5, v2}, Landroid/app/WallpaperManager;->generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_27
    .catchall {:try_start_1a .. :try_end_27} :catchall_3b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1a .. :try_end_27} :catch_2c

    #@27
    move-result-object v6

    #@28
    .line 588
    :try_start_28
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2b} :catch_46
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_2b} :catch_40

    #@2b
    .line 597
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v2           #height:I
    .end local v3           #is:Ljava/io/InputStream;
    .end local v4           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v5           #width:I
    :goto_2b
    return-object v6

    #@2c
    .line 584
    .restart local v2       #height:I
    .restart local v3       #is:Ljava/io/InputStream;
    .restart local v5       #width:I
    :catch_2c
    move-exception v1

    #@2d
    .line 585
    .local v1, e:Ljava/lang/OutOfMemoryError;
    :try_start_2d
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@30
    move-result-object v6

    #@31
    const-string v8, "Can\'t decode stream"

    #@33
    invoke-static {v6, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_36
    .catchall {:try_start_2d .. :try_end_36} :catchall_3b

    #@36
    .line 588
    :try_start_36
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_36 .. :try_end_39} :catch_44
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_39} :catch_40

    #@39
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    .end local v2           #height:I
    .end local v3           #is:Ljava/io/InputStream;
    .end local v5           #width:I
    :cond_39
    :goto_39
    move-object v6, v7

    #@3a
    .line 597
    goto :goto_2b

    #@3b
    .line 587
    .restart local v2       #height:I
    .restart local v3       #is:Ljava/io/InputStream;
    .restart local v5       #width:I
    :catchall_3b
    move-exception v6

    #@3c
    .line 588
    :try_start_3c
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_3f} :catch_42
    .catch Landroid/os/RemoteException; {:try_start_3c .. :try_end_3f} :catch_40

    #@3f
    .line 587
    :goto_3f
    :try_start_3f
    throw v6
    :try_end_40
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_40} :catch_40

    #@40
    .line 594
    .end local v2           #height:I
    .end local v3           #is:Ljava/io/InputStream;
    .end local v5           #width:I
    :catch_40
    move-exception v6

    #@41
    goto :goto_39

    #@42
    .line 589
    .restart local v2       #height:I
    .restart local v3       #is:Ljava/io/InputStream;
    .restart local v5       #width:I
    :catch_42
    move-exception v8

    #@43
    goto :goto_3f

    #@44
    .restart local v1       #e:Ljava/lang/OutOfMemoryError;
    :catch_44
    move-exception v6

    #@45
    goto :goto_39

    #@46
    .end local v1           #e:Ljava/lang/OutOfMemoryError;
    .restart local v0       #bm:Landroid/graphics/Bitmap;
    .restart local v4       #options:Landroid/graphics/BitmapFactory$Options;
    :catch_46
    move-exception v7

    #@47
    goto :goto_2b
.end method

.method private getDefaultWallpaperLockedFromCarrierPartition(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 15
    .parameter "context"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 413
    const/4 v4, 0x0

    #@2
    .line 414
    .local v4, fis:Ljava/io/FileInputStream;
    const/4 v0, 0x0

    #@3
    .line 418
    .local v0, bis:Ljava/io/BufferedInputStream;
    const-string/jumbo v11, "ro.sprint.hfa.flag"

    #@6
    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v8

    #@a
    .line 419
    .local v8, phoneActivated:Ljava/lang/String;
    const-string v11, "activationOK"

    #@c
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v11

    #@10
    if-nez v11, :cond_13

    #@12
    .line 472
    :goto_12
    return-object v10

    #@13
    .line 426
    :cond_13
    :try_start_13
    new-instance v5, Ljava/io/FileInputStream;

    #@15
    const-string v11, "/carrier/media/default_wallpaper.jpg"

    #@17
    invoke-direct {v5, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_cf
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_1a} :catch_ff
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_1a} :catch_97
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_1a} :catch_a5
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_1a} :catch_b3
    .catch Ljava/lang/NullPointerException; {:try_start_13 .. :try_end_1a} :catch_c1

    #@1a
    .line 427
    .end local v4           #fis:Ljava/io/FileInputStream;
    .local v5, fis:Ljava/io/FileInputStream;
    :try_start_1a
    new-instance v1, Ljava/io/BufferedInputStream;

    #@1c
    invoke-direct {v1, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_dc
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_1f} :catch_101
    .catch Ljava/lang/SecurityException; {:try_start_1a .. :try_end_1f} :catch_f8
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1f} :catch_f1
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1f} :catch_ea
    .catch Ljava/lang/NullPointerException; {:try_start_1a .. :try_end_1f} :catch_e3

    #@1f
    .line 429
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .local v1, bis:Ljava/io/BufferedInputStream;
    if-eqz v5, :cond_61

    #@21
    .line 430
    :try_start_21
    iget-object v11, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@23
    invoke-interface {v11}, Landroid/app/IWallpaperManager;->getWidthHint()I

    #@26
    move-result v9

    #@27
    .line 431
    .local v9, width:I
    iget-object v11, p0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@29
    invoke-interface {v11}, Landroid/app/IWallpaperManager;->getHeightHint()I
    :try_end_2c
    .catchall {:try_start_21 .. :try_end_2c} :catchall_df
    .catch Ljava/io/FileNotFoundException; {:try_start_21 .. :try_end_2c} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_21 .. :try_end_2c} :catch_fb
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_2c} :catch_f4
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2c} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_21 .. :try_end_2c} :catch_e6

    #@2c
    move-result v6

    #@2d
    .line 434
    .local v6, height:I
    :try_start_2d
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    #@2f
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@32
    .line 435
    .local v7, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v11, 0x0

    #@33
    invoke-static {v1, v11, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@36
    move-result-object v2

    #@37
    .line 437
    .local v2, bm:Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    #@3a
    move-result v11

    #@3b
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    #@3e
    move-result v12

    #@3f
    invoke-static {p1, v2, v11, v12}, Landroid/app/WallpaperManager;->generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_42
    .catchall {:try_start_2d .. :try_end_42} :catchall_7c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2d .. :try_end_42} :catch_52
    .catch Ljava/lang/NullPointerException; {:try_start_2d .. :try_end_42} :catch_6a

    #@42
    move-result-object v11

    #@43
    .line 444
    if-eqz v5, :cond_48

    #@45
    .line 445
    :try_start_45
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_df
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_d9
    .catch Ljava/io/FileNotFoundException; {:try_start_45 .. :try_end_48} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_45 .. :try_end_48} :catch_fb
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_48} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_45 .. :try_end_48} :catch_e6

    #@48
    .line 468
    :cond_48
    :goto_48
    invoke-static {v5}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@4b
    .line 469
    invoke-static {v1}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@4e
    move-object v0, v1

    #@4f
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@50
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    move-object v10, v11

    #@51
    .line 437
    goto :goto_12

    #@52
    .line 438
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v2           #bm:Landroid/graphics/Bitmap;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_52
    move-exception v3

    #@53
    .line 439
    .local v3, e:Ljava/lang/OutOfMemoryError;
    :try_start_53
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@56
    move-result-object v11

    #@57
    const-string v12, "Can\'t decode stream"

    #@59
    invoke-static {v11, v12, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5c
    .catchall {:try_start_53 .. :try_end_5c} :catchall_7c

    #@5c
    .line 444
    if-eqz v5, :cond_61

    #@5e
    .line 445
    :try_start_5e
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_61
    .catchall {:try_start_5e .. :try_end_61} :catchall_df
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_61} :catch_7a
    .catch Ljava/io/FileNotFoundException; {:try_start_5e .. :try_end_61} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_5e .. :try_end_61} :catch_fb
    .catch Landroid/os/RemoteException; {:try_start_5e .. :try_end_61} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_5e .. :try_end_61} :catch_e6

    #@61
    .line 468
    .end local v3           #e:Ljava/lang/OutOfMemoryError;
    .end local v6           #height:I
    .end local v9           #width:I
    :cond_61
    :goto_61
    invoke-static {v5}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@64
    .line 469
    invoke-static {v1}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@67
    move-object v0, v1

    #@68
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@69
    .line 470
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_12

    #@6a
    .line 440
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    .restart local v6       #height:I
    .restart local v9       #width:I
    :catch_6a
    move-exception v3

    #@6b
    .line 441
    .local v3, e:Ljava/lang/NullPointerException;
    :try_start_6b
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@6e
    move-result-object v11

    #@6f
    const-string v12, "Image Decoding Error"

    #@71
    invoke-static {v11, v12, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_74
    .catchall {:try_start_6b .. :try_end_74} :catchall_7c

    #@74
    .line 444
    if-eqz v5, :cond_61

    #@76
    .line 445
    :try_start_76
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_79
    .catchall {:try_start_76 .. :try_end_79} :catchall_df
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_79} :catch_7a
    .catch Ljava/io/FileNotFoundException; {:try_start_76 .. :try_end_79} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_76 .. :try_end_79} :catch_fb
    .catch Landroid/os/RemoteException; {:try_start_76 .. :try_end_79} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_76 .. :try_end_79} :catch_e6

    #@79
    goto :goto_61

    #@7a
    .line 447
    .end local v3           #e:Ljava/lang/NullPointerException;
    :catch_7a
    move-exception v11

    #@7b
    goto :goto_61

    #@7c
    .line 443
    :catchall_7c
    move-exception v11

    #@7d
    .line 444
    if-eqz v5, :cond_82

    #@7f
    .line 445
    :try_start_7f
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_82
    .catchall {:try_start_7f .. :try_end_82} :catchall_df
    .catch Ljava/io/IOException; {:try_start_7f .. :try_end_82} :catch_d7
    .catch Ljava/io/FileNotFoundException; {:try_start_7f .. :try_end_82} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_7f .. :try_end_82} :catch_fb
    .catch Landroid/os/RemoteException; {:try_start_7f .. :try_end_82} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_7f .. :try_end_82} :catch_e6

    #@82
    .line 443
    :cond_82
    :goto_82
    :try_start_82
    throw v11
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_df
    .catch Ljava/io/FileNotFoundException; {:try_start_82 .. :try_end_83} :catch_83
    .catch Ljava/lang/SecurityException; {:try_start_82 .. :try_end_83} :catch_fb
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_83} :catch_f4
    .catch Landroid/os/RemoteException; {:try_start_82 .. :try_end_83} :catch_ed
    .catch Ljava/lang/NullPointerException; {:try_start_82 .. :try_end_83} :catch_e6

    #@83
    .line 452
    .end local v6           #height:I
    .end local v9           #width:I
    :catch_83
    move-exception v3

    #@84
    move-object v0, v1

    #@85
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@86
    .line 453
    .end local v5           #fis:Ljava/io/FileInputStream;
    .local v3, e:Ljava/io/FileNotFoundException;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    :goto_86
    :try_start_86
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@89
    move-result-object v11

    #@8a
    const-string v12, "default_wallpaper.jpg is not exist in carrier partition"

    #@8c
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8f
    .catchall {:try_start_86 .. :try_end_8f} :catchall_cf

    #@8f
    .line 468
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@92
    .line 469
    .end local v3           #e:Ljava/io/FileNotFoundException;
    :goto_92
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@95
    goto/16 :goto_12

    #@97
    .line 455
    :catch_97
    move-exception v3

    #@98
    .line 456
    .local v3, e:Ljava/lang/SecurityException;
    :goto_98
    :try_start_98
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@9b
    move-result-object v11

    #@9c
    const-string v12, "SecurityException in getDefaultWallpaperLockedFromCarrierPartition"

    #@9e
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a1
    .catchall {:try_start_98 .. :try_end_a1} :catchall_cf

    #@a1
    .line 468
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@a4
    goto :goto_92

    #@a5
    .line 458
    .end local v3           #e:Ljava/lang/SecurityException;
    :catch_a5
    move-exception v3

    #@a6
    .line 459
    .local v3, e:Ljava/io/IOException;
    :goto_a6
    :try_start_a6
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@a9
    move-result-object v11

    #@aa
    const-string v12, "IOException in getDefaultWallpaperLockedFromCarrierPartition"

    #@ac
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_af
    .catchall {:try_start_a6 .. :try_end_af} :catchall_cf

    #@af
    .line 468
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@b2
    goto :goto_92

    #@b3
    .line 461
    .end local v3           #e:Ljava/io/IOException;
    :catch_b3
    move-exception v3

    #@b4
    .line 462
    .local v3, e:Landroid/os/RemoteException;
    :goto_b4
    :try_start_b4
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@b7
    move-result-object v11

    #@b8
    const-string v12, "RemoteException in getDefaultWallpaperLockedFromCarrierPartition"

    #@ba
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_bd
    .catchall {:try_start_b4 .. :try_end_bd} :catchall_cf

    #@bd
    .line 468
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@c0
    goto :goto_92

    #@c1
    .line 464
    .end local v3           #e:Landroid/os/RemoteException;
    :catch_c1
    move-exception v3

    #@c2
    .line 465
    .local v3, e:Ljava/lang/NullPointerException;
    :goto_c2
    :try_start_c2
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@c5
    move-result-object v11

    #@c6
    const-string v12, "Image Decoding Error in getDefaultWallpaperLockedFromCarrierPartition"

    #@c8
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_cb
    .catchall {:try_start_c2 .. :try_end_cb} :catchall_cf

    #@cb
    .line 468
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@ce
    goto :goto_92

    #@cf
    .end local v3           #e:Ljava/lang/NullPointerException;
    :catchall_cf
    move-exception v10

    #@d0
    :goto_d0
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@d3
    .line 469
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->quiteinputStream(Ljava/io/InputStream;)V

    #@d6
    .line 468
    throw v10

    #@d7
    .line 447
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    .restart local v6       #height:I
    .restart local v9       #width:I
    :catch_d7
    move-exception v12

    #@d8
    goto :goto_82

    #@d9
    .restart local v2       #bm:Landroid/graphics/Bitmap;
    .restart local v7       #options:Landroid/graphics/BitmapFactory$Options;
    :catch_d9
    move-exception v10

    #@da
    goto/16 :goto_48

    #@dc
    .line 468
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .end local v2           #bm:Landroid/graphics/Bitmap;
    .end local v6           #height:I
    .end local v7           #options:Landroid/graphics/BitmapFactory$Options;
    .end local v9           #width:I
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    :catchall_dc
    move-exception v10

    #@dd
    move-object v4, v5

    #@de
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_d0

    #@df
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catchall_df
    move-exception v10

    #@e0
    move-object v0, v1

    #@e1
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@e2
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_d0

    #@e3
    .line 464
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_e3
    move-exception v3

    #@e4
    move-object v4, v5

    #@e5
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_c2

    #@e6
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_e6
    move-exception v3

    #@e7
    move-object v0, v1

    #@e8
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@e9
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_c2

    #@ea
    .line 461
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_ea
    move-exception v3

    #@eb
    move-object v4, v5

    #@ec
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_b4

    #@ed
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_ed
    move-exception v3

    #@ee
    move-object v0, v1

    #@ef
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@f0
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_b4

    #@f1
    .line 458
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_f1
    move-exception v3

    #@f2
    move-object v4, v5

    #@f3
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_a6

    #@f4
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_f4
    move-exception v3

    #@f5
    move-object v0, v1

    #@f6
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@f7
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_a6

    #@f8
    .line 455
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_f8
    move-exception v3

    #@f9
    move-object v4, v5

    #@fa
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_98

    #@fb
    .end local v0           #bis:Ljava/io/BufferedInputStream;
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v1       #bis:Ljava/io/BufferedInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_fb
    move-exception v3

    #@fc
    move-object v0, v1

    #@fd
    .end local v1           #bis:Ljava/io/BufferedInputStream;
    .restart local v0       #bis:Ljava/io/BufferedInputStream;
    move-object v4, v5

    #@fe
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_98

    #@ff
    .line 452
    :catch_ff
    move-exception v3

    #@100
    goto :goto_86

    #@101
    .end local v4           #fis:Ljava/io/FileInputStream;
    .restart local v5       #fis:Ljava/io/FileInputStream;
    :catch_101
    move-exception v3

    #@102
    move-object v4, v5

    #@103
    .end local v5           #fis:Ljava/io/FileInputStream;
    .restart local v4       #fis:Ljava/io/FileInputStream;
    goto :goto_86
.end method

.method private getDefaultWallpaperLockedFromNetworkCode(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 23
    .parameter "context"

    #@0
    .prologue
    .line 494
    const/4 v6, 0x0

    #@1
    .line 496
    .local v6, is:Ljava/io/InputStream;
    :try_start_1
    const-string/jumbo v17, "ro.sprint.hfa.flag"

    #@4
    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v10

    #@8
    .line 497
    .local v10, phoneActivated:Ljava/lang/String;
    const-string v17, "activationOK"

    #@a
    move-object/from16 v0, v17

    #@c
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v17

    #@10
    if-eqz v17, :cond_106

    #@12
    .line 498
    const-string/jumbo v17, "ro.cdma.home.operator.numeric"

    #@15
    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v7

    #@19
    .line 501
    .local v7, networkCode:Ljava/lang/String;
    const/4 v8, 0x0

    #@1a
    .line 502
    .local v8, noOfCarrier:I
    const-string/jumbo v17, "ro.lge.chameleon_no_of_carrier"

    #@1d
    const-string v18, "0"

    #@1f
    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@22
    move-result-object v13

    #@23
    .line 503
    .local v13, strNoOfCarrier:Ljava/lang/String;
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@26
    move-result v8

    #@27
    .line 505
    const/4 v5, 0x0

    #@28
    .local v5, i:I
    :goto_28
    if-ge v5, v8, :cond_f4

    #@2a
    .line 506
    const-string/jumbo v15, "ro.lge.chameleon_shut_wall_"

    #@2d
    .line 509
    .local v15, strPropNameOfCarrierInfo:Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@30
    move-result-object v17

    #@31
    move-object/from16 v0, v17

    #@33
    invoke-virtual {v15, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@36
    move-result-object v15

    #@37
    .line 510
    const-string v17, "11111,0,0,0,0,0"

    #@39
    move-object/from16 v0, v17

    #@3b
    invoke-static {v15, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v14

    #@3f
    .line 511
    .local v14, strPropCarrierInfo:Ljava/lang/String;
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@42
    move-result-object v17

    #@43
    new-instance v18, Ljava/lang/StringBuilder;

    #@45
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v19, "fisher === strPropNameOfCarrierInfo: "

    #@4a
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v18

    #@4e
    move-object/from16 v0, v18

    #@50
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v18

    #@54
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v18

    #@58
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 512
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@5e
    move-result-object v17

    #@5f
    new-instance v18, Ljava/lang/StringBuilder;

    #@61
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v19, "fisher === strPropCarrierInfo: "

    #@66
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v18

    #@6a
    move-object/from16 v0, v18

    #@6c
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v18

    #@70
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v18

    #@74
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 514
    new-instance v11, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;

    #@79
    invoke-direct {v11, v14}, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;-><init>(Ljava/lang/String;)V

    #@7c
    .line 516
    .local v11, r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    iget-object v0, v11, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->networkCode:Ljava/lang/String;

    #@7e
    move-object/from16 v17, v0

    #@80
    move-object/from16 v0, v17

    #@82
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@85
    move-result v17

    #@86
    if-eqz v17, :cond_102

    #@88
    .line 517
    iget-object v0, v11, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->resourceName:Ljava/lang/String;

    #@8a
    move-object/from16 v17, v0

    #@8c
    const-string v18, "0"

    #@8e
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v17

    #@92
    if-eqz v17, :cond_a0

    #@94
    .line 518
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@97
    move-result-object v17

    #@98
    const-string v18, "defaultWallpaper: 0"

    #@9a
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 519
    const/16 v17, 0x0

    #@9f
    .line 565
    .end local v5           #i:I
    .end local v7           #networkCode:Ljava/lang/String;
    .end local v8           #noOfCarrier:I
    .end local v10           #phoneActivated:Ljava/lang/String;
    .end local v11           #r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    .end local v13           #strNoOfCarrier:Ljava/lang/String;
    .end local v14           #strPropCarrierInfo:Ljava/lang/String;
    .end local v15           #strPropNameOfCarrierInfo:Ljava/lang/String;
    :goto_9f
    return-object v17

    #@a0
    .line 520
    .restart local v5       #i:I
    .restart local v7       #networkCode:Ljava/lang/String;
    .restart local v8       #noOfCarrier:I
    .restart local v10       #phoneActivated:Ljava/lang/String;
    .restart local v11       #r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    .restart local v13       #strNoOfCarrier:Ljava/lang/String;
    .restart local v14       #strPropCarrierInfo:Ljava/lang/String;
    .restart local v15       #strPropNameOfCarrierInfo:Ljava/lang/String;
    :cond_a0
    iget-object v0, v11, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->resourceName:Ljava/lang/String;

    #@a2
    move-object/from16 v17, v0

    #@a4
    const-string/jumbo v18, "no"

    #@a7
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@aa
    move-result v17

    #@ab
    if-eqz v17, :cond_b9

    #@ad
    .line 521
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@b0
    move-result-object v17

    #@b1
    const-string v18, "defaultWallpaper: no"

    #@b3
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 522
    const/16 v17, 0x0

    #@b8
    goto :goto_9f

    #@b9
    .line 525
    :cond_b9
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@bc
    move-result-object v17

    #@bd
    iget-object v0, v11, Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;->resourceName:Ljava/lang/String;

    #@bf
    move-object/from16 v18, v0

    #@c1
    const-string v19, "drawable"

    #@c3
    const-string v20, "android"

    #@c5
    invoke-virtual/range {v17 .. v20}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    move-result v12

    #@c9
    .line 526
    .local v12, resID:I
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@cc
    move-result-object v17

    #@cd
    new-instance v18, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v19, "chameleonStringResourceID: "

    #@d4
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v18

    #@d8
    move-object/from16 v0, v18

    #@da
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v18

    #@de
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v18

    #@e2
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 527
    if-nez v12, :cond_ea

    #@e7
    .line 528
    const/16 v17, 0x0

    #@e9
    goto :goto_9f

    #@ea
    .line 530
    :cond_ea
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@ed
    move-result-object v17

    #@ee
    move-object/from16 v0, v17

    #@f0
    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@f3
    move-result-object v6

    #@f4
    .line 535
    .end local v11           #r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    .end local v12           #resID:I
    .end local v14           #strPropCarrierInfo:Ljava/lang/String;
    .end local v15           #strPropNameOfCarrierInfo:Ljava/lang/String;
    :cond_f4
    if-nez v6, :cond_109

    #@f6
    .line 536
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@f9
    move-result-object v17

    #@fa
    const-string v18, "chameleonNetworkIDNotMatched"

    #@fc
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 537
    const/16 v17, 0x0

    #@101
    goto :goto_9f

    #@102
    .line 505
    .restart local v11       #r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    .restart local v14       #strPropCarrierInfo:Ljava/lang/String;
    .restart local v15       #strPropNameOfCarrierInfo:Ljava/lang/String;
    :cond_102
    add-int/lit8 v5, v5, 0x1

    #@104
    goto/16 :goto_28

    #@106
    .line 540
    .end local v5           #i:I
    .end local v7           #networkCode:Ljava/lang/String;
    .end local v8           #noOfCarrier:I
    .end local v11           #r:Landroid/app/WallpaperManager$Globals$SprintChameleonAttributes;
    .end local v13           #strNoOfCarrier:Ljava/lang/String;
    .end local v14           #strPropCarrierInfo:Ljava/lang/String;
    .end local v15           #strPropNameOfCarrierInfo:Ljava/lang/String;
    :cond_106
    const/16 v17, 0x0

    #@108
    goto :goto_9f

    #@109
    .line 544
    .restart local v5       #i:I
    .restart local v7       #networkCode:Ljava/lang/String;
    .restart local v8       #noOfCarrier:I
    .restart local v13       #strNoOfCarrier:Ljava/lang/String;
    :cond_109
    if-eqz v6, :cond_14d

    #@10b
    .line 545
    move-object/from16 v0, p0

    #@10d
    iget-object v0, v0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@10f
    move-object/from16 v17, v0

    #@111
    invoke-interface/range {v17 .. v17}, Landroid/app/IWallpaperManager;->getWidthHint()I

    #@114
    move-result v16

    #@115
    .line 546
    .local v16, width:I
    move-object/from16 v0, p0

    #@117
    iget-object v0, v0, Landroid/app/WallpaperManager$Globals;->mService:Landroid/app/IWallpaperManager;

    #@119
    move-object/from16 v17, v0

    #@11b
    invoke-interface/range {v17 .. v17}, Landroid/app/IWallpaperManager;->getHeightHint()I
    :try_end_11e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_11e} :catch_156

    #@11e
    move-result v4

    #@11f
    .line 549
    .local v4, height:I
    :try_start_11f
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    #@121
    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@124
    .line 550
    .local v9, options:Landroid/graphics/BitmapFactory$Options;
    const/16 v17, 0x0

    #@126
    move-object/from16 v0, v17

    #@128
    invoke-static {v6, v0, v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@12b
    move-result-object v2

    #@12c
    .line 551
    .local v2, bm:Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    #@12e
    move/from16 v1, v16

    #@130
    invoke-static {v0, v2, v1, v4}, Landroid/app/WallpaperManager;->generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_133
    .catchall {:try_start_11f .. :try_end_133} :catchall_151
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11f .. :try_end_133} :catch_13c

    #@133
    move-result-object v17

    #@134
    .line 556
    :try_start_134
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_137
    .catch Ljava/io/IOException; {:try_start_134 .. :try_end_137} :catch_139
    .catch Landroid/os/RemoteException; {:try_start_134 .. :try_end_137} :catch_156

    #@137
    goto/16 :goto_9f

    #@139
    .line 557
    :catch_139
    move-exception v18

    #@13a
    goto/16 :goto_9f

    #@13c
    .line 552
    .end local v2           #bm:Landroid/graphics/Bitmap;
    .end local v9           #options:Landroid/graphics/BitmapFactory$Options;
    :catch_13c
    move-exception v3

    #@13d
    .line 553
    .local v3, e:Ljava/lang/OutOfMemoryError;
    :try_start_13d
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@140
    move-result-object v17

    #@141
    const-string v18, "Can\'t decode stream"

    #@143
    move-object/from16 v0, v17

    #@145
    move-object/from16 v1, v18

    #@147
    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_14a
    .catchall {:try_start_13d .. :try_end_14a} :catchall_151

    #@14a
    .line 556
    :try_start_14a
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_14d
    .catch Ljava/io/IOException; {:try_start_14a .. :try_end_14d} :catch_15a
    .catch Landroid/os/RemoteException; {:try_start_14a .. :try_end_14d} :catch_156

    #@14d
    .line 565
    .end local v3           #e:Ljava/lang/OutOfMemoryError;
    .end local v4           #height:I
    .end local v5           #i:I
    .end local v7           #networkCode:Ljava/lang/String;
    .end local v8           #noOfCarrier:I
    .end local v10           #phoneActivated:Ljava/lang/String;
    .end local v13           #strNoOfCarrier:Ljava/lang/String;
    .end local v16           #width:I
    :cond_14d
    :goto_14d
    const/16 v17, 0x0

    #@14f
    goto/16 :goto_9f

    #@151
    .line 555
    .restart local v4       #height:I
    .restart local v5       #i:I
    .restart local v7       #networkCode:Ljava/lang/String;
    .restart local v8       #noOfCarrier:I
    .restart local v10       #phoneActivated:Ljava/lang/String;
    .restart local v13       #strNoOfCarrier:Ljava/lang/String;
    .restart local v16       #width:I
    :catchall_151
    move-exception v17

    #@152
    .line 556
    :try_start_152
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_155
    .catch Ljava/io/IOException; {:try_start_152 .. :try_end_155} :catch_158
    .catch Landroid/os/RemoteException; {:try_start_152 .. :try_end_155} :catch_156

    #@155
    .line 555
    :goto_155
    :try_start_155
    throw v17
    :try_end_156
    .catch Landroid/os/RemoteException; {:try_start_155 .. :try_end_156} :catch_156

    #@156
    .line 562
    .end local v4           #height:I
    .end local v5           #i:I
    .end local v7           #networkCode:Ljava/lang/String;
    .end local v8           #noOfCarrier:I
    .end local v10           #phoneActivated:Ljava/lang/String;
    .end local v13           #strNoOfCarrier:Ljava/lang/String;
    .end local v16           #width:I
    :catch_156
    move-exception v17

    #@157
    goto :goto_14d

    #@158
    .line 557
    .restart local v4       #height:I
    .restart local v5       #i:I
    .restart local v7       #networkCode:Ljava/lang/String;
    .restart local v8       #noOfCarrier:I
    .restart local v10       #phoneActivated:Ljava/lang/String;
    .restart local v13       #strNoOfCarrier:Ljava/lang/String;
    .restart local v16       #width:I
    :catch_158
    move-exception v18

    #@159
    goto :goto_155

    #@15a
    .restart local v3       #e:Ljava/lang/OutOfMemoryError;
    :catch_15a
    move-exception v17

    #@15b
    goto :goto_14d
.end method

.method private static quiteinputStream(Ljava/io/InputStream;)V
    .registers 2
    .parameter "stream"

    #@0
    .prologue
    .line 404
    if-eqz p0, :cond_5

    #@2
    .line 405
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    #@5
    .line 410
    :cond_5
    :goto_5
    return-void

    #@6
    .line 407
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method


# virtual methods
.method public forgetLoadedWallpaper()V
    .registers 2

    #@0
    .prologue
    .line 314
    monitor-enter p0

    #@1
    .line 315
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@4
    .line 316
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@7
    .line 317
    monitor-exit p0

    #@8
    .line 318
    return-void

    #@9
    .line 317
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_9

    #@b
    throw v0
.end method

.method public onWallpaperChanged()V
    .registers 3

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Landroid/app/WallpaperManager$Globals;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 261
    return-void
.end method

.method public peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "context"
    .parameter "returnDefault"

    #@0
    .prologue
    .line 264
    monitor-enter p0

    #@1
    .line 265
    :try_start_1
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@3
    if-eqz v2, :cond_9

    #@5
    .line 266
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@7
    monitor-exit p0

    #@8
    .line 309
    :goto_8
    return-object v2

    #@9
    .line 268
    :cond_9
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@b
    if-eqz v2, :cond_14

    #@d
    .line 269
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@f
    monitor-exit p0

    #@10
    goto :goto_8

    #@11
    .line 310
    :catchall_11
    move-exception v2

    #@12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_11

    #@13
    throw v2

    #@14
    .line 271
    :cond_14
    const/4 v2, 0x0

    #@15
    :try_start_15
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;
    :try_end_17
    .catchall {:try_start_15 .. :try_end_17} :catchall_11

    #@17
    .line 273
    :try_start_17
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getCurrentWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@1a
    move-result-object v2

    #@1b
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;
    :try_end_1d
    .catchall {:try_start_17 .. :try_end_1d} :catchall_11
    .catch Ljava/lang/OutOfMemoryError; {:try_start_17 .. :try_end_1d} :catch_5c

    #@1d
    .line 277
    :goto_1d
    if-eqz p2, :cond_7b

    #@1f
    .line 278
    :try_start_1f
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@21
    if-nez v2, :cond_78

    #@23
    .line 283
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPR_CHAMELEON:Z

    #@25
    if-eqz v2, :cond_6e

    #@27
    .line 284
    const-string/jumbo v2, "ro.build.target_operator"

    #@2a
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    .line 285
    .local v1, operator:Ljava/lang/String;
    const-string v2, "SPR"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@33
    move-result v2

    #@34
    if-nez v2, :cond_3e

    #@36
    const-string v2, "BM"

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3b
    move-result v2

    #@3c
    if-eqz v2, :cond_67

    #@3e
    .line 286
    :cond_3e
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getDefaultWallpaperLockedFromCarrierPartition(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@41
    move-result-object v2

    #@42
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@44
    .line 288
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@46
    if-nez v2, :cond_58

    #@48
    .line 289
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getDefaultWallpaperLockedFromNetworkCode(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@4b
    move-result-object v2

    #@4c
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@4e
    .line 290
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@50
    if-nez v2, :cond_58

    #@52
    .line 291
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getDefaultWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@55
    move-result-object v2

    #@56
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@58
    .line 297
    :cond_58
    :goto_58
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@5a
    monitor-exit p0

    #@5b
    goto :goto_8

    #@5c
    .line 274
    .end local v1           #operator:Ljava/lang/String;
    :catch_5c
    move-exception v0

    #@5d
    .line 275
    .local v0, e:Ljava/lang/OutOfMemoryError;
    invoke-static {}, Landroid/app/WallpaperManager;->access$200()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    const-string v3, "No memory load current wallpaper"

    #@63
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@66
    goto :goto_1d

    #@67
    .line 295
    .end local v0           #e:Ljava/lang/OutOfMemoryError;
    .restart local v1       #operator:Ljava/lang/String;
    :cond_67
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getDefaultWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@6a
    move-result-object v2

    #@6b
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@6d
    goto :goto_58

    #@6e
    .line 303
    .end local v1           #operator:Ljava/lang/String;
    :cond_6e
    invoke-direct {p0, p1}, Landroid/app/WallpaperManager$Globals;->getDefaultWallpaperLocked(Landroid/content/Context;)Landroid/graphics/Bitmap;

    #@71
    move-result-object v2

    #@72
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@74
    .line 304
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@76
    monitor-exit p0

    #@77
    goto :goto_8

    #@78
    .line 306
    :cond_78
    const/4 v2, 0x0

    #@79
    iput-object v2, p0, Landroid/app/WallpaperManager$Globals;->mDefaultWallpaper:Landroid/graphics/Bitmap;

    #@7b
    .line 309
    :cond_7b
    iget-object v2, p0, Landroid/app/WallpaperManager$Globals;->mWallpaper:Landroid/graphics/Bitmap;

    #@7d
    monitor-exit p0
    :try_end_7e
    .catchall {:try_start_1f .. :try_end_7e} :catchall_11

    #@7e
    goto :goto_8
.end method
