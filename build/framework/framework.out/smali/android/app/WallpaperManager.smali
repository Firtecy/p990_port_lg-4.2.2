.class public Landroid/app/WallpaperManager;
.super Ljava/lang/Object;
.source "WallpaperManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/WallpaperManager$1;,
        Landroid/app/WallpaperManager$Globals;,
        Landroid/app/WallpaperManager$FastBitmapDrawable;
    }
.end annotation


# static fields
.field public static final ACTION_CHANGE_LIVE_WALLPAPER:Ljava/lang/String; = "android.service.wallpaper.CHANGE_LIVE_WALLPAPER"

.field public static final ACTION_LIVE_WALLPAPER_CHOOSER:Ljava/lang/String; = "android.service.wallpaper.LIVE_WALLPAPER_CHOOSER"

.field public static final COMMAND_DROP:Ljava/lang/String; = "android.home.drop"

.field public static final COMMAND_SECONDARY_TAP:Ljava/lang/String; = "android.wallpaper.secondaryTap"

.field public static final COMMAND_TAP:Ljava/lang/String; = "android.wallpaper.tap"

.field private static DEBUG:Z = false

.field public static final EXTRA_LIVE_WALLPAPER_COMPONENT:Ljava/lang/String; = "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT"

.field private static final IMAGE_WALLPAPER:Landroid/content/ComponentName; = null

.field private static TAG:Ljava/lang/String; = null

.field public static final WALLPAPER_PREVIEW_META_DATA:Ljava/lang/String; = "android.wallpaper.preview"

.field private static sGlobals:Landroid/app/WallpaperManager$Globals;

.field private static final sSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mWallpaperXStep:F

.field private mWallpaperYStep:F


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 79
    const-string v0, "WallpaperManager"

    #@3
    sput-object v0, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@5
    .line 80
    sput-boolean v3, Landroid/app/WallpaperManager;->DEBUG:Z

    #@7
    .line 141
    new-instance v0, Landroid/content/ComponentName;

    #@9
    const-string v1, "com.android.systemui"

    #@b
    const-string v2, "com.android.systemui.ImageWallpaper"

    #@d
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    sput-object v0, Landroid/app/WallpaperManager;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@12
    .line 601
    new-array v0, v3, [Ljava/lang/Object;

    #@14
    sput-object v0, Landroid/app/WallpaperManager;->sSync:Ljava/lang/Object;

    #@16
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    const/high16 v0, -0x4080

    #@2
    .line 612
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 81
    iput v0, p0, Landroid/app/WallpaperManager;->mWallpaperXStep:F

    #@7
    .line 82
    iput v0, p0, Landroid/app/WallpaperManager;->mWallpaperYStep:F

    #@9
    .line 613
    iput-object p1, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@b
    .line 614
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@e
    move-result-object v0

    #@f
    invoke-static {v0}, Landroid/app/WallpaperManager;->initGlobals(Landroid/os/Looper;)V

    #@12
    .line 615
    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 78
    sget-object v0, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static generateBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .registers 16
    .parameter "context"
    .parameter "bm"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1097
    if-nez p1, :cond_5

    #@3
    move-object p1, v5

    #@4
    .line 1150
    .end local p1
    :cond_4
    :goto_4
    return-object p1

    #@5
    .line 1101
    .restart local p1
    :cond_5
    const-string/jumbo v10, "window"

    #@8
    invoke-virtual {p0, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v9

    #@c
    check-cast v9, Landroid/view/WindowManager;

    #@e
    .line 1102
    .local v9, wm:Landroid/view/WindowManager;
    new-instance v4, Landroid/util/DisplayMetrics;

    #@10
    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    #@13
    .line 1103
    .local v4, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@16
    move-result-object v10

    #@17
    invoke-virtual {v10, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    #@1a
    .line 1104
    iget v10, v4, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@1c
    invoke-virtual {p1, v10}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@1f
    .line 1106
    if-lez p2, :cond_4

    #@21
    if-lez p3, :cond_4

    #@23
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@26
    move-result v10

    #@27
    if-ne v10, p2, :cond_2f

    #@29
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@2c
    move-result v10

    #@2d
    if-eq v10, p3, :cond_4

    #@2f
    .line 1113
    :cond_2f
    :try_start_2f
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@31
    invoke-static {p2, p3, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@34
    move-result-object v5

    #@35
    .line 1114
    .local v5, newbm:Landroid/graphics/Bitmap;
    iget v10, v4, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@37
    invoke-virtual {v5, v10}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@3a
    .line 1116
    new-instance v0, Landroid/graphics/Canvas;

    #@3c
    invoke-direct {v0, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    #@3f
    .line 1117
    .local v0, c:Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Rect;

    #@41
    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    #@44
    .line 1118
    .local v8, targetRect:Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@47
    move-result v10

    #@48
    iput v10, v8, Landroid/graphics/Rect;->right:I

    #@4a
    .line 1119
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@4d
    move-result v10

    #@4e
    iput v10, v8, Landroid/graphics/Rect;->bottom:I

    #@50
    .line 1121
    iget v10, v8, Landroid/graphics/Rect;->right:I

    #@52
    sub-int v2, p2, v10

    #@54
    .line 1122
    .local v2, deltaw:I
    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    #@56
    sub-int v1, p3, v10

    #@58
    .line 1124
    .local v1, deltah:I
    if-gtz v2, :cond_5c

    #@5a
    if-lez v1, :cond_7a

    #@5c
    .line 1127
    :cond_5c
    if-le v2, v1, :cond_a2

    #@5e
    .line 1128
    int-to-float v10, p2

    #@5f
    iget v11, v8, Landroid/graphics/Rect;->right:I

    #@61
    int-to-float v11, v11

    #@62
    div-float v7, v10, v11

    #@64
    .line 1132
    .local v7, scale:F
    :goto_64
    iget v10, v8, Landroid/graphics/Rect;->right:I

    #@66
    int-to-float v10, v10

    #@67
    mul-float/2addr v10, v7

    #@68
    float-to-int v10, v10

    #@69
    iput v10, v8, Landroid/graphics/Rect;->right:I

    #@6b
    .line 1133
    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    #@6d
    int-to-float v10, v10

    #@6e
    mul-float/2addr v10, v7

    #@6f
    float-to-int v10, v10

    #@70
    iput v10, v8, Landroid/graphics/Rect;->bottom:I

    #@72
    .line 1134
    iget v10, v8, Landroid/graphics/Rect;->right:I

    #@74
    sub-int v2, p2, v10

    #@76
    .line 1135
    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    #@78
    sub-int v1, p3, v10

    #@7a
    .line 1138
    .end local v7           #scale:F
    :cond_7a
    div-int/lit8 v10, v2, 0x2

    #@7c
    div-int/lit8 v11, v1, 0x2

    #@7e
    invoke-virtual {v8, v10, v11}, Landroid/graphics/Rect;->offset(II)V

    #@81
    .line 1140
    new-instance v6, Landroid/graphics/Paint;

    #@83
    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    #@86
    .line 1141
    .local v6, paint:Landroid/graphics/Paint;
    const/4 v10, 0x1

    #@87
    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@8a
    .line 1142
    new-instance v10, Landroid/graphics/PorterDuffXfermode;

    #@8c
    sget-object v11, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    #@8e
    invoke-direct {v10, v11}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    #@91
    invoke-virtual {v6, v10}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    #@94
    .line 1143
    const/4 v10, 0x0

    #@95
    invoke-virtual {v0, p1, v10, v8, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@98
    .line 1145
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    #@9b
    .line 1146
    const/4 v10, 0x0

    #@9c
    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@9f
    move-object p1, v5

    #@a0
    .line 1147
    goto/16 :goto_4

    #@a2
    .line 1130
    .end local v6           #paint:Landroid/graphics/Paint;
    :cond_a2
    int-to-float v10, p3

    #@a3
    iget v11, v8, Landroid/graphics/Rect;->bottom:I
    :try_end_a5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2f .. :try_end_a5} :catch_a9

    #@a5
    int-to-float v11, v11

    #@a6
    div-float v7, v10, v11

    #@a8
    .restart local v7       #scale:F
    goto :goto_64

    #@a9
    .line 1148
    .end local v0           #c:Landroid/graphics/Canvas;
    .end local v1           #deltah:I
    .end local v2           #deltaw:I
    .end local v5           #newbm:Landroid/graphics/Bitmap;
    .end local v7           #scale:F
    .end local v8           #targetRect:Landroid/graphics/Rect;
    :catch_a9
    move-exception v3

    #@aa
    .line 1149
    .local v3, e:Ljava/lang/OutOfMemoryError;
    sget-object v10, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@ac
    const-string v11, "Can\'t generate default bitmap"

    #@ae
    invoke-static {v10, v11, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b1
    goto/16 :goto_4
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 621
    const-string/jumbo v0, "wallpaper"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/app/WallpaperManager;

    #@9
    return-object v0
.end method

.method static initGlobals(Landroid/os/Looper;)V
    .registers 3
    .parameter "looper"

    #@0
    .prologue
    .line 605
    sget-object v1, Landroid/app/WallpaperManager;->sSync:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 606
    :try_start_3
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 607
    new-instance v0, Landroid/app/WallpaperManager$Globals;

    #@9
    invoke-direct {v0, p0}, Landroid/app/WallpaperManager$Globals;-><init>(Landroid/os/Looper;)V

    #@c
    sput-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@e
    .line 609
    :cond_e
    monitor-exit v1

    #@f
    .line 610
    return-void

    #@10
    .line 609
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method private setWallpaper(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V
    .registers 6
    .parameter "data"
    .parameter "fos"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 889
    const v2, 0x8000

    #@3
    new-array v1, v2, [B

    #@5
    .line 891
    .local v1, buffer:[B
    :goto_5
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    #@8
    move-result v0

    #@9
    .local v0, amt:I
    if-lez v0, :cond_10

    #@b
    .line 892
    const/4 v2, 0x0

    #@c
    invoke-virtual {p2, v1, v2, v0}, Ljava/io/FileOutputStream;->write([BII)V

    #@f
    goto :goto_5

    #@10
    .line 894
    :cond_10
    return-void
.end method

.method private setWallpaperType(Z)V
    .registers 3
    .parameter "fixed"

    #@0
    .prologue
    .line 1157
    :try_start_0
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0, p1}, Landroid/app/IWallpaperManager;->setWallpaperType(Z)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 1161
    :goto_9
    return-void

    #@a
    .line 1158
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method


# virtual methods
.method public clear()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 1093
    const v0, 0x1080222

    #@3
    invoke-virtual {p0, v0}, Landroid/app/WallpaperManager;->setResource(I)V

    #@6
    .line 1094
    return-void
.end method

.method public clearWallpaperOffsets(Landroid/os/IBinder;)V
    .registers 8
    .parameter "windowToken"

    #@0
    .prologue
    .line 1074
    :try_start_0
    iget-object v0, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/view/WindowManagerGlobal;->getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;

    #@9
    move-result-object v0

    #@a
    const/high16 v2, -0x4080

    #@c
    const/high16 v3, -0x4080

    #@e
    const/high16 v4, -0x4080

    #@10
    const/high16 v5, -0x4080

    #@12
    move-object v1, p1

    #@13
    invoke-interface/range {v0 .. v5}, Landroid/view/IWindowSession;->setWallpaperPosition(Landroid/os/IBinder;FFFF)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_16} :catch_17

    #@16
    .line 1079
    :goto_16
    return-void

    #@17
    .line 1076
    :catch_17
    move-exception v0

    #@18
    goto :goto_16
.end method

.method public forgetLoadedWallpaper()V
    .registers 2

    #@0
    .prologue
    .line 720
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-virtual {v0}, Landroid/app/WallpaperManager$Globals;->forgetLoadedWallpaper()V

    #@5
    .line 721
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 4

    #@0
    .prologue
    .line 710
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    iget-object v1, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@4
    const/4 v2, 0x1

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/app/WallpaperManager$Globals;->peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getDesiredMinimumHeight()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 957
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@6
    move-result-object v2

    #@7
    if-nez v2, :cond_11

    #@9
    .line 958
    sget-object v2, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "WallpaperService not running"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 965
    :goto_10
    return v1

    #@11
    .line 962
    :cond_11
    :try_start_11
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@13
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@16
    move-result-object v2

    #@17
    invoke-interface {v2}, Landroid/app/IWallpaperManager;->getHeightHint()I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    goto :goto_10

    #@1c
    .line 963
    :catch_1c
    move-exception v0

    #@1d
    .line 965
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_10
.end method

.method public getDesiredMinimumWidth()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 930
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@6
    move-result-object v2

    #@7
    if-nez v2, :cond_11

    #@9
    .line 931
    sget-object v2, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "WallpaperService not running"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 938
    :goto_10
    return v1

    #@11
    .line 935
    :cond_11
    :try_start_11
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@13
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@16
    move-result-object v2

    #@17
    invoke-interface {v2}, Landroid/app/IWallpaperManager;->getWidthHint()I
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1a} :catch_1c

    #@1a
    move-result v1

    #@1b
    goto :goto_10

    #@1c
    .line 936
    :catch_1c
    move-exception v0

    #@1d
    .line 938
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_10
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    .line 640
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    iget-object v3, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@4
    const/4 v4, 0x1

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/app/WallpaperManager$Globals;->peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    #@8
    move-result-object v0

    #@9
    .line 641
    .local v0, bm:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1b

    #@b
    .line 642
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    #@d
    iget-object v2, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v2

    #@13
    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@16
    .line 643
    .local v1, dr:Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    #@17
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setDither(Z)V

    #@1a
    .line 646
    .end local v1           #dr:Landroid/graphics/drawable/Drawable;
    :goto_1a
    return-object v1

    #@1b
    :cond_1b
    const/4 v1, 0x0

    #@1c
    goto :goto_1a
.end method

.method public getFastDrawable()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 682
    sget-object v1, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    iget-object v3, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@5
    const/4 v4, 0x1

    #@6
    invoke-virtual {v1, v3, v4}, Landroid/app/WallpaperManager$Globals;->peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    #@9
    move-result-object v0

    #@a
    .line 683
    .local v0, bm:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_12

    #@c
    .line 684
    new-instance v1, Landroid/app/WallpaperManager$FastBitmapDrawable;

    #@e
    invoke-direct {v1, v0, v2}, Landroid/app/WallpaperManager$FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;Landroid/app/WallpaperManager$1;)V

    #@11
    .line 686
    :goto_11
    return-object v1

    #@12
    :cond_12
    move-object v1, v2

    #@13
    goto :goto_11
.end method

.method public getIWallpaperManager()Landroid/app/IWallpaperManager;
    .registers 2

    #@0
    .prologue
    .line 627
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getWallpaperInfo()Landroid/app/WallpaperInfo;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 730
    :try_start_1
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@6
    move-result-object v2

    #@7
    if-nez v2, :cond_11

    #@9
    .line 731
    sget-object v2, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@b
    const-string v3, "WallpaperService not running"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 737
    :goto_10
    return-object v1

    #@11
    .line 734
    :cond_11
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@13
    invoke-static {v2}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@16
    move-result-object v2

    #@17
    invoke-interface {v2}, Landroid/app/IWallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1a} :catch_1c

    #@1a
    move-result-object v1

    #@1b
    goto :goto_10

    #@1c
    .line 736
    :catch_1c
    move-exception v0

    #@1d
    .line 737
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_10
.end method

.method public getWallpaperType()Z
    .registers 3

    #@0
    .prologue
    .line 1166
    :try_start_0
    sget-object v1, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v1}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v1}, Landroid/app/IWallpaperManager;->getWallpaperType()Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_b

    #@9
    move-result v1

    #@a
    .line 1169
    :goto_a
    return v1

    #@b
    .line 1167
    :catch_b
    move-exception v0

    #@c
    .line 1169
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@d
    goto :goto_a
.end method

.method public hasResourceWallpaper(I)Z
    .registers 8
    .parameter "resid"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 902
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@6
    move-result-object v4

    #@7
    if-nez v4, :cond_11

    #@9
    .line 903
    sget-object v4, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@b
    const-string v5, "WallpaperService not running"

    #@d
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 911
    :goto_10
    return v3

    #@11
    .line 907
    :cond_11
    :try_start_11
    iget-object v4, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16
    move-result-object v2

    #@17
    .line 908
    .local v2, resources:Landroid/content/res/Resources;
    new-instance v4, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string/jumbo v5, "res:"

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    .line 909
    .local v1, name:Ljava/lang/String;
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@31
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@34
    move-result-object v4

    #@35
    invoke-interface {v4, v1}, Landroid/app/IWallpaperManager;->hasNamedWallpaper(Ljava/lang/String;)Z
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_38} :catch_3a

    #@38
    move-result v3

    #@39
    goto :goto_10

    #@3a
    .line 910
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #resources:Landroid/content/res/Resources;
    :catch_3a
    move-exception v0

    #@3b
    .line 911
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_10
.end method

.method public peekDrawable()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 659
    sget-object v2, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    iget-object v3, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v2, v3, v4}, Landroid/app/WallpaperManager$Globals;->peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    #@8
    move-result-object v0

    #@9
    .line 660
    .local v0, bm:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1a

    #@b
    .line 661
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    #@d
    iget-object v2, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v2

    #@13
    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@16
    .line 662
    .local v1, dr:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/BitmapDrawable;->setDither(Z)V

    #@19
    .line 665
    .end local v1           #dr:Landroid/graphics/drawable/Drawable;
    :goto_19
    return-object v1

    #@1a
    :cond_1a
    const/4 v1, 0x0

    #@1b
    goto :goto_19
.end method

.method public peekFastDrawable()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 697
    sget-object v1, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@3
    iget-object v3, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@5
    const/4 v4, 0x0

    #@6
    invoke-virtual {v1, v3, v4}, Landroid/app/WallpaperManager$Globals;->peekWallpaperBitmap(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    #@9
    move-result-object v0

    #@a
    .line 698
    .local v0, bm:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_12

    #@c
    .line 699
    new-instance v1, Landroid/app/WallpaperManager$FastBitmapDrawable;

    #@e
    invoke-direct {v1, v0, v2}, Landroid/app/WallpaperManager$FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;Landroid/app/WallpaperManager$1;)V

    #@11
    .line 701
    :goto_11
    return-object v1

    #@12
    :cond_12
    move-object v1, v2

    #@13
    goto :goto_11
.end method

.method public sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V
    .registers 15
    .parameter "windowToken"
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"

    #@0
    .prologue
    .line 1054
    :try_start_0
    iget-object v0, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/view/WindowManagerGlobal;->getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;

    #@9
    move-result-object v0

    #@a
    const/4 v7, 0x0

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move v3, p3

    #@e
    move v4, p4

    #@f
    move v5, p5

    #@10
    move-object v6, p6

    #@11
    invoke-interface/range {v0 .. v7}, Landroid/view/IWindowSession;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_14} :catch_15

    #@14
    .line 1060
    :goto_14
    return-void

    #@15
    .line 1057
    :catch_15
    move-exception v0

    #@16
    goto :goto_14
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 8
    .parameter "bitmap"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 799
    sget-object v3, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v3}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v3

    #@6
    if-nez v3, :cond_10

    #@8
    .line 800
    sget-object v3, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@a
    const-string v4, "WallpaperService not running"

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 828
    :cond_f
    :goto_f
    return-void

    #@10
    .line 804
    :cond_10
    :try_start_10
    sget-object v3, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@12
    invoke-static {v3}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@15
    move-result-object v3

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v3, v4}, Landroid/app/IWallpaperManager;->setWallpaper(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_1a} :catch_55

    #@1a
    move-result-object v0

    #@1b
    .line 805
    .local v0, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v0, :cond_f

    #@1d
    .line 808
    const/4 v1, 0x0

    #@1e
    .line 810
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_1e
    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@20
    invoke-direct {v2, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_6a

    #@23
    .line 811
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_23
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    #@25
    const/16 v4, 0x5a

    #@27
    invoke-virtual {p1, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@2a
    .line 813
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@2d
    move-result v3

    #@2e
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@31
    move-result v4

    #@32
    if-lt v3, v4, :cond_57

    #@34
    .line 814
    sget-object v3, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@36
    invoke-static {v3}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@39
    move-result-object v3

    #@3a
    const/4 v4, 0x0

    #@3b
    invoke-interface {v3, v4}, Landroid/app/IWallpaperManager;->setWallpaperType(Z)V

    #@3e
    .line 818
    :goto_3e
    sget-object v3, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@40
    invoke-static {v3}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@43
    move-result-object v3

    #@44
    iget-object v4, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@46
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    sget-object v5, Landroid/app/WallpaperManager;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@4c
    invoke-interface {v3, v4, v5}, Landroid/app/IWallpaperManager;->decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V
    :try_end_4f
    .catchall {:try_start_23 .. :try_end_4f} :catchall_62

    #@4f
    .line 821
    if-eqz v2, :cond_f

    #@51
    .line 822
    :try_start_51
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_54
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_54} :catch_55

    #@54
    goto :goto_f

    #@55
    .line 825
    .end local v0           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v2           #fos:Ljava/io/FileOutputStream;
    :catch_55
    move-exception v3

    #@56
    goto :goto_f

    #@57
    .line 816
    .restart local v0       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :cond_57
    :try_start_57
    sget-object v3, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@59
    invoke-static {v3}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5c
    move-result-object v3

    #@5d
    const/4 v4, 0x1

    #@5e
    invoke-interface {v3, v4}, Landroid/app/IWallpaperManager;->setWallpaperType(Z)V
    :try_end_61
    .catchall {:try_start_57 .. :try_end_61} :catchall_62

    #@61
    goto :goto_3e

    #@62
    .line 821
    :catchall_62
    move-exception v3

    #@63
    move-object v1, v2

    #@64
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    :goto_64
    if-eqz v1, :cond_69

    #@66
    .line 822
    :try_start_66
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V

    #@69
    .line 821
    :cond_69
    throw v3
    :try_end_6a
    .catch Landroid/os/RemoteException; {:try_start_66 .. :try_end_6a} :catch_55

    #@6a
    :catchall_6a
    move-exception v3

    #@6b
    goto :goto_64
.end method

.method public setResource(I)V
    .registers 9
    .parameter "resid"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 756
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v4

    #@6
    if-nez v4, :cond_10

    #@8
    .line 757
    sget-object v4, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@a
    const-string v5, "WallpaperService not running"

    #@c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 783
    :cond_f
    :goto_f
    return-void

    #@10
    .line 761
    :cond_10
    :try_start_10
    iget-object v4, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@12
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15
    move-result-object v3

    #@16
    .line 763
    .local v3, resources:Landroid/content/res/Resources;
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@18
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@1b
    move-result-object v4

    #@1c
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string/jumbo v6, "res:"

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    invoke-interface {v4, v5}, Landroid/app/IWallpaperManager;->setWallpaper(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_37} :catch_68

    #@37
    move-result-object v0

    #@38
    .line 765
    .local v0, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v0, :cond_f

    #@3a
    .line 766
    const/4 v1, 0x0

    #@3b
    .line 768
    .local v1, fos:Ljava/io/FileOutputStream;
    :try_start_3b
    new-instance v2, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@3d
    invoke-direct {v2, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_40
    .catchall {:try_start_3b .. :try_end_40} :catchall_6a

    #@40
    .line 770
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_40
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@42
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@45
    move-result-object v4

    #@46
    const/4 v5, 0x0

    #@47
    invoke-interface {v4, v5}, Landroid/app/IWallpaperManager;->setWallpaperType(Z)V

    #@4a
    .line 771
    sget-object v4, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@4c
    invoke-static {v4}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@4f
    move-result-object v4

    #@50
    iget-object v5, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@52
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    sget-object v6, Landroid/app/WallpaperManager;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@58
    invoke-interface {v4, v5, v6}, Landroid/app/IWallpaperManager;->decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V

    #@5b
    .line 773
    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    #@5e
    move-result-object v4

    #@5f
    invoke-direct {p0, v4, v2}, Landroid/app/WallpaperManager;->setWallpaper(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V
    :try_end_62
    .catchall {:try_start_40 .. :try_end_62} :catchall_71

    #@62
    .line 775
    if-eqz v2, :cond_f

    #@64
    .line 776
    :try_start_64
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V

    #@67
    goto :goto_f

    #@68
    .line 780
    .end local v0           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .end local v3           #resources:Landroid/content/res/Resources;
    :catch_68
    move-exception v4

    #@69
    goto :goto_f

    #@6a
    .line 775
    .restart local v0       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    .restart local v3       #resources:Landroid/content/res/Resources;
    :catchall_6a
    move-exception v4

    #@6b
    :goto_6b
    if-eqz v1, :cond_70

    #@6d
    .line 776
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V

    #@70
    .line 775
    :cond_70
    throw v4
    :try_end_71
    .catch Landroid/os/RemoteException; {:try_start_64 .. :try_end_71} :catch_68

    #@71
    .end local v1           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catchall_71
    move-exception v4

    #@72
    move-object v1, v2

    #@73
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v1       #fos:Ljava/io/FileOutputStream;
    goto :goto_6b
.end method

.method public setStream(Ljava/io/InputStream;)V
    .registers 11
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 846
    sget-object v6, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v6}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v6

    #@6
    if-nez v6, :cond_10

    #@8
    .line 847
    sget-object v6, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@a
    const-string v7, "WallpaperService not running"

    #@c
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 885
    .end local p1
    :cond_f
    :goto_f
    return-void

    #@10
    .line 851
    .restart local p1
    :cond_10
    :try_start_10
    sget-object v6, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@12
    invoke-static {v6}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@15
    move-result-object v6

    #@16
    const/4 v7, 0x0

    #@17
    invoke-interface {v6, v7}, Landroid/app/IWallpaperManager;->setWallpaper(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_1a} :catch_45

    #@1a
    move-result-object v1

    #@1b
    .line 852
    .local v1, fd:Landroid/os/ParcelFileDescriptor;
    if-eqz v1, :cond_f

    #@1d
    .line 855
    const/4 v2, 0x0

    #@1e
    .line 857
    .local v2, fos:Ljava/io/FileOutputStream;
    :try_start_1e
    new-instance v3, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    #@20
    invoke-direct {v3, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_77

    #@23
    .line 859
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .local v3, fos:Ljava/io/FileOutputStream;
    :try_start_23
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_DRM:Z

    #@25
    if-eqz v6, :cond_5d

    #@27
    instance-of v6, p1, Lcom/lge/lgdrm/DrmStream;
    :try_end_29
    .catchall {:try_start_23 .. :try_end_29} :catchall_55

    #@29
    if-eqz v6, :cond_5d

    #@2b
    .line 860
    const/4 v4, 0x0

    #@2c
    .line 862
    .local v4, is:Ljava/io/InputStream;
    :try_start_2c
    new-instance v5, Ljava/io/FileInputStream;

    #@2e
    check-cast p1, Lcom/lge/lgdrm/DrmStream;

    #@30
    .end local p1
    invoke-virtual {p1}, Lcom/lge/lgdrm/DrmStream;->getPath()Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_37
    .catchall {:try_start_2c .. :try_end_37} :catchall_4e
    .catch Ljava/io/FileNotFoundException; {:try_start_2c .. :try_end_37} :catch_47

    #@37
    .line 863
    .end local v4           #is:Ljava/io/InputStream;
    .local v5, is:Ljava/io/InputStream;
    :try_start_37
    invoke-direct {p0, v5, v3}, Landroid/app/WallpaperManager;->setWallpaper(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_79
    .catch Ljava/io/FileNotFoundException; {:try_start_37 .. :try_end_3a} :catch_7c

    #@3a
    .line 867
    if-eqz v5, :cond_3f

    #@3c
    .line 868
    :try_start_3c
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_55

    #@3f
    .line 878
    :cond_3f
    if-eqz v3, :cond_f

    #@41
    .line 879
    :try_start_41
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_44} :catch_45

    #@44
    goto :goto_f

    #@45
    .line 882
    .end local v1           #fd:Landroid/os/ParcelFileDescriptor;
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .end local v5           #is:Ljava/io/InputStream;
    :catch_45
    move-exception v6

    #@46
    goto :goto_f

    #@47
    .line 864
    .restart local v1       #fd:Landroid/os/ParcelFileDescriptor;
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    .restart local v4       #is:Ljava/io/InputStream;
    :catch_47
    move-exception v0

    #@48
    .line 865
    .local v0, e:Ljava/io/FileNotFoundException;
    :goto_48
    :try_start_48
    new-instance v6, Ljava/io/IOException;

    #@4a
    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    #@4d
    throw v6
    :try_end_4e
    .catchall {:try_start_48 .. :try_end_4e} :catchall_4e

    #@4e
    .line 867
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catchall_4e
    move-exception v6

    #@4f
    :goto_4f
    if-eqz v4, :cond_54

    #@51
    .line 868
    :try_start_51
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@54
    .line 867
    :cond_54
    throw v6
    :try_end_55
    .catchall {:try_start_51 .. :try_end_55} :catchall_55

    #@55
    .line 878
    .end local v4           #is:Ljava/io/InputStream;
    :catchall_55
    move-exception v6

    #@56
    move-object v2, v3

    #@57
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :goto_57
    if-eqz v2, :cond_5c

    #@59
    .line 879
    :try_start_59
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V

    #@5c
    .line 878
    :cond_5c
    throw v6
    :try_end_5d
    .catch Landroid/os/RemoteException; {:try_start_59 .. :try_end_5d} :catch_45

    #@5d
    .line 873
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    .restart local p1
    :cond_5d
    :try_start_5d
    invoke-direct {p0, p1, v3}, Landroid/app/WallpaperManager;->setWallpaper(Ljava/io/InputStream;Ljava/io/FileOutputStream;)V

    #@60
    .line 875
    sget-object v6, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@62
    invoke-static {v6}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@65
    move-result-object v6

    #@66
    iget-object v7, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@68
    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    sget-object v8, Landroid/app/WallpaperManager;->IMAGE_WALLPAPER:Landroid/content/ComponentName;

    #@6e
    invoke-interface {v6, v7, v8}, Landroid/app/IWallpaperManager;->decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V
    :try_end_71
    .catchall {:try_start_5d .. :try_end_71} :catchall_55

    #@71
    .line 878
    if-eqz v3, :cond_f

    #@73
    .line 879
    :try_start_73
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;->close()V
    :try_end_76
    .catch Landroid/os/RemoteException; {:try_start_73 .. :try_end_76} :catch_45

    #@76
    goto :goto_f

    #@77
    .line 878
    .end local v3           #fos:Ljava/io/FileOutputStream;
    .restart local v2       #fos:Ljava/io/FileOutputStream;
    :catchall_77
    move-exception v6

    #@78
    goto :goto_57

    #@79
    .line 867
    .end local v2           #fos:Ljava/io/FileOutputStream;
    .end local p1
    .restart local v3       #fos:Ljava/io/FileOutputStream;
    .restart local v5       #is:Ljava/io/InputStream;
    :catchall_79
    move-exception v6

    #@7a
    move-object v4, v5

    #@7b
    .end local v5           #is:Ljava/io/InputStream;
    .restart local v4       #is:Ljava/io/InputStream;
    goto :goto_4f

    #@7c
    .line 864
    .end local v4           #is:Ljava/io/InputStream;
    .restart local v5       #is:Ljava/io/InputStream;
    :catch_7c
    move-exception v0

    #@7d
    move-object v4, v5

    #@7e
    .end local v5           #is:Ljava/io/InputStream;
    .restart local v4       #is:Ljava/io/InputStream;
    goto :goto_48
.end method

.method public setWallpaperOffsetSteps(FF)V
    .registers 3
    .parameter "xStep"
    .parameter "yStep"

    #@0
    .prologue
    .line 1033
    iput p1, p0, Landroid/app/WallpaperManager;->mWallpaperXStep:F

    #@2
    .line 1034
    iput p2, p0, Landroid/app/WallpaperManager;->mWallpaperYStep:F

    #@4
    .line 1035
    return-void
.end method

.method public setWallpaperOffsets(Landroid/os/IBinder;FF)V
    .registers 10
    .parameter "windowToken"
    .parameter "xOffset"
    .parameter "yOffset"

    #@0
    .prologue
    .line 1016
    :try_start_0
    iget-object v0, p0, Landroid/app/WallpaperManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/view/WindowManagerGlobal;->getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;

    #@9
    move-result-object v0

    #@a
    iget v4, p0, Landroid/app/WallpaperManager;->mWallpaperXStep:F

    #@c
    iget v5, p0, Landroid/app/WallpaperManager;->mWallpaperYStep:F

    #@e
    move-object v1, p1

    #@f
    move v2, p2

    #@10
    move v3, p3

    #@11
    invoke-interface/range {v0 .. v5}, Landroid/view/IWindowSession;->setWallpaperPosition(Landroid/os/IBinder;FFFF)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_14} :catch_15

    #@14
    .line 1022
    :goto_14
    return-void

    #@15
    .line 1019
    :catch_15
    move-exception v0

    #@16
    goto :goto_14
.end method

.method public suggestDesiredDimensions(II)V
    .registers 5
    .parameter "minimumWidth"
    .parameter "minimumHeight"

    #@0
    .prologue
    .line 990
    :try_start_0
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@2
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_10

    #@8
    .line 991
    sget-object v0, Landroid/app/WallpaperManager;->TAG:Ljava/lang/String;

    #@a
    const-string v1, "WallpaperService not running"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 998
    :goto_f
    return-void

    #@10
    .line 993
    :cond_10
    sget-object v0, Landroid/app/WallpaperManager;->sGlobals:Landroid/app/WallpaperManager$Globals;

    #@12
    invoke-static {v0}, Landroid/app/WallpaperManager$Globals;->access$300(Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;

    #@15
    move-result-object v0

    #@16
    invoke-interface {v0, p1, p2}, Landroid/app/IWallpaperManager;->setDimensionHints(II)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    goto :goto_f

    #@1a
    .line 995
    :catch_1a
    move-exception v0

    #@1b
    goto :goto_f
.end method
