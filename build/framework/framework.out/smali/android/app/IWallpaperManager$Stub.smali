.class public abstract Landroid/app/IWallpaperManager$Stub;
.super Landroid/os/Binder;
.source "IWallpaperManager.java"

# interfaces
.implements Landroid/app/IWallpaperManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/IWallpaperManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/IWallpaperManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.IWallpaperManager"

.field static final TRANSACTION_clearWallpaper:I = 0x5

.field static final TRANSACTION_decideStatusBarBgAlpha:I = 0xc

.field static final TRANSACTION_getHeightHint:I = 0x9

.field static final TRANSACTION_getWallpaper:I = 0x3

.field static final TRANSACTION_getWallpaperInfo:I = 0x4

.field static final TRANSACTION_getWallpaperType:I = 0xb

.field static final TRANSACTION_getWidthHint:I = 0x8

.field static final TRANSACTION_hasNamedWallpaper:I = 0x6

.field static final TRANSACTION_setDimensionHints:I = 0x7

.field static final TRANSACTION_setWallpaper:I = 0x1

.field static final TRANSACTION_setWallpaperComponent:I = 0x2

.field static final TRANSACTION_setWallpaperType:I = 0xa


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.app.IWallpaperManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/app/IWallpaperManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/IWallpaperManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.app.IWallpaperManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/app/IWallpaperManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/app/IWallpaperManager;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/app/IWallpaperManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/IWallpaperManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_13e

    #@5
    .line 193
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 43
    :sswitch_a
    const-string v3, "android.app.IWallpaperManager"

    #@c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v5, "android.app.IWallpaperManager"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 51
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/IWallpaperManager$Stub;->setWallpaper(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    #@1c
    move-result-object v2

    #@1d
    .line 52
    .local v2, _result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 53
    if-eqz v2, :cond_29

    #@22
    .line 54
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 55
    invoke-virtual {v2, p3, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@28
    goto :goto_9

    #@29
    .line 58
    :cond_29
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_9

    #@2d
    .line 64
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_2d
    const-string v3, "android.app.IWallpaperManager"

    #@2f
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_47

    #@38
    .line 67
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3a
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@3d
    move-result-object v0

    #@3e
    check-cast v0, Landroid/content/ComponentName;

    #@40
    .line 72
    .local v0, _arg0:Landroid/content/ComponentName;
    :goto_40
    invoke-virtual {p0, v0}, Landroid/app/IWallpaperManager$Stub;->setWallpaperComponent(Landroid/content/ComponentName;)V

    #@43
    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@46
    goto :goto_9

    #@47
    .line 70
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :cond_47
    const/4 v0, 0x0

    #@48
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_40

    #@49
    .line 78
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_49
    const-string v5, "android.app.IWallpaperManager"

    #@4b
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@51
    move-result-object v5

    #@52
    invoke-static {v5}, Landroid/app/IWallpaperManagerCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IWallpaperManagerCallback;

    #@55
    move-result-object v0

    #@56
    .line 82
    .local v0, _arg0:Landroid/app/IWallpaperManagerCallback;
    new-instance v1, Landroid/os/Bundle;

    #@58
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    #@5b
    .line 83
    .local v1, _arg1:Landroid/os/Bundle;
    invoke-virtual {p0, v0, v1}, Landroid/app/IWallpaperManager$Stub;->getWallpaper(Landroid/app/IWallpaperManagerCallback;Landroid/os/Bundle;)Landroid/os/ParcelFileDescriptor;

    #@5e
    move-result-object v2

    #@5f
    .line 84
    .restart local v2       #_result:Landroid/os/ParcelFileDescriptor;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@62
    .line 85
    if-eqz v2, :cond_73

    #@64
    .line 86
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@67
    .line 87
    invoke-virtual {v2, p3, v4}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@6a
    .line 92
    :goto_6a
    if-eqz v1, :cond_77

    #@6c
    .line 93
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6f
    .line 94
    invoke-virtual {v1, p3, v4}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@72
    goto :goto_9

    #@73
    .line 90
    :cond_73
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@76
    goto :goto_6a

    #@77
    .line 97
    :cond_77
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    goto :goto_9

    #@7b
    .line 103
    .end local v0           #_arg0:Landroid/app/IWallpaperManagerCallback;
    .end local v1           #_arg1:Landroid/os/Bundle;
    .end local v2           #_result:Landroid/os/ParcelFileDescriptor;
    :sswitch_7b
    const-string v5, "android.app.IWallpaperManager"

    #@7d
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 104
    invoke-virtual {p0}, Landroid/app/IWallpaperManager$Stub;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    #@83
    move-result-object v2

    #@84
    .line 105
    .local v2, _result:Landroid/app/WallpaperInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@87
    .line 106
    if-eqz v2, :cond_91

    #@89
    .line 107
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@8c
    .line 108
    invoke-virtual {v2, p3, v4}, Landroid/app/WallpaperInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@8f
    goto/16 :goto_9

    #@91
    .line 111
    :cond_91
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@94
    goto/16 :goto_9

    #@96
    .line 117
    .end local v2           #_result:Landroid/app/WallpaperInfo;
    :sswitch_96
    const-string v3, "android.app.IWallpaperManager"

    #@98
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b
    .line 118
    invoke-virtual {p0}, Landroid/app/IWallpaperManager$Stub;->clearWallpaper()V

    #@9e
    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1
    goto/16 :goto_9

    #@a3
    .line 124
    :sswitch_a3
    const-string v5, "android.app.IWallpaperManager"

    #@a5
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a8
    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ab
    move-result-object v0

    #@ac
    .line 127
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/IWallpaperManager$Stub;->hasNamedWallpaper(Ljava/lang/String;)Z

    #@af
    move-result v2

    #@b0
    .line 128
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b3
    .line 129
    if-eqz v2, :cond_b6

    #@b5
    move v3, v4

    #@b6
    :cond_b6
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@b9
    goto/16 :goto_9

    #@bb
    .line 134
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_result:Z
    :sswitch_bb
    const-string v3, "android.app.IWallpaperManager"

    #@bd
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c0
    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c3
    move-result v0

    #@c4
    .line 138
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v1

    #@c8
    .line 139
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/app/IWallpaperManager$Stub;->setDimensionHints(II)V

    #@cb
    .line 140
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ce
    goto/16 :goto_9

    #@d0
    .line 145
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_d0
    const-string v3, "android.app.IWallpaperManager"

    #@d2
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d5
    .line 146
    invoke-virtual {p0}, Landroid/app/IWallpaperManager$Stub;->getWidthHint()I

    #@d8
    move-result v2

    #@d9
    .line 147
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@dc
    .line 148
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@df
    goto/16 :goto_9

    #@e1
    .line 153
    .end local v2           #_result:I
    :sswitch_e1
    const-string v3, "android.app.IWallpaperManager"

    #@e3
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e6
    .line 154
    invoke-virtual {p0}, Landroid/app/IWallpaperManager$Stub;->getHeightHint()I

    #@e9
    move-result v2

    #@ea
    .line 155
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ed
    .line 156
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@f0
    goto/16 :goto_9

    #@f2
    .line 161
    .end local v2           #_result:I
    :sswitch_f2
    const-string v5, "android.app.IWallpaperManager"

    #@f4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f7
    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@fa
    move-result v5

    #@fb
    if-eqz v5, :cond_106

    #@fd
    move v0, v4

    #@fe
    .line 164
    .local v0, _arg0:Z
    :goto_fe
    invoke-virtual {p0, v0}, Landroid/app/IWallpaperManager$Stub;->setWallpaperType(Z)V

    #@101
    .line 165
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@104
    goto/16 :goto_9

    #@106
    .end local v0           #_arg0:Z
    :cond_106
    move v0, v3

    #@107
    .line 163
    goto :goto_fe

    #@108
    .line 170
    :sswitch_108
    const-string v5, "android.app.IWallpaperManager"

    #@10a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 171
    invoke-virtual {p0}, Landroid/app/IWallpaperManager$Stub;->getWallpaperType()Z

    #@110
    move-result v2

    #@111
    .line 172
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@114
    .line 173
    if-eqz v2, :cond_117

    #@116
    move v3, v4

    #@117
    :cond_117
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11a
    goto/16 :goto_9

    #@11c
    .line 178
    .end local v2           #_result:Z
    :sswitch_11c
    const-string v3, "android.app.IWallpaperManager"

    #@11e
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@121
    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@124
    move-result-object v0

    #@125
    .line 182
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@128
    move-result v3

    #@129
    if-eqz v3, :cond_13b

    #@12b
    .line 183
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12d
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@130
    move-result-object v1

    #@131
    check-cast v1, Landroid/content/ComponentName;

    #@133
    .line 188
    .local v1, _arg1:Landroid/content/ComponentName;
    :goto_133
    invoke-virtual {p0, v0, v1}, Landroid/app/IWallpaperManager$Stub;->decideStatusBarBgAlpha(Ljava/lang/String;Landroid/content/ComponentName;)V

    #@136
    .line 189
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@139
    goto/16 :goto_9

    #@13b
    .line 186
    .end local v1           #_arg1:Landroid/content/ComponentName;
    :cond_13b
    const/4 v1, 0x0

    #@13c
    .restart local v1       #_arg1:Landroid/content/ComponentName;
    goto :goto_133

    #@13d
    .line 39
    nop

    #@13e
    :sswitch_data_13e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_2d
        0x3 -> :sswitch_49
        0x4 -> :sswitch_7b
        0x5 -> :sswitch_96
        0x6 -> :sswitch_a3
        0x7 -> :sswitch_bb
        0x8 -> :sswitch_d0
        0x9 -> :sswitch_e1
        0xa -> :sswitch_f2
        0xb -> :sswitch_108
        0xc -> :sswitch_11c
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
