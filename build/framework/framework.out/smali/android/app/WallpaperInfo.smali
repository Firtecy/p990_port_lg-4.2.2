.class public final Landroid/app/WallpaperInfo;
.super Ljava/lang/Object;
.source "WallpaperInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/WallpaperInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "WallpaperInfo"


# instance fields
.field final mAuthorResource:I

.field final mDescriptionResource:I

.field final mService:Landroid/content/pm/ResolveInfo;

.field final mSettingsActivityName:Ljava/lang/String;

.field final mThumbnailResource:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 296
    new-instance v0, Landroid/app/WallpaperInfo$1;

    #@2
    invoke-direct {v0}, Landroid/app/WallpaperInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/app/WallpaperInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .registers 21
    .parameter "context"
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 84
    move-object/from16 v0, p2

    #@5
    move-object/from16 v1, p0

    #@7
    iput-object v0, v1, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@9
    .line 85
    move-object/from16 v0, p2

    #@b
    iget-object v12, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@d
    .line 87
    .local v12, si:Landroid/content/pm/ServiceInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@10
    move-result-object v8

    #@11
    .line 88
    .local v8, pm:Landroid/content/pm/PackageManager;
    const/4 v11, 0x0

    #@12
    .line 89
    .local v11, settingsActivityComponent:Ljava/lang/String;
    const/4 v13, -0x1

    #@13
    .line 90
    .local v13, thumbnailRes:I
    const/4 v3, -0x1

    #@14
    .line 91
    .local v3, authorRes:I
    const/4 v4, -0x1

    #@15
    .line 93
    .local v4, descriptionRes:I
    const/4 v7, 0x0

    #@16
    .line 95
    .local v7, parser:Landroid/content/res/XmlResourceParser;
    :try_start_16
    const-string v15, "android.service.wallpaper"

    #@18
    invoke-virtual {v12, v8, v15}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@1b
    move-result-object v7

    #@1c
    .line 96
    if-nez v7, :cond_4b

    #@1e
    .line 97
    new-instance v15, Lorg/xmlpull/v1/XmlPullParserException;

    #@20
    const-string v16, "No android.service.wallpaper meta-data"

    #@22
    invoke-direct/range {v15 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@25
    throw v15
    :try_end_26
    .catchall {:try_start_16 .. :try_end_26} :catchall_44
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_26} :catch_26

    #@26
    .line 132
    :catch_26
    move-exception v5

    #@27
    .line 133
    .local v5, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_27
    new-instance v15, Lorg/xmlpull/v1/XmlPullParserException;

    #@29
    new-instance v16, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v17, "Unable to create context for: "

    #@30
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v16

    #@34
    iget-object v0, v12, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@36
    move-object/from16 v17, v0

    #@38
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v16

    #@3c
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v16

    #@40
    invoke-direct/range {v15 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@43
    throw v15
    :try_end_44
    .catchall {:try_start_27 .. :try_end_44} :catchall_44

    #@44
    .line 136
    .end local v5           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_44
    move-exception v15

    #@45
    if-eqz v7, :cond_4a

    #@47
    invoke-interface {v7}, Landroid/content/res/XmlResourceParser;->close()V

    #@4a
    :cond_4a
    throw v15

    #@4b
    .line 101
    :cond_4b
    :try_start_4b
    iget-object v15, v12, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4d
    invoke-virtual {v8, v15}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@50
    move-result-object v9

    #@51
    .line 103
    .local v9, res:Landroid/content/res/Resources;
    invoke-static {v7}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@54
    move-result-object v2

    #@55
    .line 107
    .local v2, attrs:Landroid/util/AttributeSet;
    :cond_55
    invoke-interface {v7}, Landroid/content/res/XmlResourceParser;->next()I

    #@58
    move-result v14

    #@59
    .local v14, type:I
    const/4 v15, 0x1

    #@5a
    if-eq v14, v15, :cond_5f

    #@5c
    const/4 v15, 0x2

    #@5d
    if-ne v14, v15, :cond_55

    #@5f
    .line 110
    :cond_5f
    invoke-interface {v7}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@62
    move-result-object v6

    #@63
    .line 111
    .local v6, nodeName:Ljava/lang/String;
    const-string/jumbo v15, "wallpaper"

    #@66
    invoke-virtual {v15, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v15

    #@6a
    if-nez v15, :cond_74

    #@6c
    .line 112
    new-instance v15, Lorg/xmlpull/v1/XmlPullParserException;

    #@6e
    const-string v16, "Meta-data does not start with wallpaper tag"

    #@70
    invoke-direct/range {v15 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@73
    throw v15

    #@74
    .line 116
    :cond_74
    sget-object v15, Lcom/android/internal/R$styleable;->Wallpaper:[I

    #@76
    invoke-virtual {v9, v2, v15}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@79
    move-result-object v10

    #@7a
    .line 118
    .local v10, sa:Landroid/content/res/TypedArray;
    const/4 v15, 0x1

    #@7b
    invoke-virtual {v10, v15}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@7e
    move-result-object v11

    #@7f
    .line 121
    const/4 v15, 0x2

    #@80
    const/16 v16, -0x1

    #@82
    move/from16 v0, v16

    #@84
    invoke-virtual {v10, v15, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@87
    move-result v13

    #@88
    .line 124
    const/4 v15, 0x3

    #@89
    const/16 v16, -0x1

    #@8b
    move/from16 v0, v16

    #@8d
    invoke-virtual {v10, v15, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@90
    move-result v3

    #@91
    .line 127
    const/4 v15, 0x0

    #@92
    const/16 v16, -0x1

    #@94
    move/from16 v0, v16

    #@96
    invoke-virtual {v10, v15, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@99
    move-result v4

    #@9a
    .line 131
    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_9d
    .catchall {:try_start_4b .. :try_end_9d} :catchall_44
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4b .. :try_end_9d} :catch_26

    #@9d
    .line 136
    if-eqz v7, :cond_a2

    #@9f
    invoke-interface {v7}, Landroid/content/res/XmlResourceParser;->close()V

    #@a2
    .line 139
    :cond_a2
    move-object/from16 v0, p0

    #@a4
    iput-object v11, v0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@a6
    .line 140
    move-object/from16 v0, p0

    #@a8
    iput v13, v0, Landroid/app/WallpaperInfo;->mThumbnailResource:I

    #@aa
    .line 141
    move-object/from16 v0, p0

    #@ac
    iput v3, v0, Landroid/app/WallpaperInfo;->mAuthorResource:I

    #@ae
    .line 142
    move-object/from16 v0, p0

    #@b0
    iput v4, v0, Landroid/app/WallpaperInfo;->mDescriptionResource:I

    #@b2
    .line 143
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 145
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@9
    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/app/WallpaperInfo;->mThumbnailResource:I

    #@f
    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/app/WallpaperInfo;->mAuthorResource:I

    #@15
    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    iput v0, p0, Landroid/app/WallpaperInfo;->mDescriptionResource:I

    #@1b
    .line 150
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@20
    move-result-object v0

    #@21
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@23
    iput-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@25
    .line 151
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 307
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    const-string v1, "Service:"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@16
    .line 268
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "  "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ResolveInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@2e
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string/jumbo v1, "mSettingsActivityName="

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v0

    #@3e
    iget-object v1, p0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@4b
    .line 270
    return-void
.end method

.method public getComponent()Landroid/content/ComponentName;
    .registers 4

    #@0
    .prologue
    .line 180
    new-instance v0, Landroid/content/ComponentName;

    #@2
    iget-object v1, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@4
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@6
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@8
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@a
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@c
    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@e
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getServiceInfo()Landroid/content/pm/ServiceInfo;
    .registers 2

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    return-object v0
.end method

.method public getServiceName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@4
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@6
    return-object v0
.end method

.method public getSettingsActivity()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 263
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public loadAuthor(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 5
    .parameter "pm"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 222
    iget v2, p0, Landroid/app/WallpaperInfo;->mAuthorResource:I

    #@2
    if-gtz v2, :cond_a

    #@4
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@6
    invoke-direct {v2}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@9
    throw v2

    #@a
    .line 223
    :cond_a
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@c
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@e
    .line 224
    .local v1, packageName:Ljava/lang/String;
    const/4 v0, 0x0

    #@f
    .line 225
    .local v0, applicationInfo:Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_1d

    #@11
    .line 226
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@13
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@15
    iget-object v1, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@17
    .line 227
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@19
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@1b
    iget-object v0, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1d
    .line 229
    :cond_1d
    iget v2, p0, Landroid/app/WallpaperInfo;->mAuthorResource:I

    #@1f
    invoke-virtual {p1, v1, v2, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@22
    move-result-object v2

    #@23
    return-object v2
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "pm"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 236
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    #@4
    .line 237
    .local v1, packageName:Ljava/lang/String;
    const/4 v0, 0x0

    #@5
    .line 238
    .local v0, applicationInfo:Landroid/content/pm/ApplicationInfo;
    if-nez v1, :cond_13

    #@7
    .line 239
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@9
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@b
    iget-object v1, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@d
    .line 240
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@f
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@11
    iget-object v0, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@13
    .line 242
    :cond_13
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@15
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@17
    iget v2, v2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@19
    if-eqz v2, :cond_26

    #@1b
    .line 243
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@1d
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@1f
    iget v2, v2, Landroid/content/pm/ComponentInfo;->descriptionRes:I

    #@21
    invoke-virtual {p1, v1, v2, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@24
    move-result-object v2

    #@25
    .line 248
    :goto_25
    return-object v2

    #@26
    .line 247
    :cond_26
    iget v2, p0, Landroid/app/WallpaperInfo;->mDescriptionResource:I

    #@28
    if-gtz v2, :cond_30

    #@2a
    new-instance v2, Landroid/content/res/Resources$NotFoundException;

    #@2c
    invoke-direct {v2}, Landroid/content/res/Resources$NotFoundException;-><init>()V

    #@2f
    throw v2

    #@30
    .line 248
    :cond_30
    iget v2, p0, Landroid/app/WallpaperInfo;->mDescriptionResource:I

    #@32
    iget-object v3, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@34
    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@36
    iget-object v3, v3, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@38
    invoke-virtual {p1, v1, v2, v3}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@3b
    move-result-object v2

    #@3c
    goto :goto_25
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "pm"

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public loadThumbnail(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "pm"

    #@0
    .prologue
    .line 211
    iget v0, p0, Landroid/app/WallpaperInfo;->mThumbnailResource:I

    #@2
    if-gez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 213
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@8
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@a
    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@c
    iget v1, p0, Landroid/app/WallpaperInfo;->mThumbnailResource:I

    #@e
    iget-object v2, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@10
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@12
    iget-object v2, v2, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@14
    invoke-virtual {p1, v0, v1, v2}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "WallpaperInfo{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@d
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@f
    iget-object v1, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, ", settings: "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    iget-object v1, p0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string/jumbo v1, "}"

    #@24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mSettingsActivityName:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 287
    iget v0, p0, Landroid/app/WallpaperInfo;->mThumbnailResource:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 288
    iget v0, p0, Landroid/app/WallpaperInfo;->mAuthorResource:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 289
    iget v0, p0, Landroid/app/WallpaperInfo;->mDescriptionResource:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 290
    iget-object v0, p0, Landroid/app/WallpaperInfo;->mService:Landroid/content/pm/ResolveInfo;

    #@16
    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@19
    .line 291
    return-void
.end method
