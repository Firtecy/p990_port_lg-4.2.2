.class public Landroid/app/Dialog;
.super Ljava/lang/Object;
.source "Dialog.java"

# interfaces
.implements Landroid/content/DialogInterface;
.implements Landroid/view/Window$Callback;
.implements Landroid/view/KeyEvent$Callback;
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Dialog$ListenersHandler;
    }
.end annotation


# static fields
.field private static final CANCEL:I = 0x44

.field private static final DIALOG_HIERARCHY_TAG:Ljava/lang/String; = "android:dialogHierarchy"

.field private static final DIALOG_SHOWING_TAG:Ljava/lang/String; = "android:dialogShowing"

.field private static final DISMISS:I = 0x43

.field private static final SHOW:I = 0x45

.field private static final TAG:Ljava/lang/String; = "Dialog"


# instance fields
.field private mActionBar:Lcom/android/internal/app/ActionBarImpl;

.field private mActionMode:Landroid/view/ActionMode;

.field private mCancelAndDismissTaken:Ljava/lang/String;

.field private mCancelMessage:Landroid/os/Message;

.field protected mCancelable:Z

.field private mCanceled:Z

.field final mContext:Landroid/content/Context;

.field private mCreated:Z

.field mDecor:Landroid/view/View;

.field private final mDismissAction:Ljava/lang/Runnable;

.field private mDismissMessage:Landroid/os/Message;

.field private final mHandler:Landroid/os/Handler;

.field private mListenersHandler:Landroid/os/Handler;

.field private mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOwnerActivity:Landroid/app/Activity;

.field private mShowMessage:Landroid/os/Message;

.field private mShowing:Z

.field mWindow:Landroid/view/Window;

.field final mWindowManager:Landroid/view/WindowManager;

.field private viewAgent:Lcom/lge/app/atsagent/IViewAgent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 140
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;IZ)V

    #@5
    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "theme"

    #@0
    .prologue
    .line 156
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;IZ)V

    #@4
    .line 157
    return-void
.end method

.method constructor <init>(Landroid/content/Context;IZ)V
    .registers 10
    .parameter "context"
    .parameter "theme"
    .parameter "createContextThemeWrapper"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 101
    iput-boolean v5, p0, Landroid/app/Dialog;->mCancelable:Z

    #@8
    .line 110
    iput-boolean v2, p0, Landroid/app/Dialog;->mCreated:Z

    #@a
    .line 111
    iput-boolean v2, p0, Landroid/app/Dialog;->mShowing:Z

    #@c
    .line 112
    iput-boolean v2, p0, Landroid/app/Dialog;->mCanceled:Z

    #@e
    .line 114
    new-instance v2, Landroid/os/Handler;

    #@10
    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    #@13
    iput-object v2, p0, Landroid/app/Dialog;->mHandler:Landroid/os/Handler;

    #@15
    .line 124
    iput-object v4, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@17
    .line 126
    new-instance v2, Landroid/app/Dialog$1;

    #@19
    invoke-direct {v2, p0}, Landroid/app/Dialog$1;-><init>(Landroid/app/Dialog;)V

    #@1c
    iput-object v2, p0, Landroid/app/Dialog;->mDismissAction:Ljava/lang/Runnable;

    #@1e
    .line 160
    if-eqz p3, :cond_62

    #@20
    .line 161
    if-nez p2, :cond_33

    #@22
    .line 162
    new-instance v0, Landroid/util/TypedValue;

    #@24
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    #@27
    .line 163
    .local v0, outValue:Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    #@2a
    move-result-object v2

    #@2b
    const v3, 0x1010308

    #@2e
    invoke-virtual {v2, v3, v0, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    #@31
    .line 165
    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    #@33
    .line 167
    .end local v0           #outValue:Landroid/util/TypedValue;
    :cond_33
    new-instance v2, Landroid/view/ContextThemeWrapper;

    #@35
    invoke-direct {v2, p1, p2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    #@38
    iput-object v2, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@3a
    .line 172
    :goto_3a
    const-string/jumbo v2, "window"

    #@3d
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@40
    move-result-object v2

    #@41
    check-cast v2, Landroid/view/WindowManager;

    #@43
    iput-object v2, p0, Landroid/app/Dialog;->mWindowManager:Landroid/view/WindowManager;

    #@45
    .line 173
    iget-object v2, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@47
    invoke-static {v2}, Lcom/android/internal/policy/PolicyManager;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@4a
    move-result-object v1

    #@4b
    .line 174
    .local v1, w:Landroid/view/Window;
    iput-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@4d
    .line 175
    invoke-virtual {v1, p0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    #@50
    .line 176
    iget-object v2, p0, Landroid/app/Dialog;->mWindowManager:Landroid/view/WindowManager;

    #@52
    invoke-virtual {v1, v2, v4, v4}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;)V

    #@55
    .line 177
    const/16 v2, 0x11

    #@57
    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    #@5a
    .line 178
    new-instance v2, Landroid/app/Dialog$ListenersHandler;

    #@5c
    invoke-direct {v2, p0}, Landroid/app/Dialog$ListenersHandler;-><init>(Landroid/app/Dialog;)V

    #@5f
    iput-object v2, p0, Landroid/app/Dialog;->mListenersHandler:Landroid/os/Handler;

    #@61
    .line 179
    return-void

    #@62
    .line 169
    .end local v1           #w:Landroid/view/Window;
    :cond_62
    iput-object p1, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@64
    goto :goto_3a
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .registers 4
    .parameter "context"
    .parameter "cancelable"
    .parameter "cancelListener"

    #@0
    .prologue
    .line 195
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    #@3
    .line 196
    iput-boolean p2, p0, Landroid/app/Dialog;->mCancelable:Z

    #@5
    .line 197
    invoke-virtual {p0, p3}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@8
    .line 198
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/os/Message;)V
    .registers 4
    .parameter "context"
    .parameter "cancelable"
    .parameter "cancelCallback"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 188
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    #@3
    .line 189
    iput-boolean p2, p0, Landroid/app/Dialog;->mCancelable:Z

    #@5
    .line 190
    iput-object p3, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@7
    .line 191
    return-void
.end method

.method private getAssociatedActivity()Landroid/content/ComponentName;
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1050
    iget-object v0, p0, Landroid/app/Dialog;->mOwnerActivity:Landroid/app/Activity;

    #@3
    .line 1051
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    #@6
    move-result-object v1

    #@7
    .line 1052
    .local v1, context:Landroid/content/Context;
    :goto_7
    if-nez v0, :cond_20

    #@9
    if-eqz v1, :cond_20

    #@b
    .line 1053
    instance-of v3, v1, Landroid/app/Activity;

    #@d
    if-eqz v3, :cond_13

    #@f
    move-object v0, v1

    #@10
    .line 1054
    check-cast v0, Landroid/app/Activity;

    #@12
    goto :goto_7

    #@13
    .line 1056
    :cond_13
    instance-of v3, v1, Landroid/content/ContextWrapper;

    #@15
    if-eqz v3, :cond_1e

    #@17
    check-cast v1, Landroid/content/ContextWrapper;

    #@19
    .end local v1           #context:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    #@1c
    move-result-object v1

    #@1d
    .restart local v1       #context:Landroid/content/Context;
    :goto_1d
    goto :goto_7

    #@1e
    :cond_1e
    move-object v1, v2

    #@1f
    goto :goto_1d

    #@20
    .line 1061
    :cond_20
    if-nez v0, :cond_23

    #@22
    :goto_22
    return-object v2

    #@23
    :cond_23
    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    #@26
    move-result-object v2

    #@27
    goto :goto_22
.end method

.method private sendDismissMessage()V
    .registers 2

    #@0
    .prologue
    .line 385
    iget-object v0, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 387
    iget-object v0, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@6
    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 389
    :cond_d
    return-void
.end method

.method private sendShowMessage()V
    .registers 2

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/app/Dialog;->mShowMessage:Landroid/os/Message;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 394
    iget-object v0, p0, Landroid/app/Dialog;->mShowMessage:Landroid/os/Message;

    #@6
    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 396
    :cond_d
    return-void
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 559
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    .line 560
    return-void
.end method

.method public cancel()V
    .registers 2

    #@0
    .prologue
    .line 1157
    iget-boolean v0, p0, Landroid/app/Dialog;->mCanceled:Z

    #@2
    if-nez v0, :cond_14

    #@4
    iget-object v0, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 1158
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/app/Dialog;->mCanceled:Z

    #@b
    .line 1160
    iget-object v0, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@d
    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@14
    .line 1162
    :cond_14
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    #@17
    .line 1163
    return-void
.end method

.method public closeOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 949
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/view/Window;->closePanel(I)V

    #@6
    .line 950
    return-void
.end method

.method public dismiss()V
    .registers 8

    #@0
    .prologue
    .line 333
    iget-object v2, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@5
    move-result-object v1

    #@6
    .line 334
    .local v1, l:Landroid/view/WindowManager$LayoutParams;
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@8
    if-lez v2, :cond_d

    #@a
    .line 335
    invoke-virtual {p0}, Landroid/app/Dialog;->refreshExternalDisplay()V

    #@d
    .line 341
    :cond_d
    :try_start_d
    iget-object v2, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@f
    if-eqz v2, :cond_16

    #@11
    .line 342
    iget-object v2, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@13
    invoke-interface {v2}, Lcom/lge/app/atsagent/IViewAgent;->onHide()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_16} :catch_26

    #@16
    .line 350
    :cond_16
    :goto_16
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@19
    move-result-object v2

    #@1a
    iget-object v3, p0, Landroid/app/Dialog;->mHandler:Landroid/os/Handler;

    #@1c
    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@1f
    move-result-object v3

    #@20
    if-ne v2, v3, :cond_40

    #@22
    .line 351
    invoke-virtual {p0}, Landroid/app/Dialog;->dismissDialog()V

    #@25
    .line 355
    :goto_25
    return-void

    #@26
    .line 343
    :catch_26
    move-exception v0

    #@27
    .line 344
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "ViewAgent"

    #@29
    const-string v3, "Dialog dismiss exception: %s"

    #@2b
    const/4 v4, 0x1

    #@2c
    new-array v4, v4, [Ljava/lang/Object;

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    aput-object v6, v4, v5

    #@35
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 346
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@3f
    goto :goto_16

    #@40
    .line 353
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_40
    iget-object v2, p0, Landroid/app/Dialog;->mHandler:Landroid/os/Handler;

    #@42
    iget-object v3, p0, Landroid/app/Dialog;->mDismissAction:Ljava/lang/Runnable;

    #@44
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@47
    goto :goto_25
.end method

.method dismissDialog()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 358
    iget-object v1, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@4
    if-eqz v1, :cond_a

    #@6
    iget-boolean v1, p0, Landroid/app/Dialog;->mShowing:Z

    #@8
    if-nez v1, :cond_b

    #@a
    .line 382
    :cond_a
    :goto_a
    return-void

    #@b
    .line 362
    :cond_b
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@d
    invoke-virtual {v1}, Landroid/view/Window;->isDestroyed()Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_1b

    #@13
    .line 363
    const-string v1, "Dialog"

    #@15
    const-string v2, "Tried to dismissDialog() but the Dialog\'s window was already destroyed!"

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    goto :goto_a

    #@1b
    .line 368
    :cond_1b
    :try_start_1b
    iget-object v1, p0, Landroid/app/Dialog;->mWindowManager:Landroid/view/WindowManager;

    #@1d
    iget-object v2, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@1f
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_55
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_22} :catch_3b

    #@22
    .line 372
    iget-object v1, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@24
    if-eqz v1, :cond_2b

    #@26
    .line 373
    iget-object v1, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@28
    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    #@2b
    .line 375
    :cond_2b
    iput-object v4, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@2d
    .line 376
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2f
    invoke-virtual {v1}, Landroid/view/Window;->closeAllPanels()V

    #@32
    .line 377
    invoke-virtual {p0}, Landroid/app/Dialog;->onStop()V

    #@35
    .line 378
    iput-boolean v3, p0, Landroid/app/Dialog;->mShowing:Z

    #@37
    .line 380
    :goto_37
    invoke-direct {p0}, Landroid/app/Dialog;->sendDismissMessage()V

    #@3a
    goto :goto_a

    #@3b
    .line 369
    :catch_3b
    move-exception v0

    #@3c
    .line 370
    .local v0, ex:Ljava/lang/Exception;
    :try_start_3c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_55

    #@3f
    .line 372
    iget-object v1, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@41
    if-eqz v1, :cond_48

    #@43
    .line 373
    iget-object v1, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@45
    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    #@48
    .line 375
    :cond_48
    iput-object v4, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@4a
    .line 376
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@4c
    invoke-virtual {v1}, Landroid/view/Window;->closeAllPanels()V

    #@4f
    .line 377
    invoke-virtual {p0}, Landroid/app/Dialog;->onStop()V

    #@52
    .line 378
    iput-boolean v3, p0, Landroid/app/Dialog;->mShowing:Z

    #@54
    goto :goto_37

    #@55
    .line 372
    .end local v0           #ex:Ljava/lang/Exception;
    :catchall_55
    move-exception v1

    #@56
    iget-object v2, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@58
    if-eqz v2, :cond_5f

    #@5a
    .line 373
    iget-object v2, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@5c
    invoke-virtual {v2}, Landroid/view/ActionMode;->finish()V

    #@5f
    .line 375
    :cond_5f
    iput-object v4, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@61
    .line 376
    iget-object v2, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@63
    invoke-virtual {v2}, Landroid/view/Window;->closeAllPanels()V

    #@66
    .line 377
    invoke-virtual {p0}, Landroid/app/Dialog;->onStop()V

    #@69
    .line 378
    iput-boolean v3, p0, Landroid/app/Dialog;->mShowing:Z

    #@6b
    .line 380
    invoke-direct {p0}, Landroid/app/Dialog;->sendDismissMessage()V

    #@6e
    .line 372
    throw v1
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 828
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 829
    const/4 v0, 0x1

    #@9
    .line 831
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 757
    iget-object v1, p0, Landroid/app/Dialog;->mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    #@3
    if-eqz v1, :cond_12

    #@5
    iget-object v1, p0, Landroid/app/Dialog;->mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    #@7
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@a
    move-result v2

    #@b
    invoke-interface {v1, p0, v2, p1}, Landroid/content/DialogInterface$OnKeyListener;->onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_12

    #@11
    .line 763
    :cond_11
    :goto_11
    return v0

    #@12
    .line 760
    :cond_12
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@14
    invoke-virtual {v1, p1}, Landroid/view/Window;->superDispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_11

    #@1a
    .line 763
    iget-object v0, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@1c
    if-eqz v0, :cond_29

    #@1e
    iget-object v0, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@20
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    #@23
    move-result-object v0

    #@24
    :goto_24
    invoke-virtual {p1, p0, v0, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    #@27
    move-result v0

    #@28
    goto :goto_11

    #@29
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_24
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 777
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 778
    const/4 v0, 0x1

    #@9
    .line 780
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@d
    move-result v0

    #@e
    invoke-virtual {p0, v0, p1}, Landroid/app/Dialog;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    #@11
    move-result v0

    #@12
    goto :goto_9
.end method

.method dispatchOnCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 401
    iget-boolean v0, p0, Landroid/app/Dialog;->mCreated:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 402
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    #@7
    .line 403
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/app/Dialog;->mCreated:Z

    #@a
    .line 405
    :cond_a
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 7
    .parameter "event"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 835
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v3

    #@6
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    #@d
    .line 836
    iget-object v3, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    #@16
    .line 838
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@1d
    move-result-object v1

    #@1e
    .line 839
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@20
    if-ne v3, v4, :cond_2b

    #@22
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@24
    if-ne v3, v4, :cond_2b

    #@26
    const/4 v0, 0x1

    #@27
    .line 841
    .local v0, isFullScreen:Z
    :goto_27
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFullScreen(Z)V

    #@2a
    .line 843
    return v2

    #@2b
    .end local v0           #isFullScreen:Z
    :cond_2b
    move v0, v2

    #@2c
    .line 839
    goto :goto_27
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 794
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 795
    const/4 v0, 0x1

    #@9
    .line 797
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "ev"

    #@0
    .prologue
    .line 811
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    .line 812
    const/4 v0, 0x1

    #@9
    .line 814
    :goto_9
    return v0

    #@a
    :cond_a
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    #@d
    move-result v0

    #@e
    goto :goto_9
.end method

.method public findViewById(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 515
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .registers 2

    #@0
    .prologue
    .line 215
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2
    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getCurrentFocus()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 504
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@6
    invoke-virtual {v0}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getLayoutInflater()Landroid/view/LayoutInflater;
    .registers 2

    #@0
    .prologue
    .line 1125
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final getOwnerActivity()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/app/Dialog;->mOwnerActivity:Landroid/app/Activity;

    #@2
    return-object v0
.end method

.method public final getVolumeControlStream()I
    .registers 2

    #@0
    .prologue
    .line 1265
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/view/Window;->getVolumeControlStream()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getWindow()Landroid/view/Window;
    .registers 2

    #@0
    .prologue
    .line 491
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    return-object v0
.end method

.method public hide()V
    .registers 3

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 321
    iget-object v0, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@6
    const/16 v1, 0x8

    #@8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@b
    .line 323
    :cond_b
    return-void
.end method

.method public invalidateOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 956
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/view/Window;->invalidatePanelMenu(I)V

    #@6
    .line 957
    return-void
.end method

.method public isShowing()Z
    .registers 2

    #@0
    .prologue
    .line 246
    iget-boolean v0, p0, Landroid/app/Dialog;->mShowing:Z

    #@2
    return v0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 1041
    iget-object v0, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@2
    if-ne p1, v0, :cond_7

    #@4
    .line 1042
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@7
    .line 1044
    :cond_7
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 1031
    iput-object p1, p0, Landroid/app/Dialog;->mActionMode:Landroid/view/ActionMode;

    #@2
    .line 1032
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 742
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    #@0
    .prologue
    .line 644
    iget-boolean v0, p0, Landroid/app/Dialog;->mCancelable:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 645
    invoke-virtual {p0}, Landroid/app/Dialog;->cancel()V

    #@7
    .line 647
    :cond_7
    return-void
.end method

.method public onContentChanged()V
    .registers 1

    #@0
    .prologue
    .line 736
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 990
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onContextMenuClosed(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 997
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 416
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 4
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    #@0
    .prologue
    .line 963
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 910
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .registers 4
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 857
    if-nez p1, :cond_7

    #@2
    .line 858
    invoke-virtual {p0, p2}, Landroid/app/Dialog;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    #@5
    move-result v0

    #@6
    .line 861
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .registers 3
    .parameter "featureId"

    #@0
    .prologue
    .line 850
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 745
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 726
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 594
    const/4 v0, 0x4

    #@1
    if-ne p1, v0, :cond_8

    #@3
    .line 595
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    #@6
    .line 596
    const/4 v0, 0x1

    #@7
    .line 599
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 608
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "keyCode"
    .parameter "repeatCount"
    .parameter "event"

    #@0
    .prologue
    .line 635
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 660
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    .line 621
    const/4 v0, 0x4

    #@1
    if-ne p1, v0, :cond_14

    #@3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_14

    #@9
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_14

    #@f
    .line 623
    invoke-virtual {p0}, Landroid/app/Dialog;->onBackPressed()V

    #@12
    .line 624
    const/4 v0, 0x1

    #@13
    .line 626
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .registers 4
    .parameter "featureId"
    .parameter "item"

    #@0
    .prologue
    .line 889
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 5
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 879
    const/16 v0, 0x8

    #@3
    if-ne p1, v0, :cond_a

    #@5
    .line 880
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ActionBarImpl;->dispatchMenuVisibilityChanged(Z)V

    #@a
    .line 882
    :cond_a
    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 929
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 936
    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 5
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 896
    const/16 v0, 0x8

    #@2
    if-ne p1, v0, :cond_a

    #@4
    .line 897
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ActionBarImpl;->dispatchMenuVisibilityChanged(Z)V

    #@a
    .line 899
    :cond_a
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 922
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 7
    .parameter "featureId"
    .parameter "view"
    .parameter "menu"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 868
    if-nez p1, :cond_11

    #@3
    if-eqz p3, :cond_11

    #@5
    .line 869
    invoke-virtual {p0, p3}, Landroid/app/Dialog;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@8
    move-result v0

    #@9
    .line 870
    .local v0, goforit:Z
    if-eqz v0, :cond_12

    #@b
    invoke-interface {p3}, Landroid/view/Menu;->hasVisibleItems()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_12

    #@11
    .line 872
    .end local v0           #goforit:Z
    :cond_11
    :goto_11
    return v1

    #@12
    .line 870
    .restart local v0       #goforit:Z
    :cond_12
    const/4 v1, 0x0

    #@13
    goto :goto_11
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 470
    const-string v1, "android:dialogHierarchy"

    #@2
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@5
    move-result-object v0

    #@6
    .line 471
    .local v0, dialogHierarchyState:Landroid/os/Bundle;
    if-nez v0, :cond_9

    #@8
    .line 480
    :cond_8
    :goto_8
    return-void

    #@9
    .line 475
    :cond_9
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->dispatchOnCreate(Landroid/os/Bundle;)V

    #@c
    .line 476
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@e
    invoke-virtual {v1, v0}, Landroid/view/Window;->restoreHierarchyState(Landroid/os/Bundle;)V

    #@11
    .line 477
    const-string v1, "android:dialogShowing"

    #@13
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_8

    #@19
    .line 478
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    #@1c
    goto :goto_8
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 451
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 452
    .local v0, bundle:Landroid/os/Bundle;
    const-string v1, "android:dialogShowing"

    #@7
    iget-boolean v2, p0, Landroid/app/Dialog;->mShowing:Z

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@c
    .line 453
    iget-boolean v1, p0, Landroid/app/Dialog;->mCreated:Z

    #@e
    if-eqz v1, :cond_1b

    #@10
    .line 454
    const-string v1, "android:dialogHierarchy"

    #@12
    iget-object v2, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@14
    invoke-virtual {v2}, Landroid/view/Window;->saveHierarchyState()Landroid/os/Bundle;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    #@1b
    .line 456
    :cond_1b
    return-object v0
.end method

.method public onSearchRequested()Z
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1003
    iget-object v4, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@4
    const-string/jumbo v5, "search"

    #@7
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Landroid/app/SearchManager;

    #@d
    .line 1007
    .local v0, searchManager:Landroid/app/SearchManager;
    invoke-direct {p0}, Landroid/app/Dialog;->getAssociatedActivity()Landroid/content/ComponentName;

    #@10
    move-result-object v3

    #@11
    .line 1008
    .local v3, appName:Landroid/content/ComponentName;
    if-eqz v3, :cond_22

    #@13
    invoke-virtual {v0, v3}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    #@16
    move-result-object v4

    #@17
    if-eqz v4, :cond_22

    #@19
    move-object v4, v1

    #@1a
    move v5, v2

    #@1b
    .line 1009
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    #@1e
    .line 1010
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    #@21
    .line 1011
    const/4 v2, 0x1

    #@22
    .line 1013
    :cond_22
    return v2
.end method

.method protected onStart()V
    .registers 4

    #@0
    .prologue
    .line 422
    iget-object v1, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2
    if-eqz v1, :cond_a

    #@4
    iget-object v1, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@6
    const/4 v2, 0x1

    #@7
    invoke-virtual {v1, v2}, Lcom/android/internal/app/ActionBarImpl;->setShowHideAnimationEnabled(Z)V

    #@a
    .line 424
    :cond_a
    iget-object v1, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@c
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@f
    move-result-object v0

    #@10
    .line 425
    .local v0, l:Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->extUsage:I

    #@12
    if-lez v1, :cond_17

    #@14
    .line 426
    invoke-virtual {p0}, Landroid/app/Dialog;->refreshExternalDisplay()V

    #@17
    .line 430
    :cond_17
    return-void
.end method

.method protected onStop()V
    .registers 3

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-virtual {v0, v1}, Lcom/android/internal/app/ActionBarImpl;->setShowHideAnimationEnabled(Z)V

    #@a
    .line 437
    :cond_a
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 674
    iget-boolean v0, p0, Landroid/app/Dialog;->mCancelable:Z

    #@2
    if-eqz v0, :cond_17

    #@4
    iget-boolean v0, p0, Landroid/app/Dialog;->mShowing:Z

    #@6
    if-eqz v0, :cond_17

    #@8
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@a
    iget-object v1, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/view/Window;->shouldCloseOnTouch(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_17

    #@12
    .line 675
    invoke-virtual {p0}, Landroid/app/Dialog;->cancel()V

    #@15
    .line 676
    const/4 v0, 0x1

    #@16
    .line 679
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 697
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "params"

    #@0
    .prologue
    .line 730
    iget-object v0, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 731
    iget-object v0, p0, Landroid/app/Dialog;->mWindowManager:Landroid/view/WindowManager;

    #@6
    iget-object v1, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@8
    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@b
    .line 733
    :cond_b
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasFocus"

    #@0
    .prologue
    .line 739
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 1018
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 1019
    iget-object v0, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/app/ActionBarImpl;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    #@9
    move-result-object v0

    #@a
    .line 1021
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public openContextMenu(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 983
    invoke-virtual {p1}, Landroid/view/View;->showContextMenu()Z

    #@3
    .line 984
    return-void
.end method

.method public openOptionsMenu()V
    .registers 4

    #@0
    .prologue
    .line 942
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->openPanel(ILandroid/view/KeyEvent;)V

    #@7
    .line 943
    return-void
.end method

.method refreshExternalDisplay()V
    .registers 5

    #@0
    .prologue
    .line 1302
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    #@3
    move-result-object v1

    #@4
    new-instance v2, Landroid/content/Intent;

    #@6
    const-string v3, "com.lge.intent.action.LG_DUAL_SCREEN_UPDATE"

    #@8
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 1306
    :goto_e
    return-void

    #@f
    .line 1303
    :catch_f
    move-exception v0

    #@10
    .line 1304
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "Dialog"

    #@12
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_e
.end method

.method public registerForContextMenu(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 969
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@3
    .line 970
    return-void
.end method

.method public final requestWindowFeature(I)Z
    .registers 3
    .parameter "featureId"

    #@0
    .prologue
    .line 1089
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->requestFeature(I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public setCancelMessage(Landroid/os/Message;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 1195
    iput-object p1, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@2
    .line 1196
    return-void
.end method

.method public setCancelable(Z)V
    .registers 2
    .parameter "flag"

    #@0
    .prologue
    .line 1133
    iput-boolean p1, p0, Landroid/app/Dialog;->mCancelable:Z

    #@2
    .line 1134
    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .registers 3
    .parameter "cancel"

    #@0
    .prologue
    .line 1145
    if-eqz p1, :cond_9

    #@2
    iget-boolean v0, p0, Landroid/app/Dialog;->mCancelable:Z

    #@4
    if-nez v0, :cond_9

    #@6
    .line 1146
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/app/Dialog;->mCancelable:Z

    #@9
    .line 1149
    :cond_9
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@b
    invoke-virtual {v0, p1}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    #@e
    .line 1150
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .parameter "layoutResID"

    #@0
    .prologue
    .line 525
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(I)V

    #@5
    .line 526
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 536
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    #@5
    .line 537
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 548
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@5
    .line 549
    return-void
.end method

.method public setDismissMessage(Landroid/os/Message;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 1232
    iput-object p1, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@2
    .line 1233
    return-void
.end method

.method public final setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "featureId"
    .parameter "drawable"

    #@0
    .prologue
    .line 1113
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    #@7
    .line 1114
    return-void
.end method

.method public final setFeatureDrawableAlpha(II)V
    .registers 4
    .parameter "featureId"
    .parameter "alpha"

    #@0
    .prologue
    .line 1121
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableAlpha(II)V

    #@7
    .line 1122
    return-void
.end method

.method public final setFeatureDrawableResource(II)V
    .registers 4
    .parameter "featureId"
    .parameter "resId"

    #@0
    .prologue
    .line 1097
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    #@7
    .line 1098
    return-void
.end method

.method public final setFeatureDrawableUri(ILandroid/net/Uri;)V
    .registers 4
    .parameter "featureId"
    .parameter "uri"

    #@0
    .prologue
    .line 1105
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setFeatureDrawableUri(ILandroid/net/Uri;)V

    #@7
    .line 1106
    return-void
.end method

.method public setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 1177
    iget-object v0, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_25

    #@4
    .line 1178
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "OnCancelListener is already taken by "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " and can not be replaced."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 1182
    :cond_25
    if-eqz p1, :cond_32

    #@27
    .line 1183
    iget-object v0, p0, Landroid/app/Dialog;->mListenersHandler:Landroid/os/Handler;

    #@29
    const/16 v1, 0x44

    #@2b
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@31
    .line 1187
    :goto_31
    return-void

    #@32
    .line 1185
    :cond_32
    const/4 v0, 0x0

    #@33
    iput-object v0, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@35
    goto :goto_31
.end method

.method public setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 1203
    iget-object v0, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_25

    #@4
    .line 1204
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "OnDismissListener is already taken by "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " and can not be replaced."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 1208
    :cond_25
    if-eqz p1, :cond_32

    #@27
    .line 1209
    iget-object v0, p0, Landroid/app/Dialog;->mListenersHandler:Landroid/os/Handler;

    #@29
    const/16 v1, 0x43

    #@2b
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@31
    .line 1213
    :goto_31
    return-void

    #@32
    .line 1211
    :cond_32
    const/4 v0, 0x0

    #@33
    iput-object v0, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@35
    goto :goto_31
.end method

.method public setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V
    .registers 2
    .parameter "onKeyListener"

    #@0
    .prologue
    .line 1272
    iput-object p1, p0, Landroid/app/Dialog;->mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    #@2
    .line 1273
    return-void
.end method

.method public setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 1220
    if-eqz p1, :cond_d

    #@2
    .line 1221
    iget-object v0, p0, Landroid/app/Dialog;->mListenersHandler:Landroid/os/Handler;

    #@4
    const/16 v1, 0x45

    #@6
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/app/Dialog;->mShowMessage:Landroid/os/Message;

    #@c
    .line 1225
    :goto_c
    return-void

    #@d
    .line 1223
    :cond_d
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Landroid/app/Dialog;->mShowMessage:Landroid/os/Message;

    #@10
    goto :goto_c
.end method

.method public final setOwnerActivity(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    .line 225
    iput-object p1, p0, Landroid/app/Dialog;->mOwnerActivity:Landroid/app/Activity;

    #@2
    .line 227
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@5
    move-result-object v0

    #@6
    iget-object v1, p0, Landroid/app/Dialog;->mOwnerActivity:Landroid/app/Activity;

    #@8
    invoke-virtual {v1}, Landroid/app/Activity;->getVolumeControlStream()I

    #@b
    move-result v1

    #@c
    invoke-virtual {v0, v1}, Landroid/view/Window;->setVolumeControlStream(I)V

    #@f
    .line 228
    return-void
.end method

.method public setTitle(I)V
    .registers 3
    .parameter "titleId"

    #@0
    .prologue
    .line 579
    iget-object v0, p0, Landroid/app/Dialog;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    #@9
    .line 580
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "title"

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@5
    .line 569
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@7
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@e
    .line 570
    return-void
.end method

.method public final setVolumeControlStream(I)V
    .registers 3
    .parameter "streamType"

    #@0
    .prologue
    .line 1258
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setVolumeControlStream(I)V

    #@7
    .line 1259
    return-void
.end method

.method public show()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/16 v4, 0x8

    #@3
    .line 256
    iget-boolean v3, p0, Landroid/app/Dialog;->mShowing:Z

    #@5
    if-eqz v3, :cond_1e

    #@7
    .line 257
    iget-object v3, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@9
    if-eqz v3, :cond_1d

    #@b
    .line 258
    iget-object v3, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@d
    invoke-virtual {v3, v4}, Landroid/view/Window;->hasFeature(I)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_18

    #@13
    .line 259
    iget-object v3, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@15
    invoke-virtual {v3, v4}, Landroid/view/Window;->invalidatePanelMenu(I)V

    #@18
    .line 261
    :cond_18
    iget-object v3, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@1a
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    #@1d
    .line 314
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 266
    :cond_1e
    iput-boolean v5, p0, Landroid/app/Dialog;->mCanceled:Z

    #@20
    .line 268
    iget-boolean v3, p0, Landroid/app/Dialog;->mCreated:Z

    #@22
    if-nez v3, :cond_28

    #@24
    .line 269
    const/4 v3, 0x0

    #@25
    invoke-virtual {p0, v3}, Landroid/app/Dialog;->dispatchOnCreate(Landroid/os/Bundle;)V

    #@28
    .line 272
    :cond_28
    invoke-virtual {p0}, Landroid/app/Dialog;->onStart()V

    #@2b
    .line 273
    iget-object v3, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2d
    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@30
    move-result-object v3

    #@31
    iput-object v3, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@33
    .line 275
    iget-object v3, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@35
    if-nez v3, :cond_46

    #@37
    iget-object v3, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@39
    invoke-virtual {v3, v4}, Landroid/view/Window;->hasFeature(I)Z

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_46

    #@3f
    .line 276
    new-instance v3, Lcom/android/internal/app/ActionBarImpl;

    #@41
    invoke-direct {v3, p0}, Lcom/android/internal/app/ActionBarImpl;-><init>(Landroid/app/Dialog;)V

    #@44
    iput-object v3, p0, Landroid/app/Dialog;->mActionBar:Lcom/android/internal/app/ActionBarImpl;

    #@46
    .line 279
    :cond_46
    iget-object v3, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@48
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@4b
    move-result-object v1

    #@4c
    .line 280
    .local v1, l:Landroid/view/WindowManager$LayoutParams;
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@4e
    and-int/lit16 v3, v3, 0x100

    #@50
    if-nez v3, :cond_8a

    #@52
    .line 282
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    #@54
    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@57
    .line 283
    .local v2, nl:Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v2, v1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    #@5a
    .line 284
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@5c
    or-int/lit16 v3, v3, 0x100

    #@5e
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@60
    .line 286
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@62
    if-eqz v3, :cond_89

    #@64
    .line 287
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@66
    const/high16 v4, 0x1000

    #@68
    or-int/2addr v3, v4

    #@69
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@6b
    .line 288
    const-string v3, "SplitWindow"

    #@6d
    new-instance v4, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v5, "extend value set as split dialog.. extend = "

    #@74
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@7a
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 290
    :cond_89
    move-object v1, v2

    #@8a
    .line 294
    .end local v2           #nl:Landroid/view/WindowManager$LayoutParams;
    :cond_8a
    :try_start_8a
    iget-object v3, p0, Landroid/app/Dialog;->mWindowManager:Landroid/view/WindowManager;

    #@8c
    iget-object v4, p0, Landroid/app/Dialog;->mDecor:Landroid/view/View;

    #@8e
    invoke-interface {v3, v4, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@91
    .line 295
    const/4 v3, 0x1

    #@92
    iput-boolean v3, p0, Landroid/app/Dialog;->mShowing:Z

    #@94
    .line 297
    invoke-direct {p0}, Landroid/app/Dialog;->sendShowMessage()V
    :try_end_97
    .catchall {:try_start_8a .. :try_end_97} :catchall_ff

    #@97
    .line 302
    :try_start_97
    sget-boolean v3, Lcom/lge/app/atsagent/AtsViewAgent;->IS_ETA2_ACTIVATED:Z

    #@99
    if-eqz v3, :cond_d2

    #@9b
    iget-object v3, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@9d
    if-nez v3, :cond_d2

    #@9f
    .line 303
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    #@a2
    move-result-object v3

    #@a3
    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@a6
    move-result-object v3

    #@a7
    new-instance v4, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@af
    move-result-object v5

    #@b0
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b3
    move-result-object v5

    #@b4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v4

    #@b8
    const-string v5, "::"

    #@ba
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v4

    #@be
    const-class v5, Landroid/app/Dialog;

    #@c0
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c3
    move-result-object v5

    #@c4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v4

    #@cc
    invoke-static {v3, v4}, Lcom/lge/app/atsagent/AtsViewAgent;->createViewAgent(Landroid/view/View;Ljava/lang/String;)Lcom/lge/app/atsagent/IViewAgent;

    #@cf
    move-result-object v3

    #@d0
    iput-object v3, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@d2
    .line 306
    :cond_d2
    iget-object v3, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@d4
    if-eqz v3, :cond_1d

    #@d6
    .line 307
    iget-object v3, p0, Landroid/app/Dialog;->viewAgent:Lcom/lge/app/atsagent/IViewAgent;

    #@d8
    invoke-interface {v3}, Lcom/lge/app/atsagent/IViewAgent;->onShow()V
    :try_end_db
    .catch Ljava/lang/Exception; {:try_start_97 .. :try_end_db} :catch_dd

    #@db
    goto/16 :goto_1d

    #@dd
    .line 309
    :catch_dd
    move-exception v0

    #@de
    .line 310
    .local v0, ex:Ljava/lang/Exception;
    const-string v3, "ViewAgent"

    #@e0
    new-instance v4, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v5, " ViewAgent.onShow Exception: "

    #@e7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v4

    #@eb
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v4

    #@f3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v4

    #@f7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    .line 311
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@fd
    goto/16 :goto_1d

    #@ff
    .line 298
    .end local v0           #ex:Ljava/lang/Exception;
    :catchall_ff
    move-exception v3

    #@100
    throw v3
.end method

.method public takeCancelAndDismissListeners(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnDismissListener;)Z
    .registers 5
    .parameter "msg"
    .parameter "cancel"
    .parameter "dismiss"

    #@0
    .prologue
    .line 1238
    iget-object v0, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1239
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@7
    .line 1244
    :cond_7
    invoke-virtual {p0, p2}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    #@a
    .line 1245
    invoke-virtual {p0, p3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    #@d
    .line 1246
    iput-object p1, p0, Landroid/app/Dialog;->mCancelAndDismissTaken:Ljava/lang/String;

    #@f
    .line 1248
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    .line 1240
    :cond_11
    iget-object v0, p0, Landroid/app/Dialog;->mCancelMessage:Landroid/os/Message;

    #@13
    if-nez v0, :cond_19

    #@15
    iget-object v0, p0, Landroid/app/Dialog;->mDismissMessage:Landroid/os/Message;

    #@17
    if-eqz v0, :cond_7

    #@19
    .line 1241
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_10
.end method

.method public takeKeyEvents(Z)V
    .registers 3
    .parameter "get"

    #@0
    .prologue
    .line 1074
    iget-object v0, p0, Landroid/app/Dialog;->mWindow:Landroid/view/Window;

    #@2
    invoke-virtual {v0, p1}, Landroid/view/Window;->takeKeyEvents(Z)V

    #@5
    .line 1075
    return-void
.end method

.method public unregisterForContextMenu(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 976
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@4
    .line 977
    return-void
.end method
