.class public Landroid/app/ListFragment;
.super Landroid/app/Fragment;
.source "ListFragment.java"


# instance fields
.field mAdapter:Landroid/widget/ListAdapter;

.field mEmptyText:Ljava/lang/CharSequence;

.field mEmptyView:Landroid/view/View;

.field private final mHandler:Landroid/os/Handler;

.field mList:Landroid/widget/ListView;

.field mListContainer:Landroid/view/View;

.field mListShown:Z

.field private final mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mProgressContainer:Landroid/view/View;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field mStandardEmptyView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 173
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    #@3
    .line 149
    new-instance v0, Landroid/os/Handler;

    #@5
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@8
    iput-object v0, p0, Landroid/app/ListFragment;->mHandler:Landroid/os/Handler;

    #@a
    .line 151
    new-instance v0, Landroid/app/ListFragment$1;

    #@c
    invoke-direct {v0, p0}, Landroid/app/ListFragment$1;-><init>(Landroid/app/ListFragment;)V

    #@f
    iput-object v0, p0, Landroid/app/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@11
    .line 157
    new-instance v0, Landroid/app/ListFragment$2;

    #@13
    invoke-direct {v0, p0}, Landroid/app/ListFragment$2;-><init>(Landroid/app/ListFragment;)V

    #@16
    iput-object v0, p0, Landroid/app/ListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@18
    .line 174
    return-void
.end method

.method private ensureList()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 381
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@3
    if-eqz v3, :cond_6

    #@5
    .line 433
    :goto_5
    return-void

    #@6
    .line 384
    :cond_6
    invoke-virtual {p0}, Landroid/app/ListFragment;->getView()Landroid/view/View;

    #@9
    move-result-object v2

    #@a
    .line 385
    .local v2, root:Landroid/view/View;
    if-nez v2, :cond_14

    #@c
    .line 386
    new-instance v3, Ljava/lang/IllegalStateException;

    #@e
    const-string v4, "Content view not yet created"

    #@10
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v3

    #@14
    .line 388
    :cond_14
    instance-of v3, v2, Landroid/widget/ListView;

    #@16
    if-eqz v3, :cond_3a

    #@18
    .line 389
    check-cast v2, Landroid/widget/ListView;

    #@1a
    .end local v2           #root:Landroid/view/View;
    iput-object v2, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@1c
    .line 419
    :cond_1c
    :goto_1c
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, p0, Landroid/app/ListFragment;->mListShown:Z

    #@1f
    .line 420
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@21
    iget-object v4, p0, Landroid/app/ListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    #@23
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@26
    .line 421
    iget-object v3, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@28
    if-eqz v3, :cond_af

    #@2a
    .line 422
    iget-object v0, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@2c
    .line 423
    .local v0, adapter:Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    #@2d
    iput-object v3, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@2f
    .line 424
    invoke-virtual {p0, v0}, Landroid/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    #@32
    .line 432
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    :cond_32
    :goto_32
    iget-object v3, p0, Landroid/app/ListFragment;->mHandler:Landroid/os/Handler;

    #@34
    iget-object v4, p0, Landroid/app/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@36
    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@39
    goto :goto_5

    #@3a
    .line 391
    .restart local v2       #root:Landroid/view/View;
    :cond_3a
    const v3, 0x1020320

    #@3d
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@40
    move-result-object v3

    #@41
    check-cast v3, Landroid/widget/TextView;

    #@43
    iput-object v3, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@45
    .line 393
    iget-object v3, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@47
    if-nez v3, :cond_77

    #@49
    .line 394
    const v3, 0x1020004

    #@4c
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@4f
    move-result-object v3

    #@50
    iput-object v3, p0, Landroid/app/ListFragment;->mEmptyView:Landroid/view/View;

    #@52
    .line 398
    :goto_52
    const v3, 0x102031e

    #@55
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@58
    move-result-object v3

    #@59
    iput-object v3, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@5b
    .line 399
    const v3, 0x102031f

    #@5e
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@61
    move-result-object v3

    #@62
    iput-object v3, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@64
    .line 400
    const v3, 0x102000a

    #@67
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6a
    move-result-object v1

    #@6b
    .line 401
    .local v1, rawListView:Landroid/view/View;
    instance-of v3, v1, Landroid/widget/ListView;

    #@6d
    if-nez v3, :cond_7f

    #@6f
    .line 402
    new-instance v3, Ljava/lang/RuntimeException;

    #@71
    const-string v4, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    #@73
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@76
    throw v3

    #@77
    .line 396
    .end local v1           #rawListView:Landroid/view/View;
    :cond_77
    iget-object v3, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@79
    const/16 v4, 0x8

    #@7b
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    #@7e
    goto :goto_52

    #@7f
    .line 406
    .restart local v1       #rawListView:Landroid/view/View;
    :cond_7f
    check-cast v1, Landroid/widget/ListView;

    #@81
    .end local v1           #rawListView:Landroid/view/View;
    iput-object v1, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@83
    .line 407
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@85
    if-nez v3, :cond_8f

    #@87
    .line 408
    new-instance v3, Ljava/lang/RuntimeException;

    #@89
    const-string v4, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    #@8b
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8e
    throw v3

    #@8f
    .line 412
    :cond_8f
    iget-object v3, p0, Landroid/app/ListFragment;->mEmptyView:Landroid/view/View;

    #@91
    if-eqz v3, :cond_9b

    #@93
    .line 413
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@95
    iget-object v4, p0, Landroid/app/ListFragment;->mEmptyView:Landroid/view/View;

    #@97
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    #@9a
    goto :goto_1c

    #@9b
    .line 414
    :cond_9b
    iget-object v3, p0, Landroid/app/ListFragment;->mEmptyText:Ljava/lang/CharSequence;

    #@9d
    if-eqz v3, :cond_1c

    #@9f
    .line 415
    iget-object v3, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@a1
    iget-object v4, p0, Landroid/app/ListFragment;->mEmptyText:Ljava/lang/CharSequence;

    #@a3
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@a6
    .line 416
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@a8
    iget-object v4, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@aa
    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    #@ad
    goto/16 :goto_1c

    #@af
    .line 428
    .end local v2           #root:Landroid/view/View;
    :cond_af
    iget-object v3, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@b1
    if-eqz v3, :cond_32

    #@b3
    .line 429
    invoke-direct {p0, v5, v5}, Landroid/app/ListFragment;->setListShown(ZZ)V

    #@b6
    goto/16 :goto_32
.end method

.method private setListShown(ZZ)V
    .registers 9
    .parameter "shown"
    .parameter "animate"

    #@0
    .prologue
    const v5, 0x10a0001

    #@3
    const/high16 v4, 0x10a

    #@5
    const/16 v3, 0x8

    #@7
    const/4 v2, 0x0

    #@8
    .line 338
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@b
    .line 339
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@d
    if-nez v0, :cond_17

    #@f
    .line 340
    new-instance v0, Ljava/lang/IllegalStateException;

    #@11
    const-string v1, "Can\'t be used with a custom content view"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 342
    :cond_17
    iget-boolean v0, p0, Landroid/app/ListFragment;->mListShown:Z

    #@19
    if-ne v0, p1, :cond_1c

    #@1b
    .line 371
    :goto_1b
    return-void

    #@1c
    .line 345
    :cond_1c
    iput-boolean p1, p0, Landroid/app/ListFragment;->mListShown:Z

    #@1e
    .line 346
    if-eqz p1, :cond_52

    #@20
    .line 347
    if-eqz p2, :cond_47

    #@22
    .line 348
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@24
    invoke-virtual {p0}, Landroid/app/ListFragment;->getActivity()Landroid/app/Activity;

    #@27
    move-result-object v1

    #@28
    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@2f
    .line 350
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@31
    invoke-virtual {p0}, Landroid/app/ListFragment;->getActivity()Landroid/app/Activity;

    #@34
    move-result-object v1

    #@35
    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@3c
    .line 356
    :goto_3c
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@3e
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@41
    .line 357
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@43
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@46
    goto :goto_1b

    #@47
    .line 353
    :cond_47
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@49
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    #@4c
    .line 354
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@4e
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    #@51
    goto :goto_3c

    #@52
    .line 359
    :cond_52
    if-eqz p2, :cond_79

    #@54
    .line 360
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@56
    invoke-virtual {p0}, Landroid/app/ListFragment;->getActivity()Landroid/app/Activity;

    #@59
    move-result-object v1

    #@5a
    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@61
    .line 362
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@63
    invoke-virtual {p0}, Landroid/app/ListFragment;->getActivity()Landroid/app/Activity;

    #@66
    move-result-object v1

    #@67
    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    #@6e
    .line 368
    :goto_6e
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@70
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    #@73
    .line 369
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@75
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@78
    goto :goto_1b

    #@79
    .line 365
    :cond_79
    iget-object v0, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@7b
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    #@7e
    .line 366
    iget-object v0, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@80
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    #@83
    goto :goto_6e
.end method


# virtual methods
.method public getListAdapter()Landroid/widget/ListAdapter;
    .registers 2

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@2
    return-object v0
.end method

.method public getListView()Landroid/widget/ListView;
    .registers 2

    #@0
    .prologue
    .line 280
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@3
    .line 281
    iget-object v0, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@5
    return-object v0
.end method

.method public getSelectedItemId()J
    .registers 3

    #@0
    .prologue
    .line 272
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@3
    .line 273
    iget-object v0, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@5
    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemId()J

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .registers 2

    #@0
    .prologue
    .line 264
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@3
    .line 265
    iget-object v0, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@5
    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 193
    const v0, 0x1090014

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public onDestroyView()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 211
    iget-object v0, p0, Landroid/app/ListFragment;->mHandler:Landroid/os/Handler;

    #@3
    iget-object v1, p0, Landroid/app/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    #@5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@8
    .line 212
    iput-object v2, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@a
    .line 213
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Landroid/app/ListFragment;->mListShown:Z

    #@d
    .line 214
    iput-object v2, p0, Landroid/app/ListFragment;->mListContainer:Landroid/view/View;

    #@f
    iput-object v2, p0, Landroid/app/ListFragment;->mProgressContainer:Landroid/view/View;

    #@11
    iput-object v2, p0, Landroid/app/ListFragment;->mEmptyView:Landroid/view/View;

    #@13
    .line 215
    iput-object v2, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@15
    .line 216
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    #@18
    .line 217
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 6
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    #@0
    .prologue
    .line 231
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3
    .parameter "view"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 202
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    #@3
    .line 203
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@6
    .line 204
    return-void
.end method

.method public setEmptyText(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 290
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@3
    .line 291
    iget-object v0, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@5
    if-nez v0, :cond_f

    #@7
    .line 292
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string v1, "Can\'t be used with a custom content view"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 294
    :cond_f
    iget-object v0, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@11
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@14
    .line 295
    iget-object v0, p0, Landroid/app/ListFragment;->mEmptyText:Ljava/lang/CharSequence;

    #@16
    if-nez v0, :cond_1f

    #@18
    .line 296
    iget-object v0, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@1a
    iget-object v1, p0, Landroid/app/ListFragment;->mStandardEmptyView:Landroid/widget/TextView;

    #@1c
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    #@1f
    .line 298
    :cond_1f
    iput-object p1, p0, Landroid/app/ListFragment;->mEmptyText:Ljava/lang/CharSequence;

    #@21
    .line 299
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .registers 6
    .parameter "adapter"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 237
    iget-object v3, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@4
    if-eqz v3, :cond_27

    #@6
    move v0, v1

    #@7
    .line 238
    .local v0, hadAdapter:Z
    :goto_7
    iput-object p1, p0, Landroid/app/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    #@9
    .line 239
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@b
    if-eqz v3, :cond_26

    #@d
    .line 240
    iget-object v3, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@f
    invoke-virtual {v3, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@12
    .line 241
    iget-boolean v3, p0, Landroid/app/ListFragment;->mListShown:Z

    #@14
    if-nez v3, :cond_26

    #@16
    if-nez v0, :cond_26

    #@18
    .line 244
    invoke-virtual {p0}, Landroid/app/ListFragment;->getView()Landroid/view/View;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@1f
    move-result-object v3

    #@20
    if-eqz v3, :cond_23

    #@22
    move v2, v1

    #@23
    :cond_23
    invoke-direct {p0, v1, v2}, Landroid/app/ListFragment;->setListShown(ZZ)V

    #@26
    .line 247
    :cond_26
    return-void

    #@27
    .end local v0           #hadAdapter:Z
    :cond_27
    move v0, v2

    #@28
    .line 237
    goto :goto_7
.end method

.method public setListShown(Z)V
    .registers 3
    .parameter "shown"

    #@0
    .prologue
    .line 316
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Landroid/app/ListFragment;->setListShown(ZZ)V

    #@4
    .line 317
    return-void
.end method

.method public setListShownNoAnimation(Z)V
    .registers 3
    .parameter "shown"

    #@0
    .prologue
    .line 324
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/app/ListFragment;->setListShown(ZZ)V

    #@4
    .line 325
    return-void
.end method

.method public setSelection(I)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 256
    invoke-direct {p0}, Landroid/app/ListFragment;->ensureList()V

    #@3
    .line 257
    iget-object v0, p0, Landroid/app/ListFragment;->mList:Landroid/widget/ListView;

    #@5
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    #@8
    .line 258
    return-void
.end method
