.class public Landroid/app/DownloadManager$Request;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Request"
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field public static final NETWORK_BLUETOOTH:I = 0x4

.field public static final NETWORK_MOBILE:I = 0x1

.field public static final NETWORK_WIFI:I = 0x2

.field private static final SCANNABLE_VALUE_NO:I = 0x2

.field private static final SCANNABLE_VALUE_YES:I = 0x0

.field public static final VISIBILITY_HIDDEN:I = 0x2

.field public static final VISIBILITY_VISIBLE:I = 0x0

.field public static final VISIBILITY_VISIBLE_NOTIFY_COMPLETED:I = 0x1

.field public static final VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION:I = 0x3


# instance fields
.field private mAllowedNetworkTypes:I

.field private mDescription:Ljava/lang/CharSequence;

.field private mDestinationUri:Landroid/net/Uri;

.field private mIsVisibleInDownloadsUi:Z

.field private mMeteredAllowed:Z

.field private mMimeType:Ljava/lang/String;

.field private mNotificationVisibility:I

.field private mRequestHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRoamingAllowed:Z

.field private mScannable:Z

.field private mTitle:Ljava/lang/CharSequence;

.field private mUri:Landroid/net/Uri;

.field private mUseSystemCache:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 334
    const-class v0, Landroid/app/DownloadManager;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_c

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    sput-boolean v0, Landroid/app/DownloadManager$Request;->$assertionsDisabled:Z

    #@b
    return-void

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_9
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 412
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 356
    new-instance v1, Ljava/util/ArrayList;

    #@7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v1, p0, Landroid/app/DownloadManager$Request;->mRequestHeaders:Ljava/util/List;

    #@c
    .line 360
    const/4 v1, -0x1

    #@d
    iput v1, p0, Landroid/app/DownloadManager$Request;->mAllowedNetworkTypes:I

    #@f
    .line 361
    iput-boolean v3, p0, Landroid/app/DownloadManager$Request;->mRoamingAllowed:Z

    #@11
    .line 362
    iput-boolean v3, p0, Landroid/app/DownloadManager$Request;->mMeteredAllowed:Z

    #@13
    .line 363
    iput-boolean v3, p0, Landroid/app/DownloadManager$Request;->mIsVisibleInDownloadsUi:Z

    #@15
    .line 364
    iput-boolean v2, p0, Landroid/app/DownloadManager$Request;->mScannable:Z

    #@17
    .line 365
    iput-boolean v2, p0, Landroid/app/DownloadManager$Request;->mUseSystemCache:Z

    #@19
    .line 407
    iput v2, p0, Landroid/app/DownloadManager$Request;->mNotificationVisibility:I

    #@1b
    .line 413
    if-nez p1, :cond_23

    #@1d
    .line 414
    new-instance v1, Ljava/lang/NullPointerException;

    #@1f
    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    #@22
    throw v1

    #@23
    .line 416
    :cond_23
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 417
    .local v0, scheme:Ljava/lang/String;
    if-eqz v0, :cond_39

    #@29
    const-string v1, "http"

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_52

    #@31
    const-string v1, "https"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36
    move-result v1

    #@37
    if-nez v1, :cond_52

    #@39
    .line 418
    :cond_39
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@3b
    new-instance v2, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v3, "Can only download HTTP/HTTPS URIs: "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@51
    throw v1

    #@52
    .line 420
    :cond_52
    iput-object p1, p0, Landroid/app/DownloadManager$Request;->mUri:Landroid/net/Uri;

    #@54
    .line 421
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 5
    .parameter "uriString"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 423
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 356
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/app/DownloadManager$Request;->mRequestHeaders:Ljava/util/List;

    #@c
    .line 360
    const/4 v0, -0x1

    #@d
    iput v0, p0, Landroid/app/DownloadManager$Request;->mAllowedNetworkTypes:I

    #@f
    .line 361
    iput-boolean v2, p0, Landroid/app/DownloadManager$Request;->mRoamingAllowed:Z

    #@11
    .line 362
    iput-boolean v2, p0, Landroid/app/DownloadManager$Request;->mMeteredAllowed:Z

    #@13
    .line 363
    iput-boolean v2, p0, Landroid/app/DownloadManager$Request;->mIsVisibleInDownloadsUi:Z

    #@15
    .line 364
    iput-boolean v1, p0, Landroid/app/DownloadManager$Request;->mScannable:Z

    #@17
    .line 365
    iput-boolean v1, p0, Landroid/app/DownloadManager$Request;->mUseSystemCache:Z

    #@19
    .line 407
    iput v1, p0, Landroid/app/DownloadManager$Request;->mNotificationVisibility:I

    #@1b
    .line 424
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/app/DownloadManager$Request;->mUri:Landroid/net/Uri;

    #@21
    .line 425
    return-void
.end method

.method private encodeHttpHeaders(Landroid/content/ContentValues;)V
    .registers 8
    .parameter "values"

    #@0
    .prologue
    .line 710
    const/4 v3, 0x0

    #@1
    .line 711
    .local v3, index:I
    iget-object v4, p0, Landroid/app/DownloadManager$Request;->mRequestHeaders:Ljava/util/List;

    #@3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v2

    #@7
    .local v2, i$:Ljava/util/Iterator;
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_4b

    #@d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Landroid/util/Pair;

    #@13
    .line 712
    .local v0, header:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@1a
    check-cast v4, Ljava/lang/String;

    #@1c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    const-string v5, ": "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@28
    check-cast v4, Ljava/lang/String;

    #@2a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    .line 713
    .local v1, headerString:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v5, "http_header_"

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {p1, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    .line 714
    add-int/lit8 v3, v3, 0x1

    #@4a
    .line 715
    goto :goto_7

    #@4b
    .line 716
    .end local v0           #header:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #headerString:Ljava/lang/String;
    :cond_4b
    return-void
.end method

.method private putIfNonNull(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .parameter "contentValues"
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 719
    if-eqz p3, :cond_9

    #@2
    .line 720
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p1, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 722
    :cond_9
    return-void
.end method

.method private setDestinationFromBase(Ljava/io/File;Ljava/lang/String;)V
    .registers 5
    .parameter "base"
    .parameter "subPath"

    #@0
    .prologue
    .line 512
    if-nez p2, :cond_b

    #@2
    .line 513
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "subPath cannot be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 515
    :cond_b
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@e
    move-result-object v0

    #@f
    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/app/DownloadManager$Request;->mDestinationUri:Landroid/net/Uri;

    #@15
    .line 516
    return-void
.end method


# virtual methods
.method public addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    .registers 5
    .parameter "header"
    .parameter "value"

    #@0
    .prologue
    .line 536
    if-nez p1, :cond_a

    #@2
    .line 537
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "header cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 539
    :cond_a
    const-string v0, ":"

    #@c
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_1a

    #@12
    .line 540
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v1, "header may not contain \':\'"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 542
    :cond_1a
    if-nez p2, :cond_1e

    #@1c
    .line 543
    const-string p2, ""

    #@1e
    .line 545
    :cond_1e
    iget-object v0, p0, Landroid/app/DownloadManager$Request;->mRequestHeaders:Ljava/util/List;

    #@20
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    #@23
    move-result-object v1

    #@24
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@27
    .line 546
    return-object p0
.end method

.method public allowScanningByMediaScanner()V
    .registers 2

    #@0
    .prologue
    .line 523
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/DownloadManager$Request;->mScannable:Z

    #@3
    .line 524
    return-void
.end method

.method public setAllowedNetworkTypes(I)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 632
    iput p1, p0, Landroid/app/DownloadManager$Request;->mAllowedNetworkTypes:I

    #@2
    .line 633
    return-object p0
.end method

.method public setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "allow"

    #@0
    .prologue
    .line 654
    iput-boolean p1, p0, Landroid/app/DownloadManager$Request;->mMeteredAllowed:Z

    #@2
    .line 655
    return-object p0
.end method

.method public setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "allowed"

    #@0
    .prologue
    .line 643
    iput-boolean p1, p0, Landroid/app/DownloadManager$Request;->mRoamingAllowed:Z

    #@2
    .line 644
    return-object p0
.end method

.method public setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "description"

    #@0
    .prologue
    .line 565
    iput-object p1, p0, Landroid/app/DownloadManager$Request;->mDescription:Ljava/lang/CharSequence;

    #@2
    .line 566
    return-object p0
.end method

.method public setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    .registers 5
    .parameter "context"
    .parameter "dirType"
    .parameter "subPath"

    #@0
    .prologue
    .line 477
    invoke-virtual {p1, p2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    invoke-direct {p0, v0, p3}, Landroid/app/DownloadManager$Request;->setDestinationFromBase(Ljava/io/File;Ljava/lang/String;)V

    #@7
    .line 478
    return-object p0
.end method

.method public setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    .registers 7
    .parameter "dirType"
    .parameter "subPath"

    #@0
    .prologue
    .line 495
    invoke-static {p1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    .line 496
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_2d

    #@a
    .line 497
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_50

    #@10
    .line 498
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    new-instance v2, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " already exists and is not a directory"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v1

    #@2d
    .line 502
    :cond_2d
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_50

    #@33
    .line 503
    new-instance v1, Ljava/lang/IllegalStateException;

    #@35
    new-instance v2, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v3, "Unable to create directory: "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v1

    #@50
    .line 507
    :cond_50
    invoke-direct {p0, v0, p2}, Landroid/app/DownloadManager$Request;->setDestinationFromBase(Ljava/io/File;Ljava/lang/String;)V

    #@53
    .line 508
    return-object p0
.end method

.method public setDestinationToSystemCache()Landroid/app/DownloadManager$Request;
    .registers 2

    #@0
    .prologue
    .line 459
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/DownloadManager$Request;->mUseSystemCache:Z

    #@3
    .line 460
    return-object p0
.end method

.method public setDestinationUri(Landroid/net/Uri;)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "uri"

    #@0
    .prologue
    .line 441
    iput-object p1, p0, Landroid/app/DownloadManager$Request;->mDestinationUri:Landroid/net/Uri;

    #@2
    .line 442
    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "mimeType"

    #@0
    .prologue
    .line 577
    iput-object p1, p0, Landroid/app/DownloadManager$Request;->mMimeType:Ljava/lang/String;

    #@2
    .line 578
    return-object p0
.end method

.method public setNotificationVisibility(I)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "visibility"

    #@0
    .prologue
    .line 618
    iput p1, p0, Landroid/app/DownloadManager$Request;->mNotificationVisibility:I

    #@2
    .line 619
    return-object p0
.end method

.method public setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;
    .registers 3
    .parameter "show"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 596
    if-eqz p1, :cond_8

    #@2
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, v0}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    #@6
    move-result-object v0

    #@7
    :goto_7
    return-object v0

    #@8
    :cond_8
    const/4 v0, 0x2

    #@9
    invoke-virtual {p0, v0}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    #@c
    move-result-object v0

    #@d
    goto :goto_7
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "title"

    #@0
    .prologue
    .line 556
    iput-object p1, p0, Landroid/app/DownloadManager$Request;->mTitle:Ljava/lang/CharSequence;

    #@2
    .line 557
    return-object p0
.end method

.method public setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;
    .registers 2
    .parameter "isVisible"

    #@0
    .prologue
    .line 665
    iput-boolean p1, p0, Landroid/app/DownloadManager$Request;->mIsVisibleInDownloadsUi:Z

    #@2
    .line 666
    return-object p0
.end method

.method toContentValues(Ljava/lang/String;)Landroid/content/ContentValues;
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 673
    new-instance v0, Landroid/content/ContentValues;

    #@3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 674
    .local v0, values:Landroid/content/ContentValues;
    sget-boolean v1, Landroid/app/DownloadManager$Request;->$assertionsDisabled:Z

    #@8
    if-nez v1, :cond_14

    #@a
    iget-object v1, p0, Landroid/app/DownloadManager$Request;->mUri:Landroid/net/Uri;

    #@c
    if-nez v1, :cond_14

    #@e
    new-instance v1, Ljava/lang/AssertionError;

    #@10
    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    #@13
    throw v1

    #@14
    .line 675
    :cond_14
    const-string/jumbo v1, "uri"

    #@17
    iget-object v3, p0, Landroid/app/DownloadManager$Request;->mUri:Landroid/net/Uri;

    #@19
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 676
    const-string/jumbo v1, "is_public_api"

    #@23
    const/4 v3, 0x1

    #@24
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@2b
    .line 677
    const-string/jumbo v1, "notificationpackage"

    #@2e
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 679
    iget-object v1, p0, Landroid/app/DownloadManager$Request;->mDestinationUri:Landroid/net/Uri;

    #@33
    if-eqz v1, :cond_b5

    #@35
    .line 680
    const-string v1, "destination"

    #@37
    const/4 v3, 0x4

    #@38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3f
    .line 681
    const-string v1, "hint"

    #@41
    iget-object v3, p0, Landroid/app/DownloadManager$Request;->mDestinationUri:Landroid/net/Uri;

    #@43
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 689
    :goto_4a
    const-string/jumbo v1, "scanned"

    #@4d
    iget-boolean v3, p0, Landroid/app/DownloadManager$Request;->mScannable:Z

    #@4f
    if-eqz v3, :cond_52

    #@51
    const/4 v2, 0x0

    #@52
    :cond_52
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@59
    .line 692
    iget-object v1, p0, Landroid/app/DownloadManager$Request;->mRequestHeaders:Ljava/util/List;

    #@5b
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@5e
    move-result v1

    #@5f
    if-nez v1, :cond_64

    #@61
    .line 693
    invoke-direct {p0, v0}, Landroid/app/DownloadManager$Request;->encodeHttpHeaders(Landroid/content/ContentValues;)V

    #@64
    .line 696
    :cond_64
    const-string/jumbo v1, "title"

    #@67
    iget-object v2, p0, Landroid/app/DownloadManager$Request;->mTitle:Ljava/lang/CharSequence;

    #@69
    invoke-direct {p0, v0, v1, v2}, Landroid/app/DownloadManager$Request;->putIfNonNull(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    #@6c
    .line 697
    const-string v1, "description"

    #@6e
    iget-object v2, p0, Landroid/app/DownloadManager$Request;->mDescription:Ljava/lang/CharSequence;

    #@70
    invoke-direct {p0, v0, v1, v2}, Landroid/app/DownloadManager$Request;->putIfNonNull(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    #@73
    .line 698
    const-string/jumbo v1, "mimetype"

    #@76
    iget-object v2, p0, Landroid/app/DownloadManager$Request;->mMimeType:Ljava/lang/String;

    #@78
    invoke-direct {p0, v0, v1, v2}, Landroid/app/DownloadManager$Request;->putIfNonNull(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    #@7b
    .line 700
    const-string/jumbo v1, "visibility"

    #@7e
    iget v2, p0, Landroid/app/DownloadManager$Request;->mNotificationVisibility:I

    #@80
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@83
    move-result-object v2

    #@84
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@87
    .line 701
    const-string v1, "allowed_network_types"

    #@89
    iget v2, p0, Landroid/app/DownloadManager$Request;->mAllowedNetworkTypes:I

    #@8b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@92
    .line 702
    const-string v1, "allow_roaming"

    #@94
    iget-boolean v2, p0, Landroid/app/DownloadManager$Request;->mRoamingAllowed:Z

    #@96
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@9d
    .line 703
    const-string v1, "allow_metered"

    #@9f
    iget-boolean v2, p0, Landroid/app/DownloadManager$Request;->mMeteredAllowed:Z

    #@a1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@a4
    move-result-object v2

    #@a5
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@a8
    .line 704
    const-string/jumbo v1, "is_visible_in_downloads_ui"

    #@ab
    iget-boolean v2, p0, Landroid/app/DownloadManager$Request;->mIsVisibleInDownloadsUi:Z

    #@ad
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@b0
    move-result-object v2

    #@b1
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    #@b4
    .line 706
    return-object v0

    #@b5
    .line 683
    :cond_b5
    const-string v3, "destination"

    #@b7
    iget-boolean v1, p0, Landroid/app/DownloadManager$Request;->mUseSystemCache:Z

    #@b9
    if-eqz v1, :cond_c4

    #@bb
    const/4 v1, 0x5

    #@bc
    :goto_bc
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bf
    move-result-object v1

    #@c0
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c3
    goto :goto_4a

    #@c4
    :cond_c4
    move v1, v2

    #@c5
    goto :goto_bc
.end method
