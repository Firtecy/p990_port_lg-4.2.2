.class public Landroid/app/Fragment;
.super Ljava/lang/Object;
.source "Fragment.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Fragment$InstantiationException;,
        Landroid/app/Fragment$SavedState;
    }
.end annotation


# static fields
.field static final ACTIVITY_CREATED:I = 0x2

.field static final CREATED:I = 0x1

.field static final INITIALIZING:I = 0x0

.field static final INVALID_STATE:I = -0x1

.field static final RESUMED:I = 0x5

.field static final STARTED:I = 0x4

.field static final STOPPED:I = 0x3

.field private static final sClassMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field mActivity:Landroid/app/Activity;

.field mAdded:Z

.field mAnimatingAway:Landroid/animation/Animator;

.field mArguments:Landroid/os/Bundle;

.field mBackStackNesting:I

.field mCalled:Z

.field mCheckedForLoaderManager:Z

.field mChildFragmentManager:Landroid/app/FragmentManagerImpl;

.field mContainer:Landroid/view/ViewGroup;

.field mContainerId:I

.field mDeferStart:Z

.field mDetached:Z

.field mFragmentId:I

.field mFragmentManager:Landroid/app/FragmentManagerImpl;

.field mFromLayout:Z

.field mHasMenu:Z

.field mHidden:Z

.field mInLayout:Z

.field mIndex:I

.field mLoaderManager:Landroid/app/LoaderManagerImpl;

.field mLoadersStarted:Z

.field mMenuVisible:Z

.field mNextAnim:I

.field mParentFragment:Landroid/app/Fragment;

.field mRemoving:Z

.field mRestored:Z

.field mResumed:Z

.field mRetainInstance:Z

.field mRetaining:Z

.field mSavedFragmentState:Landroid/os/Bundle;

.field mSavedViewState:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field mState:I

.field mStateAfterAnimating:I

.field mTag:Ljava/lang/String;

.field mTarget:Landroid/app/Fragment;

.field mTargetIndex:I

.field mTargetRequestCode:I

.field mUserVisibleHint:Z

.field mView:Landroid/view/View;

.field mWho:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 345
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/app/Fragment;->sClassMap:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, -0x1

    #@2
    .line 552
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 356
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/app/Fragment;->mState:I

    #@8
    .line 372
    iput v1, p0, Landroid/app/Fragment;->mIndex:I

    #@a
    .line 384
    iput v1, p0, Landroid/app/Fragment;->mTargetIndex:I

    #@c
    .line 455
    iput-boolean v2, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@e
    .line 474
    iput-boolean v2, p0, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@10
    .line 553
    return-void
.end method

.method public static instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;
    .registers 3
    .parameter "context"
    .parameter "fname"

    #@0
    .prologue
    .line 560
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;
    .registers 9
    .parameter "context"
    .parameter "fname"
    .parameter "args"

    #@0
    .prologue
    .line 579
    :try_start_0
    sget-object v3, Landroid/app/Fragment;->sClassMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Class;

    #@8
    .line 580
    .local v0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    if-nez v0, :cond_17

    #@a
    .line 582
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@11
    move-result-object v0

    #@12
    .line 583
    sget-object v3, Landroid/app/Fragment;->sClassMap:Ljava/util/HashMap;

    #@14
    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 585
    :cond_17
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/app/Fragment;

    #@1d
    .line 586
    .local v2, f:Landroid/app/Fragment;
    if-eqz p2, :cond_2c

    #@1f
    .line 587
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {p2, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@2a
    .line 588
    iput-object p2, v2, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;
    :try_end_2c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_2c} :catch_2d
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_2c} :catch_53
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_2c} :catch_79

    #@2c
    .line 590
    :cond_2c
    return-object v2

    #@2d
    .line 591
    .end local v0           #clazz:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .end local v2           #f:Landroid/app/Fragment;
    :catch_2d
    move-exception v1

    #@2e
    .line 592
    .local v1, e:Ljava/lang/ClassNotFoundException;
    new-instance v3, Landroid/app/Fragment$InstantiationException;

    #@30
    new-instance v4, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v5, "Unable to instantiate fragment "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, ": make sure class name exists, is public, and has an"

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    const-string v5, " empty constructor that is public"

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-direct {v3, v4, v1}, Landroid/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    #@52
    throw v3

    #@53
    .line 595
    .end local v1           #e:Ljava/lang/ClassNotFoundException;
    :catch_53
    move-exception v1

    #@54
    .line 596
    .local v1, e:Ljava/lang/InstantiationException;
    new-instance v3, Landroid/app/Fragment$InstantiationException;

    #@56
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "Unable to instantiate fragment "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    const-string v5, ": make sure class name exists, is public, and has an"

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    const-string v5, " empty constructor that is public"

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v4

    #@75
    invoke-direct {v3, v4, v1}, Landroid/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    #@78
    throw v3

    #@79
    .line 599
    .end local v1           #e:Ljava/lang/InstantiationException;
    :catch_79
    move-exception v1

    #@7a
    .line 600
    .local v1, e:Ljava/lang/IllegalAccessException;
    new-instance v3, Landroid/app/Fragment$InstantiationException;

    #@7c
    new-instance v4, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v5, "Unable to instantiate fragment "

    #@83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    const-string v5, ": make sure class name exists, is public, and has an"

    #@8d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    const-string v5, " empty constructor that is public"

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    invoke-direct {v3, v4, v1}, Landroid/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    #@9e
    throw v3
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter "prefix"
    .parameter "fd"
    .parameter "writer"
    .parameter "args"

    #@0
    .prologue
    .line 1573
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string/jumbo v0, "mFragmentId=#"

    #@6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9
    .line 1574
    iget v0, p0, Landroid/app/Fragment;->mFragmentId:I

    #@b
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    .line 1575
    const-string v0, " mContainerId=#"

    #@14
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    .line 1576
    iget v0, p0, Landroid/app/Fragment;->mContainerId:I

    #@19
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@20
    .line 1577
    const-string v0, " mTag="

    #@22
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@25
    iget-object v0, p0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@27
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2a
    .line 1578
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    const-string/jumbo v0, "mState="

    #@30
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@35
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@38
    .line 1579
    const-string v0, " mIndex="

    #@3a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    iget v0, p0, Landroid/app/Fragment;->mIndex:I

    #@3f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@42
    .line 1580
    const-string v0, " mWho="

    #@44
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47
    iget-object v0, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@49
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    .line 1581
    const-string v0, " mBackStackNesting="

    #@4e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51
    iget v0, p0, Landroid/app/Fragment;->mBackStackNesting:I

    #@53
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@56
    .line 1582
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@59
    const-string/jumbo v0, "mAdded="

    #@5c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5f
    iget-boolean v0, p0, Landroid/app/Fragment;->mAdded:Z

    #@61
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@64
    .line 1583
    const-string v0, " mRemoving="

    #@66
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@69
    iget-boolean v0, p0, Landroid/app/Fragment;->mRemoving:Z

    #@6b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@6e
    .line 1584
    const-string v0, " mResumed="

    #@70
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@73
    iget-boolean v0, p0, Landroid/app/Fragment;->mResumed:Z

    #@75
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@78
    .line 1585
    const-string v0, " mFromLayout="

    #@7a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7d
    iget-boolean v0, p0, Landroid/app/Fragment;->mFromLayout:Z

    #@7f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@82
    .line 1586
    const-string v0, " mInLayout="

    #@84
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@87
    iget-boolean v0, p0, Landroid/app/Fragment;->mInLayout:Z

    #@89
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@8c
    .line 1587
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    const-string/jumbo v0, "mHidden="

    #@92
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@95
    iget-boolean v0, p0, Landroid/app/Fragment;->mHidden:Z

    #@97
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@9a
    .line 1588
    const-string v0, " mDetached="

    #@9c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9f
    iget-boolean v0, p0, Landroid/app/Fragment;->mDetached:Z

    #@a1
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@a4
    .line 1589
    const-string v0, " mMenuVisible="

    #@a6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a9
    iget-boolean v0, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@ab
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@ae
    .line 1590
    const-string v0, " mHasMenu="

    #@b0
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b3
    iget-boolean v0, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@b5
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@b8
    .line 1591
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bb
    const-string/jumbo v0, "mRetainInstance="

    #@be
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c1
    iget-boolean v0, p0, Landroid/app/Fragment;->mRetainInstance:Z

    #@c3
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@c6
    .line 1592
    const-string v0, " mRetaining="

    #@c8
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cb
    iget-boolean v0, p0, Landroid/app/Fragment;->mRetaining:Z

    #@cd
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@d0
    .line 1593
    const-string v0, " mUserVisibleHint="

    #@d2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d5
    iget-boolean v0, p0, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@d7
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@da
    .line 1594
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@dc
    if-eqz v0, :cond_ec

    #@de
    .line 1595
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e1
    const-string/jumbo v0, "mFragmentManager="

    #@e4
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e7
    .line 1596
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@e9
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@ec
    .line 1598
    :cond_ec
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@ee
    if-eqz v0, :cond_fe

    #@f0
    .line 1599
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f3
    const-string/jumbo v0, "mActivity="

    #@f6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f9
    .line 1600
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@fb
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@fe
    .line 1602
    :cond_fe
    iget-object v0, p0, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@100
    if-eqz v0, :cond_110

    #@102
    .line 1603
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@105
    const-string/jumbo v0, "mParentFragment="

    #@108
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10b
    .line 1604
    iget-object v0, p0, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@10d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@110
    .line 1606
    :cond_110
    iget-object v0, p0, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;

    #@112
    if-eqz v0, :cond_122

    #@114
    .line 1607
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@117
    const-string/jumbo v0, "mArguments="

    #@11a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11d
    iget-object v0, p0, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;

    #@11f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@122
    .line 1609
    :cond_122
    iget-object v0, p0, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@124
    if-eqz v0, :cond_134

    #@126
    .line 1610
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@129
    const-string/jumbo v0, "mSavedFragmentState="

    #@12c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12f
    .line 1611
    iget-object v0, p0, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@131
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@134
    .line 1613
    :cond_134
    iget-object v0, p0, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@136
    if-eqz v0, :cond_146

    #@138
    .line 1614
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13b
    const-string/jumbo v0, "mSavedViewState="

    #@13e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@141
    .line 1615
    iget-object v0, p0, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@143
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@146
    .line 1617
    :cond_146
    iget-object v0, p0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@148
    if-eqz v0, :cond_162

    #@14a
    .line 1618
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14d
    const-string/jumbo v0, "mTarget="

    #@150
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@153
    iget-object v0, p0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@155
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@158
    .line 1619
    const-string v0, " mTargetRequestCode="

    #@15a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15d
    .line 1620
    iget v0, p0, Landroid/app/Fragment;->mTargetRequestCode:I

    #@15f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@162
    .line 1622
    :cond_162
    iget v0, p0, Landroid/app/Fragment;->mNextAnim:I

    #@164
    if-eqz v0, :cond_174

    #@166
    .line 1623
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@169
    const-string/jumbo v0, "mNextAnim="

    #@16c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16f
    iget v0, p0, Landroid/app/Fragment;->mNextAnim:I

    #@171
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@174
    .line 1625
    :cond_174
    iget-object v0, p0, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@176
    if-eqz v0, :cond_186

    #@178
    .line 1626
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17b
    const-string/jumbo v0, "mContainer="

    #@17e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@181
    iget-object v0, p0, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    #@183
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@186
    .line 1628
    :cond_186
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@188
    if-eqz v0, :cond_198

    #@18a
    .line 1629
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18d
    const-string/jumbo v0, "mView="

    #@190
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@193
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@195
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@198
    .line 1631
    :cond_198
    iget-object v0, p0, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@19a
    if-eqz v0, :cond_1b8

    #@19c
    .line 1632
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19f
    const-string/jumbo v0, "mAnimatingAway="

    #@1a2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a5
    iget-object v0, p0, Landroid/app/Fragment;->mAnimatingAway:Landroid/animation/Animator;

    #@1a7
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@1aa
    .line 1633
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ad
    const-string/jumbo v0, "mStateAfterAnimating="

    #@1b0
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b3
    .line 1634
    iget v0, p0, Landroid/app/Fragment;->mStateAfterAnimating:I

    #@1b5
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@1b8
    .line 1636
    :cond_1b8
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1ba
    if-eqz v0, :cond_1dc

    #@1bc
    .line 1637
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1bf
    const-string v0, "Loader Manager:"

    #@1c1
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1c4
    .line 1638
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1c6
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1cb
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v1

    #@1cf
    const-string v2, "  "

    #@1d1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v1

    #@1d5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d8
    move-result-object v1

    #@1d9
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/app/LoaderManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@1dc
    .line 1640
    :cond_1dc
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@1de
    if-eqz v0, :cond_219

    #@1e0
    .line 1641
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e3
    new-instance v0, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    const-string v1, "Child "

    #@1ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v0

    #@1ee
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@1f0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f3
    move-result-object v0

    #@1f4
    const-string v1, ":"

    #@1f6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v0

    #@1fa
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fd
    move-result-object v0

    #@1fe
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@201
    .line 1642
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@203
    new-instance v1, Ljava/lang/StringBuilder;

    #@205
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@208
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    move-result-object v1

    #@20c
    const-string v2, "  "

    #@20e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@211
    move-result-object v1

    #@212
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@215
    move-result-object v1

    #@216
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/app/FragmentManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@219
    .line 1644
    :cond_219
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 636
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 1647
    iget-object v0, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_9

    #@8
    .line 1653
    .end local p0
    :goto_8
    return-object p0

    #@9
    .line 1650
    .restart local p0
    :cond_9
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@b
    if-eqz v0, :cond_14

    #@d
    .line 1651
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@f
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;

    #@12
    move-result-object p0

    #@13
    goto :goto_8

    #@14
    .line 1653
    :cond_14
    const/4 p0, 0x0

    #@15
    goto :goto_8
.end method

.method public final getActivity()Landroid/app/Activity;
    .registers 2

    #@0
    .prologue
    .line 754
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2
    return-object v0
.end method

.method public final getArguments()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 701
    iget-object v0, p0, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public final getChildFragmentManager()Landroid/app/FragmentManager;
    .registers 3

    #@0
    .prologue
    .line 819
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 820
    invoke-virtual {p0}, Landroid/app/Fragment;->instantiateChildFragmentManager()V

    #@7
    .line 821
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@9
    const/4 v1, 0x5

    #@a
    if-lt v0, v1, :cond_14

    #@c
    .line 822
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@e
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchResume()V

    #@11
    .line 831
    :cond_11
    :goto_11
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@13
    return-object v0

    #@14
    .line 823
    :cond_14
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@16
    const/4 v1, 0x4

    #@17
    if-lt v0, v1, :cond_1f

    #@19
    .line 824
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@1b
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchStart()V

    #@1e
    goto :goto_11

    #@1f
    .line 825
    :cond_1f
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@21
    const/4 v1, 0x2

    #@22
    if-lt v0, v1, :cond_2a

    #@24
    .line 826
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@26
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchActivityCreated()V

    #@29
    goto :goto_11

    #@2a
    .line 827
    :cond_2a
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@2c
    const/4 v1, 0x1

    #@2d
    if-lt v0, v1, :cond_11

    #@2f
    .line 828
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@31
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchCreate()V

    #@34
    goto :goto_11
.end method

.method public final getFragmentManager()Landroid/app/FragmentManager;
    .registers 2

    #@0
    .prologue
    .line 811
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    return-object v0
.end method

.method public final getId()I
    .registers 2

    #@0
    .prologue
    .line 672
    iget v0, p0, Landroid/app/Fragment;->mFragmentId:I

    #@2
    return v0
.end method

.method public getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1102
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLoaderManager()Landroid/app/LoaderManager;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1011
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@3
    if-eqz v0, :cond_8

    #@5
    .line 1012
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@7
    .line 1019
    :goto_7
    return-object v0

    #@8
    .line 1014
    :cond_8
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@a
    if-nez v0, :cond_2b

    #@c
    .line 1015
    new-instance v0, Ljava/lang/IllegalStateException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Fragment "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " not attached to Activity"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 1017
    :cond_2b
    iput-boolean v3, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@2d
    .line 1018
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2f
    iget-object v1, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@31
    iget-boolean v2, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@33
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@39
    .line 1019
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@3b
    goto :goto_7
.end method

.method public final getParentFragment()Landroid/app/Fragment;
    .registers 2

    #@0
    .prologue
    .line 839
    iget-object v0, p0, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@2
    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .registers 4

    #@0
    .prologue
    .line 761
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_23

    #@4
    .line 762
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Fragment "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " not attached to Activity"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 764
    :cond_23
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@25
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    #@28
    move-result-object v0

    #@29
    return-object v0
.end method

.method public final getRetainInstance()Z
    .registers 2

    #@0
    .prologue
    .line 940
    iget-boolean v0, p0, Landroid/app/Fragment;->mRetainInstance:Z

    #@2
    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 784
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final varargs getString(I[Ljava/lang/Object;)Ljava/lang/String;
    .registers 4
    .parameter "resId"
    .parameter "formatArgs"

    #@0
    .prologue
    .line 797
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final getTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 679
    iget-object v0, p0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getTargetFragment()Landroid/app/Fragment;
    .registers 2

    #@0
    .prologue
    .line 740
    iget-object v0, p0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@2
    return-object v0
.end method

.method public final getTargetRequestCode()I
    .registers 2

    #@0
    .prologue
    .line 747
    iget v0, p0, Landroid/app/Fragment;->mTargetRequestCode:I

    #@2
    return v0
.end method

.method public final getText(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter "resId"

    #@0
    .prologue
    .line 774
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getUserVisibleHint()Z
    .registers 2

    #@0
    .prologue
    .line 1004
    iget-boolean v0, p0, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@2
    return v0
.end method

.method public getView()Landroid/view/View;
    .registers 2

    #@0
    .prologue
    .line 1237
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@2
    return-object v0
.end method

.method public final hashCode()I
    .registers 2

    #@0
    .prologue
    .line 643
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method initState()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 1391
    const/4 v0, -0x1

    #@3
    iput v0, p0, Landroid/app/Fragment;->mIndex:I

    #@5
    .line 1392
    iput-object v2, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@7
    .line 1393
    iput-boolean v1, p0, Landroid/app/Fragment;->mAdded:Z

    #@9
    .line 1394
    iput-boolean v1, p0, Landroid/app/Fragment;->mRemoving:Z

    #@b
    .line 1395
    iput-boolean v1, p0, Landroid/app/Fragment;->mResumed:Z

    #@d
    .line 1396
    iput-boolean v1, p0, Landroid/app/Fragment;->mFromLayout:Z

    #@f
    .line 1397
    iput-boolean v1, p0, Landroid/app/Fragment;->mInLayout:Z

    #@11
    .line 1398
    iput-boolean v1, p0, Landroid/app/Fragment;->mRestored:Z

    #@13
    .line 1399
    iput v1, p0, Landroid/app/Fragment;->mBackStackNesting:I

    #@15
    .line 1400
    iput-object v2, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@17
    .line 1401
    iput-object v2, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@19
    .line 1402
    iput v1, p0, Landroid/app/Fragment;->mFragmentId:I

    #@1b
    .line 1403
    iput v1, p0, Landroid/app/Fragment;->mContainerId:I

    #@1d
    .line 1404
    iput-object v2, p0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@1f
    .line 1405
    iput-boolean v1, p0, Landroid/app/Fragment;->mHidden:Z

    #@21
    .line 1406
    iput-boolean v1, p0, Landroid/app/Fragment;->mDetached:Z

    #@23
    .line 1407
    iput-boolean v1, p0, Landroid/app/Fragment;->mRetaining:Z

    #@25
    .line 1408
    iput-object v2, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@27
    .line 1409
    iput-boolean v1, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@29
    .line 1410
    iput-boolean v1, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@2b
    .line 1411
    return-void
.end method

.method instantiateChildFragmentManager()V
    .registers 4

    #@0
    .prologue
    .line 1657
    new-instance v0, Landroid/app/FragmentManagerImpl;

    #@2
    invoke-direct {v0}, Landroid/app/FragmentManagerImpl;-><init>()V

    #@5
    iput-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@7
    .line 1658
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@9
    iget-object v1, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@b
    new-instance v2, Landroid/app/Fragment$1;

    #@d
    invoke-direct {v2, p0}, Landroid/app/Fragment$1;-><init>(Landroid/app/Fragment;)V

    #@10
    invoke-virtual {v0, v1, v2, p0}, Landroid/app/FragmentManagerImpl;->attachActivity(Landroid/app/Activity;Landroid/app/FragmentContainer;Landroid/app/Fragment;)V

    #@13
    .line 1667
    return-void
.end method

.method public final isAdded()Z
    .registers 2

    #@0
    .prologue
    .line 846
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p0, Landroid/app/Fragment;->mAdded:Z

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isDetached()Z
    .registers 2

    #@0
    .prologue
    .line 855
    iget-boolean v0, p0, Landroid/app/Fragment;->mDetached:Z

    #@2
    return v0
.end method

.method public final isHidden()Z
    .registers 2

    #@0
    .prologue
    .line 904
    iget-boolean v0, p0, Landroid/app/Fragment;->mHidden:Z

    #@2
    return v0
.end method

.method final isInBackStack()Z
    .registers 2

    #@0
    .prologue
    .line 629
    iget v0, p0, Landroid/app/Fragment;->mBackStackNesting:I

    #@2
    if-lez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public final isInLayout()Z
    .registers 2

    #@0
    .prologue
    .line 875
    iget-boolean v0, p0, Landroid/app/Fragment;->mInLayout:Z

    #@2
    return v0
.end method

.method public final isRemoving()Z
    .registers 2

    #@0
    .prologue
    .line 864
    iget-boolean v0, p0, Landroid/app/Fragment;->mRemoving:Z

    #@2
    return v0
.end method

.method public final isResumed()Z
    .registers 2

    #@0
    .prologue
    .line 883
    iget-boolean v0, p0, Landroid/app/Fragment;->mResumed:Z

    #@2
    return v0
.end method

.method public final isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 892
    invoke-virtual {p0}, Landroid/app/Fragment;->isAdded()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_22

    #@6
    invoke-virtual {p0}, Landroid/app/Fragment;->isHidden()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_22

    #@c
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@e
    if-eqz v0, :cond_22

    #@10
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@12
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    #@15
    move-result-object v0

    #@16
    if-eqz v0, :cond_22

    #@18
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@1a
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_22

    #@20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1254
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1255
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    #@0
    .prologue
    .line 1094
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 3
    .parameter "activity"

    #@0
    .prologue
    .line 1165
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1166
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 1325
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1326
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 1560
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1190
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1191
    return-void
.end method

.method public onCreateAnimator(IZI)Landroid/animation/Animator;
    .registers 5
    .parameter "transit"
    .parameter "enter"
    .parameter "nextAnim"

    #@0
    .prologue
    .line 1172
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 5
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    #@0
    .prologue
    .line 1514
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    #@7
    .line 1515
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3
    .parameter "menu"
    .parameter "inflater"

    #@0
    .prologue
    .line 1435
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 5
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1214
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onDestroy()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1372
    iput-boolean v1, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1375
    iget-boolean v0, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@5
    if-nez v0, :cond_16

    #@7
    .line 1376
    iput-boolean v1, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@9
    .line 1377
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@b
    iget-object v1, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@d
    iget-boolean v2, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@f
    const/4 v3, 0x0

    #@10
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@13
    move-result-object v0

    #@14
    iput-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@16
    .line 1379
    :cond_16
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 1380
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1c
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    #@1f
    .line 1382
    :cond_1f
    return-void
.end method

.method public onDestroyOptionsMenu()V
    .registers 1

    #@0
    .prologue
    .line 1462
    return-void
.end method

.method public onDestroyView()V
    .registers 2

    #@0
    .prologue
    .line 1364
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1365
    return-void
.end method

.method public onDetach()V
    .registers 2

    #@0
    .prologue
    .line 1418
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1419
    return-void
.end method

.method public onHiddenChanged(Z)V
    .registers 2
    .parameter "hidden"

    #@0
    .prologue
    .line 915
    return-void
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .registers 5
    .parameter "activity"
    .parameter "attrs"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1156
    invoke-virtual {p0, p2, p3}, Landroid/app/Fragment;->onInflate(Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    #@3
    .line 1157
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@6
    .line 1158
    return-void
.end method

.method public onInflate(Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .registers 4
    .parameter "attrs"
    .parameter "savedInstanceState"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1110
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1111
    return-void
.end method

.method public onLowMemory()V
    .registers 2

    #@0
    .prologue
    .line 1347
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1348
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    #@0
    .prologue
    .line 1483
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 1494
    return-void
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 1334
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1335
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    #@0
    .prologue
    .line 1452
    return-void
.end method

.method public onResume()V
    .registers 2

    #@0
    .prologue
    .line 1299
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1300
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "outState"

    #@0
    .prologue
    .line 1322
    return-void
.end method

.method public onStart()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1278
    iput-boolean v1, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1280
    iget-boolean v0, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@5
    if-nez v0, :cond_25

    #@7
    .line 1281
    iput-boolean v1, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@9
    .line 1282
    iget-boolean v0, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@b
    if-nez v0, :cond_1c

    #@d
    .line 1283
    iput-boolean v1, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@f
    .line 1284
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@11
    iget-object v1, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@13
    iget-boolean v2, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@15
    const/4 v3, 0x0

    #@16
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1c
    .line 1286
    :cond_1c
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@1e
    if-eqz v0, :cond_25

    #@20
    .line 1287
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@22
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doStart()V

    #@25
    .line 1290
    :cond_25
    return-void
.end method

.method public onStop()V
    .registers 2

    #@0
    .prologue
    .line 1343
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1344
    return-void
.end method

.method public onTrimMemory(I)V
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 1351
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1352
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3
    .parameter "view"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1228
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1269
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@3
    .line 1270
    return-void
.end method

.method performActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1700
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1701
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@9
    .line 1703
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1704
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    #@f
    .line 1705
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v0, :cond_32

    #@13
    .line 1706
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Fragment "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " did not call through to super.onActivityCreated()"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1709
    :cond_32
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@34
    if-eqz v0, :cond_3b

    #@36
    .line 1710
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@38
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchActivityCreated()V

    #@3b
    .line 1712
    :cond_3b
    return-void
.end method

.method performConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    #@0
    .prologue
    .line 1751
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 1752
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 1753
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@9
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    #@c
    .line 1755
    :cond_c
    return-void
.end method

.method performContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1816
    iget-boolean v1, p0, Landroid/app/Fragment;->mHidden:Z

    #@3
    if-nez v1, :cond_18

    #@5
    .line 1817
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_c

    #@b
    .line 1826
    :cond_b
    :goto_b
    return v0

    #@c
    .line 1820
    :cond_c
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@e
    if-eqz v1, :cond_18

    #@10
    .line 1821
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@12
    invoke-virtual {v1, p1}, Landroid/app/FragmentManagerImpl;->dispatchContextItemSelected(Landroid/view/MenuItem;)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_b

    #@18
    .line 1826
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_b
.end method

.method performCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1670
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v1, :cond_9

    #@4
    .line 1671
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v1}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@9
    .line 1673
    :cond_9
    const/4 v1, 0x0

    #@a
    iput-boolean v1, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1674
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    #@f
    .line 1675
    iget-boolean v1, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v1, :cond_32

    #@13
    .line 1676
    new-instance v1, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "Fragment "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    const-string v3, " did not call through to super.onCreate()"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-direct {v1, v2}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v1

    #@32
    .line 1679
    :cond_32
    if-eqz p1, :cond_4e

    #@34
    .line 1680
    const-string v1, "android:fragments"

    #@36
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@39
    move-result-object v0

    #@3a
    .line 1681
    .local v0, p:Landroid/os/Parcelable;
    if-eqz v0, :cond_4e

    #@3c
    .line 1682
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@3e
    if-nez v1, :cond_43

    #@40
    .line 1683
    invoke-virtual {p0}, Landroid/app/Fragment;->instantiateChildFragmentManager()V

    #@43
    .line 1685
    :cond_43
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@45
    const/4 v2, 0x0

    #@46
    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentManagerImpl;->restoreAllState(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    #@49
    .line 1686
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@4b
    invoke-virtual {v1}, Landroid/app/FragmentManagerImpl;->dispatchCreate()V

    #@4e
    .line 1689
    .end local v0           #p:Landroid/os/Parcelable;
    :cond_4e
    return-void
.end method

.method performCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 5
    .parameter "menu"
    .parameter "inflater"

    #@0
    .prologue
    .line 1772
    const/4 v0, 0x0

    #@1
    .line 1773
    .local v0, show:Z
    iget-boolean v1, p0, Landroid/app/Fragment;->mHidden:Z

    #@3
    if-nez v1, :cond_1c

    #@5
    .line 1774
    iget-boolean v1, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@7
    if-eqz v1, :cond_11

    #@9
    iget-boolean v1, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 1775
    const/4 v0, 0x1

    #@e
    .line 1776
    invoke-virtual {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    #@11
    .line 1778
    :cond_11
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@13
    if-eqz v1, :cond_1c

    #@15
    .line 1779
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@17
    invoke-virtual {v1, p1, p2}, Landroid/app/FragmentManagerImpl;->dispatchCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    #@1a
    move-result v1

    #@1b
    or-int/2addr v0, v1

    #@1c
    .line 1782
    :cond_1c
    return v0
.end method

.method performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 5
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 1693
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1694
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@9
    .line 1696
    :cond_9
    invoke-virtual {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method performDestroy()V
    .registers 4

    #@0
    .prologue
    .line 1905
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1906
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchDestroy()V

    #@9
    .line 1908
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1909
    invoke-virtual {p0}, Landroid/app/Fragment;->onDestroy()V

    #@f
    .line 1910
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v0, :cond_32

    #@13
    .line 1911
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Fragment "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " did not call through to super.onDestroy()"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1914
    :cond_32
    return-void
.end method

.method performDestroyView()V
    .registers 4

    #@0
    .prologue
    .line 1890
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1891
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchDestroyView()V

    #@9
    .line 1893
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1894
    invoke-virtual {p0}, Landroid/app/Fragment;->onDestroyView()V

    #@f
    .line 1895
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v0, :cond_32

    #@13
    .line 1896
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Fragment "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " did not call through to super.onDestroyView()"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1899
    :cond_32
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@34
    if-eqz v0, :cond_3b

    #@36
    .line 1900
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@38
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doReportNextStart()V

    #@3b
    .line 1902
    :cond_3b
    return-void
.end method

.method performLowMemory()V
    .registers 2

    #@0
    .prologue
    .line 1758
    invoke-virtual {p0}, Landroid/app/Fragment;->onLowMemory()V

    #@3
    .line 1759
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 1760
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@9
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchLowMemory()V

    #@c
    .line 1762
    :cond_c
    return-void
.end method

.method performOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1800
    iget-boolean v1, p0, Landroid/app/Fragment;->mHidden:Z

    #@3
    if-nez v1, :cond_20

    #@5
    .line 1801
    iget-boolean v1, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@7
    if-eqz v1, :cond_14

    #@9
    iget-boolean v1, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 1802
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_14

    #@13
    .line 1812
    :cond_13
    :goto_13
    return v0

    #@14
    .line 1806
    :cond_14
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@16
    if-eqz v1, :cond_20

    #@18
    .line 1807
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@1a
    invoke-virtual {v1, p1}, Landroid/app/FragmentManagerImpl;->dispatchOptionsItemSelected(Landroid/view/MenuItem;)Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_13

    #@20
    .line 1812
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_13
.end method

.method performOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 3
    .parameter "menu"

    #@0
    .prologue
    .line 1830
    iget-boolean v0, p0, Landroid/app/Fragment;->mHidden:Z

    #@2
    if-nez v0, :cond_18

    #@4
    .line 1831
    iget-boolean v0, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-boolean v0, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 1832
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onOptionsMenuClosed(Landroid/view/Menu;)V

    #@f
    .line 1834
    :cond_f
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@11
    if-eqz v0, :cond_18

    #@13
    .line 1835
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@15
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->dispatchOptionsMenuClosed(Landroid/view/Menu;)V

    #@18
    .line 1838
    :cond_18
    return-void
.end method

.method performPause()V
    .registers 4

    #@0
    .prologue
    .line 1851
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1852
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchPause()V

    #@9
    .line 1854
    :cond_9
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1855
    invoke-virtual {p0}, Landroid/app/Fragment;->onPause()V

    #@f
    .line 1856
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v0, :cond_32

    #@13
    .line 1857
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Fragment "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " did not call through to super.onPause()"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1860
    :cond_32
    return-void
.end method

.method performPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    #@0
    .prologue
    .line 1786
    const/4 v0, 0x0

    #@1
    .line 1787
    .local v0, show:Z
    iget-boolean v1, p0, Landroid/app/Fragment;->mHidden:Z

    #@3
    if-nez v1, :cond_1c

    #@5
    .line 1788
    iget-boolean v1, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@7
    if-eqz v1, :cond_11

    #@9
    iget-boolean v1, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@b
    if-eqz v1, :cond_11

    #@d
    .line 1789
    const/4 v0, 0x1

    #@e
    .line 1790
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    #@11
    .line 1792
    :cond_11
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@13
    if-eqz v1, :cond_1c

    #@15
    .line 1793
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@17
    invoke-virtual {v1, p1}, Landroid/app/FragmentManagerImpl;->dispatchPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@1a
    move-result v1

    #@1b
    or-int/2addr v0, v1

    #@1c
    .line 1796
    :cond_1c
    return v0
.end method

.method performResume()V
    .registers 4

    #@0
    .prologue
    .line 1734
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 1735
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@9
    .line 1736
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@b
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@e
    .line 1738
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    .line 1739
    invoke-virtual {p0}, Landroid/app/Fragment;->onResume()V

    #@14
    .line 1740
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@16
    if-nez v0, :cond_37

    #@18
    .line 1741
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Fragment "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, " did not call through to super.onResume()"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@36
    throw v0

    #@37
    .line 1744
    :cond_37
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@39
    if-eqz v0, :cond_45

    #@3b
    .line 1745
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@3d
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchResume()V

    #@40
    .line 1746
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@42
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@45
    .line 1748
    :cond_45
    return-void
.end method

.method performSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    #@0
    .prologue
    .line 1841
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 1842
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@5
    if-eqz v1, :cond_14

    #@7
    .line 1843
    iget-object v1, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@9
    invoke-virtual {v1}, Landroid/app/FragmentManagerImpl;->saveAllState()Landroid/os/Parcelable;

    #@c
    move-result-object v0

    #@d
    .line 1844
    .local v0, p:Landroid/os/Parcelable;
    if-eqz v0, :cond_14

    #@f
    .line 1845
    const-string v1, "android:fragments"

    #@11
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@14
    .line 1848
    .end local v0           #p:Landroid/os/Parcelable;
    :cond_14
    return-void
.end method

.method performStart()V
    .registers 4

    #@0
    .prologue
    .line 1715
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 1716
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@6
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->noteStateNotSaved()V

    #@9
    .line 1717
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@b
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    #@e
    .line 1719
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    .line 1720
    invoke-virtual {p0}, Landroid/app/Fragment;->onStart()V

    #@14
    .line 1721
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@16
    if-nez v0, :cond_37

    #@18
    .line 1722
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Fragment "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, " did not call through to super.onStart()"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@36
    throw v0

    #@37
    .line 1725
    :cond_37
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@39
    if-eqz v0, :cond_40

    #@3b
    .line 1726
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@3d
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchStart()V

    #@40
    .line 1728
    :cond_40
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@42
    if-eqz v0, :cond_49

    #@44
    .line 1729
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@46
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doReportStart()V

    #@49
    .line 1731
    :cond_49
    return-void
.end method

.method performStop()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1863
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 1864
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@7
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->dispatchStop()V

    #@a
    .line 1866
    :cond_a
    iput-boolean v3, p0, Landroid/app/Fragment;->mCalled:Z

    #@c
    .line 1867
    invoke-virtual {p0}, Landroid/app/Fragment;->onStop()V

    #@f
    .line 1868
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    if-nez v0, :cond_32

    #@13
    .line 1869
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "Fragment "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, " did not call through to super.onStop()"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1873
    :cond_32
    iget-boolean v0, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@34
    if-eqz v0, :cond_5e

    #@36
    .line 1874
    iput-boolean v3, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@38
    .line 1875
    iget-boolean v0, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@3a
    if-nez v0, :cond_4b

    #@3c
    .line 1876
    const/4 v0, 0x1

    #@3d
    iput-boolean v0, p0, Landroid/app/Fragment;->mCheckedForLoaderManager:Z

    #@3f
    .line 1877
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@41
    iget-object v1, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@43
    iget-boolean v2, p0, Landroid/app/Fragment;->mLoadersStarted:Z

    #@45
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    #@48
    move-result-object v0

    #@49
    iput-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@4b
    .line 1879
    :cond_4b
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@4d
    if-eqz v0, :cond_5e

    #@4f
    .line 1880
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@51
    if-eqz v0, :cond_59

    #@53
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@55
    iget-boolean v0, v0, Landroid/app/Activity;->mChangingConfigurations:Z

    #@57
    if-nez v0, :cond_5f

    #@59
    .line 1881
    :cond_59
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@5b
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doStop()V

    #@5e
    .line 1887
    :cond_5e
    :goto_5e
    return-void

    #@5f
    .line 1883
    :cond_5f
    iget-object v0, p0, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    #@61
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doRetain()V

    #@64
    goto :goto_5e
.end method

.method performTrimMemory(I)V
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 1765
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onTrimMemory(I)V

    #@3
    .line 1766
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 1767
    iget-object v0, p0, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    #@9
    invoke-virtual {v0, p1}, Landroid/app/FragmentManagerImpl;->dispatchTrimMemory(I)V

    #@c
    .line 1769
    :cond_c
    return-void
.end method

.method public registerForContextMenu(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    #@0
    .prologue
    .line 1528
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@3
    .line 1529
    return-void
.end method

.method final restoreViewState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 607
    iget-object v0, p0, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 608
    iget-object v0, p0, Landroid/app/Fragment;->mView:Landroid/view/View;

    #@6
    iget-object v1, p0, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@8
    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    #@b
    .line 609
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    #@e
    .line 611
    :cond_e
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@11
    .line 612
    invoke-virtual {p0, p1}, Landroid/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    #@14
    .line 613
    iget-boolean v0, p0, Landroid/app/Fragment;->mCalled:Z

    #@16
    if-nez v0, :cond_37

    #@18
    .line 614
    new-instance v0, Landroid/app/SuperNotCalledException;

    #@1a
    new-instance v1, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v2, "Fragment "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, " did not call through to super.onViewStateRestored()"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-direct {v0, v1}, Landroid/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    #@36
    throw v0

    #@37
    .line 617
    :cond_37
    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .registers 4
    .parameter "args"

    #@0
    .prologue
    .line 690
    iget v0, p0, Landroid/app/Fragment;->mIndex:I

    #@2
    if-ltz v0, :cond_c

    #@4
    .line 691
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Fragment already active"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 693
    :cond_c
    iput-object p1, p0, Landroid/app/Fragment;->mArguments:Landroid/os/Bundle;

    #@e
    .line 694
    return-void
.end method

.method public setHasOptionsMenu(Z)V
    .registers 3
    .parameter "hasMenu"

    #@0
    .prologue
    .line 951
    iget-boolean v0, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@2
    if-eq v0, p1, :cond_17

    #@4
    .line 952
    iput-boolean p1, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@6
    .line 953
    invoke-virtual {p0}, Landroid/app/Fragment;->isAdded()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_17

    #@c
    invoke-virtual {p0}, Landroid/app/Fragment;->isHidden()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_17

    #@12
    .line 954
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@14
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->invalidateOptionsMenu()V

    #@17
    .line 957
    :cond_17
    return-void
.end method

.method final setIndex(ILandroid/app/Fragment;)V
    .registers 5
    .parameter "index"
    .parameter "parent"

    #@0
    .prologue
    .line 620
    iput p1, p0, Landroid/app/Fragment;->mIndex:I

    #@2
    .line 621
    if-eqz p2, :cond_22

    #@4
    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    iget-object v1, p2, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ":"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    iget v1, p0, Landroid/app/Fragment;->mIndex:I

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@21
    .line 626
    :goto_21
    return-void

    #@22
    .line 624
    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v1, "android:fragment:"

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget v1, p0, Landroid/app/Fragment;->mIndex:I

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    iput-object v0, p0, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    #@39
    goto :goto_21
.end method

.method public setInitialSavedState(Landroid/app/Fragment$SavedState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 713
    iget v0, p0, Landroid/app/Fragment;->mIndex:I

    #@2
    if-ltz v0, :cond_c

    #@4
    .line 714
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Fragment already active"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 716
    :cond_c
    if-eqz p1, :cond_17

    #@e
    iget-object v0, p1, Landroid/app/Fragment$SavedState;->mState:Landroid/os/Bundle;

    #@10
    if-eqz v0, :cond_17

    #@12
    iget-object v0, p1, Landroid/app/Fragment$SavedState;->mState:Landroid/os/Bundle;

    #@14
    :goto_14
    iput-object v0, p0, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    #@16
    .line 718
    return-void

    #@17
    .line 716
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_14
.end method

.method public setMenuVisibility(Z)V
    .registers 3
    .parameter "menuVisible"

    #@0
    .prologue
    .line 969
    iget-boolean v0, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@2
    if-eq v0, p1, :cond_1b

    #@4
    .line 970
    iput-boolean p1, p0, Landroid/app/Fragment;->mMenuVisible:Z

    #@6
    .line 971
    iget-boolean v0, p0, Landroid/app/Fragment;->mHasMenu:Z

    #@8
    if-eqz v0, :cond_1b

    #@a
    invoke-virtual {p0}, Landroid/app/Fragment;->isAdded()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_1b

    #@10
    invoke-virtual {p0}, Landroid/app/Fragment;->isHidden()Z

    #@13
    move-result v0

    #@14
    if-nez v0, :cond_1b

    #@16
    .line 972
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@18
    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->invalidateOptionsMenu()V

    #@1b
    .line 975
    :cond_1b
    return-void
.end method

.method public setRetainInstance(Z)V
    .registers 4
    .parameter "retain"

    #@0
    .prologue
    .line 932
    if-eqz p1, :cond_e

    #@2
    iget-object v0, p0, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 933
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "Can\'t retain fragements that are nested in other fragments"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 936
    :cond_e
    iput-boolean p1, p0, Landroid/app/Fragment;->mRetainInstance:Z

    #@10
    .line 937
    return-void
.end method

.method public setTargetFragment(Landroid/app/Fragment;I)V
    .registers 3
    .parameter "fragment"
    .parameter "requestCode"

    #@0
    .prologue
    .line 732
    iput-object p1, p0, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    #@2
    .line 733
    iput p2, p0, Landroid/app/Fragment;->mTargetRequestCode:I

    #@4
    .line 734
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .registers 4
    .parameter "isVisibleToUser"

    #@0
    .prologue
    .line 991
    iget-boolean v0, p0, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@2
    if-nez v0, :cond_14

    #@4
    if-eqz p1, :cond_14

    #@6
    iget v0, p0, Landroid/app/Fragment;->mState:I

    #@8
    const/4 v1, 0x4

    #@9
    if-ge v0, v1, :cond_14

    #@b
    .line 992
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 993
    iget-object v0, p0, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    #@11
    invoke-virtual {v0, p0}, Landroid/app/FragmentManagerImpl;->performPendingDeferredStart(Landroid/app/Fragment;)V

    #@14
    .line 995
    :cond_14
    iput-boolean p1, p0, Landroid/app/Fragment;->mUserVisibleHint:Z

    #@16
    .line 996
    if-nez p1, :cond_1c

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    iput-boolean v0, p0, Landroid/app/Fragment;->mDeferStart:Z

    #@1b
    .line 997
    return-void

    #@1c
    .line 996
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_19
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 1029
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/app/Fragment;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    #@4
    .line 1030
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 6
    .parameter "intent"
    .parameter "options"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 1042
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@3
    if-nez v0, :cond_24

    #@5
    .line 1043
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Fragment "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, " not attached to Activity"

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 1045
    :cond_24
    if-eqz p2, :cond_2c

    #@26
    .line 1046
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@28
    invoke-virtual {v0, p0, p1, v1, p2}, Landroid/app/Activity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@2b
    .line 1052
    :goto_2b
    return-void

    #@2c
    .line 1050
    :cond_2c
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2e
    invoke-virtual {v0, p0, p1, v1}, Landroid/app/Activity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V

    #@31
    goto :goto_2b
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .registers 4
    .parameter "intent"
    .parameter "requestCode"

    #@0
    .prologue
    .line 1059
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@4
    .line 1060
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .registers 7
    .parameter "intent"
    .parameter "requestCode"
    .parameter "options"

    #@0
    .prologue
    .line 1067
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2
    if-nez v0, :cond_23

    #@4
    .line 1068
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Fragment "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " not attached to Activity"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 1070
    :cond_23
    if-eqz p3, :cond_2b

    #@25
    .line 1071
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@27
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/app/Activity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@2a
    .line 1077
    :goto_2a
    return-void

    #@2b
    .line 1075
    :cond_2b
    iget-object v0, p0, Landroid/app/Fragment;->mActivity:Landroid/app/Activity;

    #@2d
    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/app/Activity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    #@30
    goto :goto_2a
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 648
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x80

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 649
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-static {p0, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    #@a
    .line 650
    iget v1, p0, Landroid/app/Fragment;->mIndex:I

    #@c
    if-ltz v1, :cond_18

    #@e
    .line 651
    const-string v1, " #"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 652
    iget v1, p0, Landroid/app/Fragment;->mIndex:I

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    .line 654
    :cond_18
    iget v1, p0, Landroid/app/Fragment;->mFragmentId:I

    #@1a
    if-eqz v1, :cond_2a

    #@1c
    .line 655
    const-string v1, " id=0x"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 656
    iget v1, p0, Landroid/app/Fragment;->mFragmentId:I

    #@23
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 658
    :cond_2a
    iget-object v1, p0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@2c
    if-eqz v1, :cond_38

    #@2e
    .line 659
    const-string v1, " "

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 660
    iget-object v1, p0, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 662
    :cond_38
    const/16 v1, 0x7d

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3d
    .line 663
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    return-object v1
.end method

.method public unregisterForContextMenu(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 1539
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@4
    .line 1540
    return-void
.end method
