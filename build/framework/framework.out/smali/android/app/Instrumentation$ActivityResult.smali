.class public final Landroid/app/Instrumentation$ActivityResult;
.super Ljava/lang/Object;
.source "Instrumentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Instrumentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActivityResult"
.end annotation


# instance fields
.field private final mResultCode:I

.field private final mResultData:Landroid/content/Intent;


# direct methods
.method public constructor <init>(ILandroid/content/Intent;)V
    .registers 3
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    .line 1323
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1324
    iput p1, p0, Landroid/app/Instrumentation$ActivityResult;->mResultCode:I

    #@5
    .line 1325
    iput-object p2, p0, Landroid/app/Instrumentation$ActivityResult;->mResultData:Landroid/content/Intent;

    #@7
    .line 1326
    return-void
.end method


# virtual methods
.method public getResultCode()I
    .registers 2

    #@0
    .prologue
    .line 1332
    iget v0, p0, Landroid/app/Instrumentation$ActivityResult;->mResultCode:I

    #@2
    return v0
.end method

.method public getResultData()Landroid/content/Intent;
    .registers 2

    #@0
    .prologue
    .line 1339
    iget-object v0, p0, Landroid/app/Instrumentation$ActivityResult;->mResultData:Landroid/content/Intent;

    #@2
    return-object v0
.end method
