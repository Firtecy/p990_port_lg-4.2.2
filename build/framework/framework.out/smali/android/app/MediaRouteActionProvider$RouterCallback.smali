.class Landroid/app/MediaRouteActionProvider$RouterCallback;
.super Landroid/media/MediaRouter$SimpleCallback;
.source "MediaRouteActionProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/MediaRouteActionProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RouterCallback"
.end annotation


# instance fields
.field private mAp:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/MediaRouteActionProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/MediaRouteActionProvider;)V
    .registers 3
    .parameter "ap"

    #@0
    .prologue
    .line 145
    invoke-direct {p0}, Landroid/media/MediaRouter$SimpleCallback;-><init>()V

    #@3
    .line 146
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/app/MediaRouteActionProvider$RouterCallback;->mAp:Ljava/lang/ref/WeakReference;

    #@a
    .line 147
    return-void
.end method


# virtual methods
.method public onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 151
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider$RouterCallback;->mAp:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/MediaRouteActionProvider;

    #@8
    .line 152
    .local v0, ap:Landroid/app/MediaRouteActionProvider;
    if-nez v0, :cond_e

    #@a
    .line 153
    invoke-virtual {p1, p0}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@d
    .line 158
    :goto_d
    return-void

    #@e
    .line 157
    :cond_e
    invoke-virtual {v0}, Landroid/app/MediaRouteActionProvider;->refreshVisibility()V

    #@11
    goto :goto_d
.end method

.method public onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .registers 5
    .parameter "router"
    .parameter "info"

    #@0
    .prologue
    .line 162
    iget-object v1, p0, Landroid/app/MediaRouteActionProvider$RouterCallback;->mAp:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/MediaRouteActionProvider;

    #@8
    .line 163
    .local v0, ap:Landroid/app/MediaRouteActionProvider;
    if-nez v0, :cond_e

    #@a
    .line 164
    invoke-virtual {p1, p0}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    #@d
    .line 169
    :goto_d
    return-void

    #@e
    .line 168
    :cond_e
    invoke-virtual {v0}, Landroid/app/MediaRouteActionProvider;->refreshVisibility()V

    #@11
    goto :goto_d
.end method
