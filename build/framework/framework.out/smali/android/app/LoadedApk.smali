.class public final Landroid/app/LoadedApk;
.super Ljava/lang/Object;
.source "LoadedApk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/LoadedApk$1;,
        Landroid/app/LoadedApk$ServiceDispatcher;,
        Landroid/app/LoadedApk$ReceiverDispatcher;,
        Landroid/app/LoadedApk$WarningContextClassLoader;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LoadedApk"


# instance fields
.field private final mActivityThread:Landroid/app/ActivityThread;

.field private final mAppDir:Ljava/lang/String;

.field private mApplication:Landroid/app/Application;

.field private final mApplicationInfo:Landroid/content/pm/ApplicationInfo;

.field private final mBaseClassLoader:Ljava/lang/ClassLoader;

.field private mClassLoader:Ljava/lang/ClassLoader;

.field mClientCount:I

.field public final mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

.field private final mDataDir:Ljava/lang/String;

.field private final mDataDirFile:Ljava/io/File;

.field private final mIncludeCode:Z

.field private final mLibDir:Ljava/lang/String;

.field final mPackageName:Ljava/lang/String;

.field private final mReceivers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/BroadcastReceiver;",
            "Landroid/app/LoadedApk$ReceiverDispatcher;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mResDir:Ljava/lang/String;

.field mResources:Landroid/content/res/Resources;

.field private final mSecurityViolation:Z

.field private final mServices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ServiceConnection;",
            "Landroid/app/LoadedApk$ServiceDispatcher;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSharedLibraries:[Ljava/lang/String;

.field private final mUnboundServices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ServiceConnection;",
            "Landroid/app/LoadedApk$ServiceDispatcher;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mUnregisteredReceivers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/BroadcastReceiver;",
            "Landroid/app/LoadedApk$ReceiverDispatcher;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Landroid/app/ActivityThread;Ljava/lang/ClassLoader;ZZ)V
    .registers 12
    .parameter "activityThread"
    .parameter "aInfo"
    .parameter "compatInfo"
    .parameter "mainThread"
    .parameter "baseLoader"
    .parameter "securityViolation"
    .parameter "includeCode"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 116
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 87
    new-instance v1, Landroid/view/CompatibilityInfoHolder;

    #@6
    invoke-direct {v1}, Landroid/view/CompatibilityInfoHolder;-><init>()V

    #@9
    iput-object v1, p0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@b
    .line 92
    new-instance v1, Ljava/util/HashMap;

    #@d
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v1, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@12
    .line 94
    new-instance v1, Ljava/util/HashMap;

    #@14
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v1, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@19
    .line 96
    new-instance v1, Ljava/util/HashMap;

    #@1b
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@1e
    iput-object v1, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@20
    .line 98
    new-instance v1, Ljava/util/HashMap;

    #@22
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v1, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@27
    .line 101
    iput v3, p0, Landroid/app/LoadedApk;->mClientCount:I

    #@29
    .line 117
    iput-object p1, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@2b
    .line 118
    iput-object p2, p0, Landroid/app/LoadedApk;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2d
    .line 119
    iget-object v1, p2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@2f
    iput-object v1, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@31
    .line 120
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@33
    iput-object v1, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@35
    .line 121
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@38
    move-result v0

    #@39
    .line 122
    .local v0, myUid:I
    iget v1, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@3b
    if-ne v1, v0, :cond_af

    #@3d
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    #@3f
    :goto_3f
    iput-object v1, p0, Landroid/app/LoadedApk;->mResDir:Ljava/lang/String;

    #@41
    .line 124
    iget v1, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    #@43
    invoke-static {v1, v0}, Landroid/os/UserHandle;->isSameUser(II)Z

    #@46
    move-result v1

    #@47
    if-nez v1, :cond_5b

    #@49
    invoke-static {}, Landroid/os/Process;->isIsolated()Z

    #@4c
    move-result v1

    #@4d
    if-nez v1, :cond_5b

    #@4f
    .line 125
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@52
    move-result v1

    #@53
    iget-object v2, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@55
    invoke-static {v1, v2}, Landroid/content/pm/PackageManager;->getDataDirForUser(ILjava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    iput-object v1, p2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@5b
    .line 128
    :cond_5b
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@5d
    iput-object v1, p0, Landroid/app/LoadedApk;->mSharedLibraries:[Ljava/lang/String;

    #@5f
    .line 129
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    #@61
    iput-object v1, p0, Landroid/app/LoadedApk;->mDataDir:Ljava/lang/String;

    #@63
    .line 130
    iget-object v1, p0, Landroid/app/LoadedApk;->mDataDir:Ljava/lang/String;

    #@65
    if-eqz v1, :cond_b2

    #@67
    new-instance v1, Ljava/io/File;

    #@69
    iget-object v2, p0, Landroid/app/LoadedApk;->mDataDir:Ljava/lang/String;

    #@6b
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6e
    :goto_6e
    iput-object v1, p0, Landroid/app/LoadedApk;->mDataDirFile:Ljava/io/File;

    #@70
    .line 131
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    #@72
    iput-object v1, p0, Landroid/app/LoadedApk;->mLibDir:Ljava/lang/String;

    #@74
    .line 132
    iput-object p5, p0, Landroid/app/LoadedApk;->mBaseClassLoader:Ljava/lang/ClassLoader;

    #@76
    .line 133
    iput-boolean p6, p0, Landroid/app/LoadedApk;->mSecurityViolation:Z

    #@78
    .line 134
    iput-boolean p7, p0, Landroid/app/LoadedApk;->mIncludeCode:Z

    #@7a
    .line 135
    iget-object v1, p0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@7c
    invoke-virtual {v1, p3}, Landroid/view/CompatibilityInfoHolder;->set(Landroid/content/res/CompatibilityInfo;)V

    #@7f
    .line 137
    iget-object v1, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@81
    if-nez v1, :cond_ae

    #@83
    .line 138
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@85
    if-nez v1, :cond_9e

    #@87
    .line 139
    invoke-static {p4}, Landroid/app/ContextImpl;->createSystemContext(Landroid/app/ActivityThread;)Landroid/app/ContextImpl;

    #@8a
    move-result-object v1

    #@8b
    sput-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@8d
    .line 141
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@8f
    invoke-virtual {v1}, Landroid/app/ContextImpl;->getResources()Landroid/content/res/Resources;

    #@92
    move-result-object v1

    #@93
    invoke-virtual {p4}, Landroid/app/ActivityThread;->getConfiguration()Landroid/content/res/Configuration;

    #@96
    move-result-object v2

    #@97
    invoke-virtual {p4, v3, p3}, Landroid/app/ActivityThread;->getDisplayMetricsLocked(ILandroid/content/res/CompatibilityInfo;)Landroid/util/DisplayMetrics;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {v1, v2, v3, p3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V

    #@9e
    .line 150
    :cond_9e
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@a0
    invoke-virtual {v1}, Landroid/app/ContextImpl;->getClassLoader()Ljava/lang/ClassLoader;

    #@a3
    move-result-object v1

    #@a4
    iput-object v1, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@a6
    .line 151
    sget-object v1, Landroid/app/ActivityThread;->mSystemContext:Landroid/app/ContextImpl;

    #@a8
    invoke-virtual {v1}, Landroid/app/ContextImpl;->getResources()Landroid/content/res/Resources;

    #@ab
    move-result-object v1

    #@ac
    iput-object v1, p0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@ae
    .line 153
    :cond_ae
    return-void

    #@af
    .line 122
    :cond_af
    iget-object v1, p2, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    #@b1
    goto :goto_3f

    #@b2
    .line 130
    :cond_b2
    const/4 v1, 0x0

    #@b3
    goto :goto_6e
.end method

.method public constructor <init>(Landroid/app/ActivityThread;Ljava/lang/String;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)V
    .registers 9
    .parameter "activityThread"
    .parameter "name"
    .parameter "systemContext"
    .parameter "info"
    .parameter "compatInfo"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 156
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 87
    new-instance v0, Landroid/view/CompatibilityInfoHolder;

    #@7
    invoke-direct {v0}, Landroid/view/CompatibilityInfoHolder;-><init>()V

    #@a
    iput-object v0, p0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@c
    .line 92
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    iput-object v0, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@13
    .line 94
    new-instance v0, Ljava/util/HashMap;

    #@15
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v0, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@1a
    .line 96
    new-instance v0, Ljava/util/HashMap;

    #@1c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1f
    iput-object v0, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@21
    .line 98
    new-instance v0, Ljava/util/HashMap;

    #@23
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@26
    iput-object v0, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@28
    .line 101
    iput v2, p0, Landroid/app/LoadedApk;->mClientCount:I

    #@2a
    .line 157
    iput-object p1, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@2c
    .line 158
    if-eqz p4, :cond_5b

    #@2e
    .end local p4
    :goto_2e
    iput-object p4, p0, Landroid/app/LoadedApk;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    #@30
    .line 159
    iget-object v0, p0, Landroid/app/LoadedApk;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    #@32
    iput-object p2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@34
    .line 160
    iput-object p2, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@36
    .line 161
    iput-object v1, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@38
    .line 162
    iput-object v1, p0, Landroid/app/LoadedApk;->mResDir:Ljava/lang/String;

    #@3a
    .line 163
    iput-object v1, p0, Landroid/app/LoadedApk;->mSharedLibraries:[Ljava/lang/String;

    #@3c
    .line 164
    iput-object v1, p0, Landroid/app/LoadedApk;->mDataDir:Ljava/lang/String;

    #@3e
    .line 165
    iput-object v1, p0, Landroid/app/LoadedApk;->mDataDirFile:Ljava/io/File;

    #@40
    .line 166
    iput-object v1, p0, Landroid/app/LoadedApk;->mLibDir:Ljava/lang/String;

    #@42
    .line 167
    iput-object v1, p0, Landroid/app/LoadedApk;->mBaseClassLoader:Ljava/lang/ClassLoader;

    #@44
    .line 168
    iput-boolean v2, p0, Landroid/app/LoadedApk;->mSecurityViolation:Z

    #@46
    .line 169
    const/4 v0, 0x1

    #@47
    iput-boolean v0, p0, Landroid/app/LoadedApk;->mIncludeCode:Z

    #@49
    .line 170
    invoke-virtual {p3}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    #@4c
    move-result-object v0

    #@4d
    iput-object v0, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@4f
    .line 171
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@52
    move-result-object v0

    #@53
    iput-object v0, p0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@55
    .line 172
    iget-object v0, p0, Landroid/app/LoadedApk;->mCompatibilityInfo:Landroid/view/CompatibilityInfoHolder;

    #@57
    invoke-virtual {v0, p5}, Landroid/view/CompatibilityInfoHolder;->set(Landroid/content/res/CompatibilityInfo;)V

    #@5a
    .line 173
    return-void

    #@5b
    .line 158
    .restart local p4
    :cond_5b
    new-instance p4, Landroid/content/pm/ApplicationInfo;

    #@5d
    .end local p4
    invoke-direct {p4}, Landroid/content/pm/ApplicationInfo;-><init>()V

    #@60
    goto :goto_2e
.end method

.method private static combineLibs([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "list1"
    .parameter "list2"

    #@0
    .prologue
    const/16 v8, 0x3a

    #@2
    .line 224
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    const/16 v7, 0x12c

    #@6
    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    .line 225
    .local v5, result:Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    #@a
    .line 227
    .local v2, first:Z
    if-eqz p0, :cond_20

    #@c
    .line 228
    move-object v0, p0

    #@d
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v4, :cond_20

    #@11
    aget-object v6, v0, v3

    #@13
    .line 229
    .local v6, s:Ljava/lang/String;
    if-eqz v2, :cond_1c

    #@15
    .line 230
    const/4 v2, 0x0

    #@16
    .line 234
    :goto_16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 228
    add-int/lit8 v3, v3, 0x1

    #@1b
    goto :goto_f

    #@1c
    .line 232
    :cond_1c
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    goto :goto_16

    #@20
    .line 239
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #s:Ljava/lang/String;
    :cond_20
    if-nez v2, :cond_37

    #@22
    const/4 v1, 0x1

    #@23
    .line 241
    .local v1, dupCheck:Z
    :goto_23
    if-eqz p1, :cond_44

    #@25
    .line 242
    move-object v0, p1

    #@26
    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v4, v0

    #@27
    .restart local v4       #len$:I
    const/4 v3, 0x0

    #@28
    .restart local v3       #i$:I
    :goto_28
    if-ge v3, v4, :cond_44

    #@2a
    aget-object v6, v0, v3

    #@2c
    .line 243
    .restart local v6       #s:Ljava/lang/String;
    if-eqz v1, :cond_39

    #@2e
    invoke-static {p0, v6}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    #@31
    move-result v7

    #@32
    if-eqz v7, :cond_39

    #@34
    .line 242
    :goto_34
    add-int/lit8 v3, v3, 0x1

    #@36
    goto :goto_28

    #@37
    .line 239
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #dupCheck:Z
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #s:Ljava/lang/String;
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_23

    #@39
    .line 247
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #dupCheck:Z
    .restart local v3       #i$:I
    .restart local v4       #len$:I
    .restart local v6       #s:Ljava/lang/String;
    :cond_39
    if-eqz v2, :cond_40

    #@3b
    .line 248
    const/4 v2, 0x0

    #@3c
    .line 252
    :goto_3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    goto :goto_34

    #@40
    .line 250
    :cond_40
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@43
    goto :goto_3c

    #@44
    .line 256
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v6           #s:Ljava/lang/String;
    :cond_44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v7

    #@48
    return-object v7
.end method

.method private static getLibrariesFor(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 197
    const/4 v0, 0x0

    #@1
    .line 199
    .local v0, ai:Landroid/content/pm/ApplicationInfo;
    :try_start_1
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4
    move-result-object v2

    #@5
    const/16 v3, 0x400

    #@7
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@a
    move-result v4

    #@b
    invoke-interface {v2, p0, v3, v4}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_e} :catch_13

    #@e
    move-result-object v0

    #@f
    .line 205
    if-nez v0, :cond_1a

    #@11
    .line 206
    const/4 v2, 0x0

    #@12
    .line 209
    :goto_12
    return-object v2

    #@13
    .line 201
    :catch_13
    move-exception v1

    #@14
    .line 202
    .local v1, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/AssertionError;

    #@16
    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@19
    throw v2

    #@1a
    .line 209
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_1a
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->sharedLibraryFiles:[Ljava/lang/String;

    #@1c
    goto :goto_12
.end method

.method private initializeJavaContextClassLoader()V
    .registers 13

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 360
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@5
    move-result-object v3

    #@6
    .line 363
    .local v3, pm:Landroid/content/pm/IPackageManager;
    :try_start_6
    iget-object v9, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@8
    const/4 v10, 0x0

    #@9
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@c
    move-result v11

    #@d
    invoke-interface {v3, v9, v10, v11}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_10} :catch_3c

    #@10
    move-result-object v2

    #@11
    .line 379
    .local v2, pi:Landroid/content/pm/PackageInfo;
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    #@13
    if-eqz v9, :cond_43

    #@15
    move v6, v7

    #@16
    .line 380
    .local v6, sharedUserIdSet:Z
    :goto_16
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@18
    if-eqz v9, :cond_45

    #@1a
    iget-object v9, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@1c
    iget-object v10, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@1e
    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    #@20
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v9

    #@24
    if-nez v9, :cond_45

    #@26
    move v4, v7

    #@27
    .line 383
    .local v4, processNameNotDefault:Z
    :goto_27
    if-nez v6, :cond_2b

    #@29
    if-eqz v4, :cond_47

    #@2b
    :cond_2b
    move v5, v7

    #@2c
    .line 384
    .local v5, sharable:Z
    :goto_2c
    if-eqz v5, :cond_49

    #@2e
    new-instance v0, Landroid/app/LoadedApk$WarningContextClassLoader;

    #@30
    const/4 v7, 0x0

    #@31
    invoke-direct {v0, v7}, Landroid/app/LoadedApk$WarningContextClassLoader;-><init>(Landroid/app/LoadedApk$1;)V

    #@34
    .line 388
    .local v0, contextClassLoader:Ljava/lang/ClassLoader;
    :goto_34
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v7, v0}, Ljava/lang/Thread;->setContextClassLoader(Ljava/lang/ClassLoader;)V

    #@3b
    .line 389
    return-void

    #@3c
    .line 364
    .end local v0           #contextClassLoader:Ljava/lang/ClassLoader;
    .end local v2           #pi:Landroid/content/pm/PackageInfo;
    .end local v4           #processNameNotDefault:Z
    .end local v5           #sharable:Z
    .end local v6           #sharedUserIdSet:Z
    :catch_3c
    move-exception v1

    #@3d
    .line 365
    .local v1, e:Landroid/os/RemoteException;
    new-instance v7, Ljava/lang/AssertionError;

    #@3f
    invoke-direct {v7, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    #@42
    throw v7

    #@43
    .end local v1           #e:Landroid/os/RemoteException;
    .restart local v2       #pi:Landroid/content/pm/PackageInfo;
    :cond_43
    move v6, v8

    #@44
    .line 379
    goto :goto_16

    #@45
    .restart local v6       #sharedUserIdSet:Z
    :cond_45
    move v4, v8

    #@46
    .line 380
    goto :goto_27

    #@47
    .restart local v4       #processNameNotDefault:Z
    :cond_47
    move v5, v8

    #@48
    .line 383
    goto :goto_2c

    #@49
    .line 384
    .restart local v5       #sharable:Z
    :cond_49
    iget-object v0, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@4b
    goto :goto_34
.end method


# virtual methods
.method public forgetReceiverDispatcher(Landroid/content/Context;Landroid/content/BroadcastReceiver;)Landroid/content/IIntentReceiver;
    .registers 11
    .parameter "context"
    .parameter "r"

    #@0
    .prologue
    .line 615
    iget-object v5, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@2
    monitor-enter v5

    #@3
    .line 616
    :try_start_3
    iget-object v4, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@5
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Ljava/util/HashMap;

    #@b
    .line 617
    .local v2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    const/4 v3, 0x0

    #@c
    .line 618
    .local v3, rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    if-eqz v2, :cond_57

    #@e
    .line 619
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    .end local v3           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    check-cast v3, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@14
    .line 620
    .restart local v3       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    if-eqz v3, :cond_57

    #@16
    .line 621
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 622
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1c
    move-result v4

    #@1d
    if-nez v4, :cond_24

    #@1f
    .line 623
    iget-object v4, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@21
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 625
    :cond_24
    invoke-virtual {p2}, Landroid/content/BroadcastReceiver;->getDebugUnregister()Z

    #@27
    move-result v4

    #@28
    if-eqz v4, :cond_4e

    #@2a
    .line 626
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@2c
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v1

    #@30
    check-cast v1, Ljava/util/HashMap;

    #@32
    .line 628
    .local v1, holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    if-nez v1, :cond_3e

    #@34
    .line 629
    new-instance v1, Ljava/util/HashMap;

    #@36
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@39
    .line 630
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@3b
    invoke-virtual {v4, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    .line 632
    :cond_3e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@40
    const-string v4, "Originally unregistered here:"

    #@42
    invoke-direct {v0, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@45
    .line 634
    .local v0, ex:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@48
    .line 635
    invoke-virtual {v3, v0}, Landroid/app/LoadedApk$ReceiverDispatcher;->setUnregisterLocation(Ljava/lang/RuntimeException;)V

    #@4b
    .line 636
    invoke-virtual {v1, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    .line 638
    .end local v0           #ex:Ljava/lang/RuntimeException;
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :cond_4e
    const/4 v4, 0x1

    #@4f
    iput-boolean v4, v3, Landroid/app/LoadedApk$ReceiverDispatcher;->mForgotten:Z

    #@51
    .line 639
    invoke-virtual {v3}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@54
    move-result-object v4

    #@55
    monitor-exit v5

    #@56
    return-object v4

    #@57
    .line 642
    :cond_57
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@59
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5c
    move-result-object v1

    #@5d
    check-cast v1, Ljava/util/HashMap;

    #@5f
    .line 644
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    if-eqz v1, :cond_8f

    #@61
    .line 645
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@64
    move-result-object v3

    #@65
    .end local v3           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    check-cast v3, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@67
    .line 646
    .restart local v3       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    if-eqz v3, :cond_8f

    #@69
    .line 647
    invoke-virtual {v3}, Landroid/app/LoadedApk$ReceiverDispatcher;->getUnregisterLocation()Ljava/lang/RuntimeException;

    #@6c
    move-result-object v0

    #@6d
    .line 648
    .restart local v0       #ex:Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@6f
    new-instance v6, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v7, "Unregistering Receiver "

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    const-string v7, " that was already unregistered"

    #@80
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v6

    #@84
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v6

    #@88
    invoke-direct {v4, v6, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8b
    throw v4

    #@8c
    .line 660
    .end local v0           #ex:Ljava/lang/RuntimeException;
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .end local v2           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .end local v3           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :catchall_8c
    move-exception v4

    #@8d
    monitor-exit v5
    :try_end_8e
    .catchall {:try_start_3 .. :try_end_8e} :catchall_8c

    #@8e
    throw v4

    #@8f
    .line 653
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v2       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v3       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :cond_8f
    if-nez p1, :cond_b4

    #@91
    .line 654
    :try_start_91
    new-instance v4, Ljava/lang/IllegalStateException;

    #@93
    new-instance v6, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v7, "Unbinding Receiver "

    #@9a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v6

    #@9e
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    const-string v7, " from Context that is no longer in use: "

    #@a4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v6

    #@a8
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v6

    #@ac
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v6

    #@b0
    invoke-direct {v4, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b3
    throw v4

    #@b4
    .line 657
    :cond_b4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@b6
    new-instance v6, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v7, "Receiver not registered: "

    #@bd
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v6

    #@c1
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v6

    #@c5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@cc
    throw v4
    :try_end_cd
    .catchall {:try_start_91 .. :try_end_cd} :catchall_8c
.end method

.method public final forgetServiceDispatcher(Landroid/content/Context;Landroid/content/ServiceConnection;)Landroid/app/IServiceConnection;
    .registers 11
    .parameter "context"
    .parameter "c"

    #@0
    .prologue
    .line 878
    iget-object v5, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@2
    monitor-enter v5

    #@3
    .line 879
    :try_start_3
    iget-object v4, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@5
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Ljava/util/HashMap;

    #@b
    .line 881
    .local v2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    const/4 v3, 0x0

    #@c
    .line 882
    .local v3, sd:Landroid/app/LoadedApk$ServiceDispatcher;
    if-eqz v2, :cond_59

    #@e
    .line 883
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    check-cast v3, Landroid/app/LoadedApk$ServiceDispatcher;

    #@14
    .line 884
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    if-eqz v3, :cond_59

    #@16
    .line 885
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    .line 886
    invoke-virtual {v3}, Landroid/app/LoadedApk$ServiceDispatcher;->doForget()V

    #@1c
    .line 887
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_27

    #@22
    .line 888
    iget-object v4, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@24
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 890
    :cond_27
    invoke-virtual {v3}, Landroid/app/LoadedApk$ServiceDispatcher;->getFlags()I

    #@2a
    move-result v4

    #@2b
    and-int/lit8 v4, v4, 0x2

    #@2d
    if-eqz v4, :cond_53

    #@2f
    .line 891
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@31
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v1

    #@35
    check-cast v1, Ljava/util/HashMap;

    #@37
    .line 893
    .local v1, holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    if-nez v1, :cond_43

    #@39
    .line 894
    new-instance v1, Ljava/util/HashMap;

    #@3b
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@3e
    .line 895
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@40
    invoke-virtual {v4, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 897
    :cond_43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@45
    const-string v4, "Originally unbound here:"

    #@47
    invoke-direct {v0, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4a
    .line 899
    .local v0, ex:Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    #@4d
    .line 900
    invoke-virtual {v3, v0}, Landroid/app/LoadedApk$ServiceDispatcher;->setUnbindLocation(Ljava/lang/RuntimeException;)V

    #@50
    .line 901
    invoke-virtual {v1, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    .line 903
    .end local v0           #ex:Ljava/lang/RuntimeException;
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    :cond_53
    invoke-virtual {v3}, Landroid/app/LoadedApk$ServiceDispatcher;->getIServiceConnection()Landroid/app/IServiceConnection;

    #@56
    move-result-object v4

    #@57
    monitor-exit v5

    #@58
    return-object v4

    #@59
    .line 906
    :cond_59
    iget-object v4, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@5b
    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5e
    move-result-object v1

    #@5f
    check-cast v1, Ljava/util/HashMap;

    #@61
    .line 908
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    if-eqz v1, :cond_91

    #@63
    .line 909
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@66
    move-result-object v3

    #@67
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    check-cast v3, Landroid/app/LoadedApk$ServiceDispatcher;

    #@69
    .line 910
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    if-eqz v3, :cond_91

    #@6b
    .line 911
    invoke-virtual {v3}, Landroid/app/LoadedApk$ServiceDispatcher;->getUnbindLocation()Ljava/lang/RuntimeException;

    #@6e
    move-result-object v0

    #@6f
    .line 912
    .restart local v0       #ex:Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@71
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v7, "Unbinding Service "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    const-string v7, " that was already unbound"

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v6

    #@8a
    invoke-direct {v4, v6, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8d
    throw v4

    #@8e
    .line 923
    .end local v0           #ex:Ljava/lang/RuntimeException;
    .end local v1           #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .end local v2           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :catchall_8e
    move-exception v4

    #@8f
    monitor-exit v5
    :try_end_90
    .catchall {:try_start_3 .. :try_end_90} :catchall_8e

    #@90
    throw v4

    #@91
    .line 917
    .restart local v1       #holder:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .restart local v2       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :cond_91
    if-nez p1, :cond_b6

    #@93
    .line 918
    :try_start_93
    new-instance v4, Ljava/lang/IllegalStateException;

    #@95
    new-instance v6, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v7, "Unbinding Service "

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v6

    #@a4
    const-string v7, " from Context that is no longer in use: "

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-direct {v4, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b5
    throw v4

    #@b6
    .line 921
    :cond_b6
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@b8
    new-instance v6, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    const-string v7, "Service not registered: "

    #@bf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v6

    #@c3
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v6

    #@cb
    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@ce
    throw v4
    :try_end_cf
    .catchall {:try_start_93 .. :try_end_cf} :catchall_8e
.end method

.method public getAppDir()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 451
    iget-object v0, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method getApplication()Landroid/app/Application;
    .registers 2

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/app/LoadedApk;->mApplication:Landroid/app/Application;

    #@2
    return-object v0
.end method

.method public getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .registers 2

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Landroid/app/LoadedApk;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    return-object v0
.end method

.method public getAssets(Landroid/app/ActivityThread;)Landroid/content/res/AssetManager;
    .registers 3
    .parameter "mainThread"

    #@0
    .prologue
    .line 471
    invoke-virtual {p0, p1}, Landroid/app/LoadedApk;->getResources(Landroid/app/ActivityThread;)Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getClassLoader()Ljava/lang/ClassLoader;
    .registers 12

    #@0
    .prologue
    .line 260
    monitor-enter p0

    #@1
    .line 261
    :try_start_1
    iget-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@3
    if-eqz v9, :cond_9

    #@5
    .line 262
    iget-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@7
    monitor-exit p0

    #@8
    .line 332
    :goto_8
    return-object v9

    #@9
    .line 265
    :cond_9
    iget-boolean v9, p0, Landroid/app/LoadedApk;->mIncludeCode:Z

    #@b
    if-eqz v9, :cond_b9

    #@d
    iget-object v9, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@f
    const-string v10, "android"

    #@11
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v9

    #@15
    if-nez v9, :cond_b9

    #@17
    .line 266
    iget-object v8, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@19
    .line 267
    .local v8, zip:Ljava/lang/String;
    iget-object v6, p0, Landroid/app/LoadedApk;->mLibDir:Ljava/lang/String;

    #@1b
    .line 278
    .local v6, libraryPath:Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@1d
    iget-object v0, v9, Landroid/app/ActivityThread;->mInstrumentationAppDir:Ljava/lang/String;

    #@1f
    .line 280
    .local v0, instrumentationAppDir:Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@21
    iget-object v1, v9, Landroid/app/ActivityThread;->mInstrumentationAppLibraryDir:Ljava/lang/String;

    #@23
    .line 282
    .local v1, instrumentationAppLibraryDir:Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@25
    iget-object v2, v9, Landroid/app/ActivityThread;->mInstrumentationAppPackage:Ljava/lang/String;

    #@27
    .line 284
    .local v2, instrumentationAppPackage:Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@29
    iget-object v4, v9, Landroid/app/ActivityThread;->mInstrumentedAppDir:Ljava/lang/String;

    #@2b
    .line 286
    .local v4, instrumentedAppDir:Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@2d
    iget-object v5, v9, Landroid/app/ActivityThread;->mInstrumentedAppLibraryDir:Ljava/lang/String;

    #@2f
    .line 288
    .local v5, instrumentedAppLibraryDir:Ljava/lang/String;
    const/4 v3, 0x0

    #@30
    .line 290
    .local v3, instrumentationLibs:[Ljava/lang/String;
    iget-object v9, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@32
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v9

    #@36
    if-nez v9, :cond_40

    #@38
    iget-object v9, p0, Landroid/app/LoadedApk;->mAppDir:Ljava/lang/String;

    #@3a
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3d
    move-result v9

    #@3e
    if-eqz v9, :cond_78

    #@40
    .line 292
    :cond_40
    new-instance v9, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    const-string v10, ":"

    #@4b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v9

    #@4f
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v8

    #@57
    .line 293
    new-instance v9, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    const-string v10, ":"

    #@62
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    .line 294
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v9

    #@72
    if-nez v9, :cond_78

    #@74
    .line 295
    invoke-static {v2}, Landroid/app/LoadedApk;->getLibrariesFor(Ljava/lang/String;)[Ljava/lang/String;

    #@77
    move-result-object v3

    #@78
    .line 300
    :cond_78
    iget-object v9, p0, Landroid/app/LoadedApk;->mSharedLibraries:[Ljava/lang/String;

    #@7a
    if-nez v9, :cond_7e

    #@7c
    if-eqz v3, :cond_9b

    #@7e
    .line 302
    :cond_7e
    new-instance v9, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    iget-object v10, p0, Landroid/app/LoadedApk;->mSharedLibraries:[Ljava/lang/String;

    #@85
    invoke-static {v10, v3}, Landroid/app/LoadedApk;->combineLibs([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    #@88
    move-result-object v10

    #@89
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v9

    #@8d
    const/16 v10, 0x3a

    #@8f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@92
    move-result-object v9

    #@93
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v9

    #@97
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    .line 317
    :cond_9b
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    #@9e
    move-result-object v7

    #@9f
    .line 319
    .local v7, oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    invoke-static {}, Landroid/app/ApplicationLoaders;->getDefault()Landroid/app/ApplicationLoaders;

    #@a2
    move-result-object v9

    #@a3
    iget-object v10, p0, Landroid/app/LoadedApk;->mBaseClassLoader:Ljava/lang/ClassLoader;

    #@a5
    invoke-virtual {v9, v8, v6, v10}, Landroid/app/ApplicationLoaders;->getClassLoader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;

    #@a8
    move-result-object v9

    #@a9
    iput-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@ab
    .line 322
    invoke-direct {p0}, Landroid/app/LoadedApk;->initializeJavaContextClassLoader()V

    #@ae
    .line 324
    invoke-static {v7}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@b1
    .line 332
    .end local v0           #instrumentationAppDir:Ljava/lang/String;
    .end local v1           #instrumentationAppLibraryDir:Ljava/lang/String;
    .end local v2           #instrumentationAppPackage:Ljava/lang/String;
    .end local v3           #instrumentationLibs:[Ljava/lang/String;
    .end local v4           #instrumentedAppDir:Ljava/lang/String;
    .end local v5           #instrumentedAppLibraryDir:Ljava/lang/String;
    .end local v6           #libraryPath:Ljava/lang/String;
    .end local v7           #oldPolicy:Landroid/os/StrictMode$ThreadPolicy;
    .end local v8           #zip:Ljava/lang/String;
    :goto_b1
    iget-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@b3
    monitor-exit p0

    #@b4
    goto/16 :goto_8

    #@b6
    .line 333
    :catchall_b6
    move-exception v9

    #@b7
    monitor-exit p0
    :try_end_b8
    .catchall {:try_start_1 .. :try_end_b8} :catchall_b6

    #@b8
    throw v9

    #@b9
    .line 326
    :cond_b9
    :try_start_b9
    iget-object v9, p0, Landroid/app/LoadedApk;->mBaseClassLoader:Ljava/lang/ClassLoader;

    #@bb
    if-nez v9, :cond_c4

    #@bd
    .line 327
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    #@c0
    move-result-object v9

    #@c1
    iput-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;

    #@c3
    goto :goto_b1

    #@c4
    .line 329
    :cond_c4
    iget-object v9, p0, Landroid/app/LoadedApk;->mBaseClassLoader:Ljava/lang/ClassLoader;

    #@c6
    iput-object v9, p0, Landroid/app/LoadedApk;->mClassLoader:Ljava/lang/ClassLoader;
    :try_end_c8
    .catchall {:try_start_b9 .. :try_end_c8} :catchall_b6

    #@c8
    goto :goto_b1
.end method

.method public getDataDir()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 463
    iget-object v0, p0, Landroid/app/LoadedApk;->mDataDir:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getDataDirFile()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 467
    iget-object v0, p0, Landroid/app/LoadedApk;->mDataDirFile:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public getLibDir()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 455
    iget-object v0, p0, Landroid/app/LoadedApk;->mLibDir:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getReceiverDispatcher(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)Landroid/content/IIntentReceiver;
    .registers 17
    .parameter "r"
    .parameter "context"
    .parameter "handler"
    .parameter "instrumentation"
    .parameter "registered"

    #@0
    .prologue
    .line 586
    iget-object v10, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@2
    monitor-enter v10

    #@3
    .line 587
    const/4 v1, 0x0

    #@4
    .line 588
    .local v1, rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    const/4 v7, 0x0

    #@5
    .line 589
    .local v7, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    if-eqz p5, :cond_58

    #@7
    .line 590
    :try_start_7
    iget-object v2, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@9
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    move-object v0, v2

    #@e
    check-cast v0, Ljava/util/HashMap;

    #@10
    move-object v7, v0

    #@11
    .line 591
    if-eqz v7, :cond_58

    #@13
    .line 592
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@16
    move-result-object v2

    #@17
    move-object v0, v2

    #@18
    check-cast v0, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@1a
    move-object v1, v0
    :try_end_1b
    .catchall {:try_start_7 .. :try_end_1b} :catchall_4a

    #@1b
    move-object v8, v7

    #@1c
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .local v8, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    move-object v9, v1

    #@1d
    .line 595
    .end local v1           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .local v9, rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :goto_1d
    if-nez v9, :cond_44

    #@1f
    .line 596
    :try_start_1f
    new-instance v1, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@21
    move-object v2, p1

    #@22
    move-object v3, p2

    #@23
    move-object v4, p3

    #@24
    move-object v5, p4

    #@25
    move/from16 v6, p5

    #@27
    invoke-direct/range {v1 .. v6}, Landroid/app/LoadedApk$ReceiverDispatcher;-><init>(Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/os/Handler;Landroid/app/Instrumentation;Z)V
    :try_end_2a
    .catchall {:try_start_1f .. :try_end_2a} :catchall_4d

    #@2a
    .line 598
    .end local v9           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .restart local v1       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    if-eqz p5, :cond_56

    #@2c
    .line 599
    if-nez v8, :cond_54

    #@2e
    .line 600
    :try_start_2e
    new-instance v7, Ljava/util/HashMap;

    #@30
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V
    :try_end_33
    .catchall {:try_start_2e .. :try_end_33} :catchall_51

    #@33
    .line 601
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :try_start_33
    iget-object v2, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@35
    invoke-virtual {v2, p2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    .line 603
    :goto_38
    invoke-virtual {v7, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3b
    .line 608
    :goto_3b
    const/4 v2, 0x0

    #@3c
    iput-boolean v2, v1, Landroid/app/LoadedApk$ReceiverDispatcher;->mForgotten:Z

    #@3e
    .line 609
    invoke-virtual {v1}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@41
    move-result-object v2

    #@42
    monitor-exit v10
    :try_end_43
    .catchall {:try_start_33 .. :try_end_43} :catchall_4a

    #@43
    return-object v2

    #@44
    .line 606
    .end local v1           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v9       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :cond_44
    :try_start_44
    invoke-virtual {v9, p2, p3}, Landroid/app/LoadedApk$ReceiverDispatcher;->validate(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_4d

    #@47
    move-object v7, v8

    #@48
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    move-object v1, v9

    #@49
    .end local v9           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .restart local v1       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    goto :goto_3b

    #@4a
    .line 610
    :catchall_4a
    move-exception v2

    #@4b
    :goto_4b
    :try_start_4b
    monitor-exit v10
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4a

    #@4c
    throw v2

    #@4d
    .end local v1           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v9       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :catchall_4d
    move-exception v2

    #@4e
    move-object v7, v8

    #@4f
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    move-object v1, v9

    #@50
    .end local v9           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .restart local v1       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    goto :goto_4b

    #@51
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :catchall_51
    move-exception v2

    #@52
    move-object v7, v8

    #@53
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    goto :goto_4b

    #@54
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :cond_54
    move-object v7, v8

    #@55
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    goto :goto_38

    #@56
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :cond_56
    move-object v7, v8

    #@57
    .end local v8           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v7       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    goto :goto_3b

    #@58
    :cond_58
    move-object v8, v7

    #@59
    .end local v7           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .restart local v8       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    move-object v9, v1

    #@5a
    .end local v1           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    .restart local v9       #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    goto :goto_1d
.end method

.method public getResDir()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 459
    iget-object v0, p0, Landroid/app/LoadedApk;->mResDir:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getResources(Landroid/app/ActivityThread;)Landroid/content/res/Resources;
    .registers 5
    .parameter "mainThread"

    #@0
    .prologue
    .line 475
    iget-object v0, p0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 476
    iget-object v0, p0, Landroid/app/LoadedApk;->mResDir:Ljava/lang/String;

    #@6
    const/4 v1, 0x0

    #@7
    const/4 v2, 0x0

    #@8
    invoke-virtual {p1, v0, v1, v2, p0}, Landroid/app/ActivityThread;->getTopLevelResources(Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/app/LoadedApk;)Landroid/content/res/Resources;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@e
    .line 479
    :cond_e
    iget-object v0, p0, Landroid/app/LoadedApk;->mResources:Landroid/content/res/Resources;

    #@10
    return-object v0
.end method

.method public final getServiceDispatcher(Landroid/content/ServiceConnection;Landroid/content/Context;Landroid/os/Handler;I)Landroid/app/IServiceConnection;
    .registers 11
    .parameter "c"
    .parameter "context"
    .parameter "handler"
    .parameter "flags"

    #@0
    .prologue
    .line 856
    iget-object v5, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@2
    monitor-enter v5

    #@3
    .line 857
    const/4 v2, 0x0

    #@4
    .line 858
    .local v2, sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :try_start_4
    iget-object v4, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@6
    invoke-virtual {v4, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Ljava/util/HashMap;

    #@c
    .line 859
    .local v1, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    if-eqz v1, :cond_3e

    #@e
    .line 860
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    move-object v0, v4

    #@13
    check-cast v0, Landroid/app/LoadedApk$ServiceDispatcher;

    #@15
    move-object v2, v0
    :try_end_16
    .catchall {:try_start_4 .. :try_end_16} :catchall_38

    #@16
    move-object v3, v2

    #@17
    .line 862
    .end local v2           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .local v3, sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :goto_17
    if-nez v3, :cond_33

    #@19
    .line 863
    :try_start_19
    new-instance v2, Landroid/app/LoadedApk$ServiceDispatcher;

    #@1b
    invoke-direct {v2, p1, p2, p3, p4}, Landroid/app/LoadedApk$ServiceDispatcher;-><init>(Landroid/content/ServiceConnection;Landroid/content/Context;Landroid/os/Handler;I)V
    :try_end_1e
    .catchall {:try_start_19 .. :try_end_1e} :catchall_3b

    #@1e
    .line 864
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v2       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    if-nez v1, :cond_2a

    #@20
    .line 865
    :try_start_20
    new-instance v1, Ljava/util/HashMap;

    #@22
    .end local v1           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@25
    .line 866
    .restart local v1       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    iget-object v4, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@27
    invoke-virtual {v4, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2a
    .line 868
    :cond_2a
    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    .line 872
    :goto_2d
    invoke-virtual {v2}, Landroid/app/LoadedApk$ServiceDispatcher;->getIServiceConnection()Landroid/app/IServiceConnection;

    #@30
    move-result-object v4

    #@31
    monitor-exit v5
    :try_end_32
    .catchall {:try_start_20 .. :try_end_32} :catchall_38

    #@32
    return-object v4

    #@33
    .line 870
    .end local v2           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :cond_33
    :try_start_33
    invoke-virtual {v3, p2, p3}, Landroid/app/LoadedApk$ServiceDispatcher;->validate(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_3b

    #@36
    move-object v2, v3

    #@37
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v2       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    goto :goto_2d

    #@38
    .line 873
    .end local v1           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    :catchall_38
    move-exception v4

    #@39
    :goto_39
    :try_start_39
    monitor-exit v5
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    #@3a
    throw v4

    #@3b
    .end local v2           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v1       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :catchall_3b
    move-exception v4

    #@3c
    move-object v2, v3

    #@3d
    .end local v3           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v2       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    goto :goto_39

    #@3e
    :cond_3e
    move-object v3, v2

    #@3f
    .end local v2           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    .restart local v3       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    goto :goto_17
.end method

.method public isSecurityViolation()Z
    .registers 2

    #@0
    .prologue
    .line 184
    iget-boolean v0, p0, Landroid/app/LoadedApk;->mSecurityViolation:Z

    #@2
    return v0
.end method

.method public makeApplication(ZLandroid/app/Instrumentation;)Landroid/app/Application;
    .registers 11
    .parameter "forceDefaultAppClass"
    .parameter "instrumentation"

    #@0
    .prologue
    .line 484
    iget-object v5, p0, Landroid/app/LoadedApk;->mApplication:Landroid/app/Application;

    #@2
    if-eqz v5, :cond_7

    #@4
    .line 485
    iget-object v0, p0, Landroid/app/LoadedApk;->mApplication:Landroid/app/Application;

    #@6
    .line 524
    :cond_6
    :goto_6
    return-object v0

    #@7
    .line 488
    :cond_7
    const/4 v0, 0x0

    #@8
    .line 490
    .local v0, app:Landroid/app/Application;
    iget-object v5, p0, Landroid/app/LoadedApk;->mApplicationInfo:Landroid/content/pm/ApplicationInfo;

    #@a
    iget-object v1, v5, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    #@c
    .line 491
    .local v1, appClass:Ljava/lang/String;
    if-nez p1, :cond_10

    #@e
    if-nez v1, :cond_12

    #@10
    .line 492
    :cond_10
    const-string v1, "android.app.Application"

    #@12
    .line 496
    :cond_12
    :try_start_12
    invoke-virtual {p0}, Landroid/app/LoadedApk;->getClassLoader()Ljava/lang/ClassLoader;

    #@15
    move-result-object v3

    #@16
    .line 497
    .local v3, cl:Ljava/lang/ClassLoader;
    new-instance v2, Landroid/app/ContextImpl;

    #@18
    invoke-direct {v2}, Landroid/app/ContextImpl;-><init>()V

    #@1b
    .line 498
    .local v2, appContext:Landroid/app/ContextImpl;
    const/4 v5, 0x0

    #@1c
    iget-object v6, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@1e
    invoke-virtual {v2, p0, v5, v6}, Landroid/app/ContextImpl;->init(Landroid/app/LoadedApk;Landroid/os/IBinder;Landroid/app/ActivityThread;)V

    #@21
    .line 499
    iget-object v5, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@23
    iget-object v5, v5, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@25
    invoke-virtual {v5, v3, v1, v2}, Landroid/app/Instrumentation;->newApplication(Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Context;)Landroid/app/Application;

    #@28
    move-result-object v0

    #@29
    .line 501
    invoke-virtual {v2, v0}, Landroid/app/ContextImpl;->setOuterContext(Landroid/content/Context;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_2c} :catch_71

    #@2c
    .line 509
    .end local v2           #appContext:Landroid/app/ContextImpl;
    .end local v3           #cl:Ljava/lang/ClassLoader;
    :cond_2c
    iget-object v5, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@2e
    iget-object v5, v5, Landroid/app/ActivityThread;->mAllApplications:Ljava/util/ArrayList;

    #@30
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@33
    .line 510
    iput-object v0, p0, Landroid/app/LoadedApk;->mApplication:Landroid/app/Application;

    #@35
    .line 512
    if-eqz p2, :cond_6

    #@37
    .line 514
    :try_start_37
    invoke-virtual {p2, v0}, Landroid/app/Instrumentation;->callApplicationOnCreate(Landroid/app/Application;)V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_3a} :catch_3b

    #@3a
    goto :goto_6

    #@3b
    .line 515
    :catch_3b
    move-exception v4

    #@3c
    .line 516
    .local v4, e:Ljava/lang/Exception;
    invoke-virtual {p2, v0, v4}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@3f
    move-result v5

    #@40
    if-nez v5, :cond_6

    #@42
    .line 517
    new-instance v5, Ljava/lang/RuntimeException;

    #@44
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v7, "Unable to create application "

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    const-string v7, ": "

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v6

    #@6d
    invoke-direct {v5, v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@70
    throw v5

    #@71
    .line 502
    .end local v4           #e:Ljava/lang/Exception;
    :catch_71
    move-exception v4

    #@72
    .line 503
    .restart local v4       #e:Ljava/lang/Exception;
    iget-object v5, p0, Landroid/app/LoadedApk;->mActivityThread:Landroid/app/ActivityThread;

    #@74
    iget-object v5, v5, Landroid/app/ActivityThread;->mInstrumentation:Landroid/app/Instrumentation;

    #@76
    invoke-virtual {v5, v0, v4}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    #@79
    move-result v5

    #@7a
    if-nez v5, :cond_2c

    #@7c
    .line 504
    new-instance v5, Ljava/lang/RuntimeException;

    #@7e
    new-instance v6, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v7, "Unable to instantiate application "

    #@85
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v6

    #@8d
    const-string v7, ": "

    #@8f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v6

    #@93
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9e
    move-result-object v6

    #@9f
    invoke-direct {v5, v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@a2
    throw v5
.end method

.method public removeContextRegistrations(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "context"
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 529
    invoke-static {}, Landroid/os/StrictMode;->vmRegistrationLeaksEnabled()Z

    #@3
    move-result v4

    #@4
    .line 530
    .local v4, reportRegistrationLeaks:Z
    iget-object v8, p0, Landroid/app/LoadedApk;->mReceivers:Ljava/util/HashMap;

    #@6
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v5

    #@a
    check-cast v5, Ljava/util/HashMap;

    #@c
    .line 532
    .local v5, rmap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/BroadcastReceiver;Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    if-eqz v5, :cond_86

    #@e
    .line 533
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v0

    #@16
    .line 534
    .local v0, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_86

    #@1c
    .line 535
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Landroid/app/LoadedApk$ReceiverDispatcher;

    #@22
    .line 536
    .local v3, rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    new-instance v2, Landroid/app/IntentReceiverLeaked;

    #@24
    new-instance v8, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    const-string v9, " "

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v8

    #@37
    const-string v9, " has leaked IntentReceiver "

    #@39
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v3}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIntentReceiver()Landroid/content/BroadcastReceiver;

    #@40
    move-result-object v9

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    const-string v9, " that was "

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    const-string/jumbo v9, "originally registered here. Are you missing a "

    #@4e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v8

    #@52
    const-string v9, "call to unregisterReceiver()?"

    #@54
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v8

    #@58
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v8

    #@5c
    invoke-direct {v2, v8}, Landroid/app/IntentReceiverLeaked;-><init>(Ljava/lang/String;)V

    #@5f
    .line 541
    .local v2, leak:Landroid/app/IntentReceiverLeaked;
    invoke-virtual {v3}, Landroid/app/LoadedApk$ReceiverDispatcher;->getLocation()Landroid/app/IntentReceiverLeaked;

    #@62
    move-result-object v8

    #@63
    invoke-virtual {v8}, Landroid/app/IntentReceiverLeaked;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@66
    move-result-object v8

    #@67
    invoke-virtual {v2, v8}, Landroid/app/IntentReceiverLeaked;->setStackTrace([Ljava/lang/StackTraceElement;)V

    #@6a
    .line 542
    const-string v8, "ActivityThread"

    #@6c
    invoke-virtual {v2}, Landroid/app/IntentReceiverLeaked;->getMessage()Ljava/lang/String;

    #@6f
    move-result-object v9

    #@70
    invoke-static {v8, v9, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@73
    .line 543
    if-eqz v4, :cond_78

    #@75
    .line 544
    invoke-static {v2}, Landroid/os/StrictMode;->onIntentReceiverLeaked(Ljava/lang/Throwable;)V

    #@78
    .line 547
    :cond_78
    :try_start_78
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@7b
    move-result-object v8

    #@7c
    invoke-virtual {v3}, Landroid/app/LoadedApk$ReceiverDispatcher;->getIIntentReceiver()Landroid/content/IIntentReceiver;

    #@7f
    move-result-object v9

    #@80
    invoke-interface {v8, v9}, Landroid/app/IActivityManager;->unregisterReceiver(Landroid/content/IIntentReceiver;)V
    :try_end_83
    .catch Landroid/os/RemoteException; {:try_start_78 .. :try_end_83} :catch_84

    #@83
    goto :goto_16

    #@84
    .line 549
    :catch_84
    move-exception v8

    #@85
    goto :goto_16

    #@86
    .line 554
    .end local v0           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/LoadedApk$ReceiverDispatcher;>;"
    .end local v2           #leak:Landroid/app/IntentReceiverLeaked;
    .end local v3           #rd:Landroid/app/LoadedApk$ReceiverDispatcher;
    :cond_86
    iget-object v8, p0, Landroid/app/LoadedApk;->mUnregisteredReceivers:Ljava/util/HashMap;

    #@88
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8b
    .line 556
    iget-object v8, p0, Landroid/app/LoadedApk;->mServices:Ljava/util/HashMap;

    #@8d
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@90
    move-result-object v7

    #@91
    check-cast v7, Ljava/util/HashMap;

    #@93
    .line 558
    .local v7, smap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ServiceConnection;Landroid/app/LoadedApk$ServiceDispatcher;>;"
    if-eqz v7, :cond_101

    #@95
    .line 559
    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@98
    move-result-object v8

    #@99
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9c
    move-result-object v1

    #@9d
    .line 560
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/LoadedApk$ServiceDispatcher;>;"
    :goto_9d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a0
    move-result v8

    #@a1
    if-eqz v8, :cond_101

    #@a3
    .line 561
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a6
    move-result-object v6

    #@a7
    check-cast v6, Landroid/app/LoadedApk$ServiceDispatcher;

    #@a9
    .line 562
    .local v6, sd:Landroid/app/LoadedApk$ServiceDispatcher;
    new-instance v2, Landroid/app/ServiceConnectionLeaked;

    #@ab
    new-instance v8, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    const-string v9, " "

    #@b6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v8

    #@ba
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v8

    #@be
    const-string v9, " has leaked ServiceConnection "

    #@c0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v8

    #@c4
    invoke-virtual {v6}, Landroid/app/LoadedApk$ServiceDispatcher;->getServiceConnection()Landroid/content/ServiceConnection;

    #@c7
    move-result-object v9

    #@c8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v8

    #@cc
    const-string v9, " that was originally bound here"

    #@ce
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v8

    #@d2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v8

    #@d6
    invoke-direct {v2, v8}, Landroid/app/ServiceConnectionLeaked;-><init>(Ljava/lang/String;)V

    #@d9
    .line 565
    .local v2, leak:Landroid/app/ServiceConnectionLeaked;
    invoke-virtual {v6}, Landroid/app/LoadedApk$ServiceDispatcher;->getLocation()Landroid/app/ServiceConnectionLeaked;

    #@dc
    move-result-object v8

    #@dd
    invoke-virtual {v8}, Landroid/app/ServiceConnectionLeaked;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@e0
    move-result-object v8

    #@e1
    invoke-virtual {v2, v8}, Landroid/app/ServiceConnectionLeaked;->setStackTrace([Ljava/lang/StackTraceElement;)V

    #@e4
    .line 566
    const-string v8, "ActivityThread"

    #@e6
    invoke-virtual {v2}, Landroid/app/ServiceConnectionLeaked;->getMessage()Ljava/lang/String;

    #@e9
    move-result-object v9

    #@ea
    invoke-static {v8, v9, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ed
    .line 567
    if-eqz v4, :cond_f2

    #@ef
    .line 568
    invoke-static {v2}, Landroid/os/StrictMode;->onServiceConnectionLeaked(Ljava/lang/Throwable;)V

    #@f2
    .line 571
    :cond_f2
    :try_start_f2
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@f5
    move-result-object v8

    #@f6
    invoke-virtual {v6}, Landroid/app/LoadedApk$ServiceDispatcher;->getIServiceConnection()Landroid/app/IServiceConnection;

    #@f9
    move-result-object v9

    #@fa
    invoke-interface {v8, v9}, Landroid/app/IActivityManager;->unbindService(Landroid/app/IServiceConnection;)Z
    :try_end_fd
    .catch Landroid/os/RemoteException; {:try_start_f2 .. :try_end_fd} :catch_107

    #@fd
    .line 576
    :goto_fd
    invoke-virtual {v6}, Landroid/app/LoadedApk$ServiceDispatcher;->doForget()V

    #@100
    goto :goto_9d

    #@101
    .line 579
    .end local v1           #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .end local v2           #leak:Landroid/app/ServiceConnectionLeaked;
    .end local v6           #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :cond_101
    iget-object v8, p0, Landroid/app/LoadedApk;->mUnboundServices:Ljava/util/HashMap;

    #@103
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@106
    .line 581
    return-void

    #@107
    .line 573
    .restart local v1       #it:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/app/LoadedApk$ServiceDispatcher;>;"
    .restart local v2       #leak:Landroid/app/ServiceConnectionLeaked;
    .restart local v6       #sd:Landroid/app/LoadedApk$ServiceDispatcher;
    :catch_107
    move-exception v8

    #@108
    goto :goto_fd
.end method
