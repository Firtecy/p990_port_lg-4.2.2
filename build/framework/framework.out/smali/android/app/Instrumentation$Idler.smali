.class final Landroid/app/Instrumentation$Idler;
.super Ljava/lang/Object;
.source "Instrumentation.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Instrumentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Idler"
.end annotation


# instance fields
.field private final mCallback:Ljava/lang/Runnable;

.field private mIdle:Z


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 1727
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1728
    iput-object p1, p0, Landroid/app/Instrumentation$Idler;->mCallback:Ljava/lang/Runnable;

    #@5
    .line 1729
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/app/Instrumentation$Idler;->mIdle:Z

    #@8
    .line 1730
    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .registers 2

    #@0
    .prologue
    .line 1733
    iget-object v0, p0, Landroid/app/Instrumentation$Idler;->mCallback:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1734
    iget-object v0, p0, Landroid/app/Instrumentation$Idler;->mCallback:Ljava/lang/Runnable;

    #@6
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@9
    .line 1736
    :cond_9
    monitor-enter p0

    #@a
    .line 1737
    const/4 v0, 0x1

    #@b
    :try_start_b
    iput-boolean v0, p0, Landroid/app/Instrumentation$Idler;->mIdle:Z

    #@d
    .line 1738
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@10
    .line 1739
    monitor-exit p0

    #@11
    .line 1740
    const/4 v0, 0x0

    #@12
    return v0

    #@13
    .line 1739
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method public waitForIdle()V
    .registers 2

    #@0
    .prologue
    .line 1744
    monitor-enter p0

    #@1
    .line 1745
    :goto_1
    :try_start_1
    iget-boolean v0, p0, Landroid/app/Instrumentation$Idler;->mIdle:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_d

    #@3
    if-nez v0, :cond_b

    #@5
    .line 1747
    :try_start_5
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_8} :catch_9

    #@8
    goto :goto_1

    #@9
    .line 1748
    :catch_9
    move-exception v0

    #@a
    goto :goto_1

    #@b
    .line 1751
    :cond_b
    :try_start_b
    monitor-exit p0

    #@c
    .line 1752
    return-void

    #@d
    .line 1751
    :catchall_d
    move-exception v0

    #@e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_b .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method
