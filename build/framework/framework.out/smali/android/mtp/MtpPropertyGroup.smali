.class Landroid/mtp/MtpPropertyGroup;
.super Ljava/lang/Object;
.source "MtpPropertyGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/mtp/MtpPropertyGroup$Property;
    }
.end annotation


# static fields
.field private static final FORMAT_WHERE:Ljava/lang/String; = "format=?"

.field private static final ID_FORMAT_WHERE:Ljava/lang/String; = "_id=? AND format=?"

.field private static final ID_WHERE:Ljava/lang/String; = "_id=?"

.field private static final PARENT_FORMAT_WHERE:Ljava/lang/String; = "parent=? AND format=?"

.field private static final PARENT_WHERE:Ljava/lang/String; = "parent=?"

.field private static final TAG:Ljava/lang/String; = "MtpPropertyGroup"


# instance fields
.field private mColumns:[Ljava/lang/String;

.field private final mDatabase:Landroid/mtp/MtpDatabase;

.field private final mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

.field private final mProvider:Landroid/content/IContentProvider;

.field private final mUri:Landroid/net/Uri;

.field private final mVolumeName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/mtp/MtpDatabase;Landroid/content/IContentProvider;Ljava/lang/String;[I)V
    .registers 10
    .parameter "database"
    .parameter "provider"
    .parameter "volume"
    .parameter "properties"

    #@0
    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    iput-object p1, p0, Landroid/mtp/MtpPropertyGroup;->mDatabase:Landroid/mtp/MtpDatabase;

    #@5
    .line 71
    iput-object p2, p0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@7
    .line 72
    iput-object p3, p0, Landroid/mtp/MtpPropertyGroup;->mVolumeName:Ljava/lang/String;

    #@9
    .line 73
    invoke-static {p3}, Landroid/provider/MediaStore$Files;->getMtpObjectsUri(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v3

    #@d
    iput-object v3, p0, Landroid/mtp/MtpPropertyGroup;->mUri:Landroid/net/Uri;

    #@f
    .line 75
    array-length v1, p4

    #@10
    .line 76
    .local v1, count:I
    new-instance v0, Ljava/util/ArrayList;

    #@12
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@15
    .line 77
    .local v0, columns:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "_id"

    #@17
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1a
    .line 79
    new-array v3, v1, [Landroid/mtp/MtpPropertyGroup$Property;

    #@1c
    iput-object v3, p0, Landroid/mtp/MtpPropertyGroup;->mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

    #@1e
    .line 80
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    if-ge v2, v1, :cond_2e

    #@21
    .line 81
    iget-object v3, p0, Landroid/mtp/MtpPropertyGroup;->mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

    #@23
    aget v4, p4, v2

    #@25
    invoke-direct {p0, v4, v0}, Landroid/mtp/MtpPropertyGroup;->createProperty(ILjava/util/ArrayList;)Landroid/mtp/MtpPropertyGroup$Property;

    #@28
    move-result-object v4

    #@29
    aput-object v4, v3, v2

    #@2b
    .line 80
    add-int/lit8 v2, v2, 0x1

    #@2d
    goto :goto_1f

    #@2e
    .line 83
    :cond_2e
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v1

    #@32
    .line 84
    new-array v3, v1, [Ljava/lang/String;

    #@34
    iput-object v3, p0, Landroid/mtp/MtpPropertyGroup;->mColumns:[Ljava/lang/String;

    #@36
    .line 85
    const/4 v2, 0x0

    #@37
    :goto_37
    if-ge v2, v1, :cond_46

    #@39
    .line 86
    iget-object v4, p0, Landroid/mtp/MtpPropertyGroup;->mColumns:[Ljava/lang/String;

    #@3b
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Ljava/lang/String;

    #@41
    aput-object v3, v4, v2

    #@43
    .line 85
    add-int/lit8 v2, v2, 0x1

    #@45
    goto :goto_37

    #@46
    .line 88
    :cond_46
    return-void
.end method

.method private createProperty(ILjava/util/ArrayList;)Landroid/mtp/MtpPropertyGroup$Property;
    .registers 8
    .parameter "code"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/mtp/MtpPropertyGroup$Property;"
        }
    .end annotation

    #@0
    .prologue
    .line 91
    .local p2, columns:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    #@1
    .line 94
    .local v0, column:Ljava/lang/String;
    sparse-switch p1, :sswitch_data_b0

    #@4
    .line 195
    const/4 v1, 0x0

    #@5
    .line 196
    .local v1, type:I
    const-string v2, "MtpPropertyGroup"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string/jumbo v4, "unsupported property "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 200
    :goto_1e
    if-eqz v0, :cond_a8

    #@20
    .line 201
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 202
    new-instance v2, Landroid/mtp/MtpPropertyGroup$Property;

    #@25
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    #@28
    move-result v3

    #@29
    add-int/lit8 v3, v3, -0x1

    #@2b
    invoke-direct {v2, p0, p1, v1, v3}, Landroid/mtp/MtpPropertyGroup$Property;-><init>(Landroid/mtp/MtpPropertyGroup;III)V

    #@2e
    .line 204
    :goto_2e
    return-object v2

    #@2f
    .line 96
    .end local v1           #type:I
    :sswitch_2f
    const-string/jumbo v0, "storage_id"

    #@32
    .line 97
    const/4 v1, 0x6

    #@33
    .line 98
    .restart local v1       #type:I
    goto :goto_1e

    #@34
    .line 100
    .end local v1           #type:I
    :sswitch_34
    const-string v0, "format"

    #@36
    .line 101
    const/4 v1, 0x4

    #@37
    .line 102
    .restart local v1       #type:I
    goto :goto_1e

    #@38
    .line 105
    .end local v1           #type:I
    :sswitch_38
    const/4 v1, 0x4

    #@39
    .line 106
    .restart local v1       #type:I
    goto :goto_1e

    #@3a
    .line 108
    .end local v1           #type:I
    :sswitch_3a
    const-string v0, "_size"

    #@3c
    .line 109
    const/16 v1, 0x8

    #@3e
    .line 110
    .restart local v1       #type:I
    goto :goto_1e

    #@3f
    .line 112
    .end local v1           #type:I
    :sswitch_3f
    const-string v0, "_data"

    #@41
    .line 113
    const v1, 0xffff

    #@44
    .line 114
    .restart local v1       #type:I
    goto :goto_1e

    #@45
    .line 116
    .end local v1           #type:I
    :sswitch_45
    const-string/jumbo v0, "title"

    #@48
    .line 117
    const v1, 0xffff

    #@4b
    .line 118
    .restart local v1       #type:I
    goto :goto_1e

    #@4c
    .line 120
    .end local v1           #type:I
    :sswitch_4c
    const-string v0, "date_modified"

    #@4e
    .line 121
    const v1, 0xffff

    #@51
    .line 122
    .restart local v1       #type:I
    goto :goto_1e

    #@52
    .line 130
    .end local v1           #type:I
    :sswitch_52
    const-string/jumbo v0, "year"

    #@55
    .line 131
    const v1, 0xffff

    #@58
    .line 132
    .restart local v1       #type:I
    goto :goto_1e

    #@59
    .line 134
    .end local v1           #type:I
    :sswitch_59
    const-string/jumbo v0, "parent"

    #@5c
    .line 135
    const/4 v1, 0x6

    #@5d
    .line 136
    .restart local v1       #type:I
    goto :goto_1e

    #@5e
    .line 139
    .end local v1           #type:I
    :sswitch_5e
    const-string/jumbo v0, "storage_id"

    #@61
    .line 140
    const/16 v1, 0xa

    #@63
    .line 141
    .restart local v1       #type:I
    goto :goto_1e

    #@64
    .line 143
    .end local v1           #type:I
    :sswitch_64
    const-string v0, "duration"

    #@66
    .line 144
    const/4 v1, 0x6

    #@67
    .line 145
    .restart local v1       #type:I
    goto :goto_1e

    #@68
    .line 147
    .end local v1           #type:I
    :sswitch_68
    const-string/jumbo v0, "track"

    #@6b
    .line 148
    const/4 v1, 0x4

    #@6c
    .line 149
    .restart local v1       #type:I
    goto :goto_1e

    #@6d
    .line 151
    .end local v1           #type:I
    :sswitch_6d
    const-string v0, "_display_name"

    #@6f
    .line 152
    const v1, 0xffff

    #@72
    .line 153
    .restart local v1       #type:I
    goto :goto_1e

    #@73
    .line 155
    .end local v1           #type:I
    :sswitch_73
    const v1, 0xffff

    #@76
    .line 156
    .restart local v1       #type:I
    goto :goto_1e

    #@77
    .line 158
    .end local v1           #type:I
    :sswitch_77
    const v1, 0xffff

    #@7a
    .line 159
    .restart local v1       #type:I
    goto :goto_1e

    #@7b
    .line 161
    .end local v1           #type:I
    :sswitch_7b
    const-string v0, "album_artist"

    #@7d
    .line 162
    const v1, 0xffff

    #@80
    .line 163
    .restart local v1       #type:I
    goto :goto_1e

    #@81
    .line 166
    .end local v1           #type:I
    :sswitch_81
    const v1, 0xffff

    #@84
    .line 167
    .restart local v1       #type:I
    goto :goto_1e

    #@85
    .line 169
    .end local v1           #type:I
    :sswitch_85
    const-string v0, "composer"

    #@87
    .line 170
    const v1, 0xffff

    #@8a
    .line 171
    .restart local v1       #type:I
    goto :goto_1e

    #@8b
    .line 173
    .end local v1           #type:I
    :sswitch_8b
    const-string v0, "description"

    #@8d
    .line 174
    const v1, 0xffff

    #@90
    .line 175
    .restart local v1       #type:I
    goto :goto_1e

    #@91
    .line 178
    .end local v1           #type:I
    :sswitch_91
    const-string/jumbo v0, "width"

    #@94
    .line 179
    const/4 v1, 0x6

    #@95
    .line 180
    .restart local v1       #type:I
    goto :goto_1e

    #@96
    .line 182
    .end local v1           #type:I
    :sswitch_96
    const-string v0, "height"

    #@98
    .line 183
    const/4 v1, 0x6

    #@99
    .line 184
    .restart local v1       #type:I
    goto :goto_1e

    #@9a
    .line 186
    .end local v1           #type:I
    :sswitch_9a
    const-string v0, "datetaken"

    #@9c
    .line 187
    const v1, 0xffff

    #@9f
    .line 188
    .restart local v1       #type:I
    goto/16 :goto_1e

    #@a1
    .line 190
    .end local v1           #type:I
    :sswitch_a1
    const-string v0, "date_added"

    #@a3
    .line 191
    const v1, 0xffff

    #@a6
    .line 192
    .restart local v1       #type:I
    goto/16 :goto_1e

    #@a8
    .line 204
    :cond_a8
    new-instance v2, Landroid/mtp/MtpPropertyGroup$Property;

    #@aa
    const/4 v3, -0x1

    #@ab
    invoke-direct {v2, p0, p1, v1, v3}, Landroid/mtp/MtpPropertyGroup$Property;-><init>(Landroid/mtp/MtpPropertyGroup;III)V

    #@ae
    goto :goto_2e

    #@af
    .line 94
    nop

    #@b0
    :sswitch_data_b0
    .sparse-switch
        0xdc01 -> :sswitch_2f
        0xdc02 -> :sswitch_34
        0xdc03 -> :sswitch_38
        0xdc04 -> :sswitch_3a
        0xdc07 -> :sswitch_3f
        0xdc08 -> :sswitch_a1
        0xdc09 -> :sswitch_4c
        0xdc0b -> :sswitch_59
        0xdc41 -> :sswitch_5e
        0xdc44 -> :sswitch_45
        0xdc46 -> :sswitch_73
        0xdc47 -> :sswitch_9a
        0xdc48 -> :sswitch_8b
        0xdc87 -> :sswitch_91
        0xdc88 -> :sswitch_96
        0xdc89 -> :sswitch_64
        0xdc8b -> :sswitch_68
        0xdc8c -> :sswitch_81
        0xdc96 -> :sswitch_85
        0xdc99 -> :sswitch_52
        0xdc9a -> :sswitch_77
        0xdc9b -> :sswitch_7b
        0xdce0 -> :sswitch_6d
    .end sparse-switch
.end method

.method private native format_date_time(J)Ljava/lang/String;
.end method

.method private static nameFromPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "path"

    #@0
    .prologue
    .line 292
    const/4 v2, 0x0

    #@1
    .line 293
    .local v2, start:I
    const/16 v3, 0x2f

    #@3
    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@6
    move-result v1

    #@7
    .line 294
    .local v1, lastSlash:I
    if-ltz v1, :cond_b

    #@9
    .line 295
    add-int/lit8 v2, v1, 0x1

    #@b
    .line 297
    :cond_b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v0

    #@f
    .line 298
    .local v0, end:I
    sub-int v3, v0, v2

    #@11
    const/16 v4, 0xff

    #@13
    if-le v3, v4, :cond_17

    #@15
    .line 299
    add-int/lit16 v0, v2, 0xff

    #@17
    .line 301
    :cond_17
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    return-object v3
.end method

.method private queryAudio(ILjava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "id"
    .parameter "column"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 230
    const/4 v7, 0x0

    #@2
    .line 232
    .local v7, c:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@4
    iget-object v1, p0, Landroid/mtp/MtpPropertyGroup;->mVolumeName:Ljava/lang/String;

    #@6
    invoke-static {v1}, Landroid/provider/MediaStore$Audio$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@9
    move-result-object v1

    #@a
    const/4 v2, 0x2

    #@b
    new-array v2, v2, [Ljava/lang/String;

    #@d
    const/4 v3, 0x0

    #@e
    const-string v4, "_id"

    #@10
    aput-object v4, v2, v3

    #@12
    const/4 v3, 0x1

    #@13
    aput-object p2, v2, v3

    #@15
    const-string v3, "_id=?"

    #@17
    const/4 v4, 0x1

    #@18
    new-array v4, v4, [Ljava/lang/String;

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    aput-object v6, v4, v5

    #@21
    const/4 v5, 0x0

    #@22
    const/4 v6, 0x0

    #@23
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@26
    move-result-object v7

    #@27
    .line 235
    if-eqz v7, :cond_3a

    #@29
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_3a

    #@2f
    .line 236
    const/4 v0, 0x1

    #@30
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_33
    .catchall {:try_start_2 .. :try_end_33} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_33} :catch_42

    #@33
    move-result-object v0

    #@34
    .line 243
    if-eqz v7, :cond_39

    #@36
    .line 244
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@39
    :cond_39
    :goto_39
    return-object v0

    #@3a
    .line 238
    :cond_3a
    :try_start_3a
    const-string v0, ""
    :try_end_3c
    .catchall {:try_start_3a .. :try_end_3c} :catchall_4a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3c} :catch_42

    #@3c
    .line 243
    if-eqz v7, :cond_39

    #@3e
    .line 244
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@41
    goto :goto_39

    #@42
    .line 240
    :catch_42
    move-exception v8

    #@43
    .line 243
    .local v8, e:Ljava/lang/Exception;
    if-eqz v7, :cond_48

    #@45
    .line 244
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@48
    :cond_48
    move-object v0, v9

    #@49
    goto :goto_39

    #@4a
    .line 243
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_4a
    move-exception v0

    #@4b
    if-eqz v7, :cond_50

    #@4d
    .line 244
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@50
    :cond_50
    throw v0
.end method

.method private queryGenre(I)Ljava/lang/String;
    .registers 12
    .parameter "id"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 250
    const/4 v7, 0x0

    #@2
    .line 252
    .local v7, c:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Landroid/mtp/MtpPropertyGroup;->mVolumeName:Ljava/lang/String;

    #@4
    invoke-static {v0, p1}, Landroid/provider/MediaStore$Audio$Genres;->getContentUriForAudioId(Ljava/lang/String;I)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 253
    .local v1, uri:Landroid/net/Uri;
    iget-object v0, p0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@a
    const/4 v2, 0x2

    #@b
    new-array v2, v2, [Ljava/lang/String;

    #@d
    const/4 v3, 0x0

    #@e
    const-string v4, "_id"

    #@10
    aput-object v4, v2, v3

    #@12
    const/4 v3, 0x1

    #@13
    const-string/jumbo v4, "name"

    #@16
    aput-object v4, v2, v3

    #@18
    const/4 v3, 0x0

    #@19
    const/4 v4, 0x0

    #@1a
    const/4 v5, 0x0

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1f
    move-result-object v7

    #@20
    .line 256
    if-eqz v7, :cond_33

    #@22
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_33

    #@28
    .line 257
    const/4 v0, 0x1

    #@29
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_2 .. :try_end_2c} :catchall_4b
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2c} :catch_3b

    #@2c
    move-result-object v0

    #@2d
    .line 265
    if-eqz v7, :cond_32

    #@2f
    .line 266
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@32
    .end local v1           #uri:Landroid/net/Uri;
    :cond_32
    :goto_32
    return-object v0

    #@33
    .line 259
    .restart local v1       #uri:Landroid/net/Uri;
    :cond_33
    :try_start_33
    const-string v0, ""
    :try_end_35
    .catchall {:try_start_33 .. :try_end_35} :catchall_4b
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_35} :catch_3b

    #@35
    .line 265
    if-eqz v7, :cond_32

    #@37
    .line 266
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3a
    goto :goto_32

    #@3b
    .line 261
    .end local v1           #uri:Landroid/net/Uri;
    :catch_3b
    move-exception v8

    #@3c
    .line 262
    .local v8, e:Ljava/lang/Exception;
    :try_start_3c
    const-string v0, "MtpPropertyGroup"

    #@3e
    const-string/jumbo v2, "queryGenre exception"

    #@41
    invoke-static {v0, v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_44
    .catchall {:try_start_3c .. :try_end_44} :catchall_4b

    #@44
    .line 265
    if-eqz v7, :cond_49

    #@46
    .line 266
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@49
    :cond_49
    move-object v0, v9

    #@4a
    goto :goto_32

    #@4b
    .line 265
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_4b
    move-exception v0

    #@4c
    if-eqz v7, :cond_51

    #@4e
    .line 266
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@51
    :cond_51
    throw v0
.end method

.method private queryLong(ILjava/lang/String;)Ljava/lang/Long;
    .registers 12
    .parameter "id"
    .parameter "column"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 272
    const/4 v7, 0x0

    #@2
    .line 275
    .local v7, c:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@4
    iget-object v1, p0, Landroid/mtp/MtpPropertyGroup;->mUri:Landroid/net/Uri;

    #@6
    const/4 v2, 0x2

    #@7
    new-array v2, v2, [Ljava/lang/String;

    #@9
    const/4 v3, 0x0

    #@a
    const-string v4, "_id"

    #@c
    aput-object v4, v2, v3

    #@e
    const/4 v3, 0x1

    #@f
    aput-object p2, v2, v3

    #@11
    const-string v3, "_id=?"

    #@13
    const/4 v4, 0x1

    #@14
    new-array v4, v4, [Ljava/lang/String;

    #@16
    const/4 v5, 0x0

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    aput-object v6, v4, v5

    #@1d
    const/4 v5, 0x0

    #@1e
    const/4 v6, 0x0

    #@1f
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@22
    move-result-object v7

    #@23
    .line 278
    if-eqz v7, :cond_3b

    #@25
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_3b

    #@2b
    .line 279
    new-instance v0, Ljava/lang/Long;

    #@2d
    const/4 v1, 0x1

    #@2e
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    #@31
    move-result-wide v1

    #@32
    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V
    :try_end_35
    .catchall {:try_start_2 .. :try_end_35} :catchall_49
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_35} :catch_42

    #@35
    .line 283
    if-eqz v7, :cond_3a

    #@37
    .line 284
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3a
    .line 287
    :cond_3a
    :goto_3a
    return-object v0

    #@3b
    .line 283
    :cond_3b
    if-eqz v7, :cond_40

    #@3d
    .line 284
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@40
    :cond_40
    :goto_40
    move-object v0, v8

    #@41
    .line 287
    goto :goto_3a

    #@42
    .line 281
    :catch_42
    move-exception v0

    #@43
    .line 283
    if-eqz v7, :cond_40

    #@45
    .line 284
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@48
    goto :goto_40

    #@49
    .line 283
    :catchall_49
    move-exception v0

    #@4a
    if-eqz v7, :cond_4f

    #@4c
    .line 284
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4f
    :cond_4f
    throw v0
.end method

.method private queryString(ILjava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "id"
    .parameter "column"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 209
    const/4 v7, 0x0

    #@2
    .line 212
    .local v7, c:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@4
    iget-object v1, p0, Landroid/mtp/MtpPropertyGroup;->mUri:Landroid/net/Uri;

    #@6
    const/4 v2, 0x2

    #@7
    new-array v2, v2, [Ljava/lang/String;

    #@9
    const/4 v3, 0x0

    #@a
    const-string v4, "_id"

    #@c
    aput-object v4, v2, v3

    #@e
    const/4 v3, 0x1

    #@f
    aput-object p2, v2, v3

    #@11
    const-string v3, "_id=?"

    #@13
    const/4 v4, 0x1

    #@14
    new-array v4, v4, [Ljava/lang/String;

    #@16
    const/4 v5, 0x0

    #@17
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1a
    move-result-object v6

    #@1b
    aput-object v6, v4, v5

    #@1d
    const/4 v5, 0x0

    #@1e
    const/4 v6, 0x0

    #@1f
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@22
    move-result-object v7

    #@23
    .line 215
    if-eqz v7, :cond_36

    #@25
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_36

    #@2b
    .line 216
    const/4 v0, 0x1

    #@2c
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2f
    .catchall {:try_start_2 .. :try_end_2f} :catchall_46
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2f} :catch_3e

    #@2f
    move-result-object v0

    #@30
    .line 223
    if-eqz v7, :cond_35

    #@32
    .line 224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@35
    :cond_35
    :goto_35
    return-object v0

    #@36
    .line 218
    :cond_36
    :try_start_36
    const-string v0, ""
    :try_end_38
    .catchall {:try_start_36 .. :try_end_38} :catchall_46
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_38} :catch_3e

    #@38
    .line 223
    if-eqz v7, :cond_35

    #@3a
    .line 224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3d
    goto :goto_35

    #@3e
    .line 220
    :catch_3e
    move-exception v8

    #@3f
    .line 223
    .local v8, e:Ljava/lang/Exception;
    if-eqz v7, :cond_44

    #@41
    .line 224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@44
    :cond_44
    move-object v0, v9

    #@45
    goto :goto_35

    #@46
    .line 223
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_46
    move-exception v0

    #@47
    if-eqz v7, :cond_4c

    #@49
    .line 224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4c
    :cond_4c
    throw v0
.end method


# virtual methods
.method getPropertyList(III)Landroid/mtp/MtpPropertyList;
    .registers 35
    .parameter "handle"
    .parameter "format"
    .parameter "depth"

    #@0
    .prologue
    .line 306
    const/4 v2, 0x1

    #@1
    move/from16 v0, p3

    #@3
    if-le v0, v2, :cond_f

    #@5
    .line 309
    new-instance v7, Landroid/mtp/MtpPropertyList;

    #@7
    const/4 v2, 0x0

    #@8
    const v3, 0xa808

    #@b
    invoke-direct {v7, v2, v3}, Landroid/mtp/MtpPropertyList;-><init>(II)V

    #@e
    .line 475
    :cond_e
    :goto_e
    return-object v7

    #@f
    .line 314
    :cond_f
    if-nez p2, :cond_61

    #@11
    .line 315
    const/4 v2, -0x1

    #@12
    move/from16 v0, p1

    #@14
    if-ne v0, v2, :cond_4b

    #@16
    .line 317
    const/4 v5, 0x0

    #@17
    .line 318
    .local v5, where:Ljava/lang/String;
    const/4 v6, 0x0

    #@18
    .line 342
    .local v6, whereArgs:[Ljava/lang/String;
    :goto_18
    const/16 v19, 0x0

    #@1a
    .line 345
    .local v19, c:Landroid/database/Cursor;
    if-gtz p3, :cond_29

    #@1c
    const/4 v2, -0x1

    #@1d
    move/from16 v0, p1

    #@1f
    if-eq v0, v2, :cond_29

    #@21
    :try_start_21
    move-object/from16 v0, p0

    #@23
    iget-object v2, v0, Landroid/mtp/MtpPropertyGroup;->mColumns:[Ljava/lang/String;

    #@25
    array-length v2, v2

    #@26
    const/4 v3, 0x1

    #@27
    if-le v2, v3, :cond_90

    #@29
    .line 346
    :cond_29
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Landroid/mtp/MtpPropertyGroup;->mProvider:Landroid/content/IContentProvider;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v3, v0, Landroid/mtp/MtpPropertyGroup;->mUri:Landroid/net/Uri;

    #@31
    move-object/from16 v0, p0

    #@33
    iget-object v4, v0, Landroid/mtp/MtpPropertyGroup;->mColumns:[Ljava/lang/String;

    #@35
    const/4 v7, 0x0

    #@36
    const/4 v8, 0x0

    #@37
    invoke-interface/range {v2 .. v8}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@3a
    move-result-object v19

    #@3b
    .line 347
    if-nez v19, :cond_90

    #@3d
    .line 348
    new-instance v7, Landroid/mtp/MtpPropertyList;

    #@3f
    const/4 v2, 0x0

    #@40
    const/16 v3, 0x2009

    #@42
    invoke-direct {v7, v2, v3}, Landroid/mtp/MtpPropertyList;-><init>(II)V
    :try_end_45
    .catchall {:try_start_21 .. :try_end_45} :catchall_11a
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_45} :catch_fa

    #@45
    .line 474
    if-eqz v19, :cond_e

    #@47
    .line 475
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@4a
    goto :goto_e

    #@4b
    .line 320
    .end local v5           #where:Ljava/lang/String;
    .end local v6           #whereArgs:[Ljava/lang/String;
    .end local v19           #c:Landroid/database/Cursor;
    :cond_4b
    const/4 v2, 0x1

    #@4c
    new-array v6, v2, [Ljava/lang/String;

    #@4e
    const/4 v2, 0x0

    #@4f
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    aput-object v3, v6, v2

    #@55
    .line 321
    .restart local v6       #whereArgs:[Ljava/lang/String;
    const/4 v2, 0x1

    #@56
    move/from16 v0, p3

    #@58
    if-ne v0, v2, :cond_5e

    #@5a
    .line 322
    const-string/jumbo v5, "parent=?"

    #@5d
    .restart local v5       #where:Ljava/lang/String;
    goto :goto_18

    #@5e
    .line 324
    .end local v5           #where:Ljava/lang/String;
    :cond_5e
    const-string v5, "_id=?"

    #@60
    .restart local v5       #where:Ljava/lang/String;
    goto :goto_18

    #@61
    .line 328
    .end local v5           #where:Ljava/lang/String;
    .end local v6           #whereArgs:[Ljava/lang/String;
    :cond_61
    const/4 v2, -0x1

    #@62
    move/from16 v0, p1

    #@64
    if-ne v0, v2, :cond_73

    #@66
    .line 330
    const-string v5, "format=?"

    #@68
    .line 331
    .restart local v5       #where:Ljava/lang/String;
    const/4 v2, 0x1

    #@69
    new-array v6, v2, [Ljava/lang/String;

    #@6b
    const/4 v2, 0x0

    #@6c
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6f
    move-result-object v3

    #@70
    aput-object v3, v6, v2

    #@72
    .restart local v6       #whereArgs:[Ljava/lang/String;
    goto :goto_18

    #@73
    .line 333
    .end local v5           #where:Ljava/lang/String;
    .end local v6           #whereArgs:[Ljava/lang/String;
    :cond_73
    const/4 v2, 0x2

    #@74
    new-array v6, v2, [Ljava/lang/String;

    #@76
    const/4 v2, 0x0

    #@77
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    aput-object v3, v6, v2

    #@7d
    const/4 v2, 0x1

    #@7e
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@81
    move-result-object v3

    #@82
    aput-object v3, v6, v2

    #@84
    .line 334
    .restart local v6       #whereArgs:[Ljava/lang/String;
    const/4 v2, 0x1

    #@85
    move/from16 v0, p3

    #@87
    if-ne v0, v2, :cond_8d

    #@89
    .line 335
    const-string/jumbo v5, "parent=? AND format=?"

    #@8c
    .restart local v5       #where:Ljava/lang/String;
    goto :goto_18

    #@8d
    .line 337
    .end local v5           #where:Ljava/lang/String;
    :cond_8d
    const-string v5, "_id=? AND format=?"

    #@8f
    .restart local v5       #where:Ljava/lang/String;
    goto :goto_18

    #@90
    .line 352
    .restart local v19       #c:Landroid/database/Cursor;
    :cond_90
    if-nez v19, :cond_ec

    #@92
    const/16 v21, 0x1

    #@94
    .line 353
    .local v21, count:I
    :goto_94
    :try_start_94
    new-instance v7, Landroid/mtp/MtpPropertyList;

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v2, v0, Landroid/mtp/MtpPropertyGroup;->mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

    #@9a
    array-length v2, v2

    #@9b
    mul-int v2, v2, v21

    #@9d
    const/16 v3, 0x2001

    #@9f
    invoke-direct {v7, v2, v3}, Landroid/mtp/MtpPropertyList;-><init>(II)V

    #@a2
    .line 357
    .local v7, result:Landroid/mtp/MtpPropertyList;
    const/16 v26, 0x0

    #@a4
    .local v26, objectIndex:I
    :goto_a4
    move/from16 v0, v26

    #@a6
    move/from16 v1, v21

    #@a8
    if-ge v0, v1, :cond_27a

    #@aa
    .line 358
    if-eqz v19, :cond_b9

    #@ac
    .line 359
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    #@af
    .line 360
    const/4 v2, 0x0

    #@b0
    move-object/from16 v0, v19

    #@b2
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    #@b5
    move-result-wide v2

    #@b6
    long-to-int v0, v2

    #@b7
    move/from16 p1, v0

    #@b9
    .line 364
    :cond_b9
    const/16 v28, 0x0

    #@bb
    .local v28, propertyIndex:I
    :goto_bb
    move-object/from16 v0, p0

    #@bd
    iget-object v2, v0, Landroid/mtp/MtpPropertyGroup;->mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

    #@bf
    array-length v2, v2

    #@c0
    move/from16 v0, v28

    #@c2
    if-ge v0, v2, :cond_276

    #@c4
    .line 365
    move-object/from16 v0, p0

    #@c6
    iget-object v2, v0, Landroid/mtp/MtpPropertyGroup;->mProperties:[Landroid/mtp/MtpPropertyGroup$Property;

    #@c8
    aget-object v27, v2, v28

    #@ca
    .line 366
    .local v27, property:Landroid/mtp/MtpPropertyGroup$Property;
    move-object/from16 v0, v27

    #@cc
    iget v9, v0, Landroid/mtp/MtpPropertyGroup$Property;->code:I

    #@ce
    .line 367
    .local v9, propertyCode:I
    move-object/from16 v0, v27

    #@d0
    iget v0, v0, Landroid/mtp/MtpPropertyGroup$Property;->column:I

    #@d2
    move/from16 v20, v0

    #@d4
    .line 370
    .local v20, column:I
    sparse-switch v9, :sswitch_data_282

    #@d7
    .line 457
    move-object/from16 v0, v27

    #@d9
    iget v2, v0, Landroid/mtp/MtpPropertyGroup$Property;->type:I

    #@db
    const v3, 0xffff

    #@de
    if-ne v2, v3, :cond_24c

    #@e0
    .line 458
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    move/from16 v0, p1

    #@e6
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@e9
    .line 364
    :goto_e9
    add-int/lit8 v28, v28, 0x1

    #@eb
    goto :goto_bb

    #@ec
    .line 352
    .end local v7           #result:Landroid/mtp/MtpPropertyList;
    .end local v9           #propertyCode:I
    .end local v20           #column:I
    .end local v21           #count:I
    .end local v26           #objectIndex:I
    .end local v27           #property:Landroid/mtp/MtpPropertyGroup$Property;
    .end local v28           #propertyIndex:I
    :cond_ec
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    #@ef
    move-result v21

    #@f0
    goto :goto_94

    #@f1
    .line 373
    .restart local v7       #result:Landroid/mtp/MtpPropertyList;
    .restart local v9       #propertyCode:I
    .restart local v20       #column:I
    .restart local v21       #count:I
    .restart local v26       #objectIndex:I
    .restart local v27       #property:Landroid/mtp/MtpPropertyGroup$Property;
    .restart local v28       #propertyIndex:I
    :sswitch_f1
    const/4 v10, 0x4

    #@f2
    const-wide/16 v11, 0x0

    #@f4
    move/from16 v8, p1

    #@f6
    invoke-virtual/range {v7 .. v12}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V
    :try_end_f9
    .catchall {:try_start_94 .. :try_end_f9} :catchall_11a
    .catch Landroid/os/RemoteException; {:try_start_94 .. :try_end_f9} :catch_fa

    #@f9
    goto :goto_e9

    #@fa
    .line 471
    .end local v7           #result:Landroid/mtp/MtpPropertyList;
    .end local v9           #propertyCode:I
    .end local v20           #column:I
    .end local v21           #count:I
    .end local v26           #objectIndex:I
    .end local v27           #property:Landroid/mtp/MtpPropertyGroup$Property;
    .end local v28           #propertyIndex:I
    :catch_fa
    move-exception v23

    #@fb
    .line 472
    .local v23, e:Landroid/os/RemoteException;
    :try_start_fb
    new-instance v7, Landroid/mtp/MtpPropertyList;

    #@fd
    const/4 v2, 0x0

    #@fe
    const/16 v3, 0x2002

    #@100
    invoke-direct {v7, v2, v3}, Landroid/mtp/MtpPropertyList;-><init>(II)V
    :try_end_103
    .catchall {:try_start_fb .. :try_end_103} :catchall_11a

    #@103
    .line 474
    if-eqz v19, :cond_e

    #@105
    .line 475
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@108
    goto/16 :goto_e

    #@10a
    .line 377
    .end local v23           #e:Landroid/os/RemoteException;
    .restart local v7       #result:Landroid/mtp/MtpPropertyList;
    .restart local v9       #propertyCode:I
    .restart local v20       #column:I
    .restart local v21       #count:I
    .restart local v26       #objectIndex:I
    .restart local v27       #property:Landroid/mtp/MtpPropertyGroup$Property;
    .restart local v28       #propertyIndex:I
    :sswitch_10a
    :try_start_10a
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@10d
    move-result-object v29

    #@10e
    .line 378
    .local v29, value:Ljava/lang/String;
    if-eqz v29, :cond_121

    #@110
    .line 379
    invoke-static/range {v29 .. v29}, Landroid/mtp/MtpPropertyGroup;->nameFromPath(Ljava/lang/String;)Ljava/lang/String;

    #@113
    move-result-object v2

    #@114
    move/from16 v0, p1

    #@116
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V
    :try_end_119
    .catchall {:try_start_10a .. :try_end_119} :catchall_11a
    .catch Landroid/os/RemoteException; {:try_start_10a .. :try_end_119} :catch_fa

    #@119
    goto :goto_e9

    #@11a
    .line 474
    .end local v7           #result:Landroid/mtp/MtpPropertyList;
    .end local v9           #propertyCode:I
    .end local v20           #column:I
    .end local v21           #count:I
    .end local v26           #objectIndex:I
    .end local v27           #property:Landroid/mtp/MtpPropertyGroup$Property;
    .end local v28           #propertyIndex:I
    .end local v29           #value:Ljava/lang/String;
    :catchall_11a
    move-exception v2

    #@11b
    if-eqz v19, :cond_120

    #@11d
    .line 475
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@120
    :cond_120
    throw v2

    #@121
    .line 381
    .restart local v7       #result:Landroid/mtp/MtpPropertyList;
    .restart local v9       #propertyCode:I
    .restart local v20       #column:I
    .restart local v21       #count:I
    .restart local v26       #objectIndex:I
    .restart local v27       #property:Landroid/mtp/MtpPropertyGroup$Property;
    .restart local v28       #propertyIndex:I
    .restart local v29       #value:Ljava/lang/String;
    :cond_121
    const/16 v2, 0x2009

    #@123
    :try_start_123
    invoke-virtual {v7, v2}, Landroid/mtp/MtpPropertyList;->setResult(I)V

    #@126
    goto :goto_e9

    #@127
    .line 386
    .end local v29           #value:Ljava/lang/String;
    :sswitch_127
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@12a
    move-result-object v25

    #@12b
    .line 388
    .local v25, name:Ljava/lang/String;
    if-nez v25, :cond_138

    #@12d
    .line 389
    const-string/jumbo v2, "name"

    #@130
    move-object/from16 v0, p0

    #@132
    move/from16 v1, p1

    #@134
    invoke-direct {v0, v1, v2}, Landroid/mtp/MtpPropertyGroup;->queryString(ILjava/lang/String;)Ljava/lang/String;

    #@137
    move-result-object v25

    #@138
    .line 392
    :cond_138
    if-nez v25, :cond_14a

    #@13a
    .line 393
    const-string v2, "_data"

    #@13c
    move-object/from16 v0, p0

    #@13e
    move/from16 v1, p1

    #@140
    invoke-direct {v0, v1, v2}, Landroid/mtp/MtpPropertyGroup;->queryString(ILjava/lang/String;)Ljava/lang/String;

    #@143
    move-result-object v25

    #@144
    .line 394
    if-eqz v25, :cond_14a

    #@146
    .line 395
    invoke-static/range {v25 .. v25}, Landroid/mtp/MtpPropertyGroup;->nameFromPath(Ljava/lang/String;)Ljava/lang/String;

    #@149
    move-result-object v25

    #@14a
    .line 398
    :cond_14a
    if-eqz v25, :cond_154

    #@14c
    .line 399
    move/from16 v0, p1

    #@14e
    move-object/from16 v1, v25

    #@150
    invoke-virtual {v7, v0, v9, v1}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@153
    goto :goto_e9

    #@154
    .line 401
    :cond_154
    const/16 v2, 0x2009

    #@156
    invoke-virtual {v7, v2}, Landroid/mtp/MtpPropertyList;->setResult(I)V

    #@159
    goto :goto_e9

    #@15a
    .line 407
    .end local v25           #name:Ljava/lang/String;
    :sswitch_15a
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@15d
    move-result v2

    #@15e
    int-to-long v2, v2

    #@15f
    move-object/from16 v0, p0

    #@161
    invoke-direct {v0, v2, v3}, Landroid/mtp/MtpPropertyGroup;->format_date_time(J)Ljava/lang/String;

    #@164
    move-result-object v2

    #@165
    move/from16 v0, p1

    #@167
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@16a
    goto/16 :goto_e9

    #@16c
    .line 411
    :sswitch_16c
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@16f
    move-result v30

    #@170
    .line 412
    .local v30, year:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@172
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@175
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@178
    move-result-object v3

    #@179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v2

    #@17d
    const-string v3, "0101T000000"

    #@17f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v2

    #@183
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@186
    move-result-object v22

    #@187
    .line 413
    .local v22, dateTime:Ljava/lang/String;
    move/from16 v0, p1

    #@189
    move-object/from16 v1, v22

    #@18b
    invoke-virtual {v7, v0, v9, v1}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@18e
    goto/16 :goto_e9

    #@190
    .line 417
    .end local v22           #dateTime:Ljava/lang/String;
    .end local v30           #year:I
    :sswitch_190
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getLong(I)J

    #@193
    move-result-wide v11

    #@194
    .line 418
    .local v11, puid:J
    const/16 v2, 0x20

    #@196
    shl-long/2addr v11, v2

    #@197
    .line 419
    move/from16 v0, p1

    #@199
    int-to-long v2, v0

    #@19a
    add-long/2addr v11, v2

    #@19b
    .line 420
    const/16 v10, 0xa

    #@19d
    move/from16 v8, p1

    #@19f
    invoke-virtual/range {v7 .. v12}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V

    #@1a2
    goto/16 :goto_e9

    #@1a4
    .line 423
    .end local v11           #puid:J
    :sswitch_1a4
    const/16 v16, 0x4

    #@1a6
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@1a9
    move-result v2

    #@1aa
    rem-int/lit16 v2, v2, 0x3e8

    #@1ac
    int-to-long v0, v2

    #@1ad
    move-wide/from16 v17, v0

    #@1af
    move-object v13, v7

    #@1b0
    move/from16 v14, p1

    #@1b2
    move v15, v9

    #@1b3
    invoke-virtual/range {v13 .. v18}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V

    #@1b6
    goto/16 :goto_e9

    #@1b8
    .line 427
    :sswitch_1b8
    const-string v2, "artist"

    #@1ba
    move-object/from16 v0, p0

    #@1bc
    move/from16 v1, p1

    #@1be
    invoke-direct {v0, v1, v2}, Landroid/mtp/MtpPropertyGroup;->queryAudio(ILjava/lang/String;)Ljava/lang/String;

    #@1c1
    move-result-object v2

    #@1c2
    move/from16 v0, p1

    #@1c4
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@1c7
    goto/16 :goto_e9

    #@1c9
    .line 431
    :sswitch_1c9
    const-string v2, "album"

    #@1cb
    move-object/from16 v0, p0

    #@1cd
    move/from16 v1, p1

    #@1cf
    invoke-direct {v0, v1, v2}, Landroid/mtp/MtpPropertyGroup;->queryAudio(ILjava/lang/String;)Ljava/lang/String;

    #@1d2
    move-result-object v2

    #@1d3
    move/from16 v0, p1

    #@1d5
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@1d8
    goto/16 :goto_e9

    #@1da
    .line 435
    :sswitch_1da
    invoke-direct/range {p0 .. p1}, Landroid/mtp/MtpPropertyGroup;->queryGenre(I)Ljava/lang/String;

    #@1dd
    move-result-object v24

    #@1de
    .line 436
    .local v24, genre:Ljava/lang/String;
    if-eqz v24, :cond_1e9

    #@1e0
    .line 437
    move/from16 v0, p1

    #@1e2
    move-object/from16 v1, v24

    #@1e4
    invoke-virtual {v7, v0, v9, v1}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@1e7
    goto/16 :goto_e9

    #@1e9
    .line 439
    :cond_1e9
    const/16 v2, 0x2009

    #@1eb
    invoke-virtual {v7, v2}, Landroid/mtp/MtpPropertyList;->setResult(I)V

    #@1ee
    goto/16 :goto_e9

    #@1f0
    .line 444
    .end local v24           #genre:Ljava/lang/String;
    :sswitch_1f0
    const/16 v16, 0x6

    #@1f2
    if-eqz v19, :cond_204

    #@1f4
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@1f7
    move-result v2

    #@1f8
    :goto_1f8
    int-to-long v0, v2

    #@1f9
    move-wide/from16 v17, v0

    #@1fb
    move-object v13, v7

    #@1fc
    move/from16 v14, p1

    #@1fe
    move v15, v9

    #@1ff
    invoke-virtual/range {v13 .. v18}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V

    #@202
    goto/16 :goto_e9

    #@204
    :cond_204
    const/4 v2, 0x0

    #@205
    goto :goto_1f8

    #@206
    .line 447
    :sswitch_206
    const/16 v16, 0x6

    #@208
    if-eqz v19, :cond_21a

    #@20a
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@20d
    move-result v2

    #@20e
    :goto_20e
    int-to-long v0, v2

    #@20f
    move-wide/from16 v17, v0

    #@211
    move-object v13, v7

    #@212
    move/from16 v14, p1

    #@214
    move v15, v9

    #@215
    invoke-virtual/range {v13 .. v18}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V

    #@218
    goto/16 :goto_e9

    #@21a
    :cond_21a
    const/4 v2, 0x0

    #@21b
    goto :goto_20e

    #@21c
    .line 450
    :sswitch_21c
    if-eqz v19, :cond_232

    #@21e
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getLong(I)J

    #@221
    move-result-wide v2

    #@222
    const-wide/16 v13, 0x3e8

    #@224
    div-long/2addr v2, v13

    #@225
    :goto_225
    move-object/from16 v0, p0

    #@227
    invoke-direct {v0, v2, v3}, Landroid/mtp/MtpPropertyGroup;->format_date_time(J)Ljava/lang/String;

    #@22a
    move-result-object v2

    #@22b
    move/from16 v0, p1

    #@22d
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@230
    goto/16 :goto_e9

    #@232
    :cond_232
    const-wide/16 v2, 0x0

    #@234
    goto :goto_225

    #@235
    .line 453
    :sswitch_235
    if-eqz v19, :cond_249

    #@237
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getInt(I)I

    #@23a
    move-result v2

    #@23b
    int-to-long v2, v2

    #@23c
    :goto_23c
    move-object/from16 v0, p0

    #@23e
    invoke-direct {v0, v2, v3}, Landroid/mtp/MtpPropertyGroup;->format_date_time(J)Ljava/lang/String;

    #@241
    move-result-object v2

    #@242
    move/from16 v0, p1

    #@244
    invoke-virtual {v7, v0, v9, v2}, Landroid/mtp/MtpPropertyList;->append(IILjava/lang/String;)V

    #@247
    goto/16 :goto_e9

    #@249
    :cond_249
    const-wide/16 v2, 0x0

    #@24b
    goto :goto_23c

    #@24c
    .line 459
    :cond_24c
    move-object/from16 v0, v27

    #@24e
    iget v2, v0, Landroid/mtp/MtpPropertyGroup$Property;->type:I

    #@250
    if-nez v2, :cond_263

    #@252
    .line 460
    move-object/from16 v0, v27

    #@254
    iget v0, v0, Landroid/mtp/MtpPropertyGroup$Property;->type:I

    #@256
    move/from16 v16, v0

    #@258
    const-wide/16 v17, 0x0

    #@25a
    move-object v13, v7

    #@25b
    move/from16 v14, p1

    #@25d
    move v15, v9

    #@25e
    invoke-virtual/range {v13 .. v18}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V

    #@261
    goto/16 :goto_e9

    #@263
    .line 462
    :cond_263
    move-object/from16 v0, v27

    #@265
    iget v0, v0, Landroid/mtp/MtpPropertyGroup$Property;->type:I

    #@267
    move/from16 v16, v0

    #@269
    invoke-interface/range {v19 .. v20}, Landroid/database/Cursor;->getLong(I)J

    #@26c
    move-result-wide v17

    #@26d
    move-object v13, v7

    #@26e
    move/from16 v14, p1

    #@270
    move v15, v9

    #@271
    invoke-virtual/range {v13 .. v18}, Landroid/mtp/MtpPropertyList;->append(IIIJ)V
    :try_end_274
    .catchall {:try_start_123 .. :try_end_274} :catchall_11a
    .catch Landroid/os/RemoteException; {:try_start_123 .. :try_end_274} :catch_fa

    #@274
    goto/16 :goto_e9

    #@276
    .line 357
    .end local v9           #propertyCode:I
    .end local v20           #column:I
    .end local v27           #property:Landroid/mtp/MtpPropertyGroup$Property;
    :cond_276
    add-int/lit8 v26, v26, 0x1

    #@278
    goto/16 :goto_a4

    #@27a
    .line 474
    .end local v28           #propertyIndex:I
    :cond_27a
    if-eqz v19, :cond_e

    #@27c
    .line 475
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@27f
    goto/16 :goto_e

    #@281
    .line 370
    nop

    #@282
    :sswitch_data_282
    .sparse-switch
        0xdc03 -> :sswitch_f1
        0xdc07 -> :sswitch_10a
        0xdc08 -> :sswitch_235
        0xdc09 -> :sswitch_15a
        0xdc41 -> :sswitch_190
        0xdc44 -> :sswitch_127
        0xdc46 -> :sswitch_1b8
        0xdc47 -> :sswitch_21c
        0xdc87 -> :sswitch_1f0
        0xdc88 -> :sswitch_206
        0xdc8b -> :sswitch_1a4
        0xdc8c -> :sswitch_1da
        0xdc99 -> :sswitch_16c
        0xdc9a -> :sswitch_1c9
    .end sparse-switch
.end method
