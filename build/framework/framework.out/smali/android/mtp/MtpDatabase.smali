.class public Landroid/mtp/MtpDatabase;
.super Ljava/lang/Object;
.source "MtpDatabase.java"


# static fields
.field static final ALL_PROPERTIES:[I = null

.field static final AUDIO_PROPERTIES:[I = null

.field private static final DEVICE_PROPERTIES_DATABASE_VERSION:I = 0x1

.field static final FILE_PROPERTIES:[I = null

.field private static final FORMAT_PARENT_WHERE:Ljava/lang/String; = "format=? AND parent=?"

.field private static final FORMAT_WHERE:Ljava/lang/String; = "format=?"

.field private static final ID_PROJECTION:[Ljava/lang/String; = null

.field private static final ID_WHERE:Ljava/lang/String; = "_id=?"

.field static final IMAGE_PROPERTIES:[I = null

.field private static final OBJECT_INFO_PROJECTION:[Ljava/lang/String; = null

.field private static final PARENT_WHERE:Ljava/lang/String; = "parent=?"

.field private static final PATH_FORMAT_PROJECTION:[Ljava/lang/String; = null

.field private static final PATH_PROJECTION:[Ljava/lang/String; = null

.field private static final PATH_WHERE:Ljava/lang/String; = "_data=?"

.field private static final STORAGE_FORMAT_PARENT_WHERE:Ljava/lang/String; = "storage_id=? AND format=? AND parent=?"

.field private static final STORAGE_FORMAT_WHERE:Ljava/lang/String; = "storage_id=? AND format=?"

.field private static final STORAGE_PARENT_WHERE:Ljava/lang/String; = "storage_id=? AND parent=?"

.field private static final STORAGE_WHERE:Ljava/lang/String; = "storage_id=?"

.field private static final TAG:Ljava/lang/String; = "MtpDatabase"

.field static final VIDEO_PROPERTIES:[I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDatabaseModified:Z

.field private mDeviceProperties:Landroid/content/SharedPreferences;

.field private final mMediaProvider:Landroid/content/IContentProvider;

.field private final mMediaScanner:Landroid/media/MediaScanner;

.field private final mMediaStoragePath:Ljava/lang/String;

.field private mNativeContext:I

.field private final mObjectsUri:Landroid/net/Uri;

.field private final mPropertyGroupsByFormat:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/mtp/MtpPropertyGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final mPropertyGroupsByProperty:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/mtp/MtpPropertyGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final mStorageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/mtp/MtpStorage;",
            ">;"
        }
    .end annotation
.end field

.field private final mSubDirectories:[Ljava/lang/String;

.field private mSubDirectoriesWhere:Ljava/lang/String;

.field private mSubDirectoriesWhereArgs:[Ljava/lang/String;

.field private final mVolumeName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 90
    new-array v0, v3, [Ljava/lang/String;

    #@6
    const-string v1, "_id"

    #@8
    aput-object v1, v0, v2

    #@a
    sput-object v0, Landroid/mtp/MtpDatabase;->ID_PROJECTION:[Ljava/lang/String;

    #@c
    .line 93
    new-array v0, v4, [Ljava/lang/String;

    #@e
    const-string v1, "_id"

    #@10
    aput-object v1, v0, v2

    #@12
    const-string v1, "_data"

    #@14
    aput-object v1, v0, v3

    #@16
    sput-object v0, Landroid/mtp/MtpDatabase;->PATH_PROJECTION:[Ljava/lang/String;

    #@18
    .line 97
    new-array v0, v5, [Ljava/lang/String;

    #@1a
    const-string v1, "_id"

    #@1c
    aput-object v1, v0, v2

    #@1e
    const-string v1, "_data"

    #@20
    aput-object v1, v0, v3

    #@22
    const-string v1, "format"

    #@24
    aput-object v1, v0, v4

    #@26
    sput-object v0, Landroid/mtp/MtpDatabase;->PATH_FORMAT_PROJECTION:[Ljava/lang/String;

    #@28
    .line 102
    const/4 v0, 0x6

    #@29
    new-array v0, v0, [Ljava/lang/String;

    #@2b
    const-string v1, "_id"

    #@2d
    aput-object v1, v0, v2

    #@2f
    const-string/jumbo v1, "storage_id"

    #@32
    aput-object v1, v0, v3

    #@34
    const-string v1, "format"

    #@36
    aput-object v1, v0, v4

    #@38
    const-string/jumbo v1, "parent"

    #@3b
    aput-object v1, v0, v5

    #@3d
    const/4 v1, 0x4

    #@3e
    const-string v2, "_data"

    #@40
    aput-object v2, v0, v1

    #@42
    const/4 v1, 0x5

    #@43
    const-string v2, "date_modified"

    #@45
    aput-object v2, v0, v1

    #@47
    sput-object v0, Landroid/mtp/MtpDatabase;->OBJECT_INFO_PROJECTION:[Ljava/lang/String;

    #@49
    .line 128
    const-string/jumbo v0, "media_jni"

    #@4c
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@4f
    .line 546
    const/16 v0, 0xa

    #@51
    new-array v0, v0, [I

    #@53
    fill-array-data v0, :array_7e

    #@56
    sput-object v0, Landroid/mtp/MtpDatabase;->FILE_PROPERTIES:[I

    #@58
    .line 563
    const/16 v0, 0x13

    #@5a
    new-array v0, v0, [I

    #@5c
    fill-array-data v0, :array_96

    #@5f
    sput-object v0, Landroid/mtp/MtpDatabase;->AUDIO_PROPERTIES:[I

    #@61
    .line 590
    const/16 v0, 0x12

    #@63
    new-array v0, v0, [I

    #@65
    fill-array-data v0, :array_c0

    #@68
    sput-object v0, Landroid/mtp/MtpDatabase;->VIDEO_PROPERTIES:[I

    #@6a
    .line 623
    const/16 v0, 0xf

    #@6c
    new-array v0, v0, [I

    #@6e
    fill-array-data v0, :array_e8

    #@71
    sput-object v0, Landroid/mtp/MtpDatabase;->IMAGE_PROPERTIES:[I

    #@73
    .line 653
    const/16 v0, 0x1c

    #@75
    new-array v0, v0, [I

    #@77
    fill-array-data v0, :array_10a

    #@7a
    sput-object v0, Landroid/mtp/MtpDatabase;->ALL_PROPERTIES:[I

    #@7c
    return-void

    #@7d
    .line 546
    nop

    #@7e
    :array_7e
    .array-data 0x4
        0x1t 0xdct 0x0t 0x0t
        0x2t 0xdct 0x0t 0x0t
        0x3t 0xdct 0x0t 0x0t
        0x4t 0xdct 0x0t 0x0t
        0x7t 0xdct 0x0t 0x0t
        0x9t 0xdct 0x0t 0x0t
        0xbt 0xdct 0x0t 0x0t
        0x41t 0xdct 0x0t 0x0t
        0x44t 0xdct 0x0t 0x0t
        0x8t 0xdct 0x0t 0x0t
    .end array-data

    #@96
    .line 563
    :array_96
    .array-data 0x4
        0x1t 0xdct 0x0t 0x0t
        0x2t 0xdct 0x0t 0x0t
        0x3t 0xdct 0x0t 0x0t
        0x4t 0xdct 0x0t 0x0t
        0x7t 0xdct 0x0t 0x0t
        0x9t 0xdct 0x0t 0x0t
        0xbt 0xdct 0x0t 0x0t
        0x41t 0xdct 0x0t 0x0t
        0x44t 0xdct 0x0t 0x0t
        0xe0t 0xdct 0x0t 0x0t
        0x8t 0xdct 0x0t 0x0t
        0x46t 0xdct 0x0t 0x0t
        0x9at 0xdct 0x0t 0x0t
        0x9bt 0xdct 0x0t 0x0t
        0x8bt 0xdct 0x0t 0x0t
        0x99t 0xdct 0x0t 0x0t
        0x89t 0xdct 0x0t 0x0t
        0x8ct 0xdct 0x0t 0x0t
        0x96t 0xdct 0x0t 0x0t
    .end array-data

    #@c0
    .line 590
    :array_c0
    .array-data 0x4
        0x1t 0xdct 0x0t 0x0t
        0x2t 0xdct 0x0t 0x0t
        0x3t 0xdct 0x0t 0x0t
        0x4t 0xdct 0x0t 0x0t
        0x7t 0xdct 0x0t 0x0t
        0x9t 0xdct 0x0t 0x0t
        0xbt 0xdct 0x0t 0x0t
        0x41t 0xdct 0x0t 0x0t
        0x44t 0xdct 0x0t 0x0t
        0xe0t 0xdct 0x0t 0x0t
        0x8t 0xdct 0x0t 0x0t
        0x46t 0xdct 0x0t 0x0t
        0x9at 0xdct 0x0t 0x0t
        0x89t 0xdct 0x0t 0x0t
        0x48t 0xdct 0x0t 0x0t
        0x87t 0xdct 0x0t 0x0t
        0x88t 0xdct 0x0t 0x0t
        0x47t 0xdct 0x0t 0x0t
    .end array-data

    #@e8
    .line 623
    :array_e8
    .array-data 0x4
        0x1t 0xdct 0x0t 0x0t
        0x2t 0xdct 0x0t 0x0t
        0x3t 0xdct 0x0t 0x0t
        0x4t 0xdct 0x0t 0x0t
        0x7t 0xdct 0x0t 0x0t
        0x9t 0xdct 0x0t 0x0t
        0xbt 0xdct 0x0t 0x0t
        0x41t 0xdct 0x0t 0x0t
        0x44t 0xdct 0x0t 0x0t
        0xe0t 0xdct 0x0t 0x0t
        0x8t 0xdct 0x0t 0x0t
        0x48t 0xdct 0x0t 0x0t
        0x87t 0xdct 0x0t 0x0t
        0x88t 0xdct 0x0t 0x0t
        0x47t 0xdct 0x0t 0x0t
    .end array-data

    #@10a
    .line 653
    :array_10a
    .array-data 0x4
        0x1t 0xdct 0x0t 0x0t
        0x2t 0xdct 0x0t 0x0t
        0x3t 0xdct 0x0t 0x0t
        0x4t 0xdct 0x0t 0x0t
        0x7t 0xdct 0x0t 0x0t
        0x9t 0xdct 0x0t 0x0t
        0xbt 0xdct 0x0t 0x0t
        0x41t 0xdct 0x0t 0x0t
        0x44t 0xdct 0x0t 0x0t
        0xe0t 0xdct 0x0t 0x0t
        0x8t 0xdct 0x0t 0x0t
        0x48t 0xdct 0x0t 0x0t
        0x46t 0xdct 0x0t 0x0t
        0x9at 0xdct 0x0t 0x0t
        0x9bt 0xdct 0x0t 0x0t
        0x8bt 0xdct 0x0t 0x0t
        0x99t 0xdct 0x0t 0x0t
        0x89t 0xdct 0x0t 0x0t
        0x8ct 0xdct 0x0t 0x0t
        0x96t 0xdct 0x0t 0x0t
        0x46t 0xdct 0x0t 0x0t
        0x9at 0xdct 0x0t 0x0t
        0x89t 0xdct 0x0t 0x0t
        0x48t 0xdct 0x0t 0x0t
        0x48t 0xdct 0x0t 0x0t
        0x87t 0xdct 0x0t 0x0t
        0x88t 0xdct 0x0t 0x0t
        0x47t 0xdct 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .registers 18
    .parameter "context"
    .parameter "volumeName"
    .parameter "storagePath"
    .parameter "subDirectories"

    #@0
    .prologue
    .line 132
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 73
    new-instance v10, Ljava/util/HashMap;

    #@5
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mStorageMap:Ljava/util/HashMap;

    #@a
    .line 76
    new-instance v10, Ljava/util/HashMap;

    #@c
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByProperty:Ljava/util/HashMap;

    #@11
    .line 80
    new-instance v10, Ljava/util/HashMap;

    #@13
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    #@16
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByFormat:Ljava/util/HashMap;

    #@18
    .line 133
    invoke-direct {p0}, Landroid/mtp/MtpDatabase;->native_setup()V

    #@1b
    .line 135
    iput-object p1, p0, Landroid/mtp/MtpDatabase;->mContext:Landroid/content/Context;

    #@1d
    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v10

    #@21
    const-string/jumbo v11, "media"

    #@24
    invoke-virtual {v10, v11}, Landroid/content/ContentResolver;->acquireProvider(Ljava/lang/String;)Landroid/content/IContentProvider;

    #@27
    move-result-object v10

    #@28
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@2a
    .line 137
    iput-object p2, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@2c
    .line 138
    move-object/from16 v0, p3

    #@2e
    iput-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaStoragePath:Ljava/lang/String;

    #@30
    .line 139
    invoke-static {p2}, Landroid/provider/MediaStore$Files;->getMtpObjectsUri(Ljava/lang/String;)Landroid/net/Uri;

    #@33
    move-result-object v10

    #@34
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@36
    .line 140
    new-instance v10, Landroid/media/MediaScanner;

    #@38
    invoke-direct {v10, p1}, Landroid/media/MediaScanner;-><init>(Landroid/content/Context;)V

    #@3b
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mMediaScanner:Landroid/media/MediaScanner;

    #@3d
    .line 142
    move-object/from16 v0, p4

    #@3f
    iput-object v0, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@41
    .line 143
    if-eqz p4, :cond_9e

    #@43
    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    .line 146
    .local v1, builder:Ljava/lang/StringBuilder;
    const-string v10, "("

    #@4a
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    .line 147
    move-object/from16 v0, p4

    #@4f
    array-length v2, v0

    #@50
    .line 148
    .local v2, count:I
    const/4 v4, 0x0

    #@51
    .local v4, i:I
    :goto_51
    if-ge v4, v2, :cond_64

    #@53
    .line 149
    const-string v10, "_data=? OR _data LIKE ?"

    #@55
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 151
    add-int/lit8 v10, v2, -0x1

    #@5a
    if-eq v4, v10, :cond_61

    #@5c
    .line 152
    const-string v10, " OR "

    #@5e
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    .line 148
    :cond_61
    add-int/lit8 v4, v4, 0x1

    #@63
    goto :goto_51

    #@64
    .line 155
    :cond_64
    const-string v10, ")"

    #@66
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 156
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v10

    #@6d
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhere:Ljava/lang/String;

    #@6f
    .line 159
    mul-int/lit8 v10, v2, 0x2

    #@71
    new-array v10, v10, [Ljava/lang/String;

    #@73
    iput-object v10, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@75
    .line 160
    const/4 v4, 0x0

    #@76
    const/4 v5, 0x0

    #@77
    .local v5, j:I
    move v6, v5

    #@78
    .end local v5           #j:I
    .local v6, j:I
    :goto_78
    if-ge v4, v2, :cond_9e

    #@7a
    .line 161
    aget-object v9, p4, v4

    #@7c
    .line 162
    .local v9, path:Ljava/lang/String;
    iget-object v10, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@7e
    add-int/lit8 v5, v6, 0x1

    #@80
    .end local v6           #j:I
    .restart local v5       #j:I
    aput-object v9, v10, v6

    #@82
    .line 163
    iget-object v10, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@84
    add-int/lit8 v6, v5, 0x1

    #@86
    .end local v5           #j:I
    .restart local v6       #j:I
    new-instance v11, Ljava/lang/StringBuilder;

    #@88
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v11

    #@8f
    const-string v12, "/%"

    #@91
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v11

    #@95
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v11

    #@99
    aput-object v11, v10, v5

    #@9b
    .line 160
    add-int/lit8 v4, v4, 0x1

    #@9d
    goto :goto_78

    #@9e
    .line 168
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v2           #count:I
    .end local v4           #i:I
    .end local v6           #j:I
    .end local v9           #path:Ljava/lang/String;
    :cond_9e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a1
    move-result-object v10

    #@a2
    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@a5
    move-result-object v10

    #@a6
    iget-object v8, v10, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@a8
    .line 169
    .local v8, locale:Ljava/util/Locale;
    if-eqz v8, :cond_d2

    #@aa
    .line 170
    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@ad
    move-result-object v7

    #@ae
    .line 171
    .local v7, language:Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    #@b1
    move-result-object v3

    #@b2
    .line 172
    .local v3, country:Ljava/lang/String;
    if-eqz v7, :cond_d2

    #@b4
    .line 173
    if-eqz v3, :cond_d6

    #@b6
    .line 174
    iget-object v10, p0, Landroid/mtp/MtpDatabase;->mMediaScanner:Landroid/media/MediaScanner;

    #@b8
    new-instance v11, Ljava/lang/StringBuilder;

    #@ba
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@bd
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v11

    #@c1
    const-string v12, "_"

    #@c3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v11

    #@c7
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v11

    #@cb
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v11

    #@cf
    invoke-virtual {v10, v11}, Landroid/media/MediaScanner;->setLocale(Ljava/lang/String;)V

    #@d2
    .line 180
    .end local v3           #country:Ljava/lang/String;
    .end local v7           #language:Ljava/lang/String;
    :cond_d2
    :goto_d2
    invoke-direct {p0, p1}, Landroid/mtp/MtpDatabase;->initDeviceProperties(Landroid/content/Context;)V

    #@d5
    .line 181
    return-void

    #@d6
    .line 176
    .restart local v3       #country:Ljava/lang/String;
    .restart local v7       #language:Ljava/lang/String;
    :cond_d6
    iget-object v10, p0, Landroid/mtp/MtpDatabase;->mMediaScanner:Landroid/media/MediaScanner;

    #@d8
    invoke-virtual {v10, v7}, Landroid/media/MediaScanner;->setLocale(Ljava/lang/String;)V

    #@db
    goto :goto_d2
.end method

.method private beginSendObject(Ljava/lang/String;IIIJJ)I
    .registers 20
    .parameter "path"
    .parameter "format"
    .parameter "parent"
    .parameter "storageId"
    .parameter "size"
    .parameter "modified"

    #@0
    .prologue
    .line 270
    invoke-direct {p0, p1}, Landroid/mtp/MtpDatabase;->inStorageSubDirectory(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, -0x1

    #@7
    .line 314
    :cond_7
    :goto_7
    return v0

    #@8
    .line 273
    :cond_8
    if-eqz p1, :cond_5f

    #@a
    .line 274
    const/4 v7, 0x0

    #@b
    .line 276
    .local v7, c:Landroid/database/Cursor;
    :try_start_b
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@d
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@f
    sget-object v2, Landroid/mtp/MtpDatabase;->ID_PROJECTION:[Ljava/lang/String;

    #@11
    const-string v3, "_data=?"

    #@13
    const/4 v4, 0x1

    #@14
    new-array v4, v4, [Ljava/lang/String;

    #@16
    const/4 v5, 0x0

    #@17
    aput-object p1, v4, v5

    #@19
    const/4 v5, 0x0

    #@1a
    const/4 v6, 0x0

    #@1b
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1e
    move-result-object v7

    #@1f
    .line 278
    if-eqz v7, :cond_46

    #@21
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@24
    move-result v0

    #@25
    if-lez v0, :cond_46

    #@27
    .line 279
    const-string v0, "MtpDatabase"

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v2, "file already exists in beginSendObject: "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3f
    .catchall {:try_start_b .. :try_end_3f} :catchall_c4
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_3f} :catch_b6

    #@3f
    .line 280
    const/4 v0, -0x1

    #@40
    .line 285
    if-eqz v7, :cond_7

    #@42
    .line 286
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@45
    goto :goto_7

    #@46
    .line 285
    :cond_46
    if-eqz v7, :cond_4b

    #@48
    .line 286
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4b
    .line 291
    :cond_4b
    :goto_4b
    const/16 v0, 0x300b

    #@4d
    if-ne p2, v0, :cond_5f

    #@4f
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@51
    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    const-string v1, ".mov"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_5f

    #@5d
    .line 292
    const/16 p2, 0x3000

    #@5f
    .line 296
    .end local v7           #c:Landroid/database/Cursor;
    :cond_5f
    const/4 v0, 0x1

    #@60
    iput-boolean v0, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@62
    .line 297
    new-instance v10, Landroid/content/ContentValues;

    #@64
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@67
    .line 298
    .local v10, values:Landroid/content/ContentValues;
    const-string v0, "_data"

    #@69
    invoke-virtual {v10, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6c
    .line 299
    const-string v0, "format"

    #@6e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@71
    move-result-object v1

    #@72
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@75
    .line 300
    const-string/jumbo v0, "parent"

    #@78
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7f
    .line 301
    const-string/jumbo v0, "storage_id"

    #@82
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v1

    #@86
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@89
    .line 302
    const-string v0, "_size"

    #@8b
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@8e
    move-result-object v1

    #@8f
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@92
    .line 303
    const-string v0, "date_modified"

    #@94
    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@97
    move-result-object v1

    #@98
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@9b
    .line 306
    :try_start_9b
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@9d
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@9f
    invoke-interface {v0, v1, v10}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@a2
    move-result-object v9

    #@a3
    .line 307
    .local v9, uri:Landroid/net/Uri;
    if-eqz v9, :cond_cb

    #@a5
    .line 308
    invoke-virtual {v9}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@a8
    move-result-object v0

    #@a9
    const/4 v1, 0x2

    #@aa
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ad
    move-result-object v0

    #@ae
    check-cast v0, Ljava/lang/String;

    #@b0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b3
    .catch Landroid/os/RemoteException; {:try_start_9b .. :try_end_b3} :catch_ce

    #@b3
    move-result v0

    #@b4
    goto/16 :goto_7

    #@b6
    .line 282
    .end local v9           #uri:Landroid/net/Uri;
    .end local v10           #values:Landroid/content/ContentValues;
    .restart local v7       #c:Landroid/database/Cursor;
    :catch_b6
    move-exception v8

    #@b7
    .line 283
    .local v8, e:Landroid/os/RemoteException;
    :try_start_b7
    const-string v0, "MtpDatabase"

    #@b9
    const-string v1, "RemoteException in beginSendObject"

    #@bb
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_be
    .catchall {:try_start_b7 .. :try_end_be} :catchall_c4

    #@be
    .line 285
    if-eqz v7, :cond_4b

    #@c0
    .line 286
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@c3
    goto :goto_4b

    #@c4
    .line 285
    .end local v8           #e:Landroid/os/RemoteException;
    :catchall_c4
    move-exception v0

    #@c5
    if-eqz v7, :cond_ca

    #@c7
    .line 286
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ca
    :cond_ca
    throw v0

    #@cb
    .line 310
    .end local v7           #c:Landroid/database/Cursor;
    .restart local v9       #uri:Landroid/net/Uri;
    .restart local v10       #values:Landroid/content/ContentValues;
    :cond_cb
    const/4 v0, -0x1

    #@cc
    goto/16 :goto_7

    #@ce
    .line 312
    .end local v9           #uri:Landroid/net/Uri;
    :catch_ce
    move-exception v8

    #@cf
    .line 313
    .restart local v8       #e:Landroid/os/RemoteException;
    const-string v0, "MtpDatabase"

    #@d1
    const-string v1, "RemoteException in beginSendObject"

    #@d3
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d6
    .line 314
    const/4 v0, -0x1

    #@d7
    goto/16 :goto_7
.end method

.method private createObjectQuery(III)Landroid/database/Cursor;
    .registers 14
    .parameter "storageID"
    .parameter "format"
    .parameter "parent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v0, -0x1

    #@3
    const/4 v2, 0x1

    #@4
    const/4 v1, 0x0

    #@5
    .line 357
    if-ne p1, v0, :cond_53

    #@7
    .line 359
    if-nez p2, :cond_32

    #@9
    .line 361
    if-nez p3, :cond_23

    #@b
    .line 363
    const/4 v3, 0x0

    #@c
    .line 364
    .local v3, where:Ljava/lang/String;
    const/4 v4, 0x0

    #@d
    .line 428
    .local v4, whereArgs:[Ljava/lang/String;
    :goto_d
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhere:Ljava/lang/String;

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 429
    if-nez v3, :cond_a9

    #@13
    .line 430
    iget-object v3, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhere:Ljava/lang/String;

    #@15
    .line 431
    iget-object v4, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@17
    .line 449
    :cond_17
    :goto_17
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@19
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@1b
    sget-object v2, Landroid/mtp/MtpDatabase;->ID_PROJECTION:[Ljava/lang/String;

    #@1d
    move-object v6, v5

    #@1e
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@21
    move-result-object v0

    #@22
    return-object v0

    #@23
    .line 366
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_23
    if-ne p3, v0, :cond_26

    #@25
    .line 368
    const/4 p3, 0x0

    #@26
    .line 370
    :cond_26
    const-string/jumbo v3, "parent=?"

    #@29
    .line 371
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v2, [Ljava/lang/String;

    #@2b
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    aput-object v0, v4, v1

    #@31
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@32
    .line 375
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_32
    if-nez p3, :cond_3f

    #@34
    .line 377
    const-string v3, "format=?"

    #@36
    .line 378
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v2, [Ljava/lang/String;

    #@38
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    aput-object v0, v4, v1

    #@3e
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@3f
    .line 380
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_3f
    if-ne p3, v0, :cond_42

    #@41
    .line 382
    const/4 p3, 0x0

    #@42
    .line 384
    :cond_42
    const-string v3, "format=? AND parent=?"

    #@44
    .line 385
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v6, [Ljava/lang/String;

    #@46
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    aput-object v0, v4, v1

    #@4c
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    aput-object v0, v4, v2

    #@52
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@53
    .line 391
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_53
    if-nez p2, :cond_78

    #@55
    .line 393
    if-nez p3, :cond_63

    #@57
    .line 395
    const-string/jumbo v3, "storage_id=?"

    #@5a
    .line 396
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v2, [Ljava/lang/String;

    #@5c
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    aput-object v0, v4, v1

    #@62
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@63
    .line 398
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_63
    if-ne p3, v0, :cond_66

    #@65
    .line 400
    const/4 p3, 0x0

    #@66
    .line 402
    :cond_66
    const-string/jumbo v3, "storage_id=? AND parent=?"

    #@69
    .line 403
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v6, [Ljava/lang/String;

    #@6b
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    aput-object v0, v4, v1

    #@71
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    aput-object v0, v4, v2

    #@77
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@78
    .line 408
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_78
    if-nez p3, :cond_8c

    #@7a
    .line 410
    const-string/jumbo v3, "storage_id=? AND format=?"

    #@7d
    .line 411
    .restart local v3       #where:Ljava/lang/String;
    new-array v4, v6, [Ljava/lang/String;

    #@7f
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    aput-object v0, v4, v1

    #@85
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@88
    move-result-object v0

    #@89
    aput-object v0, v4, v2

    #@8b
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto :goto_d

    #@8c
    .line 414
    .end local v3           #where:Ljava/lang/String;
    .end local v4           #whereArgs:[Ljava/lang/String;
    :cond_8c
    if-ne p3, v0, :cond_8f

    #@8e
    .line 416
    const/4 p3, 0x0

    #@8f
    .line 418
    :cond_8f
    const-string/jumbo v3, "storage_id=? AND format=? AND parent=?"

    #@92
    .line 419
    .restart local v3       #where:Ljava/lang/String;
    const/4 v0, 0x3

    #@93
    new-array v4, v0, [Ljava/lang/String;

    #@95
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@98
    move-result-object v0

    #@99
    aput-object v0, v4, v1

    #@9b
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9e
    move-result-object v0

    #@9f
    aput-object v0, v4, v2

    #@a1
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a4
    move-result-object v0

    #@a5
    aput-object v0, v4, v6

    #@a7
    .restart local v4       #whereArgs:[Ljava/lang/String;
    goto/16 :goto_d

    #@a9
    .line 433
    :cond_a9
    new-instance v0, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    const-string v1, " AND "

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhere:Ljava/lang/String;

    #@ba
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v0

    #@be
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v3

    #@c2
    .line 436
    array-length v0, v4

    #@c3
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@c5
    array-length v1, v1

    #@c6
    add-int/2addr v0, v1

    #@c7
    new-array v9, v0, [Ljava/lang/String;

    #@c9
    .line 439
    .local v9, newWhereArgs:[Ljava/lang/String;
    const/4 v7, 0x0

    #@ca
    .local v7, i:I
    :goto_ca
    array-length v0, v4

    #@cb
    if-ge v7, v0, :cond_d4

    #@cd
    .line 440
    aget-object v0, v4, v7

    #@cf
    aput-object v0, v9, v7

    #@d1
    .line 439
    add-int/lit8 v7, v7, 0x1

    #@d3
    goto :goto_ca

    #@d4
    .line 442
    :cond_d4
    const/4 v8, 0x0

    #@d5
    .local v8, j:I
    :goto_d5
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@d7
    array-length v0, v0

    #@d8
    if-ge v8, v0, :cond_e5

    #@da
    .line 443
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mSubDirectoriesWhereArgs:[Ljava/lang/String;

    #@dc
    aget-object v0, v0, v8

    #@de
    aput-object v0, v9, v7

    #@e0
    .line 442
    add-int/lit8 v7, v7, 0x1

    #@e2
    add-int/lit8 v8, v8, 0x1

    #@e4
    goto :goto_d5

    #@e5
    .line 445
    :cond_e5
    move-object v4, v9

    #@e6
    goto/16 :goto_17
.end method

.method private deleteFile(I)I
    .registers 16
    .parameter "handle"

    #@0
    .prologue
    const/16 v13, 0x2002

    #@2
    const/4 v0, 0x1

    #@3
    .line 1012
    iput-boolean v0, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@5
    .line 1013
    const/4 v11, 0x0

    #@6
    .line 1014
    .local v11, path:Ljava/lang/String;
    const/4 v9, 0x0

    #@7
    .line 1016
    .local v9, format:I
    const/4 v7, 0x0

    #@8
    .line 1018
    .local v7, c:Landroid/database/Cursor;
    :try_start_8
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@a
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@c
    sget-object v2, Landroid/mtp/MtpDatabase;->PATH_FORMAT_PROJECTION:[Ljava/lang/String;

    #@e
    const-string v3, "_id=?"

    #@10
    const/4 v4, 0x1

    #@11
    new-array v4, v4, [Ljava/lang/String;

    #@13
    const/4 v5, 0x0

    #@14
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v6

    #@18
    aput-object v6, v4, v5

    #@1a
    const/4 v5, 0x0

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1f
    move-result-object v7

    #@20
    .line 1020
    if-eqz v7, :cond_3d

    #@22
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_3d

    #@28
    .line 1023
    const/4 v0, 0x1

    #@29
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v11

    #@2d
    .line 1024
    const/4 v0, 0x2

    #@2e
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_31} :catch_fa

    #@31
    move-result v9

    #@32
    .line 1029
    if-eqz v11, :cond_36

    #@34
    if-nez v9, :cond_45

    #@36
    .line 1067
    :cond_36
    if-eqz v7, :cond_3b

    #@38
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3b
    :cond_3b
    move v0, v13

    #@3c
    :cond_3c
    :goto_3c
    return v0

    #@3d
    .line 1026
    :cond_3d
    const/16 v0, 0x2009

    #@3f
    .line 1067
    if-eqz v7, :cond_3c

    #@41
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@44
    goto :goto_3c

    #@45
    .line 1034
    :cond_45
    :try_start_45
    invoke-direct {p0, v11}, Landroid/mtp/MtpDatabase;->isStorageSubDirectory(Ljava/lang/String;)Z
    :try_end_48
    .catchall {:try_start_45 .. :try_end_48} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_48} :catch_fa

    #@48
    move-result v0

    #@49
    if-eqz v0, :cond_53

    #@4b
    .line 1035
    const/16 v0, 0x200d

    #@4d
    .line 1067
    if-eqz v7, :cond_3c

    #@4f
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@52
    goto :goto_3c

    #@53
    .line 1038
    :cond_53
    const/16 v0, 0x3001

    #@55
    if-ne v9, v0, :cond_a0

    #@57
    .line 1040
    :try_start_57
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@59
    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getMtpObjectsUri(Ljava/lang/String;)Landroid/net/Uri;

    #@5c
    move-result-object v12

    #@5d
    .line 1041
    .local v12, uri:Landroid/net/Uri;
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@5f
    const-string v1, "_data LIKE ?1 AND lower(substr(_data,1,?2))=lower(?3)"

    #@61
    const/4 v2, 0x3

    #@62
    new-array v2, v2, [Ljava/lang/String;

    #@64
    const/4 v3, 0x0

    #@65
    new-instance v4, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, "/%"

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v4

    #@78
    aput-object v4, v2, v3

    #@7a
    const/4 v3, 0x1

    #@7b
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@7e
    move-result v4

    #@7f
    add-int/lit8 v4, v4, 0x1

    #@81
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@84
    move-result-object v4

    #@85
    aput-object v4, v2, v3

    #@87
    const/4 v3, 0x2

    #@88
    new-instance v4, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v4

    #@91
    const-string v5, "/"

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v4

    #@9b
    aput-object v4, v2, v3

    #@9d
    invoke-interface {v0, v12, v1, v2}, Landroid/content/IContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@a0
    .line 1048
    .end local v12           #uri:Landroid/net/Uri;
    :cond_a0
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@a2
    int-to-long v1, p1

    #@a3
    invoke-static {v0, v1, v2}, Landroid/provider/MediaStore$Files;->getMtpObjectsUri(Ljava/lang/String;J)Landroid/net/Uri;

    #@a6
    move-result-object v12

    #@a7
    .line 1049
    .restart local v12       #uri:Landroid/net/Uri;
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@a9
    const/4 v1, 0x0

    #@aa
    const/4 v2, 0x0

    #@ab
    invoke-interface {v0, v12, v1, v2}, Landroid/content/IContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@ae
    move-result v0

    #@af
    if-lez v0, :cond_10a

    #@b1
    .line 1050
    const/16 v0, 0x3001

    #@b3
    if-eq v9, v0, :cond_d7

    #@b5
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@b7
    invoke-virtual {v11, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@ba
    move-result-object v0

    #@bb
    const-string v1, "/.nomedia"

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_c0
    .catchall {:try_start_57 .. :try_end_c0} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_57 .. :try_end_c0} :catch_fa

    #@c0
    move-result v0

    #@c1
    if-eqz v0, :cond_d7

    #@c3
    .line 1053
    const/4 v0, 0x0

    #@c4
    :try_start_c4
    const-string v1, "/"

    #@c6
    invoke-virtual {v11, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@c9
    move-result v1

    #@ca
    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@cd
    move-result-object v10

    #@ce
    .line 1054
    .local v10, parentPath:Ljava/lang/String;
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@d0
    const-string/jumbo v1, "unhide"

    #@d3
    const/4 v2, 0x0

    #@d4
    invoke-interface {v0, v1, v10, v2}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_d7
    .catchall {:try_start_c4 .. :try_end_d7} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_c4 .. :try_end_d7} :catch_e0

    #@d7
    .line 1059
    .end local v10           #parentPath:Ljava/lang/String;
    :cond_d7
    :goto_d7
    const/16 v0, 0x2001

    #@d9
    .line 1067
    if-eqz v7, :cond_3c

    #@db
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@de
    goto/16 :goto_3c

    #@e0
    .line 1055
    :catch_e0
    move-exception v8

    #@e1
    .line 1056
    .local v8, e:Landroid/os/RemoteException;
    :try_start_e1
    const-string v0, "MtpDatabase"

    #@e3
    new-instance v1, Ljava/lang/StringBuilder;

    #@e5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e8
    const-string v2, "failed to unhide/rescan for "

    #@ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v1

    #@ee
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v1

    #@f2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f5
    move-result-object v1

    #@f6
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f9
    .catchall {:try_start_e1 .. :try_end_f9} :catchall_113
    .catch Landroid/os/RemoteException; {:try_start_e1 .. :try_end_f9} :catch_fa

    #@f9
    goto :goto_d7

    #@fa
    .line 1063
    .end local v8           #e:Landroid/os/RemoteException;
    .end local v12           #uri:Landroid/net/Uri;
    :catch_fa
    move-exception v8

    #@fb
    .line 1064
    .restart local v8       #e:Landroid/os/RemoteException;
    :try_start_fb
    const-string v0, "MtpDatabase"

    #@fd
    const-string v1, "RemoteException in deleteFile"

    #@ff
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_102
    .catchall {:try_start_fb .. :try_end_102} :catchall_113

    #@102
    .line 1067
    if-eqz v7, :cond_107

    #@104
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@107
    :cond_107
    move v0, v13

    #@108
    goto/16 :goto_3c

    #@10a
    .line 1061
    .end local v8           #e:Landroid/os/RemoteException;
    .restart local v12       #uri:Landroid/net/Uri;
    :cond_10a
    const/16 v0, 0x2009

    #@10c
    .line 1067
    if-eqz v7, :cond_3c

    #@10e
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@111
    goto/16 :goto_3c

    #@113
    .line 1067
    .end local v12           #uri:Landroid/net/Uri;
    :catchall_113
    move-exception v0

    #@114
    if-eqz v7, :cond_119

    #@116
    .line 1068
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@119
    :cond_119
    throw v0
.end method

.method private doMoveObject(III)I
    .registers 26
    .parameter "handle"
    .parameter "storageID"
    .parameter "parent"

    #@0
    .prologue
    .line 1193
    const/4 v13, 0x0

    #@1
    .line 1194
    .local v13, c:Landroid/database/Cursor;
    const/16 v17, 0x0

    #@3
    .line 1195
    .local v17, path_handle:Ljava/lang/String;
    const/4 v1, 0x1

    #@4
    new-array v5, v1, [Ljava/lang/String;

    #@6
    const/4 v1, 0x0

    #@7
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    aput-object v2, v5, v1

    #@d
    .line 1198
    .local v5, whereArgs:[Ljava/lang/String;
    :try_start_d
    move-object/from16 v0, p0

    #@f
    iget-object v1, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@11
    move-object/from16 v0, p0

    #@13
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@15
    sget-object v3, Landroid/mtp/MtpDatabase;->PATH_PROJECTION:[Ljava/lang/String;

    #@17
    const-string v4, "_id=?"

    #@19
    const/4 v6, 0x0

    #@1a
    const/4 v7, 0x0

    #@1b
    invoke-interface/range {v1 .. v7}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1e
    move-result-object v13

    #@1f
    .line 1199
    if-eqz v13, :cond_2c

    #@21
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_2c

    #@27
    .line 1200
    const/4 v1, 0x1

    #@28
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2b
    .catchall {:try_start_d .. :try_end_2b} :catchall_46
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_2b} :catch_36

    #@2b
    move-result-object v17

    #@2c
    .line 1206
    :cond_2c
    if-eqz v13, :cond_31

    #@2e
    .line 1207
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@31
    .line 1211
    :cond_31
    if-nez v17, :cond_4d

    #@33
    .line 1212
    const/16 v1, 0x2009

    #@35
    .line 1273
    :cond_35
    :goto_35
    return v1

    #@36
    .line 1202
    :catch_36
    move-exception v15

    #@37
    .line 1203
    .local v15, e:Landroid/os/RemoteException;
    :try_start_37
    const-string v1, "MtpDatabase"

    #@39
    const-string v2, "RemoteException in getObjectFilePath"

    #@3b
    invoke-static {v1, v2, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_46

    #@3e
    .line 1204
    const/16 v1, 0x2002

    #@40
    .line 1206
    if-eqz v13, :cond_35

    #@42
    .line 1207
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@45
    goto :goto_35

    #@46
    .line 1206
    .end local v15           #e:Landroid/os/RemoteException;
    :catchall_46
    move-exception v1

    #@47
    if-eqz v13, :cond_4c

    #@49
    .line 1207
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@4c
    :cond_4c
    throw v1

    #@4d
    .line 1215
    :cond_4d
    const/16 v18, 0x0

    #@4f
    .line 1216
    .local v18, path_parent:Ljava/lang/String;
    const/4 v1, 0x1

    #@50
    new-array v10, v1, [Ljava/lang/String;

    #@52
    const/4 v1, 0x0

    #@53
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    aput-object v2, v10, v1

    #@59
    .line 1218
    .local v10, whereArgs_parent:[Ljava/lang/String;
    if-eqz p3, :cond_9b

    #@5b
    .line 1220
    :try_start_5b
    move-object/from16 v0, p0

    #@5d
    iget-object v6, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@5f
    move-object/from16 v0, p0

    #@61
    iget-object v7, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@63
    sget-object v8, Landroid/mtp/MtpDatabase;->PATH_PROJECTION:[Ljava/lang/String;

    #@65
    const-string v9, "_id=?"

    #@67
    const/4 v11, 0x0

    #@68
    const/4 v12, 0x0

    #@69
    invoke-interface/range {v6 .. v12}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@6c
    move-result-object v13

    #@6d
    .line 1221
    if-eqz v13, :cond_7a

    #@6f
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    #@72
    move-result v1

    #@73
    if-eqz v1, :cond_7a

    #@75
    .line 1222
    const/4 v1, 0x1

    #@76
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_79
    .catchall {:try_start_5b .. :try_end_79} :catchall_94
    .catch Landroid/os/RemoteException; {:try_start_5b .. :try_end_79} :catch_84

    #@79
    move-result-object v18

    #@7a
    .line 1228
    :cond_7a
    if-eqz v13, :cond_7f

    #@7c
    .line 1229
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@7f
    .line 1240
    :cond_7f
    :goto_7f
    if-nez v18, :cond_b2

    #@81
    .line 1241
    const/16 v1, 0x2009

    #@83
    goto :goto_35

    #@84
    .line 1224
    :catch_84
    move-exception v15

    #@85
    .line 1225
    .restart local v15       #e:Landroid/os/RemoteException;
    :try_start_85
    const-string v1, "MtpDatabase"

    #@87
    const-string v2, "RemoteException in getObjectFilePath"

    #@89
    invoke-static {v1, v2, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8c
    .catchall {:try_start_85 .. :try_end_8c} :catchall_94

    #@8c
    .line 1226
    const/16 v1, 0x2002

    #@8e
    .line 1228
    if-eqz v13, :cond_35

    #@90
    .line 1229
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@93
    goto :goto_35

    #@94
    .line 1228
    .end local v15           #e:Landroid/os/RemoteException;
    :catchall_94
    move-exception v1

    #@95
    if-eqz v13, :cond_9a

    #@97
    .line 1229
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@9a
    :cond_9a
    throw v1

    #@9b
    .line 1233
    :cond_9b
    const v1, 0x20001

    #@9e
    move/from16 v0, p2

    #@a0
    if-ne v0, v1, :cond_a9

    #@a2
    .line 1234
    const-string v1, "EXTERNAL_ADD_STORAGE"

    #@a4
    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@a7
    move-result-object v18

    #@a8
    goto :goto_7f

    #@a9
    .line 1236
    :cond_a9
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@ac
    move-result-object v1

    #@ad
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@b0
    move-result-object v18

    #@b1
    goto :goto_7f

    #@b2
    .line 1246
    :cond_b2
    const/4 v1, 0x0

    #@b3
    const/16 v2, 0x13

    #@b5
    move-object/from16 v0, v17

    #@b7
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@ba
    move-result-object v1

    #@bb
    move-object/from16 v0, v18

    #@bd
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@c0
    move-result v1

    #@c1
    if-eqz v1, :cond_17d

    #@c3
    .line 1247
    new-instance v16, Ljava/io/File;

    #@c5
    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@c8
    .line 1248
    .local v16, file:Ljava/io/File;
    new-instance v14, Ljava/io/File;

    #@ca
    const-string v1, "/"

    #@cc
    move-object/from16 v0, v18

    #@ce
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@d1
    move-result-object v1

    #@d2
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getName()Ljava/lang/String;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-direct {v14, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@dd
    .line 1249
    .local v14, dir:Ljava/io/File;
    const-string v1, "MtpDatabase"

    #@df
    new-instance v2, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string/jumbo v3, "rename "

    #@e7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v2

    #@eb
    move-object/from16 v0, v17

    #@ed
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v2

    #@f1
    const-string v3, " to "

    #@f3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v2

    #@f7
    move-object/from16 v0, v18

    #@f9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v2

    #@fd
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v2

    #@101
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    .line 1250
    invoke-virtual {v14}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    #@107
    move-result-object v1

    #@108
    move-object/from16 v0, v16

    #@10a
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@10d
    move-result v19

    #@10e
    .line 1251
    .local v19, success:Z
    if-nez v19, :cond_12e

    #@110
    .line 1252
    const-string v1, "MtpDatabase"

    #@112
    new-instance v2, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v3, "Failed to move = "

    #@119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v2

    #@11d
    move-object/from16 v0, v16

    #@11f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v2

    #@123
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v2

    #@127
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 1253
    const/16 v1, 0x2002

    #@12c
    goto/16 :goto_35

    #@12e
    .line 1256
    :cond_12e
    const-string v1, "MtpDatabase"

    #@130
    const-string v2, "Success to move"

    #@132
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    .line 1257
    new-instance v21, Landroid/content/ContentValues;

    #@137
    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    #@13a
    .line 1258
    .local v21, values:Landroid/content/ContentValues;
    const-string v1, "_data"

    #@13c
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@13f
    move-result-object v2

    #@140
    move-object/from16 v0, v21

    #@142
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@145
    .line 1259
    const-string/jumbo v1, "parent"

    #@148
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14b
    move-result-object v2

    #@14c
    move-object/from16 v0, v21

    #@14e
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@151
    .line 1260
    const/16 v20, 0x0

    #@153
    .line 1262
    .local v20, updated:I
    :try_start_153
    move-object/from16 v0, p0

    #@155
    iget-object v1, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@157
    move-object/from16 v0, p0

    #@159
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@15b
    const-string v3, "_id=?"

    #@15d
    move-object/from16 v0, v21

    #@15f
    invoke-interface {v1, v2, v0, v3, v5}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_162
    .catch Landroid/os/RemoteException; {:try_start_153 .. :try_end_162} :catch_170

    #@162
    move-result v20

    #@163
    .line 1266
    :goto_163
    if-nez v20, :cond_179

    #@165
    .line 1267
    const-string v1, "MtpDatabase"

    #@167
    const-string v2, "Unable to update path "

    #@169
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    .line 1268
    const/16 v1, 0x2002

    #@16e
    goto/16 :goto_35

    #@170
    .line 1263
    :catch_170
    move-exception v15

    #@171
    .line 1264
    .restart local v15       #e:Landroid/os/RemoteException;
    const-string v1, "MtpDatabase"

    #@173
    const-string v2, "RemoteException in mMediaProvider.update"

    #@175
    invoke-static {v1, v2, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@178
    goto :goto_163

    #@179
    .line 1270
    .end local v15           #e:Landroid/os/RemoteException;
    :cond_179
    const/16 v1, 0x2001

    #@17b
    goto/16 :goto_35

    #@17d
    .line 1273
    .end local v14           #dir:Ljava/io/File;
    .end local v16           #file:Ljava/io/File;
    .end local v19           #success:Z
    .end local v20           #updated:I
    .end local v21           #values:Landroid/content/ContentValues;
    :cond_17d
    const/16 v1, 0x2005

    #@17f
    goto/16 :goto_35
.end method

.method private endSendObject(Ljava/lang/String;IIZ)V
    .registers 14
    .parameter "path"
    .parameter "handle"
    .parameter "format"
    .parameter "succeeded"

    #@0
    .prologue
    .line 319
    if-eqz p4, :cond_76

    #@2
    .line 322
    const v4, 0xba05

    #@5
    if-ne p3, v4, :cond_6e

    #@7
    .line 324
    move-object v2, p1

    #@8
    .line 325
    .local v2, name:Ljava/lang/String;
    const/16 v4, 0x2f

    #@a
    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    #@d
    move-result v1

    #@e
    .line 326
    .local v1, lastSlash:I
    if-ltz v1, :cond_16

    #@10
    .line 327
    add-int/lit8 v4, v1, 0x1

    #@12
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    .line 330
    :cond_16
    const-string v4, ".pla"

    #@18
    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@1b
    move-result v4

    #@1c
    if-eqz v4, :cond_29

    #@1e
    .line 331
    const/4 v4, 0x0

    #@1f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@22
    move-result v5

    #@23
    add-int/lit8 v5, v5, -0x4

    #@25
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    .line 334
    :cond_29
    new-instance v3, Landroid/content/ContentValues;

    #@2b
    const/4 v4, 0x1

    #@2c
    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    #@2f
    .line 335
    .local v3, values:Landroid/content/ContentValues;
    const-string v4, "_data"

    #@31
    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 336
    const-string/jumbo v4, "name"

    #@37
    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 337
    const-string v4, "format"

    #@3c
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@43
    .line 338
    const-string v4, "date_modified"

    #@45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@48
    move-result-wide v5

    #@49
    const-wide/16 v7, 0x3e8

    #@4b
    div-long/2addr v5, v7

    #@4c
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@53
    .line 339
    const-string/jumbo v4, "media_scanner_new_object_id"

    #@56
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@5d
    .line 341
    :try_start_5d
    iget-object v4, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@5f
    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    #@61
    invoke-interface {v4, v5, v3}, Landroid/content/IContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_64
    .catch Landroid/os/RemoteException; {:try_start_5d .. :try_end_64} :catch_65

    #@64
    .line 351
    .end local v1           #lastSlash:I
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #values:Landroid/content/ContentValues;
    :goto_64
    return-void

    #@65
    .line 342
    .restart local v1       #lastSlash:I
    .restart local v2       #name:Ljava/lang/String;
    .restart local v3       #values:Landroid/content/ContentValues;
    :catch_65
    move-exception v0

    #@66
    .line 343
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "MtpDatabase"

    #@68
    const-string v5, "RemoteException in endSendObject"

    #@6a
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d
    goto :goto_64

    #@6e
    .line 346
    .end local v0           #e:Landroid/os/RemoteException;
    .end local v1           #lastSlash:I
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #values:Landroid/content/ContentValues;
    :cond_6e
    iget-object v4, p0, Landroid/mtp/MtpDatabase;->mMediaScanner:Landroid/media/MediaScanner;

    #@70
    iget-object v5, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@72
    invoke-virtual {v4, p1, v5, p2, p3}, Landroid/media/MediaScanner;->scanMtpFile(Ljava/lang/String;Ljava/lang/String;II)V

    #@75
    goto :goto_64

    #@76
    .line 349
    :cond_76
    invoke-direct {p0, p2}, Landroid/mtp/MtpDatabase;->deleteFile(I)I

    #@79
    goto :goto_64
.end method

.method private getDeviceProperty(I[J[C)I
    .registers 16
    .parameter "property"
    .parameter "outIntValue"
    .parameter "outStringValue"

    #@0
    .prologue
    .line 875
    sparse-switch p1, :sswitch_data_b6

    #@3
    .line 923
    const/16 v9, 0x200a

    #@5
    :goto_5
    return v9

    #@6
    .line 879
    :sswitch_6
    iget-object v9, p0, Landroid/mtp/MtpDatabase;->mDeviceProperties:Landroid/content/SharedPreferences;

    #@8
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b
    move-result-object v10

    #@c
    const-string v11, ""

    #@e
    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v7

    #@12
    .line 880
    .local v7, value:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@15
    move-result v4

    #@16
    .line 881
    .local v4, length:I
    const/16 v9, 0xff

    #@18
    if-le v4, v9, :cond_1c

    #@1a
    .line 882
    const/16 v4, 0xff

    #@1c
    .line 884
    :cond_1c
    const/4 v9, 0x0

    #@1d
    const/4 v10, 0x0

    #@1e
    invoke-virtual {v7, v9, v4, p3, v10}, Ljava/lang/String;->getChars(II[CI)V

    #@21
    .line 885
    const/4 v9, 0x0

    #@22
    aput-char v9, p3, v4

    #@24
    .line 886
    const/16 v9, 0x2001

    #@26
    goto :goto_5

    #@27
    .line 891
    .end local v4           #length:I
    .end local v7           #value:Ljava/lang/String;
    :sswitch_27
    const-string v0, "LG G2"

    #@29
    .line 892
    .local v0, DEFAULT_DEVICE_NAME:Ljava/lang/String;
    iget-object v9, p0, Landroid/mtp/MtpDatabase;->mContext:Landroid/content/Context;

    #@2b
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2e
    move-result-object v9

    #@2f
    const-string/jumbo v10, "lg_device_name"

    #@32
    invoke-static {v9, v10}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v5

    #@36
    .line 894
    .local v5, name:Ljava/lang/String;
    if-nez v5, :cond_3a

    #@38
    .line 895
    const-string v5, "LG G2"

    #@3a
    .line 898
    :cond_3a
    const-string/jumbo v9, "ro.build.target_operator"

    #@3d
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v9

    #@41
    const-string v10, "VZW"

    #@43
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v9

    #@47
    if-eqz v9, :cond_55

    #@49
    .line 899
    iget-object v9, p0, Landroid/mtp/MtpDatabase;->mDeviceProperties:Landroid/content/SharedPreferences;

    #@4b
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4e
    move-result-object v10

    #@4f
    const-string v11, ""

    #@51
    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    .line 902
    :cond_55
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@58
    move-result v6

    #@59
    .line 903
    .local v6, name_length:I
    const/16 v9, 0xff

    #@5b
    if-le v6, v9, :cond_5f

    #@5d
    .line 904
    const/16 v6, 0xff

    #@5f
    .line 907
    :cond_5f
    const/4 v9, 0x0

    #@60
    const/4 v10, 0x0

    #@61
    invoke-virtual {v5, v9, v6, p3, v10}, Ljava/lang/String;->getChars(II[CI)V

    #@64
    .line 908
    const/4 v9, 0x0

    #@65
    aput-char v9, p3, v6

    #@67
    .line 909
    const/16 v9, 0x2001

    #@69
    goto :goto_5

    #@6a
    .line 913
    .end local v0           #DEFAULT_DEVICE_NAME:Ljava/lang/String;
    .end local v5           #name:Ljava/lang/String;
    .end local v6           #name_length:I
    :sswitch_6a
    iget-object v9, p0, Landroid/mtp/MtpDatabase;->mContext:Landroid/content/Context;

    #@6c
    const-string/jumbo v10, "window"

    #@6f
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@72
    move-result-object v9

    #@73
    check-cast v9, Landroid/view/WindowManager;

    #@75
    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    #@78
    move-result-object v1

    #@79
    .line 915
    .local v1, display:Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getMaximumSizeDimension()I

    #@7c
    move-result v8

    #@7d
    .line 916
    .local v8, width:I
    invoke-virtual {v1}, Landroid/view/Display;->getMaximumSizeDimension()I

    #@80
    move-result v2

    #@81
    .line 917
    .local v2, height:I
    new-instance v9, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@89
    move-result-object v10

    #@8a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v9

    #@8e
    const-string/jumbo v10, "x"

    #@91
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v9

    #@95
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@98
    move-result-object v10

    #@99
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v9

    #@9d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v3

    #@a1
    .line 918
    .local v3, imageSize:Ljava/lang/String;
    const/4 v9, 0x0

    #@a2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@a5
    move-result v10

    #@a6
    const/4 v11, 0x0

    #@a7
    invoke-virtual {v3, v9, v10, p3, v11}, Ljava/lang/String;->getChars(II[CI)V

    #@aa
    .line 919
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@ad
    move-result v9

    #@ae
    const/4 v10, 0x0

    #@af
    aput-char v10, p3, v9

    #@b1
    .line 920
    const/16 v9, 0x2001

    #@b3
    goto/16 :goto_5

    #@b5
    .line 875
    nop

    #@b6
    :sswitch_data_b6
    .sparse-switch
        0x5003 -> :sswitch_6a
        0xd401 -> :sswitch_6
        0xd402 -> :sswitch_27
    .end sparse-switch
.end method

.method private getNumObjects(III)I
    .registers 8
    .parameter "storageID"
    .parameter "format"
    .parameter "parent"

    #@0
    .prologue
    .line 479
    const/4 v0, 0x0

    #@1
    .line 481
    .local v0, c:Landroid/database/Cursor;
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Landroid/mtp/MtpDatabase;->createObjectQuery(III)Landroid/database/Cursor;

    #@4
    move-result-object v0

    #@5
    .line 482
    if-eqz v0, :cond_11

    #@7
    .line 483
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_26
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_18

    #@a
    move-result v2

    #@b
    .line 488
    if-eqz v0, :cond_10

    #@d
    .line 489
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@10
    .line 492
    :cond_10
    :goto_10
    return v2

    #@11
    .line 488
    :cond_11
    if-eqz v0, :cond_16

    #@13
    .line 489
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@16
    .line 492
    :cond_16
    :goto_16
    const/4 v2, -0x1

    #@17
    goto :goto_10

    #@18
    .line 485
    :catch_18
    move-exception v1

    #@19
    .line 486
    .local v1, e:Landroid/os/RemoteException;
    :try_start_19
    const-string v2, "MtpDatabase"

    #@1b
    const-string v3, "RemoteException in getNumObjects"

    #@1d
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_26

    #@20
    .line 488
    if-eqz v0, :cond_16

    #@22
    .line 489
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@25
    goto :goto_16

    #@26
    .line 488
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_26
    move-exception v2

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 489
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2c
    :cond_2c
    throw v2
.end method

.method private getObjectFilePath(I[C[J)I
    .registers 15
    .parameter "handle"
    .parameter "outFilePath"
    .parameter "outFileLengthFormat"

    #@0
    .prologue
    const/16 v10, 0x2001

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 977
    if-nez p1, :cond_23

    #@6
    .line 979
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaStoragePath:Ljava/lang/String;

    #@8
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mMediaStoragePath:Ljava/lang/String;

    #@a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    invoke-virtual {v0, v2, v1, p2, v2}, Ljava/lang/String;->getChars(II[CI)V

    #@11
    .line 980
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaStoragePath:Ljava/lang/String;

    #@13
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@16
    move-result v0

    #@17
    aput-char v2, p2, v0

    #@19
    .line 981
    const-wide/16 v0, 0x0

    #@1b
    aput-wide v0, p3, v2

    #@1d
    .line 982
    const-wide/16 v0, 0x3001

    #@1f
    aput-wide v0, p3, v3

    #@21
    move v0, v10

    #@22
    .line 1006
    :cond_22
    :goto_22
    return v0

    #@23
    .line 985
    :cond_23
    const/4 v7, 0x0

    #@24
    .line 987
    .local v7, c:Landroid/database/Cursor;
    :try_start_24
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@26
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@28
    sget-object v2, Landroid/mtp/MtpDatabase;->PATH_FORMAT_PROJECTION:[Ljava/lang/String;

    #@2a
    const-string v3, "_id=?"

    #@2c
    const/4 v4, 0x1

    #@2d
    new-array v4, v4, [Ljava/lang/String;

    #@2f
    const/4 v5, 0x0

    #@30
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@33
    move-result-object v6

    #@34
    aput-object v6, v4, v5

    #@36
    const/4 v5, 0x0

    #@37
    const/4 v6, 0x0

    #@38
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@3b
    move-result-object v7

    #@3c
    .line 989
    if-eqz v7, :cond_74

    #@3e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_74

    #@44
    .line 990
    const/4 v0, 0x1

    #@45
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    .line 991
    .local v9, path:Ljava/lang/String;
    const/4 v0, 0x0

    #@4a
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@4d
    move-result v1

    #@4e
    const/4 v2, 0x0

    #@4f
    invoke-virtual {v9, v0, v1, p2, v2}, Ljava/lang/String;->getChars(II[CI)V

    #@52
    .line 992
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@55
    move-result v0

    #@56
    const/4 v1, 0x0

    #@57
    aput-char v1, p2, v0

    #@59
    .line 995
    const/4 v0, 0x0

    #@5a
    new-instance v1, Ljava/io/File;

    #@5c
    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5f
    invoke-virtual {v1}, Ljava/io/File;->length()J

    #@62
    move-result-wide v1

    #@63
    aput-wide v1, p3, v0

    #@65
    .line 996
    const/4 v0, 0x1

    #@66
    const/4 v1, 0x2

    #@67
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    #@6a
    move-result-wide v1

    #@6b
    aput-wide v1, p3, v0
    :try_end_6d
    .catchall {:try_start_24 .. :try_end_6d} :catchall_8c
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_6d} :catch_7c

    #@6d
    .line 1005
    if-eqz v7, :cond_72

    #@6f
    .line 1006
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@72
    :cond_72
    move v0, v10

    #@73
    goto :goto_22

    #@74
    .line 999
    .end local v9           #path:Ljava/lang/String;
    :cond_74
    const/16 v0, 0x2009

    #@76
    .line 1005
    if-eqz v7, :cond_22

    #@78
    .line 1006
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@7b
    goto :goto_22

    #@7c
    .line 1001
    :catch_7c
    move-exception v8

    #@7d
    .line 1002
    .local v8, e:Landroid/os/RemoteException;
    :try_start_7d
    const-string v0, "MtpDatabase"

    #@7f
    const-string v1, "RemoteException in getObjectFilePath"

    #@81
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_84
    .catchall {:try_start_7d .. :try_end_84} :catchall_8c

    #@84
    .line 1003
    const/16 v0, 0x2002

    #@86
    .line 1005
    if-eqz v7, :cond_22

    #@88
    .line 1006
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8b
    goto :goto_22

    #@8c
    .line 1005
    .end local v8           #e:Landroid/os/RemoteException;
    :catchall_8c
    move-exception v0

    #@8d
    if-eqz v7, :cond_92

    #@8f
    .line 1006
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@92
    :cond_92
    throw v0
.end method

.method private getObjectInfo(I[I[C[J)Z
    .registers 19
    .parameter "handle"
    .parameter "outStorageFormatParent"
    .parameter "outName"
    .parameter "outModified"

    #@0
    .prologue
    .line 943
    const/4 v8, 0x0

    #@1
    .line 945
    .local v8, c:Landroid/database/Cursor;
    :try_start_1
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@3
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@5
    sget-object v3, Landroid/mtp/MtpDatabase;->OBJECT_INFO_PROJECTION:[Ljava/lang/String;

    #@7
    const-string v4, "_id=?"

    #@9
    const/4 v5, 0x1

    #@a
    new-array v5, v5, [Ljava/lang/String;

    #@c
    const/4 v6, 0x0

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v7

    #@11
    aput-object v7, v5, v6

    #@13
    const/4 v6, 0x0

    #@14
    const/4 v7, 0x0

    #@15
    invoke-interface/range {v1 .. v7}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@18
    move-result-object v8

    #@19
    .line 947
    if-eqz v8, :cond_70

    #@1b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_70

    #@21
    .line 948
    const/4 v1, 0x0

    #@22
    const/4 v2, 0x1

    #@23
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    #@26
    move-result v2

    #@27
    aput v2, p2, v1

    #@29
    .line 949
    const/4 v1, 0x1

    #@2a
    const/4 v2, 0x2

    #@2b
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    #@2e
    move-result v2

    #@2f
    aput v2, p2, v1

    #@31
    .line 950
    const/4 v1, 0x2

    #@32
    const/4 v2, 0x3

    #@33
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    #@36
    move-result v2

    #@37
    aput v2, p2, v1

    #@39
    .line 953
    const/4 v1, 0x4

    #@3a
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v12

    #@3e
    .line 954
    .local v12, path:Ljava/lang/String;
    const/16 v1, 0x2f

    #@40
    invoke-virtual {v12, v1}, Ljava/lang/String;->lastIndexOf(I)I

    #@43
    move-result v11

    #@44
    .line 955
    .local v11, lastSlash:I
    if-ltz v11, :cond_6e

    #@46
    add-int/lit8 v13, v11, 0x1

    #@48
    .line 956
    .local v13, start:I
    :goto_48
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@4b
    move-result v10

    #@4c
    .line 957
    .local v10, end:I
    sub-int v1, v10, v13

    #@4e
    const/16 v2, 0xff

    #@50
    if-le v1, v2, :cond_54

    #@52
    .line 958
    add-int/lit16 v10, v13, 0xff

    #@54
    .line 960
    :cond_54
    const/4 v1, 0x0

    #@55
    move-object/from16 v0, p3

    #@57
    invoke-virtual {v12, v13, v10, v0, v1}, Ljava/lang/String;->getChars(II[CI)V

    #@5a
    .line 961
    sub-int v1, v10, v13

    #@5c
    const/4 v2, 0x0

    #@5d
    aput-char v2, p3, v1

    #@5f
    .line 963
    const/4 v1, 0x0

    #@60
    const/4 v2, 0x5

    #@61
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    #@64
    move-result-wide v2

    #@65
    aput-wide v2, p4, v1
    :try_end_67
    .catchall {:try_start_1 .. :try_end_67} :catchall_85
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_67} :catch_77

    #@67
    .line 964
    const/4 v1, 0x1

    #@68
    .line 969
    if-eqz v8, :cond_6d

    #@6a
    .line 970
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6d
    .line 973
    .end local v10           #end:I
    .end local v11           #lastSlash:I
    .end local v12           #path:Ljava/lang/String;
    .end local v13           #start:I
    :cond_6d
    :goto_6d
    return v1

    #@6e
    .line 955
    .restart local v11       #lastSlash:I
    .restart local v12       #path:Ljava/lang/String;
    :cond_6e
    const/4 v13, 0x0

    #@6f
    goto :goto_48

    #@70
    .line 969
    .end local v11           #lastSlash:I
    .end local v12           #path:Ljava/lang/String;
    :cond_70
    if-eqz v8, :cond_75

    #@72
    .line 970
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@75
    .line 973
    :cond_75
    :goto_75
    const/4 v1, 0x0

    #@76
    goto :goto_6d

    #@77
    .line 966
    :catch_77
    move-exception v9

    #@78
    .line 967
    .local v9, e:Landroid/os/RemoteException;
    :try_start_78
    const-string v1, "MtpDatabase"

    #@7a
    const-string v2, "RemoteException in getObjectInfo"

    #@7c
    invoke-static {v1, v2, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7f
    .catchall {:try_start_78 .. :try_end_7f} :catchall_85

    #@7f
    .line 969
    if-eqz v8, :cond_75

    #@81
    .line 970
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@84
    goto :goto_75

    #@85
    .line 969
    .end local v9           #e:Landroid/os/RemoteException;
    :catchall_85
    move-exception v1

    #@86
    if-eqz v8, :cond_8b

    #@88
    .line 970
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@8b
    :cond_8b
    throw v1
.end method

.method private getObjectList(III)[I
    .registers 12
    .parameter "storageID"
    .parameter "format"
    .parameter "parent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 453
    const/4 v0, 0x0

    #@2
    .line 455
    .local v0, c:Landroid/database/Cursor;
    :try_start_2
    invoke-direct {p0, p1, p2, p3}, Landroid/mtp/MtpDatabase;->createObjectQuery(III)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_2 .. :try_end_5} :catchall_42
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_5} :catch_34

    #@5
    move-result-object v0

    #@6
    .line 456
    if-nez v0, :cond_f

    #@8
    .line 471
    if-eqz v0, :cond_d

    #@a
    .line 472
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@d
    :cond_d
    move-object v4, v5

    #@e
    .line 475
    :cond_e
    :goto_e
    return-object v4

    #@f
    .line 459
    :cond_f
    :try_start_f
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    #@12
    move-result v1

    #@13
    .line 460
    .local v1, count:I
    if-lez v1, :cond_2d

    #@15
    .line 461
    new-array v4, v1, [I

    #@17
    .line 462
    .local v4, result:[I
    const/4 v3, 0x0

    #@18
    .local v3, i:I
    :goto_18
    if-ge v3, v1, :cond_27

    #@1a
    .line 463
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    #@1d
    .line 464
    const/4 v6, 0x0

    #@1e
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    #@21
    move-result v6

    #@22
    aput v6, v4, v3
    :try_end_24
    .catchall {:try_start_f .. :try_end_24} :catchall_42
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_24} :catch_34

    #@24
    .line 462
    add-int/lit8 v3, v3, 0x1

    #@26
    goto :goto_18

    #@27
    .line 471
    :cond_27
    if-eqz v0, :cond_e

    #@29
    .line 472
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@2c
    goto :goto_e

    #@2d
    .line 471
    .end local v3           #i:I
    .end local v4           #result:[I
    :cond_2d
    if-eqz v0, :cond_32

    #@2f
    .line 472
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@32
    .end local v1           #count:I
    :cond_32
    :goto_32
    move-object v4, v5

    #@33
    .line 475
    goto :goto_e

    #@34
    .line 468
    :catch_34
    move-exception v2

    #@35
    .line 469
    .local v2, e:Landroid/os/RemoteException;
    :try_start_35
    const-string v6, "MtpDatabase"

    #@37
    const-string v7, "RemoteException in getObjectList"

    #@39
    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3c
    .catchall {:try_start_35 .. :try_end_3c} :catchall_42

    #@3c
    .line 471
    if-eqz v0, :cond_32

    #@3e
    .line 472
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@41
    goto :goto_32

    #@42
    .line 471
    .end local v2           #e:Landroid/os/RemoteException;
    :catchall_42
    move-exception v5

    #@43
    if-eqz v0, :cond_48

    #@45
    .line 472
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@48
    :cond_48
    throw v5
.end method

.method private getObjectPropertyList(JIJII)Landroid/mtp/MtpPropertyList;
    .registers 13
    .parameter "handle"
    .parameter "format"
    .parameter "property"
    .parameter "groupCode"
    .parameter "depth"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 743
    if-eqz p6, :cond_c

    #@3
    .line 744
    new-instance v2, Landroid/mtp/MtpPropertyList;

    #@5
    const v3, 0xa807

    #@8
    invoke-direct {v2, v4, v3}, Landroid/mtp/MtpPropertyList;-><init>(II)V

    #@b
    .line 764
    :goto_b
    return-object v2

    #@c
    .line 748
    :cond_c
    const-wide v2, 0xffffffffL

    #@11
    cmp-long v2, p4, v2

    #@13
    if-nez v2, :cond_40

    #@15
    .line 749
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByFormat:Ljava/util/HashMap;

    #@17
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/mtp/MtpPropertyGroup;

    #@21
    .line 750
    .local v0, propertyGroup:Landroid/mtp/MtpPropertyGroup;
    if-nez v0, :cond_3a

    #@23
    .line 751
    invoke-direct {p0, p3}, Landroid/mtp/MtpDatabase;->getSupportedObjectProperties(I)[I

    #@26
    move-result-object v1

    #@27
    .line 752
    .local v1, propertyList:[I
    new-instance v0, Landroid/mtp/MtpPropertyGroup;

    #@29
    .end local v0           #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@2b
    iget-object v3, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@2d
    invoke-direct {v0, p0, v2, v3, v1}, Landroid/mtp/MtpPropertyGroup;-><init>(Landroid/mtp/MtpDatabase;Landroid/content/IContentProvider;Ljava/lang/String;[I)V

    #@30
    .line 753
    .restart local v0       #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByFormat:Ljava/util/HashMap;

    #@32
    new-instance v3, Ljava/lang/Integer;

    #@34
    invoke-direct {v3, p3}, Ljava/lang/Integer;-><init>(I)V

    #@37
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    .line 764
    .end local v1           #propertyList:[I
    :cond_3a
    :goto_3a
    long-to-int v2, p1

    #@3b
    invoke-virtual {v0, v2, p3, p7}, Landroid/mtp/MtpPropertyGroup;->getPropertyList(III)Landroid/mtp/MtpPropertyList;

    #@3e
    move-result-object v2

    #@3f
    goto :goto_b

    #@40
    .line 756
    .end local v0           #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    :cond_40
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByProperty:Ljava/util/HashMap;

    #@42
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, Landroid/mtp/MtpPropertyGroup;

    #@4c
    .line 757
    .restart local v0       #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    if-nez v0, :cond_3a

    #@4e
    .line 758
    const/4 v2, 0x1

    #@4f
    new-array v1, v2, [I

    #@51
    long-to-int v2, p4

    #@52
    aput v2, v1, v4

    #@54
    .line 759
    .restart local v1       #propertyList:[I
    new-instance v0, Landroid/mtp/MtpPropertyGroup;

    #@56
    .end local v0           #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@58
    iget-object v3, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@5a
    invoke-direct {v0, p0, v2, v3, v1}, Landroid/mtp/MtpPropertyGroup;-><init>(Landroid/mtp/MtpDatabase;Landroid/content/IContentProvider;Ljava/lang/String;[I)V

    #@5d
    .line 760
    .restart local v0       #propertyGroup:Landroid/mtp/MtpPropertyGroup;
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mPropertyGroupsByProperty:Ljava/util/HashMap;

    #@5f
    new-instance v3, Ljava/lang/Integer;

    #@61
    long-to-int v4, p4

    #@62
    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(I)V

    #@65
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    goto :goto_3a
.end method

.method private getObjectReferences(I)[I
    .registers 15
    .parameter "handle"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 1074
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@3
    int-to-long v2, p1

    #@4
    invoke-static {v0, v2, v3}, Landroid/provider/MediaStore$Files;->getMtpReferencesUri(Ljava/lang/String;J)Landroid/net/Uri;

    #@7
    move-result-object v1

    #@8
    .line 1075
    .local v1, uri:Landroid/net/Uri;
    const/4 v7, 0x0

    #@9
    .line 1077
    .local v7, c:Landroid/database/Cursor;
    :try_start_9
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@b
    sget-object v2, Landroid/mtp/MtpDatabase;->ID_PROJECTION:[Ljava/lang/String;

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x0

    #@f
    const/4 v5, 0x0

    #@10
    const/4 v6, 0x0

    #@11
    invoke-interface/range {v0 .. v6}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;
    :try_end_14
    .catchall {:try_start_9 .. :try_end_14} :catchall_51
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_14} :catch_43

    #@14
    move-result-object v7

    #@15
    .line 1078
    if-nez v7, :cond_1e

    #@17
    .line 1093
    if-eqz v7, :cond_1c

    #@19
    .line 1094
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@1c
    :cond_1c
    move-object v11, v12

    #@1d
    .line 1097
    :cond_1d
    :goto_1d
    return-object v11

    #@1e
    .line 1081
    :cond_1e
    :try_start_1e
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@21
    move-result v8

    #@22
    .line 1082
    .local v8, count:I
    if-lez v8, :cond_3c

    #@24
    .line 1083
    new-array v11, v8, [I

    #@26
    .line 1084
    .local v11, result:[I
    const/4 v10, 0x0

    #@27
    .local v10, i:I
    :goto_27
    if-ge v10, v8, :cond_36

    #@29
    .line 1085
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    .line 1086
    const/4 v0, 0x0

    #@2d
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    #@30
    move-result v0

    #@31
    aput v0, v11, v10
    :try_end_33
    .catchall {:try_start_1e .. :try_end_33} :catchall_51
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_33} :catch_43

    #@33
    .line 1084
    add-int/lit8 v10, v10, 0x1

    #@35
    goto :goto_27

    #@36
    .line 1093
    :cond_36
    if-eqz v7, :cond_1d

    #@38
    .line 1094
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3b
    goto :goto_1d

    #@3c
    .line 1093
    .end local v10           #i:I
    .end local v11           #result:[I
    :cond_3c
    if-eqz v7, :cond_41

    #@3e
    .line 1094
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@41
    .end local v8           #count:I
    :cond_41
    :goto_41
    move-object v11, v12

    #@42
    .line 1097
    goto :goto_1d

    #@43
    .line 1090
    :catch_43
    move-exception v9

    #@44
    .line 1091
    .local v9, e:Landroid/os/RemoteException;
    :try_start_44
    const-string v0, "MtpDatabase"

    #@46
    const-string v2, "RemoteException in getObjectList"

    #@48
    invoke-static {v0, v2, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_51

    #@4b
    .line 1093
    if-eqz v7, :cond_41

    #@4d
    .line 1094
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@50
    goto :goto_41

    #@51
    .line 1093
    .end local v9           #e:Landroid/os/RemoteException;
    :catchall_51
    move-exception v0

    #@52
    if-eqz v7, :cond_57

    #@54
    .line 1094
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@57
    :cond_57
    throw v0
.end method

.method private getSupportedCaptureFormats()[I
    .registers 2

    #@0
    .prologue
    .line 543
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method private getSupportedDeviceProperties()[I
    .registers 2

    #@0
    .prologue
    .line 732
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_8

    #@6
    return-object v0

    #@7
    nop

    #@8
    :array_8
    .array-data 0x4
        0x1t 0xd4t 0x0t 0x0t
        0x2t 0xd4t 0x0t 0x0t
        0x3t 0x50t 0x0t 0x0t
    .end array-data
.end method

.method private getSupportedObjectProperties(I)[I
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 703
    sparse-switch p1, :sswitch_data_12

    #@3
    .line 727
    sget-object v0, Landroid/mtp/MtpDatabase;->FILE_PROPERTIES:[I

    #@5
    :goto_5
    return-object v0

    #@6
    .line 709
    :sswitch_6
    sget-object v0, Landroid/mtp/MtpDatabase;->AUDIO_PROPERTIES:[I

    #@8
    goto :goto_5

    #@9
    .line 718
    :sswitch_9
    sget-object v0, Landroid/mtp/MtpDatabase;->VIDEO_PROPERTIES:[I

    #@b
    goto :goto_5

    #@c
    .line 723
    :sswitch_c
    sget-object v0, Landroid/mtp/MtpDatabase;->IMAGE_PROPERTIES:[I

    #@e
    goto :goto_5

    #@f
    .line 725
    :sswitch_f
    sget-object v0, Landroid/mtp/MtpDatabase;->ALL_PROPERTIES:[I

    #@11
    goto :goto_5

    #@12
    .line 703
    :sswitch_data_12
    .sparse-switch
        0x0 -> :sswitch_f
        0x3008 -> :sswitch_6
        0x3009 -> :sswitch_6
        0x300a -> :sswitch_9
        0x300b -> :sswitch_9
        0x300c -> :sswitch_9
        0x3801 -> :sswitch_c
        0x3804 -> :sswitch_c
        0x3807 -> :sswitch_c
        0x380b -> :sswitch_c
        0xb901 -> :sswitch_6
        0xb902 -> :sswitch_6
        0xb903 -> :sswitch_6
        0xb982 -> :sswitch_9
        0xb984 -> :sswitch_9
    .end sparse-switch
.end method

.method private getSupportedPlaybackFormats()[I
    .registers 2

    #@0
    .prologue
    .line 496
    const/16 v0, 0x1b

    #@2
    new-array v0, v0, [I

    #@4
    fill-array-data v0, :array_8

    #@7
    return-object v0

    #@8
    :array_8
    .array-data 0x4
        0x0t 0x30t 0x0t 0x0t
        0x1t 0x30t 0x0t 0x0t
        0x4t 0x30t 0x0t 0x0t
        0x5t 0x30t 0x0t 0x0t
        0x8t 0x30t 0x0t 0x0t
        0x9t 0x30t 0x0t 0x0t
        0xbt 0x30t 0x0t 0x0t
        0x1t 0x38t 0x0t 0x0t
        0x4t 0x38t 0x0t 0x0t
        0x7t 0x38t 0x0t 0x0t
        0xbt 0x38t 0x0t 0x0t
        0x1t 0xb9t 0x0t 0x0t
        0x2t 0xb9t 0x0t 0x0t
        0x3t 0xb9t 0x0t 0x0t
        0x82t 0xb9t 0x0t 0x0t
        0x84t 0xb9t 0x0t 0x0t
        0x5t 0xbat 0x0t 0x0t
        0x10t 0xbat 0x0t 0x0t
        0x11t 0xbat 0x0t 0x0t
        0x14t 0xbat 0x0t 0x0t
        0x82t 0xbat 0x0t 0x0t
        0x6t 0xb9t 0x0t 0x0t
        0xat 0x30t 0x0t 0x0t
        0xct 0x30t 0x0t 0x0t
        0x83t 0xbat 0x0t 0x0t
        0x85t 0xbat 0x0t 0x0t
        0x86t 0xbat 0x0t 0x0t
    .end array-data
.end method

.method private getThumbnailData(I[B[I)I
    .registers 21
    .parameter "handle"
    .parameter "outData"
    .parameter "length"

    #@0
    .prologue
    .line 1134
    const/4 v9, 0x0

    #@1
    .line 1136
    .local v9, c:Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v1, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@5
    move-object/from16 v0, p0

    #@7
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@9
    sget-object v3, Landroid/mtp/MtpDatabase;->PATH_FORMAT_PROJECTION:[Ljava/lang/String;

    #@b
    const-string v4, "_id=?"

    #@d
    const/4 v5, 0x1

    #@e
    new-array v5, v5, [Ljava/lang/String;

    #@10
    const/4 v6, 0x0

    #@11
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@14
    move-result-object v7

    #@15
    aput-object v7, v5, v6

    #@17
    const/4 v6, 0x0

    #@18
    const/4 v7, 0x0

    #@19
    invoke-interface/range {v1 .. v7}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1c
    move-result-object v9

    #@1d
    .line 1139
    if-eqz v9, :cond_b3

    #@1f
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_b3

    #@25
    .line 1140
    const/4 v1, 0x1

    #@26
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v16

    #@2a
    .line 1141
    .local v16, path:Ljava/lang/String;
    const/4 v1, 0x2

    #@2b
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    #@2e
    move-result v13

    #@2f
    .line 1143
    .local v13, format:I
    const/4 v14, 0x0

    #@30
    .line 1144
    .local v14, getData:Z
    const/4 v8, 0x0

    #@31
    .line 1145
    .local v8, bitmap:Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    #@32
    .line 1147
    .local v10, data:[B
    const/16 v1, 0x3801

    #@34
    if-eq v13, v1, :cond_42

    #@36
    const/16 v1, 0x380b

    #@38
    if-eq v13, v1, :cond_42

    #@3a
    const/16 v1, 0x3804

    #@3c
    if-eq v13, v1, :cond_42

    #@3e
    const/16 v1, 0x3807

    #@40
    if-ne v13, v1, :cond_79

    #@42
    .line 1152
    :cond_42
    const/4 v1, 0x3

    #@43
    move-object/from16 v0, v16

    #@45
    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    #@48
    move-result-object v8

    #@49
    .line 1157
    :goto_49
    if-eqz v8, :cond_62

    #@4b
    .line 1158
    new-instance v15, Ljava/io/ByteArrayOutputStream;

    #@4d
    invoke-direct {v15}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@50
    .line 1159
    .local v15, miniOutStream:Ljava/io/ByteArrayOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@52
    const/16 v2, 0x55

    #@54
    invoke-virtual {v8, v1, v2, v15}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@57
    .line 1160
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_5a
    .catchall {:try_start_1 .. :try_end_5a} :catchall_bb
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_5a} :catch_9b

    #@5a
    .line 1162
    :try_start_5a
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->close()V

    #@5d
    .line 1163
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_60
    .catchall {:try_start_5a .. :try_end_60} :catchall_bb
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_60} :catch_81
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_60} :catch_9b

    #@60
    move-result-object v10

    #@61
    .line 1164
    const/4 v14, 0x1

    #@62
    .line 1170
    .end local v15           #miniOutStream:Ljava/io/ByteArrayOutputStream;
    :cond_62
    :goto_62
    const/4 v1, 0x1

    #@63
    if-ne v14, v1, :cond_ab

    #@65
    .line 1171
    const/4 v1, 0x0

    #@66
    const/4 v2, 0x0

    #@67
    :try_start_67
    array-length v3, v10

    #@68
    move-object/from16 v0, p2

    #@6a
    invoke-static {v10, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@6d
    .line 1172
    const/4 v1, 0x0

    #@6e
    array-length v2, v10

    #@6f
    aput v2, p3, v1
    :try_end_71
    .catchall {:try_start_67 .. :try_end_71} :catchall_bb
    .catch Landroid/os/RemoteException; {:try_start_67 .. :try_end_71} :catch_9b

    #@71
    .line 1174
    const/16 v1, 0x2001

    #@73
    .line 1185
    if-eqz v9, :cond_78

    #@75
    .line 1186
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@78
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v10           #data:[B
    .end local v13           #format:I
    .end local v14           #getData:Z
    .end local v16           #path:Ljava/lang/String;
    :cond_78
    :goto_78
    return v1

    #@79
    .line 1154
    .restart local v8       #bitmap:Landroid/graphics/Bitmap;
    .restart local v10       #data:[B
    .restart local v13       #format:I
    .restart local v14       #getData:Z
    .restart local v16       #path:Ljava/lang/String;
    :cond_79
    const/4 v1, 0x3

    #@7a
    :try_start_7a
    move-object/from16 v0, v16

    #@7c
    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    #@7f
    move-result-object v8

    #@80
    goto :goto_49

    #@81
    .line 1165
    .restart local v15       #miniOutStream:Ljava/io/ByteArrayOutputStream;
    :catch_81
    move-exception v12

    #@82
    .line 1166
    .local v12, ex:Ljava/io/IOException;
    const-string v1, "MtpDatabase"

    #@84
    new-instance v2, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v3, "got exception ex "

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v2

    #@97
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9a
    .catchall {:try_start_7a .. :try_end_9a} :catchall_bb
    .catch Landroid/os/RemoteException; {:try_start_7a .. :try_end_9a} :catch_9b

    #@9a
    goto :goto_62

    #@9b
    .line 1181
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v10           #data:[B
    .end local v12           #ex:Ljava/io/IOException;
    .end local v13           #format:I
    .end local v14           #getData:Z
    .end local v15           #miniOutStream:Ljava/io/ByteArrayOutputStream;
    .end local v16           #path:Ljava/lang/String;
    :catch_9b
    move-exception v11

    #@9c
    .line 1182
    .local v11, e:Landroid/os/RemoteException;
    :try_start_9c
    const-string v1, "MtpDatabase"

    #@9e
    const-string v2, "RemoteException in getThumbnailData"

    #@a0
    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a3
    .catchall {:try_start_9c .. :try_end_a3} :catchall_bb

    #@a3
    .line 1183
    const/16 v1, 0x2002

    #@a5
    .line 1185
    if-eqz v9, :cond_78

    #@a7
    .line 1186
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@aa
    goto :goto_78

    #@ab
    .line 1176
    .end local v11           #e:Landroid/os/RemoteException;
    .restart local v8       #bitmap:Landroid/graphics/Bitmap;
    .restart local v10       #data:[B
    .restart local v13       #format:I
    .restart local v14       #getData:Z
    .restart local v16       #path:Ljava/lang/String;
    :cond_ab
    const/16 v1, 0x2009

    #@ad
    .line 1185
    if-eqz v9, :cond_78

    #@af
    .line 1186
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@b2
    goto :goto_78

    #@b3
    .line 1179
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v10           #data:[B
    .end local v13           #format:I
    .end local v14           #getData:Z
    .end local v16           #path:Ljava/lang/String;
    :cond_b3
    const/16 v1, 0x2009

    #@b5
    .line 1185
    if-eqz v9, :cond_78

    #@b7
    .line 1186
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@ba
    goto :goto_78

    #@bb
    .line 1185
    :catchall_bb
    move-exception v1

    #@bc
    if-eqz v9, :cond_c1

    #@be
    .line 1186
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c1
    :cond_c1
    throw v1
.end method

.method private inStorageSubDirectory(Ljava/lang/String;)Z
    .registers 9
    .parameter "path"

    #@0
    .prologue
    .line 238
    iget-object v5, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@2
    if-nez v5, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    .line 252
    :cond_5
    :goto_5
    return v0

    #@6
    .line 239
    :cond_6
    if-nez p1, :cond_a

    #@8
    const/4 v0, 0x0

    #@9
    goto :goto_5

    #@a
    .line 241
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 242
    .local v0, allowed:Z
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v2

    #@f
    .line 243
    .local v2, pathLength:I
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    iget-object v5, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@12
    array-length v5, v5

    #@13
    if-ge v1, v5, :cond_5

    #@15
    if-nez v0, :cond_5

    #@17
    .line 244
    iget-object v5, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@19
    aget-object v3, v5, v1

    #@1b
    .line 245
    .local v3, subdir:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1e
    move-result v4

    #@1f
    .line 246
    .local v4, subdirLength:I
    if-ge v4, v2, :cond_30

    #@21
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@24
    move-result v5

    #@25
    const/16 v6, 0x2f

    #@27
    if-ne v5, v6, :cond_30

    #@29
    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2c
    move-result v5

    #@2d
    if-eqz v5, :cond_30

    #@2f
    .line 249
    const/4 v0, 0x1

    #@30
    .line 243
    :cond_30
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_10
.end method

.method private initDeviceProperties(Landroid/content/Context;)V
    .registers 16
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 201
    const-string v10, "device-properties"

    #@3
    .line 202
    .local v10, devicePropertiesName:Ljava/lang/String;
    const-string v1, "device-properties"

    #@5
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@8
    move-result-object v1

    #@9
    iput-object v1, p0, Landroid/mtp/MtpDatabase;->mDeviceProperties:Landroid/content/SharedPreferences;

    #@b
    .line 203
    const-string v1, "device-properties"

    #@d
    invoke-virtual {p1, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    #@10
    move-result-object v9

    #@11
    .line 205
    .local v9, databaseFile:Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_73

    #@17
    .line 208
    const/4 v0, 0x0

    #@18
    .line 209
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    #@19
    .line 211
    .local v8, c:Landroid/database/Cursor;
    :try_start_19
    const-string v1, "device-properties"

    #@1b
    const/4 v2, 0x0

    #@1c
    const/4 v3, 0x0

    #@1d
    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    #@20
    move-result-object v0

    #@21
    .line 212
    if-eqz v0, :cond_77

    #@23
    .line 213
    const-string/jumbo v1, "properties"

    #@26
    const/4 v2, 0x3

    #@27
    new-array v2, v2, [Ljava/lang/String;

    #@29
    const/4 v3, 0x0

    #@2a
    const-string v4, "_id"

    #@2c
    aput-object v4, v2, v3

    #@2e
    const/4 v3, 0x1

    #@2f
    const-string v4, "code"

    #@31
    aput-object v4, v2, v3

    #@33
    const/4 v3, 0x2

    #@34
    const-string/jumbo v4, "value"

    #@37
    aput-object v4, v2, v3

    #@39
    const/4 v3, 0x0

    #@3a
    const/4 v4, 0x0

    #@3b
    const/4 v5, 0x0

    #@3c
    const/4 v6, 0x0

    #@3d
    const/4 v7, 0x0

    #@3e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@41
    move-result-object v8

    #@42
    .line 215
    if-eqz v8, :cond_77

    #@44
    .line 216
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mDeviceProperties:Landroid/content/SharedPreferences;

    #@46
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@49
    move-result-object v11

    #@4a
    .line 217
    .local v11, e:Landroid/content/SharedPreferences$Editor;
    :goto_4a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@4d
    move-result v1

    #@4e
    if-eqz v1, :cond_74

    #@50
    .line 218
    const/4 v1, 0x1

    #@51
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    .line 219
    .local v12, name:Ljava/lang/String;
    const/4 v1, 0x2

    #@56
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@59
    move-result-object v13

    #@5a
    .line 220
    .local v13, value:Ljava/lang/String;
    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_5d
    .catchall {:try_start_19 .. :try_end_5d} :catchall_82
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_4a

    #@5e
    .line 225
    .end local v11           #e:Landroid/content/SharedPreferences$Editor;
    .end local v12           #name:Ljava/lang/String;
    .end local v13           #value:Ljava/lang/String;
    :catch_5e
    move-exception v11

    #@5f
    .line 226
    .local v11, e:Ljava/lang/Exception;
    :try_start_5f
    const-string v1, "MtpDatabase"

    #@61
    const-string v2, "failed to migrate device properties"

    #@63
    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_66
    .catchall {:try_start_5f .. :try_end_66} :catchall_82

    #@66
    .line 228
    if-eqz v8, :cond_6b

    #@68
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6b
    .line 229
    :cond_6b
    if-eqz v0, :cond_70

    #@6d
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@70
    .line 231
    .end local v11           #e:Ljava/lang/Exception;
    :cond_70
    :goto_70
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    #@73
    .line 233
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v8           #c:Landroid/database/Cursor;
    :cond_73
    return-void

    #@74
    .line 222
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8       #c:Landroid/database/Cursor;
    .local v11, e:Landroid/content/SharedPreferences$Editor;
    :cond_74
    :try_start_74
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_77
    .catchall {:try_start_74 .. :try_end_77} :catchall_82
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_77} :catch_5e

    #@77
    .line 228
    .end local v11           #e:Landroid/content/SharedPreferences$Editor;
    :cond_77
    if-eqz v8, :cond_7c

    #@79
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@7c
    .line 229
    :cond_7c
    if-eqz v0, :cond_70

    #@7e
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@81
    goto :goto_70

    #@82
    .line 228
    :catchall_82
    move-exception v1

    #@83
    if-eqz v8, :cond_88

    #@85
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@88
    .line 229
    :cond_88
    if-eqz v0, :cond_8d

    #@8a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    #@8d
    :cond_8d
    throw v1
.end method

.method private isStorageSubDirectory(Ljava/lang/String;)Z
    .registers 5
    .parameter "path"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 258
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 264
    :cond_5
    :goto_5
    return v1

    #@6
    .line 259
    :cond_6
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@9
    array-length v2, v2

    #@a
    if-ge v0, v2, :cond_5

    #@c
    .line 260
    iget-object v2, p0, Landroid/mtp/MtpDatabase;->mSubDirectories:[Ljava/lang/String;

    #@e
    aget-object v2, v2, v0

    #@10
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_18

    #@16
    .line 261
    const/4 v1, 0x1

    #@17
    goto :goto_5

    #@18
    .line 259
    :cond_18
    add-int/lit8 v0, v0, 0x1

    #@1a
    goto :goto_7
.end method

.method private final native native_finalize()V
.end method

.method private final native native_setup()V
.end method

.method private renameFile(ILjava/lang/String;)I
    .registers 22
    .parameter "handle"
    .parameter "newName"

    #@0
    .prologue
    .line 768
    const/4 v9, 0x0

    #@1
    .line 771
    .local v9, c:Landroid/database/Cursor;
    const/4 v15, 0x0

    #@2
    .line 772
    .local v15, path:Ljava/lang/String;
    const/4 v2, 0x1

    #@3
    new-array v6, v2, [Ljava/lang/String;

    #@5
    const/4 v2, 0x0

    #@6
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9
    move-result-object v3

    #@a
    aput-object v3, v6, v2

    #@c
    .line 774
    .local v6, whereArgs:[Ljava/lang/String;
    :try_start_c
    move-object/from16 v0, p0

    #@e
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@10
    move-object/from16 v0, p0

    #@12
    iget-object v3, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@14
    sget-object v4, Landroid/mtp/MtpDatabase;->PATH_PROJECTION:[Ljava/lang/String;

    #@16
    const-string v5, "_id=?"

    #@18
    const/4 v7, 0x0

    #@19
    const/4 v8, 0x0

    #@1a
    invoke-interface/range {v2 .. v8}, Landroid/content/IContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/ICancellationSignal;)Landroid/database/Cursor;

    #@1d
    move-result-object v9

    #@1e
    .line 775
    if-eqz v9, :cond_2b

    #@20
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_2b

    #@26
    .line 776
    const/4 v2, 0x1

    #@27
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2a
    .catchall {:try_start_c .. :try_end_2a} :catchall_45
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_2a} :catch_35

    #@2a
    move-result-object v15

    #@2b
    .line 782
    :cond_2b
    if-eqz v9, :cond_30

    #@2d
    .line 783
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@30
    .line 786
    :cond_30
    if-nez v15, :cond_4c

    #@32
    .line 787
    const/16 v2, 0x2009

    #@34
    .line 860
    :cond_34
    :goto_34
    return v2

    #@35
    .line 778
    :catch_35
    move-exception v10

    #@36
    .line 779
    .local v10, e:Landroid/os/RemoteException;
    :try_start_36
    const-string v2, "MtpDatabase"

    #@38
    const-string v3, "RemoteException in getObjectFilePath"

    #@3a
    invoke-static {v2, v3, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3d
    .catchall {:try_start_36 .. :try_end_3d} :catchall_45

    #@3d
    .line 780
    const/16 v2, 0x2002

    #@3f
    .line 782
    if-eqz v9, :cond_34

    #@41
    .line 783
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@44
    goto :goto_34

    #@45
    .line 782
    .end local v10           #e:Landroid/os/RemoteException;
    :catchall_45
    move-exception v2

    #@46
    if-eqz v9, :cond_4b

    #@48
    .line 783
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@4b
    :cond_4b
    throw v2

    #@4c
    .line 791
    :cond_4c
    move-object/from16 v0, p0

    #@4e
    invoke-direct {v0, v15}, Landroid/mtp/MtpDatabase;->isStorageSubDirectory(Ljava/lang/String;)Z

    #@51
    move-result v2

    #@52
    if-eqz v2, :cond_57

    #@54
    .line 792
    const/16 v2, 0x200d

    #@56
    goto :goto_34

    #@57
    .line 796
    :cond_57
    new-instance v14, Ljava/io/File;

    #@59
    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@5c
    .line 797
    .local v14, oldFile:Ljava/io/File;
    const/16 v2, 0x2f

    #@5e
    invoke-virtual {v15, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@61
    move-result v11

    #@62
    .line 798
    .local v11, lastSlash:I
    const/4 v2, 0x1

    #@63
    if-gt v11, v2, :cond_68

    #@65
    .line 799
    const/16 v2, 0x2002

    #@67
    goto :goto_34

    #@68
    .line 801
    :cond_68
    new-instance v2, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const/4 v3, 0x0

    #@6e
    add-int/lit8 v4, v11, 0x1

    #@70
    invoke-virtual {v15, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v2

    #@78
    move-object/from16 v0, p2

    #@7a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v13

    #@82
    .line 804
    .local v13, newPath:Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@85
    move-result v2

    #@86
    const/16 v3, 0xff

    #@88
    if-lt v2, v3, :cond_91

    #@8a
    .line 805
    const/4 v2, 0x0

    #@8b
    const/16 v3, 0xfe

    #@8d
    invoke-virtual {v13, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@90
    move-result-object v13

    #@91
    .line 809
    :cond_91
    new-instance v12, Ljava/io/File;

    #@93
    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@96
    .line 810
    .local v12, newFile:Ljava/io/File;
    invoke-virtual {v14, v12}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@99
    move-result v16

    #@9a
    .line 811
    .local v16, success:Z
    if-nez v16, :cond_c9

    #@9c
    .line 812
    const-string v2, "MtpDatabase"

    #@9e
    new-instance v3, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string/jumbo v4, "renaming "

    #@a6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v3

    #@aa
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v3

    #@ae
    const-string v4, " to "

    #@b0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v3

    #@b4
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v3

    #@b8
    const-string v4, " failed"

    #@ba
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v3

    #@be
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v3

    #@c2
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    .line 813
    const/16 v2, 0x2002

    #@c7
    goto/16 :goto_34

    #@c9
    .line 817
    :cond_c9
    new-instance v18, Landroid/content/ContentValues;

    #@cb
    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    #@ce
    .line 818
    .local v18, values:Landroid/content/ContentValues;
    const-string v2, "_data"

    #@d0
    move-object/from16 v0, v18

    #@d2
    invoke-virtual {v0, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d5
    .line 820
    const-string v2, "_display_name"

    #@d7
    move-object/from16 v0, v18

    #@d9
    move-object/from16 v1, p2

    #@db
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@de
    .line 822
    const/16 v17, 0x0

    #@e0
    .line 826
    .local v17, updated:I
    :try_start_e0
    move-object/from16 v0, p0

    #@e2
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@e4
    move-object/from16 v0, p0

    #@e6
    iget-object v3, v0, Landroid/mtp/MtpDatabase;->mObjectsUri:Landroid/net/Uri;

    #@e8
    const-string v4, "_id=?"

    #@ea
    move-object/from16 v0, v18

    #@ec
    invoke-interface {v2, v3, v0, v4, v6}, Landroid/content/IContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_ef
    .catch Landroid/os/RemoteException; {:try_start_e0 .. :try_end_ef} :catch_11b

    #@ef
    move-result v17

    #@f0
    .line 830
    :goto_f0
    if-nez v17, :cond_124

    #@f2
    .line 831
    const-string v2, "MtpDatabase"

    #@f4
    new-instance v3, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v4, "Unable to update path for "

    #@fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v3

    #@103
    const-string v4, " to "

    #@105
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v3

    #@10d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v3

    #@111
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 833
    invoke-virtual {v12, v14}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    #@117
    .line 834
    const/16 v2, 0x2002

    #@119
    goto/16 :goto_34

    #@11b
    .line 827
    :catch_11b
    move-exception v10

    #@11c
    .line 828
    .restart local v10       #e:Landroid/os/RemoteException;
    const-string v2, "MtpDatabase"

    #@11e
    const-string v3, "RemoteException in mMediaProvider.update"

    #@120
    invoke-static {v2, v3, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@123
    goto :goto_f0

    #@124
    .line 838
    .end local v10           #e:Landroid/os/RemoteException;
    :cond_124
    invoke-virtual {v12}, Ljava/io/File;->isDirectory()Z

    #@127
    move-result v2

    #@128
    if-eqz v2, :cond_167

    #@12a
    .line 840
    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    #@12d
    move-result-object v2

    #@12e
    const-string v3, "."

    #@130
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@133
    move-result v2

    #@134
    if-eqz v2, :cond_149

    #@136
    const-string v2, "."

    #@138
    invoke-virtual {v13, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@13b
    move-result v2

    #@13c
    if-nez v2, :cond_149

    #@13e
    .line 843
    :try_start_13e
    move-object/from16 v0, p0

    #@140
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@142
    const-string/jumbo v3, "unhide"

    #@145
    const/4 v4, 0x0

    #@146
    invoke-interface {v2, v3, v13, v4}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_149
    .catch Landroid/os/RemoteException; {:try_start_13e .. :try_end_149} :catch_14d

    #@149
    .line 860
    :cond_149
    :goto_149
    const/16 v2, 0x2001

    #@14b
    goto/16 :goto_34

    #@14d
    .line 844
    :catch_14d
    move-exception v10

    #@14e
    .line 845
    .restart local v10       #e:Landroid/os/RemoteException;
    const-string v2, "MtpDatabase"

    #@150
    new-instance v3, Ljava/lang/StringBuilder;

    #@152
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@155
    const-string v4, "failed to unhide/rescan for "

    #@157
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v3

    #@15b
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v3

    #@15f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v3

    #@163
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@166
    goto :goto_149

    #@167
    .line 850
    .end local v10           #e:Landroid/os/RemoteException;
    :cond_167
    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    #@16a
    move-result-object v2

    #@16b
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@16d
    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@170
    move-result-object v2

    #@171
    const-string v3, ".nomedia"

    #@173
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@176
    move-result v2

    #@177
    if-eqz v2, :cond_149

    #@179
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@17b
    invoke-virtual {v13, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    const-string v3, ".nomedia"

    #@181
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@184
    move-result v2

    #@185
    if-nez v2, :cond_149

    #@187
    .line 853
    :try_start_187
    move-object/from16 v0, p0

    #@189
    iget-object v2, v0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@18b
    const-string/jumbo v3, "unhide"

    #@18e
    invoke-virtual {v14}, Ljava/io/File;->getParent()Ljava/lang/String;

    #@191
    move-result-object v4

    #@192
    const/4 v5, 0x0

    #@193
    invoke-interface {v2, v3, v4, v5}, Landroid/content/IContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_196
    .catch Landroid/os/RemoteException; {:try_start_187 .. :try_end_196} :catch_197

    #@196
    goto :goto_149

    #@197
    .line 854
    :catch_197
    move-exception v10

    #@198
    .line 855
    .restart local v10       #e:Landroid/os/RemoteException;
    const-string v2, "MtpDatabase"

    #@19a
    new-instance v3, Ljava/lang/StringBuilder;

    #@19c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19f
    const-string v4, "failed to unhide/rescan for "

    #@1a1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v3

    #@1a5
    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v3

    #@1a9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v3

    #@1ad
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    goto :goto_149
.end method

.method private sessionEnded()V
    .registers 4

    #@0
    .prologue
    .line 1125
    iget-boolean v0, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@2
    if-eqz v0, :cond_13

    #@4
    .line 1126
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mContext:Landroid/content/Context;

    #@6
    new-instance v1, Landroid/content/Intent;

    #@8
    const-string v2, "android.provider.action.MTP_SESSION_END"

    #@a
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@10
    .line 1127
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@13
    .line 1129
    :cond_13
    return-void
.end method

.method private sessionStarted()V
    .registers 2

    #@0
    .prologue
    .line 1121
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@3
    .line 1122
    return-void
.end method

.method private setDeviceProperty(IJLjava/lang/String;)I
    .registers 7
    .parameter "property"
    .parameter "intValue"
    .parameter "stringValue"

    #@0
    .prologue
    .line 928
    packed-switch p1, :pswitch_data_20

    #@3
    .line 938
    const/16 v1, 0x200a

    #@5
    :goto_5
    return v1

    #@6
    .line 932
    :pswitch_6
    iget-object v1, p0, Landroid/mtp/MtpDatabase;->mDeviceProperties:Landroid/content/SharedPreferences;

    #@8
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@b
    move-result-object v0

    #@c
    .line 933
    .local v0, e:Landroid/content/SharedPreferences$Editor;
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-interface {v0, v1, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@13
    .line 934
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_1c

    #@19
    const/16 v1, 0x2001

    #@1b
    goto :goto_5

    #@1c
    :cond_1c
    const/16 v1, 0x2002

    #@1e
    goto :goto_5

    #@1f
    .line 928
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0xd401
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private setObjectProperty(IIJLjava/lang/String;)I
    .registers 7
    .parameter "handle"
    .parameter "property"
    .parameter "intValue"
    .parameter "stringValue"

    #@0
    .prologue
    .line 865
    packed-switch p2, :pswitch_data_c

    #@3
    .line 870
    const v0, 0xa80a

    #@6
    :goto_6
    return v0

    #@7
    .line 867
    :pswitch_7
    invoke-direct {p0, p1, p5}, Landroid/mtp/MtpDatabase;->renameFile(ILjava/lang/String;)I

    #@a
    move-result v0

    #@b
    goto :goto_6

    #@c
    .line 865
    :pswitch_data_c
    .packed-switch 0xdc07
        :pswitch_7
    .end packed-switch
.end method

.method private setObjectReferences(I[I)I
    .registers 12
    .parameter "handle"
    .parameter "references"

    #@0
    .prologue
    .line 1101
    const/4 v6, 0x1

    #@1
    iput-boolean v6, p0, Landroid/mtp/MtpDatabase;->mDatabaseModified:Z

    #@3
    .line 1102
    iget-object v6, p0, Landroid/mtp/MtpDatabase;->mVolumeName:Ljava/lang/String;

    #@5
    int-to-long v7, p1

    #@6
    invoke-static {v6, v7, v8}, Landroid/provider/MediaStore$Files;->getMtpReferencesUri(Ljava/lang/String;J)Landroid/net/Uri;

    #@9
    move-result-object v3

    #@a
    .line 1103
    .local v3, uri:Landroid/net/Uri;
    array-length v0, p2

    #@b
    .line 1104
    .local v0, count:I
    new-array v5, v0, [Landroid/content/ContentValues;

    #@d
    .line 1105
    .local v5, valuesList:[Landroid/content/ContentValues;
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v0, :cond_25

    #@10
    .line 1106
    new-instance v4, Landroid/content/ContentValues;

    #@12
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    #@15
    .line 1107
    .local v4, values:Landroid/content/ContentValues;
    const-string v6, "_id"

    #@17
    aget v7, p2, v2

    #@19
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@20
    .line 1108
    aput-object v4, v5, v2

    #@22
    .line 1105
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_e

    #@25
    .line 1111
    .end local v4           #values:Landroid/content/ContentValues;
    :cond_25
    :try_start_25
    iget-object v6, p0, Landroid/mtp/MtpDatabase;->mMediaProvider:Landroid/content/IContentProvider;

    #@27
    invoke-interface {v6, v3, v5}, Landroid/content/IContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_2a
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_2a} :catch_30

    #@2a
    move-result v6

    #@2b
    if-lez v6, :cond_38

    #@2d
    .line 1112
    const/16 v6, 0x2001

    #@2f
    .line 1117
    :goto_2f
    return v6

    #@30
    .line 1114
    :catch_30
    move-exception v1

    #@31
    .line 1115
    .local v1, e:Landroid/os/RemoteException;
    const-string v6, "MtpDatabase"

    #@33
    const-string v7, "RemoteException in setObjectReferences"

    #@35
    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@38
    .line 1117
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_38
    const/16 v6, 0x2002

    #@3a
    goto :goto_2f
.end method


# virtual methods
.method public addStorage(Landroid/mtp/MtpStorage;)V
    .registers 4
    .parameter "storage"

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mStorageMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {p1}, Landroid/mtp/MtpStorage;->getPath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 194
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 186
    :try_start_0
    invoke-direct {p0}, Landroid/mtp/MtpDatabase;->native_finalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 188
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 190
    return-void

    #@7
    .line 188
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public removeStorage(Landroid/mtp/MtpStorage;)V
    .registers 4
    .parameter "storage"

    #@0
    .prologue
    .line 197
    iget-object v0, p0, Landroid/mtp/MtpDatabase;->mStorageMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {p1}, Landroid/mtp/MtpStorage;->getPath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 198
    return-void
.end method
