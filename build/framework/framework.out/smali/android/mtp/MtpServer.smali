.class public Landroid/mtp/MtpServer;
.super Ljava/lang/Object;
.source "MtpServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 28
    const-string/jumbo v0, "media_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/mtp/MtpDatabase;Z)V
    .registers 3
    .parameter "database"
    .parameter "usePtp"

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/mtp/MtpServer;->native_setup(Landroid/mtp/MtpDatabase;Z)V

    #@6
    .line 33
    return-void
.end method

.method private final native native_add_storage(Landroid/mtp/MtpStorage;)V
.end method

.method private final native native_cleanup()V
.end method

.method private final native_is_busy()Z
	.registers 2
	const/4 v0, 0x0
	return v0
.end method

.method private final native native_remove_storage(I)V
.end method

.method private final native native_run()V
.end method

.method private final native native_send_object_added(I)V
.end method

.method private final native native_send_object_removed(I)V
.end method

.method private final native native_setup(Landroid/mtp/MtpDatabase;Z)V
.end method


# virtual methods
.method public addStorage(Landroid/mtp/MtpStorage;)V
    .registers 2
    .parameter "storage"

    #@0
    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/mtp/MtpServer;->native_add_storage(Landroid/mtp/MtpStorage;)V

    #@3
    .line 56
    return-void
.end method

.method public isBusy()Z
    .registers 2

    #@0
    .prologue
    .line 64
    invoke-direct {p0}, Landroid/mtp/MtpServer;->native_is_busy()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public removeStorage(Landroid/mtp/MtpStorage;)V
    .registers 3
    .parameter "storage"

    #@0
    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/mtp/MtpStorage;->getStorageId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/mtp/MtpServer;->native_remove_storage(I)V

    #@7
    .line 60
    return-void
.end method

.method public run()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/mtp/MtpServer;->native_run()V

    #@3
    .line 43
    invoke-direct {p0}, Landroid/mtp/MtpServer;->native_cleanup()V

    #@6
    .line 44
    return-void
.end method

.method public sendObjectAdded(I)V
    .registers 2
    .parameter "handle"

    #@0
    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/mtp/MtpServer;->native_send_object_added(I)V

    #@3
    .line 48
    return-void
.end method

.method public sendObjectRemoved(I)V
    .registers 2
    .parameter "handle"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/mtp/MtpServer;->native_send_object_removed(I)V

    #@3
    .line 52
    return-void
.end method

.method public start()V
    .registers 3

    #@0
    .prologue
    .line 36
    new-instance v0, Ljava/lang/Thread;

    #@2
    const-string v1, "MtpServer"

    #@4
    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    #@7
    .line 37
    .local v0, thread:Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@a
    .line 38
    return-void
.end method
