.class public Landroid/mtp/MtpStorage;
.super Ljava/lang/Object;
.source "MtpStorage.java"


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mMaxFileSize:J

.field private final mPath:Ljava/lang/String;

.field private final mRemovable:Z

.field private final mReserveSpace:J

.field private final mStorageId:I


# direct methods
.method public constructor <init>(Landroid/os/storage/StorageVolume;Landroid/content/Context;)V
    .registers 5
    .parameter "volume"
    .parameter "context"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getStorageId()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/mtp/MtpStorage;->mStorageId:I

    #@9
    .line 40
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/mtp/MtpStorage;->mPath:Ljava/lang/String;

    #@f
    .line 41
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getDescriptionId()I

    #@16
    move-result v1

    #@17
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/mtp/MtpStorage;->mDescription:Ljava/lang/String;

    #@1d
    .line 42
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getMtpReserveSpace()I

    #@20
    move-result v0

    #@21
    mul-int/lit16 v0, v0, 0x400

    #@23
    mul-int/lit16 v0, v0, 0x400

    #@25
    int-to-long v0, v0

    #@26
    iput-wide v0, p0, Landroid/mtp/MtpStorage;->mReserveSpace:J

    #@28
    .line 43
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@2b
    move-result v0

    #@2c
    iput-boolean v0, p0, Landroid/mtp/MtpStorage;->mRemovable:Z

    #@2e
    .line 44
    invoke-virtual {p1}, Landroid/os/storage/StorageVolume;->getMaxFileSize()J

    #@31
    move-result-wide v0

    #@32
    iput-wide v0, p0, Landroid/mtp/MtpStorage;->mMaxFileSize:J

    #@34
    .line 45
    return-void
.end method

.method public static getStorageId(I)I
    .registers 2
    .parameter "index"

    #@0
    .prologue
    .line 65
    add-int/lit8 v0, p0, 0x1

    #@2
    shl-int/lit8 v0, v0, 0x10

    #@4
    add-int/lit8 v0, v0, 0x1

    #@6
    return v0
.end method


# virtual methods
.method public final getDescription()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 83
    iget-object v0, p0, Landroid/mtp/MtpStorage;->mDescription:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMaxFileSize()J
    .registers 3

    #@0
    .prologue
    .line 111
    iget-wide v0, p0, Landroid/mtp/MtpStorage;->mMaxFileSize:J

    #@2
    return-wide v0
.end method

.method public final getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Landroid/mtp/MtpStorage;->mPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getReserveSpace()J
    .registers 3

    #@0
    .prologue
    .line 93
    iget-wide v0, p0, Landroid/mtp/MtpStorage;->mReserveSpace:J

    #@2
    return-wide v0
.end method

.method public final getStorageId()I
    .registers 2

    #@0
    .prologue
    .line 53
    iget v0, p0, Landroid/mtp/MtpStorage;->mStorageId:I

    #@2
    return v0
.end method

.method public final isRemovable()Z
    .registers 2

    #@0
    .prologue
    .line 102
    iget-boolean v0, p0, Landroid/mtp/MtpStorage;->mRemovable:Z

    #@2
    return v0
.end method
