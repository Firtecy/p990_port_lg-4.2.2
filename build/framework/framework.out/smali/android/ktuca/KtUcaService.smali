.class public Landroid/ktuca/KtUcaService;
.super Landroid/ktuca/IKtUcaIF$Stub;
.source "KtUcaService.java"


# instance fields
.field private KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

.field private final TAG:Ljava/lang/String;

.field private mChannel:[B

.field private mChannelLen:[I

.field private mContext:Landroid/content/Context;

.field private referenceCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/ktuca/IKtUcaIF$Stub;-><init>()V

    #@3
    .line 11
    const-string v2, "KT_UCA_SERVICE"

    #@5
    iput-object v2, p0, Landroid/ktuca/KtUcaService;->TAG:Ljava/lang/String;

    #@7
    .line 12
    const/4 v2, 0x0

    #@8
    iput v2, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@a
    .line 14
    const/16 v2, 0xa

    #@c
    new-array v2, v2, [B

    #@e
    iput-object v2, p0, Landroid/ktuca/KtUcaService;->mChannel:[B

    #@10
    .line 15
    const/4 v2, 0x1

    #@11
    new-array v2, v2, [I

    #@13
    iput-object v2, p0, Landroid/ktuca/KtUcaService;->mChannelLen:[I

    #@15
    .line 16
    const/4 v2, 0x0

    #@16
    iput-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@18
    .line 20
    const-wide/16 v0, -0x1

    #@1a
    .line 21
    .local v0, ret:J
    iput-object p1, p0, Landroid/ktuca/KtUcaService;->mContext:Landroid/content/Context;

    #@1c
    .line 22
    const-string v2, "KT_UCA_SERVICE"

    #@1e
    const-string v3, "+[KtUcaService]"

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 23
    new-instance v2, Ljava/lang/Thread;

    #@25
    new-instance v3, Landroid/ktuca/KtUcaService$1;

    #@27
    invoke-direct {v3, p0}, Landroid/ktuca/KtUcaService$1;-><init>(Landroid/ktuca/KtUcaService;)V

    #@2a
    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    #@2d
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    #@30
    .line 33
    const-string v2, "KT_UCA_SERVICE"

    #@32
    const-string v3, "-[KtUcaService]"

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 34
    return-void
.end method

.method static synthetic access$000(Landroid/ktuca/KtUcaService;)Landroid/ktuca/KtUcaServiceJni;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 10
    iget-object v0, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Landroid/ktuca/KtUcaService;Landroid/ktuca/KtUcaServiceJni;)Landroid/ktuca/KtUcaServiceJni;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 10
    iput-object p1, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Landroid/ktuca/KtUcaService;)[B
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 10
    iget-object v0, p0, Landroid/ktuca/KtUcaService;->mChannel:[B

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/ktuca/KtUcaService;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 10
    iget-object v0, p0, Landroid/ktuca/KtUcaService;->mChannelLen:[I

    #@2
    return-object v0
.end method


# virtual methods
.method public KUCA_CHInit(B[B[I)J
    .registers 9
    .parameter "ucatag"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 189
    const-wide/16 v0, -0x1

    #@2
    .line 191
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_CHInit]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 192
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3}, Landroid/ktuca/KtUcaServiceJni;->KUCA_CHInit(B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 193
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_CHInit] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 194
    return-wide v0
.end method

.method public KUCA_Close([BB)J
    .registers 8
    .parameter "handle"
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    const-wide/16 v0, -0x1

    #@2
    .line 164
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_Close]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 165
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2}, Landroid/ktuca/KtUcaServiceJni;->KUCA_Close([BB)J

    #@e
    move-result-wide v0

    #@f
    .line 166
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_Close] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 167
    return-wide v0
.end method

.method public KUCA_CloseT([BB)J
    .registers 7
    .parameter "appId"
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 243
    const-wide/16 v0, -0x1

    #@2
    .line 245
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_CloseT]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 246
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2}, Landroid/ktuca/KtUcaServiceJni;->KUCA_CloseT([BB)J

    #@e
    move-result-wide v0

    #@f
    .line 247
    const-string v2, "KT_UCA_SERVICE"

    #@11
    const-string v3, "-[KUCA_CloseT]"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 248
    return-wide v0
.end method

.method public KUCA_KUH_Establish(B)J
    .registers 7
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 207
    const-wide/16 v0, -0x1

    #@2
    .line 209
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_KUH_Establish]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 210
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1}, Landroid/ktuca/KtUcaServiceJni;->KUCA_KUH_Establish(B)J

    #@e
    move-result-wide v0

    #@f
    .line 211
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_KUH_Establish] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 212
    return-wide v0
.end method

.method public KUCA_KUH_Release(B)J
    .registers 6
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 216
    const-wide/16 v0, -0x1

    #@2
    .line 218
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_KUH_Release]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 219
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1}, Landroid/ktuca/KtUcaServiceJni;->KUCA_KUH_Release(B)J

    #@e
    move-result-wide v0

    #@f
    .line 220
    const-string v2, "KT_UCA_SERVICE"

    #@11
    const-string v3, "+[KUCA_KUH_Release]"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 221
    return-wide v0
.end method

.method public KUCA_KUH_Transmit(B[BI[B[I)J
    .registers 10
    .parameter "ucatag"
    .parameter "pbSendBuffer"
    .parameter "cbSendLength"
    .parameter "pbRecvBuffer"
    .parameter "pcbRecvLength"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 225
    const-wide/16 v0, -0x1

    #@2
    .line 227
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_KUH_Transmit]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 228
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_KUH_Transmit(B[BI[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 229
    const-string v2, "KT_UCA_SERVICE"

    #@11
    const-string v3, "-[KUCA_KUH_Transmit]"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 230
    return-wide v0
.end method

.method public KUCA_Open([B[B[I)J
    .registers 9
    .parameter "handle"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    const-wide/16 v0, -0x1

    #@2
    .line 145
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_Open]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 146
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3}, Landroid/ktuca/KtUcaServiceJni;->KUCA_Open([B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 147
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_Open] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 148
    return-wide v0
.end method

.method public KUCA_OpenT([B[B[I)J
    .registers 8
    .parameter "appId"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 234
    const-wide/16 v0, -0x1

    #@2
    .line 236
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_OpenT]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 237
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3}, Landroid/ktuca/KtUcaServiceJni;->KUCA_OpenT([B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 238
    const-string v2, "KT_UCA_SERVICE"

    #@11
    const-string v3, "-[KUCA_OpenT]"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 239
    return-wide v0
.end method

.method public KUCA_Transmit([B[BI[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "input"
    .parameter "inputLen"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    const-wide/16 v0, -0x1

    #@2
    .line 155
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_Transmit]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 156
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_Transmit([B[BI[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 157
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_Transmit] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 158
    return-wide v0
.end method

.method public KUCA_UCAVersion([B[B[I)J
    .registers 9
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 180
    const-wide/16 v0, -0x1

    #@2
    .line 182
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_UCAVersion]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 183
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3}, Landroid/ktuca/KtUcaServiceJni;->KUCA_UCAVersion([B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 184
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_UCAVersion] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 185
    return-wide v0
.end method

.method public KUCA_getHandle([B[B[B[B[I)J
    .registers 14
    .parameter "callerId"
    .parameter "preKey"
    .parameter "appId"
    .parameter "handle"
    .parameter "handleLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    const-wide/16 v6, -0x1

    #@2
    .line 40
    .local v6, ret:J
    const-string v0, "KT_UCA_SERVICE"

    #@4
    const-string v1, "+[KUCA_getHandle]"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 41
    iget-object v0, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v3, p3

    #@e
    move-object v4, p4

    #@f
    move-object v5, p5

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/KtUcaServiceJni;->uca_getHandle([B[B[B[B[I)J

    #@13
    move-result-wide v6

    #@14
    .line 42
    const-string v0, "KT_UCA_SERVICE"

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "-[KUCA_getHandle] ret : "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 43
    return-wide v6
.end method

.method public KUCA_getICCID([B[B[II[B)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 68
    const-wide/16 v0, -0x1

    #@2
    .line 70
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getICCID]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 71
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getICCID([B[B[II[B)J

    #@e
    move-result-wide v0

    #@f
    .line 72
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getICCID] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 73
    return-wide v0
.end method

.method public KUCA_getIMSI([B[B[II[B)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 58
    const-wide/16 v0, -0x1

    #@2
    .line 60
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getIMSI]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 61
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getIMSI([B[B[II[B)J

    #@e
    move-result-wide v0

    #@f
    .line 62
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getIMSI] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 63
    return-wide v0
.end method

.method public KUCA_getMDN([B[B[II[B)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    const-wide/16 v0, -0x1

    #@2
    .line 90
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getMDN]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 91
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getMDN([B[B[II[B)J

    #@e
    move-result-wide v0

    #@f
    .line 92
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getMDN] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 93
    return-wide v0
.end method

.method public KUCA_getMODEL([B[B[II[B)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 98
    const-wide/16 v0, -0x1

    #@2
    .line 100
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getMODEL]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 101
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getMODEL([B[B[II[B)J

    #@e
    move-result-wide v0

    #@f
    .line 102
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getMODEL] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 103
    return-wide v0
.end method

.method public KUCA_getMSISDN([B[B[II[B)J
    .registers 14
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 48
    const-wide/16 v6, -0x1

    #@2
    .line 50
    .local v6, ret:J
    const-string v0, "KT_UCA_SERVICE"

    #@4
    const-string v1, "+[KUCA_getMSISDN]"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 51
    iget-object v0, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v3, p3

    #@e
    move v4, p4

    #@f
    move-object v5, p5

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/KtUcaServiceJni;->uca_getMSISDN([B[B[II[B)J

    #@13
    move-result-wide v6

    #@14
    .line 52
    const-string v0, "KT_UCA_SERVICE"

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "-[KUCA_getMSISDN] ret : "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 53
    return-wide v6
.end method

.method public KUCA_getPUID([B[B[II[B)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 78
    const-wide/16 v0, -0x1

    #@2
    .line 80
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getPUID]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 81
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getPUID([B[B[II[B)J

    #@e
    move-result-wide v0

    #@f
    .line 82
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getPUID] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 83
    return-wide v0
.end method

.method public KUCA_getPinStatus([BI[B[I)J
    .registers 10
    .parameter "handle"
    .parameter "pinId"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 125
    const-wide/16 v0, -0x1

    #@2
    .line 127
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getPinStatus]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 128
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getPinStatus([BI[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 129
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getPinStatus] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 130
    return-wide v0
.end method

.method public KUCA_getSIMInfo([B[B[I)J
    .registers 9
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 107
    const-wide/16 v0, -0x1

    #@2
    .line 109
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getSIMInfo]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 110
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getSIMInfo([B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 111
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getSIMInfo] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 112
    return-wide v0
.end method

.method public KUCA_getSimStatus([B[B)J
    .registers 8
    .parameter "handle"
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 171
    const-wide/16 v0, -0x1

    #@2
    .line 173
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_getSimStatus]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 174
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2}, Landroid/ktuca/KtUcaServiceJni;->KUCA_getSimStatus([B[B)J

    #@e
    move-result-wide v0

    #@f
    .line 175
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_getSimStatus] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 176
    return-wide v0
.end method

.method public KUCA_printCHInfo(B)J
    .registers 7
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 198
    const-wide/16 v0, -0x1

    #@2
    .line 200
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_printCHInfo]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 201
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1}, Landroid/ktuca/KtUcaServiceJni;->KUCA_printCHInfo(B)J

    #@e
    move-result-wide v0

    #@f
    .line 202
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_printCHInfo] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 203
    return-wide v0
.end method

.method public KUCA_usimAUTH([B[B[B[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "rand"
    .parameter "autn"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    const-wide/16 v0, -0x1

    #@2
    .line 118
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_usimAUTH]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 119
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_usimAUTH([B[B[B[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 120
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_usimAUTH] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 121
    return-wide v0
.end method

.method public KUCA_verifyPin([BILjava/lang/String;[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "pinId"
    .parameter "pinCode"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 134
    const-wide/16 v0, -0x1

    #@2
    .line 136
    .local v0, ret:J
    const-string v2, "KT_UCA_SERVICE"

    #@4
    const-string v3, "+[KUCA_verifyPin]"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 137
    iget-object v2, p0, Landroid/ktuca/KtUcaService;->KtUcaServiceJni:Landroid/ktuca/KtUcaServiceJni;

    #@b
    invoke-static {p1, p2, p3, p4, p5}, Landroid/ktuca/KtUcaServiceJni;->KUCA_verifyPin([BILjava/lang/String;[B[I)J

    #@e
    move-result-wide v0

    #@f
    .line 138
    const-string v2, "KT_UCA_SERVICE"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "-[KUCA_verifyPin] ret : "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 139
    return-wide v0
.end method

.method public getResource()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 253
    const-string v0, "2.1.0_R08_121129"

    #@2
    .line 255
    .local v0, KTUCA_VER:Ljava/lang/String;
    iget v1, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@4
    add-int/lit8 v1, v1, 0x1

    #@6
    iput v1, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@8
    .line 256
    const-string v1, "KT_UCA_SERVICE"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "+[KtUcaService Ver]"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 257
    const-string v1, "KT_UCA_SERVICE"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "Ref. Cnt. : "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    iget v3, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 258
    iget v1, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@3c
    return v1
.end method

.method public releaseResource()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    iget v0, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@6
    .line 264
    const-string v0, "KT_UCA_SERVICE"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Ref. Cnt. : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget v2, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 265
    iget v0, p0, Landroid/ktuca/KtUcaService;->referenceCount:I

    #@22
    return v0
.end method
