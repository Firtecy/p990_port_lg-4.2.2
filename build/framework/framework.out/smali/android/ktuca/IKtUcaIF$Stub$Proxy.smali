.class Landroid/ktuca/IKtUcaIF$Stub$Proxy;
.super Ljava/lang/Object;
.source "IKtUcaIF.java"

# interfaces
.implements Landroid/ktuca/IKtUcaIF;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/ktuca/IKtUcaIF$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 652
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 653
    iput-object p1, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 654
    return-void
.end method


# virtual methods
.method public KUCA_CHInit(B[B[I)J
    .registers 11
    .parameter "ucatag"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1202
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1203
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1206
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1207
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 1208
    if-nez p2, :cond_38

    #@12
    .line 1209
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1214
    :goto_16
    if-nez p3, :cond_45

    #@18
    .line 1215
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1220
    :goto_1c
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0x13

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 1221
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 1222
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v2

    #@2b
    .line 1223
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@2e
    .line 1224
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 1227
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 1228
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 1230
    return-wide v2

    #@38
    .line 1212
    .end local v2           #_result:J
    :cond_38
    :try_start_38
    array-length v4, p2

    #@39
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_16

    #@3d
    .line 1227
    :catchall_3d
    move-exception v4

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 1228
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v4

    #@45
    .line 1218
    :cond_45
    :try_start_45
    array-length v4, p3

    #@46
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_1c
.end method

.method public KUCA_Close([BB)J
    .registers 10
    .parameter "handle"
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1126
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1127
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1130
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1131
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1132
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByte(B)V

    #@13
    .line 1133
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v5, 0x10

    #@17
    const/4 v6, 0x0

    #@18
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 1134
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 1135
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result-wide v2

    #@22
    .line 1138
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1139
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1141
    return-wide v2

    #@29
    .line 1138
    .end local v2           #_result:J
    :catchall_29
    move-exception v4

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1139
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v4
.end method

.method public KUCA_CloseT([BB)J
    .registers 10
    .parameter "appId"
    .parameter "channel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1354
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1355
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1358
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1359
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1360
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByte(B)V

    #@13
    .line 1361
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v5, 0x19

    #@17
    const/4 v6, 0x0

    #@18
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 1362
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1e
    .line 1363
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_29

    #@21
    move-result-wide v2

    #@22
    .line 1366
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1367
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 1369
    return-wide v2

    #@29
    .line 1366
    .end local v2           #_result:J
    :catchall_29
    move-exception v4

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 1367
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v4
.end method

.method public KUCA_KUH_Establish(B)J
    .registers 9
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1252
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1253
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1256
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1257
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 1258
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v5, 0x15

    #@14
    const/4 v6, 0x0

    #@15
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1259
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1260
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-wide v2

    #@1f
    .line 1263
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1264
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1266
    return-wide v2

    #@26
    .line 1263
    .end local v2           #_result:J
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1264
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v4
.end method

.method public KUCA_KUH_Release(B)J
    .registers 9
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1270
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1271
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1274
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1275
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 1276
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v5, 0x16

    #@14
    const/4 v6, 0x0

    #@15
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1277
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1278
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-wide v2

    #@1f
    .line 1281
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1282
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1284
    return-wide v2

    #@26
    .line 1281
    .end local v2           #_result:J
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1282
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v4
.end method

.method public KUCA_KUH_Transmit(B[BI[B[I)J
    .registers 13
    .parameter "ucatag"
    .parameter "pbSendBuffer"
    .parameter "cbSendLength"
    .parameter "pbRecvBuffer"
    .parameter "pcbRecvLength"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1288
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1289
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1292
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1293
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 1294
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@13
    .line 1295
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1296
    if-nez p4, :cond_3e

    #@18
    .line 1297
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1302
    :goto_1c
    if-nez p5, :cond_4b

    #@1e
    .line 1303
    const/4 v4, -0x1

    #@1f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 1308
    :goto_22
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0x17

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 1309
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 1310
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 1311
    .local v2, _result:J
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 1312
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 1315
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1316
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 1318
    return-wide v2

    #@3e
    .line 1300
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p4

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_1c

    #@43
    .line 1315
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 1316
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 1306
    :cond_4b
    :try_start_4b
    array-length v4, p5

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_22
.end method

.method public KUCA_Open([B[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1060
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1061
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1064
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1065
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1066
    if-nez p2, :cond_38

    #@12
    .line 1067
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1072
    :goto_16
    if-nez p3, :cond_45

    #@18
    .line 1073
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1078
    :goto_1c
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xe

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 1079
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 1080
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v2

    #@2b
    .line 1081
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@2e
    .line 1082
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 1085
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 1086
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 1088
    return-wide v2

    #@38
    .line 1070
    .end local v2           #_result:J
    :cond_38
    :try_start_38
    array-length v4, p2

    #@39
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_16

    #@3d
    .line 1085
    :catchall_3d
    move-exception v4

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 1086
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v4

    #@45
    .line 1076
    :cond_45
    :try_start_45
    array-length v4, p3

    #@46
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_1c
.end method

.method public KUCA_OpenT([B[B[I)J
    .registers 11
    .parameter "appId"
    .parameter "channel"
    .parameter "channelLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1322
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1323
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1326
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1327
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1328
    if-nez p2, :cond_38

    #@12
    .line 1329
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1334
    :goto_16
    if-nez p3, :cond_45

    #@18
    .line 1335
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1340
    :goto_1c
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0x18

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 1341
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 1342
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v2

    #@2b
    .line 1343
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@2e
    .line 1344
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 1347
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 1348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 1350
    return-wide v2

    #@38
    .line 1332
    .end local v2           #_result:J
    :cond_38
    :try_start_38
    array-length v4, p2

    #@39
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_16

    #@3d
    .line 1347
    :catchall_3d
    move-exception v4

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 1348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v4

    #@45
    .line 1338
    :cond_45
    :try_start_45
    array-length v4, p3

    #@46
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_1c
.end method

.method public KUCA_Transmit([B[BI[B[I)J
    .registers 13
    .parameter "handle"
    .parameter "input"
    .parameter "inputLen"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1092
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1093
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1096
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1097
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1098
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@13
    .line 1099
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1100
    if-nez p4, :cond_3e

    #@18
    .line 1101
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1106
    :goto_1c
    if-nez p5, :cond_4b

    #@1e
    .line 1107
    const/4 v4, -0x1

    #@1f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 1112
    :goto_22
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0xf

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 1113
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 1114
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 1115
    .local v2, _result:J
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 1116
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 1119
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1120
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 1122
    return-wide v2

    #@3e
    .line 1104
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p4

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_1c

    #@43
    .line 1119
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 1120
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 1110
    :cond_4b
    :try_start_4b
    array-length v4, p5

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_22
.end method

.method public KUCA_UCAVersion([B[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1170
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1171
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1174
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1175
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1176
    if-nez p2, :cond_38

    #@12
    .line 1177
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1182
    :goto_16
    if-nez p3, :cond_45

    #@18
    .line 1183
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1188
    :goto_1c
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0x12

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 1189
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 1190
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v2

    #@2b
    .line 1191
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@2e
    .line 1192
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 1195
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 1196
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 1198
    return-wide v2

    #@38
    .line 1180
    .end local v2           #_result:J
    :cond_38
    :try_start_38
    array-length v4, p2

    #@39
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_16

    #@3d
    .line 1195
    :catchall_3d
    move-exception v4

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 1196
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v4

    #@45
    .line 1186
    :cond_45
    :try_start_45
    array-length v4, p3

    #@46
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_1c
.end method

.method public KUCA_getHandle([B[B[B[B[I)J
    .registers 13
    .parameter "callerId"
    .parameter "preKey"
    .parameter "appId"
    .parameter "handle"
    .parameter "handleLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 699
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 700
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 703
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 704
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 705
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@13
    .line 706
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@16
    .line 707
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@19
    .line 708
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1c
    .line 709
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/4 v5, 0x3

    #@1f
    const/4 v6, 0x0

    #@20
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@23
    .line 710
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@26
    .line 711
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@29
    move-result-wide v2

    #@2a
    .line 712
    .local v2, _result:J
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readByteArray([B)V

    #@2d
    .line 713
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_30
    .catchall {:try_start_8 .. :try_end_30} :catchall_37

    #@30
    .line 716
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 717
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 719
    return-wide v2

    #@37
    .line 716
    .end local v2           #_result:J
    :catchall_37
    move-exception v4

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 717
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v4
.end method

.method public KUCA_getICCID([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 791
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 792
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 795
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 796
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 797
    if-nez p2, :cond_3d

    #@12
    .line 798
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 803
    :goto_16
    if-nez p3, :cond_4a

    #@18
    .line 804
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 809
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 810
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 811
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v5, 0x6

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 812
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 813
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v2

    #@30
    .line 814
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@33
    .line 815
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_36
    .catchall {:try_start_8 .. :try_end_36} :catchall_42

    #@36
    .line 818
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 819
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 821
    return-wide v2

    #@3d
    .line 801
    .end local v2           #_result:J
    :cond_3d
    :try_start_3d
    array-length v4, p2

    #@3e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_42

    #@41
    goto :goto_16

    #@42
    .line 818
    :catchall_42
    move-exception v4

    #@43
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 819
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    throw v4

    #@4a
    .line 807
    :cond_4a
    :try_start_4a
    array-length v4, p3

    #@4b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_42

    #@4e
    goto :goto_1c
.end method

.method public KUCA_getIMSI([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 757
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 758
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 761
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 762
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 763
    if-nez p2, :cond_3d

    #@12
    .line 764
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 769
    :goto_16
    if-nez p3, :cond_4a

    #@18
    .line 770
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 775
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 776
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 777
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v5, 0x5

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 778
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 779
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v2

    #@30
    .line 780
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@33
    .line 781
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_36
    .catchall {:try_start_8 .. :try_end_36} :catchall_42

    #@36
    .line 784
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 785
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 787
    return-wide v2

    #@3d
    .line 767
    .end local v2           #_result:J
    :cond_3d
    :try_start_3d
    array-length v4, p2

    #@3e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_42

    #@41
    goto :goto_16

    #@42
    .line 784
    :catchall_42
    move-exception v4

    #@43
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 785
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    throw v4

    #@4a
    .line 773
    :cond_4a
    :try_start_4a
    array-length v4, p3

    #@4b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_42

    #@4e
    goto :goto_1c
.end method

.method public KUCA_getMDN([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 859
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 860
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 863
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 864
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 865
    if-nez p2, :cond_3e

    #@12
    .line 866
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 871
    :goto_16
    if-nez p3, :cond_4b

    #@18
    .line 872
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 877
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 878
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 879
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0x8

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 880
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 881
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 882
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 883
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 886
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 887
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 889
    return-wide v2

    #@3e
    .line 869
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p2

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_16

    #@43
    .line 886
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 887
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 875
    :cond_4b
    :try_start_4b
    array-length v4, p3

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_1c
.end method

.method public KUCA_getMODEL([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 893
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 894
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 897
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 898
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 899
    if-nez p2, :cond_3e

    #@12
    .line 900
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 905
    :goto_16
    if-nez p3, :cond_4b

    #@18
    .line 906
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 911
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 912
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 913
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0x9

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 914
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 915
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 916
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 917
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 920
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 921
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 923
    return-wide v2

    #@3e
    .line 903
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p2

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_16

    #@43
    .line 920
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 921
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 909
    :cond_4b
    :try_start_4b
    array-length v4, p3

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_1c
.end method

.method public KUCA_getMSISDN([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 723
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 724
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 727
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 728
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 729
    if-nez p2, :cond_3d

    #@12
    .line 730
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 735
    :goto_16
    if-nez p3, :cond_4a

    #@18
    .line 736
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 741
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 742
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 743
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v5, 0x4

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 744
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 745
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v2

    #@30
    .line 746
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@33
    .line 747
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_36
    .catchall {:try_start_8 .. :try_end_36} :catchall_42

    #@36
    .line 750
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 751
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 753
    return-wide v2

    #@3d
    .line 733
    .end local v2           #_result:J
    :cond_3d
    :try_start_3d
    array-length v4, p2

    #@3e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_42

    #@41
    goto :goto_16

    #@42
    .line 750
    :catchall_42
    move-exception v4

    #@43
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 751
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    throw v4

    #@4a
    .line 739
    :cond_4a
    :try_start_4a
    array-length v4, p3

    #@4b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_42

    #@4e
    goto :goto_1c
.end method

.method public KUCA_getPUID([B[B[II[B)J
    .registers 13
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .parameter "encryptType"
    .parameter "deviceIp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 825
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 826
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 829
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 830
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 831
    if-nez p2, :cond_3d

    #@12
    .line 832
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 837
    :goto_16
    if-nez p3, :cond_4a

    #@18
    .line 838
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 843
    :goto_1c
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 844
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    #@22
    .line 845
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/4 v5, 0x7

    #@25
    const/4 v6, 0x0

    #@26
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 846
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2c
    .line 847
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2f
    move-result-wide v2

    #@30
    .line 848
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@33
    .line 849
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_36
    .catchall {:try_start_8 .. :try_end_36} :catchall_42

    #@36
    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 853
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 855
    return-wide v2

    #@3d
    .line 835
    .end local v2           #_result:J
    :cond_3d
    :try_start_3d
    array-length v4, p2

    #@3e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_41
    .catchall {:try_start_3d .. :try_end_41} :catchall_42

    #@41
    goto :goto_16

    #@42
    .line 852
    :catchall_42
    move-exception v4

    #@43
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 853
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    throw v4

    #@4a
    .line 841
    :cond_4a
    :try_start_4a
    array-length v4, p3

    #@4b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_42

    #@4e
    goto :goto_1c
.end method

.method public KUCA_getPinStatus([BI[B[I)J
    .registers 12
    .parameter "handle"
    .parameter "pinId"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 993
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 994
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 997
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 998
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 999
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1000
    if-nez p3, :cond_3b

    #@15
    .line 1001
    const/4 v4, -0x1

    #@16
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1006
    :goto_19
    if-nez p4, :cond_48

    #@1b
    .line 1007
    const/4 v4, -0x1

    #@1c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 1012
    :goto_1f
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@21
    const/16 v5, 0xc

    #@23
    const/4 v6, 0x0

    #@24
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@27
    .line 1013
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2a
    .line 1014
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2d
    move-result-wide v2

    #@2e
    .line 1015
    .local v2, _result:J
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readByteArray([B)V

    #@31
    .line 1016
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_34
    .catchall {:try_start_8 .. :try_end_34} :catchall_40

    #@34
    .line 1019
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 1020
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1022
    return-wide v2

    #@3b
    .line 1004
    .end local v2           #_result:J
    :cond_3b
    :try_start_3b
    array-length v4, p3

    #@3c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3f
    .catchall {:try_start_3b .. :try_end_3f} :catchall_40

    #@3f
    goto :goto_19

    #@40
    .line 1019
    :catchall_40
    move-exception v4

    #@41
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@44
    .line 1020
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@47
    throw v4

    #@48
    .line 1010
    :cond_48
    :try_start_48
    array-length v4, p4

    #@49
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4c
    .catchall {:try_start_48 .. :try_end_4c} :catchall_40

    #@4c
    goto :goto_1f
.end method

.method public KUCA_getSIMInfo([B[B[I)J
    .registers 11
    .parameter "handle"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 927
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 928
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 931
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 932
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 933
    if-nez p2, :cond_38

    #@12
    .line 934
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 939
    :goto_16
    if-nez p3, :cond_45

    #@18
    .line 940
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 945
    :goto_1c
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1e
    const/16 v5, 0xa

    #@20
    const/4 v6, 0x0

    #@21
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@24
    .line 946
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@27
    .line 947
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@2a
    move-result-wide v2

    #@2b
    .line 948
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V

    #@2e
    .line 949
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_3d

    #@31
    .line 952
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@34
    .line 953
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 955
    return-wide v2

    #@38
    .line 937
    .end local v2           #_result:J
    :cond_38
    :try_start_38
    array-length v4, p2

    #@39
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3c
    .catchall {:try_start_38 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_16

    #@3d
    .line 952
    :catchall_3d
    move-exception v4

    #@3e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@41
    .line 953
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@44
    throw v4

    #@45
    .line 943
    :cond_45
    :try_start_45
    array-length v4, p3

    #@46
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_49
    .catchall {:try_start_45 .. :try_end_49} :catchall_3d

    #@49
    goto :goto_1c
.end method

.method public KUCA_getSimStatus([B[B)J
    .registers 10
    .parameter "handle"
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1145
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1146
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1149
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1150
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1151
    if-nez p2, :cond_2f

    #@12
    .line 1152
    const/4 v4, -0x1

    #@13
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 1157
    :goto_16
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v5, 0x11

    #@1a
    const/4 v6, 0x0

    #@1b
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 1158
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@21
    .line 1159
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@24
    move-result-wide v2

    #@25
    .line 1160
    .local v2, _result:J
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->readByteArray([B)V
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_34

    #@28
    .line 1163
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 1164
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 1166
    return-wide v2

    #@2f
    .line 1155
    .end local v2           #_result:J
    :cond_2f
    :try_start_2f
    array-length v4, p2

    #@30
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_33
    .catchall {:try_start_2f .. :try_end_33} :catchall_34

    #@33
    goto :goto_16

    #@34
    .line 1163
    :catchall_34
    move-exception v4

    #@35
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 1164
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3b
    throw v4
.end method

.method public KUCA_printCHInfo(B)J
    .registers 9
    .parameter "ucatag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1234
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1235
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1238
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1239
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByte(B)V

    #@10
    .line 1240
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v5, 0x14

    #@14
    const/4 v6, 0x0

    #@15
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 1241
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1b
    .line 1242
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-wide v2

    #@1f
    .line 1245
    .local v2, _result:J
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 1246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 1248
    return-wide v2

    #@26
    .line 1245
    .end local v2           #_result:J
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 1246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v4
.end method

.method public KUCA_usimAUTH([B[B[B[B[I)J
    .registers 13
    .parameter "handle"
    .parameter "rand"
    .parameter "autn"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 959
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 960
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 963
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 964
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 965
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@13
    .line 966
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@16
    .line 967
    if-nez p4, :cond_3e

    #@18
    .line 968
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 973
    :goto_1c
    if-nez p5, :cond_4b

    #@1e
    .line 974
    const/4 v4, -0x1

    #@1f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 979
    :goto_22
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0xb

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 980
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 981
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 982
    .local v2, _result:J
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 983
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 986
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 987
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 989
    return-wide v2

    #@3e
    .line 971
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p4

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_1c

    #@43
    .line 986
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 987
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 977
    :cond_4b
    :try_start_4b
    array-length v4, p5

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_22
.end method

.method public KUCA_verifyPin([BILjava/lang/String;[B[I)J
    .registers 13
    .parameter "handle"
    .parameter "pinId"
    .parameter "pinCode"
    .parameter "output"
    .parameter "outputLen"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1026
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 1027
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 1030
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v4, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 1031
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 1032
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1033
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 1034
    if-nez p4, :cond_3e

    #@18
    .line 1035
    const/4 v4, -0x1

    #@19
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1040
    :goto_1c
    if-nez p5, :cond_4b

    #@1e
    .line 1041
    const/4 v4, -0x1

    #@1f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 1046
    :goto_22
    iget-object v4, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@24
    const/16 v5, 0xd

    #@26
    const/4 v6, 0x0

    #@27
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 1047
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2d
    .line 1048
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    #@30
    move-result-wide v2

    #@31
    .line 1049
    .local v2, _result:J
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->readByteArray([B)V

    #@34
    .line 1050
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->readIntArray([I)V
    :try_end_37
    .catchall {:try_start_8 .. :try_end_37} :catchall_43

    #@37
    .line 1053
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 1054
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3d
    .line 1056
    return-wide v2

    #@3e
    .line 1038
    .end local v2           #_result:J
    :cond_3e
    :try_start_3e
    array-length v4, p4

    #@3f
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_43

    #@42
    goto :goto_1c

    #@43
    .line 1053
    :catchall_43
    move-exception v4

    #@44
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@47
    .line 1054
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@4a
    throw v4

    #@4b
    .line 1044
    :cond_4b
    :try_start_4b
    array-length v4, p5

    #@4c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4f
    .catchall {:try_start_4b .. :try_end_4f} :catchall_43

    #@4f
    goto :goto_22
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 657
    iget-object v0, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 661
    const-string v0, "android.ktuca.IKtUcaIF"

    #@2
    return-object v0
.end method

.method public getResource()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 665
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 666
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 669
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 670
    iget-object v3, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x1

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 671
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 672
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 675
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 676
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 678
    return v2

    #@22
    .line 675
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 676
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public releaseResource()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 682
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 683
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 686
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 687
    iget-object v3, p0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 688
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 689
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 692
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 693
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 695
    return v2

    #@22
    .line 692
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 693
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method
