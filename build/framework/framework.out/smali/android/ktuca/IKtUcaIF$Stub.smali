.class public abstract Landroid/ktuca/IKtUcaIF$Stub;
.super Landroid/os/Binder;
.source "IKtUcaIF.java"

# interfaces
.implements Landroid/ktuca/IKtUcaIF;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/ktuca/IKtUcaIF;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/ktuca/IKtUcaIF$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.ktuca.IKtUcaIF"

.field static final TRANSACTION_KUCA_CHInit:I = 0x13

.field static final TRANSACTION_KUCA_Close:I = 0x10

.field static final TRANSACTION_KUCA_CloseT:I = 0x19

.field static final TRANSACTION_KUCA_KUH_Establish:I = 0x15

.field static final TRANSACTION_KUCA_KUH_Release:I = 0x16

.field static final TRANSACTION_KUCA_KUH_Transmit:I = 0x17

.field static final TRANSACTION_KUCA_Open:I = 0xe

.field static final TRANSACTION_KUCA_OpenT:I = 0x18

.field static final TRANSACTION_KUCA_Transmit:I = 0xf

.field static final TRANSACTION_KUCA_UCAVersion:I = 0x12

.field static final TRANSACTION_KUCA_getHandle:I = 0x3

.field static final TRANSACTION_KUCA_getICCID:I = 0x6

.field static final TRANSACTION_KUCA_getIMSI:I = 0x5

.field static final TRANSACTION_KUCA_getMDN:I = 0x8

.field static final TRANSACTION_KUCA_getMODEL:I = 0x9

.field static final TRANSACTION_KUCA_getMSISDN:I = 0x4

.field static final TRANSACTION_KUCA_getPUID:I = 0x7

.field static final TRANSACTION_KUCA_getPinStatus:I = 0xc

.field static final TRANSACTION_KUCA_getSIMInfo:I = 0xa

.field static final TRANSACTION_KUCA_getSimStatus:I = 0x11

.field static final TRANSACTION_KUCA_printCHInfo:I = 0x14

.field static final TRANSACTION_KUCA_usimAUTH:I = 0xb

.field static final TRANSACTION_KUCA_verifyPin:I = 0xd

.field static final TRANSACTION_getResource:I = 0x1

.field static final TRANSACTION_releaseResource:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.ktuca.IKtUcaIF"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/ktuca/IKtUcaIF$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/ktuca/IKtUcaIF;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.ktuca.IKtUcaIF"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/ktuca/IKtUcaIF;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/ktuca/IKtUcaIF;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/ktuca/IKtUcaIF$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/ktuca/IKtUcaIF$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 17
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    sparse-switch p1, :sswitch_data_454

    #@3
    .line 646
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v0

    #@7
    :goto_7
    return v0

    #@8
    .line 43
    :sswitch_8
    const-string v0, "android.ktuca.IKtUcaIF"

    #@a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d
    .line 44
    const/4 v0, 0x1

    #@e
    goto :goto_7

    #@f
    .line 48
    :sswitch_f
    const-string v0, "android.ktuca.IKtUcaIF"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p0}, Landroid/ktuca/IKtUcaIF$Stub;->getResource()I

    #@17
    move-result v10

    #@18
    .line 50
    .local v10, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 51
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 52
    const/4 v0, 0x1

    #@1f
    goto :goto_7

    #@20
    .line 56
    .end local v10           #_result:I
    :sswitch_20
    const-string v0, "android.ktuca.IKtUcaIF"

    #@22
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 57
    invoke-virtual {p0}, Landroid/ktuca/IKtUcaIF$Stub;->releaseResource()I

    #@28
    move-result v10

    #@29
    .line 58
    .restart local v10       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c
    .line 59
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 60
    const/4 v0, 0x1

    #@30
    goto :goto_7

    #@31
    .line 64
    .end local v10           #_result:I
    :sswitch_31
    const-string v0, "android.ktuca.IKtUcaIF"

    #@33
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@39
    move-result-object v1

    #@3a
    .line 68
    .local v1, _arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@3d
    move-result-object v2

    #@3e
    .line 70
    .local v2, _arg1:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@41
    move-result-object v3

    #@42
    .line 72
    .local v3, _arg2:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@45
    move-result-object v4

    #@46
    .line 74
    .local v4, _arg3:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@49
    move-result-object v5

    #@4a
    .local v5, _arg4:[I
    move-object v0, p0

    #@4b
    .line 75
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getHandle([B[B[B[B[I)J

    #@4e
    move-result-wide v10

    #@4f
    .line 76
    .local v10, _result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@52
    .line 77
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@55
    .line 78
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@58
    .line 79
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@5b
    .line 80
    const/4 v0, 0x1

    #@5c
    goto :goto_7

    #@5d
    .line 84
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[B
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v10           #_result:J
    :sswitch_5d
    const-string v0, "android.ktuca.IKtUcaIF"

    #@5f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@62
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@65
    move-result-object v1

    #@66
    .line 88
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v6

    #@6a
    .line 89
    .local v6, _arg1_length:I
    if-gez v6, :cond_90

    #@6c
    .line 90
    const/4 v2, 0x0

    #@6d
    .line 96
    .restart local v2       #_arg1:[B
    :goto_6d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@70
    move-result v7

    #@71
    .line 97
    .local v7, _arg2_length:I
    if-gez v7, :cond_93

    #@73
    .line 98
    const/4 v3, 0x0

    #@74
    .line 104
    .local v3, _arg2:[I
    :goto_74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@77
    move-result v4

    #@78
    .line 106
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@7b
    move-result-object v5

    #@7c
    .local v5, _arg4:[B
    move-object v0, p0

    #@7d
    .line 107
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getMSISDN([B[B[II[B)J

    #@80
    move-result-wide v10

    #@81
    .line 108
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@84
    .line 109
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@87
    .line 110
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@8a
    .line 111
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@8d
    .line 112
    const/4 v0, 0x1

    #@8e
    goto/16 :goto_7

    #@90
    .line 93
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_90
    new-array v2, v6, [B

    #@92
    .restart local v2       #_arg1:[B
    goto :goto_6d

    #@93
    .line 101
    .restart local v7       #_arg2_length:I
    :cond_93
    new-array v3, v7, [I

    #@95
    .restart local v3       #_arg2:[I
    goto :goto_74

    #@96
    .line 116
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_96
    const-string v0, "android.ktuca.IKtUcaIF"

    #@98
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@9e
    move-result-object v1

    #@9f
    .line 120
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a2
    move-result v6

    #@a3
    .line 121
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_c9

    #@a5
    .line 122
    const/4 v2, 0x0

    #@a6
    .line 128
    .restart local v2       #_arg1:[B
    :goto_a6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a9
    move-result v7

    #@aa
    .line 129
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_cc

    #@ac
    .line 130
    const/4 v3, 0x0

    #@ad
    .line 136
    .restart local v3       #_arg2:[I
    :goto_ad
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b0
    move-result v4

    #@b1
    .line 138
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@b4
    move-result-object v5

    #@b5
    .restart local v5       #_arg4:[B
    move-object v0, p0

    #@b6
    .line 139
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getIMSI([B[B[II[B)J

    #@b9
    move-result-wide v10

    #@ba
    .line 140
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@bd
    .line 141
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@c0
    .line 142
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@c3
    .line 143
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@c6
    .line 144
    const/4 v0, 0x1

    #@c7
    goto/16 :goto_7

    #@c9
    .line 125
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_c9
    new-array v2, v6, [B

    #@cb
    .restart local v2       #_arg1:[B
    goto :goto_a6

    #@cc
    .line 133
    .restart local v7       #_arg2_length:I
    :cond_cc
    new-array v3, v7, [I

    #@ce
    .restart local v3       #_arg2:[I
    goto :goto_ad

    #@cf
    .line 148
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_cf
    const-string v0, "android.ktuca.IKtUcaIF"

    #@d1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d4
    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@d7
    move-result-object v1

    #@d8
    .line 152
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@db
    move-result v6

    #@dc
    .line 153
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_102

    #@de
    .line 154
    const/4 v2, 0x0

    #@df
    .line 160
    .restart local v2       #_arg1:[B
    :goto_df
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e2
    move-result v7

    #@e3
    .line 161
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_105

    #@e5
    .line 162
    const/4 v3, 0x0

    #@e6
    .line 168
    .restart local v3       #_arg2:[I
    :goto_e6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e9
    move-result v4

    #@ea
    .line 170
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@ed
    move-result-object v5

    #@ee
    .restart local v5       #_arg4:[B
    move-object v0, p0

    #@ef
    .line 171
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getICCID([B[B[II[B)J

    #@f2
    move-result-wide v10

    #@f3
    .line 172
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f6
    .line 173
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@f9
    .line 174
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@fc
    .line 175
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@ff
    .line 176
    const/4 v0, 0x1

    #@100
    goto/16 :goto_7

    #@102
    .line 157
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_102
    new-array v2, v6, [B

    #@104
    .restart local v2       #_arg1:[B
    goto :goto_df

    #@105
    .line 165
    .restart local v7       #_arg2_length:I
    :cond_105
    new-array v3, v7, [I

    #@107
    .restart local v3       #_arg2:[I
    goto :goto_e6

    #@108
    .line 180
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_108
    const-string v0, "android.ktuca.IKtUcaIF"

    #@10a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 182
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@110
    move-result-object v1

    #@111
    .line 184
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@114
    move-result v6

    #@115
    .line 185
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_13b

    #@117
    .line 186
    const/4 v2, 0x0

    #@118
    .line 192
    .restart local v2       #_arg1:[B
    :goto_118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11b
    move-result v7

    #@11c
    .line 193
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_13e

    #@11e
    .line 194
    const/4 v3, 0x0

    #@11f
    .line 200
    .restart local v3       #_arg2:[I
    :goto_11f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@122
    move-result v4

    #@123
    .line 202
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@126
    move-result-object v5

    #@127
    .restart local v5       #_arg4:[B
    move-object v0, p0

    #@128
    .line 203
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getPUID([B[B[II[B)J

    #@12b
    move-result-wide v10

    #@12c
    .line 204
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@12f
    .line 205
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@132
    .line 206
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@135
    .line 207
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@138
    .line 208
    const/4 v0, 0x1

    #@139
    goto/16 :goto_7

    #@13b
    .line 189
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_13b
    new-array v2, v6, [B

    #@13d
    .restart local v2       #_arg1:[B
    goto :goto_118

    #@13e
    .line 197
    .restart local v7       #_arg2_length:I
    :cond_13e
    new-array v3, v7, [I

    #@140
    .restart local v3       #_arg2:[I
    goto :goto_11f

    #@141
    .line 212
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_141
    const-string v0, "android.ktuca.IKtUcaIF"

    #@143
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@146
    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@149
    move-result-object v1

    #@14a
    .line 216
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@14d
    move-result v6

    #@14e
    .line 217
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_174

    #@150
    .line 218
    const/4 v2, 0x0

    #@151
    .line 224
    .restart local v2       #_arg1:[B
    :goto_151
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@154
    move-result v7

    #@155
    .line 225
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_177

    #@157
    .line 226
    const/4 v3, 0x0

    #@158
    .line 232
    .restart local v3       #_arg2:[I
    :goto_158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15b
    move-result v4

    #@15c
    .line 234
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@15f
    move-result-object v5

    #@160
    .restart local v5       #_arg4:[B
    move-object v0, p0

    #@161
    .line 235
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getMDN([B[B[II[B)J

    #@164
    move-result-wide v10

    #@165
    .line 236
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@168
    .line 237
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@16b
    .line 238
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@16e
    .line 239
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@171
    .line 240
    const/4 v0, 0x1

    #@172
    goto/16 :goto_7

    #@174
    .line 221
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_174
    new-array v2, v6, [B

    #@176
    .restart local v2       #_arg1:[B
    goto :goto_151

    #@177
    .line 229
    .restart local v7       #_arg2_length:I
    :cond_177
    new-array v3, v7, [I

    #@179
    .restart local v3       #_arg2:[I
    goto :goto_158

    #@17a
    .line 244
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_17a
    const-string v0, "android.ktuca.IKtUcaIF"

    #@17c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@17f
    .line 246
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@182
    move-result-object v1

    #@183
    .line 248
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@186
    move-result v6

    #@187
    .line 249
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_1ad

    #@189
    .line 250
    const/4 v2, 0x0

    #@18a
    .line 256
    .restart local v2       #_arg1:[B
    :goto_18a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18d
    move-result v7

    #@18e
    .line 257
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_1b0

    #@190
    .line 258
    const/4 v3, 0x0

    #@191
    .line 264
    .restart local v3       #_arg2:[I
    :goto_191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@194
    move-result v4

    #@195
    .line 266
    .restart local v4       #_arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@198
    move-result-object v5

    #@199
    .restart local v5       #_arg4:[B
    move-object v0, p0

    #@19a
    .line 267
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getMODEL([B[B[II[B)J

    #@19d
    move-result-wide v10

    #@19e
    .line 268
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a1
    .line 269
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@1a4
    .line 270
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@1a7
    .line 271
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1aa
    .line 272
    const/4 v0, 0x1

    #@1ab
    goto/16 :goto_7

    #@1ad
    .line 253
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:[B
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_1ad
    new-array v2, v6, [B

    #@1af
    .restart local v2       #_arg1:[B
    goto :goto_18a

    #@1b0
    .line 261
    .restart local v7       #_arg2_length:I
    :cond_1b0
    new-array v3, v7, [I

    #@1b2
    .restart local v3       #_arg2:[I
    goto :goto_191

    #@1b3
    .line 276
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_1b3
    const-string v0, "android.ktuca.IKtUcaIF"

    #@1b5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1b8
    .line 278
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1bb
    move-result-object v1

    #@1bc
    .line 280
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1bf
    move-result v6

    #@1c0
    .line 281
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_1dd

    #@1c2
    .line 282
    const/4 v2, 0x0

    #@1c3
    .line 288
    .restart local v2       #_arg1:[B
    :goto_1c3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c6
    move-result v7

    #@1c7
    .line 289
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_1e0

    #@1c9
    .line 290
    const/4 v3, 0x0

    #@1ca
    .line 295
    .restart local v3       #_arg2:[I
    :goto_1ca
    invoke-virtual {p0, v1, v2, v3}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getSIMInfo([B[B[I)J

    #@1cd
    move-result-wide v10

    #@1ce
    .line 296
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d1
    .line 297
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@1d4
    .line 298
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@1d7
    .line 299
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@1da
    .line 300
    const/4 v0, 0x1

    #@1db
    goto/16 :goto_7

    #@1dd
    .line 285
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_1dd
    new-array v2, v6, [B

    #@1df
    .restart local v2       #_arg1:[B
    goto :goto_1c3

    #@1e0
    .line 293
    .restart local v7       #_arg2_length:I
    :cond_1e0
    new-array v3, v7, [I

    #@1e2
    .restart local v3       #_arg2:[I
    goto :goto_1ca

    #@1e3
    .line 304
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_1e3
    const-string v0, "android.ktuca.IKtUcaIF"

    #@1e5
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e8
    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1eb
    move-result-object v1

    #@1ec
    .line 308
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1ef
    move-result-object v2

    #@1f0
    .line 310
    .restart local v2       #_arg1:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@1f3
    move-result-object v3

    #@1f4
    .line 312
    .local v3, _arg2:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f7
    move-result v8

    #@1f8
    .line 313
    .local v8, _arg3_length:I
    if-gez v8, :cond_216

    #@1fa
    .line 314
    const/4 v4, 0x0

    #@1fb
    .line 320
    .local v4, _arg3:[B
    :goto_1fb
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1fe
    move-result v9

    #@1ff
    .line 321
    .local v9, _arg4_length:I
    if-gez v9, :cond_219

    #@201
    .line 322
    const/4 v5, 0x0

    #@202
    .local v5, _arg4:[I
    :goto_202
    move-object v0, p0

    #@203
    .line 327
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_usimAUTH([B[B[B[B[I)J

    #@206
    move-result-wide v10

    #@207
    .line 328
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20a
    .line 329
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@20d
    .line 330
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@210
    .line 331
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@213
    .line 332
    const/4 v0, 0x1

    #@214
    goto/16 :goto_7

    #@216
    .line 317
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v9           #_arg4_length:I
    .end local v10           #_result:J
    :cond_216
    new-array v4, v8, [B

    #@218
    .restart local v4       #_arg3:[B
    goto :goto_1fb

    #@219
    .line 325
    .restart local v9       #_arg4_length:I
    :cond_219
    new-array v5, v9, [I

    #@21b
    .restart local v5       #_arg4:[I
    goto :goto_202

    #@21c
    .line 336
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[B
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v8           #_arg3_length:I
    .end local v9           #_arg4_length:I
    :sswitch_21c
    const-string v0, "android.ktuca.IKtUcaIF"

    #@21e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@221
    .line 338
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@224
    move-result-object v1

    #@225
    .line 340
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@228
    move-result v2

    #@229
    .line 342
    .local v2, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@22c
    move-result v7

    #@22d
    .line 343
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_24a

    #@22f
    .line 344
    const/4 v3, 0x0

    #@230
    .line 350
    .restart local v3       #_arg2:[B
    :goto_230
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@233
    move-result v8

    #@234
    .line 351
    .restart local v8       #_arg3_length:I
    if-gez v8, :cond_24d

    #@236
    .line 352
    const/4 v4, 0x0

    #@237
    .line 357
    .local v4, _arg3:[I
    :goto_237
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getPinStatus([BI[B[I)J

    #@23a
    move-result-wide v10

    #@23b
    .line 358
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23e
    .line 359
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@241
    .line 360
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    #@244
    .line 361
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeIntArray([I)V

    #@247
    .line 362
    const/4 v0, 0x1

    #@248
    goto/16 :goto_7

    #@24a
    .line 347
    .end local v3           #_arg2:[B
    .end local v4           #_arg3:[I
    .end local v8           #_arg3_length:I
    .end local v10           #_result:J
    :cond_24a
    new-array v3, v7, [B

    #@24c
    .restart local v3       #_arg2:[B
    goto :goto_230

    #@24d
    .line 355
    .restart local v8       #_arg3_length:I
    :cond_24d
    new-array v4, v8, [I

    #@24f
    .restart local v4       #_arg3:[I
    goto :goto_237

    #@250
    .line 366
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:I
    .end local v3           #_arg2:[B
    .end local v4           #_arg3:[I
    .end local v7           #_arg2_length:I
    .end local v8           #_arg3_length:I
    :sswitch_250
    const-string v0, "android.ktuca.IKtUcaIF"

    #@252
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@255
    .line 368
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@258
    move-result-object v1

    #@259
    .line 370
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@25c
    move-result v2

    #@25d
    .line 372
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@260
    move-result-object v3

    #@261
    .line 374
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@264
    move-result v8

    #@265
    .line 375
    .restart local v8       #_arg3_length:I
    if-gez v8, :cond_283

    #@267
    .line 376
    const/4 v4, 0x0

    #@268
    .line 382
    .local v4, _arg3:[B
    :goto_268
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26b
    move-result v9

    #@26c
    .line 383
    .restart local v9       #_arg4_length:I
    if-gez v9, :cond_286

    #@26e
    .line 384
    const/4 v5, 0x0

    #@26f
    .restart local v5       #_arg4:[I
    :goto_26f
    move-object v0, p0

    #@270
    .line 389
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_verifyPin([BILjava/lang/String;[B[I)J

    #@273
    move-result-wide v10

    #@274
    .line 390
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@277
    .line 391
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@27a
    .line 392
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@27d
    .line 393
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@280
    .line 394
    const/4 v0, 0x1

    #@281
    goto/16 :goto_7

    #@283
    .line 379
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v9           #_arg4_length:I
    .end local v10           #_result:J
    :cond_283
    new-array v4, v8, [B

    #@285
    .restart local v4       #_arg3:[B
    goto :goto_268

    #@286
    .line 387
    .restart local v9       #_arg4_length:I
    :cond_286
    new-array v5, v9, [I

    #@288
    .restart local v5       #_arg4:[I
    goto :goto_26f

    #@289
    .line 398
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:I
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v8           #_arg3_length:I
    .end local v9           #_arg4_length:I
    :sswitch_289
    const-string v0, "android.ktuca.IKtUcaIF"

    #@28b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28e
    .line 400
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@291
    move-result-object v1

    #@292
    .line 402
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@295
    move-result v6

    #@296
    .line 403
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_2b3

    #@298
    .line 404
    const/4 v2, 0x0

    #@299
    .line 410
    .local v2, _arg1:[B
    :goto_299
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@29c
    move-result v7

    #@29d
    .line 411
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_2b6

    #@29f
    .line 412
    const/4 v3, 0x0

    #@2a0
    .line 417
    .local v3, _arg2:[I
    :goto_2a0
    invoke-virtual {p0, v1, v2, v3}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_Open([B[B[I)J

    #@2a3
    move-result-wide v10

    #@2a4
    .line 418
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a7
    .line 419
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@2aa
    .line 420
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2ad
    .line 421
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@2b0
    .line 422
    const/4 v0, 0x1

    #@2b1
    goto/16 :goto_7

    #@2b3
    .line 407
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_2b3
    new-array v2, v6, [B

    #@2b5
    .restart local v2       #_arg1:[B
    goto :goto_299

    #@2b6
    .line 415
    .restart local v7       #_arg2_length:I
    :cond_2b6
    new-array v3, v7, [I

    #@2b8
    .restart local v3       #_arg2:[I
    goto :goto_2a0

    #@2b9
    .line 426
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_2b9
    const-string v0, "android.ktuca.IKtUcaIF"

    #@2bb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2be
    .line 428
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@2c1
    move-result-object v1

    #@2c2
    .line 430
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@2c5
    move-result-object v2

    #@2c6
    .line 432
    .restart local v2       #_arg1:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c9
    move-result v3

    #@2ca
    .line 434
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cd
    move-result v8

    #@2ce
    .line 435
    .restart local v8       #_arg3_length:I
    if-gez v8, :cond_2ec

    #@2d0
    .line 436
    const/4 v4, 0x0

    #@2d1
    .line 442
    .restart local v4       #_arg3:[B
    :goto_2d1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d4
    move-result v9

    #@2d5
    .line 443
    .restart local v9       #_arg4_length:I
    if-gez v9, :cond_2ef

    #@2d7
    .line 444
    const/4 v5, 0x0

    #@2d8
    .restart local v5       #_arg4:[I
    :goto_2d8
    move-object v0, p0

    #@2d9
    .line 449
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_Transmit([B[BI[B[I)J

    #@2dc
    move-result-wide v10

    #@2dd
    .line 450
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e0
    .line 451
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@2e3
    .line 452
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@2e6
    .line 453
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@2e9
    .line 454
    const/4 v0, 0x1

    #@2ea
    goto/16 :goto_7

    #@2ec
    .line 439
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v9           #_arg4_length:I
    .end local v10           #_result:J
    :cond_2ec
    new-array v4, v8, [B

    #@2ee
    .restart local v4       #_arg3:[B
    goto :goto_2d1

    #@2ef
    .line 447
    .restart local v9       #_arg4_length:I
    :cond_2ef
    new-array v5, v9, [I

    #@2f1
    .restart local v5       #_arg4:[I
    goto :goto_2d8

    #@2f2
    .line 458
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:I
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v8           #_arg3_length:I
    .end local v9           #_arg4_length:I
    :sswitch_2f2
    const-string v0, "android.ktuca.IKtUcaIF"

    #@2f4
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f7
    .line 460
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@2fa
    move-result-object v1

    #@2fb
    .line 462
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@2fe
    move-result v2

    #@2ff
    .line 463
    .local v2, _arg1:B
    invoke-virtual {p0, v1, v2}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_Close([BB)J

    #@302
    move-result-wide v10

    #@303
    .line 464
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@306
    .line 465
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@309
    .line 466
    const/4 v0, 0x1

    #@30a
    goto/16 :goto_7

    #@30c
    .line 470
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:B
    .end local v10           #_result:J
    :sswitch_30c
    const-string v0, "android.ktuca.IKtUcaIF"

    #@30e
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@311
    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@314
    move-result-object v1

    #@315
    .line 474
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@318
    move-result v6

    #@319
    .line 475
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_32c

    #@31b
    .line 476
    const/4 v2, 0x0

    #@31c
    .line 481
    .local v2, _arg1:[B
    :goto_31c
    invoke-virtual {p0, v1, v2}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_getSimStatus([B[B)J

    #@31f
    move-result-wide v10

    #@320
    .line 482
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@323
    .line 483
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@326
    .line 484
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@329
    .line 485
    const/4 v0, 0x1

    #@32a
    goto/16 :goto_7

    #@32c
    .line 479
    .end local v2           #_arg1:[B
    .end local v10           #_result:J
    :cond_32c
    new-array v2, v6, [B

    #@32e
    .restart local v2       #_arg1:[B
    goto :goto_31c

    #@32f
    .line 489
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v6           #_arg1_length:I
    :sswitch_32f
    const-string v0, "android.ktuca.IKtUcaIF"

    #@331
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@334
    .line 491
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@337
    move-result-object v1

    #@338
    .line 493
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33b
    move-result v6

    #@33c
    .line 494
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_359

    #@33e
    .line 495
    const/4 v2, 0x0

    #@33f
    .line 501
    .restart local v2       #_arg1:[B
    :goto_33f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@342
    move-result v7

    #@343
    .line 502
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_35c

    #@345
    .line 503
    const/4 v3, 0x0

    #@346
    .line 508
    .local v3, _arg2:[I
    :goto_346
    invoke-virtual {p0, v1, v2, v3}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_UCAVersion([B[B[I)J

    #@349
    move-result-wide v10

    #@34a
    .line 509
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34d
    .line 510
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@350
    .line 511
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@353
    .line 512
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@356
    .line 513
    const/4 v0, 0x1

    #@357
    goto/16 :goto_7

    #@359
    .line 498
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_359
    new-array v2, v6, [B

    #@35b
    .restart local v2       #_arg1:[B
    goto :goto_33f

    #@35c
    .line 506
    .restart local v7       #_arg2_length:I
    :cond_35c
    new-array v3, v7, [I

    #@35e
    .restart local v3       #_arg2:[I
    goto :goto_346

    #@35f
    .line 517
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_35f
    const-string v0, "android.ktuca.IKtUcaIF"

    #@361
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@364
    .line 519
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@367
    move-result v1

    #@368
    .line 521
    .local v1, _arg0:B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@36b
    move-result v6

    #@36c
    .line 522
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_389

    #@36e
    .line 523
    const/4 v2, 0x0

    #@36f
    .line 529
    .restart local v2       #_arg1:[B
    :goto_36f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@372
    move-result v7

    #@373
    .line 530
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_38c

    #@375
    .line 531
    const/4 v3, 0x0

    #@376
    .line 536
    .restart local v3       #_arg2:[I
    :goto_376
    invoke-virtual {p0, v1, v2, v3}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_CHInit(B[B[I)J

    #@379
    move-result-wide v10

    #@37a
    .line 537
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37d
    .line 538
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@380
    .line 539
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@383
    .line 540
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@386
    .line 541
    const/4 v0, 0x1

    #@387
    goto/16 :goto_7

    #@389
    .line 526
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_389
    new-array v2, v6, [B

    #@38b
    .restart local v2       #_arg1:[B
    goto :goto_36f

    #@38c
    .line 534
    .restart local v7       #_arg2_length:I
    :cond_38c
    new-array v3, v7, [I

    #@38e
    .restart local v3       #_arg2:[I
    goto :goto_376

    #@38f
    .line 545
    .end local v1           #_arg0:B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_38f
    const-string v0, "android.ktuca.IKtUcaIF"

    #@391
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@394
    .line 547
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@397
    move-result v1

    #@398
    .line 548
    .restart local v1       #_arg0:B
    invoke-virtual {p0, v1}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_printCHInfo(B)J

    #@39b
    move-result-wide v10

    #@39c
    .line 549
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@39f
    .line 550
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@3a2
    .line 551
    const/4 v0, 0x1

    #@3a3
    goto/16 :goto_7

    #@3a5
    .line 555
    .end local v1           #_arg0:B
    .end local v10           #_result:J
    :sswitch_3a5
    const-string v0, "android.ktuca.IKtUcaIF"

    #@3a7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3aa
    .line 557
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@3ad
    move-result v1

    #@3ae
    .line 558
    .restart local v1       #_arg0:B
    invoke-virtual {p0, v1}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_KUH_Establish(B)J

    #@3b1
    move-result-wide v10

    #@3b2
    .line 559
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b5
    .line 560
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@3b8
    .line 561
    const/4 v0, 0x1

    #@3b9
    goto/16 :goto_7

    #@3bb
    .line 565
    .end local v1           #_arg0:B
    .end local v10           #_result:J
    :sswitch_3bb
    const-string v0, "android.ktuca.IKtUcaIF"

    #@3bd
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c0
    .line 567
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@3c3
    move-result v1

    #@3c4
    .line 568
    .restart local v1       #_arg0:B
    invoke-virtual {p0, v1}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_KUH_Release(B)J

    #@3c7
    move-result-wide v10

    #@3c8
    .line 569
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3cb
    .line 570
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@3ce
    .line 571
    const/4 v0, 0x1

    #@3cf
    goto/16 :goto_7

    #@3d1
    .line 575
    .end local v1           #_arg0:B
    .end local v10           #_result:J
    :sswitch_3d1
    const-string v0, "android.ktuca.IKtUcaIF"

    #@3d3
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d6
    .line 577
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@3d9
    move-result v1

    #@3da
    .line 579
    .restart local v1       #_arg0:B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@3dd
    move-result-object v2

    #@3de
    .line 581
    .restart local v2       #_arg1:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3e1
    move-result v3

    #@3e2
    .line 583
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3e5
    move-result v8

    #@3e6
    .line 584
    .restart local v8       #_arg3_length:I
    if-gez v8, :cond_404

    #@3e8
    .line 585
    const/4 v4, 0x0

    #@3e9
    .line 591
    .restart local v4       #_arg3:[B
    :goto_3e9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3ec
    move-result v9

    #@3ed
    .line 592
    .restart local v9       #_arg4_length:I
    if-gez v9, :cond_407

    #@3ef
    .line 593
    const/4 v5, 0x0

    #@3f0
    .restart local v5       #_arg4:[I
    :goto_3f0
    move-object v0, p0

    #@3f1
    .line 598
    invoke-virtual/range {v0 .. v5}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_KUH_Transmit(B[BI[B[I)J

    #@3f4
    move-result-wide v10

    #@3f5
    .line 599
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f8
    .line 600
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@3fb
    .line 601
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeByteArray([B)V

    #@3fe
    .line 602
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeIntArray([I)V

    #@401
    .line 603
    const/4 v0, 0x1

    #@402
    goto/16 :goto_7

    #@404
    .line 588
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v9           #_arg4_length:I
    .end local v10           #_result:J
    :cond_404
    new-array v4, v8, [B

    #@406
    .restart local v4       #_arg3:[B
    goto :goto_3e9

    #@407
    .line 596
    .restart local v9       #_arg4_length:I
    :cond_407
    new-array v5, v9, [I

    #@409
    .restart local v5       #_arg4:[I
    goto :goto_3f0

    #@40a
    .line 607
    .end local v1           #_arg0:B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:I
    .end local v4           #_arg3:[B
    .end local v5           #_arg4:[I
    .end local v8           #_arg3_length:I
    .end local v9           #_arg4_length:I
    :sswitch_40a
    const-string v0, "android.ktuca.IKtUcaIF"

    #@40c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40f
    .line 609
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@412
    move-result-object v1

    #@413
    .line 611
    .local v1, _arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@416
    move-result v6

    #@417
    .line 612
    .restart local v6       #_arg1_length:I
    if-gez v6, :cond_434

    #@419
    .line 613
    const/4 v2, 0x0

    #@41a
    .line 619
    .restart local v2       #_arg1:[B
    :goto_41a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41d
    move-result v7

    #@41e
    .line 620
    .restart local v7       #_arg2_length:I
    if-gez v7, :cond_437

    #@420
    .line 621
    const/4 v3, 0x0

    #@421
    .line 626
    .local v3, _arg2:[I
    :goto_421
    invoke-virtual {p0, v1, v2, v3}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_OpenT([B[B[I)J

    #@424
    move-result-wide v10

    #@425
    .line 627
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@428
    .line 628
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@42b
    .line 629
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    #@42e
    .line 630
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@431
    .line 631
    const/4 v0, 0x1

    #@432
    goto/16 :goto_7

    #@434
    .line 616
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v7           #_arg2_length:I
    .end local v10           #_result:J
    :cond_434
    new-array v2, v6, [B

    #@436
    .restart local v2       #_arg1:[B
    goto :goto_41a

    #@437
    .line 624
    .restart local v7       #_arg2_length:I
    :cond_437
    new-array v3, v7, [I

    #@439
    .restart local v3       #_arg2:[I
    goto :goto_421

    #@43a
    .line 635
    .end local v1           #_arg0:[B
    .end local v2           #_arg1:[B
    .end local v3           #_arg2:[I
    .end local v6           #_arg1_length:I
    .end local v7           #_arg2_length:I
    :sswitch_43a
    const-string v0, "android.ktuca.IKtUcaIF"

    #@43c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43f
    .line 637
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@442
    move-result-object v1

    #@443
    .line 639
    .restart local v1       #_arg0:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    #@446
    move-result v2

    #@447
    .line 640
    .local v2, _arg1:B
    invoke-virtual {p0, v1, v2}, Landroid/ktuca/IKtUcaIF$Stub;->KUCA_CloseT([BB)J

    #@44a
    move-result-wide v10

    #@44b
    .line 641
    .restart local v10       #_result:J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44e
    .line 642
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    #@451
    .line 643
    const/4 v0, 0x1

    #@452
    goto/16 :goto_7

    #@454
    .line 39
    :sswitch_data_454
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_31
        0x4 -> :sswitch_5d
        0x5 -> :sswitch_96
        0x6 -> :sswitch_cf
        0x7 -> :sswitch_108
        0x8 -> :sswitch_141
        0x9 -> :sswitch_17a
        0xa -> :sswitch_1b3
        0xb -> :sswitch_1e3
        0xc -> :sswitch_21c
        0xd -> :sswitch_250
        0xe -> :sswitch_289
        0xf -> :sswitch_2b9
        0x10 -> :sswitch_2f2
        0x11 -> :sswitch_30c
        0x12 -> :sswitch_32f
        0x13 -> :sswitch_35f
        0x14 -> :sswitch_38f
        0x15 -> :sswitch_3a5
        0x16 -> :sswitch_3bb
        0x17 -> :sswitch_3d1
        0x18 -> :sswitch_40a
        0x19 -> :sswitch_43a
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
