.class Landroid/test/InstrumentationTestCase$2;
.super Ljava/lang/Object;
.source "InstrumentationTestCase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/test/InstrumentationTestCase;->runTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/test/InstrumentationTestCase;

.field final synthetic val$exceptions:[Ljava/lang/Throwable;

.field final synthetic val$repetitive:Z

.field final synthetic val$testMethod:Ljava/lang/reflect/Method;

.field final synthetic val$tolerance:I


# direct methods
.method constructor <init>(Landroid/test/InstrumentationTestCase;Ljava/lang/reflect/Method;IZ[Ljava/lang/Throwable;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 186
    iput-object p1, p0, Landroid/test/InstrumentationTestCase$2;->this$0:Landroid/test/InstrumentationTestCase;

    #@2
    iput-object p2, p0, Landroid/test/InstrumentationTestCase$2;->val$testMethod:Ljava/lang/reflect/Method;

    #@4
    iput p3, p0, Landroid/test/InstrumentationTestCase$2;->val$tolerance:I

    #@6
    iput-boolean p4, p0, Landroid/test/InstrumentationTestCase$2;->val$repetitive:Z

    #@8
    iput-object p5, p0, Landroid/test/InstrumentationTestCase$2;->val$exceptions:[Ljava/lang/Throwable;

    #@a
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@d
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 189
    :try_start_0
    iget-object v1, p0, Landroid/test/InstrumentationTestCase$2;->this$0:Landroid/test/InstrumentationTestCase;

    #@2
    iget-object v2, p0, Landroid/test/InstrumentationTestCase$2;->val$testMethod:Ljava/lang/reflect/Method;

    #@4
    iget v3, p0, Landroid/test/InstrumentationTestCase$2;->val$tolerance:I

    #@6
    iget-boolean v4, p0, Landroid/test/InstrumentationTestCase$2;->val$repetitive:Z

    #@8
    invoke-static {v1, v2, v3, v4}, Landroid/test/InstrumentationTestCase;->access$000(Landroid/test/InstrumentationTestCase;Ljava/lang/reflect/Method;IZ)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 193
    :goto_b
    return-void

    #@c
    .line 190
    :catch_c
    move-exception v0

    #@d
    .line 191
    .local v0, throwable:Ljava/lang/Throwable;
    iget-object v1, p0, Landroid/test/InstrumentationTestCase$2;->val$exceptions:[Ljava/lang/Throwable;

    #@f
    const/4 v2, 0x0

    #@10
    aput-object v0, v1, v2

    #@12
    goto :goto_b
.end method
