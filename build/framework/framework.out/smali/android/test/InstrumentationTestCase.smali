.class public Landroid/test/InstrumentationTestCase;
.super Ljunit/framework/TestCase;
.source "InstrumentationTestCase.java"


# instance fields
.field private mInstrumentation:Landroid/app/Instrumentation;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Ljunit/framework/TestCase;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Landroid/test/InstrumentationTestCase;Ljava/lang/reflect/Method;IZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/test/InstrumentationTestCase;->runMethod(Ljava/lang/reflect/Method;IZ)V

    #@3
    return-void
.end method

.method private runMethod(Ljava/lang/reflect/Method;I)V
    .registers 4
    .parameter "runMethod"
    .parameter "tolerance"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/test/InstrumentationTestCase;->runMethod(Ljava/lang/reflect/Method;IZ)V

    #@4
    .line 206
    return-void
.end method

.method private runMethod(Ljava/lang/reflect/Method;IZ)V
    .registers 11
    .parameter "runMethod"
    .parameter "tolerance"
    .parameter "isRepetitive"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 209
    const/4 v1, 0x0

    #@2
    .line 211
    .local v1, exception:Ljava/lang/Throwable;
    const/4 v3, 0x0

    #@3
    .line 214
    .local v3, runCount:I
    :cond_3
    const/4 v4, 0x0

    #@4
    :try_start_4
    check-cast v4, [Ljava/lang/Object;

    #@6
    invoke-virtual {p1, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_61
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_9} :catch_28
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_9} :catch_46

    #@9
    .line 215
    const/4 v1, 0x0

    #@a
    .line 223
    add-int/lit8 v3, v3, 0x1

    #@c
    .line 225
    if-eqz p3, :cond_1f

    #@e
    .line 226
    new-instance v2, Landroid/os/Bundle;

    #@10
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@13
    .line 227
    .local v2, iterations:Landroid/os/Bundle;
    const-string v4, "currentiterations"

    #@15
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 228
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v6, v2}, Landroid/app/Instrumentation;->sendStatus(ILandroid/os/Bundle;)V

    #@1f
    .line 231
    .end local v2           #iterations:Landroid/os/Bundle;
    :cond_1f
    :goto_1f
    if-ge v3, p2, :cond_25

    #@21
    if-nez p3, :cond_3

    #@23
    if-nez v1, :cond_3

    #@25
    .line 233
    :cond_25
    if-eqz v1, :cond_78

    #@27
    .line 234
    throw v1

    #@28
    .line 216
    :catch_28
    move-exception v0

    #@29
    .line 217
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    :try_start_29
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->fillInStackTrace()Ljava/lang/Throwable;

    #@2c
    .line 218
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;
    :try_end_2f
    .catchall {:try_start_29 .. :try_end_2f} :catchall_61

    #@2f
    move-result-object v1

    #@30
    .line 223
    add-int/lit8 v3, v3, 0x1

    #@32
    .line 225
    if-eqz p3, :cond_1f

    #@34
    .line 226
    new-instance v2, Landroid/os/Bundle;

    #@36
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@39
    .line 227
    .restart local v2       #iterations:Landroid/os/Bundle;
    const-string v4, "currentiterations"

    #@3b
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@3e
    .line 228
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, v6, v2}, Landroid/app/Instrumentation;->sendStatus(ILandroid/os/Bundle;)V

    #@45
    goto :goto_1f

    #@46
    .line 219
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    .end local v2           #iterations:Landroid/os/Bundle;
    :catch_46
    move-exception v0

    #@47
    .line 220
    .local v0, e:Ljava/lang/IllegalAccessException;
    :try_start_47
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->fillInStackTrace()Ljava/lang/Throwable;
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_61

    #@4a
    .line 221
    move-object v1, v0

    #@4b
    .line 223
    add-int/lit8 v3, v3, 0x1

    #@4d
    .line 225
    if-eqz p3, :cond_1f

    #@4f
    .line 226
    new-instance v2, Landroid/os/Bundle;

    #@51
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@54
    .line 227
    .restart local v2       #iterations:Landroid/os/Bundle;
    const-string v4, "currentiterations"

    #@56
    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@59
    .line 228
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v4, v6, v2}, Landroid/app/Instrumentation;->sendStatus(ILandroid/os/Bundle;)V

    #@60
    goto :goto_1f

    #@61
    .line 223
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    .end local v2           #iterations:Landroid/os/Bundle;
    :catchall_61
    move-exception v4

    #@62
    add-int/lit8 v3, v3, 0x1

    #@64
    .line 225
    if-eqz p3, :cond_77

    #@66
    .line 226
    new-instance v2, Landroid/os/Bundle;

    #@68
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@6b
    .line 227
    .restart local v2       #iterations:Landroid/os/Bundle;
    const-string v5, "currentiterations"

    #@6d
    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@70
    .line 228
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v5, v6, v2}, Landroid/app/Instrumentation;->sendStatus(ILandroid/os/Bundle;)V

    #@77
    .line 229
    .end local v2           #iterations:Landroid/os/Bundle;
    :cond_77
    throw v4

    #@78
    .line 236
    :cond_78
    return-void
.end method


# virtual methods
.method public getInstrumentation()Landroid/app/Instrumentation;
    .registers 2

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/test/InstrumentationTestCase;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    return-object v0
.end method

.method public injectInsrumentation(Landroid/app/Instrumentation;)V
    .registers 2
    .parameter "instrumentation"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 61
    invoke-virtual {p0, p1}, Landroid/test/InstrumentationTestCase;->injectInstrumentation(Landroid/app/Instrumentation;)V

    #@3
    .line 62
    return-void
.end method

.method public injectInstrumentation(Landroid/app/Instrumentation;)V
    .registers 2
    .parameter "instrumentation"

    #@0
    .prologue
    .line 47
    iput-object p1, p0, Landroid/test/InstrumentationTestCase;->mInstrumentation:Landroid/app/Instrumentation;

    #@2
    .line 48
    return-void
.end method

.method public final launchActivity(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)Landroid/app/Activity;
    .registers 6
    .parameter "pkg"
    .parameter
    .parameter "extras"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Activity;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 93
    .local p2, activityCls:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.intent.action.MAIN"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 94
    .local v0, intent:Landroid/content/Intent;
    if-eqz p3, :cond_c

    #@9
    .line 95
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@c
    .line 97
    :cond_c
    invoke-virtual {p0, p1, p2, v0}, Landroid/test/InstrumentationTestCase;->launchActivityWithIntent(Ljava/lang/String;Ljava/lang/Class;Landroid/content/Intent;)Landroid/app/Activity;

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method public final launchActivityWithIntent(Ljava/lang/String;Ljava/lang/Class;Landroid/content/Intent;)Landroid/app/Activity;
    .registers 6
    .parameter "pkg"
    .parameter
    .parameter "intent"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Activity;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/content/Intent;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 117
    .local p2, activityCls:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {p3, p1, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7
    .line 118
    const/high16 v1, 0x1000

    #@9
    invoke-virtual {p3, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c
    .line 119
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p3}, Landroid/app/Instrumentation;->startActivitySync(Landroid/content/Intent;)Landroid/app/Activity;

    #@13
    move-result-object v0

    #@14
    .line 120
    .local v0, activity:Landroid/app/Activity;,"TT;"
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@1b
    .line 121
    return-object v0
.end method

.method protected runTest()V
    .registers 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 155
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getName()Ljava/lang/String;

    #@4
    move-result-object v7

    #@5
    .line 156
    .local v7, fName:Ljava/lang/String;
    invoke-static {v7}, Landroid/test/InstrumentationTestCase;->assertNotNull(Ljava/lang/Object;)V

    #@8
    .line 157
    const/4 v9, 0x0

    #@9
    .line 163
    .local v9, method:Ljava/lang/reflect/Method;
    :try_start_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@c
    move-result-object v1

    #@d
    const/4 v0, 0x0

    #@e
    check-cast v0, [Ljava/lang/Class;

    #@10
    invoke-virtual {v1, v7, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_13
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9 .. :try_end_13} :catch_72

    #@13
    move-result-object v9

    #@14
    .line 168
    :goto_14
    invoke-virtual {v9}, Ljava/lang/reflect/Method;->getModifiers()I

    #@17
    move-result v0

    #@18
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_3a

    #@1e
    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v1, "Method \""

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, "\" should be public"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-static {v0}, Landroid/test/InstrumentationTestCase;->fail(Ljava/lang/String;)V

    #@3a
    .line 172
    :cond_3a
    const/4 v10, 0x1

    #@3b
    .line 173
    .local v10, runCount:I
    const/4 v8, 0x0

    #@3c
    .line 174
    .local v8, isRepetitive:Z
    const-class v0, Landroid/test/FlakyTest;

    #@3e
    invoke-virtual {v9, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_90

    #@44
    .line 175
    const-class v0, Landroid/test/FlakyTest;

    #@46
    invoke-virtual {v9, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, Landroid/test/FlakyTest;

    #@4c
    invoke-interface {v0}, Landroid/test/FlakyTest;->tolerance()I

    #@4f
    move-result v10

    #@50
    .line 181
    :cond_50
    :goto_50
    const-class v0, Landroid/test/UiThreadTest;

    #@52
    invoke-virtual {v9, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_a6

    #@58
    .line 182
    move v3, v10

    #@59
    .line 183
    .local v3, tolerance:I
    move v4, v8

    #@5a
    .line 184
    .local v4, repetitive:Z
    move-object v2, v9

    #@5b
    .line 185
    .local v2, testMethod:Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    #@5c
    new-array v5, v0, [Ljava/lang/Throwable;

    #@5e
    .line 186
    .local v5, exceptions:[Ljava/lang/Throwable;
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@61
    move-result-object v11

    #@62
    new-instance v0, Landroid/test/InstrumentationTestCase$2;

    #@64
    move-object v1, p0

    #@65
    invoke-direct/range {v0 .. v5}, Landroid/test/InstrumentationTestCase$2;-><init>(Landroid/test/InstrumentationTestCase;Ljava/lang/reflect/Method;IZ[Ljava/lang/Throwable;)V

    #@68
    invoke-virtual {v11, v0}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    #@6b
    .line 195
    aget-object v0, v5, v12

    #@6d
    if-eqz v0, :cond_a9

    #@6f
    .line 196
    aget-object v0, v5, v12

    #@71
    throw v0

    #@72
    .line 164
    .end local v2           #testMethod:Ljava/lang/reflect/Method;
    .end local v3           #tolerance:I
    .end local v4           #repetitive:Z
    .end local v5           #exceptions:[Ljava/lang/Throwable;
    .end local v8           #isRepetitive:Z
    .end local v10           #runCount:I
    :catch_72
    move-exception v6

    #@73
    .line 165
    .local v6, e:Ljava/lang/NoSuchMethodException;
    new-instance v0, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v1, "Method \""

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v0

    #@82
    const-string v1, "\" not found"

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v0

    #@88
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v0

    #@8c
    invoke-static {v0}, Landroid/test/InstrumentationTestCase;->fail(Ljava/lang/String;)V

    #@8f
    goto :goto_14

    #@90
    .line 176
    .end local v6           #e:Ljava/lang/NoSuchMethodException;
    .restart local v8       #isRepetitive:Z
    .restart local v10       #runCount:I
    :cond_90
    const-class v0, Landroid/test/RepetitiveTest;

    #@92
    invoke-virtual {v9, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    #@95
    move-result v0

    #@96
    if-eqz v0, :cond_50

    #@98
    .line 177
    const-class v0, Landroid/test/RepetitiveTest;

    #@9a
    invoke-virtual {v9, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@9d
    move-result-object v0

    #@9e
    check-cast v0, Landroid/test/RepetitiveTest;

    #@a0
    invoke-interface {v0}, Landroid/test/RepetitiveTest;->numIterations()I

    #@a3
    move-result v10

    #@a4
    .line 178
    const/4 v8, 0x1

    #@a5
    goto :goto_50

    #@a6
    .line 199
    :cond_a6
    invoke-direct {p0, v9, v10, v8}, Landroid/test/InstrumentationTestCase;->runMethod(Ljava/lang/reflect/Method;IZ)V

    #@a9
    .line 201
    :cond_a9
    return-void
.end method

.method public runTestOnUiThread(Ljava/lang/Runnable;)V
    .registers 6
    .parameter "r"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 134
    const/4 v1, 0x1

    #@2
    new-array v0, v1, [Ljava/lang/Throwable;

    #@4
    .line 135
    .local v0, exceptions:[Ljava/lang/Throwable;
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@7
    move-result-object v1

    #@8
    new-instance v2, Landroid/test/InstrumentationTestCase$1;

    #@a
    invoke-direct {v2, p0, p1, v0}, Landroid/test/InstrumentationTestCase$1;-><init>(Landroid/test/InstrumentationTestCase;Ljava/lang/Runnable;[Ljava/lang/Throwable;)V

    #@d
    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->runOnMainSync(Ljava/lang/Runnable;)V

    #@10
    .line 144
    aget-object v1, v0, v3

    #@12
    if-eqz v1, :cond_17

    #@14
    .line 145
    aget-object v1, v0, v3

    #@16
    throw v1

    #@17
    .line 147
    :cond_17
    return-void
.end method

.method public sendKeys(Ljava/lang/String;)V
    .registers 16
    .parameter "keysSequence"

    #@0
    .prologue
    .line 248
    const-string v11, " "

    #@2
    invoke-virtual {p1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@5
    move-result-object v9

    #@6
    .line 249
    .local v9, keys:[Ljava/lang/String;
    array-length v0, v9

    #@7
    .line 251
    .local v0, count:I
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@a
    move-result-object v3

    #@b
    .line 253
    .local v3, instrumentation:Landroid/app/Instrumentation;
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v0, :cond_a4

    #@e
    .line 254
    aget-object v5, v9, v2

    #@10
    .line 255
    .local v5, key:Ljava/lang/String;
    const/16 v11, 0x2a

    #@12
    invoke-virtual {v5, v11}, Ljava/lang/String;->indexOf(I)I

    #@15
    move-result v10

    #@16
    .line 259
    .local v10, repeater:I
    const/4 v11, -0x1

    #@17
    if-ne v10, v11, :cond_4a

    #@19
    const/4 v8, 0x1

    #@1a
    .line 265
    .local v8, keyCount:I
    :goto_1a
    const/4 v11, -0x1

    #@1b
    if-eq v10, v11, :cond_23

    #@1d
    .line 266
    add-int/lit8 v11, v10, 0x1

    #@1f
    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    .line 269
    :cond_23
    const/4 v4, 0x0

    #@24
    .local v4, j:I
    :goto_24
    if-ge v4, v8, :cond_6d

    #@26
    .line 271
    :try_start_26
    const-class v11, Landroid/view/KeyEvent;

    #@28
    new-instance v12, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v13, "KEYCODE_"

    #@2f
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v12

    #@33
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v12

    #@37
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v12

    #@3b
    invoke-virtual {v11, v12}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@3e
    move-result-object v7

    #@3f
    .line 272
    .local v7, keyCodeField:Ljava/lang/reflect/Field;
    const/4 v11, 0x0

    #@40
    invoke-virtual {v7, v11}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_43
    .catch Ljava/lang/NoSuchFieldException; {:try_start_26 .. :try_end_43} :catch_70
    .catch Ljava/lang/IllegalAccessException; {:try_start_26 .. :try_end_43} :catch_8a

    #@43
    move-result v6

    #@44
    .line 274
    .local v6, keyCode:I
    :try_start_44
    invoke-virtual {v3, v6}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_47
    .catch Ljava/lang/SecurityException; {:try_start_44 .. :try_end_47} :catch_a8
    .catch Ljava/lang/NoSuchFieldException; {:try_start_44 .. :try_end_47} :catch_70
    .catch Ljava/lang/IllegalAccessException; {:try_start_44 .. :try_end_47} :catch_8a

    #@47
    .line 269
    :goto_47
    add-int/lit8 v4, v4, 0x1

    #@49
    goto :goto_24

    #@4a
    .line 259
    .end local v4           #j:I
    .end local v6           #keyCode:I
    .end local v7           #keyCodeField:Ljava/lang/reflect/Field;
    .end local v8           #keyCount:I
    :cond_4a
    const/4 v11, 0x0

    #@4b
    :try_start_4b
    invoke-virtual {v5, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@4e
    move-result-object v11

    #@4f
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_52
    .catch Ljava/lang/NumberFormatException; {:try_start_4b .. :try_end_52} :catch_54

    #@52
    move-result v8

    #@53
    goto :goto_1a

    #@54
    .line 260
    :catch_54
    move-exception v1

    #@55
    .line 261
    .local v1, e:Ljava/lang/NumberFormatException;
    const-string v11, "ActivityTestCase"

    #@57
    new-instance v12, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v13, "Invalid repeat count: "

    #@5e
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v12

    #@62
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v12

    #@66
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v12

    #@6a
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 253
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_6d
    :goto_6d
    add-int/lit8 v2, v2, 0x1

    #@6f
    goto :goto_c

    #@70
    .line 280
    .restart local v4       #j:I
    .restart local v8       #keyCount:I
    :catch_70
    move-exception v1

    #@71
    .line 281
    .local v1, e:Ljava/lang/NoSuchFieldException;
    const-string v11, "ActivityTestCase"

    #@73
    new-instance v12, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v13, "Unknown keycode: KEYCODE_"

    #@7a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v12

    #@7e
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v12

    #@82
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v12

    #@86
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_6d

    #@8a
    .line 283
    .end local v1           #e:Ljava/lang/NoSuchFieldException;
    :catch_8a
    move-exception v1

    #@8b
    .line 284
    .local v1, e:Ljava/lang/IllegalAccessException;
    const-string v11, "ActivityTestCase"

    #@8d
    new-instance v12, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v13, "Unknown keycode: KEYCODE_"

    #@94
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v12

    #@98
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v12

    #@9c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v12

    #@a0
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_6d

    #@a4
    .line 290
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    .end local v4           #j:I
    .end local v5           #key:Ljava/lang/String;
    .end local v8           #keyCount:I
    .end local v10           #repeater:I
    :cond_a4
    invoke-virtual {v3}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@a7
    .line 291
    return-void

    #@a8
    .line 275
    .restart local v4       #j:I
    .restart local v5       #key:Ljava/lang/String;
    .restart local v6       #keyCode:I
    .restart local v7       #keyCodeField:Ljava/lang/reflect/Field;
    .restart local v8       #keyCount:I
    .restart local v10       #repeater:I
    :catch_a8
    move-exception v11

    #@a9
    goto :goto_47
.end method

.method public varargs sendKeys([I)V
    .registers 6
    .parameter "keys"

    #@0
    .prologue
    .line 300
    array-length v0, p1

    #@1
    .line 301
    .local v0, count:I
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@4
    move-result-object v2

    #@5
    .line 303
    .local v2, instrumentation:Landroid/app/Instrumentation;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    if-ge v1, v0, :cond_10

    #@8
    .line 305
    :try_start_8
    aget v3, p1, v1

    #@a
    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_d
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_d} :catch_14

    #@d
    .line 303
    :goto_d
    add-int/lit8 v1, v1, 0x1

    #@f
    goto :goto_6

    #@10
    .line 313
    :cond_10
    invoke-virtual {v2}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@13
    .line 314
    return-void

    #@14
    .line 306
    :catch_14
    move-exception v3

    #@15
    goto :goto_d
.end method

.method public varargs sendRepeatedKeys([I)V
    .registers 10
    .parameter "keys"

    #@0
    .prologue
    .line 324
    array-length v0, p1

    #@1
    .line 325
    .local v0, count:I
    and-int/lit8 v6, v0, 0x1

    #@3
    const/4 v7, 0x1

    #@4
    if-ne v6, v7, :cond_e

    #@6
    .line 326
    new-instance v6, Ljava/lang/IllegalArgumentException;

    #@8
    const-string v7, "The size of the keys array must be a multiple of 2"

    #@a
    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v6

    #@e
    .line 330
    :cond_e
    invoke-virtual {p0}, Landroid/test/InstrumentationTestCase;->getInstrumentation()Landroid/app/Instrumentation;

    #@11
    move-result-object v2

    #@12
    .line 332
    .local v2, instrumentation:Landroid/app/Instrumentation;
    const/4 v1, 0x0

    #@13
    .local v1, i:I
    :goto_13
    if-ge v1, v0, :cond_27

    #@15
    .line 333
    aget v5, p1, v1

    #@17
    .line 334
    .local v5, keyCount:I
    add-int/lit8 v6, v1, 0x1

    #@19
    aget v4, p1, v6

    #@1b
    .line 335
    .local v4, keyCode:I
    const/4 v3, 0x0

    #@1c
    .local v3, j:I
    :goto_1c
    if-ge v3, v5, :cond_24

    #@1e
    .line 337
    :try_start_1e
    invoke-virtual {v2, v4}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V
    :try_end_21
    .catch Ljava/lang/SecurityException; {:try_start_1e .. :try_end_21} :catch_2b

    #@21
    .line 335
    :goto_21
    add-int/lit8 v3, v3, 0x1

    #@23
    goto :goto_1c

    #@24
    .line 332
    :cond_24
    add-int/lit8 v1, v1, 0x2

    #@26
    goto :goto_13

    #@27
    .line 346
    .end local v3           #j:I
    .end local v4           #keyCode:I
    .end local v5           #keyCount:I
    :cond_27
    invoke-virtual {v2}, Landroid/app/Instrumentation;->waitForIdleSync()V

    #@2a
    .line 347
    return-void

    #@2b
    .line 338
    .restart local v3       #j:I
    .restart local v4       #keyCode:I
    .restart local v5       #keyCount:I
    :catch_2b
    move-exception v6

    #@2c
    goto :goto_21
.end method

.method protected tearDown()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 358
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@7
    .line 359
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    #@e
    .line 360
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@15
    .line 361
    invoke-super {p0}, Ljunit/framework/TestCase;->tearDown()V

    #@18
    .line 362
    return-void
.end method
