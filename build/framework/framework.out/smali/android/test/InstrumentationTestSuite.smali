.class public Landroid/test/InstrumentationTestSuite;
.super Ljunit/framework/TestSuite;
.source "InstrumentationTestSuite.java"


# instance fields
.field private final mInstrumentation:Landroid/app/Instrumentation;


# direct methods
.method public constructor <init>(Landroid/app/Instrumentation;)V
    .registers 2
    .parameter "instr"

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Ljunit/framework/TestSuite;-><init>()V

    #@3
    .line 38
    iput-object p1, p0, Landroid/test/InstrumentationTestSuite;->mInstrumentation:Landroid/app/Instrumentation;

    #@5
    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Landroid/app/Instrumentation;)V
    .registers 3
    .parameter "theClass"
    .parameter "instr"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Ljunit/framework/TestSuite;-><init>(Ljava/lang/Class;)V

    #@3
    .line 54
    iput-object p2, p0, Landroid/test/InstrumentationTestSuite;->mInstrumentation:Landroid/app/Instrumentation;

    #@5
    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/app/Instrumentation;)V
    .registers 3
    .parameter "name"
    .parameter "instr"

    #@0
    .prologue
    .line 43
    invoke-direct {p0, p1}, Ljunit/framework/TestSuite;-><init>(Ljava/lang/String;)V

    #@3
    .line 44
    iput-object p2, p0, Landroid/test/InstrumentationTestSuite;->mInstrumentation:Landroid/app/Instrumentation;

    #@5
    .line 45
    return-void
.end method


# virtual methods
.method public addTestSuite(Ljava/lang/Class;)V
    .registers 4
    .parameter "testClass"

    #@0
    .prologue
    .line 60
    new-instance v0, Landroid/test/InstrumentationTestSuite;

    #@2
    iget-object v1, p0, Landroid/test/InstrumentationTestSuite;->mInstrumentation:Landroid/app/Instrumentation;

    #@4
    invoke-direct {v0, p1, v1}, Landroid/test/InstrumentationTestSuite;-><init>(Ljava/lang/Class;Landroid/app/Instrumentation;)V

    #@7
    invoke-virtual {p0, v0}, Landroid/test/InstrumentationTestSuite;->addTest(Ljunit/framework/Test;)V

    #@a
    .line 61
    return-void
.end method

.method public runTest(Ljunit/framework/Test;Ljunit/framework/TestResult;)V
    .registers 5
    .parameter "test"
    .parameter "result"

    #@0
    .prologue
    .line 67
    instance-of v0, p1, Landroid/test/InstrumentationTestCase;

    #@2
    if-eqz v0, :cond_c

    #@4
    move-object v0, p1

    #@5
    .line 68
    check-cast v0, Landroid/test/InstrumentationTestCase;

    #@7
    iget-object v1, p0, Landroid/test/InstrumentationTestSuite;->mInstrumentation:Landroid/app/Instrumentation;

    #@9
    invoke-virtual {v0, v1}, Landroid/test/InstrumentationTestCase;->injectInstrumentation(Landroid/app/Instrumentation;)V

    #@c
    .line 72
    :cond_c
    invoke-super {p0, p1, p2}, Ljunit/framework/TestSuite;->runTest(Ljunit/framework/Test;Ljunit/framework/TestResult;)V

    #@f
    .line 73
    return-void
.end method
