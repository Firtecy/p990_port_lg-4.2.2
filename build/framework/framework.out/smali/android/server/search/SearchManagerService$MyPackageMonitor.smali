.class Landroid/server/search/SearchManagerService$MyPackageMonitor;
.super Lcom/android/internal/content/PackageMonitor;
.source "SearchManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/server/search/SearchManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyPackageMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/server/search/SearchManagerService;


# direct methods
.method constructor <init>(Landroid/server/search/SearchManagerService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 139
    iput-object p1, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@2
    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    #@5
    return-void
.end method

.method private updateSearchables()V
    .registers 7

    #@0
    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/server/search/SearchManagerService$MyPackageMonitor;->getChangingUserId()I

    #@3
    move-result v0

    #@4
    .line 153
    .local v0, changingUserId:I
    iget-object v3, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@6
    invoke-static {v3}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@9
    move-result-object v4

    #@a
    monitor-enter v4

    #@b
    .line 155
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    :try_start_c
    iget-object v3, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@e
    invoke-static {v3}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    #@15
    move-result v3

    #@16
    if-ge v1, v3, :cond_37

    #@18
    .line 156
    iget-object v3, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@1a
    invoke-static {v3}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@21
    move-result v3

    #@22
    if-ne v0, v3, :cond_53

    #@24
    .line 157
    iget-object v3, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@26
    iget-object v5, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@28
    invoke-static {v5}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@2f
    move-result v5

    #@30
    invoke-static {v3, v5}, Landroid/server/search/SearchManagerService;->access$300(Landroid/server/search/SearchManagerService;I)Landroid/server/search/Searchables;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Landroid/server/search/Searchables;->buildSearchableList()V

    #@37
    .line 161
    :cond_37
    monitor-exit v4
    :try_end_38
    .catchall {:try_start_c .. :try_end_38} :catchall_56

    #@38
    .line 163
    new-instance v2, Landroid/content/Intent;

    #@3a
    const-string v3, "android.search.action.SEARCHABLES_CHANGED"

    #@3c
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 164
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x2800

    #@41
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@44
    .line 166
    iget-object v3, p0, Landroid/server/search/SearchManagerService$MyPackageMonitor;->this$0:Landroid/server/search/SearchManagerService;

    #@46
    invoke-static {v3}, Landroid/server/search/SearchManagerService;->access$200(Landroid/server/search/SearchManagerService;)Landroid/content/Context;

    #@49
    move-result-object v3

    #@4a
    new-instance v4, Landroid/os/UserHandle;

    #@4c
    invoke-direct {v4, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@4f
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@52
    .line 167
    return-void

    #@53
    .line 155
    .end local v2           #intent:Landroid/content/Intent;
    :cond_53
    add-int/lit8 v1, v1, 0x1

    #@55
    goto :goto_c

    #@56
    .line 161
    :catchall_56
    move-exception v3

    #@57
    :try_start_57
    monitor-exit v4
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_56

    #@58
    throw v3
.end method


# virtual methods
.method public onPackageModified(Ljava/lang/String;)V
    .registers 2
    .parameter "pkg"

    #@0
    .prologue
    .line 148
    invoke-direct {p0}, Landroid/server/search/SearchManagerService$MyPackageMonitor;->updateSearchables()V

    #@3
    .line 149
    return-void
.end method

.method public onSomePackagesChanged()V
    .registers 1

    #@0
    .prologue
    .line 143
    invoke-direct {p0}, Landroid/server/search/SearchManagerService$MyPackageMonitor;->updateSearchables()V

    #@3
    .line 144
    return-void
.end method
