.class public Landroid/server/search/SearchManagerService;
.super Landroid/app/ISearchManager$Stub;
.source "SearchManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/server/search/SearchManagerService$1;,
        Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;,
        Landroid/server/search/SearchManagerService$MyPackageMonitor;,
        Landroid/server/search/SearchManagerService$UserReceiver;,
        Landroid/server/search/SearchManagerService$BootCompletedReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SearchManagerService"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSearchables:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/server/search/Searchables;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 73
    invoke-direct {p0}, Landroid/app/ISearchManager$Stub;-><init>()V

    #@4
    .line 65
    new-instance v0, Landroid/util/SparseArray;

    #@6
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@9
    iput-object v0, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@b
    .line 74
    iput-object p1, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@d
    .line 75
    iget-object v0, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@f
    new-instance v1, Landroid/server/search/SearchManagerService$BootCompletedReceiver;

    #@11
    invoke-direct {v1, p0, v4}, Landroid/server/search/SearchManagerService$BootCompletedReceiver;-><init>(Landroid/server/search/SearchManagerService;Landroid/server/search/SearchManagerService$1;)V

    #@14
    new-instance v2, Landroid/content/IntentFilter;

    #@16
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    #@18
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e
    .line 77
    iget-object v0, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@20
    new-instance v1, Landroid/server/search/SearchManagerService$UserReceiver;

    #@22
    invoke-direct {v1, p0, v4}, Landroid/server/search/SearchManagerService$UserReceiver;-><init>(Landroid/server/search/SearchManagerService;Landroid/server/search/SearchManagerService$1;)V

    #@25
    new-instance v2, Landroid/content/IntentFilter;

    #@27
    const-string v3, "android.intent.action.USER_REMOVED"

    #@29
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2c
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2f
    .line 79
    new-instance v0, Landroid/server/search/SearchManagerService$MyPackageMonitor;

    #@31
    invoke-direct {v0, p0}, Landroid/server/search/SearchManagerService$MyPackageMonitor;-><init>(Landroid/server/search/SearchManagerService;)V

    #@34
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@36
    const/4 v2, 0x1

    #@37
    invoke-virtual {v0, p1, v4, v1, v2}, Landroid/server/search/SearchManagerService$MyPackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    #@3a
    .line 80
    return-void
.end method

.method static synthetic access$200(Landroid/server/search/SearchManagerService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/server/search/SearchManagerService;I)Landroid/server/search/Searchables;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Landroid/server/search/SearchManagerService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/server/search/SearchManagerService;->onUserRemoved(I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@2
    return-object v0
.end method

.method private getSearchables(I)Landroid/server/search/Searchables;
    .registers 8
    .parameter "userId"

    #@0
    .prologue
    .line 83
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@3
    move-result-wide v0

    #@4
    .line 85
    .local v0, origId:J
    :try_start_4
    iget-object v4, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@6
    const-string/jumbo v5, "user"

    #@9
    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v4

    #@d
    check-cast v4, Landroid/os/UserManager;

    #@f
    invoke-virtual {v4, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_1f

    #@12
    move-result-object v4

    #@13
    if-eqz v4, :cond_1d

    #@15
    const/4 v3, 0x1

    #@16
    .line 87
    .local v3, userExists:Z
    :goto_16
    if-nez v3, :cond_24

    #@18
    const/4 v2, 0x0

    #@19
    .line 89
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@1c
    .line 100
    :goto_1c
    return-object v2

    #@1d
    .line 85
    .end local v3           #userExists:Z
    :cond_1d
    const/4 v3, 0x0

    #@1e
    goto :goto_16

    #@1f
    .line 89
    :catchall_1f
    move-exception v4

    #@20
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@23
    throw v4

    #@24
    .restart local v3       #userExists:Z
    :cond_24
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@27
    .line 91
    iget-object v5, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@29
    monitor-enter v5

    #@2a
    .line 92
    :try_start_2a
    iget-object v4, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@2c
    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v2

    #@30
    check-cast v2, Landroid/server/search/Searchables;

    #@32
    .line 94
    .local v2, searchables:Landroid/server/search/Searchables;
    if-nez v2, :cond_43

    #@34
    .line 96
    new-instance v2, Landroid/server/search/Searchables;

    #@36
    .end local v2           #searchables:Landroid/server/search/Searchables;
    iget-object v4, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@38
    invoke-direct {v2, v4, p1}, Landroid/server/search/Searchables;-><init>(Landroid/content/Context;I)V

    #@3b
    .line 97
    .restart local v2       #searchables:Landroid/server/search/Searchables;
    invoke-virtual {v2}, Landroid/server/search/Searchables;->buildSearchableList()V

    #@3e
    .line 98
    iget-object v4, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@40
    invoke-virtual {v4, p1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@43
    .line 100
    :cond_43
    monitor-exit v5

    #@44
    goto :goto_1c

    #@45
    .line 101
    .end local v2           #searchables:Landroid/server/search/Searchables;
    :catchall_45
    move-exception v4

    #@46
    monitor-exit v5
    :try_end_47
    .catchall {:try_start_2a .. :try_end_47} :catchall_45

    #@47
    throw v4
.end method

.method private onUserRemoved(I)V
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 105
    if-eqz p1, :cond_b

    #@2
    .line 106
    iget-object v1, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@4
    monitor-enter v1

    #@5
    .line 107
    :try_start_5
    iget-object v0, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@7
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    #@a
    .line 108
    monitor-exit v1

    #@b
    .line 110
    :cond_b
    return-void

    #@c
    .line 108
    :catchall_c
    move-exception v0

    #@d
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_c

    #@e
    throw v0
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 9
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 287
    iget-object v2, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "android.permission.DUMP"

    #@4
    const-string v4, "SearchManagerService"

    #@6
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 289
    new-instance v1, Lcom/android/internal/util/IndentingPrintWriter;

    #@b
    const-string v2, "  "

    #@d
    invoke-direct {v1, p2, v2}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    #@10
    .line 290
    .local v1, ipw:Lcom/android/internal/util/IndentingPrintWriter;
    iget-object v3, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@12
    monitor-enter v3

    #@13
    .line 291
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    :try_start_14
    iget-object v2, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@16
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@19
    move-result v2

    #@1a
    if-ge v0, v2, :cond_3e

    #@1c
    .line 292
    const-string v2, "\nUser: "

    #@1e
    invoke-virtual {v1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;)V

    #@21
    iget-object v2, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@23
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Lcom/android/internal/util/IndentingPrintWriter;->println(I)V

    #@2a
    .line 293
    invoke-virtual {v1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    #@2d
    .line 294
    iget-object v2, p0, Landroid/server/search/SearchManagerService;->mSearchables:Landroid/util/SparseArray;

    #@2f
    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@32
    move-result-object v2

    #@33
    check-cast v2, Landroid/server/search/Searchables;

    #@35
    invoke-virtual {v2, p1, v1, p3}, Landroid/server/search/Searchables;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@38
    .line 295
    invoke-virtual {v1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    #@3b
    .line 291
    add-int/lit8 v0, v0, 0x1

    #@3d
    goto :goto_14

    #@3e
    .line 297
    :cond_3e
    monitor-exit v3

    #@3f
    .line 298
    return-void

    #@40
    .line 297
    :catchall_40
    move-exception v2

    #@41
    monitor-exit v3
    :try_end_42
    .catchall {:try_start_14 .. :try_end_42} :catchall_40

    #@42
    throw v2
.end method

.method public getAssistIntent(I)Landroid/content/ComponentName;
    .registers 15
    .parameter "userHandle"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 243
    :try_start_1
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@4
    move-result v9

    #@5
    if-eq p1, v9, :cond_29

    #@7
    .line 245
    const-string v9, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@9
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@c
    move-result v10

    #@d
    const/4 v11, -0x1

    #@e
    const/4 v12, 0x1

    #@f
    invoke-static {v9, v10, v11, v12}, Landroid/app/ActivityManager;->checkComponentPermission(Ljava/lang/String;IIZ)I

    #@12
    move-result v9

    #@13
    if-nez v9, :cond_57

    #@15
    .line 250
    const/4 v9, -0x2

    #@16
    if-ne p1, v9, :cond_29

    #@18
    .line 251
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v2

    #@1c
    .line 252
    .local v2, identity:J
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1f
    move-result-object v9

    #@20
    invoke-interface {v9}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    #@23
    move-result-object v9

    #@24
    iget p1, v9, Landroid/content/pm/UserInfo;->id:I

    #@26
    .line 253
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@29
    .line 265
    .end local v2           #identity:J
    :cond_29
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@2c
    move-result-object v6

    #@2d
    .line 266
    .local v6, pm:Landroid/content/pm/IPackageManager;
    new-instance v0, Landroid/content/Intent;

    #@2f
    const-string v9, "android.intent.action.ASSIST"

    #@31
    invoke-direct {v0, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@34
    .line 267
    .local v0, assistIntent:Landroid/content/Intent;
    iget-object v9, p0, Landroid/server/search/SearchManagerService;->mContext:Landroid/content/Context;

    #@36
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v0, v9}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@3d
    move-result-object v9

    #@3e
    const/high16 v10, 0x1

    #@40
    invoke-interface {v6, v0, v9, v10, p1}, Landroid/content/pm/IPackageManager;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;II)Landroid/content/pm/ResolveInfo;

    #@43
    move-result-object v4

    #@44
    .line 271
    .local v4, info:Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_56

    #@46
    .line 272
    new-instance v9, Landroid/content/ComponentName;

    #@48
    iget-object v10, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@4a
    iget-object v10, v10, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4c
    iget-object v10, v10, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@4e
    iget-object v11, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@50
    iget-object v11, v11, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@52
    invoke-direct {v9, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    move-object v8, v9

    #@56
    .line 282
    .end local v0           #assistIntent:Landroid/content/Intent;
    .end local v4           #info:Landroid/content/pm/ResolveInfo;
    .end local v6           #pm:Landroid/content/pm/IPackageManager;
    :cond_56
    :goto_56
    return-object v8

    #@57
    .line 256
    :cond_57
    new-instance v9, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v10, "Permission Denial: Request to getAssistIntent for "

    #@5e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v9

    #@62
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    const-string v10, " but is calling from user "

    #@68
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v9

    #@6c
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@6f
    move-result v10

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    const-string v10, "; this requires "

    #@76
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    const-string v10, "android.permission.INTERACT_ACROSS_USERS_FULL"

    #@7c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v9

    #@80
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    .line 261
    .local v5, msg:Ljava/lang/String;
    const-string v9, "SearchManagerService"

    #@86
    invoke-static {v9, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_89} :catch_8a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_89} :catch_a4

    #@89
    goto :goto_56

    #@8a
    .line 276
    .end local v5           #msg:Ljava/lang/String;
    :catch_8a
    move-exception v7

    #@8b
    .line 278
    .local v7, re:Landroid/os/RemoteException;
    const-string v9, "SearchManagerService"

    #@8d
    new-instance v10, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v11, "RemoteException in getAssistIntent: "

    #@94
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v10

    #@98
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v10

    #@9c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v10

    #@a0
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto :goto_56

    #@a4
    .line 279
    .end local v7           #re:Landroid/os/RemoteException;
    :catch_a4
    move-exception v1

    #@a5
    .line 280
    .local v1, e:Ljava/lang/Exception;
    const-string v9, "SearchManagerService"

    #@a7
    new-instance v10, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v11, "Exception in getAssistIntent: "

    #@ae
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v10

    #@b2
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v10

    #@b6
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v10

    #@ba
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    goto :goto_56
.end method

.method public getGlobalSearchActivities()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 223
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/server/search/Searchables;->getGlobalSearchActivities()Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getGlobalSearchActivity()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 230
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/server/search/Searchables;->getGlobalSearchActivity()Landroid/content/ComponentName;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
    .registers 4
    .parameter "launchActivity"

    #@0
    .prologue
    .line 208
    if-nez p1, :cond_b

    #@2
    .line 209
    const-string v0, "SearchManagerService"

    #@4
    const-string v1, "getSearchableInfo(), activity == null"

    #@6
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 210
    const/4 v0, 0x0

    #@a
    .line 212
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@e
    move-result v0

    #@f
    invoke-direct {p0, v0}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1}, Landroid/server/search/Searchables;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    #@16
    move-result-object v0

    #@17
    goto :goto_a
.end method

.method public getSearchablesInGlobalSearch()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 219
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/server/search/Searchables;->getSearchablesInGlobalSearchList()Ljava/util/ArrayList;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getWebSearchActivity()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 237
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    invoke-direct {p0, v0}, Landroid/server/search/SearchManagerService;->getSearchables(I)Landroid/server/search/Searchables;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Landroid/server/search/Searchables;->getWebSearchActivity()Landroid/content/ComponentName;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method
