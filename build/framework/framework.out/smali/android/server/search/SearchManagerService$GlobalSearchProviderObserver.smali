.class Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;
.super Landroid/database/ContentObserver;
.source "SearchManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/server/search/SearchManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GlobalSearchProviderObserver"
.end annotation


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Landroid/server/search/SearchManagerService;


# direct methods
.method public constructor <init>(Landroid/server/search/SearchManagerService;Landroid/content/ContentResolver;)V
    .registers 6
    .parameter
    .parameter "resolver"

    #@0
    .prologue
    .line 173
    iput-object p1, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@2
    .line 174
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@6
    .line 175
    iput-object p2, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->mResolver:Landroid/content/ContentResolver;

    #@8
    .line 176
    iget-object v0, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->mResolver:Landroid/content/ContentResolver;

    #@a
    const-string/jumbo v1, "search_global_search_activity"

    #@d
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@10
    move-result-object v1

    #@11
    const/4 v2, 0x0

    #@12
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@15
    .line 180
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 7
    .parameter "selfChange"

    #@0
    .prologue
    .line 184
    iget-object v2, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@2
    invoke-static {v2}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@5
    move-result-object v3

    #@6
    monitor-enter v3

    #@7
    .line 185
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    :try_start_8
    iget-object v2, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@a
    invoke-static {v2}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    #@11
    move-result v2

    #@12
    if-ge v0, v2, :cond_2a

    #@14
    .line 186
    iget-object v2, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@16
    iget-object v4, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@18
    invoke-static {v4}, Landroid/server/search/SearchManagerService;->access$500(Landroid/server/search/SearchManagerService;)Landroid/util/SparseArray;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    #@1f
    move-result v4

    #@20
    invoke-static {v2, v4}, Landroid/server/search/SearchManagerService;->access$300(Landroid/server/search/SearchManagerService;I)Landroid/server/search/Searchables;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Landroid/server/search/Searchables;->buildSearchableList()V

    #@27
    .line 185
    add-int/lit8 v0, v0, 0x1

    #@29
    goto :goto_8

    #@2a
    .line 188
    :cond_2a
    monitor-exit v3
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_43

    #@2b
    .line 189
    new-instance v1, Landroid/content/Intent;

    #@2d
    const-string v2, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    #@2f
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 190
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x2000

    #@34
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@37
    .line 191
    iget-object v2, p0, Landroid/server/search/SearchManagerService$GlobalSearchProviderObserver;->this$0:Landroid/server/search/SearchManagerService;

    #@39
    invoke-static {v2}, Landroid/server/search/SearchManagerService;->access$200(Landroid/server/search/SearchManagerService;)Landroid/content/Context;

    #@3c
    move-result-object v2

    #@3d
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@3f
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@42
    .line 192
    return-void

    #@43
    .line 188
    .end local v1           #intent:Landroid/content/Intent;
    :catchall_43
    move-exception v2

    #@44
    :try_start_44
    monitor-exit v3
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    #@45
    throw v2
.end method
