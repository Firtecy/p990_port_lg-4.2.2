.class final Landroid/server/search/Searchables$1;
.super Ljava/lang/Object;
.source "Searchables.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/server/search/Searchables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 332
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I
    .registers 7
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 335
    if-ne p1, p2, :cond_4

    #@2
    .line 336
    const/4 v2, 0x0

    #@3
    .line 351
    :goto_3
    return v2

    #@4
    .line 338
    :cond_4
    invoke-static {p1}, Landroid/server/search/Searchables;->access$000(Landroid/content/pm/ResolveInfo;)Z

    #@7
    move-result v0

    #@8
    .line 339
    .local v0, lhsSystem:Z
    invoke-static {p2}, Landroid/server/search/Searchables;->access$000(Landroid/content/pm/ResolveInfo;)Z

    #@b
    move-result v1

    #@c
    .line 341
    .local v1, rhsSystem:Z
    if-eqz v0, :cond_12

    #@e
    if-nez v1, :cond_12

    #@10
    .line 342
    const/4 v2, -0x1

    #@11
    goto :goto_3

    #@12
    .line 343
    :cond_12
    if-eqz v1, :cond_18

    #@14
    if-nez v0, :cond_18

    #@16
    .line 344
    const/4 v2, 0x1

    #@17
    goto :goto_3

    #@18
    .line 351
    :cond_18
    iget v2, p2, Landroid/content/pm/ResolveInfo;->priority:I

    #@1a
    iget v3, p1, Landroid/content/pm/ResolveInfo;->priority:I

    #@1c
    sub-int/2addr v2, v3

    #@1d
    goto :goto_3
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 332
    check-cast p1, Landroid/content/pm/ResolveInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/content/pm/ResolveInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/server/search/Searchables$1;->compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
