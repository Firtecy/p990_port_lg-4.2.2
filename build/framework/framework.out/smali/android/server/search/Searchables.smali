.class public Landroid/server/search/Searchables;
.super Ljava/lang/Object;
.source "Searchables.java"


# static fields
.field public static ENHANCED_GOOGLE_SEARCH_COMPONENT_NAME:Ljava/lang/String; = null

.field private static final GLOBAL_SEARCH_RANKER:Ljava/util/Comparator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static GOOGLE_SEARCH_COMPONENT_NAME:Ljava/lang/String; = null

.field private static final LOG_TAG:Ljava/lang/String; = "Searchables"

.field private static final MD_LABEL_DEFAULT_SEARCHABLE:Ljava/lang/String; = "android.app.default_searchable"

.field private static final MD_SEARCHABLE_SYSTEM_SEARCH:Ljava/lang/String; = "*"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentGlobalSearchActivity:Landroid/content/ComponentName;

.field private mGlobalSearchActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPm:Landroid/content/pm/IPackageManager;

.field private mSearchablesInGlobalSearchList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchablesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchablesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUserId:I

.field private mWebSearchActivity:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 70
    const-string v0, "com.android.googlesearch/.GoogleSearch"

    #@2
    sput-object v0, Landroid/server/search/Searchables;->GOOGLE_SEARCH_COMPONENT_NAME:Ljava/lang/String;

    #@4
    .line 72
    const-string v0, "com.google.android.providers.enhancedgooglesearch/.Launcher"

    #@6
    sput-object v0, Landroid/server/search/Searchables;->ENHANCED_GOOGLE_SEARCH_COMPONENT_NAME:Ljava/lang/String;

    #@8
    .line 331
    new-instance v0, Landroid/server/search/Searchables$1;

    #@a
    invoke-direct {v0}, Landroid/server/search/Searchables$1;-><init>()V

    #@d
    sput-object v0, Landroid/server/search/Searchables;->GLOBAL_SEARCH_RANKER:Ljava/util/Comparator;

    #@f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "userId"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 61
    iput-object v0, p0, Landroid/server/search/Searchables;->mSearchablesMap:Ljava/util/HashMap;

    #@6
    .line 62
    iput-object v0, p0, Landroid/server/search/Searchables;->mSearchablesList:Ljava/util/ArrayList;

    #@8
    .line 63
    iput-object v0, p0, Landroid/server/search/Searchables;->mSearchablesInGlobalSearchList:Ljava/util/ArrayList;

    #@a
    .line 67
    iput-object v0, p0, Landroid/server/search/Searchables;->mCurrentGlobalSearchActivity:Landroid/content/ComponentName;

    #@c
    .line 68
    iput-object v0, p0, Landroid/server/search/Searchables;->mWebSearchActivity:Landroid/content/ComponentName;

    #@e
    .line 85
    iput-object p1, p0, Landroid/server/search/Searchables;->mContext:Landroid/content/Context;

    #@10
    .line 86
    iput p2, p0, Landroid/server/search/Searchables;->mUserId:I

    #@12
    .line 87
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Landroid/server/search/Searchables;->mPm:Landroid/content/pm/IPackageManager;

    #@18
    .line 88
    return-void
.end method

.method static synthetic access$000(Landroid/content/pm/ResolveInfo;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    invoke-static {p0}, Landroid/server/search/Searchables;->isSystemApp(Landroid/content/pm/ResolveInfo;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private findGlobalSearchActivities()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 284
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "android.search.action.GLOBAL_SEARCH"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 285
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x1

    #@9
    invoke-direct {p0, v1, v2}, Landroid/server/search/Searchables;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@c
    move-result-object v0

    #@d
    .line 287
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_1a

    #@f
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_1a

    #@15
    .line 289
    sget-object v2, Landroid/server/search/Searchables;->GLOBAL_SEARCH_RANKER:Ljava/util/Comparator;

    #@17
    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@1a
    .line 292
    :cond_1a
    return-object v0
.end method

.method private findGlobalSearchActivity(Ljava/util/List;)Landroid/content/ComponentName;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    #@0
    .prologue
    .line 301
    .local p1, installed:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0}, Landroid/server/search/Searchables;->getGlobalSearchProviderSetting()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 302
    .local v1, searchProviderSetting:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_17

    #@a
    .line 303
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@d
    move-result-object v0

    #@e
    .line 305
    .local v0, globalSearchComponent:Landroid/content/ComponentName;
    if-eqz v0, :cond_17

    #@10
    invoke-direct {p0, v0}, Landroid/server/search/Searchables;->isInstalled(Landroid/content/ComponentName;)Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_17

    #@16
    .line 310
    .end local v0           #globalSearchComponent:Landroid/content/ComponentName;
    :goto_16
    return-object v0

    #@17
    :cond_17
    invoke-direct {p0, p1}, Landroid/server/search/Searchables;->getDefaultGlobalSearchProvider(Ljava/util/List;)Landroid/content/ComponentName;

    #@1a
    move-result-object v0

    #@1b
    goto :goto_16
.end method

.method private findWebSearchActivity(Landroid/content/ComponentName;)Landroid/content/ComponentName;
    .registers 8
    .parameter "globalSearchActivity"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 388
    if-nez p1, :cond_4

    #@3
    .line 402
    :goto_3
    return-object v3

    #@4
    .line 391
    :cond_4
    new-instance v2, Landroid/content/Intent;

    #@6
    const-string v4, "android.intent.action.WEB_SEARCH"

    #@8
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 392
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 393
    const/high16 v4, 0x1

    #@14
    invoke-direct {p0, v2, v4}, Landroid/server/search/Searchables;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@17
    move-result-object v0

    #@18
    .line 396
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_33

    #@1a
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@1d
    move-result v4

    #@1e
    if-nez v4, :cond_33

    #@20
    .line 397
    const/4 v3, 0x0

    #@21
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v3

    #@25
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@27
    iget-object v1, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@29
    .line 399
    .local v1, ai:Landroid/content/pm/ActivityInfo;
    new-instance v3, Landroid/content/ComponentName;

    #@2b
    iget-object v4, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@2d
    iget-object v5, v1, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@2f
    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    goto :goto_3

    #@33
    .line 401
    .end local v1           #ai:Landroid/content/pm/ActivityInfo;
    :cond_33
    const-string v4, "Searchables"

    #@35
    const-string v5, "No web search activity found"

    #@37
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_3
.end method

.method private getDefaultGlobalSearchProvider(Ljava/util/List;)Landroid/content/ComponentName;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Landroid/content/ComponentName;"
        }
    .end annotation

    #@0
    .prologue
    .line 368
    .local p1, providerList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p1, :cond_1b

    #@2
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_1b

    #@8
    .line 369
    const/4 v1, 0x0

    #@9
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, Landroid/content/pm/ResolveInfo;

    #@f
    iget-object v0, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@11
    .line 370
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    new-instance v1, Landroid/content/ComponentName;

    #@13
    iget-object v2, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@15
    iget-object v3, v0, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@17
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 374
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    :goto_1a
    return-object v1

    #@1b
    .line 373
    :cond_1b
    const-string v1, "Searchables"

    #@1d
    const-string v2, "No global search activity found"

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 374
    const/4 v1, 0x0

    #@23
    goto :goto_1a
.end method

.method private getGlobalSearchProviderSetting()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 378
    iget-object v0, p0, Landroid/server/search/Searchables;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const-string/jumbo v1, "search_global_search_activity"

    #@9
    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method private isInstalled(Landroid/content/ComponentName;)Z
    .registers 5
    .parameter "globalSearch"

    #@0
    .prologue
    .line 319
    new-instance v1, Landroid/content/Intent;

    #@2
    const-string v2, "android.search.action.GLOBAL_SEARCH"

    #@4
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 320
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@a
    .line 322
    const/high16 v2, 0x1

    #@c
    invoke-direct {p0, v1, v2}, Landroid/server/search/Searchables;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@f
    move-result-object v0

    #@10
    .line 324
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_1a

    #@12
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_1a

    #@18
    .line 325
    const/4 v2, 0x1

    #@19
    .line 328
    :goto_19
    return v2

    #@1a
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_19
.end method

.method private static final isSystemApp(Landroid/content/pm/ResolveInfo;)Z
    .registers 2
    .parameter "res"

    #@0
    .prologue
    .line 360
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@2
    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@4
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@6
    and-int/lit8 v0, v0, 0x1

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
    .registers 7
    .parameter "intent"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 406
    const/4 v0, 0x0

    #@1
    .line 408
    .local v0, activities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :try_start_1
    iget-object v1, p0, Landroid/server/search/Searchables;->mPm:Landroid/content/pm/IPackageManager;

    #@3
    iget-object v2, p0, Landroid/server/search/Searchables;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v2

    #@9
    invoke-virtual {p1, v2}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    iget v3, p0, Landroid/server/search/Searchables;->mUserId:I

    #@f
    invoke-interface {v1, p1, v2, p2, v3}, Landroid/content/pm/IPackageManager;->queryIntentActivities(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_12} :catch_14

    #@12
    move-result-object v0

    #@13
    .line 415
    :goto_13
    return-object v0

    #@14
    .line 412
    :catch_14
    move-exception v1

    #@15
    goto :goto_13
.end method


# virtual methods
.method public buildSearchableList()V
    .registers 26

    #@0
    .prologue
    .line 205
    new-instance v14, Ljava/util/HashMap;

    #@2
    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 207
    .local v14, newSearchablesMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Landroid/content/ComponentName;Landroid/app/SearchableInfo;>;"
    new-instance v13, Ljava/util/ArrayList;

    #@7
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    #@a
    .line 209
    .local v13, newSearchablesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    new-instance v12, Ljava/util/ArrayList;

    #@c
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 214
    .local v12, newSearchablesInGlobalSearchList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    new-instance v9, Landroid/content/Intent;

    #@11
    const-string v22, "android.intent.action.SEARCH"

    #@13
    move-object/from16 v0, v22

    #@15
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@18
    .line 216
    .local v9, intent:Landroid/content/Intent;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@1b
    move-result-wide v5

    #@1c
    .line 218
    .local v5, ident:J
    const/16 v22, 0x80

    #@1e
    :try_start_1e
    move-object/from16 v0, p0

    #@20
    move/from16 v1, v22

    #@22
    invoke-direct {v0, v9, v1}, Landroid/server/search/Searchables;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@25
    move-result-object v16

    #@26
    .line 221
    .local v16, searchList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v20, Landroid/content/Intent;

    #@28
    const-string v22, "android.intent.action.WEB_SEARCH"

    #@2a
    move-object/from16 v0, v20

    #@2c
    move-object/from16 v1, v22

    #@2e
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@31
    .line 222
    .local v20, webSearchIntent:Landroid/content/Intent;
    const/16 v22, 0x80

    #@33
    move-object/from16 v0, p0

    #@35
    move-object/from16 v1, v20

    #@37
    move/from16 v2, v22

    #@39
    invoke-direct {v0, v1, v2}, Landroid/server/search/Searchables;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@3c
    move-result-object v19

    #@3d
    .line 225
    .local v19, webSearchInfoList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v16, :cond_41

    #@3f
    if-eqz v19, :cond_c0

    #@41
    .line 226
    :cond_41
    if-nez v16, :cond_a7

    #@43
    const/16 v17, 0x0

    #@45
    .line 227
    .local v17, search_count:I
    :goto_45
    if-nez v19, :cond_ac

    #@47
    const/16 v21, 0x0

    #@49
    .line 228
    .local v21, web_search_count:I
    :goto_49
    add-int v4, v17, v21

    #@4b
    .line 229
    .local v4, count:I
    const/4 v7, 0x0

    #@4c
    .local v7, ii:I
    :goto_4c
    if-ge v7, v4, :cond_c0

    #@4e
    .line 231
    move/from16 v0, v17

    #@50
    if-ge v7, v0, :cond_b1

    #@52
    move-object/from16 v0, v16

    #@54
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@57
    move-result-object v22

    #@58
    check-cast v22, Landroid/content/pm/ResolveInfo;

    #@5a
    move-object/from16 v8, v22

    #@5c
    .line 234
    .local v8, info:Landroid/content/pm/ResolveInfo;
    :goto_5c
    iget-object v3, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    #@5e
    .line 236
    .local v3, ai:Landroid/content/pm/ActivityInfo;
    new-instance v22, Landroid/content/ComponentName;

    #@60
    iget-object v0, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@62
    move-object/from16 v23, v0

    #@64
    iget-object v0, v3, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@66
    move-object/from16 v24, v0

    #@68
    invoke-direct/range {v22 .. v24}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    move-object/from16 v0, v22

    #@6d
    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@70
    move-result-object v22

    #@71
    if-nez v22, :cond_a4

    #@73
    .line 237
    move-object/from16 v0, p0

    #@75
    iget-object v0, v0, Landroid/server/search/Searchables;->mContext:Landroid/content/Context;

    #@77
    move-object/from16 v22, v0

    #@79
    move-object/from16 v0, p0

    #@7b
    iget v0, v0, Landroid/server/search/Searchables;->mUserId:I

    #@7d
    move/from16 v23, v0

    #@7f
    move-object/from16 v0, v22

    #@81
    move/from16 v1, v23

    #@83
    invoke-static {v0, v3, v1}, Landroid/app/SearchableInfo;->getActivityMetaData(Landroid/content/Context;Landroid/content/pm/ActivityInfo;I)Landroid/app/SearchableInfo;

    #@86
    move-result-object v18

    #@87
    .line 239
    .local v18, searchable:Landroid/app/SearchableInfo;
    if-eqz v18, :cond_a4

    #@89
    .line 240
    move-object/from16 v0, v18

    #@8b
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@8e
    .line 241
    invoke-virtual/range {v18 .. v18}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    #@91
    move-result-object v22

    #@92
    move-object/from16 v0, v22

    #@94
    move-object/from16 v1, v18

    #@96
    invoke-virtual {v14, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@99
    .line 242
    invoke-virtual/range {v18 .. v18}, Landroid/app/SearchableInfo;->shouldIncludeInGlobalSearch()Z

    #@9c
    move-result v22

    #@9d
    if-eqz v22, :cond_a4

    #@9f
    .line 243
    move-object/from16 v0, v18

    #@a1
    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a4
    .line 229
    .end local v18           #searchable:Landroid/app/SearchableInfo;
    :cond_a4
    add-int/lit8 v7, v7, 0x1

    #@a6
    goto :goto_4c

    #@a7
    .line 226
    .end local v3           #ai:Landroid/content/pm/ActivityInfo;
    .end local v4           #count:I
    .end local v7           #ii:I
    .end local v8           #info:Landroid/content/pm/ResolveInfo;
    .end local v17           #search_count:I
    .end local v21           #web_search_count:I
    :cond_a7
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    #@aa
    move-result v17

    #@ab
    goto :goto_45

    #@ac
    .line 227
    .restart local v17       #search_count:I
    :cond_ac
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    #@af
    move-result v21

    #@b0
    goto :goto_49

    #@b1
    .line 231
    .restart local v4       #count:I
    .restart local v7       #ii:I
    .restart local v21       #web_search_count:I
    :cond_b1
    sub-int v22, v7, v17

    #@b3
    move-object/from16 v0, v19

    #@b5
    move/from16 v1, v22

    #@b7
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@ba
    move-result-object v22

    #@bb
    check-cast v22, Landroid/content/pm/ResolveInfo;

    #@bd
    move-object/from16 v8, v22

    #@bf
    goto :goto_5c

    #@c0
    .line 250
    .end local v4           #count:I
    .end local v7           #ii:I
    .end local v17           #search_count:I
    .end local v21           #web_search_count:I
    :cond_c0
    invoke-direct/range {p0 .. p0}, Landroid/server/search/Searchables;->findGlobalSearchActivities()Ljava/util/List;

    #@c3
    move-result-object v10

    #@c4
    .line 253
    .local v10, newGlobalSearchActivities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object/from16 v0, p0

    #@c6
    invoke-direct {v0, v10}, Landroid/server/search/Searchables;->findGlobalSearchActivity(Ljava/util/List;)Landroid/content/ComponentName;

    #@c9
    move-result-object v11

    #@ca
    .line 257
    .local v11, newGlobalSearchActivity:Landroid/content/ComponentName;
    move-object/from16 v0, p0

    #@cc
    invoke-direct {v0, v11}, Landroid/server/search/Searchables;->findWebSearchActivity(Landroid/content/ComponentName;)Landroid/content/ComponentName;

    #@cf
    move-result-object v15

    #@d0
    .line 260
    .local v15, newWebSearchActivity:Landroid/content/ComponentName;
    monitor-enter p0
    :try_end_d1
    .catchall {:try_start_1e .. :try_end_d1} :catchall_f1

    #@d1
    .line 261
    :try_start_d1
    move-object/from16 v0, p0

    #@d3
    iput-object v14, v0, Landroid/server/search/Searchables;->mSearchablesMap:Ljava/util/HashMap;

    #@d5
    .line 262
    move-object/from16 v0, p0

    #@d7
    iput-object v13, v0, Landroid/server/search/Searchables;->mSearchablesList:Ljava/util/ArrayList;

    #@d9
    .line 263
    move-object/from16 v0, p0

    #@db
    iput-object v12, v0, Landroid/server/search/Searchables;->mSearchablesInGlobalSearchList:Ljava/util/ArrayList;

    #@dd
    .line 264
    move-object/from16 v0, p0

    #@df
    iput-object v10, v0, Landroid/server/search/Searchables;->mGlobalSearchActivities:Ljava/util/List;

    #@e1
    .line 265
    move-object/from16 v0, p0

    #@e3
    iput-object v11, v0, Landroid/server/search/Searchables;->mCurrentGlobalSearchActivity:Landroid/content/ComponentName;

    #@e5
    .line 266
    move-object/from16 v0, p0

    #@e7
    iput-object v15, v0, Landroid/server/search/Searchables;->mWebSearchActivity:Landroid/content/ComponentName;

    #@e9
    .line 267
    monitor-exit p0
    :try_end_ea
    .catchall {:try_start_d1 .. :try_end_ea} :catchall_ee

    #@ea
    .line 269
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@ed
    .line 271
    return-void

    #@ee
    .line 267
    :catchall_ee
    move-exception v22

    #@ef
    :try_start_ef
    monitor-exit p0
    :try_end_f0
    .catchall {:try_start_ef .. :try_end_f0} :catchall_ee

    #@f0
    :try_start_f0
    throw v22
    :try_end_f1
    .catchall {:try_start_f0 .. :try_end_f1} :catchall_f1

    #@f1
    .line 269
    .end local v10           #newGlobalSearchActivities:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v11           #newGlobalSearchActivity:Landroid/content/ComponentName;
    .end local v15           #newWebSearchActivity:Landroid/content/ComponentName;
    .end local v16           #searchList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v19           #webSearchInfoList:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v20           #webSearchIntent:Landroid/content/Intent;
    :catchall_f1
    move-exception v22

    #@f2
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@f5
    throw v22
.end method

.method dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 455
    const-string v2, "Searchable authorities:"

    #@2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5
    .line 456
    monitor-enter p0

    #@6
    .line 457
    :try_start_6
    iget-object v2, p0, Landroid/server/search/Searchables;->mSearchablesList:Ljava/util/ArrayList;

    #@8
    if-eqz v2, :cond_2c

    #@a
    .line 458
    iget-object v2, p0, Landroid/server/search/Searchables;->mSearchablesList:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v0

    #@10
    .local v0, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_2c

    #@16
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Landroid/app/SearchableInfo;

    #@1c
    .line 459
    .local v1, info:Landroid/app/SearchableInfo;
    const-string v2, "  "

    #@1e
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@28
    goto :goto_10

    #@29
    .line 462
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #info:Landroid/app/SearchableInfo;
    :catchall_29
    move-exception v2

    #@2a
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    #@2b
    throw v2

    #@2c
    :cond_2c
    :try_start_2c
    monitor-exit p0
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_29

    #@2d
    .line 463
    return-void
.end method

.method public declared-synchronized getGlobalSearchActivities()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 437
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    #@3
    iget-object v1, p0, Landroid/server/search/Searchables;->mGlobalSearchActivities:Ljava/util/List;

    #@5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    monitor-exit p0

    #@9
    return-object v0

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized getGlobalSearchActivity()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 444
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/server/search/Searchables;->mCurrentGlobalSearchActivity:Landroid/content/ComponentName;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;
    .registers 14
    .parameter "activity"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 120
    monitor-enter p0

    #@2
    .line 121
    :try_start_2
    iget-object v8, p0, Landroid/server/search/Searchables;->mSearchablesMap:Ljava/util/HashMap;

    #@4
    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    move-result-object v7

    #@8
    check-cast v7, Landroid/app/SearchableInfo;

    #@a
    .line 122
    .local v7, result:Landroid/app/SearchableInfo;
    if-eqz v7, :cond_f

    #@c
    monitor-exit p0

    #@d
    move-object v8, v7

    #@e
    .line 179
    :goto_e
    return-object v8

    #@f
    .line 123
    :cond_f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_2 .. :try_end_10} :catchall_40

    #@10
    .line 130
    const/4 v1, 0x0

    #@11
    .line 132
    .local v1, ai:Landroid/content/pm/ActivityInfo;
    :try_start_11
    iget-object v8, p0, Landroid/server/search/Searchables;->mPm:Landroid/content/pm/IPackageManager;

    #@13
    const/16 v10, 0x80

    #@15
    iget v11, p0, Landroid/server/search/Searchables;->mUserId:I

    #@17
    invoke-interface {v8, p1, v10, v11}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_1a} :catch_43

    #@1a
    move-result-object v1

    #@1b
    .line 137
    const/4 v5, 0x0

    #@1c
    .line 140
    .local v5, refActivityName:Ljava/lang/String;
    iget-object v2, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@1e
    .line 141
    .local v2, md:Landroid/os/Bundle;
    if-eqz v2, :cond_26

    #@20
    .line 142
    const-string v8, "android.app.default_searchable"

    #@22
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 145
    :cond_26
    if-nez v5, :cond_34

    #@28
    .line 146
    iget-object v8, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2a
    iget-object v2, v8, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    #@2c
    .line 147
    if-eqz v2, :cond_34

    #@2e
    .line 148
    const-string v8, "android.app.default_searchable"

    #@30
    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v5

    #@34
    .line 153
    :cond_34
    if-eqz v5, :cond_9e

    #@36
    .line 156
    const-string v8, "*"

    #@38
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v8

    #@3c
    if-eqz v8, :cond_5e

    #@3e
    move-object v8, v9

    #@3f
    .line 157
    goto :goto_e

    #@40
    .line 123
    .end local v1           #ai:Landroid/content/pm/ActivityInfo;
    .end local v2           #md:Landroid/os/Bundle;
    .end local v5           #refActivityName:Ljava/lang/String;
    .end local v7           #result:Landroid/app/SearchableInfo;
    :catchall_40
    move-exception v8

    #@41
    :try_start_41
    monitor-exit p0
    :try_end_42
    .catchall {:try_start_41 .. :try_end_42} :catchall_40

    #@42
    throw v8

    #@43
    .line 133
    .restart local v1       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v7       #result:Landroid/app/SearchableInfo;
    :catch_43
    move-exception v4

    #@44
    .line 134
    .local v4, re:Landroid/os/RemoteException;
    const-string v8, "Searchables"

    #@46
    new-instance v10, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v11, "Error getting activity info "

    #@4d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v10

    #@51
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v10

    #@55
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v10

    #@59
    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    move-object v8, v9

    #@5d
    .line 135
    goto :goto_e

    #@5e
    .line 159
    .end local v4           #re:Landroid/os/RemoteException;
    .restart local v2       #md:Landroid/os/Bundle;
    .restart local v5       #refActivityName:Ljava/lang/String;
    :cond_5e
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    .line 161
    .local v3, pkg:Ljava/lang/String;
    const/4 v8, 0x0

    #@63
    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    #@66
    move-result v8

    #@67
    const/16 v10, 0x2e

    #@69
    if-ne v8, v10, :cond_97

    #@6b
    .line 162
    new-instance v6, Landroid/content/ComponentName;

    #@6d
    new-instance v8, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-direct {v6, v3, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    .line 169
    .local v6, referredActivity:Landroid/content/ComponentName;
    :goto_81
    monitor-enter p0

    #@82
    .line 170
    :try_start_82
    iget-object v8, p0, Landroid/server/search/Searchables;->mSearchablesMap:Ljava/util/HashMap;

    #@84
    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@87
    move-result-object v8

    #@88
    move-object v0, v8

    #@89
    check-cast v0, Landroid/app/SearchableInfo;

    #@8b
    move-object v7, v0

    #@8c
    .line 171
    if-eqz v7, :cond_9d

    #@8e
    .line 172
    iget-object v8, p0, Landroid/server/search/Searchables;->mSearchablesMap:Ljava/util/HashMap;

    #@90
    invoke-virtual {v8, p1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@93
    .line 173
    monitor-exit p0
    :try_end_94
    .catchall {:try_start_82 .. :try_end_94} :catchall_a1

    #@94
    move-object v8, v7

    #@95
    goto/16 :goto_e

    #@97
    .line 164
    .end local v6           #referredActivity:Landroid/content/ComponentName;
    :cond_97
    new-instance v6, Landroid/content/ComponentName;

    #@99
    invoke-direct {v6, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .restart local v6       #referredActivity:Landroid/content/ComponentName;
    goto :goto_81

    #@9d
    .line 175
    :cond_9d
    :try_start_9d
    monitor-exit p0

    #@9e
    .end local v3           #pkg:Ljava/lang/String;
    .end local v6           #referredActivity:Landroid/content/ComponentName;
    :cond_9e
    move-object v8, v9

    #@9f
    .line 179
    goto/16 :goto_e

    #@a1
    .line 175
    .restart local v3       #pkg:Ljava/lang/String;
    .restart local v6       #referredActivity:Landroid/content/ComponentName;
    :catchall_a1
    move-exception v8

    #@a2
    monitor-exit p0
    :try_end_a3
    .catchall {:try_start_9d .. :try_end_a3} :catchall_a1

    #@a3
    throw v8
.end method

.method public declared-synchronized getSearchablesInGlobalSearchList()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 430
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    #@3
    iget-object v1, p0, Landroid/server/search/Searchables;->mSearchablesInGlobalSearchList:Ljava/util/ArrayList;

    #@5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    monitor-exit p0

    #@9
    return-object v0

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public declared-synchronized getSearchablesList()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/SearchableInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 422
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    #@3
    iget-object v1, p0, Landroid/server/search/Searchables;->mSearchablesList:Ljava/util/ArrayList;

    #@5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 423
    .local v0, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    monitor-exit p0

    #@9
    return-object v0

    #@a
    .line 422
    .end local v0           #result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/SearchableInfo;>;"
    :catchall_a
    move-exception v1

    #@b
    monitor-exit p0

    #@c
    throw v1
.end method

.method public declared-synchronized getWebSearchActivity()Landroid/content/ComponentName;
    .registers 2

    #@0
    .prologue
    .line 451
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/server/search/Searchables;->mWebSearchActivity:Landroid/content/ComponentName;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method
