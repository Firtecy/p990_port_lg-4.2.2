.class public Landroid/icu/text/ArabicShaping;
.super Ljava/lang/Object;
.source "ArabicShaping.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/icu/text/ArabicShaping$ArabicShapingException;
    }
.end annotation


# static fields
.field private static final ALEFTYPE:I = 0x20

.field private static final DESHAPE_MODE:I = 0x1

.field public static final DIGITS_AN2EN:I = 0x40

.field public static final DIGITS_EN2AN:I = 0x20

.field public static final DIGITS_EN2AN_INIT_AL:I = 0x80

.field public static final DIGITS_EN2AN_INIT_LR:I = 0x60

.field public static final DIGITS_MASK:I = 0xe0

.field public static final DIGITS_NOOP:I = 0x0

.field public static final DIGIT_TYPE_AN:I = 0x0

.field public static final DIGIT_TYPE_AN_EXTENDED:I = 0x100

.field public static final DIGIT_TYPE_MASK:I = 0x100

.field private static final HAMZA06_CHAR:C = '\u0621'

.field private static final HAMZAFE_CHAR:C = '\ufe80'

.field private static final IRRELEVANT:I = 0x4

.field public static final LAMALEF_AUTO:I = 0x10000

.field public static final LAMALEF_BEGIN:I = 0x3

.field public static final LAMALEF_END:I = 0x2

.field public static final LAMALEF_MASK:I = 0x10003

.field public static final LAMALEF_NEAR:I = 0x1

.field public static final LAMALEF_RESIZE:I = 0x0

.field private static final LAMALEF_SPACE_SUB:C = '\uffff'

.field private static final LAMTYPE:I = 0x10

.field private static final LAM_CHAR:C = '\u0644'

.field public static final LENGTH_FIXED_SPACES_AT_BEGINNING:I = 0x3

.field public static final LENGTH_FIXED_SPACES_AT_END:I = 0x2

.field public static final LENGTH_FIXED_SPACES_NEAR:I = 0x1

.field public static final LENGTH_GROW_SHRINK:I = 0x0

.field public static final LENGTH_MASK:I = 0x10003

.field public static final LETTERS_MASK:I = 0x18

.field public static final LETTERS_NOOP:I = 0x0

.field public static final LETTERS_SHAPE:I = 0x8

.field public static final LETTERS_SHAPE_TASHKEEL_ISOLATED:I = 0x18

.field public static final LETTERS_UNSHAPE:I = 0x10

.field private static final LINKL:I = 0x2

.field private static final LINKR:I = 0x1

.field private static final LINK_MASK:I = 0x3

.field private static final NEW_TAIL_CHAR:C = '\ufe73'

.field private static final OLD_TAIL_CHAR:C = '\u200b'

.field public static final SEEN_MASK:I = 0x700000

.field public static final SEEN_TWOCELL_NEAR:I = 0x200000

.field private static final SHADDA_CHAR:C = '\ufe7c'

.field private static final SHADDA_TATWEEL_CHAR:C = '\ufe7d'

.field public static final SHAPER:Landroid/icu/text/ArabicShaping; = null

.field private static final SHAPE_MODE:I = 0x0

.field public static final SHAPE_TAIL_NEW_UNICODE:I = 0x8000000

.field public static final SHAPE_TAIL_TYPE_MASK:I = 0x8000000

.field public static final SPACES_RELATIVE_TO_TEXT_BEGIN_END:I = 0x4000000

.field public static final SPACES_RELATIVE_TO_TEXT_MASK:I = 0x4000000

.field private static final SPACE_CHAR:C = ' '

.field private static final SPACE_CHAR_FOR_LAMALEF:C = '\ufeff'

.field public static final TASHKEEL_BEGIN:I = 0x40000

.field public static final TASHKEEL_END:I = 0x60000

.field public static final TASHKEEL_MASK:I = 0xe0000

.field public static final TASHKEEL_REPLACE_BY_TATWEEL:I = 0xc0000

.field public static final TASHKEEL_RESIZE:I = 0x80000

.field private static final TASHKEEL_SPACE_SUB:C = '\ufffe'

.field private static final TATWEEL_CHAR:C = '\u0640'

.field public static final TEXT_DIRECTION_LOGICAL:I = 0x0

.field public static final TEXT_DIRECTION_MASK:I = 0x4

.field public static final TEXT_DIRECTION_VISUAL_LTR:I = 0x4

.field public static final TEXT_DIRECTION_VISUAL_RTL:I = 0x0

.field public static final YEHHAMZA_MASK:I = 0x3800000

.field public static final YEHHAMZA_TWOCELL_NEAR:I = 0x1000000

.field private static final YEH_HAMZAFE_CHAR:C = '\ufe89'

.field private static final YEH_HAMZA_CHAR:C = '\u0626'

.field private static final araLink:[I

.field private static convertFEto06:[I

.field private static final convertNormalizedLamAlef:[C

.field private static final irrelevantPos:[I

.field private static final presLink:[I

.field private static final shapeTable:[[[I

.field private static final tailFamilyIsolatedFinal:[I

.field private static final tashkeelMedial:[I

.field private static final yehHamzaToYeh:[C


# instance fields
.field private isLogical:Z

.field private final options:I

.field private spacesRelativeToTextBeginEnd:Z

.field private tailChar:C


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x4

    #@5
    .line 66
    new-instance v0, Landroid/icu/text/ArabicShaping;

    #@7
    const/16 v1, 0x9

    #@9
    invoke-direct {v0, v1}, Landroid/icu/text/ArabicShaping;-><init>(I)V

    #@c
    sput-object v0, Landroid/icu/text/ArabicShaping;->SHAPER:Landroid/icu/text/ArabicShaping;

    #@e
    .line 648
    const/16 v0, 0x8

    #@10
    new-array v0, v0, [I

    #@12
    fill-array-data v0, :array_d8

    #@15
    sput-object v0, Landroid/icu/text/ArabicShaping;->irrelevantPos:[I

    #@17
    .line 665
    const/16 v0, 0xe

    #@19
    new-array v0, v0, [I

    #@1b
    fill-array-data v0, :array_ec

    #@1e
    sput-object v0, Landroid/icu/text/ArabicShaping;->tailFamilyIsolatedFinal:[I

    #@20
    .line 682
    const/16 v0, 0x10

    #@22
    new-array v0, v0, [I

    #@24
    fill-array-data v0, :array_10c

    #@27
    sput-object v0, Landroid/icu/text/ArabicShaping;->tashkeelMedial:[I

    #@29
    .line 701
    new-array v0, v4, [C

    #@2b
    fill-array-data v0, :array_130

    #@2e
    sput-object v0, Landroid/icu/text/ArabicShaping;->yehHamzaToYeh:[C

    #@30
    .line 707
    new-array v0, v3, [C

    #@32
    fill-array-data v0, :array_136

    #@35
    sput-object v0, Landroid/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    #@37
    .line 714
    const/16 v0, 0xb2

    #@39
    new-array v0, v0, [I

    #@3b
    fill-array-data v0, :array_13e

    #@3e
    sput-object v0, Landroid/icu/text/ArabicShaping;->araLink:[I

    #@40
    .line 792
    const/16 v0, 0x8d

    #@42
    new-array v0, v0, [I

    #@44
    fill-array-data v0, :array_2a6

    #@47
    sput-object v0, Landroid/icu/text/ArabicShaping;->presLink:[I

    #@49
    .line 833
    const/16 v0, 0x8d

    #@4b
    new-array v0, v0, [I

    #@4d
    fill-array-data v0, :array_3c4

    #@50
    sput-object v0, Landroid/icu/text/ArabicShaping;->convertFEto06:[I

    #@52
    .line 846
    new-array v0, v3, [[[I

    #@54
    new-array v1, v3, [[I

    #@56
    new-array v2, v3, [I

    #@58
    fill-array-data v2, :array_4e2

    #@5b
    aput-object v2, v1, v5

    #@5d
    new-array v2, v3, [I

    #@5f
    fill-array-data v2, :array_4ee

    #@62
    aput-object v2, v1, v6

    #@64
    new-array v2, v3, [I

    #@66
    fill-array-data v2, :array_4fa

    #@69
    aput-object v2, v1, v4

    #@6b
    new-array v2, v3, [I

    #@6d
    fill-array-data v2, :array_506

    #@70
    aput-object v2, v1, v7

    #@72
    aput-object v1, v0, v5

    #@74
    new-array v1, v3, [[I

    #@76
    new-array v2, v3, [I

    #@78
    fill-array-data v2, :array_512

    #@7b
    aput-object v2, v1, v5

    #@7d
    new-array v2, v3, [I

    #@7f
    fill-array-data v2, :array_51e

    #@82
    aput-object v2, v1, v6

    #@84
    new-array v2, v3, [I

    #@86
    fill-array-data v2, :array_52a

    #@89
    aput-object v2, v1, v4

    #@8b
    new-array v2, v3, [I

    #@8d
    fill-array-data v2, :array_536

    #@90
    aput-object v2, v1, v7

    #@92
    aput-object v1, v0, v6

    #@94
    new-array v1, v3, [[I

    #@96
    new-array v2, v3, [I

    #@98
    fill-array-data v2, :array_542

    #@9b
    aput-object v2, v1, v5

    #@9d
    new-array v2, v3, [I

    #@9f
    fill-array-data v2, :array_54e

    #@a2
    aput-object v2, v1, v6

    #@a4
    new-array v2, v3, [I

    #@a6
    fill-array-data v2, :array_55a

    #@a9
    aput-object v2, v1, v4

    #@ab
    new-array v2, v3, [I

    #@ad
    fill-array-data v2, :array_566

    #@b0
    aput-object v2, v1, v7

    #@b2
    aput-object v1, v0, v4

    #@b4
    new-array v1, v3, [[I

    #@b6
    new-array v2, v3, [I

    #@b8
    fill-array-data v2, :array_572

    #@bb
    aput-object v2, v1, v5

    #@bd
    new-array v2, v3, [I

    #@bf
    fill-array-data v2, :array_57e

    #@c2
    aput-object v2, v1, v6

    #@c4
    new-array v2, v3, [I

    #@c6
    fill-array-data v2, :array_58a

    #@c9
    aput-object v2, v1, v4

    #@cb
    new-array v2, v3, [I

    #@cd
    fill-array-data v2, :array_596

    #@d0
    aput-object v2, v1, v7

    #@d2
    aput-object v1, v0, v7

    #@d4
    sput-object v0, Landroid/icu/text/ArabicShaping;->shapeTable:[[[I

    #@d6
    return-void

    #@d7
    .line 648
    nop

    #@d8
    :array_d8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0xct 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
    .end array-data

    #@ec
    .line 665
    :array_ec
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@10c
    .line 682
    :array_10c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@130
    .line 701
    :array_130
    .array-data 0x2
        0xeft 0xfet
        0xf0t 0xfet
    .end array-data

    #@136
    .line 707
    :array_136
    .array-data 0x2
        0x22t 0x6t
        0x23t 0x6t
        0x25t 0x6t
        0x27t 0x6t
    .end array-data

    #@13e
    .line 714
    :array_13e
    .array-data 0x4
        0x21t 0x11t 0x0t 0x0t
        0x21t 0x13t 0x0t 0x0t
        0x1t 0x15t 0x0t 0x0t
        0x21t 0x17t 0x0t 0x0t
        0x3t 0x19t 0x0t 0x0t
        0x21t 0x1dt 0x0t 0x0t
        0x3t 0x1ft 0x0t 0x0t
        0x1t 0x23t 0x0t 0x0t
        0x3t 0x25t 0x0t 0x0t
        0x3t 0x29t 0x0t 0x0t
        0x3t 0x2dt 0x0t 0x0t
        0x3t 0x31t 0x0t 0x0t
        0x3t 0x35t 0x0t 0x0t
        0x1t 0x39t 0x0t 0x0t
        0x1t 0x3bt 0x0t 0x0t
        0x1t 0x3dt 0x0t 0x0t
        0x1t 0x3ft 0x0t 0x0t
        0x3t 0x41t 0x0t 0x0t
        0x3t 0x45t 0x0t 0x0t
        0x3t 0x49t 0x0t 0x0t
        0x3t 0x4dt 0x0t 0x0t
        0x3t 0x51t 0x0t 0x0t
        0x3t 0x55t 0x0t 0x0t
        0x3t 0x59t 0x0t 0x0t
        0x3t 0x5dt 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x61t 0x0t 0x0t
        0x3t 0x65t 0x0t 0x0t
        0x3t 0x69t 0x0t 0x0t
        0x13t 0x6dt 0x0t 0x0t
        0x3t 0x71t 0x0t 0x0t
        0x3t 0x75t 0x0t 0x0t
        0x3t 0x79t 0x0t 0x0t
        0x1t 0x7dt 0x0t 0x0t
        0x1t 0x7ft 0x0t 0x0t
        0x3t 0x81t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x85t 0x0t 0x0t
        0x1t 0x87t 0x0t 0x0t
        0x1t 0x89t 0x0t 0x0t
        0x1t 0x8bt 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@2a6
    .line 792
    :array_2a6
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x21t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x10t 0x0t 0x0t 0x0t
        0x12t 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@3c4
    .line 833
    :array_3c4
    .array-data 0x4
        0x4bt 0x6t 0x0t 0x0t
        0x4bt 0x6t 0x0t 0x0t
        0x4ct 0x6t 0x0t 0x0t
        0x4ct 0x6t 0x0t 0x0t
        0x4dt 0x6t 0x0t 0x0t
        0x4dt 0x6t 0x0t 0x0t
        0x4et 0x6t 0x0t 0x0t
        0x4et 0x6t 0x0t 0x0t
        0x4ft 0x6t 0x0t 0x0t
        0x4ft 0x6t 0x0t 0x0t
        0x50t 0x6t 0x0t 0x0t
        0x50t 0x6t 0x0t 0x0t
        0x51t 0x6t 0x0t 0x0t
        0x51t 0x6t 0x0t 0x0t
        0x52t 0x6t 0x0t 0x0t
        0x52t 0x6t 0x0t 0x0t
        0x21t 0x6t 0x0t 0x0t
        0x22t 0x6t 0x0t 0x0t
        0x22t 0x6t 0x0t 0x0t
        0x23t 0x6t 0x0t 0x0t
        0x23t 0x6t 0x0t 0x0t
        0x24t 0x6t 0x0t 0x0t
        0x24t 0x6t 0x0t 0x0t
        0x25t 0x6t 0x0t 0x0t
        0x25t 0x6t 0x0t 0x0t
        0x26t 0x6t 0x0t 0x0t
        0x26t 0x6t 0x0t 0x0t
        0x26t 0x6t 0x0t 0x0t
        0x26t 0x6t 0x0t 0x0t
        0x27t 0x6t 0x0t 0x0t
        0x27t 0x6t 0x0t 0x0t
        0x28t 0x6t 0x0t 0x0t
        0x28t 0x6t 0x0t 0x0t
        0x28t 0x6t 0x0t 0x0t
        0x28t 0x6t 0x0t 0x0t
        0x29t 0x6t 0x0t 0x0t
        0x29t 0x6t 0x0t 0x0t
        0x2at 0x6t 0x0t 0x0t
        0x2at 0x6t 0x0t 0x0t
        0x2at 0x6t 0x0t 0x0t
        0x2at 0x6t 0x0t 0x0t
        0x2bt 0x6t 0x0t 0x0t
        0x2bt 0x6t 0x0t 0x0t
        0x2bt 0x6t 0x0t 0x0t
        0x2bt 0x6t 0x0t 0x0t
        0x2ct 0x6t 0x0t 0x0t
        0x2ct 0x6t 0x0t 0x0t
        0x2ct 0x6t 0x0t 0x0t
        0x2ct 0x6t 0x0t 0x0t
        0x2dt 0x6t 0x0t 0x0t
        0x2dt 0x6t 0x0t 0x0t
        0x2dt 0x6t 0x0t 0x0t
        0x2dt 0x6t 0x0t 0x0t
        0x2et 0x6t 0x0t 0x0t
        0x2et 0x6t 0x0t 0x0t
        0x2et 0x6t 0x0t 0x0t
        0x2et 0x6t 0x0t 0x0t
        0x2ft 0x6t 0x0t 0x0t
        0x2ft 0x6t 0x0t 0x0t
        0x30t 0x6t 0x0t 0x0t
        0x30t 0x6t 0x0t 0x0t
        0x31t 0x6t 0x0t 0x0t
        0x31t 0x6t 0x0t 0x0t
        0x32t 0x6t 0x0t 0x0t
        0x32t 0x6t 0x0t 0x0t
        0x33t 0x6t 0x0t 0x0t
        0x33t 0x6t 0x0t 0x0t
        0x33t 0x6t 0x0t 0x0t
        0x33t 0x6t 0x0t 0x0t
        0x34t 0x6t 0x0t 0x0t
        0x34t 0x6t 0x0t 0x0t
        0x34t 0x6t 0x0t 0x0t
        0x34t 0x6t 0x0t 0x0t
        0x35t 0x6t 0x0t 0x0t
        0x35t 0x6t 0x0t 0x0t
        0x35t 0x6t 0x0t 0x0t
        0x35t 0x6t 0x0t 0x0t
        0x36t 0x6t 0x0t 0x0t
        0x36t 0x6t 0x0t 0x0t
        0x36t 0x6t 0x0t 0x0t
        0x36t 0x6t 0x0t 0x0t
        0x37t 0x6t 0x0t 0x0t
        0x37t 0x6t 0x0t 0x0t
        0x37t 0x6t 0x0t 0x0t
        0x37t 0x6t 0x0t 0x0t
        0x38t 0x6t 0x0t 0x0t
        0x38t 0x6t 0x0t 0x0t
        0x38t 0x6t 0x0t 0x0t
        0x38t 0x6t 0x0t 0x0t
        0x39t 0x6t 0x0t 0x0t
        0x39t 0x6t 0x0t 0x0t
        0x39t 0x6t 0x0t 0x0t
        0x39t 0x6t 0x0t 0x0t
        0x3at 0x6t 0x0t 0x0t
        0x3at 0x6t 0x0t 0x0t
        0x3at 0x6t 0x0t 0x0t
        0x3at 0x6t 0x0t 0x0t
        0x41t 0x6t 0x0t 0x0t
        0x41t 0x6t 0x0t 0x0t
        0x41t 0x6t 0x0t 0x0t
        0x41t 0x6t 0x0t 0x0t
        0x42t 0x6t 0x0t 0x0t
        0x42t 0x6t 0x0t 0x0t
        0x42t 0x6t 0x0t 0x0t
        0x42t 0x6t 0x0t 0x0t
        0x43t 0x6t 0x0t 0x0t
        0x43t 0x6t 0x0t 0x0t
        0x43t 0x6t 0x0t 0x0t
        0x43t 0x6t 0x0t 0x0t
        0x44t 0x6t 0x0t 0x0t
        0x44t 0x6t 0x0t 0x0t
        0x44t 0x6t 0x0t 0x0t
        0x44t 0x6t 0x0t 0x0t
        0x45t 0x6t 0x0t 0x0t
        0x45t 0x6t 0x0t 0x0t
        0x45t 0x6t 0x0t 0x0t
        0x45t 0x6t 0x0t 0x0t
        0x46t 0x6t 0x0t 0x0t
        0x46t 0x6t 0x0t 0x0t
        0x46t 0x6t 0x0t 0x0t
        0x46t 0x6t 0x0t 0x0t
        0x47t 0x6t 0x0t 0x0t
        0x47t 0x6t 0x0t 0x0t
        0x47t 0x6t 0x0t 0x0t
        0x47t 0x6t 0x0t 0x0t
        0x48t 0x6t 0x0t 0x0t
        0x48t 0x6t 0x0t 0x0t
        0x49t 0x6t 0x0t 0x0t
        0x49t 0x6t 0x0t 0x0t
        0x4at 0x6t 0x0t 0x0t
        0x4at 0x6t 0x0t 0x0t
        0x4at 0x6t 0x0t 0x0t
        0x4at 0x6t 0x0t 0x0t
        0x5ct 0x6t 0x0t 0x0t
        0x5ct 0x6t 0x0t 0x0t
        0x5dt 0x6t 0x0t 0x0t
        0x5dt 0x6t 0x0t 0x0t
        0x5et 0x6t 0x0t 0x0t
        0x5et 0x6t 0x0t 0x0t
        0x5ft 0x6t 0x0t 0x0t
        0x5ft 0x6t 0x0t 0x0t
    .end array-data

    #@4e2
    .line 846
    :array_4e2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@4ee
    :array_4ee
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@4fa
    :array_4fa
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@506
    :array_506
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    #@512
    :array_512
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@51e
    :array_51e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@52a
    :array_52a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@536
    :array_536
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@542
    :array_542
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@54e
    :array_54e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    #@55a
    :array_55a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@566
    :array_566
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@572
    :array_572
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@57e
    :array_57e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@58a
    :array_58a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@596
    :array_596
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(I)V
    .registers 8
    .parameter "options"

    #@0
    .prologue
    const/high16 v5, 0x800

    #@2
    const/high16 v4, 0x400

    #@4
    const/4 v1, 0x1

    #@5
    const/4 v2, 0x0

    #@6
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 199
    iput p1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@b
    .line 200
    and-int/lit16 v0, p1, 0xe0

    #@d
    const/16 v3, 0x80

    #@f
    if-le v0, v3, :cond_19

    #@11
    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "bad DIGITS options"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 204
    :cond_19
    and-int/lit8 v0, p1, 0x4

    #@1b
    if-nez v0, :cond_30

    #@1d
    move v0, v1

    #@1e
    :goto_1e
    iput-boolean v0, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@20
    .line 206
    and-int v0, p1, v4

    #@22
    if-ne v0, v4, :cond_32

    #@24
    :goto_24
    iput-boolean v1, p0, Landroid/icu/text/ArabicShaping;->spacesRelativeToTextBeginEnd:Z

    #@26
    .line 207
    and-int v0, p1, v5

    #@28
    if-ne v0, v5, :cond_34

    #@2a
    .line 208
    const v0, 0xfe73

    #@2d
    iput-char v0, p0, Landroid/icu/text/ArabicShaping;->tailChar:C

    #@2f
    .line 212
    :goto_2f
    return-void

    #@30
    :cond_30
    move v0, v2

    #@31
    .line 204
    goto :goto_1e

    #@32
    :cond_32
    move v1, v2

    #@33
    .line 206
    goto :goto_24

    #@34
    .line 210
    :cond_34
    const/16 v0, 0x200b

    #@36
    iput-char v0, p0, Landroid/icu/text/ArabicShaping;->tailChar:C

    #@38
    goto :goto_2f
.end method

.method private calculateSize([CII)I
    .registers 9
    .parameter "source"
    .parameter "sourceStart"
    .parameter "sourceLength"

    #@0
    .prologue
    const/16 v4, 0x644

    #@2
    .line 1136
    move v0, p3

    #@3
    .line 1138
    .local v0, destSize:I
    iget v3, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@5
    and-int/lit8 v3, v3, 0x18

    #@7
    sparse-switch v3, :sswitch_data_64

    #@a
    .line 1168
    :cond_a
    return v0

    #@b
    .line 1141
    :sswitch_b
    iget-boolean v3, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@d
    if-eqz v3, :cond_31

    #@f
    .line 1142
    move v2, p2

    #@10
    .local v2, i:I
    add-int v3, p2, p3

    #@12
    add-int/lit8 v1, v3, -0x1

    #@14
    .local v1, e:I
    :goto_14
    if-ge v2, v1, :cond_a

    #@16
    .line 1143
    aget-char v3, p1, v2

    #@18
    if-ne v3, v4, :cond_24

    #@1a
    add-int/lit8 v3, v2, 0x1

    #@1c
    aget-char v3, p1, v3

    #@1e
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isAlefChar(C)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2c

    #@24
    :cond_24
    aget-char v3, p1, v2

    #@26
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isTashkeelCharFE(C)Z

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_2e

    #@2c
    .line 1144
    :cond_2c
    add-int/lit8 v0, v0, -0x1

    #@2e
    .line 1142
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    #@30
    goto :goto_14

    #@31
    .line 1148
    .end local v1           #e:I
    .end local v2           #i:I
    :cond_31
    add-int/lit8 v2, p2, 0x1

    #@33
    .restart local v2       #i:I
    add-int v1, p2, p3

    #@35
    .restart local v1       #e:I
    :goto_35
    if-ge v2, v1, :cond_a

    #@37
    .line 1149
    aget-char v3, p1, v2

    #@39
    if-ne v3, v4, :cond_45

    #@3b
    add-int/lit8 v3, v2, -0x1

    #@3d
    aget-char v3, p1, v3

    #@3f
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isAlefChar(C)Z

    #@42
    move-result v3

    #@43
    if-nez v3, :cond_4d

    #@45
    :cond_45
    aget-char v3, p1, v2

    #@47
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isTashkeelCharFE(C)Z

    #@4a
    move-result v3

    #@4b
    if-eqz v3, :cond_4f

    #@4d
    .line 1150
    :cond_4d
    add-int/lit8 v0, v0, -0x1

    #@4f
    .line 1148
    :cond_4f
    add-int/lit8 v2, v2, 0x1

    #@51
    goto :goto_35

    #@52
    .line 1157
    .end local v1           #e:I
    .end local v2           #i:I
    :sswitch_52
    move v2, p2

    #@53
    .restart local v2       #i:I
    add-int v1, p2, p3

    #@55
    .restart local v1       #e:I
    :goto_55
    if-ge v2, v1, :cond_a

    #@57
    .line 1158
    aget-char v3, p1, v2

    #@59
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isLamAlefChar(C)Z

    #@5c
    move-result v3

    #@5d
    if-eqz v3, :cond_61

    #@5f
    .line 1159
    add-int/lit8 v0, v0, 0x1

    #@61
    .line 1157
    :cond_61
    add-int/lit8 v2, v2, 0x1

    #@63
    goto :goto_55

    #@64
    .line 1138
    :sswitch_data_64
    .sparse-switch
        0x8 -> :sswitch_b
        0x10 -> :sswitch_52
        0x18 -> :sswitch_b
    .end sparse-switch
.end method

.method private static changeLamAlef(C)C
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 912
    packed-switch p0, :pswitch_data_12

    #@3
    .line 917
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return v0

    #@5
    .line 913
    :pswitch_5
    const/16 v0, 0x65c

    #@7
    goto :goto_4

    #@8
    .line 914
    :pswitch_8
    const/16 v0, 0x65d

    #@a
    goto :goto_4

    #@b
    .line 915
    :pswitch_b
    const/16 v0, 0x65e

    #@d
    goto :goto_4

    #@e
    .line 916
    :pswitch_e
    const/16 v0, 0x65f

    #@10
    goto :goto_4

    #@11
    .line 912
    nop

    #@12
    :pswitch_data_12
    .packed-switch 0x622
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_b
        :pswitch_3
        :pswitch_e
    .end packed-switch
.end method

.method public static countSpaceSub([CIC)I
    .registers 6
    .parameter "dest"
    .parameter "length"
    .parameter "subChar"

    #@0
    .prologue
    .line 1177
    const/4 v1, 0x0

    #@1
    .line 1178
    .local v1, i:I
    const/4 v0, 0x0

    #@2
    .line 1179
    .local v0, count:I
    :goto_2
    if-ge v1, p1, :cond_d

    #@4
    .line 1180
    aget-char v2, p0, v1

    #@6
    if-ne v2, p2, :cond_a

    #@8
    .line 1181
    add-int/lit8 v0, v0, 0x1

    #@a
    .line 1183
    :cond_a
    add-int/lit8 v1, v1, 0x1

    #@c
    goto :goto_2

    #@d
    .line 1185
    :cond_d
    return v0
.end method

.method private static countSpacesLeft([CII)I
    .registers 7
    .parameter "dest"
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 972
    move v1, p1

    #@1
    .local v1, i:I
    add-int v0, p1, p2

    #@3
    .local v0, e:I
    :goto_3
    if-ge v1, v0, :cond_d

    #@5
    .line 973
    aget-char v2, p0, v1

    #@7
    const/16 v3, 0x20

    #@9
    if-eq v2, v3, :cond_e

    #@b
    .line 974
    sub-int p2, v1, p1

    #@d
    .line 977
    .end local p2
    :cond_d
    return p2

    #@e
    .line 972
    .restart local p2
    :cond_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_3
.end method

.method private static countSpacesRight([CII)I
    .registers 6
    .parameter "dest"
    .parameter "start"
    .parameter "count"

    #@0
    .prologue
    .line 984
    add-int v0, p1, p2

    #@2
    .local v0, i:I
    :cond_2
    add-int/lit8 v0, v0, -0x1

    #@4
    if-lt v0, p1, :cond_12

    #@6
    .line 985
    aget-char v1, p0, v0

    #@8
    const/16 v2, 0x20

    #@a
    if-eq v1, v2, :cond_2

    #@c
    .line 986
    add-int v1, p1, p2

    #@e
    add-int/lit8 v1, v1, -0x1

    #@10
    sub-int p2, v1, v0

    #@12
    .line 989
    .end local p2
    :cond_12
    return p2
.end method

.method private deShapeUnicode([CIII)I
    .registers 11
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "destSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1802
    invoke-direct {p0, p1, p2, p3}, Landroid/icu/text/ArabicShaping;->deshapeNormalize([CII)I

    #@3
    move-result v4

    #@4
    .line 1805
    .local v4, lamalef_count:I
    if-eqz v4, :cond_10

    #@6
    .line 1807
    const/4 v5, 0x1

    #@7
    move-object v0, p0

    #@8
    move-object v1, p1

    #@9
    move v2, p2

    #@a
    move v3, p3

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/icu/text/ArabicShaping;->expandCompositChar([CIIII)I

    #@e
    move-result p4

    #@f
    .line 1812
    :goto_f
    return p4

    #@10
    .line 1809
    :cond_10
    move p4, p3

    #@11
    goto :goto_f
.end method

.method private deshapeNormalize([CII)I
    .registers 16
    .parameter "dest"
    .parameter "start"
    .parameter "length"

    #@0
    .prologue
    const v11, 0xfe70

    #@3
    const/16 v10, 0x20

    #@5
    const/4 v7, 0x0

    #@6
    const/4 v6, 0x1

    #@7
    .line 1615
    const/4 v3, 0x0

    #@8
    .line 1616
    .local v3, lacount:I
    const/4 v5, 0x0

    #@9
    .line 1617
    .local v5, yehHamzaComposeEnabled:I
    const/4 v4, 0x0

    #@a
    .line 1619
    .local v4, seenComposeEnabled:I
    iget v8, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@c
    const/high16 v9, 0x380

    #@e
    and-int/2addr v8, v9

    #@f
    const/high16 v9, 0x100

    #@11
    if-ne v8, v9, :cond_49

    #@13
    move v5, v6

    #@14
    .line 1620
    :goto_14
    iget v8, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@16
    const/high16 v9, 0x70

    #@18
    and-int/2addr v8, v9

    #@19
    const/high16 v9, 0x20

    #@1b
    if-ne v8, v9, :cond_4b

    #@1d
    move v4, v6

    #@1e
    .line 1622
    :goto_1e
    move v2, p2

    #@1f
    .local v2, i:I
    add-int v1, v2, p3

    #@21
    .local v1, e:I
    :goto_21
    if-ge v2, v1, :cond_7f

    #@23
    .line 1623
    aget-char v0, p1, v2

    #@25
    .line 1625
    .local v0, ch:C
    if-ne v5, v6, :cond_4d

    #@27
    const/16 v7, 0x621

    #@29
    if-eq v0, v7, :cond_30

    #@2b
    const v7, 0xfe80

    #@2e
    if-ne v0, v7, :cond_4d

    #@30
    :cond_30
    add-int/lit8 v7, p3, -0x1

    #@32
    if-ge v2, v7, :cond_4d

    #@34
    add-int/lit8 v7, v2, 0x1

    #@36
    aget-char v7, p1, v7

    #@38
    invoke-static {v7}, Landroid/icu/text/ArabicShaping;->isAlefMaksouraChar(C)Z

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_4d

    #@3e
    .line 1627
    aput-char v10, p1, v2

    #@40
    .line 1628
    add-int/lit8 v7, v2, 0x1

    #@42
    const/16 v8, 0x626

    #@44
    aput-char v8, p1, v7

    #@46
    .line 1622
    :cond_46
    :goto_46
    add-int/lit8 v2, v2, 0x1

    #@48
    goto :goto_21

    #@49
    .end local v0           #ch:C
    .end local v1           #e:I
    .end local v2           #i:I
    :cond_49
    move v5, v7

    #@4a
    .line 1619
    goto :goto_14

    #@4b
    :cond_4b
    move v4, v7

    #@4c
    .line 1620
    goto :goto_1e

    #@4d
    .line 1629
    .restart local v0       #ch:C
    .restart local v1       #e:I
    .restart local v2       #i:I
    :cond_4d
    if-ne v4, v6, :cond_66

    #@4f
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isTailChar(C)Z

    #@52
    move-result v7

    #@53
    if-eqz v7, :cond_66

    #@55
    add-int/lit8 v7, p3, -0x1

    #@57
    if-ge v2, v7, :cond_66

    #@59
    add-int/lit8 v7, v2, 0x1

    #@5b
    aget-char v7, p1, v7

    #@5d
    invoke-static {v7}, Landroid/icu/text/ArabicShaping;->isSeenTailFamilyChar(C)I

    #@60
    move-result v7

    #@61
    if-ne v7, v6, :cond_66

    #@63
    .line 1631
    aput-char v10, p1, v2

    #@65
    goto :goto_46

    #@66
    .line 1633
    :cond_66
    if-lt v0, v11, :cond_46

    #@68
    const v7, 0xfefc

    #@6b
    if-gt v0, v7, :cond_46

    #@6d
    .line 1634
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isLamAlefChar(C)Z

    #@70
    move-result v7

    #@71
    if-eqz v7, :cond_75

    #@73
    .line 1635
    add-int/lit8 v3, v3, 0x1

    #@75
    .line 1637
    :cond_75
    sget-object v7, Landroid/icu/text/ArabicShaping;->convertFEto06:[I

    #@77
    sub-int v8, v0, v11

    #@79
    aget v7, v7, v8

    #@7b
    int-to-char v7, v7

    #@7c
    aput-char v7, p1, v2

    #@7e
    goto :goto_46

    #@7f
    .line 1640
    .end local v0           #ch:C
    :cond_7f
    return v3
.end method

.method private expandCompositChar([CIIII)I
    .registers 21
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "lacount"
    .parameter "shapingMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1503
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2
    const v2, 0x10003

    #@5
    and-int v9, v1, v2

    #@7
    .line 1504
    .local v9, lenOptionsLamAlef:I
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@9
    const/high16 v2, 0x70

    #@b
    and-int v10, v1, v2

    #@d
    .line 1505
    .local v10, lenOptionsSeen:I
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@f
    const/high16 v2, 0x380

    #@11
    and-int v11, v1, v2

    #@13
    .line 1506
    .local v11, lenOptionsYehHamza:I
    const/4 v13, 0x0

    #@14
    .line 1508
    .local v13, spaceNotFound:Z
    iget-boolean v1, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@16
    if-nez v1, :cond_1f

    #@18
    iget-boolean v1, p0, Landroid/icu/text/ArabicShaping;->spacesRelativeToTextBeginEnd:Z

    #@1a
    if-nez v1, :cond_1f

    #@1c
    .line 1509
    packed-switch v9, :pswitch_data_11a

    #@1f
    .line 1516
    :cond_1f
    :goto_1f
    const/4 v1, 0x1

    #@20
    move/from16 v0, p5

    #@22
    if-ne v0, v1, :cond_e2

    #@24
    .line 1517
    const/high16 v1, 0x1

    #@26
    if-ne v9, v1, :cond_78

    #@28
    .line 1518
    iget-boolean v1, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@2a
    if-eqz v1, :cond_54

    #@2c
    .line 1519
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtEnd([CIII)Z

    #@2f
    move-result v13

    #@30
    .line 1520
    if-eqz v13, :cond_36

    #@32
    .line 1521
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtBegin([CIII)Z

    #@35
    move-result v13

    #@36
    .line 1523
    :cond_36
    if-eqz v13, :cond_46

    #@38
    .line 1524
    const/4 v5, 0x0

    #@39
    const/4 v6, 0x0

    #@3a
    const/4 v7, 0x1

    #@3b
    move-object v1, p0

    #@3c
    move-object/from16 v2, p1

    #@3e
    move/from16 v3, p2

    #@40
    move/from16 v4, p3

    #@42
    invoke-direct/range {v1 .. v7}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtNear([CIIIII)Z

    #@45
    move-result v13

    #@46
    .line 1526
    :cond_46
    if-eqz v13, :cond_e1

    #@48
    .line 1527
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@4a
    const-string v2, "No spacefor lamalef"

    #@4c
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v1

    #@50
    .line 1510
    :pswitch_50
    const/4 v9, 0x2

    #@51
    goto :goto_1f

    #@52
    .line 1511
    :pswitch_52
    const/4 v9, 0x3

    #@53
    goto :goto_1f

    #@54
    .line 1530
    :cond_54
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtBegin([CIII)Z

    #@57
    move-result v13

    #@58
    .line 1531
    if-eqz v13, :cond_5e

    #@5a
    .line 1532
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtEnd([CIII)Z

    #@5d
    move-result v13

    #@5e
    .line 1534
    :cond_5e
    if-eqz v13, :cond_6e

    #@60
    .line 1535
    const/4 v5, 0x0

    #@61
    const/4 v6, 0x0

    #@62
    const/4 v7, 0x1

    #@63
    move-object v1, p0

    #@64
    move-object/from16 v2, p1

    #@66
    move/from16 v3, p2

    #@68
    move/from16 v4, p3

    #@6a
    invoke-direct/range {v1 .. v7}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtNear([CIIIII)Z

    #@6d
    move-result v13

    #@6e
    .line 1537
    :cond_6e
    if-eqz v13, :cond_e1

    #@70
    .line 1538
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@72
    const-string v2, "No spacefor lamalef"

    #@74
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@77
    throw v1

    #@78
    .line 1541
    :cond_78
    const/4 v1, 0x2

    #@79
    if-ne v9, v1, :cond_89

    #@7b
    .line 1542
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtEnd([CIII)Z

    #@7e
    move-result v13

    #@7f
    .line 1543
    if-eqz v13, :cond_e1

    #@81
    .line 1544
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@83
    const-string v2, "No spacefor lamalef"

    #@85
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@88
    throw v1

    #@89
    .line 1546
    :cond_89
    const/4 v1, 0x3

    #@8a
    if-ne v9, v1, :cond_9a

    #@8c
    .line 1547
    invoke-direct/range {p0 .. p4}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtBegin([CIII)Z

    #@8f
    move-result v13

    #@90
    .line 1548
    if-eqz v13, :cond_e1

    #@92
    .line 1549
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@94
    const-string v2, "No spacefor lamalef"

    #@96
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@99
    throw v1

    #@9a
    .line 1551
    :cond_9a
    const/4 v1, 0x1

    #@9b
    if-ne v9, v1, :cond_b5

    #@9d
    .line 1552
    const/4 v5, 0x0

    #@9e
    const/4 v6, 0x0

    #@9f
    const/4 v7, 0x1

    #@a0
    move-object v1, p0

    #@a1
    move-object/from16 v2, p1

    #@a3
    move/from16 v3, p2

    #@a5
    move/from16 v4, p3

    #@a7
    invoke-direct/range {v1 .. v7}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtNear([CIIIII)Z

    #@aa
    move-result v13

    #@ab
    .line 1553
    if-eqz v13, :cond_e1

    #@ad
    .line 1554
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@af
    const-string v2, "No spacefor lamalef"

    #@b1
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@b4
    throw v1

    #@b5
    .line 1556
    :cond_b5
    if-nez v9, :cond_e1

    #@b7
    .line 1557
    add-int v12, p2, p3

    #@b9
    .local v12, r:I
    add-int v14, v12, p4

    #@bb
    .local v14, w:I
    :goto_bb
    add-int/lit8 v12, v12, -0x1

    #@bd
    move/from16 v0, p2

    #@bf
    if-lt v12, v0, :cond_df

    #@c1
    .line 1558
    aget-char v8, p1, v12

    #@c3
    .line 1559
    .local v8, ch:C
    invoke-static {v8}, Landroid/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    #@c6
    move-result v1

    #@c7
    if-eqz v1, :cond_da

    #@c9
    .line 1560
    add-int/lit8 v14, v14, -0x1

    #@cb
    const/16 v1, 0x644

    #@cd
    aput-char v1, p1, v14

    #@cf
    .line 1561
    add-int/lit8 v14, v14, -0x1

    #@d1
    sget-object v1, Landroid/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    #@d3
    add-int/lit16 v2, v8, -0x65c

    #@d5
    aget-char v1, v1, v2

    #@d7
    aput-char v1, p1, v14

    #@d9
    goto :goto_bb

    #@da
    .line 1563
    :cond_da
    add-int/lit8 v14, v14, -0x1

    #@dc
    aput-char v8, p1, v14

    #@de
    goto :goto_bb

    #@df
    .line 1566
    .end local v8           #ch:C
    :cond_df
    add-int p3, p3, p4

    #@e1
    .line 1582
    .end local v12           #r:I
    .end local v14           #w:I
    :cond_e1
    return p3

    #@e2
    .line 1569
    :cond_e2
    const/high16 v1, 0x20

    #@e4
    if-ne v10, v1, :cond_fe

    #@e6
    .line 1570
    const/4 v5, 0x0

    #@e7
    const/4 v6, 0x1

    #@e8
    const/4 v7, 0x0

    #@e9
    move-object v1, p0

    #@ea
    move-object/from16 v2, p1

    #@ec
    move/from16 v3, p2

    #@ee
    move/from16 v4, p3

    #@f0
    invoke-direct/range {v1 .. v7}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtNear([CIIIII)Z

    #@f3
    move-result v13

    #@f4
    .line 1571
    if-eqz v13, :cond_fe

    #@f6
    .line 1572
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@f8
    const-string v2, "No space for Seen tail expansion"

    #@fa
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@fd
    throw v1

    #@fe
    .line 1575
    :cond_fe
    const/high16 v1, 0x100

    #@100
    if-ne v11, v1, :cond_e1

    #@102
    .line 1576
    const/4 v5, 0x1

    #@103
    const/4 v6, 0x0

    #@104
    const/4 v7, 0x0

    #@105
    move-object v1, p0

    #@106
    move-object/from16 v2, p1

    #@108
    move/from16 v3, p2

    #@10a
    move/from16 v4, p3

    #@10c
    invoke-direct/range {v1 .. v7}, Landroid/icu/text/ArabicShaping;->expandCompositCharAtNear([CIIIII)Z

    #@10f
    move-result v13

    #@110
    .line 1577
    if-eqz v13, :cond_e1

    #@112
    .line 1578
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@114
    const-string v2, "No space for YehHamza expansion"

    #@116
    invoke-direct {v1, v2}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@119
    throw v1

    #@11a
    .line 1509
    :pswitch_data_11a
    .packed-switch 0x2
        :pswitch_52
        :pswitch_50
    .end packed-switch
.end method

.method private expandCompositCharAtBegin([CIII)Z
    .registers 12
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "lacount"

    #@0
    .prologue
    .line 1387
    const/4 v2, 0x0

    #@1
    .line 1389
    .local v2, spaceNotFound:Z
    invoke-static {p1, p2, p3}, Landroid/icu/text/ArabicShaping;->countSpacesRight([CII)I

    #@4
    move-result v5

    #@5
    if-le p4, v5, :cond_a

    #@7
    .line 1390
    const/4 v2, 0x1

    #@8
    move v3, v2

    #@9
    .line 1402
    .end local v2           #spaceNotFound:Z
    .local v3, spaceNotFound:I
    :goto_9
    return v3

    #@a
    .line 1393
    .end local v3           #spaceNotFound:I
    .restart local v2       #spaceNotFound:Z
    :cond_a
    add-int v5, p2, p3

    #@c
    sub-int v1, v5, p4

    #@e
    .local v1, r:I
    add-int v4, p2, p3

    #@10
    .local v4, w:I
    :goto_10
    add-int/lit8 v1, v1, -0x1

    #@12
    if-lt v1, p2, :cond_32

    #@14
    .line 1394
    aget-char v0, p1, v1

    #@16
    .line 1395
    .local v0, ch:C
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    #@19
    move-result v5

    #@1a
    if-eqz v5, :cond_2d

    #@1c
    .line 1396
    add-int/lit8 v4, v4, -0x1

    #@1e
    const/16 v5, 0x644

    #@20
    aput-char v5, p1, v4

    #@22
    .line 1397
    add-int/lit8 v4, v4, -0x1

    #@24
    sget-object v5, Landroid/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    #@26
    add-int/lit16 v6, v0, -0x65c

    #@28
    aget-char v5, v5, v6

    #@2a
    aput-char v5, p1, v4

    #@2c
    goto :goto_10

    #@2d
    .line 1399
    :cond_2d
    add-int/lit8 v4, v4, -0x1

    #@2f
    aput-char v0, p1, v4

    #@31
    goto :goto_10

    #@32
    .end local v0           #ch:C
    :cond_32
    move v3, v2

    #@33
    .line 1402
    .restart local v3       #spaceNotFound:I
    goto :goto_9
.end method

.method private expandCompositCharAtEnd([CIII)Z
    .registers 14
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "lacount"

    #@0
    .prologue
    .line 1417
    const/4 v3, 0x0

    #@1
    .line 1419
    .local v3, spaceNotFound:Z
    invoke-static {p1, p2, p3}, Landroid/icu/text/ArabicShaping;->countSpacesLeft([CII)I

    #@4
    move-result v7

    #@5
    if-le p4, v7, :cond_a

    #@7
    .line 1420
    const/4 v3, 0x1

    #@8
    move v4, v3

    #@9
    .line 1432
    .end local v3           #spaceNotFound:Z
    .local v4, spaceNotFound:I
    :goto_9
    return v4

    #@a
    .line 1423
    .end local v4           #spaceNotFound:I
    .restart local v3       #spaceNotFound:Z
    :cond_a
    add-int v2, p2, p4

    #@c
    .local v2, r:I
    move v5, p2

    #@d
    .local v5, w:I
    add-int v1, p2, p3

    #@f
    .local v1, e:I
    move v6, v5

    #@10
    .end local v5           #w:I
    .local v6, w:I
    :goto_10
    if-ge v2, v1, :cond_34

    #@12
    .line 1424
    aget-char v0, p1, v2

    #@14
    .line 1425
    .local v0, ch:C
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    #@17
    move-result v7

    #@18
    if-eqz v7, :cond_2f

    #@1a
    .line 1426
    add-int/lit8 v5, v6, 0x1

    #@1c
    .end local v6           #w:I
    .restart local v5       #w:I
    sget-object v7, Landroid/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    #@1e
    add-int/lit16 v8, v0, -0x65c

    #@20
    aget-char v7, v7, v8

    #@22
    aput-char v7, p1, v6

    #@24
    .line 1427
    add-int/lit8 v6, v5, 0x1

    #@26
    .end local v5           #w:I
    .restart local v6       #w:I
    const/16 v7, 0x644

    #@28
    aput-char v7, p1, v5

    #@2a
    move v5, v6

    #@2b
    .line 1423
    .end local v6           #w:I
    .restart local v5       #w:I
    :goto_2b
    add-int/lit8 v2, v2, 0x1

    #@2d
    move v6, v5

    #@2e
    .end local v5           #w:I
    .restart local v6       #w:I
    goto :goto_10

    #@2f
    .line 1429
    :cond_2f
    add-int/lit8 v5, v6, 0x1

    #@31
    .end local v6           #w:I
    .restart local v5       #w:I
    aput-char v0, p1, v6

    #@33
    goto :goto_2b

    #@34
    .end local v0           #ch:C
    .end local v5           #w:I
    .restart local v6       #w:I
    :cond_34
    move v4, v3

    #@35
    .line 1432
    .restart local v4       #spaceNotFound:I
    goto :goto_9
.end method

.method private expandCompositCharAtNear([CIIIII)Z
    .registers 14
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "yehHamzaOption"
    .parameter "seenTailOption"
    .parameter "lamAlefOption"

    #@0
    .prologue
    const/16 v6, 0x20

    #@2
    const/4 v5, 0x1

    #@3
    .line 1445
    const/4 v2, 0x0

    #@4
    .line 1449
    .local v2, spaceNotFound:Z
    aget-char v3, p1, p2

    #@6
    invoke-static {v3}, Landroid/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_f

    #@c
    .line 1450
    const/4 v2, 0x1

    #@d
    move v3, v2

    #@e
    .line 1483
    :goto_e
    return v3

    #@f
    .line 1453
    :cond_f
    add-int v1, p2, p3

    #@11
    .local v1, i:I
    :cond_11
    :goto_11
    add-int/lit8 v1, v1, -0x1

    #@13
    if-lt v1, p2, :cond_79

    #@15
    .line 1454
    aget-char v0, p1, v1

    #@17
    .line 1455
    .local v0, ch:C
    if-ne p6, v5, :cond_39

    #@19
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_39

    #@1f
    .line 1456
    if-le v1, p2, :cond_36

    #@21
    add-int/lit8 v3, v1, -0x1

    #@23
    aget-char v3, p1, v3

    #@25
    if-ne v3, v6, :cond_36

    #@27
    .line 1457
    const/16 v3, 0x644

    #@29
    aput-char v3, p1, v1

    #@2b
    .line 1458
    add-int/lit8 v1, v1, -0x1

    #@2d
    sget-object v3, Landroid/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    #@2f
    add-int/lit16 v4, v0, -0x65c

    #@31
    aget-char v3, v3, v4

    #@33
    aput-char v3, p1, v1

    #@35
    goto :goto_11

    #@36
    .line 1460
    :cond_36
    const/4 v2, 0x1

    #@37
    move v3, v2

    #@38
    .line 1461
    goto :goto_e

    #@39
    .line 1463
    :cond_39
    if-ne p5, v5, :cond_53

    #@3b
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isSeenTailFamilyChar(C)I

    #@3e
    move-result v3

    #@3f
    if-ne v3, v5, :cond_53

    #@41
    .line 1464
    if-le v1, p2, :cond_50

    #@43
    add-int/lit8 v3, v1, -0x1

    #@45
    aget-char v3, p1, v3

    #@47
    if-ne v3, v6, :cond_50

    #@49
    .line 1465
    add-int/lit8 v3, v1, -0x1

    #@4b
    iget-char v4, p0, Landroid/icu/text/ArabicShaping;->tailChar:C

    #@4d
    aput-char v4, p1, v3

    #@4f
    goto :goto_11

    #@50
    .line 1467
    :cond_50
    const/4 v2, 0x1

    #@51
    move v3, v2

    #@52
    .line 1468
    goto :goto_e

    #@53
    .line 1470
    :cond_53
    if-ne p4, v5, :cond_11

    #@55
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isYehHamzaChar(C)Z

    #@58
    move-result v3

    #@59
    if-eqz v3, :cond_11

    #@5b
    .line 1472
    if-le v1, p2, :cond_76

    #@5d
    add-int/lit8 v3, v1, -0x1

    #@5f
    aget-char v3, p1, v3

    #@61
    if-ne v3, v6, :cond_76

    #@63
    .line 1473
    sget-object v3, Landroid/icu/text/ArabicShaping;->yehHamzaToYeh:[C

    #@65
    const v4, 0xfe89

    #@68
    sub-int v4, v0, v4

    #@6a
    aget-char v3, v3, v4

    #@6c
    aput-char v3, p1, v1

    #@6e
    .line 1474
    add-int/lit8 v3, v1, -0x1

    #@70
    const v4, 0xfe80

    #@73
    aput-char v4, p1, v3

    #@75
    goto :goto_11

    #@76
    .line 1476
    :cond_76
    const/4 v2, 0x1

    #@77
    move v3, v2

    #@78
    .line 1477
    goto :goto_e

    #@79
    .line 1483
    .end local v0           #ch:C
    :cond_79
    const/4 v3, 0x0

    #@7a
    goto :goto_e
.end method

.method public static flipArray([CIII)I
    .registers 8
    .parameter "dest"
    .parameter "start"
    .parameter "e"
    .parameter "w"

    #@0
    .prologue
    .line 1212
    if-le p3, p1, :cond_13

    #@2
    .line 1214
    move v0, p3

    #@3
    .line 1215
    .local v0, r:I
    move p3, p1

    #@4
    move v1, v0

    #@5
    .end local v0           #r:I
    .local v1, r:I
    move v2, p3

    #@6
    .line 1216
    .end local p3
    .local v2, w:I
    :goto_6
    if-ge v1, p2, :cond_15

    #@8
    .line 1217
    add-int/lit8 p3, v2, 0x1

    #@a
    .end local v2           #w:I
    .restart local p3
    add-int/lit8 v0, v1, 0x1

    #@c
    .end local v1           #r:I
    .restart local v0       #r:I
    aget-char v3, p0, v1

    #@e
    aput-char v3, p0, v2

    #@10
    move v1, v0

    #@11
    .end local v0           #r:I
    .restart local v1       #r:I
    move v2, p3

    #@12
    .end local p3
    .restart local v2       #w:I
    goto :goto_6

    #@13
    .line 1220
    .end local v1           #r:I
    .end local v2           #w:I
    .restart local p3
    :cond_13
    move p3, p2

    #@14
    .line 1222
    :goto_14
    return p3

    #@15
    .end local p3
    .restart local v1       #r:I
    .restart local v2       #w:I
    :cond_15
    move p3, v2

    #@16
    .end local v2           #w:I
    .restart local p3
    goto :goto_14
.end method

.method private static getLink(C)I
    .registers 3
    .parameter "ch"

    #@0
    .prologue
    const v1, 0xfe70

    #@3
    .line 951
    const/16 v0, 0x622

    #@5
    if-lt p0, v0, :cond_12

    #@7
    const/16 v0, 0x6d3

    #@9
    if-gt p0, v0, :cond_12

    #@b
    .line 952
    sget-object v0, Landroid/icu/text/ArabicShaping;->araLink:[I

    #@d
    add-int/lit16 v1, p0, -0x622

    #@f
    aget v0, v0, v1

    #@11
    .line 960
    :goto_11
    return v0

    #@12
    .line 953
    :cond_12
    const/16 v0, 0x200d

    #@14
    if-ne p0, v0, :cond_18

    #@16
    .line 954
    const/4 v0, 0x3

    #@17
    goto :goto_11

    #@18
    .line 955
    :cond_18
    const/16 v0, 0x206d

    #@1a
    if-lt p0, v0, :cond_22

    #@1c
    const/16 v0, 0x206f

    #@1e
    if-gt p0, v0, :cond_22

    #@20
    .line 956
    const/4 v0, 0x4

    #@21
    goto :goto_11

    #@22
    .line 957
    :cond_22
    if-lt p0, v1, :cond_30

    #@24
    const v0, 0xfefc

    #@27
    if-gt p0, v0, :cond_30

    #@29
    .line 958
    sget-object v0, Landroid/icu/text/ArabicShaping;->presLink:[I

    #@2b
    sub-int v1, p0, v1

    #@2d
    aget v0, v0, v1

    #@2f
    goto :goto_11

    #@30
    .line 960
    :cond_30
    const/4 v0, 0x0

    #@31
    goto :goto_11
.end method

.method private handleGeneratedSpaces([CII)I
    .registers 16
    .parameter "dest"
    .parameter "start"
    .parameter "length"

    #@0
    .prologue
    .line 1270
    iget v10, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2
    const v11, 0x10003

    #@5
    and-int v3, v10, v11

    #@7
    .line 1271
    .local v3, lenOptionsLamAlef:I
    iget v10, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@9
    const/high16 v11, 0xe

    #@b
    and-int v4, v10, v11

    #@d
    .line 1272
    .local v4, lenOptionsTashkeel:I
    const/4 v2, 0x0

    #@e
    .line 1273
    .local v2, lamAlefOn:Z
    const/4 v5, 0x0

    #@f
    .line 1275
    .local v5, tashkeelOn:Z
    iget-boolean v10, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@11
    if-nez v10, :cond_39

    #@13
    const/4 v10, 0x1

    #@14
    :goto_14
    iget-boolean v11, p0, Landroid/icu/text/ArabicShaping;->spacesRelativeToTextBeginEnd:Z

    #@16
    if-nez v11, :cond_3b

    #@18
    const/4 v11, 0x1

    #@19
    :goto_19
    and-int/2addr v10, v11

    #@1a
    if-eqz v10, :cond_22

    #@1c
    .line 1276
    packed-switch v3, :pswitch_data_100

    #@1f
    .line 1281
    :goto_1f
    sparse-switch v4, :sswitch_data_108

    #@22
    .line 1289
    :cond_22
    :goto_22
    const/4 v10, 0x1

    #@23
    if-ne v3, v10, :cond_47

    #@25
    .line 1290
    move v1, p2

    #@26
    .local v1, i:I
    add-int v0, v1, p3

    #@28
    .local v0, e:I
    :goto_28
    if-ge v1, v0, :cond_fe

    #@2a
    .line 1291
    aget-char v10, p1, v1

    #@2c
    const v11, 0xffff

    #@2f
    if-ne v10, v11, :cond_36

    #@31
    .line 1292
    const v10, 0xfeff

    #@34
    aput-char v10, p1, v1

    #@36
    .line 1290
    :cond_36
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_28

    #@39
    .line 1275
    .end local v0           #e:I
    .end local v1           #i:I
    :cond_39
    const/4 v10, 0x0

    #@3a
    goto :goto_14

    #@3b
    :cond_3b
    const/4 v11, 0x0

    #@3c
    goto :goto_19

    #@3d
    .line 1277
    :pswitch_3d
    const/4 v3, 0x2

    #@3e
    goto :goto_1f

    #@3f
    .line 1278
    :pswitch_3f
    const/4 v3, 0x3

    #@40
    goto :goto_1f

    #@41
    .line 1282
    :sswitch_41
    const/high16 v4, 0x6

    #@43
    goto :goto_22

    #@44
    .line 1283
    :sswitch_44
    const/high16 v4, 0x4

    #@46
    goto :goto_22

    #@47
    .line 1298
    :cond_47
    add-int v0, p2, p3

    #@49
    .line 1299
    .restart local v0       #e:I
    const v10, 0xffff

    #@4c
    invoke-static {p1, p3, v10}, Landroid/icu/text/ArabicShaping;->countSpaceSub([CIC)I

    #@4f
    move-result v6

    #@50
    .line 1300
    .local v6, wL:I
    const v10, 0xfffe

    #@53
    invoke-static {p1, p3, v10}, Landroid/icu/text/ArabicShaping;->countSpaceSub([CIC)I

    #@56
    move-result v8

    #@57
    .line 1302
    .local v8, wT:I
    const/4 v10, 0x2

    #@58
    if-ne v3, v10, :cond_5b

    #@5a
    .line 1303
    const/4 v2, 0x1

    #@5b
    .line 1305
    :cond_5b
    const/high16 v10, 0x6

    #@5d
    if-ne v4, v10, :cond_60

    #@5f
    .line 1306
    const/4 v5, 0x1

    #@60
    .line 1310
    :cond_60
    if-eqz v2, :cond_74

    #@62
    const/4 v10, 0x2

    #@63
    if-ne v3, v10, :cond_74

    #@65
    .line 1311
    const v10, 0xffff

    #@68
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@6b
    .line 1312
    :goto_6b
    if-le v6, p2, :cond_74

    #@6d
    .line 1313
    add-int/lit8 v6, v6, -0x1

    #@6f
    const/16 v10, 0x20

    #@71
    aput-char v10, p1, v6

    #@73
    goto :goto_6b

    #@74
    .line 1317
    :cond_74
    if-eqz v5, :cond_89

    #@76
    const/high16 v10, 0x6

    #@78
    if-ne v4, v10, :cond_89

    #@7a
    .line 1318
    const v10, 0xfffe

    #@7d
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@80
    .line 1319
    :goto_80
    if-le v8, p2, :cond_89

    #@82
    .line 1320
    add-int/lit8 v8, v8, -0x1

    #@84
    const/16 v10, 0x20

    #@86
    aput-char v10, p1, v8

    #@88
    goto :goto_80

    #@89
    .line 1324
    :cond_89
    const/4 v2, 0x0

    #@8a
    .line 1325
    const/4 v5, 0x0

    #@8b
    .line 1327
    if-nez v3, :cond_8e

    #@8d
    .line 1328
    const/4 v2, 0x1

    #@8e
    .line 1330
    :cond_8e
    const/high16 v10, 0x8

    #@90
    if-ne v4, v10, :cond_93

    #@92
    .line 1331
    const/4 v5, 0x1

    #@93
    .line 1334
    :cond_93
    if-eqz v2, :cond_a3

    #@95
    if-nez v3, :cond_a3

    #@97
    .line 1335
    const v10, 0xffff

    #@9a
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@9d
    .line 1336
    invoke-static {p1, p2, v0, v6}, Landroid/icu/text/ArabicShaping;->flipArray([CIII)I

    #@a0
    move-result v6

    #@a1
    .line 1337
    sub-int p3, v6, p2

    #@a3
    .line 1339
    :cond_a3
    if-eqz v5, :cond_b5

    #@a5
    const/high16 v10, 0x8

    #@a7
    if-ne v4, v10, :cond_b5

    #@a9
    .line 1340
    const v10, 0xfffe

    #@ac
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@af
    .line 1341
    invoke-static {p1, p2, v0, v8}, Landroid/icu/text/ArabicShaping;->flipArray([CIII)I

    #@b2
    move-result v8

    #@b3
    .line 1342
    sub-int p3, v8, p2

    #@b5
    .line 1345
    :cond_b5
    const/4 v2, 0x0

    #@b6
    .line 1346
    const/4 v5, 0x0

    #@b7
    .line 1348
    const/4 v10, 0x3

    #@b8
    if-eq v3, v10, :cond_be

    #@ba
    const/high16 v10, 0x1

    #@bc
    if-ne v3, v10, :cond_bf

    #@be
    .line 1350
    :cond_be
    const/4 v2, 0x1

    #@bf
    .line 1352
    :cond_bf
    const/high16 v10, 0x4

    #@c1
    if-ne v4, v10, :cond_c4

    #@c3
    .line 1353
    const/4 v5, 0x1

    #@c4
    .line 1356
    :cond_c4
    if-eqz v2, :cond_e3

    #@c6
    const/4 v10, 0x3

    #@c7
    if-eq v3, v10, :cond_cd

    #@c9
    const/high16 v10, 0x1

    #@cb
    if-ne v3, v10, :cond_e3

    #@cd
    .line 1358
    :cond_cd
    const v10, 0xffff

    #@d0
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@d3
    .line 1359
    invoke-static {p1, p2, v0, v6}, Landroid/icu/text/ArabicShaping;->flipArray([CIII)I

    #@d6
    move-result v6

    #@d7
    move v7, v6

    #@d8
    .line 1360
    .end local v6           #wL:I
    .local v7, wL:I
    :goto_d8
    if-ge v7, v0, :cond_e2

    #@da
    .line 1361
    add-int/lit8 v6, v7, 0x1

    #@dc
    .end local v7           #wL:I
    .restart local v6       #wL:I
    const/16 v10, 0x20

    #@de
    aput-char v10, p1, v7

    #@e0
    move v7, v6

    #@e1
    .end local v6           #wL:I
    .restart local v7       #wL:I
    goto :goto_d8

    #@e2
    :cond_e2
    move v6, v7

    #@e3
    .line 1364
    .end local v7           #wL:I
    .restart local v6       #wL:I
    :cond_e3
    if-eqz v5, :cond_fe

    #@e5
    const/high16 v10, 0x4

    #@e7
    if-ne v4, v10, :cond_fe

    #@e9
    .line 1365
    const v10, 0xfffe

    #@ec
    invoke-static {p1, p2, v0, v10}, Landroid/icu/text/ArabicShaping;->shiftArray([CIIC)V

    #@ef
    .line 1366
    invoke-static {p1, p2, v0, v8}, Landroid/icu/text/ArabicShaping;->flipArray([CIII)I

    #@f2
    move-result v8

    #@f3
    move v9, v8

    #@f4
    .line 1367
    .end local v8           #wT:I
    .local v9, wT:I
    :goto_f4
    if-ge v9, v0, :cond_fe

    #@f6
    .line 1368
    add-int/lit8 v8, v9, 0x1

    #@f8
    .end local v9           #wT:I
    .restart local v8       #wT:I
    const/16 v10, 0x20

    #@fa
    aput-char v10, p1, v9

    #@fc
    move v9, v8

    #@fd
    .end local v8           #wT:I
    .restart local v9       #wT:I
    goto :goto_f4

    #@fe
    .line 1373
    .end local v6           #wL:I
    .end local v9           #wT:I
    :cond_fe
    return p3

    #@ff
    .line 1276
    nop

    #@100
    :pswitch_data_100
    .packed-switch 0x2
        :pswitch_3f
        :pswitch_3d
    .end packed-switch

    #@108
    .line 1281
    :sswitch_data_108
    .sparse-switch
        0x40000 -> :sswitch_41
        0x60000 -> :sswitch_44
    .end sparse-switch
.end method

.method private static handleTashkeelWithTatweel([CI)I
    .registers 6
    .parameter "dest"
    .parameter "sourceLength"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1236
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, p1, :cond_36

    #@4
    .line 1237
    aget-char v1, p0, v0

    #@6
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isTashkeelOnTatweelChar(C)I

    #@9
    move-result v1

    #@a
    if-ne v1, v3, :cond_13

    #@c
    .line 1238
    const/16 v1, 0x640

    #@e
    aput-char v1, p0, v0

    #@10
    .line 1236
    :cond_10
    :goto_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_2

    #@13
    .line 1239
    :cond_13
    aget-char v1, p0, v0

    #@15
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isTashkeelOnTatweelChar(C)I

    #@18
    move-result v1

    #@19
    const/4 v2, 0x2

    #@1a
    if-ne v1, v2, :cond_22

    #@1c
    .line 1240
    const v1, 0xfe7d

    #@1f
    aput-char v1, p0, v0

    #@21
    goto :goto_10

    #@22
    .line 1241
    :cond_22
    aget-char v1, p0, v0

    #@24
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isIsolatedTashkeelChar(C)I

    #@27
    move-result v1

    #@28
    if-ne v1, v3, :cond_10

    #@2a
    aget-char v1, p0, v0

    #@2c
    const v2, 0xfe7c

    #@2f
    if-eq v1, v2, :cond_10

    #@31
    .line 1242
    const/16 v1, 0x20

    #@33
    aput-char v1, p0, v0

    #@35
    goto :goto_10

    #@36
    .line 1245
    :cond_36
    return p1
.end method

.method private internalShape([CII[CII)I
    .registers 18
    .parameter "source"
    .parameter "sourceStart"
    .parameter "sourceLength"
    .parameter "dest"
    .parameter "destStart"
    .parameter "destSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1822
    if-nez p3, :cond_4

    #@2
    .line 1823
    const/4 p3, 0x0

    #@3
    .line 1939
    .end local p3
    :cond_3
    :goto_3
    return p3

    #@4
    .line 1826
    .restart local p3
    :cond_4
    if-nez p6, :cond_19

    #@6
    .line 1827
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@8
    and-int/lit8 v1, v1, 0x18

    #@a
    if-eqz v1, :cond_3

    #@c
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@e
    const v3, 0x10003

    #@11
    and-int/2addr v1, v3

    #@12
    if-nez v1, :cond_3

    #@14
    .line 1830
    invoke-direct {p0, p1, p2, p3}, Landroid/icu/text/ArabicShaping;->calculateSize([CII)I

    #@17
    move-result p3

    #@18
    goto :goto_3

    #@19
    .line 1837
    :cond_19
    mul-int/lit8 v1, p3, 0x2

    #@1b
    new-array v2, v1, [C

    #@1d
    .line 1838
    .local v2, temp:[C
    const/4 v1, 0x0

    #@1e
    invoke-static {p1, p2, v2, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@21
    .line 1840
    iget-boolean v1, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@23
    if-eqz v1, :cond_29

    #@25
    .line 1841
    const/4 v1, 0x0

    #@26
    invoke-static {v2, v1, p3}, Landroid/icu/text/ArabicShaping;->invertBuffer([CII)V

    #@29
    .line 1844
    :cond_29
    move v4, p3

    #@2a
    .line 1846
    .local v4, outputSize:I
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2c
    and-int/lit8 v1, v1, 0x18

    #@2e
    sparse-switch v1, :sswitch_data_ee

    #@31
    .line 1875
    :cond_31
    :goto_31
    move/from16 v0, p6

    #@33
    if-le v4, v0, :cond_84

    #@35
    .line 1876
    new-instance v1, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@37
    const-string/jumbo v3, "not enough room for result data"

    #@3a
    invoke-direct {v1, v3}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v1

    #@3e
    .line 1848
    :sswitch_3e
    const/4 v3, 0x0

    #@3f
    const/4 v6, 0x1

    #@40
    move-object v1, p0

    #@41
    move v4, p3

    #@42
    move/from16 v5, p6

    #@44
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->shapeUnicode([CIIII)I

    #@47
    .end local v4           #outputSize:I
    move-result v4

    #@48
    .line 1849
    .restart local v4       #outputSize:I
    goto :goto_31

    #@49
    .line 1852
    :sswitch_49
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@4b
    const/high16 v3, 0xe

    #@4d
    and-int/2addr v1, v3

    #@4e
    if-lez v1, :cond_64

    #@50
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@52
    const/high16 v3, 0xe

    #@54
    and-int/2addr v1, v3

    #@55
    const/high16 v3, 0xc

    #@57
    if-eq v1, v3, :cond_64

    #@59
    .line 1855
    const/4 v3, 0x0

    #@5a
    const/4 v6, 0x2

    #@5b
    move-object v1, p0

    #@5c
    move v4, p3

    #@5d
    move/from16 v5, p6

    #@5f
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->shapeUnicode([CIIII)I

    #@62
    .end local v4           #outputSize:I
    move-result v4

    #@63
    .restart local v4       #outputSize:I
    goto :goto_31

    #@64
    .line 1858
    :cond_64
    const/4 v3, 0x0

    #@65
    const/4 v6, 0x0

    #@66
    move-object v1, p0

    #@67
    move v4, p3

    #@68
    move/from16 v5, p6

    #@6a
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->shapeUnicode([CIIII)I

    #@6d
    .end local v4           #outputSize:I
    move-result v4

    #@6e
    .line 1861
    .restart local v4       #outputSize:I
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@70
    const/high16 v3, 0xe

    #@72
    and-int/2addr v1, v3

    #@73
    const/high16 v3, 0xc

    #@75
    if-ne v1, v3, :cond_31

    #@77
    .line 1862
    invoke-static {v2, p3}, Landroid/icu/text/ArabicShaping;->handleTashkeelWithTatweel([CI)I

    #@7a
    move-result v4

    #@7b
    goto :goto_31

    #@7c
    .line 1868
    :sswitch_7c
    const/4 v1, 0x0

    #@7d
    move/from16 v0, p6

    #@7f
    invoke-direct {p0, v2, v1, p3, v0}, Landroid/icu/text/ArabicShaping;->deShapeUnicode([CIII)I

    #@82
    move-result v4

    #@83
    .line 1869
    goto :goto_31

    #@84
    .line 1879
    :cond_84
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@86
    and-int/lit16 v1, v1, 0xe0

    #@88
    if-eqz v1, :cond_9a

    #@8a
    .line 1880
    const/16 v5, 0x30

    #@8c
    .line 1881
    .local v5, digitBase:C
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@8e
    and-int/lit16 v1, v1, 0x100

    #@90
    sparse-switch v1, :sswitch_data_fc

    #@93
    .line 1894
    :goto_93
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@95
    and-int/lit16 v1, v1, 0xe0

    #@97
    sparse-switch v1, :sswitch_data_106

    #@9a
    .line 1933
    .end local v5           #digitBase:C
    :cond_9a
    :goto_9a
    iget-boolean v1, p0, Landroid/icu/text/ArabicShaping;->isLogical:Z

    #@9c
    if-eqz v1, :cond_a2

    #@9e
    .line 1934
    const/4 v1, 0x0

    #@9f
    invoke-static {v2, v1, v4}, Landroid/icu/text/ArabicShaping;->invertBuffer([CII)V

    #@a2
    .line 1937
    :cond_a2
    const/4 v1, 0x0

    #@a3
    move/from16 v0, p5

    #@a5
    invoke-static {v2, v1, p4, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a8
    move p3, v4

    #@a9
    .line 1939
    goto/16 :goto_3

    #@ab
    .line 1883
    .restart local v5       #digitBase:C
    :sswitch_ab
    const/16 v5, 0x660

    #@ad
    .line 1884
    goto :goto_93

    #@ae
    .line 1887
    :sswitch_ae
    const/16 v5, 0x6f0

    #@b0
    .line 1888
    goto :goto_93

    #@b1
    .line 1897
    :sswitch_b1
    add-int/lit8 v8, v5, -0x30

    #@b3
    .line 1898
    .local v8, digitDelta:I
    const/4 v10, 0x0

    #@b4
    .local v10, i:I
    :goto_b4
    if-ge v10, v4, :cond_9a

    #@b6
    .line 1899
    aget-char v7, v2, v10

    #@b8
    .line 1900
    .local v7, ch:C
    const/16 v1, 0x39

    #@ba
    if-gt v7, v1, :cond_c6

    #@bc
    const/16 v1, 0x30

    #@be
    if-lt v7, v1, :cond_c6

    #@c0
    .line 1901
    aget-char v1, v2, v10

    #@c2
    add-int/2addr v1, v8

    #@c3
    int-to-char v1, v1

    #@c4
    aput-char v1, v2, v10

    #@c6
    .line 1898
    :cond_c6
    add-int/lit8 v10, v10, 0x1

    #@c8
    goto :goto_b4

    #@c9
    .line 1909
    .end local v7           #ch:C
    .end local v8           #digitDelta:I
    .end local v10           #i:I
    :sswitch_c9
    add-int/lit8 v1, v5, 0x9

    #@cb
    int-to-char v9, v1

    #@cc
    .line 1910
    .local v9, digitTop:C
    rsub-int/lit8 v8, v5, 0x30

    #@ce
    .line 1911
    .restart local v8       #digitDelta:I
    const/4 v10, 0x0

    #@cf
    .restart local v10       #i:I
    :goto_cf
    if-ge v10, v4, :cond_9a

    #@d1
    .line 1912
    aget-char v7, v2, v10

    #@d3
    .line 1913
    .restart local v7       #ch:C
    if-gt v7, v9, :cond_dd

    #@d5
    if-lt v7, v5, :cond_dd

    #@d7
    .line 1914
    aget-char v1, v2, v10

    #@d9
    add-int/2addr v1, v8

    #@da
    int-to-char v1, v1

    #@db
    aput-char v1, v2, v10

    #@dd
    .line 1911
    :cond_dd
    add-int/lit8 v10, v10, 0x1

    #@df
    goto :goto_cf

    #@e0
    .line 1921
    .end local v7           #ch:C
    .end local v8           #digitDelta:I
    .end local v9           #digitTop:C
    .end local v10           #i:I
    :sswitch_e0
    const/4 v3, 0x0

    #@e1
    const/4 v6, 0x0

    #@e2
    move-object v1, p0

    #@e3
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->shapeToArabicDigitsWithContext([CIICZ)V

    #@e6
    goto :goto_9a

    #@e7
    .line 1925
    :sswitch_e7
    const/4 v3, 0x0

    #@e8
    const/4 v6, 0x1

    #@e9
    move-object v1, p0

    #@ea
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->shapeToArabicDigitsWithContext([CIICZ)V

    #@ed
    goto :goto_9a

    #@ee
    .line 1846
    :sswitch_data_ee
    .sparse-switch
        0x8 -> :sswitch_49
        0x10 -> :sswitch_7c
        0x18 -> :sswitch_3e
    .end sparse-switch

    #@fc
    .line 1881
    :sswitch_data_fc
    .sparse-switch
        0x0 -> :sswitch_ab
        0x100 -> :sswitch_ae
    .end sparse-switch

    #@106
    .line 1894
    :sswitch_data_106
    .sparse-switch
        0x20 -> :sswitch_b1
        0x40 -> :sswitch_c9
        0x60 -> :sswitch_e0
        0x80 -> :sswitch_e7
    .end sparse-switch
.end method

.method private static invertBuffer([CII)V
    .registers 7
    .parameter "buffer"
    .parameter "start"
    .parameter "length"

    #@0
    .prologue
    .line 896
    move v0, p1

    #@1
    .local v0, i:I
    add-int v3, p1, p2

    #@3
    add-int/lit8 v1, v3, -0x1

    #@5
    .local v1, j:I
    :goto_5
    if-ge v0, v1, :cond_14

    #@7
    .line 897
    aget-char v2, p0, v0

    #@9
    .line 898
    .local v2, temp:C
    aget-char v3, p0, v1

    #@b
    aput-char v3, p0, v0

    #@d
    .line 899
    aput-char v2, p0, v1

    #@f
    .line 896
    add-int/lit8 v0, v0, 0x1

    #@11
    add-int/lit8 v1, v1, -0x1

    #@13
    goto :goto_5

    #@14
    .line 901
    .end local v2           #temp:C
    :cond_14
    return-void
.end method

.method private static isAlefChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1112
    const/16 v0, 0x622

    #@2
    if-eq p0, v0, :cond_10

    #@4
    const/16 v0, 0x623

    #@6
    if-eq p0, v0, :cond_10

    #@8
    const/16 v0, 0x625

    #@a
    if-eq p0, v0, :cond_10

    #@c
    const/16 v0, 0x627

    #@e
    if-ne p0, v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method private static isAlefMaksouraChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1047
    const v0, 0xfeef

    #@3
    if-eq p0, v0, :cond_e

    #@5
    const v0, 0xfef0

    #@8
    if-eq p0, v0, :cond_e

    #@a
    const/16 v0, 0x649

    #@c
    if-ne p0, v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method private static isIsolatedTashkeelChar(C)I
    .registers 3
    .parameter "ch"

    #@0
    .prologue
    const v1, 0xfe70

    #@3
    .line 1098
    if-lt p0, v1, :cond_1d

    #@5
    const v0, 0xfe7f

    #@8
    if-gt p0, v0, :cond_1d

    #@a
    const v0, 0xfe73

    #@d
    if-eq p0, v0, :cond_1d

    #@f
    const v0, 0xfe75

    #@12
    if-eq p0, v0, :cond_1d

    #@14
    .line 1099
    sget-object v0, Landroid/icu/text/ArabicShaping;->tashkeelMedial:[I

    #@16
    sub-int v1, p0, v1

    #@18
    aget v0, v0, v1

    #@1a
    rsub-int/lit8 v0, v0, 0x1

    #@1c
    .line 1103
    :goto_1c
    return v0

    #@1d
    .line 1100
    :cond_1d
    const v0, 0xfc5e

    #@20
    if-lt p0, v0, :cond_29

    #@22
    const v0, 0xfc63

    #@25
    if-gt p0, v0, :cond_29

    #@27
    .line 1101
    const/4 v0, 0x1

    #@28
    goto :goto_1c

    #@29
    .line 1103
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_1c
.end method

.method private static isLamAlefChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1120
    const v0, 0xfef5

    #@3
    if-lt p0, v0, :cond_c

    #@5
    const v0, 0xfefc

    #@8
    if-gt p0, v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private static isNormalizedLamAlefChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1124
    const/16 v0, 0x65c

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x65f

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isSeenFamilyChar(C)I
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1020
    const/16 v0, 0x633

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x636

    #@6
    if-gt p0, v0, :cond_a

    #@8
    .line 1021
    const/4 v0, 0x1

    #@9
    .line 1023
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isSeenTailFamilyChar(C)I
    .registers 3
    .parameter "ch"

    #@0
    .prologue
    const v1, 0xfeb1

    #@3
    .line 1007
    if-lt p0, v1, :cond_11

    #@5
    const v0, 0xfebf

    #@8
    if-ge p0, v0, :cond_11

    #@a
    .line 1008
    sget-object v0, Landroid/icu/text/ArabicShaping;->tailFamilyIsolatedFinal:[I

    #@c
    sub-int v1, p0, v1

    #@e
    aget v0, v0, v1

    #@10
    .line 1010
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private static isTailChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1034
    const/16 v0, 0x200b

    #@2
    if-eq p0, v0, :cond_9

    #@4
    const v0, 0xfe73

    #@7
    if-ne p0, v0, :cond_b

    #@9
    .line 1035
    :cond_9
    const/4 v0, 0x1

    #@a
    .line 1037
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method private static isTashkeelChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 997
    const/16 v0, 0x64b

    #@2
    if-lt p0, v0, :cond_a

    #@4
    const/16 v0, 0x652

    #@6
    if-gt p0, v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private static isTashkeelCharFE(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1069
    const v0, 0xfe75

    #@3
    if-eq p0, v0, :cond_11

    #@5
    const v0, 0xfe70

    #@8
    if-lt p0, v0, :cond_11

    #@a
    const v0, 0xfe7f

    #@d
    if-gt p0, v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method private static isTashkeelOnTatweelChar(C)I
    .registers 4
    .parameter "ch"

    #@0
    .prologue
    const v2, 0xfe7d

    #@3
    const v1, 0xfe70

    #@6
    .line 1080
    if-lt p0, v1, :cond_20

    #@8
    const v0, 0xfe7f

    #@b
    if-gt p0, v0, :cond_20

    #@d
    const v0, 0xfe73

    #@10
    if-eq p0, v0, :cond_20

    #@12
    const v0, 0xfe75

    #@15
    if-eq p0, v0, :cond_20

    #@17
    if-eq p0, v2, :cond_20

    #@19
    .line 1082
    sget-object v0, Landroid/icu/text/ArabicShaping;->tashkeelMedial:[I

    #@1b
    sub-int v1, p0, v1

    #@1d
    aget v0, v0, v1

    #@1f
    .line 1086
    :goto_1f
    return v0

    #@20
    .line 1083
    :cond_20
    const v0, 0xfcf2

    #@23
    if-lt p0, v0, :cond_2a

    #@25
    const v0, 0xfcf4

    #@28
    if-le p0, v0, :cond_2c

    #@2a
    :cond_2a
    if-ne p0, v2, :cond_2e

    #@2c
    .line 1084
    :cond_2c
    const/4 v0, 0x2

    #@2d
    goto :goto_1f

    #@2e
    .line 1086
    :cond_2e
    const/4 v0, 0x0

    #@2f
    goto :goto_1f
.end method

.method private static isYehHamzaChar(C)Z
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 1056
    const v0, 0xfe89

    #@3
    if-eq p0, v0, :cond_a

    #@5
    const v0, 0xfe8a

    #@8
    if-ne p0, v0, :cond_c

    #@a
    .line 1057
    :cond_a
    const/4 v0, 0x1

    #@b
    .line 1059
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private normalize([CII)I
    .registers 11
    .parameter "dest"
    .parameter "start"
    .parameter "length"

    #@0
    .prologue
    const v6, 0xfe70

    #@3
    .line 1592
    const/4 v3, 0x0

    #@4
    .line 1593
    .local v3, lacount:I
    move v2, p2

    #@5
    .local v2, i:I
    add-int v1, v2, p3

    #@7
    .local v1, e:I
    :goto_7
    if-ge v2, v1, :cond_26

    #@9
    .line 1594
    aget-char v0, p1, v2

    #@b
    .line 1595
    .local v0, ch:C
    if-lt v0, v6, :cond_23

    #@d
    const v4, 0xfefc

    #@10
    if-gt v0, v4, :cond_23

    #@12
    .line 1596
    invoke-static {v0}, Landroid/icu/text/ArabicShaping;->isLamAlefChar(C)Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_1a

    #@18
    .line 1597
    add-int/lit8 v3, v3, 0x1

    #@1a
    .line 1599
    :cond_1a
    sget-object v4, Landroid/icu/text/ArabicShaping;->convertFEto06:[I

    #@1c
    sub-int v5, v0, v6

    #@1e
    aget v4, v4, v5

    #@20
    int-to-char v4, v4

    #@21
    aput-char v4, p1, v2

    #@23
    .line 1593
    :cond_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_7

    #@26
    .line 1602
    .end local v0           #ch:C
    :cond_26
    return v3
.end method

.method private shapeToArabicDigitsWithContext([CIICZ)V
    .registers 9
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "digitBase"
    .parameter "lastStrongWasAL"

    #@0
    .prologue
    .line 863
    add-int/lit8 v2, p4, -0x30

    #@2
    int-to-char p4, v2

    #@3
    .line 865
    add-int v1, p2, p3

    #@5
    .local v1, i:I
    :cond_5
    :goto_5
    add-int/lit8 v1, v1, -0x1

    #@7
    if-lt v1, p2, :cond_23

    #@9
    .line 866
    aget-char v0, p1, v1

    #@b
    .line 867
    .local v0, ch:C
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(C)B

    #@e
    move-result v2

    #@f
    packed-switch v2, :pswitch_data_24

    #@12
    goto :goto_5

    #@13
    .line 870
    :pswitch_13
    const/4 p5, 0x0

    #@14
    .line 871
    goto :goto_5

    #@15
    .line 873
    :pswitch_15
    const/4 p5, 0x1

    #@16
    .line 874
    goto :goto_5

    #@17
    .line 876
    :pswitch_17
    if-eqz p5, :cond_5

    #@19
    const/16 v2, 0x39

    #@1b
    if-gt v0, v2, :cond_5

    #@1d
    .line 877
    add-int v2, v0, p4

    #@1f
    int-to-char v2, v2

    #@20
    aput-char v2, p1, v1

    #@22
    goto :goto_5

    #@23
    .line 884
    .end local v0           #ch:C
    :cond_23
    return-void

    #@24
    .line 867
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_17
    .end packed-switch
.end method

.method private shapeUnicode([CIIII)I
    .registers 28
    .parameter "dest"
    .parameter "start"
    .parameter "length"
    .parameter "destSize"
    .parameter "tashkeelFlag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    .line 1654
    invoke-direct/range {p0 .. p3}, Landroid/icu/text/ArabicShaping;->normalize([CII)I

    #@3
    move-result v5

    #@4
    .line 1661
    .local v5, lamalef_count:I
    const/4 v10, 0x0

    #@5
    .local v10, lamalef_found:Z
    const/16 v17, 0x0

    #@7
    .line 1662
    .local v17, seenfam_found:Z
    const/16 v21, 0x0

    #@9
    .local v21, yehhamza_found:Z
    const/16 v19, 0x0

    #@b
    .line 1663
    .local v19, tashkeel_found:Z
    add-int v1, p2, p3

    #@d
    add-int/lit8 v9, v1, -0x1

    #@f
    .line 1664
    .local v9, i:I
    aget-char v1, p1, v9

    #@11
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->getLink(C)I

    #@14
    move-result v7

    #@15
    .line 1665
    .local v7, currLink:I
    const/4 v13, 0x0

    #@16
    .line 1666
    .local v13, nextLink:I
    const/16 v16, 0x0

    #@18
    .line 1667
    .local v16, prevLink:I
    const/4 v11, 0x0

    #@19
    .line 1669
    .local v11, lastLink:I
    move v12, v9

    #@1a
    .line 1670
    .local v12, lastPos:I
    const/4 v15, -0x2

    #@1b
    .line 1671
    .local v15, nx:I
    const/4 v14, 0x0

    #@1c
    .line 1673
    .local v14, nw:I
    :cond_1c
    :goto_1c
    if-ltz v9, :cond_128

    #@1e
    .line 1675
    const v1, 0xff00

    #@21
    and-int/2addr v1, v7

    #@22
    if-gtz v1, :cond_2c

    #@24
    aget-char v1, p1, v9

    #@26
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isTashkeelChar(C)Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_a4

    #@2c
    .line 1676
    :cond_2c
    add-int/lit8 v14, v9, -0x1

    #@2e
    .line 1677
    const/4 v15, -0x2

    #@2f
    .line 1678
    :goto_2f
    if-gez v15, :cond_48

    #@31
    .line 1679
    const/4 v1, -0x1

    #@32
    if-ne v14, v1, :cond_39

    #@34
    .line 1680
    const/4 v13, 0x0

    #@35
    .line 1681
    const v15, 0x7fffffff

    #@38
    goto :goto_2f

    #@39
    .line 1683
    :cond_39
    aget-char v1, p1, v14

    #@3b
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->getLink(C)I

    #@3e
    move-result v13

    #@3f
    .line 1684
    and-int/lit8 v1, v13, 0x4

    #@41
    if-nez v1, :cond_45

    #@43
    .line 1685
    move v15, v14

    #@44
    goto :goto_2f

    #@45
    .line 1687
    :cond_45
    add-int/lit8 v14, v14, -0x1

    #@47
    goto :goto_2f

    #@48
    .line 1692
    :cond_48
    and-int/lit8 v1, v7, 0x20

    #@4a
    if-lez v1, :cond_67

    #@4c
    and-int/lit8 v1, v11, 0x10

    #@4e
    if-lez v1, :cond_67

    #@50
    .line 1693
    const/4 v10, 0x1

    #@51
    .line 1694
    aget-char v1, p1, v9

    #@53
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->changeLamAlef(C)C

    #@56
    move-result v20

    #@57
    .line 1695
    .local v20, wLamalef:C
    if-eqz v20, :cond_61

    #@59
    .line 1697
    const v1, 0xffff

    #@5c
    aput-char v1, p1, v9

    #@5e
    .line 1698
    aput-char v20, p1, v12

    #@60
    .line 1699
    move v9, v12

    #@61
    .line 1702
    :cond_61
    move/from16 v11, v16

    #@63
    .line 1703
    invoke-static/range {v20 .. v20}, Landroid/icu/text/ArabicShaping;->getLink(C)I

    #@66
    move-result v7

    #@67
    .line 1705
    .end local v20           #wLamalef:C
    :cond_67
    if-lez v9, :cond_bd

    #@69
    add-int/lit8 v1, v9, -0x1

    #@6b
    aget-char v1, p1, v1

    #@6d
    const/16 v2, 0x20

    #@6f
    if-ne v1, v2, :cond_bd

    #@71
    .line 1707
    aget-char v1, p1, v9

    #@73
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isSeenFamilyChar(C)I

    #@76
    move-result v1

    #@77
    const/4 v2, 0x1

    #@78
    if-ne v1, v2, :cond_b4

    #@7a
    .line 1708
    const/16 v17, 0x1

    #@7c
    .line 1726
    :cond_7c
    :goto_7c
    aget-char v1, p1, v9

    #@7e
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->specialChar(C)I

    #@81
    move-result v8

    #@82
    .line 1728
    .local v8, flag:I
    sget-object v1, Landroid/icu/text/ArabicShaping;->shapeTable:[[[I

    #@84
    and-int/lit8 v2, v13, 0x3

    #@86
    aget-object v1, v1, v2

    #@88
    and-int/lit8 v2, v11, 0x3

    #@8a
    aget-object v1, v1, v2

    #@8c
    and-int/lit8 v2, v7, 0x3

    #@8e
    aget v18, v1, v2

    #@90
    .line 1732
    .local v18, shape:I
    const/4 v1, 0x1

    #@91
    if-ne v8, v1, :cond_d4

    #@93
    .line 1733
    and-int/lit8 v18, v18, 0x1

    #@95
    .line 1748
    :cond_95
    :goto_95
    const/4 v1, 0x2

    #@96
    if-ne v8, v1, :cond_111

    #@98
    .line 1749
    const/4 v1, 0x2

    #@99
    move/from16 v0, p5

    #@9b
    if-ne v0, v1, :cond_ff

    #@9d
    .line 1750
    const v1, 0xfffe

    #@a0
    aput-char v1, p1, v9

    #@a2
    .line 1751
    const/16 v19, 0x1

    #@a4
    .line 1763
    .end local v8           #flag:I
    .end local v18           #shape:I
    :cond_a4
    :goto_a4
    and-int/lit8 v1, v7, 0x4

    #@a6
    if-nez v1, :cond_ac

    #@a8
    .line 1764
    move/from16 v16, v11

    #@aa
    .line 1765
    move v11, v7

    #@ab
    .line 1767
    move v12, v9

    #@ac
    .line 1770
    :cond_ac
    add-int/lit8 v9, v9, -0x1

    #@ae
    .line 1771
    if-ne v9, v15, :cond_11d

    #@b0
    .line 1772
    move v7, v13

    #@b1
    .line 1773
    const/4 v15, -0x2

    #@b2
    goto/16 :goto_1c

    #@b4
    .line 1709
    :cond_b4
    aget-char v1, p1, v9

    #@b6
    const/16 v2, 0x626

    #@b8
    if-ne v1, v2, :cond_7c

    #@ba
    .line 1710
    const/16 v21, 0x1

    #@bc
    goto :goto_7c

    #@bd
    .line 1713
    :cond_bd
    if-nez v9, :cond_7c

    #@bf
    .line 1714
    aget-char v1, p1, v9

    #@c1
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->isSeenFamilyChar(C)I

    #@c4
    move-result v1

    #@c5
    const/4 v2, 0x1

    #@c6
    if-ne v1, v2, :cond_cb

    #@c8
    .line 1715
    const/16 v17, 0x1

    #@ca
    goto :goto_7c

    #@cb
    .line 1716
    :cond_cb
    aget-char v1, p1, v9

    #@cd
    const/16 v2, 0x626

    #@cf
    if-ne v1, v2, :cond_7c

    #@d1
    .line 1717
    const/16 v21, 0x1

    #@d3
    goto :goto_7c

    #@d4
    .line 1734
    .restart local v8       #flag:I
    .restart local v18       #shape:I
    :cond_d4
    const/4 v1, 0x2

    #@d5
    if-ne v8, v1, :cond_95

    #@d7
    .line 1735
    if-nez p5, :cond_fc

    #@d9
    and-int/lit8 v1, v11, 0x2

    #@db
    if-eqz v1, :cond_fc

    #@dd
    and-int/lit8 v1, v13, 0x1

    #@df
    if-eqz v1, :cond_fc

    #@e1
    aget-char v1, p1, v9

    #@e3
    const/16 v2, 0x64c

    #@e5
    if-eq v1, v2, :cond_fc

    #@e7
    aget-char v1, p1, v9

    #@e9
    const/16 v2, 0x64d

    #@eb
    if-eq v1, v2, :cond_fc

    #@ed
    and-int/lit8 v1, v13, 0x20

    #@ef
    const/16 v2, 0x20

    #@f1
    if-ne v1, v2, :cond_f9

    #@f3
    and-int/lit8 v1, v11, 0x10

    #@f5
    const/16 v2, 0x10

    #@f7
    if-eq v1, v2, :cond_fc

    #@f9
    .line 1743
    :cond_f9
    const/16 v18, 0x1

    #@fb
    goto :goto_95

    #@fc
    .line 1745
    :cond_fc
    const/16 v18, 0x0

    #@fe
    goto :goto_95

    #@ff
    .line 1754
    :cond_ff
    const v1, 0xfe70

    #@102
    sget-object v2, Landroid/icu/text/ArabicShaping;->irrelevantPos:[I

    #@104
    aget-char v3, p1, v9

    #@106
    add-int/lit16 v3, v3, -0x64b

    #@108
    aget v2, v2, v3

    #@10a
    add-int/2addr v1, v2

    #@10b
    add-int v1, v1, v18

    #@10d
    int-to-char v1, v1

    #@10e
    aput-char v1, p1, v9

    #@110
    goto :goto_a4

    #@111
    .line 1758
    :cond_111
    const v1, 0xfe70

    #@114
    shr-int/lit8 v2, v7, 0x8

    #@116
    add-int/2addr v1, v2

    #@117
    add-int v1, v1, v18

    #@119
    int-to-char v1, v1

    #@11a
    aput-char v1, p1, v9

    #@11c
    goto :goto_a4

    #@11d
    .line 1774
    .end local v8           #flag:I
    .end local v18           #shape:I
    :cond_11d
    const/4 v1, -0x1

    #@11e
    if-eq v9, v1, :cond_1c

    #@120
    .line 1775
    aget-char v1, p1, v9

    #@122
    invoke-static {v1}, Landroid/icu/text/ArabicShaping;->getLink(C)I

    #@125
    move-result v7

    #@126
    goto/16 :goto_1c

    #@128
    .line 1782
    :cond_128
    move/from16 p4, p3

    #@12a
    .line 1783
    if-nez v10, :cond_12e

    #@12c
    if-eqz v19, :cond_132

    #@12e
    .line 1784
    :cond_12e
    invoke-direct/range {p0 .. p3}, Landroid/icu/text/ArabicShaping;->handleGeneratedSpaces([CII)I

    #@131
    move-result p4

    #@132
    .line 1786
    :cond_132
    if-nez v17, :cond_136

    #@134
    if-eqz v21, :cond_143

    #@136
    .line 1787
    :cond_136
    const/4 v6, 0x0

    #@137
    move-object/from16 v1, p0

    #@139
    move-object/from16 v2, p1

    #@13b
    move/from16 v3, p2

    #@13d
    move/from16 v4, p4

    #@13f
    invoke-direct/range {v1 .. v6}, Landroid/icu/text/ArabicShaping;->expandCompositChar([CIIII)I

    #@142
    move-result p4

    #@143
    .line 1789
    :cond_143
    return p4
.end method

.method public static shiftArray([CIIC)V
    .registers 7
    .parameter "dest"
    .parameter "start"
    .parameter "e"
    .parameter "subChar"

    #@0
    .prologue
    .line 1193
    move v2, p2

    #@1
    .line 1194
    .local v2, w:I
    move v1, p2

    #@2
    .line 1195
    .local v1, r:I
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, -0x1

    #@4
    if-lt v1, p1, :cond_11

    #@6
    .line 1196
    aget-char v0, p0, v1

    #@8
    .line 1197
    .local v0, ch:C
    if-eq v0, p3, :cond_2

    #@a
    .line 1198
    add-int/lit8 v2, v2, -0x1

    #@c
    .line 1199
    if-eq v2, v1, :cond_2

    #@e
    .line 1200
    aput-char v0, p0, v2

    #@10
    goto :goto_2

    #@11
    .line 1204
    .end local v0           #ch:C
    :cond_11
    return-void
.end method

.method private static specialChar(C)I
    .registers 2
    .parameter "ch"

    #@0
    .prologue
    .line 927
    const/16 v0, 0x621

    #@2
    if-le p0, v0, :cond_8

    #@4
    const/16 v0, 0x626

    #@6
    if-lt p0, v0, :cond_20

    #@8
    :cond_8
    const/16 v0, 0x627

    #@a
    if-eq p0, v0, :cond_20

    #@c
    const/16 v0, 0x62e

    #@e
    if-le p0, v0, :cond_14

    #@10
    const/16 v0, 0x633

    #@12
    if-lt p0, v0, :cond_20

    #@14
    :cond_14
    const/16 v0, 0x647

    #@16
    if-le p0, v0, :cond_1c

    #@18
    const/16 v0, 0x64a

    #@1a
    if-lt p0, v0, :cond_20

    #@1c
    :cond_1c
    const/16 v0, 0x629

    #@1e
    if-ne p0, v0, :cond_22

    #@20
    .line 932
    :cond_20
    const/4 v0, 0x1

    #@21
    .line 940
    :goto_21
    return v0

    #@22
    .line 933
    :cond_22
    const/16 v0, 0x64b

    #@24
    if-lt p0, v0, :cond_2c

    #@26
    const/16 v0, 0x652

    #@28
    if-gt p0, v0, :cond_2c

    #@2a
    .line 934
    const/4 v0, 0x2

    #@2b
    goto :goto_21

    #@2c
    .line 935
    :cond_2c
    const/16 v0, 0x653

    #@2e
    if-lt p0, v0, :cond_34

    #@30
    const/16 v0, 0x655

    #@32
    if-le p0, v0, :cond_42

    #@34
    :cond_34
    const/16 v0, 0x670

    #@36
    if-eq p0, v0, :cond_42

    #@38
    const v0, 0xfe70

    #@3b
    if-lt p0, v0, :cond_44

    #@3d
    const v0, 0xfe7f

    #@40
    if-gt p0, v0, :cond_44

    #@42
    .line 938
    :cond_42
    const/4 v0, 0x3

    #@43
    goto :goto_21

    #@44
    .line 940
    :cond_44
    const/4 v0, 0x0

    #@45
    goto :goto_21
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "rhs"

    #@0
    .prologue
    .line 569
    if-eqz p1, :cond_14

    #@2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@5
    move-result-object v0

    #@6
    const-class v1, Landroid/icu/text/ArabicShaping;

    #@8
    if-ne v0, v1, :cond_14

    #@a
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@c
    check-cast p1, Landroid/icu/text/ArabicShaping;

    #@e
    .end local p1
    iget v1, p1, Landroid/icu/text/ArabicShaping;->options:I

    #@10
    if-ne v0, v1, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 579
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2
    return v0
.end method

.method public shape([CII[CII)I
    .registers 11
    .parameter "source"
    .parameter "sourceStart"
    .parameter "sourceLength"
    .parameter "dest"
    .parameter "destStart"
    .parameter "destSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    const/high16 v3, 0xe

    #@2
    const v2, 0x10003

    #@5
    .line 99
    if-nez p1, :cond_10

    #@7
    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v1, "source can not be null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 102
    :cond_10
    if-ltz p2, :cond_19

    #@12
    if-ltz p3, :cond_19

    #@14
    add-int v0, p2, p3

    #@16
    array-length v1, p1

    #@17
    if-le v0, v1, :cond_47

    #@19
    .line 103
    :cond_19
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "bad source start ("

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, ") or length ("

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string v2, ") for buffer of length "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    array-length v2, p1

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v0

    #@47
    .line 107
    :cond_47
    if-nez p4, :cond_54

    #@49
    if-eqz p6, :cond_54

    #@4b
    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4d
    const-string/jumbo v1, "null dest requires destSize == 0"

    #@50
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@53
    throw v0

    #@54
    .line 110
    :cond_54
    if-eqz p6, :cond_8d

    #@56
    if-ltz p5, :cond_5f

    #@58
    if-ltz p6, :cond_5f

    #@5a
    add-int v0, p5, p6

    #@5c
    array-length v1, p4

    #@5d
    if-le v0, v1, :cond_8d

    #@5f
    .line 112
    :cond_5f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@61
    new-instance v1, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v2, "bad dest start ("

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    const-string v2, ") or size ("

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    const-string v2, ") for buffer of length "

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    array-length v2, p4

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v1

    #@89
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@8c
    throw v0

    #@8d
    .line 117
    :cond_8d
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@8f
    and-int/2addr v0, v3

    #@90
    if-lez v0, :cond_b6

    #@92
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@94
    and-int/2addr v0, v3

    #@95
    const/high16 v1, 0x4

    #@97
    if-eq v0, v1, :cond_b6

    #@99
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@9b
    and-int/2addr v0, v3

    #@9c
    const/high16 v1, 0x6

    #@9e
    if-eq v0, v1, :cond_b6

    #@a0
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@a2
    and-int/2addr v0, v3

    #@a3
    const/high16 v1, 0x8

    #@a5
    if-eq v0, v1, :cond_b6

    #@a7
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@a9
    and-int/2addr v0, v3

    #@aa
    const/high16 v1, 0xc

    #@ac
    if-eq v0, v1, :cond_b6

    #@ae
    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b0
    const-string v1, "Wrong Tashkeel argument"

    #@b2
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b5
    throw v0

    #@b6
    .line 127
    :cond_b6
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@b8
    and-int/2addr v0, v2

    #@b9
    if-lez v0, :cond_e1

    #@bb
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@bd
    and-int/2addr v0, v2

    #@be
    const/4 v1, 0x3

    #@bf
    if-eq v0, v1, :cond_e1

    #@c1
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@c3
    and-int/2addr v0, v2

    #@c4
    const/4 v1, 0x2

    #@c5
    if-eq v0, v1, :cond_e1

    #@c7
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@c9
    and-int/2addr v0, v2

    #@ca
    if-eqz v0, :cond_e1

    #@cc
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@ce
    and-int/2addr v0, v2

    #@cf
    const/high16 v1, 0x1

    #@d1
    if-eq v0, v1, :cond_e1

    #@d3
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@d5
    and-int/2addr v0, v2

    #@d6
    const/4 v1, 0x1

    #@d7
    if-eq v0, v1, :cond_e1

    #@d9
    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@db
    const-string v1, "Wrong Lam Alef argument"

    #@dd
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e0
    throw v0

    #@e1
    .line 138
    :cond_e1
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@e3
    and-int/2addr v0, v3

    #@e4
    if-lez v0, :cond_f6

    #@e6
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@e8
    and-int/lit8 v0, v0, 0x18

    #@ea
    const/16 v1, 0x10

    #@ec
    if-ne v0, v1, :cond_f6

    #@ee
    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f0
    const-string v1, "Tashkeel replacement should not be enabled in deshaping mode "

    #@f2
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f5
    throw v0

    #@f6
    .line 141
    :cond_f6
    invoke-direct/range {p0 .. p6}, Landroid/icu/text/ArabicShaping;->internalShape([CII[CII)I

    #@f9
    move-result v0

    #@fa
    return v0
.end method

.method public shape(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "text"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 170
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    #@4
    move-result-object v1

    #@5
    .line 171
    .local v1, src:[C
    move-object v4, v1

    #@6
    .line 172
    .local v4, dest:[C
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@8
    const v3, 0x10003

    #@b
    and-int/2addr v0, v3

    #@c
    if-nez v0, :cond_1b

    #@e
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@10
    and-int/lit8 v0, v0, 0x18

    #@12
    const/16 v3, 0x10

    #@14
    if-ne v0, v3, :cond_1b

    #@16
    .line 175
    array-length v0, v1

    #@17
    mul-int/lit8 v0, v0, 0x2

    #@19
    new-array v4, v0, [C

    #@1b
    .line 177
    :cond_1b
    array-length v3, v1

    #@1c
    array-length v6, v4

    #@1d
    move-object v0, p0

    #@1e
    move v5, v2

    #@1f
    invoke-virtual/range {v0 .. v6}, Landroid/icu/text/ArabicShaping;->shape([CII[CII)I

    #@22
    move-result v7

    #@23
    .line 179
    .local v7, len:I
    new-instance v0, Ljava/lang/String;

    #@25
    invoke-direct {v0, v4, v2, v7}, Ljava/lang/String;-><init>([CII)V

    #@28
    return-object v0
.end method

.method public shape([CII)V
    .registers 11
    .parameter "source"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/icu/text/ArabicShaping$ArabicShapingException;
        }
    .end annotation

    #@0
    .prologue
    .line 155
    iget v0, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2
    const v1, 0x10003

    #@5
    and-int/2addr v0, v1

    #@6
    if-nez v0, :cond_10

    #@8
    .line 156
    new-instance v0, Landroid/icu/text/ArabicShaping$ArabicShapingException;

    #@a
    const-string v1, "Cannot shape in place with length option resize."

    #@c
    invoke-direct {v0, v1}, Landroid/icu/text/ArabicShaping$ArabicShapingException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    :cond_10
    move-object v0, p0

    #@11
    move-object v1, p1

    #@12
    move v2, p2

    #@13
    move v3, p3

    #@14
    move-object v4, p1

    #@15
    move v5, p2

    #@16
    move v6, p3

    #@17
    .line 158
    invoke-virtual/range {v0 .. v6}, Landroid/icu/text/ArabicShaping;->shape([CII[CII)I

    #@1a
    .line 159
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 586
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    #@9
    .line 587
    .local v0, buf:Ljava/lang/StringBuffer;
    const/16 v1, 0x5b

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    #@e
    .line 589
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@10
    const v2, 0x10003

    #@13
    and-int/2addr v1, v2

    #@14
    sparse-switch v1, :sswitch_data_ec

    #@17
    .line 596
    :goto_17
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@19
    and-int/lit8 v1, v1, 0x4

    #@1b
    sparse-switch v1, :sswitch_data_102

    #@1e
    .line 600
    :goto_1e
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@20
    and-int/lit8 v1, v1, 0x18

    #@22
    sparse-switch v1, :sswitch_data_10c

    #@25
    .line 606
    :goto_25
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@27
    const/high16 v2, 0x70

    #@29
    and-int/2addr v1, v2

    #@2a
    packed-switch v1, :pswitch_data_11e

    #@2d
    .line 609
    :goto_2d
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@2f
    const/high16 v2, 0x380

    #@31
    and-int/2addr v1, v2

    #@32
    packed-switch v1, :pswitch_data_124

    #@35
    .line 612
    :goto_35
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@37
    const/high16 v2, 0xe

    #@39
    and-int/2addr v1, v2

    #@3a
    sparse-switch v1, :sswitch_data_12a

    #@3d
    .line 619
    :goto_3d
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@3f
    and-int/lit16 v1, v1, 0xe0

    #@41
    sparse-switch v1, :sswitch_data_13c

    #@44
    .line 626
    :goto_44
    iget v1, p0, Landroid/icu/text/ArabicShaping;->options:I

    #@46
    and-int/lit16 v1, v1, 0x100

    #@48
    sparse-switch v1, :sswitch_data_152

    #@4b
    .line 630
    :goto_4b
    const-string v1, "]"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@50
    .line 632
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    return-object v1

    #@55
    .line 590
    :sswitch_55
    const-string v1, "LamAlef resize"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@5a
    goto :goto_17

    #@5b
    .line 591
    :sswitch_5b
    const-string v1, "LamAlef spaces at near"

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@60
    goto :goto_17

    #@61
    .line 592
    :sswitch_61
    const-string v1, "LamAlef spaces at begin"

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@66
    goto :goto_17

    #@67
    .line 593
    :sswitch_67
    const-string v1, "LamAlef spaces at end"

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@6c
    goto :goto_17

    #@6d
    .line 594
    :sswitch_6d
    const-string/jumbo v1, "lamAlef auto"

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@73
    goto :goto_17

    #@74
    .line 597
    :sswitch_74
    const-string v1, ", logical"

    #@76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@79
    goto :goto_1e

    #@7a
    .line 598
    :sswitch_7a
    const-string v1, ", visual"

    #@7c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@7f
    goto :goto_1e

    #@80
    .line 601
    :sswitch_80
    const-string v1, ", no letter shaping"

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@85
    goto :goto_25

    #@86
    .line 602
    :sswitch_86
    const-string v1, ", shape letters"

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@8b
    goto :goto_25

    #@8c
    .line 603
    :sswitch_8c
    const-string v1, ", shape letters tashkeel isolated"

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@91
    goto :goto_25

    #@92
    .line 604
    :sswitch_92
    const-string v1, ", unshape letters"

    #@94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@97
    goto :goto_25

    #@98
    .line 607
    :pswitch_98
    const-string v1, ", Seen at near"

    #@9a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@9d
    goto :goto_2d

    #@9e
    .line 610
    :pswitch_9e
    const-string v1, ", Yeh Hamza at near"

    #@a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a3
    goto :goto_35

    #@a4
    .line 613
    :sswitch_a4
    const-string v1, ", Tashkeel at begin"

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@a9
    goto :goto_3d

    #@aa
    .line 614
    :sswitch_aa
    const-string v1, ", Tashkeel at end"

    #@ac
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@af
    goto :goto_3d

    #@b0
    .line 615
    :sswitch_b0
    const-string v1, ", Tashkeel replace with tatweel"

    #@b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@b5
    goto :goto_3d

    #@b6
    .line 616
    :sswitch_b6
    const-string v1, ", Tashkeel resize"

    #@b8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@bb
    goto :goto_3d

    #@bc
    .line 620
    :sswitch_bc
    const-string v1, ", no digit shaping"

    #@be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c1
    goto :goto_44

    #@c2
    .line 621
    :sswitch_c2
    const-string v1, ", shape digits to AN"

    #@c4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@c7
    goto/16 :goto_44

    #@c9
    .line 622
    :sswitch_c9
    const-string v1, ", shape digits to EN"

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@ce
    goto/16 :goto_44

    #@d0
    .line 623
    :sswitch_d0
    const-string v1, ", shape digits to AN contextually: default EN"

    #@d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@d5
    goto/16 :goto_44

    #@d7
    .line 624
    :sswitch_d7
    const-string v1, ", shape digits to AN contextually: default AL"

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@dc
    goto/16 :goto_44

    #@de
    .line 627
    :sswitch_de
    const-string v1, ", standard Arabic-Indic digits"

    #@e0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@e3
    goto/16 :goto_4b

    #@e5
    .line 628
    :sswitch_e5
    const-string v1, ", extended Arabic-Indic digits"

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@ea
    goto/16 :goto_4b

    #@ec
    .line 589
    :sswitch_data_ec
    .sparse-switch
        0x0 -> :sswitch_55
        0x1 -> :sswitch_5b
        0x2 -> :sswitch_67
        0x3 -> :sswitch_61
        0x10000 -> :sswitch_6d
    .end sparse-switch

    #@102
    .line 596
    :sswitch_data_102
    .sparse-switch
        0x0 -> :sswitch_74
        0x4 -> :sswitch_7a
    .end sparse-switch

    #@10c
    .line 600
    :sswitch_data_10c
    .sparse-switch
        0x0 -> :sswitch_80
        0x8 -> :sswitch_86
        0x10 -> :sswitch_92
        0x18 -> :sswitch_8c
    .end sparse-switch

    #@11e
    .line 606
    :pswitch_data_11e
    .packed-switch 0x200000
        :pswitch_98
    .end packed-switch

    #@124
    .line 609
    :pswitch_data_124
    .packed-switch 0x1000000
        :pswitch_9e
    .end packed-switch

    #@12a
    .line 612
    :sswitch_data_12a
    .sparse-switch
        0x40000 -> :sswitch_a4
        0x60000 -> :sswitch_aa
        0x80000 -> :sswitch_b6
        0xc0000 -> :sswitch_b0
    .end sparse-switch

    #@13c
    .line 619
    :sswitch_data_13c
    .sparse-switch
        0x0 -> :sswitch_bc
        0x20 -> :sswitch_c2
        0x40 -> :sswitch_c9
        0x60 -> :sswitch_d0
        0x80 -> :sswitch_d7
    .end sparse-switch

    #@152
    .line 626
    :sswitch_data_152
    .sparse-switch
        0x0 -> :sswitch_de
        0x100 -> :sswitch_e5
    .end sparse-switch
.end method
