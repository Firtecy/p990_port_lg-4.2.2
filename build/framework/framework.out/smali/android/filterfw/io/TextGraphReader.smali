.class public Landroid/filterfw/io/TextGraphReader;
.super Landroid/filterfw/io/GraphReader;
.source "TextGraphReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/io/TextGraphReader$ConnectCommand;,
        Landroid/filterfw/io/TextGraphReader$InitFilterCommand;,
        Landroid/filterfw/io/TextGraphReader$AllocateFilterCommand;,
        Landroid/filterfw/io/TextGraphReader$AddLibraryCommand;,
        Landroid/filterfw/io/TextGraphReader$ImportPackageCommand;,
        Landroid/filterfw/io/TextGraphReader$Command;
    }
.end annotation


# instance fields
.field private mBoundReferences:Landroid/filterfw/core/KeyValueMap;

.field private mCommands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/filterfw/io/TextGraphReader$Command;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentFilter:Landroid/filterfw/core/Filter;

.field private mCurrentGraph:Landroid/filterfw/core/FilterGraph;

.field private mFactory:Landroid/filterfw/core/FilterFactory;

.field private mSettings:Landroid/filterfw/core/KeyValueMap;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 39
    invoke-direct {p0}, Landroid/filterfw/io/GraphReader;-><init>()V

    #@3
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@a
    .line 124
    return-void
.end method

.method static synthetic access$000(Landroid/filterfw/io/TextGraphReader;)Landroid/filterfw/core/FilterFactory;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mFactory:Landroid/filterfw/core/FilterFactory;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/filterfw/io/TextGraphReader;)Landroid/filterfw/core/Filter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentFilter:Landroid/filterfw/core/Filter;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/filterfw/io/TextGraphReader;Landroid/filterfw/core/Filter;)Landroid/filterfw/core/Filter;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 39
    iput-object p1, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentFilter:Landroid/filterfw/core/Filter;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/filterfw/io/TextGraphReader;)Landroid/filterfw/core/FilterGraph;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@2
    return-object v0
.end method

.method private applySettings()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 452
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mSettings:Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-virtual {v3}, Landroid/filterfw/core/KeyValueMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v3

    #@6
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v3

    #@e
    if-eqz v3, :cond_b1

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/lang/String;

    #@16
    .line 453
    .local v1, setting:Ljava/lang/String;
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mSettings:Landroid/filterfw/core/KeyValueMap;

    #@18
    invoke-virtual {v3, v1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    .line 454
    .local v2, value:Ljava/lang/Object;
    const-string v3, "autoBranch"

    #@1e
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_78

    #@24
    .line 455
    const-class v3, Ljava/lang/String;

    #@26
    invoke-direct {p0, v1, v2, v3}, Landroid/filterfw/io/TextGraphReader;->expectSettingClass(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    #@29
    .line 456
    const-string/jumbo v3, "synced"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_39

    #@32
    .line 457
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@34
    const/4 v4, 0x1

    #@35
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->setAutoBranchMode(I)V

    #@38
    goto :goto_a

    #@39
    .line 458
    :cond_39
    const-string/jumbo v3, "unsynced"

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_49

    #@42
    .line 459
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@44
    const/4 v4, 0x2

    #@45
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->setAutoBranchMode(I)V

    #@48
    goto :goto_a

    #@49
    .line 460
    :cond_49
    const-string/jumbo v3, "off"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v3

    #@50
    if-eqz v3, :cond_59

    #@52
    .line 461
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@54
    const/4 v4, 0x0

    #@55
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->setAutoBranchMode(I)V

    #@58
    goto :goto_a

    #@59
    .line 463
    :cond_59
    new-instance v3, Landroid/filterfw/io/GraphIOException;

    #@5b
    new-instance v4, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v5, "Unknown autobranch setting: "

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    const-string v5, "!"

    #@6c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v4

    #@74
    invoke-direct {v3, v4}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@77
    throw v3

    #@78
    .line 465
    :cond_78
    const-string v3, "discardUnconnectedOutputs"

    #@7a
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7d
    move-result v3

    #@7e
    if-eqz v3, :cond_92

    #@80
    .line 466
    const-class v3, Ljava/lang/Boolean;

    #@82
    invoke-direct {p0, v1, v2, v3}, Landroid/filterfw/io/TextGraphReader;->expectSettingClass(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    #@85
    .line 467
    iget-object v3, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@87
    check-cast v2, Ljava/lang/Boolean;

    #@89
    .end local v2           #value:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@8c
    move-result v4

    #@8d
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FilterGraph;->setDiscardUnconnectedOutputs(Z)V

    #@90
    goto/16 :goto_a

    #@92
    .line 469
    .restart local v2       #value:Ljava/lang/Object;
    :cond_92
    new-instance v3, Landroid/filterfw/io/GraphIOException;

    #@94
    new-instance v4, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v5, "Unknown @setting \'"

    #@9b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v4

    #@a3
    const-string v5, "\'!"

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v4

    #@ad
    invoke-direct {v3, v4}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@b0
    throw v3

    #@b1
    .line 472
    .end local v1           #setting:Ljava/lang/String;
    .end local v2           #value:Ljava/lang/Object;
    :cond_b1
    return-void
.end method

.method private bindExternal(Ljava/lang/String;)V
    .registers 6
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 426
    iget-object v1, p0, Landroid/filterfw/io/GraphReader;->mReferences:Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-virtual {v1, p1}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_14

    #@8
    .line 427
    iget-object v1, p0, Landroid/filterfw/io/GraphReader;->mReferences:Landroid/filterfw/core/KeyValueMap;

    #@a
    invoke-virtual {v1, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    .line 428
    .local v0, value:Ljava/lang/Object;
    iget-object v1, p0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@10
    invoke-virtual {v1, p1, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 434
    return-void

    #@14
    .line 430
    .end local v0           #value:Ljava/lang/Object;
    :cond_14
    new-instance v1, Landroid/filterfw/io/GraphIOException;

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Unknown external variable \'"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, "\'! "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, "You must add a reference to this external in the host program using "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, "addReference(...)!"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-direct {v1, v2}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@3e
    throw v1
.end method

.method private checkReferences()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 442
    iget-object v2, p0, Landroid/filterfw/io/GraphReader;->mReferences:Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-virtual {v2}, Landroid/filterfw/core/KeyValueMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_43

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/lang/String;

    #@16
    .line 443
    .local v1, reference:Ljava/lang/String;
    iget-object v2, p0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@18
    invoke-virtual {v2, v1}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_a

    #@1e
    .line 444
    new-instance v2, Landroid/filterfw/io/GraphIOException;

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Host program specifies reference to \'"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v4, "\', which is not "

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    const-string v4, "declared @external in graph file!"

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-direct {v2, v3}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@42
    throw v2

    #@43
    .line 449
    .end local v1           #reference:Ljava/lang/String;
    :cond_43
    return-void
.end method

.method private executeCommands()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 485
    iget-object v2, p0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/filterfw/io/TextGraphReader$Command;

    #@12
    .line 486
    .local v0, command:Landroid/filterfw/io/TextGraphReader$Command;
    invoke-interface {v0, p0}, Landroid/filterfw/io/TextGraphReader$Command;->execute(Landroid/filterfw/io/TextGraphReader;)V

    #@15
    goto :goto_6

    #@16
    .line 488
    .end local v0           #command:Landroid/filterfw/io/TextGraphReader$Command;
    :cond_16
    return-void
.end method

.method private expectSettingClass(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V
    .registers 7
    .parameter "setting"
    .parameter "value"
    .parameter "expectedClass"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 477
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    if-eq v0, p3, :cond_45

    #@6
    .line 478
    new-instance v0, Landroid/filterfw/io/GraphIOException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Setting \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\' must have a value of type "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ", but found a value of type "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, "!"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-direct {v0, v1}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@44
    throw v0

    #@45
    .line 482
    :cond_45
    return-void
.end method

.method private parseString(Ljava/lang/String;)V
    .registers 51
    .parameter "graphString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 170
    const-string v3, "@[a-zA-Z]+"

    #@2
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v27

    #@6
    .line 171
    .local v27, commandPattern:Ljava/util/regex/Pattern;
    const-string v3, "\\}"

    #@8
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@b
    move-result-object v31

    #@c
    .line 172
    .local v31, curlyClosePattern:Ljava/util/regex/Pattern;
    const-string v3, "\\{"

    #@e
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@11
    move-result-object v32

    #@12
    .line 173
    .local v32, curlyOpenPattern:Ljava/util/regex/Pattern;
    const-string v3, "(\\s+|//[^\\n]*\\n)+"

    #@14
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@17
    move-result-object v34

    #@18
    .line 174
    .local v34, ignorePattern:Ljava/util/regex/Pattern;
    const-string v3, "[a-zA-Z\\.]+"

    #@1a
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@1d
    move-result-object v38

    #@1e
    .line 175
    .local v38, packageNamePattern:Ljava/util/regex/Pattern;
    const-string v3, "[a-zA-Z\\./:]+"

    #@20
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@23
    move-result-object v36

    #@24
    .line 176
    .local v36, libraryNamePattern:Ljava/util/regex/Pattern;
    const-string v3, "\\[[a-zA-Z0-9\\-_]+\\]"

    #@26
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@29
    move-result-object v40

    #@2a
    .line 177
    .local v40, portPattern:Ljava/util/regex/Pattern;
    const-string v3, "=>"

    #@2c
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@2f
    move-result-object v42

    #@30
    .line 178
    .local v42, rightArrowPattern:Ljava/util/regex/Pattern;
    const-string v3, ";"

    #@32
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@35
    move-result-object v44

    #@36
    .line 179
    .local v44, semicolonPattern:Ljava/util/regex/Pattern;
    const-string v3, "[a-zA-Z0-9\\-_]+"

    #@38
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@3b
    move-result-object v47

    #@3c
    .line 181
    .local v47, wordPattern:Ljava/util/regex/Pattern;
    const/4 v11, 0x0

    #@3d
    .line 182
    .local v11, STATE_COMMAND:I
    const/16 v17, 0x1

    #@3f
    .line 183
    .local v17, STATE_IMPORT_PKG:I
    const/4 v9, 0x2

    #@40
    .line 184
    .local v9, STATE_ADD_LIBRARY:I
    const/4 v15, 0x3

    #@41
    .line 185
    .local v15, STATE_FILTER_CLASS:I
    const/16 v16, 0x4

    #@43
    .line 186
    .local v16, STATE_FILTER_NAME:I
    const/4 v13, 0x5

    #@44
    .line 187
    .local v13, STATE_CURLY_OPEN:I
    const/16 v18, 0x6

    #@46
    .line 188
    .local v18, STATE_PARAMETERS:I
    const/4 v12, 0x7

    #@47
    .line 189
    .local v12, STATE_CURLY_CLOSE:I
    const/16 v22, 0x8

    #@49
    .line 190
    .local v22, STATE_SOURCE_FILTERNAME:I
    const/16 v23, 0x9

    #@4b
    .line 191
    .local v23, STATE_SOURCE_PORT:I
    const/16 v19, 0xa

    #@4d
    .line 192
    .local v19, STATE_RIGHT_ARROW:I
    const/16 v24, 0xb

    #@4f
    .line 193
    .local v24, STATE_TARGET_FILTERNAME:I
    const/16 v25, 0xc

    #@51
    .line 194
    .local v25, STATE_TARGET_PORT:I
    const/16 v10, 0xd

    #@53
    .line 195
    .local v10, STATE_ASSIGNMENT:I
    const/16 v14, 0xe

    #@55
    .line 196
    .local v14, STATE_EXTERNAL:I
    const/16 v21, 0xf

    #@57
    .line 197
    .local v21, STATE_SETTING:I
    const/16 v20, 0x10

    #@59
    .line 199
    .local v20, STATE_SEMICOLON:I
    const/16 v46, 0x0

    #@5b
    .line 200
    .local v46, state:I
    new-instance v43, Landroid/filterfw/io/PatternScanner;

    #@5d
    move-object/from16 v0, v43

    #@5f
    move-object/from16 v1, p1

    #@61
    move-object/from16 v2, v34

    #@63
    invoke-direct {v0, v1, v2}, Landroid/filterfw/io/PatternScanner;-><init>(Ljava/lang/String;Ljava/util/regex/Pattern;)V

    #@66
    .line 202
    .local v43, scanner:Landroid/filterfw/io/PatternScanner;
    const/16 v28, 0x0

    #@68
    .line 203
    .local v28, curClassName:Ljava/lang/String;
    const/4 v5, 0x0

    #@69
    .line 204
    .local v5, curSourceFilterName:Ljava/lang/String;
    const/4 v6, 0x0

    #@6a
    .line 205
    .local v6, curSourcePortName:Ljava/lang/String;
    const/4 v7, 0x0

    #@6b
    .line 206
    .local v7, curTargetFilterName:Ljava/lang/String;
    const/4 v8, 0x0

    #@6c
    .line 209
    .local v8, curTargetPortName:Ljava/lang/String;
    :goto_6c
    invoke-virtual/range {v43 .. v43}, Landroid/filterfw/io/PatternScanner;->atEnd()Z

    #@6f
    move-result v3

    #@70
    if-nez v3, :cond_265

    #@72
    .line 210
    packed-switch v46, :pswitch_data_276

    #@75
    goto :goto_6c

    #@76
    .line 212
    :pswitch_76
    const-string v3, "<command>"

    #@78
    move-object/from16 v0, v43

    #@7a
    move-object/from16 v1, v27

    #@7c
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@7f
    move-result-object v29

    #@80
    .line 213
    .local v29, curCommand:Ljava/lang/String;
    const-string v3, "@import"

    #@82
    move-object/from16 v0, v29

    #@84
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@87
    move-result v3

    #@88
    if-eqz v3, :cond_8d

    #@8a
    .line 214
    const/16 v46, 0x1

    #@8c
    goto :goto_6c

    #@8d
    .line 215
    :cond_8d
    const-string v3, "@library"

    #@8f
    move-object/from16 v0, v29

    #@91
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v3

    #@95
    if-eqz v3, :cond_9a

    #@97
    .line 216
    const/16 v46, 0x2

    #@99
    goto :goto_6c

    #@9a
    .line 217
    :cond_9a
    const-string v3, "@filter"

    #@9c
    move-object/from16 v0, v29

    #@9e
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a1
    move-result v3

    #@a2
    if-eqz v3, :cond_a7

    #@a4
    .line 218
    const/16 v46, 0x3

    #@a6
    goto :goto_6c

    #@a7
    .line 219
    :cond_a7
    const-string v3, "@connect"

    #@a9
    move-object/from16 v0, v29

    #@ab
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ae
    move-result v3

    #@af
    if-eqz v3, :cond_b4

    #@b1
    .line 220
    const/16 v46, 0x8

    #@b3
    goto :goto_6c

    #@b4
    .line 221
    :cond_b4
    const-string v3, "@set"

    #@b6
    move-object/from16 v0, v29

    #@b8
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bb
    move-result v3

    #@bc
    if-eqz v3, :cond_c1

    #@be
    .line 222
    const/16 v46, 0xd

    #@c0
    goto :goto_6c

    #@c1
    .line 223
    :cond_c1
    const-string v3, "@external"

    #@c3
    move-object/from16 v0, v29

    #@c5
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v3

    #@c9
    if-eqz v3, :cond_ce

    #@cb
    .line 224
    const/16 v46, 0xe

    #@cd
    goto :goto_6c

    #@ce
    .line 225
    :cond_ce
    const-string v3, "@setting"

    #@d0
    move-object/from16 v0, v29

    #@d2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v3

    #@d6
    if-eqz v3, :cond_db

    #@d8
    .line 226
    const/16 v46, 0xf

    #@da
    goto :goto_6c

    #@db
    .line 228
    :cond_db
    new-instance v3, Landroid/filterfw/io/GraphIOException;

    #@dd
    new-instance v4, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v48, "Unknown command \'"

    #@e4
    move-object/from16 v0, v48

    #@e6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v4

    #@ea
    move-object/from16 v0, v29

    #@ec
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v4

    #@f0
    const-string v48, "\'!"

    #@f2
    move-object/from16 v0, v48

    #@f4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v4

    #@f8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v4

    #@fc
    invoke-direct {v3, v4}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@ff
    throw v3

    #@100
    .line 234
    .end local v29           #curCommand:Ljava/lang/String;
    :pswitch_100
    const-string v3, "<package-name>"

    #@102
    move-object/from16 v0, v43

    #@104
    move-object/from16 v1, v38

    #@106
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@109
    move-result-object v37

    #@10a
    .line 235
    .local v37, packageName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@10c
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@10e
    new-instance v4, Landroid/filterfw/io/TextGraphReader$ImportPackageCommand;

    #@110
    move-object/from16 v0, p0

    #@112
    move-object/from16 v1, v37

    #@114
    invoke-direct {v4, v0, v1}, Landroid/filterfw/io/TextGraphReader$ImportPackageCommand;-><init>(Landroid/filterfw/io/TextGraphReader;Ljava/lang/String;)V

    #@117
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@11a
    .line 236
    const/16 v46, 0x10

    #@11c
    .line 237
    goto/16 :goto_6c

    #@11e
    .line 241
    .end local v37           #packageName:Ljava/lang/String;
    :pswitch_11e
    const-string v3, "<library-name>"

    #@120
    move-object/from16 v0, v43

    #@122
    move-object/from16 v1, v36

    #@124
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@127
    move-result-object v35

    #@128
    .line 242
    .local v35, libraryName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@12a
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@12c
    new-instance v4, Landroid/filterfw/io/TextGraphReader$AddLibraryCommand;

    #@12e
    move-object/from16 v0, p0

    #@130
    move-object/from16 v1, v35

    #@132
    invoke-direct {v4, v0, v1}, Landroid/filterfw/io/TextGraphReader$AddLibraryCommand;-><init>(Landroid/filterfw/io/TextGraphReader;Ljava/lang/String;)V

    #@135
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@138
    .line 243
    const/16 v46, 0x10

    #@13a
    .line 244
    goto/16 :goto_6c

    #@13c
    .line 248
    .end local v35           #libraryName:Ljava/lang/String;
    :pswitch_13c
    const-string v3, "<class-name>"

    #@13e
    move-object/from16 v0, v43

    #@140
    move-object/from16 v1, v47

    #@142
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@145
    move-result-object v28

    #@146
    .line 249
    const/16 v46, 0x4

    #@148
    .line 250
    goto/16 :goto_6c

    #@14a
    .line 253
    :pswitch_14a
    const-string v3, "<filter-name>"

    #@14c
    move-object/from16 v0, v43

    #@14e
    move-object/from16 v1, v47

    #@150
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@153
    move-result-object v30

    #@154
    .line 254
    .local v30, curFilterName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@158
    new-instance v4, Landroid/filterfw/io/TextGraphReader$AllocateFilterCommand;

    #@15a
    move-object/from16 v0, p0

    #@15c
    move-object/from16 v1, v28

    #@15e
    move-object/from16 v2, v30

    #@160
    invoke-direct {v4, v0, v1, v2}, Landroid/filterfw/io/TextGraphReader$AllocateFilterCommand;-><init>(Landroid/filterfw/io/TextGraphReader;Ljava/lang/String;Ljava/lang/String;)V

    #@163
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@166
    .line 255
    const/16 v46, 0x5

    #@168
    .line 256
    goto/16 :goto_6c

    #@16a
    .line 260
    .end local v30           #curFilterName:Ljava/lang/String;
    :pswitch_16a
    const-string/jumbo v3, "{"

    #@16d
    move-object/from16 v0, v43

    #@16f
    move-object/from16 v1, v32

    #@171
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@174
    .line 261
    const/16 v46, 0x6

    #@176
    .line 262
    goto/16 :goto_6c

    #@178
    .line 265
    :pswitch_178
    move-object/from16 v0, p0

    #@17a
    move-object/from16 v1, v43

    #@17c
    move-object/from16 v2, v31

    #@17e
    invoke-direct {v0, v1, v2}, Landroid/filterfw/io/TextGraphReader;->readKeyValueAssignments(Landroid/filterfw/io/PatternScanner;Ljava/util/regex/Pattern;)Landroid/filterfw/core/KeyValueMap;

    #@181
    move-result-object v39

    #@182
    .line 266
    .local v39, params:Landroid/filterfw/core/KeyValueMap;
    move-object/from16 v0, p0

    #@184
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@186
    new-instance v4, Landroid/filterfw/io/TextGraphReader$InitFilterCommand;

    #@188
    move-object/from16 v0, p0

    #@18a
    move-object/from16 v1, v39

    #@18c
    invoke-direct {v4, v0, v1}, Landroid/filterfw/io/TextGraphReader$InitFilterCommand;-><init>(Landroid/filterfw/io/TextGraphReader;Landroid/filterfw/core/KeyValueMap;)V

    #@18f
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@192
    .line 267
    const/16 v46, 0x7

    #@194
    .line 268
    goto/16 :goto_6c

    #@196
    .line 272
    .end local v39           #params:Landroid/filterfw/core/KeyValueMap;
    :pswitch_196
    const-string/jumbo v3, "}"

    #@199
    move-object/from16 v0, v43

    #@19b
    move-object/from16 v1, v31

    #@19d
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1a0
    .line 273
    const/16 v46, 0x0

    #@1a2
    .line 274
    goto/16 :goto_6c

    #@1a4
    .line 277
    :pswitch_1a4
    const-string v3, "<source-filter-name>"

    #@1a6
    move-object/from16 v0, v43

    #@1a8
    move-object/from16 v1, v47

    #@1aa
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1ad
    move-result-object v5

    #@1ae
    .line 278
    const/16 v46, 0x9

    #@1b0
    .line 279
    goto/16 :goto_6c

    #@1b2
    .line 282
    :pswitch_1b2
    const-string v3, "[<source-port-name>]"

    #@1b4
    move-object/from16 v0, v43

    #@1b6
    move-object/from16 v1, v40

    #@1b8
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1bb
    move-result-object v41

    #@1bc
    .line 283
    .local v41, portString:Ljava/lang/String;
    const/4 v3, 0x1

    #@1bd
    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->length()I

    #@1c0
    move-result v4

    #@1c1
    add-int/lit8 v4, v4, -0x1

    #@1c3
    move-object/from16 v0, v41

    #@1c5
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1c8
    move-result-object v6

    #@1c9
    .line 284
    const/16 v46, 0xa

    #@1cb
    .line 285
    goto/16 :goto_6c

    #@1cd
    .line 289
    .end local v41           #portString:Ljava/lang/String;
    :pswitch_1cd
    const-string v3, "=>"

    #@1cf
    move-object/from16 v0, v43

    #@1d1
    move-object/from16 v1, v42

    #@1d3
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1d6
    .line 290
    const/16 v46, 0xb

    #@1d8
    .line 291
    goto/16 :goto_6c

    #@1da
    .line 294
    :pswitch_1da
    const-string v3, "<target-filter-name>"

    #@1dc
    move-object/from16 v0, v43

    #@1de
    move-object/from16 v1, v47

    #@1e0
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1e3
    move-result-object v7

    #@1e4
    .line 295
    const/16 v46, 0xc

    #@1e6
    .line 296
    goto/16 :goto_6c

    #@1e8
    .line 299
    :pswitch_1e8
    const-string v3, "[<target-port-name>]"

    #@1ea
    move-object/from16 v0, v43

    #@1ec
    move-object/from16 v1, v40

    #@1ee
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@1f1
    move-result-object v41

    #@1f2
    .line 300
    .restart local v41       #portString:Ljava/lang/String;
    const/4 v3, 0x1

    #@1f3
    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->length()I

    #@1f6
    move-result v4

    #@1f7
    add-int/lit8 v4, v4, -0x1

    #@1f9
    move-object/from16 v0, v41

    #@1fb
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1fe
    move-result-object v8

    #@1ff
    .line 301
    move-object/from16 v0, p0

    #@201
    iget-object v0, v0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@203
    move-object/from16 v48, v0

    #@205
    new-instance v3, Landroid/filterfw/io/TextGraphReader$ConnectCommand;

    #@207
    move-object/from16 v4, p0

    #@209
    invoke-direct/range {v3 .. v8}, Landroid/filterfw/io/TextGraphReader$ConnectCommand;-><init>(Landroid/filterfw/io/TextGraphReader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@20c
    move-object/from16 v0, v48

    #@20e
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@211
    .line 305
    const/16 v46, 0x10

    #@213
    .line 306
    goto/16 :goto_6c

    #@215
    .line 310
    .end local v41           #portString:Ljava/lang/String;
    :pswitch_215
    move-object/from16 v0, p0

    #@217
    move-object/from16 v1, v43

    #@219
    move-object/from16 v2, v44

    #@21b
    invoke-direct {v0, v1, v2}, Landroid/filterfw/io/TextGraphReader;->readKeyValueAssignments(Landroid/filterfw/io/PatternScanner;Ljava/util/regex/Pattern;)Landroid/filterfw/core/KeyValueMap;

    #@21e
    move-result-object v26

    #@21f
    .line 311
    .local v26, assignment:Landroid/filterfw/core/KeyValueMap;
    move-object/from16 v0, p0

    #@221
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@223
    move-object/from16 v0, v26

    #@225
    invoke-virtual {v3, v0}, Landroid/filterfw/core/KeyValueMap;->putAll(Ljava/util/Map;)V

    #@228
    .line 312
    const/16 v46, 0x10

    #@22a
    .line 313
    goto/16 :goto_6c

    #@22c
    .line 317
    .end local v26           #assignment:Landroid/filterfw/core/KeyValueMap;
    :pswitch_22c
    const-string v3, "<external-identifier>"

    #@22e
    move-object/from16 v0, v43

    #@230
    move-object/from16 v1, v47

    #@232
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@235
    move-result-object v33

    #@236
    .line 318
    .local v33, externalName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@238
    move-object/from16 v1, v33

    #@23a
    invoke-direct {v0, v1}, Landroid/filterfw/io/TextGraphReader;->bindExternal(Ljava/lang/String;)V

    #@23d
    .line 319
    const/16 v46, 0x10

    #@23f
    .line 320
    goto/16 :goto_6c

    #@241
    .line 324
    .end local v33           #externalName:Ljava/lang/String;
    :pswitch_241
    move-object/from16 v0, p0

    #@243
    move-object/from16 v1, v43

    #@245
    move-object/from16 v2, v44

    #@247
    invoke-direct {v0, v1, v2}, Landroid/filterfw/io/TextGraphReader;->readKeyValueAssignments(Landroid/filterfw/io/PatternScanner;Ljava/util/regex/Pattern;)Landroid/filterfw/core/KeyValueMap;

    #@24a
    move-result-object v45

    #@24b
    .line 325
    .local v45, setting:Landroid/filterfw/core/KeyValueMap;
    move-object/from16 v0, p0

    #@24d
    iget-object v3, v0, Landroid/filterfw/io/TextGraphReader;->mSettings:Landroid/filterfw/core/KeyValueMap;

    #@24f
    move-object/from16 v0, v45

    #@251
    invoke-virtual {v3, v0}, Landroid/filterfw/core/KeyValueMap;->putAll(Ljava/util/Map;)V

    #@254
    .line 326
    const/16 v46, 0x10

    #@256
    .line 327
    goto/16 :goto_6c

    #@258
    .line 331
    .end local v45           #setting:Landroid/filterfw/core/KeyValueMap;
    :pswitch_258
    const-string v3, ";"

    #@25a
    move-object/from16 v0, v43

    #@25c
    move-object/from16 v1, v44

    #@25e
    invoke-virtual {v0, v1, v3}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@261
    .line 332
    const/16 v46, 0x0

    #@263
    goto/16 :goto_6c

    #@265
    .line 338
    :cond_265
    const/16 v3, 0x10

    #@267
    move/from16 v0, v46

    #@269
    if-eq v0, v3, :cond_275

    #@26b
    if-eqz v46, :cond_275

    #@26d
    .line 339
    new-instance v3, Landroid/filterfw/io/GraphIOException;

    #@26f
    const-string v4, "Unexpected end of input!"

    #@271
    invoke-direct {v3, v4}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@274
    throw v3

    #@275
    .line 341
    :cond_275
    return-void

    #@276
    .line 210
    :pswitch_data_276
    .packed-switch 0x0
        :pswitch_76
        :pswitch_100
        :pswitch_11e
        :pswitch_13c
        :pswitch_14a
        :pswitch_16a
        :pswitch_178
        :pswitch_196
        :pswitch_1a4
        :pswitch_1b2
        :pswitch_1cd
        :pswitch_1da
        :pswitch_1e8
        :pswitch_215
        :pswitch_22c
        :pswitch_241
        :pswitch_258
    .end packed-switch
.end method

.method private readKeyValueAssignments(Landroid/filterfw/io/PatternScanner;Ljava/util/regex/Pattern;)Landroid/filterfw/core/KeyValueMap;
    .registers 27
    .parameter "scanner"
    .parameter "endPattern"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 353
    const/4 v4, 0x0

    #@1
    .line 354
    .local v4, STATE_IDENTIFIER:I
    const/4 v3, 0x1

    #@2
    .line 355
    .local v3, STATE_EQUALS:I
    const/4 v6, 0x2

    #@3
    .line 356
    .local v6, STATE_VALUE:I
    const/4 v5, 0x3

    #@4
    .line 358
    .local v5, STATE_POST_VALUE:I
    const-string v21, "="

    #@6
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@9
    move-result-object v10

    #@a
    .line 359
    .local v10, equalsPattern:Ljava/util/regex/Pattern;
    const-string v21, ";"

    #@c
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@f
    move-result-object v17

    #@10
    .line 360
    .local v17, semicolonPattern:Ljava/util/regex/Pattern;
    const-string v21, "[a-zA-Z]+[a-zA-Z0-9]*"

    #@12
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@15
    move-result-object v20

    #@16
    .line 361
    .local v20, wordPattern:Ljava/util/regex/Pattern;
    const-string v21, "\'[^\']*\'|\\\"[^\\\"]*\\\""

    #@18
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@1b
    move-result-object v19

    #@1c
    .line 362
    .local v19, stringPattern:Ljava/util/regex/Pattern;
    const-string v21, "[0-9]+"

    #@1e
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@21
    move-result-object v12

    #@22
    .line 363
    .local v12, intPattern:Ljava/util/regex/Pattern;
    const-string v21, "[0-9]*\\.[0-9]+f?"

    #@24
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@27
    move-result-object v11

    #@28
    .line 364
    .local v11, floatPattern:Ljava/util/regex/Pattern;
    const-string v21, "\\$[a-zA-Z]+[a-zA-Z0-9]"

    #@2a
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@2d
    move-result-object v15

    #@2e
    .line 365
    .local v15, referencePattern:Ljava/util/regex/Pattern;
    const-string/jumbo v21, "true|false"

    #@31
    invoke-static/range {v21 .. v21}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@34
    move-result-object v7

    #@35
    .line 367
    .local v7, booleanPattern:Ljava/util/regex/Pattern;
    const/16 v18, 0x0

    #@37
    .line 368
    .local v18, state:I
    new-instance v13, Landroid/filterfw/core/KeyValueMap;

    #@39
    invoke-direct {v13}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@3c
    .line 369
    .local v13, newVals:Landroid/filterfw/core/KeyValueMap;
    const/4 v8, 0x0

    #@3d
    .line 370
    .local v8, curKey:Ljava/lang/String;
    const/4 v9, 0x0

    #@3e
    .line 372
    .local v9, curValue:Ljava/lang/String;
    :goto_3e
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/io/PatternScanner;->atEnd()Z

    #@41
    move-result v21

    #@42
    if-nez v21, :cond_146

    #@44
    if-eqz p2, :cond_4c

    #@46
    invoke-virtual/range {p1 .. p2}, Landroid/filterfw/io/PatternScanner;->peek(Ljava/util/regex/Pattern;)Z

    #@49
    move-result v21

    #@4a
    if-nez v21, :cond_146

    #@4c
    .line 373
    :cond_4c
    packed-switch v18, :pswitch_data_174

    #@4f
    goto :goto_3e

    #@50
    .line 375
    :pswitch_50
    const-string v21, "<identifier>"

    #@52
    move-object/from16 v0, p1

    #@54
    move-object/from16 v1, v20

    #@56
    move-object/from16 v2, v21

    #@58
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@5b
    move-result-object v8

    #@5c
    .line 376
    const/16 v18, 0x1

    #@5e
    .line 377
    goto :goto_3e

    #@5f
    .line 380
    :pswitch_5f
    const-string v21, "="

    #@61
    move-object/from16 v0, p1

    #@63
    move-object/from16 v1, v21

    #@65
    invoke-virtual {v0, v10, v1}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@68
    .line 381
    const/16 v18, 0x2

    #@6a
    .line 382
    goto :goto_3e

    #@6b
    .line 385
    :pswitch_6b
    move-object/from16 v0, p1

    #@6d
    move-object/from16 v1, v19

    #@6f
    invoke-virtual {v0, v1}, Landroid/filterfw/io/PatternScanner;->tryEat(Ljava/util/regex/Pattern;)Ljava/lang/String;

    #@72
    move-result-object v9

    #@73
    if-eqz v9, :cond_8d

    #@75
    .line 386
    const/16 v21, 0x1

    #@77
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@7a
    move-result v22

    #@7b
    add-int/lit8 v22, v22, -0x1

    #@7d
    move/from16 v0, v21

    #@7f
    move/from16 v1, v22

    #@81
    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@84
    move-result-object v21

    #@85
    move-object/from16 v0, v21

    #@87
    invoke-virtual {v13, v8, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8a
    .line 406
    :goto_8a
    const/16 v18, 0x3

    #@8c
    .line 407
    goto :goto_3e

    #@8d
    .line 387
    :cond_8d
    move-object/from16 v0, p1

    #@8f
    invoke-virtual {v0, v15}, Landroid/filterfw/io/PatternScanner;->tryEat(Ljava/util/regex/Pattern;)Ljava/lang/String;

    #@92
    move-result-object v9

    #@93
    if-eqz v9, :cond_e3

    #@95
    .line 388
    const/16 v21, 0x1

    #@97
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@9a
    move-result v22

    #@9b
    move/from16 v0, v21

    #@9d
    move/from16 v1, v22

    #@9f
    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@a2
    move-result-object v14

    #@a3
    .line 389
    .local v14, refName:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a5
    iget-object v0, v0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@a7
    move-object/from16 v21, v0

    #@a9
    if-eqz v21, :cond_da

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v0, v0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@af
    move-object/from16 v21, v0

    #@b1
    move-object/from16 v0, v21

    #@b3
    invoke-virtual {v0, v14}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b6
    move-result-object v16

    #@b7
    .line 392
    .local v16, referencedObject:Ljava/lang/Object;
    :goto_b7
    if-nez v16, :cond_dd

    #@b9
    .line 393
    new-instance v21, Landroid/filterfw/io/GraphIOException;

    #@bb
    new-instance v22, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v23, "Unknown object reference to \'"

    #@c2
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v22

    #@c6
    move-object/from16 v0, v22

    #@c8
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v22

    #@cc
    const-string v23, "\'!"

    #@ce
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v22

    #@d2
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v22

    #@d6
    invoke-direct/range {v21 .. v22}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@d9
    throw v21

    #@da
    .line 389
    .end local v16           #referencedObject:Ljava/lang/Object;
    :cond_da
    const/16 v16, 0x0

    #@dc
    goto :goto_b7

    #@dd
    .line 396
    .restart local v16       #referencedObject:Ljava/lang/Object;
    :cond_dd
    move-object/from16 v0, v16

    #@df
    invoke-virtual {v13, v8, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@e2
    goto :goto_8a

    #@e3
    .line 397
    .end local v14           #refName:Ljava/lang/String;
    .end local v16           #referencedObject:Ljava/lang/Object;
    :cond_e3
    move-object/from16 v0, p1

    #@e5
    invoke-virtual {v0, v7}, Landroid/filterfw/io/PatternScanner;->tryEat(Ljava/util/regex/Pattern;)Ljava/lang/String;

    #@e8
    move-result-object v9

    #@e9
    if-eqz v9, :cond_f9

    #@eb
    .line 398
    invoke-static {v9}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    #@ee
    move-result v21

    #@ef
    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@f2
    move-result-object v21

    #@f3
    move-object/from16 v0, v21

    #@f5
    invoke-virtual {v13, v8, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f8
    goto :goto_8a

    #@f9
    .line 399
    :cond_f9
    move-object/from16 v0, p1

    #@fb
    invoke-virtual {v0, v11}, Landroid/filterfw/io/PatternScanner;->tryEat(Ljava/util/regex/Pattern;)Ljava/lang/String;

    #@fe
    move-result-object v9

    #@ff
    if-eqz v9, :cond_110

    #@101
    .line 400
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    #@104
    move-result v21

    #@105
    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@108
    move-result-object v21

    #@109
    move-object/from16 v0, v21

    #@10b
    invoke-virtual {v13, v8, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10e
    goto/16 :goto_8a

    #@110
    .line 401
    :cond_110
    move-object/from16 v0, p1

    #@112
    invoke-virtual {v0, v12}, Landroid/filterfw/io/PatternScanner;->tryEat(Ljava/util/regex/Pattern;)Ljava/lang/String;

    #@115
    move-result-object v9

    #@116
    if-eqz v9, :cond_127

    #@118
    .line 402
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@11b
    move-result v21

    #@11c
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11f
    move-result-object v21

    #@120
    move-object/from16 v0, v21

    #@122
    invoke-virtual {v13, v8, v0}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@125
    goto/16 :goto_8a

    #@127
    .line 404
    :cond_127
    new-instance v21, Landroid/filterfw/io/GraphIOException;

    #@129
    const-string v22, "<value>"

    #@12b
    move-object/from16 v0, p1

    #@12d
    move-object/from16 v1, v22

    #@12f
    invoke-virtual {v0, v1}, Landroid/filterfw/io/PatternScanner;->unexpectedTokenMessage(Ljava/lang/String;)Ljava/lang/String;

    #@132
    move-result-object v22

    #@133
    invoke-direct/range {v21 .. v22}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@136
    throw v21

    #@137
    .line 410
    :pswitch_137
    const-string v21, ";"

    #@139
    move-object/from16 v0, p1

    #@13b
    move-object/from16 v1, v17

    #@13d
    move-object/from16 v2, v21

    #@13f
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/io/PatternScanner;->eat(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    #@142
    .line 411
    const/16 v18, 0x0

    #@144
    goto/16 :goto_3e

    #@146
    .line 417
    :cond_146
    if-eqz v18, :cond_173

    #@148
    const/16 v21, 0x3

    #@14a
    move/from16 v0, v18

    #@14c
    move/from16 v1, v21

    #@14e
    if-eq v0, v1, :cond_173

    #@150
    .line 418
    new-instance v21, Landroid/filterfw/io/GraphIOException;

    #@152
    new-instance v22, Ljava/lang/StringBuilder;

    #@154
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@157
    const-string v23, "Unexpected end of assignments on line "

    #@159
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v22

    #@15d
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/io/PatternScanner;->lineNo()I

    #@160
    move-result v23

    #@161
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@164
    move-result-object v22

    #@165
    const-string v23, "!"

    #@167
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v22

    #@16b
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v22

    #@16f
    invoke-direct/range {v21 .. v22}, Landroid/filterfw/io/GraphIOException;-><init>(Ljava/lang/String;)V

    #@172
    throw v21

    #@173
    .line 422
    :cond_173
    return-object v13

    #@174
    .line 373
    :pswitch_data_174
    .packed-switch 0x0
        :pswitch_50
        :pswitch_5f
        :pswitch_6b
        :pswitch_137
    .end packed-switch
.end method

.method private reset()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 161
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@3
    .line 162
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentFilter:Landroid/filterfw/core/Filter;

    #@5
    .line 163
    iget-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCommands:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@a
    .line 164
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@c
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mBoundReferences:Landroid/filterfw/core/KeyValueMap;

    #@11
    .line 165
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@13
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@16
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mSettings:Landroid/filterfw/core/KeyValueMap;

    #@18
    .line 166
    new-instance v0, Landroid/filterfw/core/FilterFactory;

    #@1a
    invoke-direct {v0}, Landroid/filterfw/core/FilterFactory;-><init>()V

    #@1d
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mFactory:Landroid/filterfw/core/FilterFactory;

    #@1f
    .line 167
    return-void
.end method


# virtual methods
.method public readGraphString(Ljava/lang/String;)Landroid/filterfw/core/FilterGraph;
    .registers 3
    .parameter "graphString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    new-instance v0, Landroid/filterfw/core/FilterGraph;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/FilterGraph;-><init>()V

    #@5
    .line 150
    .local v0, result:Landroid/filterfw/core/FilterGraph;
    invoke-direct {p0}, Landroid/filterfw/io/TextGraphReader;->reset()V

    #@8
    .line 151
    iput-object v0, p0, Landroid/filterfw/io/TextGraphReader;->mCurrentGraph:Landroid/filterfw/core/FilterGraph;

    #@a
    .line 152
    invoke-direct {p0, p1}, Landroid/filterfw/io/TextGraphReader;->parseString(Ljava/lang/String;)V

    #@d
    .line 153
    invoke-direct {p0}, Landroid/filterfw/io/TextGraphReader;->applySettings()V

    #@10
    .line 154
    invoke-direct {p0}, Landroid/filterfw/io/TextGraphReader;->executeCommands()V

    #@13
    .line 155
    invoke-direct {p0}, Landroid/filterfw/io/TextGraphReader;->reset()V

    #@16
    .line 157
    return-object v0
.end method

.method public readKeyValueAssignments(Ljava/lang/String;)Landroid/filterfw/core/KeyValueMap;
    .registers 5
    .parameter "assignments"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/io/GraphIOException;
        }
    .end annotation

    #@0
    .prologue
    .line 345
    const-string v2, "\\s+"

    #@2
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    .line 346
    .local v0, ignorePattern:Ljava/util/regex/Pattern;
    new-instance v1, Landroid/filterfw/io/PatternScanner;

    #@8
    invoke-direct {v1, p1, v0}, Landroid/filterfw/io/PatternScanner;-><init>(Ljava/lang/String;Ljava/util/regex/Pattern;)V

    #@b
    .line 347
    .local v1, scanner:Landroid/filterfw/io/PatternScanner;
    const/4 v2, 0x0

    #@c
    invoke-direct {p0, v1, v2}, Landroid/filterfw/io/TextGraphReader;->readKeyValueAssignments(Landroid/filterfw/io/PatternScanner;Ljava/util/regex/Pattern;)Landroid/filterfw/core/KeyValueMap;

    #@f
    move-result-object v2

    #@10
    return-object v2
.end method
