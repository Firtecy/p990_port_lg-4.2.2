.class public Landroid/filterfw/format/ImageFormat;
.super Ljava/lang/Object;
.source "ImageFormat.java"


# static fields
.field public static final COLORSPACE_GRAY:I = 0x1

.field public static final COLORSPACE_KEY:Ljava/lang/String; = "colorspace"

.field public static final COLORSPACE_RGB:I = 0x2

.field public static final COLORSPACE_RGBA:I = 0x3

.field public static final COLORSPACE_YUV:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static bytesPerSampleForColorspace(I)I
    .registers 4
    .parameter "colorspace"

    #@0
    .prologue
    const/4 v0, 0x3

    #@1
    .line 79
    packed-switch p0, :pswitch_data_28

    #@4
    .line 89
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Unknown colorspace id "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, "!"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@22
    throw v0

    #@23
    .line 81
    :pswitch_23
    const/4 v0, 0x1

    #@24
    .line 87
    :goto_24
    :pswitch_24
    return v0

    #@25
    .line 85
    :pswitch_25
    const/4 v0, 0x4

    #@26
    goto :goto_24

    #@27
    .line 79
    nop

    #@28
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_24
    .end packed-switch
.end method

.method public static create(I)Landroid/filterfw/core/MutableFrameFormat;
    .registers 3
    .parameter "colorspace"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    invoke-static {p0}, Landroid/filterfw/format/ImageFormat;->bytesPerSampleForColorspace(I)I

    #@4
    move-result v0

    #@5
    invoke-static {v1, v1, p0, v0, v1}, Landroid/filterfw/format/ImageFormat;->create(IIIII)Landroid/filterfw/core/MutableFrameFormat;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static create(II)Landroid/filterfw/core/MutableFrameFormat;
    .registers 4
    .parameter "colorspace"
    .parameter "target"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 63
    invoke-static {p0}, Landroid/filterfw/format/ImageFormat;->bytesPerSampleForColorspace(I)I

    #@4
    move-result v0

    #@5
    invoke-static {v1, v1, p0, v0, p1}, Landroid/filterfw/format/ImageFormat;->create(IIIII)Landroid/filterfw/core/MutableFrameFormat;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static create(IIII)Landroid/filterfw/core/MutableFrameFormat;
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "colorspace"
    .parameter "target"

    #@0
    .prologue
    .line 55
    invoke-static {p2}, Landroid/filterfw/format/ImageFormat;->bytesPerSampleForColorspace(I)I

    #@3
    move-result v0

    #@4
    invoke-static {p0, p1, p2, v0, p3}, Landroid/filterfw/format/ImageFormat;->create(IIIII)Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static create(IIIII)Landroid/filterfw/core/MutableFrameFormat;
    .registers 8
    .parameter "width"
    .parameter "height"
    .parameter "colorspace"
    .parameter "bytesPerSample"
    .parameter "target"

    #@0
    .prologue
    .line 41
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-direct {v0, v1, p4}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@6
    .line 42
    .local v0, result:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, p0, p1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(II)V

    #@9
    .line 43
    invoke-virtual {v0, p3}, Landroid/filterfw/core/MutableFrameFormat;->setBytesPerSample(I)V

    #@c
    .line 44
    const-string v1, "colorspace"

    #@e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setMetaValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@15
    .line 45
    const/4 v1, 0x1

    #@16
    if-ne p4, v1, :cond_1d

    #@18
    .line 46
    const-class v1, Landroid/graphics/Bitmap;

    #@1a
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setObjectClass(Ljava/lang/Class;)V

    #@1d
    .line 48
    :cond_1d
    return-object v0
.end method
