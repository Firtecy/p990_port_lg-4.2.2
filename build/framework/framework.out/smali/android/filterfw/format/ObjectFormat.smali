.class public Landroid/filterfw/format/ObjectFormat;
.super Ljava/lang/Object;
.source "ObjectFormat.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static bytesPerSampleForClass(Ljava/lang/Class;I)I
    .registers 6
    .parameter "clazz"
    .parameter "target"

    #@0
    .prologue
    .line 59
    const/4 v1, 0x2

    #@1
    if-ne p1, v1, :cond_5b

    #@3
    .line 60
    const-class v1, Landroid/filterfw/core/NativeBuffer;

    #@5
    invoke-virtual {v1, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_2a

    #@b
    .line 61
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "Native object-based formats must be of a NativeBuffer subclass! (Received class: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, ")."

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 65
    :cond_2a
    :try_start_2a
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Landroid/filterfw/core/NativeBuffer;

    #@30
    invoke-virtual {v1}, Landroid/filterfw/core/NativeBuffer;->getElementSize()I
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_33} :catch_35

    #@33
    move-result v1

    #@34
    .line 72
    :goto_34
    return v1

    #@35
    .line 66
    :catch_35
    move-exception v0

    #@36
    .line 67
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "Could not determine the size of an element in a native object-based frame of type "

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const-string v3, "! Perhaps it is missing a "

    #@49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    const-string v3, "default constructor?"

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5a
    throw v1

    #@5b
    .line 72
    .end local v0           #e:Ljava/lang/Exception;
    :cond_5b
    const/4 v1, 0x1

    #@5c
    goto :goto_34
.end method

.method public static fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;
    .registers 3
    .parameter "clazz"
    .parameter "target"

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0, p1}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;
    .registers 5
    .parameter "clazz"
    .parameter "count"
    .parameter "target"

    #@0
    .prologue
    .line 31
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@2
    const/16 v1, 0x8

    #@4
    invoke-direct {v0, v1, p2}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@7
    .line 32
    .local v0, result:Landroid/filterfw/core/MutableFrameFormat;
    invoke-static {p0}, Landroid/filterfw/format/ObjectFormat;->getBoxedClass(Ljava/lang/Class;)Ljava/lang/Class;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setObjectClass(Ljava/lang/Class;)V

    #@e
    .line 33
    if-eqz p1, :cond_13

    #@10
    .line 34
    invoke-virtual {v0, p1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions(I)V

    #@13
    .line 36
    :cond_13
    invoke-static {p0, p2}, Landroid/filterfw/format/ObjectFormat;->bytesPerSampleForClass(Ljava/lang/Class;I)I

    #@16
    move-result v1

    #@17
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setBytesPerSample(I)V

    #@1a
    .line 37
    return-object v0
.end method

.method public static fromObject(Ljava/lang/Object;I)Landroid/filterfw/core/MutableFrameFormat;
    .registers 4
    .parameter "object"
    .parameter "target"

    #@0
    .prologue
    .line 45
    if-nez p0, :cond_a

    #@2
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@4
    const/16 v1, 0x8

    #@6
    invoke-direct {v0, v1, p1}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@9
    :goto_9
    return-object v0

    #@a
    :cond_a
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@d
    move-result-object v0

    #@e
    const/4 v1, 0x0

    #@f
    invoke-static {v0, v1, p1}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    #@12
    move-result-object v0

    #@13
    goto :goto_9
.end method

.method public static fromObject(Ljava/lang/Object;II)Landroid/filterfw/core/MutableFrameFormat;
    .registers 5
    .parameter "object"
    .parameter "count"
    .parameter "target"

    #@0
    .prologue
    .line 51
    if-nez p0, :cond_a

    #@2
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@4
    const/16 v1, 0x8

    #@6
    invoke-direct {v0, v1, p2}, Landroid/filterfw/core/MutableFrameFormat;-><init>(II)V

    #@9
    :goto_9
    return-object v0

    #@a
    :cond_a
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@d
    move-result-object v0

    #@e
    invoke-static {v0, p1, p2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    #@11
    move-result-object v0

    #@12
    goto :goto_9
.end method

.method private static getBoxedClass(Ljava/lang/Class;)Ljava/lang/Class;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 78
    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 80
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    #@8
    if-ne p0, v0, :cond_d

    #@a
    .line 81
    const-class p0, Ljava/lang/Boolean;

    #@c
    .line 102
    .end local p0
    :cond_c
    :goto_c
    return-object p0

    #@d
    .line 82
    .restart local p0
    :cond_d
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    #@f
    if-ne p0, v0, :cond_14

    #@11
    .line 83
    const-class p0, Ljava/lang/Byte;

    #@13
    goto :goto_c

    #@14
    .line 84
    :cond_14
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    #@16
    if-ne p0, v0, :cond_1b

    #@18
    .line 85
    const-class p0, Ljava/lang/Character;

    #@1a
    goto :goto_c

    #@1b
    .line 86
    :cond_1b
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    #@1d
    if-ne p0, v0, :cond_22

    #@1f
    .line 87
    const-class p0, Ljava/lang/Short;

    #@21
    goto :goto_c

    #@22
    .line 88
    :cond_22
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@24
    if-ne p0, v0, :cond_29

    #@26
    .line 89
    const-class p0, Ljava/lang/Integer;

    #@28
    goto :goto_c

    #@29
    .line 90
    :cond_29
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    #@2b
    if-ne p0, v0, :cond_30

    #@2d
    .line 91
    const-class p0, Ljava/lang/Long;

    #@2f
    goto :goto_c

    #@30
    .line 92
    :cond_30
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    #@32
    if-ne p0, v0, :cond_37

    #@34
    .line 93
    const-class p0, Ljava/lang/Float;

    #@36
    goto :goto_c

    #@37
    .line 94
    :cond_37
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    #@39
    if-ne p0, v0, :cond_3e

    #@3b
    .line 95
    const-class p0, Ljava/lang/Double;

    #@3d
    goto :goto_c

    #@3e
    .line 97
    :cond_3e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v2, "Unknown primitive type: "

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    const-string v2, "!"

    #@55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v1

    #@5d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@60
    throw v0
.end method
