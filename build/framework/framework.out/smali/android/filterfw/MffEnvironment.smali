.class public Landroid/filterfw/MffEnvironment;
.super Ljava/lang/Object;
.source "MffEnvironment.java"


# instance fields
.field private mContext:Landroid/filterfw/core/FilterContext;


# direct methods
.method protected constructor <init>(Landroid/filterfw/core/FrameManager;)V
    .registers 3
    .parameter "frameManager"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    if-nez p1, :cond_a

    #@5
    .line 46
    new-instance p1, Landroid/filterfw/core/CachedFrameManager;

    #@7
    .end local p1
    invoke-direct {p1}, Landroid/filterfw/core/CachedFrameManager;-><init>()V

    #@a
    .line 50
    .restart local p1
    :cond_a
    new-instance v0, Landroid/filterfw/core/FilterContext;

    #@c
    invoke-direct {v0}, Landroid/filterfw/core/FilterContext;-><init>()V

    #@f
    iput-object v0, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@11
    .line 51
    iget-object v0, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@13
    invoke-virtual {v0, p1}, Landroid/filterfw/core/FilterContext;->setFrameManager(Landroid/filterfw/core/FrameManager;)V

    #@16
    .line 53
    return-void
.end method


# virtual methods
.method public activateGLEnvironment()V
    .registers 4

    #@0
    .prologue
    .line 85
    iget-object v1, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 86
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v0, :cond_12

    #@8
    .line 87
    iget-object v1, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@a
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->activate()V

    #@11
    .line 91
    return-void

    #@12
    .line 89
    :cond_12
    new-instance v1, Ljava/lang/NullPointerException;

    #@14
    const-string v2, "No GLEnvironment in place to activate!"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public createGLEnvironment()V
    .registers 2

    #@0
    .prologue
    .line 74
    new-instance v0, Landroid/filterfw/core/GLEnvironment;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/GLEnvironment;-><init>()V

    #@5
    .line 75
    .local v0, glEnvironment:Landroid/filterfw/core/GLEnvironment;
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->initWithNewContext()V

    #@8
    .line 76
    invoke-virtual {p0, v0}, Landroid/filterfw/MffEnvironment;->setGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V

    #@b
    .line 77
    return-void
.end method

.method public deactivateGLEnvironment()V
    .registers 4

    #@0
    .prologue
    .line 99
    iget-object v1, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 100
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v0, :cond_12

    #@8
    .line 101
    iget-object v1, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@a
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/GLEnvironment;->deactivate()V

    #@11
    .line 105
    return-void

    #@12
    .line 103
    :cond_12
    new-instance v1, Ljava/lang/NullPointerException;

    #@14
    const-string v2, "No GLEnvironment in place to deactivate!"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1
.end method

.method public getContext()Landroid/filterfw/core/FilterContext;
    .registers 2

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@2
    return-object v0
.end method

.method public setGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V
    .registers 3
    .parameter "glEnvironment"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Landroid/filterfw/MffEnvironment;->mContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v0, p1}, Landroid/filterfw/core/FilterContext;->initGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V

    #@5
    .line 68
    return-void
.end method
