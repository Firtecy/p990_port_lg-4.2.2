.class Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;
.super Ljava/io/OutputStream;
.source "SerializedFrame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/filterfw/core/SerializedFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectByteOutputStream"
.end annotation


# instance fields
.field private mBuffer:[B

.field private mDataOffset:I

.field private mOffset:I

.field final synthetic this$0:Landroid/filterfw/core/SerializedFrame;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/SerializedFrame;I)V
    .registers 5
    .parameter
    .parameter "size"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 66
    iput-object p1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->this$0:Landroid/filterfw/core/SerializedFrame;

    #@3
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    #@6
    .line 62
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@9
    .line 63
    iput v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@b
    .line 64
    iput v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mDataOffset:I

    #@d
    .line 67
    new-array v0, p2, [B

    #@f
    iput-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@11
    .line 68
    return-void
.end method

.method private final ensureFit(I)V
    .registers 6
    .parameter "bytesToWrite"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 71
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@3
    add-int/2addr v1, p1

    #@4
    iget-object v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@6
    array-length v2, v2

    #@7
    if-le v1, v2, :cond_22

    #@9
    .line 72
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@b
    .line 73
    .local v0, oldBuffer:[B
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@d
    add-int/2addr v1, p1

    #@e
    iget-object v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@10
    array-length v2, v2

    #@11
    mul-int/lit8 v2, v2, 0x2

    #@13
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    #@16
    move-result v1

    #@17
    new-array v1, v1, [B

    #@19
    iput-object v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@1b
    .line 74
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@1d
    iget v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@1f
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@22
    .line 77
    .end local v0           #oldBuffer:[B
    :cond_22
    return-void
.end method


# virtual methods
.method public getByteArray()[B
    .registers 2

    #@0
    .prologue
    .line 88
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@2
    return-object v0
.end method

.method public final getInputStream()Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;
    .registers 5

    #@0
    .prologue
    .line 114
    new-instance v0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;

    #@2
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->this$0:Landroid/filterfw/core/SerializedFrame;

    #@4
    iget-object v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@6
    iget v3, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@8
    invoke-direct {v0, v1, v2, v3}, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;-><init>(Landroid/filterfw/core/SerializedFrame;[BI)V

    #@b
    return-object v0
.end method

.method public final getSize()I
    .registers 2

    #@0
    .prologue
    .line 84
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@2
    return v0
.end method

.method public final markHeaderEnd()V
    .registers 2

    #@0
    .prologue
    .line 80
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@2
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mDataOffset:I

    #@4
    .line 81
    return-void
.end method

.method public final reset()V
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mDataOffset:I

    #@2
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@4
    .line 111
    return-void
.end method

.method public final write(I)V
    .registers 5
    .parameter "b"

    #@0
    .prologue
    .line 105
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->ensureFit(I)V

    #@4
    .line 106
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@6
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@8
    add-int/lit8 v2, v1, 0x1

    #@a
    iput v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@c
    int-to-byte v2, p1

    #@d
    aput-byte v2, v0, v1

    #@f
    .line 107
    return-void
.end method

.method public final write([B)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->write([BII)V

    #@5
    .line 94
    return-void
.end method

.method public final write([BII)V
    .registers 6
    .parameter "b"
    .parameter "off"
    .parameter "len"

    #@0
    .prologue
    .line 98
    invoke-direct {p0, p3}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->ensureFit(I)V

    #@3
    .line 99
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mBuffer:[B

    #@5
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@7
    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a
    .line 100
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@c
    add-int/2addr v0, p3

    #@d
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->mOffset:I

    #@f
    .line 101
    return-void
.end method
