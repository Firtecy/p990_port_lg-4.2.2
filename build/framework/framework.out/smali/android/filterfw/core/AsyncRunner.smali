.class public Landroid/filterfw/core/AsyncRunner;
.super Landroid/filterfw/core/GraphRunner;
.source "AsyncRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/core/AsyncRunner$1;,
        Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;,
        Landroid/filterfw/core/AsyncRunner$RunnerResult;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AsyncRunner"


# instance fields
.field private isProcessing:Z

.field private mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

.field private mException:Ljava/lang/Exception;

.field private mLogVerbose:Z

.field private mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

.field private mRunner:Landroid/filterfw/core/SyncRunner;

.field private mSchedulerClass:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 164
    invoke-direct {p0, p1}, Landroid/filterfw/core/GraphRunner;-><init>(Landroid/filterfw/core/FilterContext;)V

    #@3
    .line 166
    const-class v0, Landroid/filterfw/core/SimpleScheduler;

    #@5
    iput-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mSchedulerClass:Ljava/lang/Class;

    #@7
    .line 167
    const-string v0, "AsyncRunner"

    #@9
    const/4 v1, 0x2

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@d
    move-result v0

    #@e
    iput-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@10
    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/filterfw/core/FilterContext;Ljava/lang/Class;)V
    .registers 5
    .parameter "context"
    .parameter "schedulerClass"

    #@0
    .prologue
    .line 152
    invoke-direct {p0, p1}, Landroid/filterfw/core/GraphRunner;-><init>(Landroid/filterfw/core/FilterContext;)V

    #@3
    .line 154
    iput-object p2, p0, Landroid/filterfw/core/AsyncRunner;->mSchedulerClass:Ljava/lang/Class;

    #@5
    .line 155
    const-string v0, "AsyncRunner"

    #@7
    const/4 v1, 0x2

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@b
    move-result v0

    #@c
    iput-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@e
    .line 156
    return-void
.end method

.method static synthetic access$100(Landroid/filterfw/core/AsyncRunner;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/filterfw/core/AsyncRunner;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/filterfw/core/AsyncRunner;->setRunning(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/filterfw/core/AsyncRunner;Ljava/lang/Exception;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/filterfw/core/AsyncRunner;->setException(Ljava/lang/Exception;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/filterfw/core/AsyncRunner;)Landroid/filterfw/core/SyncRunner;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/filterfw/core/AsyncRunner;)Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 35
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@2
    return-object v0
.end method

.method private declared-synchronized setException(Ljava/lang/Exception;)V
    .registers 3
    .parameter "exception"

    #@0
    .prologue
    .line 254
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Landroid/filterfw/core/AsyncRunner;->mException:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 255
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 254
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method private declared-synchronized setRunning(Z)V
    .registers 3
    .parameter "running"

    #@0
    .prologue
    .line 250
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Landroid/filterfw/core/AsyncRunner;->isProcessing:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 251
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 250
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method


# virtual methods
.method public declared-synchronized close()V
    .registers 3

    #@0
    .prologue
    .line 231
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/filterfw/core/AsyncRunner;->isRunning()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 232
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v1, "Cannot close graph while it is running!"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_f

    #@f
    .line 231
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0

    #@12
    .line 234
    :cond_12
    :try_start_12
    iget-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@14
    if-eqz v0, :cond_1d

    #@16
    const-string v0, "AsyncRunner"

    #@18
    const-string v1, "Closing filters."

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 235
    :cond_1d
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@1f
    invoke-virtual {v0}, Landroid/filterfw/core/SyncRunner;->close()V
    :try_end_22
    .catchall {:try_start_12 .. :try_end_22} :catchall_f

    #@22
    .line 236
    monitor-exit p0

    #@23
    return-void
.end method

.method public declared-synchronized getError()Ljava/lang/Exception;
    .registers 2

    #@0
    .prologue
    .line 246
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mException:Ljava/lang/Exception;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getGraph()Landroid/filterfw/core/FilterGraph;
    .registers 2

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/SyncRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public declared-synchronized isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 241
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->isProcessing:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized run()V
    .registers 6

    #@0
    .prologue
    .line 197
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    const-string v0, "AsyncRunner"

    #@7
    const-string v1, "Running graph."

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 198
    :cond_c
    const/4 v0, 0x0

    #@d
    invoke-direct {p0, v0}, Landroid/filterfw/core/AsyncRunner;->setException(Ljava/lang/Exception;)V

    #@10
    .line 200
    invoke-virtual {p0}, Landroid/filterfw/core/AsyncRunner;->isRunning()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_21

    #@16
    .line 201
    new-instance v0, Ljava/lang/RuntimeException;

    #@18
    const-string v1, "Graph is already running!"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_1e

    #@1e
    .line 197
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0

    #@20
    throw v0

    #@21
    .line 203
    :cond_21
    :try_start_21
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@23
    if-nez v0, :cond_2d

    #@25
    .line 204
    new-instance v0, Ljava/lang/RuntimeException;

    #@27
    const-string v1, "Cannot run before a graph is set!"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 206
    :cond_2d
    new-instance v0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-direct {v0, p0, v1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;-><init>(Landroid/filterfw/core/AsyncRunner;Landroid/filterfw/core/AsyncRunner$1;)V

    #@33
    iput-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@35
    .line 208
    const/4 v0, 0x1

    #@36
    invoke-direct {p0, v0}, Landroid/filterfw/core/AsyncRunner;->setRunning(Z)V

    #@39
    .line 211
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@3b
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    #@3d
    const/4 v2, 0x1

    #@3e
    new-array v2, v2, [Landroid/filterfw/core/SyncRunner;

    #@40
    const/4 v3, 0x0

    #@41
    iget-object v4, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;

    #@43
    aput-object v4, v2, v3

    #@45
    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_48
    .catchall {:try_start_21 .. :try_end_48} :catchall_1e

    #@48
    .line 213
    monitor-exit p0

    #@49
    return-void
.end method

.method public setDoneCallback(Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 176
    iput-object p1, p0, Landroid/filterfw/core/AsyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@2
    .line 177
    return-void
.end method

.method public declared-synchronized setGraph(Landroid/filterfw/core/FilterGraph;)V
    .registers 5
    .parameter "graph"

    #@0
    .prologue
    .line 183
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/filterfw/core/AsyncRunner;->isRunning()Z

    #@4
    move-result v0

    #@5
    if-eqz v0, :cond_12

    #@7
    .line 184
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v1, "Graph is already running!"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_f

    #@f
    .line 183
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0

    #@12
    .line 186
    :cond_12
    :try_start_12
    new-instance v0, Landroid/filterfw/core/SyncRunner;

    #@14
    iget-object v1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@16
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner;->mSchedulerClass:Ljava/lang/Class;

    #@18
    invoke-direct {v0, v1, p1, v2}, Landroid/filterfw/core/SyncRunner;-><init>(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FilterGraph;Ljava/lang/Class;)V

    #@1b
    iput-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunner:Landroid/filterfw/core/SyncRunner;
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_f

    #@1d
    .line 187
    monitor-exit p0

    #@1e
    return-void
.end method

.method public declared-synchronized stop()V
    .registers 3

    #@0
    .prologue
    .line 220
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@3
    if-eqz v0, :cond_24

    #@5
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@7
    invoke-virtual {v0}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->isCancelled()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_24

    #@d
    .line 221
    iget-boolean v0, p0, Landroid/filterfw/core/AsyncRunner;->mLogVerbose:Z

    #@f
    if-eqz v0, :cond_18

    #@11
    const-string v0, "AsyncRunner"

    #@13
    const-string v1, "Stopping graph."

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 222
    :cond_18
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-virtual {v0, v1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->cancel(Z)Z

    #@1e
    .line 224
    iget-object v0, p0, Landroid/filterfw/core/AsyncRunner;->mRunTask:Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;

    #@20
    const/4 v1, 0x1

    #@21
    invoke-virtual {v0, v1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->cancel(Z)Z
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    #@24
    .line 227
    :cond_24
    monitor-exit p0

    #@25
    return-void

    #@26
    .line 220
    :catchall_26
    move-exception v0

    #@27
    monitor-exit p0

    #@28
    throw v0
.end method
