.class Landroid/filterfw/core/StopWatch;
.super Ljava/lang/Object;
.source "StopWatchMap.java"


# instance fields
.field private STOP_WATCH_LOGGING_PERIOD:I

.field private TAG:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNumCalls:I

.field private mStartTime:J

.field private mTotalTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    const/16 v0, 0xc8

    #@5
    iput v0, p0, Landroid/filterfw/core/StopWatch;->STOP_WATCH_LOGGING_PERIOD:I

    #@7
    .line 29
    const-string v0, "MFF"

    #@9
    iput-object v0, p0, Landroid/filterfw/core/StopWatch;->TAG:Ljava/lang/String;

    #@b
    .line 37
    iput-object p1, p0, Landroid/filterfw/core/StopWatch;->mName:Ljava/lang/String;

    #@d
    .line 38
    const-wide/16 v0, -0x1

    #@f
    iput-wide v0, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@11
    .line 39
    const-wide/16 v0, 0x0

    #@13
    iput-wide v0, p0, Landroid/filterfw/core/StopWatch;->mTotalTime:J

    #@15
    .line 40
    const/4 v0, 0x0

    #@16
    iput v0, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@18
    .line 41
    return-void
.end method


# virtual methods
.method public start()V
    .registers 5

    #@0
    .prologue
    .line 44
    iget-wide v0, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 45
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "Calling start with StopWatch already running"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 48
    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@13
    move-result-wide v0

    #@14
    iput-wide v0, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@16
    .line 49
    return-void
.end method

.method public stop()V
    .registers 10

    #@0
    .prologue
    const-wide/16 v6, -0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 52
    iget-wide v2, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@5
    cmp-long v2, v2, v6

    #@7
    if-nez v2, :cond_11

    #@9
    .line 53
    new-instance v2, Ljava/lang/RuntimeException;

    #@b
    const-string v3, "Calling stop with StopWatch already stopped"

    #@d
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v2

    #@11
    .line 56
    :cond_11
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@14
    move-result-wide v0

    #@15
    .line 57
    .local v0, stopTime:J
    iget-wide v2, p0, Landroid/filterfw/core/StopWatch;->mTotalTime:J

    #@17
    iget-wide v4, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@19
    sub-long v4, v0, v4

    #@1b
    add-long/2addr v2, v4

    #@1c
    iput-wide v2, p0, Landroid/filterfw/core/StopWatch;->mTotalTime:J

    #@1e
    .line 58
    iget v2, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@20
    add-int/lit8 v2, v2, 0x1

    #@22
    iput v2, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@24
    .line 59
    iput-wide v6, p0, Landroid/filterfw/core/StopWatch;->mStartTime:J

    #@26
    .line 60
    iget v2, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@28
    iget v3, p0, Landroid/filterfw/core/StopWatch;->STOP_WATCH_LOGGING_PERIOD:I

    #@2a
    rem-int/2addr v2, v3

    #@2b
    if-nez v2, :cond_70

    #@2d
    .line 61
    iget-object v2, p0, Landroid/filterfw/core/StopWatch;->TAG:Ljava/lang/String;

    #@2f
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v4, "AVG ms/call "

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    iget-object v4, p0, Landroid/filterfw/core/StopWatch;->mName:Ljava/lang/String;

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    const-string v4, ": "

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, "%.1f"

    #@48
    const/4 v5, 0x1

    #@49
    new-array v5, v5, [Ljava/lang/Object;

    #@4b
    iget-wide v6, p0, Landroid/filterfw/core/StopWatch;->mTotalTime:J

    #@4d
    long-to-float v6, v6

    #@4e
    const/high16 v7, 0x3f80

    #@50
    mul-float/2addr v6, v7

    #@51
    iget v7, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@53
    int-to-float v7, v7

    #@54
    div-float/2addr v6, v7

    #@55
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@58
    move-result-object v6

    #@59
    aput-object v6, v5, v8

    #@5b
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 63
    const-wide/16 v2, 0x0

    #@6c
    iput-wide v2, p0, Landroid/filterfw/core/StopWatch;->mTotalTime:J

    #@6e
    .line 64
    iput v8, p0, Landroid/filterfw/core/StopWatch;->mNumCalls:I

    #@70
    .line 66
    :cond_70
    return-void
.end method
