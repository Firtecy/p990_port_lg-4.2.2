.class public Landroid/filterfw/core/SimpleFrame;
.super Landroid/filterfw/core/Frame;
.source "SimpleFrame.java"


# instance fields
.field private mObject:Ljava/lang/Object;


# direct methods
.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 4
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3
    .line 39
    invoke-direct {p0, p1}, Landroid/filterfw/core/SimpleFrame;->initWithFormat(Landroid/filterfw/core/FrameFormat;)V

    #@6
    .line 40
    const/4 v0, 0x0

    #@7
    invoke-virtual {p0, v0}, Landroid/filterfw/core/SimpleFrame;->setReusable(Z)V

    #@a
    .line 41
    return-void
.end method

.method private initWithFormat(Landroid/filterfw/core/FrameFormat;)V
    .registers 5
    .parameter "format"

    #@0
    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getLength()I

    #@3
    move-result v1

    #@4
    .line 52
    .local v1, count:I
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@7
    move-result v0

    #@8
    .line 53
    .local v0, baseType:I
    packed-switch v0, :pswitch_data_28

    #@b
    .line 70
    const/4 v2, 0x0

    #@c
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@e
    .line 73
    :goto_e
    return-void

    #@f
    .line 55
    :pswitch_f
    new-array v2, v1, [B

    #@11
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@13
    goto :goto_e

    #@14
    .line 58
    :pswitch_14
    new-array v2, v1, [S

    #@16
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@18
    goto :goto_e

    #@19
    .line 61
    :pswitch_19
    new-array v2, v1, [I

    #@1b
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@1d
    goto :goto_e

    #@1e
    .line 64
    :pswitch_1e
    new-array v2, v1, [F

    #@20
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@22
    goto :goto_e

    #@23
    .line 67
    :pswitch_23
    new-array v2, v1, [D

    #@25
    iput-object v2, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@27
    goto :goto_e

    #@28
    .line 53
    :pswitch_data_28
    .packed-switch 0x2
        :pswitch_f
        :pswitch_14
        :pswitch_19
        :pswitch_1e
        :pswitch_23
    .end packed-switch
.end method

.method private setFormatObjectClass(Ljava/lang/Class;)V
    .registers 4
    .parameter "objectClass"

    #@0
    .prologue
    .line 134
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@7
    move-result-object v0

    #@8
    .line 135
    .local v0, format:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, p1}, Landroid/filterfw/core/MutableFrameFormat;->setObjectClass(Ljava/lang/Class;)V

    #@b
    .line 136
    invoke-virtual {p0, v0}, Landroid/filterfw/core/SimpleFrame;->setFormat(Landroid/filterfw/core/FrameFormat;)V

    #@e
    .line 137
    return-void
.end method

.method static wrapObject(Ljava/lang/Object;Landroid/filterfw/core/FrameManager;)Landroid/filterfw/core/SimpleFrame;
    .registers 5
    .parameter "object"
    .parameter "frameManager"

    #@0
    .prologue
    .line 44
    const/4 v2, 0x1

    #@1
    invoke-static {p0, v2}, Landroid/filterfw/format/ObjectFormat;->fromObject(Ljava/lang/Object;I)Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    .line 45
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    new-instance v1, Landroid/filterfw/core/SimpleFrame;

    #@7
    invoke-direct {v1, v0, p1}, Landroid/filterfw/core/SimpleFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@a
    .line 46
    .local v1, result:Landroid/filterfw/core/SimpleFrame;
    invoke-virtual {v1, p0}, Landroid/filterfw/core/SimpleFrame;->setObjectValue(Ljava/lang/Object;)V

    #@d
    .line 47
    return-object v1
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    .line 130
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@2
    instance-of v0, v0, Landroid/graphics/Bitmap;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/graphics/Bitmap;

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@2
    instance-of v0, v0, Ljava/nio/ByteBuffer;

    #@4
    if-eqz v0, :cond_b

    #@6
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@8
    check-cast v0, Ljava/nio/ByteBuffer;

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getFloats()[F
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@2
    instance-of v0, v0, [F

    #@4
    if-eqz v0, :cond_d

    #@6
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@8
    check-cast v0, [F

    #@a
    check-cast v0, [F

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getInts()[I
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@2
    instance-of v0, v0, [I

    #@4
    if-eqz v0, :cond_d

    #@6
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@8
    check-cast v0, [I

    #@a
    check-cast v0, [I

    #@c
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getObjectValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method protected hasNativeAllocation()Z
    .registers 2

    #@0
    .prologue
    .line 77
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected releaseNativeAllocation()V
    .registers 1

    #@0
    .prologue
    .line 82
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter "bitmap"

    #@0
    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->assertFrameMutable()V

    #@3
    .line 125
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SimpleFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 126
    return-void
.end method

.method public setData(Ljava/nio/ByteBuffer;II)V
    .registers 5
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 113
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->assertFrameMutable()V

    #@3
    .line 114
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@6
    move-result-object v0

    #@7
    invoke-static {v0, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/filterfw/core/SimpleFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@e
    .line 115
    return-void
.end method

.method public setFloats([F)V
    .registers 2
    .parameter "floats"

    #@0
    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->assertFrameMutable()V

    #@3
    .line 103
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SimpleFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 104
    return-void
.end method

.method protected setGenericObjectValue(Ljava/lang/Object;)V
    .registers 6
    .parameter "object"

    #@0
    .prologue
    .line 144
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    .line 145
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@7
    move-result-object v1

    #@8
    if-nez v1, :cond_14

    #@a
    .line 146
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@d
    move-result-object v1

    #@e
    invoke-direct {p0, v1}, Landroid/filterfw/core/SimpleFrame;->setFormatObjectClass(Ljava/lang/Class;)V

    #@11
    .line 154
    :cond_11
    iput-object p1, p0, Landroid/filterfw/core/SimpleFrame;->mObject:Ljava/lang/Object;

    #@13
    .line 155
    return-void

    #@14
    .line 147
    :cond_14
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_11

    #@22
    .line 148
    new-instance v1, Ljava/lang/RuntimeException;

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "Attempting to set object value of type \'"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, "\' on "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "SimpleFrame of type \'"

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    const-string v3, "\'!"

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    throw v1
.end method

.method public setInts([I)V
    .registers 2
    .parameter "ints"

    #@0
    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->assertFrameMutable()V

    #@3
    .line 92
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SimpleFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 93
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SimpleFrame ("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ")"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method
