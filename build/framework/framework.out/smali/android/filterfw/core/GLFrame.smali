.class public Landroid/filterfw/core/GLFrame;
.super Landroid/filterfw/core/Frame;
.source "GLFrame.java"


# static fields
.field public static final EXISTING_FBO_BINDING:I = 0x65

.field public static final EXISTING_TEXTURE_BINDING:I = 0x64

.field public static final EXTERNAL_TEXTURE:I = 0x68

.field public static final NEW_FBO_BINDING:I = 0x67

.field public static final NEW_TEXTURE_BINDING:I = 0x66


# instance fields
.field private glFrameId:I

.field private mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

.field private mOwnsTexture:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 361
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 362
    return-void
.end method

.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 4
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3
    .line 56
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/GLFrame;->glFrameId:I

    #@6
    .line 62
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/filterfw/core/GLFrame;->mOwnsTexture:Z

    #@9
    .line 72
    return-void
.end method

.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;IJ)V
    .registers 7
    .parameter "format"
    .parameter "frameManager"
    .parameter "bindingType"
    .parameter "bindingId"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p5}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;IJ)V

    #@3
    .line 56
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/GLFrame;->glFrameId:I

    #@6
    .line 62
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/filterfw/core/GLFrame;->mOwnsTexture:Z

    #@9
    .line 76
    return-void
.end method

.method private assertGLEnvValid()V
    .registers 4

    #@0
    .prologue
    .line 349
    iget-object v0, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->isContextActive()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_58

    #@8
    .line 350
    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->isAnyContextActive()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_33

    #@e
    .line 351
    new-instance v0, Ljava/lang/RuntimeException;

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Attempting to access "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " with foreign GL "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, "context active!"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0

    #@33
    .line 354
    :cond_33
    new-instance v0, Ljava/lang/RuntimeException;

    #@35
    new-instance v1, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v2, "Attempting to access "

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, " with no GL context "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    const-string v2, " active!"

    #@4c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0

    #@58
    .line 358
    :cond_58
    return-void
.end method

.method private native generateNativeMipMap()Z
.end method

.method private native getNativeBitmap(Landroid/graphics/Bitmap;)Z
.end method

.method private native getNativeData()[B
.end method

.method private native getNativeFboId()I
.end method

.method private native getNativeFloats()[F
.end method

.method private native getNativeInts()[I
.end method

.method private native getNativeTextureId()I
.end method

.method private initNew(Z)V
    .registers 5
    .parameter "isExternal"

    #@0
    .prologue
    .line 115
    if-eqz p1, :cond_12

    #@2
    .line 116
    iget-object v0, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@4
    invoke-direct {p0, v0}, Landroid/filterfw/core/GLFrame;->nativeAllocateExternal(Landroid/filterfw/core/GLEnvironment;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_32

    #@a
    .line 117
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    const-string v1, "Could not allocate external GL frame!"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 120
    :cond_12
    iget-object v0, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@14
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@1b
    move-result v1

    #@1c
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@23
    move-result v2

    #@24
    invoke-direct {p0, v0, v1, v2}, Landroid/filterfw/core/GLFrame;->nativeAllocate(Landroid/filterfw/core/GLEnvironment;II)Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_32

    #@2a
    .line 121
    new-instance v0, Ljava/lang/RuntimeException;

    #@2c
    const-string v1, "Could not allocate GL frame!"

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 124
    :cond_32
    return-void
.end method

.method private initWithFbo(I)V
    .registers 6
    .parameter "fboId"

    #@0
    .prologue
    .line 137
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@7
    move-result v1

    #@8
    .line 138
    .local v1, width:I
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@f
    move-result v0

    #@10
    .line 139
    .local v0, height:I
    iget-object v2, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@12
    invoke-direct {p0, v2, p1, v1, v0}, Landroid/filterfw/core/GLFrame;->nativeAllocateWithFbo(Landroid/filterfw/core/GLEnvironment;III)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_20

    #@18
    .line 140
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "Could not allocate FBO backed GL frame!"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 142
    :cond_20
    return-void
.end method

.method private initWithTexture(I)V
    .registers 6
    .parameter "texId"

    #@0
    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@7
    move-result v1

    #@8
    .line 128
    .local v1, width:I
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@f
    move-result v0

    #@10
    .line 129
    .local v0, height:I
    iget-object v2, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@12
    invoke-direct {p0, v2, p1, v1, v0}, Landroid/filterfw/core/GLFrame;->nativeAllocateWithTexture(Landroid/filterfw/core/GLEnvironment;III)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_20

    #@18
    .line 130
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "Could not allocate texture backed GL frame!"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 132
    :cond_20
    const/4 v2, 0x0

    #@21
    iput-boolean v2, p0, Landroid/filterfw/core/GLFrame;->mOwnsTexture:Z

    #@23
    .line 133
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->markReadOnly()V

    #@26
    .line 134
    return-void
.end method

.method private native nativeAllocate(Landroid/filterfw/core/GLEnvironment;II)Z
.end method

.method private native nativeAllocateExternal(Landroid/filterfw/core/GLEnvironment;)Z
.end method

.method private native nativeAllocateWithFbo(Landroid/filterfw/core/GLEnvironment;III)Z
.end method

.method private native nativeAllocateWithTexture(Landroid/filterfw/core/GLEnvironment;III)Z
.end method

.method private native nativeCopyFromGL(Landroid/filterfw/core/GLFrame;)Z
.end method

.method private native nativeCopyFromNative(Landroid/filterfw/core/NativeFrame;)Z
.end method

.method private native nativeDeallocate()Z
.end method

.method private native nativeDetachTexFromFbo()Z
.end method

.method private native nativeFocus()Z
.end method

.method private native nativeReattachTexToFbo()Z
.end method

.method private native nativeResetParams()Z
.end method

.method private native setNativeBitmap(Landroid/graphics/Bitmap;I)Z
.end method

.method private native setNativeData([BII)Z
.end method

.method private native setNativeFloats([F)Z
.end method

.method private native setNativeInts([I)Z
.end method

.method private native setNativeTextureParam(II)Z
.end method

.method private native setNativeViewport(IIII)Z
.end method


# virtual methods
.method flushGPU(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 145
    invoke-static {}, Landroid/filterfw/core/GLFrameTimer;->get()Landroid/filterfw/core/StopWatchMap;

    #@3
    move-result-object v0

    #@4
    .line 146
    .local v0, timer:Landroid/filterfw/core/StopWatchMap;
    iget-boolean v1, v0, Landroid/filterfw/core/StopWatchMap;->LOG_MFF_RUNNING_TIMES:Z

    #@6
    if-eqz v1, :cond_37

    #@8
    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "glFinish "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/filterfw/core/StopWatchMap;->start(Ljava/lang/String;)V

    #@1e
    .line 148
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    #@21
    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "glFinish "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v0, v1}, Landroid/filterfw/core/StopWatchMap;->stop(Ljava/lang/String;)V

    #@37
    .line 151
    :cond_37
    return-void
.end method

.method public focus()V
    .registers 3

    #@0
    .prologue
    .line 312
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->nativeFocus()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 313
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Could not focus on GLFrame for drawing!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 315
    :cond_e
    return-void
.end method

.method public generateMipMap()V
    .registers 3

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 288
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 289
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->generateNativeMipMap()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 290
    new-instance v0, Ljava/lang/RuntimeException;

    #@e
    const-string v1, "Could not generate mip-map for GL frame!"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 292
    :cond_14
    return-void
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 5

    #@0
    .prologue
    .line 242
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 243
    const-string v1, "getBitmap"

    #@5
    invoke-virtual {p0, v1}, Landroid/filterfw/core/GLFrame;->flushGPU(Ljava/lang/String;)V

    #@8
    .line 244
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@f
    move-result v1

    #@10
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@17
    move-result v2

    #@18
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@1a
    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@1d
    move-result-object v0

    #@1e
    .line 247
    .local v0, result:Landroid/graphics/Bitmap;
    invoke-direct {p0, v0}, Landroid/filterfw/core/GLFrame;->getNativeBitmap(Landroid/graphics/Bitmap;)Z

    #@21
    move-result v1

    #@22
    if-nez v1, :cond_2c

    #@24
    .line 248
    new-instance v1, Ljava/lang/RuntimeException;

    #@26
    const-string v2, "Could not get bitmap data from GL frame!"

    #@28
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v1

    #@2c
    .line 250
    :cond_2c
    return-object v0
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 2

    #@0
    .prologue
    .line 220
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 221
    const-string v0, "getData"

    #@5
    invoke-virtual {p0, v0}, Landroid/filterfw/core/GLFrame;->flushGPU(Ljava/lang/String;)V

    #@8
    .line 222
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeData()[B

    #@b
    move-result-object v0

    #@c
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public getFboId()I
    .registers 2

    #@0
    .prologue
    .line 308
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeFboId()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getFloats()[F
    .registers 2

    #@0
    .prologue
    .line 201
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 202
    const-string v0, "getFloats"

    #@5
    invoke-virtual {p0, v0}, Landroid/filterfw/core/GLFrame;->flushGPU(Ljava/lang/String;)V

    #@8
    .line 203
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeFloats()[F

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getGLEnvironment()Landroid/filterfw/core/GLEnvironment;
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@2
    return-object v0
.end method

.method public getInts()[I
    .registers 2

    #@0
    .prologue
    .line 185
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 186
    const-string v0, "getInts"

    #@5
    invoke-virtual {p0, v0}, Landroid/filterfw/core/GLFrame;->flushGPU(Ljava/lang/String;)V

    #@8
    .line 187
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeInts()[I

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getObjectValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 170
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 171
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeData()[B

    #@6
    move-result-object v0

    #@7
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getTextureId()I
    .registers 2

    #@0
    .prologue
    .line 304
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->getNativeTextureId()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected declared-synchronized hasNativeAllocation()Z
    .registers 3

    #@0
    .prologue
    .line 155
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/GLFrame;->glFrameId:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    #@3
    const/4 v1, -0x1

    #@4
    if-eq v0, v1, :cond_9

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    monitor-exit p0

    #@8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_7

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method init(Landroid/filterfw/core/GLEnvironment;)V
    .registers 8
    .parameter "glEnv"

    #@0
    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v1

    #@4
    .line 80
    .local v1, format:Landroid/filterfw/core/FrameFormat;
    iput-object p1, p0, Landroid/filterfw/core/GLFrame;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@6
    .line 83
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@9
    move-result v3

    #@a
    const/4 v4, 0x4

    #@b
    if-eq v3, v4, :cond_15

    #@d
    .line 84
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v4, "GL frames must have 4 bytes per sample!"

    #@11
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v3

    #@15
    .line 85
    :cond_15
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@18
    move-result v3

    #@19
    const/4 v4, 0x2

    #@1a
    if-eq v3, v4, :cond_24

    #@1c
    .line 86
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1e
    const-string v4, "GL frames must be 2-dimensional!"

    #@20
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v3

    #@24
    .line 87
    :cond_24
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@2b
    move-result v3

    #@2c
    if-gez v3, :cond_36

    #@2e
    .line 88
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@30
    const-string v4, "Initializing GL frame with zero size!"

    #@32
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v3

    #@36
    .line 92
    :cond_36
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getBindingType()I

    #@39
    move-result v0

    #@3a
    .line 93
    .local v0, bindingType:I
    const/4 v2, 0x1

    #@3b
    .line 94
    .local v2, reusable:Z
    if-nez v0, :cond_45

    #@3d
    .line 95
    const/4 v3, 0x0

    #@3e
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initNew(Z)V

    #@41
    .line 111
    :goto_41
    invoke-virtual {p0, v2}, Landroid/filterfw/core/GLFrame;->setReusable(Z)V

    #@44
    .line 112
    return-void

    #@45
    .line 96
    :cond_45
    const/16 v3, 0x68

    #@47
    if-ne v0, v3, :cond_4f

    #@49
    .line 97
    const/4 v3, 0x1

    #@4a
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initNew(Z)V

    #@4d
    .line 98
    const/4 v2, 0x0

    #@4e
    goto :goto_41

    #@4f
    .line 99
    :cond_4f
    const/16 v3, 0x64

    #@51
    if-ne v0, v3, :cond_5c

    #@53
    .line 100
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getBindingId()J

    #@56
    move-result-wide v3

    #@57
    long-to-int v3, v3

    #@58
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initWithTexture(I)V

    #@5b
    goto :goto_41

    #@5c
    .line 101
    :cond_5c
    const/16 v3, 0x65

    #@5e
    if-ne v0, v3, :cond_69

    #@60
    .line 102
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getBindingId()J

    #@63
    move-result-wide v3

    #@64
    long-to-int v3, v3

    #@65
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initWithFbo(I)V

    #@68
    goto :goto_41

    #@69
    .line 103
    :cond_69
    const/16 v3, 0x66

    #@6b
    if-ne v0, v3, :cond_76

    #@6d
    .line 104
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getBindingId()J

    #@70
    move-result-wide v3

    #@71
    long-to-int v3, v3

    #@72
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initWithTexture(I)V

    #@75
    goto :goto_41

    #@76
    .line 105
    :cond_76
    const/16 v3, 0x67

    #@78
    if-ne v0, v3, :cond_83

    #@7a
    .line 106
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getBindingId()J

    #@7d
    move-result-wide v3

    #@7e
    long-to-int v3, v3

    #@7f
    invoke-direct {p0, v3}, Landroid/filterfw/core/GLFrame;->initWithFbo(I)V

    #@82
    goto :goto_41

    #@83
    .line 108
    :cond_83
    new-instance v3, Ljava/lang/RuntimeException;

    #@85
    new-instance v4, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v5, "Attempting to create GL frame with unknown binding type "

    #@8c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v4

    #@90
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    const-string v5, "!"

    #@96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v4

    #@9e
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a1
    throw v3
.end method

.method protected onFrameFetch()V
    .registers 2

    #@0
    .prologue
    .line 341
    iget-boolean v0, p0, Landroid/filterfw/core/GLFrame;->mOwnsTexture:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 344
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->nativeReattachTexToFbo()Z

    #@7
    .line 346
    :cond_7
    return-void
.end method

.method protected onFrameStore()V
    .registers 2

    #@0
    .prologue
    .line 333
    iget-boolean v0, p0, Landroid/filterfw/core/GLFrame;->mOwnsTexture:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 335
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->nativeDetachTexFromFbo()Z

    #@7
    .line 337
    :cond_7
    return-void
.end method

.method protected declared-synchronized releaseNativeAllocation()V
    .registers 2

    #@0
    .prologue
    .line 160
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->nativeDeallocate()Z

    #@4
    .line 161
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/filterfw/core/GLFrame;->glFrameId:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 162
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 160
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method protected reset(Landroid/filterfw/core/FrameFormat;)V
    .registers 4
    .parameter "newFormat"

    #@0
    .prologue
    .line 325
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->nativeResetParams()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 326
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Could not reset GLFrame texture parameters!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 328
    :cond_e
    invoke-super {p0, p1}, Landroid/filterfw/core/Frame;->reset(Landroid/filterfw/core/FrameFormat;)V

    #@11
    .line 329
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "bitmap"

    #@0
    .prologue
    .line 227
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 228
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 229
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@d
    move-result v1

    #@e
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@11
    move-result v2

    #@12
    if-ne v1, v2, :cond_22

    #@14
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@1b
    move-result v1

    #@1c
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@1f
    move-result v2

    #@20
    if-eq v1, v2, :cond_2a

    #@22
    .line 231
    :cond_22
    new-instance v1, Ljava/lang/RuntimeException;

    #@24
    const-string v2, "Bitmap dimensions do not match GL frame dimensions!"

    #@26
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 233
    :cond_2a
    invoke-static {p1}, Landroid/filterfw/core/GLFrame;->convertBitmapToRGBA(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@2d
    move-result-object v0

    #@2e
    .line 234
    .local v0, rgbaBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    #@31
    move-result v1

    #@32
    invoke-direct {p0, v0, v1}, Landroid/filterfw/core/GLFrame;->setNativeBitmap(Landroid/graphics/Bitmap;I)Z

    #@35
    move-result v1

    #@36
    if-nez v1, :cond_40

    #@38
    .line 235
    new-instance v1, Ljava/lang/RuntimeException;

    #@3a
    const-string v2, "Could not set GL frame bitmap data!"

    #@3c
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v1

    #@40
    .line 238
    :cond_40
    return-void
.end method

.method public setData(Ljava/nio/ByteBuffer;II)V
    .registers 7
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 208
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 209
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 210
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@9
    move-result-object v0

    #@a
    .line 211
    .local v0, bytes:[B
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@11
    move-result v1

    #@12
    array-length v2, v0

    #@13
    if-eq v1, v2, :cond_1d

    #@15
    .line 212
    new-instance v1, Ljava/lang/RuntimeException;

    #@17
    const-string v2, "Data size in setData does not match GL frame size!"

    #@19
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 213
    :cond_1d
    invoke-direct {p0, v0, p2, p3}, Landroid/filterfw/core/GLFrame;->setNativeData([BII)Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_2b

    #@23
    .line 214
    new-instance v1, Ljava/lang/RuntimeException;

    #@25
    const-string v2, "Could not set GL frame data!"

    #@27
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v1

    #@2b
    .line 216
    :cond_2b
    return-void
.end method

.method public setDataFromFrame(Landroid/filterfw/core/Frame;)V
    .registers 5
    .parameter "frame"

    #@0
    .prologue
    .line 255
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@3
    .line 258
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@a
    move-result v0

    #@b
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@12
    move-result v1

    #@13
    if-ge v0, v1, :cond_55

    #@15
    .line 259
    new-instance v0, Ljava/lang/RuntimeException;

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "Attempting to assign frame of size "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@29
    move-result v2

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, " to "

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    const-string/jumbo v2, "smaller GL frame of size "

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@42
    move-result v2

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    const-string v2, "!"

    #@49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v1

    #@4d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v1

    #@51
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@54
    throw v0

    #@55
    .line 265
    :cond_55
    instance-of v0, p1, Landroid/filterfw/core/NativeFrame;

    #@57
    if-eqz v0, :cond_5f

    #@59
    .line 266
    check-cast p1, Landroid/filterfw/core/NativeFrame;

    #@5b
    .end local p1
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLFrame;->nativeCopyFromNative(Landroid/filterfw/core/NativeFrame;)Z

    #@5e
    .line 274
    :goto_5e
    return-void

    #@5f
    .line 267
    .restart local p1
    :cond_5f
    instance-of v0, p1, Landroid/filterfw/core/GLFrame;

    #@61
    if-eqz v0, :cond_69

    #@63
    .line 268
    check-cast p1, Landroid/filterfw/core/GLFrame;

    #@65
    .end local p1
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLFrame;->nativeCopyFromGL(Landroid/filterfw/core/GLFrame;)Z

    #@68
    goto :goto_5e

    #@69
    .line 269
    .restart local p1
    :cond_69
    instance-of v0, p1, Landroid/filterfw/core/SimpleFrame;

    #@6b
    if-eqz v0, :cond_75

    #@6d
    .line 270
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@70
    move-result-object v0

    #@71
    invoke-virtual {p0, v0}, Landroid/filterfw/core/GLFrame;->setObjectValue(Ljava/lang/Object;)V

    #@74
    goto :goto_5e

    #@75
    .line 272
    :cond_75
    invoke-super {p0, p1}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@78
    goto :goto_5e
.end method

.method public setFloats([F)V
    .registers 4
    .parameter "floats"

    #@0
    .prologue
    .line 192
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 193
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 194
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLFrame;->setNativeFloats([F)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 195
    new-instance v0, Ljava/lang/RuntimeException;

    #@e
    const-string v1, "Could not set int values for GL frame!"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 197
    :cond_14
    return-void
.end method

.method public setInts([I)V
    .registers 4
    .parameter "ints"

    #@0
    .prologue
    .line 176
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 177
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 178
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLFrame;->setNativeInts([I)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 179
    new-instance v0, Ljava/lang/RuntimeException;

    #@e
    const-string v1, "Could not set int values for GL frame!"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 181
    :cond_14
    return-void
.end method

.method public setTextureParameter(II)V
    .registers 6
    .parameter "param"
    .parameter "value"

    #@0
    .prologue
    .line 295
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 296
    invoke-direct {p0}, Landroid/filterfw/core/GLFrame;->assertGLEnvValid()V

    #@6
    .line 297
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/GLFrame;->setNativeTextureParam(II)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_3b

    #@c
    .line 298
    new-instance v0, Ljava/lang/RuntimeException;

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Could not set texture value "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " = "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, "for GLFrame!"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v0

    #@3b
    .line 301
    :cond_3b
    return-void
.end method

.method public setViewport(IIII)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 278
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/filterfw/core/GLFrame;->setNativeViewport(IIII)Z

    #@6
    .line 279
    return-void
.end method

.method public setViewport(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "rect"

    #@0
    .prologue
    .line 282
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->assertFrameMutable()V

    #@3
    .line 283
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@5
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@7
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@9
    iget v3, p1, Landroid/graphics/Rect;->left:I

    #@b
    sub-int/2addr v2, v3

    #@c
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@e
    iget v4, p1, Landroid/graphics/Rect;->top:I

    #@10
    sub-int/2addr v3, v4

    #@11
    invoke-direct {p0, v0, v1, v2, v3}, Landroid/filterfw/core/GLFrame;->setNativeViewport(IIII)Z

    #@14
    .line 284
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "GLFrame id: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/filterfw/core/GLFrame;->glFrameId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " ("

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, ") with texture ID "

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getTextureId()I

    #@28
    move-result v1

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    const-string v1, ", FBO ID "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {p0}, Landroid/filterfw/core/GLFrame;->getFboId()I

    #@36
    move-result v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    return-object v0
.end method
