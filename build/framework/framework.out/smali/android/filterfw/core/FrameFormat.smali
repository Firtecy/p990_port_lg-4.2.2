.class public Landroid/filterfw/core/FrameFormat;
.super Ljava/lang/Object;
.source "FrameFormat.java"


# static fields
.field public static final BYTES_PER_SAMPLE_UNSPECIFIED:I = 0x1

.field protected static final SIZE_UNKNOWN:I = -0x1

.field public static final SIZE_UNSPECIFIED:I = 0x0

.field public static final TARGET_GPU:I = 0x3

.field public static final TARGET_NATIVE:I = 0x2

.field public static final TARGET_RS:I = 0x5

.field public static final TARGET_SIMPLE:I = 0x1

.field public static final TARGET_UNSPECIFIED:I = 0x0

.field public static final TARGET_VERTEXBUFFER:I = 0x4

.field public static final TYPE_BIT:I = 0x1

.field public static final TYPE_BYTE:I = 0x2

.field public static final TYPE_DOUBLE:I = 0x6

.field public static final TYPE_FLOAT:I = 0x5

.field public static final TYPE_INT16:I = 0x3

.field public static final TYPE_INT32:I = 0x4

.field public static final TYPE_OBJECT:I = 0x8

.field public static final TYPE_POINTER:I = 0x7

.field public static final TYPE_UNSPECIFIED:I


# instance fields
.field protected mBaseType:I

.field protected mBytesPerSample:I

.field protected mDimensions:[I

.field protected mMetaData:Landroid/filterfw/core/KeyValueMap;

.field protected mObjectClass:Ljava/lang/Class;

.field protected mSize:I

.field protected mTarget:I


# direct methods
.method protected constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 64
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 56
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@6
    .line 57
    const/4 v0, 0x1

    #@7
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@9
    .line 58
    const/4 v0, -0x1

    #@a
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@c
    .line 59
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@e
    .line 65
    return-void
.end method

.method public constructor <init>(II)V
    .registers 5
    .parameter "baseType"
    .parameter "target"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 56
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@6
    .line 57
    const/4 v0, 0x1

    #@7
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@9
    .line 58
    const/4 v0, -0x1

    #@a
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@c
    .line 59
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@e
    .line 68
    iput p1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@10
    .line 69
    iput p2, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@12
    .line 70
    invoke-direct {p0}, Landroid/filterfw/core/FrameFormat;->initDefaults()V

    #@15
    .line 71
    return-void
.end method

.method public static baseTypeToString(I)Ljava/lang/String;
    .registers 2
    .parameter "baseType"

    #@0
    .prologue
    .line 344
    packed-switch p0, :pswitch_data_26

    #@3
    .line 354
    const-string/jumbo v0, "unknown"

    #@6
    :goto_6
    return-object v0

    #@7
    .line 345
    :pswitch_7
    const-string/jumbo v0, "unspecified"

    #@a
    goto :goto_6

    #@b
    .line 346
    :pswitch_b
    const-string v0, "bit"

    #@d
    goto :goto_6

    #@e
    .line 347
    :pswitch_e
    const-string v0, "byte"

    #@10
    goto :goto_6

    #@11
    .line 348
    :pswitch_11
    const-string v0, "int"

    #@13
    goto :goto_6

    #@14
    .line 349
    :pswitch_14
    const-string v0, "int"

    #@16
    goto :goto_6

    #@17
    .line 350
    :pswitch_17
    const-string v0, "float"

    #@19
    goto :goto_6

    #@1a
    .line 351
    :pswitch_1a
    const-string v0, "double"

    #@1c
    goto :goto_6

    #@1d
    .line 352
    :pswitch_1d
    const-string/jumbo v0, "pointer"

    #@20
    goto :goto_6

    #@21
    .line 353
    :pswitch_21
    const-string/jumbo v0, "object"

    #@24
    goto :goto_6

    #@25
    .line 344
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_21
    .end packed-switch
.end method

.method public static bytesPerSampleOf(I)I
    .registers 2
    .parameter "baseType"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 311
    packed-switch p0, :pswitch_data_c

    #@4
    .line 324
    :goto_4
    :pswitch_4
    return v0

    #@5
    .line 316
    :pswitch_5
    const/4 v0, 0x2

    #@6
    goto :goto_4

    #@7
    .line 320
    :pswitch_7
    const/4 v0, 0x4

    #@8
    goto :goto_4

    #@9
    .line 322
    :pswitch_9
    const/16 v0, 0x8

    #@b
    goto :goto_4

    #@c
    .line 311
    :pswitch_data_c
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method

.method public static dimensionsToString([I)Ljava/lang/String;
    .registers 6
    .parameter "dimensions"

    #@0
    .prologue
    .line 329
    new-instance v0, Ljava/lang/StringBuffer;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@5
    .line 330
    .local v0, buffer:Ljava/lang/StringBuffer;
    if-eqz p0, :cond_3a

    #@7
    .line 331
    array-length v2, p0

    #@8
    .line 332
    .local v2, n:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v2, :cond_3a

    #@b
    .line 333
    aget v3, p0, v1

    #@d
    if-nez v3, :cond_17

    #@f
    .line 334
    const-string v3, "[]"

    #@11
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@14
    .line 332
    :goto_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_9

    #@17
    .line 336
    :cond_17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "["

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    aget v4, p0, v1

    #@24
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, "]"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@39
    goto :goto_14

    #@3a
    .line 340
    .end local v1           #i:I
    .end local v2           #n:I
    :cond_3a
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    return-object v3
.end method

.method private initDefaults()V
    .registers 2

    #@0
    .prologue
    .line 419
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@2
    invoke-static {v0}, Landroid/filterfw/core/FrameFormat;->bytesPerSampleOf(I)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@8
    .line 420
    return-void
.end method

.method public static metaDataToString(Landroid/filterfw/core/KeyValueMap;)Ljava/lang/String;
    .registers 6
    .parameter "metaData"

    #@0
    .prologue
    .line 371
    if-nez p0, :cond_5

    #@2
    .line 372
    const-string v3, ""

    #@4
    .line 380
    :goto_4
    return-object v3

    #@5
    .line 374
    :cond_5
    new-instance v0, Ljava/lang/StringBuffer;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    #@a
    .line 375
    .local v0, buffer:Ljava/lang/StringBuffer;
    const-string/jumbo v3, "{ "

    #@d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@10
    .line 376
    invoke-virtual {p0}, Landroid/filterfw/core/KeyValueMap;->entrySet()Ljava/util/Set;

    #@13
    move-result-object v3

    #@14
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v2

    #@18
    .local v2, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_4f

    #@1e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Ljava/util/Map$Entry;

    #@24
    .line 377
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Ljava/lang/String;

    #@2f
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ": "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    const-string v4, " "

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@4e
    goto :goto_18

    #@4f
    .line 379
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_4f
    const-string/jumbo v3, "}"

    #@52
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@55
    .line 380
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    goto :goto_4
.end method

.method public static readTargetString(Ljava/lang/String;)I
    .registers 4
    .parameter "targetString"

    #@0
    .prologue
    .line 385
    const-string v0, "CPU"

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    const-string v0, "NATIVE"

    #@a
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_12

    #@10
    .line 386
    :cond_10
    const/4 v0, 0x2

    #@11
    .line 394
    :goto_11
    return v0

    #@12
    .line 387
    :cond_12
    const-string v0, "GPU"

    #@14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    .line 388
    const/4 v0, 0x3

    #@1b
    goto :goto_11

    #@1c
    .line 389
    :cond_1c
    const-string v0, "SIMPLE"

    #@1e
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@21
    move-result v0

    #@22
    if-eqz v0, :cond_26

    #@24
    .line 390
    const/4 v0, 0x1

    #@25
    goto :goto_11

    #@26
    .line 391
    :cond_26
    const-string v0, "VERTEXBUFFER"

    #@28
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_30

    #@2e
    .line 392
    const/4 v0, 0x4

    #@2f
    goto :goto_11

    #@30
    .line 393
    :cond_30
    const-string v0, "UNSPECIFIED"

    #@32
    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@35
    move-result v0

    #@36
    if-eqz v0, :cond_3a

    #@38
    .line 394
    const/4 v0, 0x0

    #@39
    goto :goto_11

    #@3a
    .line 396
    :cond_3a
    new-instance v0, Ljava/lang/RuntimeException;

    #@3c
    new-instance v1, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v2, "Unknown target type \'"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    const-string v2, "\'!"

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    throw v0
.end method

.method public static targetToString(I)Ljava/lang/String;
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 359
    packed-switch p0, :pswitch_data_1e

    #@3
    .line 366
    const-string/jumbo v0, "unknown"

    #@6
    :goto_6
    return-object v0

    #@7
    .line 360
    :pswitch_7
    const-string/jumbo v0, "unspecified"

    #@a
    goto :goto_6

    #@b
    .line 361
    :pswitch_b
    const-string/jumbo v0, "simple"

    #@e
    goto :goto_6

    #@f
    .line 362
    :pswitch_f
    const-string/jumbo v0, "native"

    #@12
    goto :goto_6

    #@13
    .line 363
    :pswitch_13
    const-string v0, "gpu"

    #@15
    goto :goto_6

    #@16
    .line 364
    :pswitch_16
    const-string/jumbo v0, "vbo"

    #@19
    goto :goto_6

    #@1a
    .line 365
    :pswitch_1a
    const-string/jumbo v0, "renderscript"

    #@1d
    goto :goto_6

    #@1e
    .line 359
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_13
        :pswitch_16
        :pswitch_1a
    .end packed-switch
.end method

.method public static unspecified()Landroid/filterfw/core/FrameFormat;
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 74
    new-instance v0, Landroid/filterfw/core/FrameFormat;

    #@3
    invoke-direct {v0, v1, v1}, Landroid/filterfw/core/FrameFormat;-><init>(II)V

    #@6
    return-object v0
.end method


# virtual methods
.method calcSize([I)I
    .registers 8
    .parameter "dimensions"

    #@0
    .prologue
    .line 424
    if-eqz p1, :cond_14

    #@2
    array-length v5, p1

    #@3
    if-lez v5, :cond_14

    #@5
    .line 425
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@8
    move-result v4

    #@9
    .line 426
    .local v4, size:I
    move-object v0, p1

    #@a
    .local v0, arr$:[I
    array-length v3, v0

    #@b
    .local v3, len$:I
    const/4 v2, 0x0

    #@c
    .local v2, i$:I
    :goto_c
    if-ge v2, v3, :cond_15

    #@e
    aget v1, v0, v2

    #@10
    .line 427
    .local v1, dim:I
    mul-int/2addr v4, v1

    #@11
    .line 426
    add-int/lit8 v2, v2, 0x1

    #@13
    goto :goto_c

    #@14
    .line 431
    .end local v0           #arr$:[I
    .end local v1           #dim:I
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #size:I
    :cond_14
    const/4 v4, 0x0

    #@15
    :cond_15
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 172
    if-ne p0, p1, :cond_5

    #@4
    .line 181
    :cond_4
    :goto_4
    return v1

    #@5
    .line 176
    :cond_5
    instance-of v3, p1, Landroid/filterfw/core/FrameFormat;

    #@7
    if-nez v3, :cond_b

    #@9
    move v1, v2

    #@a
    .line 177
    goto :goto_4

    #@b
    :cond_b
    move-object v0, p1

    #@c
    .line 180
    check-cast v0, Landroid/filterfw/core/FrameFormat;

    #@e
    .line 181
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    iget v3, v0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@10
    iget v4, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@12
    if-ne v3, v4, :cond_34

    #@14
    iget v3, v0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@16
    iget v4, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@18
    if-ne v3, v4, :cond_34

    #@1a
    iget v3, v0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@1c
    iget v4, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@1e
    if-ne v3, v4, :cond_34

    #@20
    iget-object v3, v0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@22
    iget-object v4, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@24
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_34

    #@2a
    iget-object v3, v0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2c
    iget-object v4, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2e
    invoke-virtual {v3, v4}, Landroid/filterfw/core/KeyValueMap;->equals(Ljava/lang/Object;)Z

    #@31
    move-result v3

    #@32
    if-nez v3, :cond_4

    #@34
    :cond_34
    move v1, v2

    #@35
    goto :goto_4
.end method

.method public getBaseType()I
    .registers 2

    #@0
    .prologue
    .line 78
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@2
    return v0
.end method

.method public getBytesPerSample()I
    .registers 2

    #@0
    .prologue
    .line 86
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@2
    return v0
.end method

.method public getDepth()I
    .registers 3

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@6
    array-length v0, v0

    #@7
    const/4 v1, 0x3

    #@8
    if-lt v0, v1, :cond_10

    #@a
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@c
    const/4 v1, 0x2

    #@d
    aget v0, v0, v1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_f
.end method

.method public getDimension(I)I
    .registers 3
    .parameter "i"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public getDimensionCount()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@8
    array-length v0, v0

    #@9
    goto :goto_5
.end method

.method public getDimensions()[I
    .registers 2

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    return-object v0
.end method

.method public getHeight()I
    .registers 3

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@6
    array-length v0, v0

    #@7
    const/4 v1, 0x2

    #@8
    if-lt v0, v1, :cond_10

    #@a
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@c
    const/4 v1, 0x1

    #@d
    aget v0, v0, v1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_f
.end method

.method public getLength()I
    .registers 3

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@6
    array-length v0, v0

    #@7
    const/4 v1, 0x1

    #@8
    if-lt v0, v1, :cond_10

    #@a
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@c
    const/4 v1, 0x0

    #@d
    aget v0, v0, v1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, -0x1

    #@11
    goto :goto_f
.end method

.method public getMetaValue(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@6
    invoke-virtual {v0, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getNumberOfDimensions()I
    .registers 2

    #@0
    .prologue
    .line 131
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@6
    array-length v0, v0

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public getObjectClass()Ljava/lang/Class;
    .registers 2

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mObjectClass:Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method public getSize()I
    .registers 3

    #@0
    .prologue
    .line 151
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_d

    #@5
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@7
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FrameFormat;->calcSize([I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@d
    .line 152
    :cond_d
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@f
    return v0
.end method

.method public getTarget()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@2
    return v0
.end method

.method public getValuesPerSample()I
    .registers 3

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@2
    iget v1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@4
    invoke-static {v1}, Landroid/filterfw/core/FrameFormat;->bytesPerSampleOf(I)I

    #@7
    move-result v1

    #@8
    div-int/2addr v0, v1

    #@9
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 139
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getLength()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public hasMetaKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 110
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@6
    invoke-virtual {v0, p1}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public hasMetaKey(Ljava/lang/String;Ljava/lang/Class;)Z
    .registers 6
    .parameter "key"
    .parameter "expectedClass"

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2
    if-eqz v0, :cond_5b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@6
    invoke-virtual {v0, p1}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_5b

    #@c
    .line 115
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@e
    invoke-virtual {v0, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {p2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@19
    move-result v0

    #@1a
    if-nez v0, :cond_59

    #@1c
    .line 116
    new-instance v0, Ljava/lang/RuntimeException;

    #@1e
    new-instance v1, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v2, "FrameFormat meta-key \'"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, "\' is of type "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@35
    invoke-virtual {v2, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " but expected to be of type "

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    const-string v2, "!"

    #@4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v1

    #@51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    throw v0

    #@59
    .line 121
    :cond_59
    const/4 v0, 0x1

    #@5a
    .line 123
    :goto_5a
    return v0

    #@5b
    :cond_5b
    const/4 v0, 0x0

    #@5c
    goto :goto_5a
.end method

.method public hashCode()I
    .registers 3

    #@0
    .prologue
    .line 190
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@2
    xor-int/lit16 v0, v0, 0x1073

    #@4
    iget v1, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@6
    xor-int/2addr v0, v1

    #@7
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@a
    move-result v1

    #@b
    xor-int/2addr v0, v1

    #@c
    return v0
.end method

.method public isBinaryDataType()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 82
    iget v1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@3
    if-lt v1, v0, :cond_b

    #@5
    iget v1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@7
    const/4 v2, 0x6

    #@8
    if-gt v1, v2, :cond_b

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isCompatibleWith(Landroid/filterfw/core/FrameFormat;)Z
    .registers 10
    .parameter "specification"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 195
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_13

    #@8
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@b
    move-result v6

    #@c
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@f
    move-result v7

    #@10
    if-eq v6, v7, :cond_13

    #@12
    .line 246
    :cond_12
    :goto_12
    return v4

    #@13
    .line 201
    :cond_13
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@16
    move-result v6

    #@17
    if-eqz v6, :cond_23

    #@19
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@1c
    move-result v6

    #@1d
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@20
    move-result v7

    #@21
    if-ne v6, v7, :cond_12

    #@23
    .line 207
    :cond_23
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@26
    move-result v6

    #@27
    if-eq v6, v5, :cond_33

    #@29
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@2c
    move-result v6

    #@2d
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@30
    move-result v7

    #@31
    if-ne v6, v7, :cond_12

    #@33
    .line 213
    :cond_33
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@36
    move-result v6

    #@37
    if-lez v6, :cond_43

    #@39
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@3c
    move-result v6

    #@3d
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@40
    move-result v7

    #@41
    if-ne v6, v7, :cond_12

    #@43
    .line 219
    :cond_43
    const/4 v0, 0x0

    #@44
    .local v0, i:I
    :goto_44
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@47
    move-result v6

    #@48
    if-ge v0, v6, :cond_59

    #@4a
    .line 220
    invoke-virtual {p1, v0}, Landroid/filterfw/core/FrameFormat;->getDimension(I)I

    #@4d
    move-result v2

    #@4e
    .line 221
    .local v2, specDim:I
    if-eqz v2, :cond_56

    #@50
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FrameFormat;->getDimension(I)I

    #@53
    move-result v6

    #@54
    if-ne v6, v2, :cond_12

    #@56
    .line 219
    :cond_56
    add-int/lit8 v0, v0, 0x1

    #@58
    goto :goto_44

    #@59
    .line 227
    .end local v2           #specDim:I
    :cond_59
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@5c
    move-result-object v6

    #@5d
    if-eqz v6, :cond_73

    #@5f
    .line 228
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@62
    move-result-object v6

    #@63
    if-eqz v6, :cond_12

    #@65
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@70
    move-result v6

    #@71
    if-eqz v6, :cond_12

    #@73
    .line 235
    :cond_73
    iget-object v6, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@75
    if-eqz v6, :cond_ad

    #@77
    .line 236
    iget-object v6, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@79
    invoke-virtual {v6}, Landroid/filterfw/core/KeyValueMap;->keySet()Ljava/util/Set;

    #@7c
    move-result-object v6

    #@7d
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@80
    move-result-object v1

    #@81
    .local v1, i$:Ljava/util/Iterator;
    :cond_81
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@84
    move-result v6

    #@85
    if-eqz v6, :cond_ad

    #@87
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@8a
    move-result-object v3

    #@8b
    check-cast v3, Ljava/lang/String;

    #@8d
    .line 237
    .local v3, specKey:Ljava/lang/String;
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@8f
    if-eqz v6, :cond_12

    #@91
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@93
    invoke-virtual {v6, v3}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@96
    move-result v6

    #@97
    if-eqz v6, :cond_12

    #@99
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@9b
    invoke-virtual {v6, v3}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9e
    move-result-object v6

    #@9f
    iget-object v7, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@a1
    invoke-virtual {v7, v3}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a4
    move-result-object v7

    #@a5
    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@a8
    move-result v6

    #@a9
    if-nez v6, :cond_81

    #@ab
    goto/16 :goto_12

    #@ad
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #specKey:Ljava/lang/String;
    :cond_ad
    move v4, v5

    #@ae
    .line 246
    goto/16 :goto_12
.end method

.method isReplaceableBy(Landroid/filterfw/core/FrameFormat;)Z
    .registers 4
    .parameter "format"

    #@0
    .prologue
    .line 435
    iget v0, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@2
    iget v1, p1, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@4
    if-ne v0, v1, :cond_1c

    #@6
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@9
    move-result v0

    #@a
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@d
    move-result v1

    #@e
    if-ne v0, v1, :cond_1c

    #@10
    iget-object v0, p1, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@12
    iget-object v1, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@14
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_1c

    #@1a
    const/4 v0, 0x1

    #@1b
    :goto_1b
    return v0

    #@1c
    :cond_1c
    const/4 v0, 0x0

    #@1d
    goto :goto_1b
.end method

.method public mayBeCompatibleWith(Landroid/filterfw/core/FrameFormat;)Z
    .registers 10
    .parameter "specification"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 251
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_19

    #@8
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@b
    move-result v6

    #@c
    if-eqz v6, :cond_19

    #@e
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@11
    move-result v6

    #@12
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@15
    move-result v7

    #@16
    if-eq v6, v7, :cond_19

    #@18
    .line 306
    :cond_18
    :goto_18
    return v4

    #@19
    .line 258
    :cond_19
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_2f

    #@1f
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_2f

    #@25
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@28
    move-result v6

    #@29
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@2c
    move-result v7

    #@2d
    if-ne v6, v7, :cond_18

    #@2f
    .line 265
    :cond_2f
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@32
    move-result v6

    #@33
    if-eq v6, v5, :cond_45

    #@35
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@38
    move-result v6

    #@39
    if-eq v6, v5, :cond_45

    #@3b
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@3e
    move-result v6

    #@3f
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@42
    move-result v7

    #@43
    if-ne v6, v7, :cond_18

    #@45
    .line 272
    :cond_45
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@48
    move-result v6

    #@49
    if-lez v6, :cond_5b

    #@4b
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@4e
    move-result v6

    #@4f
    if-lez v6, :cond_5b

    #@51
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@54
    move-result v6

    #@55
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@58
    move-result v7

    #@59
    if-ne v6, v7, :cond_18

    #@5b
    .line 279
    :cond_5b
    const/4 v0, 0x0

    #@5c
    .local v0, i:I
    :goto_5c
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getDimensionCount()I

    #@5f
    move-result v6

    #@60
    if-ge v0, v6, :cond_77

    #@62
    .line 280
    invoke-virtual {p1, v0}, Landroid/filterfw/core/FrameFormat;->getDimension(I)I

    #@65
    move-result v2

    #@66
    .line 281
    .local v2, specDim:I
    if-eqz v2, :cond_74

    #@68
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FrameFormat;->getDimension(I)I

    #@6b
    move-result v6

    #@6c
    if-eqz v6, :cond_74

    #@6e
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FrameFormat;->getDimension(I)I

    #@71
    move-result v6

    #@72
    if-ne v6, v2, :cond_18

    #@74
    .line 279
    :cond_74
    add-int/lit8 v0, v0, 0x1

    #@76
    goto :goto_5c

    #@77
    .line 289
    .end local v2           #specDim:I
    :cond_77
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@7a
    move-result-object v6

    #@7b
    if-eqz v6, :cond_91

    #@7d
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@80
    move-result-object v6

    #@81
    if-eqz v6, :cond_91

    #@83
    .line 290
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@86
    move-result-object v6

    #@87
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@8a
    move-result-object v7

    #@8b
    invoke-virtual {v6, v7}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@8e
    move-result v6

    #@8f
    if-eqz v6, :cond_18

    #@91
    .line 296
    :cond_91
    iget-object v6, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@93
    if-eqz v6, :cond_cb

    #@95
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@97
    if-eqz v6, :cond_cb

    #@99
    .line 297
    iget-object v6, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@9b
    invoke-virtual {v6}, Landroid/filterfw/core/KeyValueMap;->keySet()Ljava/util/Set;

    #@9e
    move-result-object v6

    #@9f
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a2
    move-result-object v1

    #@a3
    .local v1, i$:Ljava/util/Iterator;
    :cond_a3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@a6
    move-result v6

    #@a7
    if-eqz v6, :cond_cb

    #@a9
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@ac
    move-result-object v3

    #@ad
    check-cast v3, Ljava/lang/String;

    #@af
    .line 298
    .local v3, specKey:Ljava/lang/String;
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@b1
    invoke-virtual {v6, v3}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@b4
    move-result v6

    #@b5
    if-eqz v6, :cond_a3

    #@b7
    iget-object v6, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@b9
    invoke-virtual {v6, v3}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@bc
    move-result-object v6

    #@bd
    iget-object v7, p1, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@bf
    invoke-virtual {v7, v3}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c2
    move-result-object v7

    #@c3
    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@c6
    move-result v6

    #@c7
    if-nez v6, :cond_a3

    #@c9
    goto/16 :goto_18

    #@cb
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #specKey:Ljava/lang/String;
    :cond_cb
    move v4, v5

    #@cc
    .line 306
    goto/16 :goto_18
.end method

.method public mutableCopy()Landroid/filterfw/core/MutableFrameFormat;
    .registers 3

    #@0
    .prologue
    .line 160
    new-instance v0, Landroid/filterfw/core/MutableFrameFormat;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/MutableFrameFormat;-><init>()V

    #@5
    .line 161
    .local v0, result:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@8
    move-result v1

    #@9
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setBaseType(I)V

    #@c
    .line 162
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@f
    move-result v1

    #@10
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setTarget(I)V

    #@13
    .line 163
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@16
    move-result v1

    #@17
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setBytesPerSample(I)V

    #@1a
    .line 164
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getDimensions()[I

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setDimensions([I)V

    #@21
    .line 165
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Landroid/filterfw/core/MutableFrameFormat;->setObjectClass(Ljava/lang/Class;)V

    #@28
    .line 166
    iget-object v1, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2a
    if-nez v1, :cond_30

    #@2c
    const/4 v1, 0x0

    #@2d
    :goto_2d
    iput-object v1, v0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2f
    .line 167
    return-object v0

    #@30
    .line 166
    :cond_30
    iget-object v1, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@32
    invoke-virtual {v1}, Landroid/filterfw/core/KeyValueMap;->clone()Ljava/lang/Object;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Landroid/filterfw/core/KeyValueMap;

    #@38
    goto :goto_2d
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    .line 403
    invoke-virtual {p0}, Landroid/filterfw/core/FrameFormat;->getValuesPerSample()I

    #@3
    move-result v3

    #@4
    .line 404
    .local v3, valuesPerSample:I
    const/4 v4, 0x1

    #@5
    if-ne v3, v4, :cond_49

    #@7
    const-string v1, ""

    #@9
    .line 405
    .local v1, sampleCountString:Ljava/lang/String;
    :goto_9
    iget v4, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@b
    if-nez v4, :cond_4e

    #@d
    const-string v2, ""

    #@f
    .line 406
    .local v2, targetString:Ljava/lang/String;
    :goto_f
    iget-object v4, p0, Landroid/filterfw/core/FrameFormat;->mObjectClass:Ljava/lang/Class;

    #@11
    if-nez v4, :cond_68

    #@13
    const-string v0, ""

    #@15
    .line 410
    .local v0, classString:Ljava/lang/String;
    :goto_15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    iget v5, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@20
    invoke-static {v5}, Landroid/filterfw/core/FrameFormat;->baseTypeToString(I)Ljava/lang/String;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    iget-object v5, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@2e
    invoke-static {v5}, Landroid/filterfw/core/FrameFormat;->dimensionsToString([I)Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    iget-object v5, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@3c
    invoke-static {v5}, Landroid/filterfw/core/FrameFormat;->metaDataToString(Landroid/filterfw/core/KeyValueMap;)Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    return-object v4

    #@49
    .line 404
    .end local v0           #classString:Ljava/lang/String;
    .end local v1           #sampleCountString:Ljava/lang/String;
    .end local v2           #targetString:Ljava/lang/String;
    :cond_49
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@4c
    move-result-object v1

    #@4d
    goto :goto_9

    #@4e
    .line 405
    .restart local v1       #sampleCountString:Ljava/lang/String;
    :cond_4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    iget v5, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@55
    invoke-static {v5}, Landroid/filterfw/core/FrameFormat;->targetToString(I)Ljava/lang/String;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    const-string v5, " "

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v2

    #@67
    goto :goto_f

    #@68
    .line 406
    .restart local v2       #targetString:Ljava/lang/String;
    :cond_68
    new-instance v4, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v5, " class("

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    iget-object v5, p0, Landroid/filterfw/core/FrameFormat;->mObjectClass:Ljava/lang/Class;

    #@75
    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    const-string v5, ") "

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v0

    #@87
    goto :goto_15
.end method
