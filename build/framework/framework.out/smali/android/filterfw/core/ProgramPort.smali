.class public Landroid/filterfw/core/ProgramPort;
.super Landroid/filterfw/core/FieldPort;
.source "ProgramPort.java"


# instance fields
.field protected mVarName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V
    .registers 6
    .parameter "filter"
    .parameter "name"
    .parameter "varName"
    .parameter "field"
    .parameter "hasDefault"

    #@0
    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p4, p5}, Landroid/filterfw/core/FieldPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V

    #@3
    .line 35
    iput-object p3, p0, Landroid/filterfw/core/ProgramPort;->mVarName:Ljava/lang/String;

    #@5
    .line 36
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Program "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-super {p0}, Landroid/filterfw/core/FieldPort;->toString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method public declared-synchronized transfer(Landroid/filterfw/core/FilterContext;)V
    .registers 9
    .parameter "context"

    #@0
    .prologue
    .line 45
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v4, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_45

    #@3
    if-eqz v4, :cond_1d

    #@5
    .line 47
    :try_start_5
    iget-object v4, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@7
    iget-object v5, p0, Landroid/filterfw/core/FilterPort;->mFilter:Landroid/filterfw/core/Filter;

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v2

    #@d
    .line 48
    .local v2, fieldValue:Ljava/lang/Object;
    if-eqz v2, :cond_1d

    #@f
    .line 49
    move-object v0, v2

    #@10
    check-cast v0, Landroid/filterfw/core/Program;

    #@12
    move-object v3, v0

    #@13
    .line 50
    .local v3, program:Landroid/filterfw/core/Program;
    iget-object v4, p0, Landroid/filterfw/core/ProgramPort;->mVarName:Ljava/lang/String;

    #@15
    iget-object v5, p0, Landroid/filterfw/core/FieldPort;->mValue:Ljava/lang/Object;

    #@17
    invoke-virtual {v3, v4, v5}, Landroid/filterfw/core/Program;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@1a
    .line 51
    const/4 v4, 0x0

    #@1b
    iput-boolean v4, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_45
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_1d} :catch_1f
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_1d} :catch_48

    #@1d
    .line 61
    .end local v2           #fieldValue:Ljava/lang/Object;
    .end local v3           #program:Landroid/filterfw/core/Program;
    :cond_1d
    monitor-exit p0

    #@1e
    return-void

    #@1f
    .line 53
    :catch_1f
    move-exception v1

    #@20
    .line 54
    .local v1, e:Ljava/lang/IllegalAccessException;
    :try_start_20
    new-instance v4, Ljava/lang/RuntimeException;

    #@22
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "Access to program field \'"

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    iget-object v6, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@2f
    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    const-string v6, "\' was denied!"

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@44
    throw v4
    :try_end_45
    .catchall {:try_start_20 .. :try_end_45} :catchall_45

    #@45
    .line 45
    .end local v1           #e:Ljava/lang/IllegalAccessException;
    :catchall_45
    move-exception v4

    #@46
    monitor-exit p0

    #@47
    throw v4

    #@48
    .line 56
    :catch_48
    move-exception v1

    #@49
    .line 57
    .local v1, e:Ljava/lang/ClassCastException;
    :try_start_49
    new-instance v4, Ljava/lang/RuntimeException;

    #@4b
    new-instance v5, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v6, "Non Program field \'"

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    iget-object v6, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@58
    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    const-string v6, "\' annotated with ProgramParameter!"

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@6d
    throw v4
    :try_end_6e
    .catchall {:try_start_49 .. :try_end_6e} :catchall_45
.end method
