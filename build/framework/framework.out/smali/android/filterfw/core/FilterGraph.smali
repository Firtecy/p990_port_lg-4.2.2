.class public Landroid/filterfw/core/FilterGraph;
.super Ljava/lang/Object;
.source "FilterGraph.java"


# static fields
.field public static final AUTOBRANCH_OFF:I = 0x0

.field public static final AUTOBRANCH_SYNCED:I = 0x1

.field public static final AUTOBRANCH_UNSYNCED:I = 0x2

.field public static final TYPECHECK_DYNAMIC:I = 0x1

.field public static final TYPECHECK_OFF:I = 0x0

.field public static final TYPECHECK_STRICT:I = 0x2


# instance fields
.field private TAG:Ljava/lang/String;

.field private mAutoBranchMode:I

.field private mDiscardUnconnectedOutputs:Z

.field private mFilters:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/filterfw/core/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private mIsReady:Z

.field private mLogVerbose:Z

.field private mNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/filterfw/core/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private mPreconnections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/filterfw/core/OutputPort;",
            "Ljava/util/LinkedList",
            "<",
            "Landroid/filterfw/core/InputPort;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTypeCheckMode:I


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x0

    #@2
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 40
    new-instance v0, Ljava/util/HashSet;

    #@7
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@a
    iput-object v0, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@c
    .line 41
    new-instance v0, Ljava/util/HashMap;

    #@e
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@11
    iput-object v0, p0, Landroid/filterfw/core/FilterGraph;->mNameMap:Ljava/util/HashMap;

    #@13
    .line 42
    new-instance v0, Ljava/util/HashMap;

    #@15
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v0, p0, Landroid/filterfw/core/FilterGraph;->mPreconnections:Ljava/util/HashMap;

    #@1a
    .line 53
    iput-boolean v1, p0, Landroid/filterfw/core/FilterGraph;->mIsReady:Z

    #@1c
    .line 54
    iput v1, p0, Landroid/filterfw/core/FilterGraph;->mAutoBranchMode:I

    #@1e
    .line 55
    iput v2, p0, Landroid/filterfw/core/FilterGraph;->mTypeCheckMode:I

    #@20
    .line 56
    iput-boolean v1, p0, Landroid/filterfw/core/FilterGraph;->mDiscardUnconnectedOutputs:Z

    #@22
    .line 59
    const-string v0, "FilterGraph"

    #@24
    iput-object v0, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@26
    .line 62
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@28
    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@2b
    move-result v0

    #@2c
    iput-boolean v0, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@2e
    .line 63
    return-void
.end method

.method private checkConnections()V
    .registers 1

    #@0
    .prologue
    .line 273
    return-void
.end method

.method private connectPorts()V
    .registers 15

    #@0
    .prologue
    .line 313
    const/4 v1, 0x1

    #@1
    .line 314
    .local v1, branchId:I
    iget-object v11, p0, Landroid/filterfw/core/FilterGraph;->mPreconnections:Ljava/util/HashMap;

    #@3
    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@6
    move-result-object v11

    #@7
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v6

    #@b
    :goto_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v11

    #@f
    if-eqz v11, :cond_f2

    #@11
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v5

    #@15
    check-cast v5, Ljava/util/Map$Entry;

    #@17
    .line 315
    .local v5, connection:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/filterfw/core/OutputPort;Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1a
    move-result-object v10

    #@1b
    check-cast v10, Landroid/filterfw/core/OutputPort;

    #@1d
    .line 316
    .local v10, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@20
    move-result-object v9

    #@21
    check-cast v9, Ljava/util/LinkedList;

    #@23
    .line 317
    .local v9, inputPorts:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    #@26
    move-result v11

    #@27
    const/4 v12, 0x1

    #@28
    if-ne v11, v12, :cond_35

    #@2a
    .line 318
    const/4 v11, 0x0

    #@2b
    invoke-virtual {v9, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@2e
    move-result-object v11

    #@2f
    check-cast v11, Landroid/filterfw/core/InputPort;

    #@31
    invoke-virtual {v10, v11}, Landroid/filterfw/core/OutputPort;->connectTo(Landroid/filterfw/core/InputPort;)V

    #@34
    goto :goto_b

    #@35
    .line 319
    :cond_35
    iget v11, p0, Landroid/filterfw/core/FilterGraph;->mAutoBranchMode:I

    #@37
    if-nez v11, :cond_5e

    #@39
    .line 320
    new-instance v11, Ljava/lang/RuntimeException;

    #@3b
    new-instance v12, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v13, "Attempting to connect "

    #@42
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v12

    #@46
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v12

    #@4a
    const-string v13, " to multiple "

    #@4c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v12

    #@50
    const-string v13, "filter ports! Enable auto-branching to allow this."

    #@52
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v12

    #@56
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v12

    #@5a
    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5d
    throw v11

    #@5e
    .line 323
    :cond_5e
    iget-boolean v11, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@60
    if-eqz v11, :cond_80

    #@62
    iget-object v11, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@64
    new-instance v12, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v13, "Creating branch for "

    #@6b
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v12

    #@6f
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v12

    #@73
    const-string v13, "!"

    #@75
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v12

    #@79
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v12

    #@7d
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 324
    :cond_80
    const/4 v0, 0x0

    #@81
    .line 325
    .local v0, branch:Landroid/filterpacks/base/FrameBranch;
    iget v11, p0, Landroid/filterfw/core/FilterGraph;->mAutoBranchMode:I

    #@83
    const/4 v12, 0x1

    #@84
    if-ne v11, v12, :cond_ea

    #@86
    .line 326
    new-instance v0, Landroid/filterpacks/base/FrameBranch;

    #@88
    .end local v0           #branch:Landroid/filterpacks/base/FrameBranch;
    new-instance v11, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v12, "branch"

    #@8f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v11

    #@93
    add-int/lit8 v2, v1, 0x1

    #@95
    .end local v1           #branchId:I
    .local v2, branchId:I
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v11

    #@99
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v11

    #@9d
    invoke-direct {v0, v11}, Landroid/filterpacks/base/FrameBranch;-><init>(Ljava/lang/String;)V

    #@a0
    .line 330
    .restart local v0       #branch:Landroid/filterpacks/base/FrameBranch;
    new-instance v4, Landroid/filterfw/core/KeyValueMap;

    #@a2
    invoke-direct {v4}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@a5
    .line 331
    .local v4, branchParams:Landroid/filterfw/core/KeyValueMap;
    const/4 v11, 0x2

    #@a6
    new-array v11, v11, [Ljava/lang/Object;

    #@a8
    const/4 v12, 0x0

    #@a9
    const-string/jumbo v13, "outputs"

    #@ac
    aput-object v13, v11, v12

    #@ae
    const/4 v12, 0x1

    #@af
    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    #@b2
    move-result v13

    #@b3
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b6
    move-result-object v13

    #@b7
    aput-object v13, v11, v12

    #@b9
    invoke-virtual {v0, v11}, Landroid/filterpacks/base/FrameBranch;->initWithAssignmentList([Ljava/lang/Object;)V

    #@bc
    .line 332
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FilterGraph;->addFilter(Landroid/filterfw/core/Filter;)Z

    #@bf
    .line 333
    const-string v11, "in"

    #@c1
    invoke-virtual {v0, v11}, Landroid/filterpacks/base/FrameBranch;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@c4
    move-result-object v11

    #@c5
    invoke-virtual {v10, v11}, Landroid/filterfw/core/OutputPort;->connectTo(Landroid/filterfw/core/InputPort;)V

    #@c8
    .line 334
    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@cb
    move-result-object v8

    #@cc
    .line 335
    .local v8, inputPortIter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/filterfw/core/InputPort;>;"
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getOutputPorts()Ljava/util/Collection;

    #@cf
    move-result-object v11

    #@d0
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@d3
    move-result-object v7

    #@d4
    .local v7, i$:Ljava/util/Iterator;
    :goto_d4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@d7
    move-result v11

    #@d8
    if-eqz v11, :cond_f8

    #@da
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@dd
    move-result-object v3

    #@de
    check-cast v3, Landroid/filterfw/core/OutputPort;

    #@e0
    .line 336
    .local v3, branchOutPort:Landroid/filterfw/core/OutputPort;
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e3
    move-result-object v11

    #@e4
    check-cast v11, Landroid/filterfw/core/InputPort;

    #@e6
    invoke-virtual {v3, v11}, Landroid/filterfw/core/OutputPort;->connectTo(Landroid/filterfw/core/InputPort;)V

    #@e9
    goto :goto_d4

    #@ea
    .line 328
    .end local v2           #branchId:I
    .end local v3           #branchOutPort:Landroid/filterfw/core/OutputPort;
    .end local v4           #branchParams:Landroid/filterfw/core/KeyValueMap;
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #inputPortIter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/filterfw/core/InputPort;>;"
    .restart local v1       #branchId:I
    :cond_ea
    new-instance v11, Ljava/lang/RuntimeException;

    #@ec
    const-string v12, "TODO: Unsynced branches not implemented yet!"

    #@ee
    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f1
    throw v11

    #@f2
    .line 340
    .end local v0           #branch:Landroid/filterpacks/base/FrameBranch;
    .end local v5           #connection:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/filterfw/core/OutputPort;Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;>;"
    .end local v9           #inputPorts:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    .end local v10           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_f2
    iget-object v11, p0, Landroid/filterfw/core/FilterGraph;->mPreconnections:Ljava/util/HashMap;

    #@f4
    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    #@f7
    .line 341
    return-void

    #@f8
    .end local v1           #branchId:I
    .restart local v0       #branch:Landroid/filterpacks/base/FrameBranch;
    .restart local v2       #branchId:I
    .restart local v4       #branchParams:Landroid/filterfw/core/KeyValueMap;
    .restart local v5       #connection:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/filterfw/core/OutputPort;Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;>;"
    .restart local v7       #i$:Ljava/util/Iterator;
    .restart local v8       #inputPortIter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/filterfw/core/InputPort;>;"
    .restart local v9       #inputPorts:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    .restart local v10       #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_f8
    move v1, v2

    #@f9
    .end local v2           #branchId:I
    .restart local v1       #branchId:I
    goto/16 :goto_b
.end method

.method private discardUnconnectedOutputs()V
    .registers 11

    #@0
    .prologue
    .line 277
    new-instance v0, Ljava/util/LinkedList;

    #@2
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@5
    .line 278
    .local v0, addedFilters:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/Filter;>;"
    iget-object v7, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@7
    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v7

    #@f
    if-eqz v7, :cond_86

    #@11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/filterfw/core/Filter;

    #@17
    .line 279
    .local v1, filter:Landroid/filterfw/core/Filter;
    const/4 v4, 0x0

    #@18
    .line 280
    .local v4, id:I
    invoke-virtual {v1}, Landroid/filterfw/core/Filter;->getOutputPorts()Ljava/util/Collection;

    #@1b
    move-result-object v7

    #@1c
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1f
    move-result-object v3

    #@20
    .local v3, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@23
    move-result v7

    #@24
    if-eqz v7, :cond_b

    #@26
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@29
    move-result-object v6

    #@2a
    check-cast v6, Landroid/filterfw/core/OutputPort;

    #@2c
    .line 281
    .local v6, port:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v6}, Landroid/filterfw/core/OutputPort;->isConnected()Z

    #@2f
    move-result v7

    #@30
    if-nez v7, :cond_20

    #@32
    .line 282
    iget-boolean v7, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@34
    if-eqz v7, :cond_54

    #@36
    iget-object v7, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@38
    new-instance v8, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v9, "Autoconnecting unconnected "

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    const-string v9, " to Null filter."

    #@49
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 283
    :cond_54
    new-instance v5, Landroid/filterpacks/base/NullFilter;

    #@56
    new-instance v7, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-virtual {v1}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, "ToNull"

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v7

    #@71
    invoke-direct {v5, v7}, Landroid/filterpacks/base/NullFilter;-><init>(Ljava/lang/String;)V

    #@74
    .line 284
    .local v5, nullFilter:Landroid/filterpacks/base/NullFilter;
    invoke-virtual {v5}, Landroid/filterpacks/base/NullFilter;->init()V

    #@77
    .line 285
    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@7a
    .line 286
    const-string v7, "frame"

    #@7c
    invoke-virtual {v5, v7}, Landroid/filterpacks/base/NullFilter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v6, v7}, Landroid/filterfw/core/OutputPort;->connectTo(Landroid/filterfw/core/InputPort;)V

    #@83
    .line 287
    add-int/lit8 v4, v4, 0x1

    #@85
    goto :goto_20

    #@86
    .line 292
    .end local v1           #filter:Landroid/filterfw/core/Filter;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #id:I
    .end local v5           #nullFilter:Landroid/filterpacks/base/NullFilter;
    .end local v6           #port:Landroid/filterfw/core/OutputPort;
    :cond_86
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@89
    move-result-object v2

    #@8a
    .local v2, i$:Ljava/util/Iterator;
    :goto_8a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@8d
    move-result v7

    #@8e
    if-eqz v7, :cond_9a

    #@90
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@93
    move-result-object v1

    #@94
    check-cast v1, Landroid/filterfw/core/Filter;

    #@96
    .line 293
    .restart local v1       #filter:Landroid/filterfw/core/Filter;
    invoke-virtual {p0, v1}, Landroid/filterfw/core/FilterGraph;->addFilter(Landroid/filterfw/core/Filter;)Z

    #@99
    goto :goto_8a

    #@9a
    .line 295
    .end local v1           #filter:Landroid/filterfw/core/Filter;
    :cond_9a
    return-void
.end method

.method private getSourceFilters()Ljava/util/HashSet;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Landroid/filterfw/core/Filter;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 344
    new-instance v2, Ljava/util/HashSet;

    #@2
    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 345
    .local v2, sourceFilters:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/filterfw/core/Filter;>;"
    invoke-virtual {p0}, Landroid/filterfw/core/FilterGraph;->getFilters()Ljava/util/Set;

    #@8
    move-result-object v3

    #@9
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :cond_d
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_3f

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/filterfw/core/Filter;

    #@19
    .line 346
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getNumberOfConnectedInputs()I

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_d

    #@1f
    .line 347
    iget-boolean v3, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@21
    if-eqz v3, :cond_3b

    #@23
    iget-object v3, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "Found source filter: "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 348
    :cond_3b
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@3e
    goto :goto_d

    #@3f
    .line 351
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_3f
    return-object v2
.end method

.method private preconnect(Landroid/filterfw/core/OutputPort;Landroid/filterfw/core/InputPort;)V
    .registers 5
    .parameter "outPort"
    .parameter "inPort"

    #@0
    .prologue
    .line 304
    iget-object v1, p0, Landroid/filterfw/core/FilterGraph;->mPreconnections:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/LinkedList;

    #@8
    .line 305
    .local v0, targets:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    if-nez v0, :cond_14

    #@a
    .line 306
    new-instance v0, Ljava/util/LinkedList;

    #@c
    .end local v0           #targets:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@f
    .line 307
    .restart local v0       #targets:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Landroid/filterfw/core/InputPort;>;"
    iget-object v1, p0, Landroid/filterfw/core/FilterGraph;->mPreconnections:Ljava/util/HashMap;

    #@11
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 309
    :cond_14
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@17
    .line 310
    return-void
.end method

.method private readyForProcessing(Landroid/filterfw/core/Filter;Ljava/util/Set;)Z
    .registers 8
    .parameter "filter"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/filterfw/core/Filter;",
            "Ljava/util/Set",
            "<",
            "Landroid/filterfw/core/Filter;",
            ">;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p2, processed:Ljava/util/Set;,"Ljava/util/Set<Landroid/filterfw/core/Filter;>;"
    const/4 v3, 0x0

    #@1
    .line 177
    invoke-interface {p2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@4
    move-result v4

    #@5
    if-eqz v4, :cond_8

    #@7
    .line 188
    :goto_7
    return v3

    #@8
    .line 182
    :cond_8
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getInputPorts()Ljava/util/Collection;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_29

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/filterfw/core/InputPort;

    #@1c
    .line 183
    .local v2, port:Landroid/filterfw/core/InputPort;
    invoke-virtual {v2}, Landroid/filterfw/core/InputPort;->getSourceFilter()Landroid/filterfw/core/Filter;

    #@1f
    move-result-object v0

    #@20
    .line 184
    .local v0, dependency:Landroid/filterfw/core/Filter;
    if-eqz v0, :cond_10

    #@22
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_10

    #@28
    goto :goto_7

    #@29
    .line 188
    .end local v0           #dependency:Landroid/filterfw/core/Filter;
    .end local v2           #port:Landroid/filterfw/core/InputPort;
    :cond_29
    const/4 v3, 0x1

    #@2a
    goto :goto_7
.end method

.method private removeFilter(Landroid/filterfw/core/Filter;)V
    .registers 4
    .parameter "filter"

    #@0
    .prologue
    .line 298
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    #@5
    .line 299
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mNameMap:Ljava/util/HashMap;

    #@7
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    .line 300
    return-void
.end method

.method private runTypeCheck()V
    .registers 10

    #@0
    .prologue
    .line 192
    new-instance v1, Ljava/util/Stack;

    #@2
    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    #@5
    .line 193
    .local v1, filterStack:Ljava/util/Stack;,"Ljava/util/Stack<Landroid/filterfw/core/Filter;>;"
    new-instance v4, Ljava/util/HashSet;

    #@7
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@a
    .line 194
    .local v4, processedFilters:Ljava/util/Set;,"Ljava/util/Set<Landroid/filterfw/core/Filter;>;"
    invoke-direct {p0}, Landroid/filterfw/core/FilterGraph;->getSourceFilters()Ljava/util/HashSet;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {v1, v6}, Ljava/util/Stack;->addAll(Ljava/util/Collection;)Z

    #@11
    .line 196
    :cond_11
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    #@14
    move-result v6

    #@15
    if-nez v6, :cond_6c

    #@17
    .line 198
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/filterfw/core/Filter;

    #@1d
    .line 199
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@20
    .line 202
    invoke-direct {p0, v0}, Landroid/filterfw/core/FilterGraph;->updateOutputs(Landroid/filterfw/core/Filter;)V

    #@23
    .line 205
    iget-boolean v6, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@25
    if-eqz v6, :cond_45

    #@27
    iget-object v6, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@29
    new-instance v7, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v8, "Running type check on "

    #@30
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v7

    #@34
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    const-string v8, "..."

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 206
    :cond_45
    invoke-direct {p0, v0}, Landroid/filterfw/core/FilterGraph;->runTypeCheckOn(Landroid/filterfw/core/Filter;)V

    #@48
    .line 209
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getOutputPorts()Ljava/util/Collection;

    #@4b
    move-result-object v6

    #@4c
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4f
    move-result-object v2

    #@50
    .local v2, i$:Ljava/util/Iterator;
    :cond_50
    :goto_50
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@53
    move-result v6

    #@54
    if-eqz v6, :cond_11

    #@56
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@59
    move-result-object v3

    #@5a
    check-cast v3, Landroid/filterfw/core/OutputPort;

    #@5c
    .line 210
    .local v3, port:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v3}, Landroid/filterfw/core/OutputPort;->getTargetFilter()Landroid/filterfw/core/Filter;

    #@5f
    move-result-object v5

    #@60
    .line 211
    .local v5, target:Landroid/filterfw/core/Filter;
    if-eqz v5, :cond_50

    #@62
    invoke-direct {p0, v5, v4}, Landroid/filterfw/core/FilterGraph;->readyForProcessing(Landroid/filterfw/core/Filter;Ljava/util/Set;)Z

    #@65
    move-result v6

    #@66
    if-eqz v6, :cond_50

    #@68
    .line 212
    invoke-virtual {v1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    #@6b
    goto :goto_50

    #@6c
    .line 218
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #port:Landroid/filterfw/core/OutputPort;
    .end local v5           #target:Landroid/filterfw/core/Filter;
    :cond_6c
    invoke-interface {v4}, Ljava/util/Set;->size()I

    #@6f
    move-result v6

    #@70
    invoke-virtual {p0}, Landroid/filterfw/core/FilterGraph;->getFilters()Ljava/util/Set;

    #@73
    move-result-object v7

    #@74
    invoke-interface {v7}, Ljava/util/Set;->size()I

    #@77
    move-result v7

    #@78
    if-eq v6, v7, :cond_82

    #@7a
    .line 219
    new-instance v6, Ljava/lang/RuntimeException;

    #@7c
    const-string v7, "Could not schedule all filters! Is your graph malformed?"

    #@7e
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@81
    throw v6

    #@82
    .line 221
    :cond_82
    return-void
.end method

.method private runTypeCheckOn(Landroid/filterfw/core/Filter;)V
    .registers 11
    .parameter "filter"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 240
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getInputPorts()Ljava/util/Collection;

    #@4
    move-result-object v5

    #@5
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v5

    #@d
    if-eqz v5, :cond_bf

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    check-cast v2, Landroid/filterfw/core/InputPort;

    #@15
    .line 241
    .local v2, inputPort:Landroid/filterfw/core/InputPort;
    iget-boolean v5, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@17
    if-eqz v5, :cond_31

    #@19
    iget-object v5, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@1b
    new-instance v6, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v7, "Type checking port "

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 242
    :cond_31
    invoke-virtual {v2}, Landroid/filterfw/core/InputPort;->getSourceFormat()Landroid/filterfw/core/FrameFormat;

    #@34
    move-result-object v3

    #@35
    .line 243
    .local v3, sourceFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v2}, Landroid/filterfw/core/InputPort;->getPortFormat()Landroid/filterfw/core/FrameFormat;

    #@38
    move-result-object v4

    #@39
    .line 244
    .local v4, targetFormat:Landroid/filterfw/core/FrameFormat;
    if-eqz v3, :cond_9

    #@3b
    if-eqz v4, :cond_9

    #@3d
    .line 245
    iget-boolean v5, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@3f
    if-eqz v5, :cond_69

    #@41
    iget-object v5, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@43
    new-instance v6, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v7, "Checking "

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    const-string v7, " against "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    const-string v7, "."

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 247
    :cond_69
    const/4 v0, 0x1

    #@6a
    .line 248
    .local v0, compatible:Z
    iget v5, p0, Landroid/filterfw/core/FilterGraph;->mTypeCheckMode:I

    #@6c
    packed-switch v5, :pswitch_data_c0

    #@6f
    .line 262
    :goto_6f
    if-nez v0, :cond_9

    #@71
    .line 263
    new-instance v5, Ljava/lang/RuntimeException;

    #@73
    new-instance v6, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v7, "Type mismatch: Filter "

    #@7a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    const-string v7, " expects a "

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    const-string v7, "format of type "

    #@8a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    const-string v7, " but got a format of type "

    #@94
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v6

    #@98
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v6

    #@9c
    const-string v7, "!"

    #@9e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v6

    #@a2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a9
    throw v5

    #@aa
    .line 250
    :pswitch_aa
    invoke-virtual {v2, v8}, Landroid/filterfw/core/InputPort;->setChecksType(Z)V

    #@ad
    goto :goto_6f

    #@ae
    .line 253
    :pswitch_ae
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FrameFormat;->mayBeCompatibleWith(Landroid/filterfw/core/FrameFormat;)Z

    #@b1
    move-result v0

    #@b2
    .line 254
    const/4 v5, 0x1

    #@b3
    invoke-virtual {v2, v5}, Landroid/filterfw/core/InputPort;->setChecksType(Z)V

    #@b6
    goto :goto_6f

    #@b7
    .line 257
    :pswitch_b7
    invoke-virtual {v3, v4}, Landroid/filterfw/core/FrameFormat;->isCompatibleWith(Landroid/filterfw/core/FrameFormat;)Z

    #@ba
    move-result v0

    #@bb
    .line 258
    invoke-virtual {v2, v8}, Landroid/filterfw/core/InputPort;->setChecksType(Z)V

    #@be
    goto :goto_6f

    #@bf
    .line 269
    .end local v0           #compatible:Z
    .end local v2           #inputPort:Landroid/filterfw/core/InputPort;
    .end local v3           #sourceFormat:Landroid/filterfw/core/FrameFormat;
    .end local v4           #targetFormat:Landroid/filterfw/core/FrameFormat;
    :cond_bf
    return-void

    #@c0
    .line 248
    :pswitch_data_c0
    .packed-switch 0x0
        :pswitch_aa
        :pswitch_ae
        :pswitch_b7
    .end packed-switch
.end method

.method private updateOutputs(Landroid/filterfw/core/Filter;)V
    .registers 10
    .parameter "filter"

    #@0
    .prologue
    .line 224
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getOutputPorts()Ljava/util/Collection;

    #@3
    move-result-object v5

    #@4
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v0

    #@8
    .local v0, i$:Ljava/util/Iterator;
    :cond_8
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_4b

    #@e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v4

    #@12
    check-cast v4, Landroid/filterfw/core/OutputPort;

    #@14
    .line 225
    .local v4, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v4}, Landroid/filterfw/core/OutputPort;->getBasePort()Landroid/filterfw/core/InputPort;

    #@17
    move-result-object v2

    #@18
    .line 226
    .local v2, inputPort:Landroid/filterfw/core/InputPort;
    if-eqz v2, :cond_8

    #@1a
    .line 227
    invoke-virtual {v2}, Landroid/filterfw/core/InputPort;->getSourceFormat()Landroid/filterfw/core/FrameFormat;

    #@1d
    move-result-object v1

    #@1e
    .line 228
    .local v1, inputFormat:Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v4}, Landroid/filterfw/core/OutputPort;->getName()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-virtual {p1, v5, v1}, Landroid/filterfw/core/Filter;->getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;

    #@25
    move-result-object v3

    #@26
    .line 230
    .local v3, outputFormat:Landroid/filterfw/core/FrameFormat;
    if-nez v3, :cond_47

    #@28
    .line 231
    new-instance v5, Ljava/lang/RuntimeException;

    #@2a
    new-instance v6, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v7, "Filter did not return an output format for "

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, "!"

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v6

    #@43
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@46
    throw v5

    #@47
    .line 234
    :cond_47
    invoke-virtual {v4, v3}, Landroid/filterfw/core/OutputPort;->setPortFormat(Landroid/filterfw/core/FrameFormat;)V

    #@4a
    goto :goto_8

    #@4b
    .line 237
    .end local v1           #inputFormat:Landroid/filterfw/core/FrameFormat;
    .end local v2           #inputPort:Landroid/filterfw/core/InputPort;
    .end local v3           #outputFormat:Landroid/filterfw/core/FrameFormat;
    .end local v4           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_4b
    return-void
.end method


# virtual methods
.method public addFilter(Landroid/filterfw/core/Filter;)Z
    .registers 4
    .parameter "filter"

    #@0
    .prologue
    .line 66
    invoke-virtual {p0, p1}, Landroid/filterfw/core/FilterGraph;->containsFilter(Landroid/filterfw/core/Filter;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_16

    #@6
    .line 67
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@8
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@b
    .line 68
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mNameMap:Ljava/util/HashMap;

    #@d
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 69
    const/4 v0, 0x1

    #@15
    .line 71
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method public beginProcessing()V
    .registers 5

    #@0
    .prologue
    .line 126
    iget-boolean v2, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@2
    if-eqz v2, :cond_b

    #@4
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@6
    const-string v3, "Opening all filter connections..."

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 127
    :cond_b
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_21

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/filterfw/core/Filter;

    #@1d
    .line 128
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->openOutputs()V

    #@20
    goto :goto_11

    #@21
    .line 130
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_21
    const/4 v2, 0x1

    #@22
    iput-boolean v2, p0, Landroid/filterfw/core/FilterGraph;->mIsReady:Z

    #@24
    .line 131
    return-void
.end method

.method public closeFilters(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 140
    iget-boolean v2, p0, Landroid/filterfw/core/FilterGraph;->mLogVerbose:Z

    #@2
    if-eqz v2, :cond_b

    #@4
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->TAG:Ljava/lang/String;

    #@6
    const-string v3, "Closing all filters..."

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 141
    :cond_b
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_21

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/filterfw/core/Filter;

    #@1d
    .line 142
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0, p1}, Landroid/filterfw/core/Filter;->performClose(Landroid/filterfw/core/FilterContext;)V

    #@20
    goto :goto_11

    #@21
    .line 144
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_21
    const/4 v2, 0x0

    #@22
    iput-boolean v2, p0, Landroid/filterfw/core/FilterGraph;->mIsReady:Z

    #@24
    .line 145
    return-void
.end method

.method public connect(Landroid/filterfw/core/Filter;Ljava/lang/String;Landroid/filterfw/core/Filter;Ljava/lang/String;)V
    .registers 10
    .parameter "source"
    .parameter "outputName"
    .parameter "target"
    .parameter "inputName"

    #@0
    .prologue
    .line 86
    if-eqz p1, :cond_4

    #@2
    if-nez p3, :cond_c

    #@4
    .line 87
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@6
    const-string v3, "Passing null Filter in connect()!"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 88
    :cond_c
    invoke-virtual {p0, p1}, Landroid/filterfw/core/FilterGraph;->containsFilter(Landroid/filterfw/core/Filter;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_18

    #@12
    invoke-virtual {p0, p3}, Landroid/filterfw/core/FilterGraph;->containsFilter(Landroid/filterfw/core/Filter;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_20

    #@18
    .line 89
    :cond_18
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "Attempting to connect filter not in graph!"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 92
    :cond_20
    invoke-virtual {p1, p2}, Landroid/filterfw/core/Filter;->getOutputPort(Ljava/lang/String;)Landroid/filterfw/core/OutputPort;

    #@23
    move-result-object v1

    #@24
    .line 93
    .local v1, outPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {p3, p4}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@27
    move-result-object v0

    #@28
    .line 94
    .local v0, inPort:Landroid/filterfw/core/InputPort;
    if-nez v1, :cond_53

    #@2a
    .line 95
    new-instance v2, Ljava/lang/RuntimeException;

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Unknown output port \'"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, "\' on Filter "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, "!"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@52
    throw v2

    #@53
    .line 97
    :cond_53
    if-nez v0, :cond_7e

    #@55
    .line 98
    new-instance v2, Ljava/lang/RuntimeException;

    #@57
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v4, "Unknown input port \'"

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    const-string v4, "\' on Filter "

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, "!"

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7d
    throw v2

    #@7e
    .line 102
    :cond_7e
    invoke-direct {p0, v1, v0}, Landroid/filterfw/core/FilterGraph;->preconnect(Landroid/filterfw/core/OutputPort;Landroid/filterfw/core/InputPort;)V

    #@81
    .line 103
    return-void
.end method

.method public connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "sourceName"
    .parameter "outputName"
    .parameter "targetName"
    .parameter "inputName"

    #@0
    .prologue
    .line 109
    invoke-virtual {p0, p1}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@3
    move-result-object v0

    #@4
    .line 110
    .local v0, source:Landroid/filterfw/core/Filter;
    invoke-virtual {p0, p3}, Landroid/filterfw/core/FilterGraph;->getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@7
    move-result-object v1

    #@8
    .line 111
    .local v1, target:Landroid/filterfw/core/Filter;
    if-nez v0, :cond_29

    #@a
    .line 112
    new-instance v2, Ljava/lang/RuntimeException;

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Attempting to connect unknown source filter \'"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, "\'!"

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@28
    throw v2

    #@29
    .line 114
    :cond_29
    if-nez v1, :cond_4a

    #@2b
    .line 115
    new-instance v2, Ljava/lang/RuntimeException;

    #@2d
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "Attempting to connect unknown target filter \'"

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    const-string v4, "\'!"

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@49
    throw v2

    #@4a
    .line 118
    :cond_4a
    invoke-virtual {p0, v0, p2, v1, p4}, Landroid/filterfw/core/FilterGraph;->connect(Landroid/filterfw/core/Filter;Ljava/lang/String;Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@4d
    .line 119
    return-void
.end method

.method public containsFilter(Landroid/filterfw/core/Filter;)Z
    .registers 3
    .parameter "filter"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public flushFrames()V
    .registers 4

    #@0
    .prologue
    .line 134
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_16

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/filterfw/core/Filter;

    #@12
    .line 135
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->clearOutputs()V

    #@15
    goto :goto_6

    #@16
    .line 137
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_16
    return-void
.end method

.method public getFilter(Ljava/lang/String;)Landroid/filterfw/core/Filter;
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mNameMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/filterfw/core/Filter;

    #@8
    return-object v0
.end method

.method public getFilters()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/filterfw/core/Filter;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@2
    return-object v0
.end method

.method public isReady()Z
    .registers 2

    #@0
    .prologue
    .line 148
    iget-boolean v0, p0, Landroid/filterfw/core/FilterGraph;->mIsReady:Z

    #@2
    return v0
.end method

.method public setAutoBranchMode(I)V
    .registers 2
    .parameter "autoBranchMode"

    #@0
    .prologue
    .line 152
    iput p1, p0, Landroid/filterfw/core/FilterGraph;->mAutoBranchMode:I

    #@2
    .line 153
    return-void
.end method

.method public setDiscardUnconnectedOutputs(Z)V
    .registers 2
    .parameter "discard"

    #@0
    .prologue
    .line 156
    iput-boolean p1, p0, Landroid/filterfw/core/FilterGraph;->mDiscardUnconnectedOutputs:Z

    #@2
    .line 157
    return-void
.end method

.method public setTypeCheckMode(I)V
    .registers 2
    .parameter "typeCheckMode"

    #@0
    .prologue
    .line 160
    iput p1, p0, Landroid/filterfw/core/FilterGraph;->mTypeCheckMode:I

    #@2
    .line 161
    return-void
.end method

.method setupFilters()V
    .registers 2

    #@0
    .prologue
    .line 356
    iget-boolean v0, p0, Landroid/filterfw/core/FilterGraph;->mDiscardUnconnectedOutputs:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 357
    invoke-direct {p0}, Landroid/filterfw/core/FilterGraph;->discardUnconnectedOutputs()V

    #@7
    .line 359
    :cond_7
    invoke-direct {p0}, Landroid/filterfw/core/FilterGraph;->connectPorts()V

    #@a
    .line 360
    invoke-direct {p0}, Landroid/filterfw/core/FilterGraph;->checkConnections()V

    #@d
    .line 361
    invoke-direct {p0}, Landroid/filterfw/core/FilterGraph;->runTypeCheck()V

    #@10
    .line 362
    return-void
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 164
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_2e

    #@8
    .line 165
    invoke-virtual {p0}, Landroid/filterfw/core/FilterGraph;->flushFrames()V

    #@b
    .line 166
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@d
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v1

    #@11
    .local v1, i$:Ljava/util/Iterator;
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_21

    #@17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Landroid/filterfw/core/Filter;

    #@1d
    .line 167
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0, p1}, Landroid/filterfw/core/Filter;->performTearDown(Landroid/filterfw/core/FilterContext;)V

    #@20
    goto :goto_11

    #@21
    .line 169
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_21
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mFilters:Ljava/util/HashSet;

    #@23
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@26
    .line 170
    iget-object v2, p0, Landroid/filterfw/core/FilterGraph;->mNameMap:Ljava/util/HashMap;

    #@28
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@2b
    .line 171
    const/4 v2, 0x0

    #@2c
    iput-boolean v2, p0, Landroid/filterfw/core/FilterGraph;->mIsReady:Z

    #@2e
    .line 173
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_2e
    return-void
.end method
