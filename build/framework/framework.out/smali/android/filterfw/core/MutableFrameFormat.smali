.class public Landroid/filterfw/core/MutableFrameFormat;
.super Landroid/filterfw/core/FrameFormat;
.source "MutableFrameFormat.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct {p0}, Landroid/filterfw/core/FrameFormat;-><init>()V

    #@3
    .line 32
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "baseType"
    .parameter "target"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/FrameFormat;-><init>(II)V

    #@3
    .line 36
    return-void
.end method


# virtual methods
.method public setBaseType(I)V
    .registers 3
    .parameter "baseType"

    #@0
    .prologue
    .line 39
    iput p1, p0, Landroid/filterfw/core/FrameFormat;->mBaseType:I

    #@2
    .line 40
    invoke-static {p1}, Landroid/filterfw/core/MutableFrameFormat;->bytesPerSampleOf(I)I

    #@5
    move-result v0

    #@6
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@8
    .line 41
    return-void
.end method

.method public setBytesPerSample(I)V
    .registers 3
    .parameter "bytesPerSample"

    #@0
    .prologue
    .line 48
    iput p1, p0, Landroid/filterfw/core/FrameFormat;->mBytesPerSample:I

    #@2
    .line 49
    const/4 v0, -0x1

    #@3
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@5
    .line 50
    return-void
.end method

.method public setDimensionCount(I)V
    .registers 3
    .parameter "count"

    #@0
    .prologue
    .line 82
    new-array v0, p1, [I

    #@2
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@4
    .line 83
    return-void
.end method

.method public setDimensions(I)V
    .registers 4
    .parameter "size"

    #@0
    .prologue
    .line 58
    const/4 v1, 0x1

    #@1
    new-array v0, v1, [I

    #@3
    .line 59
    .local v0, dimensions:[I
    const/4 v1, 0x0

    #@4
    aput p1, v0, v1

    #@6
    .line 60
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@8
    .line 61
    const/4 v1, -0x1

    #@9
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@b
    .line 62
    return-void
.end method

.method public setDimensions(II)V
    .registers 5
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 65
    const/4 v1, 0x2

    #@1
    new-array v0, v1, [I

    #@3
    .line 66
    .local v0, dimensions:[I
    const/4 v1, 0x0

    #@4
    aput p1, v0, v1

    #@6
    .line 67
    const/4 v1, 0x1

    #@7
    aput p2, v0, v1

    #@9
    .line 68
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@b
    .line 69
    const/4 v1, -0x1

    #@c
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@e
    .line 70
    return-void
.end method

.method public setDimensions(III)V
    .registers 6
    .parameter "width"
    .parameter "height"
    .parameter "depth"

    #@0
    .prologue
    .line 73
    const/4 v1, 0x3

    #@1
    new-array v0, v1, [I

    #@3
    .line 74
    .local v0, dimensions:[I
    const/4 v1, 0x0

    #@4
    aput p1, v0, v1

    #@6
    .line 75
    const/4 v1, 0x1

    #@7
    aput p2, v0, v1

    #@9
    .line 76
    const/4 v1, 0x2

    #@a
    aput p3, v0, v1

    #@c
    .line 77
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@e
    .line 78
    const/4 v1, -0x1

    #@f
    iput v1, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@11
    .line 79
    return-void
.end method

.method public setDimensions([I)V
    .registers 3
    .parameter "dimensions"

    #@0
    .prologue
    .line 53
    if-nez p1, :cond_9

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mDimensions:[I

    #@5
    .line 54
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/filterfw/core/FrameFormat;->mSize:I

    #@8
    .line 55
    return-void

    #@9
    .line 53
    :cond_9
    array-length v0, p1

    #@a
    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    #@d
    move-result-object v0

    #@e
    goto :goto_3
.end method

.method public setMetaValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 91
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@6
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@9
    iput-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@b
    .line 93
    :cond_b
    iget-object v0, p0, Landroid/filterfw/core/FrameFormat;->mMetaData:Landroid/filterfw/core/KeyValueMap;

    #@d
    invoke-virtual {v0, p1, p2}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 94
    return-void
.end method

.method public setObjectClass(Ljava/lang/Class;)V
    .registers 2
    .parameter "objectClass"

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Landroid/filterfw/core/FrameFormat;->mObjectClass:Ljava/lang/Class;

    #@2
    .line 87
    return-void
.end method

.method public setTarget(I)V
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 44
    iput p1, p0, Landroid/filterfw/core/FrameFormat;->mTarget:I

    #@2
    .line 45
    return-void
.end method
