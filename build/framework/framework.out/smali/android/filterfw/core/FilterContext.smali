.class public Landroid/filterfw/core/FilterContext;
.super Ljava/lang/Object;
.source "FilterContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/core/FilterContext$OnFrameReceivedListener;
    }
.end annotation


# instance fields
.field private mFrameManager:Landroid/filterfw/core/FrameManager;

.field private mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

.field private mGraphs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/filterfw/core/FilterGraph;",
            ">;"
        }
    .end annotation
.end field

.field private mStoredFrames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/filterfw/core/Frame;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@a
    .line 37
    new-instance v0, Ljava/util/HashSet;

    #@c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@f
    iput-object v0, p0, Landroid/filterfw/core/FilterContext;->mGraphs:Ljava/util/Set;

    #@11
    .line 68
    return-void
.end method


# virtual methods
.method final addGraph(Landroid/filterfw/core/FilterGraph;)V
    .registers 3
    .parameter "graph"

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Landroid/filterfw/core/FilterContext;->mGraphs:Ljava/util/Set;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@5
    .line 125
    return-void
.end method

.method public declared-synchronized fetchFrame(Ljava/lang/String;)Landroid/filterfw/core/Frame;
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 82
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/filterfw/core/Frame;

    #@9
    .line 83
    .local v0, frame:Landroid/filterfw/core/Frame;
    if-eqz v0, :cond_e

    #@b
    .line 84
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->onFrameFetch()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    #@e
    .line 86
    :cond_e
    monitor-exit p0

    #@f
    return-object v0

    #@10
    .line 82
    .end local v0           #frame:Landroid/filterfw/core/Frame;
    :catchall_10
    move-exception v1

    #@11
    monitor-exit p0

    #@12
    throw v1
.end method

.method public getFrameManager()Landroid/filterfw/core/FrameManager;
    .registers 2

    #@0
    .prologue
    .line 40
    iget-object v0, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@2
    return-object v0
.end method

.method public getGLEnvironment()Landroid/filterfw/core/GLEnvironment;
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@2
    return-object v0
.end method

.method public initGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V
    .registers 4
    .parameter "environment"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 61
    iput-object p1, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@6
    .line 66
    return-void

    #@7
    .line 63
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    #@9
    const-string v1, "Attempting to re-initialize GL Environment for FilterContext!"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0
.end method

.method public declared-synchronized removeFrame(Ljava/lang/String;)V
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 90
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@3
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/filterfw/core/Frame;

    #@9
    .line 91
    .local v0, frame:Landroid/filterfw/core/Frame;
    if-eqz v0, :cond_13

    #@b
    .line 92
    iget-object v1, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@d
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    .line 93
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    #@13
    .line 95
    :cond_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 90
    .end local v0           #frame:Landroid/filterfw/core/Frame;
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0

    #@17
    throw v1
.end method

.method public setFrameManager(Landroid/filterfw/core/FrameManager;)V
    .registers 4
    .parameter "manager"

    #@0
    .prologue
    .line 44
    if-nez p1, :cond_a

    #@2
    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "Attempting to set null FrameManager!"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 46
    :cond_a
    invoke-virtual {p1}, Landroid/filterfw/core/FrameManager;->getContext()Landroid/filterfw/core/FilterContext;

    #@d
    move-result-object v0

    #@e
    if-eqz v0, :cond_18

    #@10
    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Attempting to set FrameManager which is already bound to another FilterContext!"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 50
    :cond_18
    iput-object p1, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@1a
    .line 51
    iget-object v0, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@1c
    invoke-virtual {v0, p0}, Landroid/filterfw/core/FrameManager;->setContext(Landroid/filterfw/core/FilterContext;)V

    #@1f
    .line 53
    return-void
.end method

.method public declared-synchronized storeFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    .registers 6
    .parameter "key"
    .parameter "frame"

    #@0
    .prologue
    .line 73
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/FilterContext;->fetchFrame(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    #@4
    move-result-object v0

    #@5
    .line 74
    .local v0, storedFrame:Landroid/filterfw/core/Frame;
    if-eqz v0, :cond_a

    #@7
    .line 75
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@a
    .line 77
    :cond_a
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->onFrameStore()V

    #@d
    .line 78
    iget-object v1, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@f
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->retain()Landroid/filterfw/core/Frame;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_18

    #@16
    .line 79
    monitor-exit p0

    #@17
    return-void

    #@18
    .line 73
    .end local v0           #storedFrame:Landroid/filterfw/core/Frame;
    :catchall_18
    move-exception v1

    #@19
    monitor-exit p0

    #@1a
    throw v1
.end method

.method public declared-synchronized tearDown()V
    .registers 5

    #@0
    .prologue
    .line 99
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    .local v2, i$:Ljava/util/Iterator;
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1e

    #@11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/filterfw/core/Frame;

    #@17
    .line 100
    .local v0, frame:Landroid/filterfw/core/Frame;
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1b

    #@1a
    goto :goto_b

    #@1b
    .line 99
    .end local v0           #frame:Landroid/filterfw/core/Frame;
    .end local v2           #i$:Ljava/util/Iterator;
    :catchall_1b
    move-exception v3

    #@1c
    monitor-exit p0

    #@1d
    throw v3

    #@1e
    .line 102
    .restart local v2       #i$:Ljava/util/Iterator;
    :cond_1e
    :try_start_1e
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mStoredFrames:Ljava/util/HashMap;

    #@20
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@23
    .line 105
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mGraphs:Ljava/util/Set;

    #@25
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@28
    move-result-object v2

    #@29
    :goto_29
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_39

    #@2f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Landroid/filterfw/core/FilterGraph;

    #@35
    .line 106
    .local v1, graph:Landroid/filterfw/core/FilterGraph;
    invoke-virtual {v1, p0}, Landroid/filterfw/core/FilterGraph;->tearDown(Landroid/filterfw/core/FilterContext;)V

    #@38
    goto :goto_29

    #@39
    .line 108
    .end local v1           #graph:Landroid/filterfw/core/FilterGraph;
    :cond_39
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mGraphs:Ljava/util/Set;

    #@3b
    invoke-interface {v3}, Ljava/util/Set;->clear()V

    #@3e
    .line 111
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@40
    if-eqz v3, :cond_4a

    #@42
    .line 112
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@44
    invoke-virtual {v3}, Landroid/filterfw/core/FrameManager;->tearDown()V

    #@47
    .line 113
    const/4 v3, 0x0

    #@48
    iput-object v3, p0, Landroid/filterfw/core/FilterContext;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@4a
    .line 117
    :cond_4a
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@4c
    if-eqz v3, :cond_56

    #@4e
    .line 118
    iget-object v3, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@50
    invoke-virtual {v3}, Landroid/filterfw/core/GLEnvironment;->tearDown()V

    #@53
    .line 119
    const/4 v3, 0x0

    #@54
    iput-object v3, p0, Landroid/filterfw/core/FilterContext;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;
    :try_end_56
    .catchall {:try_start_1e .. :try_end_56} :catchall_1b

    #@56
    .line 121
    :cond_56
    monitor-exit p0

    #@57
    return-void
.end method
