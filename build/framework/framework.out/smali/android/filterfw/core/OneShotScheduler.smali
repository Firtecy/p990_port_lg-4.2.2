.class public Landroid/filterfw/core/OneShotScheduler;
.super Landroid/filterfw/core/RoundRobinScheduler;
.source "OneShotScheduler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OneShotScheduler"


# instance fields
.field private final mLogVerbose:Z

.field private scheduled:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterGraph;)V
    .registers 4
    .parameter "graph"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/filterfw/core/RoundRobinScheduler;-><init>(Landroid/filterfw/core/FilterGraph;)V

    #@3
    .line 42
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/filterfw/core/OneShotScheduler;->scheduled:Ljava/util/HashMap;

    #@a
    .line 43
    const-string v0, "OneShotScheduler"

    #@c
    const/4 v1, 0x2

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@10
    move-result v0

    #@11
    iput-boolean v0, p0, Landroid/filterfw/core/OneShotScheduler;->mLogVerbose:Z

    #@13
    .line 44
    return-void
.end method


# virtual methods
.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 48
    invoke-super {p0}, Landroid/filterfw/core/RoundRobinScheduler;->reset()V

    #@3
    .line 49
    iget-object v0, p0, Landroid/filterfw/core/OneShotScheduler;->scheduled:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@8
    .line 50
    return-void
.end method

.method public scheduleNextNode()Landroid/filterfw/core/Filter;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 54
    const/4 v1, 0x0

    #@2
    .line 57
    .local v1, first:Landroid/filterfw/core/Filter;
    :cond_2
    :goto_2
    invoke-super {p0}, Landroid/filterfw/core/RoundRobinScheduler;->scheduleNextNode()Landroid/filterfw/core/Filter;

    #@5
    move-result-object v0

    #@6
    .line 58
    .local v0, filter:Landroid/filterfw/core/Filter;
    if-nez v0, :cond_15

    #@8
    .line 59
    iget-boolean v3, p0, Landroid/filterfw/core/OneShotScheduler;->mLogVerbose:Z

    #@a
    if-eqz v3, :cond_13

    #@c
    const-string v3, "OneShotScheduler"

    #@e
    const-string v4, "No filters available to run."

    #@10
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    :cond_13
    move-object v0, v2

    #@14
    .line 76
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_14
    :goto_14
    return-object v0

    #@15
    .line 62
    .restart local v0       #filter:Landroid/filterfw/core/Filter;
    :cond_15
    iget-object v3, p0, Landroid/filterfw/core/OneShotScheduler;->scheduled:Ljava/util/HashMap;

    #@17
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1e
    move-result v3

    #@1f
    if-nez v3, :cond_64

    #@21
    .line 63
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getNumberOfConnectedInputs()I

    #@24
    move-result v2

    #@25
    if-nez v2, :cond_35

    #@27
    .line 64
    iget-object v2, p0, Landroid/filterfw/core/OneShotScheduler;->scheduled:Ljava/util/HashMap;

    #@29
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    const/4 v4, 0x1

    #@2e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    .line 65
    :cond_35
    iget-boolean v2, p0, Landroid/filterfw/core/OneShotScheduler;->mLogVerbose:Z

    #@37
    if-eqz v2, :cond_14

    #@39
    const-string v2, "OneShotScheduler"

    #@3b
    new-instance v3, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v4, "Scheduling filter \""

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    const-string v4, "\" of type "

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getFilterClassName()Ljava/lang/String;

    #@57
    move-result-object v4

    #@58
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_14

    #@64
    .line 69
    :cond_64
    if-ne v1, v0, :cond_73

    #@66
    .line 75
    iget-boolean v3, p0, Landroid/filterfw/core/OneShotScheduler;->mLogVerbose:Z

    #@68
    if-eqz v3, :cond_71

    #@6a
    const-string v3, "OneShotScheduler"

    #@6c
    const-string v4, "One pass through graph completed."

    #@6e
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    :cond_71
    move-object v0, v2

    #@72
    .line 76
    goto :goto_14

    #@73
    .line 73
    :cond_73
    if-nez v1, :cond_2

    #@75
    move-object v1, v0

    #@76
    goto :goto_2
.end method
