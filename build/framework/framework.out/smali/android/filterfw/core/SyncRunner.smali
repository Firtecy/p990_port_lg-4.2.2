.class public Landroid/filterfw/core/SyncRunner;
.super Landroid/filterfw/core/GraphRunner;
.source "SyncRunner.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncRunner"


# instance fields
.field private mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

.field private final mLogVerbose:Z

.field private mScheduler:Landroid/filterfw/core/Scheduler;

.field private mTimer:Landroid/filterfw/core/StopWatchMap;

.field private mWakeCondition:Landroid/os/ConditionVariable;

.field private mWakeExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/FilterGraph;Ljava/lang/Class;)V
    .registers 9
    .parameter "context"
    .parameter "graph"
    .parameter "schedulerClass"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 46
    invoke-direct {p0, p1}, Landroid/filterfw/core/GraphRunner;-><init>(Landroid/filterfw/core/FilterContext;)V

    #@5
    .line 33
    iput-object v4, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@7
    .line 35
    iput-object v4, p0, Landroid/filterfw/core/SyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@9
    .line 36
    new-instance v2, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    #@b
    invoke-direct {v2, v3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    #@e
    iput-object v2, p0, Landroid/filterfw/core/SyncRunner;->mWakeExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    #@10
    .line 37
    new-instance v2, Landroid/os/ConditionVariable;

    #@12
    invoke-direct {v2}, Landroid/os/ConditionVariable;-><init>()V

    #@15
    iput-object v2, p0, Landroid/filterfw/core/SyncRunner;->mWakeCondition:Landroid/os/ConditionVariable;

    #@17
    .line 39
    iput-object v4, p0, Landroid/filterfw/core/SyncRunner;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@19
    .line 48
    const-string v2, "SyncRunner"

    #@1b
    const/4 v3, 0x2

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1f
    move-result v2

    #@20
    iput-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@22
    .line 50
    iget-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@24
    if-eqz v2, :cond_2d

    #@26
    const-string v2, "SyncRunner"

    #@28
    const-string v3, "Initializing SyncRunner"

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 53
    :cond_2d
    const-class v2, Landroid/filterfw/core/Scheduler;

    #@2f
    invoke-virtual {v2, p3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_99

    #@35
    .line 55
    const/4 v2, 0x1

    #@36
    :try_start_36
    new-array v2, v2, [Ljava/lang/Class;

    #@38
    const/4 v3, 0x0

    #@39
    const-class v4, Landroid/filterfw/core/FilterGraph;

    #@3b
    aput-object v4, v2, v3

    #@3d
    invoke-virtual {p3, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    #@40
    move-result-object v1

    #@41
    .line 56
    .local v1, schedulerConstructor:Ljava/lang/reflect/Constructor;
    const/4 v2, 0x1

    #@42
    new-array v2, v2, [Ljava/lang/Object;

    #@44
    const/4 v3, 0x0

    #@45
    aput-object p2, v2, v3

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    move-result-object v2

    #@4b
    check-cast v2, Landroid/filterfw/core/Scheduler;

    #@4d
    iput-object v2, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;
    :try_end_4f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_36 .. :try_end_4f} :catch_6c
    .catch Ljava/lang/InstantiationException; {:try_start_36 .. :try_end_4f} :catch_75
    .catch Ljava/lang/IllegalAccessException; {:try_start_36 .. :try_end_4f} :catch_7e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_36 .. :try_end_4f} :catch_87
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_4f} :catch_90

    #@4f
    .line 73
    iput-object p1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@51
    .line 74
    iget-object v2, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@53
    invoke-virtual {v2, p2}, Landroid/filterfw/core/FilterContext;->addGraph(Landroid/filterfw/core/FilterGraph;)V

    #@56
    .line 76
    new-instance v2, Landroid/filterfw/core/StopWatchMap;

    #@58
    invoke-direct {v2}, Landroid/filterfw/core/StopWatchMap;-><init>()V

    #@5b
    iput-object v2, p0, Landroid/filterfw/core/SyncRunner;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@5d
    .line 78
    iget-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@5f
    if-eqz v2, :cond_68

    #@61
    const-string v2, "SyncRunner"

    #@63
    const-string v3, "Setting up filters"

    #@65
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 81
    :cond_68
    invoke-virtual {p2}, Landroid/filterfw/core/FilterGraph;->setupFilters()V

    #@6b
    .line 82
    return-void

    #@6c
    .line 57
    .end local v1           #schedulerConstructor:Ljava/lang/reflect/Constructor;
    :catch_6c
    move-exception v0

    #@6d
    .line 58
    .local v0, e:Ljava/lang/NoSuchMethodException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@6f
    const-string v3, "Scheduler does not have constructor <init>(FilterGraph)!"

    #@71
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@74
    throw v2

    #@75
    .line 59
    .end local v0           #e:Ljava/lang/NoSuchMethodException;
    :catch_75
    move-exception v0

    #@76
    .line 60
    .local v0, e:Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@78
    const-string v3, "Could not instantiate the Scheduler instance!"

    #@7a
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@7d
    throw v2

    #@7e
    .line 61
    .end local v0           #e:Ljava/lang/InstantiationException;
    :catch_7e
    move-exception v0

    #@7f
    .line 62
    .local v0, e:Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@81
    const-string v3, "Cannot access Scheduler constructor!"

    #@83
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@86
    throw v2

    #@87
    .line 63
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catch_87
    move-exception v0

    #@88
    .line 64
    .local v0, e:Ljava/lang/reflect/InvocationTargetException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@8a
    const-string v3, "Scheduler constructor threw an exception"

    #@8c
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@8f
    throw v2

    #@90
    .line 65
    .end local v0           #e:Ljava/lang/reflect/InvocationTargetException;
    :catch_90
    move-exception v0

    #@91
    .line 66
    .local v0, e:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    #@93
    const-string v3, "Could not instantiate Scheduler"

    #@95
    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@98
    throw v2

    #@99
    .line 69
    .end local v0           #e:Ljava/lang/Exception;
    :cond_99
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@9b
    const-string v3, "Class provided is not a Scheduler subclass!"

    #@9d
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a0
    throw v2
.end method


# virtual methods
.method assertReadyToStep()V
    .registers 3

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 222
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "Attempting to run schedule with no scheduler in place!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 223
    :cond_c
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@f
    move-result-object v0

    #@10
    if-nez v0, :cond_1a

    #@12
    .line 224
    new-instance v0, Ljava/lang/RuntimeException;

    #@14
    const-string v1, "Calling step on scheduler with no graph in place!"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 226
    :cond_1a
    return-void
.end method

.method public beginProcessing()V
    .registers 2

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/Scheduler;->reset()V

    #@5
    .line 99
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Landroid/filterfw/core/FilterGraph;->beginProcessing()V

    #@c
    .line 100
    return-void
.end method

.method public close()V
    .registers 3

    #@0
    .prologue
    .line 104
    iget-boolean v0, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "SyncRunner"

    #@6
    const-string v1, "Closing graph."

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 105
    :cond_b
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@e
    move-result-object v0

    #@f
    iget-object v1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@11
    invoke-virtual {v0, v1}, Landroid/filterfw/core/FilterGraph;->closeFilters(Landroid/filterfw/core/FilterContext;)V

    #@14
    .line 106
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@16
    invoke-virtual {v0}, Landroid/filterfw/core/Scheduler;->reset()V

    #@19
    .line 107
    return-void
.end method

.method protected determinePostRunState()I
    .registers 6

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    .line 191
    const/4 v2, 0x0

    #@2
    .line 192
    .local v2, isBlocked:Z
    iget-object v4, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@4
    invoke-virtual {v4}, Landroid/filterfw/core/Scheduler;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@7
    move-result-object v4

    #@8
    invoke-virtual {v4}, Landroid/filterfw/core/FilterGraph;->getFilters()Ljava/util/Set;

    #@b
    move-result-object v4

    #@c
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v1

    #@10
    .local v1, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_2a

    #@16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Landroid/filterfw/core/Filter;

    #@1c
    .line 193
    .local v0, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->isOpen()Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_10

    #@22
    .line 194
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getStatus()I

    #@25
    move-result v4

    #@26
    if-ne v4, v3, :cond_29

    #@28
    .line 196
    const/4 v3, 0x3

    #@29
    .line 203
    .end local v0           #filter:Landroid/filterfw/core/Filter;
    :cond_29
    :goto_29
    return v3

    #@2a
    :cond_2a
    const/4 v3, 0x2

    #@2b
    goto :goto_29
.end method

.method public declared-synchronized getError()Ljava/lang/Exception;
    .registers 2

    #@0
    .prologue
    .line 155
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    monitor-exit p0

    #@3
    return-object v0
.end method

.method public getGraph()Landroid/filterfw/core/FilterGraph;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/Scheduler;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 140
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method performStep()Z
    .registers 4

    #@0
    .prologue
    .line 208
    iget-boolean v1, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@2
    if-eqz v1, :cond_b

    #@4
    const-string v1, "SyncRunner"

    #@6
    const-string v2, "Performing one step."

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 209
    :cond_b
    iget-object v1, p0, Landroid/filterfw/core/SyncRunner;->mScheduler:Landroid/filterfw/core/Scheduler;

    #@d
    invoke-virtual {v1}, Landroid/filterfw/core/Scheduler;->scheduleNextNode()Landroid/filterfw/core/Filter;

    #@10
    move-result-object v0

    #@11
    .line 210
    .local v0, filter:Landroid/filterfw/core/Filter;
    if-eqz v0, :cond_2a

    #@13
    .line 211
    iget-object v1, p0, Landroid/filterfw/core/SyncRunner;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@15
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v1, v2}, Landroid/filterfw/core/StopWatchMap;->start(Ljava/lang/String;)V

    #@1c
    .line 212
    invoke-virtual {p0, v0}, Landroid/filterfw/core/SyncRunner;->processFilterNode(Landroid/filterfw/core/Filter;)V

    #@1f
    .line 213
    iget-object v1, p0, Landroid/filterfw/core/SyncRunner;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@21
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v1, v2}, Landroid/filterfw/core/StopWatchMap;->stop(Ljava/lang/String;)V

    #@28
    .line 214
    const/4 v1, 0x1

    #@29
    .line 216
    :goto_29
    return v1

    #@2a
    :cond_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_29
.end method

.method protected processFilterNode(Landroid/filterfw/core/Filter;)V
    .registers 5
    .parameter "filter"

    #@0
    .prologue
    .line 163
    iget-boolean v0, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    const-string v0, "SyncRunner"

    #@6
    const-string v1, "Processing filter node"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 164
    :cond_b
    iget-object v0, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@d
    invoke-virtual {p1, v0}, Landroid/filterfw/core/Filter;->performProcess(Landroid/filterfw/core/FilterContext;)V

    #@10
    .line 165
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getStatus()I

    #@13
    move-result v0

    #@14
    const/4 v1, 0x6

    #@15
    if-ne v0, v1, :cond_36

    #@17
    .line 166
    new-instance v0, Ljava/lang/RuntimeException;

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "There was an error executing "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, "!"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@35
    throw v0

    #@36
    .line 167
    :cond_36
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getStatus()I

    #@39
    move-result v0

    #@3a
    const/4 v1, 0x4

    #@3b
    if-ne v0, v1, :cond_4f

    #@3d
    .line 168
    iget-boolean v0, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@3f
    if-eqz v0, :cond_48

    #@41
    const-string v0, "SyncRunner"

    #@43
    const-string v1, "Scheduling filter wakeup"

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 169
    :cond_48
    invoke-virtual {p1}, Landroid/filterfw/core/Filter;->getSleepDelay()I

    #@4b
    move-result v0

    #@4c
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/SyncRunner;->scheduleFilterWake(Landroid/filterfw/core/Filter;I)V

    #@4f
    .line 171
    :cond_4f
    return-void
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 111
    iget-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@2
    if-eqz v2, :cond_b

    #@4
    const-string v2, "SyncRunner"

    #@6
    const-string v3, "Beginning run."

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 113
    :cond_b
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->assertReadyToStep()V

    #@e
    .line 116
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->beginProcessing()V

    #@11
    .line 117
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->activateGlContext()Z

    #@14
    move-result v0

    #@15
    .line 120
    .local v0, glActivated:Z
    const/4 v1, 0x1

    #@16
    .line 121
    .local v1, keepRunning:Z
    :goto_16
    if-eqz v1, :cond_1d

    #@18
    .line 122
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->performStep()Z

    #@1b
    move-result v1

    #@1c
    goto :goto_16

    #@1d
    .line 126
    :cond_1d
    if-eqz v0, :cond_22

    #@1f
    .line 127
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->deactivateGlContext()V

    #@22
    .line 131
    :cond_22
    iget-object v2, p0, Landroid/filterfw/core/SyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@24
    if-eqz v2, :cond_3a

    #@26
    .line 132
    iget-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@28
    if-eqz v2, :cond_31

    #@2a
    const-string v2, "SyncRunner"

    #@2c
    const-string v3, "Calling completion listener."

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 133
    :cond_31
    iget-object v2, p0, Landroid/filterfw/core/SyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@33
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->determinePostRunState()I

    #@36
    move-result v3

    #@37
    invoke-interface {v2, v3}, Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;->onRunnerDone(I)V

    #@3a
    .line 135
    :cond_3a
    iget-boolean v2, p0, Landroid/filterfw/core/SyncRunner;->mLogVerbose:Z

    #@3c
    if-eqz v2, :cond_45

    #@3e
    const-string v2, "SyncRunner"

    #@40
    const-string v3, "Run complete"

    #@42
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 136
    :cond_45
    return-void
.end method

.method protected scheduleFilterWake(Landroid/filterfw/core/Filter;I)V
    .registers 10
    .parameter "filter"
    .parameter "delay"

    #@0
    .prologue
    .line 175
    iget-object v2, p0, Landroid/filterfw/core/SyncRunner;->mWakeCondition:Landroid/os/ConditionVariable;

    #@2
    invoke-virtual {v2}, Landroid/os/ConditionVariable;->close()V

    #@5
    .line 178
    move-object v1, p1

    #@6
    .line 179
    .local v1, filterToSchedule:Landroid/filterfw/core/Filter;
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mWakeCondition:Landroid/os/ConditionVariable;

    #@8
    .line 181
    .local v0, conditionToWake:Landroid/os/ConditionVariable;
    iget-object v2, p0, Landroid/filterfw/core/SyncRunner;->mWakeExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    #@a
    new-instance v3, Landroid/filterfw/core/SyncRunner$1;

    #@c
    invoke-direct {v3, p0, v1, v0}, Landroid/filterfw/core/SyncRunner$1;-><init>(Landroid/filterfw/core/SyncRunner;Landroid/filterfw/core/Filter;Landroid/os/ConditionVariable;)V

    #@f
    int-to-long v4, p2

    #@10
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    #@12
    invoke-virtual {v2, v3, v4, v5, v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    #@15
    .line 188
    return-void
.end method

.method public setDoneCallback(Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 145
    iput-object p1, p0, Landroid/filterfw/core/SyncRunner;->mDoneListener:Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@2
    .line 146
    return-void
.end method

.method public step()I
    .registers 3

    #@0
    .prologue
    .line 90
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->assertReadyToStep()V

    #@3
    .line 91
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0}, Landroid/filterfw/core/FilterGraph;->isReady()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_15

    #@d
    .line 92
    new-instance v0, Ljava/lang/RuntimeException;

    #@f
    const-string v1, "Trying to process graph that is not open!"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 94
    :cond_15
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->performStep()Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_1d

    #@1b
    const/4 v0, 0x1

    #@1c
    :goto_1c
    return v0

    #@1d
    :cond_1d
    invoke-virtual {p0}, Landroid/filterfw/core/SyncRunner;->determinePostRunState()I

    #@20
    move-result v0

    #@21
    goto :goto_1c
.end method

.method public stop()V
    .registers 3

    #@0
    .prologue
    .line 150
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "SyncRunner does not support stopping a graph!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method protected waitUntilWake()V
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/filterfw/core/SyncRunner;->mWakeCondition:Landroid/os/ConditionVariable;

    #@2
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    #@5
    .line 160
    return-void
.end method
