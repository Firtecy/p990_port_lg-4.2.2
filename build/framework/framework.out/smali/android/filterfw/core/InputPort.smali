.class public abstract Landroid/filterfw/core/InputPort;
.super Landroid/filterfw/core/FilterPort;
.source "InputPort.java"


# instance fields
.field protected mSourcePort:Landroid/filterfw/core/OutputPort;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V
    .registers 3
    .parameter "filter"
    .parameter "name"

    #@0
    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/FilterPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@3
    .line 29
    return-void
.end method


# virtual methods
.method public acceptsFrame()Z
    .registers 2

    #@0
    .prologue
    .line 81
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->hasFrame()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->isOpen()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 51
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@e
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->close()V

    #@11
    .line 53
    :cond_11
    invoke-super {p0}, Landroid/filterfw/core/FilterPort;->close()V

    #@14
    .line 54
    return-void
.end method

.method public filterMustClose()Z
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->isOpen()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_14

    #@6
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->isBlocking()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_14

    #@c
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->hasFrame()Z

    #@f
    move-result v0

    #@10
    if-nez v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method public getSourceFilter()Landroid/filterfw/core/Filter;
    .registers 2

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@8
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->getFilter()Landroid/filterfw/core/Filter;

    #@b
    move-result-object v0

    #@c
    goto :goto_5
.end method

.method public getSourceFormat()Landroid/filterfw/core/FrameFormat;
    .registers 2

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->getPortFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v0

    #@a
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->getPortFormat()Landroid/filterfw/core/FrameFormat;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method public getSourcePort()Landroid/filterfw/core/OutputPort;
    .registers 2

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    return-object v0
.end method

.method public getTarget()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 69
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public isReady()Z
    .registers 2

    #@0
    .prologue
    .line 77
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->hasFrame()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0}, Landroid/filterfw/core/InputPort;->isBlocking()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public open()V
    .registers 2

    #@0
    .prologue
    .line 43
    invoke-super {p0}, Landroid/filterfw/core/FilterPort;->open()V

    #@3
    .line 44
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@5
    if-eqz v0, :cond_14

    #@7
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@9
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->isOpen()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_14

    #@f
    .line 45
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@11
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->open()V

    #@14
    .line 47
    :cond_14
    return-void
.end method

.method public setSourcePort(Landroid/filterfw/core/OutputPort;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2
    if-eqz v0, :cond_29

    #@4
    .line 33
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, " already connected to "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "!"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 35
    :cond_29
    iput-object p1, p0, Landroid/filterfw/core/InputPort;->mSourcePort:Landroid/filterfw/core/OutputPort;

    #@2b
    .line 36
    return-void
.end method

.method public abstract transfer(Landroid/filterfw/core/FilterContext;)V
.end method
