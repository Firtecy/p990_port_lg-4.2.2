.class public Landroid/filterfw/core/SerializedFrame;
.super Landroid/filterfw/core/Frame;
.source "SerializedFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;,
        Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;
    }
.end annotation


# static fields
.field private static final INITIAL_CAPACITY:I = 0x40


# instance fields
.field private mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

.field private mObjectOut:Ljava/io/ObjectOutputStream;


# direct methods
.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 6
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    .line 170
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3
    .line 171
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v1}, Landroid/filterfw/core/SerializedFrame;->setReusable(Z)V

    #@7
    .line 175
    :try_start_7
    new-instance v1, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@9
    const/16 v2, 0x40

    #@b
    invoke-direct {v1, p0, v2}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;-><init>(Landroid/filterfw/core/SerializedFrame;I)V

    #@e
    iput-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@10
    .line 176
    new-instance v1, Ljava/io/ObjectOutputStream;

    #@12
    iget-object v2, p0, Landroid/filterfw/core/SerializedFrame;->mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@14
    invoke-direct {v1, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@17
    iput-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mObjectOut:Ljava/io/ObjectOutputStream;

    #@19
    .line 177
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@1b
    invoke-virtual {v1}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->markHeaderEnd()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_1e} :catch_1f

    #@1e
    .line 182
    return-void

    #@1f
    .line 178
    :catch_1f
    move-exception v0

    #@20
    .line 179
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "Could not create serialization streams for SerializedFrame!"

    #@24
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@27
    throw v1
.end method

.method private final deserializeObjectValue()Ljava/lang/Object;
    .registers 7

    #@0
    .prologue
    .line 272
    :try_start_0
    iget-object v3, p0, Landroid/filterfw/core/SerializedFrame;->mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@2
    invoke-virtual {v3}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->getInputStream()Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;

    #@5
    move-result-object v1

    #@6
    .line 273
    .local v1, inputStream:Ljava/io/InputStream;
    new-instance v2, Ljava/io/ObjectInputStream;

    #@8
    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    #@b
    .line 274
    .local v2, objectStream:Ljava/io/ObjectInputStream;
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_e} :catch_10
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_e} :catch_30

    #@e
    move-result-object v3

    #@f
    return-object v3

    #@10
    .line 275
    .end local v1           #inputStream:Ljava/io/InputStream;
    .end local v2           #objectStream:Ljava/io/ObjectInputStream;
    :catch_10
    move-exception v0

    #@11
    .line 276
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@13
    new-instance v4, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v5, "Could not deserialize object in "

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, "!"

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@2f
    throw v3

    #@30
    .line 277
    .end local v0           #e:Ljava/io/IOException;
    :catch_30
    move-exception v0

    #@31
    .line 278
    .local v0, e:Ljava/lang/ClassNotFoundException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "Unable to deserialize object of unknown class in "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, "!"

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4f
    throw v3
.end method

.method private final serializeObjectValue(Ljava/lang/Object;)V
    .registers 6
    .parameter "object"

    #@0
    .prologue
    .line 260
    :try_start_0
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mByteOutputStream:Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/SerializedFrame$DirectByteOutputStream;->reset()V

    #@5
    .line 261
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mObjectOut:Ljava/io/ObjectOutputStream;

    #@7
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    #@a
    .line 262
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mObjectOut:Ljava/io/ObjectOutputStream;

    #@c
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->flush()V

    #@f
    .line 263
    iget-object v1, p0, Landroid/filterfw/core/SerializedFrame;->mObjectOut:Ljava/io/ObjectOutputStream;

    #@11
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_14} :catch_15

    #@14
    .line 268
    return-void

    #@15
    .line 264
    :catch_15
    move-exception v0

    #@16
    .line 265
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "Could not serialize object "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, " in "

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    const-string v3, "!"

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3e
    throw v1
.end method

.method static wrapObject(Ljava/lang/Object;Landroid/filterfw/core/FrameManager;)Landroid/filterfw/core/SerializedFrame;
    .registers 5
    .parameter "object"
    .parameter "frameManager"

    #@0
    .prologue
    .line 185
    const/4 v2, 0x1

    #@1
    invoke-static {p0, v2}, Landroid/filterfw/format/ObjectFormat;->fromObject(Ljava/lang/Object;I)Landroid/filterfw/core/MutableFrameFormat;

    #@4
    move-result-object v0

    #@5
    .line 186
    .local v0, format:Landroid/filterfw/core/FrameFormat;
    new-instance v1, Landroid/filterfw/core/SerializedFrame;

    #@7
    invoke-direct {v1, v0, p1}, Landroid/filterfw/core/SerializedFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@a
    .line 187
    .local v1, result:Landroid/filterfw/core/SerializedFrame;
    invoke-virtual {v1, p0}, Landroid/filterfw/core/SerializedFrame;->setObjectValue(Ljava/lang/Object;)V

    #@d
    .line 188
    return-object v1
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 3

    #@0
    .prologue
    .line 249
    invoke-direct {p0}, Landroid/filterfw/core/SerializedFrame;->deserializeObjectValue()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 250
    .local v0, result:Ljava/lang/Object;
    instance-of v1, v0, Landroid/graphics/Bitmap;

    #@6
    if-eqz v1, :cond_b

    #@8
    check-cast v0, Landroid/graphics/Bitmap;

    #@a
    .end local v0           #result:Ljava/lang/Object;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #result:Ljava/lang/Object;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 3

    #@0
    .prologue
    .line 237
    invoke-direct {p0}, Landroid/filterfw/core/SerializedFrame;->deserializeObjectValue()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, result:Ljava/lang/Object;
    instance-of v1, v0, Ljava/nio/ByteBuffer;

    #@6
    if-eqz v1, :cond_b

    #@8
    check-cast v0, Ljava/nio/ByteBuffer;

    #@a
    .end local v0           #result:Ljava/lang/Object;
    :goto_a
    return-object v0

    #@b
    .restart local v0       #result:Ljava/lang/Object;
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public getFloats()[F
    .registers 3

    #@0
    .prologue
    .line 225
    invoke-direct {p0}, Landroid/filterfw/core/SerializedFrame;->deserializeObjectValue()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 226
    .local v0, result:Ljava/lang/Object;
    instance-of v1, v0, [F

    #@6
    if-eqz v1, :cond_d

    #@8
    check-cast v0, [F

    #@a
    .end local v0           #result:Ljava/lang/Object;
    check-cast v0, [F

    #@c
    :goto_c
    return-object v0

    #@d
    .restart local v0       #result:Ljava/lang/Object;
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getInts()[I
    .registers 3

    #@0
    .prologue
    .line 213
    invoke-direct {p0}, Landroid/filterfw/core/SerializedFrame;->deserializeObjectValue()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 214
    .local v0, result:Ljava/lang/Object;
    instance-of v1, v0, [I

    #@6
    if-eqz v1, :cond_d

    #@8
    check-cast v0, [I

    #@a
    .end local v0           #result:Ljava/lang/Object;
    check-cast v0, [I

    #@c
    :goto_c
    return-object v0

    #@d
    .restart local v0       #result:Ljava/lang/Object;
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getObjectValue()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 202
    invoke-direct {p0}, Landroid/filterfw/core/SerializedFrame;->deserializeObjectValue()Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected hasNativeAllocation()Z
    .registers 2

    #@0
    .prologue
    .line 193
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected releaseNativeAllocation()V
    .registers 1

    #@0
    .prologue
    .line 198
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter "bitmap"

    #@0
    .prologue
    .line 243
    invoke-virtual {p0}, Landroid/filterfw/core/SerializedFrame;->assertFrameMutable()V

    #@3
    .line 244
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SerializedFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 245
    return-void
.end method

.method public setData(Ljava/nio/ByteBuffer;II)V
    .registers 5
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 231
    invoke-virtual {p0}, Landroid/filterfw/core/SerializedFrame;->assertFrameMutable()V

    #@3
    .line 232
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@6
    move-result-object v0

    #@7
    invoke-static {v0, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/filterfw/core/SerializedFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@e
    .line 233
    return-void
.end method

.method public setFloats([F)V
    .registers 2
    .parameter "floats"

    #@0
    .prologue
    .line 219
    invoke-virtual {p0}, Landroid/filterfw/core/SerializedFrame;->assertFrameMutable()V

    #@3
    .line 220
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SerializedFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 221
    return-void
.end method

.method protected setGenericObjectValue(Ljava/lang/Object;)V
    .registers 2
    .parameter "object"

    #@0
    .prologue
    .line 255
    invoke-direct {p0, p1}, Landroid/filterfw/core/SerializedFrame;->serializeObjectValue(Ljava/lang/Object;)V

    #@3
    .line 256
    return-void
.end method

.method public setInts([I)V
    .registers 2
    .parameter "ints"

    #@0
    .prologue
    .line 207
    invoke-virtual {p0}, Landroid/filterfw/core/SerializedFrame;->assertFrameMutable()V

    #@3
    .line 208
    invoke-virtual {p0, p1}, Landroid/filterfw/core/SerializedFrame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@6
    .line 209
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 285
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SerializedFrame ("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Landroid/filterfw/core/SerializedFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ")"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method
