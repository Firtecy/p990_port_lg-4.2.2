.class public Landroid/filterfw/core/NativeBuffer;
.super Ljava/lang/Object;
.source "NativeBuffer.java"


# instance fields
.field private mAttachedFrame:Landroid/filterfw/core/Frame;

.field private mDataPointer:J

.field private mOwnsData:Z

.field private mRefCount:I

.field private mSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 104
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 105
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 28
    const-wide/16 v0, 0x0

    #@6
    iput-wide v0, p0, Landroid/filterfw/core/NativeBuffer;->mDataPointer:J

    #@8
    .line 29
    iput v2, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@a
    .line 33
    iput-boolean v2, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@c
    .line 34
    const/4 v0, 0x1

    #@d
    iput v0, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@f
    .line 37
    return-void
.end method

.method public constructor <init>(I)V
    .registers 6
    .parameter "count"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 28
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Landroid/filterfw/core/NativeBuffer;->mDataPointer:J

    #@9
    .line 29
    iput v2, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@b
    .line 33
    iput-boolean v2, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@d
    .line 34
    iput v3, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@f
    .line 40
    invoke-virtual {p0}, Landroid/filterfw/core/NativeBuffer;->getElementSize()I

    #@12
    move-result v0

    #@13
    mul-int/2addr v0, p1

    #@14
    invoke-direct {p0, v0}, Landroid/filterfw/core/NativeBuffer;->allocate(I)Z

    #@17
    .line 41
    iput-boolean v3, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@19
    .line 42
    return-void
.end method

.method private native allocate(I)Z
.end method

.method private native deallocate(Z)Z
.end method

.method private native nativeCopyTo(Landroid/filterfw/core/NativeBuffer;)Z
.end method


# virtual methods
.method protected assertReadable()V
    .registers 5

    #@0
    .prologue
    .line 114
    iget-wide v0, p0, Landroid/filterfw/core/NativeBuffer;->mDataPointer:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_18

    #@8
    iget v0, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@a
    if-eqz v0, :cond_18

    #@c
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@e
    if-eqz v0, :cond_20

    #@10
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@12
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->hasNativeAllocation()Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_20

    #@18
    .line 116
    :cond_18
    new-instance v0, Ljava/lang/NullPointerException;

    #@1a
    const-string v1, "Attempting to read from null data frame!"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 118
    :cond_20
    return-void
.end method

.method protected assertWritable()V
    .registers 3

    #@0
    .prologue
    .line 121
    invoke-virtual {p0}, Landroid/filterfw/core/NativeBuffer;->isReadOnly()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 122
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Attempting to modify read-only native (structured) data!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 124
    :cond_e
    return-void
.end method

.method attachToFrame(Landroid/filterfw/core/Frame;)V
    .registers 2
    .parameter "frame"

    #@0
    .prologue
    .line 110
    iput-object p1, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@2
    .line 111
    return-void
.end method

.method public count()I
    .registers 5

    #@0
    .prologue
    .line 64
    iget-wide v0, p0, Landroid/filterfw/core/NativeBuffer;->mDataPointer:J

    #@2
    const-wide/16 v2, 0x0

    #@4
    cmp-long v0, v0, v2

    #@6
    if-eqz v0, :cond_10

    #@8
    iget v0, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@a
    invoke-virtual {p0}, Landroid/filterfw/core/NativeBuffer;->getElementSize()I

    #@d
    move-result v1

    #@e
    div-int/2addr v0, v1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public getElementSize()I
    .registers 2

    #@0
    .prologue
    .line 68
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->isReadOnly()Z

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public mutableCopy()Landroid/filterfw/core/NativeBuffer;
    .registers 7

    #@0
    .prologue
    .line 45
    const/4 v2, 0x0

    #@1
    .line 47
    .local v2, result:Landroid/filterfw/core/NativeBuffer;
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@4
    move-result-object v1

    #@5
    .line 48
    .local v1, myClass:Ljava/lang/Class;
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    .end local v2           #result:Landroid/filterfw/core/NativeBuffer;
    check-cast v2, Landroid/filterfw/core/NativeBuffer;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_1d

    #@b
    .line 53
    .restart local v2       #result:Landroid/filterfw/core/NativeBuffer;
    iget v3, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@d
    if-lez v3, :cond_48

    #@f
    invoke-direct {p0, v2}, Landroid/filterfw/core/NativeBuffer;->nativeCopyTo(Landroid/filterfw/core/NativeBuffer;)Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_48

    #@15
    .line 54
    new-instance v3, Ljava/lang/RuntimeException;

    #@17
    const-string v4, "Failed to copy NativeBuffer to mutable instance!"

    #@19
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v3

    #@1d
    .line 49
    .end local v1           #myClass:Ljava/lang/Class;
    .end local v2           #result:Landroid/filterfw/core/NativeBuffer;
    :catch_1d
    move-exception v0

    #@1e
    .line 50
    .local v0, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Unable to allocate a copy of "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, "! Make "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    const-string/jumbo v5, "sure the class has a default constructor!"

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@47
    throw v3

    #@48
    .line 56
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #myClass:Ljava/lang/Class;
    .restart local v2       #result:Landroid/filterfw/core/NativeBuffer;
    :cond_48
    return-object v2
.end method

.method public release()Landroid/filterfw/core/NativeBuffer;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 82
    const/4 v0, 0x0

    #@3
    .line 83
    .local v0, doDealloc:Z
    iget-object v3, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@5
    if-eqz v3, :cond_1b

    #@7
    .line 84
    iget-object v3, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@9
    invoke-virtual {v3}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    #@c
    move-result-object v3

    #@d
    if-nez v3, :cond_19

    #@f
    move v0, v1

    #@10
    .line 91
    :cond_10
    :goto_10
    if-eqz v0, :cond_18

    #@12
    .line 92
    iget-boolean v1, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@14
    invoke-direct {p0, v1}, Landroid/filterfw/core/NativeBuffer;->deallocate(Z)Z

    #@17
    .line 93
    const/4 p0, 0x0

    #@18
    .line 95
    .end local p0
    :cond_18
    return-object p0

    #@19
    .restart local p0
    :cond_19
    move v0, v2

    #@1a
    .line 84
    goto :goto_10

    #@1b
    .line 85
    :cond_1b
    iget-boolean v3, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@1d
    if-eqz v3, :cond_10

    #@1f
    .line 86
    iget v3, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@21
    add-int/lit8 v3, v3, -0x1

    #@23
    iput v3, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@25
    .line 87
    iget v3, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@27
    if-nez v3, :cond_2b

    #@29
    move v0, v1

    #@2a
    :goto_2a
    goto :goto_10

    #@2b
    :cond_2b
    move v0, v2

    #@2c
    goto :goto_2a
.end method

.method public retain()Landroid/filterfw/core/NativeBuffer;
    .registers 2

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 73
    iget-object v0, p0, Landroid/filterfw/core/NativeBuffer;->mAttachedFrame:Landroid/filterfw/core/Frame;

    #@6
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->retain()Landroid/filterfw/core/Frame;

    #@9
    .line 77
    :cond_9
    :goto_9
    return-object p0

    #@a
    .line 74
    :cond_a
    iget-boolean v0, p0, Landroid/filterfw/core/NativeBuffer;->mOwnsData:Z

    #@c
    if-eqz v0, :cond_9

    #@e
    .line 75
    iget v0, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@10
    add-int/lit8 v0, v0, 0x1

    #@12
    iput v0, p0, Landroid/filterfw/core/NativeBuffer;->mRefCount:I

    #@14
    goto :goto_9
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/filterfw/core/NativeBuffer;->mSize:I

    #@2
    return v0
.end method
