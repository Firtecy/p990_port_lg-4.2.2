.class public abstract Landroid/filterfw/core/GraphRunner;
.super Ljava/lang/Object;
.source "GraphRunner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;
    }
.end annotation


# static fields
.field public static final RESULT_BLOCKED:I = 0x4

.field public static final RESULT_ERROR:I = 0x6

.field public static final RESULT_FINISHED:I = 0x2

.field public static final RESULT_RUNNING:I = 0x1

.field public static final RESULT_SLEEPING:I = 0x3

.field public static final RESULT_STOPPED:I = 0x5

.field public static final RESULT_UNKNOWN:I


# instance fields
.field protected mFilterContext:Landroid/filterfw/core/FilterContext;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 25
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@6
    .line 50
    iput-object p1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@8
    .line 51
    return-void
.end method


# virtual methods
.method protected activateGlContext()Z
    .registers 3

    #@0
    .prologue
    .line 65
    iget-object v1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 66
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v0, :cond_13

    #@8
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->isActive()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_13

    #@e
    .line 67
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->activate()V

    #@11
    .line 68
    const/4 v1, 0x1

    #@12
    .line 70
    :goto_12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_12
.end method

.method public abstract close()V
.end method

.method protected deactivateGlContext()V
    .registers 3

    #@0
    .prologue
    .line 77
    iget-object v1, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    invoke-virtual {v1}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 78
    .local v0, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v0, :cond_b

    #@8
    .line 79
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->deactivate()V

    #@b
    .line 81
    :cond_b
    return-void
.end method

.method public getContext()Landroid/filterfw/core/FilterContext;
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/filterfw/core/GraphRunner;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    return-object v0
.end method

.method public abstract getError()Ljava/lang/Exception;
.end method

.method public abstract getGraph()Landroid/filterfw/core/FilterGraph;
.end method

.method public abstract isRunning()Z
.end method

.method public abstract run()V
.end method

.method public abstract setDoneCallback(Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;)V
.end method

.method public abstract stop()V
.end method
