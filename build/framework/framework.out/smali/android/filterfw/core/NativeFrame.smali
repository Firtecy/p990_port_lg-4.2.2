.class public Landroid/filterfw/core/NativeFrame;
.super Landroid/filterfw/core/Frame;
.source "NativeFrame.java"


# instance fields
.field private nativeFrameId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 231
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 232
    return-void
.end method

.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 5
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3
    .line 36
    const/4 v1, -0x1

    #@4
    iput v1, p0, Landroid/filterfw/core/NativeFrame;->nativeFrameId:I

    #@6
    .line 40
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@9
    move-result v0

    #@a
    .line 41
    .local v0, capacity:I
    invoke-direct {p0, v0}, Landroid/filterfw/core/NativeFrame;->nativeAllocate(I)Z

    #@d
    .line 42
    if-eqz v0, :cond_14

    #@f
    const/4 v1, 0x1

    #@10
    :goto_10
    invoke-virtual {p0, v1}, Landroid/filterfw/core/NativeFrame;->setReusable(Z)V

    #@13
    .line 43
    return-void

    #@14
    .line 42
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_10
.end method

.method private native getNativeBitmap(Landroid/graphics/Bitmap;II)Z
.end method

.method private native getNativeBuffer(Landroid/filterfw/core/NativeBuffer;)Z
.end method

.method private native getNativeCapacity()I
.end method

.method private native getNativeData(I)[B
.end method

.method private native getNativeFloats(I)[F
.end method

.method private native getNativeInts(I)[I
.end method

.method private native nativeAllocate(I)Z
.end method

.method private native nativeCopyFromGL(Landroid/filterfw/core/GLFrame;)Z
.end method

.method private native nativeCopyFromNative(Landroid/filterfw/core/NativeFrame;)Z
.end method

.method private native nativeDeallocate()Z
.end method

.method private static native nativeFloatSize()I
.end method

.method private static native nativeIntSize()I
.end method

.method private native setNativeBitmap(Landroid/graphics/Bitmap;II)Z
.end method

.method private native setNativeData([BII)Z
.end method

.method private native setNativeFloats([F)Z
.end method

.method private native setNativeInts([I)Z
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 7

    #@0
    .prologue
    .line 189
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getNumberOfDimensions()I

    #@7
    move-result v3

    #@8
    const/4 v4, 0x2

    #@9
    if-eq v3, v4, :cond_13

    #@b
    .line 190
    new-instance v3, Ljava/lang/RuntimeException;

    #@d
    const-string v4, "Attempting to get Bitmap for non 2-dimensional native frame!"

    #@f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12
    throw v3

    #@13
    .line 192
    :cond_13
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@1a
    move-result v3

    #@1b
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@22
    move-result v4

    #@23
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@25
    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@28
    move-result-object v2

    #@29
    .line 195
    .local v2, result:Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getByteCount()I

    #@2c
    move-result v1

    #@2d
    .line 196
    .local v1, byteCount:I
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@34
    move-result v0

    #@35
    .line 197
    .local v0, bps:I
    invoke-direct {p0, v2, v1, v0}, Landroid/filterfw/core/NativeFrame;->getNativeBitmap(Landroid/graphics/Bitmap;II)Z

    #@38
    move-result v3

    #@39
    if-nez v3, :cond_43

    #@3b
    .line 198
    new-instance v3, Ljava/lang/RuntimeException;

    #@3d
    const-string v4, "Could not get bitmap data from native frame!"

    #@3f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42
    throw v3

    #@43
    .line 200
    :cond_43
    return-object v2
.end method

.method public getCapacity()I
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Landroid/filterfw/core/NativeFrame;->getNativeCapacity()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 3

    #@0
    .prologue
    .line 165
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@7
    move-result v1

    #@8
    invoke-direct {p0, v1}, Landroid/filterfw/core/NativeFrame;->getNativeData(I)[B

    #@b
    move-result-object v0

    #@c
    .line 166
    .local v0, data:[B
    if-nez v0, :cond_10

    #@e
    const/4 v1, 0x0

    #@f
    :goto_f
    return-object v1

    #@10
    :cond_10
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@13
    move-result-object v1

    #@14
    goto :goto_f
.end method

.method public getFloats()[F
    .registers 2

    #@0
    .prologue
    .line 141
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@7
    move-result v0

    #@8
    invoke-direct {p0, v0}, Landroid/filterfw/core/NativeFrame;->getNativeFloats(I)[F

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getInts()[I
    .registers 2

    #@0
    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@7
    move-result v0

    #@8
    invoke-direct {p0, v0}, Landroid/filterfw/core/NativeFrame;->getNativeInts(I)[I

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public getObjectValue()Ljava/lang/Object;
    .registers 7

    #@0
    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getBaseType()I

    #@7
    move-result v3

    #@8
    const/16 v4, 0x8

    #@a
    if-eq v3, v4, :cond_11

    #@c
    .line 74
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getData()Ljava/nio/ByteBuffer;

    #@f
    move-result-object v2

    #@10
    .line 107
    :goto_10
    return-object v2

    #@11
    .line 78
    :cond_11
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@18
    move-result-object v1

    #@19
    .line 79
    .local v1, structClass:Ljava/lang/Class;
    if-nez v1, :cond_23

    #@1b
    .line 80
    new-instance v3, Ljava/lang/RuntimeException;

    #@1d
    const-string v4, "Attempting to get object data from frame that does not specify a structure object class!"

    #@1f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@22
    throw v3

    #@23
    .line 85
    :cond_23
    const-class v3, Landroid/filterfw/core/NativeBuffer;

    #@25
    invoke-virtual {v3, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    #@28
    move-result v3

    #@29
    if-nez v3, :cond_33

    #@2b
    .line 86
    new-instance v3, Ljava/lang/RuntimeException;

    #@2d
    const-string v4, "NativeFrame object class must be a subclass of NativeBuffer!"

    #@2f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v3

    #@33
    .line 91
    :cond_33
    const/4 v2, 0x0

    #@34
    .line 93
    .local v2, structData:Landroid/filterfw/core/NativeBuffer;
    :try_start_34
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    #@37
    move-result-object v2

    #@38
    .end local v2           #structData:Landroid/filterfw/core/NativeBuffer;
    check-cast v2, Landroid/filterfw/core/NativeBuffer;
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_3a} :catch_48

    #@3a
    .line 100
    .restart local v2       #structData:Landroid/filterfw/core/NativeBuffer;
    invoke-direct {p0, v2}, Landroid/filterfw/core/NativeFrame;->getNativeBuffer(Landroid/filterfw/core/NativeBuffer;)Z

    #@3d
    move-result v3

    #@3e
    if-nez v3, :cond_68

    #@40
    .line 101
    new-instance v3, Ljava/lang/RuntimeException;

    #@42
    const-string v4, "Could not get the native structured data for frame!"

    #@44
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@47
    throw v3

    #@48
    .line 94
    .end local v2           #structData:Landroid/filterfw/core/NativeBuffer;
    :catch_48
    move-exception v0

    #@49
    .line 95
    .local v0, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    #@4b
    new-instance v4, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v5, "Could not instantiate new structure instance of type \'"

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    const-string v5, "\'!"

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v4

    #@64
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@67
    throw v3

    #@68
    .line 105
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v2       #structData:Landroid/filterfw/core/NativeBuffer;
    :cond_68
    invoke-virtual {v2, p0}, Landroid/filterfw/core/NativeBuffer;->attachToFrame(Landroid/filterfw/core/Frame;)V

    #@6b
    goto :goto_10
.end method

.method protected declared-synchronized hasNativeAllocation()Z
    .registers 3

    #@0
    .prologue
    .line 53
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/NativeFrame;->nativeFrameId:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    #@3
    const/4 v1, -0x1

    #@4
    if-eq v0, v1, :cond_9

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    monitor-exit p0

    #@8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_7

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method protected declared-synchronized releaseNativeAllocation()V
    .registers 2

    #@0
    .prologue
    .line 47
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Landroid/filterfw/core/NativeFrame;->nativeDeallocate()Z

    #@4
    .line 48
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/filterfw/core/NativeFrame;->nativeFrameId:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 49
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 47
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter "bitmap"

    #@0
    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->assertFrameMutable()V

    #@3
    .line 172
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@6
    move-result-object v3

    #@7
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getNumberOfDimensions()I

    #@a
    move-result v3

    #@b
    const/4 v4, 0x2

    #@c
    if-eq v3, v4, :cond_16

    #@e
    .line 173
    new-instance v3, Ljava/lang/RuntimeException;

    #@10
    const-string v4, "Attempting to set Bitmap for non 2-dimensional native frame!"

    #@12
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v3

    #@16
    .line 174
    :cond_16
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@1d
    move-result v3

    #@1e
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    #@21
    move-result v4

    #@22
    if-ne v3, v4, :cond_32

    #@24
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    #@2f
    move-result v4

    #@30
    if-eq v3, v4, :cond_3a

    #@32
    .line 176
    :cond_32
    new-instance v3, Ljava/lang/RuntimeException;

    #@34
    const-string v4, "Bitmap dimensions do not match native frame dimensions!"

    #@36
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@39
    throw v3

    #@3a
    .line 178
    :cond_3a
    invoke-static {p1}, Landroid/filterfw/core/NativeFrame;->convertBitmapToRGBA(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    #@3d
    move-result-object v2

    #@3e
    .line 179
    .local v2, rgbaBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getByteCount()I

    #@41
    move-result v1

    #@42
    .line 180
    .local v1, byteCount:I
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getBytesPerSample()I

    #@49
    move-result v0

    #@4a
    .line 181
    .local v0, bps:I
    invoke-direct {p0, v2, v1, v0}, Landroid/filterfw/core/NativeFrame;->setNativeBitmap(Landroid/graphics/Bitmap;II)Z

    #@4d
    move-result v3

    #@4e
    if-nez v3, :cond_58

    #@50
    .line 182
    new-instance v3, Ljava/lang/RuntimeException;

    #@52
    const-string v4, "Could not set native frame bitmap data!"

    #@54
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@57
    throw v3

    #@58
    .line 185
    :cond_58
    return-void
.end method

.method public setData(Ljava/nio/ByteBuffer;II)V
    .registers 8
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 148
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->assertFrameMutable()V

    #@3
    .line 149
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@6
    move-result-object v0

    #@7
    .line 150
    .local v0, bytes:[B
    add-int v1, p3, p2

    #@9
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    #@c
    move-result v2

    #@d
    if-le v1, v2, :cond_3e

    #@f
    .line 151
    new-instance v1, Ljava/lang/RuntimeException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Offset and length exceed buffer size in native setData: "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    add-int v3, p3, p2

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, " bytes given, but only "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, " bytes available!"

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v1

    #@3e
    .line 154
    :cond_3e
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@45
    move-result v1

    #@46
    if-eq v1, p3, :cond_79

    #@48
    .line 155
    new-instance v1, Ljava/lang/RuntimeException;

    #@4a
    new-instance v2, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v3, "Data size in setData does not match native frame size: Frame size is "

    #@51
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@5c
    move-result v3

    #@5d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    const-string v3, " bytes, but "

    #@63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v3, " bytes given!"

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v2

    #@75
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@78
    throw v1

    #@79
    .line 158
    :cond_79
    invoke-direct {p0, v0, p2, p3}, Landroid/filterfw/core/NativeFrame;->setNativeData([BII)Z

    #@7c
    move-result v1

    #@7d
    if-nez v1, :cond_87

    #@7f
    .line 159
    new-instance v1, Ljava/lang/RuntimeException;

    #@81
    const-string v2, "Could not set native frame data!"

    #@83
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@86
    throw v1

    #@87
    .line 161
    :cond_87
    return-void
.end method

.method public setDataFromFrame(Landroid/filterfw/core/Frame;)V
    .registers 5
    .parameter "frame"

    #@0
    .prologue
    .line 206
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@7
    move-result v0

    #@8
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@f
    move-result v1

    #@10
    if-ge v0, v1, :cond_52

    #@12
    .line 207
    new-instance v0, Ljava/lang/RuntimeException;

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "Attempting to assign frame of size "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@26
    move-result v2

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, " to "

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string/jumbo v2, "smaller native frame of size "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@3f
    move-result v2

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    const-string v2, "!"

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@51
    throw v0

    #@52
    .line 213
    :cond_52
    instance-of v0, p1, Landroid/filterfw/core/NativeFrame;

    #@54
    if-eqz v0, :cond_5c

    #@56
    .line 214
    check-cast p1, Landroid/filterfw/core/NativeFrame;

    #@58
    .end local p1
    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeFrame;->nativeCopyFromNative(Landroid/filterfw/core/NativeFrame;)Z

    #@5b
    .line 222
    :goto_5b
    return-void

    #@5c
    .line 215
    .restart local p1
    :cond_5c
    instance-of v0, p1, Landroid/filterfw/core/GLFrame;

    #@5e
    if-eqz v0, :cond_66

    #@60
    .line 216
    check-cast p1, Landroid/filterfw/core/GLFrame;

    #@62
    .end local p1
    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeFrame;->nativeCopyFromGL(Landroid/filterfw/core/GLFrame;)Z

    #@65
    goto :goto_5b

    #@66
    .line 217
    .restart local p1
    :cond_66
    instance-of v0, p1, Landroid/filterfw/core/SimpleFrame;

    #@68
    if-eqz v0, :cond_72

    #@6a
    .line 218
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {p0, v0}, Landroid/filterfw/core/NativeFrame;->setObjectValue(Ljava/lang/Object;)V

    #@71
    goto :goto_5b

    #@72
    .line 220
    :cond_72
    invoke-super {p0, p1}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@75
    goto :goto_5b
.end method

.method public setFloats([F)V
    .registers 6
    .parameter "floats"

    #@0
    .prologue
    .line 129
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->assertFrameMutable()V

    #@3
    .line 130
    array-length v0, p1

    #@4
    invoke-static {}, Landroid/filterfw/core/NativeFrame;->nativeFloatSize()I

    #@7
    move-result v1

    #@8
    mul-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@10
    move-result v1

    #@11
    if-le v0, v1, :cond_4a

    #@13
    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "NativeFrame cannot hold "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    array-length v2, p1

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " floats. (Can only hold "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@32
    move-result v2

    #@33
    invoke-static {}, Landroid/filterfw/core/NativeFrame;->nativeFloatSize()I

    #@36
    move-result v3

    #@37
    div-int/2addr v2, v3

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v2, " floats)."

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@49
    throw v0

    #@4a
    .line 134
    :cond_4a
    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeFrame;->setNativeFloats([F)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_58

    #@50
    .line 135
    new-instance v0, Ljava/lang/RuntimeException;

    #@52
    const-string v1, "Could not set int values for native frame!"

    #@54
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0

    #@58
    .line 137
    :cond_58
    return-void
.end method

.method public setInts([I)V
    .registers 6
    .parameter "ints"

    #@0
    .prologue
    .line 112
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->assertFrameMutable()V

    #@3
    .line 113
    array-length v0, p1

    #@4
    invoke-static {}, Landroid/filterfw/core/NativeFrame;->nativeIntSize()I

    #@7
    move-result v1

    #@8
    mul-int/2addr v0, v1

    #@9
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@10
    move-result v1

    #@11
    if-le v0, v1, :cond_4a

    #@13
    .line 114
    new-instance v0, Ljava/lang/RuntimeException;

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "NativeFrame cannot hold "

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    array-length v2, p1

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " integers. (Can only hold "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@32
    move-result v2

    #@33
    invoke-static {}, Landroid/filterfw/core/NativeFrame;->nativeIntSize()I

    #@36
    move-result v3

    #@37
    div-int/2addr v2, v3

    #@38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v1

    #@3c
    const-string v2, " integers)."

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@49
    throw v0

    #@4a
    .line 117
    :cond_4a
    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeFrame;->setNativeInts([I)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_58

    #@50
    .line 118
    new-instance v0, Ljava/lang/RuntimeException;

    #@52
    const-string v1, "Could not set int values for native frame!"

    #@54
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@57
    throw v0

    #@58
    .line 120
    :cond_58
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "NativeFrame id: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/filterfw/core/NativeFrame;->nativeFrameId:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, " ("

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    const-string v1, ") of size "

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {p0}, Landroid/filterfw/core/NativeFrame;->getCapacity()I

    #@28
    move-result v1

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    return-object v0
.end method
