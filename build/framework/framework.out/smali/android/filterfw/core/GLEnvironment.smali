.class public Landroid/filterfw/core/GLEnvironment;
.super Ljava/lang/Object;
.source "GLEnvironment.java"


# instance fields
.field private glEnvId:I

.field private mManageContext:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 148
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 149
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@6
    .line 37
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeAllocate()Z

    #@9
    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/filterfw/core/NativeAllocatorTag;)V
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 40
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@6
    .line 41
    return-void
.end method

.method public static isAnyContextActive()Z
    .registers 1

    #@0
    .prologue
    .line 78
    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->nativeIsAnyContextActive()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private native nativeActivate()Z
.end method

.method private native nativeActivateSurfaceId(I)Z
.end method

.method private native nativeAddSurface(Landroid/view/Surface;)I
.end method

.method private native nativeAddSurfaceFromMediaRecorder(Landroid/media/MediaRecorder;)I
.end method

.method private native nativeAddSurfaceWidthHeight(Landroid/view/Surface;II)I
.end method

.method private native nativeAllocate()Z
.end method

.method private native nativeDeactivate()Z
.end method

.method private native nativeDeallocate()Z
.end method

.method private native nativeDisconnectSurfaceMediaSource(Landroid/media/MediaRecorder;)Z
.end method

.method private native nativeInitWithCurrentContext()Z
.end method

.method private native nativeInitWithNewContext()Z
.end method

.method private native nativeIsActive()Z
.end method

.method private static native nativeIsAnyContextActive()Z
.end method

.method private native nativeIsContextActive()Z
.end method

.method private native nativeRemoveSurfaceId(I)Z
.end method

.method private native nativeSetSurfaceTimestamp(J)Z
.end method

.method private native nativeSwapBuffers()Z
.end method


# virtual methods
.method public activate()V
    .registers 3

    #@0
    .prologue
    .line 82
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_1b

    #@6
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@9
    move-result-object v0

    #@a
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 83
    const-string v0, "FilterFramework"

    #@16
    const-string v1, "Activating GL context in UI thread!"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 85
    :cond_1b
    iget-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@1d
    if-eqz v0, :cond_2d

    #@1f
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeActivate()Z

    #@22
    move-result v0

    #@23
    if-nez v0, :cond_2d

    #@25
    .line 86
    new-instance v0, Ljava/lang/RuntimeException;

    #@27
    const-string v1, "Could not activate GLEnvironment!"

    #@29
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 88
    :cond_2d
    return-void
.end method

.method public activateSurfaceWithId(I)V
    .registers 5
    .parameter "surfaceId"

    #@0
    .prologue
    .line 130
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLEnvironment;->nativeActivateSurfaceId(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not activate surface "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 133
    :cond_25
    return-void
.end method

.method public deactivate()V
    .registers 3

    #@0
    .prologue
    .line 91
    iget-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@2
    if-eqz v0, :cond_12

    #@4
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeDeactivate()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 92
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    const-string v1, "Could not deactivate GLEnvironment!"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 94
    :cond_12
    return-void
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/filterfw/core/GLEnvironment;->tearDown()V

    #@3
    .line 53
    return-void
.end method

.method public initWithCurrentContext()V
    .registers 3

    #@0
    .prologue
    .line 63
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@3
    .line 64
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeInitWithCurrentContext()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    .line 65
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "Could not initialize GLEnvironment with current context!"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 67
    :cond_11
    return-void
.end method

.method public initWithNewContext()V
    .registers 3

    #@0
    .prologue
    .line 56
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/filterfw/core/GLEnvironment;->mManageContext:Z

    #@3
    .line 57
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeInitWithNewContext()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    .line 58
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "Could not initialize GLEnvironment with new context!"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 60
    :cond_11
    return-void
.end method

.method public isActive()Z
    .registers 2

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeIsActive()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isContextActive()Z
    .registers 2

    #@0
    .prologue
    .line 74
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeIsContextActive()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public registerSurface(Landroid/view/Surface;)I
    .registers 6
    .parameter "surface"

    #@0
    .prologue
    .line 103
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLEnvironment;->nativeAddSurface(Landroid/view/Surface;)I

    #@3
    move-result v0

    #@4
    .line 104
    .local v0, result:I
    if-gez v0, :cond_25

    #@6
    .line 105
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Error registering surface "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "!"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 107
    :cond_25
    return v0
.end method

.method public registerSurfaceFromMediaRecorder(Landroid/media/MediaRecorder;)I
    .registers 6
    .parameter "mediaRecorder"

    #@0
    .prologue
    .line 121
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLEnvironment;->nativeAddSurfaceFromMediaRecorder(Landroid/media/MediaRecorder;)I

    #@3
    move-result v0

    #@4
    .line 122
    .local v0, result:I
    if-gez v0, :cond_25

    #@6
    .line 123
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Error registering surface from MediaRecorder"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, "!"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 126
    :cond_25
    return v0
.end method

.method public registerSurfaceTexture(Landroid/graphics/SurfaceTexture;II)I
    .registers 9
    .parameter "surfaceTexture"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 111
    new-instance v1, Landroid/view/Surface;

    #@2
    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    #@5
    .line 112
    .local v1, surface:Landroid/view/Surface;
    invoke-direct {p0, v1, p2, p3}, Landroid/filterfw/core/GLEnvironment;->nativeAddSurfaceWidthHeight(Landroid/view/Surface;II)I

    #@8
    move-result v0

    #@9
    .line 113
    .local v0, result:I
    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    #@c
    .line 114
    if-gez v0, :cond_2d

    #@e
    .line 115
    new-instance v2, Ljava/lang/RuntimeException;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Error registering surfaceTexture "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, "!"

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v2

    #@2d
    .line 117
    :cond_2d
    return v0
.end method

.method public setSurfaceTimestamp(J)V
    .registers 5
    .parameter "timestamp"

    #@0
    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/GLEnvironment;->nativeSetSurfaceTimestamp(J)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 143
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Could not set timestamp for current surface!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 145
    :cond_e
    return-void
.end method

.method public swapBuffers()V
    .registers 3

    #@0
    .prologue
    .line 97
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeSwapBuffers()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 98
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Error swapping EGL buffers!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 100
    :cond_e
    return-void
.end method

.method public declared-synchronized tearDown()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 44
    monitor-enter p0

    #@2
    :try_start_2
    iget v0, p0, Landroid/filterfw/core/GLEnvironment;->glEnvId:I

    #@4
    if-eq v0, v1, :cond_c

    #@6
    .line 45
    invoke-direct {p0}, Landroid/filterfw/core/GLEnvironment;->nativeDeallocate()Z

    #@9
    .line 46
    const/4 v0, -0x1

    #@a
    iput v0, p0, Landroid/filterfw/core/GLEnvironment;->glEnvId:I
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_e

    #@c
    .line 48
    :cond_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 44
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public unregisterSurfaceId(I)V
    .registers 5
    .parameter "surfaceId"

    #@0
    .prologue
    .line 136
    invoke-direct {p0, p1}, Landroid/filterfw/core/GLEnvironment;->nativeRemoveSurfaceId(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 137
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not unregister surface "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 139
    :cond_25
    return-void
.end method
