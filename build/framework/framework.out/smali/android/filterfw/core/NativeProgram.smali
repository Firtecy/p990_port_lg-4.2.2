.class public Landroid/filterfw/core/NativeProgram;
.super Landroid/filterfw/core/Program;
.source "NativeProgram.java"


# instance fields
.field private mHasGetValueFunction:Z

.field private mHasInitFunction:Z

.field private mHasResetFunction:Z

.field private mHasSetValueFunction:Z

.field private mHasTeardownFunction:Z

.field private mTornDown:Z

.field private nativeProgramId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 152
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 153
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .parameter "nativeLibName"
    .parameter "nativeFunctionPrefix"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 36
    invoke-direct {p0}, Landroid/filterfw/core/Program;-><init>()V

    #@4
    .line 29
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasInitFunction:Z

    #@6
    .line 30
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasTeardownFunction:Z

    #@8
    .line 31
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasSetValueFunction:Z

    #@a
    .line 32
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasGetValueFunction:Z

    #@c
    .line 33
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasResetFunction:Z

    #@e
    .line 34
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@10
    .line 38
    invoke-direct {p0}, Landroid/filterfw/core/NativeProgram;->allocate()Z

    #@13
    .line 41
    new-instance v7, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string/jumbo v8, "lib"

    #@1b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    const-string v8, ".so"

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 42
    .local v0, fullLibName:Ljava/lang/String;
    invoke-direct {p0, v0}, Landroid/filterfw/core/NativeProgram;->openNativeLibrary(Ljava/lang/String;)Z

    #@30
    move-result v7

    #@31
    if-nez v7, :cond_59

    #@33
    .line 43
    new-instance v7, Ljava/lang/RuntimeException;

    #@35
    new-instance v8, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v9, "Could not find native library named \'"

    #@3c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v8

    #@40
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v8

    #@44
    const-string v9, "\' "

    #@46
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v8

    #@4a
    const-string/jumbo v9, "required for native program!"

    #@4d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v8

    #@51
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@58
    throw v7

    #@59
    .line 48
    :cond_59
    new-instance v7, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v7

    #@62
    const-string v8, "_process"

    #@64
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v7

    #@68
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    .line 49
    .local v3, processFuncName:Ljava/lang/String;
    invoke-direct {p0, v3}, Landroid/filterfw/core/NativeProgram;->bindProcessFunction(Ljava/lang/String;)Z

    #@6f
    move-result v7

    #@70
    if-nez v7, :cond_a1

    #@72
    .line 50
    new-instance v7, Ljava/lang/RuntimeException;

    #@74
    new-instance v8, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v9, "Could not find native program function name "

    #@7b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v8

    #@7f
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    const-string v9, " in library "

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    const-string v9, "! "

    #@8f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v8

    #@93
    const-string v9, "This function is required!"

    #@95
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v8

    #@99
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v8

    #@9d
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a0
    throw v7

    #@a1
    .line 55
    :cond_a1
    new-instance v7, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v7

    #@aa
    const-string v8, "_init"

    #@ac
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v7

    #@b0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v2

    #@b4
    .line 56
    .local v2, initFuncName:Ljava/lang/String;
    invoke-direct {p0, v2}, Landroid/filterfw/core/NativeProgram;->bindInitFunction(Ljava/lang/String;)Z

    #@b7
    move-result v7

    #@b8
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasInitFunction:Z

    #@ba
    .line 58
    new-instance v7, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v7

    #@c3
    const-string v8, "_teardown"

    #@c5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v7

    #@c9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v6

    #@cd
    .line 59
    .local v6, teardownFuncName:Ljava/lang/String;
    invoke-direct {p0, v6}, Landroid/filterfw/core/NativeProgram;->bindTeardownFunction(Ljava/lang/String;)Z

    #@d0
    move-result v7

    #@d1
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasTeardownFunction:Z

    #@d3
    .line 61
    new-instance v7, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v7

    #@dc
    const-string v8, "_setvalue"

    #@de
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v7

    #@e2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v5

    #@e6
    .line 62
    .local v5, setValueFuncName:Ljava/lang/String;
    invoke-direct {p0, v5}, Landroid/filterfw/core/NativeProgram;->bindSetValueFunction(Ljava/lang/String;)Z

    #@e9
    move-result v7

    #@ea
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasSetValueFunction:Z

    #@ec
    .line 64
    new-instance v7, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v7

    #@f5
    const-string v8, "_getvalue"

    #@f7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v7

    #@fb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v1

    #@ff
    .line 65
    .local v1, getValueFuncName:Ljava/lang/String;
    invoke-direct {p0, v1}, Landroid/filterfw/core/NativeProgram;->bindGetValueFunction(Ljava/lang/String;)Z

    #@102
    move-result v7

    #@103
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasGetValueFunction:Z

    #@105
    .line 67
    new-instance v7, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v7

    #@10e
    const-string v8, "_reset"

    #@110
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v7

    #@114
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v4

    #@118
    .line 68
    .local v4, resetFuncName:Ljava/lang/String;
    invoke-direct {p0, v4}, Landroid/filterfw/core/NativeProgram;->bindResetFunction(Ljava/lang/String;)Z

    #@11b
    move-result v7

    #@11c
    iput-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasResetFunction:Z

    #@11e
    .line 71
    iget-boolean v7, p0, Landroid/filterfw/core/NativeProgram;->mHasInitFunction:Z

    #@120
    if-eqz v7, :cond_130

    #@122
    invoke-direct {p0}, Landroid/filterfw/core/NativeProgram;->callNativeInit()Z

    #@125
    move-result v7

    #@126
    if-nez v7, :cond_130

    #@128
    .line 72
    new-instance v7, Ljava/lang/RuntimeException;

    #@12a
    const-string v8, "Could not initialize NativeProgram!"

    #@12c
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@12f
    throw v7

    #@130
    .line 74
    :cond_130
    return-void
.end method

.method private native allocate()Z
.end method

.method private native bindGetValueFunction(Ljava/lang/String;)Z
.end method

.method private native bindInitFunction(Ljava/lang/String;)Z
.end method

.method private native bindProcessFunction(Ljava/lang/String;)Z
.end method

.method private native bindResetFunction(Ljava/lang/String;)Z
.end method

.method private native bindSetValueFunction(Ljava/lang/String;)Z
.end method

.method private native bindTeardownFunction(Ljava/lang/String;)Z
.end method

.method private native callNativeGetValue(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private native callNativeInit()Z
.end method

.method private native callNativeProcess([Landroid/filterfw/core/NativeFrame;Landroid/filterfw/core/NativeFrame;)Z
.end method

.method private native callNativeReset()Z
.end method

.method private native callNativeSetValue(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native callNativeTeardown()Z
.end method

.method private native deallocate()Z
.end method

.method private native nativeInit()Z
.end method

.method private native openNativeLibrary(Ljava/lang/String;)Z
.end method


# virtual methods
.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/filterfw/core/NativeProgram;->tearDown()V

    #@3
    .line 95
    return-void
.end method

.method public getHostValue(Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter "variableName"

    #@0
    .prologue
    .line 141
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 142
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "NativeProgram already torn down!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 144
    :cond_c
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mHasGetValueFunction:Z

    #@e
    if-nez v0, :cond_18

    #@10
    .line 145
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "Attempting to get native variable, but native code does not define native getvalue function!"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 148
    :cond_18
    invoke-direct {p0, p1}, Landroid/filterfw/core/NativeProgram;->callNativeGetValue(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    return-object v0
.end method

.method public process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V
    .registers 9
    .parameter "inputs"
    .parameter "output"

    #@0
    .prologue
    .line 99
    iget-boolean v3, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@2
    if-eqz v3, :cond_c

    #@4
    .line 100
    new-instance v3, Ljava/lang/RuntimeException;

    #@6
    const-string v4, "NativeProgram already torn down!"

    #@8
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v3

    #@c
    .line 102
    :cond_c
    array-length v3, p1

    #@d
    new-array v1, v3, [Landroid/filterfw/core/NativeFrame;

    #@f
    .line 103
    .local v1, nativeInputs:[Landroid/filterfw/core/NativeFrame;
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    array-length v3, p1

    #@11
    if-ge v0, v3, :cond_45

    #@13
    .line 104
    aget-object v3, p1, v0

    #@15
    if-eqz v3, :cond_1d

    #@17
    aget-object v3, p1, v0

    #@19
    instance-of v3, v3, Landroid/filterfw/core/NativeFrame;

    #@1b
    if-eqz v3, :cond_26

    #@1d
    .line 105
    :cond_1d
    aget-object v3, p1, v0

    #@1f
    check-cast v3, Landroid/filterfw/core/NativeFrame;

    #@21
    aput-object v3, v1, v0

    #@23
    .line 103
    add-int/lit8 v0, v0, 0x1

    #@25
    goto :goto_10

    #@26
    .line 107
    :cond_26
    new-instance v3, Ljava/lang/RuntimeException;

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "NativeProgram got non-native frame as input "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    const-string v5, "!"

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@44
    throw v3

    #@45
    .line 112
    :cond_45
    const/4 v2, 0x0

    #@46
    .line 113
    .local v2, nativeOutput:Landroid/filterfw/core/NativeFrame;
    if-eqz p2, :cond_4c

    #@48
    instance-of v3, p2, Landroid/filterfw/core/NativeFrame;

    #@4a
    if-eqz v3, :cond_5d

    #@4c
    :cond_4c
    move-object v2, p2

    #@4d
    .line 114
    check-cast v2, Landroid/filterfw/core/NativeFrame;

    #@4f
    .line 120
    invoke-direct {p0, v1, v2}, Landroid/filterfw/core/NativeProgram;->callNativeProcess([Landroid/filterfw/core/NativeFrame;Landroid/filterfw/core/NativeFrame;)Z

    #@52
    move-result v3

    #@53
    if-nez v3, :cond_65

    #@55
    .line 121
    new-instance v3, Ljava/lang/RuntimeException;

    #@57
    const-string v4, "Calling native process() caused error!"

    #@59
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v3

    #@5d
    .line 116
    :cond_5d
    new-instance v3, Ljava/lang/RuntimeException;

    #@5f
    const-string v4, "NativeProgram got non-native output frame!"

    #@61
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@64
    throw v3

    #@65
    .line 123
    :cond_65
    return-void
.end method

.method public reset()V
    .registers 3

    #@0
    .prologue
    .line 87
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mHasResetFunction:Z

    #@2
    if-eqz v0, :cond_12

    #@4
    invoke-direct {p0}, Landroid/filterfw/core/NativeProgram;->callNativeReset()Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 88
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    const-string v1, "Could not reset NativeProgram!"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 90
    :cond_12
    return-void
.end method

.method public setHostValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter "variableName"
    .parameter "value"

    #@0
    .prologue
    .line 127
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 128
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "NativeProgram already torn down!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 130
    :cond_c
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mHasSetValueFunction:Z

    #@e
    if-nez v0, :cond_18

    #@10
    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "Attempting to set native variable, but native code does not define native setvalue function!"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 134
    :cond_18
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-direct {p0, p1, v0}, Landroid/filterfw/core/NativeProgram;->callNativeSetValue(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f
    move-result v0

    #@20
    if-nez v0, :cond_41

    #@22
    .line 135
    new-instance v0, Ljava/lang/RuntimeException;

    #@24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "Error setting native value for variable \'"

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, "\'!"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@40
    throw v0

    #@41
    .line 137
    :cond_41
    return-void
.end method

.method public tearDown()V
    .registers 3

    #@0
    .prologue
    .line 77
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 83
    :goto_4
    return-void

    #@5
    .line 78
    :cond_5
    iget-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mHasTeardownFunction:Z

    #@7
    if-eqz v0, :cond_17

    #@9
    invoke-direct {p0}, Landroid/filterfw/core/NativeProgram;->callNativeTeardown()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    #@11
    const-string v1, "Could not tear down NativeProgram!"

    #@13
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@16
    throw v0

    #@17
    .line 81
    :cond_17
    invoke-direct {p0}, Landroid/filterfw/core/NativeProgram;->deallocate()Z

    #@1a
    .line 82
    const/4 v0, 0x1

    #@1b
    iput-boolean v0, p0, Landroid/filterfw/core/NativeProgram;->mTornDown:Z

    #@1d
    goto :goto_4
.end method
