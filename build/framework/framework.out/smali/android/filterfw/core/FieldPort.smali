.class public Landroid/filterfw/core/FieldPort;
.super Landroid/filterfw/core/InputPort;
.source "FieldPort.java"


# instance fields
.field protected mField:Ljava/lang/reflect/Field;

.field protected mHasFrame:Z

.field protected mValue:Ljava/lang/Object;

.field protected mValueWaiting:Z


# direct methods
.method public constructor <init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V
    .registers 6
    .parameter "filter"
    .parameter "name"
    .parameter "field"
    .parameter "hasDefault"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/InputPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@3
    .line 29
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z

    #@6
    .line 34
    iput-object p3, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@8
    .line 35
    iput-boolean p4, p0, Landroid/filterfw/core/FieldPort;->mHasFrame:Z

    #@a
    .line 36
    return-void
.end method


# virtual methods
.method public declared-synchronized acceptsFrame()Z
    .registers 2

    #@0
    .prologue
    .line 89
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    #@3
    if-nez v0, :cond_8

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_6

    #@a
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method public clear()V
    .registers 1

    #@0
    .prologue
    .line 40
    return-void
.end method

.method public getTarget()Ljava/lang/Object;
    .registers 4

    #@0
    .prologue
    .line 55
    :try_start_0
    iget-object v1, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@2
    iget-object v2, p0, Landroid/filterfw/core/FilterPort;->mFilter:Landroid/filterfw/core/Filter;

    #@4
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 57
    :goto_8
    return-object v1

    #@9
    .line 56
    :catch_9
    move-exception v0

    #@a
    .line 57
    .local v0, e:Ljava/lang/IllegalAccessException;
    const/4 v1, 0x0

    #@b
    goto :goto_8
.end method

.method public declared-synchronized hasFrame()Z
    .registers 2

    #@0
    .prologue
    .line 84
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/filterfw/core/FieldPort;->mHasFrame:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized pullFrame()Landroid/filterfw/core/Frame;
    .registers 4

    #@0
    .prologue
    .line 79
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "Cannot pull frame on "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, "!"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_20

    #@20
    :catchall_20
    move-exception v0

    #@21
    monitor-exit p0

    #@22
    throw v0
.end method

.method public pushFrame(Landroid/filterfw/core/Frame;)V
    .registers 3
    .parameter "frame"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/FieldPort;->setFieldFrame(Landroid/filterfw/core/Frame;Z)V

    #@4
    .line 45
    return-void
.end method

.method protected declared-synchronized setFieldFrame(Landroid/filterfw/core/Frame;Z)V
    .registers 5
    .parameter "frame"
    .parameter "isAssignment"

    #@0
    .prologue
    .line 98
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/filterfw/core/FieldPort;->assertPortIsOpen()V

    #@4
    .line 99
    invoke-virtual {p0, p1, p2}, Landroid/filterfw/core/FieldPort;->checkFrameType(Landroid/filterfw/core/Frame;Z)V

    #@7
    .line 102
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    .line 103
    .local v0, value:Ljava/lang/Object;
    if-nez v0, :cond_11

    #@d
    iget-object v1, p0, Landroid/filterfw/core/FieldPort;->mValue:Ljava/lang/Object;

    #@f
    if-nez v1, :cond_19

    #@11
    :cond_11
    iget-object v1, p0, Landroid/filterfw/core/FieldPort;->mValue:Ljava/lang/Object;

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_1e

    #@19
    .line 104
    :cond_19
    iput-object v0, p0, Landroid/filterfw/core/FieldPort;->mValue:Ljava/lang/Object;

    #@1b
    .line 105
    const/4 v1, 0x1

    #@1c
    iput-boolean v1, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z

    #@1e
    .line 109
    :cond_1e
    const/4 v1, 0x1

    #@1f
    iput-boolean v1, p0, Landroid/filterfw/core/FieldPort;->mHasFrame:Z
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    #@21
    .line 110
    monitor-exit p0

    #@22
    return-void

    #@23
    .line 98
    .end local v0           #value:Ljava/lang/Object;
    :catchall_23
    move-exception v1

    #@24
    monitor-exit p0

    #@25
    throw v1
.end method

.method public setFrame(Landroid/filterfw/core/Frame;)V
    .registers 3
    .parameter "frame"

    #@0
    .prologue
    .line 49
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/FieldPort;->setFieldFrame(Landroid/filterfw/core/Frame;Z)V

    #@4
    .line 50
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "field "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-super {p0}, Landroid/filterfw/core/InputPort;->toString()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    return-object v0
.end method

.method public declared-synchronized transfer(Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 63
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_42

    #@3
    if-eqz v1, :cond_1a

    #@5
    .line 65
    :try_start_5
    iget-object v1, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@7
    iget-object v2, p0, Landroid/filterfw/core/FilterPort;->mFilter:Landroid/filterfw/core/Filter;

    #@9
    iget-object v3, p0, Landroid/filterfw/core/FieldPort;->mValue:Ljava/lang/Object;

    #@b
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_42
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_e} :catch_1c

    #@e
    .line 70
    const/4 v1, 0x0

    #@f
    :try_start_f
    iput-boolean v1, p0, Landroid/filterfw/core/FieldPort;->mValueWaiting:Z

    #@11
    .line 71
    if-eqz p1, :cond_1a

    #@13
    .line 72
    iget-object v1, p0, Landroid/filterfw/core/FilterPort;->mFilter:Landroid/filterfw/core/Filter;

    #@15
    iget-object v2, p0, Landroid/filterfw/core/FilterPort;->mName:Ljava/lang/String;

    #@17
    invoke-virtual {v1, v2, p1}, Landroid/filterfw/core/Filter;->notifyFieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    :try_end_1a
    .catchall {:try_start_f .. :try_end_1a} :catchall_42

    #@1a
    .line 75
    :cond_1a
    monitor-exit p0

    #@1b
    return-void

    #@1c
    .line 66
    :catch_1c
    move-exception v0

    #@1d
    .line 67
    .local v0, e:Ljava/lang/IllegalAccessException;
    :try_start_1d
    new-instance v1, Ljava/lang/RuntimeException;

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Access to field \'"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Landroid/filterfw/core/FieldPort;->mField:Ljava/lang/reflect/Field;

    #@2c
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, "\' was denied!"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@41
    throw v1
    :try_end_42
    .catchall {:try_start_1d .. :try_end_42} :catchall_42

    #@42
    .line 63
    .end local v0           #e:Ljava/lang/IllegalAccessException;
    :catchall_42
    move-exception v1

    #@43
    monitor-exit p0

    #@44
    throw v1
.end method
