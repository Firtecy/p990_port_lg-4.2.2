.class public abstract Landroid/filterfw/core/Frame;
.super Ljava/lang/Object;
.source "Frame.java"


# static fields
.field public static final NO_BINDING:I = 0x0

.field public static final TIMESTAMP_NOT_SET:J = -0x2L

.field public static final TIMESTAMP_UNKNOWN:J = -0x1L


# instance fields
.field private mBindingId:J

.field private mBindingType:I

.field private mFormat:Landroid/filterfw/core/FrameFormat;

.field private mFrameManager:Landroid/filterfw/core/FrameManager;

.field private mReadOnly:Z

.field private mRefCount:I

.field private mReusable:Z

.field private mTimestamp:J


# direct methods
.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 5
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 39
    iput-boolean v1, p0, Landroid/filterfw/core/Frame;->mReadOnly:Z

    #@6
    .line 40
    iput-boolean v1, p0, Landroid/filterfw/core/Frame;->mReusable:Z

    #@8
    .line 41
    const/4 v0, 0x1

    #@9
    iput v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@b
    .line 42
    iput v1, p0, Landroid/filterfw/core/Frame;->mBindingType:I

    #@d
    .line 43
    const-wide/16 v0, 0x0

    #@f
    iput-wide v0, p0, Landroid/filterfw/core/Frame;->mBindingId:J

    #@11
    .line 44
    const-wide/16 v0, -0x2

    #@13
    iput-wide v0, p0, Landroid/filterfw/core/Frame;->mTimestamp:J

    #@15
    .line 47
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/filterfw/core/Frame;->mFormat:Landroid/filterfw/core/FrameFormat;

    #@1b
    .line 48
    iput-object p2, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@1d
    .line 49
    return-void
.end method

.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;IJ)V
    .registers 8
    .parameter "format"
    .parameter "frameManager"
    .parameter "bindingType"
    .parameter "bindingId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 39
    iput-boolean v1, p0, Landroid/filterfw/core/Frame;->mReadOnly:Z

    #@6
    .line 40
    iput-boolean v1, p0, Landroid/filterfw/core/Frame;->mReusable:Z

    #@8
    .line 41
    const/4 v0, 0x1

    #@9
    iput v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@b
    .line 42
    iput v1, p0, Landroid/filterfw/core/Frame;->mBindingType:I

    #@d
    .line 43
    const-wide/16 v0, 0x0

    #@f
    iput-wide v0, p0, Landroid/filterfw/core/Frame;->mBindingId:J

    #@11
    .line 44
    const-wide/16 v0, -0x2

    #@13
    iput-wide v0, p0, Landroid/filterfw/core/Frame;->mTimestamp:J

    #@15
    .line 52
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/filterfw/core/Frame;->mFormat:Landroid/filterfw/core/FrameFormat;

    #@1b
    .line 53
    iput-object p2, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@1d
    .line 54
    iput p3, p0, Landroid/filterfw/core/Frame;->mBindingType:I

    #@1f
    .line 55
    iput-wide p4, p0, Landroid/filterfw/core/Frame;->mBindingId:J

    #@21
    .line 56
    return-void
.end method

.method protected static convertBitmapToRGBA(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    .line 182
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@3
    move-result-object v1

    #@4
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@6
    if-ne v1, v2, :cond_9

    #@8
    .line 191
    .end local p0
    :goto_8
    return-object p0

    #@9
    .line 185
    .restart local p0
    :cond_9
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@b
    const/4 v2, 0x0

    #@c
    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@f
    move-result-object v0

    #@10
    .line 186
    .local v0, result:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1a

    #@12
    .line 187
    new-instance v1, Ljava/lang/RuntimeException;

    #@14
    const-string v2, "Error converting bitmap to RGBA!"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 188
    :cond_1a
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    #@1d
    move-result v1

    #@1e
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@21
    move-result v2

    #@22
    mul-int/lit8 v2, v2, 0x4

    #@24
    if-eq v1, v2, :cond_2e

    #@26
    .line 189
    new-instance v1, Ljava/lang/RuntimeException;

    #@28
    const-string v2, "Unsupported row byte count in bitmap!"

    #@2a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v1

    #@2e
    :cond_2e
    move-object p0, v0

    #@2f
    .line 191
    goto :goto_8
.end method


# virtual methods
.method protected assertFrameMutable()V
    .registers 3

    #@0
    .prologue
    .line 163
    invoke-virtual {p0}, Landroid/filterfw/core/Frame;->isReadOnly()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 164
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Attempting to modify read-only frame!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 166
    :cond_e
    return-void
.end method

.method final decRefCount()I
    .registers 2

    #@0
    .prologue
    .line 224
    iget v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@6
    .line 225
    iget v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@8
    return v0
.end method

.method public getBindingId()J
    .registers 3

    #@0
    .prologue
    .line 75
    iget-wide v0, p0, Landroid/filterfw/core/Frame;->mBindingId:J

    #@2
    return-wide v0
.end method

.method public getBindingType()I
    .registers 2

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/filterfw/core/Frame;->mBindingType:I

    #@2
    return v0
.end method

.method public abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method public getCapacity()I
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public abstract getData()Ljava/nio/ByteBuffer;
.end method

.method public abstract getFloats()[F
.end method

.method public getFormat()Landroid/filterfw/core/FrameFormat;
    .registers 2

    #@0
    .prologue
    .line 59
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFormat:Landroid/filterfw/core/FrameFormat;

    #@2
    return-object v0
.end method

.method public getFrameManager()Landroid/filterfw/core/FrameManager;
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@2
    return-object v0
.end method

.method public abstract getInts()[I
.end method

.method public abstract getObjectValue()Ljava/lang/Object;
.end method

.method public getRefCount()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@2
    return v0
.end method

.method public getTimestamp()J
    .registers 3

    #@0
    .prologue
    .line 127
    iget-wide v0, p0, Landroid/filterfw/core/Frame;->mTimestamp:J

    #@2
    return-wide v0
.end method

.method protected abstract hasNativeAllocation()Z
.end method

.method final incRefCount()I
    .registers 2

    #@0
    .prologue
    .line 219
    iget v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@6
    .line 220
    iget v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@8
    return v0
.end method

.method public isReadOnly()Z
    .registers 2

    #@0
    .prologue
    .line 67
    iget-boolean v0, p0, Landroid/filterfw/core/Frame;->mReadOnly:Z

    #@2
    return v0
.end method

.method final isReusable()Z
    .registers 2

    #@0
    .prologue
    .line 229
    iget-boolean v0, p0, Landroid/filterfw/core/Frame;->mReusable:Z

    #@2
    return v0
.end method

.method final markReadOnly()V
    .registers 2

    #@0
    .prologue
    .line 233
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/filterfw/core/Frame;->mReadOnly:Z

    #@3
    .line 234
    return-void
.end method

.method protected onFrameFetch()V
    .registers 1

    #@0
    .prologue
    .line 211
    return-void
.end method

.method protected onFrameStore()V
    .registers 1

    #@0
    .prologue
    .line 205
    return-void
.end method

.method public release()Landroid/filterfw/core/Frame;
    .registers 2

    #@0
    .prologue
    .line 143
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 144
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@6
    invoke-virtual {v0, p0}, Landroid/filterfw/core/FrameManager;->releaseFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;

    #@9
    move-result-object p0

    #@a
    .line 146
    .end local p0
    :cond_a
    return-object p0
.end method

.method protected abstract releaseNativeAllocation()V
.end method

.method protected requestResize([I)Z
    .registers 3
    .parameter "newDimensions"

    #@0
    .prologue
    .line 135
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected reset(Landroid/filterfw/core/FrameFormat;)V
    .registers 3
    .parameter "newFormat"

    #@0
    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/filterfw/core/Frame;->mFormat:Landroid/filterfw/core/FrameFormat;

    #@6
    .line 197
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Landroid/filterfw/core/Frame;->mReadOnly:Z

    #@9
    .line 198
    const/4 v0, 0x1

    #@a
    iput v0, p0, Landroid/filterfw/core/Frame;->mRefCount:I

    #@c
    .line 199
    return-void
.end method

.method public retain()Landroid/filterfw/core/Frame;
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 152
    iget-object v0, p0, Landroid/filterfw/core/Frame;->mFrameManager:Landroid/filterfw/core/FrameManager;

    #@6
    invoke-virtual {v0, p0}, Landroid/filterfw/core/FrameManager;->retainFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;

    #@9
    move-result-object p0

    #@a
    .line 154
    .end local p0
    :cond_a
    return-object p0
.end method

.method public abstract setBitmap(Landroid/graphics/Bitmap;)V
.end method

.method public setData(Ljava/nio/ByteBuffer;)V
    .registers 4
    .parameter "buffer"

    #@0
    .prologue
    .line 109
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, p1, v0, v1}, Landroid/filterfw/core/Frame;->setData(Ljava/nio/ByteBuffer;II)V

    #@8
    .line 110
    return-void
.end method

.method public abstract setData(Ljava/nio/ByteBuffer;II)V
.end method

.method public setData([BII)V
    .registers 5
    .parameter "bytes"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 113
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/filterfw/core/Frame;->setData(Ljava/nio/ByteBuffer;)V

    #@7
    .line 114
    return-void
.end method

.method public setDataFromFrame(Landroid/filterfw/core/Frame;)V
    .registers 3
    .parameter "frame"

    #@0
    .prologue
    .line 131
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->getData()Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/filterfw/core/Frame;->setData(Ljava/nio/ByteBuffer;)V

    #@7
    .line 132
    return-void
.end method

.method public abstract setFloats([F)V
.end method

.method protected setFormat(Landroid/filterfw/core/FrameFormat;)V
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->mutableCopy()Landroid/filterfw/core/MutableFrameFormat;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/filterfw/core/Frame;->mFormat:Landroid/filterfw/core/FrameFormat;

    #@6
    .line 174
    return-void
.end method

.method protected setGenericObjectValue(Ljava/lang/Object;)V
    .registers 5
    .parameter "value"

    #@0
    .prologue
    .line 177
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Cannot set object value of unsupported type: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v0
.end method

.method public abstract setInts([I)V
.end method

.method public setObjectValue(Ljava/lang/Object;)V
    .registers 3
    .parameter "object"

    #@0
    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/filterfw/core/Frame;->assertFrameMutable()V

    #@3
    .line 83
    instance-of v0, p1, [I

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 84
    check-cast p1, [I

    #@9
    .end local p1
    check-cast p1, [I

    #@b
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Frame;->setInts([I)V

    #@e
    .line 94
    :goto_e
    return-void

    #@f
    .line 85
    .restart local p1
    :cond_f
    instance-of v0, p1, [F

    #@11
    if-eqz v0, :cond_1b

    #@13
    .line 86
    check-cast p1, [F

    #@15
    .end local p1
    check-cast p1, [F

    #@17
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Frame;->setFloats([F)V

    #@1a
    goto :goto_e

    #@1b
    .line 87
    .restart local p1
    :cond_1b
    instance-of v0, p1, Ljava/nio/ByteBuffer;

    #@1d
    if-eqz v0, :cond_25

    #@1f
    .line 88
    check-cast p1, Ljava/nio/ByteBuffer;

    #@21
    .end local p1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Frame;->setData(Ljava/nio/ByteBuffer;)V

    #@24
    goto :goto_e

    #@25
    .line 89
    .restart local p1
    :cond_25
    instance-of v0, p1, Landroid/graphics/Bitmap;

    #@27
    if-eqz v0, :cond_2f

    #@29
    .line 90
    check-cast p1, Landroid/graphics/Bitmap;

    #@2b
    .end local p1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Frame;->setBitmap(Landroid/graphics/Bitmap;)V

    #@2e
    goto :goto_e

    #@2f
    .line 92
    .restart local p1
    :cond_2f
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Frame;->setGenericObjectValue(Ljava/lang/Object;)V

    #@32
    goto :goto_e
.end method

.method protected setReusable(Z)V
    .registers 2
    .parameter "reusable"

    #@0
    .prologue
    .line 169
    iput-boolean p1, p0, Landroid/filterfw/core/Frame;->mReusable:Z

    #@2
    .line 170
    return-void
.end method

.method public setTimestamp(J)V
    .registers 3
    .parameter "timestamp"

    #@0
    .prologue
    .line 123
    iput-wide p1, p0, Landroid/filterfw/core/Frame;->mTimestamp:J

    #@2
    .line 124
    return-void
.end method
