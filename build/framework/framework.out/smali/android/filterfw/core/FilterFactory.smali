.class public Landroid/filterfw/core/FilterFactory;
.super Ljava/lang/Object;
.source "FilterFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FilterFactory"

.field private static mClassLoaderGuard:Ljava/lang/Object;

.field private static mCurrentClassLoader:Ljava/lang/ClassLoader;

.field private static mLibraries:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mLogVerbose:Z

.field private static mSharedFactory:Landroid/filterfw/core/FilterFactory;


# instance fields
.field private mPackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 43
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Landroid/filterfw/core/FilterFactory;->mCurrentClassLoader:Ljava/lang/ClassLoader;

    #@a
    .line 44
    new-instance v0, Ljava/util/HashSet;

    #@c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@f
    sput-object v0, Landroid/filterfw/core/FilterFactory;->mLibraries:Ljava/util/HashSet;

    #@11
    .line 45
    new-instance v0, Ljava/lang/Object;

    #@13
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@16
    sput-object v0, Landroid/filterfw/core/FilterFactory;->mClassLoaderGuard:Ljava/lang/Object;

    #@18
    .line 49
    const-string v0, "FilterFactory"

    #@1a
    const/4 v1, 0x2

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@1e
    move-result v0

    #@1f
    sput-boolean v0, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@21
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 36
    new-instance v0, Ljava/util/HashSet;

    #@5
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@8
    iput-object v0, p0, Landroid/filterfw/core/FilterFactory;->mPackages:Ljava/util/HashSet;

    #@a
    return-void
.end method

.method public static addFilterLibrary(Ljava/lang/String;)V
    .registers 4
    .parameter "libraryPath"

    #@0
    .prologue
    .line 65
    sget-boolean v0, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    const-string v0, "FilterFactory"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Adding filter library "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 66
    :cond_1c
    sget-object v1, Landroid/filterfw/core/FilterFactory;->mClassLoaderGuard:Ljava/lang/Object;

    #@1e
    monitor-enter v1

    #@1f
    .line 67
    :try_start_1f
    sget-object v0, Landroid/filterfw/core/FilterFactory;->mLibraries:Ljava/util/HashSet;

    #@21
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    #@24
    move-result v0

    #@25
    if-eqz v0, :cond_34

    #@27
    .line 68
    sget-boolean v0, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@29
    if-eqz v0, :cond_32

    #@2b
    const-string v0, "FilterFactory"

    #@2d
    const-string v2, "Library already added"

    #@2f
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 69
    :cond_32
    monitor-exit v1

    #@33
    .line 75
    :goto_33
    return-void

    #@34
    .line 71
    :cond_34
    sget-object v0, Landroid/filterfw/core/FilterFactory;->mLibraries:Ljava/util/HashSet;

    #@36
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@39
    .line 73
    new-instance v0, Ldalvik/system/PathClassLoader;

    #@3b
    sget-object v2, Landroid/filterfw/core/FilterFactory;->mCurrentClassLoader:Ljava/lang/ClassLoader;

    #@3d
    invoke-direct {v0, p0, v2}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    #@40
    sput-object v0, Landroid/filterfw/core/FilterFactory;->mCurrentClassLoader:Ljava/lang/ClassLoader;

    #@42
    .line 74
    monitor-exit v1

    #@43
    goto :goto_33

    #@44
    :catchall_44
    move-exception v0

    #@45
    monitor-exit v1
    :try_end_46
    .catchall {:try_start_1f .. :try_end_46} :catchall_44

    #@46
    throw v0
.end method

.method public static sharedFactory()Landroid/filterfw/core/FilterFactory;
    .registers 1

    #@0
    .prologue
    .line 52
    sget-object v0, Landroid/filterfw/core/FilterFactory;->mSharedFactory:Landroid/filterfw/core/FilterFactory;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 53
    new-instance v0, Landroid/filterfw/core/FilterFactory;

    #@6
    invoke-direct {v0}, Landroid/filterfw/core/FilterFactory;-><init>()V

    #@9
    sput-object v0, Landroid/filterfw/core/FilterFactory;->mSharedFactory:Landroid/filterfw/core/FilterFactory;

    #@b
    .line 55
    :cond_b
    sget-object v0, Landroid/filterfw/core/FilterFactory;->mSharedFactory:Landroid/filterfw/core/FilterFactory;

    #@d
    return-object v0
.end method


# virtual methods
.method public addPackage(Ljava/lang/String;)V
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 78
    sget-boolean v0, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    const-string v0, "FilterFactory"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Adding package "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 85
    :cond_1c
    iget-object v0, p0, Landroid/filterfw/core/FilterFactory;->mPackages:Ljava/util/HashSet;

    #@1e
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@21
    .line 86
    return-void
.end method

.method public createFilterByClass(Ljava/lang/Class;Ljava/lang/String;)Landroid/filterfw/core/Filter;
    .registers 10
    .parameter "filterClass"
    .parameter "filterName"

    #@0
    .prologue
    .line 116
    :try_start_0
    const-class v4, Landroid/filterfw/core/Filter;

    #@2
    invoke-virtual {p1, v4}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_5} :catch_42

    #@5
    .line 123
    const/4 v3, 0x0

    #@6
    .line 125
    .local v3, filterConstructor:Ljava/lang/reflect/Constructor;
    const/4 v4, 0x1

    #@7
    :try_start_7
    new-array v4, v4, [Ljava/lang/Class;

    #@9
    const/4 v5, 0x0

    #@a
    const-class v6, Ljava/lang/String;

    #@c
    aput-object v6, v4, v5

    #@e
    invoke-virtual {p1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_11
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_11} :catch_62

    #@11
    move-result-object v3

    #@12
    .line 132
    const/4 v2, 0x0

    #@13
    .line 134
    .local v2, filter:Landroid/filterfw/core/Filter;
    const/4 v4, 0x1

    #@14
    :try_start_14
    new-array v4, v4, [Ljava/lang/Object;

    #@16
    const/4 v5, 0x0

    #@17
    aput-object p2, v4, v5

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v4

    #@1d
    move-object v0, v4

    #@1e
    check-cast v0, Landroid/filterfw/core/Filter;

    #@20
    move-object v2, v0
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_21} :catch_83

    #@21
    .line 139
    :goto_21
    if-nez v2, :cond_82

    #@23
    .line 140
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@25
    new-instance v5, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v6, "Could not construct the filter \'"

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    const-string v6, "\'!"

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@41
    throw v4

    #@42
    .line 117
    .end local v2           #filter:Landroid/filterfw/core/Filter;
    .end local v3           #filterConstructor:Ljava/lang/reflect/Constructor;
    :catch_42
    move-exception v1

    #@43
    .line 118
    .local v1, e:Ljava/lang/ClassCastException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@45
    new-instance v5, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v6, "Attempting to allocate class \'"

    #@4c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    const-string v6, "\' which is not a subclass of Filter!"

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@61
    throw v4

    #@62
    .line 126
    .end local v1           #e:Ljava/lang/ClassCastException;
    .restart local v3       #filterConstructor:Ljava/lang/reflect/Constructor;
    :catch_62
    move-exception v1

    #@63
    .line 127
    .local v1, e:Ljava/lang/NoSuchMethodException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@65
    new-instance v5, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v6, "The filter class \'"

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    const-string v6, "\' does not have a constructor of the form <init>(String name)!"

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v5

    #@7e
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@81
    throw v4

    #@82
    .line 143
    .end local v1           #e:Ljava/lang/NoSuchMethodException;
    .restart local v2       #filter:Landroid/filterfw/core/Filter;
    :cond_82
    return-object v2

    #@83
    .line 135
    :catch_83
    move-exception v4

    #@84
    goto :goto_21
.end method

.method public createFilterByClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/filterfw/core/Filter;
    .registers 11
    .parameter "className"
    .parameter "filterName"

    #@0
    .prologue
    .line 89
    sget-boolean v4, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@2
    if-eqz v4, :cond_1c

    #@4
    const-string v4, "FilterFactory"

    #@6
    new-instance v5, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v6, "Looking up class "

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 90
    :cond_1c
    const/4 v1, 0x0

    #@1d
    .line 93
    .local v1, filterClass:Ljava/lang/Class;
    iget-object v4, p0, Landroid/filterfw/core/FilterFactory;->mPackages:Ljava/util/HashSet;

    #@1f
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v2

    #@23
    .local v2, i$:Ljava/util/Iterator;
    :cond_23
    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v4

    #@27
    if-eqz v4, :cond_78

    #@29
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    check-cast v3, Ljava/lang/String;

    #@2f
    .line 95
    .local v3, packageName:Ljava/lang/String;
    :try_start_2f
    sget-boolean v4, Landroid/filterfw/core/FilterFactory;->mLogVerbose:Z

    #@31
    if-eqz v4, :cond_55

    #@33
    const-string v4, "FilterFactory"

    #@35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, "Trying "

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, "."

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 96
    :cond_55
    sget-object v5, Landroid/filterfw/core/FilterFactory;->mClassLoaderGuard:Ljava/lang/Object;

    #@57
    monitor-enter v5
    :try_end_58
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2f .. :try_end_58} :catch_9c

    #@58
    .line 97
    :try_start_58
    sget-object v4, Landroid/filterfw/core/FilterFactory;->mCurrentClassLoader:Ljava/lang/ClassLoader;

    #@5a
    new-instance v6, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    const-string v7, "."

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v4, v6}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    #@74
    move-result-object v1

    #@75
    .line 98
    monitor-exit v5
    :try_end_76
    .catchall {:try_start_58 .. :try_end_76} :catchall_99

    #@76
    .line 103
    if-eqz v1, :cond_23

    #@78
    .line 107
    .end local v3           #packageName:Ljava/lang/String;
    :cond_78
    if-nez v1, :cond_9e

    #@7a
    .line 108
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "Unknown filter class \'"

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, "\'!"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v5

    #@95
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@98
    throw v4

    #@99
    .line 98
    .restart local v3       #packageName:Ljava/lang/String;
    :catchall_99
    move-exception v4

    #@9a
    :try_start_9a
    monitor-exit v5
    :try_end_9b
    .catchall {:try_start_9a .. :try_end_9b} :catchall_99

    #@9b
    :try_start_9b
    throw v4
    :try_end_9c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9b .. :try_end_9c} :catch_9c

    #@9c
    .line 99
    :catch_9c
    move-exception v0

    #@9d
    .line 100
    .local v0, e:Ljava/lang/ClassNotFoundException;
    goto :goto_23

    #@9e
    .line 110
    .end local v0           #e:Ljava/lang/ClassNotFoundException;
    .end local v3           #packageName:Ljava/lang/String;
    :cond_9e
    invoke-virtual {p0, v1, p2}, Landroid/filterfw/core/FilterFactory;->createFilterByClass(Ljava/lang/Class;Ljava/lang/String;)Landroid/filterfw/core/Filter;

    #@a1
    move-result-object v4

    #@a2
    return-object v4
.end method
