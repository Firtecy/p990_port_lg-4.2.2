.class Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;
.super Ljava/io/InputStream;
.source "SerializedFrame.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/filterfw/core/SerializedFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectByteInputStream"
.end annotation


# instance fields
.field private mBuffer:[B

.field private mPos:I

.field private mSize:I

.field final synthetic this$0:Landroid/filterfw/core/SerializedFrame;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/SerializedFrame;[BI)V
    .registers 5
    .parameter
    .parameter "buffer"
    .parameter "size"

    #@0
    .prologue
    .line 128
    iput-object p1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->this$0:Landroid/filterfw/core/SerializedFrame;

    #@2
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@5
    .line 125
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@8
    .line 129
    iput-object p2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mBuffer:[B

    #@a
    .line 130
    iput p3, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@c
    .line 131
    return-void
.end method


# virtual methods
.method public final available()I
    .registers 3

    #@0
    .prologue
    .line 135
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@2
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public final read()I
    .registers 4

    #@0
    .prologue
    .line 140
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@2
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@4
    if-ge v0, v1, :cond_13

    #@6
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mBuffer:[B

    #@8
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@a
    add-int/lit8 v2, v1, 0x1

    #@c
    iput v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@e
    aget-byte v0, v0, v1

    #@10
    and-int/lit16 v0, v0, 0xff

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, -0x1

    #@14
    goto :goto_12
.end method

.method public final read([BII)I
    .registers 6
    .parameter "b"
    .parameter "off"
    .parameter "len"

    #@0
    .prologue
    .line 145
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@2
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@4
    if-lt v0, v1, :cond_8

    #@6
    .line 146
    const/4 v0, -0x1

    #@7
    .line 153
    :goto_7
    return v0

    #@8
    .line 148
    :cond_8
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@a
    add-int/2addr v0, p3

    #@b
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@d
    if-le v0, v1, :cond_15

    #@f
    .line 149
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@11
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@13
    sub-int p3, v0, v1

    #@15
    .line 151
    :cond_15
    iget-object v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mBuffer:[B

    #@17
    iget v1, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@19
    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1c
    .line 152
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@1e
    add-int/2addr v0, p3

    #@1f
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@21
    move v0, p3

    #@22
    .line 153
    goto :goto_7
.end method

.method public final skip(J)J
    .registers 9
    .parameter "n"

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 158
    iget v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@4
    int-to-long v2, v2

    #@5
    add-long/2addr v2, p1

    #@6
    iget v4, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@8
    int-to-long v4, v4

    #@9
    cmp-long v2, v2, v4

    #@b
    if-lez v2, :cond_13

    #@d
    .line 159
    iget v2, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mSize:I

    #@f
    iget v3, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@11
    sub-int/2addr v2, v3

    #@12
    int-to-long p1, v2

    #@13
    .line 161
    :cond_13
    cmp-long v2, p1, v0

    #@15
    if-gez v2, :cond_19

    #@17
    move-wide p1, v0

    #@18
    .line 165
    .end local p1
    :goto_18
    return-wide p1

    #@19
    .line 164
    .restart local p1
    :cond_19
    iget v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@1b
    int-to-long v0, v0

    #@1c
    add-long/2addr v0, p1

    #@1d
    long-to-int v0, v0

    #@1e
    iput v0, p0, Landroid/filterfw/core/SerializedFrame$DirectByteInputStream;->mPos:I

    #@20
    goto :goto_18
.end method
