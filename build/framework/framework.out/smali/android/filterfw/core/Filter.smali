.class public abstract Landroid/filterfw/core/Filter;
.super Ljava/lang/Object;
.source "Filter.java"


# static fields
.field static final STATUS_ERROR:I = 0x6

.field static final STATUS_FINISHED:I = 0x5

.field static final STATUS_PREINIT:I = 0x0

.field static final STATUS_PREPARED:I = 0x2

.field static final STATUS_PROCESSING:I = 0x3

.field static final STATUS_RELEASED:I = 0x7

.field static final STATUS_SLEEPING:I = 0x4

.field static final STATUS_UNPREPARED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Filter"


# instance fields
.field private mCurrentTimestamp:J

.field private mFramesToRelease:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/filterfw/core/Frame;",
            ">;"
        }
    .end annotation
.end field

.field private mFramesToSet:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/filterfw/core/Frame;",
            ">;"
        }
    .end annotation
.end field

.field private mInputCount:I

.field private mInputPorts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/filterfw/core/InputPort;",
            ">;"
        }
    .end annotation
.end field

.field private mIsOpen:Z

.field private mLogVerbose:Z

.field private mName:Ljava/lang/String;

.field private mOutputCount:I

.field private mOutputPorts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/filterfw/core/OutputPort;",
            ">;"
        }
    .end annotation
.end field

.field private mSleepDelay:I

.field private mStatus:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 55
    iput v0, p0, Landroid/filterfw/core/Filter;->mInputCount:I

    #@7
    .line 56
    iput v0, p0, Landroid/filterfw/core/Filter;->mOutputCount:I

    #@9
    .line 64
    iput v1, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@b
    .line 65
    iput-boolean v1, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z

    #@d
    .line 74
    iput-object p1, p0, Landroid/filterfw/core/Filter;->mName:Ljava/lang/String;

    #@f
    .line 75
    new-instance v0, Ljava/util/HashSet;

    #@11
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@14
    iput-object v0, p0, Landroid/filterfw/core/Filter;->mFramesToRelease:Ljava/util/HashSet;

    #@16
    .line 76
    new-instance v0, Ljava/util/HashMap;

    #@18
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/filterfw/core/Filter;->mFramesToSet:Ljava/util/HashMap;

    #@1d
    .line 77
    iput v1, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@1f
    .line 79
    const-string v0, "Filter"

    #@21
    const/4 v1, 0x2

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@25
    move-result v0

    #@26
    iput-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@28
    .line 80
    return-void
.end method

.method private final addAndSetFinalPorts(Landroid/filterfw/core/KeyValueMap;)V
    .registers 14
    .parameter "values"

    #@0
    .prologue
    .line 561
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v3

    #@4
    .line 563
    .local v3, filterClass:Ljava/lang/Class;
    invoke-virtual {v3}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    #@7
    move-result-object v1

    #@8
    .local v1, arr$:[Ljava/lang/reflect/Field;
    array-length v7, v1

    #@9
    .local v7, len$:I
    const/4 v6, 0x0

    #@a
    .local v6, i$:I
    :goto_a
    if-ge v6, v7, :cond_76

    #@c
    aget-object v2, v1, v6

    #@e
    .line 564
    .local v2, field:Ljava/lang/reflect/Field;
    const-class v9, Landroid/filterfw/core/GenerateFinalPort;

    #@10
    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@13
    move-result-object v0

    #@14
    .local v0, annotation:Ljava/lang/annotation/Annotation;
    if-eqz v0, :cond_3f

    #@16
    move-object v4, v0

    #@17
    .line 565
    check-cast v4, Landroid/filterfw/core/GenerateFinalPort;

    #@19
    .line 566
    .local v4, generator:Landroid/filterfw/core/GenerateFinalPort;
    invoke-interface {v4}, Landroid/filterfw/core/GenerateFinalPort;->name()Ljava/lang/String;

    #@1c
    move-result-object v9

    #@1d
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    #@20
    move-result v9

    #@21
    if-eqz v9, :cond_42

    #@23
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@26
    move-result-object v8

    #@27
    .line 567
    .local v8, name:Ljava/lang/String;
    :goto_27
    invoke-interface {v4}, Landroid/filterfw/core/GenerateFinalPort;->hasDefault()Z

    #@2a
    move-result v5

    #@2b
    .line 568
    .local v5, hasDefault:Z
    const/4 v9, 0x1

    #@2c
    invoke-virtual {p0, v8, v2, v5, v9}, Landroid/filterfw/core/Filter;->addFieldPort(Ljava/lang/String;Ljava/lang/reflect/Field;ZZ)V

    #@2f
    .line 569
    invoke-virtual {p1, v8}, Landroid/filterfw/core/KeyValueMap;->containsKey(Ljava/lang/Object;)Z

    #@32
    move-result v9

    #@33
    if-eqz v9, :cond_47

    #@35
    .line 570
    invoke-virtual {p1, v8}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v9

    #@39
    invoke-direct {p0, v8, v9}, Landroid/filterfw/core/Filter;->setImmediateInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@3c
    .line 571
    invoke-virtual {p1, v8}, Landroid/filterfw/core/KeyValueMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 563
    .end local v4           #generator:Landroid/filterfw/core/GenerateFinalPort;
    .end local v5           #hasDefault:Z
    .end local v8           #name:Ljava/lang/String;
    :cond_3f
    add-int/lit8 v6, v6, 0x1

    #@41
    goto :goto_a

    #@42
    .line 566
    .restart local v4       #generator:Landroid/filterfw/core/GenerateFinalPort;
    :cond_42
    invoke-interface {v4}, Landroid/filterfw/core/GenerateFinalPort;->name()Ljava/lang/String;

    #@45
    move-result-object v8

    #@46
    goto :goto_27

    #@47
    .line 572
    .restart local v5       #hasDefault:Z
    .restart local v8       #name:Ljava/lang/String;
    :cond_47
    invoke-interface {v4}, Landroid/filterfw/core/GenerateFinalPort;->hasDefault()Z

    #@4a
    move-result v9

    #@4b
    if-nez v9, :cond_3f

    #@4d
    .line 573
    new-instance v9, Ljava/lang/RuntimeException;

    #@4f
    new-instance v10, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v11, "No value specified for final input port \'"

    #@56
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v10

    #@5a
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v10

    #@5e
    const-string v11, "\' of filter "

    #@60
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v10

    #@64
    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v10

    #@68
    const-string v11, "!"

    #@6a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v10

    #@6e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v10

    #@72
    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@75
    throw v9

    #@76
    .line 578
    .end local v0           #annotation:Ljava/lang/annotation/Annotation;
    .end local v2           #field:Ljava/lang/reflect/Field;
    .end local v4           #generator:Landroid/filterfw/core/GenerateFinalPort;
    .end local v5           #hasDefault:Z
    .end local v8           #name:Ljava/lang/String;
    :cond_76
    return-void
.end method

.method private final addAnnotatedPorts()V
    .registers 13

    #@0
    .prologue
    .line 581
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v4

    #@4
    .line 583
    .local v4, filterClass:Ljava/lang/Class;
    invoke-virtual {v4}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    #@7
    move-result-object v1

    #@8
    .local v1, arr$:[Ljava/lang/reflect/Field;
    array-length v9, v1

    #@9
    .local v9, len$:I
    const/4 v7, 0x0

    #@a
    .local v7, i$:I
    move v8, v7

    #@b
    .end local v1           #arr$:[Ljava/lang/reflect/Field;
    .end local v7           #i$:I
    .end local v9           #len$:I
    .local v8, i$:I
    :goto_b
    if-ge v8, v9, :cond_4b

    #@d
    aget-object v3, v1, v8

    #@f
    .line 584
    .local v3, field:Ljava/lang/reflect/Field;
    const-class v11, Landroid/filterfw/core/GenerateFieldPort;

    #@11
    invoke-virtual {v3, v11}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@14
    move-result-object v0

    #@15
    .local v0, annotation:Ljava/lang/annotation/Annotation;
    if-eqz v0, :cond_21

    #@17
    move-object v5, v0

    #@18
    .line 585
    check-cast v5, Landroid/filterfw/core/GenerateFieldPort;

    #@1a
    .line 586
    .local v5, generator:Landroid/filterfw/core/GenerateFieldPort;
    invoke-direct {p0, v5, v3}, Landroid/filterfw/core/Filter;->addFieldGenerator(Landroid/filterfw/core/GenerateFieldPort;Ljava/lang/reflect/Field;)V

    #@1d
    .line 583
    .end local v5           #generator:Landroid/filterfw/core/GenerateFieldPort;
    .end local v8           #i$:I
    :cond_1d
    :goto_1d
    add-int/lit8 v7, v8, 0x1

    #@1f
    .restart local v7       #i$:I
    move v8, v7

    #@20
    .end local v7           #i$:I
    .restart local v8       #i$:I
    goto :goto_b

    #@21
    .line 587
    :cond_21
    const-class v11, Landroid/filterfw/core/GenerateProgramPort;

    #@23
    invoke-virtual {v3, v11}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@26
    move-result-object v0

    #@27
    if-eqz v0, :cond_30

    #@29
    move-object v5, v0

    #@2a
    .line 588
    check-cast v5, Landroid/filterfw/core/GenerateProgramPort;

    #@2c
    .line 589
    .local v5, generator:Landroid/filterfw/core/GenerateProgramPort;
    invoke-direct {p0, v5, v3}, Landroid/filterfw/core/Filter;->addProgramGenerator(Landroid/filterfw/core/GenerateProgramPort;Ljava/lang/reflect/Field;)V

    #@2f
    goto :goto_1d

    #@30
    .line 590
    .end local v5           #generator:Landroid/filterfw/core/GenerateProgramPort;
    :cond_30
    const-class v11, Landroid/filterfw/core/GenerateProgramPorts;

    #@32
    invoke-virtual {v3, v11}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    #@35
    move-result-object v0

    #@36
    if-eqz v0, :cond_1d

    #@38
    move-object v6, v0

    #@39
    .line 591
    check-cast v6, Landroid/filterfw/core/GenerateProgramPorts;

    #@3b
    .line 592
    .local v6, generators:Landroid/filterfw/core/GenerateProgramPorts;
    invoke-interface {v6}, Landroid/filterfw/core/GenerateProgramPorts;->value()[Landroid/filterfw/core/GenerateProgramPort;

    #@3e
    move-result-object v2

    #@3f
    .local v2, arr$:[Landroid/filterfw/core/GenerateProgramPort;
    array-length v10, v2

    #@40
    .local v10, len$:I
    const/4 v7, 0x0

    #@41
    .end local v8           #i$:I
    .restart local v7       #i$:I
    :goto_41
    if-ge v7, v10, :cond_1d

    #@43
    aget-object v5, v2, v7

    #@45
    .line 593
    .restart local v5       #generator:Landroid/filterfw/core/GenerateProgramPort;
    invoke-direct {p0, v5, v3}, Landroid/filterfw/core/Filter;->addProgramGenerator(Landroid/filterfw/core/GenerateProgramPort;Ljava/lang/reflect/Field;)V

    #@48
    .line 592
    add-int/lit8 v7, v7, 0x1

    #@4a
    goto :goto_41

    #@4b
    .line 597
    .end local v0           #annotation:Ljava/lang/annotation/Annotation;
    .end local v2           #arr$:[Landroid/filterfw/core/GenerateProgramPort;
    .end local v3           #field:Ljava/lang/reflect/Field;
    .end local v5           #generator:Landroid/filterfw/core/GenerateProgramPort;
    .end local v6           #generators:Landroid/filterfw/core/GenerateProgramPorts;
    .end local v7           #i$:I
    .end local v10           #len$:I
    .restart local v8       #i$:I
    :cond_4b
    return-void
.end method

.method private final addFieldGenerator(Landroid/filterfw/core/GenerateFieldPort;Ljava/lang/reflect/Field;)V
    .registers 6
    .parameter "generator"
    .parameter "field"

    #@0
    .prologue
    .line 600
    invoke-interface {p1}, Landroid/filterfw/core/GenerateFieldPort;->name()Ljava/lang/String;

    #@3
    move-result-object v2

    #@4
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_17

    #@a
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 601
    .local v1, name:Ljava/lang/String;
    :goto_e
    invoke-interface {p1}, Landroid/filterfw/core/GenerateFieldPort;->hasDefault()Z

    #@11
    move-result v0

    #@12
    .line 602
    .local v0, hasDefault:Z
    const/4 v2, 0x0

    #@13
    invoke-virtual {p0, v1, p2, v0, v2}, Landroid/filterfw/core/Filter;->addFieldPort(Ljava/lang/String;Ljava/lang/reflect/Field;ZZ)V

    #@16
    .line 603
    return-void

    #@17
    .line 600
    .end local v0           #hasDefault:Z
    .end local v1           #name:Ljava/lang/String;
    :cond_17
    invoke-interface {p1}, Landroid/filterfw/core/GenerateFieldPort;->name()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    goto :goto_e
.end method

.method private final addProgramGenerator(Landroid/filterfw/core/GenerateProgramPort;Ljava/lang/reflect/Field;)V
    .registers 9
    .parameter "generator"
    .parameter "field"

    #@0
    .prologue
    .line 606
    invoke-interface {p1}, Landroid/filterfw/core/GenerateProgramPort;->name()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 607
    .local v1, name:Ljava/lang/String;
    invoke-interface {p1}, Landroid/filterfw/core/GenerateProgramPort;->variableName()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_1d

    #@e
    move-object v2, v1

    #@f
    .line 609
    .local v2, varName:Ljava/lang/String;
    :goto_f
    invoke-interface {p1}, Landroid/filterfw/core/GenerateProgramPort;->type()Ljava/lang/Class;

    #@12
    move-result-object v4

    #@13
    .line 610
    .local v4, varType:Ljava/lang/Class;
    invoke-interface {p1}, Landroid/filterfw/core/GenerateProgramPort;->hasDefault()Z

    #@16
    move-result v5

    #@17
    .local v5, hasDefault:Z
    move-object v0, p0

    #@18
    move-object v3, p2

    #@19
    .line 611
    invoke-virtual/range {v0 .. v5}, Landroid/filterfw/core/Filter;->addProgramPort(Ljava/lang/String;Ljava/lang/String;Ljava/lang/reflect/Field;Ljava/lang/Class;Z)V

    #@1c
    .line 612
    return-void

    #@1d
    .line 607
    .end local v2           #varName:Ljava/lang/String;
    .end local v4           #varType:Ljava/lang/Class;
    .end local v5           #hasDefault:Z
    :cond_1d
    invoke-interface {p1}, Landroid/filterfw/core/GenerateProgramPort;->variableName()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    goto :goto_f
.end method

.method private final closePorts()V
    .registers 7

    #@0
    .prologue
    .line 685
    iget-boolean v3, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@2
    if-eqz v3, :cond_22

    #@4
    const-string v3, "Filter"

    #@6
    new-instance v4, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "Closing all ports on "

    #@d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    const-string v5, "!"

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 686
    :cond_22
    iget-object v3, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@24
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@27
    move-result-object v3

    #@28
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v0

    #@2c
    .local v0, i$:Ljava/util/Iterator;
    :goto_2c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_3c

    #@32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@38
    .line 687
    .local v1, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->close()V

    #@3b
    goto :goto_2c

    #@3c
    .line 689
    .end local v1           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_3c
    iget-object v3, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@3e
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@41
    move-result-object v3

    #@42
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@45
    move-result-object v0

    #@46
    :goto_46
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_56

    #@4c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4f
    move-result-object v2

    #@50
    check-cast v2, Landroid/filterfw/core/OutputPort;

    #@52
    .line 690
    .local v2, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v2}, Landroid/filterfw/core/OutputPort;->close()V

    #@55
    goto :goto_46

    #@56
    .line 692
    .end local v2           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_56
    return-void
.end method

.method private final filterMustClose()Z
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 695
    iget-object v4, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@3
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v4

    #@7
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v0

    #@b
    .local v0, i$:Ljava/util/Iterator;
    :cond_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v4

    #@f
    if-eqz v4, :cond_44

    #@11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@17
    .line 696
    .local v1, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->filterMustClose()Z

    #@1a
    move-result v4

    #@1b
    if-eqz v4, :cond_b

    #@1d
    .line 697
    iget-boolean v4, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@1f
    if-eqz v4, :cond_43

    #@21
    const-string v4, "Filter"

    #@23
    new-instance v5, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v6, "Filter "

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    const-string v6, " must close due to port "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 707
    .end local v1           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_43
    :goto_43
    return v3

    #@44
    .line 701
    :cond_44
    iget-object v4, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@46
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@49
    move-result-object v4

    #@4a
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4d
    move-result-object v0

    #@4e
    :cond_4e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@51
    move-result v4

    #@52
    if-eqz v4, :cond_87

    #@54
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@57
    move-result-object v2

    #@58
    check-cast v2, Landroid/filterfw/core/OutputPort;

    #@5a
    .line 702
    .local v2, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v2}, Landroid/filterfw/core/OutputPort;->filterMustClose()Z

    #@5d
    move-result v4

    #@5e
    if-eqz v4, :cond_4e

    #@60
    .line 703
    iget-boolean v4, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@62
    if-eqz v4, :cond_43

    #@64
    const-string v4, "Filter"

    #@66
    new-instance v5, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v6, "Filter "

    #@6d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v5

    #@71
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v5

    #@75
    const-string v6, " must close due to port "

    #@77
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v5

    #@83
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_43

    #@87
    .line 707
    .end local v2           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_87
    const/4 v3, 0x0

    #@88
    goto :goto_43
.end method

.method private final initFinalPorts(Landroid/filterfw/core/KeyValueMap;)V
    .registers 3
    .parameter "values"

    #@0
    .prologue
    .line 549
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    iput-object v0, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@7
    .line 550
    new-instance v0, Ljava/util/HashMap;

    #@9
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c
    iput-object v0, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@e
    .line 551
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->addAndSetFinalPorts(Landroid/filterfw/core/KeyValueMap;)V

    #@11
    .line 552
    return-void
.end method

.method private final initRemainingPorts(Landroid/filterfw/core/KeyValueMap;)V
    .registers 2
    .parameter "values"

    #@0
    .prologue
    .line 555
    invoke-direct {p0}, Landroid/filterfw/core/Filter;->addAnnotatedPorts()V

    #@3
    .line 556
    invoke-virtual {p0}, Landroid/filterfw/core/Filter;->setupPorts()V

    #@6
    .line 557
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->setInitialInputValues(Landroid/filterfw/core/KeyValueMap;)V

    #@9
    .line 558
    return-void
.end method

.method private final inputConditionsMet()Z
    .registers 6

    #@0
    .prologue
    .line 665
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_40

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@16
    .line 666
    .local v1, port:Landroid/filterfw/core/FilterPort;
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->isReady()Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_a

    #@1c
    .line 667
    iget-boolean v2, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@1e
    if-eqz v2, :cond_3e

    #@20
    const-string v2, "Filter"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "Input condition not met: "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, "!"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 668
    :cond_3e
    const/4 v2, 0x0

    #@3f
    .line 671
    .end local v1           #port:Landroid/filterfw/core/FilterPort;
    :goto_3f
    return v2

    #@40
    :cond_40
    const/4 v2, 0x1

    #@41
    goto :goto_3f
.end method

.method public static final isAvailable(Ljava/lang/String;)Z
    .registers 6
    .parameter "filterName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 86
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@4
    move-result-object v4

    #@5
    invoke-virtual {v4}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    #@8
    move-result-object v0

    #@9
    .line 90
    .local v0, contextClassLoader:Ljava/lang/ClassLoader;
    :try_start_9
    invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_c} :catch_14

    #@c
    move-result-object v2

    #@d
    .line 96
    .local v2, filterClass:Ljava/lang/Class;
    :try_start_d
    const-class v4, Landroid/filterfw/core/Filter;

    #@f
    invoke-virtual {v2, v4}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_12
    .catch Ljava/lang/ClassCastException; {:try_start_d .. :try_end_12} :catch_16

    #@12
    .line 100
    const/4 v3, 0x1

    #@13
    .end local v2           #filterClass:Ljava/lang/Class;
    :goto_13
    return v3

    #@14
    .line 91
    :catch_14
    move-exception v1

    #@15
    .line 92
    .local v1, e:Ljava/lang/ClassNotFoundException;
    goto :goto_13

    #@16
    .line 97
    .end local v1           #e:Ljava/lang/ClassNotFoundException;
    .restart local v2       #filterClass:Ljava/lang/Class;
    :catch_16
    move-exception v1

    #@17
    .line 98
    .local v1, e:Ljava/lang/ClassCastException;
    goto :goto_13
.end method

.method private final outputConditionsMet()Z
    .registers 6

    #@0
    .prologue
    .line 675
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_40

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/filterfw/core/OutputPort;

    #@16
    .line 676
    .local v1, port:Landroid/filterfw/core/FilterPort;
    invoke-virtual {v1}, Landroid/filterfw/core/OutputPort;->isReady()Z

    #@19
    move-result v2

    #@1a
    if-nez v2, :cond_a

    #@1c
    .line 677
    iget-boolean v2, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@1e
    if-eqz v2, :cond_3e

    #@20
    const-string v2, "Filter"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "Output condition not met: "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, "!"

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 678
    :cond_3e
    const/4 v2, 0x0

    #@3f
    .line 681
    .end local v1           #port:Landroid/filterfw/core/FilterPort;
    :goto_3f
    return v2

    #@40
    :cond_40
    const/4 v2, 0x1

    #@41
    goto :goto_3f
.end method

.method private final releasePulledFrames(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 658
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mFramesToRelease:Ljava/util/HashSet;

    #@2
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1a

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/filterfw/core/Frame;

    #@12
    .line 659
    .local v0, frame:Landroid/filterfw/core/Frame;
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, v0}, Landroid/filterfw/core/FrameManager;->releaseFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;

    #@19
    goto :goto_6

    #@1a
    .line 661
    .end local v0           #frame:Landroid/filterfw/core/Frame;
    :cond_1a
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mFramesToRelease:Ljava/util/HashSet;

    #@1c
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    #@1f
    .line 662
    return-void
.end method

.method private final setImmediateInputValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 7
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 621
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@2
    if-eqz v1, :cond_2c

    #@4
    const-string v1, "Filter"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Setting immediate value "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " for port "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "!"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 622
    :cond_2c
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@2f
    move-result-object v0

    #@30
    .line 623
    .local v0, port:Landroid/filterfw/core/FilterPort;
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->open()V

    #@33
    .line 624
    const/4 v1, 0x0

    #@34
    invoke-static {p2, v1}, Landroid/filterfw/core/SimpleFrame;->wrapObject(Ljava/lang/Object;Landroid/filterfw/core/FrameManager;)Landroid/filterfw/core/SimpleFrame;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v0, v1}, Landroid/filterfw/core/InputPort;->setFrame(Landroid/filterfw/core/Frame;)V

    #@3b
    .line 625
    return-void
.end method

.method private final setInitialInputValues(Landroid/filterfw/core/KeyValueMap;)V
    .registers 6
    .parameter "values"

    #@0
    .prologue
    .line 615
    invoke-virtual {p1}, Landroid/filterfw/core/KeyValueMap;->entrySet()Ljava/util/Set;

    #@3
    move-result-object v2

    #@4
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_22

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Ljava/util/Map$Entry;

    #@14
    .line 616
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@17
    move-result-object v2

    #@18
    check-cast v2, Ljava/lang/String;

    #@1a
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {p0, v2, v3}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@21
    goto :goto_8

    #@22
    .line 618
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_22
    return-void
.end method

.method private final transferInputFrames(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 628
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1a

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@16
    .line 629
    .local v1, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v1, p1}, Landroid/filterfw/core/InputPort;->transfer(Landroid/filterfw/core/FilterContext;)V

    #@19
    goto :goto_a

    #@1a
    .line 631
    .end local v1           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_1a
    return-void
.end method

.method private final wrapInputValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/filterfw/core/Frame;
    .registers 10
    .parameter "inputName"
    .parameter "value"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 634
    invoke-static {p2, v4}, Landroid/filterfw/format/ObjectFormat;->fromObject(Ljava/lang/Object;I)Landroid/filterfw/core/MutableFrameFormat;

    #@5
    move-result-object v1

    #@6
    .line 635
    .local v1, inputFormat:Landroid/filterfw/core/MutableFrameFormat;
    if-nez p2, :cond_16

    #@8
    .line 638
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@b
    move-result-object v6

    #@c
    invoke-virtual {v6}, Landroid/filterfw/core/InputPort;->getPortFormat()Landroid/filterfw/core/FrameFormat;

    #@f
    move-result-object v3

    #@10
    .line 639
    .local v3, portFormat:Landroid/filterfw/core/FrameFormat;
    if-nez v3, :cond_31

    #@12
    move-object v2, v5

    #@13
    .line 640
    .local v2, portClass:Ljava/lang/Class;
    :goto_13
    invoke-virtual {v1, v2}, Landroid/filterfw/core/MutableFrameFormat;->setObjectClass(Ljava/lang/Class;)V

    #@16
    .line 644
    .end local v2           #portClass:Ljava/lang/Class;
    .end local v3           #portFormat:Landroid/filterfw/core/FrameFormat;
    :cond_16
    instance-of v6, p2, Ljava/lang/Number;

    #@18
    if-nez v6, :cond_36

    #@1a
    instance-of v6, p2, Ljava/lang/Boolean;

    #@1c
    if-nez v6, :cond_36

    #@1e
    instance-of v6, p2, Ljava/lang/String;

    #@20
    if-nez v6, :cond_36

    #@22
    instance-of v6, p2, Ljava/io/Serializable;

    #@24
    if-eqz v6, :cond_36

    #@26
    .line 650
    .local v4, shouldSerialize:Z
    :goto_26
    if-eqz v4, :cond_38

    #@28
    new-instance v0, Landroid/filterfw/core/SerializedFrame;

    #@2a
    invoke-direct {v0, v1, v5}, Landroid/filterfw/core/SerializedFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@2d
    .line 653
    .local v0, frame:Landroid/filterfw/core/Frame;
    :goto_2d
    invoke-virtual {v0, p2}, Landroid/filterfw/core/Frame;->setObjectValue(Ljava/lang/Object;)V

    #@30
    .line 654
    return-object v0

    #@31
    .line 639
    .end local v0           #frame:Landroid/filterfw/core/Frame;
    .end local v4           #shouldSerialize:Z
    .restart local v3       #portFormat:Landroid/filterfw/core/FrameFormat;
    :cond_31
    invoke-virtual {v3}, Landroid/filterfw/core/FrameFormat;->getObjectClass()Ljava/lang/Class;

    #@34
    move-result-object v2

    #@35
    goto :goto_13

    #@36
    .line 644
    .end local v3           #portFormat:Landroid/filterfw/core/FrameFormat;
    :cond_36
    const/4 v4, 0x0

    #@37
    goto :goto_26

    #@38
    .line 650
    .restart local v4       #shouldSerialize:Z
    :cond_38
    new-instance v0, Landroid/filterfw/core/SimpleFrame;

    #@3a
    invoke-direct {v0, v1, v5}, Landroid/filterfw/core/SimpleFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3d
    goto :goto_2d
.end method


# virtual methods
.method protected addFieldPort(Ljava/lang/String;Ljava/lang/reflect/Field;ZZ)V
    .registers 11
    .parameter "name"
    .parameter "field"
    .parameter "hasDefault"
    .parameter "isFinal"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 360
    invoke-virtual {p2, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@4
    .line 363
    if-eqz p4, :cond_42

    #@6
    new-instance v0, Landroid/filterfw/core/FinalPort;

    #@8
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/filterfw/core/FinalPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V

    #@b
    .line 368
    .local v0, fieldPort:Landroid/filterfw/core/InputPort;
    :goto_b
    iget-boolean v2, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@d
    if-eqz v2, :cond_31

    #@f
    const-string v2, "Filter"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Filter "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, " adding "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 369
    :cond_31
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2, v5}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    #@38
    move-result-object v1

    #@39
    .line 371
    .local v1, format:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, v1}, Landroid/filterfw/core/InputPort;->setPortFormat(Landroid/filterfw/core/FrameFormat;)V

    #@3c
    .line 374
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@3e
    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    .line 375
    return-void

    #@42
    .line 363
    .end local v0           #fieldPort:Landroid/filterfw/core/InputPort;
    .end local v1           #format:Landroid/filterfw/core/MutableFrameFormat;
    :cond_42
    new-instance v0, Landroid/filterfw/core/FieldPort;

    #@44
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/filterfw/core/FieldPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V

    #@47
    goto :goto_b
.end method

.method protected addInputPort(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 303
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/Filter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    #@4
    .line 304
    return-void
.end method

.method protected addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V
    .registers 7
    .parameter "name"
    .parameter "formatMask"

    #@0
    .prologue
    .line 315
    new-instance v0, Landroid/filterfw/core/StreamPort;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/filterfw/core/StreamPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@5
    .line 316
    .local v0, port:Landroid/filterfw/core/InputPort;
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@7
    if-eqz v1, :cond_2b

    #@9
    const-string v1, "Filter"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Filter "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, " adding "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 317
    :cond_2b
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 318
    invoke-virtual {v0, p2}, Landroid/filterfw/core/InputPort;->setPortFormat(Landroid/filterfw/core/FrameFormat;)V

    #@33
    .line 319
    return-void
.end method

.method protected addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "outputName"
    .parameter "inputName"

    #@0
    .prologue
    .line 349
    new-instance v0, Landroid/filterfw/core/OutputPort;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/filterfw/core/OutputPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@5
    .line 350
    .local v0, port:Landroid/filterfw/core/OutputPort;
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@7
    if-eqz v1, :cond_2b

    #@9
    const-string v1, "Filter"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Filter "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, " adding "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 351
    :cond_2b
    invoke-virtual {p0, p2}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v0, v1}, Landroid/filterfw/core/OutputPort;->setBasePort(Landroid/filterfw/core/InputPort;)V

    #@32
    .line 352
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@34
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    .line 353
    return-void
.end method

.method protected addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V
    .registers 7
    .parameter "name"
    .parameter "format"

    #@0
    .prologue
    .line 331
    new-instance v0, Landroid/filterfw/core/OutputPort;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/filterfw/core/OutputPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;)V

    #@5
    .line 332
    .local v0, port:Landroid/filterfw/core/OutputPort;
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@7
    if-eqz v1, :cond_2b

    #@9
    const-string v1, "Filter"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "Filter "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, " adding "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 333
    :cond_2b
    invoke-virtual {v0, p2}, Landroid/filterfw/core/OutputPort;->setPortFormat(Landroid/filterfw/core/FrameFormat;)V

    #@2e
    .line 334
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@30
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    .line 335
    return-void
.end method

.method protected addProgramPort(Ljava/lang/String;Ljava/lang/String;Ljava/lang/reflect/Field;Ljava/lang/Class;Z)V
    .registers 14
    .parameter "name"
    .parameter "varName"
    .parameter "field"
    .parameter "varType"
    .parameter "hasDefault"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 383
    invoke-virtual {p3, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    #@4
    .line 386
    new-instance v0, Landroid/filterfw/core/ProgramPort;

    #@6
    move-object v1, p0

    #@7
    move-object v2, p1

    #@8
    move-object v3, p2

    #@9
    move-object v4, p3

    #@a
    move v5, p5

    #@b
    invoke-direct/range {v0 .. v5}, Landroid/filterfw/core/ProgramPort;-><init>(Landroid/filterfw/core/Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/reflect/Field;Z)V

    #@e
    .line 389
    .local v0, programPort:Landroid/filterfw/core/InputPort;
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@10
    if-eqz v1, :cond_34

    #@12
    const-string v1, "Filter"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "Filter "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, " adding "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 390
    :cond_34
    invoke-static {p4, v7}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    #@37
    move-result-object v6

    #@38
    .line 392
    .local v6, format:Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual {v0, v6}, Landroid/filterfw/core/InputPort;->setPortFormat(Landroid/filterfw/core/FrameFormat;)V

    #@3b
    .line 395
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@3d
    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    .line 396
    return-void
.end method

.method final declared-synchronized canProcess()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 500
    monitor-enter p0

    #@2
    :try_start_2
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@4
    if-eqz v1, :cond_30

    #@6
    const-string v1, "Filter"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Checking if can process: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " ("

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    iget v3, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    const-string v3, ")."

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 501
    :cond_30
    iget v1, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@32
    const/4 v2, 0x3

    #@33
    if-gt v1, v2, :cond_42

    #@35
    .line 502
    invoke-direct {p0}, Landroid/filterfw/core/Filter;->inputConditionsMet()Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_42

    #@3b
    invoke-direct {p0}, Landroid/filterfw/core/Filter;->outputConditionsMet()Z
    :try_end_3e
    .catchall {:try_start_2 .. :try_end_3e} :catchall_44

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_42

    #@41
    const/4 v0, 0x1

    #@42
    .line 504
    :cond_42
    monitor-exit p0

    #@43
    return v0

    #@44
    .line 500
    :catchall_44
    move-exception v0

    #@45
    monitor-exit p0

    #@46
    throw v0
.end method

.method final clearInputs()V
    .registers 4

    #@0
    .prologue
    .line 518
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1a

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@16
    .line 519
    .local v1, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->clear()V

    #@19
    goto :goto_a

    #@1a
    .line 521
    .end local v1           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_1a
    return-void
.end method

.method final clearOutputs()V
    .registers 4

    #@0
    .prologue
    .line 524
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v0

    #@a
    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1a

    #@10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Landroid/filterfw/core/OutputPort;

    #@16
    .line 525
    .local v1, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/OutputPort;->clear()V

    #@19
    goto :goto_a

    #@1a
    .line 527
    .end local v1           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_1a
    return-void
.end method

.method public close(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 190
    return-void
.end method

.method protected closeOutputPort(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 399
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getOutputPort(Ljava/lang/String;)Landroid/filterfw/core/OutputPort;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/core/OutputPort;->close()V

    #@7
    .line 400
    return-void
.end method

.method protected delayNextProcess(I)V
    .registers 3
    .parameter "millisecs"

    #@0
    .prologue
    .line 165
    iput p1, p0, Landroid/filterfw/core/Filter;->mSleepDelay:I

    #@2
    .line 166
    const/4 v0, 0x4

    #@3
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@5
    .line 167
    return-void
.end method

.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 3
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 270
    return-void
.end method

.method public getFilterClassName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final getInputFormat(Ljava/lang/String;)Landroid/filterfw/core/FrameFormat;
    .registers 4
    .parameter "portName"

    #@0
    .prologue
    .line 176
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@3
    move-result-object v0

    #@4
    .line 177
    .local v0, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->getSourceFormat()Landroid/filterfw/core/FrameFormat;

    #@7
    move-result-object v1

    #@8
    return-object v1
.end method

.method public final getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;
    .registers 6
    .parameter "portName"

    #@0
    .prologue
    .line 224
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    if-nez v1, :cond_2d

    #@4
    .line 225
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Attempting to access input port \'"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "\' of "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " before Filter has been initialized!"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v1

    #@2d
    .line 228
    :cond_2d
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, Landroid/filterfw/core/InputPort;

    #@35
    .line 229
    .local v0, result:Landroid/filterfw/core/InputPort;
    if-nez v0, :cond_60

    #@37
    .line 230
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unknown input port \'"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, "\' on filter "

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    const-string v3, "!"

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v1

    #@60
    .line 233
    :cond_60
    return-object v0
.end method

.method final getInputPorts()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/filterfw/core/InputPort;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 429
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getNumberOfConnectedInputs()I
    .registers 5

    #@0
    .prologue
    .line 196
    const/4 v0, 0x0

    #@1
    .line 197
    .local v0, c:I
    iget-object v3, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_20

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/filterfw/core/InputPort;

    #@17
    .line 198
    .local v2, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v2}, Landroid/filterfw/core/InputPort;->isConnected()Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_b

    #@1d
    .line 199
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_b

    #@20
    .line 202
    .end local v2           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_20
    return v0
.end method

.method public final getNumberOfConnectedOutputs()I
    .registers 5

    #@0
    .prologue
    .line 206
    const/4 v0, 0x0

    #@1
    .line 207
    .local v0, c:I
    iget-object v3, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_20

    #@11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/filterfw/core/OutputPort;

    #@17
    .line 208
    .local v2, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v2}, Landroid/filterfw/core/OutputPort;->isConnected()Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_b

    #@1d
    .line 209
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_b

    #@20
    .line 212
    .end local v2           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_20
    return v0
.end method

.method public final getNumberOfInputs()I
    .registers 2

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@8
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public final getNumberOfOutputs()I
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@8
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .registers 4
    .parameter "portName"
    .parameter "inputFormat"

    #@0
    .prologue
    .line 172
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public final getOutputPort(Ljava/lang/String;)Landroid/filterfw/core/OutputPort;
    .registers 6
    .parameter "portName"

    #@0
    .prologue
    .line 237
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@2
    if-nez v1, :cond_2d

    #@4
    .line 238
    new-instance v1, Ljava/lang/NullPointerException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Attempting to access output port \'"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "\' of "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " before Filter has been initialized!"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v1

    #@2d
    .line 241
    :cond_2d
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@2f
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, Landroid/filterfw/core/OutputPort;

    #@35
    .line 242
    .local v0, result:Landroid/filterfw/core/OutputPort;
    if-nez v0, :cond_60

    #@37
    .line 243
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unknown output port \'"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    const-string v3, "\' on filter "

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    const-string v3, "!"

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@5f
    throw v1

    #@60
    .line 246
    :cond_60
    return-object v0
.end method

.method final getOutputPorts()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/filterfw/core/OutputPort;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 433
    iget-object v0, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final getSleepDelay()I
    .registers 2

    #@0
    .prologue
    .line 186
    const/16 v0, 0xfa

    #@2
    return v0
.end method

.method final declared-synchronized getStatus()I
    .registers 2

    #@0
    .prologue
    .line 437
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final init()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/filterfw/core/ProtocolException;
        }
    .end annotation

    #@0
    .prologue
    .line 130
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@5
    .line 131
    .local v0, valueMap:Landroid/filterfw/core/KeyValueMap;
    invoke-virtual {p0, v0}, Landroid/filterfw/core/Filter;->initWithValueMap(Landroid/filterfw/core/KeyValueMap;)V

    #@8
    .line 132
    return-void
.end method

.method protected initProgramInputs(Landroid/filterfw/core/Program;Landroid/filterfw/core/FilterContext;)V
    .registers 6
    .parameter "program"
    .parameter "context"

    #@0
    .prologue
    .line 285
    if-eqz p1, :cond_22

    #@2
    .line 286
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mInputPorts:Ljava/util/HashMap;

    #@4
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@7
    move-result-object v2

    #@8
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v0

    #@c
    .local v0, i$:Ljava/util/Iterator;
    :cond_c
    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_22

    #@12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15
    move-result-object v1

    #@16
    check-cast v1, Landroid/filterfw/core/InputPort;

    #@18
    .line 287
    .local v1, inputPort:Landroid/filterfw/core/InputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->getTarget()Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    if-ne v2, p1, :cond_c

    #@1e
    .line 288
    invoke-virtual {v1, p2}, Landroid/filterfw/core/InputPort;->transfer(Landroid/filterfw/core/FilterContext;)V

    #@21
    goto :goto_c

    #@22
    .line 292
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #inputPort:Landroid/filterfw/core/InputPort;
    :cond_22
    return-void
.end method

.method public final varargs initWithAssignmentList([Ljava/lang/Object;)V
    .registers 3
    .parameter "keyValues"

    #@0
    .prologue
    .line 124
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@5
    .line 125
    .local v0, valueMap:Landroid/filterfw/core/KeyValueMap;
    invoke-virtual {v0, p1}, Landroid/filterfw/core/KeyValueMap;->setKeyValues([Ljava/lang/Object;)V

    #@8
    .line 126
    invoke-virtual {p0, v0}, Landroid/filterfw/core/Filter;->initWithValueMap(Landroid/filterfw/core/KeyValueMap;)V

    #@b
    .line 127
    return-void
.end method

.method public final initWithAssignmentString(Ljava/lang/String;)V
    .registers 6
    .parameter "assignments"

    #@0
    .prologue
    .line 116
    :try_start_0
    new-instance v2, Landroid/filterfw/io/TextGraphReader;

    #@2
    invoke-direct {v2}, Landroid/filterfw/io/TextGraphReader;-><init>()V

    #@5
    invoke-virtual {v2, p1}, Landroid/filterfw/io/TextGraphReader;->readKeyValueAssignments(Ljava/lang/String;)Landroid/filterfw/core/KeyValueMap;

    #@8
    move-result-object v1

    #@9
    .line 117
    .local v1, valueMap:Landroid/filterfw/core/KeyValueMap;
    invoke-virtual {p0, v1}, Landroid/filterfw/core/Filter;->initWithValueMap(Landroid/filterfw/core/KeyValueMap;)V
    :try_end_c
    .catch Landroid/filterfw/io/GraphIOException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 121
    return-void

    #@d
    .line 118
    .end local v1           #valueMap:Landroid/filterfw/core/KeyValueMap;
    :catch_d
    move-exception v0

    #@e
    .line 119
    .local v0, e:Landroid/filterfw/io/GraphIOException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@10
    invoke-virtual {v0}, Landroid/filterfw/io/GraphIOException;->getMessage()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2
.end method

.method public final initWithValueMap(Landroid/filterfw/core/KeyValueMap;)V
    .registers 3
    .parameter "valueMap"

    #@0
    .prologue
    .line 105
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->initFinalPorts(Landroid/filterfw/core/KeyValueMap;)V

    #@3
    .line 108
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->initRemainingPorts(Landroid/filterfw/core/KeyValueMap;)V

    #@6
    .line 111
    const/4 v0, 0x1

    #@7
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@9
    .line 112
    return-void
.end method

.method public isOpen()Z
    .registers 2

    #@0
    .prologue
    .line 143
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z

    #@2
    return v0
.end method

.method final notifyFieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 530
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@2
    const/4 v1, 0x3

    #@3
    if-eq v0, v1, :cond_a

    #@5
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@7
    const/4 v1, 0x2

    #@8
    if-ne v0, v1, :cond_d

    #@a
    .line 531
    :cond_a
    invoke-virtual {p0, p1, p2}, Landroid/filterfw/core/Filter;->fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V

    #@d
    .line 533
    :cond_d
    return-void
.end method

.method public open(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 181
    return-void
.end method

.method final openOutputs()V
    .registers 6

    #@0
    .prologue
    .line 509
    iget-boolean v2, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@2
    if-eqz v2, :cond_22

    #@4
    const-string v2, "Filter"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "Opening all output ports on "

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, "!"

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 510
    :cond_22
    iget-object v2, p0, Landroid/filterfw/core/Filter;->mOutputPorts:Ljava/util/HashMap;

    #@24
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@27
    move-result-object v2

    #@28
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v0

    #@2c
    .local v0, i$:Ljava/util/Iterator;
    :cond_2c
    :goto_2c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_42

    #@32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v1

    #@36
    check-cast v1, Landroid/filterfw/core/OutputPort;

    #@38
    .line 511
    .local v1, outputPort:Landroid/filterfw/core/OutputPort;
    invoke-virtual {v1}, Landroid/filterfw/core/OutputPort;->isOpen()Z

    #@3b
    move-result v2

    #@3c
    if-nez v2, :cond_2c

    #@3e
    .line 512
    invoke-virtual {v1}, Landroid/filterfw/core/OutputPort;->open()V

    #@41
    goto :goto_2c

    #@42
    .line 515
    .end local v1           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_42
    return-void
.end method

.method protected parametersUpdated(Ljava/util/Set;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 162
    .local p1, updated:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    return-void
.end method

.method final declared-synchronized performClose(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 482
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z

    #@3
    if-eqz v0, :cond_2d

    #@5
    .line 483
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@7
    if-eqz v0, :cond_21

    #@9
    const-string v0, "Filter"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Closing "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 484
    :cond_21
    const/4 v0, 0x0

    #@22
    iput-boolean v0, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z

    #@24
    .line 485
    const/4 v0, 0x2

    #@25
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@27
    .line 486
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->close(Landroid/filterfw/core/FilterContext;)V

    #@2a
    .line 487
    invoke-direct {p0}, Landroid/filterfw/core/Filter;->closePorts()V
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_2f

    #@2d
    .line 489
    :cond_2d
    monitor-exit p0

    #@2e
    return-void

    #@2f
    .line 482
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit p0

    #@31
    throw v0
.end method

.method final declared-synchronized performOpen(Landroid/filterfw/core/FilterContext;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x2

    #@2
    const/4 v1, 0x1

    #@3
    .line 445
    monitor-enter p0

    #@4
    :try_start_4
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z

    #@6
    if-nez v0, :cond_90

    #@8
    .line 446
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@a
    if-ne v0, v1, :cond_2e

    #@c
    .line 447
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@e
    if-eqz v0, :cond_28

    #@10
    const-string v0, "Filter"

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Preparing "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 448
    :cond_28
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->prepare(Landroid/filterfw/core/FilterContext;)V

    #@2b
    .line 449
    const/4 v0, 0x2

    #@2c
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@2e
    .line 451
    :cond_2e
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@30
    if-ne v0, v3, :cond_54

    #@32
    .line 452
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@34
    if-eqz v0, :cond_4e

    #@36
    const-string v0, "Filter"

    #@38
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v2, "Opening "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 453
    :cond_4e
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->open(Landroid/filterfw/core/FilterContext;)V

    #@51
    .line 454
    const/4 v0, 0x3

    #@52
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@54
    .line 456
    :cond_54
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@56
    if-eq v0, v4, :cond_8d

    #@58
    .line 457
    new-instance v0, Ljava/lang/RuntimeException;

    #@5a
    new-instance v1, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v2, "Filter "

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v1

    #@69
    const-string v2, " was brought into invalid state during "

    #@6b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    const-string/jumbo v2, "opening (state: "

    #@72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v1

    #@76
    iget v2, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v1

    #@7c
    const-string v2, ")!"

    #@7e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v1

    #@82
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@89
    throw v0
    :try_end_8a
    .catchall {:try_start_4 .. :try_end_8a} :catchall_8a

    #@8a
    .line 445
    :catchall_8a
    move-exception v0

    #@8b
    monitor-exit p0

    #@8c
    throw v0

    #@8d
    .line 460
    :cond_8d
    const/4 v0, 0x1

    #@8e
    :try_start_8e
    iput-boolean v0, p0, Landroid/filterfw/core/Filter;->mIsOpen:Z
    :try_end_90
    .catchall {:try_start_8e .. :try_end_90} :catchall_8a

    #@90
    .line 462
    :cond_90
    monitor-exit p0

    #@91
    return-void
.end method

.method final declared-synchronized performProcess(Landroid/filterfw/core/FilterContext;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 465
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@3
    const/4 v1, 0x7

    #@4
    if-ne v0, v1, :cond_28

    #@6
    .line 466
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Filter "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " is already torn down!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_25

    #@25
    .line 465
    :catchall_25
    move-exception v0

    #@26
    monitor-exit p0

    #@27
    throw v0

    #@28
    .line 468
    :cond_28
    :try_start_28
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->transferInputFrames(Landroid/filterfw/core/FilterContext;)V

    #@2b
    .line 469
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@2d
    const/4 v1, 0x3

    #@2e
    if-ge v0, v1, :cond_33

    #@30
    .line 470
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->performOpen(Landroid/filterfw/core/FilterContext;)V

    #@33
    .line 472
    :cond_33
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@35
    if-eqz v0, :cond_4f

    #@37
    const-string v0, "Filter"

    #@39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "Processing "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 473
    :cond_4f
    const-wide/16 v0, -0x1

    #@51
    iput-wide v0, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@53
    .line 474
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->process(Landroid/filterfw/core/FilterContext;)V

    #@56
    .line 475
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;->releasePulledFrames(Landroid/filterfw/core/FilterContext;)V

    #@59
    .line 476
    invoke-direct {p0}, Landroid/filterfw/core/Filter;->filterMustClose()Z

    #@5c
    move-result v0

    #@5d
    if-eqz v0, :cond_62

    #@5f
    .line 477
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->performClose(Landroid/filterfw/core/FilterContext;)V
    :try_end_62
    .catchall {:try_start_28 .. :try_end_62} :catchall_25

    #@62
    .line 479
    :cond_62
    monitor-exit p0

    #@63
    return-void
.end method

.method final declared-synchronized performTearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x7

    #@1
    .line 492
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->performClose(Landroid/filterfw/core/FilterContext;)V

    #@5
    .line 493
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@7
    if-eq v0, v1, :cond_f

    #@9
    .line 494
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->tearDown(Landroid/filterfw/core/FilterContext;)V

    #@c
    .line 495
    const/4 v0, 0x7

    #@d
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_11

    #@f
    .line 497
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 492
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 159
    return-void
.end method

.method public abstract process(Landroid/filterfw/core/FilterContext;)V
.end method

.method protected final pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;
    .registers 7
    .parameter "name"

    #@0
    .prologue
    .line 258
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/filterfw/core/InputPort;->pullFrame()Landroid/filterfw/core/Frame;

    #@7
    move-result-object v0

    #@8
    .line 259
    .local v0, result:Landroid/filterfw/core/Frame;
    iget-wide v1, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@a
    const-wide/16 v3, -0x1

    #@c
    cmp-long v1, v1, v3

    #@e
    if-nez v1, :cond_3e

    #@10
    .line 260
    invoke-virtual {v0}, Landroid/filterfw/core/Frame;->getTimestamp()J

    #@13
    move-result-wide v1

    #@14
    iput-wide v1, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@16
    .line 261
    iget-boolean v1, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@18
    if-eqz v1, :cond_3e

    #@1a
    const-string v1, "Filter"

    #@1c
    new-instance v2, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v3, "Default-setting current timestamp from input port "

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " to "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget-wide v3, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@33
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 264
    :cond_3e
    iget-object v1, p0, Landroid/filterfw/core/Filter;->mFramesToRelease:Ljava/util/HashSet;

    #@40
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@43
    .line 266
    return-object v0
.end method

.method final declared-synchronized pushInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    .registers 5
    .parameter "inputName"
    .parameter "frame"

    #@0
    .prologue
    .line 536
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@4
    move-result-object v0

    #@5
    .line 537
    .local v0, port:Landroid/filterfw/core/FilterPort;
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->isOpen()Z

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_e

    #@b
    .line 538
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->open()V

    #@e
    .line 540
    :cond_e
    invoke-virtual {v0, p2}, Landroid/filterfw/core/InputPort;->pushFrame(Landroid/filterfw/core/Frame;)V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 541
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 536
    .end local v0           #port:Landroid/filterfw/core/FilterPort;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0

    #@15
    throw v1
.end method

.method final declared-synchronized pushInputValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "inputName"
    .parameter "value"

    #@0
    .prologue
    .line 544
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Filter;->wrapInputValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/filterfw/core/Frame;

    #@4
    move-result-object v0

    #@5
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/Filter;->pushInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 545
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 544
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method

.method protected final pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    .registers 7
    .parameter "name"
    .parameter "frame"

    #@0
    .prologue
    .line 250
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->getTimestamp()J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, -0x2

    #@6
    cmp-long v0, v0, v2

    #@8
    if-nez v0, :cond_37

    #@a
    .line 251
    iget-boolean v0, p0, Landroid/filterfw/core/Filter;->mLogVerbose:Z

    #@c
    if-eqz v0, :cond_32

    #@e
    const-string v0, "Filter"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "Default-setting output Frame timestamp on port "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " to "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-wide v2, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@27
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 252
    :cond_32
    iget-wide v0, p0, Landroid/filterfw/core/Filter;->mCurrentTimestamp:J

    #@34
    invoke-virtual {p2, v0, v1}, Landroid/filterfw/core/Frame;->setTimestamp(J)V

    #@37
    .line 254
    :cond_37
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getOutputPort(Ljava/lang/String;)Landroid/filterfw/core/OutputPort;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0, p2}, Landroid/filterfw/core/OutputPort;->pushFrame(Landroid/filterfw/core/Frame;)V

    #@3e
    .line 255
    return-void
.end method

.method public setInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    .registers 5
    .parameter "inputName"
    .parameter "frame"

    #@0
    .prologue
    .line 147
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@3
    move-result-object v0

    #@4
    .line 148
    .local v0, port:Landroid/filterfw/core/FilterPort;
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->isOpen()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_d

    #@a
    .line 149
    invoke-virtual {v0}, Landroid/filterfw/core/InputPort;->open()V

    #@d
    .line 151
    :cond_d
    invoke-virtual {v0, p2}, Landroid/filterfw/core/InputPort;->setFrame(Landroid/filterfw/core/Frame;)V

    #@10
    .line 152
    return-void
.end method

.method public final setInputValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "inputName"
    .parameter "value"

    #@0
    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Filter;->wrapInputValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/filterfw/core/Frame;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/filterfw/core/Filter;->setInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@7
    .line 156
    return-void
.end method

.method protected setWaitsOnInputPort(Ljava/lang/String;Z)V
    .registers 4
    .parameter "portName"
    .parameter "waits"

    #@0
    .prologue
    .line 410
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/filterfw/core/InputPort;->setBlocking(Z)V

    #@7
    .line 411
    return-void
.end method

.method protected setWaitsOnOutputPort(Ljava/lang/String;Z)V
    .registers 4
    .parameter "portName"
    .parameter "waits"

    #@0
    .prologue
    .line 420
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getOutputPort(Ljava/lang/String;)Landroid/filterfw/core/OutputPort;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/filterfw/core/OutputPort;->setBlocking(Z)V

    #@7
    .line 421
    return-void
.end method

.method public abstract setupPorts()V
.end method

.method public tearDown(Landroid/filterfw/core/FilterContext;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 193
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "\'"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "\' ("

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0}, Landroid/filterfw/core/Filter;->getFilterClassName()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, ")"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    return-object v0
.end method

.method protected transferInputPortFrame(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .registers 4
    .parameter "name"
    .parameter "context"

    #@0
    .prologue
    .line 277
    invoke-virtual {p0, p1}, Landroid/filterfw/core/Filter;->getInputPort(Ljava/lang/String;)Landroid/filterfw/core/InputPort;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/filterfw/core/InputPort;->transfer(Landroid/filterfw/core/FilterContext;)V

    #@7
    .line 278
    return-void
.end method

.method final declared-synchronized unsetStatus(I)V
    .registers 4
    .parameter "flag"

    #@0
    .prologue
    .line 441
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/Filter;->mStatus:I

    #@3
    xor-int/lit8 v1, p1, -0x1

    #@5
    and-int/2addr v0, v1

    #@6
    iput v0, p0, Landroid/filterfw/core/Filter;->mStatus:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    #@8
    .line 442
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 441
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method
