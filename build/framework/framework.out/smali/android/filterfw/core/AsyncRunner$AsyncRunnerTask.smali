.class Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;
.super Landroid/os/AsyncTask;
.source "AsyncRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/filterfw/core/AsyncRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncRunnerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/filterfw/core/SyncRunner;",
        "Ljava/lang/Void;",
        "Landroid/filterfw/core/AsyncRunner$RunnerResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AsyncRunnerTask"


# instance fields
.field final synthetic this$0:Landroid/filterfw/core/AsyncRunner;


# direct methods
.method private constructor <init>(Landroid/filterfw/core/AsyncRunner;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 51
    iput-object p1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/filterfw/core/AsyncRunner;Landroid/filterfw/core/AsyncRunner$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;-><init>(Landroid/filterfw/core/AsyncRunner;)V

    #@3
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/filterfw/core/SyncRunner;)Landroid/filterfw/core/AsyncRunner$RunnerResult;
    .registers 8
    .parameter "runner"

    #@0
    .prologue
    const/4 v5, 0x6

    #@1
    const/4 v4, 0x1

    #@2
    .line 57
    new-instance v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;

    #@4
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@6
    const/4 v3, 0x0

    #@7
    invoke-direct {v1, v2, v3}, Landroid/filterfw/core/AsyncRunner$RunnerResult;-><init>(Landroid/filterfw/core/AsyncRunner;Landroid/filterfw/core/AsyncRunner$1;)V

    #@a
    .line 59
    .local v1, result:Landroid/filterfw/core/AsyncRunner$RunnerResult;
    :try_start_a
    array-length v2, p1

    #@b
    if-le v2, v4, :cond_2f

    #@d
    .line 60
    new-instance v2, Ljava/lang/RuntimeException;

    #@f
    const-string v3, "More than one runner received!"

    #@11
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v2
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_15} :catch_15

    #@15
    .line 90
    :catch_15
    move-exception v0

    #@16
    .line 91
    .local v0, exception:Ljava/lang/Exception;
    iput-object v0, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->exception:Ljava/lang/Exception;

    #@18
    .line 92
    iput v5, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@1a
    .line 97
    .end local v0           #exception:Ljava/lang/Exception;
    :cond_1a
    :goto_1a
    :try_start_1a
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@1c
    invoke-virtual {v2}, Landroid/filterfw/core/AsyncRunner;->deactivateGlContext()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1f} :catch_a6

    #@1f
    .line 103
    :goto_1f
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@21
    invoke-static {v2}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_2e

    #@27
    const-string v2, "AsyncRunnerTask"

    #@29
    const-string v3, "Done with background graph processing."

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 104
    :cond_2e
    return-object v1

    #@2f
    .line 63
    :cond_2f
    const/4 v2, 0x0

    #@30
    :try_start_30
    aget-object v2, p1, v2

    #@32
    invoke-virtual {v2}, Landroid/filterfw/core/SyncRunner;->assertReadyToStep()V

    #@35
    .line 66
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@37
    invoke-static {v2}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_44

    #@3d
    const-string v2, "AsyncRunnerTask"

    #@3f
    const-string v3, "Starting background graph processing."

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 67
    :cond_44
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@46
    invoke-virtual {v2}, Landroid/filterfw/core/AsyncRunner;->activateGlContext()Z

    #@49
    .line 69
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@4b
    invoke-static {v2}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@4e
    move-result v2

    #@4f
    if-eqz v2, :cond_58

    #@51
    const-string v2, "AsyncRunnerTask"

    #@53
    const-string v3, "Preparing filter graph for processing."

    #@55
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 70
    :cond_58
    const/4 v2, 0x0

    #@59
    aget-object v2, p1, v2

    #@5b
    invoke-virtual {v2}, Landroid/filterfw/core/SyncRunner;->beginProcessing()V

    #@5e
    .line 72
    iget-object v2, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@60
    invoke-static {v2}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@63
    move-result v2

    #@64
    if-eqz v2, :cond_6d

    #@66
    const-string v2, "AsyncRunnerTask"

    #@68
    const-string v3, "Running graph."

    #@6a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 75
    :cond_6d
    const/4 v2, 0x1

    #@6e
    iput v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@70
    .line 76
    :cond_70
    :goto_70
    invoke-virtual {p0}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->isCancelled()Z

    #@73
    move-result v2

    #@74
    if-nez v2, :cond_9b

    #@76
    iget v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@78
    if-ne v2, v4, :cond_9b

    #@7a
    .line 77
    const/4 v2, 0x0

    #@7b
    aget-object v2, p1, v2

    #@7d
    invoke-virtual {v2}, Landroid/filterfw/core/SyncRunner;->performStep()Z

    #@80
    move-result v2

    #@81
    if-nez v2, :cond_70

    #@83
    .line 78
    const/4 v2, 0x0

    #@84
    aget-object v2, p1, v2

    #@86
    invoke-virtual {v2}, Landroid/filterfw/core/SyncRunner;->determinePostRunState()I

    #@89
    move-result v2

    #@8a
    iput v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@8c
    .line 79
    iget v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@8e
    const/4 v3, 0x3

    #@8f
    if-ne v2, v3, :cond_70

    #@91
    .line 80
    const/4 v2, 0x0

    #@92
    aget-object v2, p1, v2

    #@94
    invoke-virtual {v2}, Landroid/filterfw/core/SyncRunner;->waitUntilWake()V

    #@97
    .line 81
    const/4 v2, 0x1

    #@98
    iput v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@9a
    goto :goto_70

    #@9b
    .line 87
    :cond_9b
    invoke-virtual {p0}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->isCancelled()Z

    #@9e
    move-result v2

    #@9f
    if-eqz v2, :cond_1a

    #@a1
    .line 88
    const/4 v2, 0x5

    #@a2
    iput v2, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I
    :try_end_a4
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_a4} :catch_15

    #@a4
    goto/16 :goto_1a

    #@a6
    .line 98
    :catch_a6
    move-exception v0

    #@a7
    .line 99
    .restart local v0       #exception:Ljava/lang/Exception;
    iput-object v0, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->exception:Ljava/lang/Exception;

    #@a9
    .line 100
    iput v5, v1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@ab
    goto/16 :goto_1f
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 51
    check-cast p1, [Landroid/filterfw/core/SyncRunner;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->doInBackground([Landroid/filterfw/core/SyncRunner;)Landroid/filterfw/core/AsyncRunner$RunnerResult;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method protected onCancelled(Landroid/filterfw/core/AsyncRunner$RunnerResult;)V
    .registers 2
    .parameter "result"

    #@0
    .prologue
    .line 109
    invoke-virtual {p0, p1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->onPostExecute(Landroid/filterfw/core/AsyncRunner$RunnerResult;)V

    #@3
    .line 110
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    check-cast p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->onCancelled(Landroid/filterfw/core/AsyncRunner$RunnerResult;)V

    #@5
    return-void
.end method

.method protected onPostExecute(Landroid/filterfw/core/AsyncRunner$RunnerResult;)V
    .registers 7
    .parameter "result"

    #@0
    .prologue
    const/4 v4, 0x6

    #@1
    const/4 v3, 0x5

    #@2
    .line 114
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@4
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    const-string v1, "AsyncRunnerTask"

    #@c
    const-string v2, "Starting post-execute."

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 115
    :cond_11
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-static {v1, v2}, Landroid/filterfw/core/AsyncRunner;->access$200(Landroid/filterfw/core/AsyncRunner;Z)V

    #@17
    .line 116
    if-nez p1, :cond_23

    #@19
    .line 118
    new-instance p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;

    #@1b
    .end local p1
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@1d
    const/4 v2, 0x0

    #@1e
    invoke-direct {p1, v1, v2}, Landroid/filterfw/core/AsyncRunner$RunnerResult;-><init>(Landroid/filterfw/core/AsyncRunner;Landroid/filterfw/core/AsyncRunner$1;)V

    #@21
    .line 119
    .restart local p1
    iput v3, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@23
    .line 121
    :cond_23
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@25
    iget-object v2, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->exception:Ljava/lang/Exception;

    #@27
    invoke-static {v1, v2}, Landroid/filterfw/core/AsyncRunner;->access$300(Landroid/filterfw/core/AsyncRunner;Ljava/lang/Exception;)V

    #@2a
    .line 122
    iget v1, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@2c
    if-eq v1, v3, :cond_32

    #@2e
    iget v1, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@30
    if-ne v1, v4, :cond_4d

    #@32
    .line 123
    :cond_32
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@34
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@37
    move-result v1

    #@38
    if-eqz v1, :cond_41

    #@3a
    const-string v1, "AsyncRunnerTask"

    #@3c
    const-string v2, "Closing filters."

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 125
    :cond_41
    :try_start_41
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@43
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$400(Landroid/filterfw/core/AsyncRunner;)Landroid/filterfw/core/SyncRunner;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Landroid/filterfw/core/SyncRunner;->close()V

    #@4a
    .line 127
    const/4 v1, 0x5

    #@4b
    iput v1, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_4d} :catch_7f

    #@4d
    .line 135
    :cond_4d
    :goto_4d
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@4f
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$500(Landroid/filterfw/core/AsyncRunner;)Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@52
    move-result-object v1

    #@53
    if-eqz v1, :cond_6f

    #@55
    .line 136
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@57
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@5a
    move-result v1

    #@5b
    if-eqz v1, :cond_64

    #@5d
    const-string v1, "AsyncRunnerTask"

    #@5f
    const-string v2, "Calling graph done callback."

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 137
    :cond_64
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@66
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$500(Landroid/filterfw/core/AsyncRunner;)Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;

    #@69
    move-result-object v1

    #@6a
    iget v2, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@6c
    invoke-interface {v1, v2}, Landroid/filterfw/core/GraphRunner$OnRunnerDoneListener;->onRunnerDone(I)V

    #@6f
    .line 139
    :cond_6f
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@71
    invoke-static {v1}, Landroid/filterfw/core/AsyncRunner;->access$100(Landroid/filterfw/core/AsyncRunner;)Z

    #@74
    move-result v1

    #@75
    if-eqz v1, :cond_7e

    #@77
    const-string v1, "AsyncRunnerTask"

    #@79
    const-string v2, "Completed post-execute."

    #@7b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 140
    :cond_7e
    return-void

    #@7f
    .line 130
    :catch_7f
    move-exception v0

    #@80
    .line 131
    .local v0, exception:Ljava/lang/Exception;
    iput v4, p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;->status:I

    #@82
    .line 132
    iget-object v1, p0, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->this$0:Landroid/filterfw/core/AsyncRunner;

    #@84
    invoke-static {v1, v0}, Landroid/filterfw/core/AsyncRunner;->access$300(Landroid/filterfw/core/AsyncRunner;Ljava/lang/Exception;)V

    #@87
    goto :goto_4d
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 51
    check-cast p1, Landroid/filterfw/core/AsyncRunner$RunnerResult;

    #@2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/filterfw/core/AsyncRunner$AsyncRunnerTask;->onPostExecute(Landroid/filterfw/core/AsyncRunner$RunnerResult;)V

    #@5
    return-void
.end method
