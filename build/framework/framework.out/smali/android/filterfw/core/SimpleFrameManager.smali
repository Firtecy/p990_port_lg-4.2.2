.class public Landroid/filterfw/core/SimpleFrameManager;
.super Landroid/filterfw/core/FrameManager;
.source "SimpleFrameManager.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/filterfw/core/FrameManager;-><init>()V

    #@3
    .line 34
    return-void
.end method

.method private createNewFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;
    .registers 7
    .parameter "format"

    #@0
    .prologue
    .line 60
    const/4 v1, 0x0

    #@1
    .line 61
    .local v1, result:Landroid/filterfw/core/Frame;
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@4
    move-result v2

    #@5
    packed-switch v2, :pswitch_data_50

    #@8
    .line 83
    new-instance v2, Ljava/lang/RuntimeException;

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "Unsupported frame target type: "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@18
    move-result v4

    #@19
    invoke-static {v4}, Landroid/filterfw/core/FrameFormat;->targetToString(I)Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    const-string v4, "!"

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v2

    #@2f
    .line 63
    :pswitch_2f
    new-instance v1, Landroid/filterfw/core/SimpleFrame;

    #@31
    .end local v1           #result:Landroid/filterfw/core/Frame;
    invoke-direct {v1, p1, p0}, Landroid/filterfw/core/SimpleFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@34
    .line 86
    .restart local v1       #result:Landroid/filterfw/core/Frame;
    :goto_34
    return-object v1

    #@35
    .line 67
    :pswitch_35
    new-instance v1, Landroid/filterfw/core/NativeFrame;

    #@37
    .end local v1           #result:Landroid/filterfw/core/Frame;
    invoke-direct {v1, p1, p0}, Landroid/filterfw/core/NativeFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3a
    .line 68
    .restart local v1       #result:Landroid/filterfw/core/Frame;
    goto :goto_34

    #@3b
    .line 71
    :pswitch_3b
    new-instance v0, Landroid/filterfw/core/GLFrame;

    #@3d
    invoke-direct {v0, p1, p0}, Landroid/filterfw/core/GLFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@40
    .line 72
    .local v0, glFrame:Landroid/filterfw/core/GLFrame;
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrameManager;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v0, v2}, Landroid/filterfw/core/GLFrame;->init(Landroid/filterfw/core/GLEnvironment;)V

    #@47
    .line 73
    move-object v1, v0

    #@48
    .line 74
    goto :goto_34

    #@49
    .line 78
    .end local v0           #glFrame:Landroid/filterfw/core/GLFrame;
    :pswitch_49
    new-instance v1, Landroid/filterfw/core/VertexFrame;

    #@4b
    .end local v1           #result:Landroid/filterfw/core/Frame;
    invoke-direct {v1, p1, p0}, Landroid/filterfw/core/VertexFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@4e
    .line 79
    .restart local v1       #result:Landroid/filterfw/core/Frame;
    goto :goto_34

    #@4f
    .line 61
    nop

    #@50
    :pswitch_data_50
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_35
        :pswitch_3b
        :pswitch_49
    .end packed-switch
.end method


# virtual methods
.method public newBoundFrame(Landroid/filterfw/core/FrameFormat;IJ)Landroid/filterfw/core/Frame;
    .registers 12
    .parameter "format"
    .parameter "bindingType"
    .parameter "bindingId"

    #@0
    .prologue
    .line 43
    const/4 v6, 0x0

    #@1
    .line 44
    .local v6, result:Landroid/filterfw/core/Frame;
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@4
    move-result v1

    #@5
    packed-switch v1, :pswitch_data_42

    #@8
    .line 53
    new-instance v1, Ljava/lang/RuntimeException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Attached frames are not supported for target type: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p1}, Landroid/filterfw/core/FrameFormat;->getTarget()I

    #@18
    move-result v3

    #@19
    invoke-static {v3}, Landroid/filterfw/core/FrameFormat;->targetToString(I)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    const-string v3, "!"

    #@23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v1

    #@2f
    .line 46
    :pswitch_2f
    new-instance v0, Landroid/filterfw/core/GLFrame;

    #@31
    move-object v1, p1

    #@32
    move-object v2, p0

    #@33
    move v3, p2

    #@34
    move-wide v4, p3

    #@35
    invoke-direct/range {v0 .. v5}, Landroid/filterfw/core/GLFrame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;IJ)V

    #@38
    .line 47
    .local v0, glFrame:Landroid/filterfw/core/GLFrame;
    invoke-virtual {p0}, Landroid/filterfw/core/SimpleFrameManager;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLFrame;->init(Landroid/filterfw/core/GLEnvironment;)V

    #@3f
    .line 48
    move-object v6, v0

    #@40
    .line 56
    return-object v6

    #@41
    .line 44
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x3
        :pswitch_2f
    .end packed-switch
.end method

.method public newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;
    .registers 3
    .parameter "format"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/filterfw/core/SimpleFrameManager;->createNewFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public releaseFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;
    .registers 5
    .parameter "frame"

    #@0
    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->decRefCount()I

    #@3
    move-result v0

    #@4
    .line 98
    .local v0, refCount:I
    if-nez v0, :cond_11

    #@6
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->hasNativeAllocation()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 99
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->releaseNativeAllocation()V

    #@f
    .line 100
    const/4 p1, 0x0

    #@10
    .line 104
    .end local p1
    :cond_10
    return-object p1

    #@11
    .line 101
    .restart local p1
    :cond_11
    if-gez v0, :cond_10

    #@13
    .line 102
    new-instance v1, Ljava/lang/RuntimeException;

    #@15
    const-string v2, "Frame reference count dropped below 0!"

    #@17
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1
.end method

.method public retainFrame(Landroid/filterfw/core/Frame;)Landroid/filterfw/core/Frame;
    .registers 2
    .parameter "frame"

    #@0
    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/filterfw/core/Frame;->incRefCount()I

    #@3
    .line 92
    return-object p1
.end method
