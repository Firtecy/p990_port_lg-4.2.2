.class public Landroid/filterfw/core/ShaderProgram;
.super Landroid/filterfw/core/Program;
.source "ShaderProgram.java"


# instance fields
.field private mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

.field private mMaxTileSize:I

.field private mTimer:Landroid/filterfw/core/StopWatchMap;

.field private shaderProgramId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 248
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 249
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/filterfw/core/Program;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@6
    .line 41
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@9
    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "fragmentShader"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 54
    invoke-direct {p0}, Landroid/filterfw/core/Program;-><init>()V

    #@4
    .line 35
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@7
    .line 41
    iput-object v1, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@9
    .line 55
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->getGLEnvironment(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/GLEnvironment;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@f
    .line 56
    iget-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@11
    invoke-direct {p0, v0, v1, p2}, Landroid/filterfw/core/ShaderProgram;->allocate(Landroid/filterfw/core/GLEnvironment;Ljava/lang/String;Ljava/lang/String;)Z

    #@14
    .line 57
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->compileAndLink()Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_22

    #@1a
    .line 58
    new-instance v0, Ljava/lang/RuntimeException;

    #@1c
    const-string v1, "Could not compile and link shader!"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 60
    :cond_22
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->setTimer()V

    #@25
    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "vertexShader"
    .parameter "fragmentShader"

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Landroid/filterfw/core/Program;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@6
    .line 41
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@9
    .line 64
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->getGLEnvironment(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/GLEnvironment;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@f
    .line 65
    iget-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@11
    invoke-direct {p0, v0, p2, p3}, Landroid/filterfw/core/ShaderProgram;->allocate(Landroid/filterfw/core/GLEnvironment;Ljava/lang/String;Ljava/lang/String;)Z

    #@14
    .line 66
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->compileAndLink()Z

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_22

    #@1a
    .line 67
    new-instance v0, Ljava/lang/RuntimeException;

    #@1c
    const-string v1, "Could not compile and link shader!"

    #@1e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v0

    #@22
    .line 69
    :cond_22
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->setTimer()V

    #@25
    .line 70
    return-void
.end method

.method private constructor <init>(Landroid/filterfw/core/NativeAllocatorTag;)V
    .registers 3
    .parameter "tag"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Landroid/filterfw/core/Program;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@6
    .line 41
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@9
    .line 52
    return-void
.end method

.method private native allocate(Landroid/filterfw/core/GLEnvironment;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method private native beginShaderDrawing()Z
.end method

.method private native compileAndLink()Z
.end method

.method public static createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 73
    invoke-static {p0}, Landroid/filterfw/core/ShaderProgram;->getGLEnvironment(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/GLEnvironment;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Landroid/filterfw/core/ShaderProgram;->nativeCreateIdentity(Landroid/filterfw/core/GLEnvironment;)Landroid/filterfw/core/ShaderProgram;

    #@7
    move-result-object v0

    #@8
    .line 74
    .local v0, program:Landroid/filterfw/core/ShaderProgram;
    invoke-direct {v0}, Landroid/filterfw/core/ShaderProgram;->setTimer()V

    #@b
    .line 75
    return-object v0
.end method

.method private native deallocate()Z
.end method

.method private static getGLEnvironment(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/GLEnvironment;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 239
    if-eqz p0, :cond_10

    #@2
    invoke-virtual {p0}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@5
    move-result-object v0

    #@6
    .line 240
    .local v0, result:Landroid/filterfw/core/GLEnvironment;
    :goto_6
    if-nez v0, :cond_12

    #@8
    .line 241
    new-instance v1, Ljava/lang/NullPointerException;

    #@a
    const-string v2, "Attempting to create ShaderProgram with no GL environment in place!"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 239
    .end local v0           #result:Landroid/filterfw/core/GLEnvironment;
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_6

    #@12
    .line 244
    .restart local v0       #result:Landroid/filterfw/core/GLEnvironment;
    :cond_12
    return-object v0
.end method

.method private native getUniformValue(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method private static native nativeCreateIdentity(Landroid/filterfw/core/GLEnvironment;)Landroid/filterfw/core/ShaderProgram;
.end method

.method private native setShaderAttributeValues(Ljava/lang/String;[FI)Z
.end method

.method private native setShaderAttributeVertexFrame(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)Z
.end method

.method private native setShaderBlendEnabled(Z)Z
.end method

.method private native setShaderBlendFunc(II)Z
.end method

.method private native setShaderClearColor(FFF)Z
.end method

.method private native setShaderClearsOutput(Z)Z
.end method

.method private native setShaderDrawMode(I)Z
.end method

.method private native setShaderTileCounts(II)Z
.end method

.method private native setShaderVertexCount(I)Z
.end method

.method private native setTargetRegion(FFFFFFFF)Z
.end method

.method private setTimer()V
    .registers 2

    #@0
    .prologue
    .line 44
    new-instance v0, Landroid/filterfw/core/StopWatchMap;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/StopWatchMap;-><init>()V

    #@5
    iput-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@7
    .line 45
    return-void
.end method

.method private native setUniformValue(Ljava/lang/String;Ljava/lang/Object;)Z
.end method

.method private native shaderProcess([Landroid/filterfw/core/GLFrame;Landroid/filterfw/core/GLFrame;)Z
.end method


# virtual methods
.method public beginDrawing()V
    .registers 3

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->beginShaderDrawing()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 234
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    const-string v1, "Could not prepare shader-program for drawing!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 236
    :cond_e
    return-void
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Landroid/filterfw/core/ShaderProgram;->deallocate()Z

    #@3
    .line 81
    return-void
.end method

.method public getGLEnvironment()Landroid/filterfw/core/GLEnvironment;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Landroid/filterfw/core/ShaderProgram;->mGLEnvironment:Landroid/filterfw/core/GLEnvironment;

    #@2
    return-object v0
.end method

.method public getHostValue(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "variableName"

    #@0
    .prologue
    .line 141
    invoke-direct {p0, p1}, Landroid/filterfw/core/ShaderProgram;->getUniformValue(Ljava/lang/String;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V
    .registers 11
    .parameter "inputs"
    .parameter "output"

    #@0
    .prologue
    .line 89
    iget-object v5, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@2
    iget-boolean v5, v5, Landroid/filterfw/core/StopWatchMap;->LOG_MFF_RUNNING_TIMES:Z

    #@4
    if-eqz v5, :cond_17

    #@6
    .line 90
    iget-object v5, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@8
    const-string v6, "glFinish"

    #@a
    invoke-virtual {v5, v6}, Landroid/filterfw/core/StopWatchMap;->start(Ljava/lang/String;)V

    #@d
    .line 91
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    #@10
    .line 92
    iget-object v5, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@12
    const-string v6, "glFinish"

    #@14
    invoke-virtual {v5, v6}, Landroid/filterfw/core/StopWatchMap;->stop(Ljava/lang/String;)V

    #@17
    .line 97
    :cond_17
    array-length v5, p1

    #@18
    new-array v0, v5, [Landroid/filterfw/core/GLFrame;

    #@1a
    .line 98
    .local v0, glInputs:[Landroid/filterfw/core/GLFrame;
    const/4 v2, 0x0

    #@1b
    .local v2, i:I
    :goto_1b
    array-length v5, p1

    #@1c
    if-ge v2, v5, :cond_4c

    #@1e
    .line 99
    aget-object v5, p1, v2

    #@20
    instance-of v5, v5, Landroid/filterfw/core/GLFrame;

    #@22
    if-eqz v5, :cond_2d

    #@24
    .line 100
    aget-object v5, p1, v2

    #@26
    check-cast v5, Landroid/filterfw/core/GLFrame;

    #@28
    aput-object v5, v0, v2

    #@2a
    .line 98
    add-int/lit8 v2, v2, 0x1

    #@2c
    goto :goto_1b

    #@2d
    .line 102
    :cond_2d
    new-instance v5, Ljava/lang/RuntimeException;

    #@2f
    new-instance v6, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v7, "ShaderProgram got non-GL frame as input "

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    const-string v7, "!"

    #@40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v6

    #@44
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v5

    #@4c
    .line 107
    :cond_4c
    const/4 v1, 0x0

    #@4d
    .line 108
    .local v1, glOutput:Landroid/filterfw/core/GLFrame;
    instance-of v5, p2, Landroid/filterfw/core/GLFrame;

    #@4f
    if-eqz v5, :cond_8b

    #@51
    move-object v1, p2

    #@52
    .line 109
    check-cast v1, Landroid/filterfw/core/GLFrame;

    #@54
    .line 115
    iget v5, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@56
    if-lez v5, :cond_7d

    #@58
    .line 116
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    #@5f
    move-result v5

    #@60
    iget v6, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@62
    add-int/2addr v5, v6

    #@63
    add-int/lit8 v5, v5, -0x1

    #@65
    iget v6, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@67
    div-int v3, v5, v6

    #@69
    .line 117
    .local v3, xTiles:I
    invoke-virtual {p2}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@6c
    move-result-object v5

    #@6d
    invoke-virtual {v5}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    #@70
    move-result v5

    #@71
    iget v6, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@73
    add-int/2addr v5, v6

    #@74
    add-int/lit8 v5, v5, -0x1

    #@76
    iget v6, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@78
    div-int v4, v5, v6

    #@7a
    .line 118
    .local v4, yTiles:I
    invoke-direct {p0, v3, v4}, Landroid/filterfw/core/ShaderProgram;->setShaderTileCounts(II)Z

    #@7d
    .line 122
    .end local v3           #xTiles:I
    .end local v4           #yTiles:I
    :cond_7d
    invoke-direct {p0, v0, v1}, Landroid/filterfw/core/ShaderProgram;->shaderProcess([Landroid/filterfw/core/GLFrame;Landroid/filterfw/core/GLFrame;)Z

    #@80
    move-result v5

    #@81
    if-nez v5, :cond_93

    #@83
    .line 123
    new-instance v5, Ljava/lang/RuntimeException;

    #@85
    const-string v6, "Error executing ShaderProgram!"

    #@87
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@8a
    throw v5

    #@8b
    .line 111
    :cond_8b
    new-instance v5, Ljava/lang/RuntimeException;

    #@8d
    const-string v6, "ShaderProgram got non-GL output frame!"

    #@8f
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@92
    throw v5

    #@93
    .line 126
    :cond_93
    iget-object v5, p0, Landroid/filterfw/core/ShaderProgram;->mTimer:Landroid/filterfw/core/StopWatchMap;

    #@95
    iget-boolean v5, v5, Landroid/filterfw/core/StopWatchMap;->LOG_MFF_RUNNING_TIMES:Z

    #@97
    if-eqz v5, :cond_9c

    #@99
    .line 127
    invoke-static {}, Landroid/opengl/GLES20;->glFinish()V

    #@9c
    .line 129
    :cond_9c
    return-void
.end method

.method public setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V
    .registers 11
    .parameter "attributeName"
    .parameter "vertexData"
    .parameter "type"
    .parameter "componentCount"
    .parameter "strideInBytes"
    .parameter "offsetInBytes"
    .parameter "normalize"

    #@0
    .prologue
    .line 158
    invoke-direct/range {p0 .. p7}, Landroid/filterfw/core/ShaderProgram;->setShaderAttributeVertexFrame(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 165
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Error setting attribute value for attribute \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 168
    :cond_25
    return-void
.end method

.method public setAttributeValues(Ljava/lang/String;[FI)V
    .registers 7
    .parameter "attributeName"
    .parameter "data"
    .parameter "componentCount"

    #@0
    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3}, Landroid/filterfw/core/ShaderProgram;->setShaderAttributeValues(Ljava/lang/String;[FI)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 146
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Error setting attribute value for attribute \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 149
    :cond_25
    return-void
.end method

.method public setBlendEnabled(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    .line 205
    invoke-direct {p0, p1}, Landroid/filterfw/core/ShaderProgram;->setShaderBlendEnabled(Z)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 206
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set Blending "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 208
    :cond_25
    return-void
.end method

.method public setBlendFunc(II)V
    .registers 6
    .parameter "sfactor"
    .parameter "dfactor"

    #@0
    .prologue
    .line 211
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/ShaderProgram;->setShaderBlendFunc(II)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_2f

    #@6
    .line 212
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set BlendFunc "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ","

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "!"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v0

    #@2f
    .line 214
    :cond_2f
    return-void
.end method

.method public setClearColor(FFF)V
    .registers 7
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 199
    invoke-direct {p0, p1, p2, p3}, Landroid/filterfw/core/ShaderProgram;->setShaderClearColor(FFF)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_39

    #@6
    .line 200
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set clear color to "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ","

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, ","

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, "!"

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@38
    throw v0

    #@39
    .line 202
    :cond_39
    return-void
.end method

.method public setClearsOutput(Z)V
    .registers 5
    .parameter "clears"

    #@0
    .prologue
    .line 193
    invoke-direct {p0, p1}, Landroid/filterfw/core/ShaderProgram;->setShaderClearsOutput(Z)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 194
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set clears-output flag to "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 196
    :cond_25
    return-void
.end method

.method public setDrawMode(I)V
    .registers 5
    .parameter "drawMode"

    #@0
    .prologue
    .line 217
    invoke-direct {p0, p1}, Landroid/filterfw/core/ShaderProgram;->setShaderDrawMode(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 218
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set GL draw-mode to "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 220
    :cond_25
    return-void
.end method

.method public setHostValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter "variableName"
    .parameter "value"

    #@0
    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/ShaderProgram;->setUniformValue(Ljava/lang/String;Ljava/lang/Object;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 134
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Error setting uniform value for variable \'"

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "\'!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 137
    :cond_25
    return-void
.end method

.method public setMaximumTileSize(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 229
    iput p1, p0, Landroid/filterfw/core/ShaderProgram;->mMaxTileSize:I

    #@2
    .line 230
    return-void
.end method

.method public setSourceRect(FFFF)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 185
    add-float v3, p1, p3

    #@2
    add-float v6, p2, p4

    #@4
    add-float v7, p1, p3

    #@6
    add-float v8, p2, p4

    #@8
    move-object v0, p0

    #@9
    move v1, p1

    #@a
    move v2, p2

    #@b
    move v4, p2

    #@c
    move v5, p1

    #@d
    invoke-virtual/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(FFFFFFFF)Z

    #@10
    .line 186
    return-void
.end method

.method public setSourceRegion(Landroid/filterfw/geometry/Quad;)V
    .registers 11
    .parameter "region"

    #@0
    .prologue
    .line 171
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@2
    iget v1, v0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@6
    iget v2, v0, Landroid/filterfw/geometry/Point;->y:F

    #@8
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@a
    iget v3, v0, Landroid/filterfw/geometry/Point;->x:F

    #@c
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@e
    iget v4, v0, Landroid/filterfw/geometry/Point;->y:F

    #@10
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@12
    iget v5, v0, Landroid/filterfw/geometry/Point;->x:F

    #@14
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@16
    iget v6, v0, Landroid/filterfw/geometry/Point;->y:F

    #@18
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@1a
    iget v7, v0, Landroid/filterfw/geometry/Point;->x:F

    #@1c
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@1e
    iget v8, v0, Landroid/filterfw/geometry/Point;->y:F

    #@20
    move-object v0, p0

    #@21
    invoke-virtual/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(FFFFFFFF)Z

    #@24
    .line 175
    return-void
.end method

.method public native setSourceRegion(FFFFFFFF)Z
.end method

.method public setTargetRect(FFFF)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 189
    add-float v3, p1, p3

    #@2
    add-float v6, p2, p4

    #@4
    add-float v7, p1, p3

    #@6
    add-float v8, p2, p4

    #@8
    move-object v0, p0

    #@9
    move v1, p1

    #@a
    move v2, p2

    #@b
    move v4, p2

    #@c
    move v5, p1

    #@d
    invoke-direct/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(FFFFFFFF)Z

    #@10
    .line 190
    return-void
.end method

.method public setTargetRegion(Landroid/filterfw/geometry/Quad;)V
    .registers 11
    .parameter "region"

    #@0
    .prologue
    .line 178
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@2
    iget v1, v0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p0:Landroid/filterfw/geometry/Point;

    #@6
    iget v2, v0, Landroid/filterfw/geometry/Point;->y:F

    #@8
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@a
    iget v3, v0, Landroid/filterfw/geometry/Point;->x:F

    #@c
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p1:Landroid/filterfw/geometry/Point;

    #@e
    iget v4, v0, Landroid/filterfw/geometry/Point;->y:F

    #@10
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@12
    iget v5, v0, Landroid/filterfw/geometry/Point;->x:F

    #@14
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p2:Landroid/filterfw/geometry/Point;

    #@16
    iget v6, v0, Landroid/filterfw/geometry/Point;->y:F

    #@18
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@1a
    iget v7, v0, Landroid/filterfw/geometry/Point;->x:F

    #@1c
    iget-object v0, p1, Landroid/filterfw/geometry/Quad;->p3:Landroid/filterfw/geometry/Point;

    #@1e
    iget v8, v0, Landroid/filterfw/geometry/Point;->y:F

    #@20
    move-object v0, p0

    #@21
    invoke-direct/range {v0 .. v8}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(FFFFFFFF)Z

    #@24
    .line 182
    return-void
.end method

.method public setVertexCount(I)V
    .registers 5
    .parameter "count"

    #@0
    .prologue
    .line 223
    invoke-direct {p0, p1}, Landroid/filterfw/core/ShaderProgram;->setShaderVertexCount(I)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 224
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Could not set GL vertex count to "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "!"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v0

    #@25
    .line 226
    :cond_25
    return-void
.end method
