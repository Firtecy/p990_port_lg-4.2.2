.class public Landroid/filterfw/core/RoundRobinScheduler;
.super Landroid/filterfw/core/Scheduler;
.source "RoundRobinScheduler.java"


# instance fields
.field private mLastPos:I


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterGraph;)V
    .registers 3
    .parameter "graph"

    #@0
    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/filterfw/core/Scheduler;-><init>(Landroid/filterfw/core/FilterGraph;)V

    #@3
    .line 30
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@6
    .line 34
    return-void
.end method


# virtual methods
.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 38
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@3
    .line 39
    return-void
.end method

.method public scheduleNextNode()Landroid/filterfw/core/Filter;
    .registers 9

    #@0
    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/filterfw/core/RoundRobinScheduler;->getGraph()Landroid/filterfw/core/FilterGraph;

    #@3
    move-result-object v6

    #@4
    invoke-virtual {v6}, Landroid/filterfw/core/FilterGraph;->getFilters()Ljava/util/Set;

    #@7
    move-result-object v0

    #@8
    .line 44
    .local v0, all_filters:Ljava/util/Set;,"Ljava/util/Set<Landroid/filterfw/core/Filter;>;"
    iget v6, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@a
    invoke-interface {v0}, Ljava/util/Set;->size()I

    #@d
    move-result v7

    #@e
    if-lt v6, v7, :cond_13

    #@10
    const/4 v6, -0x1

    #@11
    iput v6, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@13
    .line 45
    :cond_13
    const/4 v5, 0x0

    #@14
    .line 46
    .local v5, pos:I
    const/4 v2, 0x0

    #@15
    .line 47
    .local v2, first:Landroid/filterfw/core/Filter;
    const/4 v3, -0x1

    #@16
    .line 48
    .local v3, firstNdx:I
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@19
    move-result-object v4

    #@1a
    .local v4, i$:Ljava/util/Iterator;
    :goto_1a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@1d
    move-result v6

    #@1e
    if-eqz v6, :cond_3a

    #@20
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/filterfw/core/Filter;

    #@26
    .line 49
    .local v1, filter:Landroid/filterfw/core/Filter;
    invoke-virtual {v1}, Landroid/filterfw/core/Filter;->canProcess()Z

    #@29
    move-result v6

    #@2a
    if-eqz v6, :cond_34

    #@2c
    .line 50
    iget v6, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@2e
    if-gt v5, v6, :cond_37

    #@30
    .line 51
    if-nez v2, :cond_34

    #@32
    .line 53
    move-object v2, v1

    #@33
    .line 54
    move v3, v5

    #@34
    .line 62
    :cond_34
    add-int/lit8 v5, v5, 0x1

    #@36
    goto :goto_1a

    #@37
    .line 58
    :cond_37
    iput v5, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@39
    .line 71
    .end local v1           #filter:Landroid/filterfw/core/Filter;
    :goto_39
    return-object v1

    #@3a
    .line 65
    :cond_3a
    if-eqz v2, :cond_40

    #@3c
    .line 66
    iput v3, p0, Landroid/filterfw/core/RoundRobinScheduler;->mLastPos:I

    #@3e
    move-object v1, v2

    #@3f
    .line 67
    goto :goto_39

    #@40
    .line 71
    :cond_40
    const/4 v1, 0x0

    #@41
    goto :goto_39
.end method
