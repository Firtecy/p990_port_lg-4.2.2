.class public Landroid/filterfw/core/KeyValueMap;
.super Ljava/util/HashMap;
.source "KeyValueMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    #@3
    return-void
.end method

.method public static varargs fromKeyValues([Ljava/lang/Object;)Landroid/filterfw/core/KeyValueMap;
    .registers 2
    .parameter "keyValues"

    #@0
    .prologue
    .line 46
    new-instance v0, Landroid/filterfw/core/KeyValueMap;

    #@2
    invoke-direct {v0}, Landroid/filterfw/core/KeyValueMap;-><init>()V

    #@5
    .line 47
    .local v0, result:Landroid/filterfw/core/KeyValueMap;
    invoke-virtual {v0, p0}, Landroid/filterfw/core/KeyValueMap;->setKeyValues([Ljava/lang/Object;)V

    #@8
    .line 48
    return-object v0
.end method


# virtual methods
.method public getFloat(Ljava/lang/String;)F
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 62
    invoke-virtual {p0, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 63
    .local v0, result:Ljava/lang/Object;
    if-eqz v0, :cond_d

    #@6
    check-cast v0, Ljava/lang/Float;

    #@8
    .end local v0           #result:Ljava/lang/Object;
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@b
    move-result v1

    #@c
    return v1

    #@d
    .restart local v0       #result:Ljava/lang/Object;
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_8
.end method

.method public getInt(Ljava/lang/String;)I
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 57
    invoke-virtual {p0, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 58
    .local v0, result:Ljava/lang/Object;
    if-eqz v0, :cond_d

    #@6
    check-cast v0, Ljava/lang/Integer;

    #@8
    .end local v0           #result:Ljava/lang/Object;
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@b
    move-result v1

    #@c
    return v1

    #@d
    .restart local v0       #result:Ljava/lang/Object;
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_8
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0, p1}, Landroid/filterfw/core/KeyValueMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    .line 53
    .local v0, result:Ljava/lang/Object;
    if-eqz v0, :cond_9

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    .end local v0           #result:Ljava/lang/Object;
    :goto_8
    return-object v0

    #@9
    .restart local v0       #result:Ljava/lang/Object;
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method public varargs setKeyValues([Ljava/lang/Object;)V
    .registers 8
    .parameter "keyValues"

    #@0
    .prologue
    .line 30
    array-length v3, p1

    #@1
    rem-int/lit8 v3, v3, 0x2

    #@3
    if-eqz v3, :cond_d

    #@5
    .line 31
    new-instance v3, Ljava/lang/RuntimeException;

    #@7
    const-string v4, "Key-Value arguments passed into setKeyValues must be an alternating list of keys and values!"

    #@9
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@c
    throw v3

    #@d
    .line 34
    :cond_d
    const/4 v0, 0x0

    #@e
    .local v0, i:I
    :goto_e
    array-length v3, p1

    #@f
    if-ge v0, v3, :cond_5a

    #@11
    .line 35
    aget-object v3, p1, v0

    #@13
    instance-of v3, v3, Ljava/lang/String;

    #@15
    if-nez v3, :cond_4c

    #@17
    .line 36
    new-instance v3, Ljava/lang/RuntimeException;

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "Key-value argument "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    const-string v5, " must be a key of type "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, "String, but found an object of type "

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    aget-object v5, p1, v0

    #@36
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    const-string v5, "!"

    #@40
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v4

    #@48
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v3

    #@4c
    .line 39
    :cond_4c
    aget-object v1, p1, v0

    #@4e
    check-cast v1, Ljava/lang/String;

    #@50
    .line 40
    .local v1, key:Ljava/lang/String;
    add-int/lit8 v3, v0, 0x1

    #@52
    aget-object v2, p1, v3

    #@54
    .line 41
    .local v2, value:Ljava/lang/Object;
    invoke-virtual {p0, v1, v2}, Landroid/filterfw/core/KeyValueMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    .line 34
    add-int/lit8 v0, v0, 0x2

    #@59
    goto :goto_e

    #@5a
    .line 43
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #value:Ljava/lang/Object;
    :cond_5a
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 68
    new-instance v4, Ljava/io/StringWriter;

    #@2
    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    #@5
    .line 69
    .local v4, writer:Ljava/io/StringWriter;
    invoke-virtual {p0}, Landroid/filterfw/core/KeyValueMap;->entrySet()Ljava/util/Set;

    #@8
    move-result-object v5

    #@9
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_66

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Ljava/util/Map$Entry;

    #@19
    .line 71
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    .line 72
    .local v2, value:Ljava/lang/Object;
    instance-of v5, v2, Ljava/lang/String;

    #@1f
    if-eqz v5, :cond_61

    #@21
    .line 73
    new-instance v5, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v6, "\""

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    const-string v6, "\""

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    .line 77
    .local v3, valueString:Ljava/lang/String;
    :goto_3a
    new-instance v6, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@42
    move-result-object v5

    #@43
    check-cast v5, Ljava/lang/String;

    #@45
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    const-string v6, " = "

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    const-string v6, ";\n"

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v4, v5}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    #@60
    goto :goto_d

    #@61
    .line 75
    .end local v3           #valueString:Ljava/lang/String;
    :cond_61
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@64
    move-result-object v3

    #@65
    .restart local v3       #valueString:Ljava/lang/String;
    goto :goto_3a

    #@66
    .line 79
    .end local v0           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v2           #value:Ljava/lang/Object;
    .end local v3           #valueString:Ljava/lang/String;
    :cond_66
    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    return-object v5
.end method
