.class public Landroid/filterfw/core/FilterFunction;
.super Ljava/lang/Object;
.source "FilterFunction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/filterfw/core/FilterFunction$FrameHolderPort;
    }
.end annotation


# instance fields
.field private mFilter:Landroid/filterfw/core/Filter;

.field private mFilterContext:Landroid/filterfw/core/FilterContext;

.field private mFilterIsSetup:Z

.field private mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;


# direct methods
.method public constructor <init>(Landroid/filterfw/core/FilterContext;Landroid/filterfw/core/Filter;)V
    .registers 4
    .parameter "context"
    .parameter "filter"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/filterfw/core/FilterFunction;->mFilterIsSetup:Z

    #@6
    .line 39
    iput-object p1, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@8
    .line 40
    iput-object p2, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@a
    .line 41
    return-void
.end method

.method private connectFilterOutputs()V
    .registers 6

    #@0
    .prologue
    .line 131
    const/4 v0, 0x0

    #@1
    .line 132
    .local v0, i:I
    iget-object v3, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@3
    invoke-virtual {v3}, Landroid/filterfw/core/Filter;->getNumberOfOutputs()I

    #@6
    move-result v3

    #@7
    new-array v3, v3, [Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@9
    iput-object v3, p0, Landroid/filterfw/core/FilterFunction;->mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@b
    .line 133
    iget-object v3, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@d
    invoke-virtual {v3}, Landroid/filterfw/core/Filter;->getOutputPorts()Ljava/util/Collection;

    #@10
    move-result-object v3

    #@11
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@14
    move-result-object v1

    #@15
    .local v1, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@18
    move-result v3

    #@19
    if-eqz v3, :cond_34

    #@1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Landroid/filterfw/core/OutputPort;

    #@21
    .line 134
    .local v2, outputPort:Landroid/filterfw/core/OutputPort;
    iget-object v3, p0, Landroid/filterfw/core/FilterFunction;->mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@23
    new-instance v4, Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@25
    invoke-direct {v4, p0}, Landroid/filterfw/core/FilterFunction$FrameHolderPort;-><init>(Landroid/filterfw/core/FilterFunction;)V

    #@28
    aput-object v4, v3, v0

    #@2a
    .line 135
    iget-object v3, p0, Landroid/filterfw/core/FilterFunction;->mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@2c
    aget-object v3, v3, v0

    #@2e
    invoke-virtual {v2, v3}, Landroid/filterfw/core/OutputPort;->connectTo(Landroid/filterfw/core/InputPort;)V

    #@31
    .line 136
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_15

    #@34
    .line 138
    .end local v2           #outputPort:Landroid/filterfw/core/OutputPort;
    :cond_34
    return-void
.end method


# virtual methods
.method public close()V
    .registers 3

    #@0
    .prologue
    .line 101
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    iget-object v1, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@4
    invoke-virtual {v0, v1}, Landroid/filterfw/core/Filter;->performClose(Landroid/filterfw/core/FilterContext;)V

    #@7
    .line 102
    return-void
.end method

.method public execute(Landroid/filterfw/core/KeyValueMap;)Landroid/filterfw/core/Frame;
    .registers 13
    .parameter "inputMap"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 44
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@4
    invoke-virtual {v6}, Landroid/filterfw/core/Filter;->getNumberOfOutputs()I

    #@7
    move-result v2

    #@8
    .line 47
    .local v2, filterOutCount:I
    if-le v2, v9, :cond_32

    #@a
    .line 48
    new-instance v6, Ljava/lang/RuntimeException;

    #@c
    new-instance v7, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v8, "Calling execute on filter "

    #@13
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v7

    #@17
    iget-object v8, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    const-string v8, " with multiple "

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    const-string/jumbo v8, "outputs! Use executeMulti() instead!"

    #@26
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v7

    #@2a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@31
    throw v6

    #@32
    .line 53
    :cond_32
    iget-boolean v6, p0, Landroid/filterfw/core/FilterFunction;->mFilterIsSetup:Z

    #@34
    if-nez v6, :cond_3b

    #@36
    .line 54
    invoke-direct {p0}, Landroid/filterfw/core/FilterFunction;->connectFilterOutputs()V

    #@39
    .line 55
    iput-boolean v9, p0, Landroid/filterfw/core/FilterFunction;->mFilterIsSetup:Z

    #@3b
    .line 59
    :cond_3b
    const/4 v0, 0x0

    #@3c
    .line 60
    .local v0, didActivateGLEnv:Z
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@3e
    invoke-virtual {v6}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    #@41
    move-result-object v3

    #@42
    .line 61
    .local v3, glEnv:Landroid/filterfw/core/GLEnvironment;
    if-eqz v3, :cond_4e

    #@44
    invoke-virtual {v3}, Landroid/filterfw/core/GLEnvironment;->isActive()Z

    #@47
    move-result v6

    #@48
    if-nez v6, :cond_4e

    #@4a
    .line 62
    invoke-virtual {v3}, Landroid/filterfw/core/GLEnvironment;->activate()V

    #@4d
    .line 63
    const/4 v0, 0x1

    #@4e
    .line 67
    :cond_4e
    invoke-virtual {p1}, Landroid/filterfw/core/KeyValueMap;->entrySet()Ljava/util/Set;

    #@51
    move-result-object v6

    #@52
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@55
    move-result-object v4

    #@56
    .local v4, i$:Ljava/util/Iterator;
    :goto_56
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v6

    #@5a
    if-eqz v6, :cond_8c

    #@5c
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5f
    move-result-object v1

    #@60
    check-cast v1, Ljava/util/Map$Entry;

    #@62
    .line 68
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@65
    move-result-object v6

    #@66
    instance-of v6, v6, Landroid/filterfw/core/Frame;

    #@68
    if-eqz v6, :cond_7c

    #@6a
    .line 69
    iget-object v8, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@6c
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@6f
    move-result-object v6

    #@70
    check-cast v6, Ljava/lang/String;

    #@72
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@75
    move-result-object v7

    #@76
    check-cast v7, Landroid/filterfw/core/Frame;

    #@78
    invoke-virtual {v8, v6, v7}, Landroid/filterfw/core/Filter;->pushInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@7b
    goto :goto_56

    #@7c
    .line 71
    :cond_7c
    iget-object v7, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@7e
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@81
    move-result-object v6

    #@82
    check-cast v6, Ljava/lang/String;

    #@84
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v7, v6, v8}, Landroid/filterfw/core/Filter;->pushInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@8b
    goto :goto_56

    #@8c
    .line 76
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_8c
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@8e
    invoke-virtual {v6}, Landroid/filterfw/core/Filter;->getStatus()I

    #@91
    move-result v6

    #@92
    const/4 v7, 0x3

    #@93
    if-eq v6, v7, :cond_9a

    #@95
    .line 77
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@97
    invoke-virtual {v6}, Landroid/filterfw/core/Filter;->openOutputs()V

    #@9a
    .line 80
    :cond_9a
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@9c
    iget-object v7, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@9e
    invoke-virtual {v6, v7}, Landroid/filterfw/core/Filter;->performProcess(Landroid/filterfw/core/FilterContext;)V

    #@a1
    .line 83
    const/4 v5, 0x0

    #@a2
    .line 84
    .local v5, result:Landroid/filterfw/core/Frame;
    if-ne v2, v9, :cond_b6

    #@a4
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@a6
    aget-object v6, v6, v10

    #@a8
    invoke-virtual {v6}, Landroid/filterfw/core/FilterFunction$FrameHolderPort;->hasFrame()Z

    #@ab
    move-result v6

    #@ac
    if-eqz v6, :cond_b6

    #@ae
    .line 85
    iget-object v6, p0, Landroid/filterfw/core/FilterFunction;->mResultHolders:[Landroid/filterfw/core/FilterFunction$FrameHolderPort;

    #@b0
    aget-object v6, v6, v10

    #@b2
    invoke-virtual {v6}, Landroid/filterfw/core/FilterFunction$FrameHolderPort;->pullFrame()Landroid/filterfw/core/Frame;

    #@b5
    move-result-object v5

    #@b6
    .line 89
    :cond_b6
    if-eqz v0, :cond_bb

    #@b8
    .line 90
    invoke-virtual {v3}, Landroid/filterfw/core/GLEnvironment;->deactivate()V

    #@bb
    .line 93
    :cond_bb
    return-object v5
.end method

.method public varargs executeWithArgList([Ljava/lang/Object;)Landroid/filterfw/core/Frame;
    .registers 3
    .parameter "inputs"

    #@0
    .prologue
    .line 97
    invoke-static {p1}, Landroid/filterfw/core/KeyValueMap;->fromKeyValues([Ljava/lang/Object;)Landroid/filterfw/core/KeyValueMap;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/filterfw/core/FilterFunction;->execute(Landroid/filterfw/core/KeyValueMap;)Landroid/filterfw/core/Frame;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getContext()Landroid/filterfw/core/FilterContext;
    .registers 2

    #@0
    .prologue
    .line 105
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@2
    return-object v0
.end method

.method public getFilter()Landroid/filterfw/core/Filter;
    .registers 2

    #@0
    .prologue
    .line 109
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    return-object v0
.end method

.method public setInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V
    .registers 4
    .parameter "input"
    .parameter "frame"

    #@0
    .prologue
    .line 113
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/filterfw/core/Filter;->setInputFrame(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    #@5
    .line 114
    return-void
.end method

.method public setInputValue(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4
    .parameter "input"
    .parameter "value"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/filterfw/core/Filter;->setInputValue(Ljava/lang/String;Ljava/lang/Object;)V

    #@5
    .line 118
    return-void
.end method

.method public tearDown()V
    .registers 3

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    iget-object v1, p0, Landroid/filterfw/core/FilterFunction;->mFilterContext:Landroid/filterfw/core/FilterContext;

    #@4
    invoke-virtual {v0, v1}, Landroid/filterfw/core/Filter;->performTearDown(Landroid/filterfw/core/FilterContext;)V

    #@7
    .line 122
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@a
    .line 123
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Landroid/filterfw/core/FilterFunction;->mFilter:Landroid/filterfw/core/Filter;

    #@2
    invoke-virtual {v0}, Landroid/filterfw/core/Filter;->getName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
