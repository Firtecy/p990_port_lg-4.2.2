.class public Landroid/filterfw/core/VertexFrame;
.super Landroid/filterfw/core/Frame;
.source "VertexFrame.java"


# instance fields
.field private vertexFrameId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 129
    const-string v0, "filterfw"

    #@2
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@5
    .line 130
    return-void
.end method

.method constructor <init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V
    .registers 5
    .parameter "format"
    .parameter "frameManager"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/filterfw/core/Frame;-><init>(Landroid/filterfw/core/FrameFormat;Landroid/filterfw/core/FrameManager;)V

    #@3
    .line 32
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/filterfw/core/VertexFrame;->vertexFrameId:I

    #@6
    .line 36
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@d
    move-result v0

    #@e
    if-gtz v0, :cond_18

    #@10
    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v1, "Initializing vertex frame with zero size!"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 39
    :cond_18
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@1f
    move-result v0

    #@20
    invoke-direct {p0, v0}, Landroid/filterfw/core/VertexFrame;->nativeAllocate(I)Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_2e

    #@26
    .line 40
    new-instance v0, Ljava/lang/RuntimeException;

    #@28
    const-string v1, "Could not allocate vertex frame!"

    #@2a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v0

    #@2e
    .line 43
    :cond_2e
    return-void
.end method

.method private native getNativeVboId()I
.end method

.method private native nativeAllocate(I)Z
.end method

.method private native nativeDeallocate()Z
.end method

.method private native setNativeData([BII)Z
.end method

.method private native setNativeFloats([F)Z
.end method

.method private native setNativeInts([I)Z
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .registers 3

    #@0
    .prologue
    .line 110
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Vertex frames do not support reading data!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getData()Ljava/nio/ByteBuffer;
    .registers 3

    #@0
    .prologue
    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Vertex frames do not support reading data!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getFloats()[F
    .registers 3

    #@0
    .prologue
    .line 84
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Vertex frames do not support reading data!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getInts()[I
    .registers 3

    #@0
    .prologue
    .line 71
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Vertex frames do not support reading data!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getObjectValue()Ljava/lang/Object;
    .registers 3

    #@0
    .prologue
    .line 58
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Vertex frames do not support reading data!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getVboId()I
    .registers 2

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/filterfw/core/VertexFrame;->getNativeVboId()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method protected declared-synchronized hasNativeAllocation()Z
    .registers 3

    #@0
    .prologue
    .line 47
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/VertexFrame;->vertexFrameId:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_b

    #@3
    const/4 v1, -0x1

    #@4
    if-eq v0, v1, :cond_9

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    monitor-exit p0

    #@8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_7

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method protected declared-synchronized releaseNativeAllocation()V
    .registers 2

    #@0
    .prologue
    .line 52
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Landroid/filterfw/core/VertexFrame;->nativeDeallocate()Z

    #@4
    .line 53
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/filterfw/core/VertexFrame;->vertexFrameId:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    #@7
    .line 54
    monitor-exit p0

    #@8
    return-void

    #@9
    .line 52
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Unsupported: Cannot set vertex frame bitmap value!"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public setData(Ljava/nio/ByteBuffer;II)V
    .registers 7
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 89
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->assertFrameMutable()V

    #@3
    .line 90
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    #@6
    move-result-object v0

    #@7
    .line 91
    .local v0, bytes:[B
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Landroid/filterfw/core/FrameFormat;->getSize()I

    #@e
    move-result v1

    #@f
    array-length v2, v0

    #@10
    if-eq v1, v2, :cond_1a

    #@12
    .line 92
    new-instance v1, Ljava/lang/RuntimeException;

    #@14
    const-string v2, "Data size in setData does not match vertex frame size!"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 93
    :cond_1a
    invoke-direct {p0, v0, p2, p3}, Landroid/filterfw/core/VertexFrame;->setNativeData([BII)Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 94
    new-instance v1, Ljava/lang/RuntimeException;

    #@22
    const-string v2, "Could not set vertex frame data!"

    #@24
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1

    #@28
    .line 96
    :cond_28
    return-void
.end method

.method public setDataFromFrame(Landroid/filterfw/core/Frame;)V
    .registers 2
    .parameter "frame"

    #@0
    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    #@3
    .line 117
    return-void
.end method

.method public setFloats([F)V
    .registers 4
    .parameter "floats"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->assertFrameMutable()V

    #@3
    .line 77
    invoke-direct {p0, p1}, Landroid/filterfw/core/VertexFrame;->setNativeFloats([F)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    .line 78
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "Could not set int values for vertex frame!"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 80
    :cond_11
    return-void
.end method

.method public setInts([I)V
    .registers 4
    .parameter "ints"

    #@0
    .prologue
    .line 63
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->assertFrameMutable()V

    #@3
    .line 64
    invoke-direct {p0, p1}, Landroid/filterfw/core/VertexFrame;->setNativeInts([I)Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_11

    #@9
    .line 65
    new-instance v0, Ljava/lang/RuntimeException;

    #@b
    const-string v1, "Could not set int values for vertex frame!"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 67
    :cond_11
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "VertexFrame ("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->getFormat()Landroid/filterfw/core/FrameFormat;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ") with VBO ID "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0}, Landroid/filterfw/core/VertexFrame;->getVboId()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    return-object v0
.end method
