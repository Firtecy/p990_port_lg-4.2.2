.class public Landroid/filterfw/core/FilterSurfaceView;
.super Landroid/view/SurfaceView;
.source "FilterSurfaceView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static STATE_ALLOCATED:I

.field private static STATE_CREATED:I

.field private static STATE_INITIALIZED:I


# instance fields
.field private mFormat:I

.field private mGLEnv:Landroid/filterfw/core/GLEnvironment;

.field private mHeight:I

.field private mListener:Landroid/view/SurfaceHolder$Callback;

.field private mState:I

.field private mSurfaceId:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    const/4 v0, 0x0

    #@1
    sput v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_ALLOCATED:I

    #@3
    .line 31
    const/4 v0, 0x1

    #@4
    sput v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_CREATED:I

    #@6
    .line 32
    const/4 v0, 0x2

    #@7
    sput v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_INITIALIZED:I

    #@9
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    #@3
    .line 34
    sget v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_ALLOCATED:I

    #@5
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@7
    .line 40
    const/4 v0, -0x1

    #@8
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@a
    .line 44
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    #@11
    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@3
    .line 34
    sget v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_ALLOCATED:I

    #@5
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@7
    .line 40
    const/4 v0, -0x1

    #@8
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@a
    .line 49
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    #@11
    .line 50
    return-void
.end method

.method private registerSurface()V
    .registers 4

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@2
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLEnvironment;->registerSurface(Landroid/view/Surface;)I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@10
    .line 146
    iget v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@12
    if-gez v0, :cond_3b

    #@14
    .line 147
    new-instance v0, Ljava/lang/RuntimeException;

    #@16
    new-instance v1, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v2, "Could not register Surface: "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@24
    move-result-object v2

    #@25
    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, " in FilterSurfaceView!"

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v0

    #@3b
    .line 150
    :cond_3b
    return-void
.end method

.method private unregisterSurface()V
    .registers 3

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@6
    if-lez v0, :cond_f

    #@8
    .line 153
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@a
    iget v1, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@c
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLEnvironment;->unregisterSurfaceId(I)V

    #@f
    .line 155
    :cond_f
    return-void
.end method


# virtual methods
.method public declared-synchronized bindToListener(Landroid/view/SurfaceHolder$Callback;Landroid/filterfw/core/GLEnvironment;)V
    .registers 8
    .parameter "listener"
    .parameter "glEnv"

    #@0
    .prologue
    .line 54
    monitor-enter p0

    #@1
    if-nez p1, :cond_e

    #@3
    .line 55
    :try_start_3
    new-instance v0, Ljava/lang/NullPointerException;

    #@5
    const-string v1, "Attempting to bind null filter to SurfaceView!"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_b

    #@b
    .line 54
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0

    #@e
    .line 56
    :cond_e
    :try_start_e
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@10
    if-eqz v0, :cond_47

    #@12
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@14
    if-eq v0, p1, :cond_47

    #@16
    .line 57
    new-instance v0, Ljava/lang/RuntimeException;

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "Attempting to bind filter "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " to SurfaceView with another open "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, "filter "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    iget-object v2, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    const-string v2, " attached already!"

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@46
    throw v0

    #@47
    .line 63
    :cond_47
    iput-object p1, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@49
    .line 66
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@4b
    if-eqz v0, :cond_58

    #@4d
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@4f
    if-eq v0, p2, :cond_58

    #@51
    .line 67
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@53
    iget v1, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I

    #@55
    invoke-virtual {v0, v1}, Landroid/filterfw/core/GLEnvironment;->unregisterSurfaceId(I)V

    #@58
    .line 69
    :cond_58
    iput-object p2, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@5a
    .line 72
    iget v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@5c
    sget v1, Landroid/filterfw/core/FilterSurfaceView;->STATE_CREATED:I

    #@5e
    if-lt v0, v1, :cond_81

    #@60
    .line 75
    invoke-direct {p0}, Landroid/filterfw/core/FilterSurfaceView;->registerSurface()V

    #@63
    .line 78
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@65
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@68
    move-result-object v1

    #@69
    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    #@6c
    .line 81
    iget v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@6e
    sget v1, Landroid/filterfw/core/FilterSurfaceView;->STATE_INITIALIZED:I

    #@70
    if-ne v0, v1, :cond_81

    #@72
    .line 82
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@74
    invoke-virtual {p0}, Landroid/filterfw/core/FilterSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    #@77
    move-result-object v1

    #@78
    iget v2, p0, Landroid/filterfw/core/FilterSurfaceView;->mFormat:I

    #@7a
    iget v3, p0, Landroid/filterfw/core/FilterSurfaceView;->mWidth:I

    #@7c
    iget v4, p0, Landroid/filterfw/core/FilterSurfaceView;->mHeight:I

    #@7e
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V
    :try_end_81
    .catchall {:try_start_e .. :try_end_81} :catchall_b

    #@81
    .line 85
    :cond_81
    monitor-exit p0

    #@82
    return-void
.end method

.method public declared-synchronized getGLEnv()Landroid/filterfw/core/GLEnvironment;
    .registers 2

    #@0
    .prologue
    .line 96
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getSurfaceId()I
    .registers 2

    #@0
    .prologue
    .line 92
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mSurfaceId:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 6
    .parameter "holder"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 120
    monitor-enter p0

    #@1
    :try_start_1
    iput p2, p0, Landroid/filterfw/core/FilterSurfaceView;->mFormat:I

    #@3
    .line 121
    iput p3, p0, Landroid/filterfw/core/FilterSurfaceView;->mWidth:I

    #@5
    .line 122
    iput p4, p0, Landroid/filterfw/core/FilterSurfaceView;->mHeight:I

    #@7
    .line 123
    sget v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_INITIALIZED:I

    #@9
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@b
    .line 126
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 127
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@11
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    #@14
    .line 129
    :cond_14
    monitor-exit p0

    #@15
    return-void

    #@16
    .line 120
    :catchall_16
    move-exception v0

    #@17
    monitor-exit p0

    #@18
    throw v0
.end method

.method public declared-synchronized surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 101
    monitor-enter p0

    #@1
    :try_start_1
    sget v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_CREATED:I

    #@3
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@5
    .line 104
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mGLEnv:Landroid/filterfw/core/GLEnvironment;

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 105
    invoke-direct {p0}, Landroid/filterfw/core/FilterSurfaceView;->registerSurface()V

    #@c
    .line 109
    :cond_c
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 110
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@12
    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    #@15
    .line 112
    :cond_15
    monitor-exit p0

    #@16
    return-void

    #@17
    .line 101
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method public declared-synchronized surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter "holder"

    #@0
    .prologue
    .line 133
    monitor-enter p0

    #@1
    :try_start_1
    sget v0, Landroid/filterfw/core/FilterSurfaceView;->STATE_ALLOCATED:I

    #@3
    iput v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mState:I

    #@5
    .line 136
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 137
    iget-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;

    #@b
    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    #@e
    .line 141
    :cond_e
    invoke-direct {p0}, Landroid/filterfw/core/FilterSurfaceView;->unregisterSurface()V
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 142
    monitor-exit p0

    #@12
    return-void

    #@13
    .line 133
    :catchall_13
    move-exception v0

    #@14
    monitor-exit p0

    #@15
    throw v0
.end method

.method public declared-synchronized unbind()V
    .registers 2

    #@0
    .prologue
    .line 88
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    :try_start_2
    iput-object v0, p0, Landroid/filterfw/core/FilterSurfaceView;->mListener:Landroid/view/SurfaceHolder$Callback;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    #@4
    .line 89
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 88
    :catchall_6
    move-exception v0

    #@7
    monitor-exit p0

    #@8
    throw v0
.end method
