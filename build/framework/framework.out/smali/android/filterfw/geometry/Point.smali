.class public Landroid/filterfw/geometry/Point;
.super Ljava/lang/Object;
.source "Point.java"


# instance fields
.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    return-void
.end method

.method public constructor <init>(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    iput p1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@5
    .line 35
    iput p2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    .line 36
    return-void
.end method


# virtual methods
.method public IsInUnitRange()Z
    .registers 4

    #@0
    .prologue
    const/high16 v2, 0x3f80

    #@2
    const/4 v1, 0x0

    #@3
    .line 44
    iget v0, p0, Landroid/filterfw/geometry/Point;->x:F

    #@5
    cmpl-float v0, v0, v1

    #@7
    if-ltz v0, :cond_1d

    #@9
    iget v0, p0, Landroid/filterfw/geometry/Point;->x:F

    #@b
    cmpg-float v0, v0, v2

    #@d
    if-gtz v0, :cond_1d

    #@f
    iget v0, p0, Landroid/filterfw/geometry/Point;->y:F

    #@11
    cmpl-float v0, v0, v1

    #@13
    if-ltz v0, :cond_1d

    #@15
    iget v0, p0, Landroid/filterfw/geometry/Point;->y:F

    #@17
    cmpg-float v0, v0, v2

    #@19
    if-gtz v0, :cond_1d

    #@1b
    const/4 v0, 0x1

    #@1c
    :goto_1c
    return v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c
.end method

.method public distanceTo(Landroid/filterfw/geometry/Point;)F
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 77
    invoke-virtual {p1, p0}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/filterfw/geometry/Point;->length()F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public length()F
    .registers 4

    #@0
    .prologue
    .line 73
    iget v0, p0, Landroid/filterfw/geometry/Point;->x:F

    #@2
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    mul-float/2addr v0, v1

    #@5
    iget v1, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@9
    mul-float/2addr v1, v2

    #@a
    add-float/2addr v0, v1

    #@b
    float-to-double v0, v0

    #@c
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    #@f
    move-result-wide v0

    #@10
    double-to-float v0, v0

    #@11
    return v0
.end method

.method public minus(FF)Landroid/filterfw/geometry/Point;
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 57
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@2
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    sub-float/2addr v1, p1

    #@5
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    sub-float/2addr v2, p2

    #@8
    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@b
    return-object v0
.end method

.method public minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;
    .registers 4
    .parameter "point"

    #@0
    .prologue
    .line 61
    iget v0, p1, Landroid/filterfw/geometry/Point;->x:F

    #@2
    iget v1, p1, Landroid/filterfw/geometry/Point;->y:F

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/filterfw/geometry/Point;->minus(FF)Landroid/filterfw/geometry/Point;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public mult(FF)Landroid/filterfw/geometry/Point;
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 69
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@2
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    mul-float/2addr v1, p1

    #@5
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    mul-float/2addr v2, p2

    #@8
    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@b
    return-object v0
.end method

.method public normalize()Landroid/filterfw/geometry/Point;
    .registers 2

    #@0
    .prologue
    .line 85
    const/high16 v0, 0x3f80

    #@2
    invoke-virtual {p0, v0}, Landroid/filterfw/geometry/Point;->scaledTo(F)Landroid/filterfw/geometry/Point;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public plus(FF)Landroid/filterfw/geometry/Point;
    .registers 6
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 49
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@2
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    add-float/2addr v1, p1

    #@5
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    add-float/2addr v2, p2

    #@8
    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@b
    return-object v0
.end method

.method public plus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;
    .registers 4
    .parameter "point"

    #@0
    .prologue
    .line 53
    iget v0, p1, Landroid/filterfw/geometry/Point;->x:F

    #@2
    iget v1, p1, Landroid/filterfw/geometry/Point;->y:F

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/filterfw/geometry/Point;->plus(FF)Landroid/filterfw/geometry/Point;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public rotated(F)Landroid/filterfw/geometry/Point;
    .registers 10
    .parameter "radians"

    #@0
    .prologue
    .line 101
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@2
    float-to-double v1, p1

    #@3
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    #@6
    move-result-wide v1

    #@7
    iget v3, p0, Landroid/filterfw/geometry/Point;->x:F

    #@9
    float-to-double v3, v3

    #@a
    mul-double/2addr v1, v3

    #@b
    float-to-double v3, p1

    #@c
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    #@f
    move-result-wide v3

    #@10
    iget v5, p0, Landroid/filterfw/geometry/Point;->y:F

    #@12
    float-to-double v5, v5

    #@13
    mul-double/2addr v3, v5

    #@14
    sub-double/2addr v1, v3

    #@15
    double-to-float v1, v1

    #@16
    float-to-double v2, p1

    #@17
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    #@1a
    move-result-wide v2

    #@1b
    iget v4, p0, Landroid/filterfw/geometry/Point;->x:F

    #@1d
    float-to-double v4, v4

    #@1e
    mul-double/2addr v2, v4

    #@1f
    float-to-double v4, p1

    #@20
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    #@23
    move-result-wide v4

    #@24
    iget v6, p0, Landroid/filterfw/geometry/Point;->y:F

    #@26
    float-to-double v6, v6

    #@27
    mul-double/2addr v4, v6

    #@28
    add-double/2addr v2, v4

    #@29
    double-to-float v2, v2

    #@2a
    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@2d
    return-object v0
.end method

.method public rotated90(I)Landroid/filterfw/geometry/Point;
    .registers 7
    .parameter "count"

    #@0
    .prologue
    .line 89
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@2
    .line 90
    .local v1, nx:F
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@4
    .line 91
    .local v2, ny:F
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, p1, :cond_d

    #@7
    .line 92
    move v3, v1

    #@8
    .line 93
    .local v3, ox:F
    move v1, v2

    #@9
    .line 94
    neg-float v2, v3

    #@a
    .line 91
    add-int/lit8 v0, v0, 0x1

    #@c
    goto :goto_5

    #@d
    .line 96
    .end local v3           #ox:F
    :cond_d
    new-instance v4, Landroid/filterfw/geometry/Point;

    #@f
    invoke-direct {v4, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@12
    return-object v4
.end method

.method public rotatedAround(Landroid/filterfw/geometry/Point;F)Landroid/filterfw/geometry/Point;
    .registers 4
    .parameter "center"
    .parameter "radians"

    #@0
    .prologue
    .line 106
    invoke-virtual {p0, p1}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p2}, Landroid/filterfw/geometry/Point;->rotated(F)Landroid/filterfw/geometry/Point;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p1}, Landroid/filterfw/geometry/Point;->plus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public scaledTo(F)Landroid/filterfw/geometry/Point;
    .registers 3
    .parameter "length"

    #@0
    .prologue
    .line 81
    invoke-virtual {p0}, Landroid/filterfw/geometry/Point;->length()F

    #@3
    move-result v0

    #@4
    div-float v0, p1, v0

    #@6
    invoke-virtual {p0, v0}, Landroid/filterfw/geometry/Point;->times(F)Landroid/filterfw/geometry/Point;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public set(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 39
    iput p1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@2
    .line 40
    iput p2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@4
    .line 41
    return-void
.end method

.method public times(F)Landroid/filterfw/geometry/Point;
    .registers 5
    .parameter "s"

    #@0
    .prologue
    .line 65
    new-instance v0, Landroid/filterfw/geometry/Point;

    #@2
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@4
    mul-float/2addr v1, p1

    #@5
    iget v2, p0, Landroid/filterfw/geometry/Point;->y:F

    #@7
    mul-float/2addr v2, p1

    #@8
    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    #@b
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/filterfw/geometry/Point;->x:F

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/filterfw/geometry/Point;->y:F

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ")"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method
