.class public final Landroid/nfc/tech/MifareClassic;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "MifareClassic.java"


# static fields
.field public static final BLOCK_SIZE:I = 0x10

.field public static final KEY_DEFAULT:[B = null

.field public static final KEY_MIFARE_APPLICATION_DIRECTORY:[B = null

.field public static final KEY_NFC_FORUM:[B = null

.field private static final MAX_BLOCK_COUNT:I = 0x100

.field private static final MAX_SECTOR_COUNT:I = 0x28

.field public static final SIZE_1K:I = 0x400

.field public static final SIZE_2K:I = 0x800

.field public static final SIZE_4K:I = 0x1000

.field public static final SIZE_MINI:I = 0x140

.field private static final TAG:Ljava/lang/String; = "NFC"

.field public static final TYPE_CLASSIC:I = 0x0

.field public static final TYPE_PLUS:I = 0x1

.field public static final TYPE_PRO:I = 0x2

.field public static final TYPE_UNKNOWN:I = -0x1


# instance fields
.field private mIsEmulated:Z

.field private mSize:I

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x6

    #@1
    .line 79
    new-array v0, v1, [B

    #@3
    fill-array-data v0, :array_18

    #@6
    sput-object v0, Landroid/nfc/tech/MifareClassic;->KEY_DEFAULT:[B

    #@8
    .line 85
    new-array v0, v1, [B

    #@a
    fill-array-data v0, :array_20

    #@d
    sput-object v0, Landroid/nfc/tech/MifareClassic;->KEY_MIFARE_APPLICATION_DIRECTORY:[B

    #@f
    .line 91
    new-array v0, v1, [B

    #@11
    fill-array-data v0, :array_28

    #@14
    sput-object v0, Landroid/nfc/tech/MifareClassic;->KEY_NFC_FORUM:[B

    #@16
    return-void

    #@17
    .line 79
    nop

    #@18
    :array_18
    .array-data 0x1
        0xfft
        0xfft
        0xfft
        0xfft
        0xfft
        0xfft
    .end array-data

    #@1f
    .line 85
    nop

    #@20
    :array_20
    .array-data 0x1
        0xa0t
        0xa1t
        0xa2t
        0xa3t
        0xa4t
        0xa5t
    .end array-data

    #@27
    .line 91
    nop

    #@28
    :array_28
    .array-data 0x1
        0xd3t
        0xf7t
        0xd3t
        0xf7t
        0xd3t
        0xf7t
    .end array-data
.end method

.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 8
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v5, 0x400

    #@2
    const/16 v4, 0x1000

    #@4
    const/4 v3, 0x1

    #@5
    const/4 v2, 0x0

    #@6
    .line 146
    const/16 v1, 0x8

    #@8
    invoke-direct {p0, p1, v1}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@b
    .line 148
    invoke-static {p1}, Landroid/nfc/tech/NfcA;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcA;

    #@e
    move-result-object v0

    #@f
    .line 150
    .local v0, a:Landroid/nfc/tech/NfcA;
    iput-boolean v2, p0, Landroid/nfc/tech/MifareClassic;->mIsEmulated:Z

    #@11
    .line 152
    invoke-virtual {v0}, Landroid/nfc/tech/NfcA;->getSak()S

    #@14
    move-result v1

    #@15
    sparse-switch v1, :sswitch_data_6c

    #@18
    .line 199
    new-instance v1, Ljava/lang/RuntimeException;

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "Tag incorrectly enumerated as MIFARE Classic, SAK = "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v0}, Landroid/nfc/tech/NfcA;->getSak()S

    #@28
    move-result v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@34
    throw v1

    #@35
    .line 155
    :sswitch_35
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@37
    .line 156
    iput v5, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@39
    .line 202
    :goto_39
    return-void

    #@3a
    .line 159
    :sswitch_3a
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@3c
    .line 160
    const/16 v1, 0x140

    #@3e
    iput v1, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@40
    goto :goto_39

    #@41
    .line 163
    :sswitch_41
    iput v3, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@43
    .line 164
    const/16 v1, 0x800

    #@45
    iput v1, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@47
    goto :goto_39

    #@48
    .line 168
    :sswitch_48
    iput v3, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@4a
    .line 169
    iput v4, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@4c
    goto :goto_39

    #@4d
    .line 173
    :sswitch_4d
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@4f
    .line 174
    iput v4, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@51
    goto :goto_39

    #@52
    .line 177
    :sswitch_52
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@54
    .line 178
    iput v5, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@56
    .line 179
    iput-boolean v3, p0, Landroid/nfc/tech/MifareClassic;->mIsEmulated:Z

    #@58
    goto :goto_39

    #@59
    .line 182
    :sswitch_59
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@5b
    .line 183
    iput v4, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@5d
    .line 184
    iput-boolean v3, p0, Landroid/nfc/tech/MifareClassic;->mIsEmulated:Z

    #@5f
    goto :goto_39

    #@60
    .line 187
    :sswitch_60
    iput v2, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@62
    .line 188
    iput v5, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@64
    goto :goto_39

    #@65
    .line 193
    :sswitch_65
    const/4 v1, 0x2

    #@66
    iput v1, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@68
    .line 194
    iput v4, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@6a
    goto :goto_39

    #@6b
    .line 152
    nop

    #@6c
    :sswitch_data_6c
    .sparse-switch
        0x1 -> :sswitch_35
        0x8 -> :sswitch_35
        0x9 -> :sswitch_3a
        0x10 -> :sswitch_41
        0x11 -> :sswitch_48
        0x18 -> :sswitch_4d
        0x28 -> :sswitch_52
        0x38 -> :sswitch_59
        0x88 -> :sswitch_60
        0x98 -> :sswitch_65
        0xb8 -> :sswitch_65
    .end sparse-switch
.end method

.method private authenticate(I[BZ)Z
    .registers 13
    .parameter "sector"
    .parameter "key"
    .parameter "keyA"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v8, 0x6

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 370
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateSector(I)V

    #@6
    .line 371
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@9
    .line 373
    const/16 v5, 0xc

    #@b
    new-array v0, v5, [B

    #@d
    .line 376
    .local v0, cmd:[B
    if-eqz p3, :cond_35

    #@f
    .line 377
    const/16 v5, 0x60

    #@11
    aput-byte v5, v0, v4

    #@13
    .line 385
    :goto_13
    invoke-virtual {p0, p1}, Landroid/nfc/tech/MifareClassic;->sectorToBlock(I)I

    #@16
    move-result v5

    #@17
    int-to-byte v5, v5

    #@18
    aput-byte v5, v0, v3

    #@1a
    .line 388
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->getTag()Landroid/nfc/Tag;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Landroid/nfc/Tag;->getId()[B

    #@21
    move-result-object v2

    #@22
    .line 389
    .local v2, uid:[B
    array-length v5, v2

    #@23
    add-int/lit8 v5, v5, -0x4

    #@25
    const/4 v6, 0x2

    #@26
    const/4 v7, 0x4

    #@27
    invoke-static {v2, v5, v0, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2a
    .line 392
    invoke-static {p2, v4, v0, v8, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2d
    .line 395
    const/4 v5, 0x0

    #@2e
    :try_start_2e
    invoke-virtual {p0, v0, v5}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B
    :try_end_31
    .catch Landroid/nfc/TagLostException; {:try_start_2e .. :try_end_31} :catch_3a
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_31} :catch_3c

    #@31
    move-result-object v5

    #@32
    if-eqz v5, :cond_3d

    #@34
    .line 403
    :goto_34
    return v3

    #@35
    .line 379
    .end local v2           #uid:[B
    :cond_35
    const/16 v5, 0x61

    #@37
    aput-byte v5, v0, v4

    #@39
    goto :goto_13

    #@3a
    .line 398
    .restart local v2       #uid:[B
    :catch_3a
    move-exception v1

    #@3b
    .line 399
    .local v1, e:Landroid/nfc/TagLostException;
    throw v1

    #@3c
    .line 400
    .end local v1           #e:Landroid/nfc/TagLostException;
    :catch_3c
    move-exception v3

    #@3d
    :cond_3d
    move v3, v4

    #@3e
    .line 403
    goto :goto_34
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/MifareClassic;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 136
    const/16 v2, 0x8

    #@3
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 140
    :goto_9
    return-object v1

    #@a
    .line 138
    :cond_a
    :try_start_a
    new-instance v2, Landroid/nfc/tech/MifareClassic;

    #@c
    invoke-direct {v2, p0}, Landroid/nfc/tech/MifareClassic;-><init>(Landroid/nfc/Tag;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-object v1, v2

    #@10
    goto :goto_9

    #@11
    .line 139
    :catch_11
    move-exception v0

    #@12
    .line 140
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_9
.end method

.method private static validateBlock(I)V
    .registers 4
    .parameter "block"

    #@0
    .prologue
    .line 643
    if-ltz p0, :cond_6

    #@2
    const/16 v0, 0x100

    #@4
    if-lt p0, v0, :cond_1f

    #@6
    .line 644
    :cond_6
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "block out of bounds: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 646
    :cond_1f
    return-void
.end method

.method private static validateSector(I)V
    .registers 4
    .parameter "sector"

    #@0
    .prologue
    .line 636
    if-ltz p0, :cond_6

    #@2
    const/16 v0, 0x28

    #@4
    if-lt p0, v0, :cond_20

    #@6
    .line 637
    :cond_6
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "sector out of bounds: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 639
    :cond_20
    return-void
.end method

.method private static validateValueOperand(I)V
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 649
    if-gez p0, :cond_b

    #@2
    .line 650
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "value operand negative"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 652
    :cond_b
    return-void
.end method


# virtual methods
.method public authenticateSectorWithKeyA(I[B)Z
    .registers 4
    .parameter "sectorIndex"
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 339
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/nfc/tech/MifareClassic;->authenticate(I[BZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public authenticateSectorWithKeyB(I[B)Z
    .registers 4
    .parameter "sectorIndex"
    .parameter "key"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 366
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/nfc/tech/MifareClassic;->authenticate(I[BZ)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public blockToSector(I)I
    .registers 3
    .parameter "blockIndex"

    #@0
    .prologue
    .line 291
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@3
    .line 293
    const/16 v0, 0x80

    #@5
    if-ge p1, v0, :cond_a

    #@7
    .line 294
    div-int/lit8 v0, p1, 0x4

    #@9
    .line 296
    :goto_9
    return v0

    #@a
    :cond_a
    add-int/lit8 v0, p1, -0x80

    #@c
    div-int/lit8 v0, v0, 0x10

    #@e
    add-int/lit8 v0, v0, 0x20

    #@10
    goto :goto_9
.end method

.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public decrement(II)V
    .registers 6
    .parameter "blockIndex"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 500
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@3
    .line 501
    invoke-static {p2}, Landroid/nfc/tech/MifareClassic;->validateValueOperand(I)V

    #@6
    .line 502
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@9
    .line 504
    const/4 v1, 0x6

    #@a
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@d
    move-result-object v0

    #@e
    .line 505
    .local v0, cmd:Ljava/nio/ByteBuffer;
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    #@10
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@13
    .line 506
    const/16 v1, -0x40

    #@15
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@18
    .line 507
    int-to-byte v1, p1

    #@19
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 508
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@1f
    .line 510
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@22
    move-result-object v1

    #@23
    const/4 v2, 0x0

    #@24
    invoke-virtual {p0, v1, v2}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@27
    .line 511
    return-void
.end method

.method public getBlockCount()I
    .registers 2

    #@0
    .prologue
    .line 263
    iget v0, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@2
    div-int/lit8 v0, v0, 0x10

    #@4
    return v0
.end method

.method public getBlockCountInSector(I)I
    .registers 3
    .parameter "sectorIndex"

    #@0
    .prologue
    .line 274
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateSector(I)V

    #@3
    .line 276
    const/16 v0, 0x20

    #@5
    if-ge p1, v0, :cond_9

    #@7
    .line 277
    const/4 v0, 0x4

    #@8
    .line 279
    :goto_8
    return v0

    #@9
    :cond_9
    const/16 v0, 0x10

    #@b
    goto :goto_8
.end method

.method public getMaxTransceiveLength()I
    .registers 2

    #@0
    .prologue
    .line 584
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->getMaxTransceiveLengthInternal()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getSectorCount()I
    .registers 2

    #@0
    .prologue
    .line 243
    iget v0, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@2
    sparse-switch v0, :sswitch_data_12

    #@5
    .line 253
    const/4 v0, 0x0

    #@6
    :goto_6
    return v0

    #@7
    .line 245
    :sswitch_7
    const/16 v0, 0x10

    #@9
    goto :goto_6

    #@a
    .line 247
    :sswitch_a
    const/16 v0, 0x20

    #@c
    goto :goto_6

    #@d
    .line 249
    :sswitch_d
    const/16 v0, 0x28

    #@f
    goto :goto_6

    #@10
    .line 251
    :sswitch_10
    const/4 v0, 0x5

    #@11
    goto :goto_6

    #@12
    .line 243
    :sswitch_data_12
    .sparse-switch
        0x140 -> :sswitch_10
        0x400 -> :sswitch_7
        0x800 -> :sswitch_a
        0x1000 -> :sswitch_d
    .end sparse-switch
.end method

.method public getSize()I
    .registers 2

    #@0
    .prologue
    .line 224
    iget v0, p0, Landroid/nfc/tech/MifareClassic;->mSize:I

    #@2
    return v0
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getTimeout()I
    .registers 4

    #@0
    .prologue
    .line 621
    :try_start_0
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v1

    #@6
    const/16 v2, 0x8

    #@8
    invoke-interface {v1, v2}, Landroid/nfc/INfcTag;->getTimeout(I)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 624
    :goto_c
    return v1

    #@d
    .line 622
    :catch_d
    move-exception v0

    #@e
    .line 623
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "NFC"

    #@10
    const-string v2, "NFC service dead"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 624
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 213
    iget v0, p0, Landroid/nfc/tech/MifareClassic;->mType:I

    #@2
    return v0
.end method

.method public increment(II)V
    .registers 6
    .parameter "blockIndex"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 472
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@3
    .line 473
    invoke-static {p2}, Landroid/nfc/tech/MifareClassic;->validateValueOperand(I)V

    #@6
    .line 474
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@9
    .line 476
    const/4 v1, 0x6

    #@a
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@d
    move-result-object v0

    #@e
    .line 477
    .local v0, cmd:Ljava/nio/ByteBuffer;
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    #@10
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@13
    .line 478
    const/16 v1, -0x3f

    #@15
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@18
    .line 479
    int-to-byte v1, p1

    #@19
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@1c
    .line 480
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@1f
    .line 482
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@22
    move-result-object v1

    #@23
    const/4 v2, 0x0

    #@24
    invoke-virtual {p0, v1, v2}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@27
    .line 483
    return-void
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isEmulated()Z
    .registers 2

    #@0
    .prologue
    .line 234
    iget-boolean v0, p0, Landroid/nfc/tech/MifareClassic;->mIsEmulated:Z

    #@2
    return v0
.end method

.method public readBlock(I)[B
    .registers 6
    .parameter "blockIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 421
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@4
    .line 422
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@7
    .line 424
    const/4 v1, 0x2

    #@8
    new-array v0, v1, [B

    #@a
    const/16 v1, 0x30

    #@c
    aput-byte v1, v0, v3

    #@e
    const/4 v1, 0x1

    #@f
    int-to-byte v2, p1

    #@10
    aput-byte v2, v0, v1

    #@12
    .line 425
    .local v0, cmd:[B
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@15
    move-result-object v1

    #@16
    return-object v1
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method

.method public restore(I)V
    .registers 6
    .parameter "blockIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 549
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@4
    .line 550
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@7
    .line 552
    const/4 v1, 0x2

    #@8
    new-array v0, v1, [B

    #@a
    const/16 v1, -0x3e

    #@c
    aput-byte v1, v0, v3

    #@e
    const/4 v1, 0x1

    #@f
    int-to-byte v2, p1

    #@10
    aput-byte v2, v0, v1

    #@12
    .line 554
    .local v0, cmd:[B
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@15
    .line 555
    return-void
.end method

.method public sectorToBlock(I)I
    .registers 3
    .parameter "sectorIndex"

    #@0
    .prologue
    .line 308
    const/16 v0, 0x20

    #@2
    if-ge p1, v0, :cond_7

    #@4
    .line 309
    mul-int/lit8 v0, p1, 0x4

    #@6
    .line 311
    :goto_6
    return v0

    #@7
    :cond_7
    add-int/lit8 v0, p1, -0x20

    #@9
    mul-int/lit8 v0, v0, 0x10

    #@b
    add-int/lit16 v0, v0, 0x80

    #@d
    goto :goto_6
.end method

.method public setTimeout(I)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 603
    :try_start_0
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v2

    #@6
    const/16 v3, 0x8

    #@8
    invoke-interface {v2, v3, p1}, Landroid/nfc/INfcTag;->setTimeout(II)I

    #@b
    move-result v1

    #@c
    .line 604
    .local v1, err:I
    if-eqz v1, :cond_1e

    #@e
    .line 605
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v3, "The supplied timeout is not valid"

    #@12
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_16} :catch_16

    #@16
    .line 607
    .end local v1           #err:I
    :catch_16
    move-exception v0

    #@17
    .line 608
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@19
    const-string v3, "NFC service dead"

    #@1b
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    .line 610
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1e
    return-void
.end method

.method public transceive([B)[B
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 576
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public transfer(I)V
    .registers 6
    .parameter "blockIndex"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 527
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@4
    .line 528
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@7
    .line 530
    const/4 v1, 0x2

    #@8
    new-array v0, v1, [B

    #@a
    const/16 v1, -0x50

    #@c
    aput-byte v1, v0, v3

    #@e
    const/4 v1, 0x1

    #@f
    int-to-byte v2, p1

    #@10
    aput-byte v2, v0, v1

    #@12
    .line 532
    .local v0, cmd:[B
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@15
    .line 533
    return-void
.end method

.method public writeBlock(I[B)V
    .registers 7
    .parameter "blockIndex"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 443
    invoke-static {p1}, Landroid/nfc/tech/MifareClassic;->validateBlock(I)V

    #@4
    .line 444
    invoke-virtual {p0}, Landroid/nfc/tech/MifareClassic;->checkConnected()V

    #@7
    .line 445
    array-length v1, p2

    #@8
    const/16 v2, 0x10

    #@a
    if-eq v1, v2, :cond_15

    #@c
    .line 446
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@e
    const-string/jumbo v2, "must write 16-bytes"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 449
    :cond_15
    array-length v1, p2

    #@16
    add-int/lit8 v1, v1, 0x2

    #@18
    new-array v0, v1, [B

    #@1a
    .line 450
    .local v0, cmd:[B
    const/16 v1, -0x60

    #@1c
    aput-byte v1, v0, v3

    #@1e
    .line 451
    const/4 v1, 0x1

    #@1f
    int-to-byte v2, p1

    #@20
    aput-byte v2, v0, v1

    #@22
    .line 452
    const/4 v1, 0x2

    #@23
    array-length v2, p2

    #@24
    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@27
    .line 454
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareClassic;->transceive([BZ)[B

    #@2a
    .line 455
    return-void
.end method
