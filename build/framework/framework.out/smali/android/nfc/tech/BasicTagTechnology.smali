.class abstract Landroid/nfc/tech/BasicTagTechnology;
.super Ljava/lang/Object;
.source "BasicTagTechnology.java"

# interfaces
.implements Landroid/nfc/tech/TagTechnology;


# static fields
.field private static final TAG:Ljava/lang/String; = "NFC"


# instance fields
.field mIsConnected:Z

.field mSelectedTechnology:I

.field final mTag:Landroid/nfc/Tag;


# direct methods
.method constructor <init>(Landroid/nfc/Tag;I)V
    .registers 3
    .parameter "tag"
    .parameter "tech"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput-object p1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@5
    .line 40
    iput p2, p0, Landroid/nfc/tech/BasicTagTechnology;->mSelectedTechnology:I

    #@7
    .line 41
    return-void
.end method


# virtual methods
.method checkConnected()V
    .registers 3

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v0}, Landroid/nfc/Tag;->getConnectedTechnology()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mSelectedTechnology:I

    #@8
    if-ne v0, v1, :cond_13

    #@a
    iget-object v0, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@c
    invoke-virtual {v0}, Landroid/nfc/Tag;->getConnectedTechnology()I

    #@f
    move-result v0

    #@10
    const/4 v1, -0x1

    #@11
    if-ne v0, v1, :cond_1b

    #@13
    .line 52
    :cond_13
    new-instance v0, Ljava/lang/IllegalStateException;

    #@15
    const-string v1, "Call connect() first!"

    #@17
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v0

    #@1b
    .line 54
    :cond_1b
    return-void
.end method

.method public close()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 122
    :try_start_1
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@3
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@6
    move-result-object v1

    #@7
    invoke-interface {v1}, Landroid/nfc/INfcTag;->resetTimeouts()V

    #@a
    .line 123
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@c
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@f
    move-result-object v1

    #@10
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@12
    invoke-virtual {v2}, Landroid/nfc/Tag;->getServiceHandle()I

    #@15
    move-result v2

    #@16
    invoke-interface {v1, v2}, Landroid/nfc/INfcTag;->reconnect(I)I
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_31
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_19} :catch_21

    #@19
    .line 127
    iput-boolean v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@1b
    .line 128
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@1d
    invoke-virtual {v1}, Landroid/nfc/Tag;->setTechnologyDisconnected()V

    #@20
    .line 130
    :goto_20
    return-void

    #@21
    .line 124
    :catch_21
    move-exception v0

    #@22
    .line 125
    .local v0, e:Landroid/os/RemoteException;
    :try_start_22
    const-string v1, "NFC"

    #@24
    const-string v2, "NFC service dead"

    #@26
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catchall {:try_start_22 .. :try_end_29} :catchall_31

    #@29
    .line 127
    iput-boolean v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@2b
    .line 128
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2d
    invoke-virtual {v1}, Landroid/nfc/Tag;->setTechnologyDisconnected()V

    #@30
    goto :goto_20

    #@31
    .line 127
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_31
    move-exception v1

    #@32
    iput-boolean v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@34
    .line 128
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@36
    invoke-virtual {v2}, Landroid/nfc/Tag;->setTechnologyDisconnected()V

    #@39
    throw v1
.end method

.method public connect()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    :try_start_0
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v2

    #@6
    iget-object v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@8
    invoke-virtual {v3}, Landroid/nfc/Tag;->getServiceHandle()I

    #@b
    move-result v3

    #@c
    iget v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mSelectedTechnology:I

    #@e
    invoke-interface {v2, v3, v4}, Landroid/nfc/INfcTag;->connect(II)I

    #@11
    move-result v1

    #@12
    .line 76
    .local v1, errorCode:I
    if-nez v1, :cond_1f

    #@14
    .line 78
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@16
    iget v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mSelectedTechnology:I

    #@18
    invoke-virtual {v2, v3}, Landroid/nfc/Tag;->setConnectedTechnology(I)V

    #@1b
    .line 79
    const/4 v2, 0x1

    #@1c
    iput-boolean v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@1e
    .line 91
    return-void

    #@1f
    .line 80
    :cond_1f
    const/16 v2, -0x15

    #@21
    if-ne v1, v2, :cond_3b

    #@23
    .line 81
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@25
    const-string v3, "Connecting to this technology is not supported by the NFC adapter."

    #@27
    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_2b} :catch_2b

    #@2b
    .line 87
    .end local v1           #errorCode:I
    :catch_2b
    move-exception v0

    #@2c
    .line 88
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@2e
    const-string v3, "NFC service dead"

    #@30
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    .line 89
    new-instance v2, Ljava/io/IOException;

    #@35
    const-string v3, "NFC service died"

    #@37
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v2

    #@3b
    .line 85
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #errorCode:I
    :cond_3b
    :try_start_3b
    new-instance v2, Ljava/io/IOException;

    #@3d
    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    #@40
    throw v2
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_41} :catch_2b
.end method

.method getMaxTransceiveLengthInternal()I
    .registers 4

    #@0
    .prologue
    .line 135
    :try_start_0
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v1

    #@6
    iget v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mSelectedTechnology:I

    #@8
    invoke-interface {v1, v2}, Landroid/nfc/INfcTag;->getMaxTransceiveLength(I)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 138
    :goto_c
    return v1

    #@d
    .line 136
    :catch_d
    move-exception v0

    #@e
    .line 137
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "NFC"

    #@10
    const-string v2, "NFC service dead"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 138
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    return-object v0
.end method

.method public isConnected()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    iget-boolean v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 66
    :goto_5
    return v1

    #@6
    .line 63
    :cond_6
    :try_start_6
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@8
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@b
    move-result-object v2

    #@c
    iget-object v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@e
    invoke-virtual {v3}, Landroid/nfc/Tag;->getServiceHandle()I

    #@11
    move-result v3

    #@12
    invoke-interface {v2, v3}, Landroid/nfc/INfcTag;->isPresent(I)Z
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_15} :catch_17

    #@15
    move-result v1

    #@16
    goto :goto_5

    #@17
    .line 64
    :catch_17
    move-exception v0

    #@18
    .line 65
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@1a
    const-string v3, "NFC service dead"

    #@1c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_5
.end method

.method public reconnect()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 96
    iget-boolean v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@3
    if-nez v2, :cond_d

    #@5
    .line 97
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    const-string v3, "Technology not connected yet"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 101
    :cond_d
    :try_start_d
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@f
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@12
    move-result-object v2

    #@13
    iget-object v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@15
    invoke-virtual {v3}, Landroid/nfc/Tag;->getServiceHandle()I

    #@18
    move-result v3

    #@19
    invoke-interface {v2, v3}, Landroid/nfc/INfcTag;->reconnect(I)I

    #@1c
    move-result v1

    #@1d
    .line 103
    .local v1, errorCode:I
    if-eqz v1, :cond_44

    #@1f
    .line 104
    const/4 v2, 0x0

    #@20
    iput-boolean v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@22
    .line 105
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@24
    invoke-virtual {v2}, Landroid/nfc/Tag;->setTechnologyDisconnected()V

    #@27
    .line 106
    new-instance v2, Ljava/io/IOException;

    #@29
    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    #@2c
    throw v2
    :try_end_2d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_2d} :catch_2d

    #@2d
    .line 108
    .end local v1           #errorCode:I
    :catch_2d
    move-exception v0

    #@2e
    .line 109
    .local v0, e:Landroid/os/RemoteException;
    iput-boolean v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mIsConnected:Z

    #@30
    .line 110
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@32
    invoke-virtual {v2}, Landroid/nfc/Tag;->setTechnologyDisconnected()V

    #@35
    .line 111
    const-string v2, "NFC"

    #@37
    const-string v3, "NFC service dead"

    #@39
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3c
    .line 112
    new-instance v2, Ljava/io/IOException;

    #@3e
    const-string v3, "NFC service died"

    #@40
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@43
    throw v2

    #@44
    .line 114
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #errorCode:I
    :cond_44
    return-void
.end method

.method transceive([BZ)[B
    .registers 7
    .parameter "data"
    .parameter "raw"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    invoke-virtual {p0}, Landroid/nfc/tech/BasicTagTechnology;->checkConnected()V

    #@3
    .line 146
    :try_start_3
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@5
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@8
    move-result-object v2

    #@9
    iget-object v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@b
    invoke-virtual {v3}, Landroid/nfc/Tag;->getServiceHandle()I

    #@e
    move-result v3

    #@f
    invoke-interface {v2, v3, p1, p2}, Landroid/nfc/INfcTag;->transceive(I[BZ)Landroid/nfc/TransceiveResult;

    #@12
    move-result-object v1

    #@13
    .line 148
    .local v1, result:Landroid/nfc/TransceiveResult;
    if-nez v1, :cond_2e

    #@15
    .line 149
    new-instance v2, Ljava/io/IOException;

    #@17
    const-string/jumbo v3, "transceive failed"

    #@1a
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v2
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_1e} :catch_1e

    #@1e
    .line 153
    .end local v1           #result:Landroid/nfc/TransceiveResult;
    :catch_1e
    move-exception v0

    #@1f
    .line 154
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@21
    const-string v3, "NFC service dead"

    #@23
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 155
    new-instance v2, Ljava/io/IOException;

    #@28
    const-string v3, "NFC service died"

    #@2a
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v2

    #@2e
    .line 151
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #result:Landroid/nfc/TransceiveResult;
    :cond_2e
    :try_start_2e
    invoke-virtual {v1}, Landroid/nfc/TransceiveResult;->getResponseOrThrow()[B
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_31} :catch_1e

    #@31
    move-result-object v2

    #@32
    return-object v2
.end method
