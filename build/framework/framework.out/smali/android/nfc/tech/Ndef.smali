.class public final Landroid/nfc/tech/Ndef;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "Ndef.java"


# static fields
.field public static final EXTRA_NDEF_CARDSTATE:Ljava/lang/String; = "ndefcardstate"

.field public static final EXTRA_NDEF_MAXLENGTH:Ljava/lang/String; = "ndefmaxlength"

.field public static final EXTRA_NDEF_MSG:Ljava/lang/String; = "ndefmsg"

.field public static final EXTRA_NDEF_TYPE:Ljava/lang/String; = "ndeftype"

.field public static final ICODE_SLI:Ljava/lang/String; = "com.nxp.ndef.icodesli"

.field public static final MIFARE_CLASSIC:Ljava/lang/String; = "com.nxp.ndef.mifareclassic"

.field public static final NDEF_MODE_READ_ONLY:I = 0x1

.field public static final NDEF_MODE_READ_WRITE:I = 0x2

.field public static final NDEF_MODE_UNKNOWN:I = 0x3

.field public static final NFC_FORUM_TYPE_1:Ljava/lang/String; = "org.nfcforum.ndef.type1"

.field public static final NFC_FORUM_TYPE_2:Ljava/lang/String; = "org.nfcforum.ndef.type2"

.field public static final NFC_FORUM_TYPE_3:Ljava/lang/String; = "org.nfcforum.ndef.type3"

.field public static final NFC_FORUM_TYPE_4:Ljava/lang/String; = "org.nfcforum.ndef.type4"

.field private static final TAG:Ljava/lang/String; = "NFC"

.field public static final TYPE_1:I = 0x1

.field public static final TYPE_2:I = 0x2

.field public static final TYPE_3:I = 0x3

.field public static final TYPE_4:I = 0x4

.field public static final TYPE_ICODE_SLI:I = 0x66

.field public static final TYPE_MIFARE_CLASSIC:I = 0x65

.field public static final TYPE_OTHER:I = -0x1

.field public static final UNKNOWN:Ljava/lang/String; = "android.ndef.unknown"


# instance fields
.field private final mCardState:I

.field private final mMaxNdefSize:I

.field private final mNdefMsg:Landroid/nfc/NdefMessage;

.field private final mNdefType:I


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 5
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x6

    #@1
    .line 160
    invoke-direct {p0, p1, v1}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@4
    .line 161
    invoke-virtual {p1, v1}, Landroid/nfc/Tag;->getTechExtras(I)Landroid/os/Bundle;

    #@7
    move-result-object v0

    #@8
    .line 162
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_31

    #@a
    .line 163
    const-string/jumbo v1, "ndefmaxlength"

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/nfc/tech/Ndef;->mMaxNdefSize:I

    #@13
    .line 164
    const-string/jumbo v1, "ndefcardstate"

    #@16
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Landroid/nfc/tech/Ndef;->mCardState:I

    #@1c
    .line 165
    const-string/jumbo v1, "ndefmsg"

    #@1f
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Landroid/nfc/NdefMessage;

    #@25
    iput-object v1, p0, Landroid/nfc/tech/Ndef;->mNdefMsg:Landroid/nfc/NdefMessage;

    #@27
    .line 166
    const-string/jumbo v1, "ndeftype"

    #@2a
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/nfc/tech/Ndef;->mNdefType:I

    #@30
    .line 171
    return-void

    #@31
    .line 168
    :cond_31
    new-instance v1, Ljava/lang/NullPointerException;

    #@33
    const-string v2, "NDEF tech extras are null."

    #@35
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@38
    throw v1
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/Ndef;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 147
    const/4 v2, 0x6

    #@2
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 151
    :goto_8
    return-object v1

    #@9
    .line 149
    :cond_9
    :try_start_9
    new-instance v2, Landroid/nfc/tech/Ndef;

    #@b
    invoke-direct {v2, p0}, Landroid/nfc/tech/Ndef;-><init>(Landroid/nfc/Tag;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-object v1, v2

    #@f
    goto :goto_8

    #@10
    .line 150
    :catch_10
    move-exception v0

    #@11
    .line 151
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method


# virtual methods
.method public canMakeReadOnly()Z
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 349
    iget-object v3, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@3
    invoke-virtual {v3}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@6
    move-result-object v1

    #@7
    .line 350
    .local v1, tagService:Landroid/nfc/INfcTag;
    if-nez v1, :cond_a

    #@9
    .line 357
    :goto_9
    return v2

    #@a
    .line 354
    :cond_a
    :try_start_a
    iget v3, p0, Landroid/nfc/tech/Ndef;->mNdefType:I

    #@c
    invoke-interface {v1, v3}, Landroid/nfc/INfcTag;->canMakeReadOnly(I)Z
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-result v2

    #@10
    goto :goto_9

    #@11
    .line 355
    :catch_11
    move-exception v0

    #@12
    .line 356
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "NFC"

    #@14
    const-string v4, "NFC service dead"

    #@16
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@19
    goto :goto_9
.end method

.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public getCachedNdefMessage()Landroid/nfc/NdefMessage;
    .registers 2

    #@0
    .prologue
    .line 186
    iget-object v0, p0, Landroid/nfc/tech/Ndef;->mNdefMsg:Landroid/nfc/NdefMessage;

    #@2
    return-object v0
.end method

.method public getMaxSize()I
    .registers 2

    #@0
    .prologue
    .line 228
    iget v0, p0, Landroid/nfc/tech/Ndef;->mMaxNdefSize:I

    #@2
    return v0
.end method

.method public getNdefMessage()Landroid/nfc/NdefMessage;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 267
    invoke-virtual {p0}, Landroid/nfc/tech/Ndef;->checkConnected()V

    #@4
    .line 270
    :try_start_4
    iget-object v5, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@6
    invoke-virtual {v5}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@9
    move-result-object v3

    #@a
    .line 271
    .local v3, tagService:Landroid/nfc/INfcTag;
    if-nez v3, :cond_1e

    #@c
    .line 272
    new-instance v5, Ljava/io/IOException;

    #@e
    const-string v6, "Mock tags don\'t support this operation."

    #@10
    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@13
    throw v5
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_14} :catch_14

    #@14
    .line 290
    .end local v3           #tagService:Landroid/nfc/INfcTag;
    :catch_14
    move-exception v0

    #@15
    .line 291
    .local v0, e:Landroid/os/RemoteException;
    const-string v5, "NFC"

    #@17
    const-string v6, "NFC service dead"

    #@19
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    move-object v1, v4

    #@1d
    .line 292
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1d
    :goto_1d
    return-object v1

    #@1e
    .line 274
    .restart local v3       #tagService:Landroid/nfc/INfcTag;
    :cond_1e
    :try_start_1e
    iget-object v5, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@20
    invoke-virtual {v5}, Landroid/nfc/Tag;->getServiceHandle()I

    #@23
    move-result v2

    #@24
    .line 275
    .local v2, serviceHandle:I
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->isNdef(I)Z

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_3c

    #@2a
    .line 276
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->ndefRead(I)Landroid/nfc/NdefMessage;

    #@2d
    move-result-object v1

    #@2e
    .line 277
    .local v1, msg:Landroid/nfc/NdefMessage;
    if-nez v1, :cond_1d

    #@30
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->isPresent(I)Z

    #@33
    move-result v5

    #@34
    if-nez v5, :cond_1d

    #@36
    .line 278
    new-instance v5, Landroid/nfc/TagLostException;

    #@38
    invoke-direct {v5}, Landroid/nfc/TagLostException;-><init>()V

    #@3b
    throw v5

    #@3c
    .line 283
    .end local v1           #msg:Landroid/nfc/NdefMessage;
    :cond_3c
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->isPresent(I)Z

    #@3f
    move-result v5

    #@40
    if-nez v5, :cond_48

    #@42
    .line 284
    new-instance v5, Landroid/nfc/TagLostException;

    #@44
    invoke-direct {v5}, Landroid/nfc/TagLostException;-><init>()V

    #@47
    throw v5
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_48} :catch_14

    #@48
    :cond_48
    move-object v1, v4

    #@49
    .line 286
    goto :goto_1d
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 202
    iget v0, p0, Landroid/nfc/tech/Ndef;->mNdefType:I

    #@2
    sparse-switch v0, :sswitch_data_1e

    #@5
    .line 216
    const-string v0, "android.ndef.unknown"

    #@7
    :goto_7
    return-object v0

    #@8
    .line 204
    :sswitch_8
    const-string/jumbo v0, "org.nfcforum.ndef.type1"

    #@b
    goto :goto_7

    #@c
    .line 206
    :sswitch_c
    const-string/jumbo v0, "org.nfcforum.ndef.type2"

    #@f
    goto :goto_7

    #@10
    .line 208
    :sswitch_10
    const-string/jumbo v0, "org.nfcforum.ndef.type3"

    #@13
    goto :goto_7

    #@14
    .line 210
    :sswitch_14
    const-string/jumbo v0, "org.nfcforum.ndef.type4"

    #@17
    goto :goto_7

    #@18
    .line 212
    :sswitch_18
    const-string v0, "com.nxp.ndef.mifareclassic"

    #@1a
    goto :goto_7

    #@1b
    .line 214
    :sswitch_1b
    const-string v0, "com.nxp.ndef.icodesli"

    #@1d
    goto :goto_7

    #@1e
    .line 202
    :sswitch_data_1e
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_c
        0x3 -> :sswitch_10
        0x4 -> :sswitch_14
        0x65 -> :sswitch_18
        0x66 -> :sswitch_1b
    .end sparse-switch
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isWritable()Z
    .registers 3

    #@0
    .prologue
    .line 243
    iget v0, p0, Landroid/nfc/tech/Ndef;->mCardState:I

    #@2
    const/4 v1, 0x2

    #@3
    if-ne v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public makeReadOnly()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 380
    invoke-virtual {p0}, Landroid/nfc/tech/Ndef;->checkConnected()V

    #@4
    .line 383
    :try_start_4
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@6
    invoke-virtual {v4}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@9
    move-result-object v2

    #@a
    .line 384
    .local v2, tagService:Landroid/nfc/INfcTag;
    if-nez v2, :cond_d

    #@c
    .line 406
    .end local v2           #tagService:Landroid/nfc/INfcTag;
    :goto_c
    :sswitch_c
    return v3

    #@d
    .line 387
    .restart local v2       #tagService:Landroid/nfc/INfcTag;
    :cond_d
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@f
    invoke-virtual {v4}, Landroid/nfc/Tag;->getServiceHandle()I

    #@12
    move-result v4

    #@13
    invoke-interface {v2, v4}, Landroid/nfc/INfcTag;->isNdef(I)Z

    #@16
    move-result v4

    #@17
    if-eqz v4, :cond_3d

    #@19
    .line 388
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@1b
    invoke-virtual {v4}, Landroid/nfc/Tag;->getServiceHandle()I

    #@1e
    move-result v4

    #@1f
    invoke-interface {v2, v4}, Landroid/nfc/INfcTag;->ndefMakeReadOnly(I)I

    #@22
    move-result v1

    #@23
    .line 389
    .local v1, errorCode:I
    sparse-switch v1, :sswitch_data_46

    #@26
    .line 398
    new-instance v4, Ljava/io/IOException;

    #@28
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@2b
    throw v4
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_2c} :catch_2c

    #@2c
    .line 404
    .end local v1           #errorCode:I
    .end local v2           #tagService:Landroid/nfc/INfcTag;
    :catch_2c
    move-exception v0

    #@2d
    .line 405
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "NFC"

    #@2f
    const-string v5, "NFC service dead"

    #@31
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    goto :goto_c

    #@35
    .line 391
    .end local v0           #e:Landroid/os/RemoteException;
    .restart local v1       #errorCode:I
    .restart local v2       #tagService:Landroid/nfc/INfcTag;
    :sswitch_35
    const/4 v3, 0x1

    #@36
    goto :goto_c

    #@37
    .line 393
    :sswitch_37
    :try_start_37
    new-instance v4, Ljava/io/IOException;

    #@39
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@3c
    throw v4

    #@3d
    .line 402
    .end local v1           #errorCode:I
    :cond_3d
    new-instance v4, Ljava/io/IOException;

    #@3f
    const-string v5, "Tag is not ndef"

    #@41
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@44
    throw v4
    :try_end_45
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_45} :catch_2c

    #@45
    .line 389
    nop

    #@46
    :sswitch_data_46
    .sparse-switch
        -0x8 -> :sswitch_c
        -0x1 -> :sswitch_37
        0x0 -> :sswitch_35
    .end sparse-switch
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 72
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method

.method public writeNdefMessage(Landroid/nfc/NdefMessage;)V
    .registers 8
    .parameter "msg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 311
    invoke-virtual {p0}, Landroid/nfc/tech/Ndef;->checkConnected()V

    #@3
    .line 314
    :try_start_3
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@5
    invoke-virtual {v4}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@8
    move-result-object v3

    #@9
    .line 315
    .local v3, tagService:Landroid/nfc/INfcTag;
    if-nez v3, :cond_1c

    #@b
    .line 316
    new-instance v4, Ljava/io/IOException;

    #@d
    const-string v5, "Mock tags don\'t support this operation."

    #@f
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@12
    throw v4
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_13} :catch_13

    #@13
    .line 336
    .end local v3           #tagService:Landroid/nfc/INfcTag;
    :catch_13
    move-exception v0

    #@14
    .line 337
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "NFC"

    #@16
    const-string v5, "NFC service dead"

    #@18
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    .line 339
    .end local v0           #e:Landroid/os/RemoteException;
    :sswitch_1b
    return-void

    #@1c
    .line 318
    .restart local v3       #tagService:Landroid/nfc/INfcTag;
    :cond_1c
    :try_start_1c
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@1e
    invoke-virtual {v4}, Landroid/nfc/Tag;->getServiceHandle()I

    #@21
    move-result v2

    #@22
    .line 319
    .local v2, serviceHandle:I
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->isNdef(I)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_41

    #@28
    .line 320
    invoke-interface {v3, v2, p1}, Landroid/nfc/INfcTag;->ndefWrite(ILandroid/nfc/NdefMessage;)I

    #@2b
    move-result v1

    #@2c
    .line 321
    .local v1, errorCode:I
    sparse-switch v1, :sswitch_data_4a

    #@2f
    .line 330
    new-instance v4, Ljava/io/IOException;

    #@31
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@34
    throw v4

    #@35
    .line 325
    :sswitch_35
    new-instance v4, Ljava/io/IOException;

    #@37
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@3a
    throw v4

    #@3b
    .line 327
    :sswitch_3b
    new-instance v4, Landroid/nfc/FormatException;

    #@3d
    invoke-direct {v4}, Landroid/nfc/FormatException;-><init>()V

    #@40
    throw v4

    #@41
    .line 334
    .end local v1           #errorCode:I
    :cond_41
    new-instance v4, Ljava/io/IOException;

    #@43
    const-string v5, "Tag is not ndef"

    #@45
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@48
    throw v4
    :try_end_49
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_49} :catch_13

    #@49
    .line 321
    nop

    #@4a
    :sswitch_data_4a
    .sparse-switch
        -0x8 -> :sswitch_3b
        -0x1 -> :sswitch_35
        0x0 -> :sswitch_1b
    .end sparse-switch
.end method
