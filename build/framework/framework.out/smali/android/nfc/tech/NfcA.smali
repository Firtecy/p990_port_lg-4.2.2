.class public final Landroid/nfc/tech/NfcA;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "NfcA.java"


# static fields
.field public static final EXTRA_ATQA:Ljava/lang/String; = "atqa"

.field public static final EXTRA_SAK:Ljava/lang/String; = "sak"

.field private static final TAG:Ljava/lang/String; = "NFC"


# instance fields
.field private mAtqa:[B

.field private mSak:S


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 4
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 68
    invoke-direct {p0, p1, v1}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@4
    .line 69
    invoke-virtual {p1, v1}, Landroid/nfc/Tag;->getTechExtras(I)Landroid/os/Bundle;

    #@7
    move-result-object v0

    #@8
    .line 70
    .local v0, extras:Landroid/os/Bundle;
    const-string/jumbo v1, "sak"

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getShort(Ljava/lang/String;)S

    #@e
    move-result v1

    #@f
    iput-short v1, p0, Landroid/nfc/tech/NfcA;->mSak:S

    #@11
    .line 71
    const-string v1, "atqa"

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@16
    move-result-object v1

    #@17
    iput-object v1, p0, Landroid/nfc/tech/NfcA;->mAtqa:[B

    #@19
    .line 72
    return-void
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcA;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    const/4 v2, 0x1

    #@2
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 62
    :goto_8
    return-object v1

    #@9
    .line 60
    :cond_9
    :try_start_9
    new-instance v2, Landroid/nfc/tech/NfcA;

    #@b
    invoke-direct {v2, p0}, Landroid/nfc/tech/NfcA;-><init>(Landroid/nfc/Tag;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-object v1, v2

    #@f
    goto :goto_8

    #@10
    .line 61
    :catch_10
    move-exception v0

    #@11
    .line 62
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method


# virtual methods
.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 37
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 37
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public getAtqa()[B
    .registers 2

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/nfc/tech/NfcA;->mAtqa:[B

    #@2
    return-object v0
.end method

.method public getMaxTransceiveLength()I
    .registers 2

    #@0
    .prologue
    .line 128
    invoke-virtual {p0}, Landroid/nfc/tech/NfcA;->getMaxTransceiveLengthInternal()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getSak()S
    .registers 2

    #@0
    .prologue
    .line 93
    iget-short v0, p0, Landroid/nfc/tech/NfcA;->mSak:S

    #@2
    return v0
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getTimeout()I
    .registers 4

    #@0
    .prologue
    .line 165
    :try_start_0
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x1

    #@7
    invoke-interface {v1, v2}, Landroid/nfc/INfcTag;->getTimeout(I)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_a} :catch_c

    #@a
    move-result v1

    #@b
    .line 168
    :goto_b
    return v1

    #@c
    .line 166
    :catch_c
    move-exception v0

    #@d
    .line 167
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "NFC"

    #@f
    const-string v2, "NFC service dead"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    .line 168
    const/4 v1, 0x0

    #@15
    goto :goto_b
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 37
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method

.method public setTimeout(I)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 147
    :try_start_0
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v2

    #@6
    const/4 v3, 0x1

    #@7
    invoke-interface {v2, v3, p1}, Landroid/nfc/INfcTag;->setTimeout(II)I

    #@a
    move-result v1

    #@b
    .line 148
    .local v1, err:I
    if-eqz v1, :cond_1d

    #@d
    .line 149
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v3, "The supplied timeout is not valid"

    #@11
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v2
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_15} :catch_15

    #@15
    .line 151
    .end local v1           #err:I
    :catch_15
    move-exception v0

    #@16
    .line 152
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@18
    const-string v3, "NFC service dead"

    #@1a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1d
    .line 154
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1d
    return-void
.end method

.method public transceive([B)[B
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 120
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/NfcA;->transceive([BZ)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method
