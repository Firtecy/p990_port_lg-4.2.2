.class public final Landroid/nfc/tech/NfcB;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "NfcB.java"


# static fields
.field public static final EXTRA_APPDATA:Ljava/lang/String; = "appdata"

.field public static final EXTRA_PROTINFO:Ljava/lang/String; = "protinfo"


# instance fields
.field private mAppData:[B

.field private mProtInfo:[B


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 4
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 64
    invoke-direct {p0, p1, v1}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@4
    .line 65
    invoke-virtual {p1, v1}, Landroid/nfc/Tag;->getTechExtras(I)Landroid/os/Bundle;

    #@7
    move-result-object v0

    #@8
    .line 66
    .local v0, extras:Landroid/os/Bundle;
    const-string v1, "appdata"

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@d
    move-result-object v1

    #@e
    iput-object v1, p0, Landroid/nfc/tech/NfcB;->mAppData:[B

    #@10
    .line 67
    const-string/jumbo v1, "protinfo"

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    #@16
    move-result-object v1

    #@17
    iput-object v1, p0, Landroid/nfc/tech/NfcB;->mProtInfo:[B

    #@19
    .line 68
    return-void
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcB;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 54
    const/4 v2, 0x2

    #@2
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 58
    :goto_8
    return-object v1

    #@9
    .line 56
    :cond_9
    :try_start_9
    new-instance v2, Landroid/nfc/tech/NfcB;

    #@b
    invoke-direct {v2, p0}, Landroid/nfc/tech/NfcB;-><init>(Landroid/nfc/Tag;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-object v1, v2

    #@f
    goto :goto_8

    #@10
    .line 57
    :catch_10
    move-exception v0

    #@11
    .line 58
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method


# virtual methods
.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public getApplicationData()[B
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/nfc/tech/NfcB;->mAppData:[B

    #@2
    return-object v0
.end method

.method public getMaxTransceiveLength()I
    .registers 2

    #@0
    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/nfc/tech/NfcB;->getMaxTransceiveLengthInternal()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getProtocolInfo()[B
    .registers 2

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/nfc/tech/NfcB;->mProtInfo:[B

    #@2
    return-object v0
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method

.method public transceive([B)[B
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 115
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/NfcB;->transceive([BZ)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method
