.class public final Landroid/nfc/tech/MifareUltralight;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "MifareUltralight.java"


# static fields
.field public static final EXTRA_IS_UL_C:Ljava/lang/String; = "isulc"

.field private static final MAX_PAGE_COUNT:I = 0x100

.field private static final NXP_MANUFACTURER_ID:I = 0x4

.field public static final PAGE_SIZE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "NFC"

.field public static final TYPE_ULTRALIGHT:I = 0x1

.field public static final TYPE_ULTRALIGHT_C:I = 0x2

.field public static final TYPE_UNKNOWN:I = -0x1


# instance fields
.field private mType:I


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 7
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v4, 0x9

    #@2
    .line 104
    invoke-direct {p0, p1, v4}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@5
    .line 107
    invoke-static {p1}, Landroid/nfc/tech/NfcA;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcA;

    #@8
    move-result-object v0

    #@9
    .line 109
    .local v0, a:Landroid/nfc/tech/NfcA;
    const/4 v2, -0x1

    #@a
    iput v2, p0, Landroid/nfc/tech/MifareUltralight;->mType:I

    #@c
    .line 111
    invoke-virtual {v0}, Landroid/nfc/tech/NfcA;->getSak()S

    #@f
    move-result v2

    #@10
    if-nez v2, :cond_2c

    #@12
    invoke-virtual {p1}, Landroid/nfc/Tag;->getId()[B

    #@15
    move-result-object v2

    #@16
    const/4 v3, 0x0

    #@17
    aget-byte v2, v2, v3

    #@19
    const/4 v3, 0x4

    #@1a
    if-ne v2, v3, :cond_2c

    #@1c
    .line 112
    invoke-virtual {p1, v4}, Landroid/nfc/Tag;->getTechExtras(I)Landroid/os/Bundle;

    #@1f
    move-result-object v1

    #@20
    .line 113
    .local v1, extras:Landroid/os/Bundle;
    const-string/jumbo v2, "isulc"

    #@23
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_2d

    #@29
    .line 114
    const/4 v2, 0x2

    #@2a
    iput v2, p0, Landroid/nfc/tech/MifareUltralight;->mType:I

    #@2c
    .line 119
    .end local v1           #extras:Landroid/os/Bundle;
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 116
    .restart local v1       #extras:Landroid/os/Bundle;
    :cond_2d
    const/4 v2, 0x1

    #@2e
    iput v2, p0, Landroid/nfc/tech/MifareUltralight;->mType:I

    #@30
    goto :goto_2c
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/MifareUltralight;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 94
    const/16 v2, 0x9

    #@3
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 98
    :goto_9
    return-object v1

    #@a
    .line 96
    :cond_a
    :try_start_a
    new-instance v2, Landroid/nfc/tech/MifareUltralight;

    #@c
    invoke-direct {v2, p0}, Landroid/nfc/tech/MifareUltralight;-><init>(Landroid/nfc/Tag;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-object v1, v2

    #@10
    goto :goto_9

    #@11
    .line 97
    :catch_11
    move-exception v0

    #@12
    .line 98
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_9
.end method

.method private static validatePageIndex(I)V
    .registers 4
    .parameter "pageIndex"

    #@0
    .prologue
    .line 274
    if-ltz p0, :cond_6

    #@2
    const/16 v0, 0x100

    #@4
    if-lt p0, v0, :cond_20

    #@6
    .line 275
    :cond_6
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string/jumbo v2, "page out of bounds: "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 277
    :cond_20
    return-void
.end method


# virtual methods
.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public getMaxTransceiveLength()I
    .registers 2

    #@0
    .prologue
    .line 223
    invoke-virtual {p0}, Landroid/nfc/tech/MifareUltralight;->getMaxTransceiveLengthInternal()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getTimeout()I
    .registers 4

    #@0
    .prologue
    .line 261
    :try_start_0
    iget-object v1, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v1}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v1

    #@6
    const/16 v2, 0x9

    #@8
    invoke-interface {v1, v2}, Landroid/nfc/INfcTag;->getTimeout(I)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 264
    :goto_c
    return v1

    #@d
    .line 262
    :catch_d
    move-exception v0

    #@e
    .line 263
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "NFC"

    #@10
    const-string v2, "NFC service dead"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 264
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 133
    iget v0, p0, Landroid/nfc/tech/MifareUltralight;->mType:I

    #@2
    return v0
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public readPages(I)[B
    .registers 6
    .parameter "pageOffset"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 160
    invoke-static {p1}, Landroid/nfc/tech/MifareUltralight;->validatePageIndex(I)V

    #@4
    .line 161
    invoke-virtual {p0}, Landroid/nfc/tech/MifareUltralight;->checkConnected()V

    #@7
    .line 163
    const/4 v1, 0x2

    #@8
    new-array v0, v1, [B

    #@a
    const/16 v1, 0x30

    #@c
    aput-byte v1, v0, v3

    #@e
    const/4 v1, 0x1

    #@f
    int-to-byte v2, p1

    #@10
    aput-byte v2, v0, v1

    #@12
    .line 164
    .local v0, cmd:[B
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareUltralight;->transceive([BZ)[B

    #@15
    move-result-object v1

    #@16
    return-object v1
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method

.method public setTimeout(I)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 242
    :try_start_0
    iget-object v2, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@2
    invoke-virtual {v2}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@5
    move-result-object v2

    #@6
    const/16 v3, 0x9

    #@8
    invoke-interface {v2, v3, p1}, Landroid/nfc/INfcTag;->setTimeout(II)I

    #@b
    move-result v1

    #@c
    .line 244
    .local v1, err:I
    if-eqz v1, :cond_1e

    #@e
    .line 245
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@10
    const-string v3, "The supplied timeout is not valid"

    #@12
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_16} :catch_16

    #@16
    .line 247
    .end local v1           #err:I
    :catch_16
    move-exception v0

    #@17
    .line 248
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@19
    const-string v3, "NFC service dead"

    #@1b
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    .line 250
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1e
    return-void
.end method

.method public transceive([B)[B
    .registers 3
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 215
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/MifareUltralight;->transceive([BZ)[B

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public writePage(I[B)V
    .registers 7
    .parameter "pageOffset"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 185
    invoke-static {p1}, Landroid/nfc/tech/MifareUltralight;->validatePageIndex(I)V

    #@4
    .line 186
    invoke-virtual {p0}, Landroid/nfc/tech/MifareUltralight;->checkConnected()V

    #@7
    .line 188
    array-length v1, p2

    #@8
    add-int/lit8 v1, v1, 0x2

    #@a
    new-array v0, v1, [B

    #@c
    .line 189
    .local v0, cmd:[B
    const/16 v1, -0x5e

    #@e
    aput-byte v1, v0, v3

    #@10
    .line 190
    const/4 v1, 0x1

    #@11
    int-to-byte v2, p1

    #@12
    aput-byte v2, v0, v1

    #@14
    .line 191
    const/4 v1, 0x2

    #@15
    array-length v2, p2

    #@16
    invoke-static {p2, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 193
    invoke-virtual {p0, v0, v3}, Landroid/nfc/tech/MifareUltralight;->transceive([BZ)[B

    #@1c
    .line 194
    return-void
.end method
