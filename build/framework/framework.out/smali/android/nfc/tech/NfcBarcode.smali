.class public final Landroid/nfc/tech/NfcBarcode;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "NfcBarcode.java"


# static fields
.field public static final EXTRA_BARCODE_TYPE:Ljava/lang/String; = "barcodetype"

.field public static final TYPE_KOVIO:I = 0x1

.field public static final TYPE_UNKNOWN:I = -0x1


# instance fields
.field private mType:I


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 5
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v1, 0xa

    #@2
    .line 64
    invoke-direct {p0, p1, v1}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@5
    .line 65
    invoke-virtual {p1, v1}, Landroid/nfc/Tag;->getTechExtras(I)Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 66
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_14

    #@b
    .line 67
    const-string v1, "barcodetype"

    #@d
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/nfc/tech/NfcBarcode;->mType:I

    #@13
    .line 71
    return-void

    #@14
    .line 69
    :cond_14
    new-instance v1, Ljava/lang/NullPointerException;

    #@16
    const-string v2, "NfcBarcode tech extras are null."

    #@18
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v1
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/NfcBarcode;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 51
    const/16 v2, 0xa

    #@3
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_a

    #@9
    .line 55
    :goto_9
    return-object v1

    #@a
    .line 53
    :cond_a
    :try_start_a
    new-instance v2, Landroid/nfc/tech/NfcBarcode;

    #@c
    invoke-direct {v2, p0}, Landroid/nfc/tech/NfcBarcode;-><init>(Landroid/nfc/Tag;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_f} :catch_11

    #@f
    move-object v1, v2

    #@10
    goto :goto_9

    #@11
    .line 54
    :catch_11
    move-exception v0

    #@12
    .line 55
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_9
.end method


# virtual methods
.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 29
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 29
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public getBarcode()[B
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Landroid/nfc/tech/NfcBarcode;->mType:I

    #@2
    packed-switch v0, :pswitch_data_e

    #@5
    .line 99
    const/4 v0, 0x0

    #@6
    :goto_6
    return-object v0

    #@7
    .line 97
    :pswitch_7
    iget-object v0, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@9
    invoke-virtual {v0}, Landroid/nfc/Tag;->getId()[B

    #@c
    move-result-object v0

    #@d
    goto :goto_6

    #@e
    .line 94
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getType()I
    .registers 2

    #@0
    .prologue
    .line 83
    iget v0, p0, Landroid/nfc/tech/NfcBarcode;->mType:I

    #@2
    return v0
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 29
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 29
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method
