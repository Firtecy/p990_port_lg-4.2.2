.class public final Landroid/nfc/tech/NdefFormatable;
.super Landroid/nfc/tech/BasicTagTechnology;
.source "NdefFormatable.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NFC"


# direct methods
.method public constructor <init>(Landroid/nfc/Tag;)V
    .registers 3
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    const/4 v0, 0x7

    #@1
    invoke-direct {p0, p1, v0}, Landroid/nfc/tech/BasicTagTechnology;-><init>(Landroid/nfc/Tag;I)V

    #@4
    .line 74
    return-void
.end method

.method public static get(Landroid/nfc/Tag;)Landroid/nfc/tech/NdefFormatable;
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 60
    const/4 v2, 0x7

    #@2
    invoke-virtual {p0, v2}, Landroid/nfc/Tag;->hasTech(I)Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_9

    #@8
    .line 64
    :goto_8
    return-object v1

    #@9
    .line 62
    :cond_9
    :try_start_9
    new-instance v2, Landroid/nfc/tech/NdefFormatable;

    #@b
    invoke-direct {v2, p0}, Landroid/nfc/tech/NdefFormatable;-><init>(Landroid/nfc/Tag;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_e} :catch_10

    #@e
    move-object v1, v2

    #@f
    goto :goto_8

    #@10
    .line 63
    :catch_10
    move-exception v0

    #@11
    .line 64
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_8
.end method


# virtual methods
.method public bridge synthetic close()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->close()V

    #@3
    return-void
.end method

.method public bridge synthetic connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->connect()V

    #@3
    return-void
.end method

.method public format(Landroid/nfc/NdefMessage;)V
    .registers 3
    .parameter "firstMessage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/NdefFormatable;->format(Landroid/nfc/NdefMessage;Z)V

    #@4
    .line 96
    return-void
.end method

.method format(Landroid/nfc/NdefMessage;Z)V
    .registers 9
    .parameter "firstMessage"
    .parameter "makeReadOnly"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    invoke-virtual {p0}, Landroid/nfc/tech/NdefFormatable;->checkConnected()V

    #@3
    .line 125
    :try_start_3
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@5
    invoke-virtual {v4}, Landroid/nfc/Tag;->getServiceHandle()I

    #@8
    move-result v2

    #@9
    .line 126
    .local v2, serviceHandle:I
    iget-object v4, p0, Landroid/nfc/tech/BasicTagTechnology;->mTag:Landroid/nfc/Tag;

    #@b
    invoke-virtual {v4}, Landroid/nfc/Tag;->getTagService()Landroid/nfc/INfcTag;

    #@e
    move-result-object v3

    #@f
    .line 127
    .local v3, tagService:Landroid/nfc/INfcTag;
    sget-object v4, Landroid/nfc/tech/MifareClassic;->KEY_DEFAULT:[B

    #@11
    invoke-interface {v3, v2, v4}, Landroid/nfc/INfcTag;->formatNdef(I[B)I

    #@14
    move-result v1

    #@15
    .line 128
    .local v1, errorCode:I
    sparse-switch v1, :sswitch_data_76

    #@18
    .line 137
    new-instance v4, Ljava/io/IOException;

    #@1a
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@1d
    throw v4
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_1e} :catch_1e

    #@1e
    .line 175
    .end local v1           #errorCode:I
    .end local v2           #serviceHandle:I
    .end local v3           #tagService:Landroid/nfc/INfcTag;
    :catch_1e
    move-exception v0

    #@1f
    .line 176
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "NFC"

    #@21
    const-string v5, "NFC service dead"

    #@23
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 178
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_26
    :sswitch_26
    return-void

    #@27
    .line 132
    .restart local v1       #errorCode:I
    .restart local v2       #serviceHandle:I
    .restart local v3       #tagService:Landroid/nfc/INfcTag;
    :sswitch_27
    :try_start_27
    new-instance v4, Ljava/io/IOException;

    #@29
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@2c
    throw v4

    #@2d
    .line 134
    :sswitch_2d
    new-instance v4, Landroid/nfc/FormatException;

    #@2f
    invoke-direct {v4}, Landroid/nfc/FormatException;-><init>()V

    #@32
    throw v4

    #@33
    .line 140
    :sswitch_33
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->isNdef(I)Z

    #@36
    move-result v4

    #@37
    if-nez v4, :cond_3f

    #@39
    .line 141
    new-instance v4, Ljava/io/IOException;

    #@3b
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@3e
    throw v4

    #@3f
    .line 145
    :cond_3f
    if-eqz p1, :cond_5a

    #@41
    .line 146
    invoke-interface {v3, v2, p1}, Landroid/nfc/INfcTag;->ndefWrite(ILandroid/nfc/NdefMessage;)I

    #@44
    move-result v1

    #@45
    .line 147
    sparse-switch v1, :sswitch_data_84

    #@48
    .line 156
    new-instance v4, Ljava/io/IOException;

    #@4a
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@4d
    throw v4

    #@4e
    .line 151
    :sswitch_4e
    new-instance v4, Ljava/io/IOException;

    #@50
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@53
    throw v4

    #@54
    .line 153
    :sswitch_54
    new-instance v4, Landroid/nfc/FormatException;

    #@56
    invoke-direct {v4}, Landroid/nfc/FormatException;-><init>()V

    #@59
    throw v4

    #@5a
    .line 161
    :cond_5a
    :sswitch_5a
    if-eqz p2, :cond_26

    #@5c
    .line 162
    invoke-interface {v3, v2}, Landroid/nfc/INfcTag;->ndefMakeReadOnly(I)I

    #@5f
    move-result v1

    #@60
    .line 163
    sparse-switch v1, :sswitch_data_92

    #@63
    .line 172
    new-instance v4, Ljava/io/IOException;

    #@65
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@68
    throw v4

    #@69
    .line 167
    :sswitch_69
    new-instance v4, Ljava/io/IOException;

    #@6b
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@6e
    throw v4

    #@6f
    .line 169
    :sswitch_6f
    new-instance v4, Ljava/io/IOException;

    #@71
    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    #@74
    throw v4
    :try_end_75
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_75} :catch_1e

    #@75
    .line 128
    nop

    #@76
    :sswitch_data_76
    .sparse-switch
        -0x8 -> :sswitch_2d
        -0x1 -> :sswitch_27
        0x0 -> :sswitch_33
    .end sparse-switch

    #@84
    .line 147
    :sswitch_data_84
    .sparse-switch
        -0x8 -> :sswitch_54
        -0x1 -> :sswitch_4e
        0x0 -> :sswitch_5a
    .end sparse-switch

    #@92
    .line 163
    :sswitch_data_92
    .sparse-switch
        -0x8 -> :sswitch_6f
        -0x1 -> :sswitch_69
        0x0 -> :sswitch_26
    .end sparse-switch
.end method

.method public formatReadOnly(Landroid/nfc/NdefMessage;)V
    .registers 3
    .parameter "firstMessage"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/nfc/tech/NdefFormatable;->format(Landroid/nfc/NdefMessage;Z)V

    #@4
    .line 118
    return-void
.end method

.method public bridge synthetic getTag()Landroid/nfc/Tag;
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->getTag()Landroid/nfc/Tag;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->isConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic reconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 47
    invoke-super {p0}, Landroid/nfc/tech/BasicTagTechnology;->reconnect()V

    #@3
    return-void
.end method
