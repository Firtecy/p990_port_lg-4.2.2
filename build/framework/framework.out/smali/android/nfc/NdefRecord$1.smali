.class final Landroid/nfc/NdefRecord$1;
.super Ljava/lang/Object;
.source "NdefRecord.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/nfc/NdefRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/nfc/NdefRecord;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 950
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/nfc/NdefRecord;
    .registers 10
    .parameter "in"

    #@0
    .prologue
    .line 953
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v7

    #@4
    int-to-short v4, v7

    #@5
    .line 954
    .local v4, tnf:S
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v6

    #@9
    .line 955
    .local v6, typeLength:I
    new-array v5, v6, [B

    #@b
    .line 956
    .local v5, type:[B
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readByteArray([B)V

    #@e
    .line 957
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v1

    #@12
    .line 958
    .local v1, idLength:I
    new-array v0, v1, [B

    #@14
    .line 959
    .local v0, id:[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    #@17
    .line 960
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    .line 961
    .local v3, payloadLength:I
    new-array v2, v3, [B

    #@1d
    .line 962
    .local v2, payload:[B
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readByteArray([B)V

    #@20
    .line 964
    new-instance v7, Landroid/nfc/NdefRecord;

    #@22
    invoke-direct {v7, v4, v5, v0, v2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@25
    return-object v7
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 950
    invoke-virtual {p0, p1}, Landroid/nfc/NdefRecord$1;->createFromParcel(Landroid/os/Parcel;)Landroid/nfc/NdefRecord;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/nfc/NdefRecord;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 968
    new-array v0, p1, [Landroid/nfc/NdefRecord;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 950
    invoke-virtual {p0, p1}, Landroid/nfc/NdefRecord$1;->newArray(I)[Landroid/nfc/NdefRecord;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
