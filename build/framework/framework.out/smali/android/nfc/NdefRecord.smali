.class public final Landroid/nfc/NdefRecord;
.super Ljava/lang/Object;
.source "NdefRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/nfc/NdefRecord;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_BYTE_ARRAY:[B = null

.field private static final FLAG_CF:B = 0x20t

.field private static final FLAG_IL:B = 0x8t

.field private static final FLAG_MB:B = -0x80t

.field private static final FLAG_ME:B = 0x40t

.field private static final FLAG_SR:B = 0x10t

.field private static final MAX_PAYLOAD_SIZE:I = 0xa00000

.field public static final RTD_ALTERNATIVE_CARRIER:[B = null

.field public static final RTD_ANDROID_APP:[B = null

.field public static final RTD_HANDOVER_CARRIER:[B = null

.field public static final RTD_HANDOVER_REQUEST:[B = null

.field public static final RTD_HANDOVER_SELECT:[B = null

.field public static final RTD_SMART_POSTER:[B = null

.field public static final RTD_TEXT:[B = null

.field public static final RTD_URI:[B = null

.field public static final TNF_ABSOLUTE_URI:S = 0x3s

.field public static final TNF_EMPTY:S = 0x0s

.field public static final TNF_EXTERNAL_TYPE:S = 0x4s

.field public static final TNF_MIME_MEDIA:S = 0x2s

.field public static final TNF_RESERVED:S = 0x7s

.field public static final TNF_UNCHANGED:S = 0x6s

.field public static final TNF_UNKNOWN:S = 0x5s

.field public static final TNF_WELL_KNOWN:S = 0x1s

.field private static final URI_PREFIX_MAP:[Ljava/lang/String;


# instance fields
.field private final mId:[B

.field private final mPayload:[B

.field private final mTnf:S

.field private final mType:[B


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x2

    #@3
    .line 173
    new-array v0, v4, [B

    #@5
    const/16 v1, 0x54

    #@7
    aput-byte v1, v0, v3

    #@9
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_TEXT:[B

    #@b
    .line 179
    new-array v0, v4, [B

    #@d
    const/16 v1, 0x55

    #@f
    aput-byte v1, v0, v3

    #@11
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_URI:[B

    #@13
    .line 185
    new-array v0, v2, [B

    #@15
    fill-array-data v0, :array_12a

    #@18
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_SMART_POSTER:[B

    #@1a
    .line 191
    new-array v0, v2, [B

    #@1c
    fill-array-data v0, :array_130

    #@1f
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_ALTERNATIVE_CARRIER:[B

    #@21
    .line 197
    new-array v0, v2, [B

    #@23
    fill-array-data v0, :array_136

    #@26
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_HANDOVER_CARRIER:[B

    #@28
    .line 203
    new-array v0, v2, [B

    #@2a
    fill-array-data v0, :array_13c

    #@2d
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_HANDOVER_REQUEST:[B

    #@2f
    .line 209
    new-array v0, v2, [B

    #@31
    fill-array-data v0, :array_142

    #@34
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_HANDOVER_SELECT:[B

    #@36
    .line 223
    const-string v0, "android.com:pkg"

    #@38
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    #@3b
    move-result-object v0

    #@3c
    sput-object v0, Landroid/nfc/NdefRecord;->RTD_ANDROID_APP:[B

    #@3e
    .line 236
    const/16 v0, 0x23

    #@40
    new-array v0, v0, [Ljava/lang/String;

    #@42
    const-string v1, ""

    #@44
    aput-object v1, v0, v3

    #@46
    const-string v1, "http://www."

    #@48
    aput-object v1, v0, v4

    #@4a
    const-string v1, "https://www."

    #@4c
    aput-object v1, v0, v2

    #@4e
    const/4 v1, 0x3

    #@4f
    const-string v2, "http://"

    #@51
    aput-object v2, v0, v1

    #@53
    const/4 v1, 0x4

    #@54
    const-string v2, "https://"

    #@56
    aput-object v2, v0, v1

    #@58
    const/4 v1, 0x5

    #@59
    const-string/jumbo v2, "tel:"

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/4 v1, 0x6

    #@5f
    const-string/jumbo v2, "mailto:"

    #@62
    aput-object v2, v0, v1

    #@64
    const/4 v1, 0x7

    #@65
    const-string v2, "ftp://anonymous:anonymous@"

    #@67
    aput-object v2, v0, v1

    #@69
    const/16 v1, 0x8

    #@6b
    const-string v2, "ftp://ftp."

    #@6d
    aput-object v2, v0, v1

    #@6f
    const/16 v1, 0x9

    #@71
    const-string v2, "ftps://"

    #@73
    aput-object v2, v0, v1

    #@75
    const/16 v1, 0xa

    #@77
    const-string/jumbo v2, "sftp://"

    #@7a
    aput-object v2, v0, v1

    #@7c
    const/16 v1, 0xb

    #@7e
    const-string/jumbo v2, "smb://"

    #@81
    aput-object v2, v0, v1

    #@83
    const/16 v1, 0xc

    #@85
    const-string/jumbo v2, "nfs://"

    #@88
    aput-object v2, v0, v1

    #@8a
    const/16 v1, 0xd

    #@8c
    const-string v2, "ftp://"

    #@8e
    aput-object v2, v0, v1

    #@90
    const/16 v1, 0xe

    #@92
    const-string v2, "dav://"

    #@94
    aput-object v2, v0, v1

    #@96
    const/16 v1, 0xf

    #@98
    const-string/jumbo v2, "news:"

    #@9b
    aput-object v2, v0, v1

    #@9d
    const/16 v1, 0x10

    #@9f
    const-string/jumbo v2, "telnet://"

    #@a2
    aput-object v2, v0, v1

    #@a4
    const/16 v1, 0x11

    #@a6
    const-string v2, "imap:"

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/16 v1, 0x12

    #@ac
    const-string/jumbo v2, "rtsp://"

    #@af
    aput-object v2, v0, v1

    #@b1
    const/16 v1, 0x13

    #@b3
    const-string/jumbo v2, "urn:"

    #@b6
    aput-object v2, v0, v1

    #@b8
    const/16 v1, 0x14

    #@ba
    const-string/jumbo v2, "pop:"

    #@bd
    aput-object v2, v0, v1

    #@bf
    const/16 v1, 0x15

    #@c1
    const-string/jumbo v2, "sip:"

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0x16

    #@c8
    const-string/jumbo v2, "sips:"

    #@cb
    aput-object v2, v0, v1

    #@cd
    const/16 v1, 0x17

    #@cf
    const-string/jumbo v2, "tftp:"

    #@d2
    aput-object v2, v0, v1

    #@d4
    const/16 v1, 0x18

    #@d6
    const-string v2, "btspp://"

    #@d8
    aput-object v2, v0, v1

    #@da
    const/16 v1, 0x19

    #@dc
    const-string v2, "btl2cap://"

    #@de
    aput-object v2, v0, v1

    #@e0
    const/16 v1, 0x1a

    #@e2
    const-string v2, "btgoep://"

    #@e4
    aput-object v2, v0, v1

    #@e6
    const/16 v1, 0x1b

    #@e8
    const-string/jumbo v2, "tcpobex://"

    #@eb
    aput-object v2, v0, v1

    #@ed
    const/16 v1, 0x1c

    #@ef
    const-string v2, "irdaobex://"

    #@f1
    aput-object v2, v0, v1

    #@f3
    const/16 v1, 0x1d

    #@f5
    const-string v2, "file://"

    #@f7
    aput-object v2, v0, v1

    #@f9
    const/16 v1, 0x1e

    #@fb
    const-string/jumbo v2, "urn:epc:id:"

    #@fe
    aput-object v2, v0, v1

    #@100
    const/16 v1, 0x1f

    #@102
    const-string/jumbo v2, "urn:epc:tag:"

    #@105
    aput-object v2, v0, v1

    #@107
    const/16 v1, 0x20

    #@109
    const-string/jumbo v2, "urn:epc:pat:"

    #@10c
    aput-object v2, v0, v1

    #@10e
    const/16 v1, 0x21

    #@110
    const-string/jumbo v2, "urn:epc:raw:"

    #@113
    aput-object v2, v0, v1

    #@115
    const/16 v1, 0x22

    #@117
    const-string/jumbo v2, "urn:epc:"

    #@11a
    aput-object v2, v0, v1

    #@11c
    sput-object v0, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@11e
    .line 276
    new-array v0, v3, [B

    #@120
    sput-object v0, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@122
    .line 949
    new-instance v0, Landroid/nfc/NdefRecord$1;

    #@124
    invoke-direct {v0}, Landroid/nfc/NdefRecord$1;-><init>()V

    #@127
    sput-object v0, Landroid/nfc/NdefRecord;->CREATOR:Landroid/os/Parcelable$Creator;

    #@129
    return-void

    #@12a
    .line 185
    :array_12a
    .array-data 0x1
        0x53t
        0x70t
    .end array-data

    #@12f
    .line 191
    nop

    #@130
    :array_130
    .array-data 0x1
        0x61t
        0x63t
    .end array-data

    #@135
    .line 197
    nop

    #@136
    :array_136
    .array-data 0x1
        0x48t
        0x63t
    .end array-data

    #@13b
    .line 203
    nop

    #@13c
    :array_13c
    .array-data 0x1
        0x48t
        0x72t
    .end array-data

    #@141
    .line 209
    nop

    #@142
    :array_142
    .array-data 0x1
        0x48t
        0x73t
    .end array-data
.end method

.method public constructor <init>(S[B[B[B)V
    .registers 7
    .parameter "tnf"
    .parameter "type"
    .parameter "id"
    .parameter "payload"

    #@0
    .prologue
    .line 515
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 517
    if-nez p2, :cond_7

    #@5
    sget-object p2, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@7
    .line 518
    :cond_7
    if-nez p3, :cond_b

    #@9
    sget-object p3, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@b
    .line 519
    :cond_b
    if-nez p4, :cond_f

    #@d
    sget-object p4, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@f
    .line 521
    :cond_f
    invoke-static {p1, p2, p3, p4}, Landroid/nfc/NdefRecord;->validateTnf(S[B[B[B)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 522
    .local v0, message:Ljava/lang/String;
    if-eqz v0, :cond_1b

    #@15
    .line 523
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@17
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v1

    #@1b
    .line 526
    :cond_1b
    iput-short p1, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@1d
    .line 527
    iput-object p2, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@1f
    .line 528
    iput-object p3, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@21
    .line 529
    iput-object p4, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@23
    .line 530
    return-void
.end method

.method public constructor <init>([B)V
    .registers 6
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/nfc/FormatException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 548
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 549
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@7
    move-result-object v0

    #@8
    .line 550
    .local v0, buffer:Ljava/nio/ByteBuffer;
    const/4 v2, 0x1

    #@9
    invoke-static {v0, v2}, Landroid/nfc/NdefRecord;->parse(Ljava/nio/ByteBuffer;Z)[Landroid/nfc/NdefRecord;

    #@c
    move-result-object v1

    #@d
    .line 552
    .local v1, rs:[Landroid/nfc/NdefRecord;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    #@10
    move-result v2

    #@11
    if-lez v2, :cond_1b

    #@13
    .line 553
    new-instance v2, Landroid/nfc/FormatException;

    #@15
    const-string v3, "data too long"

    #@17
    invoke-direct {v2, v3}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 556
    :cond_1b
    aget-object v2, v1, v3

    #@1d
    iget-short v2, v2, Landroid/nfc/NdefRecord;->mTnf:S

    #@1f
    iput-short v2, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@21
    .line 557
    aget-object v2, v1, v3

    #@23
    iget-object v2, v2, Landroid/nfc/NdefRecord;->mType:[B

    #@25
    iput-object v2, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@27
    .line 558
    aget-object v2, v1, v3

    #@29
    iget-object v2, v2, Landroid/nfc/NdefRecord;->mId:[B

    #@2b
    iput-object v2, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@2d
    .line 559
    aget-object v2, v1, v3

    #@2f
    iget-object v2, v2, Landroid/nfc/NdefRecord;->mPayload:[B

    #@31
    iput-object v2, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@33
    .line 560
    return-void
.end method

.method private static bytesToString([B)Ljava/lang/StringBuilder;
    .registers 10
    .parameter "bs"

    #@0
    .prologue
    .line 1009
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 1010
    .local v4, s:Ljava/lang/StringBuilder;
    move-object v0, p0

    #@6
    .local v0, arr$:[B
    array-length v3, v0

    #@7
    .local v3, len$:I
    const/4 v2, 0x0

    #@8
    .local v2, i$:I
    :goto_8
    if-ge v2, v3, :cond_22

    #@a
    aget-byte v1, v0, v2

    #@c
    .line 1011
    .local v1, b:B
    const-string v5, "%02X"

    #@e
    const/4 v6, 0x1

    #@f
    new-array v6, v6, [Ljava/lang/Object;

    #@11
    const/4 v7, 0x0

    #@12
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@15
    move-result-object v8

    #@16
    aput-object v8, v6, v7

    #@18
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 1010
    add-int/lit8 v2, v2, 0x1

    #@21
    goto :goto_8

    #@22
    .line 1013
    .end local v1           #b:B
    :cond_22
    return-object v4
.end method

.method public static createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;
    .registers 6
    .parameter "packageName"

    #@0
    .prologue
    .line 310
    if-nez p0, :cond_b

    #@2
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "packageName is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 311
    :cond_b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_1a

    #@11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string/jumbo v1, "packageName is empty"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 313
    :cond_1a
    new-instance v0, Landroid/nfc/NdefRecord;

    #@1c
    const/4 v1, 0x4

    #@1d
    sget-object v2, Landroid/nfc/NdefRecord;->RTD_ANDROID_APP:[B

    #@1f
    const/4 v3, 0x0

    #@20
    sget-object v4, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@22
    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@25
    move-result-object v4

    #@26
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@29
    return-object v0
.end method

.method public static createExternal(Ljava/lang/String;Ljava/lang/String;[B)Landroid/nfc/NdefRecord;
    .registers 9
    .parameter "domain"
    .parameter "type"
    .parameter "data"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 456
    if-nez p0, :cond_b

    #@3
    new-instance v3, Ljava/lang/NullPointerException;

    #@5
    const-string v4, "domain is null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 457
    :cond_b
    if-nez p1, :cond_16

    #@d
    new-instance v3, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v4, "type is null"

    #@12
    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v3

    #@16
    .line 459
    :cond_16
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@1f
    move-result-object p0

    #@20
    .line 460
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@29
    move-result-object p1

    #@2a
    .line 462
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@2d
    move-result v3

    #@2e
    if-nez v3, :cond_38

    #@30
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@32
    const-string v4, "domain is empty"

    #@34
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@37
    throw v3

    #@38
    .line 463
    :cond_38
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3b
    move-result v3

    #@3c
    if-nez v3, :cond_47

    #@3e
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@40
    const-string/jumbo v4, "type is empty"

    #@43
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v3

    #@47
    .line 465
    :cond_47
    sget-object v3, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@49
    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@4c
    move-result-object v1

    #@4d
    .line 466
    .local v1, byteDomain:[B
    sget-object v3, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@4f
    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@52
    move-result-object v2

    #@53
    .line 467
    .local v2, byteType:[B
    array-length v3, v1

    #@54
    add-int/lit8 v3, v3, 0x1

    #@56
    array-length v4, v2

    #@57
    add-int/2addr v3, v4

    #@58
    new-array v0, v3, [B

    #@5a
    .line 468
    .local v0, b:[B
    array-length v3, v1

    #@5b
    invoke-static {v1, v5, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5e
    .line 469
    array-length v3, v1

    #@5f
    const/16 v4, 0x3a

    #@61
    aput-byte v4, v0, v3

    #@63
    .line 470
    array-length v3, v1

    #@64
    add-int/lit8 v3, v3, 0x1

    #@66
    array-length v4, v2

    #@67
    invoke-static {v2, v5, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@6a
    .line 472
    new-instance v3, Landroid/nfc/NdefRecord;

    #@6c
    const/4 v4, 0x4

    #@6d
    const/4 v5, 0x0

    #@6e
    invoke-direct {v3, v4, v0, v5, p2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@71
    return-object v3
.end method

.method public static createMime(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;
    .registers 7
    .parameter "mimeType"
    .parameter "mimeData"

    #@0
    .prologue
    .line 410
    if-nez p0, :cond_b

    #@2
    new-instance v2, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v3, "mimeType is null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 415
    :cond_b
    invoke-static {p0}, Landroid/content/Intent;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object p0

    #@f
    .line 416
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@12
    move-result v2

    #@13
    if-nez v2, :cond_1e

    #@15
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@17
    const-string/jumbo v3, "mimeType is empty"

    #@1a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v2

    #@1e
    .line 417
    :cond_1e
    const/16 v2, 0x2f

    #@20
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    #@23
    move-result v0

    #@24
    .line 418
    .local v0, slashIndex:I
    if-nez v0, :cond_2f

    #@26
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@28
    const-string/jumbo v3, "mimeType must have major type"

    #@2b
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2e
    throw v2

    #@2f
    .line 419
    :cond_2f
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@32
    move-result v2

    #@33
    add-int/lit8 v2, v2, -0x1

    #@35
    if-ne v0, v2, :cond_40

    #@37
    .line 420
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@39
    const-string/jumbo v3, "mimeType must have minor type"

    #@3c
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v2

    #@40
    .line 425
    :cond_40
    sget-object v2, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@42
    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@45
    move-result-object v1

    #@46
    .line 426
    .local v1, typeBytes:[B
    new-instance v2, Landroid/nfc/NdefRecord;

    #@48
    const/4 v3, 0x2

    #@49
    const/4 v4, 0x0

    #@4a
    invoke-direct {v2, v3, v1, v4, p1}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@4d
    return-object v2
.end method

.method public static createUri(Landroid/net/Uri;)Landroid/nfc/NdefRecord;
    .registers 10
    .parameter "uri"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 339
    if-nez p0, :cond_d

    #@4
    new-instance v5, Ljava/lang/NullPointerException;

    #@6
    const-string/jumbo v6, "uri is null"

    #@9
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v5

    #@d
    .line 341
    :cond_d
    invoke-virtual {p0}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    #@10
    move-result-object p0

    #@11
    .line 342
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 343
    .local v4, uriString:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@18
    move-result v5

    #@19
    if-nez v5, :cond_24

    #@1b
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string/jumbo v6, "uri is empty"

    #@20
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v5

    #@24
    .line 345
    :cond_24
    const/4 v1, 0x0

    #@25
    .line 346
    .local v1, prefix:B
    const/4 v0, 0x1

    #@26
    .local v0, i:I
    :goto_26
    sget-object v5, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@28
    array-length v5, v5

    #@29
    if-ge v0, v5, :cond_42

    #@2b
    .line 347
    sget-object v5, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@2d
    aget-object v5, v5, v0

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@32
    move-result v5

    #@33
    if-eqz v5, :cond_5c

    #@35
    .line 348
    int-to-byte v1, v0

    #@36
    .line 349
    sget-object v5, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@38
    aget-object v5, v5, v0

    #@3a
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@3d
    move-result v5

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@41
    move-result-object v4

    #@42
    .line 353
    :cond_42
    sget-object v5, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    #@47
    move-result-object v3

    #@48
    .line 354
    .local v3, uriBytes:[B
    array-length v5, v3

    #@49
    add-int/lit8 v5, v5, 0x1

    #@4b
    new-array v2, v5, [B

    #@4d
    .line 355
    .local v2, recordBytes:[B
    aput-byte v1, v2, v6

    #@4f
    .line 356
    array-length v5, v3

    #@50
    invoke-static {v3, v6, v2, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@53
    .line 357
    new-instance v5, Landroid/nfc/NdefRecord;

    #@55
    sget-object v6, Landroid/nfc/NdefRecord;->RTD_URI:[B

    #@57
    const/4 v7, 0x0

    #@58
    invoke-direct {v5, v8, v6, v7, v2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@5b
    return-object v5

    #@5c
    .line 346
    .end local v2           #recordBytes:[B
    .end local v3           #uriBytes:[B
    :cond_5c
    add-int/lit8 v0, v0, 0x1

    #@5e
    goto :goto_26
.end method

.method public static createUri(Ljava/lang/String;)Landroid/nfc/NdefRecord;
    .registers 2
    .parameter "uriString"

    #@0
    .prologue
    .line 382
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/nfc/NdefRecord;->createUri(Landroid/net/Uri;)Landroid/nfc/NdefRecord;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private static ensureSanePayloadSize(J)V
    .registers 5
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 847
    const-wide/32 v0, 0xa00000

    #@3
    cmp-long v0, p0, v0

    #@5
    if-lez v0, :cond_2d

    #@7
    .line 848
    new-instance v0, Landroid/nfc/FormatException;

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string/jumbo v2, "payload above max limit: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, " > "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const/high16 v2, 0xa0

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {v0, v1}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v0

    #@2d
    .line 851
    :cond_2d
    return-void
.end method

.method static parse(Ljava/nio/ByteBuffer;Z)[Landroid/nfc/NdefRecord;
    .registers 33
    .parameter "buffer"
    .parameter "ignoreMbMe"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 744
    new-instance v22, Ljava/util/ArrayList;

    #@2
    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 747
    .local v22, records:Ljava/util/List;,"Ljava/util/List<Landroid/nfc/NdefRecord;>;"
    const/16 v25, 0x0

    #@7
    .line 748
    .local v25, type:[B
    const/4 v12, 0x0

    #@8
    .line 749
    .local v12, id:[B
    const/16 v19, 0x0

    #@a
    .line 750
    .local v19, payload:[B
    :try_start_a
    new-instance v6, Ljava/util/ArrayList;

    #@c
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@f
    .line 751
    .local v6, chunks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/4 v15, 0x0

    #@10
    .line 752
    .local v15, inChunk:Z
    const/4 v5, -0x1

    #@11
    .line 753
    .local v5, chunkTnf:S
    const/16 v17, 0x0

    #@13
    .line 755
    .local v17, me:Z
    :cond_13
    :goto_13
    if-nez v17, :cond_1de

    #@15
    .line 756
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@18
    move-result v9

    #@19
    .line 758
    .local v9, flag:B
    and-int/lit8 v27, v9, -0x80

    #@1b
    if-eqz v27, :cond_5d

    #@1d
    const/16 v16, 0x1

    #@1f
    .line 759
    .local v16, mb:Z
    :goto_1f
    and-int/lit8 v27, v9, 0x40

    #@21
    if-eqz v27, :cond_60

    #@23
    const/16 v17, 0x1

    #@25
    .line 760
    :goto_25
    and-int/lit8 v27, v9, 0x20

    #@27
    if-eqz v27, :cond_63

    #@29
    const/4 v4, 0x1

    #@2a
    .line 761
    .local v4, cf:Z
    :goto_2a
    and-int/lit8 v27, v9, 0x10

    #@2c
    if-eqz v27, :cond_65

    #@2e
    const/16 v23, 0x1

    #@30
    .line 762
    .local v23, sr:Z
    :goto_30
    and-int/lit8 v27, v9, 0x8

    #@32
    if-eqz v27, :cond_68

    #@34
    const/4 v14, 0x1

    #@35
    .line 763
    .local v14, il:Z
    :goto_35
    and-int/lit8 v27, v9, 0x7

    #@37
    move/from16 v0, v27

    #@39
    int-to-short v0, v0

    #@3a
    move/from16 v24, v0

    #@3c
    .line 765
    .local v24, tnf:S
    if-nez v16, :cond_6a

    #@3e
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@41
    move-result v27

    #@42
    if-nez v27, :cond_6a

    #@44
    if-nez v15, :cond_6a

    #@46
    if-nez p1, :cond_6a

    #@48
    .line 766
    new-instance v27, Landroid/nfc/FormatException;

    #@4a
    const-string v28, "expected MB flag"

    #@4c
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@4f
    throw v27
    :try_end_50
    .catch Ljava/nio/BufferUnderflowException; {:try_start_a .. :try_end_50} :catch_50

    #@50
    .line 840
    .end local v4           #cf:Z
    .end local v5           #chunkTnf:S
    .end local v6           #chunks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    .end local v9           #flag:B
    .end local v14           #il:Z
    .end local v15           #inChunk:Z
    .end local v16           #mb:Z
    .end local v17           #me:Z
    .end local v23           #sr:Z
    .end local v24           #tnf:S
    :catch_50
    move-exception v7

    #@51
    .line 841
    .local v7, e:Ljava/nio/BufferUnderflowException;
    new-instance v27, Landroid/nfc/FormatException;

    #@53
    const-string v28, "expected more data"

    #@55
    move-object/from16 v0, v27

    #@57
    move-object/from16 v1, v28

    #@59
    invoke-direct {v0, v1, v7}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5c
    throw v27

    #@5d
    .line 758
    .end local v7           #e:Ljava/nio/BufferUnderflowException;
    .restart local v5       #chunkTnf:S
    .restart local v6       #chunks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    .restart local v9       #flag:B
    .restart local v15       #inChunk:Z
    .restart local v17       #me:Z
    :cond_5d
    const/16 v16, 0x0

    #@5f
    goto :goto_1f

    #@60
    .line 759
    .restart local v16       #mb:Z
    :cond_60
    const/16 v17, 0x0

    #@62
    goto :goto_25

    #@63
    .line 760
    :cond_63
    const/4 v4, 0x0

    #@64
    goto :goto_2a

    #@65
    .line 761
    .restart local v4       #cf:Z
    :cond_65
    const/16 v23, 0x0

    #@67
    goto :goto_30

    #@68
    .line 762
    .restart local v23       #sr:Z
    :cond_68
    const/4 v14, 0x0

    #@69
    goto :goto_35

    #@6a
    .line 767
    .restart local v14       #il:Z
    .restart local v24       #tnf:S
    :cond_6a
    if-eqz v16, :cond_7d

    #@6c
    :try_start_6c
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@6f
    move-result v27

    #@70
    if-eqz v27, :cond_7d

    #@72
    if-nez p1, :cond_7d

    #@74
    .line 768
    new-instance v27, Landroid/nfc/FormatException;

    #@76
    const-string/jumbo v28, "unexpected MB flag"

    #@79
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@7c
    throw v27

    #@7d
    .line 769
    :cond_7d
    if-eqz v15, :cond_8a

    #@7f
    if-eqz v14, :cond_8a

    #@81
    .line 770
    new-instance v27, Landroid/nfc/FormatException;

    #@83
    const-string/jumbo v28, "unexpected IL flag in non-leading chunk"

    #@86
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@89
    throw v27

    #@8a
    .line 771
    :cond_8a
    if-eqz v4, :cond_97

    #@8c
    if-eqz v17, :cond_97

    #@8e
    .line 772
    new-instance v27, Landroid/nfc/FormatException;

    #@90
    const-string/jumbo v28, "unexpected ME flag in non-trailing chunk"

    #@93
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@96
    throw v27

    #@97
    .line 773
    :cond_97
    if-eqz v15, :cond_a9

    #@99
    const/16 v27, 0x6

    #@9b
    move/from16 v0, v24

    #@9d
    move/from16 v1, v27

    #@9f
    if-eq v0, v1, :cond_a9

    #@a1
    .line 774
    new-instance v27, Landroid/nfc/FormatException;

    #@a3
    const-string v28, "expected TNF_UNCHANGED in non-leading chunk"

    #@a5
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@a8
    throw v27

    #@a9
    .line 775
    :cond_a9
    if-nez v15, :cond_bc

    #@ab
    const/16 v27, 0x6

    #@ad
    move/from16 v0, v24

    #@af
    move/from16 v1, v27

    #@b1
    if-ne v0, v1, :cond_bc

    #@b3
    .line 776
    new-instance v27, Landroid/nfc/FormatException;

    #@b5
    const-string/jumbo v28, "unexpected TNF_UNCHANGED in first chunk or unchunked record"

    #@b8
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@bb
    throw v27

    #@bc
    .line 780
    :cond_bc
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@bf
    move-result v27

    #@c0
    move/from16 v0, v27

    #@c2
    and-int/lit16 v0, v0, 0xff

    #@c4
    move/from16 v26, v0

    #@c6
    .line 781
    .local v26, typeLength:I
    if-eqz v23, :cond_ed

    #@c8
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@cb
    move-result v27

    #@cc
    move/from16 v0, v27

    #@ce
    and-int/lit16 v0, v0, 0xff

    #@d0
    move/from16 v27, v0

    #@d2
    move/from16 v0, v27

    #@d4
    int-to-long v0, v0

    #@d5
    move-wide/from16 v20, v0

    #@d7
    .line 782
    .local v20, payloadLength:J
    :goto_d7
    if-eqz v14, :cond_fe

    #@d9
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->get()B

    #@dc
    move-result v27

    #@dd
    move/from16 v0, v27

    #@df
    and-int/lit16 v13, v0, 0xff

    #@e1
    .line 784
    .local v13, idLength:I
    :goto_e1
    if-eqz v15, :cond_100

    #@e3
    if-eqz v26, :cond_100

    #@e5
    .line 785
    new-instance v27, Landroid/nfc/FormatException;

    #@e7
    const-string v28, "expected zero-length type in non-leading chunk"

    #@e9
    invoke-direct/range {v27 .. v28}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@ec
    throw v27

    #@ed
    .line 781
    .end local v13           #idLength:I
    .end local v20           #payloadLength:J
    :cond_ed
    invoke-virtual/range {p0 .. p0}, Ljava/nio/ByteBuffer;->getInt()I

    #@f0
    move-result v27

    #@f1
    move/from16 v0, v27

    #@f3
    int-to-long v0, v0

    #@f4
    move-wide/from16 v27, v0

    #@f6
    const-wide v29, 0xffffffffL

    #@fb
    and-long v20, v27, v29

    #@fd
    goto :goto_d7

    #@fe
    .line 782
    .restart local v20       #payloadLength:J
    :cond_fe
    const/4 v13, 0x0

    #@ff
    goto :goto_e1

    #@100
    .line 788
    .restart local v13       #idLength:I
    :cond_100
    if-nez v15, :cond_11a

    #@102
    .line 789
    if-lez v26, :cond_16a

    #@104
    move/from16 v0, v26

    #@106
    new-array v0, v0, [B

    #@108
    move-object/from16 v25, v0

    #@10a
    .line 790
    :goto_10a
    if-lez v13, :cond_16d

    #@10c
    new-array v12, v13, [B

    #@10e
    .line 791
    :goto_10e
    move-object/from16 v0, p0

    #@110
    move-object/from16 v1, v25

    #@112
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@115
    .line 792
    move-object/from16 v0, p0

    #@117
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@11a
    .line 795
    :cond_11a
    invoke-static/range {v20 .. v21}, Landroid/nfc/NdefRecord;->ensureSanePayloadSize(J)V

    #@11d
    .line 796
    const-wide/16 v27, 0x0

    #@11f
    cmp-long v27, v20, v27

    #@121
    if-lez v27, :cond_170

    #@123
    move-wide/from16 v0, v20

    #@125
    long-to-int v0, v0

    #@126
    move/from16 v27, v0

    #@128
    move/from16 v0, v27

    #@12a
    new-array v0, v0, [B

    #@12c
    move-object/from16 v19, v0

    #@12e
    .line 797
    :goto_12e
    move-object/from16 v0, p0

    #@130
    move-object/from16 v1, v19

    #@132
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    #@135
    .line 799
    if-eqz v4, :cond_13e

    #@137
    if-nez v15, :cond_13e

    #@139
    .line 801
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@13c
    .line 802
    move/from16 v5, v24

    #@13e
    .line 804
    :cond_13e
    if-nez v4, :cond_142

    #@140
    if-eqz v15, :cond_147

    #@142
    .line 806
    :cond_142
    move-object/from16 v0, v19

    #@144
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@147
    .line 808
    :cond_147
    if-nez v4, :cond_1ae

    #@149
    if-eqz v15, :cond_1ae

    #@14b
    .line 810
    const-wide/16 v20, 0x0

    #@14d
    .line 811
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@150
    move-result-object v11

    #@151
    .local v11, i$:Ljava/util/Iterator;
    :goto_151
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@154
    move-result v27

    #@155
    if-eqz v27, :cond_173

    #@157
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@15a
    move-result-object v18

    #@15b
    check-cast v18, [B

    #@15d
    .line 812
    .local v18, p:[B
    move-object/from16 v0, v18

    #@15f
    array-length v0, v0

    #@160
    move/from16 v27, v0

    #@162
    move/from16 v0, v27

    #@164
    int-to-long v0, v0

    #@165
    move-wide/from16 v27, v0

    #@167
    add-long v20, v20, v27

    #@169
    goto :goto_151

    #@16a
    .line 789
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v18           #p:[B
    :cond_16a
    sget-object v25, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@16c
    goto :goto_10a

    #@16d
    .line 790
    :cond_16d
    sget-object v12, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@16f
    goto :goto_10e

    #@170
    .line 796
    :cond_170
    sget-object v19, Landroid/nfc/NdefRecord;->EMPTY_BYTE_ARRAY:[B

    #@172
    goto :goto_12e

    #@173
    .line 814
    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_173
    invoke-static/range {v20 .. v21}, Landroid/nfc/NdefRecord;->ensureSanePayloadSize(J)V

    #@176
    .line 815
    move-wide/from16 v0, v20

    #@178
    long-to-int v0, v0

    #@179
    move/from16 v27, v0

    #@17b
    move/from16 v0, v27

    #@17d
    new-array v0, v0, [B

    #@17f
    move-object/from16 v19, v0

    #@181
    .line 816
    const/4 v10, 0x0

    #@182
    .line 817
    .local v10, i:I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@185
    move-result-object v11

    #@186
    :goto_186
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    #@189
    move-result v27

    #@18a
    if-eqz v27, :cond_1ac

    #@18c
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18f
    move-result-object v18

    #@190
    check-cast v18, [B

    #@192
    .line 818
    .restart local v18       #p:[B
    const/16 v27, 0x0

    #@194
    move-object/from16 v0, v18

    #@196
    array-length v0, v0

    #@197
    move/from16 v28, v0

    #@199
    move-object/from16 v0, v18

    #@19b
    move/from16 v1, v27

    #@19d
    move-object/from16 v2, v19

    #@19f
    move/from16 v3, v28

    #@1a1
    invoke-static {v0, v1, v2, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1a4
    .line 819
    move-object/from16 v0, v18

    #@1a6
    array-length v0, v0

    #@1a7
    move/from16 v27, v0

    #@1a9
    add-int v10, v10, v27

    #@1ab
    goto :goto_186

    #@1ac
    .line 821
    .end local v18           #p:[B
    :cond_1ac
    move/from16 v24, v5

    #@1ae
    .line 823
    .end local v10           #i:I
    .end local v11           #i$:Ljava/util/Iterator;
    :cond_1ae
    if-eqz v4, :cond_1b3

    #@1b0
    .line 825
    const/4 v15, 0x1

    #@1b1
    .line 826
    goto/16 :goto_13

    #@1b3
    .line 828
    :cond_1b3
    const/4 v15, 0x0

    #@1b4
    .line 831
    move/from16 v0, v24

    #@1b6
    move-object/from16 v1, v25

    #@1b8
    move-object/from16 v2, v19

    #@1ba
    invoke-static {v0, v1, v12, v2}, Landroid/nfc/NdefRecord;->validateTnf(S[B[B[B)Ljava/lang/String;

    #@1bd
    move-result-object v8

    #@1be
    .line 832
    .local v8, error:Ljava/lang/String;
    if-eqz v8, :cond_1c8

    #@1c0
    .line 833
    new-instance v27, Landroid/nfc/FormatException;

    #@1c2
    move-object/from16 v0, v27

    #@1c4
    invoke-direct {v0, v8}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@1c7
    throw v27

    #@1c8
    .line 835
    :cond_1c8
    new-instance v27, Landroid/nfc/NdefRecord;

    #@1ca
    move-object/from16 v0, v27

    #@1cc
    move/from16 v1, v24

    #@1ce
    move-object/from16 v2, v25

    #@1d0
    move-object/from16 v3, v19

    #@1d2
    invoke-direct {v0, v1, v2, v12, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    #@1d5
    move-object/from16 v0, v22

    #@1d7
    move-object/from16 v1, v27

    #@1d9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1dc
    .catch Ljava/nio/BufferUnderflowException; {:try_start_6c .. :try_end_1dc} :catch_50

    #@1dc
    .line 836
    if-eqz p1, :cond_13

    #@1de
    .line 843
    .end local v4           #cf:Z
    .end local v8           #error:Ljava/lang/String;
    .end local v9           #flag:B
    .end local v13           #idLength:I
    .end local v14           #il:Z
    .end local v16           #mb:Z
    .end local v20           #payloadLength:J
    .end local v23           #sr:Z
    .end local v24           #tnf:S
    .end local v26           #typeLength:I
    :cond_1de
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    #@1e1
    move-result v27

    #@1e2
    move/from16 v0, v27

    #@1e4
    new-array v0, v0, [Landroid/nfc/NdefRecord;

    #@1e6
    move-object/from16 v27, v0

    #@1e8
    move-object/from16 v0, v22

    #@1ea
    move-object/from16 v1, v27

    #@1ec
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@1ef
    move-result-object v27

    #@1f0
    check-cast v27, [Landroid/nfc/NdefRecord;

    #@1f2
    return-object v27
.end method

.method private parseWktUri()Landroid/net/Uri;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 714
    iget-object v4, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@3
    array-length v4, v4

    #@4
    const/4 v5, 0x2

    #@5
    if-ge v4, v5, :cond_8

    #@7
    .line 727
    :cond_7
    :goto_7
    return-object v3

    #@8
    .line 720
    :cond_8
    iget-object v4, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@a
    const/4 v5, 0x0

    #@b
    aget-byte v4, v4, v5

    #@d
    and-int/lit8 v1, v4, -0x1

    #@f
    .line 721
    .local v1, prefixIndex:I
    if-ltz v1, :cond_7

    #@11
    sget-object v4, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@13
    array-length v4, v4

    #@14
    if-ge v1, v4, :cond_7

    #@16
    .line 724
    sget-object v3, Landroid/nfc/NdefRecord;->URI_PREFIX_MAP:[Ljava/lang/String;

    #@18
    aget-object v0, v3, v1

    #@1a
    .line 725
    .local v0, prefix:Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    #@1c
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@1e
    const/4 v4, 0x1

    #@1f
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@21
    array-length v5, v5

    #@22
    invoke-static {v3, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    #@25
    move-result-object v3

    #@26
    sget-object v4, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@28
    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@2b
    .line 727
    .local v2, suffix:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3f
    move-result-object v3

    #@40
    goto :goto_7
.end method

.method private toUri(Z)Landroid/net/Uri;
    .registers 13
    .parameter "inSmartPoster"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 677
    iget-short v8, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@3
    packed-switch v8, :pswitch_data_7e

    #@6
    .line 706
    :cond_6
    :goto_6
    :pswitch_6
    return-object v7

    #@7
    .line 679
    :pswitch_7
    iget-object v8, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@9
    sget-object v9, Landroid/nfc/NdefRecord;->RTD_SMART_POSTER:[B

    #@b
    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([B[B)Z

    #@e
    move-result v8

    #@f
    if-eqz v8, :cond_30

    #@11
    if-nez p1, :cond_30

    #@13
    .line 682
    :try_start_13
    new-instance v3, Landroid/nfc/NdefMessage;

    #@15
    iget-object v8, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@17
    invoke-direct {v3, v8}, Landroid/nfc/NdefMessage;-><init>([B)V

    #@1a
    .line 683
    .local v3, nestedMessage:Landroid/nfc/NdefMessage;
    invoke-virtual {v3}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    #@1d
    move-result-object v0

    #@1e
    .local v0, arr$:[Landroid/nfc/NdefRecord;
    array-length v2, v0

    #@1f
    .local v2, len$:I
    const/4 v1, 0x0

    #@20
    .local v1, i$:I
    :goto_20
    if-ge v1, v2, :cond_6

    #@22
    aget-object v4, v0, v1

    #@24
    .line 684
    .local v4, nestedRecord:Landroid/nfc/NdefRecord;
    const/4 v8, 0x1

    #@25
    invoke-direct {v4, v8}, Landroid/nfc/NdefRecord;->toUri(Z)Landroid/net/Uri;
    :try_end_28
    .catch Landroid/nfc/FormatException; {:try_start_13 .. :try_end_28} :catch_7b

    #@28
    move-result-object v5

    #@29
    .line 685
    .local v5, uri:Landroid/net/Uri;
    if-eqz v5, :cond_2d

    #@2b
    move-object v7, v5

    #@2c
    .line 686
    goto :goto_6

    #@2d
    .line 683
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_20

    #@30
    .line 690
    .end local v0           #arr$:[Landroid/nfc/NdefRecord;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #nestedMessage:Landroid/nfc/NdefMessage;
    .end local v4           #nestedRecord:Landroid/nfc/NdefRecord;
    .end local v5           #uri:Landroid/net/Uri;
    :cond_30
    iget-object v8, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@32
    sget-object v9, Landroid/nfc/NdefRecord;->RTD_URI:[B

    #@34
    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([B[B)Z

    #@37
    move-result v8

    #@38
    if-eqz v8, :cond_6

    #@3a
    .line 691
    invoke-direct {p0}, Landroid/nfc/NdefRecord;->parseWktUri()Landroid/net/Uri;

    #@3d
    move-result-object v6

    #@3e
    .line 692
    .local v6, wktUri:Landroid/net/Uri;
    if-eqz v6, :cond_6

    #@40
    invoke-virtual {v6}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    #@43
    move-result-object v7

    #@44
    goto :goto_6

    #@45
    .line 697
    .end local v6           #wktUri:Landroid/net/Uri;
    :pswitch_45
    new-instance v7, Ljava/lang/String;

    #@47
    iget-object v8, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@49
    sget-object v9, Ljava/nio/charset/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    #@4b
    invoke-direct {v7, v8, v9}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@4e
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@51
    move-result-object v5

    #@52
    .line 698
    .restart local v5       #uri:Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->normalizeScheme()Landroid/net/Uri;

    #@55
    move-result-object v7

    #@56
    goto :goto_6

    #@57
    .line 701
    .end local v5           #uri:Landroid/net/Uri;
    :pswitch_57
    if-nez p1, :cond_6

    #@59
    .line 704
    new-instance v7, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string/jumbo v8, "vnd.android.nfc://ext/"

    #@61
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v7

    #@65
    new-instance v8, Ljava/lang/String;

    #@67
    iget-object v9, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@69
    sget-object v10, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@6b
    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@6e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v7

    #@72
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v7

    #@76
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@79
    move-result-object v7

    #@7a
    goto :goto_6

    #@7b
    .line 689
    :catch_7b
    move-exception v8

    #@7c
    goto :goto_6

    #@7d
    .line 677
    nop

    #@7e
    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_45
        :pswitch_57
    .end packed-switch
.end method

.method static validateTnf(S[B[B[B)Ljava/lang/String;
    .registers 8
    .parameter "tnf"
    .parameter "type"
    .parameter "id"
    .parameter "payload"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 866
    packed-switch p0, :pswitch_data_2e

    #@4
    .line 886
    const-string/jumbo v0, "unexpected tnf value: 0x%02x"

    #@7
    const/4 v1, 0x1

    #@8
    new-array v1, v1, [Ljava/lang/Object;

    #@a
    const/4 v2, 0x0

    #@b
    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@e
    move-result-object v3

    #@f
    aput-object v3, v1, v2

    #@11
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    :cond_15
    :goto_15
    :pswitch_15
    return-object v0

    #@16
    .line 868
    :pswitch_16
    array-length v1, p1

    #@17
    if-nez v1, :cond_1f

    #@19
    array-length v1, p2

    #@1a
    if-nez v1, :cond_1f

    #@1c
    array-length v1, p3

    #@1d
    if-eqz v1, :cond_15

    #@1f
    .line 869
    :cond_1f
    const-string/jumbo v0, "unexpected data in TNF_EMPTY record"

    #@22
    goto :goto_15

    #@23
    .line 879
    :pswitch_23
    array-length v1, p1

    #@24
    if-eqz v1, :cond_15

    #@26
    .line 880
    const-string/jumbo v0, "unexpected type field in TNF_UNKNOWN or TNF_RESERVEd record"

    #@29
    goto :goto_15

    #@2a
    .line 884
    :pswitch_2a
    const-string/jumbo v0, "unexpected TNF_UNCHANGED in first chunk or logical record"

    #@2d
    goto :goto_15

    #@2e
    .line 866
    :pswitch_data_2e
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_23
        :pswitch_2a
        :pswitch_23
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 935
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 989
    if-ne p0, p1, :cond_5

    #@3
    const/4 v1, 0x1

    #@4
    .line 996
    :cond_4
    :goto_4
    return v1

    #@5
    .line 990
    :cond_5
    if-eqz p1, :cond_4

    #@7
    .line 991
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v3

    #@f
    if-ne v2, v3, :cond_4

    #@11
    move-object v0, p1

    #@12
    .line 992
    check-cast v0, Landroid/nfc/NdefRecord;

    #@14
    .line 993
    .local v0, other:Landroid/nfc/NdefRecord;
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@16
    iget-object v3, v0, Landroid/nfc/NdefRecord;->mId:[B

    #@18
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_4

    #@1e
    .line 994
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@20
    iget-object v3, v0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@22
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_4

    #@28
    .line 995
    iget-short v2, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@2a
    iget-short v3, v0, Landroid/nfc/NdefRecord;->mTnf:S

    #@2c
    if-ne v2, v3, :cond_4

    #@2e
    .line 996
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@30
    iget-object v2, v0, Landroid/nfc/NdefRecord;->mType:[B

    #@32
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    #@35
    move-result v1

    #@36
    goto :goto_4
.end method

.method getByteLength()I
    .registers 8

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 922
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@4
    array-length v5, v5

    #@5
    add-int/lit8 v5, v5, 0x3

    #@7
    iget-object v6, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@9
    array-length v6, v6

    #@a
    add-int/2addr v5, v6

    #@b
    iget-object v6, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@d
    array-length v6, v6

    #@e
    add-int v1, v5, v6

    #@10
    .line 924
    .local v1, length:I
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@12
    array-length v5, v5

    #@13
    const/16 v6, 0x100

    #@15
    if-ge v5, v6, :cond_27

    #@17
    move v2, v3

    #@18
    .line 925
    .local v2, sr:Z
    :goto_18
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@1a
    array-length v5, v5

    #@1b
    if-lez v5, :cond_29

    #@1d
    move v0, v3

    #@1e
    .line 927
    .local v0, il:Z
    :goto_1e
    if-nez v2, :cond_22

    #@20
    add-int/lit8 v1, v1, 0x3

    #@22
    .line 928
    :cond_22
    if-eqz v0, :cond_26

    #@24
    add-int/lit8 v1, v1, 0x1

    #@26
    .line 930
    :cond_26
    return v1

    #@27
    .end local v0           #il:Z
    .end local v2           #sr:Z
    :cond_27
    move v2, v4

    #@28
    .line 924
    goto :goto_18

    #@29
    .restart local v2       #sr:Z
    :cond_29
    move v0, v4

    #@2a
    .line 925
    goto :goto_1e
.end method

.method public getId()[B
    .registers 2

    #@0
    .prologue
    .line 591
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@2
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [B

    #@8
    return-object v0
.end method

.method public getPayload()[B
    .registers 2

    #@0
    .prologue
    .line 601
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@2
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [B

    #@8
    return-object v0
.end method

.method public getTnf()S
    .registers 2

    #@0
    .prologue
    .line 568
    iget-short v0, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@2
    return v0
.end method

.method public getType()[B
    .registers 2

    #@0
    .prologue
    .line 581
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@2
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [B

    #@8
    return-object v0
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    .line 974
    const/16 v0, 0x1f

    #@2
    .line 975
    .local v0, prime:I
    const/4 v1, 0x1

    #@3
    .line 976
    .local v1, result:I
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@5
    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    #@8
    move-result v2

    #@9
    add-int/lit8 v1, v2, 0x1f

    #@b
    .line 977
    mul-int/lit8 v2, v1, 0x1f

    #@d
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@f
    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    #@12
    move-result v3

    #@13
    add-int v1, v2, v3

    #@15
    .line 978
    mul-int/lit8 v2, v1, 0x1f

    #@17
    iget-short v3, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@19
    add-int v1, v2, v3

    #@1b
    .line 979
    mul-int/lit8 v2, v1, 0x1f

    #@1d
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@1f
    invoke-static {v3}, Ljava/util/Arrays;->hashCode([B)I

    #@22
    move-result v3

    #@23
    add-int v1, v2, v3

    #@25
    .line 980
    return v1
.end method

.method public toByteArray()[B
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 618
    invoke-virtual {p0}, Landroid/nfc/NdefRecord;->getByteLength()I

    #@4
    move-result v1

    #@5
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@8
    move-result-object v0

    #@9
    .line 619
    .local v0, buffer:Ljava/nio/ByteBuffer;
    invoke-virtual {p0, v0, v2, v2}, Landroid/nfc/NdefRecord;->writeToByteBuffer(Ljava/nio/ByteBuffer;ZZ)V

    #@c
    .line 620
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@f
    move-result-object v1

    #@10
    return-object v1
.end method

.method public toMimeType()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 639
    iget-short v1, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@2
    packed-switch v1, :pswitch_data_24

    #@5
    .line 649
    :cond_5
    const/4 v1, 0x0

    #@6
    :goto_6
    return-object v1

    #@7
    .line 641
    :pswitch_7
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@9
    sget-object v2, Landroid/nfc/NdefRecord;->RTD_TEXT:[B

    #@b
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_5

    #@11
    .line 642
    const-string/jumbo v1, "text/plain"

    #@14
    goto :goto_6

    #@15
    .line 646
    :pswitch_15
    new-instance v0, Ljava/lang/String;

    #@17
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@19
    sget-object v2, Ljava/nio/charset/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    #@1b
    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    #@1e
    .line 647
    .local v0, mimeType:Ljava/lang/String;
    invoke-static {v0}, Landroid/content/Intent;->normalizeMimeType(Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    goto :goto_6

    #@23
    .line 639
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x1
        :pswitch_7
        :pswitch_15
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 1001
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "NdefRecord tnf=%X"

    #@4
    const/4 v2, 0x1

    #@5
    new-array v2, v2, [Ljava/lang/Object;

    #@7
    const/4 v3, 0x0

    #@8
    iget-short v4, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@a
    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@d
    move-result-object v4

    #@e
    aput-object v4, v2, v3

    #@10
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@17
    .line 1002
    .local v0, b:Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@19
    array-length v1, v1

    #@1a
    if-lez v1, :cond_2b

    #@1c
    const-string v1, " type="

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@24
    invoke-static {v2}, Landroid/nfc/NdefRecord;->bytesToString([B)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2b
    .line 1003
    :cond_2b
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@2d
    array-length v1, v1

    #@2e
    if-lez v1, :cond_3f

    #@30
    const-string v1, " id="

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@38
    invoke-static {v2}, Landroid/nfc/NdefRecord;->bytesToString([B)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@3f
    .line 1004
    :cond_3f
    iget-object v1, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@41
    array-length v1, v1

    #@42
    if-lez v1, :cond_53

    #@44
    const-string v1, " payload="

    #@46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    iget-object v2, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@4c
    invoke-static {v2}, Landroid/nfc/NdefRecord;->bytesToString([B)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@53
    .line 1005
    :cond_53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    return-object v1
.end method

.method public toUri()Landroid/net/Uri;
    .registers 2

    #@0
    .prologue
    .line 673
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/nfc/NdefRecord;->toUri(Z)Landroid/net/Uri;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method writeToByteBuffer(Ljava/nio/ByteBuffer;ZZ)V
    .registers 11
    .parameter "buffer"
    .parameter "mb"
    .parameter "me"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 896
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@4
    array-length v5, v5

    #@5
    const/16 v6, 0x100

    #@7
    if-ge v5, v6, :cond_54

    #@9
    move v2, v3

    #@a
    .line 897
    .local v2, sr:Z
    :goto_a
    iget-object v5, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@c
    array-length v5, v5

    #@d
    if-lez v5, :cond_56

    #@f
    move v1, v3

    #@10
    .line 899
    .local v1, il:Z
    :goto_10
    if-eqz p2, :cond_58

    #@12
    const/16 v3, -0x80

    #@14
    move v5, v3

    #@15
    :goto_15
    if-eqz p3, :cond_5a

    #@17
    const/16 v3, 0x40

    #@19
    :goto_19
    or-int/2addr v5, v3

    #@1a
    if-eqz v2, :cond_5c

    #@1c
    const/16 v3, 0x10

    #@1e
    :goto_1e
    or-int/2addr v3, v5

    #@1f
    if-eqz v1, :cond_23

    #@21
    const/16 v4, 0x8

    #@23
    :cond_23
    or-int/2addr v3, v4

    #@24
    iget-short v4, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@26
    or-int/2addr v3, v4

    #@27
    int-to-byte v0, v3

    #@28
    .line 901
    .local v0, flags:B
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@2b
    .line 903
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@2d
    array-length v3, v3

    #@2e
    int-to-byte v3, v3

    #@2f
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@32
    .line 904
    if-eqz v2, :cond_5e

    #@34
    .line 905
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@36
    array-length v3, v3

    #@37
    int-to-byte v3, v3

    #@38
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@3b
    .line 909
    :goto_3b
    if-eqz v1, :cond_44

    #@3d
    .line 910
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@3f
    array-length v3, v3

    #@40
    int-to-byte v3, v3

    #@41
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    #@44
    .line 913
    :cond_44
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@46
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@49
    .line 914
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@4b
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@4e
    .line 915
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@50
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@53
    .line 916
    return-void

    #@54
    .end local v0           #flags:B
    .end local v1           #il:Z
    .end local v2           #sr:Z
    :cond_54
    move v2, v4

    #@55
    .line 896
    goto :goto_a

    #@56
    .restart local v2       #sr:Z
    :cond_56
    move v1, v4

    #@57
    .line 897
    goto :goto_10

    #@58
    .restart local v1       #il:Z
    :cond_58
    move v5, v4

    #@59
    .line 899
    goto :goto_15

    #@5a
    :cond_5a
    move v3, v4

    #@5b
    goto :goto_19

    #@5c
    :cond_5c
    move v3, v4

    #@5d
    goto :goto_1e

    #@5e
    .line 907
    .restart local v0       #flags:B
    :cond_5e
    iget-object v3, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@60
    array-length v3, v3

    #@61
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@64
    goto :goto_3b
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 940
    iget-short v0, p0, Landroid/nfc/NdefRecord;->mTnf:S

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 941
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@7
    array-length v0, v0

    #@8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 942
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mType:[B

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@10
    .line 943
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@12
    array-length v0, v0

    #@13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 944
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mId:[B

    #@18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@1b
    .line 945
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@1d
    array-length v0, v0

    #@1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@21
    .line 946
    iget-object v0, p0, Landroid/nfc/NdefRecord;->mPayload:[B

    #@23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@26
    .line 947
    return-void
.end method
