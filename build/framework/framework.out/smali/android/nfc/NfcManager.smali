.class public final Landroid/nfc/NfcManager;
.super Ljava/lang/Object;
.source "NfcManager.java"


# instance fields
.field private final mAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@6
    move-result-object p1

    #@7
    .line 49
    if-nez p1, :cond_11

    #@9
    .line 50
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v3, "context not associated with any application (using a mock context?)"

    #@d
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v2

    #@11
    .line 54
    :cond_11
    :try_start_11
    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getNfcAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;
    :try_end_14
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_11 .. :try_end_14} :catch_18

    #@14
    move-result-object v0

    #@15
    .line 58
    .local v0, adapter:Landroid/nfc/NfcAdapter;
    :goto_15
    iput-object v0, p0, Landroid/nfc/NfcManager;->mAdapter:Landroid/nfc/NfcAdapter;

    #@17
    .line 59
    return-void

    #@18
    .line 55
    .end local v0           #adapter:Landroid/nfc/NfcAdapter;
    :catch_18
    move-exception v1

    #@19
    .line 56
    .local v1, e:Ljava/lang/UnsupportedOperationException;
    const/4 v0, 0x0

    #@1a
    .restart local v0       #adapter:Landroid/nfc/NfcAdapter;
    goto :goto_15
.end method


# virtual methods
.method public getDefaultAdapter()Landroid/nfc/NfcAdapter;
    .registers 2

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Landroid/nfc/NfcManager;->mAdapter:Landroid/nfc/NfcAdapter;

    #@2
    return-object v0
.end method
