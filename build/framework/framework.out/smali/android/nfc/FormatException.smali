.class public Landroid/nfc/FormatException;
.super Ljava/lang/Exception;
.source "FormatException.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    #@3
    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 25
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@3
    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 3
    .parameter "message"
    .parameter "e"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3
    .line 30
    return-void
.end method
