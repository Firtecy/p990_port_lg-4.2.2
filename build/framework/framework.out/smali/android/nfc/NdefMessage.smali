.class public final Landroid/nfc/NdefMessage;
.super Ljava/lang/Object;
.source "NdefMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/nfc/NdefMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mRecords:[Landroid/nfc/NdefRecord;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 216
    new-instance v0, Landroid/nfc/NdefMessage$1;

    #@2
    invoke-direct {v0}, Landroid/nfc/NdefMessage$1;-><init>()V

    #@5
    sput-object v0, Landroid/nfc/NdefMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public varargs constructor <init>(Landroid/nfc/NdefRecord;[Landroid/nfc/NdefRecord;)V
    .registers 11
    .parameter "record"
    .parameter "records"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 113
    if-nez p1, :cond_f

    #@6
    new-instance v4, Ljava/lang/NullPointerException;

    #@8
    const-string/jumbo v5, "record cannot be null"

    #@b
    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@e
    throw v4

    #@f
    .line 115
    :cond_f
    move-object v0, p2

    #@10
    .local v0, arr$:[Landroid/nfc/NdefRecord;
    array-length v2, v0

    #@11
    .local v2, len$:I
    const/4 v1, 0x0

    #@12
    .local v1, i$:I
    :goto_12
    if-ge v1, v2, :cond_24

    #@14
    aget-object v3, v0, v1

    #@16
    .line 116
    .local v3, r:Landroid/nfc/NdefRecord;
    if-nez v3, :cond_21

    #@18
    .line 117
    new-instance v4, Ljava/lang/NullPointerException;

    #@1a
    const-string/jumbo v5, "record cannot be null"

    #@1d
    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@20
    throw v4

    #@21
    .line 115
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_12

    #@24
    .line 121
    .end local v3           #r:Landroid/nfc/NdefRecord;
    :cond_24
    array-length v4, p2

    #@25
    add-int/lit8 v4, v4, 0x1

    #@27
    new-array v4, v4, [Landroid/nfc/NdefRecord;

    #@29
    iput-object v4, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@2b
    .line 122
    iget-object v4, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@2d
    aput-object p1, v4, v7

    #@2f
    .line 123
    iget-object v4, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@31
    const/4 v5, 0x1

    #@32
    array-length v6, p2

    #@33
    invoke-static {p2, v7, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@36
    .line 124
    return-void
.end method

.method public constructor <init>([B)V
    .registers 5
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/nfc/FormatException;
        }
    .end annotation

    #@0
    .prologue
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 95
    if-nez p1, :cond_d

    #@5
    new-instance v1, Ljava/lang/NullPointerException;

    #@7
    const-string v2, "data is null"

    #@9
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1

    #@d
    .line 96
    :cond_d
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@10
    move-result-object v0

    #@11
    .line 98
    .local v0, buffer:Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    #@12
    invoke-static {v0, v1}, Landroid/nfc/NdefRecord;->parse(Ljava/nio/ByteBuffer;Z)[Landroid/nfc/NdefRecord;

    #@15
    move-result-object v1

    #@16
    iput-object v1, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@18
    .line 100
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    #@1b
    move-result v1

    #@1c
    if-lez v1, :cond_27

    #@1e
    .line 101
    new-instance v1, Landroid/nfc/FormatException;

    #@20
    const-string/jumbo v2, "trailing data"

    #@23
    invoke-direct {v1, v2}, Landroid/nfc/FormatException;-><init>(Ljava/lang/String;)V

    #@26
    throw v1

    #@27
    .line 103
    :cond_27
    return-void
.end method

.method public constructor <init>([Landroid/nfc/NdefRecord;)V
    .registers 8
    .parameter "records"

    #@0
    .prologue
    .line 131
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 133
    array-length v4, p1

    #@4
    const/4 v5, 0x1

    #@5
    if-ge v4, v5, :cond_10

    #@7
    .line 134
    new-instance v4, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v5, "must have at least one record"

    #@c
    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v4

    #@10
    .line 136
    :cond_10
    move-object v0, p1

    #@11
    .local v0, arr$:[Landroid/nfc/NdefRecord;
    array-length v2, v0

    #@12
    .local v2, len$:I
    const/4 v1, 0x0

    #@13
    .local v1, i$:I
    :goto_13
    if-ge v1, v2, :cond_25

    #@15
    aget-object v3, v0, v1

    #@17
    .line 137
    .local v3, r:Landroid/nfc/NdefRecord;
    if-nez v3, :cond_22

    #@19
    .line 138
    new-instance v4, Ljava/lang/NullPointerException;

    #@1b
    const-string/jumbo v5, "records cannot contain null"

    #@1e
    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@21
    throw v4

    #@22
    .line 136
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_13

    #@25
    .line 142
    .end local v3           #r:Landroid/nfc/NdefRecord;
    :cond_25
    iput-object p1, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@27
    .line 143
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 207
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "obj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 242
    if-ne p0, p1, :cond_5

    #@3
    const/4 v1, 0x1

    #@4
    .line 246
    :cond_4
    :goto_4
    return v1

    #@5
    .line 243
    :cond_5
    if-eqz p1, :cond_4

    #@7
    .line 244
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v3

    #@f
    if-ne v2, v3, :cond_4

    #@11
    move-object v0, p1

    #@12
    .line 245
    check-cast v0, Landroid/nfc/NdefMessage;

    #@14
    .line 246
    .local v0, other:Landroid/nfc/NdefMessage;
    iget-object v1, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@16
    iget-object v2, v0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@18
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    goto :goto_4
.end method

.method public getByteArrayLength()I
    .registers 7

    #@0
    .prologue
    .line 174
    const/4 v3, 0x0

    #@1
    .line 175
    .local v3, length:I
    iget-object v0, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@3
    .local v0, arr$:[Landroid/nfc/NdefRecord;
    array-length v2, v0

    #@4
    .local v2, len$:I
    const/4 v1, 0x0

    #@5
    .local v1, i$:I
    :goto_5
    if-ge v1, v2, :cond_11

    #@7
    aget-object v4, v0, v1

    #@9
    .line 176
    .local v4, r:Landroid/nfc/NdefRecord;
    invoke-virtual {v4}, Landroid/nfc/NdefRecord;->getByteLength()I

    #@c
    move-result v5

    #@d
    add-int/2addr v3, v5

    #@e
    .line 175
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_5

    #@11
    .line 178
    .end local v4           #r:Landroid/nfc/NdefRecord;
    :cond_11
    return v3
.end method

.method public getRecords()[Landroid/nfc/NdefRecord;
    .registers 2

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@2
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toByteArray()[B
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 193
    invoke-virtual {p0}, Landroid/nfc/NdefMessage;->getByteArrayLength()I

    #@5
    move-result v2

    #@6
    .line 194
    .local v2, length:I
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@9
    move-result-object v0

    #@a
    .line 196
    .local v0, buffer:Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    iget-object v7, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@d
    array-length v7, v7

    #@e
    if-ge v1, v7, :cond_29

    #@10
    .line 197
    if-nez v1, :cond_25

    #@12
    move v3, v5

    #@13
    .line 198
    .local v3, mb:Z
    :goto_13
    iget-object v7, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@15
    array-length v7, v7

    #@16
    add-int/lit8 v7, v7, -0x1

    #@18
    if-ne v1, v7, :cond_27

    #@1a
    move v4, v5

    #@1b
    .line 199
    .local v4, me:Z
    :goto_1b
    iget-object v7, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@1d
    aget-object v7, v7, v1

    #@1f
    invoke-virtual {v7, v0, v3, v4}, Landroid/nfc/NdefRecord;->writeToByteBuffer(Ljava/nio/ByteBuffer;ZZ)V

    #@22
    .line 196
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_b

    #@25
    .end local v3           #mb:Z
    .end local v4           #me:Z
    :cond_25
    move v3, v6

    #@26
    .line 197
    goto :goto_13

    #@27
    .restart local v3       #mb:Z
    :cond_27
    move v4, v6

    #@28
    .line 198
    goto :goto_1b

    #@29
    .line 202
    .end local v3           #mb:Z
    :cond_29
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@2c
    move-result-object v5

    #@2d
    return-object v5
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "NdefMessage "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@d
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@2
    array-length v0, v0

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 213
    iget-object v0, p0, Landroid/nfc/NdefMessage;->mRecords:[Landroid/nfc/NdefRecord;

    #@8
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@b
    .line 214
    return-void
.end method
