.class public final Landroid/nfc/Tag;
.super Ljava/lang/Object;
.source "Tag.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/nfc/Tag;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mConnectedTechnology:I

.field final mId:[B

.field final mServiceHandle:I

.field final mTagService:Landroid/nfc/INfcTag;

.field final mTechExtras:[Landroid/os/Bundle;

.field final mTechList:[I

.field final mTechStringList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 376
    new-instance v0, Landroid/nfc/Tag$1;

    #@2
    invoke-direct {v0}, Landroid/nfc/Tag$1;-><init>()V

    #@5
    sput-object v0, Landroid/nfc/Tag;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>([B[I[Landroid/os/Bundle;ILandroid/nfc/INfcTag;)V
    .registers 8
    .parameter "id"
    .parameter "techList"
    .parameter "techListExtras"
    .parameter "serviceHandle"
    .parameter "tagService"

    #@0
    .prologue
    .line 126
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 127
    if-nez p2, :cond_e

    #@5
    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v1, "rawTargets cannot be null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 130
    :cond_e
    iput-object p1, p0, Landroid/nfc/Tag;->mId:[B

    #@10
    .line 131
    array-length v0, p2

    #@11
    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([II)[I

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/nfc/Tag;->mTechList:[I

    #@17
    .line 132
    invoke-direct {p0, p2}, Landroid/nfc/Tag;->generateTechStringList([I)[Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Landroid/nfc/Tag;->mTechStringList:[Ljava/lang/String;

    #@1d
    .line 134
    array-length v0, p2

    #@1e
    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, [Landroid/os/Bundle;

    #@24
    iput-object v0, p0, Landroid/nfc/Tag;->mTechExtras:[Landroid/os/Bundle;

    #@26
    .line 135
    iput p4, p0, Landroid/nfc/Tag;->mServiceHandle:I

    #@28
    .line 136
    iput-object p5, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@2a
    .line 138
    const/4 v0, -0x1

    #@2b
    iput v0, p0, Landroid/nfc/Tag;->mConnectedTechnology:I

    #@2d
    .line 139
    return-void
.end method

.method public static createMockTag([B[I[Landroid/os/Bundle;)Landroid/nfc/Tag;
    .registers 9
    .parameter "id"
    .parameter "techList"
    .parameter "techListExtras"

    #@0
    .prologue
    .line 153
    new-instance v0, Landroid/nfc/Tag;

    #@2
    const/4 v4, 0x0

    #@3
    const/4 v5, 0x0

    #@4
    move-object v1, p0

    #@5
    move-object v2, p1

    #@6
    move-object v3, p2

    #@7
    invoke-direct/range {v0 .. v5}, Landroid/nfc/Tag;-><init>([B[I[Landroid/os/Bundle;ILandroid/nfc/INfcTag;)V

    #@a
    return-object v0
.end method

.method private generateTechStringList([I)[Ljava/lang/String;
    .registers 8
    .parameter "techList"

    #@0
    .prologue
    .line 157
    array-length v1, p1

    #@1
    .line 158
    .local v1, size:I
    new-array v2, v1, [Ljava/lang/String;

    #@3
    .line 159
    .local v2, strings:[Ljava/lang/String;
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_82

    #@6
    .line 160
    aget v3, p1, v0

    #@8
    packed-switch v3, :pswitch_data_84

    #@b
    .line 192
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "Unknown tech type "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    aget v5, p1, v0

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v3

    #@26
    .line 162
    :pswitch_26
    const-class v3, Landroid/nfc/tech/IsoDep;

    #@28
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    aput-object v3, v2, v0

    #@2e
    .line 159
    :goto_2e
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_4

    #@31
    .line 165
    :pswitch_31
    const-class v3, Landroid/nfc/tech/MifareClassic;

    #@33
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    aput-object v3, v2, v0

    #@39
    goto :goto_2e

    #@3a
    .line 168
    :pswitch_3a
    const-class v3, Landroid/nfc/tech/MifareUltralight;

    #@3c
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    aput-object v3, v2, v0

    #@42
    goto :goto_2e

    #@43
    .line 171
    :pswitch_43
    const-class v3, Landroid/nfc/tech/Ndef;

    #@45
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    aput-object v3, v2, v0

    #@4b
    goto :goto_2e

    #@4c
    .line 174
    :pswitch_4c
    const-class v3, Landroid/nfc/tech/NdefFormatable;

    #@4e
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    aput-object v3, v2, v0

    #@54
    goto :goto_2e

    #@55
    .line 177
    :pswitch_55
    const-class v3, Landroid/nfc/tech/NfcA;

    #@57
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    aput-object v3, v2, v0

    #@5d
    goto :goto_2e

    #@5e
    .line 180
    :pswitch_5e
    const-class v3, Landroid/nfc/tech/NfcB;

    #@60
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    aput-object v3, v2, v0

    #@66
    goto :goto_2e

    #@67
    .line 183
    :pswitch_67
    const-class v3, Landroid/nfc/tech/NfcF;

    #@69
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    aput-object v3, v2, v0

    #@6f
    goto :goto_2e

    #@70
    .line 186
    :pswitch_70
    const-class v3, Landroid/nfc/tech/NfcV;

    #@72
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    aput-object v3, v2, v0

    #@78
    goto :goto_2e

    #@79
    .line 189
    :pswitch_79
    const-class v3, Landroid/nfc/tech/NfcBarcode;

    #@7b
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    aput-object v3, v2, v0

    #@81
    goto :goto_2e

    #@82
    .line 195
    :cond_82
    return-object v2

    #@83
    .line 160
    nop

    #@84
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_55
        :pswitch_5e
        :pswitch_26
        :pswitch_67
        :pswitch_70
        :pswitch_43
        :pswitch_4c
        :pswitch_31
        :pswitch_3a
        :pswitch_79
    .end packed-switch
.end method

.method static readBytesWithNull(Landroid/os/Parcel;)[B
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 337
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 338
    .local v0, len:I
    const/4 v1, 0x0

    #@5
    .line 339
    .local v1, result:[B
    if-ltz v0, :cond_c

    #@7
    .line 340
    new-array v1, v0, [B

    #@9
    .line 341
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readByteArray([B)V

    #@c
    .line 343
    :cond_c
    return-object v1
.end method

.method static writeBytesWithNull(Landroid/os/Parcel;[B)V
    .registers 3
    .parameter "out"
    .parameter "b"

    #@0
    .prologue
    .line 347
    if-nez p1, :cond_7

    #@2
    .line 348
    const/4 v0, -0x1

    #@3
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 353
    :goto_6
    return-void

    #@7
    .line 351
    :cond_7
    array-length v0, p1

    #@8
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@b
    .line 352
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@e
    goto :goto_6
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 357
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getConnectedTechnology()I
    .registers 2

    #@0
    .prologue
    .line 424
    iget v0, p0, Landroid/nfc/Tag;->mConnectedTechnology:I

    #@2
    return v0
.end method

.method public getId()[B
    .registers 2

    #@0
    .prologue
    .line 219
    iget-object v0, p0, Landroid/nfc/Tag;->mId:[B

    #@2
    return-object v0
.end method

.method public getServiceHandle()I
    .registers 2

    #@0
    .prologue
    .line 203
    iget v0, p0, Landroid/nfc/Tag;->mServiceHandle:I

    #@2
    return v0
.end method

.method public getTagService()Landroid/nfc/INfcTag;
    .registers 2

    #@0
    .prologue
    .line 315
    iget-object v0, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@2
    return-object v0
.end method

.method public getTechExtras(I)Landroid/os/Bundle;
    .registers 5
    .parameter "tech"

    #@0
    .prologue
    .line 299
    const/4 v1, -0x1

    #@1
    .line 300
    .local v1, pos:I
    const/4 v0, 0x0

    #@2
    .local v0, idx:I
    :goto_2
    iget-object v2, p0, Landroid/nfc/Tag;->mTechList:[I

    #@4
    array-length v2, v2

    #@5
    if-ge v0, v2, :cond_e

    #@7
    .line 301
    iget-object v2, p0, Landroid/nfc/Tag;->mTechList:[I

    #@9
    aget v2, v2, v0

    #@b
    if-ne v2, p1, :cond_12

    #@d
    .line 302
    move v1, v0

    #@e
    .line 306
    :cond_e
    if-gez v1, :cond_15

    #@10
    .line 307
    const/4 v2, 0x0

    #@11
    .line 310
    :goto_11
    return-object v2

    #@12
    .line 300
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_2

    #@15
    .line 310
    :cond_15
    iget-object v2, p0, Landroid/nfc/Tag;->mTechExtras:[Landroid/os/Bundle;

    #@17
    aget-object v2, v2, v1

    #@19
    goto :goto_11
.end method

.method public getTechList()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/nfc/Tag;->mTechStringList:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public hasTech(I)Z
    .registers 7
    .parameter "techType"

    #@0
    .prologue
    .line 291
    iget-object v0, p0, Landroid/nfc/Tag;->mTechList:[I

    #@2
    .local v0, arr$:[I
    array-length v2, v0

    #@3
    .local v2, len$:I
    const/4 v1, 0x0

    #@4
    .local v1, i$:I
    :goto_4
    if-ge v1, v2, :cond_f

    #@6
    aget v3, v0, v1

    #@8
    .line 292
    .local v3, tech:I
    if-ne v3, p1, :cond_c

    #@a
    const/4 v4, 0x1

    #@b
    .line 294
    .end local v3           #tech:I
    :goto_b
    return v4

    #@c
    .line 291
    .restart local v3       #tech:I
    :cond_c
    add-int/lit8 v1, v1, 0x1

    #@e
    goto :goto_4

    #@f
    .line 294
    .end local v3           #tech:I
    :cond_f
    const/4 v4, 0x0

    #@10
    goto :goto_b
.end method

.method public rediscover()Landroid/nfc/Tag;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 269
    invoke-virtual {p0}, Landroid/nfc/Tag;->getConnectedTechnology()I

    #@3
    move-result v2

    #@4
    const/4 v3, -0x1

    #@5
    if-eq v2, v3, :cond_f

    #@7
    .line 270
    new-instance v2, Ljava/lang/IllegalStateException;

    #@9
    const-string v3, "Close connection to the technology first!"

    #@b
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v2

    #@f
    .line 273
    :cond_f
    iget-object v2, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@11
    if-nez v2, :cond_1b

    #@13
    .line 274
    new-instance v2, Ljava/io/IOException;

    #@15
    const-string v3, "Mock tags don\'t support this operation."

    #@17
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 277
    :cond_1b
    :try_start_1b
    iget-object v2, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@1d
    invoke-virtual {p0}, Landroid/nfc/Tag;->getServiceHandle()I

    #@20
    move-result v3

    #@21
    invoke-interface {v2, v3}, Landroid/nfc/INfcTag;->rediscover(I)Landroid/nfc/Tag;

    #@24
    move-result-object v1

    #@25
    .line 278
    .local v1, newTag:Landroid/nfc/Tag;
    if-eqz v1, :cond_28

    #@27
    .line 279
    return-object v1

    #@28
    .line 281
    :cond_28
    new-instance v2, Ljava/io/IOException;

    #@2a
    const-string v3, "Failed to rediscover tag"

    #@2c
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v2
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_30} :catch_30

    #@30
    .line 283
    .end local v1           #newTag:Landroid/nfc/Tag;
    :catch_30
    move-exception v0

    #@31
    .line 284
    .local v0, e:Landroid/os/RemoteException;
    new-instance v2, Ljava/io/IOException;

    #@33
    const-string v3, "NFC service dead"

    #@35
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@38
    throw v2
.end method

.method public declared-synchronized setConnectedTechnology(I)V
    .registers 4
    .parameter "technology"

    #@0
    .prologue
    .line 411
    monitor-enter p0

    #@1
    :try_start_1
    iget v0, p0, Landroid/nfc/Tag;->mConnectedTechnology:I

    #@3
    const/4 v1, -0x1

    #@4
    if-ne v0, v1, :cond_a

    #@6
    .line 412
    iput p1, p0, Landroid/nfc/Tag;->mConnectedTechnology:I
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_12

    #@8
    .line 416
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 414
    :cond_a
    :try_start_a
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    const-string v1, "Close other technology first!"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0
    :try_end_12
    .catchall {:try_start_a .. :try_end_12} :catchall_12

    #@12
    .line 411
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public setTechnologyDisconnected()V
    .registers 2

    #@0
    .prologue
    .line 433
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/nfc/Tag;->mConnectedTechnology:I

    #@3
    .line 434
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 323
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    const-string v4, "TAG: Tech ["

    #@4
    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 324
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    .line 325
    .local v3, techList:[Ljava/lang/String;
    array-length v1, v3

    #@c
    .line 326
    .local v1, length:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v1, :cond_20

    #@f
    .line 327
    aget-object v4, v3, v0

    #@11
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 328
    add-int/lit8 v4, v1, -0x1

    #@16
    if-ge v0, v4, :cond_1d

    #@18
    .line 329
    const-string v4, ", "

    #@1a
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 326
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_d

    #@20
    .line 332
    :cond_20
    const-string v4, "]"

    #@22
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 333
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 363
    iget-object v2, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@3
    if-nez v2, :cond_2f

    #@5
    const/4 v0, 0x1

    #@6
    .line 365
    .local v0, isMock:I
    :goto_6
    iget-object v2, p0, Landroid/nfc/Tag;->mId:[B

    #@8
    invoke-static {p1, v2}, Landroid/nfc/Tag;->writeBytesWithNull(Landroid/os/Parcel;[B)V

    #@b
    .line 366
    iget-object v2, p0, Landroid/nfc/Tag;->mTechList:[I

    #@d
    array-length v2, v2

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 367
    iget-object v2, p0, Landroid/nfc/Tag;->mTechList:[I

    #@13
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    #@16
    .line 368
    iget-object v2, p0, Landroid/nfc/Tag;->mTechExtras:[Landroid/os/Bundle;

    #@18
    invoke-virtual {p1, v2, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@1b
    .line 369
    iget v1, p0, Landroid/nfc/Tag;->mServiceHandle:I

    #@1d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 370
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 371
    if-nez v0, :cond_2e

    #@25
    .line 372
    iget-object v1, p0, Landroid/nfc/Tag;->mTagService:Landroid/nfc/INfcTag;

    #@27
    invoke-interface {v1}, Landroid/nfc/INfcTag;->asBinder()Landroid/os/IBinder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2e
    .line 374
    :cond_2e
    return-void

    #@2f
    .end local v0           #isMock:I
    :cond_2f
    move v0, v1

    #@30
    .line 363
    goto :goto_6
.end method
