.class public abstract Landroid/nfc/INfcTag$Stub;
.super Landroid/os/Binder;
.source "INfcTag.java"

# interfaces
.implements Landroid/nfc/INfcTag;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/nfc/INfcTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/nfc/INfcTag$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.nfc.INfcTag"

.field static final TRANSACTION_canMakeReadOnly:I = 0x11

.field static final TRANSACTION_close:I = 0x1

.field static final TRANSACTION_connect:I = 0x2

.field static final TRANSACTION_formatNdef:I = 0xc

.field static final TRANSACTION_getExtendedLengthApdusSupported:I = 0x13

.field static final TRANSACTION_getMaxTransceiveLength:I = 0x12

.field static final TRANSACTION_getTechList:I = 0x4

.field static final TRANSACTION_getTimeout:I = 0xf

.field static final TRANSACTION_isNdef:I = 0x5

.field static final TRANSACTION_isPresent:I = 0x6

.field static final TRANSACTION_ndefIsWritable:I = 0xb

.field static final TRANSACTION_ndefMakeReadOnly:I = 0xa

.field static final TRANSACTION_ndefRead:I = 0x8

.field static final TRANSACTION_ndefWrite:I = 0x9

.field static final TRANSACTION_reconnect:I = 0x3

.field static final TRANSACTION_rediscover:I = 0xd

.field static final TRANSACTION_resetTimeouts:I = 0x10

.field static final TRANSACTION_setTimeout:I = 0xe

.field static final TRANSACTION_transceive:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.nfc.INfcTag"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/nfc/INfcTag$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcTag;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.nfc.INfcTag"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/nfc/INfcTag;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/nfc/INfcTag;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/nfc/INfcTag$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/nfc/INfcTag$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 41
    sparse-switch p1, :sswitch_data_1e4

    #@5
    .line 269
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 45
    :sswitch_a
    const-string v4, "android.nfc.INfcTag"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 50
    :sswitch_10
    const-string v4, "android.nfc.INfcTag"

    #@12
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 53
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->close(I)I

    #@1c
    move-result v3

    #@1d
    .line 54
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20
    .line 55
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    goto :goto_9

    #@24
    .line 60
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_24
    const-string v4, "android.nfc.INfcTag"

    #@26
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c
    move-result v0

    #@2d
    .line 64
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v1

    #@31
    .line 65
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/nfc/INfcTag$Stub;->connect(II)I

    #@34
    move-result v3

    #@35
    .line 66
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 67
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    goto :goto_9

    #@3c
    .line 72
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v3           #_result:I
    :sswitch_3c
    const-string v4, "android.nfc.INfcTag"

    #@3e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v0

    #@45
    .line 75
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->reconnect(I)I

    #@48
    move-result v3

    #@49
    .line 76
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    .line 77
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4f
    goto :goto_9

    #@50
    .line 82
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_50
    const-string v4, "android.nfc.INfcTag"

    #@52
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@55
    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@58
    move-result v0

    #@59
    .line 85
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->getTechList(I)[I

    #@5c
    move-result-object v3

    #@5d
    .line 86
    .local v3, _result:[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@60
    .line 87
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    #@63
    goto :goto_9

    #@64
    .line 92
    .end local v0           #_arg0:I
    .end local v3           #_result:[I
    :sswitch_64
    const-string v6, "android.nfc.INfcTag"

    #@66
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@69
    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6c
    move-result v0

    #@6d
    .line 95
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->isNdef(I)Z

    #@70
    move-result v3

    #@71
    .line 96
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@74
    .line 97
    if-eqz v3, :cond_77

    #@76
    move v4, v5

    #@77
    :cond_77
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    goto :goto_9

    #@7b
    .line 102
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_7b
    const-string v6, "android.nfc.INfcTag"

    #@7d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@80
    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@83
    move-result v0

    #@84
    .line 105
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->isPresent(I)Z

    #@87
    move-result v3

    #@88
    .line 106
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b
    .line 107
    if-eqz v3, :cond_8e

    #@8d
    move v4, v5

    #@8e
    :cond_8e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@91
    goto/16 :goto_9

    #@93
    .line 112
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_93
    const-string v6, "android.nfc.INfcTag"

    #@95
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@98
    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9b
    move-result v0

    #@9c
    .line 116
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@9f
    move-result-object v1

    #@a0
    .line 118
    .local v1, _arg1:[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a3
    move-result v6

    #@a4
    if-eqz v6, :cond_b8

    #@a6
    move v2, v5

    #@a7
    .line 119
    .local v2, _arg2:Z
    :goto_a7
    invoke-virtual {p0, v0, v1, v2}, Landroid/nfc/INfcTag$Stub;->transceive(I[BZ)Landroid/nfc/TransceiveResult;

    #@aa
    move-result-object v3

    #@ab
    .line 120
    .local v3, _result:Landroid/nfc/TransceiveResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae
    .line 121
    if-eqz v3, :cond_ba

    #@b0
    .line 122
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b3
    .line 123
    invoke-virtual {v3, p3, v5}, Landroid/nfc/TransceiveResult;->writeToParcel(Landroid/os/Parcel;I)V

    #@b6
    goto/16 :goto_9

    #@b8
    .end local v2           #_arg2:Z
    .end local v3           #_result:Landroid/nfc/TransceiveResult;
    :cond_b8
    move v2, v4

    #@b9
    .line 118
    goto :goto_a7

    #@ba
    .line 126
    .restart local v2       #_arg2:Z
    .restart local v3       #_result:Landroid/nfc/TransceiveResult;
    :cond_ba
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@bd
    goto/16 :goto_9

    #@bf
    .line 132
    .end local v0           #_arg0:I
    .end local v1           #_arg1:[B
    .end local v2           #_arg2:Z
    .end local v3           #_result:Landroid/nfc/TransceiveResult;
    :sswitch_bf
    const-string v6, "android.nfc.INfcTag"

    #@c1
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c4
    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c7
    move-result v0

    #@c8
    .line 135
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->ndefRead(I)Landroid/nfc/NdefMessage;

    #@cb
    move-result-object v3

    #@cc
    .line 136
    .local v3, _result:Landroid/nfc/NdefMessage;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@cf
    .line 137
    if-eqz v3, :cond_d9

    #@d1
    .line 138
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@d4
    .line 139
    invoke-virtual {v3, p3, v5}, Landroid/nfc/NdefMessage;->writeToParcel(Landroid/os/Parcel;I)V

    #@d7
    goto/16 :goto_9

    #@d9
    .line 142
    :cond_d9
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@dc
    goto/16 :goto_9

    #@de
    .line 148
    .end local v0           #_arg0:I
    .end local v3           #_result:Landroid/nfc/NdefMessage;
    :sswitch_de
    const-string v4, "android.nfc.INfcTag"

    #@e0
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e3
    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e6
    move-result v0

    #@e7
    .line 152
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ea
    move-result v4

    #@eb
    if-eqz v4, :cond_101

    #@ed
    .line 153
    sget-object v4, Landroid/nfc/NdefMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ef
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f2
    move-result-object v1

    #@f3
    check-cast v1, Landroid/nfc/NdefMessage;

    #@f5
    .line 158
    .local v1, _arg1:Landroid/nfc/NdefMessage;
    :goto_f5
    invoke-virtual {p0, v0, v1}, Landroid/nfc/INfcTag$Stub;->ndefWrite(ILandroid/nfc/NdefMessage;)I

    #@f8
    move-result v3

    #@f9
    .line 159
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fc
    .line 160
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ff
    goto/16 :goto_9

    #@101
    .line 156
    .end local v1           #_arg1:Landroid/nfc/NdefMessage;
    .end local v3           #_result:I
    :cond_101
    const/4 v1, 0x0

    #@102
    .restart local v1       #_arg1:Landroid/nfc/NdefMessage;
    goto :goto_f5

    #@103
    .line 165
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/nfc/NdefMessage;
    :sswitch_103
    const-string v4, "android.nfc.INfcTag"

    #@105
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@108
    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10b
    move-result v0

    #@10c
    .line 168
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->ndefMakeReadOnly(I)I

    #@10f
    move-result v3

    #@110
    .line 169
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@113
    .line 170
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@116
    goto/16 :goto_9

    #@118
    .line 175
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_118
    const-string v6, "android.nfc.INfcTag"

    #@11a
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@11d
    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@120
    move-result v0

    #@121
    .line 178
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->ndefIsWritable(I)Z

    #@124
    move-result v3

    #@125
    .line 179
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@128
    .line 180
    if-eqz v3, :cond_12b

    #@12a
    move v4, v5

    #@12b
    :cond_12b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@12e
    goto/16 :goto_9

    #@130
    .line 185
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_130
    const-string v4, "android.nfc.INfcTag"

    #@132
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@135
    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@138
    move-result v0

    #@139
    .line 189
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@13c
    move-result-object v1

    #@13d
    .line 190
    .local v1, _arg1:[B
    invoke-virtual {p0, v0, v1}, Landroid/nfc/INfcTag$Stub;->formatNdef(I[B)I

    #@140
    move-result v3

    #@141
    .line 191
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@144
    .line 192
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@147
    goto/16 :goto_9

    #@149
    .line 197
    .end local v0           #_arg0:I
    .end local v1           #_arg1:[B
    .end local v3           #_result:I
    :sswitch_149
    const-string v6, "android.nfc.INfcTag"

    #@14b
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14e
    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@151
    move-result v0

    #@152
    .line 200
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->rediscover(I)Landroid/nfc/Tag;

    #@155
    move-result-object v3

    #@156
    .line 201
    .local v3, _result:Landroid/nfc/Tag;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@159
    .line 202
    if-eqz v3, :cond_163

    #@15b
    .line 203
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@15e
    .line 204
    invoke-virtual {v3, p3, v5}, Landroid/nfc/Tag;->writeToParcel(Landroid/os/Parcel;I)V

    #@161
    goto/16 :goto_9

    #@163
    .line 207
    :cond_163
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@166
    goto/16 :goto_9

    #@168
    .line 213
    .end local v0           #_arg0:I
    .end local v3           #_result:Landroid/nfc/Tag;
    :sswitch_168
    const-string v4, "android.nfc.INfcTag"

    #@16a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16d
    .line 215
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@170
    move-result v0

    #@171
    .line 217
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@174
    move-result v1

    #@175
    .line 218
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/nfc/INfcTag$Stub;->setTimeout(II)I

    #@178
    move-result v3

    #@179
    .line 219
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17c
    .line 220
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@17f
    goto/16 :goto_9

    #@181
    .line 225
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    .end local v3           #_result:I
    :sswitch_181
    const-string v4, "android.nfc.INfcTag"

    #@183
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@186
    .line 227
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@189
    move-result v0

    #@18a
    .line 228
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->getTimeout(I)I

    #@18d
    move-result v3

    #@18e
    .line 229
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@191
    .line 230
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@194
    goto/16 :goto_9

    #@196
    .line 235
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_196
    const-string v4, "android.nfc.INfcTag"

    #@198
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19b
    .line 236
    invoke-virtual {p0}, Landroid/nfc/INfcTag$Stub;->resetTimeouts()V

    #@19e
    .line 237
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a1
    goto/16 :goto_9

    #@1a3
    .line 242
    :sswitch_1a3
    const-string v6, "android.nfc.INfcTag"

    #@1a5
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a8
    .line 244
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ab
    move-result v0

    #@1ac
    .line 245
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->canMakeReadOnly(I)Z

    #@1af
    move-result v3

    #@1b0
    .line 246
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b3
    .line 247
    if-eqz v3, :cond_1b6

    #@1b5
    move v4, v5

    #@1b6
    :cond_1b6
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1b9
    goto/16 :goto_9

    #@1bb
    .line 252
    .end local v0           #_arg0:I
    .end local v3           #_result:Z
    :sswitch_1bb
    const-string v4, "android.nfc.INfcTag"

    #@1bd
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c0
    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c3
    move-result v0

    #@1c4
    .line 255
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/nfc/INfcTag$Stub;->getMaxTransceiveLength(I)I

    #@1c7
    move-result v3

    #@1c8
    .line 256
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cb
    .line 257
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1ce
    goto/16 :goto_9

    #@1d0
    .line 262
    .end local v0           #_arg0:I
    .end local v3           #_result:I
    :sswitch_1d0
    const-string v6, "android.nfc.INfcTag"

    #@1d2
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d5
    .line 263
    invoke-virtual {p0}, Landroid/nfc/INfcTag$Stub;->getExtendedLengthApdusSupported()Z

    #@1d8
    move-result v3

    #@1d9
    .line 264
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1dc
    .line 265
    if-eqz v3, :cond_1df

    #@1de
    move v4, v5

    #@1df
    :cond_1df
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e2
    goto/16 :goto_9

    #@1e4
    .line 41
    :sswitch_data_1e4
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_24
        0x3 -> :sswitch_3c
        0x4 -> :sswitch_50
        0x5 -> :sswitch_64
        0x6 -> :sswitch_7b
        0x7 -> :sswitch_93
        0x8 -> :sswitch_bf
        0x9 -> :sswitch_de
        0xa -> :sswitch_103
        0xb -> :sswitch_118
        0xc -> :sswitch_130
        0xd -> :sswitch_149
        0xe -> :sswitch_168
        0xf -> :sswitch_181
        0x10 -> :sswitch_196
        0x11 -> :sswitch_1a3
        0x12 -> :sswitch_1bb
        0x13 -> :sswitch_1d0
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
