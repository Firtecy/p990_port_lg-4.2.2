.class public final Landroid/nfc/NfcActivityManager;
.super Landroid/nfc/INdefPushCallback$Stub;
.source "NfcActivityManager.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/nfc/NfcActivityManager$NfcActivityState;,
        Landroid/nfc/NfcActivityManager$NfcApplicationState;
    }
.end annotation


# static fields
.field static final DBG:Ljava/lang/Boolean; = null

.field static final TAG:Ljava/lang/String; = "NFC"


# instance fields
.field final mActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/nfc/NfcActivityManager$NfcActivityState;",
            ">;"
        }
    .end annotation
.end field

.field final mAdapter:Landroid/nfc/NfcAdapter;

.field final mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/nfc/NfcActivityManager$NfcApplicationState;",
            ">;"
        }
    .end annotation
.end field

.field final mDefaultEvent:Landroid/nfc/NfcEvent;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4
    move-result-object v0

    #@5
    sput-object v0, Landroid/nfc/NfcActivityManager;->DBG:Ljava/lang/Boolean;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/nfc/NfcAdapter;)V
    .registers 4
    .parameter "adapter"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Landroid/nfc/INdefPushCallback$Stub;-><init>()V

    #@3
    .line 192
    iput-object p1, p0, Landroid/nfc/NfcActivityManager;->mAdapter:Landroid/nfc/NfcAdapter;

    #@5
    .line 193
    new-instance v0, Ljava/util/LinkedList;

    #@7
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@a
    iput-object v0, p0, Landroid/nfc/NfcActivityManager;->mActivities:Ljava/util/List;

    #@c
    .line 194
    new-instance v0, Ljava/util/ArrayList;

    #@e
    const/4 v1, 0x1

    #@f
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    #@12
    iput-object v0, p0, Landroid/nfc/NfcActivityManager;->mApps:Ljava/util/List;

    #@14
    .line 195
    new-instance v0, Landroid/nfc/NfcEvent;

    #@16
    iget-object v1, p0, Landroid/nfc/NfcActivityManager;->mAdapter:Landroid/nfc/NfcAdapter;

    #@18
    invoke-direct {v0, v1}, Landroid/nfc/NfcEvent;-><init>(Landroid/nfc/NfcAdapter;)V

    #@1b
    iput-object v0, p0, Landroid/nfc/NfcActivityManager;->mDefaultEvent:Landroid/nfc/NfcEvent;

    #@1d
    .line 196
    return-void
.end method


# virtual methods
.method public createMessage()Landroid/nfc/NdefMessage;
    .registers 5

    #@0
    .prologue
    .line 280
    monitor-enter p0

    #@1
    .line 281
    :try_start_1
    invoke-virtual {p0}, Landroid/nfc/NfcActivityManager;->findResumedActivityState()Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v2

    #@5
    .line 282
    .local v2, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    if-nez v2, :cond_a

    #@7
    const/4 v1, 0x0

    #@8
    monitor-exit p0

    #@9
    .line 292
    :cond_9
    :goto_9
    return-object v1

    #@a
    .line 284
    :cond_a
    iget-object v0, v2, Landroid/nfc/NfcActivityManager$NfcActivityState;->ndefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    #@c
    .line 285
    .local v0, callback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
    iget-object v1, v2, Landroid/nfc/NfcActivityManager$NfcActivityState;->ndefMessage:Landroid/nfc/NdefMessage;

    #@e
    .line 286
    .local v1, message:Landroid/nfc/NdefMessage;
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_18

    #@f
    .line 289
    if-eqz v0, :cond_9

    #@11
    .line 290
    iget-object v3, p0, Landroid/nfc/NfcActivityManager;->mDefaultEvent:Landroid/nfc/NfcEvent;

    #@13
    invoke-interface {v0, v3}, Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;->createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;

    #@16
    move-result-object v1

    #@17
    goto :goto_9

    #@18
    .line 286
    .end local v0           #callback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
    .end local v1           #message:Landroid/nfc/NdefMessage;
    .end local v2           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_18
    move-exception v3

    #@19
    :try_start_19
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    #@1a
    throw v3
.end method

.method declared-synchronized destroyActivityState(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    .line 184
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v0

    #@5
    .line 185
    .local v0, activityState:Landroid/nfc/NfcActivityManager$NfcActivityState;
    if-eqz v0, :cond_f

    #@7
    .line 186
    invoke-virtual {v0}, Landroid/nfc/NfcActivityManager$NfcActivityState;->destroy()V

    #@a
    .line 187
    iget-object v1, p0, Landroid/nfc/NfcActivityManager;->mActivities:Ljava/util/List;

    #@c
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    #@f
    .line 189
    :cond_f
    monitor-exit p0

    #@10
    return-void

    #@11
    .line 184
    .end local v0           #activityState:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v1

    #@12
    monitor-exit p0

    #@13
    throw v1
.end method

.method declared-synchronized findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    .registers 5
    .parameter "activity"

    #@0
    .prologue
    .line 156
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/nfc/NfcActivityManager;->mActivities:Ljava/util/List;

    #@3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v0

    #@7
    .local v0, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_19

    #@d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@13
    .line 157
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iget-object v2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->activity:Landroid/app/Activity;
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_1b

    #@15
    if-ne v2, p1, :cond_7

    #@17
    .line 161
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :goto_17
    monitor-exit p0

    #@18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_17

    #@1b
    .line 156
    .end local v0           #i$:Ljava/util/Iterator;
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit p0

    #@1d
    throw v2
.end method

.method findAppState(Landroid/app/Application;)Landroid/nfc/NfcActivityManager$NfcApplicationState;
    .registers 5
    .parameter "app"

    #@0
    .prologue
    .line 76
    iget-object v2, p0, Landroid/nfc/NfcActivityManager;->mApps:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_17

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Landroid/nfc/NfcActivityManager$NfcApplicationState;

    #@12
    .line 77
    .local v0, appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    iget-object v2, v0, Landroid/nfc/NfcActivityManager$NfcApplicationState;->app:Landroid/app/Application;

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 81
    .end local v0           #appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    :goto_16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method declared-synchronized findResumedActivityState()Landroid/nfc/NfcActivityManager$NfcActivityState;
    .registers 4

    #@0
    .prologue
    .line 175
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/nfc/NfcActivityManager;->mActivities:Ljava/util/List;

    #@3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@6
    move-result-object v0

    #@7
    .local v0, i$:Ljava/util/Iterator;
    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_19

    #@d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@13
    .line 176
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iget-boolean v2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_1b

    #@15
    if-eqz v2, :cond_7

    #@17
    .line 180
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :goto_17
    monitor-exit p0

    #@18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_17

    #@1b
    .line 175
    .end local v0           #i$:Ljava/util/Iterator;
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit p0

    #@1d
    throw v2
.end method

.method declared-synchronized getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    .line 166
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v0

    #@5
    .line 167
    .local v0, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    if-nez v0, :cond_11

    #@7
    .line 168
    new-instance v0, Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@9
    .end local v0           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    invoke-direct {v0, p0, p1}, Landroid/nfc/NfcActivityManager$NfcActivityState;-><init>(Landroid/nfc/NfcActivityManager;Landroid/app/Activity;)V

    #@c
    .line 169
    .restart local v0       #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iget-object v1, p0, Landroid/nfc/NfcActivityManager;->mActivities:Ljava/util/List;

    #@e
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_13

    #@11
    .line 171
    :cond_11
    monitor-exit p0

    #@12
    return-object v0

    #@13
    .line 166
    .end local v0           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0

    #@15
    throw v1
.end method

.method public getUris()[Landroid/net/Uri;
    .registers 12

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 301
    monitor-enter p0

    #@2
    .line 302
    :try_start_2
    invoke-virtual {p0}, Landroid/nfc/NfcActivityManager;->findResumedActivityState()Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@5
    move-result-object v5

    #@6
    .line 303
    .local v5, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    if-nez v5, :cond_b

    #@8
    monitor-exit p0

    #@9
    move-object v7, v8

    #@a
    .line 326
    :cond_a
    :goto_a
    return-object v7

    #@b
    .line 304
    :cond_b
    iget-object v7, v5, Landroid/nfc/NfcActivityManager$NfcActivityState;->uris:[Landroid/net/Uri;

    #@d
    .line 305
    .local v7, uris:[Landroid/net/Uri;
    iget-object v1, v5, Landroid/nfc/NfcActivityManager$NfcActivityState;->uriCallback:Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;

    #@f
    .line 306
    .local v1, callback:Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_2 .. :try_end_10} :catchall_2c

    #@10
    .line 307
    if-eqz v1, :cond_a

    #@12
    .line 308
    iget-object v9, p0, Landroid/nfc/NfcActivityManager;->mDefaultEvent:Landroid/nfc/NfcEvent;

    #@14
    invoke-interface {v1, v9}, Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;->createBeamUris(Landroid/nfc/NfcEvent;)[Landroid/net/Uri;

    #@17
    move-result-object v7

    #@18
    .line 309
    if-eqz v7, :cond_a

    #@1a
    .line 310
    move-object v0, v7

    #@1b
    .local v0, arr$:[Landroid/net/Uri;
    array-length v3, v0

    #@1c
    .local v3, len$:I
    const/4 v2, 0x0

    #@1d
    .local v2, i$:I
    :goto_1d
    if-ge v2, v3, :cond_a

    #@1f
    aget-object v6, v0, v2

    #@21
    .line 311
    .local v6, uri:Landroid/net/Uri;
    if-nez v6, :cond_2f

    #@23
    .line 312
    const-string v9, "NFC"

    #@25
    const-string v10, "Uri not allowed to be null."

    #@27
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    move-object v7, v8

    #@2b
    .line 313
    goto :goto_a

    #@2c
    .line 306
    .end local v0           #arr$:[Landroid/net/Uri;
    .end local v1           #callback:Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    .end local v6           #uri:Landroid/net/Uri;
    .end local v7           #uris:[Landroid/net/Uri;
    :catchall_2c
    move-exception v8

    #@2d
    :try_start_2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    #@2e
    throw v8

    #@2f
    .line 315
    .restart local v0       #arr$:[Landroid/net/Uri;
    .restart local v1       #callback:Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v5       #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    .restart local v6       #uri:Landroid/net/Uri;
    .restart local v7       #uris:[Landroid/net/Uri;
    :cond_2f
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@32
    move-result-object v4

    #@33
    .line 316
    .local v4, scheme:Ljava/lang/String;
    if-eqz v4, :cond_45

    #@35
    const-string v9, "file"

    #@37
    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3a
    move-result v9

    #@3b
    if-nez v9, :cond_4e

    #@3d
    const-string v9, "content"

    #@3f
    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@42
    move-result v9

    #@43
    if-nez v9, :cond_4e

    #@45
    .line 318
    :cond_45
    const-string v9, "NFC"

    #@47
    const-string v10, "Uri needs to have either scheme file or scheme content"

    #@49
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    move-object v7, v8

    #@4d
    .line 320
    goto :goto_a

    #@4e
    .line 310
    :cond_4e
    add-int/lit8 v2, v2, 0x1

    #@50
    goto :goto_1d
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .parameter "activity"
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 349
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .registers 6
    .parameter "activity"

    #@0
    .prologue
    .line 390
    monitor-enter p0

    #@1
    .line 391
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v0

    #@5
    .line 392
    .local v0, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    sget-object v1, Landroid/nfc/NfcActivityManager;->DBG:Ljava/lang/Boolean;

    #@7
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_30

    #@d
    const-string v1, "NFC"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string/jumbo v3, "onDestroy() for "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 393
    :cond_30
    if-eqz v0, :cond_35

    #@32
    .line 395
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->destroyActivityState(Landroid/app/Activity;)V

    #@35
    .line 397
    :cond_35
    monitor-exit p0

    #@36
    .line 398
    return-void

    #@37
    .line 397
    .end local v0           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_37
    move-exception v1

    #@38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_1 .. :try_end_39} :catchall_37

    #@39
    throw v1
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .registers 7
    .parameter "activity"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 370
    monitor-enter p0

    #@2
    .line 371
    :try_start_2
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@5
    move-result-object v0

    #@6
    .line 372
    .local v0, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    sget-object v1, Landroid/nfc/NfcActivityManager;->DBG:Ljava/lang/Boolean;

    #@8
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_31

    #@e
    const-string v1, "NFC"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string/jumbo v3, "onPause() for "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, " "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 373
    :cond_31
    if-nez v0, :cond_35

    #@33
    monitor-exit p0

    #@34
    .line 377
    :goto_34
    return-void

    #@35
    .line 374
    :cond_35
    const/4 v1, 0x0

    #@36
    iput-boolean v1, v0, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@38
    .line 375
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_2 .. :try_end_39} :catchall_3d

    #@39
    .line 376
    invoke-virtual {p0, v4}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@3c
    goto :goto_34

    #@3d
    .line 375
    .end local v0           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_3d
    move-exception v1

    #@3e
    :try_start_3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v1
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .registers 7
    .parameter "activity"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 358
    monitor-enter p0

    #@2
    .line 359
    :try_start_2
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@5
    move-result-object v0

    #@6
    .line 360
    .local v0, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    sget-object v1, Landroid/nfc/NfcActivityManager;->DBG:Ljava/lang/Boolean;

    #@8
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    #@b
    move-result v1

    #@c
    if-eqz v1, :cond_31

    #@e
    const-string v1, "NFC"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string/jumbo v3, "onResume() for "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, " "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 361
    :cond_31
    if-nez v0, :cond_35

    #@33
    monitor-exit p0

    #@34
    .line 365
    :goto_34
    return-void

    #@35
    .line 362
    :cond_35
    const/4 v1, 0x1

    #@36
    iput-boolean v1, v0, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@38
    .line 363
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_2 .. :try_end_39} :catchall_3d

    #@39
    .line 364
    invoke-virtual {p0, v4}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@3c
    goto :goto_34

    #@3d
    .line 363
    .end local v0           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_3d
    move-exception v1

    #@3e
    :try_start_3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_3e .. :try_end_3f} :catchall_3d

    #@3f
    throw v1
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 3
    .parameter "activity"
    .parameter "outState"

    #@0
    .prologue
    .line 385
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 353
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    #@0
    .prologue
    .line 381
    return-void
.end method

.method public onNdefPushComplete()V
    .registers 4

    #@0
    .prologue
    .line 334
    monitor-enter p0

    #@1
    .line 335
    :try_start_1
    invoke-virtual {p0}, Landroid/nfc/NfcActivityManager;->findResumedActivityState()Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 336
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    if-nez v1, :cond_9

    #@7
    monitor-exit p0

    #@8
    .line 345
    :cond_8
    :goto_8
    return-void

    #@9
    .line 338
    :cond_9
    iget-object v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->onNdefPushCompleteCallback:Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;

    #@b
    .line 339
    .local v0, callback:Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_14

    #@c
    .line 342
    if-eqz v0, :cond_8

    #@e
    .line 343
    iget-object v2, p0, Landroid/nfc/NfcActivityManager;->mDefaultEvent:Landroid/nfc/NfcEvent;

    #@10
    invoke-interface {v0, v2}, Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;->onNdefPushComplete(Landroid/nfc/NfcEvent;)V

    #@13
    goto :goto_8

    #@14
    .line 339
    .end local v0           #callback:Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_14
    move-exception v2

    #@15
    :try_start_15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_14

    #@16
    throw v2
.end method

.method registerApplication(Landroid/app/Application;)V
    .registers 4
    .parameter "app"

    #@0
    .prologue
    .line 85
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findAppState(Landroid/app/Application;)Landroid/nfc/NfcActivityManager$NfcApplicationState;

    #@3
    move-result-object v0

    #@4
    .line 86
    .local v0, appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    if-nez v0, :cond_10

    #@6
    .line 87
    new-instance v0, Landroid/nfc/NfcActivityManager$NfcApplicationState;

    #@8
    .end local v0           #appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    invoke-direct {v0, p0, p1}, Landroid/nfc/NfcActivityManager$NfcApplicationState;-><init>(Landroid/nfc/NfcActivityManager;Landroid/app/Application;)V

    #@b
    .line 88
    .restart local v0       #appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    iget-object v1, p0, Landroid/nfc/NfcActivityManager;->mApps:Ljava/util/List;

    #@d
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@10
    .line 90
    :cond_10
    invoke-virtual {v0}, Landroid/nfc/NfcActivityManager$NfcApplicationState;->register()V

    #@13
    .line 91
    return-void
.end method

.method requestNfcServiceCallback(Z)V
    .registers 5
    .parameter "request"

    #@0
    .prologue
    .line 269
    :try_start_0
    sget-object v2, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    if-eqz p1, :cond_9

    #@4
    move-object v1, p0

    #@5
    :goto_5
    invoke-interface {v2, v1}, Landroid/nfc/INfcAdapter;->setNdefPushCallback(Landroid/nfc/INdefPushCallback;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_8} :catch_b

    #@8
    .line 273
    :goto_8
    return-void

    #@9
    .line 269
    :cond_9
    const/4 v1, 0x0

    #@a
    goto :goto_5

    #@b
    .line 270
    :catch_b
    move-exception v0

    #@c
    .line 271
    .local v0, e:Landroid/os/RemoteException;
    iget-object v1, p0, Landroid/nfc/NfcActivityManager;->mAdapter:Landroid/nfc/NfcAdapter;

    #@e
    invoke-virtual {v1, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@11
    goto :goto_8
.end method

.method public setNdefPushContentUri(Landroid/app/Activity;[Landroid/net/Uri;)V
    .registers 6
    .parameter "activity"
    .parameter "uris"

    #@0
    .prologue
    .line 200
    monitor-enter p0

    #@1
    .line 201
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 202
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iput-object p2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->uris:[Landroid/net/Uri;

    #@7
    .line 203
    iget-boolean v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@9
    .line 204
    .local v0, isResumed:Z
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    #@a
    .line 205
    if-eqz v0, :cond_10

    #@c
    .line 206
    const/4 v2, 0x1

    #@d
    invoke-virtual {p0, v2}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@10
    .line 208
    :cond_10
    return-void

    #@11
    .line 204
    .end local v0           #isResumed:Z
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v2

    #@12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method public setNdefPushContentUriCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;)V
    .registers 6
    .parameter "activity"
    .parameter "callback"

    #@0
    .prologue
    .line 214
    monitor-enter p0

    #@1
    .line 215
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 216
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iput-object p2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->uriCallback:Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;

    #@7
    .line 217
    iget-boolean v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@9
    .line 218
    .local v0, isResumed:Z
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    #@a
    .line 219
    if-eqz v0, :cond_10

    #@c
    .line 220
    const/4 v2, 0x1

    #@d
    invoke-virtual {p0, v2}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@10
    .line 222
    :cond_10
    return-void

    #@11
    .line 218
    .end local v0           #isResumed:Z
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v2

    #@12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method public setNdefPushMessage(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V
    .registers 6
    .parameter "activity"
    .parameter "message"

    #@0
    .prologue
    .line 226
    monitor-enter p0

    #@1
    .line 227
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 228
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iput-object p2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->ndefMessage:Landroid/nfc/NdefMessage;

    #@7
    .line 229
    iget-boolean v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@9
    .line 230
    .local v0, isResumed:Z
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    #@a
    .line 231
    if-eqz v0, :cond_10

    #@c
    .line 232
    const/4 v2, 0x1

    #@d
    invoke-virtual {p0, v2}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@10
    .line 234
    :cond_10
    return-void

    #@11
    .line 230
    .end local v0           #isResumed:Z
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v2

    #@12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method public setNdefPushMessageCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;)V
    .registers 6
    .parameter "activity"
    .parameter "callback"

    #@0
    .prologue
    .line 239
    monitor-enter p0

    #@1
    .line 240
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 241
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iput-object p2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->ndefMessageCallback:Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;

    #@7
    .line 242
    iget-boolean v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@9
    .line 243
    .local v0, isResumed:Z
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    #@a
    .line 244
    if-eqz v0, :cond_10

    #@c
    .line 245
    const/4 v2, 0x1

    #@d
    invoke-virtual {p0, v2}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@10
    .line 247
    :cond_10
    return-void

    #@11
    .line 243
    .end local v0           #isResumed:Z
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v2

    #@12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method public setOnNdefPushCompleteCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V
    .registers 6
    .parameter "activity"
    .parameter "callback"

    #@0
    .prologue
    .line 252
    monitor-enter p0

    #@1
    .line 253
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->getActivityState(Landroid/app/Activity;)Landroid/nfc/NfcActivityManager$NfcActivityState;

    #@4
    move-result-object v1

    #@5
    .line 254
    .local v1, state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    iput-object p2, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->onNdefPushCompleteCallback:Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;

    #@7
    .line 255
    iget-boolean v0, v1, Landroid/nfc/NfcActivityManager$NfcActivityState;->resumed:Z

    #@9
    .line 256
    .local v0, isResumed:Z
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_11

    #@a
    .line 257
    if-eqz v0, :cond_10

    #@c
    .line 258
    const/4 v2, 0x1

    #@d
    invoke-virtual {p0, v2}, Landroid/nfc/NfcActivityManager;->requestNfcServiceCallback(Z)V

    #@10
    .line 260
    :cond_10
    return-void

    #@11
    .line 256
    .end local v0           #isResumed:Z
    .end local v1           #state:Landroid/nfc/NfcActivityManager$NfcActivityState;
    :catchall_11
    move-exception v2

    #@12
    :try_start_12
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_11

    #@13
    throw v2
.end method

.method unregisterApplication(Landroid/app/Application;)V
    .registers 6
    .parameter "app"

    #@0
    .prologue
    .line 94
    invoke-virtual {p0, p1}, Landroid/nfc/NfcActivityManager;->findAppState(Landroid/app/Application;)Landroid/nfc/NfcActivityManager$NfcApplicationState;

    #@3
    move-result-object v0

    #@4
    .line 95
    .local v0, appState:Landroid/nfc/NfcActivityManager$NfcApplicationState;
    if-nez v0, :cond_1f

    #@6
    .line 96
    const-string v1, "NFC"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "app was not registered "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 100
    :goto_1e
    return-void

    #@1f
    .line 99
    :cond_1f
    invoke-virtual {v0}, Landroid/nfc/NfcActivityManager$NfcApplicationState;->unregister()V

    #@22
    goto :goto_1e
.end method
