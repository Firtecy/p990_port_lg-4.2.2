.class public final Landroid/nfc/TransceiveResult;
.super Ljava/lang/Object;
.source "TransceiveResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/nfc/TransceiveResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final RESULT_EXCEEDED_LENGTH:I = 0x3

.field public static final RESULT_FAILURE:I = 0x1

.field public static final RESULT_SUCCESS:I = 0x0

.field public static final RESULT_TAGLOST:I = 0x2


# instance fields
.field final mResponseData:[B

.field final mResult:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 70
    new-instance v0, Landroid/nfc/TransceiveResult$1;

    #@2
    invoke-direct {v0}, Landroid/nfc/TransceiveResult$1;-><init>()V

    #@5
    sput-object v0, Landroid/nfc/TransceiveResult;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(I[B)V
    .registers 3
    .parameter "result"
    .parameter "data"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput p1, p0, Landroid/nfc/TransceiveResult;->mResult:I

    #@5
    .line 40
    iput-object p2, p0, Landroid/nfc/TransceiveResult;->mResponseData:[B

    #@7
    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 58
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getResponseOrThrow()[B
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 44
    iget v0, p0, Landroid/nfc/TransceiveResult;->mResult:I

    #@2
    packed-switch v0, :pswitch_data_20

    #@5
    .line 52
    :pswitch_5
    new-instance v0, Ljava/io/IOException;

    #@7
    const-string v1, "Transceive failed"

    #@9
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 46
    :pswitch_d
    iget-object v0, p0, Landroid/nfc/TransceiveResult;->mResponseData:[B

    #@f
    return-object v0

    #@10
    .line 48
    :pswitch_10
    new-instance v0, Landroid/nfc/TagLostException;

    #@12
    const-string v1, "Tag was lost."

    #@14
    invoke-direct {v0, v1}, Landroid/nfc/TagLostException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 50
    :pswitch_18
    new-instance v0, Ljava/io/IOException;

    #@1a
    const-string v1, "Transceive length exceeds supported maximum"

    #@1c
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 44
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_d
        :pswitch_5
        :pswitch_10
        :pswitch_18
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/nfc/TransceiveResult;->mResult:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 64
    iget v0, p0, Landroid/nfc/TransceiveResult;->mResult:I

    #@7
    if-nez v0, :cond_14

    #@9
    .line 65
    iget-object v0, p0, Landroid/nfc/TransceiveResult;->mResponseData:[B

    #@b
    array-length v0, v0

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 66
    iget-object v0, p0, Landroid/nfc/TransceiveResult;->mResponseData:[B

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@14
    .line 68
    :cond_14
    return-void
.end method
