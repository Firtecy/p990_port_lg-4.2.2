.class public final Landroid/nfc/NfcAdapter;
.super Ljava/lang/Object;
.source "NfcAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;,
        Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;,
        Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;
    }
.end annotation


# static fields
.field public static final ACTION_ADAPTER_STATE_CHANGED:Ljava/lang/String; = "android.nfc.action.ADAPTER_STATE_CHANGED"

.field public static final ACTION_HANDOVER_TRANSFER_DONE:Ljava/lang/String; = "android.nfc.action.HANDOVER_TRANSFER_DONE"

.field public static final ACTION_HANDOVER_TRANSFER_STARTED:Ljava/lang/String; = "android.nfc.action.HANDOVER_TRANSFER_STARTED"

.field public static final ACTION_NDEF_DISCOVERED:Ljava/lang/String; = "android.nfc.action.NDEF_DISCOVERED"

.field public static final ACTION_TAG_DISCOVERED:Ljava/lang/String; = "android.nfc.action.TAG_DISCOVERED"

.field public static final ACTION_TAG_LEFT_FIELD:Ljava/lang/String; = "android.nfc.action.TAG_LOST"

.field public static final ACTION_TECH_DISCOVERED:Ljava/lang/String; = "android.nfc.action.TECH_DISCOVERED"

.field public static final EXTRA_ADAPTER_STATE:Ljava/lang/String; = "android.nfc.extra.ADAPTER_STATE"

.field public static final EXTRA_HANDOVER_TRANSFER_STATUS:Ljava/lang/String; = "android.nfc.extra.HANDOVER_TRANSFER_STATUS"

.field public static final EXTRA_HANDOVER_TRANSFER_URI:Ljava/lang/String; = "android.nfc.extra.HANDOVER_TRANSFER_URI"

.field public static final EXTRA_ID:Ljava/lang/String; = "android.nfc.extra.ID"

.field public static final EXTRA_NDEF_MESSAGES:Ljava/lang/String; = "android.nfc.extra.NDEF_MESSAGES"

.field public static final EXTRA_TAG:Ljava/lang/String; = "android.nfc.extra.TAG"

.field public static final HANDOVER_TRANSFER_STATUS_FAILURE:I = 0x1

.field public static final HANDOVER_TRANSFER_STATUS_SUCCESS:I = 0x0

.field public static final STATE_OFF:I = 0x1

.field public static final STATE_ON:I = 0x3

.field public static final STATE_TURNING_OFF:I = 0x4

.field public static final STATE_TURNING_ON:I = 0x2

.field static final TAG:Ljava/lang/String; = "NFC"

.field static sIsInitialized:Z

.field static sNfcAdapters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Landroid/nfc/NfcAdapter;",
            ">;"
        }
    .end annotation
.end field

.field static sNullContextNfcAdapter:Landroid/nfc/NfcAdapter;

.field static sService:Landroid/nfc/INfcAdapter;

.field static sTagService:Landroid/nfc/INfcTag;


# instance fields
.field final mContext:Landroid/content/Context;

.field mForegroundDispatchListener:Landroid/app/OnActivityPausedListener;

.field final mNfcActivityManager:Landroid/nfc/NfcActivityManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 226
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/nfc/NfcAdapter;->sIsInitialized:Z

    #@3
    .line 239
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    sput-object v0, Landroid/nfc/NfcAdapter;->sNfcAdapters:Ljava/util/HashMap;

    #@a
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 431
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1053
    new-instance v0, Landroid/nfc/NfcAdapter$1;

    #@5
    invoke-direct {v0, p0}, Landroid/nfc/NfcAdapter$1;-><init>(Landroid/nfc/NfcAdapter;)V

    #@8
    iput-object v0, p0, Landroid/nfc/NfcAdapter;->mForegroundDispatchListener:Landroid/app/OnActivityPausedListener;

    #@a
    .line 432
    iput-object p1, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@c
    .line 433
    new-instance v0, Landroid/nfc/NfcActivityManager;

    #@e
    invoke-direct {v0, p0}, Landroid/nfc/NfcActivityManager;-><init>(Landroid/nfc/NfcAdapter;)V

    #@11
    iput-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@13
    .line 434
    return-void
.end method

.method public static getDefaultAdapter()Landroid/nfc/NfcAdapter;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 425
    const-string v0, "NFC"

    #@2
    const-string v1, "WARNING: NfcAdapter.getDefaultAdapter() is deprecated, use NfcAdapter.getDefaultAdapter(Context) instead"

    #@4
    new-instance v2, Ljava/lang/Exception;

    #@6
    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    #@9
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c
    .line 428
    const/4 v0, 0x0

    #@d
    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getNfcAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public static getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 394
    if-nez p0, :cond_a

    #@2
    .line 395
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v2, "context cannot be null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 397
    :cond_a
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@d
    move-result-object p0

    #@e
    .line 398
    if-nez p0, :cond_18

    #@10
    .line 399
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v2, "context not associated with any application (using a mock context?)"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1

    #@18
    .line 403
    :cond_18
    const-string/jumbo v1, "nfc"

    #@1b
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/nfc/NfcManager;

    #@21
    .line 404
    .local v0, manager:Landroid/nfc/NfcManager;
    if-nez v0, :cond_25

    #@23
    .line 406
    const/4 v1, 0x0

    #@24
    .line 408
    :goto_24
    return-object v1

    #@25
    :cond_25
    invoke-virtual {v0}, Landroid/nfc/NfcManager;->getDefaultAdapter()Landroid/nfc/NfcAdapter;

    #@28
    move-result-object v1

    #@29
    goto :goto_24
.end method

.method public static declared-synchronized getNfcAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 335
    const-class v3, Landroid/nfc/NfcAdapter;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-boolean v2, Landroid/nfc/NfcAdapter;->sIsInitialized:Z

    #@5
    if-nez v2, :cond_40

    #@7
    .line 337
    invoke-static {}, Landroid/nfc/NfcAdapter;->hasNfcFeature()Z

    #@a
    move-result v2

    #@b
    if-nez v2, :cond_1e

    #@d
    .line 338
    const-string v2, "NFC"

    #@f
    const-string/jumbo v4, "this device does not have NFC support"

    #@12
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 339
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@17
    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@1a
    throw v2
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_1b

    #@1b
    .line 335
    :catchall_1b
    move-exception v2

    #@1c
    monitor-exit v3

    #@1d
    throw v2

    #@1e
    .line 342
    :cond_1e
    :try_start_1e
    invoke-static {}, Landroid/nfc/NfcAdapter;->getServiceInterface()Landroid/nfc/INfcAdapter;

    #@21
    move-result-object v2

    #@22
    sput-object v2, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@24
    .line 343
    sget-object v2, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@26
    if-nez v2, :cond_35

    #@28
    .line 344
    const-string v2, "NFC"

    #@2a
    const-string v4, "could not retrieve NFC service"

    #@2c
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 345
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@31
    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@34
    throw v2
    :try_end_35
    .catchall {:try_start_1e .. :try_end_35} :catchall_1b

    #@35
    .line 348
    :cond_35
    :try_start_35
    sget-object v2, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@37
    invoke-interface {v2}, Landroid/nfc/INfcAdapter;->getNfcTagInterface()Landroid/nfc/INfcTag;

    #@3a
    move-result-object v2

    #@3b
    sput-object v2, Landroid/nfc/NfcAdapter;->sTagService:Landroid/nfc/INfcTag;
    :try_end_3d
    .catchall {:try_start_35 .. :try_end_3d} :catchall_1b
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_3d} :catch_52

    #@3d
    .line 354
    const/4 v2, 0x1

    #@3e
    :try_start_3e
    sput-boolean v2, Landroid/nfc/NfcAdapter;->sIsInitialized:Z

    #@40
    .line 356
    :cond_40
    if-nez p0, :cond_60

    #@42
    .line 357
    sget-object v2, Landroid/nfc/NfcAdapter;->sNullContextNfcAdapter:Landroid/nfc/NfcAdapter;

    #@44
    if-nez v2, :cond_4e

    #@46
    .line 358
    new-instance v2, Landroid/nfc/NfcAdapter;

    #@48
    const/4 v4, 0x0

    #@49
    invoke-direct {v2, v4}, Landroid/nfc/NfcAdapter;-><init>(Landroid/content/Context;)V

    #@4c
    sput-object v2, Landroid/nfc/NfcAdapter;->sNullContextNfcAdapter:Landroid/nfc/NfcAdapter;

    #@4e
    .line 360
    :cond_4e
    sget-object v0, Landroid/nfc/NfcAdapter;->sNullContextNfcAdapter:Landroid/nfc/NfcAdapter;
    :try_end_50
    .catchall {:try_start_3e .. :try_end_50} :catchall_1b

    #@50
    .line 367
    :cond_50
    :goto_50
    monitor-exit v3

    #@51
    return-object v0

    #@52
    .line 349
    :catch_52
    move-exception v1

    #@53
    .line 350
    .local v1, e:Landroid/os/RemoteException;
    :try_start_53
    const-string v2, "NFC"

    #@55
    const-string v4, "could not retrieve NFC Tag service"

    #@57
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 351
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    #@5c
    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    #@5f
    throw v2

    #@60
    .line 362
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_60
    sget-object v2, Landroid/nfc/NfcAdapter;->sNfcAdapters:Ljava/util/HashMap;

    #@62
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    move-result-object v0

    #@66
    check-cast v0, Landroid/nfc/NfcAdapter;

    #@68
    .line 363
    .local v0, adapter:Landroid/nfc/NfcAdapter;
    if-nez v0, :cond_50

    #@6a
    .line 364
    new-instance v0, Landroid/nfc/NfcAdapter;

    #@6c
    .end local v0           #adapter:Landroid/nfc/NfcAdapter;
    invoke-direct {v0, p0}, Landroid/nfc/NfcAdapter;-><init>(Landroid/content/Context;)V

    #@6f
    .line 365
    .restart local v0       #adapter:Landroid/nfc/NfcAdapter;
    sget-object v2, Landroid/nfc/NfcAdapter;->sNfcAdapters:Ljava/util/HashMap;

    #@71
    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_74
    .catchall {:try_start_53 .. :try_end_74} :catchall_1b

    #@74
    goto :goto_50
.end method

.method private static getServiceInterface()Landroid/nfc/INfcAdapter;
    .registers 2

    #@0
    .prologue
    .line 373
    const-string/jumbo v1, "nfc"

    #@3
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v0

    #@7
    .line 374
    .local v0, b:Landroid/os/IBinder;
    if-nez v0, :cond_b

    #@9
    .line 375
    const/4 v1, 0x0

    #@a
    .line 377
    :goto_a
    return-object v1

    #@b
    :cond_b
    invoke-static {v0}, Landroid/nfc/INfcAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcAdapter;

    #@e
    move-result-object v1

    #@f
    goto :goto_a
.end method

.method private static hasNfcFeature()Z
    .registers 5

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 316
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    #@4
    move-result-object v1

    #@5
    .line 317
    .local v1, pm:Landroid/content/pm/IPackageManager;
    if-nez v1, :cond_f

    #@7
    .line 318
    const-string v3, "NFC"

    #@9
    const-string v4, "Cannot get package manager, assuming no NFC feature"

    #@b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 325
    :goto_e
    return v2

    #@f
    .line 322
    :cond_f
    :try_start_f
    const-string v3, "android.hardware.nfc"

    #@11
    invoke-interface {v1, v3}, Landroid/content/pm/IPackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_14} :catch_16

    #@14
    move-result v2

    #@15
    goto :goto_e

    #@16
    .line 323
    :catch_16
    move-exception v0

    #@17
    .line 324
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "NFC"

    #@19
    const-string v4, "Package manager query failed, assuming no NFC feature"

    #@1b
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1e
    goto :goto_e
.end method


# virtual methods
.method public attemptDeadServiceRecovery(Ljava/lang/Exception;)V
    .registers 6
    .parameter "e"

    #@0
    .prologue
    .line 466
    const-string v2, "NFC"

    #@2
    const-string v3, "NFC service dead - attempting to recover"

    #@4
    invoke-static {v2, v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7
    .line 467
    invoke-static {}, Landroid/nfc/NfcAdapter;->getServiceInterface()Landroid/nfc/INfcAdapter;

    #@a
    move-result-object v1

    #@b
    .line 468
    .local v1, service:Landroid/nfc/INfcAdapter;
    if-nez v1, :cond_15

    #@d
    .line 469
    const-string v2, "NFC"

    #@f
    const-string v3, "could not retrieve NFC service during service recovery"

    #@11
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 485
    :goto_14
    return-void

    #@15
    .line 476
    :cond_15
    sput-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@17
    .line 478
    :try_start_17
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->getNfcTagInterface()Landroid/nfc/INfcTag;

    #@1a
    move-result-object v2

    #@1b
    sput-object v2, Landroid/nfc/NfcAdapter;->sTagService:Landroid/nfc/INfcTag;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_1d} :catch_1e

    #@1d
    goto :goto_14

    #@1e
    .line 479
    :catch_1e
    move-exception v0

    #@1f
    .line 480
    .local v0, ee:Landroid/os/RemoteException;
    const-string v2, "NFC"

    #@21
    const-string v3, "could not retrieve NFC tag service during service recovery"

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    goto :goto_14
.end method

.method public disable()Z
    .registers 4

    #@0
    .prologue
    .line 577
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-interface {v1, v2}, Landroid/nfc/INfcAdapter;->disable(Z)Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_8

    #@6
    move-result v1

    #@7
    .line 580
    :goto_7
    return v1

    #@8
    .line 578
    :catch_8
    move-exception v0

    #@9
    .line 579
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@c
    .line 580
    const/4 v1, 0x0

    #@d
    goto :goto_7
.end method

.method public disableForegroundDispatch(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    .line 1048
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Landroid/nfc/NfcAdapter;->mForegroundDispatchListener:Landroid/app/OnActivityPausedListener;

    #@6
    invoke-virtual {v0, p1, v1}, Landroid/app/ActivityThread;->unregisterOnActivityPausedListener(Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V

    #@9
    .line 1050
    const/4 v0, 0x0

    #@a
    invoke-virtual {p0, p1, v0}, Landroid/nfc/NfcAdapter;->disableForegroundDispatchInternal(Landroid/app/Activity;Z)V

    #@d
    .line 1051
    return-void
.end method

.method disableForegroundDispatchInternal(Landroid/app/Activity;Z)V
    .registers 8
    .parameter "activity"
    .parameter "force"

    #@0
    .prologue
    .line 1062
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    const/4 v2, 0x0

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v4, 0x0

    #@5
    invoke-interface {v1, v2, v3, v4}, Landroid/nfc/INfcAdapter;->setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;Landroid/nfc/TechListParcel;)V

    #@8
    .line 1063
    if-nez p2, :cond_1c

    #@a
    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_1c

    #@10
    .line 1064
    new-instance v1, Ljava/lang/IllegalStateException;

    #@12
    const-string v2, "You must disable foreground dispatching while your activity is still resumed"

    #@14
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v1
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_18} :catch_18

    #@18
    .line 1067
    :catch_18
    move-exception v0

    #@19
    .line 1068
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@1c
    .line 1070
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1c
    return-void
.end method

.method public disableForegroundNdefPush(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1128
    if-nez p1, :cond_9

    #@3
    .line 1129
    new-instance v0, Ljava/lang/NullPointerException;

    #@5
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@8
    throw v0

    #@9
    .line 1131
    :cond_9
    invoke-virtual {p0, p1}, Landroid/nfc/NfcAdapter;->enforceResumed(Landroid/app/Activity;)V

    #@c
    .line 1132
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@e
    invoke-virtual {v0, p1, v1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessage(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V

    #@11
    .line 1133
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@13
    invoke-virtual {v0, p1, v1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessageCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;)V

    #@16
    .line 1134
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@18
    invoke-virtual {v0, p1, v1}, Landroid/nfc/NfcActivityManager;->setOnNdefPushCompleteCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V

    #@1b
    .line 1135
    return-void
.end method

.method public disableNdefPush()Z
    .registers 3

    #@0
    .prologue
    .line 1158
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->disableNdefPush()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1161
    :goto_6
    return v1

    #@7
    .line 1159
    :catch_7
    move-exception v0

    #@8
    .line 1160
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@b
    .line 1161
    const/4 v1, 0x0

    #@c
    goto :goto_6
.end method

.method public dispatch(Landroid/nfc/Tag;)V
    .registers 5
    .parameter "tag"

    #@0
    .prologue
    .line 1205
    if-nez p1, :cond_b

    #@2
    .line 1206
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v2, "tag cannot be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1209
    :cond_b
    :try_start_b
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@d
    invoke-interface {v1, p1}, Landroid/nfc/INfcAdapter;->dispatch(Landroid/nfc/Tag;)V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_10} :catch_11

    #@10
    .line 1213
    :goto_10
    return-void

    #@11
    .line 1210
    :catch_11
    move-exception v0

    #@12
    .line 1211
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@15
    goto :goto_10
.end method

.method public enable()Z
    .registers 3

    #@0
    .prologue
    .line 549
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->enable()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 552
    :goto_6
    return v1

    #@7
    .line 550
    :catch_7
    move-exception v0

    #@8
    .line 551
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@b
    .line 552
    const/4 v1, 0x0

    #@c
    goto :goto_6
.end method

.method public enableForegroundDispatch(Landroid/app/Activity;Landroid/app/PendingIntent;[Landroid/content/IntentFilter;[[Ljava/lang/String;)V
    .registers 10
    .parameter "activity"
    .parameter "intent"
    .parameter "filters"
    .parameter "techLists"

    #@0
    .prologue
    .line 1013
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 1014
    :cond_4
    new-instance v3, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v3

    #@a
    .line 1016
    :cond_a
    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    #@d
    move-result v3

    #@e
    if-nez v3, :cond_18

    #@10
    .line 1017
    new-instance v3, Ljava/lang/IllegalStateException;

    #@12
    const-string v4, "Foreground dispatch can only be enabled when your activity is resumed"

    #@14
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3

    #@18
    .line 1021
    :cond_18
    const/4 v1, 0x0

    #@19
    .line 1022
    .local v1, parcel:Landroid/nfc/TechListParcel;
    if-eqz p4, :cond_24

    #@1b
    :try_start_1b
    array-length v3, p4

    #@1c
    if-lez v3, :cond_24

    #@1e
    .line 1023
    new-instance v2, Landroid/nfc/TechListParcel;

    #@20
    invoke-direct {v2, p4}, Landroid/nfc/TechListParcel;-><init>([[Ljava/lang/String;)V

    #@23
    .end local v1           #parcel:Landroid/nfc/TechListParcel;
    .local v2, parcel:Landroid/nfc/TechListParcel;
    move-object v1, v2

    #@24
    .line 1025
    .end local v2           #parcel:Landroid/nfc/TechListParcel;
    .restart local v1       #parcel:Landroid/nfc/TechListParcel;
    :cond_24
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@27
    move-result-object v3

    #@28
    iget-object v4, p0, Landroid/nfc/NfcAdapter;->mForegroundDispatchListener:Landroid/app/OnActivityPausedListener;

    #@2a
    invoke-virtual {v3, p1, v4}, Landroid/app/ActivityThread;->registerOnActivityPausedListener(Landroid/app/Activity;Landroid/app/OnActivityPausedListener;)V

    #@2d
    .line 1027
    sget-object v3, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2f
    invoke-interface {v3, p2, p3, v1}, Landroid/nfc/INfcAdapter;->setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;Landroid/nfc/TechListParcel;)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_32} :catch_33

    #@32
    .line 1031
    :goto_32
    return-void

    #@33
    .line 1028
    :catch_33
    move-exception v0

    #@34
    .line 1029
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@37
    goto :goto_32
.end method

.method public enableForegroundNdefPush(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V
    .registers 4
    .parameter "activity"
    .parameter "message"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1100
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 1101
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v0

    #@a
    .line 1103
    :cond_a
    invoke-virtual {p0, p1}, Landroid/nfc/NfcAdapter;->enforceResumed(Landroid/app/Activity;)V

    #@d
    .line 1104
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@f
    invoke-virtual {v0, p1, p2}, Landroid/nfc/NfcActivityManager;->setNdefPushMessage(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V

    #@12
    .line 1105
    return-void
.end method

.method public enableNdefPush()Z
    .registers 3

    #@0
    .prologue
    .line 1144
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->enableNdefPush()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1147
    :goto_6
    return v1

    #@7
    .line 1145
    :catch_7
    move-exception v0

    #@8
    .line 1146
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@b
    .line 1147
    const/4 v1, 0x0

    #@c
    goto :goto_6
.end method

.method enforceResumed(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    #@0
    .prologue
    .line 1243
    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    .line 1244
    new-instance v0, Ljava/lang/IllegalStateException;

    #@8
    const-string v1, "API cannot be called while activity is paused"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 1246
    :cond_e
    return-void
.end method

.method public getAdapterState()I
    .registers 3

    #@0
    .prologue
    .line 524
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->getState()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 527
    :goto_6
    return v1

    #@7
    .line 525
    :catch_7
    move-exception v0

    #@8
    .line 526
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@b
    .line 527
    const/4 v1, 0x1

    #@c
    goto :goto_6
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    #@0
    .prologue
    .line 440
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method public getNfcAdapterExtrasInterface()Landroid/nfc/INfcAdapterExtras;
    .registers 4

    #@0
    .prologue
    .line 1230
    iget-object v1, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 1231
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    #@6
    const-string v2, "You need a context on NfcAdapter to use the  NFC extras APIs"

    #@8
    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@b
    throw v1

    #@c
    .line 1235
    :cond_c
    :try_start_c
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@e
    iget-object v2, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    invoke-interface {v1, v2}, Landroid/nfc/INfcAdapter;->getNfcAdapterExtrasInterface(Ljava/lang/String;)Landroid/nfc/INfcAdapterExtras;
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_17} :catch_19

    #@17
    move-result-object v1

    #@18
    .line 1238
    :goto_18
    return-object v1

    #@19
    .line 1236
    :catch_19
    move-exception v0

    #@1a
    .line 1237
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@1d
    .line 1238
    const/4 v1, 0x0

    #@1e
    goto :goto_18
.end method

.method getSdkVersion()I
    .registers 2

    #@0
    .prologue
    .line 1249
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_7

    #@4
    .line 1250
    const/16 v0, 0x9

    #@6
    .line 1252
    :goto_6
    return v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    #@c
    move-result-object v0

    #@d
    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@f
    goto :goto_6
.end method

.method public getService()Landroid/nfc/INfcAdapter;
    .registers 2

    #@0
    .prologue
    .line 448
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    #@3
    .line 449
    sget-object v0, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@5
    return-object v0
.end method

.method public getTagService()Landroid/nfc/INfcTag;
    .registers 2

    #@0
    .prologue
    .line 457
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    #@3
    .line 458
    sget-object v0, Landroid/nfc/NfcAdapter;->sTagService:Landroid/nfc/INfcTag;

    #@5
    return-object v0
.end method

.method public isEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 502
    :try_start_1
    sget-object v2, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@3
    invoke-interface {v2}, Landroid/nfc/INfcAdapter;->getState()I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_c

    #@6
    move-result v2

    #@7
    const/4 v3, 0x3

    #@8
    if-ne v2, v3, :cond_b

    #@a
    const/4 v1, 0x1

    #@b
    .line 505
    :cond_b
    :goto_b
    return v1

    #@c
    .line 503
    :catch_c
    move-exception v0

    #@d
    .line 504
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@10
    goto :goto_b
.end method

.method public isNdefPushEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1190
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1}, Landroid/nfc/INfcAdapter;->isNdefPushEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 1193
    :goto_6
    return v1

    #@7
    .line 1191
    :catch_7
    move-exception v0

    #@8
    .line 1192
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@b
    .line 1193
    const/4 v1, 0x0

    #@c
    goto :goto_6
.end method

.method public setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V
    .registers 10
    .parameter "uris"
    .parameter "activity"

    #@0
    .prologue
    .line 645
    if-nez p2, :cond_a

    #@2
    .line 646
    new-instance v5, Ljava/lang/NullPointerException;

    #@4
    const-string v6, "activity cannot be null"

    #@6
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v5

    #@a
    .line 648
    :cond_a
    if-eqz p1, :cond_3e

    #@c
    .line 649
    move-object v0, p1

    #@d
    .local v0, arr$:[Landroid/net/Uri;
    array-length v2, v0

    #@e
    .local v2, len$:I
    const/4 v1, 0x0

    #@f
    .local v1, i$:I
    :goto_f
    if-ge v1, v2, :cond_3e

    #@11
    aget-object v4, v0, v1

    #@13
    .line 650
    .local v4, uri:Landroid/net/Uri;
    if-nez v4, :cond_1d

    #@15
    new-instance v5, Ljava/lang/NullPointerException;

    #@17
    const-string v6, "Uri not allowed to be null"

    #@19
    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v5

    #@1d
    .line 652
    :cond_1d
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    .line 653
    .local v3, scheme:Ljava/lang/String;
    if-eqz v3, :cond_33

    #@23
    const-string v5, "file"

    #@25
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v5

    #@29
    if-nez v5, :cond_3b

    #@2b
    const-string v5, "content"

    #@2d
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@30
    move-result v5

    #@31
    if-nez v5, :cond_3b

    #@33
    .line 655
    :cond_33
    new-instance v5, Ljava/lang/IllegalArgumentException;

    #@35
    const-string v6, "URI needs to have either scheme file or scheme content"

    #@37
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v5

    #@3b
    .line 649
    :cond_3b
    add-int/lit8 v1, v1, 0x1

    #@3d
    goto :goto_f

    #@3e
    .line 660
    .end local v0           #arr$:[Landroid/net/Uri;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v3           #scheme:Ljava/lang/String;
    .end local v4           #uri:Landroid/net/Uri;
    :cond_3e
    iget-object v5, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@40
    invoke-virtual {v5, p2, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushContentUri(Landroid/app/Activity;[Landroid/net/Uri;)V

    #@43
    .line 661
    return-void
.end method

.method public setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V
    .registers 5
    .parameter "callback"
    .parameter "activity"

    #@0
    .prologue
    .line 721
    if-nez p2, :cond_a

    #@2
    .line 722
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "activity cannot be null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 724
    :cond_a
    iget-object v0, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@c
    invoke-virtual {v0, p2, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushContentUriCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;)V

    #@f
    .line 725
    return-void
.end method

.method public varargs setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V
    .registers 12
    .parameter "message"
    .parameter "activity"
    .parameter "activities"

    #@0
    .prologue
    .line 800
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getSdkVersion()I

    #@3
    move-result v5

    #@4
    .line 802
    .local v5, targetSdkVersion:I
    if-nez p2, :cond_1b

    #@6
    .line 803
    :try_start_6
    new-instance v6, Ljava/lang/NullPointerException;

    #@8
    const-string v7, "activity cannot be null"

    #@a
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v6
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_e} :catch_e

    #@e
    .line 812
    :catch_e
    move-exception v2

    #@f
    .line 813
    .local v2, e:Ljava/lang/IllegalStateException;
    const/16 v6, 0x10

    #@11
    if-ge v5, v6, :cond_39

    #@13
    .line 815
    const-string v6, "NFC"

    #@15
    const-string v7, "Cannot call API with Activity that has already been destroyed"

    #@17
    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 822
    .end local v2           #e:Ljava/lang/IllegalStateException;
    :cond_1a
    return-void

    #@1b
    .line 805
    :cond_1b
    :try_start_1b
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@1d
    invoke-virtual {v6, p2, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessage(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V

    #@20
    .line 806
    move-object v1, p3

    #@21
    .local v1, arr$:[Landroid/app/Activity;
    array-length v4, v1

    #@22
    .local v4, len$:I
    const/4 v3, 0x0

    #@23
    .local v3, i$:I
    :goto_23
    if-ge v3, v4, :cond_1a

    #@25
    aget-object v0, v1, v3

    #@27
    .line 807
    .local v0, a:Landroid/app/Activity;
    if-nez v0, :cond_31

    #@29
    .line 808
    new-instance v6, Ljava/lang/NullPointerException;

    #@2b
    const-string v7, "activities cannot contain null"

    #@2d
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@30
    throw v6

    #@31
    .line 810
    :cond_31
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@33
    invoke-virtual {v6, v0, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessage(Landroid/app/Activity;Landroid/nfc/NdefMessage;)V
    :try_end_36
    .catch Ljava/lang/IllegalStateException; {:try_start_1b .. :try_end_36} :catch_e

    #@36
    .line 806
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_23

    #@39
    .line 819
    .end local v0           #a:Landroid/app/Activity;
    .end local v1           #arr$:[Landroid/app/Activity;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .restart local v2       #e:Ljava/lang/IllegalStateException;
    :cond_39
    throw v2
.end method

.method public varargs setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    .registers 12
    .parameter "callback"
    .parameter "activity"
    .parameter "activities"

    #@0
    .prologue
    .line 891
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getSdkVersion()I

    #@3
    move-result v5

    #@4
    .line 893
    .local v5, targetSdkVersion:I
    if-nez p2, :cond_1b

    #@6
    .line 894
    :try_start_6
    new-instance v6, Ljava/lang/NullPointerException;

    #@8
    const-string v7, "activity cannot be null"

    #@a
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v6
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_e} :catch_e

    #@e
    .line 903
    :catch_e
    move-exception v2

    #@f
    .line 904
    .local v2, e:Ljava/lang/IllegalStateException;
    const/16 v6, 0x10

    #@11
    if-ge v5, v6, :cond_39

    #@13
    .line 906
    const-string v6, "NFC"

    #@15
    const-string v7, "Cannot call API with Activity that has already been destroyed"

    #@17
    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 913
    .end local v2           #e:Ljava/lang/IllegalStateException;
    :cond_1a
    return-void

    #@1b
    .line 896
    :cond_1b
    :try_start_1b
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@1d
    invoke-virtual {v6, p2, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessageCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;)V

    #@20
    .line 897
    move-object v1, p3

    #@21
    .local v1, arr$:[Landroid/app/Activity;
    array-length v4, v1

    #@22
    .local v4, len$:I
    const/4 v3, 0x0

    #@23
    .local v3, i$:I
    :goto_23
    if-ge v3, v4, :cond_1a

    #@25
    aget-object v0, v1, v3

    #@27
    .line 898
    .local v0, a:Landroid/app/Activity;
    if-nez v0, :cond_31

    #@29
    .line 899
    new-instance v6, Ljava/lang/NullPointerException;

    #@2b
    const-string v7, "activities cannot contain null"

    #@2d
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@30
    throw v6

    #@31
    .line 901
    :cond_31
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@33
    invoke-virtual {v6, v0, p1}, Landroid/nfc/NfcActivityManager;->setNdefPushMessageCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;)V
    :try_end_36
    .catch Ljava/lang/IllegalStateException; {:try_start_1b .. :try_end_36} :catch_e

    #@36
    .line 897
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_23

    #@39
    .line 910
    .end local v0           #a:Landroid/app/Activity;
    .end local v1           #arr$:[Landroid/app/Activity;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .restart local v2       #e:Ljava/lang/IllegalStateException;
    :cond_39
    throw v2
.end method

.method public varargs setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    .registers 12
    .parameter "callback"
    .parameter "activity"
    .parameter "activities"

    #@0
    .prologue
    .line 954
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getSdkVersion()I

    #@3
    move-result v5

    #@4
    .line 956
    .local v5, targetSdkVersion:I
    if-nez p2, :cond_1b

    #@6
    .line 957
    :try_start_6
    new-instance v6, Ljava/lang/NullPointerException;

    #@8
    const-string v7, "activity cannot be null"

    #@a
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v6
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_e} :catch_e

    #@e
    .line 966
    :catch_e
    move-exception v2

    #@f
    .line 967
    .local v2, e:Ljava/lang/IllegalStateException;
    const/16 v6, 0x10

    #@11
    if-ge v5, v6, :cond_39

    #@13
    .line 969
    const-string v6, "NFC"

    #@15
    const-string v7, "Cannot call API with Activity that has already been destroyed"

    #@17
    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    .line 976
    .end local v2           #e:Ljava/lang/IllegalStateException;
    :cond_1a
    return-void

    #@1b
    .line 959
    :cond_1b
    :try_start_1b
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@1d
    invoke-virtual {v6, p2, p1}, Landroid/nfc/NfcActivityManager;->setOnNdefPushCompleteCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V

    #@20
    .line 960
    move-object v1, p3

    #@21
    .local v1, arr$:[Landroid/app/Activity;
    array-length v4, v1

    #@22
    .local v4, len$:I
    const/4 v3, 0x0

    #@23
    .local v3, i$:I
    :goto_23
    if-ge v3, v4, :cond_1a

    #@25
    aget-object v0, v1, v3

    #@27
    .line 961
    .local v0, a:Landroid/app/Activity;
    if-nez v0, :cond_31

    #@29
    .line 962
    new-instance v6, Ljava/lang/NullPointerException;

    #@2b
    const-string v7, "activities cannot contain null"

    #@2d
    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@30
    throw v6

    #@31
    .line 964
    :cond_31
    iget-object v6, p0, Landroid/nfc/NfcAdapter;->mNfcActivityManager:Landroid/nfc/NfcActivityManager;

    #@33
    invoke-virtual {v6, v0, p1}, Landroid/nfc/NfcActivityManager;->setOnNdefPushCompleteCallback(Landroid/app/Activity;Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;)V
    :try_end_36
    .catch Ljava/lang/IllegalStateException; {:try_start_1b .. :try_end_36} :catch_e

    #@36
    .line 960
    add-int/lit8 v3, v3, 0x1

    #@38
    goto :goto_23

    #@39
    .line 973
    .end local v0           #a:Landroid/app/Activity;
    .end local v1           #arr$:[Landroid/app/Activity;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .restart local v2       #e:Ljava/lang/IllegalStateException;
    :cond_39
    throw v2
.end method

.method public setP2pModes(II)V
    .registers 5
    .parameter "initiatorModes"
    .parameter "targetModes"

    #@0
    .prologue
    .line 1220
    :try_start_0
    sget-object v1, Landroid/nfc/NfcAdapter;->sService:Landroid/nfc/INfcAdapter;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/nfc/INfcAdapter;->setP2pModes(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 1224
    :goto_5
    return-void

    #@6
    .line 1221
    :catch_6
    move-exception v0

    #@7
    .line 1222
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    #@a
    goto :goto_5
.end method
