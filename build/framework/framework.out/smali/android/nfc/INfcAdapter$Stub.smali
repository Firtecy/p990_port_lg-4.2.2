.class public abstract Landroid/nfc/INfcAdapter$Stub;
.super Landroid/os/Binder;
.source "INfcAdapter.java"

# interfaces
.implements Landroid/nfc/INfcAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/nfc/INfcAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/nfc/INfcAdapter$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.nfc.INfcAdapter"

.field static final TRANSACTION_disable:I = 0x4

.field static final TRANSACTION_disableNdefPush:I = 0x7

.field static final TRANSACTION_dispatch:I = 0xb

.field static final TRANSACTION_enable:I = 0x5

.field static final TRANSACTION_enableNdefPush:I = 0x6

.field static final TRANSACTION_getNfcAdapterExtrasInterface:I = 0x2

.field static final TRANSACTION_getNfcTagInterface:I = 0x1

.field static final TRANSACTION_getState:I = 0x3

.field static final TRANSACTION_isNdefPushEnabled:I = 0x8

.field static final TRANSACTION_setForegroundDispatch:I = 0x9

.field static final TRANSACTION_setNdefPushCallback:I = 0xa

.field static final TRANSACTION_setP2pModes:I = 0xc


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.nfc.INfcAdapter"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/nfc/INfcAdapter$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/nfc/INfcAdapter;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.nfc.INfcAdapter"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/nfc/INfcAdapter;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/nfc/INfcAdapter;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/nfc/INfcAdapter$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/nfc/INfcAdapter$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v5, 0x1

    #@3
    .line 41
    sparse-switch p1, :sswitch_data_138

    #@6
    .line 174
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@9
    move-result v5

    #@a
    :goto_a
    return v5

    #@b
    .line 45
    :sswitch_b
    const-string v4, "android.nfc.INfcAdapter"

    #@d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    goto :goto_a

    #@11
    .line 50
    :sswitch_11
    const-string v6, "android.nfc.INfcAdapter"

    #@13
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16
    .line 51
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->getNfcTagInterface()Landroid/nfc/INfcTag;

    #@19
    move-result-object v3

    #@1a
    .line 52
    .local v3, _result:Landroid/nfc/INfcTag;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d
    .line 53
    if-eqz v3, :cond_23

    #@1f
    invoke-interface {v3}, Landroid/nfc/INfcTag;->asBinder()Landroid/os/IBinder;

    #@22
    move-result-object v4

    #@23
    :cond_23
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@26
    goto :goto_a

    #@27
    .line 58
    .end local v3           #_result:Landroid/nfc/INfcTag;
    :sswitch_27
    const-string v6, "android.nfc.INfcAdapter"

    #@29
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c
    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f
    move-result-object v0

    #@30
    .line 61
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/nfc/INfcAdapter$Stub;->getNfcAdapterExtrasInterface(Ljava/lang/String;)Landroid/nfc/INfcAdapterExtras;

    #@33
    move-result-object v3

    #@34
    .line 62
    .local v3, _result:Landroid/nfc/INfcAdapterExtras;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37
    .line 63
    if-eqz v3, :cond_3d

    #@39
    invoke-interface {v3}, Landroid/nfc/INfcAdapterExtras;->asBinder()Landroid/os/IBinder;

    #@3c
    move-result-object v4

    #@3d
    :cond_3d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@40
    goto :goto_a

    #@41
    .line 68
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v3           #_result:Landroid/nfc/INfcAdapterExtras;
    :sswitch_41
    const-string v4, "android.nfc.INfcAdapter"

    #@43
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 69
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->getState()I

    #@49
    move-result v3

    #@4a
    .line 70
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d
    .line 71
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@50
    goto :goto_a

    #@51
    .line 76
    .end local v3           #_result:I
    :sswitch_51
    const-string v4, "android.nfc.INfcAdapter"

    #@53
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@56
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@59
    move-result v4

    #@5a
    if-eqz v4, :cond_6b

    #@5c
    move v0, v5

    #@5d
    .line 79
    .local v0, _arg0:Z
    :goto_5d
    invoke-virtual {p0, v0}, Landroid/nfc/INfcAdapter$Stub;->disable(Z)Z

    #@60
    move-result v3

    #@61
    .line 80
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@64
    .line 81
    if-eqz v3, :cond_67

    #@66
    move v6, v5

    #@67
    :cond_67
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@6a
    goto :goto_a

    #@6b
    .end local v0           #_arg0:Z
    .end local v3           #_result:Z
    :cond_6b
    move v0, v6

    #@6c
    .line 78
    goto :goto_5d

    #@6d
    .line 86
    :sswitch_6d
    const-string v4, "android.nfc.INfcAdapter"

    #@6f
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@72
    .line 87
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->enable()Z

    #@75
    move-result v3

    #@76
    .line 88
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@79
    .line 89
    if-eqz v3, :cond_7c

    #@7b
    move v6, v5

    #@7c
    :cond_7c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_a

    #@80
    .line 94
    .end local v3           #_result:Z
    :sswitch_80
    const-string v4, "android.nfc.INfcAdapter"

    #@82
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 95
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->enableNdefPush()Z

    #@88
    move-result v3

    #@89
    .line 96
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    .line 97
    if-eqz v3, :cond_8f

    #@8e
    move v6, v5

    #@8f
    :cond_8f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@92
    goto/16 :goto_a

    #@94
    .line 102
    .end local v3           #_result:Z
    :sswitch_94
    const-string v4, "android.nfc.INfcAdapter"

    #@96
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@99
    .line 103
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->disableNdefPush()Z

    #@9c
    move-result v3

    #@9d
    .line 104
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a0
    .line 105
    if-eqz v3, :cond_a3

    #@a2
    move v6, v5

    #@a3
    :cond_a3
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@a6
    goto/16 :goto_a

    #@a8
    .line 110
    .end local v3           #_result:Z
    :sswitch_a8
    const-string v4, "android.nfc.INfcAdapter"

    #@aa
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ad
    .line 111
    invoke-virtual {p0}, Landroid/nfc/INfcAdapter$Stub;->isNdefPushEnabled()Z

    #@b0
    move-result v3

    #@b1
    .line 112
    .restart local v3       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b4
    .line 113
    if-eqz v3, :cond_b7

    #@b6
    move v6, v5

    #@b7
    :cond_b7
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@ba
    goto/16 :goto_a

    #@bc
    .line 118
    .end local v3           #_result:Z
    :sswitch_bc
    const-string v4, "android.nfc.INfcAdapter"

    #@be
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c1
    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@c4
    move-result v4

    #@c5
    if-eqz v4, :cond_ed

    #@c7
    .line 121
    sget-object v4, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c9
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@cc
    move-result-object v0

    #@cd
    check-cast v0, Landroid/app/PendingIntent;

    #@cf
    .line 127
    .local v0, _arg0:Landroid/app/PendingIntent;
    :goto_cf
    sget-object v4, Landroid/content/IntentFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    #@d1
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@d4
    move-result-object v1

    #@d5
    check-cast v1, [Landroid/content/IntentFilter;

    #@d7
    .line 129
    .local v1, _arg1:[Landroid/content/IntentFilter;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@da
    move-result v4

    #@db
    if-eqz v4, :cond_ef

    #@dd
    .line 130
    sget-object v4, Landroid/nfc/TechListParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    #@df
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e2
    move-result-object v2

    #@e3
    check-cast v2, Landroid/nfc/TechListParcel;

    #@e5
    .line 135
    .local v2, _arg2:Landroid/nfc/TechListParcel;
    :goto_e5
    invoke-virtual {p0, v0, v1, v2}, Landroid/nfc/INfcAdapter$Stub;->setForegroundDispatch(Landroid/app/PendingIntent;[Landroid/content/IntentFilter;Landroid/nfc/TechListParcel;)V

    #@e8
    .line 136
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@eb
    goto/16 :goto_a

    #@ed
    .line 124
    .end local v0           #_arg0:Landroid/app/PendingIntent;
    .end local v1           #_arg1:[Landroid/content/IntentFilter;
    .end local v2           #_arg2:Landroid/nfc/TechListParcel;
    :cond_ed
    const/4 v0, 0x0

    #@ee
    .restart local v0       #_arg0:Landroid/app/PendingIntent;
    goto :goto_cf

    #@ef
    .line 133
    .restart local v1       #_arg1:[Landroid/content/IntentFilter;
    :cond_ef
    const/4 v2, 0x0

    #@f0
    .restart local v2       #_arg2:Landroid/nfc/TechListParcel;
    goto :goto_e5

    #@f1
    .line 141
    .end local v0           #_arg0:Landroid/app/PendingIntent;
    .end local v1           #_arg1:[Landroid/content/IntentFilter;
    .end local v2           #_arg2:Landroid/nfc/TechListParcel;
    :sswitch_f1
    const-string v4, "android.nfc.INfcAdapter"

    #@f3
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f6
    .line 143
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@f9
    move-result-object v4

    #@fa
    invoke-static {v4}, Landroid/nfc/INdefPushCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/nfc/INdefPushCallback;

    #@fd
    move-result-object v0

    #@fe
    .line 144
    .local v0, _arg0:Landroid/nfc/INdefPushCallback;
    invoke-virtual {p0, v0}, Landroid/nfc/INfcAdapter$Stub;->setNdefPushCallback(Landroid/nfc/INdefPushCallback;)V

    #@101
    .line 145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@104
    goto/16 :goto_a

    #@106
    .line 150
    .end local v0           #_arg0:Landroid/nfc/INdefPushCallback;
    :sswitch_106
    const-string v4, "android.nfc.INfcAdapter"

    #@108
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10b
    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10e
    move-result v4

    #@10f
    if-eqz v4, :cond_121

    #@111
    .line 153
    sget-object v4, Landroid/nfc/Tag;->CREATOR:Landroid/os/Parcelable$Creator;

    #@113
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@116
    move-result-object v0

    #@117
    check-cast v0, Landroid/nfc/Tag;

    #@119
    .line 158
    .local v0, _arg0:Landroid/nfc/Tag;
    :goto_119
    invoke-virtual {p0, v0}, Landroid/nfc/INfcAdapter$Stub;->dispatch(Landroid/nfc/Tag;)V

    #@11c
    .line 159
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f
    goto/16 :goto_a

    #@121
    .line 156
    .end local v0           #_arg0:Landroid/nfc/Tag;
    :cond_121
    const/4 v0, 0x0

    #@122
    .restart local v0       #_arg0:Landroid/nfc/Tag;
    goto :goto_119

    #@123
    .line 164
    .end local v0           #_arg0:Landroid/nfc/Tag;
    :sswitch_123
    const-string v4, "android.nfc.INfcAdapter"

    #@125
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@128
    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12b
    move-result v0

    #@12c
    .line 168
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12f
    move-result v1

    #@130
    .line 169
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/nfc/INfcAdapter$Stub;->setP2pModes(II)V

    #@133
    .line 170
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@136
    goto/16 :goto_a

    #@138
    .line 41
    :sswitch_data_138
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_27
        0x3 -> :sswitch_41
        0x4 -> :sswitch_51
        0x5 -> :sswitch_6d
        0x6 -> :sswitch_80
        0x7 -> :sswitch_94
        0x8 -> :sswitch_a8
        0x9 -> :sswitch_bc
        0xa -> :sswitch_f1
        0xb -> :sswitch_106
        0xc -> :sswitch_123
        0x5f4e5446 -> :sswitch_b
    .end sparse-switch
.end method
