.class public Landroid/os/ResultReceiver;
.super Ljava/lang/Object;
.source "ResultReceiver.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/ResultReceiver$MyResultReceiver;,
        Landroid/os/ResultReceiver$MyRunnable;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/ResultReceiver;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mHandler:Landroid/os/Handler;

.field final mLocal:Z

.field mReceiver:Lcom/android/internal/os/IResultReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 121
    new-instance v0, Landroid/os/ResultReceiver$1;

    #@2
    invoke-direct {v0}, Landroid/os/ResultReceiver$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/ResultReceiver;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 63
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/os/ResultReceiver;->mLocal:Z

    #@6
    .line 64
    iput-object p1, p0, Landroid/os/ResultReceiver;->mHandler:Landroid/os/Handler;

    #@8
    .line 65
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 116
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/os/ResultReceiver;->mLocal:Z

    #@6
    .line 117
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/os/ResultReceiver;->mHandler:Landroid/os/Handler;

    #@9
    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    invoke-static {v0}, Lcom/android/internal/os/IResultReceiver$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/os/IResultReceiver;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@13
    .line 119
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    .line 100
    return-void
.end method

.method public send(ILandroid/os/Bundle;)V
    .registers 5
    .parameter "resultCode"
    .parameter "resultData"

    #@0
    .prologue
    .line 75
    iget-boolean v0, p0, Landroid/os/ResultReceiver;->mLocal:Z

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 76
    iget-object v0, p0, Landroid/os/ResultReceiver;->mHandler:Landroid/os/Handler;

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 77
    iget-object v0, p0, Landroid/os/ResultReceiver;->mHandler:Landroid/os/Handler;

    #@a
    new-instance v1, Landroid/os/ResultReceiver$MyRunnable;

    #@c
    invoke-direct {v1, p0, p1, p2}, Landroid/os/ResultReceiver$MyRunnable;-><init>(Landroid/os/ResultReceiver;ILandroid/os/Bundle;)V

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@12
    .line 90
    :cond_12
    :goto_12
    return-void

    #@13
    .line 79
    :cond_13
    invoke-virtual {p0, p1, p2}, Landroid/os/ResultReceiver;->onReceiveResult(ILandroid/os/Bundle;)V

    #@16
    goto :goto_12

    #@17
    .line 84
    :cond_17
    iget-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@19
    if-eqz v0, :cond_12

    #@1b
    .line 86
    :try_start_1b
    iget-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@1d
    invoke-interface {v0, p1, p2}, Lcom/android/internal/os/IResultReceiver;->send(ILandroid/os/Bundle;)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_20} :catch_21

    #@20
    goto :goto_12

    #@21
    .line 87
    :catch_21
    move-exception v0

    #@22
    goto :goto_12
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 107
    monitor-enter p0

    #@1
    .line 108
    :try_start_1
    iget-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@3
    if-nez v0, :cond_c

    #@5
    .line 109
    new-instance v0, Landroid/os/ResultReceiver$MyResultReceiver;

    #@7
    invoke-direct {v0, p0}, Landroid/os/ResultReceiver$MyResultReceiver;-><init>(Landroid/os/ResultReceiver;)V

    #@a
    iput-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@c
    .line 111
    :cond_c
    iget-object v0, p0, Landroid/os/ResultReceiver;->mReceiver:Lcom/android/internal/os/IResultReceiver;

    #@e
    invoke-interface {v0}, Lcom/android/internal/os/IResultReceiver;->asBinder()Landroid/os/IBinder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@15
    .line 112
    monitor-exit p0

    #@16
    .line 113
    return-void

    #@17
    .line 112
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method
