.class public Landroid/os/StrictMode$Span;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Span"
.end annotation


# instance fields
.field private final mContainerState:Landroid/os/StrictMode$ThreadSpanState;

.field private mCreateMillis:J

.field private mName:Ljava/lang/String;

.field private mNext:Landroid/os/StrictMode$Span;

.field private mPrev:Landroid/os/StrictMode$Span;


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 1709
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1710
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/os/StrictMode$Span;->mContainerState:Landroid/os/StrictMode$ThreadSpanState;

    #@6
    .line 1711
    return-void
.end method

.method constructor <init>(Landroid/os/StrictMode$ThreadSpanState;)V
    .registers 2
    .parameter "threadState"

    #@0
    .prologue
    .line 1704
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1705
    iput-object p1, p0, Landroid/os/StrictMode$Span;->mContainerState:Landroid/os/StrictMode$ThreadSpanState;

    #@5
    .line 1706
    return-void
.end method

.method static synthetic access$1800(Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1697
    iget-object v0, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@2
    return-object v0
.end method

.method static synthetic access$1802(Landroid/os/StrictMode$Span;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1697
    iput-object p1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@2
    return-object p1
.end method

.method static synthetic access$1900(Landroid/os/StrictMode$Span;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1697
    iget-object v0, p0, Landroid/os/StrictMode$Span;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1902(Landroid/os/StrictMode$Span;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1697
    iput-object p1, p0, Landroid/os/StrictMode$Span;->mName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$2002(Landroid/os/StrictMode$Span;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1697
    iput-wide p1, p0, Landroid/os/StrictMode$Span;->mCreateMillis:J

    #@2
    return-wide p1
.end method

.method static synthetic access$2102(Landroid/os/StrictMode$Span;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1697
    iput-object p1, p0, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@2
    return-object p1
.end method


# virtual methods
.method public finish()V
    .registers 5

    #@0
    .prologue
    .line 1724
    iget-object v0, p0, Landroid/os/StrictMode$Span;->mContainerState:Landroid/os/StrictMode$ThreadSpanState;

    #@2
    .line 1725
    .local v0, state:Landroid/os/StrictMode$ThreadSpanState;
    monitor-enter v0

    #@3
    .line 1726
    :try_start_3
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mName:Ljava/lang/String;

    #@5
    if-nez v1, :cond_9

    #@7
    .line 1728
    monitor-exit v0

    #@8
    .line 1759
    :goto_8
    return-void

    #@9
    .line 1732
    :cond_9
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@b
    if-eqz v1, :cond_13

    #@d
    .line 1733
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@f
    iget-object v2, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@11
    iput-object v2, v1, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@13
    .line 1735
    :cond_13
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@15
    if-eqz v1, :cond_1d

    #@17
    .line 1736
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@19
    iget-object v2, p0, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@1b
    iput-object v2, v1, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@1d
    .line 1738
    :cond_1d
    iget-object v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mActiveHead:Landroid/os/StrictMode$Span;

    #@1f
    if-ne v1, p0, :cond_25

    #@21
    .line 1739
    iget-object v1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@23
    iput-object v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mActiveHead:Landroid/os/StrictMode$Span;

    #@25
    .line 1742
    :cond_25
    iget v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@27
    add-int/lit8 v1, v1, -0x1

    #@29
    iput v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@2b
    .line 1744
    invoke-static {}, Landroid/os/StrictMode;->access$800()Z

    #@2e
    move-result v1

    #@2f
    if-eqz v1, :cond_57

    #@31
    const-string v1, "StrictMode"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "Span finished="

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    iget-object v3, p0, Landroid/os/StrictMode$Span;->mName:Ljava/lang/String;

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    const-string v3, "; size="

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    iget v3, v0, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1746
    :cond_57
    const-wide/16 v1, -0x1

    #@59
    iput-wide v1, p0, Landroid/os/StrictMode$Span;->mCreateMillis:J

    #@5b
    .line 1747
    const/4 v1, 0x0

    #@5c
    iput-object v1, p0, Landroid/os/StrictMode$Span;->mName:Ljava/lang/String;

    #@5e
    .line 1748
    const/4 v1, 0x0

    #@5f
    iput-object v1, p0, Landroid/os/StrictMode$Span;->mPrev:Landroid/os/StrictMode$Span;

    #@61
    .line 1749
    const/4 v1, 0x0

    #@62
    iput-object v1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@64
    .line 1753
    iget v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mFreeListSize:I

    #@66
    const/4 v2, 0x5

    #@67
    if-ge v1, v2, :cond_75

    #@69
    .line 1754
    iget-object v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mFreeListHead:Landroid/os/StrictMode$Span;

    #@6b
    iput-object v1, p0, Landroid/os/StrictMode$Span;->mNext:Landroid/os/StrictMode$Span;

    #@6d
    .line 1755
    iput-object p0, v0, Landroid/os/StrictMode$ThreadSpanState;->mFreeListHead:Landroid/os/StrictMode$Span;

    #@6f
    .line 1756
    iget v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mFreeListSize:I

    #@71
    add-int/lit8 v1, v1, 0x1

    #@73
    iput v1, v0, Landroid/os/StrictMode$ThreadSpanState;->mFreeListSize:I

    #@75
    .line 1758
    :cond_75
    monitor-exit v0

    #@76
    goto :goto_8

    #@77
    :catchall_77
    move-exception v1

    #@78
    monitor-exit v0
    :try_end_79
    .catchall {:try_start_3 .. :try_end_79} :catchall_77

    #@79
    throw v1
.end method
