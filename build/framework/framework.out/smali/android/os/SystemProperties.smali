.class public Landroid/os/SystemProperties;
.super Ljava/lang/Object;
.source "SystemProperties.java"


# static fields
.field public static final PROP_NAME_MAX:I = 0x1f

.field public static final PROP_VALUE_MAX:I = 0x5b

.field private static final sChangeCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    sput-object v0, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addChangeCallback(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 135
    sget-object v1, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 136
    :try_start_3
    sget-object v0, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_e

    #@b
    .line 137
    invoke-static {}, Landroid/os/SystemProperties;->native_add_change_callback()V

    #@e
    .line 139
    :cond_e
    sget-object v0, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@13
    .line 140
    monitor-exit v1

    #@14
    .line 141
    return-void

    #@15
    .line 140
    :catchall_15
    move-exception v0

    #@16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method static callChangeCallbacks()V
    .registers 4

    #@0
    .prologue
    .line 144
    sget-object v3, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@2
    monitor-enter v3

    #@3
    .line 146
    :try_start_3
    sget-object v2, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v2

    #@9
    if-nez v2, :cond_d

    #@b
    .line 147
    monitor-exit v3

    #@c
    .line 154
    .local v0, callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
    .local v1, i:I
    :goto_c
    return-void

    #@d
    .line 149
    .end local v0           #callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
    .end local v1           #i:I
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    #@f
    sget-object v2, Landroid/os/SystemProperties;->sChangeCallbacks:Ljava/util/ArrayList;

    #@11
    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@14
    .line 150
    .restart local v0       #callbacks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Runnable;>;"
    const/4 v1, 0x0

    #@15
    .restart local v1       #i:I
    :goto_15
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v2

    #@19
    if-ge v1, v2, :cond_27

    #@1b
    .line 151
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    check-cast v2, Ljava/lang/Runnable;

    #@21
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    #@24
    .line 150
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_15

    #@27
    .line 153
    :cond_27
    monitor-exit v3

    #@28
    goto :goto_c

    #@29
    :catchall_29
    move-exception v2

    #@2a
    monitor-exit v3
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_29

    #@2b
    throw v2
.end method

.method public static get(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 53
    :cond_11
    invoke-static {p0}, Landroid/os/SystemProperties;->native_get(Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method

.method public static get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "key"
    .parameter "def"

    #@0
    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 65
    :cond_11
    invoke-static {p0, p1}, Landroid/os/SystemProperties;->native_get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .registers 4
    .parameter "key"
    .parameter "def"

    #@0
    .prologue
    .line 112
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 115
    :cond_11
    invoke-static {p0, p1}, Landroid/os/SystemProperties;->native_get_boolean(Ljava/lang/String;Z)Z

    #@14
    move-result v0

    #@15
    return v0
.end method

.method public static getInt(Ljava/lang/String;I)I
    .registers 4
    .parameter "key"
    .parameter "def"

    #@0
    .prologue
    .line 77
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 80
    :cond_11
    invoke-static {p0, p1}, Landroid/os/SystemProperties;->native_get_int(Ljava/lang/String;I)I

    #@14
    move-result v0

    #@15
    return v0
.end method

.method public static getLong(Ljava/lang/String;J)J
    .registers 5
    .parameter "key"
    .parameter "def"

    #@0
    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 95
    :cond_11
    invoke-static {p0, p1, p2}, Landroid/os/SystemProperties;->native_get_long(Ljava/lang/String;J)J

    #@14
    move-result-wide v0

    #@15
    return-wide v0
.end method

.method private static native native_add_change_callback()V
.end method

.method private static native native_get(Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native native_get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method private static native native_get_boolean(Ljava/lang/String;Z)Z
.end method

.method private static native native_get_int(Ljava/lang/String;I)I
.end method

.method private static native native_get_long(Ljava/lang/String;J)J
.end method

.method private static native native_set(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static set(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "val"

    #@0
    .prologue
    .line 124
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x1f

    #@6
    if-le v0, v1, :cond_11

    #@8
    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "key.length > 31"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 127
    :cond_11
    if-eqz p1, :cond_24

    #@13
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@16
    move-result v0

    #@17
    const/16 v1, 0x5b

    #@19
    if-le v0, v1, :cond_24

    #@1b
    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string/jumbo v1, "val.length > 91"

    #@20
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@23
    throw v0

    #@24
    .line 131
    :cond_24
    invoke-static {p0, p1}, Landroid/os/SystemProperties;->native_set(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 132
    return-void
.end method
