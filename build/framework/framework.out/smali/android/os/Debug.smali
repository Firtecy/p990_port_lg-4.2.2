.class public final Landroid/os/Debug;
.super Ljava/lang/Object;
.source "Debug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Debug$1;,
        Landroid/os/Debug$DebugProperty;,
        Landroid/os/Debug$InstructionCount;,
        Landroid/os/Debug$MemoryInfo;
    }
.end annotation


# static fields
.field private static final DEFAULT_TRACE_BODY:Ljava/lang/String; = "dmtrace"

.field private static final DEFAULT_TRACE_EXTENSION:Ljava/lang/String; = ".trace"

.field private static final DEFAULT_TRACE_FILE_PATH:Ljava/lang/String; = null

.field private static final DEFAULT_TRACE_PATH_PREFIX:Ljava/lang/String; = null

.field private static final MIN_DEBUGGER_IDLE:I = 0x514

.field public static final SHOW_CLASSLOADER:I = 0x2

.field public static final SHOW_FULL_DETAIL:I = 0x1

.field public static final SHOW_INITIALIZED:I = 0x4

.field private static final SPIN_DELAY:I = 0xc8

.field private static final SYSFS_QEMU_TRACE_STATE:Ljava/lang/String; = "/sys/qemu_trace/state"

.field private static final TAG:Ljava/lang/String; = "Debug"

.field public static final TRACE_COUNT_ALLOCS:I = 0x1

.field private static final debugProperties:Lcom/android/internal/util/TypedProperties;

.field private static volatile mWaiting:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 79
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Landroid/os/Debug;->mWaiting:Z

    #@3
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    const-string v1, "/"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Landroid/os/Debug;->DEFAULT_TRACE_PATH_PREFIX:Ljava/lang/String;

    #@20
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    sget-object v1, Landroid/os/Debug;->DEFAULT_TRACE_PATH_PREFIX:Ljava/lang/String;

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    const-string v1, "dmtrace"

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    const-string v1, ".trace"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    sput-object v0, Landroid/os/Debug;->DEFAULT_TRACE_FILE_PATH:Ljava/lang/String;

    #@3d
    .line 1114
    const/4 v0, 0x0

    #@3e
    sput-object v0, Landroid/os/Debug;->debugProperties:Lcom/android/internal/util/TypedProperties;

    #@40
    .line 1116
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static final cacheRegisterMap(Ljava/lang/String;)Z
    .registers 2
    .parameter "classAndMethodDesc"

    #@0
    .prologue
    .line 975
    invoke-static {p0}, Ldalvik/system/VMDebug;->cacheRegisterMap(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static changeDebugPort(I)V
    .registers 1
    .parameter "port"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 325
    return-void
.end method

.method public static countInstancesOfClass(Ljava/lang/Class;)J
    .registers 3
    .parameter "cls"

    #@0
    .prologue
    .line 926
    const/4 v0, 0x1

    #@1
    invoke-static {p0, v0}, Ldalvik/system/VMDebug;->countInstancesOfClass(Ljava/lang/Class;Z)J

    #@4
    move-result-wide v0

    #@5
    return-wide v0
.end method

.method public static dumpHprofData(Ljava/lang/String;)V
    .registers 1
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 885
    invoke-static {p0}, Ldalvik/system/VMDebug;->dumpHprofData(Ljava/lang/String;)V

    #@3
    .line 886
    return-void
.end method

.method public static dumpHprofData(Ljava/lang/String;Ljava/io/FileDescriptor;)V
    .registers 2
    .parameter "fileName"
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 899
    invoke-static {p0, p1}, Ldalvik/system/VMDebug;->dumpHprofData(Ljava/lang/String;Ljava/io/FileDescriptor;)V

    #@3
    .line 900
    return-void
.end method

.method public static dumpHprofDataDdms()V
    .registers 0

    #@0
    .prologue
    .line 910
    invoke-static {}, Ldalvik/system/VMDebug;->dumpHprofDataDdms()V

    #@3
    .line 911
    return-void
.end method

.method public static native dumpNativeBacktraceToFile(ILjava/lang/String;)V
.end method

.method public static native dumpNativeHeap(Ljava/io/FileDescriptor;)V
.end method

.method public static final dumpReferenceTables()V
    .registers 0

    #@0
    .prologue
    .line 985
    invoke-static {}, Ldalvik/system/VMDebug;->dumpReferenceTables()V

    #@3
    .line 986
    return-void
.end method

.method public static dumpService(Ljava/lang/String;Ljava/io/FileDescriptor;[Ljava/lang/String;)Z
    .registers 9
    .parameter "name"
    .parameter "fd"
    .parameter "args"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1313
    invoke-static {p0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@4
    move-result-object v1

    #@5
    .line 1314
    .local v1, service:Landroid/os/IBinder;
    if-nez v1, :cond_20

    #@7
    .line 1315
    const-string v3, "Debug"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v5, "Can\'t find service to dump: "

    #@10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1324
    :goto_1f
    return v2

    #@20
    .line 1320
    :cond_20
    :try_start_20
    invoke-interface {v1, p1, p2}, Landroid/os/IBinder;->dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_23} :catch_25

    #@23
    .line 1321
    const/4 v2, 0x1

    #@24
    goto :goto_1f

    #@25
    .line 1322
    :catch_25
    move-exception v0

    #@26
    .line 1323
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "Debug"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v5, "Can\'t dump service: "

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    goto :goto_1f
.end method

.method public static enableEmulatorTraceOutput()V
    .registers 0

    #@0
    .prologue
    .line 418
    invoke-static {}, Ldalvik/system/VMDebug;->startEmulatorTracing()V

    #@3
    .line 419
    return-void
.end method

.method private static fieldTypeMatches(Ljava/lang/reflect/Field;Ljava/lang/Class;)Z
    .registers 8
    .parameter "field"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1126
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@5
    move-result-object v1

    #@6
    .line 1127
    .local v1, fieldClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    if-ne v1, p1, :cond_a

    #@8
    move v5, v4

    #@9
    .line 1143
    :goto_9
    return v5

    #@a
    .line 1136
    :cond_a
    :try_start_a
    const-string v3, "TYPE"

    #@c
    invoke-virtual {p1, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_f
    .catch Ljava/lang/NoSuchFieldException; {:try_start_a .. :try_end_f} :catch_1c

    #@f
    move-result-object v2

    #@10
    .line 1141
    .local v2, primitiveTypeField:Ljava/lang/reflect/Field;
    const/4 v3, 0x0

    #@11
    :try_start_11
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v3

    #@15
    check-cast v3, Ljava/lang/Class;
    :try_end_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_11 .. :try_end_17} :catch_20

    #@17
    if-ne v1, v3, :cond_1e

    #@19
    move v3, v4

    #@1a
    :goto_1a
    move v5, v3

    #@1b
    goto :goto_9

    #@1c
    .line 1137
    .end local v2           #primitiveTypeField:Ljava/lang/reflect/Field;
    :catch_1c
    move-exception v0

    #@1d
    .line 1138
    .local v0, ex:Ljava/lang/NoSuchFieldException;
    goto :goto_9

    #@1e
    .end local v0           #ex:Ljava/lang/NoSuchFieldException;
    .restart local v2       #primitiveTypeField:Ljava/lang/reflect/Field;
    :cond_1e
    move v3, v5

    #@1f
    .line 1141
    goto :goto_1a

    #@20
    .line 1142
    :catch_20
    move-exception v0

    #@21
    .line 1143
    .local v0, ex:Ljava/lang/IllegalAccessException;
    goto :goto_9
.end method

.method public static final native getBinderDeathObjectCount()I
.end method

.method public static final native getBinderLocalObjectCount()I
.end method

.method public static final native getBinderProxyObjectCount()I
.end method

.method public static native getBinderReceivedTransactions()I
.end method

.method public static native getBinderSentTransactions()I
.end method

.method public static getCaller()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1387
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@7
    move-result-object v0

    #@8
    const/4 v1, 0x0

    #@9
    invoke-static {v0, v1}, Landroid/os/Debug;->getCaller([Ljava/lang/StackTraceElement;I)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method private static getCaller([Ljava/lang/StackTraceElement;I)Ljava/lang/String;
    .registers 5
    .parameter "callStack"
    .parameter "depth"

    #@0
    .prologue
    .line 1343
    add-int/lit8 v1, p1, 0x4

    #@2
    array-length v2, p0

    #@3
    if-lt v1, v2, :cond_8

    #@5
    .line 1344
    const-string v1, "<bottom of call stack>"

    #@7
    .line 1347
    :goto_7
    return-object v1

    #@8
    .line 1346
    :cond_8
    add-int/lit8 v1, p1, 0x4

    #@a
    aget-object v0, p0, v1

    #@c
    .line 1347
    .local v0, caller:Ljava/lang/StackTraceElement;
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, "."

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, ":"

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    #@30
    move-result v2

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    goto :goto_7
.end method

.method public static getCallers(I)Ljava/lang/String;
    .registers 6
    .parameter "depth"

    #@0
    .prologue
    .line 1357
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@7
    move-result-object v0

    #@8
    .line 1358
    .local v0, callStack:[Ljava/lang/StackTraceElement;
    new-instance v2, Ljava/lang/StringBuffer;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    #@d
    .line 1359
    .local v2, sb:Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, p0, :cond_20

    #@10
    .line 1360
    invoke-static {v0, v1}, Landroid/os/Debug;->getCaller([Ljava/lang/StackTraceElement;I)Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@17
    move-result-object v3

    #@18
    const-string v4, " "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1d
    .line 1359
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_e

    #@20
    .line 1362
    :cond_20
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    return-object v3
.end method

.method public static getCallers(ILjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "depth"
    .parameter "linePrefix"

    #@0
    .prologue
    .line 1374
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v3

    #@4
    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@7
    move-result-object v0

    #@8
    .line 1375
    .local v0, callStack:[Ljava/lang/StackTraceElement;
    new-instance v2, Ljava/lang/StringBuffer;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    #@d
    .line 1376
    .local v2, sb:Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, p0, :cond_24

    #@10
    .line 1377
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@13
    move-result-object v3

    #@14
    invoke-static {v0, v1}, Landroid/os/Debug;->getCaller([Ljava/lang/StackTraceElement;I)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, "\n"

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    #@21
    .line 1376
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_e

    #@24
    .line 1379
    :cond_24
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    return-object v3
.end method

.method public static getGlobalAllocCount()I
    .registers 1

    #@0
    .prologue
    .line 581
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static getGlobalAllocSize()I
    .registers 1

    #@0
    .prologue
    .line 584
    const/4 v0, 0x2

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static getGlobalClassInitCount()I
    .registers 1

    #@0
    .prologue
    .line 594
    const/16 v0, 0x20

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getGlobalClassInitTime()I
    .registers 1

    #@0
    .prologue
    .line 598
    const/16 v0, 0x40

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getGlobalExternalAllocCount()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 610
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getGlobalExternalAllocSize()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 622
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getGlobalExternalFreedCount()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 635
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getGlobalExternalFreedSize()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 648
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getGlobalFreedCount()I
    .registers 1

    #@0
    .prologue
    .line 587
    const/4 v0, 0x4

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static getGlobalFreedSize()I
    .registers 1

    #@0
    .prologue
    .line 590
    const/16 v0, 0x8

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getGlobalGcInvocationCount()I
    .registers 1

    #@0
    .prologue
    .line 652
    const/16 v0, 0x10

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getLoadedClassCount()I
    .registers 1

    #@0
    .prologue
    .line 873
    invoke-static {}, Ldalvik/system/VMDebug;->getLoadedClassCount()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static native getMemoryInfo(ILandroid/os/Debug$MemoryInfo;)V
.end method

.method public static native getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V
.end method

.method public static native getNativeHeapAllocatedSize()J
.end method

.method public static native getNativeHeapFreeSize()J
.end method

.method public static native getNativeHeapSize()J
.end method

.method public static native getPss()J
.end method

.method public static native getPss(I)J
.end method

.method public static getThreadAllocCount()I
    .registers 1

    #@0
    .prologue
    .line 655
    const/high16 v0, 0x1

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getThreadAllocSize()I
    .registers 1

    #@0
    .prologue
    .line 658
    const/high16 v0, 0x2

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getThreadExternalAllocCount()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 671
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getThreadExternalAllocSize()I
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 683
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static getThreadGcInvocationCount()I
    .registers 1

    #@0
    .prologue
    .line 687
    const/high16 v0, 0x10

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->getAllocCount(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getVmFeatureList()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 316
    invoke-static {}, Ldalvik/system/VMDebug;->getVmFeatureList()[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static isDebuggerConnected()Z
    .registers 1

    #@0
    .prologue
    .line 305
    invoke-static {}, Ldalvik/system/VMDebug;->isDebuggerConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static isMethodTracingActive()Z
    .registers 1

    #@0
    .prologue
    .line 529
    invoke-static {}, Ldalvik/system/VMDebug;->isMethodTracingActive()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static modifyFieldIfSet(Ljava/lang/reflect/Field;Lcom/android/internal/util/TypedProperties;Ljava/lang/String;)V
    .registers 9
    .parameter "field"
    .parameter "properties"
    .parameter "propertyName"

    #@0
    .prologue
    .line 1154
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@3
    move-result-object v3

    #@4
    const-class v4, Ljava/lang/String;

    #@6
    if-ne v3, v4, :cond_85

    #@8
    .line 1155
    invoke-virtual {p1, p2}, Lcom/android/internal/util/TypedProperties;->getStringInfo(Ljava/lang/String;)I

    #@b
    move-result v1

    #@c
    .line 1156
    .local v1, stringInfo:I
    packed-switch v1, :pswitch_data_f6

    #@f
    .line 1175
    new-instance v3, Ljava/lang/IllegalStateException;

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "Unexpected getStringInfo("

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    const-string v5, ") return value "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@31
    throw v3

    #@32
    .line 1162
    :pswitch_32
    const/4 v3, 0x0

    #@33
    const/4 v4, 0x0

    #@34
    :try_start_34
    invoke-virtual {p0, v3, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_37
    .catch Ljava/lang/IllegalAccessException; {:try_start_34 .. :try_end_37} :catch_38

    #@37
    .line 1194
    .end local v1           #stringInfo:I
    :cond_37
    :goto_37
    :pswitch_37
    return-void

    #@38
    .line 1163
    .restart local v1       #stringInfo:I
    :catch_38
    move-exception v0

    #@39
    .line 1164
    .local v0, ex:Ljava/lang/IllegalAccessException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@3b
    new-instance v4, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v5, "Cannot set field for "

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@51
    throw v3

    #@52
    .line 1171
    .end local v0           #ex:Ljava/lang/IllegalAccessException;
    :pswitch_52
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@54
    new-instance v4, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v5, "Type of "

    #@5b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v4

    #@5f
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    const-string v5, " "

    #@65
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v4

    #@69
    const-string v5, " does not match field type ("

    #@6b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v4

    #@6f
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    const-string v5, ")"

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@84
    throw v3

    #@85
    .line 1180
    .end local v1           #stringInfo:I
    :cond_85
    :pswitch_85
    invoke-virtual {p1, p2}, Lcom/android/internal/util/TypedProperties;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@88
    move-result-object v2

    #@89
    .line 1181
    .local v2, value:Ljava/lang/Object;
    if-eqz v2, :cond_37

    #@8b
    .line 1182
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@8e
    move-result-object v3

    #@8f
    invoke-static {p0, v3}, Landroid/os/Debug;->fieldTypeMatches(Ljava/lang/reflect/Field;Ljava/lang/Class;)Z

    #@92
    move-result v3

    #@93
    if-nez v3, :cond_d6

    #@95
    .line 1183
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@97
    new-instance v4, Ljava/lang/StringBuilder;

    #@99
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9c
    const-string v5, "Type of "

    #@9e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v4

    #@a2
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v4

    #@a6
    const-string v5, " ("

    #@a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v4

    #@ac
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@af
    move-result-object v5

    #@b0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v4

    #@b4
    const-string v5, ") "

    #@b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v4

    #@ba
    const-string v5, " does not match field type ("

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    #@c3
    move-result-object v5

    #@c4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v4

    #@c8
    const-string v5, ")"

    #@ca
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v4

    #@d2
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d5
    throw v3

    #@d6
    .line 1188
    :cond_d6
    const/4 v3, 0x0

    #@d7
    :try_start_d7
    invoke-virtual {p0, v3, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_da
    .catch Ljava/lang/IllegalAccessException; {:try_start_d7 .. :try_end_da} :catch_dc

    #@da
    goto/16 :goto_37

    #@dc
    .line 1189
    :catch_dc
    move-exception v0

    #@dd
    .line 1190
    .restart local v0       #ex:Ljava/lang/IllegalAccessException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@df
    new-instance v4, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v5, "Cannot set field for "

    #@e6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v4

    #@ea
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v4

    #@ee
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    invoke-direct {v3, v4, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@f5
    throw v3

    #@f6
    .line 1156
    :pswitch_data_f6
    .packed-switch -0x2
        :pswitch_52
        :pswitch_37
        :pswitch_32
        :pswitch_85
    .end packed-switch
.end method

.method public static printLoadedClasses(I)V
    .registers 1
    .parameter "flags"

    #@0
    .prologue
    .line 865
    invoke-static {p0}, Ldalvik/system/VMDebug;->printLoadedClasses(I)V

    #@3
    .line 866
    return-void
.end method

.method public static resetAllCounts()V
    .registers 1

    #@0
    .prologue
    .line 786
    const/4 v0, -0x1

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@4
    .line 787
    return-void
.end method

.method public static resetGlobalAllocCount()V
    .registers 1

    #@0
    .prologue
    .line 691
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@4
    .line 692
    return-void
.end method

.method public static resetGlobalAllocSize()V
    .registers 1

    #@0
    .prologue
    .line 694
    const/4 v0, 0x2

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@4
    .line 695
    return-void
.end method

.method public static resetGlobalClassInitCount()V
    .registers 1

    #@0
    .prologue
    .line 703
    const/16 v0, 0x20

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 704
    return-void
.end method

.method public static resetGlobalClassInitTime()V
    .registers 1

    #@0
    .prologue
    .line 706
    const/16 v0, 0x40

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 707
    return-void
.end method

.method public static resetGlobalExternalAllocCount()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 717
    return-void
.end method

.method public static resetGlobalExternalAllocSize()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 727
    return-void
.end method

.method public static resetGlobalExternalFreedCount()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 737
    return-void
.end method

.method public static resetGlobalExternalFreedSize()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 748
    return-void
.end method

.method public static resetGlobalFreedCount()V
    .registers 1

    #@0
    .prologue
    .line 697
    const/4 v0, 0x4

    #@1
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@4
    .line 698
    return-void
.end method

.method public static resetGlobalFreedSize()V
    .registers 1

    #@0
    .prologue
    .line 700
    const/16 v0, 0x8

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 701
    return-void
.end method

.method public static resetGlobalGcInvocationCount()V
    .registers 1

    #@0
    .prologue
    .line 751
    const/16 v0, 0x10

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 752
    return-void
.end method

.method public static resetThreadAllocCount()V
    .registers 1

    #@0
    .prologue
    .line 754
    const/high16 v0, 0x1

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 755
    return-void
.end method

.method public static resetThreadAllocSize()V
    .registers 1

    #@0
    .prologue
    .line 757
    const/high16 v0, 0x2

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 758
    return-void
.end method

.method public static resetThreadExternalAllocCount()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 769
    return-void
.end method

.method public static resetThreadExternalAllocSize()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 780
    return-void
.end method

.method public static resetThreadGcInvocationCount()V
    .registers 1

    #@0
    .prologue
    .line 783
    const/high16 v0, 0x10

    #@2
    invoke-static {v0}, Ldalvik/system/VMDebug;->resetAllocCount(I)V

    #@5
    .line 784
    return-void
.end method

.method public static setAllocationLimit(I)I
    .registers 2
    .parameter "limit"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 843
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public static setFieldsOn(Ljava/lang/Class;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1205
    .local p0, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/os/Debug;->setFieldsOn(Ljava/lang/Class;Z)V

    #@4
    .line 1206
    return-void
.end method

.method public static setFieldsOn(Ljava/lang/Class;Z)V
    .registers 5
    .parameter
    .parameter "partial"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1284
    .local p0, cl:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-string v1, "Debug"

    #@2
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string/jumbo v2, "setFieldsOn("

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    if-nez p0, :cond_25

    #@10
    const-string/jumbo v0, "null"

    #@13
    :goto_13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v2, ") called in non-DEBUG build"

    #@19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1288
    return-void

    #@25
    .line 1284
    :cond_25
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_13
.end method

.method public static setGlobalAllocationLimit(I)I
    .registers 2
    .parameter "limit"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 856
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public static startAllocCounting()V
    .registers 0

    #@0
    .prologue
    .line 568
    invoke-static {}, Ldalvik/system/VMDebug;->startAllocCounting()V

    #@3
    .line 569
    return-void
.end method

.method public static startMethodTracing()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 428
    sget-object v0, Landroid/os/Debug;->DEFAULT_TRACE_FILE_PATH:Ljava/lang/String;

    #@3
    invoke-static {v0, v1, v1}, Ldalvik/system/VMDebug;->startMethodTracing(Ljava/lang/String;II)V

    #@6
    .line 429
    return-void
.end method

.method public static startMethodTracing(Ljava/lang/String;)V
    .registers 2
    .parameter "traceName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 444
    invoke-static {p0, v0, v0}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;II)V

    #@4
    .line 445
    return-void
.end method

.method public static startMethodTracing(Ljava/lang/String;I)V
    .registers 3
    .parameter "traceName"
    .parameter "bufferSize"

    #@0
    .prologue
    .line 461
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;II)V

    #@4
    .line 462
    return-void
.end method

.method public static startMethodTracing(Ljava/lang/String;II)V
    .registers 6
    .parameter "traceName"
    .parameter "bufferSize"
    .parameter "flags"

    #@0
    .prologue
    .line 490
    move-object v0, p0

    #@1
    .line 491
    .local v0, pathName:Ljava/lang/String;
    const/4 v1, 0x0

    #@2
    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v1

    #@6
    const/16 v2, 0x2f

    #@8
    if-eq v1, v2, :cond_1d

    #@a
    .line 492
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    sget-object v2, Landroid/os/Debug;->DEFAULT_TRACE_PATH_PREFIX:Ljava/lang/String;

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 493
    :cond_1d
    const-string v1, ".trace"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_38

    #@25
    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    const-string v2, ".trace"

    #@30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    .line 496
    :cond_38
    invoke-static {v0, p1, p2}, Ldalvik/system/VMDebug;->startMethodTracing(Ljava/lang/String;II)V

    #@3b
    .line 497
    return-void
.end method

.method public static startMethodTracing(Ljava/lang/String;Ljava/io/FileDescriptor;II)V
    .registers 4
    .parameter "traceName"
    .parameter "fd"
    .parameter "bufferSize"
    .parameter "flags"

    #@0
    .prologue
    .line 510
    invoke-static {p0, p1, p2, p3}, Ldalvik/system/VMDebug;->startMethodTracing(Ljava/lang/String;Ljava/io/FileDescriptor;II)V

    #@3
    .line 511
    return-void
.end method

.method public static startMethodTracingDdms(II)V
    .registers 2
    .parameter "bufferSize"
    .parameter "flags"

    #@0
    .prologue
    .line 521
    invoke-static {p0, p1}, Ldalvik/system/VMDebug;->startMethodTracingDdms(II)V

    #@3
    .line 522
    return-void
.end method

.method public static startNativeTracing()V
    .registers 4

    #@0
    .prologue
    .line 363
    const/4 v1, 0x0

    #@1
    .line 365
    .local v1, outStream:Ljava/io/PrintWriter;
    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    #@3
    const-string v3, "/sys/qemu_trace/state"

    #@5
    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@8
    .line 366
    .local v0, fos:Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/PrintWriter;

    #@a
    new-instance v3, Ljava/io/OutputStreamWriter;

    #@c
    invoke-direct {v3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@f
    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_28
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_12} :catch_21

    #@12
    .line 367
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .local v2, outStream:Ljava/io/PrintWriter;
    :try_start_12
    const-string v3, "1"

    #@14
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_12 .. :try_end_17} :catchall_2f
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_17} :catch_32

    #@17
    .line 370
    if-eqz v2, :cond_35

    #@19
    .line 371
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    #@1c
    move-object v1, v2

    #@1d
    .line 374
    .end local v0           #fos:Ljava/io/FileOutputStream;
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    :cond_1d
    :goto_1d
    invoke-static {}, Ldalvik/system/VMDebug;->startEmulatorTracing()V

    #@20
    .line 375
    return-void

    #@21
    .line 368
    :catch_21
    move-exception v3

    #@22
    .line 370
    :goto_22
    if-eqz v1, :cond_1d

    #@24
    .line 371
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@27
    goto :goto_1d

    #@28
    .line 370
    :catchall_28
    move-exception v3

    #@29
    :goto_29
    if-eqz v1, :cond_2e

    #@2b
    .line 371
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@2e
    :cond_2e
    throw v3

    #@2f
    .line 370
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v0       #fos:Ljava/io/FileOutputStream;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :catchall_2f
    move-exception v3

    #@30
    move-object v1, v2

    #@31
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_29

    #@32
    .line 368
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :catch_32
    move-exception v3

    #@33
    move-object v1, v2

    #@34
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_22

    #@35
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :cond_35
    move-object v1, v2

    #@36
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_1d
.end method

.method public static stopAllocCounting()V
    .registers 0

    #@0
    .prologue
    .line 577
    invoke-static {}, Ldalvik/system/VMDebug;->stopAllocCounting()V

    #@3
    .line 578
    return-void
.end method

.method public static stopMethodTracing()V
    .registers 0

    #@0
    .prologue
    .line 536
    invoke-static {}, Ldalvik/system/VMDebug;->stopMethodTracing()V

    #@3
    .line 537
    return-void
.end method

.method public static stopNativeTracing()V
    .registers 4

    #@0
    .prologue
    .line 388
    invoke-static {}, Ldalvik/system/VMDebug;->stopEmulatorTracing()V

    #@3
    .line 391
    const/4 v1, 0x0

    #@4
    .line 393
    .local v1, outStream:Ljava/io/PrintWriter;
    :try_start_4
    new-instance v0, Ljava/io/FileOutputStream;

    #@6
    const-string v3, "/sys/qemu_trace/state"

    #@8
    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@b
    .line 394
    .local v0, fos:Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/PrintWriter;

    #@d
    new-instance v3, Ljava/io/OutputStreamWriter;

    #@f
    invoke-direct {v3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@12
    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_15
    .catchall {:try_start_4 .. :try_end_15} :catchall_28
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_15} :catch_21

    #@15
    .line 395
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .local v2, outStream:Ljava/io/PrintWriter;
    :try_start_15
    const-string v3, "0"

    #@17
    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_15 .. :try_end_1a} :catchall_2f
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_1a} :catch_32

    #@1a
    .line 400
    if-eqz v2, :cond_35

    #@1c
    .line 401
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    #@1f
    move-object v1, v2

    #@20
    .line 403
    .end local v0           #fos:Ljava/io/FileOutputStream;
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 396
    :catch_21
    move-exception v3

    #@22
    .line 400
    :goto_22
    if-eqz v1, :cond_20

    #@24
    .line 401
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@27
    goto :goto_20

    #@28
    .line 400
    :catchall_28
    move-exception v3

    #@29
    :goto_29
    if-eqz v1, :cond_2e

    #@2b
    .line 401
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    #@2e
    :cond_2e
    throw v3

    #@2f
    .line 400
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v0       #fos:Ljava/io/FileOutputStream;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :catchall_2f
    move-exception v3

    #@30
    move-object v1, v2

    #@31
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_29

    #@32
    .line 396
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :catch_32
    move-exception v3

    #@33
    move-object v1, v2

    #@34
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_22

    #@35
    .end local v1           #outStream:Ljava/io/PrintWriter;
    .restart local v2       #outStream:Ljava/io/PrintWriter;
    :cond_35
    move-object v1, v2

    #@36
    .end local v2           #outStream:Ljava/io/PrintWriter;
    .restart local v1       #outStream:Ljava/io/PrintWriter;
    goto :goto_20
.end method

.method public static threadCpuTimeNanos()J
    .registers 2

    #@0
    .prologue
    .line 551
    invoke-static {}, Ldalvik/system/VMDebug;->threadCpuTimeNanos()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method public static waitForDebugger()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 244
    invoke-static {}, Ldalvik/system/VMDebug;->isDebuggingEnabled()Z

    #@5
    move-result v4

    #@6
    if-nez v4, :cond_9

    #@8
    .line 291
    .local v0, data:[B
    .local v1, delta:J
    .local v3, waitChunk:Lorg/apache/harmony/dalvik/ddmc/Chunk;
    :cond_8
    :goto_8
    return-void

    #@9
    .line 248
    .end local v0           #data:[B
    .end local v1           #delta:J
    .end local v3           #waitChunk:Lorg/apache/harmony/dalvik/ddmc/Chunk;
    :cond_9
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_8

    #@f
    .line 252
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@11
    const-string v5, "Sending WAIT chunk"

    #@13
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@16
    .line 253
    new-array v0, v7, [B

    #@18
    aput-byte v6, v0, v6

    #@1a
    .line 254
    .restart local v0       #data:[B
    new-instance v3, Lorg/apache/harmony/dalvik/ddmc/Chunk;

    #@1c
    const-string v4, "WAIT"

    #@1e
    invoke-static {v4}, Lorg/apache/harmony/dalvik/ddmc/ChunkHandler;->type(Ljava/lang/String;)I

    #@21
    move-result v4

    #@22
    invoke-direct {v3, v4, v0, v6, v7}, Lorg/apache/harmony/dalvik/ddmc/Chunk;-><init>(I[BII)V

    #@25
    .line 255
    .restart local v3       #waitChunk:Lorg/apache/harmony/dalvik/ddmc/Chunk;
    invoke-static {v3}, Lorg/apache/harmony/dalvik/ddmc/DdmServer;->sendChunk(Lorg/apache/harmony/dalvik/ddmc/Chunk;)V

    #@28
    .line 257
    sput-boolean v7, Landroid/os/Debug;->mWaiting:Z

    #@2a
    .line 258
    :goto_2a
    invoke-static {}, Landroid/os/Debug;->isDebuggerConnected()Z

    #@2d
    move-result v4

    #@2e
    if-nez v4, :cond_38

    #@30
    .line 259
    const-wide/16 v4, 0xc8

    #@32
    :try_start_32
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_35
    .catch Ljava/lang/InterruptedException; {:try_start_32 .. :try_end_35} :catch_36

    #@35
    goto :goto_2a

    #@36
    .line 260
    :catch_36
    move-exception v4

    #@37
    goto :goto_2a

    #@38
    .line 262
    :cond_38
    sput-boolean v6, Landroid/os/Debug;->mWaiting:Z

    #@3a
    .line 264
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@3c
    const-string v5, "Debugger has connected"

    #@3e
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@41
    .line 276
    :goto_41
    invoke-static {}, Ldalvik/system/VMDebug;->lastDebuggerActivity()J

    #@44
    move-result-wide v1

    #@45
    .line 277
    .restart local v1       #delta:J
    const-wide/16 v4, 0x0

    #@47
    cmp-long v4, v1, v4

    #@49
    if-gez v4, :cond_53

    #@4b
    .line 278
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@4d
    const-string v5, "debugger detached?"

    #@4f
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@52
    goto :goto_8

    #@53
    .line 282
    :cond_53
    const-wide/16 v4, 0x514

    #@55
    cmp-long v4, v1, v4

    #@57
    if-gez v4, :cond_69

    #@59
    .line 283
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@5b
    const-string/jumbo v5, "waiting for debugger to settle..."

    #@5e
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@61
    .line 284
    const-wide/16 v4, 0xc8

    #@63
    :try_start_63
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_66
    .catch Ljava/lang/InterruptedException; {:try_start_63 .. :try_end_66} :catch_67

    #@66
    goto :goto_41

    #@67
    .line 285
    :catch_67
    move-exception v4

    #@68
    goto :goto_41

    #@69
    .line 287
    :cond_69
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@6b
    new-instance v5, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v6, "debugger has settled ("

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    const-string v6, ")"

    #@7c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v5

    #@80
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@87
    goto :goto_8
.end method

.method public static waitingForDebugger()Z
    .registers 1

    #@0
    .prologue
    .line 298
    sget-boolean v0, Landroid/os/Debug;->mWaiting:Z

    #@2
    return v0
.end method
