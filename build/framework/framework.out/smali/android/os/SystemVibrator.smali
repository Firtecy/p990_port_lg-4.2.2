.class public Landroid/os/SystemVibrator;
.super Landroid/os/Vibrator;
.source "SystemVibrator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Vibrator"


# instance fields
.field private final mService:Landroid/os/IVibratorService;

.field private final mToken:Landroid/os/Binder;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/os/Vibrator;-><init>()V

    #@3
    .line 30
    new-instance v0, Landroid/os/Binder;

    #@5
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/SystemVibrator;->mToken:Landroid/os/Binder;

    #@a
    .line 33
    const-string/jumbo v0, "vibrator"

    #@d
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@10
    move-result-object v0

    #@11
    invoke-static {v0}, Landroid/os/IVibratorService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IVibratorService;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@17
    .line 35
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 4

    #@0
    .prologue
    .line 85
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 93
    :goto_4
    return-void

    #@5
    .line 89
    :cond_5
    :try_start_5
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@7
    iget-object v2, p0, Landroid/os/SystemVibrator;->mToken:Landroid/os/Binder;

    #@9
    invoke-interface {v1, v2}, Landroid/os/IVibratorService;->cancelVibrate(Landroid/os/IBinder;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_c} :catch_d

    #@c
    goto :goto_4

    #@d
    .line 90
    :catch_d
    move-exception v0

    #@e
    .line 91
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "Vibrator"

    #@10
    const-string v2, "Failed to cancel vibration."

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    goto :goto_4
.end method

.method public hasVibrator()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 39
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@3
    if-nez v1, :cond_d

    #@5
    .line 40
    const-string v1, "Vibrator"

    #@7
    const-string v2, "Failed to vibrate; no vibrator service."

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 47
    :goto_c
    return v0

    #@d
    .line 44
    :cond_d
    :try_start_d
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@f
    invoke-interface {v1}, Landroid/os/IVibratorService;->hasVibrator()Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_14

    #@12
    move-result v0

    #@13
    goto :goto_c

    #@14
    .line 45
    :catch_14
    move-exception v1

    #@15
    goto :goto_c
.end method

.method public vibrate(J)V
    .registers 6
    .parameter "milliseconds"

    #@0
    .prologue
    .line 52
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 53
    const-string v1, "Vibrator"

    #@6
    const-string v2, "Failed to vibrate; no vibrator service."

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 61
    :goto_b
    return-void

    #@c
    .line 57
    :cond_c
    :try_start_c
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@e
    iget-object v2, p0, Landroid/os/SystemVibrator;->mToken:Landroid/os/Binder;

    #@10
    invoke-interface {v1, p1, p2, v2}, Landroid/os/IVibratorService;->vibrate(JLandroid/os/IBinder;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_13} :catch_14

    #@13
    goto :goto_b

    #@14
    .line 58
    :catch_14
    move-exception v0

    #@15
    .line 59
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "Vibrator"

    #@17
    const-string v2, "Failed to vibrate."

    #@19
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    goto :goto_b
.end method

.method public vibrate([JI)V
    .registers 6
    .parameter "pattern"
    .parameter "repeat"

    #@0
    .prologue
    .line 65
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 66
    const-string v1, "Vibrator"

    #@6
    const-string v2, "Failed to vibrate; no vibrator service."

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 81
    :goto_b
    return-void

    #@c
    .line 72
    :cond_c
    array-length v1, p1

    #@d
    if-ge p2, v1, :cond_20

    #@f
    .line 74
    :try_start_f
    iget-object v1, p0, Landroid/os/SystemVibrator;->mService:Landroid/os/IVibratorService;

    #@11
    iget-object v2, p0, Landroid/os/SystemVibrator;->mToken:Landroid/os/Binder;

    #@13
    invoke-interface {v1, p1, p2, v2}, Landroid/os/IVibratorService;->vibratePattern([JILandroid/os/IBinder;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_16} :catch_17

    #@16
    goto :goto_b

    #@17
    .line 75
    :catch_17
    move-exception v0

    #@18
    .line 76
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "Vibrator"

    #@1a
    const-string v2, "Failed to vibrate."

    #@1c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1f
    goto :goto_b

    #@20
    .line 79
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_20
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@22
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@25
    throw v1
.end method
