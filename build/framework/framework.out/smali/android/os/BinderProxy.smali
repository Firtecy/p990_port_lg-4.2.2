.class final Landroid/os/BinderProxy;
.super Ljava/lang/Object;
.source "Binder.java"

# interfaces
.implements Landroid/os/IBinder;


# instance fields
.field private mObject:I

.field private mOrgue:I

.field private final mSelf:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 418
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 419
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/os/BinderProxy;->mSelf:Ljava/lang/ref/WeakReference;

    #@a
    .line 420
    return-void
.end method

.method private final native destroy()V
.end method

.method private static final sendDeathNotice(Landroid/os/IBinder$DeathRecipient;)V
    .registers 4
    .parameter "recipient"

    #@0
    .prologue
    .line 444
    :try_start_0
    invoke-interface {p0}, Landroid/os/IBinder$DeathRecipient;->binderDied()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_4

    #@3
    .line 450
    :goto_3
    return-void

    #@4
    .line 446
    :catch_4
    move-exception v0

    #@5
    .line 447
    .local v0, exc:Ljava/lang/RuntimeException;
    const-string v1, "BinderNative"

    #@7
    const-string v2, "Uncaught exception from death notification"

    #@9
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c
    goto :goto_3
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 391
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 392
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 393
    .local v1, reply:Landroid/os/Parcel;
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeFileDescriptor(Ljava/io/FileDescriptor;)V

    #@b
    .line 394
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@e
    .line 396
    const v2, 0x5f444d50

    #@11
    const/4 v3, 0x0

    #@12
    :try_start_12
    invoke-virtual {p0, v2, v0, v1, v3}, Landroid/os/BinderProxy;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 397
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_1f

    #@18
    .line 399
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 400
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 402
    return-void

    #@1f
    .line 399
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 400
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public dumpAsync(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "args"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 405
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 406
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 407
    .local v1, reply:Landroid/os/Parcel;
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeFileDescriptor(Ljava/io/FileDescriptor;)V

    #@b
    .line 408
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@e
    .line 410
    const v2, 0x5f444d50

    #@11
    const/4 v3, 0x1

    #@12
    :try_start_12
    invoke-virtual {p0, v2, v0, v1, v3}, Landroid/os/BinderProxy;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 411
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_1f

    #@18
    .line 413
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 416
    return-void

    #@1f
    .line 413
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 414
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 425
    :try_start_0
    invoke-direct {p0}, Landroid/os/BinderProxy;->destroy()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_7

    #@3
    .line 430
    :try_start_3
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_6
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_6} :catch_13

    #@6
    .line 437
    :goto_6
    return-void

    #@7
    .line 426
    :catch_7
    move-exception v0

    #@8
    .line 430
    :try_start_8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_b
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_8 .. :try_end_b} :catch_c

    #@b
    goto :goto_6

    #@c
    .line 432
    :catch_c
    move-exception v0

    #@d
    goto :goto_6

    #@e
    .line 429
    :catchall_e
    move-exception v0

    #@f
    .line 430
    :try_start_f
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_12
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_f .. :try_end_12} :catch_15

    #@12
    .line 434
    :goto_12
    throw v0

    #@13
    .line 432
    :catch_13
    move-exception v0

    #@14
    goto :goto_6

    #@15
    :catch_15
    move-exception v1

    #@16
    goto :goto_12
.end method

.method public native getInterfaceDescriptor()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public native isBinderAlive()Z
.end method

.method public native linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public native pingBinder()Z
.end method

.method public queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;
    .registers 3
    .parameter "descriptor"

    #@0
    .prologue
    .line 380
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public native transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public native unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
.end method
