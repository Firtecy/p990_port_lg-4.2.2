.class final Landroid/os/StrictMode$4;
.super Ljava/lang/Thread;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/os/StrictMode;->dropboxViolationAsync(ILandroid/os/StrictMode$ViolationInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$info:Landroid/os/StrictMode$ViolationInfo;

.field final synthetic val$violationMaskSubset:I


# direct methods
.method constructor <init>(Ljava/lang/String;ILandroid/os/StrictMode$ViolationInfo;)V
    .registers 4
    .parameter "x0"
    .parameter
    .parameter

    #@0
    .prologue
    .line 1370
    iput p2, p0, Landroid/os/StrictMode$4;->val$violationMaskSubset:I

    #@2
    iput-object p3, p0, Landroid/os/StrictMode$4;->val$info:Landroid/os/StrictMode$ViolationInfo;

    #@4
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 1372
    const/16 v3, 0xa

    #@2
    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    #@5
    .line 1374
    :try_start_5
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@8
    move-result-object v0

    #@9
    .line 1375
    .local v0, am:Landroid/app/IActivityManager;
    if-nez v0, :cond_39

    #@b
    .line 1376
    const-string v3, "StrictMode"

    #@d
    const-string v4, "No activity manager; failed to Dropbox violation."

    #@f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_12} :catch_45

    #@12
    .line 1386
    .end local v0           #am:Landroid/app/IActivityManager;
    :goto_12
    invoke-static {}, Landroid/os/StrictMode;->access$1400()Ljava/util/concurrent/atomic/AtomicInteger;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    #@19
    move-result v2

    #@1a
    .line 1387
    .local v2, outstanding:I
    invoke-static {}, Landroid/os/StrictMode;->access$800()Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_38

    #@20
    const-string v3, "StrictMode"

    #@22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "Dropbox complete; in-flight="

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1388
    :cond_38
    return-void

    #@39
    .line 1378
    .end local v2           #outstanding:I
    .restart local v0       #am:Landroid/app/IActivityManager;
    :cond_39
    :try_start_39
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->getApplicationObject()Landroid/os/IBinder;

    #@3c
    move-result-object v3

    #@3d
    iget v4, p0, Landroid/os/StrictMode$4;->val$violationMaskSubset:I

    #@3f
    iget-object v5, p0, Landroid/os/StrictMode$4;->val$info:Landroid/os/StrictMode$ViolationInfo;

    #@41
    invoke-interface {v0, v3, v4, v5}, Landroid/app/IActivityManager;->handleApplicationStrictModeViolation(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V
    :try_end_44
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_44} :catch_45

    #@44
    goto :goto_12

    #@45
    .line 1383
    .end local v0           #am:Landroid/app/IActivityManager;
    :catch_45
    move-exception v1

    #@46
    .line 1384
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "StrictMode"

    #@48
    const-string v4, "RemoteException handling StrictMode violation"

    #@4a
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_12
.end method
