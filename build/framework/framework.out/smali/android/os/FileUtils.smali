.class public Landroid/os/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field private static final SAFE_FILENAME_PATTERN:Ljava/util/regex/Pattern; = null

.field public static final S_IRGRP:I = 0x20

.field public static final S_IROTH:I = 0x4

.field public static final S_IRUSR:I = 0x100

.field public static final S_IRWXG:I = 0x38

.field public static final S_IRWXO:I = 0x7

.field public static final S_IRWXU:I = 0x1c0

.field public static final S_IWGRP:I = 0x10

.field public static final S_IWOTH:I = 0x2

.field public static final S_IWUSR:I = 0x80

.field public static final S_IXGRP:I = 0x8

.field public static final S_IXOTH:I = 0x1

.field public static final S_IXUSR:I = 0x40


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    const-string v0, "[\\w%+,./=_-]+"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/os/FileUtils;->SAFE_FILENAME_PATTERN:Ljava/util/regex/Pattern;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static checksumCrc32(Ljava/io/File;)J
    .registers 8
    .parameter "file"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    new-instance v1, Ljava/util/zip/CRC32;

    #@2
    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    #@5
    .line 222
    .local v1, checkSummer:Ljava/util/zip/CRC32;
    const/4 v2, 0x0

    #@6
    .line 225
    .local v2, cis:Ljava/util/zip/CheckedInputStream;
    :try_start_6
    new-instance v3, Ljava/util/zip/CheckedInputStream;

    #@8
    new-instance v4, Ljava/io/FileInputStream;

    #@a
    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@d
    invoke-direct {v3, v4, v1}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_24

    #@10
    .line 226
    .end local v2           #cis:Ljava/util/zip/CheckedInputStream;
    .local v3, cis:Ljava/util/zip/CheckedInputStream;
    const/16 v4, 0x80

    #@12
    :try_start_12
    new-array v0, v4, [B

    #@14
    .line 227
    .local v0, buf:[B
    :cond_14
    invoke-virtual {v3, v0}, Ljava/util/zip/CheckedInputStream;->read([B)I

    #@17
    move-result v4

    #@18
    if-gez v4, :cond_14

    #@1a
    .line 230
    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J
    :try_end_1d
    .catchall {:try_start_12 .. :try_end_1d} :catchall_2f

    #@1d
    move-result-wide v4

    #@1e
    .line 232
    if-eqz v3, :cond_23

    #@20
    .line 234
    :try_start_20
    invoke-virtual {v3}, Ljava/util/zip/CheckedInputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_23} :catch_2b

    #@23
    .line 236
    :cond_23
    :goto_23
    return-wide v4

    #@24
    .line 232
    .end local v0           #buf:[B
    .end local v3           #cis:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #cis:Ljava/util/zip/CheckedInputStream;
    :catchall_24
    move-exception v4

    #@25
    :goto_25
    if-eqz v2, :cond_2a

    #@27
    .line 234
    :try_start_27
    invoke-virtual {v2}, Ljava/util/zip/CheckedInputStream;->close()V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_2a} :catch_2d

    #@2a
    .line 236
    :cond_2a
    :goto_2a
    throw v4

    #@2b
    .line 235
    .end local v2           #cis:Ljava/util/zip/CheckedInputStream;
    .restart local v0       #buf:[B
    .restart local v3       #cis:Ljava/util/zip/CheckedInputStream;
    :catch_2b
    move-exception v6

    #@2c
    goto :goto_23

    #@2d
    .end local v0           #buf:[B
    .end local v3           #cis:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #cis:Ljava/util/zip/CheckedInputStream;
    :catch_2d
    move-exception v5

    #@2e
    goto :goto_2a

    #@2f
    .line 232
    .end local v2           #cis:Ljava/util/zip/CheckedInputStream;
    .restart local v3       #cis:Ljava/util/zip/CheckedInputStream;
    :catchall_2f
    move-exception v4

    #@30
    move-object v2, v3

    #@31
    .end local v3           #cis:Ljava/util/zip/CheckedInputStream;
    .restart local v2       #cis:Ljava/util/zip/CheckedInputStream;
    goto :goto_25
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .registers 6
    .parameter "srcFile"
    .parameter "destFile"

    #@0
    .prologue
    .line 82
    const/4 v2, 0x0

    #@1
    .line 84
    .local v2, result:Z
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    #@3
    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_13

    #@6
    .line 86
    .local v1, in:Ljava/io/InputStream;
    :try_start_6
    invoke-static {v1, p1}, Landroid/os/FileUtils;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_e

    #@9
    move-result v2

    #@a
    .line 88
    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    #@d
    .line 93
    .end local v1           #in:Ljava/io/InputStream;
    :goto_d
    return v2

    #@e
    .line 88
    .restart local v1       #in:Ljava/io/InputStream;
    :catchall_e
    move-exception v3

    #@f
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    #@12
    throw v3
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_13} :catch_13

    #@13
    .line 90
    .end local v1           #in:Ljava/io/InputStream;
    :catch_13
    move-exception v0

    #@14
    .line 91
    .local v0, e:Ljava/io/IOException;
    const/4 v2, 0x0

    #@15
    goto :goto_d
.end method

.method public static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .registers 9
    .parameter "inputStream"
    .parameter "destFile"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 102
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    #@4
    move-result v5

    #@5
    if-eqz v5, :cond_a

    #@7
    .line 103
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    #@a
    .line 105
    :cond_a
    new-instance v3, Ljava/io/FileOutputStream;

    #@c
    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_f} :catch_2d

    #@f
    .line 107
    .local v3, out:Ljava/io/FileOutputStream;
    const/16 v5, 0x1000

    #@11
    :try_start_11
    new-array v0, v5, [B

    #@13
    .line 109
    .local v0, buffer:[B
    :goto_13
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    #@16
    move-result v1

    #@17
    .local v1, bytesRead:I
    if-ltz v1, :cond_2f

    #@19
    .line 110
    const/4 v5, 0x0

    #@1a
    invoke-virtual {v3, v0, v5, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_1e

    #@1d
    goto :goto_13

    #@1e
    .line 113
    .end local v0           #buffer:[B
    .end local v1           #bytesRead:I
    :catchall_1e
    move-exception v5

    #@1f
    :try_start_1f
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_22} :catch_2d

    #@22
    .line 115
    :try_start_22
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_29} :catch_3e

    #@29
    .line 118
    :goto_29
    :try_start_29
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    #@2c
    throw v5

    #@2d
    .line 121
    .end local v3           #out:Ljava/io/FileOutputStream;
    :catch_2d
    move-exception v2

    #@2e
    .line 122
    :goto_2e
    return v4

    #@2f
    .line 113
    .restart local v0       #buffer:[B
    .restart local v1       #bytesRead:I
    .restart local v3       #out:Ljava/io/FileOutputStream;
    :cond_2f
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_32} :catch_2d

    #@32
    .line 115
    :try_start_32
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_39} :catch_40

    #@39
    .line 118
    :goto_39
    :try_start_39
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_3c} :catch_2d

    #@3c
    .line 120
    const/4 v4, 0x1

    #@3d
    goto :goto_2e

    #@3e
    .line 116
    .end local v0           #buffer:[B
    .end local v1           #bytesRead:I
    :catch_3e
    move-exception v6

    #@3f
    goto :goto_29

    #@40
    .restart local v0       #buffer:[B
    .restart local v1       #bytesRead:I
    :catch_40
    move-exception v5

    #@41
    goto :goto_39
.end method

.method public static native getFatVolumeId(Ljava/lang/String;)I
.end method

.method public static native getVolumeUUID(Ljava/lang/String;)I
.end method

.method public static isFilenameSafe(Ljava/io/File;)Z
    .registers 3
    .parameter "file"

    #@0
    .prologue
    .line 134
    sget-object v0, Landroid/os/FileUtils;->SAFE_FILENAME_PATTERN:Ljava/util/regex/Pattern;

    #@2
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public static readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    .registers 18
    .parameter "file"
    .parameter "max"
    .parameter "ellipsis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    new-instance v4, Ljava/io/FileInputStream;

    #@2
    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    #@5
    .line 150
    .local v4, input:Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedInputStream;

    #@7
    invoke-direct {v1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    #@a
    .line 152
    .local v1, bis:Ljava/io/BufferedInputStream;
    :try_start_a
    invoke-virtual {p0}, Ljava/io/File;->length()J

    #@d
    move-result-wide v9

    #@e
    .line 153
    .local v9, size:J
    if-gtz p1, :cond_18

    #@10
    const-wide/16 v12, 0x0

    #@12
    cmp-long v12, v9, v12

    #@14
    if-lez v12, :cond_81

    #@16
    if-nez p1, :cond_81

    #@18
    .line 154
    :cond_18
    const-wide/16 v12, 0x0

    #@1a
    cmp-long v12, v9, v12

    #@1c
    if-lez v12, :cond_2a

    #@1e
    if-eqz p1, :cond_27

    #@20
    move/from16 v0, p1

    #@22
    int-to-long v12, v0

    #@23
    cmp-long v12, v9, v12

    #@25
    if-gez v12, :cond_2a

    #@27
    :cond_27
    long-to-int v0, v9

    #@28
    move/from16 p1, v0

    #@2a
    .line 155
    :cond_2a
    add-int/lit8 v12, p1, 0x1

    #@2c
    new-array v3, v12, [B

    #@2e
    .line 156
    .local v3, data:[B
    invoke-virtual {v1, v3}, Ljava/io/BufferedInputStream;->read([B)I

    #@31
    move-result v7

    #@32
    .line 157
    .local v7, length:I
    if-gtz v7, :cond_3d

    #@34
    const-string v12, ""
    :try_end_36
    .catchall {:try_start_a .. :try_end_36} :catchall_118

    #@36
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@39
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@3c
    .end local v7           #length:I
    :goto_3c
    return-object v12

    #@3d
    .line 158
    .restart local v7       #length:I
    :cond_3d
    move/from16 v0, p1

    #@3f
    if-gt v7, v0, :cond_4e

    #@41
    :try_start_41
    new-instance v12, Ljava/lang/String;

    #@43
    const/4 v13, 0x0

    #@44
    invoke-direct {v12, v3, v13, v7}, Ljava/lang/String;-><init>([BII)V
    :try_end_47
    .catchall {:try_start_41 .. :try_end_47} :catchall_118

    #@47
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@4a
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@4d
    goto :goto_3c

    #@4e
    .line 159
    :cond_4e
    if-nez p2, :cond_5f

    #@50
    :try_start_50
    new-instance v12, Ljava/lang/String;

    #@52
    const/4 v13, 0x0

    #@53
    move/from16 v0, p1

    #@55
    invoke-direct {v12, v3, v13, v0}, Ljava/lang/String;-><init>([BII)V
    :try_end_58
    .catchall {:try_start_50 .. :try_end_58} :catchall_118

    #@58
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@5b
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@5e
    goto :goto_3c

    #@5f
    .line 160
    :cond_5f
    :try_start_5f
    new-instance v12, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    new-instance v13, Ljava/lang/String;

    #@66
    const/4 v14, 0x0

    #@67
    move/from16 v0, p1

    #@69
    invoke-direct {v13, v3, v14, v0}, Ljava/lang/String;-><init>([BII)V

    #@6c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v12

    #@70
    move-object/from16 v0, p2

    #@72
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v12

    #@76
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_79
    .catchall {:try_start_5f .. :try_end_79} :catchall_118

    #@79
    move-result-object v12

    #@7a
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@7d
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@80
    goto :goto_3c

    #@81
    .line 161
    .end local v3           #data:[B
    .end local v7           #length:I
    :cond_81
    if-gez p1, :cond_f6

    #@83
    .line 163
    const/4 v8, 0x0

    #@84
    .line 164
    .local v8, rolled:Z
    const/4 v5, 0x0

    #@85
    .local v5, last:[B
    const/4 v3, 0x0

    #@86
    .line 166
    .restart local v3       #data:[B
    :cond_86
    if-eqz v5, :cond_89

    #@88
    const/4 v8, 0x1

    #@89
    .line 167
    :cond_89
    move-object v11, v5

    #@8a
    .local v11, tmp:[B
    move-object v5, v3

    #@8b
    move-object v3, v11

    #@8c
    .line 168
    if-nez v3, :cond_93

    #@8e
    move/from16 v0, p1

    #@90
    neg-int v12, v0

    #@91
    :try_start_91
    new-array v3, v12, [B

    #@93
    .line 169
    :cond_93
    invoke-virtual {v1, v3}, Ljava/io/BufferedInputStream;->read([B)I

    #@96
    move-result v6

    #@97
    .line 170
    .local v6, len:I
    array-length v12, v3

    #@98
    if-eq v6, v12, :cond_86

    #@9a
    .line 172
    if-nez v5, :cond_a7

    #@9c
    if-gtz v6, :cond_a7

    #@9e
    const-string v12, ""
    :try_end_a0
    .catchall {:try_start_91 .. :try_end_a0} :catchall_118

    #@a0
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@a3
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@a6
    goto :goto_3c

    #@a7
    .line 173
    :cond_a7
    if-nez v5, :cond_b6

    #@a9
    :try_start_a9
    new-instance v12, Ljava/lang/String;

    #@ab
    const/4 v13, 0x0

    #@ac
    invoke-direct {v12, v3, v13, v6}, Ljava/lang/String;-><init>([BII)V
    :try_end_af
    .catchall {:try_start_a9 .. :try_end_af} :catchall_118

    #@af
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@b2
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@b5
    goto :goto_3c

    #@b6
    .line 174
    :cond_b6
    if-lez v6, :cond_c5

    #@b8
    .line 175
    const/4 v8, 0x1

    #@b9
    .line 176
    const/4 v12, 0x0

    #@ba
    :try_start_ba
    array-length v13, v5

    #@bb
    sub-int/2addr v13, v6

    #@bc
    invoke-static {v5, v6, v5, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@bf
    .line 177
    const/4 v12, 0x0

    #@c0
    array-length v13, v5

    #@c1
    sub-int/2addr v13, v6

    #@c2
    invoke-static {v3, v12, v5, v13, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c5
    .line 179
    :cond_c5
    if-eqz p2, :cond_c9

    #@c7
    if-nez v8, :cond_d6

    #@c9
    :cond_c9
    new-instance v12, Ljava/lang/String;

    #@cb
    invoke-direct {v12, v5}, Ljava/lang/String;-><init>([B)V
    :try_end_ce
    .catchall {:try_start_ba .. :try_end_ce} :catchall_118

    #@ce
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@d1
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@d4
    goto/16 :goto_3c

    #@d6
    .line 180
    :cond_d6
    :try_start_d6
    new-instance v12, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    move-object/from16 v0, p2

    #@dd
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v12

    #@e1
    new-instance v13, Ljava/lang/String;

    #@e3
    invoke-direct {v13, v5}, Ljava/lang/String;-><init>([B)V

    #@e6
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v12

    #@ea
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_ed
    .catchall {:try_start_d6 .. :try_end_ed} :catchall_118

    #@ed
    move-result-object v12

    #@ee
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@f1
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@f4
    goto/16 :goto_3c

    #@f6
    .line 182
    .end local v3           #data:[B
    .end local v5           #last:[B
    .end local v6           #len:I
    .end local v8           #rolled:Z
    .end local v11           #tmp:[B
    :cond_f6
    :try_start_f6
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    #@f8
    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@fb
    .line 184
    .local v2, contents:Ljava/io/ByteArrayOutputStream;
    const/16 v12, 0x400

    #@fd
    new-array v3, v12, [B

    #@ff
    .line 186
    .restart local v3       #data:[B
    :cond_ff
    invoke-virtual {v1, v3}, Ljava/io/BufferedInputStream;->read([B)I

    #@102
    move-result v6

    #@103
    .line 187
    .restart local v6       #len:I
    if-lez v6, :cond_109

    #@105
    const/4 v12, 0x0

    #@106
    invoke-virtual {v2, v3, v12, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    #@109
    .line 188
    :cond_109
    array-length v12, v3

    #@10a
    if-eq v6, v12, :cond_ff

    #@10c
    .line 189
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_10f
    .catchall {:try_start_f6 .. :try_end_10f} :catchall_118

    #@10f
    move-result-object v12

    #@110
    .line 192
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@113
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@116
    goto/16 :goto_3c

    #@118
    .line 192
    .end local v2           #contents:Ljava/io/ByteArrayOutputStream;
    .end local v3           #data:[B
    .end local v6           #len:I
    .end local v9           #size:J
    :catchall_118
    move-exception v12

    #@119
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    #@11c
    .line 193
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    #@11f
    throw v12
.end method

.method public static native setPermissions(Ljava/lang/String;III)I
.end method

.method public static stringToFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "filename"
    .parameter "string"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    new-instance v0, Ljava/io/FileWriter;

    #@2
    invoke-direct {v0, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    #@5
    .line 207
    .local v0, out:Ljava/io/FileWriter;
    :try_start_5
    invoke-virtual {v0, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_c

    #@8
    .line 209
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    #@b
    .line 211
    return-void

    #@c
    .line 209
    :catchall_c
    move-exception v1

    #@d
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    #@10
    throw v1
.end method

.method public static sync(Ljava/io/FileOutputStream;)Z
    .registers 2
    .parameter "stream"

    #@0
    .prologue
    .line 70
    if-eqz p0, :cond_9

    #@2
    .line 71
    :try_start_2
    invoke-virtual {p0}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_9} :catch_b

    #@9
    .line 73
    :cond_9
    const/4 v0, 0x1

    #@a
    .line 76
    :goto_a
    return v0

    #@b
    .line 74
    :catch_b
    move-exception v0

    #@c
    .line 76
    const/4 v0, 0x0

    #@d
    goto :goto_a
.end method
