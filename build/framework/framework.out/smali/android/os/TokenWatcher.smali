.class public abstract Landroid/os/TokenWatcher;
.super Ljava/lang/Object;
.source "TokenWatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/TokenWatcher$Death;
    }
.end annotation


# instance fields
.field private volatile mAcquired:Z

.field private mHandler:Landroid/os/Handler;

.field private mNotificationQueue:I

.field private mNotificationTask:Ljava/lang/Runnable;

.field private mTag:Ljava/lang/String;

.field private mTokens:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/os/TokenWatcher$Death;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;)V
    .registers 4
    .parameter "h"
    .parameter "tag"

    #@0
    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 147
    new-instance v0, Landroid/os/TokenWatcher$1;

    #@5
    invoke-direct {v0, p0}, Landroid/os/TokenWatcher$1;-><init>(Landroid/os/TokenWatcher;)V

    #@8
    iput-object v0, p0, Landroid/os/TokenWatcher;->mNotificationTask:Ljava/lang/Runnable;

    #@a
    .line 210
    new-instance v0, Ljava/util/WeakHashMap;

    #@c
    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@11
    .line 213
    const/4 v0, -0x1

    #@12
    iput v0, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@14
    .line 214
    const/4 v0, 0x0

    #@15
    iput-boolean v0, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@17
    .line 44
    iput-object p1, p0, Landroid/os/TokenWatcher;->mHandler:Landroid/os/Handler;

    #@19
    .line 45
    if-eqz p2, :cond_1e

    #@1b
    .end local p2
    :goto_1b
    iput-object p2, p0, Landroid/os/TokenWatcher;->mTag:Ljava/lang/String;

    #@1d
    .line 46
    return-void

    #@1e
    .line 45
    .restart local p2
    :cond_1e
    const-string p2, "TokenWatcher"

    #@20
    goto :goto_1b
.end method

.method static synthetic access$000(Landroid/os/TokenWatcher;)Ljava/util/WeakHashMap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/os/TokenWatcher;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget v0, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/os/TokenWatcher;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 31
    iput p1, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/os/TokenWatcher;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/os/TokenWatcher;->mTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private dumpInternal()Ljava/util/ArrayList;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 134
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 135
    .local v0, a:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@7
    monitor-enter v6

    #@8
    .line 136
    :try_start_8
    iget-object v5, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@a
    invoke-virtual {v5}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    #@d
    move-result-object v4

    #@e
    .line 137
    .local v4, keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/os/IBinder;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v7, "Token count: "

    #@15
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    iget-object v7, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@1b
    invoke-virtual {v7}, Ljava/util/WeakHashMap;->size()I

    #@1e
    move-result v7

    #@1f
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a
    .line 138
    const/4 v2, 0x0

    #@2b
    .line 139
    .local v2, i:I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2e
    move-result-object v3

    #@2f
    .local v3, i$:Ljava/util/Iterator;
    :goto_2f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@32
    move-result v5

    #@33
    if-eqz v5, :cond_72

    #@35
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    check-cast v1, Landroid/os/IBinder;

    #@3b
    .line 140
    .local v1, b:Landroid/os/IBinder;
    new-instance v5, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v7, "["

    #@42
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    const-string v7, "] "

    #@4c
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    iget-object v5, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@52
    invoke-virtual {v5, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    move-result-object v5

    #@56
    check-cast v5, Landroid/os/TokenWatcher$Death;

    #@58
    iget-object v5, v5, Landroid/os/TokenWatcher$Death;->tag:Ljava/lang/String;

    #@5a
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v5

    #@5e
    const-string v7, " - "

    #@60
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v5

    #@64
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@6f
    .line 141
    add-int/lit8 v2, v2, 0x1

    #@71
    goto :goto_2f

    #@72
    .line 143
    .end local v1           #b:Landroid/os/IBinder;
    :cond_72
    monitor-exit v6

    #@73
    .line 144
    return-object v0

    #@74
    .line 143
    .end local v2           #i:I
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #keys:Ljava/util/Set;,"Ljava/util/Set<Landroid/os/IBinder;>;"
    :catchall_74
    move-exception v5

    #@75
    monitor-exit v6
    :try_end_76
    .catchall {:try_start_8 .. :try_end_76} :catchall_74

    #@76
    throw v5
.end method

.method private sendNotificationLocked(Z)V
    .registers 5
    .parameter "on"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 166
    if-eqz p1, :cond_12

    #@3
    const/4 v0, 0x1

    #@4
    .line 167
    .local v0, value:I
    :goto_4
    iget v1, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@6
    if-ne v1, v2, :cond_14

    #@8
    .line 169
    iput v0, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@a
    .line 170
    iget-object v1, p0, Landroid/os/TokenWatcher;->mHandler:Landroid/os/Handler;

    #@c
    iget-object v2, p0, Landroid/os/TokenWatcher;->mNotificationTask:Ljava/lang/Runnable;

    #@e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@11
    .line 178
    :cond_11
    :goto_11
    return-void

    #@12
    .line 166
    .end local v0           #value:I
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_4

    #@14
    .line 172
    .restart local v0       #value:I
    :cond_14
    iget v1, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@16
    if-eq v1, v0, :cond_11

    #@18
    .line 174
    iput v2, p0, Landroid/os/TokenWatcher;->mNotificationQueue:I

    #@1a
    .line 175
    iget-object v1, p0, Landroid/os/TokenWatcher;->mHandler:Landroid/os/Handler;

    #@1c
    iget-object v2, p0, Landroid/os/TokenWatcher;->mNotificationTask:Ljava/lang/Runnable;

    #@1e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@21
    goto :goto_11
.end method


# virtual methods
.method public acquire(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 8
    .parameter "token"
    .parameter "tag"

    #@0
    .prologue
    .line 70
    iget-object v4, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@2
    monitor-enter v4

    #@3
    .line 73
    :try_start_3
    iget-object v3, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@5
    invoke-virtual {v3}, Ljava/util/WeakHashMap;->size()I

    #@8
    move-result v2

    #@9
    .line 75
    .local v2, oldSize:I
    new-instance v0, Landroid/os/TokenWatcher$Death;

    #@b
    invoke-direct {v0, p0, p1, p2}, Landroid/os/TokenWatcher$Death;-><init>(Landroid/os/TokenWatcher;Landroid/os/IBinder;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_29

    #@e
    .line 77
    .local v0, d:Landroid/os/TokenWatcher$Death;
    const/4 v3, 0x0

    #@f
    :try_start_f
    invoke-interface {p1, v0, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_12} :catch_26

    #@12
    .line 81
    :try_start_12
    iget-object v3, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@14
    invoke-virtual {v3, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 83
    if-nez v2, :cond_24

    #@19
    iget-boolean v3, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@1b
    if-nez v3, :cond_24

    #@1d
    .line 84
    const/4 v3, 0x1

    #@1e
    invoke-direct {p0, v3}, Landroid/os/TokenWatcher;->sendNotificationLocked(Z)V

    #@21
    .line 85
    const/4 v3, 0x1

    #@22
    iput-boolean v3, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@24
    .line 87
    :cond_24
    monitor-exit v4

    #@25
    .line 88
    :goto_25
    return-void

    #@26
    .line 78
    :catch_26
    move-exception v1

    #@27
    .line 79
    .local v1, e:Landroid/os/RemoteException;
    monitor-exit v4

    #@28
    goto :goto_25

    #@29
    .line 87
    .end local v0           #d:Landroid/os/TokenWatcher$Death;
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v2           #oldSize:I
    :catchall_29
    move-exception v3

    #@2a
    monitor-exit v4
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_29

    #@2b
    throw v3
.end method

.method public abstract acquired()V
.end method

.method public cleanup(Landroid/os/IBinder;Z)V
    .registers 7
    .parameter "token"
    .parameter "unlink"

    #@0
    .prologue
    .line 92
    iget-object v2, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@2
    monitor-enter v2

    #@3
    .line 93
    :try_start_3
    iget-object v1, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@5
    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/os/TokenWatcher$Death;

    #@b
    .line 94
    .local v0, d:Landroid/os/TokenWatcher$Death;
    if-eqz p2, :cond_18

    #@d
    if-eqz v0, :cond_18

    #@f
    .line 95
    iget-object v1, v0, Landroid/os/TokenWatcher$Death;->token:Landroid/os/IBinder;

    #@11
    const/4 v3, 0x0

    #@12
    invoke-interface {v1, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@15
    .line 96
    const/4 v1, 0x0

    #@16
    iput-object v1, v0, Landroid/os/TokenWatcher$Death;->token:Landroid/os/IBinder;

    #@18
    .line 99
    :cond_18
    iget-object v1, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@1a
    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_2b

    #@20
    iget-boolean v1, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@22
    if-eqz v1, :cond_2b

    #@24
    .line 100
    const/4 v1, 0x0

    #@25
    invoke-direct {p0, v1}, Landroid/os/TokenWatcher;->sendNotificationLocked(Z)V

    #@28
    .line 101
    const/4 v1, 0x0

    #@29
    iput-boolean v1, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@2b
    .line 103
    :cond_2b
    monitor-exit v2

    #@2c
    .line 104
    return-void

    #@2d
    .line 103
    .end local v0           #d:Landroid/os/TokenWatcher$Death;
    :catchall_2d
    move-exception v1

    #@2e
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_3 .. :try_end_2f} :catchall_2d

    #@2f
    throw v1
.end method

.method public dump()V
    .registers 5

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/os/TokenWatcher;->dumpInternal()Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    .line 121
    .local v0, a:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_1a

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/String;

    #@14
    .line 122
    .local v2, s:Ljava/lang/String;
    iget-object v3, p0, Landroid/os/TokenWatcher;->mTag:Ljava/lang/String;

    #@16
    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_8

    #@1a
    .line 124
    .end local v2           #s:Ljava/lang/String;
    :cond_1a
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .registers 6
    .parameter "pw"

    #@0
    .prologue
    .line 127
    invoke-direct {p0}, Landroid/os/TokenWatcher;->dumpInternal()Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    .line 128
    .local v0, a:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v1

    #@8
    .local v1, i$:Ljava/util/Iterator;
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_18

    #@e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v2

    #@12
    check-cast v2, Ljava/lang/String;

    #@14
    .line 129
    .local v2, s:Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@17
    goto :goto_8

    #@18
    .line 131
    .end local v2           #s:Ljava/lang/String;
    :cond_18
    return-void
.end method

.method public isAcquired()Z
    .registers 3

    #@0
    .prologue
    .line 113
    iget-object v1, p0, Landroid/os/TokenWatcher;->mTokens:Ljava/util/WeakHashMap;

    #@2
    monitor-enter v1

    #@3
    .line 114
    :try_start_3
    iget-boolean v0, p0, Landroid/os/TokenWatcher;->mAcquired:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 115
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public release(Landroid/os/IBinder;)V
    .registers 3
    .parameter "token"

    #@0
    .prologue
    .line 108
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/os/TokenWatcher;->cleanup(Landroid/os/IBinder;Z)V

    #@4
    .line 109
    return-void
.end method

.method public abstract released()V
.end method
