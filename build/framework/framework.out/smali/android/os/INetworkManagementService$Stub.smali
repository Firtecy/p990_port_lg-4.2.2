.class public abstract Landroid/os/INetworkManagementService$Stub;
.super Landroid/os/Binder;
.source "INetworkManagementService.java"

# interfaces
.implements Landroid/os/INetworkManagementService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/INetworkManagementService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/INetworkManagementService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.INetworkManagementService"

.field static final TRANSACTION_acceptPacket:I = 0x54

.field static final TRANSACTION_addIdleTimer:I = 0x3d

.field static final TRANSACTION_addRoute:I = 0xf

.field static final TRANSACTION_addRouteWithMetric:I = 0x4c

.field static final TRANSACTION_addRule:I = 0x20

.field static final TRANSACTION_addSecondaryRoute:I = 0x11

.field static final TRANSACTION_addUpstreamV6Interface:I = 0x24

.field static final TRANSACTION_attachPppd:I = 0x27

.field static final TRANSACTION_blockIPv6Interface:I = 0x59

.field static final TRANSACTION_clearInterfaceAddresses:I = 0x8

.field static final TRANSACTION_clearMdmIpRule:I = 0x5f

.field static final TRANSACTION_delRule:I = 0x21

.field static final TRANSACTION_delSrcRoute:I = 0x4e

.field static final TRANSACTION_detachPppd:I = 0x28

.field static final TRANSACTION_disableIpv6:I = 0xc

.field static final TRANSACTION_disableNat:I = 0x23

.field static final TRANSACTION_dropPacket:I = 0x53

.field static final TRANSACTION_enableIpv6:I = 0xd

.field static final TRANSACTION_enableNat:I = 0x22

.field static final TRANSACTION_exitMdmIpRule:I = 0x61

.field static final TRANSACTION_flushDefaultDnsCache:I = 0x41

.field static final TRANSACTION_flushInterfaceDnsCache:I = 0x42

.field static final TRANSACTION_getAssociatedIpHostnameWithMac:I = 0x6a

.field static final TRANSACTION_getAssociatedIpWithMac:I = 0x6d

.field static final TRANSACTION_getDnsForwarders:I = 0x1f

.field static final TRANSACTION_getInterfaceConfig:I = 0x6

.field static final TRANSACTION_getInterfaceRxCounter:I = 0x5b

.field static final TRANSACTION_getInterfaceRxThrottle:I = 0x3b

.field static final TRANSACTION_getInterfaceTxCounter:I = 0x5c

.field static final TRANSACTION_getInterfaceTxThrottle:I = 0x3c

.field static final TRANSACTION_getIpForwardingEnabled:I = 0x14

.field static final TRANSACTION_getNetworkStatsDetail:I = 0x2f

.field static final TRANSACTION_getNetworkStatsSummaryDev:I = 0x2d

.field static final TRANSACTION_getNetworkStatsSummaryXt:I = 0x2e

.field static final TRANSACTION_getNetworkStatsTethering:I = 0x32

.field static final TRANSACTION_getNetworkStatsUidDetail:I = 0x30

.field static final TRANSACTION_getNetworkStatsUidInterface:I = 0x31

.field static final TRANSACTION_getRouteList_debug:I = 0x57

.field static final TRANSACTION_getRoutes:I = 0xe

.field static final TRANSACTION_getSapAutoChannelSelection:I = 0x51

.field static final TRANSACTION_getSapOperatingChannel:I = 0x50

.field static final TRANSACTION_initMdmIpRule:I = 0x60

.field static final TRANSACTION_isBandwidthControlEnabled:I = 0x39

.field static final TRANSACTION_isFirewallEnabled:I = 0x46

.field static final TRANSACTION_isTetheringStarted:I = 0x18

.field static final TRANSACTION_listInterfaces:I = 0x5

.field static final TRANSACTION_listTetheredInterfaces:I = 0x1d

.field static final TRANSACTION_listTtys:I = 0x26

.field static final TRANSACTION_packetList_Indrop:I = 0x56

.field static final TRANSACTION_packetList_Indrop_view:I = 0x58

.field static final TRANSACTION_registerObserver:I = 0x1

.field static final TRANSACTION_registerObserverEx:I = 0x3

.field static final TRANSACTION_removeIdleTimer:I = 0x3e

.field static final TRANSACTION_removeInterfaceAlert:I = 0x36

.field static final TRANSACTION_removeInterfaceQuota:I = 0x34

.field static final TRANSACTION_removeRoute:I = 0x10

.field static final TRANSACTION_removeSecondaryRoute:I = 0x12

.field static final TRANSACTION_removeUpstreamV6Interface:I = 0x25

.field static final TRANSACTION_replaceSrcRoute:I = 0x4d

.field static final TRANSACTION_resetPacketDrop:I = 0x55

.field static final TRANSACTION_runClearNatRule:I = 0x64

.field static final TRANSACTION_runClearPortFilterRule:I = 0x66

.field static final TRANSACTION_runClearPortForwardRule:I = 0x68

.field static final TRANSACTION_runDataCommand:I = 0x52

.field static final TRANSACTION_runSetNatForwardRule:I = 0x65

.field static final TRANSACTION_runSetPortFilterRule:I = 0x67

.field static final TRANSACTION_runSetPortForwardRule:I = 0x69

.field static final TRANSACTION_runSetPortRedirectRule:I = 0x44

.field static final TRANSACTION_runShellCommand:I = 0x43

.field static final TRANSACTION_setAccessPoint:I = 0x2c

.field static final TRANSACTION_setChannelRange:I = 0x4f

.field static final TRANSACTION_setDefaultInterfaceForDns:I = 0x3f

.field static final TRANSACTION_setDnsForwarders:I = 0x1e

.field static final TRANSACTION_setDnsServersForInterface:I = 0x40

.field static final TRANSACTION_setFirewallEgressDestRule:I = 0x49

.field static final TRANSACTION_setFirewallEgressSourceRule:I = 0x48

.field static final TRANSACTION_setFirewallEnabled:I = 0x45

.field static final TRANSACTION_setFirewallInterfaceRule:I = 0x47

.field static final TRANSACTION_setFirewallUidRule:I = 0x4a

.field static final TRANSACTION_setGlobalAlert:I = 0x37

.field static final TRANSACTION_setInterfaceAlert:I = 0x35

.field static final TRANSACTION_setInterfaceConfig:I = 0x7

.field static final TRANSACTION_setInterfaceDown:I = 0x9

.field static final TRANSACTION_setInterfaceIpv6PrivacyExtensions:I = 0xb

.field static final TRANSACTION_setInterfaceQuota:I = 0x33

.field static final TRANSACTION_setInterfaceThrottle:I = 0x3a

.field static final TRANSACTION_setInterfaceUp:I = 0xa

.field static final TRANSACTION_setIpForwardingEnabled:I = 0x15

.field static final TRANSACTION_setIpv6AcceptRaDefrtr:I = 0x6e

.field static final TRANSACTION_setMdmIpRule:I = 0x5d

.field static final TRANSACTION_setMdmIpRuleFile:I = 0x5e

.field static final TRANSACTION_setMdmIptables:I = 0x62

.field static final TRANSACTION_setMdmIptablesFile:I = 0x63

.field static final TRANSACTION_setUidNetworkRules:I = 0x38

.field static final TRANSACTION_setVoiceProtectionEnabled:I = 0x4b

.field static final TRANSACTION_shutdown:I = 0x13

.field static final TRANSACTION_startAccessPoint:I = 0x2a

.field static final TRANSACTION_startReverseTethering:I = 0x19

.field static final TRANSACTION_startTethering:I = 0x16

.field static final TRANSACTION_startVZWAccessPoint:I = 0x6b

.field static final TRANSACTION_stopAccessPoint:I = 0x2b

.field static final TRANSACTION_stopReverseTethering:I = 0x1a

.field static final TRANSACTION_stopTethering:I = 0x17

.field static final TRANSACTION_stopVZWAccessPoint:I = 0x6c

.field static final TRANSACTION_tetherInterface:I = 0x1b

.field static final TRANSACTION_unblockIPv6Interface:I = 0x5a

.field static final TRANSACTION_unregisterObserver:I = 0x2

.field static final TRANSACTION_unregisterObserverEx:I = 0x4

.field static final TRANSACTION_untetherInterface:I = 0x1c

.field static final TRANSACTION_wifiFirmwareReload:I = 0x29


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 19
    const-string v0, "android.os.INetworkManagementService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/INetworkManagementService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 27
    if-nez p0, :cond_4

    #@2
    .line 28
    const/4 v0, 0x0

    #@3
    .line 34
    :goto_3
    return-object v0

    #@4
    .line 30
    :cond_4
    const-string v1, "android.os.INetworkManagementService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 31
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/INetworkManagementService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 32
    check-cast v0, Landroid/os/INetworkManagementService;

    #@12
    goto :goto_3

    #@13
    .line 34
    :cond_13
    new-instance v0, Landroid/os/INetworkManagementService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/INetworkManagementService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 35
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_c02

    #@3
    .line 1226
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v3

    #@7
    :goto_7
    return v3

    #@8
    .line 46
    :sswitch_8
    const-string v3, "android.os.INetworkManagementService"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 47
    const/4 v3, 0x1

    #@10
    goto :goto_7

    #@11
    .line 51
    :sswitch_11
    const-string v3, "android.os.INetworkManagementService"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Landroid/net/INetworkManagementEventObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkManagementEventObserver;

    #@1f
    move-result-object v4

    #@20
    .line 54
    .local v4, _arg0:Landroid/net/INetworkManagementEventObserver;
    move-object/from16 v0, p0

    #@22
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->registerObserver(Landroid/net/INetworkManagementEventObserver;)V

    #@25
    .line 55
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@28
    .line 56
    const/4 v3, 0x1

    #@29
    goto :goto_7

    #@2a
    .line 60
    .end local v4           #_arg0:Landroid/net/INetworkManagementEventObserver;
    :sswitch_2a
    const-string v3, "android.os.INetworkManagementService"

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 62
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@34
    move-result-object v3

    #@35
    invoke-static {v3}, Landroid/net/INetworkManagementEventObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkManagementEventObserver;

    #@38
    move-result-object v4

    #@39
    .line 63
    .restart local v4       #_arg0:Landroid/net/INetworkManagementEventObserver;
    move-object/from16 v0, p0

    #@3b
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->unregisterObserver(Landroid/net/INetworkManagementEventObserver;)V

    #@3e
    .line 64
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@41
    .line 65
    const/4 v3, 0x1

    #@42
    goto :goto_7

    #@43
    .line 69
    .end local v4           #_arg0:Landroid/net/INetworkManagementEventObserver;
    :sswitch_43
    const-string v3, "android.os.INetworkManagementService"

    #@45
    move-object/from16 v0, p2

    #@47
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a
    .line 71
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v3}, Landroid/net/INetworkManagementEventObserverEx$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkManagementEventObserverEx;

    #@51
    move-result-object v4

    #@52
    .line 72
    .local v4, _arg0:Landroid/net/INetworkManagementEventObserverEx;
    move-object/from16 v0, p0

    #@54
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->registerObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V

    #@57
    .line 73
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    .line 74
    const/4 v3, 0x1

    #@5b
    goto :goto_7

    #@5c
    .line 78
    .end local v4           #_arg0:Landroid/net/INetworkManagementEventObserverEx;
    :sswitch_5c
    const-string v3, "android.os.INetworkManagementService"

    #@5e
    move-object/from16 v0, p2

    #@60
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@63
    .line 80
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@66
    move-result-object v3

    #@67
    invoke-static {v3}, Landroid/net/INetworkManagementEventObserverEx$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkManagementEventObserverEx;

    #@6a
    move-result-object v4

    #@6b
    .line 81
    .restart local v4       #_arg0:Landroid/net/INetworkManagementEventObserverEx;
    move-object/from16 v0, p0

    #@6d
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->unregisterObserverEx(Landroid/net/INetworkManagementEventObserverEx;)V

    #@70
    .line 82
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@73
    .line 83
    const/4 v3, 0x1

    #@74
    goto :goto_7

    #@75
    .line 87
    .end local v4           #_arg0:Landroid/net/INetworkManagementEventObserverEx;
    :sswitch_75
    const-string v3, "android.os.INetworkManagementService"

    #@77
    move-object/from16 v0, p2

    #@79
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7c
    .line 88
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->listInterfaces()[Ljava/lang/String;

    #@7f
    move-result-object v28

    #@80
    .line 89
    .local v28, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@83
    .line 90
    move-object/from16 v0, p3

    #@85
    move-object/from16 v1, v28

    #@87
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@8a
    .line 91
    const/4 v3, 0x1

    #@8b
    goto/16 :goto_7

    #@8d
    .line 95
    .end local v28           #_result:[Ljava/lang/String;
    :sswitch_8d
    const-string v3, "android.os.INetworkManagementService"

    #@8f
    move-object/from16 v0, p2

    #@91
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94
    .line 97
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@97
    move-result-object v4

    #@98
    .line 98
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9a
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@9d
    move-result-object v28

    #@9e
    .line 99
    .local v28, _result:Landroid/net/InterfaceConfiguration;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1
    .line 100
    if-eqz v28, :cond_b4

    #@a3
    .line 101
    const/4 v3, 0x1

    #@a4
    move-object/from16 v0, p3

    #@a6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@a9
    .line 102
    const/4 v3, 0x1

    #@aa
    move-object/from16 v0, v28

    #@ac
    move-object/from16 v1, p3

    #@ae
    invoke-virtual {v0, v1, v3}, Landroid/net/InterfaceConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    #@b1
    .line 107
    :goto_b1
    const/4 v3, 0x1

    #@b2
    goto/16 :goto_7

    #@b4
    .line 105
    :cond_b4
    const/4 v3, 0x0

    #@b5
    move-object/from16 v0, p3

    #@b7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ba
    goto :goto_b1

    #@bb
    .line 111
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:Landroid/net/InterfaceConfiguration;
    :sswitch_bb
    const-string v3, "android.os.INetworkManagementService"

    #@bd
    move-object/from16 v0, p2

    #@bf
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c2
    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c5
    move-result-object v4

    #@c6
    .line 115
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c9
    move-result v3

    #@ca
    if-eqz v3, :cond_e1

    #@cc
    .line 116
    sget-object v3, Landroid/net/InterfaceConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@ce
    move-object/from16 v0, p2

    #@d0
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@d3
    move-result-object v5

    #@d4
    check-cast v5, Landroid/net/InterfaceConfiguration;

    #@d6
    .line 121
    .local v5, _arg1:Landroid/net/InterfaceConfiguration;
    :goto_d6
    move-object/from16 v0, p0

    #@d8
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@db
    .line 122
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@de
    .line 123
    const/4 v3, 0x1

    #@df
    goto/16 :goto_7

    #@e1
    .line 119
    .end local v5           #_arg1:Landroid/net/InterfaceConfiguration;
    :cond_e1
    const/4 v5, 0x0

    #@e2
    .restart local v5       #_arg1:Landroid/net/InterfaceConfiguration;
    goto :goto_d6

    #@e3
    .line 127
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/net/InterfaceConfiguration;
    :sswitch_e3
    const-string v3, "android.os.INetworkManagementService"

    #@e5
    move-object/from16 v0, p2

    #@e7
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 129
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ed
    move-result-object v4

    #@ee
    .line 130
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@f0
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->clearInterfaceAddresses(Ljava/lang/String;)V

    #@f3
    .line 131
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f6
    .line 132
    const/4 v3, 0x1

    #@f7
    goto/16 :goto_7

    #@f9
    .line 136
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_f9
    const-string v3, "android.os.INetworkManagementService"

    #@fb
    move-object/from16 v0, p2

    #@fd
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@100
    .line 138
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@103
    move-result-object v4

    #@104
    .line 139
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@106
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setInterfaceDown(Ljava/lang/String;)V

    #@109
    .line 140
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@10c
    .line 141
    const/4 v3, 0x1

    #@10d
    goto/16 :goto_7

    #@10f
    .line 145
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_10f
    const-string v3, "android.os.INetworkManagementService"

    #@111
    move-object/from16 v0, p2

    #@113
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@116
    .line 147
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@119
    move-result-object v4

    #@11a
    .line 148
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@11c
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setInterfaceUp(Ljava/lang/String;)V

    #@11f
    .line 149
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    .line 150
    const/4 v3, 0x1

    #@123
    goto/16 :goto_7

    #@125
    .line 154
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_125
    const-string v3, "android.os.INetworkManagementService"

    #@127
    move-object/from16 v0, p2

    #@129
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12c
    .line 156
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12f
    move-result-object v4

    #@130
    .line 158
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@133
    move-result v3

    #@134
    if-eqz v3, :cond_142

    #@136
    const/4 v5, 0x1

    #@137
    .line 159
    .local v5, _arg1:Z
    :goto_137
    move-object/from16 v0, p0

    #@139
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setInterfaceIpv6PrivacyExtensions(Ljava/lang/String;Z)V

    #@13c
    .line 160
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@13f
    .line 161
    const/4 v3, 0x1

    #@140
    goto/16 :goto_7

    #@142
    .line 158
    .end local v5           #_arg1:Z
    :cond_142
    const/4 v5, 0x0

    #@143
    goto :goto_137

    #@144
    .line 165
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_144
    const-string v3, "android.os.INetworkManagementService"

    #@146
    move-object/from16 v0, p2

    #@148
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14b
    .line 167
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14e
    move-result-object v4

    #@14f
    .line 168
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@151
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->disableIpv6(Ljava/lang/String;)V

    #@154
    .line 169
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@157
    .line 170
    const/4 v3, 0x1

    #@158
    goto/16 :goto_7

    #@15a
    .line 174
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_15a
    const-string v3, "android.os.INetworkManagementService"

    #@15c
    move-object/from16 v0, p2

    #@15e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@161
    .line 176
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@164
    move-result-object v4

    #@165
    .line 177
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@167
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->enableIpv6(Ljava/lang/String;)V

    #@16a
    .line 178
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@16d
    .line 179
    const/4 v3, 0x1

    #@16e
    goto/16 :goto_7

    #@170
    .line 183
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_170
    const-string v3, "android.os.INetworkManagementService"

    #@172
    move-object/from16 v0, p2

    #@174
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@177
    .line 185
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@17a
    move-result-object v4

    #@17b
    .line 186
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@17d
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getRoutes(Ljava/lang/String;)[Landroid/net/RouteInfo;

    #@180
    move-result-object v28

    #@181
    .line 187
    .local v28, _result:[Landroid/net/RouteInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@184
    .line 188
    const/4 v3, 0x1

    #@185
    move-object/from16 v0, p3

    #@187
    move-object/from16 v1, v28

    #@189
    invoke-virtual {v0, v1, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@18c
    .line 189
    const/4 v3, 0x1

    #@18d
    goto/16 :goto_7

    #@18f
    .line 193
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:[Landroid/net/RouteInfo;
    :sswitch_18f
    const-string v3, "android.os.INetworkManagementService"

    #@191
    move-object/from16 v0, p2

    #@193
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@196
    .line 195
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@199
    move-result-object v4

    #@19a
    .line 197
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19d
    move-result v3

    #@19e
    if-eqz v3, :cond_1b5

    #@1a0
    .line 198
    sget-object v3, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a2
    move-object/from16 v0, p2

    #@1a4
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a7
    move-result-object v5

    #@1a8
    check-cast v5, Landroid/net/RouteInfo;

    #@1aa
    .line 203
    .local v5, _arg1:Landroid/net/RouteInfo;
    :goto_1aa
    move-object/from16 v0, p0

    #@1ac
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->addRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V

    #@1af
    .line 204
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b2
    .line 205
    const/4 v3, 0x1

    #@1b3
    goto/16 :goto_7

    #@1b5
    .line 201
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :cond_1b5
    const/4 v5, 0x0

    #@1b6
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    goto :goto_1aa

    #@1b7
    .line 209
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :sswitch_1b7
    const-string v3, "android.os.INetworkManagementService"

    #@1b9
    move-object/from16 v0, p2

    #@1bb
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1be
    .line 211
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c1
    move-result-object v4

    #@1c2
    .line 213
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c5
    move-result v3

    #@1c6
    if-eqz v3, :cond_1dd

    #@1c8
    .line 214
    sget-object v3, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1ca
    move-object/from16 v0, p2

    #@1cc
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1cf
    move-result-object v5

    #@1d0
    check-cast v5, Landroid/net/RouteInfo;

    #@1d2
    .line 219
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    :goto_1d2
    move-object/from16 v0, p0

    #@1d4
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->removeRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V

    #@1d7
    .line 220
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1da
    .line 221
    const/4 v3, 0x1

    #@1db
    goto/16 :goto_7

    #@1dd
    .line 217
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :cond_1dd
    const/4 v5, 0x0

    #@1de
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    goto :goto_1d2

    #@1df
    .line 225
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :sswitch_1df
    const-string v3, "android.os.INetworkManagementService"

    #@1e1
    move-object/from16 v0, p2

    #@1e3
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e6
    .line 227
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e9
    move-result-object v4

    #@1ea
    .line 229
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1ed
    move-result v3

    #@1ee
    if-eqz v3, :cond_205

    #@1f0
    .line 230
    sget-object v3, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f2
    move-object/from16 v0, p2

    #@1f4
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f7
    move-result-object v5

    #@1f8
    check-cast v5, Landroid/net/RouteInfo;

    #@1fa
    .line 235
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    :goto_1fa
    move-object/from16 v0, p0

    #@1fc
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->addSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V

    #@1ff
    .line 236
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@202
    .line 237
    const/4 v3, 0x1

    #@203
    goto/16 :goto_7

    #@205
    .line 233
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :cond_205
    const/4 v5, 0x0

    #@206
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    goto :goto_1fa

    #@207
    .line 241
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :sswitch_207
    const-string v3, "android.os.INetworkManagementService"

    #@209
    move-object/from16 v0, p2

    #@20b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20e
    .line 243
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@211
    move-result-object v4

    #@212
    .line 245
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@215
    move-result v3

    #@216
    if-eqz v3, :cond_22d

    #@218
    .line 246
    sget-object v3, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21a
    move-object/from16 v0, p2

    #@21c
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@21f
    move-result-object v5

    #@220
    check-cast v5, Landroid/net/RouteInfo;

    #@222
    .line 251
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    :goto_222
    move-object/from16 v0, p0

    #@224
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->removeSecondaryRoute(Ljava/lang/String;Landroid/net/RouteInfo;)V

    #@227
    .line 252
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@22a
    .line 253
    const/4 v3, 0x1

    #@22b
    goto/16 :goto_7

    #@22d
    .line 249
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :cond_22d
    const/4 v5, 0x0

    #@22e
    .restart local v5       #_arg1:Landroid/net/RouteInfo;
    goto :goto_222

    #@22f
    .line 257
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Landroid/net/RouteInfo;
    :sswitch_22f
    const-string v3, "android.os.INetworkManagementService"

    #@231
    move-object/from16 v0, p2

    #@233
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@236
    .line 258
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->shutdown()V

    #@239
    .line 259
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@23c
    .line 260
    const/4 v3, 0x1

    #@23d
    goto/16 :goto_7

    #@23f
    .line 264
    :sswitch_23f
    const-string v3, "android.os.INetworkManagementService"

    #@241
    move-object/from16 v0, p2

    #@243
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@246
    .line 265
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getIpForwardingEnabled()Z

    #@249
    move-result v28

    #@24a
    .line 266
    .local v28, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@24d
    .line 267
    if-eqz v28, :cond_258

    #@24f
    const/4 v3, 0x1

    #@250
    :goto_250
    move-object/from16 v0, p3

    #@252
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@255
    .line 268
    const/4 v3, 0x1

    #@256
    goto/16 :goto_7

    #@258
    .line 267
    :cond_258
    const/4 v3, 0x0

    #@259
    goto :goto_250

    #@25a
    .line 272
    .end local v28           #_result:Z
    :sswitch_25a
    const-string v3, "android.os.INetworkManagementService"

    #@25c
    move-object/from16 v0, p2

    #@25e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@261
    .line 274
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@264
    move-result v3

    #@265
    if-eqz v3, :cond_273

    #@267
    const/4 v4, 0x1

    #@268
    .line 275
    .local v4, _arg0:Z
    :goto_268
    move-object/from16 v0, p0

    #@26a
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setIpForwardingEnabled(Z)V

    #@26d
    .line 276
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@270
    .line 277
    const/4 v3, 0x1

    #@271
    goto/16 :goto_7

    #@273
    .line 274
    .end local v4           #_arg0:Z
    :cond_273
    const/4 v4, 0x0

    #@274
    goto :goto_268

    #@275
    .line 281
    :sswitch_275
    const-string v3, "android.os.INetworkManagementService"

    #@277
    move-object/from16 v0, p2

    #@279
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27c
    .line 283
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@27f
    move-result-object v4

    #@280
    .line 284
    .local v4, _arg0:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@282
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->startTethering([Ljava/lang/String;)V

    #@285
    .line 285
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@288
    .line 286
    const/4 v3, 0x1

    #@289
    goto/16 :goto_7

    #@28b
    .line 290
    .end local v4           #_arg0:[Ljava/lang/String;
    :sswitch_28b
    const-string v3, "android.os.INetworkManagementService"

    #@28d
    move-object/from16 v0, p2

    #@28f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@292
    .line 291
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->stopTethering()V

    #@295
    .line 292
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@298
    .line 293
    const/4 v3, 0x1

    #@299
    goto/16 :goto_7

    #@29b
    .line 297
    :sswitch_29b
    const-string v3, "android.os.INetworkManagementService"

    #@29d
    move-object/from16 v0, p2

    #@29f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a2
    .line 298
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->isTetheringStarted()Z

    #@2a5
    move-result v28

    #@2a6
    .line 299
    .restart local v28       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a9
    .line 300
    if-eqz v28, :cond_2b4

    #@2ab
    const/4 v3, 0x1

    #@2ac
    :goto_2ac
    move-object/from16 v0, p3

    #@2ae
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2b1
    .line 301
    const/4 v3, 0x1

    #@2b2
    goto/16 :goto_7

    #@2b4
    .line 300
    :cond_2b4
    const/4 v3, 0x0

    #@2b5
    goto :goto_2ac

    #@2b6
    .line 305
    .end local v28           #_result:Z
    :sswitch_2b6
    const-string v3, "android.os.INetworkManagementService"

    #@2b8
    move-object/from16 v0, p2

    #@2ba
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2bd
    .line 307
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c0
    move-result-object v4

    #@2c1
    .line 308
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2c3
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->startReverseTethering(Ljava/lang/String;)V

    #@2c6
    .line 309
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c9
    .line 310
    const/4 v3, 0x1

    #@2ca
    goto/16 :goto_7

    #@2cc
    .line 314
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_2cc
    const-string v3, "android.os.INetworkManagementService"

    #@2ce
    move-object/from16 v0, p2

    #@2d0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d3
    .line 315
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->stopReverseTethering()V

    #@2d6
    .line 316
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2d9
    .line 317
    const/4 v3, 0x1

    #@2da
    goto/16 :goto_7

    #@2dc
    .line 321
    :sswitch_2dc
    const-string v3, "android.os.INetworkManagementService"

    #@2de
    move-object/from16 v0, p2

    #@2e0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2e3
    .line 323
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2e6
    move-result-object v4

    #@2e7
    .line 324
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2e9
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->tetherInterface(Ljava/lang/String;)V

    #@2ec
    .line 325
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ef
    .line 326
    const/4 v3, 0x1

    #@2f0
    goto/16 :goto_7

    #@2f2
    .line 330
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_2f2
    const-string v3, "android.os.INetworkManagementService"

    #@2f4
    move-object/from16 v0, p2

    #@2f6
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f9
    .line 332
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2fc
    move-result-object v4

    #@2fd
    .line 333
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@2ff
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->untetherInterface(Ljava/lang/String;)V

    #@302
    .line 334
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@305
    .line 335
    const/4 v3, 0x1

    #@306
    goto/16 :goto_7

    #@308
    .line 339
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_308
    const-string v3, "android.os.INetworkManagementService"

    #@30a
    move-object/from16 v0, p2

    #@30c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30f
    .line 340
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->listTetheredInterfaces()[Ljava/lang/String;

    #@312
    move-result-object v28

    #@313
    .line 341
    .local v28, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@316
    .line 342
    move-object/from16 v0, p3

    #@318
    move-object/from16 v1, v28

    #@31a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@31d
    .line 343
    const/4 v3, 0x1

    #@31e
    goto/16 :goto_7

    #@320
    .line 347
    .end local v28           #_result:[Ljava/lang/String;
    :sswitch_320
    const-string v3, "android.os.INetworkManagementService"

    #@322
    move-object/from16 v0, p2

    #@324
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@327
    .line 349
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@32a
    move-result-object v4

    #@32b
    .line 350
    .local v4, _arg0:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@32d
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setDnsForwarders([Ljava/lang/String;)V

    #@330
    .line 351
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@333
    .line 352
    const/4 v3, 0x1

    #@334
    goto/16 :goto_7

    #@336
    .line 356
    .end local v4           #_arg0:[Ljava/lang/String;
    :sswitch_336
    const-string v3, "android.os.INetworkManagementService"

    #@338
    move-object/from16 v0, p2

    #@33a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@33d
    .line 357
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getDnsForwarders()[Ljava/lang/String;

    #@340
    move-result-object v28

    #@341
    .line 358
    .restart local v28       #_result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@344
    .line 359
    move-object/from16 v0, p3

    #@346
    move-object/from16 v1, v28

    #@348
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@34b
    .line 360
    const/4 v3, 0x1

    #@34c
    goto/16 :goto_7

    #@34e
    .line 364
    .end local v28           #_result:[Ljava/lang/String;
    :sswitch_34e
    const-string v3, "android.os.INetworkManagementService"

    #@350
    move-object/from16 v0, p2

    #@352
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@355
    .line 366
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@358
    move-result-object v4

    #@359
    .line 368
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@35c
    move-result-object v5

    #@35d
    .line 370
    .local v5, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@360
    move-result-object v6

    #@361
    .line 371
    .local v6, _arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@363
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->addRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@366
    .line 372
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@369
    .line 373
    const/4 v3, 0x1

    #@36a
    goto/16 :goto_7

    #@36c
    .line 377
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:Ljava/lang/String;
    :sswitch_36c
    const-string v3, "android.os.INetworkManagementService"

    #@36e
    move-object/from16 v0, p2

    #@370
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@373
    .line 379
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@376
    move-result-object v4

    #@377
    .line 381
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37a
    move-result-object v5

    #@37b
    .line 383
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@37e
    move-result-object v6

    #@37f
    .line 384
    .restart local v6       #_arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@381
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->delRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@384
    .line 385
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@387
    .line 386
    const/4 v3, 0x1

    #@388
    goto/16 :goto_7

    #@38a
    .line 390
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:Ljava/lang/String;
    :sswitch_38a
    const-string v3, "android.os.INetworkManagementService"

    #@38c
    move-object/from16 v0, p2

    #@38e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@391
    .line 392
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@394
    move-result-object v4

    #@395
    .line 394
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@398
    move-result-object v5

    #@399
    .line 395
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@39b
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->enableNat(Ljava/lang/String;Ljava/lang/String;)V

    #@39e
    .line 396
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a1
    .line 397
    const/4 v3, 0x1

    #@3a2
    goto/16 :goto_7

    #@3a4
    .line 401
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_3a4
    const-string v3, "android.os.INetworkManagementService"

    #@3a6
    move-object/from16 v0, p2

    #@3a8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ab
    .line 403
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ae
    move-result-object v4

    #@3af
    .line 405
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b2
    move-result-object v5

    #@3b3
    .line 406
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3b5
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->disableNat(Ljava/lang/String;Ljava/lang/String;)V

    #@3b8
    .line 407
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3bb
    .line 408
    const/4 v3, 0x1

    #@3bc
    goto/16 :goto_7

    #@3be
    .line 412
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_3be
    const-string v3, "android.os.INetworkManagementService"

    #@3c0
    move-object/from16 v0, p2

    #@3c2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c5
    .line 414
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c8
    move-result-object v4

    #@3c9
    .line 415
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3cb
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->addUpstreamV6Interface(Ljava/lang/String;)V

    #@3ce
    .line 416
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d1
    .line 417
    const/4 v3, 0x1

    #@3d2
    goto/16 :goto_7

    #@3d4
    .line 421
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_3d4
    const-string v3, "android.os.INetworkManagementService"

    #@3d6
    move-object/from16 v0, p2

    #@3d8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3db
    .line 423
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3de
    move-result-object v4

    #@3df
    .line 424
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3e1
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->removeUpstreamV6Interface(Ljava/lang/String;)V

    #@3e4
    .line 425
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e7
    .line 426
    const/4 v3, 0x1

    #@3e8
    goto/16 :goto_7

    #@3ea
    .line 430
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_3ea
    const-string v3, "android.os.INetworkManagementService"

    #@3ec
    move-object/from16 v0, p2

    #@3ee
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3f1
    .line 431
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->listTtys()[Ljava/lang/String;

    #@3f4
    move-result-object v28

    #@3f5
    .line 432
    .restart local v28       #_result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f8
    .line 433
    move-object/from16 v0, p3

    #@3fa
    move-object/from16 v1, v28

    #@3fc
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@3ff
    .line 434
    const/4 v3, 0x1

    #@400
    goto/16 :goto_7

    #@402
    .line 438
    .end local v28           #_result:[Ljava/lang/String;
    :sswitch_402
    const-string v3, "android.os.INetworkManagementService"

    #@404
    move-object/from16 v0, p2

    #@406
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@409
    .line 440
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40c
    move-result-object v4

    #@40d
    .line 442
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@410
    move-result-object v5

    #@411
    .line 444
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@414
    move-result-object v6

    #@415
    .line 446
    .restart local v6       #_arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@418
    move-result-object v7

    #@419
    .line 448
    .local v7, _arg3:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@41c
    move-result-object v8

    #@41d
    .local v8, _arg4:Ljava/lang/String;
    move-object/from16 v3, p0

    #@41f
    .line 449
    invoke-virtual/range {v3 .. v8}, Landroid/os/INetworkManagementService$Stub;->attachPppd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@422
    .line 450
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@425
    .line 451
    const/4 v3, 0x1

    #@426
    goto/16 :goto_7

    #@428
    .line 455
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:Ljava/lang/String;
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Ljava/lang/String;
    :sswitch_428
    const-string v3, "android.os.INetworkManagementService"

    #@42a
    move-object/from16 v0, p2

    #@42c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42f
    .line 457
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@432
    move-result-object v4

    #@433
    .line 458
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@435
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->detachPppd(Ljava/lang/String;)V

    #@438
    .line 459
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@43b
    .line 460
    const/4 v3, 0x1

    #@43c
    goto/16 :goto_7

    #@43e
    .line 464
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_43e
    const-string v3, "android.os.INetworkManagementService"

    #@440
    move-object/from16 v0, p2

    #@442
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@445
    .line 466
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@448
    move-result-object v4

    #@449
    .line 468
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44c
    move-result-object v5

    #@44d
    .line 469
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@44f
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->wifiFirmwareReload(Ljava/lang/String;Ljava/lang/String;)V

    #@452
    .line 470
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@455
    .line 471
    const/4 v3, 0x1

    #@456
    goto/16 :goto_7

    #@458
    .line 475
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_458
    const-string v3, "android.os.INetworkManagementService"

    #@45a
    move-object/from16 v0, p2

    #@45c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45f
    .line 477
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@462
    move-result v3

    #@463
    if-eqz v3, :cond_47e

    #@465
    .line 478
    sget-object v3, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@467
    move-object/from16 v0, p2

    #@469
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@46c
    move-result-object v4

    #@46d
    check-cast v4, Landroid/net/wifi/WifiConfiguration;

    #@46f
    .line 484
    .local v4, _arg0:Landroid/net/wifi/WifiConfiguration;
    :goto_46f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@472
    move-result-object v5

    #@473
    .line 485
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@475
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->startAccessPoint(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V

    #@478
    .line 486
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@47b
    .line 487
    const/4 v3, 0x1

    #@47c
    goto/16 :goto_7

    #@47e
    .line 481
    .end local v4           #_arg0:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #_arg1:Ljava/lang/String;
    :cond_47e
    const/4 v4, 0x0

    #@47f
    .restart local v4       #_arg0:Landroid/net/wifi/WifiConfiguration;
    goto :goto_46f

    #@480
    .line 491
    .end local v4           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :sswitch_480
    const-string v3, "android.os.INetworkManagementService"

    #@482
    move-object/from16 v0, p2

    #@484
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@487
    .line 493
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@48a
    move-result-object v4

    #@48b
    .line 494
    .local v4, _arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@48d
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->stopAccessPoint(Ljava/lang/String;)V

    #@490
    .line 495
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@493
    .line 496
    const/4 v3, 0x1

    #@494
    goto/16 :goto_7

    #@496
    .line 500
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_496
    const-string v3, "android.os.INetworkManagementService"

    #@498
    move-object/from16 v0, p2

    #@49a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@49d
    .line 502
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4a0
    move-result v3

    #@4a1
    if-eqz v3, :cond_4bc

    #@4a3
    .line 503
    sget-object v3, Landroid/net/wifi/WifiConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    #@4a5
    move-object/from16 v0, p2

    #@4a7
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4aa
    move-result-object v4

    #@4ab
    check-cast v4, Landroid/net/wifi/WifiConfiguration;

    #@4ad
    .line 509
    .local v4, _arg0:Landroid/net/wifi/WifiConfiguration;
    :goto_4ad
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b0
    move-result-object v5

    #@4b1
    .line 510
    .restart local v5       #_arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@4b3
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setAccessPoint(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V

    #@4b6
    .line 511
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b9
    .line 512
    const/4 v3, 0x1

    #@4ba
    goto/16 :goto_7

    #@4bc
    .line 506
    .end local v4           #_arg0:Landroid/net/wifi/WifiConfiguration;
    .end local v5           #_arg1:Ljava/lang/String;
    :cond_4bc
    const/4 v4, 0x0

    #@4bd
    .restart local v4       #_arg0:Landroid/net/wifi/WifiConfiguration;
    goto :goto_4ad

    #@4be
    .line 516
    .end local v4           #_arg0:Landroid/net/wifi/WifiConfiguration;
    :sswitch_4be
    const-string v3, "android.os.INetworkManagementService"

    #@4c0
    move-object/from16 v0, p2

    #@4c2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c5
    .line 517
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsSummaryDev()Landroid/net/NetworkStats;

    #@4c8
    move-result-object v28

    #@4c9
    .line 518
    .local v28, _result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4cc
    .line 519
    if-eqz v28, :cond_4df

    #@4ce
    .line 520
    const/4 v3, 0x1

    #@4cf
    move-object/from16 v0, p3

    #@4d1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4d4
    .line 521
    const/4 v3, 0x1

    #@4d5
    move-object/from16 v0, v28

    #@4d7
    move-object/from16 v1, p3

    #@4d9
    invoke-virtual {v0, v1, v3}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@4dc
    .line 526
    :goto_4dc
    const/4 v3, 0x1

    #@4dd
    goto/16 :goto_7

    #@4df
    .line 524
    :cond_4df
    const/4 v3, 0x0

    #@4e0
    move-object/from16 v0, p3

    #@4e2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4e5
    goto :goto_4dc

    #@4e6
    .line 530
    .end local v28           #_result:Landroid/net/NetworkStats;
    :sswitch_4e6
    const-string v3, "android.os.INetworkManagementService"

    #@4e8
    move-object/from16 v0, p2

    #@4ea
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ed
    .line 531
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsSummaryXt()Landroid/net/NetworkStats;

    #@4f0
    move-result-object v28

    #@4f1
    .line 532
    .restart local v28       #_result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f4
    .line 533
    if-eqz v28, :cond_507

    #@4f6
    .line 534
    const/4 v3, 0x1

    #@4f7
    move-object/from16 v0, p3

    #@4f9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@4fc
    .line 535
    const/4 v3, 0x1

    #@4fd
    move-object/from16 v0, v28

    #@4ff
    move-object/from16 v1, p3

    #@501
    invoke-virtual {v0, v1, v3}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@504
    .line 540
    :goto_504
    const/4 v3, 0x1

    #@505
    goto/16 :goto_7

    #@507
    .line 538
    :cond_507
    const/4 v3, 0x0

    #@508
    move-object/from16 v0, p3

    #@50a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@50d
    goto :goto_504

    #@50e
    .line 544
    .end local v28           #_result:Landroid/net/NetworkStats;
    :sswitch_50e
    const-string v3, "android.os.INetworkManagementService"

    #@510
    move-object/from16 v0, p2

    #@512
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@515
    .line 545
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsDetail()Landroid/net/NetworkStats;

    #@518
    move-result-object v28

    #@519
    .line 546
    .restart local v28       #_result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@51c
    .line 547
    if-eqz v28, :cond_52f

    #@51e
    .line 548
    const/4 v3, 0x1

    #@51f
    move-object/from16 v0, p3

    #@521
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@524
    .line 549
    const/4 v3, 0x1

    #@525
    move-object/from16 v0, v28

    #@527
    move-object/from16 v1, p3

    #@529
    invoke-virtual {v0, v1, v3}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@52c
    .line 554
    :goto_52c
    const/4 v3, 0x1

    #@52d
    goto/16 :goto_7

    #@52f
    .line 552
    :cond_52f
    const/4 v3, 0x0

    #@530
    move-object/from16 v0, p3

    #@532
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@535
    goto :goto_52c

    #@536
    .line 558
    .end local v28           #_result:Landroid/net/NetworkStats;
    :sswitch_536
    const-string v3, "android.os.INetworkManagementService"

    #@538
    move-object/from16 v0, p2

    #@53a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@53d
    .line 560
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@540
    move-result v4

    #@541
    .line 561
    .local v4, _arg0:I
    move-object/from16 v0, p0

    #@543
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsUidDetail(I)Landroid/net/NetworkStats;

    #@546
    move-result-object v28

    #@547
    .line 562
    .restart local v28       #_result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@54a
    .line 563
    if-eqz v28, :cond_55d

    #@54c
    .line 564
    const/4 v3, 0x1

    #@54d
    move-object/from16 v0, p3

    #@54f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@552
    .line 565
    const/4 v3, 0x1

    #@553
    move-object/from16 v0, v28

    #@555
    move-object/from16 v1, p3

    #@557
    invoke-virtual {v0, v1, v3}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@55a
    .line 570
    :goto_55a
    const/4 v3, 0x1

    #@55b
    goto/16 :goto_7

    #@55d
    .line 568
    :cond_55d
    const/4 v3, 0x0

    #@55e
    move-object/from16 v0, p3

    #@560
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@563
    goto :goto_55a

    #@564
    .line 574
    .end local v4           #_arg0:I
    .end local v28           #_result:Landroid/net/NetworkStats;
    :sswitch_564
    const-string v3, "android.os.INetworkManagementService"

    #@566
    move-object/from16 v0, p2

    #@568
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@56b
    .line 576
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@56e
    move-result v4

    #@56f
    .line 578
    .restart local v4       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@572
    move-result-object v5

    #@573
    .line 580
    .restart local v5       #_arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@576
    move-result v6

    #@577
    .line 581
    .local v6, _arg2:I
    move-object/from16 v0, p0

    #@579
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsUidInterface(ILjava/lang/String;I)J

    #@57c
    move-result-wide v28

    #@57d
    .line 582
    .local v28, _result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@580
    .line 583
    move-object/from16 v0, p3

    #@582
    move-wide/from16 v1, v28

    #@584
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@587
    .line 584
    const/4 v3, 0x1

    #@588
    goto/16 :goto_7

    #@58a
    .line 588
    .end local v4           #_arg0:I
    .end local v5           #_arg1:Ljava/lang/String;
    .end local v6           #_arg2:I
    .end local v28           #_result:J
    :sswitch_58a
    const-string v3, "android.os.INetworkManagementService"

    #@58c
    move-object/from16 v0, p2

    #@58e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@591
    .line 590
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@594
    move-result-object v4

    #@595
    .line 591
    .local v4, _arg0:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@597
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getNetworkStatsTethering([Ljava/lang/String;)Landroid/net/NetworkStats;

    #@59a
    move-result-object v28

    #@59b
    .line 592
    .local v28, _result:Landroid/net/NetworkStats;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@59e
    .line 593
    if-eqz v28, :cond_5b1

    #@5a0
    .line 594
    const/4 v3, 0x1

    #@5a1
    move-object/from16 v0, p3

    #@5a3
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5a6
    .line 595
    const/4 v3, 0x1

    #@5a7
    move-object/from16 v0, v28

    #@5a9
    move-object/from16 v1, p3

    #@5ab
    invoke-virtual {v0, v1, v3}, Landroid/net/NetworkStats;->writeToParcel(Landroid/os/Parcel;I)V

    #@5ae
    .line 600
    :goto_5ae
    const/4 v3, 0x1

    #@5af
    goto/16 :goto_7

    #@5b1
    .line 598
    :cond_5b1
    const/4 v3, 0x0

    #@5b2
    move-object/from16 v0, p3

    #@5b4
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5b7
    goto :goto_5ae

    #@5b8
    .line 604
    .end local v4           #_arg0:[Ljava/lang/String;
    .end local v28           #_result:Landroid/net/NetworkStats;
    :sswitch_5b8
    const-string v3, "android.os.INetworkManagementService"

    #@5ba
    move-object/from16 v0, p2

    #@5bc
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5bf
    .line 606
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5c2
    move-result-object v4

    #@5c3
    .line 608
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@5c6
    move-result-wide v26

    #@5c7
    .line 609
    .local v26, _arg1:J
    move-object/from16 v0, p0

    #@5c9
    move-wide/from16 v1, v26

    #@5cb
    invoke-virtual {v0, v4, v1, v2}, Landroid/os/INetworkManagementService$Stub;->setInterfaceQuota(Ljava/lang/String;J)V

    #@5ce
    .line 610
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5d1
    .line 611
    const/4 v3, 0x1

    #@5d2
    goto/16 :goto_7

    #@5d4
    .line 615
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v26           #_arg1:J
    :sswitch_5d4
    const-string v3, "android.os.INetworkManagementService"

    #@5d6
    move-object/from16 v0, p2

    #@5d8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5db
    .line 617
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5de
    move-result-object v4

    #@5df
    .line 618
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@5e1
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->removeInterfaceQuota(Ljava/lang/String;)V

    #@5e4
    .line 619
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e7
    .line 620
    const/4 v3, 0x1

    #@5e8
    goto/16 :goto_7

    #@5ea
    .line 624
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_5ea
    const-string v3, "android.os.INetworkManagementService"

    #@5ec
    move-object/from16 v0, p2

    #@5ee
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5f1
    .line 626
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5f4
    move-result-object v4

    #@5f5
    .line 628
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@5f8
    move-result-wide v26

    #@5f9
    .line 629
    .restart local v26       #_arg1:J
    move-object/from16 v0, p0

    #@5fb
    move-wide/from16 v1, v26

    #@5fd
    invoke-virtual {v0, v4, v1, v2}, Landroid/os/INetworkManagementService$Stub;->setInterfaceAlert(Ljava/lang/String;J)V

    #@600
    .line 630
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@603
    .line 631
    const/4 v3, 0x1

    #@604
    goto/16 :goto_7

    #@606
    .line 635
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v26           #_arg1:J
    :sswitch_606
    const-string v3, "android.os.INetworkManagementService"

    #@608
    move-object/from16 v0, p2

    #@60a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60d
    .line 637
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@610
    move-result-object v4

    #@611
    .line 638
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@613
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->removeInterfaceAlert(Ljava/lang/String;)V

    #@616
    .line 639
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@619
    .line 640
    const/4 v3, 0x1

    #@61a
    goto/16 :goto_7

    #@61c
    .line 644
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_61c
    const-string v3, "android.os.INetworkManagementService"

    #@61e
    move-object/from16 v0, p2

    #@620
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@623
    .line 646
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@626
    move-result-wide v24

    #@627
    .line 647
    .local v24, _arg0:J
    move-object/from16 v0, p0

    #@629
    move-wide/from16 v1, v24

    #@62b
    invoke-virtual {v0, v1, v2}, Landroid/os/INetworkManagementService$Stub;->setGlobalAlert(J)V

    #@62e
    .line 648
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@631
    .line 649
    const/4 v3, 0x1

    #@632
    goto/16 :goto_7

    #@634
    .line 653
    .end local v24           #_arg0:J
    :sswitch_634
    const-string v3, "android.os.INetworkManagementService"

    #@636
    move-object/from16 v0, p2

    #@638
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@63b
    .line 655
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@63e
    move-result v4

    #@63f
    .line 657
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@642
    move-result v3

    #@643
    if-eqz v3, :cond_651

    #@645
    const/4 v5, 0x1

    #@646
    .line 658
    .local v5, _arg1:Z
    :goto_646
    move-object/from16 v0, p0

    #@648
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setUidNetworkRules(IZ)V

    #@64b
    .line 659
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@64e
    .line 660
    const/4 v3, 0x1

    #@64f
    goto/16 :goto_7

    #@651
    .line 657
    .end local v5           #_arg1:Z
    :cond_651
    const/4 v5, 0x0

    #@652
    goto :goto_646

    #@653
    .line 664
    .end local v4           #_arg0:I
    :sswitch_653
    const-string v3, "android.os.INetworkManagementService"

    #@655
    move-object/from16 v0, p2

    #@657
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@65a
    .line 665
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->isBandwidthControlEnabled()Z

    #@65d
    move-result v28

    #@65e
    .line 666
    .local v28, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@661
    .line 667
    if-eqz v28, :cond_66c

    #@663
    const/4 v3, 0x1

    #@664
    :goto_664
    move-object/from16 v0, p3

    #@666
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@669
    .line 668
    const/4 v3, 0x1

    #@66a
    goto/16 :goto_7

    #@66c
    .line 667
    :cond_66c
    const/4 v3, 0x0

    #@66d
    goto :goto_664

    #@66e
    .line 672
    .end local v28           #_result:Z
    :sswitch_66e
    const-string v3, "android.os.INetworkManagementService"

    #@670
    move-object/from16 v0, p2

    #@672
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@675
    .line 674
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@678
    move-result-object v4

    #@679
    .line 676
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@67c
    move-result v5

    #@67d
    .line 678
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@680
    move-result v6

    #@681
    .line 679
    .restart local v6       #_arg2:I
    move-object/from16 v0, p0

    #@683
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->setInterfaceThrottle(Ljava/lang/String;II)V

    #@686
    .line 680
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@689
    .line 681
    const/4 v3, 0x1

    #@68a
    goto/16 :goto_7

    #@68c
    .line 685
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    :sswitch_68c
    const-string v3, "android.os.INetworkManagementService"

    #@68e
    move-object/from16 v0, p2

    #@690
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@693
    .line 687
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@696
    move-result-object v4

    #@697
    .line 688
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@699
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getInterfaceRxThrottle(Ljava/lang/String;)I

    #@69c
    move-result v28

    #@69d
    .line 689
    .local v28, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a0
    .line 690
    move-object/from16 v0, p3

    #@6a2
    move/from16 v1, v28

    #@6a4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6a7
    .line 691
    const/4 v3, 0x1

    #@6a8
    goto/16 :goto_7

    #@6aa
    .line 695
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:I
    :sswitch_6aa
    const-string v3, "android.os.INetworkManagementService"

    #@6ac
    move-object/from16 v0, p2

    #@6ae
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6b1
    .line 697
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6b4
    move-result-object v4

    #@6b5
    .line 698
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6b7
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getInterfaceTxThrottle(Ljava/lang/String;)I

    #@6ba
    move-result v28

    #@6bb
    .line 699
    .restart local v28       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6be
    .line 700
    move-object/from16 v0, p3

    #@6c0
    move/from16 v1, v28

    #@6c2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6c5
    .line 701
    const/4 v3, 0x1

    #@6c6
    goto/16 :goto_7

    #@6c8
    .line 705
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:I
    :sswitch_6c8
    const-string v3, "android.os.INetworkManagementService"

    #@6ca
    move-object/from16 v0, p2

    #@6cc
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6cf
    .line 707
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6d2
    move-result-object v4

    #@6d3
    .line 709
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6d6
    move-result v5

    #@6d7
    .line 711
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6da
    move-result-object v6

    #@6db
    .line 712
    .local v6, _arg2:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6dd
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->addIdleTimer(Ljava/lang/String;ILjava/lang/String;)V

    #@6e0
    .line 713
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e3
    .line 714
    const/4 v3, 0x1

    #@6e4
    goto/16 :goto_7

    #@6e6
    .line 718
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:Ljava/lang/String;
    :sswitch_6e6
    const-string v3, "android.os.INetworkManagementService"

    #@6e8
    move-object/from16 v0, p2

    #@6ea
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6ed
    .line 720
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6f0
    move-result-object v4

    #@6f1
    .line 721
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@6f3
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->removeIdleTimer(Ljava/lang/String;)V

    #@6f6
    .line 722
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@6f9
    .line 723
    const/4 v3, 0x1

    #@6fa
    goto/16 :goto_7

    #@6fc
    .line 727
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_6fc
    const-string v3, "android.os.INetworkManagementService"

    #@6fe
    move-object/from16 v0, p2

    #@700
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@703
    .line 729
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@706
    move-result-object v4

    #@707
    .line 730
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@709
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setDefaultInterfaceForDns(Ljava/lang/String;)V

    #@70c
    .line 731
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@70f
    .line 732
    const/4 v3, 0x1

    #@710
    goto/16 :goto_7

    #@712
    .line 736
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_712
    const-string v3, "android.os.INetworkManagementService"

    #@714
    move-object/from16 v0, p2

    #@716
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@719
    .line 738
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@71c
    move-result-object v4

    #@71d
    .line 740
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@720
    move-result-object v5

    #@721
    .line 741
    .local v5, _arg1:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@723
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setDnsServersForInterface(Ljava/lang/String;[Ljava/lang/String;)V

    #@726
    .line 742
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@729
    .line 743
    const/4 v3, 0x1

    #@72a
    goto/16 :goto_7

    #@72c
    .line 747
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:[Ljava/lang/String;
    :sswitch_72c
    const-string v3, "android.os.INetworkManagementService"

    #@72e
    move-object/from16 v0, p2

    #@730
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@733
    .line 748
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->flushDefaultDnsCache()V

    #@736
    .line 749
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@739
    .line 750
    const/4 v3, 0x1

    #@73a
    goto/16 :goto_7

    #@73c
    .line 754
    :sswitch_73c
    const-string v3, "android.os.INetworkManagementService"

    #@73e
    move-object/from16 v0, p2

    #@740
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@743
    .line 756
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@746
    move-result-object v4

    #@747
    .line 757
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@749
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->flushInterfaceDnsCache(Ljava/lang/String;)V

    #@74c
    .line 758
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@74f
    .line 759
    const/4 v3, 0x1

    #@750
    goto/16 :goto_7

    #@752
    .line 763
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_752
    const-string v3, "android.os.INetworkManagementService"

    #@754
    move-object/from16 v0, p2

    #@756
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@759
    .line 765
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@75c
    move-result-object v4

    #@75d
    .line 766
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@75f
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->runShellCommand(Ljava/lang/String;)V

    #@762
    .line 767
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@765
    .line 768
    const/4 v3, 0x1

    #@766
    goto/16 :goto_7

    #@768
    .line 772
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_768
    const-string v3, "android.os.INetworkManagementService"

    #@76a
    move-object/from16 v0, p2

    #@76c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@76f
    .line 774
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@772
    move-result-object v4

    #@773
    .line 776
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@776
    move-result v5

    #@777
    .line 777
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@779
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->runSetPortRedirectRule(Ljava/lang/String;I)V

    #@77c
    .line 778
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@77f
    .line 779
    const/4 v3, 0x1

    #@780
    goto/16 :goto_7

    #@782
    .line 783
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    :sswitch_782
    const-string v3, "android.os.INetworkManagementService"

    #@784
    move-object/from16 v0, p2

    #@786
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@789
    .line 785
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@78c
    move-result v3

    #@78d
    if-eqz v3, :cond_79b

    #@78f
    const/4 v4, 0x1

    #@790
    .line 786
    .local v4, _arg0:Z
    :goto_790
    move-object/from16 v0, p0

    #@792
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setFirewallEnabled(Z)V

    #@795
    .line 787
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@798
    .line 788
    const/4 v3, 0x1

    #@799
    goto/16 :goto_7

    #@79b
    .line 785
    .end local v4           #_arg0:Z
    :cond_79b
    const/4 v4, 0x0

    #@79c
    goto :goto_790

    #@79d
    .line 792
    :sswitch_79d
    const-string v3, "android.os.INetworkManagementService"

    #@79f
    move-object/from16 v0, p2

    #@7a1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a4
    .line 793
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->isFirewallEnabled()Z

    #@7a7
    move-result v28

    #@7a8
    .line 794
    .local v28, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7ab
    .line 795
    if-eqz v28, :cond_7b6

    #@7ad
    const/4 v3, 0x1

    #@7ae
    :goto_7ae
    move-object/from16 v0, p3

    #@7b0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7b3
    .line 796
    const/4 v3, 0x1

    #@7b4
    goto/16 :goto_7

    #@7b6
    .line 795
    :cond_7b6
    const/4 v3, 0x0

    #@7b7
    goto :goto_7ae

    #@7b8
    .line 800
    .end local v28           #_result:Z
    :sswitch_7b8
    const-string v3, "android.os.INetworkManagementService"

    #@7ba
    move-object/from16 v0, p2

    #@7bc
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7bf
    .line 802
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7c2
    move-result-object v4

    #@7c3
    .line 804
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7c6
    move-result v3

    #@7c7
    if-eqz v3, :cond_7d5

    #@7c9
    const/4 v5, 0x1

    #@7ca
    .line 805
    .local v5, _arg1:Z
    :goto_7ca
    move-object/from16 v0, p0

    #@7cc
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setFirewallInterfaceRule(Ljava/lang/String;Z)V

    #@7cf
    .line 806
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7d2
    .line 807
    const/4 v3, 0x1

    #@7d3
    goto/16 :goto_7

    #@7d5
    .line 804
    .end local v5           #_arg1:Z
    :cond_7d5
    const/4 v5, 0x0

    #@7d6
    goto :goto_7ca

    #@7d7
    .line 811
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_7d7
    const-string v3, "android.os.INetworkManagementService"

    #@7d9
    move-object/from16 v0, p2

    #@7db
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7de
    .line 813
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e1
    move-result-object v4

    #@7e2
    .line 815
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7e5
    move-result v3

    #@7e6
    if-eqz v3, :cond_7f4

    #@7e8
    const/4 v5, 0x1

    #@7e9
    .line 816
    .restart local v5       #_arg1:Z
    :goto_7e9
    move-object/from16 v0, p0

    #@7eb
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setFirewallEgressSourceRule(Ljava/lang/String;Z)V

    #@7ee
    .line 817
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7f1
    .line 818
    const/4 v3, 0x1

    #@7f2
    goto/16 :goto_7

    #@7f4
    .line 815
    .end local v5           #_arg1:Z
    :cond_7f4
    const/4 v5, 0x0

    #@7f5
    goto :goto_7e9

    #@7f6
    .line 822
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_7f6
    const-string v3, "android.os.INetworkManagementService"

    #@7f8
    move-object/from16 v0, p2

    #@7fa
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7fd
    .line 824
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@800
    move-result-object v4

    #@801
    .line 826
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@804
    move-result v5

    #@805
    .line 828
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@808
    move-result v3

    #@809
    if-eqz v3, :cond_817

    #@80b
    const/4 v6, 0x1

    #@80c
    .line 829
    .local v6, _arg2:Z
    :goto_80c
    move-object/from16 v0, p0

    #@80e
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->setFirewallEgressDestRule(Ljava/lang/String;IZ)V

    #@811
    .line 830
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@814
    .line 831
    const/4 v3, 0x1

    #@815
    goto/16 :goto_7

    #@817
    .line 828
    .end local v6           #_arg2:Z
    :cond_817
    const/4 v6, 0x0

    #@818
    goto :goto_80c

    #@819
    .line 835
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    :sswitch_819
    const-string v3, "android.os.INetworkManagementService"

    #@81b
    move-object/from16 v0, p2

    #@81d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@820
    .line 837
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@823
    move-result v4

    #@824
    .line 839
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@827
    move-result v3

    #@828
    if-eqz v3, :cond_836

    #@82a
    const/4 v5, 0x1

    #@82b
    .line 840
    .local v5, _arg1:Z
    :goto_82b
    move-object/from16 v0, p0

    #@82d
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setFirewallUidRule(IZ)V

    #@830
    .line 841
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@833
    .line 842
    const/4 v3, 0x1

    #@834
    goto/16 :goto_7

    #@836
    .line 839
    .end local v5           #_arg1:Z
    :cond_836
    const/4 v5, 0x0

    #@837
    goto :goto_82b

    #@838
    .line 846
    .end local v4           #_arg0:I
    :sswitch_838
    const-string v3, "android.os.INetworkManagementService"

    #@83a
    move-object/from16 v0, p2

    #@83c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@83f
    .line 848
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@842
    move-result v3

    #@843
    if-eqz v3, :cond_851

    #@845
    const/4 v4, 0x1

    #@846
    .line 849
    .local v4, _arg0:Z
    :goto_846
    move-object/from16 v0, p0

    #@848
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setVoiceProtectionEnabled(Z)V

    #@84b
    .line 850
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@84e
    .line 851
    const/4 v3, 0x1

    #@84f
    goto/16 :goto_7

    #@851
    .line 848
    .end local v4           #_arg0:Z
    :cond_851
    const/4 v4, 0x0

    #@852
    goto :goto_846

    #@853
    .line 855
    :sswitch_853
    const-string v3, "android.os.INetworkManagementService"

    #@855
    move-object/from16 v0, p2

    #@857
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85a
    .line 857
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@85d
    move-result-object v4

    #@85e
    .line 859
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@861
    move-result v5

    #@862
    .line 861
    .local v5, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@865
    move-result v3

    #@866
    if-eqz v3, :cond_886

    #@868
    .line 862
    sget-object v3, Landroid/net/RouteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@86a
    move-object/from16 v0, p2

    #@86c
    invoke-interface {v3, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@86f
    move-result-object v6

    #@870
    check-cast v6, Landroid/net/RouteInfo;

    #@872
    .line 867
    .local v6, _arg2:Landroid/net/RouteInfo;
    :goto_872
    move-object/from16 v0, p0

    #@874
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->addRouteWithMetric(Ljava/lang/String;ILandroid/net/RouteInfo;)Z

    #@877
    move-result v28

    #@878
    .line 868
    .restart local v28       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@87b
    .line 869
    if-eqz v28, :cond_888

    #@87d
    const/4 v3, 0x1

    #@87e
    :goto_87e
    move-object/from16 v0, p3

    #@880
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@883
    .line 870
    const/4 v3, 0x1

    #@884
    goto/16 :goto_7

    #@886
    .line 865
    .end local v6           #_arg2:Landroid/net/RouteInfo;
    .end local v28           #_result:Z
    :cond_886
    const/4 v6, 0x0

    #@887
    .restart local v6       #_arg2:Landroid/net/RouteInfo;
    goto :goto_872

    #@888
    .line 869
    .restart local v28       #_result:Z
    :cond_888
    const/4 v3, 0x0

    #@889
    goto :goto_87e

    #@88a
    .line 874
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:Landroid/net/RouteInfo;
    .end local v28           #_result:Z
    :sswitch_88a
    const-string v3, "android.os.INetworkManagementService"

    #@88c
    move-object/from16 v0, p2

    #@88e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@891
    .line 876
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@894
    move-result-object v4

    #@895
    .line 878
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@898
    move-result-object v5

    #@899
    .line 880
    .local v5, _arg1:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@89c
    move-result-object v6

    #@89d
    .line 882
    .local v6, _arg2:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8a0
    move-result v7

    #@8a1
    .line 883
    .local v7, _arg3:I
    move-object/from16 v0, p0

    #@8a3
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/os/INetworkManagementService$Stub;->replaceSrcRoute(Ljava/lang/String;[B[BI)Z

    #@8a6
    move-result v28

    #@8a7
    .line 884
    .restart local v28       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8aa
    .line 885
    if-eqz v28, :cond_8b5

    #@8ac
    const/4 v3, 0x1

    #@8ad
    :goto_8ad
    move-object/from16 v0, p3

    #@8af
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@8b2
    .line 886
    const/4 v3, 0x1

    #@8b3
    goto/16 :goto_7

    #@8b5
    .line 885
    :cond_8b5
    const/4 v3, 0x0

    #@8b6
    goto :goto_8ad

    #@8b7
    .line 890
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:[B
    .end local v6           #_arg2:[B
    .end local v7           #_arg3:I
    .end local v28           #_result:Z
    :sswitch_8b7
    const-string v3, "android.os.INetworkManagementService"

    #@8b9
    move-object/from16 v0, p2

    #@8bb
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8be
    .line 892
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    #@8c1
    move-result-object v4

    #@8c2
    .line 894
    .local v4, _arg0:[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8c5
    move-result v5

    #@8c6
    .line 895
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@8c8
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->delSrcRoute([BI)Z

    #@8cb
    move-result v28

    #@8cc
    .line 896
    .restart local v28       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8cf
    .line 897
    if-eqz v28, :cond_8da

    #@8d1
    const/4 v3, 0x1

    #@8d2
    :goto_8d2
    move-object/from16 v0, p3

    #@8d4
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@8d7
    .line 898
    const/4 v3, 0x1

    #@8d8
    goto/16 :goto_7

    #@8da
    .line 897
    :cond_8da
    const/4 v3, 0x0

    #@8db
    goto :goto_8d2

    #@8dc
    .line 902
    .end local v4           #_arg0:[B
    .end local v5           #_arg1:I
    .end local v28           #_result:Z
    :sswitch_8dc
    const-string v3, "android.os.INetworkManagementService"

    #@8de
    move-object/from16 v0, p2

    #@8e0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8e3
    .line 904
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8e6
    move-result v4

    #@8e7
    .line 906
    .local v4, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8ea
    move-result v5

    #@8eb
    .line 908
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@8ee
    move-result v6

    #@8ef
    .line 909
    .local v6, _arg2:I
    move-object/from16 v0, p0

    #@8f1
    invoke-virtual {v0, v4, v5, v6}, Landroid/os/INetworkManagementService$Stub;->setChannelRange(III)V

    #@8f4
    .line 910
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8f7
    .line 911
    const/4 v3, 0x1

    #@8f8
    goto/16 :goto_7

    #@8fa
    .line 915
    .end local v4           #_arg0:I
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    :sswitch_8fa
    const-string v3, "android.os.INetworkManagementService"

    #@8fc
    move-object/from16 v0, p2

    #@8fe
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@901
    .line 916
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getSapOperatingChannel()I

    #@904
    move-result v28

    #@905
    .line 917
    .local v28, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@908
    .line 918
    move-object/from16 v0, p3

    #@90a
    move/from16 v1, v28

    #@90c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@90f
    .line 919
    const/4 v3, 0x1

    #@910
    goto/16 :goto_7

    #@912
    .line 923
    .end local v28           #_result:I
    :sswitch_912
    const-string v3, "android.os.INetworkManagementService"

    #@914
    move-object/from16 v0, p2

    #@916
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@919
    .line 924
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->getSapAutoChannelSelection()I

    #@91c
    move-result v28

    #@91d
    .line 925
    .restart local v28       #_result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@920
    .line 926
    move-object/from16 v0, p3

    #@922
    move/from16 v1, v28

    #@924
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@927
    .line 927
    const/4 v3, 0x1

    #@928
    goto/16 :goto_7

    #@92a
    .line 931
    .end local v28           #_result:I
    :sswitch_92a
    const-string v3, "android.os.INetworkManagementService"

    #@92c
    move-object/from16 v0, p2

    #@92e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@931
    .line 933
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@934
    move-result-object v4

    #@935
    .line 935
    .local v4, _arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@938
    move-result-object v5

    #@939
    .line 936
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@93b
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->runDataCommand(Ljava/lang/String;Ljava/lang/String;)V

    #@93e
    .line 937
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@941
    .line 938
    const/4 v3, 0x1

    #@942
    goto/16 :goto_7

    #@944
    .line 942
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:Ljava/lang/String;
    :sswitch_944
    const-string v3, "android.os.INetworkManagementService"

    #@946
    move-object/from16 v0, p2

    #@948
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@94b
    .line 944
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@94e
    move-result-object v4

    #@94f
    .line 945
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@951
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->dropPacket(Ljava/lang/String;)V

    #@954
    .line 946
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@957
    .line 947
    const/4 v3, 0x1

    #@958
    goto/16 :goto_7

    #@95a
    .line 951
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_95a
    const-string v3, "android.os.INetworkManagementService"

    #@95c
    move-object/from16 v0, p2

    #@95e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@961
    .line 953
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@964
    move-result-object v4

    #@965
    .line 954
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@967
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->acceptPacket(Ljava/lang/String;)V

    #@96a
    .line 955
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@96d
    .line 956
    const/4 v3, 0x1

    #@96e
    goto/16 :goto_7

    #@970
    .line 960
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_970
    const-string v3, "android.os.INetworkManagementService"

    #@972
    move-object/from16 v0, p2

    #@974
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@977
    .line 961
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->resetPacketDrop()V

    #@97a
    .line 962
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@97d
    .line 963
    const/4 v3, 0x1

    #@97e
    goto/16 :goto_7

    #@980
    .line 967
    :sswitch_980
    const-string v3, "android.os.INetworkManagementService"

    #@982
    move-object/from16 v0, p2

    #@984
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@987
    .line 968
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->packetList_Indrop()Z

    #@98a
    move-result v28

    #@98b
    .line 969
    .local v28, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@98e
    .line 970
    if-eqz v28, :cond_999

    #@990
    const/4 v3, 0x1

    #@991
    :goto_991
    move-object/from16 v0, p3

    #@993
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@996
    .line 971
    const/4 v3, 0x1

    #@997
    goto/16 :goto_7

    #@999
    .line 970
    :cond_999
    const/4 v3, 0x0

    #@99a
    goto :goto_991

    #@99b
    .line 975
    .end local v28           #_result:Z
    :sswitch_99b
    const-string v3, "android.os.INetworkManagementService"

    #@99d
    move-object/from16 v0, p2

    #@99f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a2
    .line 977
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9a5
    move-result-object v4

    #@9a6
    .line 978
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9a8
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getRouteList_debug(Ljava/lang/String;)V

    #@9ab
    .line 979
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ae
    .line 980
    const/4 v3, 0x1

    #@9af
    goto/16 :goto_7

    #@9b1
    .line 984
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_9b1
    const-string v3, "android.os.INetworkManagementService"

    #@9b3
    move-object/from16 v0, p2

    #@9b5
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9b8
    .line 985
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->packetList_Indrop_view()V

    #@9bb
    .line 986
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9be
    .line 987
    const/4 v3, 0x1

    #@9bf
    goto/16 :goto_7

    #@9c1
    .line 991
    :sswitch_9c1
    const-string v3, "android.os.INetworkManagementService"

    #@9c3
    move-object/from16 v0, p2

    #@9c5
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9c8
    .line 993
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9cb
    move-result-object v4

    #@9cc
    .line 994
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9ce
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->blockIPv6Interface(Ljava/lang/String;)V

    #@9d1
    .line 995
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9d4
    .line 996
    const/4 v3, 0x1

    #@9d5
    goto/16 :goto_7

    #@9d7
    .line 1000
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_9d7
    const-string v3, "android.os.INetworkManagementService"

    #@9d9
    move-object/from16 v0, p2

    #@9db
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9de
    .line 1002
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9e1
    move-result-object v4

    #@9e2
    .line 1003
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9e4
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->unblockIPv6Interface(Ljava/lang/String;)V

    #@9e7
    .line 1004
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@9ea
    .line 1005
    const/4 v3, 0x1

    #@9eb
    goto/16 :goto_7

    #@9ed
    .line 1009
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_9ed
    const-string v3, "android.os.INetworkManagementService"

    #@9ef
    move-object/from16 v0, p2

    #@9f1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9f4
    .line 1011
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9f7
    move-result-object v4

    #@9f8
    .line 1012
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@9fa
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getInterfaceRxCounter(Ljava/lang/String;)J

    #@9fd
    move-result-wide v28

    #@9fe
    .line 1013
    .local v28, _result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a01
    .line 1014
    move-object/from16 v0, p3

    #@a03
    move-wide/from16 v1, v28

    #@a05
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@a08
    .line 1015
    const/4 v3, 0x1

    #@a09
    goto/16 :goto_7

    #@a0b
    .line 1019
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:J
    :sswitch_a0b
    const-string v3, "android.os.INetworkManagementService"

    #@a0d
    move-object/from16 v0, p2

    #@a0f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a12
    .line 1021
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a15
    move-result-object v4

    #@a16
    .line 1022
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a18
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getInterfaceTxCounter(Ljava/lang/String;)J

    #@a1b
    move-result-wide v28

    #@a1c
    .line 1023
    .restart local v28       #_result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a1f
    .line 1024
    move-object/from16 v0, p3

    #@a21
    move-wide/from16 v1, v28

    #@a23
    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@a26
    .line 1025
    const/4 v3, 0x1

    #@a27
    goto/16 :goto_7

    #@a29
    .line 1029
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:J
    :sswitch_a29
    const-string v3, "android.os.INetworkManagementService"

    #@a2b
    move-object/from16 v0, p2

    #@a2d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a30
    .line 1031
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a33
    move-result-object v4

    #@a34
    .line 1033
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a37
    move-result v5

    #@a38
    .line 1034
    .local v5, _arg1:I
    move-object/from16 v0, p0

    #@a3a
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setMdmIpRule(Ljava/lang/String;I)V

    #@a3d
    .line 1035
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a40
    .line 1036
    const/4 v3, 0x1

    #@a41
    goto/16 :goto_7

    #@a43
    .line 1040
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    :sswitch_a43
    const-string v3, "android.os.INetworkManagementService"

    #@a45
    move-object/from16 v0, p2

    #@a47
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a4a
    .line 1042
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a4d
    move-result-object v4

    #@a4e
    .line 1043
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a50
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setMdmIpRuleFile(Ljava/lang/String;)V

    #@a53
    .line 1044
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a56
    .line 1045
    const/4 v3, 0x1

    #@a57
    goto/16 :goto_7

    #@a59
    .line 1049
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_a59
    const-string v3, "android.os.INetworkManagementService"

    #@a5b
    move-object/from16 v0, p2

    #@a5d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a60
    .line 1050
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->clearMdmIpRule()V

    #@a63
    .line 1051
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a66
    .line 1052
    const/4 v3, 0x1

    #@a67
    goto/16 :goto_7

    #@a69
    .line 1056
    :sswitch_a69
    const-string v3, "android.os.INetworkManagementService"

    #@a6b
    move-object/from16 v0, p2

    #@a6d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a70
    .line 1057
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->initMdmIpRule()V

    #@a73
    .line 1058
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a76
    .line 1059
    const/4 v3, 0x1

    #@a77
    goto/16 :goto_7

    #@a79
    .line 1063
    :sswitch_a79
    const-string v3, "android.os.INetworkManagementService"

    #@a7b
    move-object/from16 v0, p2

    #@a7d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a80
    .line 1064
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->exitMdmIpRule()V

    #@a83
    .line 1065
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a86
    .line 1066
    const/4 v3, 0x1

    #@a87
    goto/16 :goto_7

    #@a89
    .line 1070
    :sswitch_a89
    const-string v3, "android.os.INetworkManagementService"

    #@a8b
    move-object/from16 v0, p2

    #@a8d
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a90
    .line 1072
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a93
    move-result-object v4

    #@a94
    .line 1073
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a96
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setMdmIptables(Ljava/lang/String;)V

    #@a99
    .line 1074
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a9c
    .line 1075
    const/4 v3, 0x1

    #@a9d
    goto/16 :goto_7

    #@a9f
    .line 1079
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_a9f
    const-string v3, "android.os.INetworkManagementService"

    #@aa1
    move-object/from16 v0, p2

    #@aa3
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@aa6
    .line 1081
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@aa9
    move-result-object v4

    #@aaa
    .line 1082
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@aac
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->setMdmIptablesFile(Ljava/lang/String;)V

    #@aaf
    .line 1083
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ab2
    .line 1084
    const/4 v3, 0x1

    #@ab3
    goto/16 :goto_7

    #@ab5
    .line 1088
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_ab5
    const-string v3, "android.os.INetworkManagementService"

    #@ab7
    move-object/from16 v0, p2

    #@ab9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@abc
    .line 1089
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->runClearNatRule()V

    #@abf
    .line 1090
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ac2
    .line 1091
    const/4 v3, 0x1

    #@ac3
    goto/16 :goto_7

    #@ac5
    .line 1095
    :sswitch_ac5
    const-string v3, "android.os.INetworkManagementService"

    #@ac7
    move-object/from16 v0, p2

    #@ac9
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@acc
    .line 1097
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@acf
    move-result-object v4

    #@ad0
    .line 1098
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ad2
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->runSetNatForwardRule(Ljava/lang/String;)V

    #@ad5
    .line 1099
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ad8
    .line 1100
    const/4 v3, 0x1

    #@ad9
    goto/16 :goto_7

    #@adb
    .line 1104
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_adb
    const-string v3, "android.os.INetworkManagementService"

    #@add
    move-object/from16 v0, p2

    #@adf
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae2
    .line 1105
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->runClearPortFilterRule()V

    #@ae5
    .line 1106
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae8
    .line 1107
    const/4 v3, 0x1

    #@ae9
    goto/16 :goto_7

    #@aeb
    .line 1111
    :sswitch_aeb
    const-string v3, "android.os.INetworkManagementService"

    #@aed
    move-object/from16 v0, p2

    #@aef
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@af2
    .line 1113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@af5
    move-result-object v4

    #@af6
    .line 1115
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@af9
    move-result v5

    #@afa
    .line 1116
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@afc
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->runSetPortFilterRule(Ljava/lang/String;I)V

    #@aff
    .line 1117
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b02
    .line 1118
    const/4 v3, 0x1

    #@b03
    goto/16 :goto_7

    #@b05
    .line 1122
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    :sswitch_b05
    const-string v3, "android.os.INetworkManagementService"

    #@b07
    move-object/from16 v0, p2

    #@b09
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b0c
    .line 1123
    invoke-virtual/range {p0 .. p0}, Landroid/os/INetworkManagementService$Stub;->runClearPortForwardRule()V

    #@b0f
    .line 1124
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b12
    .line 1125
    const/4 v3, 0x1

    #@b13
    goto/16 :goto_7

    #@b15
    .line 1129
    :sswitch_b15
    const-string v3, "android.os.INetworkManagementService"

    #@b17
    move-object/from16 v0, p2

    #@b19
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1c
    .line 1131
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1f
    move-result-object v4

    #@b20
    .line 1133
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b23
    move-result v5

    #@b24
    .line 1134
    .restart local v5       #_arg1:I
    move-object/from16 v0, p0

    #@b26
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->runSetPortForwardRule(Ljava/lang/String;I)V

    #@b29
    .line 1135
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b2c
    .line 1136
    const/4 v3, 0x1

    #@b2d
    goto/16 :goto_7

    #@b2f
    .line 1140
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    :sswitch_b2f
    const-string v3, "android.os.INetworkManagementService"

    #@b31
    move-object/from16 v0, p2

    #@b33
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b36
    .line 1142
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b39
    move-result-object v4

    #@b3a
    .line 1143
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@b3c
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getAssociatedIpHostnameWithMac(Ljava/lang/String;)[Ljava/lang/String;

    #@b3f
    move-result-object v28

    #@b40
    .line 1144
    .local v28, _result:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b43
    .line 1145
    move-object/from16 v0, p3

    #@b45
    move-object/from16 v1, v28

    #@b47
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@b4a
    .line 1146
    const/4 v3, 0x1

    #@b4b
    goto/16 :goto_7

    #@b4d
    .line 1150
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:[Ljava/lang/String;
    :sswitch_b4d
    const-string v3, "android.os.INetworkManagementService"

    #@b4f
    move-object/from16 v0, p2

    #@b51
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b54
    .line 1152
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b57
    move-result-object v4

    #@b58
    .line 1154
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b5b
    move-result v5

    #@b5c
    .line 1156
    .restart local v5       #_arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b5f
    move-result v6

    #@b60
    .line 1158
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b63
    move-result-object v7

    #@b64
    .line 1160
    .local v7, _arg3:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b67
    move-result-object v8

    #@b68
    .line 1162
    .restart local v8       #_arg4:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b6b
    move-result-object v9

    #@b6c
    .line 1164
    .local v9, _arg5:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b6f
    move-result-object v10

    #@b70
    .line 1166
    .local v10, _arg6:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b73
    move-result-object v11

    #@b74
    .line 1168
    .local v11, _arg7:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b77
    move-result v3

    #@b78
    if-eqz v3, :cond_bb2

    #@b7a
    const/4 v12, 0x1

    #@b7b
    .line 1170
    .local v12, _arg8:Z
    :goto_b7b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b7e
    move-result v13

    #@b7f
    .line 1172
    .local v13, _arg9:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b82
    move-result v14

    #@b83
    .line 1174
    .local v14, _arg10:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b86
    move-result-object v15

    #@b87
    .line 1176
    .local v15, _arg11:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b8a
    move-result-object v16

    #@b8b
    .line 1178
    .local v16, _arg12:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b8e
    move-result v17

    #@b8f
    .line 1180
    .local v17, _arg13:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b92
    move-result v18

    #@b93
    .line 1182
    .local v18, _arg14:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b96
    move-result v19

    #@b97
    .line 1184
    .local v19, _arg15:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b9a
    move-result-object v20

    #@b9b
    .line 1186
    .local v20, _arg16:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b9e
    move-result-object v21

    #@b9f
    .line 1188
    .local v21, _arg17:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ba2
    move-result-object v22

    #@ba3
    .line 1190
    .local v22, _arg18:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ba6
    move-result-object v23

    #@ba7
    .local v23, _arg19:Ljava/lang/String;
    move-object/from16 v3, p0

    #@ba9
    .line 1191
    invoke-virtual/range {v3 .. v23}, Landroid/os/INetworkManagementService$Stub;->startVZWAccessPoint(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@bac
    .line 1192
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@baf
    .line 1193
    const/4 v3, 0x1

    #@bb0
    goto/16 :goto_7

    #@bb2
    .line 1168
    .end local v12           #_arg8:Z
    .end local v13           #_arg9:I
    .end local v14           #_arg10:I
    .end local v15           #_arg11:Ljava/lang/String;
    .end local v16           #_arg12:Ljava/lang/String;
    .end local v17           #_arg13:I
    .end local v18           #_arg14:I
    .end local v19           #_arg15:I
    .end local v20           #_arg16:Ljava/lang/String;
    .end local v21           #_arg17:Ljava/lang/String;
    .end local v22           #_arg18:Ljava/lang/String;
    .end local v23           #_arg19:Ljava/lang/String;
    :cond_bb2
    const/4 v12, 0x0

    #@bb3
    goto :goto_b7b

    #@bb4
    .line 1197
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v5           #_arg1:I
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Ljava/lang/String;
    .end local v8           #_arg4:Ljava/lang/String;
    .end local v9           #_arg5:Ljava/lang/String;
    .end local v10           #_arg6:Ljava/lang/String;
    .end local v11           #_arg7:Ljava/lang/String;
    :sswitch_bb4
    const-string v3, "android.os.INetworkManagementService"

    #@bb6
    move-object/from16 v0, p2

    #@bb8
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bbb
    .line 1199
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bbe
    move-result-object v4

    #@bbf
    .line 1200
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@bc1
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->stopVZWAccessPoint(Ljava/lang/String;)V

    #@bc4
    .line 1201
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bc7
    .line 1202
    const/4 v3, 0x1

    #@bc8
    goto/16 :goto_7

    #@bca
    .line 1206
    .end local v4           #_arg0:Ljava/lang/String;
    :sswitch_bca
    const-string v3, "android.os.INetworkManagementService"

    #@bcc
    move-object/from16 v0, p2

    #@bce
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bd1
    .line 1208
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bd4
    move-result-object v4

    #@bd5
    .line 1209
    .restart local v4       #_arg0:Ljava/lang/String;
    move-object/from16 v0, p0

    #@bd7
    invoke-virtual {v0, v4}, Landroid/os/INetworkManagementService$Stub;->getAssociatedIpWithMac(Ljava/lang/String;)Ljava/lang/String;

    #@bda
    move-result-object v28

    #@bdb
    .line 1210
    .local v28, _result:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bde
    .line 1211
    move-object/from16 v0, p3

    #@be0
    move-object/from16 v1, v28

    #@be2
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@be5
    .line 1212
    const/4 v3, 0x1

    #@be6
    goto/16 :goto_7

    #@be8
    .line 1216
    .end local v4           #_arg0:Ljava/lang/String;
    .end local v28           #_result:Ljava/lang/String;
    :sswitch_be8
    const-string v3, "android.os.INetworkManagementService"

    #@bea
    move-object/from16 v0, p2

    #@bec
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bef
    .line 1218
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bf2
    move-result-object v4

    #@bf3
    .line 1220
    .restart local v4       #_arg0:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@bf6
    move-result-object v5

    #@bf7
    .line 1221
    .local v5, _arg1:Ljava/lang/String;
    move-object/from16 v0, p0

    #@bf9
    invoke-virtual {v0, v4, v5}, Landroid/os/INetworkManagementService$Stub;->setIpv6AcceptRaDefrtr(Ljava/lang/String;Ljava/lang/String;)V

    #@bfc
    .line 1222
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@bff
    .line 1223
    const/4 v3, 0x1

    #@c00
    goto/16 :goto_7

    #@c02
    .line 42
    :sswitch_data_c02
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2a
        0x3 -> :sswitch_43
        0x4 -> :sswitch_5c
        0x5 -> :sswitch_75
        0x6 -> :sswitch_8d
        0x7 -> :sswitch_bb
        0x8 -> :sswitch_e3
        0x9 -> :sswitch_f9
        0xa -> :sswitch_10f
        0xb -> :sswitch_125
        0xc -> :sswitch_144
        0xd -> :sswitch_15a
        0xe -> :sswitch_170
        0xf -> :sswitch_18f
        0x10 -> :sswitch_1b7
        0x11 -> :sswitch_1df
        0x12 -> :sswitch_207
        0x13 -> :sswitch_22f
        0x14 -> :sswitch_23f
        0x15 -> :sswitch_25a
        0x16 -> :sswitch_275
        0x17 -> :sswitch_28b
        0x18 -> :sswitch_29b
        0x19 -> :sswitch_2b6
        0x1a -> :sswitch_2cc
        0x1b -> :sswitch_2dc
        0x1c -> :sswitch_2f2
        0x1d -> :sswitch_308
        0x1e -> :sswitch_320
        0x1f -> :sswitch_336
        0x20 -> :sswitch_34e
        0x21 -> :sswitch_36c
        0x22 -> :sswitch_38a
        0x23 -> :sswitch_3a4
        0x24 -> :sswitch_3be
        0x25 -> :sswitch_3d4
        0x26 -> :sswitch_3ea
        0x27 -> :sswitch_402
        0x28 -> :sswitch_428
        0x29 -> :sswitch_43e
        0x2a -> :sswitch_458
        0x2b -> :sswitch_480
        0x2c -> :sswitch_496
        0x2d -> :sswitch_4be
        0x2e -> :sswitch_4e6
        0x2f -> :sswitch_50e
        0x30 -> :sswitch_536
        0x31 -> :sswitch_564
        0x32 -> :sswitch_58a
        0x33 -> :sswitch_5b8
        0x34 -> :sswitch_5d4
        0x35 -> :sswitch_5ea
        0x36 -> :sswitch_606
        0x37 -> :sswitch_61c
        0x38 -> :sswitch_634
        0x39 -> :sswitch_653
        0x3a -> :sswitch_66e
        0x3b -> :sswitch_68c
        0x3c -> :sswitch_6aa
        0x3d -> :sswitch_6c8
        0x3e -> :sswitch_6e6
        0x3f -> :sswitch_6fc
        0x40 -> :sswitch_712
        0x41 -> :sswitch_72c
        0x42 -> :sswitch_73c
        0x43 -> :sswitch_752
        0x44 -> :sswitch_768
        0x45 -> :sswitch_782
        0x46 -> :sswitch_79d
        0x47 -> :sswitch_7b8
        0x48 -> :sswitch_7d7
        0x49 -> :sswitch_7f6
        0x4a -> :sswitch_819
        0x4b -> :sswitch_838
        0x4c -> :sswitch_853
        0x4d -> :sswitch_88a
        0x4e -> :sswitch_8b7
        0x4f -> :sswitch_8dc
        0x50 -> :sswitch_8fa
        0x51 -> :sswitch_912
        0x52 -> :sswitch_92a
        0x53 -> :sswitch_944
        0x54 -> :sswitch_95a
        0x55 -> :sswitch_970
        0x56 -> :sswitch_980
        0x57 -> :sswitch_99b
        0x58 -> :sswitch_9b1
        0x59 -> :sswitch_9c1
        0x5a -> :sswitch_9d7
        0x5b -> :sswitch_9ed
        0x5c -> :sswitch_a0b
        0x5d -> :sswitch_a29
        0x5e -> :sswitch_a43
        0x5f -> :sswitch_a59
        0x60 -> :sswitch_a69
        0x61 -> :sswitch_a79
        0x62 -> :sswitch_a89
        0x63 -> :sswitch_a9f
        0x64 -> :sswitch_ab5
        0x65 -> :sswitch_ac5
        0x66 -> :sswitch_adb
        0x67 -> :sswitch_aeb
        0x68 -> :sswitch_b05
        0x69 -> :sswitch_b15
        0x6a -> :sswitch_b2f
        0x6b -> :sswitch_b4d
        0x6c -> :sswitch_bb4
        0x6d -> :sswitch_bca
        0x6e -> :sswitch_be8
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
