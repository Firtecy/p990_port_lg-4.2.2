.class final Landroid/os/Handler$BlockingRunnable;
.super Ljava/lang/Object;
.source "Handler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/Handler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BlockingRunnable"
.end annotation


# instance fields
.field private mDone:Z

.field private final mTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "task"

    #@0
    .prologue
    .line 738
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 739
    iput-object p1, p0, Landroid/os/Handler$BlockingRunnable;->mTask:Ljava/lang/Runnable;

    #@5
    .line 740
    return-void
.end method


# virtual methods
.method public postAndWait(Landroid/os/Handler;J)Z
    .registers 13
    .parameter "handler"
    .parameter "timeout"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v4, 0x0

    #@3
    .line 755
    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@6
    move-result v5

    #@7
    if-nez v5, :cond_a

    #@9
    .line 781
    :goto_9
    return v4

    #@a
    .line 759
    :cond_a
    monitor-enter p0

    #@b
    .line 760
    cmp-long v5, p2, v7

    #@d
    if-lez v5, :cond_2e

    #@f
    .line 761
    :try_start_f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@12
    move-result-wide v5

    #@13
    add-long v2, v5, p2

    #@15
    .line 762
    .local v2, expirationTime:J
    :goto_15
    iget-boolean v5, p0, Landroid/os/Handler$BlockingRunnable;->mDone:Z

    #@17
    if-nez v5, :cond_38

    #@19
    .line 763
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1c
    move-result-wide v5

    #@1d
    sub-long v0, v2, v5

    #@1f
    .line 764
    .local v0, delay:J
    cmp-long v5, v0, v7

    #@21
    if-gtz v5, :cond_28

    #@23
    .line 765
    monitor-exit p0

    #@24
    goto :goto_9

    #@25
    .line 780
    .end local v0           #delay:J
    .end local v2           #expirationTime:J
    :catchall_25
    move-exception v4

    #@26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_f .. :try_end_27} :catchall_25

    #@27
    throw v4

    #@28
    .line 768
    .restart local v0       #delay:J
    .restart local v2       #expirationTime:J
    :cond_28
    :try_start_28
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_2b
    .catchall {:try_start_28 .. :try_end_2b} :catchall_25
    .catch Ljava/lang/InterruptedException; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_15

    #@2c
    .line 769
    :catch_2c
    move-exception v5

    #@2d
    goto :goto_15

    #@2e
    .line 773
    .end local v0           #delay:J
    .end local v2           #expirationTime:J
    :cond_2e
    :goto_2e
    :try_start_2e
    iget-boolean v4, p0, Landroid/os/Handler$BlockingRunnable;->mDone:Z
    :try_end_30
    .catchall {:try_start_2e .. :try_end_30} :catchall_25

    #@30
    if-nez v4, :cond_38

    #@32
    .line 775
    :try_start_32
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_35
    .catchall {:try_start_32 .. :try_end_35} :catchall_25
    .catch Ljava/lang/InterruptedException; {:try_start_32 .. :try_end_35} :catch_36

    #@35
    goto :goto_2e

    #@36
    .line 776
    :catch_36
    move-exception v4

    #@37
    goto :goto_2e

    #@38
    .line 780
    :cond_38
    :try_start_38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_25

    #@39
    .line 781
    const/4 v4, 0x1

    #@3a
    goto :goto_9
.end method

.method public run()V
    .registers 3

    #@0
    .prologue
    .line 745
    :try_start_0
    iget-object v0, p0, Landroid/os/Handler$BlockingRunnable;->mTask:Ljava/lang/Runnable;

    #@2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_11

    #@5
    .line 747
    monitor-enter p0

    #@6
    .line 748
    const/4 v0, 0x1

    #@7
    :try_start_7
    iput-boolean v0, p0, Landroid/os/Handler$BlockingRunnable;->mDone:Z

    #@9
    .line 749
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@c
    .line 750
    monitor-exit p0

    #@d
    .line 752
    return-void

    #@e
    .line 750
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_e

    #@10
    throw v0

    #@11
    .line 747
    :catchall_11
    move-exception v0

    #@12
    monitor-enter p0

    #@13
    .line 748
    const/4 v1, 0x1

    #@14
    :try_start_14
    iput-boolean v1, p0, Landroid/os/Handler$BlockingRunnable;->mDone:Z

    #@16
    .line 749
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@19
    .line 750
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_14 .. :try_end_1a} :catchall_1b

    #@1a
    throw v0

    #@1b
    :catchall_1b
    move-exception v0

    #@1c
    :try_start_1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    #@1d
    throw v0
.end method
