.class public Landroid/os/RecoverySystem;
.super Ljava/lang/Object;
.source "RecoverySystem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/RecoverySystem$ProgressListener;
    }
.end annotation


# static fields
.field private static COMMAND_FILE:Ljava/io/File; = null

.field private static final DEFAULT_KEYSTORE:Ljava/io/File; = null

.field private static LAST_PREFIX:Ljava/lang/String; = null

.field private static LOG_FILE:Ljava/io/File; = null

.field private static LOG_FILE_MAX_LENGTH:I = 0x0

.field private static final PUBLISH_PROGRESS_INTERVAL_MS:J = 0x1f4L

.field private static RECOVERY_DIR:Ljava/io/File; = null

.field private static final TAG:Ljava/lang/String; = "RecoverySystem"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 68
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "/system/etc/security/otacerts.zip"

    #@4
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Landroid/os/RecoverySystem;->DEFAULT_KEYSTORE:Ljava/io/File;

    #@9
    .line 75
    new-instance v0, Ljava/io/File;

    #@b
    const-string v1, "/cache/recovery"

    #@d
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    sput-object v0, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@12
    .line 76
    new-instance v0, Ljava/io/File;

    #@14
    sget-object v1, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@16
    const-string v2, "command"

    #@18
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1b
    sput-object v0, Landroid/os/RecoverySystem;->COMMAND_FILE:Ljava/io/File;

    #@1d
    .line 77
    new-instance v0, Ljava/io/File;

    #@1f
    sget-object v1, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@21
    const-string/jumbo v2, "log"

    #@24
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@27
    sput-object v0, Landroid/os/RecoverySystem;->LOG_FILE:Ljava/io/File;

    #@29
    .line 78
    const-string/jumbo v0, "last_"

    #@2c
    sput-object v0, Landroid/os/RecoverySystem;->LAST_PREFIX:Ljava/lang/String;

    #@2e
    .line 81
    const/high16 v0, 0x1

    #@30
    sput v0, Landroid/os/RecoverySystem;->LOG_FILE_MAX_LENGTH:I

    #@32
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 87
    return-void
.end method

.method private RecoverySystem()V
    .registers 1

    #@0
    .prologue
    .line 463
    return-void
.end method

.method private static bootCommand(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "arg"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 405
    sget-object v2, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@2
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    #@5
    .line 406
    sget-object v2, Landroid/os/RecoverySystem;->COMMAND_FILE:Ljava/io/File;

    #@7
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@a
    .line 407
    sget-object v2, Landroid/os/RecoverySystem;->LOG_FILE:Ljava/io/File;

    #@c
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    #@f
    .line 409
    new-instance v0, Ljava/io/FileWriter;

    #@11
    sget-object v2, Landroid/os/RecoverySystem;->COMMAND_FILE:Ljava/io/File;

    #@13
    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    #@16
    .line 411
    .local v0, command:Ljava/io/FileWriter;
    :try_start_16
    invoke-virtual {v0, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    #@19
    .line 412
    const-string v2, "\n"

    #@1b
    invoke-virtual {v0, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1e
    .catchall {:try_start_16 .. :try_end_1e} :catchall_38

    #@1e
    .line 414
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    #@21
    .line 418
    const-string/jumbo v2, "power"

    #@24
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Landroid/os/PowerManager;

    #@2a
    .line 419
    .local v1, pm:Landroid/os/PowerManager;
    const-string/jumbo v2, "recovery"

    #@2d
    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    #@30
    .line 421
    new-instance v2, Ljava/io/IOException;

    #@32
    const-string v3, "Reboot failed (no permissions?)"

    #@34
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@37
    throw v2

    #@38
    .line 414
    .end local v1           #pm:Landroid/os/PowerManager;
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    #@3c
    throw v2
.end method

.method private static getTrustedCerts(Ljava/io/File;)Ljava/util/HashSet;
    .registers 8
    .parameter "keystore"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/security/cert/Certificate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 101
    new-instance v4, Ljava/util/HashSet;

    #@2
    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 102
    .local v4, trusted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/security/cert/Certificate;>;"
    if-nez p0, :cond_9

    #@7
    .line 103
    sget-object p0, Landroid/os/RecoverySystem;->DEFAULT_KEYSTORE:Ljava/io/File;

    #@9
    .line 105
    :cond_9
    new-instance v5, Ljava/util/zip/ZipFile;

    #@b
    invoke-direct {v5, p0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    #@e
    .line 107
    .local v5, zip:Ljava/util/zip/ZipFile;
    :try_start_e
    const-string v6, "X.509"

    #@10
    invoke-static {v6}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    #@13
    move-result-object v0

    #@14
    .line 108
    .local v0, cf:Ljava/security/cert/CertificateFactory;
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    #@17
    move-result-object v1

    #@18
    .line 109
    .local v1, entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    :goto_18
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_3d

    #@1e
    .line 110
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    check-cast v2, Ljava/util/zip/ZipEntry;

    #@24
    .line 111
    .local v2, entry:Ljava/util/zip/ZipEntry;
    invoke-virtual {v5, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_27
    .catchall {:try_start_e .. :try_end_27} :catchall_33

    #@27
    move-result-object v3

    #@28
    .line 113
    .local v3, is:Ljava/io/InputStream;
    :try_start_28
    invoke-virtual {v0, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    #@2b
    move-result-object v6

    #@2c
    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_2f
    .catchall {:try_start_28 .. :try_end_2f} :catchall_38

    #@2f
    .line 115
    :try_start_2f
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_33

    #@32
    goto :goto_18

    #@33
    .line 119
    .end local v0           #cf:Ljava/security/cert/CertificateFactory;
    .end local v1           #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .end local v2           #entry:Ljava/util/zip/ZipEntry;
    .end local v3           #is:Ljava/io/InputStream;
    :catchall_33
    move-exception v6

    #@34
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V

    #@37
    throw v6

    #@38
    .line 115
    .restart local v0       #cf:Ljava/security/cert/CertificateFactory;
    .restart local v1       #entries:Ljava/util/Enumeration;,"Ljava/util/Enumeration<+Ljava/util/zip/ZipEntry;>;"
    .restart local v2       #entry:Ljava/util/zip/ZipEntry;
    .restart local v3       #is:Ljava/io/InputStream;
    :catchall_38
    move-exception v6

    #@39
    :try_start_39
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    #@3c
    throw v6
    :try_end_3d
    .catchall {:try_start_39 .. :try_end_3d} :catchall_33

    #@3d
    .line 119
    .end local v2           #entry:Ljava/util/zip/ZipEntry;
    .end local v3           #is:Ljava/io/InputStream;
    :cond_3d
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V

    #@40
    .line 121
    return-object v4
.end method

.method public static handleAftermath()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 432
    const/4 v3, 0x0

    #@1
    .line 434
    .local v3, log:Ljava/lang/String;
    :try_start_1
    sget-object v5, Landroid/os/RecoverySystem;->LOG_FILE:Ljava/io/File;

    #@3
    sget v6, Landroid/os/RecoverySystem;->LOG_FILE_MAX_LENGTH:I

    #@5
    neg-int v6, v6

    #@6
    const-string v7, "...\n"

    #@8
    invoke-static {v5, v6, v7}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_b} :catch_30
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_b} :catch_39

    #@b
    move-result-object v3

    #@c
    .line 443
    :goto_c
    sget-object v5, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@e
    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 444
    .local v4, names:[Ljava/lang/String;
    const/4 v2, 0x0

    #@13
    .local v2, i:I
    :goto_13
    if-eqz v4, :cond_83

    #@15
    array-length v5, v4

    #@16
    if-ge v2, v5, :cond_83

    #@18
    .line 445
    aget-object v5, v4, v2

    #@1a
    sget-object v6, Landroid/os/RecoverySystem;->LAST_PREFIX:Ljava/lang/String;

    #@1c
    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1f
    move-result v5

    #@20
    if-nez v5, :cond_2d

    #@22
    aget-object v5, v4, v2

    #@24
    const-string/jumbo v6, "recovery.img"

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_42

    #@2d
    .line 444
    :cond_2d
    :goto_2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_13

    #@30
    .line 435
    .end local v2           #i:I
    .end local v4           #names:[Ljava/lang/String;
    :catch_30
    move-exception v0

    #@31
    .line 436
    .local v0, e:Ljava/io/FileNotFoundException;
    const-string v5, "RecoverySystem"

    #@33
    const-string v6, "No recovery log file"

    #@35
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_c

    #@39
    .line 437
    .end local v0           #e:Ljava/io/FileNotFoundException;
    :catch_39
    move-exception v0

    #@3a
    .line 438
    .local v0, e:Ljava/io/IOException;
    const-string v5, "RecoverySystem"

    #@3c
    const-string v6, "Error reading recovery log"

    #@3e
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    goto :goto_c

    #@42
    .line 452
    .end local v0           #e:Ljava/io/IOException;
    .restart local v2       #i:I
    .restart local v4       #names:[Ljava/lang/String;
    :cond_42
    new-instance v1, Ljava/io/File;

    #@44
    sget-object v5, Landroid/os/RecoverySystem;->RECOVERY_DIR:Ljava/io/File;

    #@46
    aget-object v6, v4, v2

    #@48
    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@4b
    .line 453
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@4e
    move-result v5

    #@4f
    if-nez v5, :cond_6a

    #@51
    .line 454
    const-string v5, "RecoverySystem"

    #@53
    new-instance v6, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v7, "Can\'t delete: "

    #@5a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v6

    #@5e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v6

    #@66
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_2d

    #@6a
    .line 456
    :cond_6a
    const-string v5, "RecoverySystem"

    #@6c
    new-instance v6, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v7, "Deleted: "

    #@73
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v6

    #@7b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v6

    #@7f
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_2d

    #@83
    .line 460
    .end local v1           #f:Ljava/io/File;
    :cond_83
    return-object v3
.end method

.method public static installPackage(Landroid/content/Context;Ljava/io/File;)V
    .registers 8
    .parameter "context"
    .parameter "packageFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 331
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 335
    .local v1, filename:Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    #@6
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@9
    .line 336
    .local v2, newIntent:Landroid/content/Intent;
    const-string v3, "com.lge.intent.action.GOTA_START"

    #@b
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@e
    .line 337
    const-string v3, "PACKAGE_FILENAME"

    #@10
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13
    .line 338
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@16
    .line 341
    const-string v3, "RecoverySystem"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "!!! REBOOTING TO INSTALL "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    const-string v5, " !!!"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "--update_package="

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    const-string v4, "\n--locale="

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    .line 344
    .local v0, arg:Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/os/RecoverySystem;->bootCommand(Landroid/content/Context;Ljava/lang/String;)V

    #@5c
    .line 345
    return-void
.end method

.method public static rebootWipeCache(Landroid/content/Context;)V
    .registers 3
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "--wipe_cache\n--locale="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {p0, v0}, Landroid/os/RecoverySystem;->bootCommand(Landroid/content/Context;Ljava/lang/String;)V

    #@1e
    .line 397
    return-void
.end method

.method public static rebootWipeUserData(Landroid/content/Context;)V
    .registers 11
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 361
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4
    move-result-object v0

    #@5
    if-eqz v0, :cond_1b

    #@7
    .line 362
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v0

    #@b
    const-string v2, "LGMDMWiperAdapter"

    #@d
    invoke-interface {v0, v5, v2}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_14

    #@13
    .line 389
    :goto_13
    return-void

    #@14
    .line 367
    :cond_14
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@17
    move-result-object v0

    #@18
    invoke-interface {v0}, Lcom/lge/cappuccino/IMdm;->deleteHardwareFactoryResetFlagFile()V

    #@1b
    .line 370
    :cond_1b
    new-instance v9, Landroid/os/ConditionVariable;

    #@1d
    invoke-direct {v9}, Landroid/os/ConditionVariable;-><init>()V

    #@20
    .line 372
    .local v9, condition:Landroid/os/ConditionVariable;
    new-instance v1, Landroid/content/Intent;

    #@22
    const-string v0, "android.intent.action.MASTER_CLEAR_NOTIFICATION"

    #@24
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@27
    .line 373
    .local v1, intent:Landroid/content/Intent;
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@29
    const-string v3, "android.permission.MASTER_CLEAR"

    #@2b
    new-instance v4, Landroid/os/RecoverySystem$1;

    #@2d
    invoke-direct {v4, v9}, Landroid/os/RecoverySystem$1;-><init>(Landroid/os/ConditionVariable;)V

    #@30
    const/4 v6, 0x0

    #@31
    move-object v0, p0

    #@32
    move-object v7, v5

    #@33
    move-object v8, v5

    #@34
    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@37
    .line 382
    const-string/jumbo v0, "sys.factory"

    #@3a
    const-string v2, "1"

    #@3c
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    .line 386
    invoke-virtual {v9}, Landroid/os/ConditionVariable;->block()V

    #@42
    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v2, "--wipe_data\n--locale="

    #@49
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v0

    #@5d
    invoke-static {p0, v0}, Landroid/os/RecoverySystem;->bootCommand(Landroid/content/Context;Ljava/lang/String;)V

    #@60
    goto :goto_13
.end method

.method public static verifyPackage(Ljava/io/File;Landroid/os/RecoverySystem$ProgressListener;Ljava/io/File;)V
    .registers 49
    .parameter "packageFile"
    .parameter "listener"
    .parameter "deviceCertsZipFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    #@0
    .prologue
    .line 155
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->length()J

    #@3
    move-result-wide v13

    #@4
    .line 157
    .local v13, fileLen:J
    new-instance v27, Ljava/io/RandomAccessFile;

    #@6
    const-string/jumbo v42, "r"

    #@9
    move-object/from16 v0, v27

    #@b
    move-object/from16 v1, p0

    #@d
    move-object/from16 v2, v42

    #@f
    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@12
    .line 159
    .local v27, raf:Ljava/io/RandomAccessFile;
    const/16 v21, 0x0

    #@14
    .line 160
    .local v21, lastPercent:I
    :try_start_14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@17
    move-result-wide v22

    #@18
    .line 161
    .local v22, lastPublishTime:J
    if-eqz p1, :cond_21

    #@1a
    .line 162
    move-object/from16 v0, p1

    #@1c
    move/from16 v1, v21

    #@1e
    invoke-interface {v0, v1}, Landroid/os/RecoverySystem$ProgressListener;->onProgress(I)V

    #@21
    .line 165
    :cond_21
    const-wide/16 v42, 0x6

    #@23
    sub-long v42, v13, v42

    #@25
    move-object/from16 v0, v27

    #@27
    move-wide/from16 v1, v42

    #@29
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    #@2c
    .line 166
    const/16 v42, 0x6

    #@2e
    move/from16 v0, v42

    #@30
    new-array v15, v0, [B

    #@32
    .line 167
    .local v15, footer:[B
    move-object/from16 v0, v27

    #@34
    invoke-virtual {v0, v15}, Ljava/io/RandomAccessFile;->readFully([B)V

    #@37
    .line 169
    const/16 v42, 0x2

    #@39
    aget-byte v42, v15, v42

    #@3b
    const/16 v43, -0x1

    #@3d
    move/from16 v0, v42

    #@3f
    move/from16 v1, v43

    #@41
    if-ne v0, v1, :cond_4f

    #@43
    const/16 v42, 0x3

    #@45
    aget-byte v42, v15, v42

    #@47
    const/16 v43, -0x1

    #@49
    move/from16 v0, v42

    #@4b
    move/from16 v1, v43

    #@4d
    if-eq v0, v1, :cond_5d

    #@4f
    .line 170
    :cond_4f
    new-instance v42, Ljava/security/SignatureException;

    #@51
    const-string/jumbo v43, "no signature in file (no footer)"

    #@54
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@57
    throw v42
    :try_end_58
    .catchall {:try_start_14 .. :try_end_58} :catchall_58

    #@58
    .line 311
    .end local v15           #footer:[B
    .end local v22           #lastPublishTime:J
    .end local p2
    :catchall_58
    move-exception v42

    #@59
    invoke-virtual/range {v27 .. v27}, Ljava/io/RandomAccessFile;->close()V

    #@5c
    throw v42

    #@5d
    .line 173
    .restart local v15       #footer:[B
    .restart local v22       #lastPublishTime:J
    .restart local p2
    :cond_5d
    const/16 v42, 0x4

    #@5f
    :try_start_5f
    aget-byte v42, v15, v42

    #@61
    move/from16 v0, v42

    #@63
    and-int/lit16 v0, v0, 0xff

    #@65
    move/from16 v42, v0

    #@67
    const/16 v43, 0x5

    #@69
    aget-byte v43, v15, v43

    #@6b
    move/from16 v0, v43

    #@6d
    and-int/lit16 v0, v0, 0xff

    #@6f
    move/from16 v43, v0

    #@71
    shl-int/lit8 v43, v43, 0x8

    #@73
    or-int v8, v42, v43

    #@75
    .line 174
    .local v8, commentSize:I
    const/16 v42, 0x0

    #@77
    aget-byte v42, v15, v42

    #@79
    move/from16 v0, v42

    #@7b
    and-int/lit16 v0, v0, 0xff

    #@7d
    move/from16 v42, v0

    #@7f
    const/16 v43, 0x1

    #@81
    aget-byte v43, v15, v43

    #@83
    move/from16 v0, v43

    #@85
    and-int/lit16 v0, v0, 0xff

    #@87
    move/from16 v43, v0

    #@89
    shl-int/lit8 v43, v43, 0x8

    #@8b
    or-int v33, v42, v43

    #@8d
    .line 176
    .local v33, signatureStart:I
    add-int/lit8 v42, v8, 0x16

    #@8f
    move/from16 v0, v42

    #@91
    new-array v12, v0, [B

    #@93
    .line 177
    .local v12, eocd:[B
    add-int/lit8 v42, v8, 0x16

    #@95
    move/from16 v0, v42

    #@97
    int-to-long v0, v0

    #@98
    move-wide/from16 v42, v0

    #@9a
    sub-long v42, v13, v42

    #@9c
    move-object/from16 v0, v27

    #@9e
    move-wide/from16 v1, v42

    #@a0
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    #@a3
    .line 178
    move-object/from16 v0, v27

    #@a5
    invoke-virtual {v0, v12}, Ljava/io/RandomAccessFile;->readFully([B)V

    #@a8
    .line 182
    const/16 v42, 0x0

    #@aa
    aget-byte v42, v12, v42

    #@ac
    const/16 v43, 0x50

    #@ae
    move/from16 v0, v42

    #@b0
    move/from16 v1, v43

    #@b2
    if-ne v0, v1, :cond_d8

    #@b4
    const/16 v42, 0x1

    #@b6
    aget-byte v42, v12, v42

    #@b8
    const/16 v43, 0x4b

    #@ba
    move/from16 v0, v42

    #@bc
    move/from16 v1, v43

    #@be
    if-ne v0, v1, :cond_d8

    #@c0
    const/16 v42, 0x2

    #@c2
    aget-byte v42, v12, v42

    #@c4
    const/16 v43, 0x5

    #@c6
    move/from16 v0, v42

    #@c8
    move/from16 v1, v43

    #@ca
    if-ne v0, v1, :cond_d8

    #@cc
    const/16 v42, 0x3

    #@ce
    aget-byte v42, v12, v42

    #@d0
    const/16 v43, 0x6

    #@d2
    move/from16 v0, v42

    #@d4
    move/from16 v1, v43

    #@d6
    if-eq v0, v1, :cond_e1

    #@d8
    .line 184
    :cond_d8
    new-instance v42, Ljava/security/SignatureException;

    #@da
    const-string/jumbo v43, "no signature in file (bad footer)"

    #@dd
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@e0
    throw v42

    #@e1
    .line 187
    :cond_e1
    const/16 v16, 0x4

    #@e3
    .local v16, i:I
    :goto_e3
    array-length v0, v12

    #@e4
    move/from16 v42, v0

    #@e6
    add-int/lit8 v42, v42, -0x3

    #@e8
    move/from16 v0, v16

    #@ea
    move/from16 v1, v42

    #@ec
    if-ge v0, v1, :cond_127

    #@ee
    .line 188
    aget-byte v42, v12, v16

    #@f0
    const/16 v43, 0x50

    #@f2
    move/from16 v0, v42

    #@f4
    move/from16 v1, v43

    #@f6
    if-ne v0, v1, :cond_124

    #@f8
    add-int/lit8 v42, v16, 0x1

    #@fa
    aget-byte v42, v12, v42

    #@fc
    const/16 v43, 0x4b

    #@fe
    move/from16 v0, v42

    #@100
    move/from16 v1, v43

    #@102
    if-ne v0, v1, :cond_124

    #@104
    add-int/lit8 v42, v16, 0x2

    #@106
    aget-byte v42, v12, v42

    #@108
    const/16 v43, 0x5

    #@10a
    move/from16 v0, v42

    #@10c
    move/from16 v1, v43

    #@10e
    if-ne v0, v1, :cond_124

    #@110
    add-int/lit8 v42, v16, 0x3

    #@112
    aget-byte v42, v12, v42

    #@114
    const/16 v43, 0x6

    #@116
    move/from16 v0, v42

    #@118
    move/from16 v1, v43

    #@11a
    if-ne v0, v1, :cond_124

    #@11c
    .line 190
    new-instance v42, Ljava/security/SignatureException;

    #@11e
    const-string v43, "EOCD marker found after start of EOCD"

    #@120
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@123
    throw v42

    #@124
    .line 187
    :cond_124
    add-int/lit8 v16, v16, 0x1

    #@126
    goto :goto_e3

    #@127
    .line 200
    :cond_127
    new-instance v4, Lorg/apache/harmony/security/asn1/BerInputStream;

    #@129
    new-instance v42, Ljava/io/ByteArrayInputStream;

    #@12b
    add-int/lit8 v43, v8, 0x16

    #@12d
    sub-int v43, v43, v33

    #@12f
    move-object/from16 v0, v42

    #@131
    move/from16 v1, v43

    #@133
    move/from16 v2, v33

    #@135
    invoke-direct {v0, v12, v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    #@138
    move-object/from16 v0, v42

    #@13a
    invoke-direct {v4, v0}, Lorg/apache/harmony/security/asn1/BerInputStream;-><init>(Ljava/io/InputStream;)V

    #@13d
    .line 202
    .local v4, bis:Lorg/apache/harmony/security/asn1/BerInputStream;
    sget-object v42, Lorg/apache/harmony/security/pkcs7/ContentInfo;->ASN1:Lorg/apache/harmony/security/asn1/ASN1Sequence;

    #@13f
    move-object/from16 v0, v42

    #@141
    invoke-virtual {v0, v4}, Lorg/apache/harmony/security/asn1/ASN1Sequence;->decode(Lorg/apache/harmony/security/asn1/BerInputStream;)Ljava/lang/Object;

    #@144
    move-result-object v18

    #@145
    check-cast v18, Lorg/apache/harmony/security/pkcs7/ContentInfo;

    #@147
    .line 203
    .local v18, info:Lorg/apache/harmony/security/pkcs7/ContentInfo;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/harmony/security/pkcs7/ContentInfo;->getSignedData()Lorg/apache/harmony/security/pkcs7/SignedData;

    #@14a
    move-result-object v34

    #@14b
    .line 204
    .local v34, signedData:Lorg/apache/harmony/security/pkcs7/SignedData;
    if-nez v34, :cond_156

    #@14d
    .line 205
    new-instance v42, Ljava/io/IOException;

    #@14f
    const-string/jumbo v43, "signedData is null"

    #@152
    invoke-direct/range {v42 .. v43}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@155
    throw v42

    #@156
    .line 207
    :cond_156
    invoke-virtual/range {v34 .. v34}, Lorg/apache/harmony/security/pkcs7/SignedData;->getCertificates()Ljava/util/List;

    #@159
    move-result-object v11

    #@15a
    .line 208
    .local v11, encCerts:Ljava/util/Collection;
    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    #@15d
    move-result v42

    #@15e
    if-eqz v42, :cond_168

    #@160
    .line 209
    new-instance v42, Ljava/io/IOException;

    #@162
    const-string v43, "encCerts is empty"

    #@164
    invoke-direct/range {v42 .. v43}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@167
    throw v42

    #@168
    .line 213
    :cond_168
    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@16b
    move-result-object v20

    #@16c
    .line 214
    .local v20, it:Ljava/util/Iterator;
    const/4 v7, 0x0

    #@16d
    .line 215
    .local v7, cert:Ljava/security/cert/X509Certificate;
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    #@170
    move-result v42

    #@171
    if-eqz v42, :cond_1cf

    #@173
    .line 216
    new-instance v7, Lorg/apache/harmony/security/provider/cert/X509CertImpl;

    #@175
    .end local v7           #cert:Ljava/security/cert/X509Certificate;
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@178
    move-result-object v42

    #@179
    check-cast v42, Lorg/apache/harmony/security/x509/Certificate;

    #@17b
    move-object/from16 v0, v42

    #@17d
    invoke-direct {v7, v0}, Lorg/apache/harmony/security/provider/cert/X509CertImpl;-><init>(Lorg/apache/harmony/security/x509/Certificate;)V

    #@180
    .line 221
    .restart local v7       #cert:Ljava/security/cert/X509Certificate;
    invoke-virtual/range {v34 .. v34}, Lorg/apache/harmony/security/pkcs7/SignedData;->getSignerInfos()Ljava/util/List;

    #@183
    move-result-object v31

    #@184
    .line 223
    .local v31, sigInfos:Ljava/util/List;
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->isEmpty()Z

    #@187
    move-result v42

    #@188
    if-nez v42, :cond_1d8

    #@18a
    .line 224
    const/16 v42, 0x0

    #@18c
    move-object/from16 v0, v31

    #@18e
    move/from16 v1, v42

    #@190
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@193
    move-result-object v30

    #@194
    check-cast v30, Lorg/apache/harmony/security/pkcs7/SignerInfo;

    #@196
    .line 232
    .local v30, sigInfo:Lorg/apache/harmony/security/pkcs7/SignerInfo;
    if-nez p2, :cond_19a

    #@198
    sget-object p2, Landroid/os/RecoverySystem;->DEFAULT_KEYSTORE:Ljava/io/File;

    #@19a
    .end local p2
    :cond_19a
    invoke-static/range {p2 .. p2}, Landroid/os/RecoverySystem;->getTrustedCerts(Ljava/io/File;)Ljava/util/HashSet;

    #@19d
    move-result-object v40

    #@19e
    .line 235
    .local v40, trusted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/security/cert/Certificate;>;"
    invoke-virtual {v7}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    #@1a1
    move-result-object v32

    #@1a2
    .line 236
    .local v32, signatureKey:Ljava/security/PublicKey;
    const/16 v41, 0x0

    #@1a4
    .line 237
    .local v41, verified:Z
    invoke-virtual/range {v40 .. v40}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    #@1a7
    move-result-object v17

    #@1a8
    .local v17, i$:Ljava/util/Iterator;
    :cond_1a8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@1ab
    move-result v42

    #@1ac
    if-eqz v42, :cond_1c4

    #@1ae
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b1
    move-result-object v6

    #@1b2
    check-cast v6, Ljava/security/cert/Certificate;

    #@1b4
    .line 238
    .local v6, c:Ljava/security/cert/Certificate;
    invoke-virtual {v6}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    #@1b7
    move-result-object v42

    #@1b8
    move-object/from16 v0, v42

    #@1ba
    move-object/from16 v1, v32

    #@1bc
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    #@1bf
    move-result v42

    #@1c0
    if-eqz v42, :cond_1a8

    #@1c2
    .line 239
    const/16 v41, 0x1

    #@1c4
    .line 243
    .end local v6           #c:Ljava/security/cert/Certificate;
    :cond_1c4
    if-nez v41, :cond_1e1

    #@1c6
    .line 244
    new-instance v42, Ljava/security/SignatureException;

    #@1c8
    const-string/jumbo v43, "signature doesn\'t match any trusted key"

    #@1cb
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@1ce
    throw v42

    #@1cf
    .line 218
    .end local v17           #i$:Ljava/util/Iterator;
    .end local v30           #sigInfo:Lorg/apache/harmony/security/pkcs7/SignerInfo;
    .end local v31           #sigInfos:Ljava/util/List;
    .end local v32           #signatureKey:Ljava/security/PublicKey;
    .end local v40           #trusted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/security/cert/Certificate;>;"
    .end local v41           #verified:Z
    .restart local p2
    :cond_1cf
    new-instance v42, Ljava/security/SignatureException;

    #@1d1
    const-string/jumbo v43, "signature contains no certificates"

    #@1d4
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@1d7
    throw v42

    #@1d8
    .line 226
    .restart local v31       #sigInfos:Ljava/util/List;
    :cond_1d8
    new-instance v42, Ljava/io/IOException;

    #@1da
    const-string/jumbo v43, "no signer infos!"

    #@1dd
    invoke-direct/range {v42 .. v43}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1e0
    throw v42

    #@1e1
    .line 257
    .end local p2
    .restart local v17       #i$:Ljava/util/Iterator;
    .restart local v30       #sigInfo:Lorg/apache/harmony/security/pkcs7/SignerInfo;
    .restart local v32       #signatureKey:Ljava/security/PublicKey;
    .restart local v40       #trusted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/security/cert/Certificate;>;"
    .restart local v41       #verified:Z
    :cond_1e1
    invoke-virtual/range {v30 .. v30}, Lorg/apache/harmony/security/pkcs7/SignerInfo;->getDigestAlgorithm()Ljava/lang/String;

    #@1e4
    move-result-object v9

    #@1e5
    .line 258
    .local v9, da:Ljava/lang/String;
    invoke-virtual/range {v30 .. v30}, Lorg/apache/harmony/security/pkcs7/SignerInfo;->getDigestEncryptionAlgorithm()Ljava/lang/String;

    #@1e8
    move-result-object v10

    #@1e9
    .line 259
    .local v10, dea:Ljava/lang/String;
    const/4 v3, 0x0

    #@1ea
    .line 260
    .local v3, alg:Ljava/lang/String;
    if-eqz v9, :cond_1ee

    #@1ec
    if-nez v10, :cond_237

    #@1ee
    .line 263
    :cond_1ee
    invoke-virtual {v7}, Ljava/security/cert/X509Certificate;->getSigAlgName()Ljava/lang/String;

    #@1f1
    move-result-object v3

    #@1f2
    .line 267
    :goto_1f2
    invoke-static {v3}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    #@1f5
    move-result-object v29

    #@1f6
    .line 268
    .local v29, sig:Ljava/security/Signature;
    move-object/from16 v0, v29

    #@1f8
    invoke-virtual {v0, v7}, Ljava/security/Signature;->initVerify(Ljava/security/cert/Certificate;)V

    #@1fb
    .line 272
    int-to-long v0, v8

    #@1fc
    move-wide/from16 v42, v0

    #@1fe
    sub-long v42, v13, v42

    #@200
    const-wide/16 v44, 0x2

    #@202
    sub-long v38, v42, v44

    #@204
    .line 273
    .local v38, toRead:J
    const-wide/16 v36, 0x0

    #@206
    .line 274
    .local v36, soFar:J
    const-wide/16 v42, 0x0

    #@208
    move-object/from16 v0, v27

    #@20a
    move-wide/from16 v1, v42

    #@20c
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    #@20f
    .line 275
    const/16 v42, 0x1000

    #@211
    move/from16 v0, v42

    #@213
    new-array v5, v0, [B

    #@215
    .line 276
    .local v5, buffer:[B
    const/16 v19, 0x0

    #@217
    .line 277
    .local v19, interrupted:Z
    :cond_217
    :goto_217
    cmp-long v42, v36, v38

    #@219
    if-gez v42, :cond_221

    #@21b
    .line 278
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    #@21e
    move-result v19

    #@21f
    .line 279
    if-eqz v19, :cond_254

    #@221
    .line 299
    :cond_221
    if-eqz p1, :cond_22c

    #@223
    .line 300
    const/16 v42, 0x64

    #@225
    move-object/from16 v0, p1

    #@227
    move/from16 v1, v42

    #@229
    invoke-interface {v0, v1}, Landroid/os/RecoverySystem$ProgressListener;->onProgress(I)V

    #@22c
    .line 303
    :cond_22c
    if-eqz v19, :cond_2b3

    #@22e
    .line 304
    new-instance v42, Ljava/security/SignatureException;

    #@230
    const-string/jumbo v43, "verification was interrupted"

    #@233
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@236
    throw v42

    #@237
    .line 265
    .end local v5           #buffer:[B
    .end local v19           #interrupted:Z
    .end local v29           #sig:Ljava/security/Signature;
    .end local v36           #soFar:J
    .end local v38           #toRead:J
    :cond_237
    new-instance v42, Ljava/lang/StringBuilder;

    #@239
    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    #@23c
    move-object/from16 v0, v42

    #@23e
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@241
    move-result-object v42

    #@242
    const-string/jumbo v43, "with"

    #@245
    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v42

    #@249
    move-object/from16 v0, v42

    #@24b
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v42

    #@24f
    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@252
    move-result-object v3

    #@253
    goto :goto_1f2

    #@254
    .line 280
    .restart local v5       #buffer:[B
    .restart local v19       #interrupted:Z
    .restart local v29       #sig:Ljava/security/Signature;
    .restart local v36       #soFar:J
    .restart local v38       #toRead:J
    :cond_254
    array-length v0, v5

    #@255
    move/from16 v35, v0

    #@257
    .line 281
    .local v35, size:I
    move/from16 v0, v35

    #@259
    int-to-long v0, v0

    #@25a
    move-wide/from16 v42, v0

    #@25c
    add-long v42, v42, v36

    #@25e
    cmp-long v42, v42, v38

    #@260
    if-lez v42, :cond_269

    #@262
    .line 282
    sub-long v42, v38, v36

    #@264
    move-wide/from16 v0, v42

    #@266
    long-to-int v0, v0

    #@267
    move/from16 v35, v0

    #@269
    .line 284
    :cond_269
    const/16 v42, 0x0

    #@26b
    move-object/from16 v0, v27

    #@26d
    move/from16 v1, v42

    #@26f
    move/from16 v2, v35

    #@271
    invoke-virtual {v0, v5, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    #@274
    move-result v28

    #@275
    .line 285
    .local v28, read:I
    const/16 v42, 0x0

    #@277
    move-object/from16 v0, v29

    #@279
    move/from16 v1, v42

    #@27b
    move/from16 v2, v28

    #@27d
    invoke-virtual {v0, v5, v1, v2}, Ljava/security/Signature;->update([BII)V

    #@280
    .line 286
    move/from16 v0, v28

    #@282
    int-to-long v0, v0

    #@283
    move-wide/from16 v42, v0

    #@285
    add-long v36, v36, v42

    #@287
    .line 288
    if-eqz p1, :cond_217

    #@289
    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@28c
    move-result-wide v24

    #@28d
    .line 290
    .local v24, now:J
    const-wide/16 v42, 0x64

    #@28f
    mul-long v42, v42, v36

    #@291
    div-long v42, v42, v38

    #@293
    move-wide/from16 v0, v42

    #@295
    long-to-int v0, v0

    #@296
    move/from16 v26, v0

    #@298
    .line 291
    .local v26, p:I
    move/from16 v0, v26

    #@29a
    move/from16 v1, v21

    #@29c
    if-le v0, v1, :cond_217

    #@29e
    sub-long v42, v24, v22

    #@2a0
    const-wide/16 v44, 0x1f4

    #@2a2
    cmp-long v42, v42, v44

    #@2a4
    if-lez v42, :cond_217

    #@2a6
    .line 293
    move/from16 v21, v26

    #@2a8
    .line 294
    move-wide/from16 v22, v24

    #@2aa
    .line 295
    move-object/from16 v0, p1

    #@2ac
    move/from16 v1, v21

    #@2ae
    invoke-interface {v0, v1}, Landroid/os/RecoverySystem$ProgressListener;->onProgress(I)V

    #@2b1
    goto/16 :goto_217

    #@2b3
    .line 307
    .end local v24           #now:J
    .end local v26           #p:I
    .end local v28           #read:I
    .end local v35           #size:I
    :cond_2b3
    invoke-virtual/range {v30 .. v30}, Lorg/apache/harmony/security/pkcs7/SignerInfo;->getEncryptedDigest()[B

    #@2b6
    move-result-object v42

    #@2b7
    move-object/from16 v0, v29

    #@2b9
    move-object/from16 v1, v42

    #@2bb
    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    #@2be
    move-result v42

    #@2bf
    if-nez v42, :cond_2ca

    #@2c1
    .line 308
    new-instance v42, Ljava/security/SignatureException;

    #@2c3
    const-string/jumbo v43, "signature digest verification failed"

    #@2c6
    invoke-direct/range {v42 .. v43}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    #@2c9
    throw v42
    :try_end_2ca
    .catchall {:try_start_5f .. :try_end_2ca} :catchall_58

    #@2ca
    .line 311
    :cond_2ca
    invoke-virtual/range {v27 .. v27}, Ljava/io/RandomAccessFile;->close()V

    #@2cd
    .line 313
    return-void
.end method
