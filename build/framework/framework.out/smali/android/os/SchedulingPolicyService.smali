.class public Landroid/os/SchedulingPolicyService;
.super Landroid/os/ISchedulingPolicyService$Stub;
.source "SchedulingPolicyService.java"


# static fields
.field private static final PRIORITY_MAX:I = 0x3

.field private static final PRIORITY_MIN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SchedulingPolicyService"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/os/ISchedulingPolicyService$Stub;-><init>()V

    #@3
    .line 39
    return-void
.end method


# virtual methods
.method public requestPriority(III)I
    .registers 10
    .parameter "pid"
    .parameter "tid"
    .parameter "prio"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v1, -0x1

    #@3
    .line 49
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@6
    move-result v3

    #@7
    const/16 v4, 0x3f5

    #@9
    if-ne v3, v4, :cond_15

    #@b
    if-lt p3, v5, :cond_15

    #@d
    if-gt p3, v2, :cond_15

    #@f
    invoke-static {p2}, Landroid/os/Process;->getThreadGroupLeader(I)I

    #@12
    move-result v3

    #@13
    if-eq v3, p1, :cond_16

    #@15
    .line 62
    :cond_15
    :goto_15
    return v1

    #@16
    .line 55
    :cond_16
    :try_start_16
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@19
    move-result v3

    #@1a
    if-ne v3, p1, :cond_1d

    #@1c
    const/4 v2, 0x4

    #@1d
    :cond_1d
    invoke-static {p2, v2}, Landroid/os/Process;->setThreadGroup(II)V

    #@20
    .line 58
    const/4 v2, 0x1

    #@21
    invoke-static {p2, v2, p3}, Landroid/os/Process;->setThreadScheduler(III)V
    :try_end_24
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_24} :catch_26

    #@24
    .line 62
    const/4 v1, 0x0

    #@25
    goto :goto_15

    #@26
    .line 59
    :catch_26
    move-exception v0

    #@27
    .line 60
    .local v0, e:Ljava/lang/RuntimeException;
    goto :goto_15
.end method
