.class public Landroid/os/Debug$MemoryInfo;
.super Ljava/lang/Object;
.source "Debug.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/Debug;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MemoryInfo"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/Debug$MemoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final NUM_OTHER_STATS:I = 0x9


# instance fields
.field public dalvikPrivateDirty:I

.field public dalvikPss:I

.field public dalvikSharedDirty:I

.field public nativePrivateDirty:I

.field public nativePss:I

.field public nativeSharedDirty:I

.field public otherPrivateDirty:I

.field public otherPss:I

.field public otherSharedDirty:I

.field private otherStats:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 223
    new-instance v0, Landroid/os/Debug$MemoryInfo$1;

    #@2
    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/Debug$MemoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 137
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 135
    const/16 v0, 0x1b

    #@5
    new-array v0, v0, [I

    #@7
    iput-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@9
    .line 138
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 232
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 135
    const/16 v0, 0x1b

    #@5
    new-array v0, v0, [I

    #@7
    iput-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@9
    .line 233
    invoke-virtual {p0, p1}, Landroid/os/Debug$MemoryInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@c
    .line 234
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/os/Debug$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 110
    invoke-direct {p0, p1}, Landroid/os/Debug$MemoryInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public static getOtherLabel(I)Ljava/lang/String;
    .registers 2
    .parameter "which"

    #@0
    .prologue
    .line 179
    packed-switch p0, :pswitch_data_22

    #@3
    .line 189
    const-string v0, "????"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 180
    :pswitch_6
    const-string v0, "Cursor"

    #@8
    goto :goto_5

    #@9
    .line 181
    :pswitch_9
    const-string v0, "Ashmem"

    #@b
    goto :goto_5

    #@c
    .line 182
    :pswitch_c
    const-string v0, "Other dev"

    #@e
    goto :goto_5

    #@f
    .line 183
    :pswitch_f
    const-string v0, ".so mmap"

    #@11
    goto :goto_5

    #@12
    .line 184
    :pswitch_12
    const-string v0, ".jar mmap"

    #@14
    goto :goto_5

    #@15
    .line 185
    :pswitch_15
    const-string v0, ".apk mmap"

    #@17
    goto :goto_5

    #@18
    .line 186
    :pswitch_18
    const-string v0, ".ttf mmap"

    #@1a
    goto :goto_5

    #@1b
    .line 187
    :pswitch_1b
    const-string v0, ".dex mmap"

    #@1d
    goto :goto_5

    #@1e
    .line 188
    :pswitch_1e
    const-string v0, "Other mmap"

    #@20
    goto :goto_5

    #@21
    .line 179
    nop

    #@22
    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 194
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getOtherPrivateDirty(I)I
    .registers 4
    .parameter "which"

    #@0
    .prologue
    .line 168
    iget-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@2
    mul-int/lit8 v1, p1, 0x3

    #@4
    add-int/lit8 v1, v1, 0x1

    #@6
    aget v0, v0, v1

    #@8
    return v0
.end method

.method public getOtherPss(I)I
    .registers 4
    .parameter "which"

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@2
    mul-int/lit8 v1, p1, 0x3

    #@4
    aget v0, v0, v1

    #@6
    return v0
.end method

.method public getOtherSharedDirty(I)I
    .registers 4
    .parameter "which"

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@2
    mul-int/lit8 v1, p1, 0x3

    #@4
    add-int/lit8 v1, v1, 0x2

    #@6
    aget v0, v0, v1

    #@8
    return v0
.end method

.method public getTotalPrivateDirty()I
    .registers 3

    #@0
    .prologue
    .line 151
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@2
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@4
    add-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@7
    add-int/2addr v0, v1

    #@8
    return v0
.end method

.method public getTotalPss()I
    .registers 3

    #@0
    .prologue
    .line 144
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@2
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@4
    add-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@7
    add-int/2addr v0, v1

    #@8
    return v0
.end method

.method public getTotalSharedDirty()I
    .registers 3

    #@0
    .prologue
    .line 158
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@2
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@4
    add-int/2addr v0, v1

    #@5
    iget v1, p0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@7
    add-int/2addr v0, v1

    #@8
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@6
    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@c
    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@12
    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@18
    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@1e
    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@24
    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@2a
    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@30
    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v0

    #@34
    iput v0, p0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@36
    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@3c
    .line 221
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 198
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 199
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 200
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 201
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 202
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 203
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 204
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 205
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 206
    iget v0, p0, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 207
    iget-object v0, p0, Landroid/os/Debug$MemoryInfo;->otherStats:[I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@32
    .line 208
    return-void
.end method
