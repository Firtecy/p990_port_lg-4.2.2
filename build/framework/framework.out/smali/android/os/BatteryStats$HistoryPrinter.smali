.class public Landroid/os/BatteryStats$HistoryPrinter;
.super Ljava/lang/Object;
.source "BatteryStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/BatteryStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HistoryPrinter"
.end annotation


# instance fields
.field oldHealth:I

.field oldPlug:I

.field oldState:I

.field oldStatus:I

.field oldTemp:I

.field oldVolt:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 2051
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 2052
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldState:I

    #@7
    .line 2053
    iput v1, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldStatus:I

    #@9
    .line 2054
    iput v1, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldHealth:I

    #@b
    .line 2055
    iput v1, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldPlug:I

    #@d
    .line 2056
    iput v1, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldTemp:I

    #@f
    .line 2057
    iput v1, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldVolt:I

    #@11
    return-void
.end method


# virtual methods
.method public printNextItem(Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;J)V
    .registers 8
    .parameter "pw"
    .parameter "rec"
    .parameter "now"

    #@0
    .prologue
    .line 2060
    const-string v0, "  "

    #@2
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    .line 2061
    iget-wide v0, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@7
    sub-long/2addr v0, p3

    #@8
    const/16 v2, 0x13

    #@a
    invoke-static {v0, v1, p1, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;I)V

    #@d
    .line 2062
    const-string v0, " "

    #@f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    .line 2063
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@14
    const/4 v1, 0x2

    #@15
    if-ne v0, v1, :cond_21

    #@17
    .line 2064
    const-string v0, " START"

    #@19
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1c
    .line 2166
    :goto_1c
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@1e
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldState:I

    #@20
    .line 2167
    return-void

    #@21
    .line 2065
    :cond_21
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@23
    const/4 v1, 0x3

    #@24
    if-ne v0, v1, :cond_2c

    #@26
    .line 2066
    const-string v0, " *OVERFLOW*"

    #@28
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2b
    goto :goto_1c

    #@2c
    .line 2068
    :cond_2c
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@2e
    const/16 v1, 0xa

    #@30
    if-ge v0, v1, :cond_d6

    #@32
    const-string v0, "00"

    #@34
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37
    .line 2070
    :cond_37
    :goto_37
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@39
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@3c
    .line 2071
    const-string v0, " "

    #@3e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@41
    .line 2072
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@43
    const/16 v1, 0x10

    #@45
    if-ge v0, v1, :cond_e3

    #@47
    const-string v0, "0000000"

    #@49
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4c
    .line 2079
    :cond_4c
    :goto_4c
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@4e
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@55
    .line 2080
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldStatus:I

    #@57
    iget-byte v1, p2, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@59
    if-eq v0, v1, :cond_6e

    #@5b
    .line 2081
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@5d
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldStatus:I

    #@5f
    .line 2082
    const-string v0, " status="

    #@61
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64
    .line 2083
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldStatus:I

    #@66
    packed-switch v0, :pswitch_data_1a2

    #@69
    .line 2100
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldStatus:I

    #@6b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@6e
    .line 2104
    :cond_6e
    :goto_6e
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldHealth:I

    #@70
    iget-byte v1, p2, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@72
    if-eq v0, v1, :cond_87

    #@74
    .line 2105
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@76
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldHealth:I

    #@78
    .line 2106
    const-string v0, " health="

    #@7a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@7d
    .line 2107
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldHealth:I

    #@7f
    packed-switch v0, :pswitch_data_1b0

    #@82
    .line 2127
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldHealth:I

    #@84
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@87
    .line 2131
    :cond_87
    :goto_87
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldPlug:I

    #@89
    iget-byte v1, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@8b
    if-eq v0, v1, :cond_a0

    #@8d
    .line 2132
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@8f
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldPlug:I

    #@91
    .line 2133
    const-string v0, " plug="

    #@93
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@96
    .line 2134
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldPlug:I

    #@98
    packed-switch v0, :pswitch_data_1c0

    #@9b
    .line 2148
    :pswitch_9b
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldPlug:I

    #@9d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@a0
    .line 2152
    :cond_a0
    :goto_a0
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldTemp:I

    #@a2
    iget-char v1, p2, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@a4
    if-eq v0, v1, :cond_b4

    #@a6
    .line 2153
    iget-char v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@a8
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldTemp:I

    #@aa
    .line 2154
    const-string v0, " temp="

    #@ac
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af
    .line 2155
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldTemp:I

    #@b1
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@b4
    .line 2157
    :cond_b4
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldVolt:I

    #@b6
    iget-char v1, p2, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@b8
    if-eq v0, v1, :cond_c8

    #@ba
    .line 2158
    iget-char v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@bc
    iput v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldVolt:I

    #@be
    .line 2159
    const-string v0, " volt="

    #@c0
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    .line 2160
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldVolt:I

    #@c5
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@c8
    .line 2162
    :cond_c8
    iget v0, p0, Landroid/os/BatteryStats$HistoryPrinter;->oldState:I

    #@ca
    iget v1, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@cc
    sget-object v2, Landroid/os/BatteryStats;->HISTORY_STATE_DESCRIPTIONS:[Landroid/os/BatteryStats$BitDescription;

    #@ce
    invoke-static {p1, v0, v1, v2}, Landroid/os/BatteryStats;->printBitDescriptions(Ljava/io/PrintWriter;II[Landroid/os/BatteryStats$BitDescription;)V

    #@d1
    .line 2164
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    #@d4
    goto/16 :goto_1c

    #@d6
    .line 2069
    :cond_d6
    iget-byte v0, p2, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@d8
    const/16 v1, 0x64

    #@da
    if-ge v0, v1, :cond_37

    #@dc
    const-string v0, "0"

    #@de
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e1
    goto/16 :goto_37

    #@e3
    .line 2073
    :cond_e3
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@e5
    const/16 v1, 0x100

    #@e7
    if-ge v0, v1, :cond_f0

    #@e9
    const-string v0, "000000"

    #@eb
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ee
    goto/16 :goto_4c

    #@f0
    .line 2074
    :cond_f0
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@f2
    const/16 v1, 0x1000

    #@f4
    if-ge v0, v1, :cond_fd

    #@f6
    const-string v0, "00000"

    #@f8
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fb
    goto/16 :goto_4c

    #@fd
    .line 2075
    :cond_fd
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@ff
    const/high16 v1, 0x1

    #@101
    if-ge v0, v1, :cond_10a

    #@103
    const-string v0, "0000"

    #@105
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@108
    goto/16 :goto_4c

    #@10a
    .line 2076
    :cond_10a
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@10c
    const/high16 v1, 0x10

    #@10e
    if-ge v0, v1, :cond_117

    #@110
    const-string v0, "000"

    #@112
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@115
    goto/16 :goto_4c

    #@117
    .line 2077
    :cond_117
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@119
    const/high16 v1, 0x100

    #@11b
    if-ge v0, v1, :cond_124

    #@11d
    const-string v0, "00"

    #@11f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@122
    goto/16 :goto_4c

    #@124
    .line 2078
    :cond_124
    iget v0, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@126
    const/high16 v1, 0x1000

    #@128
    if-ge v0, v1, :cond_4c

    #@12a
    const-string v0, "0"

    #@12c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12f
    goto/16 :goto_4c

    #@131
    .line 2085
    :pswitch_131
    const-string/jumbo v0, "unknown"

    #@134
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@137
    goto/16 :goto_6e

    #@139
    .line 2088
    :pswitch_139
    const-string v0, "charging"

    #@13b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13e
    goto/16 :goto_6e

    #@140
    .line 2091
    :pswitch_140
    const-string v0, "discharging"

    #@142
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@145
    goto/16 :goto_6e

    #@147
    .line 2094
    :pswitch_147
    const-string/jumbo v0, "not-charging"

    #@14a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14d
    goto/16 :goto_6e

    #@14f
    .line 2097
    :pswitch_14f
    const-string v0, "full"

    #@151
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@154
    goto/16 :goto_6e

    #@156
    .line 2109
    :pswitch_156
    const-string/jumbo v0, "unknown"

    #@159
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15c
    goto/16 :goto_87

    #@15e
    .line 2112
    :pswitch_15e
    const-string v0, "good"

    #@160
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@163
    goto/16 :goto_87

    #@165
    .line 2115
    :pswitch_165
    const-string/jumbo v0, "overheat"

    #@168
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16b
    goto/16 :goto_87

    #@16d
    .line 2118
    :pswitch_16d
    const-string v0, "dead"

    #@16f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@172
    goto/16 :goto_87

    #@174
    .line 2121
    :pswitch_174
    const-string/jumbo v0, "over-voltage"

    #@177
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17a
    goto/16 :goto_87

    #@17c
    .line 2124
    :pswitch_17c
    const-string v0, "failure"

    #@17e
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@181
    goto/16 :goto_87

    #@183
    .line 2136
    :pswitch_183
    const-string/jumbo v0, "none"

    #@186
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@189
    goto/16 :goto_a0

    #@18b
    .line 2139
    :pswitch_18b
    const-string v0, "ac"

    #@18d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@190
    goto/16 :goto_a0

    #@192
    .line 2142
    :pswitch_192
    const-string/jumbo v0, "usb"

    #@195
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@198
    goto/16 :goto_a0

    #@19a
    .line 2145
    :pswitch_19a
    const-string/jumbo v0, "wireless"

    #@19d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a0
    goto/16 :goto_a0

    #@1a2
    .line 2083
    :pswitch_data_1a2
    .packed-switch 0x1
        :pswitch_131
        :pswitch_139
        :pswitch_140
        :pswitch_147
        :pswitch_14f
    .end packed-switch

    #@1b0
    .line 2107
    :pswitch_data_1b0
    .packed-switch 0x1
        :pswitch_156
        :pswitch_15e
        :pswitch_165
        :pswitch_16d
        :pswitch_174
        :pswitch_17c
    .end packed-switch

    #@1c0
    .line 2134
    :pswitch_data_1c0
    .packed-switch 0x0
        :pswitch_183
        :pswitch_18b
        :pswitch_192
        :pswitch_9b
        :pswitch_19a
    .end packed-switch
.end method
