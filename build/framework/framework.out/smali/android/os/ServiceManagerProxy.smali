.class Landroid/os/ServiceManagerProxy;
.super Ljava/lang/Object;
.source "ServiceManagerNative.java"

# interfaces
.implements Landroid/os/IServiceManager;


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 110
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 111
    iput-object p1, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 112
    return-void
.end method


# virtual methods
.method public addService(Ljava/lang/String;Landroid/os/IBinder;Z)V
    .registers 9
    .parameter "name"
    .parameter "service"
    .parameter "allowIsolated"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 144
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 145
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 146
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.os.IServiceManager"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 147
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@11
    .line 148
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 149
    if-eqz p3, :cond_27

    #@16
    const/4 v2, 0x1

    #@17
    :goto_17
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 150
    iget-object v2, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1c
    const/4 v4, 0x3

    #@1d
    invoke-interface {v2, v4, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 151
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 152
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 153
    return-void

    #@27
    :cond_27
    move v2, v3

    #@28
    .line 149
    goto :goto_17
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public checkService(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 8
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 132
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 133
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.os.IServiceManager"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 134
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 135
    iget-object v3, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x2

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 136
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v0

    #@1b
    .line 137
    .local v0, binder:Landroid/os/IBinder;
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 138
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 139
    return-object v0
.end method

.method public getService(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 8
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 119
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v1

    #@4
    .line 120
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 121
    .local v2, reply:Landroid/os/Parcel;
    const-string v3, "android.os.IServiceManager"

    #@a
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 122
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 123
    iget-object v3, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x1

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 124
    invoke-virtual {v2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v0

    #@1b
    .line 125
    .local v0, binder:Landroid/os/IBinder;
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 126
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 127
    return-object v0
.end method

.method public listServices()[Ljava/lang/String;
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 156
    new-instance v6, Ljava/util/ArrayList;

    #@2
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 157
    .local v6, services:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@6
    .line 159
    .local v3, n:I
    :goto_6
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 160
    .local v1, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v4

    #@e
    .line 161
    .local v4, reply:Landroid/os/Parcel;
    const-string v7, "android.os.IServiceManager"

    #@10
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@13
    .line 162
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 163
    add-int/lit8 v3, v3, 0x1

    #@18
    .line 165
    :try_start_18
    iget-object v7, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/4 v8, 0x4

    #@1b
    const/4 v9, 0x0

    #@1c
    invoke-interface {v7, v8, v1, v4, v9}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1f
    .catch Ljava/lang/RuntimeException; {:try_start_18 .. :try_end_1f} :catch_2c

    #@1f
    move-result v5

    #@20
    .line 166
    .local v5, res:Z
    if-nez v5, :cond_2e

    #@22
    .line 179
    .end local v5           #res:Z
    :goto_22
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v7

    #@26
    new-array v0, v7, [Ljava/lang/String;

    #@28
    .line 180
    .local v0, array:[Ljava/lang/String;
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@2b
    .line 181
    return-object v0

    #@2c
    .line 169
    .end local v0           #array:[Ljava/lang/String;
    :catch_2c
    move-exception v2

    #@2d
    .line 173
    .local v2, e:Ljava/lang/RuntimeException;
    goto :goto_22

    #@2e
    .line 175
    .end local v2           #e:Ljava/lang/RuntimeException;
    .restart local v5       #res:Z
    :cond_2e
    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    .line 176
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 177
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    goto :goto_6
.end method

.method public setPermissionController(Landroid/os/IPermissionController;)V
    .registers 7
    .parameter "controller"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 187
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 188
    .local v1, reply:Landroid/os/Parcel;
    const-string v2, "android.os.IServiceManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 189
    invoke-interface {p1}, Landroid/os/IPermissionController;->asBinder()Landroid/os/IBinder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@14
    .line 190
    iget-object v2, p0, Landroid/os/ServiceManagerProxy;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v3, 0x6

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 191
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 192
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 193
    return-void
.end method
