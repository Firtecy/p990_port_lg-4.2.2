.class final Landroid/os/DropBoxManager$Entry$1;
.super Ljava/lang/Object;
.source "DropBoxManager.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/DropBoxManager$Entry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 222
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/os/DropBoxManager$Entry;
    .registers 8
    .parameter "in"

    #@0
    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 226
    .local v1, tag:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@7
    move-result-wide v2

    #@8
    .line 227
    .local v2, millis:J
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v5

    #@c
    .line 228
    .local v5, flags:I
    and-int/lit8 v0, v5, 0x8

    #@e
    if-eqz v0, :cond_1c

    #@10
    .line 229
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@12
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    #@15
    move-result-object v4

    #@16
    and-int/lit8 v5, v5, -0x9

    #@18
    invoke-direct/range {v0 .. v5}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;J[BI)V

    #@1b
    .line 231
    .end local v5           #flags:I
    :goto_1b
    return-object v0

    #@1c
    .restart local v5       #flags:I
    :cond_1c
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@1e
    invoke-virtual {p1}, Landroid/os/Parcel;->readFileDescriptor()Landroid/os/ParcelFileDescriptor;

    #@21
    move-result-object v4

    #@22
    invoke-direct/range {v0 .. v5}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;JLandroid/os/ParcelFileDescriptor;I)V

    #@25
    goto :goto_1b
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 222
    invoke-virtual {p0, p1}, Landroid/os/DropBoxManager$Entry$1;->createFromParcel(Landroid/os/Parcel;)Landroid/os/DropBoxManager$Entry;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/os/DropBoxManager$Entry;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 223
    new-array v0, p1, [Landroid/os/DropBoxManager$Entry;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 222
    invoke-virtual {p0, p1}, Landroid/os/DropBoxManager$Entry$1;->newArray(I)[Landroid/os/DropBoxManager$Entry;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
