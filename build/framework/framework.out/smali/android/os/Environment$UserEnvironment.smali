.class public Landroid/os/Environment$UserEnvironment;
.super Ljava/lang/Object;
.source "Environment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/Environment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserEnvironment"
.end annotation


# instance fields
.field private final mExternalStorage:Ljava/io/File;

.field private final mExternalStorageAndroidData:Ljava/io/File;

.field private final mExternalStorageAndroidMedia:Ljava/io/File;

.field private final mExternalStorageAndroidObb:Ljava/io/File;

.field private final mMediaStorage:Ljava/io/File;


# direct methods
.method public constructor <init>(I)V
    .registers 14
    .parameter "userId"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 97
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 99
    const-string v6, "EXTERNAL_STORAGE"

    #@8
    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v3

    #@c
    .line 100
    .local v3, rawExternalStorage:Ljava/lang/String;
    const-string v6, "EMULATED_STORAGE_TARGET"

    #@e
    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    .line 101
    .local v2, rawEmulatedStorageTarget:Ljava/lang/String;
    const-string v6, "MEDIA_STORAGE"

    #@14
    invoke-static {v6}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    .line 102
    .local v4, rawMediaStorage:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_20

    #@1e
    .line 103
    const-string v4, "/data/media"

    #@20
    .line 106
    :cond_20
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@23
    move-result v6

    #@24
    if-nez v6, :cond_81

    #@26
    .line 109
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    .line 110
    .local v5, rawUserId:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@2c
    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2f
    .line 111
    .local v0, emulatedBase:Ljava/io/File;
    new-instance v1, Ljava/io/File;

    #@31
    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@34
    .line 114
    .local v1, mediaBase:Ljava/io/File;
    new-array v6, v10, [Ljava/lang/String;

    #@36
    aput-object v5, v6, v9

    #@38
    invoke-static {v0, v6}, Landroid/os/Environment;->access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@3b
    move-result-object v6

    #@3c
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@3e
    .line 116
    new-array v6, v10, [Ljava/lang/String;

    #@40
    aput-object v5, v6, v9

    #@42
    invoke-static {v1, v6}, Landroid/os/Environment;->access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@45
    move-result-object v6

    #@46
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mMediaStorage:Ljava/io/File;

    #@48
    .line 131
    .end local v0           #emulatedBase:Ljava/io/File;
    .end local v1           #mediaBase:Ljava/io/File;
    .end local v5           #rawUserId:Ljava/lang/String;
    :goto_48
    iget-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@4a
    new-array v7, v11, [Ljava/lang/String;

    #@4c
    sget-object v8, Landroid/os/Environment;->DIRECTORY_ANDROID:Ljava/lang/String;

    #@4e
    aput-object v8, v7, v9

    #@50
    const-string/jumbo v8, "obb"

    #@53
    aput-object v8, v7, v10

    #@55
    invoke-static {v6, v7}, Landroid/os/Environment;->access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@58
    move-result-object v6

    #@59
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidObb:Ljava/io/File;

    #@5b
    .line 132
    iget-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@5d
    new-array v7, v11, [Ljava/lang/String;

    #@5f
    sget-object v8, Landroid/os/Environment;->DIRECTORY_ANDROID:Ljava/lang/String;

    #@61
    aput-object v8, v7, v9

    #@63
    const-string v8, "data"

    #@65
    aput-object v8, v7, v10

    #@67
    invoke-static {v6, v7}, Landroid/os/Environment;->access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@6a
    move-result-object v6

    #@6b
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidData:Ljava/io/File;

    #@6d
    .line 133
    iget-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@6f
    new-array v7, v11, [Ljava/lang/String;

    #@71
    sget-object v8, Landroid/os/Environment;->DIRECTORY_ANDROID:Ljava/lang/String;

    #@73
    aput-object v8, v7, v9

    #@75
    const-string/jumbo v8, "media"

    #@78
    aput-object v8, v7, v10

    #@7a
    invoke-static {v6, v7}, Landroid/os/Environment;->access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@7d
    move-result-object v6

    #@7e
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidMedia:Ljava/io/File;

    #@80
    .line 134
    return-void

    #@81
    .line 120
    :cond_81
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@84
    move-result v6

    #@85
    if-eqz v6, :cond_90

    #@87
    .line 121
    const-string v6, "Environment"

    #@89
    const-string v7, "EXTERNAL_STORAGE undefined; falling back to default"

    #@8b
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 122
    const-string v3, "/storage/sdcard0"

    #@90
    .line 126
    :cond_90
    new-instance v6, Ljava/io/File;

    #@92
    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@95
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@97
    .line 128
    new-instance v6, Ljava/io/File;

    #@99
    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@9c
    iput-object v6, p0, Landroid/os/Environment$UserEnvironment;->mMediaStorage:Ljava/io/File;

    #@9e
    goto :goto_48
.end method


# virtual methods
.method public getExternalStorageAndroidDataDir()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidData:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public getExternalStorageAppCacheDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 169
    new-instance v0, Ljava/io/File;

    #@2
    new-instance v1, Ljava/io/File;

    #@4
    iget-object v2, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidData:Ljava/io/File;

    #@6
    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    const-string v2, "cache"

    #@b
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@e
    return-object v0
.end method

.method public getExternalStorageAppDataDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 153
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidData:Ljava/io/File;

    #@4
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public getExternalStorageAppFilesDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 5
    .parameter "packageName"

    #@0
    .prologue
    .line 165
    new-instance v0, Ljava/io/File;

    #@2
    new-instance v1, Ljava/io/File;

    #@4
    iget-object v2, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidData:Ljava/io/File;

    #@6
    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@9
    const-string v2, "files"

    #@b
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@e
    return-object v0
.end method

.method public getExternalStorageAppMediaDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 157
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidMedia:Ljava/io/File;

    #@4
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter "packageName"

    #@0
    .prologue
    .line 161
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidObb:Ljava/io/File;

    #@4
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public getExternalStorageDirectory()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 137
    iget-object v0, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public getExternalStorageObbDirectory()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorageAndroidObb:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter "type"

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/io/File;

    #@2
    iget-object v1, p0, Landroid/os/Environment$UserEnvironment;->mExternalStorage:Ljava/io/File;

    #@4
    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@7
    return-object v0
.end method

.method public getMediaStorageDirectory()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 173
    iget-object v0, p0, Landroid/os/Environment$UserEnvironment;->mMediaStorage:Ljava/io/File;

    #@2
    return-object v0
.end method
