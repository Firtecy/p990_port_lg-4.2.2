.class public Landroid/os/RemoteCallbackList;
.super Ljava/lang/Object;
.source "RemoteCallbackList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/RemoteCallbackList$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mActiveBroadcast:[Ljava/lang/Object;

.field private mBroadcastCount:I

.field mCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/os/RemoteCallbackList",
            "<TE;>.Callback;>;"
        }
    .end annotation
.end field

.field private mKilled:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 49
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 50
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@a
    .line 53
    const/4 v0, -0x1

    #@b
    iput v0, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@d
    .line 54
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Landroid/os/RemoteCallbackList;->mKilled:Z

    #@10
    .line 56
    return-void
.end method


# virtual methods
.method public beginBroadcast()I
    .registers 10

    #@0
    .prologue
    .line 227
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget-object v7, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@2
    monitor-enter v7

    #@3
    .line 228
    :try_start_3
    iget v6, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@5
    if-lez v6, :cond_12

    #@7
    .line 229
    new-instance v6, Ljava/lang/IllegalStateException;

    #@9
    const-string v8, "beginBroadcast() called while already in a broadcast"

    #@b
    invoke-direct {v6, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v6

    #@f
    .line 246
    :catchall_f
    move-exception v6

    #@10
    monitor-exit v7
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v6

    #@12
    .line 233
    :cond_12
    :try_start_12
    iget-object v6, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@14
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@1a
    .line 234
    .local v0, N:I
    if-gtz v0, :cond_1f

    #@1c
    .line 235
    const/4 v4, 0x0

    #@1d
    monitor-exit v7

    #@1e
    .line 245
    :goto_1e
    return v4

    #@1f
    .line 237
    :cond_1f
    iget-object v1, p0, Landroid/os/RemoteCallbackList;->mActiveBroadcast:[Ljava/lang/Object;

    #@21
    .line 238
    .local v1, active:[Ljava/lang/Object;
    if-eqz v1, :cond_26

    #@23
    array-length v6, v1

    #@24
    if-ge v6, v0, :cond_2a

    #@26
    .line 239
    :cond_26
    new-array v1, v0, [Ljava/lang/Object;

    #@28
    iput-object v1, p0, Landroid/os/RemoteCallbackList;->mActiveBroadcast:[Ljava/lang/Object;

    #@2a
    .line 241
    :cond_2a
    const/4 v3, 0x0

    #@2b
    .line 242
    .local v3, i:I
    iget-object v6, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@30
    move-result-object v6

    #@31
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v5

    #@35
    .local v5, i$:Ljava/util/Iterator;
    move v4, v3

    #@36
    .end local v3           #i:I
    .local v4, i:I
    :goto_36
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@39
    move-result v6

    #@3a
    if-eqz v6, :cond_48

    #@3c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3f
    move-result-object v2

    #@40
    check-cast v2, Landroid/os/RemoteCallbackList$Callback;

    #@42
    .line 243
    .local v2, cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    add-int/lit8 v3, v4, 0x1

    #@44
    .end local v4           #i:I
    .restart local v3       #i:I
    aput-object v2, v1, v4

    #@46
    move v4, v3

    #@47
    .end local v3           #i:I
    .restart local v4       #i:I
    goto :goto_36

    #@48
    .line 245
    .end local v2           #cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    :cond_48
    monitor-exit v7
    :try_end_49
    .catchall {:try_start_12 .. :try_end_49} :catchall_f

    #@49
    goto :goto_1e
.end method

.method public finishBroadcast()V
    .registers 6

    #@0
    .prologue
    .line 292
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget v3, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@2
    if-gez v3, :cond_c

    #@4
    .line 293
    new-instance v3, Ljava/lang/IllegalStateException;

    #@6
    const-string v4, "finishBroadcast() called outside of a broadcast"

    #@8
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v3

    #@c
    .line 297
    :cond_c
    iget-object v1, p0, Landroid/os/RemoteCallbackList;->mActiveBroadcast:[Ljava/lang/Object;

    #@e
    .line 298
    .local v1, active:[Ljava/lang/Object;
    if-eqz v1, :cond_1b

    #@10
    .line 299
    iget v0, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@12
    .line 300
    .local v0, N:I
    const/4 v2, 0x0

    #@13
    .local v2, i:I
    :goto_13
    if-ge v2, v0, :cond_1b

    #@15
    .line 301
    const/4 v3, 0x0

    #@16
    aput-object v3, v1, v2

    #@18
    .line 300
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_13

    #@1b
    .line 305
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_1b
    const/4 v3, -0x1

    #@1c
    iput v3, p0, Landroid/os/RemoteCallbackList;->mBroadcastCount:I

    #@1e
    .line 306
    return-void
.end method

.method public getBroadcastCookie(I)Ljava/lang/Object;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 281
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget-object v0, p0, Landroid/os/RemoteCallbackList;->mActiveBroadcast:[Ljava/lang/Object;

    #@2
    aget-object v0, v0, p1

    #@4
    check-cast v0, Landroid/os/RemoteCallbackList$Callback;

    #@6
    iget-object v0, v0, Landroid/os/RemoteCallbackList$Callback;->mCookie:Ljava/lang/Object;

    #@8
    return-object v0
.end method

.method public getBroadcastItem(I)Landroid/os/IInterface;
    .registers 3
    .parameter "index"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    #@0
    .prologue
    .line 271
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget-object v0, p0, Landroid/os/RemoteCallbackList;->mActiveBroadcast:[Ljava/lang/Object;

    #@2
    aget-object v0, v0, p1

    #@4
    check-cast v0, Landroid/os/RemoteCallbackList$Callback;

    #@6
    iget-object v0, v0, Landroid/os/RemoteCallbackList$Callback;->mCallback:Landroid/os/IInterface;

    #@8
    return-object v0
.end method

.method public getRegisteredCallbackCount()I
    .registers 3

    #@0
    .prologue
    .line 321
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget-object v1, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 322
    :try_start_3
    iget-boolean v0, p0, Landroid/os/RemoteCallbackList;->mKilled:Z

    #@5
    if-eqz v0, :cond_a

    #@7
    .line 323
    const/4 v0, 0x0

    #@8
    monitor-exit v1

    #@9
    .line 325
    :goto_9
    return v0

    #@a
    :cond_a
    iget-object v0, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@c
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@f
    move-result v0

    #@10
    monitor-exit v1

    #@11
    goto :goto_9

    #@12
    .line 326
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public kill()V
    .registers 6

    #@0
    .prologue
    .line 161
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    iget-object v3, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@2
    monitor-enter v3

    #@3
    .line 162
    :try_start_3
    iget-object v2, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@5
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@8
    move-result-object v2

    #@9
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@c
    move-result-object v1

    #@d
    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_27

    #@13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/os/RemoteCallbackList$Callback;

    #@19
    .line 163
    .local v0, cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    iget-object v2, v0, Landroid/os/RemoteCallbackList$Callback;->mCallback:Landroid/os/IInterface;

    #@1b
    invoke-interface {v2}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    const/4 v4, 0x0

    #@20
    invoke-interface {v2, v0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@23
    goto :goto_d

    #@24
    .line 167
    .end local v0           #cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_24
    move-exception v2

    #@25
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_24

    #@26
    throw v2

    #@27
    .line 165
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_27
    :try_start_27
    iget-object v2, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@29
    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    #@2c
    .line 166
    const/4 v2, 0x1

    #@2d
    iput-boolean v2, p0, Landroid/os/RemoteCallbackList;->mKilled:Z

    #@2f
    .line 167
    monitor-exit v3
    :try_end_30
    .catchall {:try_start_27 .. :try_end_30} :catchall_24

    #@30
    .line 168
    return-void
.end method

.method public onCallbackDied(Landroid/os/IInterface;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 175
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    .local p1, callback:Landroid/os/IInterface;,"TE;"
    return-void
.end method

.method public onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter "cookie"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 192
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    .local p1, callback:Landroid/os/IInterface;,"TE;"
    invoke-virtual {p0, p1}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;)V

    #@3
    .line 193
    return-void
.end method

.method public register(Landroid/os/IInterface;)Z
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 78
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    .local p1, callback:Landroid/os/IInterface;,"TE;"
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public register(Landroid/os/IInterface;Ljava/lang/Object;)Z
    .registers 9
    .parameter
    .parameter "cookie"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    .local p1, callback:Landroid/os/IInterface;,"TE;"
    const/4 v3, 0x0

    #@1
    .line 108
    iget-object v4, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@3
    monitor-enter v4

    #@4
    .line 109
    :try_start_4
    iget-boolean v5, p0, Landroid/os/RemoteCallbackList;->mKilled:Z

    #@6
    if-eqz v5, :cond_a

    #@8
    .line 110
    monitor-exit v4

    #@9
    .line 119
    :goto_9
    return v3

    #@a
    .line 112
    :cond_a
    invoke-interface {p1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_1f

    #@d
    move-result-object v0

    #@e
    .line 114
    .local v0, binder:Landroid/os/IBinder;
    :try_start_e
    new-instance v1, Landroid/os/RemoteCallbackList$Callback;

    #@10
    invoke-direct {v1, p0, p1, p2}, Landroid/os/RemoteCallbackList$Callback;-><init>(Landroid/os/RemoteCallbackList;Landroid/os/IInterface;Ljava/lang/Object;)V

    #@13
    .line 115
    .local v1, cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    const/4 v5, 0x0

    #@14
    invoke-interface {v0, v1, v5}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@17
    .line 116
    iget-object v5, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@19
    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1c
    .catchall {:try_start_e .. :try_end_1c} :catchall_1f
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_1c} :catch_22

    #@1c
    .line 117
    const/4 v3, 0x1

    #@1d
    :try_start_1d
    monitor-exit v4

    #@1e
    goto :goto_9

    #@1f
    .line 121
    .end local v0           #binder:Landroid/os/IBinder;
    .end local v1           #cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    :catchall_1f
    move-exception v3

    #@20
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_1d .. :try_end_21} :catchall_1f

    #@21
    throw v3

    #@22
    .line 118
    .restart local v0       #binder:Landroid/os/IBinder;
    :catch_22
    move-exception v2

    #@23
    .line 119
    .local v2, e:Landroid/os/RemoteException;
    :try_start_23
    monitor-exit v4
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_1f

    #@24
    goto :goto_9
.end method

.method public unregister(Landroid/os/IInterface;)Z
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    #@0
    .prologue
    .local p0, this:Landroid/os/RemoteCallbackList;,"Landroid/os/RemoteCallbackList<TE;>;"
    .local p1, callback:Landroid/os/IInterface;,"TE;"
    const/4 v1, 0x0

    #@1
    .line 142
    iget-object v2, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@3
    monitor-enter v2

    #@4
    .line 143
    :try_start_4
    iget-object v3, p0, Landroid/os/RemoteCallbackList;->mCallbacks:Ljava/util/HashMap;

    #@6
    invoke-interface {p1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@9
    move-result-object v4

    #@a
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/os/RemoteCallbackList$Callback;

    #@10
    .line 144
    .local v0, cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    if-eqz v0, :cond_1f

    #@12
    .line 145
    iget-object v1, v0, Landroid/os/RemoteCallbackList$Callback;->mCallback:Landroid/os/IInterface;

    #@14
    invoke-interface {v1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@17
    move-result-object v1

    #@18
    const/4 v3, 0x0

    #@19
    invoke-interface {v1, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    #@1c
    .line 146
    const/4 v1, 0x1

    #@1d
    monitor-exit v2

    #@1e
    .line 148
    :goto_1e
    return v1

    #@1f
    :cond_1f
    monitor-exit v2

    #@20
    goto :goto_1e

    #@21
    .line 149
    .end local v0           #cb:Landroid/os/RemoteCallbackList$Callback;,"Landroid/os/RemoteCallbackList<TE;>.Callback;"
    :catchall_21
    move-exception v1

    #@22
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_4 .. :try_end_23} :catchall_21

    #@23
    throw v1
.end method
