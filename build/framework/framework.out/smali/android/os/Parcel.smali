.class public final Landroid/os/Parcel;
.super Ljava/lang/Object;
.source "Parcel.java"


# static fields
.field private static final DEBUG_RECYCLE:Z = false

.field private static final EX_BAD_PARCELABLE:I = -0x2

.field private static final EX_HAS_REPLY_HEADER:I = -0x80

.field private static final EX_ILLEGAL_ARGUMENT:I = -0x3

.field private static final EX_ILLEGAL_STATE:I = -0x5

.field private static final EX_NULL_POINTER:I = -0x4

.field private static final EX_SECURITY:I = -0x1

.field private static final POOL_SIZE:I = 0x6

.field public static final STRING_CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "Parcel"

.field private static final VAL_BOOLEAN:I = 0x9

.field private static final VAL_BOOLEANARRAY:I = 0x17

.field private static final VAL_BUNDLE:I = 0x3

.field private static final VAL_BYTE:I = 0x14

.field private static final VAL_BYTEARRAY:I = 0xd

.field private static final VAL_CHARSEQUENCE:I = 0xa

.field private static final VAL_CHARSEQUENCEARRAY:I = 0x18

.field private static final VAL_DOUBLE:I = 0x8

.field private static final VAL_FLOAT:I = 0x7

.field private static final VAL_IBINDER:I = 0xf

.field private static final VAL_INTARRAY:I = 0x12

.field private static final VAL_INTEGER:I = 0x1

.field private static final VAL_LIST:I = 0xb

.field private static final VAL_LONG:I = 0x6

.field private static final VAL_LONGARRAY:I = 0x13

.field private static final VAL_MAP:I = 0x2

.field private static final VAL_NULL:I = -0x1

.field private static final VAL_OBJECTARRAY:I = 0x11

.field private static final VAL_PARCELABLE:I = 0x4

.field private static final VAL_PARCELABLEARRAY:I = 0x10

.field private static final VAL_SERIALIZABLE:I = 0x15

.field private static final VAL_SHORT:I = 0x5

.field private static final VAL_SPARSEARRAY:I = 0xc

.field private static final VAL_SPARSEBOOLEANARRAY:I = 0x16

.field private static final VAL_STRING:I = 0x0

.field private static final VAL_STRINGARRAY:I = 0xe

.field private static final mCreators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/ClassLoader;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable$Creator;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sHolderPool:[Landroid/os/Parcel;

.field private static final sOwnedPool:[Landroid/os/Parcel;


# instance fields
.field private mNativePtr:I

.field private mOwnsNativeParcelObject:Z

.field private mStack:Ljava/lang/RuntimeException;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x6

    #@1
    .line 194
    new-array v0, v1, [Landroid/os/Parcel;

    #@3
    sput-object v0, Landroid/os/Parcel;->sOwnedPool:[Landroid/os/Parcel;

    #@5
    .line 195
    new-array v0, v1, [Landroid/os/Parcel;

    #@7
    sput-object v0, Landroid/os/Parcel;->sHolderPool:[Landroid/os/Parcel;

    #@9
    .line 274
    new-instance v0, Landroid/os/Parcel$1;

    #@b
    invoke-direct {v0}, Landroid/os/Parcel$1;-><init>()V

    #@e
    sput-object v0, Landroid/os/Parcel;->STRING_CREATOR:Landroid/os/Parcelable$Creator;

    #@10
    .line 2158
    new-instance v0, Ljava/util/HashMap;

    #@12
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@15
    sput-object v0, Landroid/os/Parcel;->mCreators:Ljava/util/HashMap;

    #@17
    return-void
.end method

.method private constructor <init>(I)V
    .registers 2
    .parameter "nativePtr"

    #@0
    .prologue
    .line 2179
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2184
    invoke-direct {p0, p1}, Landroid/os/Parcel;->init(I)V

    #@6
    .line 2185
    return-void
.end method

.method static native clearFileDescriptor(Ljava/io/FileDescriptor;)V
.end method

.method static native closeFileDescriptor(Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private destroy()V
    .registers 2

    #@0
    .prologue
    .line 2204
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 2205
    iget-boolean v0, p0, Landroid/os/Parcel;->mOwnsNativeParcelObject:Z

    #@6
    if-eqz v0, :cond_d

    #@8
    .line 2206
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@a
    invoke-static {v0}, Landroid/os/Parcel;->nativeDestroy(I)V

    #@d
    .line 2208
    :cond_d
    const/4 v0, 0x0

    #@e
    iput v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@10
    .line 2210
    :cond_10
    return-void
.end method

.method static native dupFileDescriptor(Ljava/io/FileDescriptor;)Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private freeBuffer()V
    .registers 2

    #@0
    .prologue
    .line 2198
    iget-boolean v0, p0, Landroid/os/Parcel;->mOwnsNativeParcelObject:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 2199
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@6
    invoke-static {v0}, Landroid/os/Parcel;->nativeFreeBuffer(I)V

    #@9
    .line 2201
    :cond_9
    return-void
.end method

.method private init(I)V
    .registers 3
    .parameter "nativePtr"

    #@0
    .prologue
    .line 2188
    if-eqz p1, :cond_8

    #@2
    .line 2189
    iput p1, p0, Landroid/os/Parcel;->mNativePtr:I

    #@4
    .line 2190
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/os/Parcel;->mOwnsNativeParcelObject:Z

    #@7
    .line 2195
    :goto_7
    return-void

    #@8
    .line 2192
    :cond_8
    invoke-static {}, Landroid/os/Parcel;->nativeCreate()I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@e
    .line 2193
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Landroid/os/Parcel;->mOwnsNativeParcelObject:Z

    #@11
    goto :goto_7
.end method

.method private static native nativeAppendFrom(IIII)V
.end method

.method private static native nativeCreate()I
.end method

.method private static native nativeCreateByteArray(I)[B
.end method

.method private static native nativeDataAvail(I)I
.end method

.method private static native nativeDataCapacity(I)I
.end method

.method private static native nativeDataPosition(I)I
.end method

.method private static native nativeDataSize(I)I
.end method

.method private static native nativeDestroy(I)V
.end method

.method private static native nativeEnforceInterface(ILjava/lang/String;)V
.end method

.method private static native nativeFreeBuffer(I)V
.end method

.method private static native nativeHasFileDescriptors(I)Z
.end method

.method private static native nativeMarshall(I)[B
.end method

.method private static native nativePushAllowFds(IZ)Z
.end method

.method private static native nativeReadDouble(I)D
.end method

.method private static native nativeReadFileDescriptor(I)Ljava/io/FileDescriptor;
.end method

.method private static native nativeReadFloat(I)F
.end method

.method private static native nativeReadInt(I)I
.end method

.method private static native nativeReadLong(I)J
.end method

.method private static native nativeReadString(I)Ljava/lang/String;
.end method

.method private static native nativeReadStrongBinder(I)Landroid/os/IBinder;
.end method

.method private static native nativeRestoreAllowFds(IZ)V
.end method

.method private static native nativeSetDataCapacity(II)V
.end method

.method private static native nativeSetDataPosition(II)V
.end method

.method private static native nativeSetDataSize(II)V
.end method

.method private static native nativeUnmarshall(I[BII)V
.end method

.method private static native nativeWriteByteArray(I[BII)V
.end method

.method private static native nativeWriteDouble(ID)V
.end method

.method private static native nativeWriteFileDescriptor(ILjava/io/FileDescriptor;)V
.end method

.method private static native nativeWriteFloat(IF)V
.end method

.method private static native nativeWriteInt(II)V
.end method

.method private static native nativeWriteInterfaceToken(ILjava/lang/String;)V
.end method

.method private static native nativeWriteLong(IJ)V
.end method

.method private static native nativeWriteString(ILjava/lang/String;)V
.end method

.method private static native nativeWriteStrongBinder(ILandroid/os/IBinder;)V
.end method

.method public static obtain()Landroid/os/Parcel;
    .registers 4

    #@0
    .prologue
    .line 288
    sget-object v2, Landroid/os/Parcel;->sOwnedPool:[Landroid/os/Parcel;

    #@2
    .line 289
    .local v2, pool:[Landroid/os/Parcel;
    monitor-enter v2

    #@3
    .line 291
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    const/4 v3, 0x6

    #@5
    if-ge v0, v3, :cond_13

    #@7
    .line 292
    :try_start_7
    aget-object v1, v2, v0

    #@9
    .line 293
    .local v1, p:Landroid/os/Parcel;
    if-eqz v1, :cond_10

    #@b
    .line 294
    const/4 v3, 0x0

    #@c
    aput-object v3, v2, v0

    #@e
    .line 298
    monitor-exit v2

    #@f
    .line 302
    .end local v1           #p:Landroid/os/Parcel;
    :goto_f
    return-object v1

    #@10
    .line 291
    .restart local v1       #p:Landroid/os/Parcel;
    :cond_10
    add-int/lit8 v0, v0, 0x1

    #@12
    goto :goto_4

    #@13
    .line 301
    .end local v1           #p:Landroid/os/Parcel;
    :cond_13
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_1b

    #@14
    .line 302
    new-instance v1, Landroid/os/Parcel;

    #@16
    const/4 v3, 0x0

    #@17
    invoke-direct {v1, v3}, Landroid/os/Parcel;-><init>(I)V

    #@1a
    goto :goto_f

    #@1b
    .line 301
    :catchall_1b
    move-exception v3

    #@1c
    :try_start_1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    #@1d
    throw v3
.end method

.method protected static final obtain(I)Landroid/os/Parcel;
    .registers 5
    .parameter "obj"

    #@0
    .prologue
    .line 2161
    sget-object v2, Landroid/os/Parcel;->sHolderPool:[Landroid/os/Parcel;

    #@2
    .line 2162
    .local v2, pool:[Landroid/os/Parcel;
    monitor-enter v2

    #@3
    .line 2164
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    const/4 v3, 0x6

    #@5
    if-ge v0, v3, :cond_16

    #@7
    .line 2165
    :try_start_7
    aget-object v1, v2, v0

    #@9
    .line 2166
    .local v1, p:Landroid/os/Parcel;
    if-eqz v1, :cond_13

    #@b
    .line 2167
    const/4 v3, 0x0

    #@c
    aput-object v3, v2, v0

    #@e
    .line 2171
    invoke-direct {v1, p0}, Landroid/os/Parcel;->init(I)V

    #@11
    .line 2172
    monitor-exit v2

    #@12
    .line 2176
    .end local v1           #p:Landroid/os/Parcel;
    :goto_12
    return-object v1

    #@13
    .line 2164
    .restart local v1       #p:Landroid/os/Parcel;
    :cond_13
    add-int/lit8 v0, v0, 0x1

    #@15
    goto :goto_4

    #@16
    .line 2175
    .end local v1           #p:Landroid/os/Parcel;
    :cond_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_1d

    #@17
    .line 2176
    new-instance v1, Landroid/os/Parcel;

    #@19
    invoke-direct {v1, p0}, Landroid/os/Parcel;-><init>(I)V

    #@1c
    goto :goto_12

    #@1d
    .line 2175
    :catchall_1d
    move-exception v3

    #@1e
    :try_start_1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v3
.end method

.method static native openFileDescriptor(Ljava/lang/String;I)Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method private readArrayInternal([Ljava/lang/Object;ILjava/lang/ClassLoader;)V
    .registers 6
    .parameter "outVal"
    .parameter "N"
    .parameter "loader"

    #@0
    .prologue
    .line 2244
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    if-ge v0, p2, :cond_c

    #@3
    .line 2245
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    .line 2247
    .local v1, value:Ljava/lang/Object;
    aput-object v1, p1, v0

    #@9
    .line 2244
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_1

    #@c
    .line 2249
    .end local v1           #value:Ljava/lang/Object;
    :cond_c
    return-void
.end method

.method private readListInternal(Ljava/util/List;ILjava/lang/ClassLoader;)V
    .registers 5
    .parameter "outVal"
    .parameter "N"
    .parameter "loader"

    #@0
    .prologue
    .line 2234
    :goto_0
    if-lez p2, :cond_c

    #@2
    .line 2235
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 2237
    .local v0, value:Ljava/lang/Object;
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@9
    .line 2238
    add-int/lit8 p2, p2, -0x1

    #@b
    .line 2239
    goto :goto_0

    #@c
    .line 2240
    .end local v0           #value:Ljava/lang/Object;
    :cond_c
    return-void
.end method

.method private readSparseArrayInternal(Landroid/util/SparseArray;ILjava/lang/ClassLoader;)V
    .registers 6
    .parameter "outVal"
    .parameter "N"
    .parameter "loader"

    #@0
    .prologue
    .line 2253
    :goto_0
    if-lez p2, :cond_10

    #@2
    .line 2254
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@5
    move-result v0

    #@6
    .line 2255
    .local v0, key:I
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 2257
    .local v1, value:Ljava/lang/Object;
    invoke-virtual {p1, v0, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@d
    .line 2258
    add-int/lit8 p2, p2, -0x1

    #@f
    .line 2259
    goto :goto_0

    #@10
    .line 2260
    .end local v0           #key:I
    .end local v1           #value:Ljava/lang/Object;
    :cond_10
    return-void
.end method

.method private readSparseBooleanArrayInternal(Landroid/util/SparseBooleanArray;I)V
    .registers 7
    .parameter "outVal"
    .parameter "N"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2264
    :goto_1
    if-lez p2, :cond_16

    #@3
    .line 2265
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    .line 2266
    .local v0, key:I
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@a
    move-result v3

    #@b
    if-ne v3, v2, :cond_14

    #@d
    move v1, v2

    #@e
    .line 2268
    .local v1, value:Z
    :goto_e
    invoke-virtual {p1, v0, v1}, Landroid/util/SparseBooleanArray;->append(IZ)V

    #@11
    .line 2269
    add-int/lit8 p2, p2, -0x1

    #@13
    .line 2270
    goto :goto_1

    #@14
    .line 2266
    .end local v1           #value:Z
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_e

    #@16
    .line 2271
    .end local v0           #key:I
    :cond_16
    return-void
.end method


# virtual methods
.method public final appendFrom(Landroid/os/Parcel;II)V
    .registers 6
    .parameter "parcel"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 428
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    iget v1, p1, Landroid/os/Parcel;->mNativePtr:I

    #@4
    invoke-static {v0, v1, p2, p3}, Landroid/os/Parcel;->nativeAppendFrom(IIII)V

    #@7
    .line 429
    return-void
.end method

.method public final createBinderArray()[Landroid/os/IBinder;
    .registers 5

    #@0
    .prologue
    .line 968
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 969
    .local v0, N:I
    if-ltz v0, :cond_14

    #@6
    .line 970
    new-array v2, v0, [Landroid/os/IBinder;

    #@8
    .line 971
    .local v2, val:[Landroid/os/IBinder;
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_15

    #@b
    .line 972
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@e
    move-result-object v3

    #@f
    aput-object v3, v2, v1

    #@11
    .line 971
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_9

    #@14
    .line 976
    .end local v1           #i:I
    .end local v2           #val:[Landroid/os/IBinder;
    :cond_14
    const/4 v2, 0x0

    #@15
    :cond_15
    return-object v2
.end method

.method public final createBinderArrayList()Ljava/util/ArrayList;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1807
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1808
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1809
    const/4 v1, 0x0

    #@7
    .line 1816
    :cond_7
    return-object v1

    #@8
    .line 1811
    :cond_8
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@d
    .line 1812
    .local v1, l:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/IBinder;>;"
    :goto_d
    if-lez v0, :cond_7

    #@f
    .line 1813
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@16
    .line 1814
    add-int/lit8 v0, v0, -0x1

    #@18
    goto :goto_d
.end method

.method public final createBooleanArray()[Z
    .registers 5

    #@0
    .prologue
    .line 695
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 699
    .local v0, N:I
    if-ltz v0, :cond_21

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x2

    #@c
    if-gt v0, v3, :cond_21

    #@e
    .line 700
    new-array v2, v0, [Z

    #@10
    .line 701
    .local v2, val:[Z
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_22

    #@13
    .line 702
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_1f

    #@19
    const/4 v3, 0x1

    #@1a
    :goto_1a
    aput-boolean v3, v2, v1

    #@1c
    .line 701
    add-int/lit8 v1, v1, 0x1

    #@1e
    goto :goto_11

    #@1f
    .line 702
    :cond_1f
    const/4 v3, 0x0

    #@20
    goto :goto_1a

    #@21
    .line 706
    .end local v1           #i:I
    .end local v2           #val:[Z
    :cond_21
    const/4 v2, 0x0

    #@22
    :cond_22
    return-object v2
.end method

.method public final createByteArray()[B
    .registers 2

    #@0
    .prologue
    .line 1584
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeCreateByteArray(I)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final createCharArray()[C
    .registers 5

    #@0
    .prologue
    .line 734
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 735
    .local v0, N:I
    if-ltz v0, :cond_1d

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x2

    #@c
    if-gt v0, v3, :cond_1d

    #@e
    .line 736
    new-array v2, v0, [C

    #@10
    .line 737
    .local v2, val:[C
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_1e

    #@13
    .line 738
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v3

    #@17
    int-to-char v3, v3

    #@18
    aput-char v3, v2, v1

    #@1a
    .line 737
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_11

    #@1d
    .line 742
    .end local v1           #i:I
    .end local v2           #val:[C
    :cond_1d
    const/4 v2, 0x0

    #@1e
    :cond_1e
    return-object v2
.end method

.method public final createDoubleArray()[D
    .registers 6

    #@0
    .prologue
    .line 880
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 882
    .local v0, N:I
    if-ltz v0, :cond_1c

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x3

    #@c
    if-gt v0, v3, :cond_1c

    #@e
    .line 883
    new-array v2, v0, [D

    #@10
    .line 884
    .local v2, val:[D
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_1d

    #@13
    .line 885
    invoke-virtual {p0}, Landroid/os/Parcel;->readDouble()D

    #@16
    move-result-wide v3

    #@17
    aput-wide v3, v2, v1

    #@19
    .line 884
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_11

    #@1c
    .line 889
    .end local v1           #i:I
    .end local v2           #val:[D
    :cond_1c
    const/4 v2, 0x0

    #@1d
    :cond_1d
    return-object v2
.end method

.method public final createFloatArray()[F
    .registers 5

    #@0
    .prologue
    .line 843
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 845
    .local v0, N:I
    if-ltz v0, :cond_1c

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x2

    #@c
    if-gt v0, v3, :cond_1c

    #@e
    .line 846
    new-array v2, v0, [F

    #@10
    .line 847
    .local v2, val:[F
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_1d

    #@13
    .line 848
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    #@16
    move-result v3

    #@17
    aput v3, v2, v1

    #@19
    .line 847
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_11

    #@1c
    .line 852
    .end local v1           #i:I
    .end local v2           #val:[F
    :cond_1c
    const/4 v2, 0x0

    #@1d
    :cond_1d
    return-object v2
.end method

.method public final createIntArray()[I
    .registers 5

    #@0
    .prologue
    .line 770
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 771
    .local v0, N:I
    if-ltz v0, :cond_1c

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x2

    #@c
    if-gt v0, v3, :cond_1c

    #@e
    .line 772
    new-array v2, v0, [I

    #@10
    .line 773
    .local v2, val:[I
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_1d

    #@13
    .line 774
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v3

    #@17
    aput v3, v2, v1

    #@19
    .line 773
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_11

    #@1c
    .line 778
    .end local v1           #i:I
    .end local v2           #val:[I
    :cond_1c
    const/4 v2, 0x0

    #@1d
    :cond_1d
    return-object v2
.end method

.method public final createLongArray()[J
    .registers 6

    #@0
    .prologue
    .line 806
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 808
    .local v0, N:I
    if-ltz v0, :cond_1c

    #@6
    invoke-virtual {p0}, Landroid/os/Parcel;->dataAvail()I

    #@9
    move-result v3

    #@a
    shr-int/lit8 v3, v3, 0x3

    #@c
    if-gt v0, v3, :cond_1c

    #@e
    .line 809
    new-array v2, v0, [J

    #@10
    .line 810
    .local v2, val:[J
    const/4 v1, 0x0

    #@11
    .local v1, i:I
    :goto_11
    if-ge v1, v0, :cond_1d

    #@13
    .line 811
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    #@16
    move-result-wide v3

    #@17
    aput-wide v3, v2, v1

    #@19
    .line 810
    add-int/lit8 v1, v1, 0x1

    #@1b
    goto :goto_11

    #@1c
    .line 815
    .end local v1           #i:I
    .end local v2           #val:[J
    :cond_1c
    const/4 v2, 0x0

    #@1d
    :cond_1d
    return-object v2
.end method

.method public final createStringArray()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 917
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 918
    .local v0, N:I
    if-ltz v0, :cond_14

    #@6
    .line 919
    new-array v2, v0, [Ljava/lang/String;

    #@8
    .line 920
    .local v2, val:[Ljava/lang/String;
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    :goto_9
    if-ge v1, v0, :cond_15

    #@b
    .line 921
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    aput-object v3, v2, v1

    #@11
    .line 920
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_9

    #@14
    .line 925
    .end local v1           #i:I
    .end local v2           #val:[Ljava/lang/String;
    :cond_14
    const/4 v2, 0x0

    #@15
    :cond_15
    return-object v2
.end method

.method public final createStringArrayList()Ljava/util/ArrayList;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1783
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1784
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1785
    const/4 v1, 0x0

    #@7
    .line 1792
    :cond_7
    return-object v1

    #@8
    .line 1787
    :cond_8
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@d
    .line 1788
    .local v1, l:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_d
    if-lez v0, :cond_7

    #@f
    .line 1789
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@16
    .line 1790
    add-int/lit8 v0, v0, -0x1

    #@18
    goto :goto_d
.end method

.method public final createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 1880
    .local p1, c:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1881
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1882
    const/4 v2, 0x0

    #@7
    .line 1890
    :cond_7
    return-object v2

    #@8
    .line 1884
    :cond_8
    invoke-interface {p1, v0}, Landroid/os/Parcelable$Creator;->newArray(I)[Ljava/lang/Object;

    #@b
    move-result-object v2

    #@c
    .line 1885
    .local v2, l:[Ljava/lang/Object;,"[TT;"
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_7

    #@f
    .line 1886
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_1b

    #@15
    .line 1887
    invoke-interface {p1, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    aput-object v3, v2, v1

    #@1b
    .line 1885
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    #@1d
    goto :goto_d
.end method

.method public final createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    .local p1, c:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    const/4 v2, 0x0

    #@1
    .line 1720
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v0

    #@5
    .line 1721
    .local v0, N:I
    if-gez v0, :cond_9

    #@7
    move-object v1, v2

    #@8
    .line 1733
    :cond_8
    return-object v1

    #@9
    .line 1724
    :cond_9
    new-instance v1, Ljava/util/ArrayList;

    #@b
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@e
    .line 1725
    .local v1, l:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    :goto_e
    if-lez v0, :cond_8

    #@10
    .line 1726
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_20

    #@16
    .line 1727
    invoke-interface {p1, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1d
    .line 1731
    :goto_1d
    add-int/lit8 v0, v0, -0x1

    #@1f
    goto :goto_e

    #@20
    .line 1729
    :cond_20
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    goto :goto_1d
.end method

.method public final dataAvail()I
    .registers 2

    #@0
    .prologue
    .line 343
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeDataAvail(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final dataCapacity()I
    .registers 2

    #@0
    .prologue
    .line 361
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeDataCapacity(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final dataPosition()I
    .registers 2

    #@0
    .prologue
    .line 351
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeDataPosition(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final dataSize()I
    .registers 2

    #@0
    .prologue
    .line 335
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeDataSize(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final enforceInterface(Ljava/lang/String;)V
    .registers 3
    .parameter "interfaceName"

    #@0
    .prologue
    .line 448
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeEnforceInterface(ILjava/lang/String;)V

    #@5
    .line 449
    return-void
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 2219
    invoke-direct {p0}, Landroid/os/Parcel;->destroy()V

    #@3
    .line 2220
    return-void
.end method

.method public final hasFileDescriptors()Z
    .registers 2

    #@0
    .prologue
    .line 435
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeHasFileDescriptors(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final marshall()[B
    .registers 2

    #@0
    .prologue
    .line 417
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeMarshall(I)[B

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final pushAllowFds(Z)Z
    .registers 3
    .parameter "allowFds"

    #@0
    .prologue
    .line 397
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativePushAllowFds(IZ)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    .line 1666
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1667
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1668
    const/4 v1, 0x0

    #@7
    .line 1672
    :goto_7
    return-object v1

    #@8
    .line 1670
    :cond_8
    new-array v1, v0, [Ljava/lang/Object;

    #@a
    .line 1671
    .local v1, l:[Ljava/lang/Object;
    invoke-direct {p0, v1, v0, p1}, Landroid/os/Parcel;->readArrayInternal([Ljava/lang/Object;ILjava/lang/ClassLoader;)V

    #@d
    goto :goto_7
.end method

.method public final readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    .line 1650
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1651
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1652
    const/4 v1, 0x0

    #@7
    .line 1656
    :goto_7
    return-object v1

    #@8
    .line 1654
    :cond_8
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@d
    .line 1655
    .local v1, l:Ljava/util/ArrayList;
    invoke-direct {p0, v1, v0, p1}, Landroid/os/Parcel;->readListInternal(Ljava/util/List;ILjava/lang/ClassLoader;)V

    #@10
    goto :goto_7
.end method

.method public final readBinderArray([Landroid/os/IBinder;)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 981
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 982
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 983
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 984
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@d
    move-result-object v2

    #@e
    aput-object v2, p1, v1

    #@10
    .line 983
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 987
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 989
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readBinderList(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/IBinder;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1853
    .local p1, list:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    .line 1854
    .local v0, M:I
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    .line 1855
    .local v1, N:I
    const/4 v2, 0x0

    #@9
    .line 1856
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_17

    #@b
    if-ge v2, v1, :cond_17

    #@d
    .line 1857
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@10
    move-result-object v3

    #@11
    invoke-interface {p1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 1856
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_9

    #@17
    .line 1859
    :cond_17
    :goto_17
    if-ge v2, v1, :cond_23

    #@19
    .line 1860
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@20
    .line 1859
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_17

    #@23
    .line 1862
    :cond_23
    :goto_23
    if-ge v2, v0, :cond_2b

    #@25
    .line 1863
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@28
    .line 1862
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_23

    #@2b
    .line 1865
    :cond_2b
    return-void
.end method

.method public final readBooleanArray([Z)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 711
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 712
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_18

    #@7
    .line 713
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_20

    #@a
    .line 714
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_16

    #@10
    const/4 v2, 0x1

    #@11
    :goto_11
    aput-boolean v2, p1, v1

    #@13
    .line 713
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_8

    #@16
    .line 714
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_11

    #@18
    .line 717
    .end local v1           #i:I
    :cond_18
    new-instance v2, Ljava/lang/RuntimeException;

    #@1a
    const-string v3, "bad array lengths"

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    .line 719
    .restart local v1       #i:I
    :cond_20
    return-void
.end method

.method public final readBundle()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 1558
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public final readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    .line 1568
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v1

    #@4
    .line 1569
    .local v1, length:I
    if-gez v1, :cond_8

    #@6
    .line 1570
    const/4 v0, 0x0

    #@7
    .line 1577
    :cond_7
    :goto_7
    return-object v0

    #@8
    .line 1573
    :cond_8
    new-instance v0, Landroid/os/Bundle;

    #@a
    invoke-direct {v0, p0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Parcel;I)V

    #@d
    .line 1574
    .local v0, bundle:Landroid/os/Bundle;
    if-eqz p1, :cond_7

    #@f
    .line 1575
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    #@12
    goto :goto_7
.end method

.method public final readByte()B
    .registers 2

    #@0
    .prologue
    .line 1511
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    and-int/lit16 v0, v0, 0xff

    #@6
    int-to-byte v0, v0

    #@7
    return v0
.end method

.method public final readByteArray([B)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1593
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    #@4
    move-result-object v0

    #@5
    .line 1594
    .local v0, ba:[B
    array-length v1, v0

    #@6
    array-length v2, p1

    #@7
    if-ne v1, v2, :cond_e

    #@9
    .line 1595
    array-length v1, v0

    #@a
    invoke-static {v0, v3, p1, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@d
    .line 1599
    return-void

    #@e
    .line 1597
    :cond_e
    new-instance v1, Ljava/lang/RuntimeException;

    #@10
    const-string v2, "bad array lengths"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1
.end method

.method public final readCharArray([C)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 747
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 748
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_14

    #@7
    .line 749
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1c

    #@a
    .line 750
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v2

    #@e
    int-to-char v2, v2

    #@f
    aput-char v2, p1, v1

    #@11
    .line 749
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_8

    #@14
    .line 753
    .end local v1           #i:I
    :cond_14
    new-instance v2, Ljava/lang/RuntimeException;

    #@16
    const-string v3, "bad array lengths"

    #@18
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v2

    #@1c
    .line 755
    .restart local v1       #i:I
    :cond_1c
    return-void
.end method

.method public final readCharSequence()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 1481
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    #@2
    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/CharSequence;

    #@8
    return-object v0
.end method

.method public final readCharSequenceArray()[Ljava/lang/CharSequence;
    .registers 5

    #@0
    .prologue
    .line 1627
    const/4 v0, 0x0

    #@1
    .line 1629
    .local v0, array:[Ljava/lang/CharSequence;
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v2

    #@5
    .line 1630
    .local v2, length:I
    if-ltz v2, :cond_15

    #@7
    .line 1632
    new-array v0, v2, [Ljava/lang/CharSequence;

    #@9
    .line 1634
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_15

    #@c
    .line 1636
    invoke-virtual {p0}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@f
    move-result-object v3

    #@10
    aput-object v3, v0, v1

    #@12
    .line 1634
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_a

    #@15
    .line 1640
    .end local v1           #i:I
    :cond_15
    return-object v0
.end method

.method public final readDouble()D
    .registers 3

    #@0
    .prologue
    .line 1466
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadDouble(I)D

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public final readDoubleArray([D)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 894
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 895
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 896
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 897
    invoke-virtual {p0}, Landroid/os/Parcel;->readDouble()D

    #@d
    move-result-wide v2

    #@e
    aput-wide v2, p1, v1

    #@10
    .line 896
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 900
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 902
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readException()V
    .registers 3

    #@0
    .prologue
    .line 1376
    invoke-virtual {p0}, Landroid/os/Parcel;->readExceptionCode()I

    #@3
    move-result v0

    #@4
    .line 1377
    .local v0, code:I
    if-eqz v0, :cond_d

    #@6
    .line 1378
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 1379
    .local v1, msg:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readException(ILjava/lang/String;)V

    #@d
    .line 1381
    .end local v1           #msg:Ljava/lang/String;
    :cond_d
    return-void
.end method

.method public final readException(ILjava/lang/String;)V
    .registers 6
    .parameter "code"
    .parameter "msg"

    #@0
    .prologue
    .line 1423
    packed-switch p1, :pswitch_data_44

    #@3
    .line 1435
    new-instance v0, Ljava/lang/RuntimeException;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown exception code: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, " msg "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 1425
    :pswitch_26
    new-instance v0, Ljava/lang/SecurityException;

    #@28
    invoke-direct {v0, p2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v0

    #@2c
    .line 1427
    :pswitch_2c
    new-instance v0, Landroid/os/BadParcelableException;

    #@2e
    invoke-direct {v0, p2}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    .line 1429
    :pswitch_32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@34
    invoke-direct {v0, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@37
    throw v0

    #@38
    .line 1431
    :pswitch_38
    new-instance v0, Ljava/lang/NullPointerException;

    #@3a
    invoke-direct {v0, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@3d
    throw v0

    #@3e
    .line 1433
    :pswitch_3e
    new-instance v0, Ljava/lang/IllegalStateException;

    #@40
    invoke-direct {v0, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@43
    throw v0

    #@44
    .line 1423
    :pswitch_data_44
    .packed-switch -0x5
        :pswitch_3e
        :pswitch_38
        :pswitch_32
        :pswitch_2c
        :pswitch_26
    .end packed-switch
.end method

.method public final readExceptionCode()I
    .registers 5

    #@0
    .prologue
    .line 1397
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1398
    .local v0, code:I
    const/16 v2, -0x80

    #@6
    if-ne v0, v2, :cond_16

    #@8
    .line 1399
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v1

    #@c
    .line 1400
    .local v1, headerSize:I
    if-nez v1, :cond_17

    #@e
    .line 1401
    const-string v2, "Parcel"

    #@10
    const-string v3, "Unexpected zero-sized Parcel reply header."

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 1411
    :goto_15
    const/4 v0, 0x0

    #@16
    .line 1413
    .end local v0           #code:I
    .end local v1           #headerSize:I
    :cond_16
    return v0

    #@17
    .line 1407
    .restart local v0       #code:I
    .restart local v1       #headerSize:I
    :cond_17
    invoke-static {p0}, Landroid/os/StrictMode;->readAndHandleBinderCallViolations(Landroid/os/Parcel;)V

    #@1a
    goto :goto_15
.end method

.method public final readFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .registers 3

    #@0
    .prologue
    .line 1495
    iget v1, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v1}, Landroid/os/Parcel;->nativeReadFileDescriptor(I)Ljava/io/FileDescriptor;

    #@5
    move-result-object v0

    #@6
    .line 1496
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_e

    #@8
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@a
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@d
    :goto_d
    return-object v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method public final readFloat()F
    .registers 2

    #@0
    .prologue
    .line 1458
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadFloat(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final readFloatArray([F)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 857
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 858
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 859
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 860
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    #@d
    move-result v2

    #@e
    aput v2, p1, v1

    #@10
    .line 859
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 863
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 865
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    .line 1543
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1544
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1545
    const/4 v1, 0x0

    #@7
    .line 1549
    :goto_7
    return-object v1

    #@8
    .line 1547
    :cond_8
    new-instance v1, Ljava/util/HashMap;

    #@a
    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    #@d
    .line 1548
    .local v1, m:Ljava/util/HashMap;
    invoke-virtual {p0, v1, v0, p1}, Landroid/os/Parcel;->readMapInternal(Ljava/util/Map;ILjava/lang/ClassLoader;)V

    #@10
    goto :goto_7
.end method

.method public final readInt()I
    .registers 2

    #@0
    .prologue
    .line 1443
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadInt(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final readIntArray([I)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 783
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 784
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 785
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 786
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v2

    #@e
    aput v2, p1, v1

    #@10
    .line 785
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 789
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 791
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readList(Ljava/util/List;Ljava/lang/ClassLoader;)V
    .registers 4
    .parameter "outVal"
    .parameter "loader"

    #@0
    .prologue
    .line 1530
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1531
    .local v0, N:I
    invoke-direct {p0, p1, v0, p2}, Landroid/os/Parcel;->readListInternal(Ljava/util/List;ILjava/lang/ClassLoader;)V

    #@7
    .line 1532
    return-void
.end method

.method public final readLong()J
    .registers 3

    #@0
    .prologue
    .line 1450
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadLong(I)J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public final readLongArray([J)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 820
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 821
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 822
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 823
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    #@d
    move-result-wide v2

    #@e
    aput-wide v2, p1, v1

    #@10
    .line 822
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 826
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 828
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V
    .registers 4
    .parameter "outVal"
    .parameter "loader"

    #@0
    .prologue
    .line 1520
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1521
    .local v0, N:I
    invoke-virtual {p0, p1, v0, p2}, Landroid/os/Parcel;->readMapInternal(Ljava/util/Map;ILjava/lang/ClassLoader;)V

    #@7
    .line 1522
    return-void
.end method

.method readMapInternal(Ljava/util/Map;ILjava/lang/ClassLoader;)V
    .registers 6
    .parameter "outVal"
    .parameter "N"
    .parameter "loader"

    #@0
    .prologue
    .line 2224
    :goto_0
    if-lez p2, :cond_10

    #@2
    .line 2225
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 2226
    .local v0, key:Ljava/lang/Object;
    invoke-virtual {p0, p3}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 2227
    .local v1, value:Ljava/lang/Object;
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 2228
    add-int/lit8 p2, p2, -0x1

    #@f
    .line 2229
    goto :goto_0

    #@10
    .line 2230
    .end local v0           #key:Ljava/lang/Object;
    .end local v1           #value:Ljava/lang/Object;
    :cond_10
    return-void
.end method

.method public final readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
    .registers 12
    .parameter "loader"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/ClassLoader;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 2049
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4
    move-result-object v5

    #@5
    .line 2050
    .local v5, name:Ljava/lang/String;
    if-nez v5, :cond_8

    #@7
    .line 2103
    :goto_7
    return-object v6

    #@8
    .line 2054
    :cond_8
    sget-object v7, Landroid/os/Parcel;->mCreators:Ljava/util/HashMap;

    #@a
    monitor-enter v7

    #@b
    .line 2055
    :try_start_b
    sget-object v6, Landroid/os/Parcel;->mCreators:Ljava/util/HashMap;

    #@d
    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@10
    move-result-object v4

    #@11
    check-cast v4, Ljava/util/HashMap;

    #@13
    .line 2056
    .local v4, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/Parcelable$Creator;>;"
    if-nez v4, :cond_1f

    #@15
    .line 2057
    new-instance v4, Ljava/util/HashMap;

    #@17
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/Parcelable$Creator;>;"
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@1a
    .line 2058
    .restart local v4       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/Parcelable$Creator;>;"
    sget-object v6, Landroid/os/Parcel;->mCreators:Ljava/util/HashMap;

    #@1c
    invoke-virtual {v6, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    .line 2060
    :cond_1f
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    move-result-object v1

    #@23
    check-cast v1, Landroid/os/Parcelable$Creator;
    :try_end_25
    .catchall {:try_start_b .. :try_end_25} :catchall_55

    #@25
    .line 2061
    .local v1, creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    if-nez v1, :cond_10d

    #@27
    .line 2063
    if-nez p1, :cond_58

    #@29
    :try_start_29
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    #@2c
    move-result-object v0

    #@2d
    .line 2065
    .local v0, c:Ljava/lang/Class;
    :goto_2d
    const-string v6, "CREATOR"

    #@2f
    invoke-virtual {v0, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    #@32
    move-result-object v3

    #@33
    .line 2066
    .local v3, f:Ljava/lang/reflect/Field;
    const/4 v6, 0x0

    #@34
    invoke-virtual {v3, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    move-result-object v1

    #@38
    .end local v1           #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    check-cast v1, Landroid/os/Parcelable$Creator;
    :try_end_3a
    .catchall {:try_start_29 .. :try_end_3a} :catchall_55
    .catch Ljava/lang/IllegalAccessException; {:try_start_29 .. :try_end_3a} :catch_5e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_29 .. :try_end_3a} :catch_9a
    .catch Ljava/lang/ClassCastException; {:try_start_29 .. :try_end_3a} :catch_d6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_29 .. :try_end_3a} :catch_f0

    #@3a
    .line 2090
    .restart local v1       #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    if-nez v1, :cond_10a

    #@3c
    .line 2091
    :try_start_3c
    new-instance v6, Landroid/os/BadParcelableException;

    #@3e
    new-instance v8, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v9, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-direct {v6, v8}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@54
    throw v6

    #@55
    .line 2098
    .end local v0           #c:Ljava/lang/Class;
    .end local v1           #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    .end local v3           #f:Ljava/lang/reflect/Field;
    .end local v4           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/Parcelable$Creator;>;"
    :catchall_55
    move-exception v6

    #@56
    monitor-exit v7
    :try_end_57
    .catchall {:try_start_3c .. :try_end_57} :catchall_55

    #@57
    throw v6

    #@58
    .line 2063
    .restart local v1       #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    .restart local v4       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Landroid/os/Parcelable$Creator;>;"
    :cond_58
    const/4 v6, 0x1

    #@59
    :try_start_59
    invoke-static {v5, v6, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_55
    .catch Ljava/lang/IllegalAccessException; {:try_start_59 .. :try_end_5c} :catch_5e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_59 .. :try_end_5c} :catch_9a
    .catch Ljava/lang/ClassCastException; {:try_start_59 .. :try_end_5c} :catch_d6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_59 .. :try_end_5c} :catch_f0

    #@5c
    move-result-object v0

    #@5d
    goto :goto_2d

    #@5e
    .line 2068
    .end local v1           #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    :catch_5e
    move-exception v2

    #@5f
    .line 2069
    .local v2, e:Ljava/lang/IllegalAccessException;
    :try_start_5f
    const-string v6, "Parcel"

    #@61
    new-instance v8, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v9, "Class not found when unmarshalling: "

    #@68
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v8

    #@70
    const-string v9, ", e: "

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 2071
    new-instance v6, Landroid/os/BadParcelableException;

    #@83
    new-instance v8, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v9, "IllegalAccessException when unmarshalling: "

    #@8a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v8

    #@8e
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v8

    #@92
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v8

    #@96
    invoke-direct {v6, v8}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@99
    throw v6

    #@9a
    .line 2074
    .end local v2           #e:Ljava/lang/IllegalAccessException;
    :catch_9a
    move-exception v2

    #@9b
    .line 2075
    .local v2, e:Ljava/lang/ClassNotFoundException;
    const-string v6, "Parcel"

    #@9d
    new-instance v8, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v9, "Class not found when unmarshalling: "

    #@a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v8

    #@ac
    const-string v9, ", e: "

    #@ae
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v8

    #@b2
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v8

    #@ba
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 2077
    new-instance v6, Landroid/os/BadParcelableException;

    #@bf
    new-instance v8, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v9, "ClassNotFoundException when unmarshalling: "

    #@c6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v8

    #@ca
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v8

    #@ce
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d1
    move-result-object v8

    #@d2
    invoke-direct {v6, v8}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@d5
    throw v6

    #@d6
    .line 2080
    .end local v2           #e:Ljava/lang/ClassNotFoundException;
    :catch_d6
    move-exception v2

    #@d7
    .line 2081
    .local v2, e:Ljava/lang/ClassCastException;
    new-instance v6, Landroid/os/BadParcelableException;

    #@d9
    new-instance v8, Ljava/lang/StringBuilder;

    #@db
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@de
    const-string v9, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class "

    #@e0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v8

    #@e4
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v8

    #@e8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v8

    #@ec
    invoke-direct {v6, v8}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@ef
    throw v6

    #@f0
    .line 2085
    .end local v2           #e:Ljava/lang/ClassCastException;
    :catch_f0
    move-exception v2

    #@f1
    .line 2086
    .local v2, e:Ljava/lang/NoSuchFieldException;
    new-instance v6, Landroid/os/BadParcelableException;

    #@f3
    new-instance v8, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v9, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class "

    #@fa
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v8

    #@fe
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v8

    #@106
    invoke-direct {v6, v8}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    #@109
    throw v6

    #@10a
    .line 2096
    .end local v2           #e:Ljava/lang/NoSuchFieldException;
    .restart local v0       #c:Ljava/lang/Class;
    .restart local v1       #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    .restart local v3       #f:Ljava/lang/reflect/Field;
    :cond_10a
    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10d
    .line 2098
    .end local v0           #c:Ljava/lang/Class;
    .end local v3           #f:Ljava/lang/reflect/Field;
    :cond_10d
    monitor-exit v7
    :try_end_10e
    .catchall {:try_start_5f .. :try_end_10e} :catchall_55

    #@10e
    .line 2100
    instance-of v6, v1, Landroid/os/Parcelable$ClassLoaderCreator;

    #@110
    if-eqz v6, :cond_11c

    #@112
    .line 2101
    check-cast v1, Landroid/os/Parcelable$ClassLoaderCreator;

    #@114
    .end local v1           #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    invoke-interface {v1, p0, p1}, Landroid/os/Parcelable$ClassLoaderCreator;->createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    #@117
    move-result-object v6

    #@118
    check-cast v6, Landroid/os/Parcelable;

    #@11a
    goto/16 :goto_7

    #@11c
    .line 2103
    .restart local v1       #creator:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    :cond_11c
    invoke-interface {v1, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@11f
    move-result-object v6

    #@120
    check-cast v6, Landroid/os/Parcelable;

    #@122
    goto/16 :goto_7
.end method

.method public final readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;
    .registers 6
    .parameter "loader"

    #@0
    .prologue
    .line 2113
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 2114
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 2115
    const/4 v2, 0x0

    #@7
    .line 2121
    :cond_7
    return-object v2

    #@8
    .line 2117
    :cond_8
    new-array v2, v0, [Landroid/os/Parcelable;

    #@a
    .line 2118
    .local v2, p:[Landroid/os/Parcelable;
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_7

    #@d
    .line 2119
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@10
    move-result-object v3

    #@11
    aput-object v3, v2, v1

    #@13
    .line 2118
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_b
.end method

.method public final readSerializable()Ljava/io/Serializable;
    .registers 10

    #@0
    .prologue
    .line 2130
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 2131
    .local v3, name:Ljava/lang/String;
    if-nez v3, :cond_8

    #@6
    .line 2135
    const/4 v6, 0x0

    #@7
    .line 2142
    :goto_7
    return-object v6

    #@8
    .line 2138
    :cond_8
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    #@b
    move-result-object v5

    #@c
    .line 2139
    .local v5, serializedData:[B
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@e
    invoke-direct {v0, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@11
    .line 2141
    .local v0, bais:Ljava/io/ByteArrayInputStream;
    :try_start_11
    new-instance v4, Ljava/io/ObjectInputStream;

    #@13
    invoke-direct {v4, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    #@16
    .line 2142
    .local v4, ois:Ljava/io/ObjectInputStream;
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    #@19
    move-result-object v6

    #@1a
    check-cast v6, Ljava/io/Serializable;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_1c} :catch_1d
    .catch Ljava/lang/ClassNotFoundException; {:try_start_11 .. :try_end_1c} :catch_3d

    #@1c
    goto :goto_7

    #@1d
    .line 2143
    .end local v4           #ois:Ljava/io/ObjectInputStream;
    :catch_1d
    move-exception v2

    #@1e
    .line 2144
    .local v2, ioe:Ljava/io/IOException;
    new-instance v6, Ljava/lang/RuntimeException;

    #@20
    new-instance v7, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v8, "Parcelable encountered IOException reading a Serializable object (name = "

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v7

    #@2f
    const-string v8, ")"

    #@31
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v7

    #@35
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v7

    #@39
    invoke-direct {v6, v7, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3c
    throw v6

    #@3d
    .line 2147
    .end local v2           #ioe:Ljava/io/IOException;
    :catch_3d
    move-exception v1

    #@3e
    .line 2148
    .local v1, cnfe:Ljava/lang/ClassNotFoundException;
    new-instance v6, Ljava/lang/RuntimeException;

    #@40
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "Parcelable encounteredClassNotFoundException reading a Serializable object (name = "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    const-string v8, ")"

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-direct {v6, v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@5c
    throw v6
.end method

.method public final readSparseArray(Ljava/lang/ClassLoader;)Landroid/util/SparseArray;
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    .line 1682
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1683
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1684
    const/4 v1, 0x0

    #@7
    .line 1688
    :goto_7
    return-object v1

    #@8
    .line 1686
    :cond_8
    new-instance v1, Landroid/util/SparseArray;

    #@a
    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    #@d
    .line 1687
    .local v1, sa:Landroid/util/SparseArray;
    invoke-direct {p0, v1, v0, p1}, Landroid/os/Parcel;->readSparseArrayInternal(Landroid/util/SparseArray;ILjava/lang/ClassLoader;)V

    #@10
    goto :goto_7
.end method

.method public final readSparseBooleanArray()Landroid/util/SparseBooleanArray;
    .registers 3

    #@0
    .prologue
    .line 1697
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1698
    .local v0, N:I
    if-gez v0, :cond_8

    #@6
    .line 1699
    const/4 v1, 0x0

    #@7
    .line 1703
    :goto_7
    return-object v1

    #@8
    .line 1701
    :cond_8
    new-instance v1, Landroid/util/SparseBooleanArray;

    #@a
    invoke-direct {v1, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    #@d
    .line 1702
    .local v1, sa:Landroid/util/SparseBooleanArray;
    invoke-direct {p0, v1, v0}, Landroid/os/Parcel;->readSparseBooleanArrayInternal(Landroid/util/SparseBooleanArray;I)V

    #@10
    goto :goto_7
.end method

.method public final readString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 1473
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadString(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final readStringArray([Ljava/lang/String;)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 930
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 931
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_13

    #@7
    .line 932
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_1b

    #@a
    .line 933
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    aput-object v2, p1, v1

    #@10
    .line 932
    add-int/lit8 v1, v1, 0x1

    #@12
    goto :goto_8

    #@13
    .line 936
    .end local v1           #i:I
    :cond_13
    new-instance v2, Ljava/lang/RuntimeException;

    #@15
    const-string v3, "bad array lengths"

    #@17
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1a
    throw v2

    #@1b
    .line 938
    .restart local v1       #i:I
    :cond_1b
    return-void
.end method

.method public final readStringArray()[Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1606
    const/4 v0, 0x0

    #@1
    .line 1608
    .local v0, array:[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v2

    #@5
    .line 1609
    .local v2, length:I
    if-ltz v2, :cond_15

    #@7
    .line 1611
    new-array v0, v2, [Ljava/lang/String;

    #@9
    .line 1613
    const/4 v1, 0x0

    #@a
    .local v1, i:I
    :goto_a
    if-ge v1, v2, :cond_15

    #@c
    .line 1615
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    aput-object v3, v0, v1

    #@12
    .line 1613
    add-int/lit8 v1, v1, 0x1

    #@14
    goto :goto_a

    #@15
    .line 1619
    .end local v1           #i:I
    :cond_15
    return-object v0
.end method

.method public final readStringList(Ljava/util/List;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1829
    .local p1, list:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@3
    move-result v0

    #@4
    .line 1830
    .local v0, M:I
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@7
    move-result v1

    #@8
    .line 1831
    .local v1, N:I
    const/4 v2, 0x0

    #@9
    .line 1832
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_17

    #@b
    if-ge v2, v1, :cond_17

    #@d
    .line 1833
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-interface {p1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 1832
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_9

    #@17
    .line 1835
    :cond_17
    :goto_17
    if-ge v2, v1, :cond_23

    #@19
    .line 1836
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@20
    .line 1835
    add-int/lit8 v2, v2, 0x1

    #@22
    goto :goto_17

    #@23
    .line 1838
    :cond_23
    :goto_23
    if-ge v2, v0, :cond_2b

    #@25
    .line 1839
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@28
    .line 1838
    add-int/lit8 v2, v2, 0x1

    #@2a
    goto :goto_23

    #@2b
    .line 1841
    :cond_2b
    return-void
.end method

.method public final readStrongBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 1488
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0}, Landroid/os/Parcel;->nativeReadStrongBinder(I)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public final readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1894
    .local p1, val:[Ljava/lang/Object;,"[TT;"
    .local p2, c:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1895
    .local v0, N:I
    array-length v2, p1

    #@5
    if-ne v0, v2, :cond_1d

    #@7
    .line 1896
    const/4 v1, 0x0

    #@8
    .local v1, i:I
    :goto_8
    if-ge v1, v0, :cond_25

    #@a
    .line 1897
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_19

    #@10
    .line 1898
    invoke-interface {p2, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    aput-object v2, p1, v1

    #@16
    .line 1896
    :goto_16
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_8

    #@19
    .line 1900
    :cond_19
    const/4 v2, 0x0

    #@1a
    aput-object v2, p1, v1

    #@1c
    goto :goto_16

    #@1d
    .line 1904
    .end local v1           #i:I
    :cond_1d
    new-instance v2, Ljava/lang/RuntimeException;

    #@1f
    const-string v3, "bad array lengths"

    #@21
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@24
    throw v2

    #@25
    .line 1906
    .restart local v1       #i:I
    :cond_25
    return-void
.end method

.method public final readTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)[TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1914
    .local p1, c:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Landroid/os/Parcelable$Creator",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, list:Ljava/util/List;,"Ljava/util/List<TT;>;"
    .local p2, c:Landroid/os/Parcelable$Creator;,"Landroid/os/Parcelable$Creator<TT;>;"
    const/4 v4, 0x0

    #@1
    .line 1749
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@4
    move-result v0

    #@5
    .line 1750
    .local v0, M:I
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v1

    #@9
    .line 1751
    .local v1, N:I
    const/4 v2, 0x0

    #@a
    .line 1752
    .local v2, i:I
    :goto_a
    if-ge v2, v0, :cond_22

    #@c
    if-ge v2, v1, :cond_22

    #@e
    .line 1753
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_1e

    #@14
    .line 1754
    invoke-interface {p2, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17
    move-result-object v3

    #@18
    invoke-interface {p1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@1b
    .line 1752
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_a

    #@1e
    .line 1756
    :cond_1e
    invoke-interface {p1, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@21
    goto :goto_1b

    #@22
    .line 1759
    :cond_22
    :goto_22
    if-ge v2, v1, :cond_38

    #@24
    .line 1760
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_34

    #@2a
    .line 1761
    invoke-interface {p2, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d
    move-result-object v3

    #@2e
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    .line 1759
    :goto_31
    add-int/lit8 v2, v2, 0x1

    #@33
    goto :goto_22

    #@34
    .line 1763
    :cond_34
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@37
    goto :goto_31

    #@38
    .line 1766
    :cond_38
    :goto_38
    if-ge v2, v0, :cond_40

    #@3a
    .line 1767
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@3d
    .line 1766
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_38

    #@40
    .line 1769
    :cond_40
    return-void
.end method

.method public final readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;
    .registers 7
    .parameter "loader"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1949
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v1

    #@5
    .line 1951
    .local v1, type:I
    packed-switch v1, :pswitch_data_e2

    #@8
    .line 2031
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    #@b
    move-result v2

    #@c
    add-int/lit8 v0, v2, -0x4

    #@e
    .line 2032
    .local v0, off:I
    new-instance v2, Ljava/lang/RuntimeException;

    #@10
    new-instance v3, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v4, "Parcel "

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ": Unmarshalling unknown type code "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, " at offset "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v2

    #@3b
    .line 1953
    .end local v0           #off:I
    :pswitch_3b
    const/4 v2, 0x0

    #@3c
    .line 2028
    :goto_3c
    return-object v2

    #@3d
    .line 1956
    :pswitch_3d
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    goto :goto_3c

    #@42
    .line 1959
    :pswitch_42
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@45
    move-result v2

    #@46
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@49
    move-result-object v2

    #@4a
    goto :goto_3c

    #@4b
    .line 1962
    :pswitch_4b
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@4e
    move-result-object v2

    #@4f
    goto :goto_3c

    #@50
    .line 1965
    :pswitch_50
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@53
    move-result-object v2

    #@54
    goto :goto_3c

    #@55
    .line 1968
    :pswitch_55
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@58
    move-result v2

    #@59
    int-to-short v2, v2

    #@5a
    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@5d
    move-result-object v2

    #@5e
    goto :goto_3c

    #@5f
    .line 1971
    :pswitch_5f
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    #@62
    move-result-wide v2

    #@63
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@66
    move-result-object v2

    #@67
    goto :goto_3c

    #@68
    .line 1974
    :pswitch_68
    invoke-virtual {p0}, Landroid/os/Parcel;->readFloat()F

    #@6b
    move-result v2

    #@6c
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@6f
    move-result-object v2

    #@70
    goto :goto_3c

    #@71
    .line 1977
    :pswitch_71
    invoke-virtual {p0}, Landroid/os/Parcel;->readDouble()D

    #@74
    move-result-wide v2

    #@75
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@78
    move-result-object v2

    #@79
    goto :goto_3c

    #@7a
    .line 1980
    :pswitch_7a
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@7d
    move-result v3

    #@7e
    if-ne v3, v2, :cond_85

    #@80
    :goto_80
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@83
    move-result-object v2

    #@84
    goto :goto_3c

    #@85
    :cond_85
    const/4 v2, 0x0

    #@86
    goto :goto_80

    #@87
    .line 1983
    :pswitch_87
    invoke-virtual {p0}, Landroid/os/Parcel;->readCharSequence()Ljava/lang/CharSequence;

    #@8a
    move-result-object v2

    #@8b
    goto :goto_3c

    #@8c
    .line 1986
    :pswitch_8c
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    #@8f
    move-result-object v2

    #@90
    goto :goto_3c

    #@91
    .line 1989
    :pswitch_91
    invoke-virtual {p0}, Landroid/os/Parcel;->createBooleanArray()[Z

    #@94
    move-result-object v2

    #@95
    goto :goto_3c

    #@96
    .line 1992
    :pswitch_96
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    #@99
    move-result-object v2

    #@9a
    goto :goto_3c

    #@9b
    .line 1995
    :pswitch_9b
    invoke-virtual {p0}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@9e
    move-result-object v2

    #@9f
    goto :goto_3c

    #@a0
    .line 1998
    :pswitch_a0
    invoke-virtual {p0}, Landroid/os/Parcel;->readCharSequenceArray()[Ljava/lang/CharSequence;

    #@a3
    move-result-object v2

    #@a4
    goto :goto_3c

    #@a5
    .line 2001
    :pswitch_a5
    invoke-virtual {p0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a8
    move-result-object v2

    #@a9
    goto :goto_3c

    #@aa
    .line 2004
    :pswitch_aa
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    #@ad
    move-result-object v2

    #@ae
    goto :goto_3c

    #@af
    .line 2007
    :pswitch_af
    invoke-virtual {p0}, Landroid/os/Parcel;->createIntArray()[I

    #@b2
    move-result-object v2

    #@b3
    goto :goto_3c

    #@b4
    .line 2010
    :pswitch_b4
    invoke-virtual {p0}, Landroid/os/Parcel;->createLongArray()[J

    #@b7
    move-result-object v2

    #@b8
    goto :goto_3c

    #@b9
    .line 2013
    :pswitch_b9
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    #@bc
    move-result v2

    #@bd
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@c0
    move-result-object v2

    #@c1
    goto/16 :goto_3c

    #@c3
    .line 2016
    :pswitch_c3
    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    #@c6
    move-result-object v2

    #@c7
    goto/16 :goto_3c

    #@c9
    .line 2019
    :pswitch_c9
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    #@cc
    move-result-object v2

    #@cd
    goto/16 :goto_3c

    #@cf
    .line 2022
    :pswitch_cf
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readSparseArray(Ljava/lang/ClassLoader;)Landroid/util/SparseArray;

    #@d2
    move-result-object v2

    #@d3
    goto/16 :goto_3c

    #@d5
    .line 2025
    :pswitch_d5
    invoke-virtual {p0}, Landroid/os/Parcel;->readSparseBooleanArray()Landroid/util/SparseBooleanArray;

    #@d8
    move-result-object v2

    #@d9
    goto/16 :goto_3c

    #@db
    .line 2028
    :pswitch_db
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    #@de
    move-result-object v2

    #@df
    goto/16 :goto_3c

    #@e1
    .line 1951
    nop

    #@e2
    :pswitch_data_e2
    .packed-switch -0x1
        :pswitch_3b
        :pswitch_3d
        :pswitch_42
        :pswitch_4b
        :pswitch_db
        :pswitch_50
        :pswitch_55
        :pswitch_5f
        :pswitch_68
        :pswitch_71
        :pswitch_7a
        :pswitch_87
        :pswitch_8c
        :pswitch_cf
        :pswitch_96
        :pswitch_9b
        :pswitch_a5
        :pswitch_c9
        :pswitch_aa
        :pswitch_af
        :pswitch_b4
        :pswitch_b9
        :pswitch_c3
        :pswitch_d5
        :pswitch_91
        :pswitch_a0
    .end packed-switch
.end method

.method public final recycle()V
    .registers 4

    #@0
    .prologue
    .line 311
    invoke-direct {p0}, Landroid/os/Parcel;->freeBuffer()V

    #@3
    .line 314
    iget-boolean v2, p0, Landroid/os/Parcel;->mOwnsNativeParcelObject:Z

    #@5
    if-eqz v2, :cond_16

    #@7
    .line 315
    sget-object v1, Landroid/os/Parcel;->sOwnedPool:[Landroid/os/Parcel;

    #@9
    .line 321
    .local v1, pool:[Landroid/os/Parcel;
    :goto_9
    monitor-enter v1

    #@a
    .line 322
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    const/4 v2, 0x6

    #@c
    if-ge v0, v2, :cond_1f

    #@e
    .line 323
    :try_start_e
    aget-object v2, v1, v0

    #@10
    if-nez v2, :cond_1c

    #@12
    .line 324
    aput-object p0, v1, v0

    #@14
    .line 325
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_e .. :try_end_15} :catchall_21

    #@15
    .line 329
    :goto_15
    return-void

    #@16
    .line 317
    .end local v0           #i:I
    .end local v1           #pool:[Landroid/os/Parcel;
    :cond_16
    const/4 v2, 0x0

    #@17
    iput v2, p0, Landroid/os/Parcel;->mNativePtr:I

    #@19
    .line 318
    sget-object v1, Landroid/os/Parcel;->sHolderPool:[Landroid/os/Parcel;

    #@1b
    .restart local v1       #pool:[Landroid/os/Parcel;
    goto :goto_9

    #@1c
    .line 322
    .restart local v0       #i:I
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    #@1e
    goto :goto_b

    #@1f
    .line 328
    :cond_1f
    :try_start_1f
    monitor-exit v1

    #@20
    goto :goto_15

    #@21
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_1f .. :try_end_23} :catchall_21

    #@23
    throw v2
.end method

.method public final restoreAllowFds(Z)V
    .registers 3
    .parameter "lastValue"

    #@0
    .prologue
    .line 402
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeRestoreAllowFds(IZ)V

    #@5
    .line 403
    return-void
.end method

.method public final setDataCapacity(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 392
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeSetDataCapacity(II)V

    #@5
    .line 393
    return-void
.end method

.method public final setDataPosition(I)V
    .registers 3
    .parameter "pos"

    #@0
    .prologue
    .line 381
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeSetDataPosition(II)V

    #@5
    .line 382
    return-void
.end method

.method public final setDataSize(I)V
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 372
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeSetDataSize(II)V

    #@5
    .line 373
    return-void
.end method

.method public final unmarshall([BII)V
    .registers 5
    .parameter "data"
    .parameter "offest"
    .parameter "length"

    #@0
    .prologue
    .line 424
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/os/Parcel;->nativeUnmarshall(I[BII)V

    #@5
    .line 425
    return-void
.end method

.method public final writeArray([Ljava/lang/Object;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 633
    if-nez p1, :cond_7

    #@2
    .line 634
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 644
    :cond_6
    return-void

    #@7
    .line 637
    :cond_7
    array-length v0, p1

    #@8
    .line 638
    .local v0, N:I
    const/4 v1, 0x0

    #@9
    .line 639
    .local v1, i:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 640
    :goto_c
    if-ge v1, v0, :cond_6

    #@e
    .line 641
    aget-object v2, p1, v1

    #@10
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@13
    .line 642
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_c
.end method

.method public final writeBinderArray([Landroid/os/IBinder;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 941
    if-eqz p1, :cond_11

    #@2
    .line 942
    array-length v0, p1

    #@3
    .line 943
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 944
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 945
    aget-object v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@e
    .line 944
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 948
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 950
    :cond_15
    return-void
.end method

.method public final writeBinderList(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/IBinder;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1062
    .local p1, val:Ljava/util/List;,"Ljava/util/List<Landroid/os/IBinder;>;"
    if-nez p1, :cond_7

    #@2
    .line 1063
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1073
    :cond_6
    return-void

    #@7
    .line 1066
    :cond_7
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 1067
    .local v0, N:I
    const/4 v1, 0x0

    #@c
    .line 1068
    .local v1, i:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 1069
    :goto_f
    if-ge v1, v0, :cond_6

    #@11
    .line 1070
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Landroid/os/IBinder;

    #@17
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1a
    .line 1071
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_f
.end method

.method public final writeBooleanArray([Z)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 683
    if-eqz p1, :cond_16

    #@2
    .line 684
    array-length v0, p1

    #@3
    .line 685
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 686
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_1a

    #@9
    .line 687
    aget-boolean v2, p1, v1

    #@b
    if-eqz v2, :cond_14

    #@d
    const/4 v2, 0x1

    #@e
    :goto_e
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 686
    add-int/lit8 v1, v1, 0x1

    #@13
    goto :goto_7

    #@14
    .line 687
    :cond_14
    const/4 v2, 0x0

    #@15
    goto :goto_e

    #@16
    .line 690
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_16
    const/4 v2, -0x1

    #@17
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 692
    :cond_1a
    return-void
.end method

.method public final writeBundle(Landroid/os/Bundle;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 600
    if-nez p1, :cond_7

    #@2
    .line 601
    const/4 v0, -0x1

    #@3
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 606
    :goto_6
    return-void

    #@7
    .line 605
    :cond_7
    const/4 v0, 0x0

    #@8
    invoke-virtual {p1, p0, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@b
    goto :goto_6
.end method

.method public final writeByte(B)V
    .registers 2
    .parameter "val"

    #@0
    .prologue
    .line 560
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@3
    .line 561
    return-void
.end method

.method public final writeByteArray([B)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 457
    if-eqz p1, :cond_8

    #@3
    array-length v0, p1

    #@4
    :goto_4
    invoke-virtual {p0, p1, v1, v0}, Landroid/os/Parcel;->writeByteArray([BII)V

    #@7
    .line 458
    return-void

    #@8
    :cond_8
    move v0, v1

    #@9
    .line 457
    goto :goto_4
.end method

.method public final writeByteArray([BII)V
    .registers 5
    .parameter "b"
    .parameter "offset"
    .parameter "len"

    #@0
    .prologue
    .line 468
    if-nez p1, :cond_7

    #@2
    .line 469
    const/4 v0, -0x1

    #@3
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 474
    :goto_6
    return-void

    #@7
    .line 472
    :cond_7
    array-length v0, p1

    #@8
    invoke-static {v0, p2, p3}, Ljava/util/Arrays;->checkOffsetAndCount(III)V

    #@b
    .line 473
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@d
    invoke-static {v0, p1, p2, p3}, Landroid/os/Parcel;->nativeWriteByteArray(I[BII)V

    #@10
    goto :goto_6
.end method

.method public final writeCharArray([C)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 722
    if-eqz p1, :cond_11

    #@2
    .line 723
    array-length v0, p1

    #@3
    .line 724
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 725
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 726
    aget-char v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 725
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 729
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 731
    :cond_15
    return-void
.end method

.method public final writeCharSequence(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 522
    const/4 v0, 0x0

    #@1
    invoke-static {p1, p0, v0}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    #@4
    .line 523
    return-void
.end method

.method public final writeCharSequenceArray([Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 956
    if-eqz p1, :cond_11

    #@2
    .line 957
    array-length v0, p1

    #@3
    .line 958
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 959
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 960
    aget-object v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@e
    .line 959
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 963
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 965
    :cond_15
    return-void
.end method

.method public final writeDouble(D)V
    .registers 4
    .parameter "val"

    #@0
    .prologue
    .line 505
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/os/Parcel;->nativeWriteDouble(ID)V

    #@5
    .line 506
    return-void
.end method

.method public final writeDoubleArray([D)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 868
    if-eqz p1, :cond_11

    #@2
    .line 869
    array-length v0, p1

    #@3
    .line 870
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 871
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 872
    aget-wide v2, p1, v1

    #@b
    invoke-virtual {p0, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    #@e
    .line 871
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 875
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 877
    :cond_15
    return-void
.end method

.method public final writeException(Ljava/lang/Exception;)V
    .registers 4
    .parameter "e"

    #@0
    .prologue
    .line 1308
    const/4 v0, 0x0

    #@1
    .line 1309
    .local v0, code:I
    instance-of v1, p1, Ljava/lang/SecurityException;

    #@3
    if-eqz v1, :cond_15

    #@5
    .line 1310
    const/4 v0, -0x1

    #@6
    .line 1320
    :cond_6
    :goto_6
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 1321
    invoke-static {}, Landroid/os/StrictMode;->clearGatheredViolations()V

    #@c
    .line 1322
    if-nez v0, :cond_33

    #@e
    .line 1323
    instance-of v1, p1, Ljava/lang/RuntimeException;

    #@10
    if-eqz v1, :cond_2d

    #@12
    .line 1324
    check-cast p1, Ljava/lang/RuntimeException;

    #@14
    .end local p1
    throw p1

    #@15
    .line 1311
    .restart local p1
    :cond_15
    instance-of v1, p1, Landroid/os/BadParcelableException;

    #@17
    if-eqz v1, :cond_1b

    #@19
    .line 1312
    const/4 v0, -0x2

    #@1a
    goto :goto_6

    #@1b
    .line 1313
    :cond_1b
    instance-of v1, p1, Ljava/lang/IllegalArgumentException;

    #@1d
    if-eqz v1, :cond_21

    #@1f
    .line 1314
    const/4 v0, -0x3

    #@20
    goto :goto_6

    #@21
    .line 1315
    :cond_21
    instance-of v1, p1, Ljava/lang/NullPointerException;

    #@23
    if-eqz v1, :cond_27

    #@25
    .line 1316
    const/4 v0, -0x4

    #@26
    goto :goto_6

    #@27
    .line 1317
    :cond_27
    instance-of v1, p1, Ljava/lang/IllegalStateException;

    #@29
    if-eqz v1, :cond_6

    #@2b
    .line 1318
    const/4 v0, -0x5

    #@2c
    goto :goto_6

    #@2d
    .line 1326
    :cond_2d
    new-instance v1, Ljava/lang/RuntimeException;

    #@2f
    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@32
    throw v1

    #@33
    .line 1328
    :cond_33
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3a
    .line 1329
    return-void
.end method

.method public final writeFileDescriptor(Ljava/io/FileDescriptor;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 552
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteFileDescriptor(ILjava/io/FileDescriptor;)V

    #@5
    .line 553
    return-void
.end method

.method public final writeFloat(F)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 497
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteFloat(IF)V

    #@5
    .line 498
    return-void
.end method

.method public final writeFloatArray([F)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 831
    if-eqz p1, :cond_11

    #@2
    .line 832
    array-length v0, p1

    #@3
    .line 833
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 834
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 835
    aget v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeFloat(F)V

    #@e
    .line 834
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 838
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 840
    :cond_15
    return-void
.end method

.method public final writeInt(I)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 481
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteInt(II)V

    #@5
    .line 482
    return-void
.end method

.method public final writeIntArray([I)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 758
    if-eqz p1, :cond_11

    #@2
    .line 759
    array-length v0, p1

    #@3
    .line 760
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 761
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 762
    aget v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 761
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 765
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 767
    :cond_15
    return-void
.end method

.method public final writeInterfaceToken(Ljava/lang/String;)V
    .registers 3
    .parameter "interfaceName"

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteInterfaceToken(ILjava/lang/String;)V

    #@5
    .line 445
    return-void
.end method

.method public final writeList(Ljava/util/List;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 614
    if-nez p1, :cond_7

    #@2
    .line 615
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 625
    :cond_6
    return-void

    #@7
    .line 618
    :cond_7
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 619
    .local v0, N:I
    const/4 v1, 0x0

    #@c
    .line 620
    .local v1, i:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 621
    :goto_f
    if-ge v1, v0, :cond_6

    #@11
    .line 622
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@18
    .line 623
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_f
.end method

.method public final writeLong(J)V
    .registers 4
    .parameter "val"

    #@0
    .prologue
    .line 489
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/os/Parcel;->nativeWriteLong(IJ)V

    #@5
    .line 490
    return-void
.end method

.method public final writeLongArray([J)V
    .registers 6
    .parameter "val"

    #@0
    .prologue
    .line 794
    if-eqz p1, :cond_11

    #@2
    .line 795
    array-length v0, p1

    #@3
    .line 796
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 797
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 798
    aget-wide v2, p1, v1

    #@b
    invoke-virtual {p0, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@e
    .line 797
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 801
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 803
    :cond_15
    return-void
.end method

.method public final writeMap(Ljava/util/Map;)V
    .registers 2
    .parameter "val"

    #@0
    .prologue
    .line 575
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeMapInternal(Ljava/util/Map;)V

    #@3
    .line 576
    return-void
.end method

.method writeMapInternal(Ljava/util/Map;)V
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 583
    .local p1, val:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p1, :cond_7

    #@2
    .line 584
    const/4 v3, -0x1

    #@3
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 593
    :cond_6
    return-void

    #@7
    .line 587
    :cond_7
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@a
    move-result-object v1

    #@b
    .line 588
    .local v1, entries:Ljava/util/Set;,"Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->size()I

    #@e
    move-result v3

    #@f
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 589
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    .local v2, i$:Ljava/util/Iterator;
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_6

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/util/Map$Entry;

    #@22
    .line 590
    .local v0, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@29
    .line 591
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@30
    goto :goto_16
.end method

.method public final writeNoException()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1352
    invoke-static {}, Landroid/os/StrictMode;->hasGatheredViolations()Z

    #@4
    move-result v2

    #@5
    if-eqz v2, :cond_26

    #@7
    .line 1353
    const/16 v2, -0x80

    #@9
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 1354
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    #@f
    move-result v1

    #@10
    .line 1355
    .local v1, sizePosition:I
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1356
    invoke-static {p0}, Landroid/os/StrictMode;->writeGatheredViolationsToParcel(Landroid/os/Parcel;)V

    #@16
    .line 1357
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    #@19
    move-result v0

    #@1a
    .line 1358
    .local v0, payloadPosition:I
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@1d
    .line 1359
    sub-int v2, v0, v1

    #@1f
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 1360
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    #@25
    .line 1364
    .end local v0           #payloadPosition:I
    .end local v1           #sizePosition:I
    :goto_25
    return-void

    #@26
    .line 1362
    :cond_26
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_25
.end method

.method public final writeParcelable(Landroid/os/Parcelable;I)V
    .registers 5
    .parameter "p"
    .parameter "parcelableFlags"

    #@0
    .prologue
    .line 1248
    if-nez p1, :cond_7

    #@2
    .line 1249
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6
    .line 1255
    :goto_6
    return-void

    #@7
    .line 1252
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 1253
    .local v0, name:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 1254
    invoke-interface {p1, p0, p2}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    #@15
    goto :goto_6
.end method

.method public final writeParcelableArray([Landroid/os/Parcelable;I)V
    .registers 6
    .parameter
    .parameter "parcelableFlags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">([TT;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1932
    .local p1, value:[Landroid/os/Parcelable;,"[TT;"
    if-eqz p1, :cond_11

    #@2
    .line 1933
    array-length v0, p1

    #@3
    .line 1934
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1935
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 1936
    aget-object v2, p1, v1

    #@b
    invoke-virtual {p0, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@e
    .line 1935
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 1939
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 1941
    :cond_15
    return-void
.end method

.method public final writeSerializable(Ljava/io/Serializable;)V
    .registers 9
    .parameter "s"

    #@0
    .prologue
    .line 1264
    if-nez p1, :cond_7

    #@2
    .line 1265
    const/4 v4, 0x0

    #@3
    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6
    .line 1283
    :goto_6
    return-void

    #@7
    .line 1268
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v4

    #@b
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 1269
    .local v2, name:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 1271
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    #@14
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@17
    .line 1273
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    :try_start_17
    new-instance v3, Ljava/io/ObjectOutputStream;

    #@19
    invoke-direct {v3, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@1c
    .line 1274
    .local v3, oos:Ljava/io/ObjectOutputStream;
    invoke-virtual {v3, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    #@1f
    .line 1275
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V

    #@22
    .line 1277
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@25
    move-result-object v4

    #@26
    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeByteArray([B)V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_29} :catch_2a

    #@29
    goto :goto_6

    #@2a
    .line 1278
    .end local v3           #oos:Ljava/io/ObjectOutputStream;
    :catch_2a
    move-exception v1

    #@2b
    .line 1279
    .local v1, ioe:Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@2d
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "Parcelable encountered IOException writing serializable object (name = "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    const-string v6, ")"

    #@3e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v5

    #@46
    invoke-direct {v4, v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@49
    throw v4
.end method

.method public final writeSparseArray(Landroid/util/SparseArray;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 653
    .local p1, val:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/Object;>;"
    if-nez p1, :cond_7

    #@2
    .line 654
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 665
    :cond_6
    return-void

    #@7
    .line 657
    :cond_7
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    #@a
    move-result v0

    #@b
    .line 658
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 659
    const/4 v1, 0x0

    #@f
    .line 660
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_6

    #@11
    .line 661
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@14
    move-result v2

    #@15
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 662
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    #@1f
    .line 663
    add-int/lit8 v1, v1, 0x1

    #@21
    goto :goto_f
.end method

.method public final writeSparseBooleanArray(Landroid/util/SparseBooleanArray;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 668
    if-nez p1, :cond_7

    #@2
    .line 669
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 680
    :cond_6
    return-void

    #@7
    .line 672
    :cond_7
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    #@a
    move-result v0

    #@b
    .line 673
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 674
    const/4 v1, 0x0

    #@f
    .line 675
    .local v1, i:I
    :goto_f
    if-ge v1, v0, :cond_6

    #@11
    .line 676
    invoke-virtual {p1, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    #@14
    move-result v2

    #@15
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 677
    invoke-virtual {p1, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_26

    #@1e
    const/4 v2, 0x1

    #@1f
    :goto_1f
    int-to-byte v2, v2

    #@20
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeByte(B)V

    #@23
    .line 678
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_f

    #@26
    .line 677
    :cond_26
    const/4 v2, 0x0

    #@27
    goto :goto_1f
.end method

.method public final writeString(Ljava/lang/String;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 513
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteString(ILjava/lang/String;)V

    #@5
    .line 514
    return-void
.end method

.method public final writeStringArray([Ljava/lang/String;)V
    .registers 5
    .parameter "val"

    #@0
    .prologue
    .line 905
    if-eqz p1, :cond_11

    #@2
    .line 906
    array-length v0, p1

    #@3
    .line 907
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 908
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_15

    #@9
    .line 909
    aget-object v2, p1, v1

    #@b
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    .line 908
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 912
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_11
    const/4 v2, -0x1

    #@12
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 914
    :cond_15
    return-void
.end method

.method public final writeStringList(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1037
    .local p1, val:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_7

    #@2
    .line 1038
    const/4 v2, -0x1

    #@3
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1048
    :cond_6
    return-void

    #@7
    .line 1041
    :cond_7
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@a
    move-result v0

    #@b
    .line 1042
    .local v0, N:I
    const/4 v1, 0x0

    #@c
    .line 1043
    .local v1, i:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 1044
    :goto_f
    if-ge v1, v0, :cond_6

    #@11
    .line 1045
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v2

    #@15
    check-cast v2, Ljava/lang/String;

    #@17
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 1046
    add-int/lit8 v1, v1, 0x1

    #@1c
    goto :goto_f
.end method

.method public final writeStrongBinder(Landroid/os/IBinder;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 530
    iget v0, p0, Landroid/os/Parcel;->mNativePtr:I

    #@2
    invoke-static {v0, p1}, Landroid/os/Parcel;->nativeWriteStrongBinder(ILandroid/os/IBinder;)V

    #@5
    .line 531
    return-void
.end method

.method public final writeStrongInterface(Landroid/os/IInterface;)V
    .registers 3
    .parameter "val"

    #@0
    .prologue
    .line 538
    if-nez p1, :cond_7

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@6
    .line 539
    return-void

    #@7
    .line 538
    :cond_7
    invoke-interface {p1}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    goto :goto_3
.end method

.method public final writeTypedArray([Landroid/os/Parcelable;I)V
    .registers 7
    .parameter
    .parameter "parcelableFlags"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">([TT;I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1095
    .local p1, val:[Landroid/os/Parcelable;,"[TT;"
    if-eqz p1, :cond_1c

    #@2
    .line 1096
    array-length v0, p1

    #@3
    .line 1097
    .local v0, N:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 1098
    const/4 v1, 0x0

    #@7
    .local v1, i:I
    :goto_7
    if-ge v1, v0, :cond_20

    #@9
    .line 1099
    aget-object v2, p1, v1

    #@b
    .line 1100
    .local v2, item:Landroid/os/Parcelable;,"TT;"
    if-eqz v2, :cond_17

    #@d
    .line 1101
    const/4 v3, 0x1

    #@e
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 1102
    invoke-interface {v2, p0, p2}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    #@14
    .line 1098
    :goto_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_7

    #@17
    .line 1104
    :cond_17
    const/4 v3, 0x0

    #@18
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    goto :goto_14

    #@1c
    .line 1108
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #item:Landroid/os/Parcelable;,"TT;"
    :cond_1c
    const/4 v3, -0x1

    #@1d
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 1110
    :cond_20
    return-void
.end method

.method public final writeTypedList(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, val:Ljava/util/List;,"Ljava/util/List<TT;>;"
    const/4 v4, 0x0

    #@1
    .line 1006
    if-nez p1, :cond_8

    #@3
    .line 1007
    const/4 v3, -0x1

    #@4
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 1023
    :cond_7
    return-void

    #@8
    .line 1010
    :cond_8
    invoke-interface {p1}, Ljava/util/List;->size()I

    #@b
    move-result v0

    #@c
    .line 1011
    .local v0, N:I
    const/4 v1, 0x0

    #@d
    .line 1012
    .local v1, i:I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1013
    :goto_10
    if-ge v1, v0, :cond_7

    #@12
    .line 1014
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@15
    move-result-object v2

    #@16
    check-cast v2, Landroid/os/Parcelable;

    #@18
    .line 1015
    .local v2, item:Landroid/os/Parcelable;,"TT;"
    if-eqz v2, :cond_24

    #@1a
    .line 1016
    const/4 v3, 0x1

    #@1b
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 1017
    invoke-interface {v2, p0, v4}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 1021
    :goto_21
    add-int/lit8 v1, v1, 0x1

    #@23
    .line 1022
    goto :goto_10

    #@24
    .line 1019
    :cond_24
    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    goto :goto_21
.end method

.method public final writeValue(Ljava/lang/Object;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1156
    if-nez p1, :cond_9

    #@4
    .line 1157
    const/4 v0, -0x1

    #@5
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 1237
    .end local p1
    :goto_8
    return-void

    #@9
    .line 1158
    .restart local p1
    :cond_9
    instance-of v2, p1, Ljava/lang/String;

    #@b
    if-eqz v2, :cond_16

    #@d
    .line 1159
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 1160
    check-cast p1, Ljava/lang/String;

    #@12
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@15
    goto :goto_8

    #@16
    .line 1161
    .restart local p1
    :cond_16
    instance-of v2, p1, Ljava/lang/Integer;

    #@18
    if-eqz v2, :cond_27

    #@1a
    .line 1162
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 1163
    check-cast p1, Ljava/lang/Integer;

    #@1f
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    #@22
    move-result v0

    #@23
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@26
    goto :goto_8

    #@27
    .line 1164
    .restart local p1
    :cond_27
    instance-of v2, p1, Ljava/util/Map;

    #@29
    if-eqz v2, :cond_35

    #@2b
    .line 1165
    const/4 v0, 0x2

    #@2c
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 1166
    check-cast p1, Ljava/util/Map;

    #@31
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    #@34
    goto :goto_8

    #@35
    .line 1167
    .restart local p1
    :cond_35
    instance-of v2, p1, Landroid/os/Bundle;

    #@37
    if-eqz v2, :cond_43

    #@39
    .line 1169
    const/4 v0, 0x3

    #@3a
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@3d
    .line 1170
    check-cast p1, Landroid/os/Bundle;

    #@3f
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@42
    goto :goto_8

    #@43
    .line 1171
    .restart local p1
    :cond_43
    instance-of v2, p1, Landroid/os/Parcelable;

    #@45
    if-eqz v2, :cond_51

    #@47
    .line 1172
    const/4 v0, 0x4

    #@48
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4b
    .line 1173
    check-cast p1, Landroid/os/Parcelable;

    #@4d
    .end local p1
    invoke-virtual {p0, p1, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@50
    goto :goto_8

    #@51
    .line 1174
    .restart local p1
    :cond_51
    instance-of v2, p1, Ljava/lang/Short;

    #@53
    if-eqz v2, :cond_63

    #@55
    .line 1175
    const/4 v0, 0x5

    #@56
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@59
    .line 1176
    check-cast p1, Ljava/lang/Short;

    #@5b
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Short;->intValue()I

    #@5e
    move-result v0

    #@5f
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@62
    goto :goto_8

    #@63
    .line 1177
    .restart local p1
    :cond_63
    instance-of v2, p1, Ljava/lang/Long;

    #@65
    if-eqz v2, :cond_75

    #@67
    .line 1178
    const/4 v0, 0x6

    #@68
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6b
    .line 1179
    check-cast p1, Ljava/lang/Long;

    #@6d
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    #@70
    move-result-wide v0

    #@71
    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@74
    goto :goto_8

    #@75
    .line 1180
    .restart local p1
    :cond_75
    instance-of v2, p1, Ljava/lang/Float;

    #@77
    if-eqz v2, :cond_87

    #@79
    .line 1181
    const/4 v0, 0x7

    #@7a
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7d
    .line 1182
    check-cast p1, Ljava/lang/Float;

    #@7f
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    #@82
    move-result v0

    #@83
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@86
    goto :goto_8

    #@87
    .line 1183
    .restart local p1
    :cond_87
    instance-of v2, p1, Ljava/lang/Double;

    #@89
    if-eqz v2, :cond_9b

    #@8b
    .line 1184
    const/16 v0, 0x8

    #@8d
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@90
    .line 1185
    check-cast p1, Ljava/lang/Double;

    #@92
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    #@95
    move-result-wide v0

    #@96
    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    #@99
    goto/16 :goto_8

    #@9b
    .line 1186
    .restart local p1
    :cond_9b
    instance-of v2, p1, Ljava/lang/Boolean;

    #@9d
    if-eqz v2, :cond_b3

    #@9f
    .line 1187
    const/16 v2, 0x9

    #@a1
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@a4
    .line 1188
    check-cast p1, Ljava/lang/Boolean;

    #@a6
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    #@a9
    move-result v2

    #@aa
    if-eqz v2, :cond_b1

    #@ac
    :goto_ac
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@af
    goto/16 :goto_8

    #@b1
    :cond_b1
    move v0, v1

    #@b2
    goto :goto_ac

    #@b3
    .line 1189
    .restart local p1
    :cond_b3
    instance-of v0, p1, Ljava/lang/CharSequence;

    #@b5
    if-eqz v0, :cond_c3

    #@b7
    .line 1191
    const/16 v0, 0xa

    #@b9
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@bc
    .line 1192
    check-cast p1, Ljava/lang/CharSequence;

    #@be
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeCharSequence(Ljava/lang/CharSequence;)V

    #@c1
    goto/16 :goto_8

    #@c3
    .line 1193
    .restart local p1
    :cond_c3
    instance-of v0, p1, Ljava/util/List;

    #@c5
    if-eqz v0, :cond_d3

    #@c7
    .line 1194
    const/16 v0, 0xb

    #@c9
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@cc
    .line 1195
    check-cast p1, Ljava/util/List;

    #@ce
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    #@d1
    goto/16 :goto_8

    #@d3
    .line 1196
    .restart local p1
    :cond_d3
    instance-of v0, p1, Landroid/util/SparseArray;

    #@d5
    if-eqz v0, :cond_e3

    #@d7
    .line 1197
    const/16 v0, 0xc

    #@d9
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@dc
    .line 1198
    check-cast p1, Landroid/util/SparseArray;

    #@de
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeSparseArray(Landroid/util/SparseArray;)V

    #@e1
    goto/16 :goto_8

    #@e3
    .line 1199
    .restart local p1
    :cond_e3
    instance-of v0, p1, [Z

    #@e5
    if-eqz v0, :cond_f5

    #@e7
    .line 1200
    const/16 v0, 0x17

    #@e9
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@ec
    .line 1201
    check-cast p1, [Z

    #@ee
    .end local p1
    check-cast p1, [Z

    #@f0
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    #@f3
    goto/16 :goto_8

    #@f5
    .line 1202
    .restart local p1
    :cond_f5
    instance-of v0, p1, [B

    #@f7
    if-eqz v0, :cond_107

    #@f9
    .line 1203
    const/16 v0, 0xd

    #@fb
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@fe
    .line 1204
    check-cast p1, [B

    #@100
    .end local p1
    check-cast p1, [B

    #@102
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    #@105
    goto/16 :goto_8

    #@107
    .line 1205
    .restart local p1
    :cond_107
    instance-of v0, p1, [Ljava/lang/String;

    #@109
    if-eqz v0, :cond_119

    #@10b
    .line 1206
    const/16 v0, 0xe

    #@10d
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@110
    .line 1207
    check-cast p1, [Ljava/lang/String;

    #@112
    .end local p1
    check-cast p1, [Ljava/lang/String;

    #@114
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@117
    goto/16 :goto_8

    #@119
    .line 1208
    .restart local p1
    :cond_119
    instance-of v0, p1, [Ljava/lang/CharSequence;

    #@11b
    if-eqz v0, :cond_12b

    #@11d
    .line 1210
    const/16 v0, 0x18

    #@11f
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@122
    .line 1211
    check-cast p1, [Ljava/lang/CharSequence;

    #@124
    .end local p1
    check-cast p1, [Ljava/lang/CharSequence;

    #@126
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeCharSequenceArray([Ljava/lang/CharSequence;)V

    #@129
    goto/16 :goto_8

    #@12b
    .line 1212
    .restart local p1
    :cond_12b
    instance-of v0, p1, Landroid/os/IBinder;

    #@12d
    if-eqz v0, :cond_13b

    #@12f
    .line 1213
    const/16 v0, 0xf

    #@131
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@134
    .line 1214
    check-cast p1, Landroid/os/IBinder;

    #@136
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@139
    goto/16 :goto_8

    #@13b
    .line 1215
    .restart local p1
    :cond_13b
    instance-of v0, p1, [Landroid/os/Parcelable;

    #@13d
    if-eqz v0, :cond_14d

    #@13f
    .line 1216
    const/16 v0, 0x10

    #@141
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@144
    .line 1217
    check-cast p1, [Landroid/os/Parcelable;

    #@146
    .end local p1
    check-cast p1, [Landroid/os/Parcelable;

    #@148
    invoke-virtual {p0, p1, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    #@14b
    goto/16 :goto_8

    #@14d
    .line 1218
    .restart local p1
    :cond_14d
    instance-of v0, p1, [Ljava/lang/Object;

    #@14f
    if-eqz v0, :cond_15f

    #@151
    .line 1219
    const/16 v0, 0x11

    #@153
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@156
    .line 1220
    check-cast p1, [Ljava/lang/Object;

    #@158
    .end local p1
    check-cast p1, [Ljava/lang/Object;

    #@15a
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    #@15d
    goto/16 :goto_8

    #@15f
    .line 1221
    .restart local p1
    :cond_15f
    instance-of v0, p1, [I

    #@161
    if-eqz v0, :cond_171

    #@163
    .line 1222
    const/16 v0, 0x12

    #@165
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@168
    .line 1223
    check-cast p1, [I

    #@16a
    .end local p1
    check-cast p1, [I

    #@16c
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@16f
    goto/16 :goto_8

    #@171
    .line 1224
    .restart local p1
    :cond_171
    instance-of v0, p1, [J

    #@173
    if-eqz v0, :cond_183

    #@175
    .line 1225
    const/16 v0, 0x13

    #@177
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17a
    .line 1226
    check-cast p1, [J

    #@17c
    .end local p1
    check-cast p1, [J

    #@17e
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeLongArray([J)V

    #@181
    goto/16 :goto_8

    #@183
    .line 1227
    .restart local p1
    :cond_183
    instance-of v0, p1, Ljava/lang/Byte;

    #@185
    if-eqz v0, :cond_197

    #@187
    .line 1228
    const/16 v0, 0x14

    #@189
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@18c
    .line 1229
    check-cast p1, Ljava/lang/Byte;

    #@18e
    .end local p1
    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    #@191
    move-result v0

    #@192
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@195
    goto/16 :goto_8

    #@197
    .line 1230
    .restart local p1
    :cond_197
    instance-of v0, p1, Ljava/io/Serializable;

    #@199
    if-eqz v0, :cond_1a7

    #@19b
    .line 1232
    const/16 v0, 0x15

    #@19d
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1a0
    .line 1233
    check-cast p1, Ljava/io/Serializable;

    #@1a2
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    #@1a5
    goto/16 :goto_8

    #@1a7
    .line 1235
    .restart local p1
    :cond_1a7
    new-instance v0, Ljava/lang/RuntimeException;

    #@1a9
    new-instance v1, Ljava/lang/StringBuilder;

    #@1ab
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1ae
    const-string v2, "Parcel: unable to marshal value "

    #@1b0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b3
    move-result-object v1

    #@1b4
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b7
    move-result-object v1

    #@1b8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bb
    move-result-object v1

    #@1bc
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1bf
    throw v0
.end method
