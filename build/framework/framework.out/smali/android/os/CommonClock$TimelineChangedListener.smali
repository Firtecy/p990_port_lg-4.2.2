.class Landroid/os/CommonClock$TimelineChangedListener;
.super Landroid/os/Binder;
.source "CommonClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/CommonClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimelineChangedListener"
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.ICommonClockListener"


# instance fields
.field final synthetic this$0:Landroid/os/CommonClock;


# direct methods
.method private constructor <init>(Landroid/os/CommonClock;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 323
    iput-object p1, p0, Landroid/os/CommonClock$TimelineChangedListener;->this$0:Landroid/os/CommonClock;

    #@2
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/CommonClock;Landroid/os/CommonClock$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 323
    invoke-direct {p0, p1}, Landroid/os/CommonClock$TimelineChangedListener;-><init>(Landroid/os/CommonClock;)V

    #@3
    return-void
.end method


# virtual methods
.method protected onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 327
    packed-switch p1, :pswitch_data_30

    #@3
    .line 338
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 329
    :pswitch_8
    const-string v2, "android.os.ICommonClockListener"

    #@a
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d
    .line 330
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@10
    move-result-wide v0

    #@11
    .line 331
    .local v0, timelineId:J
    iget-object v2, p0, Landroid/os/CommonClock$TimelineChangedListener;->this$0:Landroid/os/CommonClock;

    #@13
    invoke-static {v2}, Landroid/os/CommonClock;->access$000(Landroid/os/CommonClock;)Ljava/lang/Object;

    #@16
    move-result-object v3

    #@17
    monitor-enter v3

    #@18
    .line 332
    :try_start_18
    iget-object v2, p0, Landroid/os/CommonClock$TimelineChangedListener;->this$0:Landroid/os/CommonClock;

    #@1a
    invoke-static {v2}, Landroid/os/CommonClock;->access$200(Landroid/os/CommonClock;)Landroid/os/CommonClock$OnTimelineChangedListener;

    #@1d
    move-result-object v2

    #@1e
    if-eqz v2, :cond_29

    #@20
    .line 333
    iget-object v2, p0, Landroid/os/CommonClock$TimelineChangedListener;->this$0:Landroid/os/CommonClock;

    #@22
    invoke-static {v2}, Landroid/os/CommonClock;->access$200(Landroid/os/CommonClock;)Landroid/os/CommonClock$OnTimelineChangedListener;

    #@25
    move-result-object v2

    #@26
    invoke-interface {v2, v0, v1}, Landroid/os/CommonClock$OnTimelineChangedListener;->onTimelineChanged(J)V

    #@29
    .line 334
    :cond_29
    monitor-exit v3

    #@2a
    .line 335
    const/4 v2, 0x1

    #@2b
    goto :goto_7

    #@2c
    .line 334
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_18 .. :try_end_2e} :catchall_2c

    #@2e
    throw v2

    #@2f
    .line 327
    nop

    #@30
    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method
