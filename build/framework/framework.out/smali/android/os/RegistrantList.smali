.class public Landroid/os/RegistrantList;
.super Ljava/lang/Object;
.source "RegistrantList.java"


# instance fields
.field registrants:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@a
    return-void
.end method

.method private declared-synchronized internalNotifyRegistrants(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 7
    .parameter "result"
    .parameter "exception"

    #@0
    .prologue
    .line 78
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :try_start_2
    iget-object v3, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v2

    #@8
    .local v2, s:I
    :goto_8
    if-ge v0, v2, :cond_18

    #@a
    .line 79
    iget-object v3, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/os/Registrant;

    #@12
    .line 80
    .local v1, r:Landroid/os/Registrant;
    invoke-virtual {v1, p1, p2}, Landroid/os/Registrant;->internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_1a

    #@15
    .line 78
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_8

    #@18
    .line 82
    .end local v1           #r:Landroid/os/Registrant;
    :cond_18
    monitor-exit p0

    #@19
    return-void

    #@1a
    .line 78
    .end local v2           #s:I
    :catchall_1a
    move-exception v3

    #@1b
    monitor-exit p0

    #@1c
    throw v3
.end method


# virtual methods
.method public declared-synchronized add(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 33
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Landroid/os/Registrant;

    #@3
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6
    invoke-virtual {p0, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 34
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 33
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized add(Landroid/os/Registrant;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 47
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0}, Landroid/os/RegistrantList;->removeCleared()V

    #@4
    .line 48
    iget-object v0, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    #@9
    .line 49
    monitor-exit p0

    #@a
    return-void

    #@b
    .line 47
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized addUnique(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 40
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@4
    .line 41
    new-instance v0, Landroid/os/Registrant;

    #@6
    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@9
    invoke-virtual {p0, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    #@c
    .line 42
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 40
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized get(I)Ljava/lang/Object;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 72
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result-object v0

    #@7
    monitor-exit p0

    #@8
    return-object v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method

.method public notifyException(Ljava/lang/Throwable;)V
    .registers 3
    .parameter "exception"

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Landroid/os/RegistrantList;->internalNotifyRegistrants(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 94
    return-void
.end method

.method public notifyRegistrants()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 87
    invoke-direct {p0, v0, v0}, Landroid/os/RegistrantList;->internalNotifyRegistrants(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 88
    return-void
.end method

.method public notifyRegistrants(Landroid/os/AsyncResult;)V
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 106
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4
    invoke-direct {p0, v0, v1}, Landroid/os/RegistrantList;->internalNotifyRegistrants(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@7
    .line 107
    return-void
.end method

.method public notifyResult(Ljava/lang/Object;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 99
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/os/RegistrantList;->internalNotifyRegistrants(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 100
    return-void
.end method

.method public declared-synchronized remove(Landroid/os/Handler;)V
    .registers 7
    .parameter "h"

    #@0
    .prologue
    .line 112
    monitor-enter p0

    #@1
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :try_start_2
    iget-object v4, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    .local v3, s:I
    :goto_8
    if-ge v0, v3, :cond_20

    #@a
    .line 113
    iget-object v4, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/os/Registrant;

    #@12
    .line 116
    .local v1, r:Landroid/os/Registrant;
    invoke-virtual {v1}, Landroid/os/Registrant;->getHandler()Landroid/os/Handler;

    #@15
    move-result-object v2

    #@16
    .line 121
    .local v2, rh:Landroid/os/Handler;
    if-eqz v2, :cond_1a

    #@18
    if-ne v2, p1, :cond_1d

    #@1a
    .line 122
    :cond_1a
    invoke-virtual {v1}, Landroid/os/Registrant;->clear()V

    #@1d
    .line 112
    :cond_1d
    add-int/lit8 v0, v0, 0x1

    #@1f
    goto :goto_8

    #@20
    .line 126
    .end local v1           #r:Landroid/os/Registrant;
    .end local v2           #rh:Landroid/os/Handler;
    :cond_20
    invoke-virtual {p0}, Landroid/os/RegistrantList;->removeCleared()V
    :try_end_23
    .catchall {:try_start_2 .. :try_end_23} :catchall_25

    #@23
    .line 127
    monitor-exit p0

    #@24
    return-void

    #@25
    .line 112
    .end local v3           #s:I
    :catchall_25
    move-exception v4

    #@26
    monitor-exit p0

    #@27
    throw v4
.end method

.method public declared-synchronized removeCleared()V
    .registers 4

    #@0
    .prologue
    .line 54
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    add-int/lit8 v0, v2, -0x1

    #@9
    .local v0, i:I
    :goto_9
    if-ltz v0, :cond_1f

    #@b
    .line 55
    iget-object v2, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/os/Registrant;

    #@13
    .line 57
    .local v1, r:Landroid/os/Registrant;
    iget-object v2, v1, Landroid/os/Registrant;->refH:Ljava/lang/ref/WeakReference;

    #@15
    if-nez v2, :cond_1c

    #@17
    .line 58
    iget-object v2, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_21

    #@1c
    .line 54
    :cond_1c
    add-int/lit8 v0, v0, -0x1

    #@1e
    goto :goto_9

    #@1f
    .line 61
    .end local v1           #r:Landroid/os/Registrant;
    :cond_1f
    monitor-exit p0

    #@20
    return-void

    #@21
    .line 54
    .end local v0           #i:I
    :catchall_21
    move-exception v2

    #@22
    monitor-exit p0

    #@23
    throw v2
.end method

.method public declared-synchronized size()I
    .registers 2

    #@0
    .prologue
    .line 66
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/os/RegistrantList;->registrants:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_9

    #@6
    move-result v0

    #@7
    monitor-exit p0

    #@8
    return v0

    #@9
    :catchall_9
    move-exception v0

    #@a
    monitor-exit p0

    #@b
    throw v0
.end method
