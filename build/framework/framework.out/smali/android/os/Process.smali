.class public Landroid/os/Process;
.super Ljava/lang/Object;
.source "Process.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Process$ProcessStartResult;
    }
.end annotation


# static fields
.field public static final ANDROID_SHARED_MEDIA:Ljava/lang/String; = "com.android.process.media"

.field public static final BLUETOOTH_GID:I = 0x7d0

.field public static final BLUETOOTH_UID:I = 0x3ea

.field public static final DRM_UID:I = 0x3fb

.field public static final FIRST_APPLICATION_UID:I = 0x2710

.field public static final FIRST_ISOLATED_UID:I = 0x182b8

.field public static final FIRST_SHARED_APPLICATION_GID:I = 0xc350

.field public static final FM_RADIO_UID:I = 0x405

.field public static final GOOGLE_SHARED_APP_CONTENT:Ljava/lang/String; = "com.google.process.content"

.field public static final LAST_APPLICATION_UID:I = 0x4e1f

.field public static final LAST_ISOLATED_UID:I = 0x1869f

.field public static final LAST_SHARED_APPLICATION_GID:I = 0xea5f

.field private static final LOG_TAG:Ljava/lang/String; = "Process"

.field public static final LOG_UID:I = 0x3ef

.field public static final MEDIA_RW_GID:I = 0x3ff

.field public static final MEDIA_UID:I = 0x3f5

.field public static final NFCLOCK_UID:I = 0x406

.field public static final NFC_UID:I = 0x403

.field public static final PHONE_UID:I = 0x3e9

.field private static final PROCESS_STATE_FORMAT:[I = null

.field public static final PROC_COMBINE:I = 0x100

.field public static final PROC_OUT_FLOAT:I = 0x4000

.field public static final PROC_OUT_LONG:I = 0x2000

.field public static final PROC_OUT_STRING:I = 0x1000

.field public static final PROC_PARENS:I = 0x200

.field public static final PROC_SPACE_TERM:I = 0x20

.field public static final PROC_TAB_TERM:I = 0x9

.field public static final PROC_TERM_MASK:I = 0xff

.field public static final PROC_ZERO_TERM:I = 0x0

.field public static final SCHED_BATCH:I = 0x3

.field public static final SCHED_FIFO:I = 0x1

.field public static final SCHED_IDLE:I = 0x5

.field public static final SCHED_OTHER:I = 0x0

.field public static final SCHED_RR:I = 0x2

.field public static final SDCARD_RW_GID:I = 0x3f7

.field public static final SHELL_UID:I = 0x7d0

.field public static final SIGNAL_KILL:I = 0x9

.field public static final SIGNAL_QUIT:I = 0x3

.field public static final SIGNAL_USR1:I = 0xa

.field public static final SYSTEM_UID:I = 0x3e8

.field public static final THREAD_GROUP_AUDIO_APP:I = 0x3

.field public static final THREAD_GROUP_AUDIO_SYS:I = 0x4

.field public static final THREAD_GROUP_BG_NONINTERACTIVE:I = 0x0

.field public static final THREAD_GROUP_DEFAULT:I = -0x1

.field private static final THREAD_GROUP_FOREGROUND:I = 0x1

.field public static final THREAD_GROUP_SYSTEM:I = 0x2

.field public static final THREAD_PRIORITY_AUDIO:I = -0x10

.field public static final THREAD_PRIORITY_BACKGROUND:I = 0xa

.field public static final THREAD_PRIORITY_DEFAULT:I = 0x0

.field public static final THREAD_PRIORITY_DISPLAY:I = -0x4

.field public static final THREAD_PRIORITY_FOREGROUND:I = -0x2

.field public static final THREAD_PRIORITY_LESS_FAVORABLE:I = 0x1

.field public static final THREAD_PRIORITY_LOWEST:I = 0x13

.field public static final THREAD_PRIORITY_MORE_FAVORABLE:I = -0x1

.field public static final THREAD_PRIORITY_URGENT_AUDIO:I = -0x13

.field public static final THREAD_PRIORITY_URGENT_DISPLAY:I = -0x8

.field public static final UID_NET_ADMIN:I = 0xbbd

.field public static final UID_NET_RAW:I = 0xbbc

.field public static final VPN_UID:I = 0x3f8

.field public static final WIFI_UID:I = 0x3f2

.field static final ZYGOTE_RETRY_MILLIS:I = 0x1f4

.field private static final ZYGOTE_SOCKET:Ljava/lang/String; = "zygote"

.field static sPreviousZygoteOpenFailed:Z

.field static sZygoteInputStream:Ljava/io/DataInputStream;

.field static sZygoteSocket:Landroid/net/LocalSocket;

.field static sZygoteWriter:Ljava/io/BufferedWriter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1041
    const/4 v0, 0x3

    #@1
    new-array v0, v0, [I

    #@3
    fill-array-data v0, :array_a

    #@6
    sput-object v0, Landroid/os/Process;->PROCESS_STATE_FORMAT:[I

    #@8
    return-void

    #@9
    nop

    #@a
    :array_a
    .array-data 0x4
        0x20t 0x0t 0x0t 0x0t
        0x20t 0x2t 0x0t 0x0t
        0x20t 0x10t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1028
    return-void
.end method

.method public static final native getElapsedCpuTime()J
.end method

.method public static final native getFreeMemory()J
.end method

.method public static final native getGidForName(Ljava/lang/String;)I
.end method

.method public static final getParentPid(I)I
    .registers 6
    .parameter "pid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 752
    new-array v0, v3, [Ljava/lang/String;

    #@4
    const-string v2, "PPid:"

    #@6
    aput-object v2, v0, v4

    #@8
    .line 753
    .local v0, procStatusLabels:[Ljava/lang/String;
    new-array v1, v3, [J

    #@a
    .line 754
    .local v1, procStatusValues:[J
    const-wide/16 v2, -0x1

    #@c
    aput-wide v2, v1, v4

    #@e
    .line 755
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "/proc/"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "/status"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v2, v0, v1}, Landroid/os/Process;->readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V

    #@2a
    .line 756
    aget-wide v2, v1, v4

    #@2c
    long-to-int v2, v2

    #@2d
    return v2
.end method

.method public static final native getPids(Ljava/lang/String;[I)[I
.end method

.method public static final native getPidsForCommands([Ljava/lang/String;)[I
.end method

.method public static final native getPss(I)J
.end method

.method public static final getThreadGroupLeader(I)I
    .registers 6
    .parameter "tid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 767
    new-array v0, v3, [Ljava/lang/String;

    #@4
    const-string v2, "Tgid:"

    #@6
    aput-object v2, v0, v4

    #@8
    .line 768
    .local v0, procStatusLabels:[Ljava/lang/String;
    new-array v1, v3, [J

    #@a
    .line 769
    .local v1, procStatusValues:[J
    const-wide/16 v2, -0x1

    #@c
    aput-wide v2, v1, v4

    #@e
    .line 770
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "/proc/"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "/status"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v2, v0, v1}, Landroid/os/Process;->readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V

    #@2a
    .line 771
    aget-wide v2, v1, v4

    #@2c
    long-to-int v2, v2

    #@2d
    return v2
.end method

.method public static final native getThreadPriority(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public static final native getTotalMemory()J
.end method

.method public static final native getUidForName(Ljava/lang/String;)I
.end method

.method public static final getUidForPid(I)I
    .registers 6
    .parameter "pid"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 738
    new-array v0, v3, [Ljava/lang/String;

    #@4
    const-string v2, "Uid:"

    #@6
    aput-object v2, v0, v4

    #@8
    .line 739
    .local v0, procStatusLabels:[Ljava/lang/String;
    new-array v1, v3, [J

    #@a
    .line 740
    .local v1, procStatusValues:[J
    const-wide/16 v2, -0x1

    #@c
    aput-wide v2, v1, v4

    #@e
    .line 741
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "/proc/"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "/status"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v2, v0, v1}, Landroid/os/Process;->readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V

    #@2a
    .line 742
    aget-wide v2, v1, v4

    #@2c
    long-to-int v2, v2

    #@2d
    return v2
.end method

.method public static final isAlive(I)Z
    .registers 6
    .parameter "pid"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1053
    const/4 v1, 0x0

    #@2
    .line 1054
    .local v1, ret:Z
    const/4 v2, 0x1

    #@3
    new-array v0, v2, [Ljava/lang/String;

    #@5
    .line 1055
    .local v0, processStateString:[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "/proc/"

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, "/stat"

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    sget-object v3, Landroid/os/Process;->PROCESS_STATE_FORMAT:[I

    #@20
    invoke-static {v2, v3, v0, v4, v4}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_33

    #@26
    .line 1057
    const/4 v1, 0x1

    #@27
    .line 1059
    const/4 v2, 0x0

    #@28
    aget-object v2, v0, v2

    #@2a
    const-string v3, "Z"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v2

    #@30
    if-eqz v2, :cond_33

    #@32
    .line 1060
    const/4 v1, 0x0

    #@33
    .line 1063
    :cond_33
    return v1
.end method

.method public static final isIsolated()Z
    .registers 2

    #@0
    .prologue
    .line 713
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v1

    #@4
    invoke-static {v1}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v0

    #@8
    .line 714
    .local v0, uid:I
    const v1, 0x182b8

    #@b
    if-lt v0, v1, :cond_14

    #@d
    const v1, 0x1869f

    #@10
    if-gt v0, v1, :cond_14

    #@12
    const/4 v1, 0x1

    #@13
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method public static final killProcess(I)V
    .registers 2
    .parameter "pid"

    #@0
    .prologue
    .line 936
    const/16 v0, 0x9

    #@2
    invoke-static {p0, v0}, Landroid/os/Process;->sendSignal(II)V

    #@5
    .line 937
    return-void
.end method

.method public static final killProcessQuiet(I)V
    .registers 2
    .parameter "pid"

    #@0
    .prologue
    .line 960
    const/16 v0, 0x9

    #@2
    invoke-static {p0, v0}, Landroid/os/Process;->sendSignalQuiet(II)V

    #@5
    .line 961
    return-void
.end method

.method public static final native myPid()I
.end method

.method public static final native myTid()I
.end method

.method public static final native myUid()I
.end method

.method public static final myUserHandle()Landroid/os/UserHandle;
    .registers 2

    #@0
    .prologue
    .line 705
    new-instance v0, Landroid/os/UserHandle;

    #@2
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@5
    move-result v1

    #@6
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    #@9
    move-result v1

    #@a
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@d
    return-object v0
.end method

.method private static openZygoteSocketIfNeeded()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/ZygoteStartFailedEx;
        }
    .end annotation

    #@0
    .prologue
    .line 447
    sget-boolean v4, Landroid/os/Process;->sPreviousZygoteOpenFailed:Z

    #@2
    if-eqz v4, :cond_6c

    #@4
    .line 452
    const/4 v3, 0x0

    #@5
    .line 462
    .local v3, retryCount:I
    :goto_5
    const/4 v2, 0x0

    #@6
    .line 463
    .local v2, retry:I
    :goto_6
    sget-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@8
    if-nez v4, :cond_5d

    #@a
    add-int/lit8 v4, v3, 0x1

    #@c
    if-ge v2, v4, :cond_5d

    #@e
    .line 466
    if-lez v2, :cond_1c

    #@10
    .line 468
    :try_start_10
    const-string v4, "Zygote"

    #@12
    const-string v5, "Zygote not up yet, sleeping..."

    #@14
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 469
    const-wide/16 v4, 0x1f4

    #@19
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1c
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_1c} :catch_89

    #@1c
    .line 476
    :cond_1c
    :goto_1c
    :try_start_1c
    new-instance v4, Landroid/net/LocalSocket;

    #@1e
    invoke-direct {v4}, Landroid/net/LocalSocket;-><init>()V

    #@21
    sput-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@23
    .line 478
    sget-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@25
    new-instance v5, Landroid/net/LocalSocketAddress;

    #@27
    const-string/jumbo v6, "zygote"

    #@2a
    sget-object v7, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    #@2c
    invoke-direct {v5, v6, v7}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    #@2f
    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    #@32
    .line 481
    new-instance v4, Ljava/io/DataInputStream;

    #@34
    sget-object v5, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@36
    invoke-virtual {v5}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    #@39
    move-result-object v5

    #@3a
    invoke-direct {v4, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@3d
    sput-object v4, Landroid/os/Process;->sZygoteInputStream:Ljava/io/DataInputStream;

    #@3f
    .line 484
    new-instance v4, Ljava/io/BufferedWriter;

    #@41
    new-instance v5, Ljava/io/OutputStreamWriter;

    #@43
    sget-object v6, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@45
    invoke-virtual {v6}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    #@48
    move-result-object v6

    #@49
    invoke-direct {v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    #@4c
    const/16 v6, 0x100

    #@4e
    invoke-direct {v4, v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    #@51
    sput-object v4, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@53
    .line 490
    const-string v4, "Zygote"

    #@55
    const-string v5, "Process: zygote socket opened"

    #@57
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 492
    const/4 v4, 0x0

    #@5b
    sput-boolean v4, Landroid/os/Process;->sPreviousZygoteOpenFailed:Z
    :try_end_5d
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_5d} :catch_6f

    #@5d
    .line 508
    :cond_5d
    sget-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@5f
    if-nez v4, :cond_88

    #@61
    .line 509
    const/4 v4, 0x1

    #@62
    sput-boolean v4, Landroid/os/Process;->sPreviousZygoteOpenFailed:Z

    #@64
    .line 510
    new-instance v4, Landroid/os/ZygoteStartFailedEx;

    #@66
    const-string v5, "connect failed"

    #@68
    invoke-direct {v4, v5}, Landroid/os/ZygoteStartFailedEx;-><init>(Ljava/lang/String;)V

    #@6b
    throw v4

    #@6c
    .line 454
    .end local v2           #retry:I
    .end local v3           #retryCount:I
    :cond_6c
    const/16 v3, 0xa

    #@6e
    .restart local v3       #retryCount:I
    goto :goto_5

    #@6f
    .line 494
    .restart local v2       #retry:I
    :catch_6f
    move-exception v0

    #@70
    .line 495
    .local v0, ex:Ljava/io/IOException;
    sget-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@72
    if-eqz v4, :cond_79

    #@74
    .line 497
    :try_start_74
    sget-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@76
    invoke-virtual {v4}, Landroid/net/LocalSocket;->close()V
    :try_end_79
    .catch Ljava/io/IOException; {:try_start_74 .. :try_end_79} :catch_7f

    #@79
    .line 504
    :cond_79
    :goto_79
    const/4 v4, 0x0

    #@7a
    sput-object v4, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@7c
    .line 464
    add-int/lit8 v2, v2, 0x1

    #@7e
    goto :goto_6

    #@7f
    .line 498
    :catch_7f
    move-exception v1

    #@80
    .line 499
    .local v1, ex2:Ljava/io/IOException;
    const-string v4, "Process"

    #@82
    const-string v5, "I/O exception on close after exception"

    #@84
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@87
    goto :goto_79

    #@88
    .line 512
    .end local v0           #ex:Ljava/io/IOException;
    .end local v1           #ex2:Ljava/io/IOException;
    :cond_88
    return-void

    #@89
    .line 470
    :catch_89
    move-exception v4

    #@8a
    goto :goto_1c
.end method

.method public static final native parseProcLine([BII[I[Ljava/lang/String;[J[F)Z
.end method

.method public static final native readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z
.end method

.method public static final native readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V
.end method

.method public static final native sendSignal(II)V
.end method

.method public static final native sendSignalQuiet(II)V
.end method

.method public static final native setArgV0(Ljava/lang/String;)V
.end method

.method public static final native setCanSelfBackground(Z)V
.end method

.method public static final native setGid(I)I
.end method

.method public static final native setOomAdj(II)Z
.end method

.method public static final native setProcessGroup(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation
.end method

.method public static final native setThreadGroup(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation
.end method

.method public static final native setThreadPriority(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation
.end method

.method public static final native setThreadPriority(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;
        }
    .end annotation
.end method

.method public static final native setThreadScheduler(III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public static final native setUid(I)I
.end method

.method public static final start(Ljava/lang/String;Ljava/lang/String;II[IIIILjava/lang/String;[Ljava/lang/String;)Landroid/os/Process$ProcessStartResult;
    .registers 13
    .parameter "processClass"
    .parameter "niceName"
    .parameter "uid"
    .parameter "gid"
    .parameter "gids"
    .parameter "debugFlags"
    .parameter "mountExternal"
    .parameter "targetSdkVersion"
    .parameter "seInfo"
    .parameter "zygoteArgs"

    #@0
    .prologue
    .line 425
    :try_start_0
    invoke-static/range {p0 .. p9}, Landroid/os/Process;->startViaZygote(Ljava/lang/String;Ljava/lang/String;II[IIIILjava/lang/String;[Ljava/lang/String;)Landroid/os/Process$ProcessStartResult;
    :try_end_3
    .catch Landroid/os/ZygoteStartFailedEx; {:try_start_0 .. :try_end_3} :catch_5

    #@3
    move-result-object v1

    #@4
    return-object v1

    #@5
    .line 427
    :catch_5
    move-exception v0

    #@6
    .line 428
    .local v0, ex:Landroid/os/ZygoteStartFailedEx;
    const-string v1, "Process"

    #@8
    const-string v2, "Starting VM process through Zygote failed"

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 430
    new-instance v1, Ljava/lang/RuntimeException;

    #@f
    const-string v2, "Starting VM process through Zygote failed"

    #@11
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@14
    throw v1
.end method

.method private static startViaZygote(Ljava/lang/String;Ljava/lang/String;II[IIIILjava/lang/String;[Ljava/lang/String;)Landroid/os/Process$ProcessStartResult;
    .registers 22
    .parameter "processClass"
    .parameter "niceName"
    .parameter "uid"
    .parameter "gid"
    .parameter "gids"
    .parameter "debugFlags"
    .parameter "mountExternal"
    .parameter "targetSdkVersion"
    .parameter "seInfo"
    .parameter "extraArgs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/ZygoteStartFailedEx;
        }
    .end annotation

    #@0
    .prologue
    .line 603
    const-class v10, Landroid/os/Process;

    #@2
    monitor-enter v10

    #@3
    .line 604
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    #@5
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@8
    .line 608
    .local v2, argsForZygote:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v9, "--runtime-init"

    #@a
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    .line 609
    new-instance v9, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v11, "--setuid="

    #@14
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v9

    #@18
    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@23
    .line 610
    new-instance v9, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v11, "--setgid="

    #@2a
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v9

    #@32
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v9

    #@36
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 611
    and-int/lit8 v9, p5, 0x10

    #@3b
    if-eqz v9, :cond_42

    #@3d
    .line 612
    const-string v9, "--enable-jni-logging"

    #@3f
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    .line 614
    :cond_42
    and-int/lit8 v9, p5, 0x8

    #@44
    if-eqz v9, :cond_4b

    #@46
    .line 615
    const-string v9, "--enable-safemode"

    #@48
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4b
    .line 617
    :cond_4b
    and-int/lit8 v9, p5, 0x1

    #@4d
    if-eqz v9, :cond_54

    #@4f
    .line 618
    const-string v9, "--enable-debugger"

    #@51
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    .line 620
    :cond_54
    and-int/lit8 v9, p5, 0x2

    #@56
    if-eqz v9, :cond_5d

    #@58
    .line 621
    const-string v9, "--enable-checkjni"

    #@5a
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5d
    .line 623
    :cond_5d
    and-int/lit8 v9, p5, 0x4

    #@5f
    if-eqz v9, :cond_66

    #@61
    .line 624
    const-string v9, "--enable-assert"

    #@63
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@66
    .line 626
    :cond_66
    const/4 v9, 0x2

    #@67
    move/from16 v0, p6

    #@69
    if-ne v0, v9, :cond_ae

    #@6b
    .line 627
    const-string v9, "--mount-external-multiuser"

    #@6d
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@70
    .line 631
    :cond_70
    :goto_70
    new-instance v9, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v11, "--target-sdk-version="

    #@77
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v9

    #@7b
    move/from16 v0, p7

    #@7d
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v9

    #@81
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v9

    #@85
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@88
    .line 637
    if-eqz p4, :cond_c3

    #@8a
    move-object/from16 v0, p4

    #@8c
    array-length v9, v0

    #@8d
    if-lez v9, :cond_c3

    #@8f
    .line 638
    new-instance v7, Ljava/lang/StringBuilder;

    #@91
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    .line 639
    .local v7, sb:Ljava/lang/StringBuilder;
    const-string v9, "--setgroups="

    #@96
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    .line 641
    move-object/from16 v0, p4

    #@9b
    array-length v8, v0

    #@9c
    .line 642
    .local v8, sz:I
    const/4 v4, 0x0

    #@9d
    .local v4, i:I
    :goto_9d
    if-ge v4, v8, :cond_bc

    #@9f
    .line 643
    if-eqz v4, :cond_a6

    #@a1
    .line 644
    const/16 v9, 0x2c

    #@a3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@a6
    .line 646
    :cond_a6
    aget v9, p4, v4

    #@a8
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab
    .line 642
    add-int/lit8 v4, v4, 0x1

    #@ad
    goto :goto_9d

    #@ae
    .line 628
    .end local v4           #i:I
    .end local v7           #sb:Ljava/lang/StringBuilder;
    .end local v8           #sz:I
    :cond_ae
    const/4 v9, 0x3

    #@af
    move/from16 v0, p6

    #@b1
    if-ne v0, v9, :cond_70

    #@b3
    .line 629
    const-string v9, "--mount-external-multiuser-all"

    #@b5
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b8
    goto :goto_70

    #@b9
    .line 669
    .end local v2           #argsForZygote:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_b9
    move-exception v9

    #@ba
    monitor-exit v10
    :try_end_bb
    .catchall {:try_start_3 .. :try_end_bb} :catchall_b9

    #@bb
    throw v9

    #@bc
    .line 649
    .restart local v2       #argsForZygote:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4       #i:I
    .restart local v7       #sb:Ljava/lang/StringBuilder;
    .restart local v8       #sz:I
    :cond_bc
    :try_start_bc
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v9

    #@c0
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c3
    .line 652
    .end local v4           #i:I
    .end local v7           #sb:Ljava/lang/StringBuilder;
    .end local v8           #sz:I
    :cond_c3
    if-eqz p1, :cond_db

    #@c5
    .line 653
    new-instance v9, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v11, "--nice-name="

    #@cc
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v9

    #@d0
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v9

    #@d4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v9

    #@d8
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@db
    .line 656
    :cond_db
    if-eqz p8, :cond_f5

    #@dd
    .line 657
    new-instance v9, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v11, "--seinfo="

    #@e4
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v9

    #@e8
    move-object/from16 v0, p8

    #@ea
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v9

    #@ee
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v9

    #@f2
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f5
    .line 660
    :cond_f5
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@f8
    .line 662
    if-eqz p9, :cond_108

    #@fa
    .line 663
    move-object/from16 v3, p9

    #@fc
    .local v3, arr$:[Ljava/lang/String;
    array-length v6, v3

    #@fd
    .local v6, len$:I
    const/4 v5, 0x0

    #@fe
    .local v5, i$:I
    :goto_fe
    if-ge v5, v6, :cond_108

    #@100
    aget-object v1, v3, v5

    #@102
    .line 664
    .local v1, arg:Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@105
    .line 663
    add-int/lit8 v5, v5, 0x1

    #@107
    goto :goto_fe

    #@108
    .line 668
    .end local v1           #arg:Ljava/lang/String;
    .end local v3           #arr$:[Ljava/lang/String;
    .end local v5           #i$:I
    .end local v6           #len$:I
    :cond_108
    invoke-static {v2}, Landroid/os/Process;->zygoteSendArgsAndGetResult(Ljava/util/ArrayList;)Landroid/os/Process$ProcessStartResult;

    #@10b
    move-result-object v9

    #@10c
    monitor-exit v10
    :try_end_10d
    .catchall {:try_start_bc .. :try_end_10d} :catchall_b9

    #@10d
    return-object v9
.end method

.method public static final supportsProcesses()Z
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 898
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method private static zygoteSendArgsAndGetResult(Ljava/util/ArrayList;)Landroid/os/Process$ProcessStartResult;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Process$ProcessStartResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/ZygoteStartFailedEx;
        }
    .end annotation

    #@0
    .prologue
    .line 524
    .local p0, args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Process;->openZygoteSocketIfNeeded()V

    #@3
    .line 538
    :try_start_3
    sget-object v6, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@5
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v7

    #@9
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v6, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@10
    .line 539
    sget-object v6, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@12
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V

    #@15
    .line 541
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v5

    #@19
    .line 542
    .local v5, sz:I
    const/4 v3, 0x0

    #@1a
    .local v3, i:I
    :goto_1a
    if-ge v3, v5, :cond_52

    #@1c
    .line 543
    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Ljava/lang/String;

    #@22
    .line 544
    .local v0, arg:Ljava/lang/String;
    const/16 v6, 0xa

    #@24
    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    #@27
    move-result v6

    #@28
    if-ltz v6, :cond_45

    #@2a
    .line 545
    new-instance v6, Landroid/os/ZygoteStartFailedEx;

    #@2c
    const-string v7, "embedded newlines not allowed"

    #@2e
    invoke-direct {v6, v7}, Landroid/os/ZygoteStartFailedEx;-><init>(Ljava/lang/String;)V

    #@31
    throw v6
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_32} :catch_32

    #@32
    .line 562
    .end local v0           #arg:Ljava/lang/String;
    .end local v3           #i:I
    .end local v5           #sz:I
    :catch_32
    move-exception v1

    #@33
    .line 564
    .local v1, ex:Ljava/io/IOException;
    :try_start_33
    sget-object v6, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@35
    if-eqz v6, :cond_3c

    #@37
    .line 565
    sget-object v6, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@39
    invoke-virtual {v6}, Landroid/net/LocalSocket;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_3c} :catch_79

    #@3c
    .line 572
    :cond_3c
    :goto_3c
    const/4 v6, 0x0

    #@3d
    sput-object v6, Landroid/os/Process;->sZygoteSocket:Landroid/net/LocalSocket;

    #@3f
    .line 574
    new-instance v6, Landroid/os/ZygoteStartFailedEx;

    #@41
    invoke-direct {v6, v1}, Landroid/os/ZygoteStartFailedEx;-><init>(Ljava/lang/Throwable;)V

    #@44
    throw v6

    #@45
    .line 548
    .end local v1           #ex:Ljava/io/IOException;
    .restart local v0       #arg:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v5       #sz:I
    :cond_45
    :try_start_45
    sget-object v6, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@47
    invoke-virtual {v6, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    #@4a
    .line 549
    sget-object v6, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@4c
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V

    #@4f
    .line 542
    add-int/lit8 v3, v3, 0x1

    #@51
    goto :goto_1a

    #@52
    .line 552
    .end local v0           #arg:Ljava/lang/String;
    :cond_52
    sget-object v6, Landroid/os/Process;->sZygoteWriter:Ljava/io/BufferedWriter;

    #@54
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->flush()V

    #@57
    .line 555
    new-instance v4, Landroid/os/Process$ProcessStartResult;

    #@59
    invoke-direct {v4}, Landroid/os/Process$ProcessStartResult;-><init>()V

    #@5c
    .line 556
    .local v4, result:Landroid/os/Process$ProcessStartResult;
    sget-object v6, Landroid/os/Process;->sZygoteInputStream:Ljava/io/DataInputStream;

    #@5e
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    #@61
    move-result v6

    #@62
    iput v6, v4, Landroid/os/Process$ProcessStartResult;->pid:I

    #@64
    .line 557
    iget v6, v4, Landroid/os/Process$ProcessStartResult;->pid:I

    #@66
    if-gez v6, :cond_70

    #@68
    .line 558
    new-instance v6, Landroid/os/ZygoteStartFailedEx;

    #@6a
    const-string v7, "fork() failed"

    #@6c
    invoke-direct {v6, v7}, Landroid/os/ZygoteStartFailedEx;-><init>(Ljava/lang/String;)V

    #@6f
    throw v6

    #@70
    .line 560
    :cond_70
    sget-object v6, Landroid/os/Process;->sZygoteInputStream:Ljava/io/DataInputStream;

    #@72
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readBoolean()Z

    #@75
    move-result v6

    #@76
    iput-boolean v6, v4, Landroid/os/Process$ProcessStartResult;->usingWrapper:Z
    :try_end_78
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_78} :catch_32

    #@78
    .line 561
    return-object v4

    #@79
    .line 567
    .end local v3           #i:I
    .end local v4           #result:Landroid/os/Process$ProcessStartResult;
    .end local v5           #sz:I
    .restart local v1       #ex:Ljava/io/IOException;
    :catch_79
    move-exception v2

    #@7a
    .line 569
    .local v2, ex2:Ljava/io/IOException;
    const-string v6, "Process"

    #@7c
    const-string v7, "I/O exception on routine close"

    #@7e
    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@81
    goto :goto_3c
.end method
