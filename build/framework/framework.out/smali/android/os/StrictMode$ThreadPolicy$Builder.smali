.class public final Landroid/os/StrictMode$ThreadPolicy$Builder;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode$ThreadPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mMask:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 353
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 345
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@6
    .line 354
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@8
    .line 355
    return-void
.end method

.method public constructor <init>(Landroid/os/StrictMode$ThreadPolicy;)V
    .registers 3
    .parameter "policy"

    #@0
    .prologue
    .line 360
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 345
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@6
    .line 361
    iget v0, p1, Landroid/os/StrictMode$ThreadPolicy;->mask:I

    #@8
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@a
    .line 362
    return-void
.end method

.method private disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 4
    .parameter "bit"

    #@0
    .prologue
    .line 500
    iget v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@2
    xor-int/lit8 v1, p1, -0x1

    #@4
    and-int/2addr v0, v1

    #@5
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@7
    .line 501
    return-object p0
.end method

.method private enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 3
    .parameter "bit"

    #@0
    .prologue
    .line 495
    iget v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@5
    .line 496
    return-object p0
.end method


# virtual methods
.method public build()Landroid/os/StrictMode$ThreadPolicy;
    .registers 4

    #@0
    .prologue
    .line 514
    iget v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@6
    and-int/lit16 v0, v0, 0xf0

    #@8
    if-nez v0, :cond_d

    #@a
    .line 517
    invoke-virtual {p0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@d
    .line 519
    :cond_d
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy;

    #@f
    iget v1, p0, Landroid/os/StrictMode$ThreadPolicy$Builder;->mMask:I

    #@11
    const/4 v2, 0x0

    #@12
    invoke-direct {v0, v1, v2}, Landroid/os/StrictMode$ThreadPolicy;-><init>(ILandroid/os/StrictMode$1;)V

    #@15
    return-object v0
.end method

.method public detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 371
    const/16 v0, 0xf

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectCustomSlowCalls()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 413
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 399
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public detectDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 427
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 385
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public penaltyDeath()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 455
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyDeathOnNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 467
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyDialog()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 442
    const/16 v0, 0x20

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyDropBox()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 491
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyFlashScreen()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 474
    const/16 v0, 0x800

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 481
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->enable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public permitAll()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 378
    const/16 v0, 0xf

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public permitCustomSlowCalls()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 420
    const/16 v0, 0x8

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public permitDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 406
    const/4 v0, 0x2

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public permitDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 434
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public permitNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 392
    const/4 v0, 0x4

    #@1
    invoke-direct {p0, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->disable(I)Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method
