.class public abstract Landroid/os/BatteryStats;
.super Ljava/lang/Object;
.source "BatteryStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/BatteryStats$HistoryPrinter;,
        Landroid/os/BatteryStats$TimerEntry;,
        Landroid/os/BatteryStats$BitDescription;,
        Landroid/os/BatteryStats$HistoryItem;,
        Landroid/os/BatteryStats$Uid;,
        Landroid/os/BatteryStats$Timer;,
        Landroid/os/BatteryStats$Counter;
    }
.end annotation


# static fields
.field private static final APK_DATA:Ljava/lang/String; = "apk"

.field public static final AUDIO_TURNED_ON:I = 0x7

.field private static final BATTERY_DATA:Ljava/lang/String; = "bt"

.field private static final BATTERY_DISCHARGE_DATA:Ljava/lang/String; = "dc"

.field private static final BATTERY_LEVEL_DATA:Ljava/lang/String; = "lv"

.field private static final BATTERY_STATS_CHECKIN_VERSION:I = 0x5

.field private static final BYTES_PER_GB:J = 0x40000000L

.field private static final BYTES_PER_KB:J = 0x400L

.field private static final BYTES_PER_MB:J = 0x100000L

.field public static final DATA_CONNECTION_1xRTT:I = 0x7

.field public static final DATA_CONNECTION_CDMA:I = 0x4

.field private static final DATA_CONNECTION_COUNT_DATA:Ljava/lang/String; = "dcc"

.field public static final DATA_CONNECTION_EDGE:I = 0x2

.field public static final DATA_CONNECTION_EHRPD:I = 0xe

.field public static final DATA_CONNECTION_EVDO_0:I = 0x5

.field public static final DATA_CONNECTION_EVDO_A:I = 0x6

.field public static final DATA_CONNECTION_EVDO_B:I = 0xc

.field public static final DATA_CONNECTION_GPRS:I = 0x1

.field public static final DATA_CONNECTION_HSDPA:I = 0x8

.field public static final DATA_CONNECTION_HSPA:I = 0xa

.field public static final DATA_CONNECTION_HSUPA:I = 0x9

.field public static final DATA_CONNECTION_IDEN:I = 0xb

.field public static final DATA_CONNECTION_LTE:I = 0xd

.field static final DATA_CONNECTION_NAMES:[Ljava/lang/String; = null

.field public static final DATA_CONNECTION_NONE:I = 0x0

.field public static final DATA_CONNECTION_OTHER:I = 0xf

.field private static final DATA_CONNECTION_TIME_DATA:Ljava/lang/String; = "dct"

.field public static final DATA_CONNECTION_UMTS:I = 0x3

.field public static final FULL_WIFI_LOCK:I = 0x5

.field public static final HISTORY_STATE_DESCRIPTIONS:[Landroid/os/BatteryStats$BitDescription; = null

.field private static final KERNEL_WAKELOCK_DATA:Ljava/lang/String; = "kwl"

.field private static final LOCAL_LOGV:Z = false

.field private static final MISC_DATA:Ljava/lang/String; = "m"

.field private static final NETWORK_DATA:Ljava/lang/String; = "nt"

.field public static final NUM_DATA_CONNECTION_TYPES:I = 0x10

.field public static final NUM_SCREEN_BRIGHTNESS_BINS:I = 0x5

.field private static final PROCESS_DATA:Ljava/lang/String; = "pr"

.field public static final SCREEN_BRIGHTNESS_BRIGHT:I = 0x4

.field public static final SCREEN_BRIGHTNESS_DARK:I = 0x0

.field private static final SCREEN_BRIGHTNESS_DATA:Ljava/lang/String; = "br"

.field public static final SCREEN_BRIGHTNESS_DIM:I = 0x1

.field public static final SCREEN_BRIGHTNESS_LIGHT:I = 0x3

.field public static final SCREEN_BRIGHTNESS_MEDIUM:I = 0x2

.field static final SCREEN_BRIGHTNESS_NAMES:[Ljava/lang/String; = null

.field public static final SENSOR:I = 0x3

.field private static final SENSOR_DATA:Ljava/lang/String; = "sr"

.field private static final SIGNAL_SCANNING_TIME_DATA:Ljava/lang/String; = "sst"

.field private static final SIGNAL_STRENGTH_COUNT_DATA:Ljava/lang/String; = "sgc"

.field private static final SIGNAL_STRENGTH_TIME_DATA:Ljava/lang/String; = "sgt"

.field public static final STATS_CURRENT:I = 0x2

.field public static final STATS_LAST:I = 0x1

.field public static final STATS_SINCE_CHARGED:I = 0x0

.field public static final STATS_SINCE_UNPLUGGED:I = 0x3

.field private static final STAT_NAMES:[Ljava/lang/String; = null

.field private static final UID_DATA:Ljava/lang/String; = "uid"

.field private static final USER_ACTIVITY_DATA:Ljava/lang/String; = "ua"

.field public static final VIDEO_TURNED_ON:I = 0x8

.field private static final WAKELOCK_DATA:Ljava/lang/String; = "wl"

.field public static final WAKE_TYPE_FULL:I = 0x1

.field public static final WAKE_TYPE_PARTIAL:I = 0x0

.field public static final WAKE_TYPE_WINDOW:I = 0x2

.field private static final WIFI_DATA:Ljava/lang/String; = "wfl"

.field public static final WIFI_MULTICAST_ENABLED:I = 0x7

.field public static final WIFI_RUNNING:I = 0x4

.field public static final WIFI_SCAN:I = 0x6


# instance fields
.field private final mFormatBuilder:Ljava/lang/StringBuilder;

.field private final mFormatter:Ljava/util/Formatter;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x2

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v6, 0x4

    #@4
    const/4 v8, 0x0

    #@5
    .line 118
    new-array v0, v6, [Ljava/lang/String;

    #@7
    const-string/jumbo v1, "t"

    #@a
    aput-object v1, v0, v8

    #@c
    const-string/jumbo v1, "l"

    #@f
    aput-object v1, v0, v9

    #@11
    const-string v1, "c"

    #@13
    aput-object v1, v0, v10

    #@15
    const-string/jumbo v1, "u"

    #@18
    aput-object v1, v0, v11

    #@1a
    sput-object v0, Landroid/os/BatteryStats;->STAT_NAMES:[Ljava/lang/String;

    #@1c
    .line 764
    const/4 v0, 0x5

    #@1d
    new-array v0, v0, [Ljava/lang/String;

    #@1f
    const-string v1, "dark"

    #@21
    aput-object v1, v0, v8

    #@23
    const-string v1, "dim"

    #@25
    aput-object v1, v0, v9

    #@27
    const-string/jumbo v1, "medium"

    #@2a
    aput-object v1, v0, v10

    #@2c
    const-string/jumbo v1, "light"

    #@2f
    aput-object v1, v0, v11

    #@31
    const-string v1, "bright"

    #@33
    aput-object v1, v0, v6

    #@35
    sput-object v0, Landroid/os/BatteryStats;->SCREEN_BRIGHTNESS_NAMES:[Ljava/lang/String;

    #@37
    .line 831
    const/16 v0, 0x10

    #@39
    new-array v0, v0, [Ljava/lang/String;

    #@3b
    const-string/jumbo v1, "none"

    #@3e
    aput-object v1, v0, v8

    #@40
    const-string v1, "gprs"

    #@42
    aput-object v1, v0, v9

    #@44
    const-string v1, "edge"

    #@46
    aput-object v1, v0, v10

    #@48
    const-string/jumbo v1, "umts"

    #@4b
    aput-object v1, v0, v11

    #@4d
    const-string v1, "cdma"

    #@4f
    aput-object v1, v0, v6

    #@51
    const/4 v1, 0x5

    #@52
    const-string v2, "evdo_0"

    #@54
    aput-object v2, v0, v1

    #@56
    const/4 v1, 0x6

    #@57
    const-string v2, "evdo_A"

    #@59
    aput-object v2, v0, v1

    #@5b
    const/4 v1, 0x7

    #@5c
    const-string v2, "1xrtt"

    #@5e
    aput-object v2, v0, v1

    #@60
    const/16 v1, 0x8

    #@62
    const-string v2, "hsdpa"

    #@64
    aput-object v2, v0, v1

    #@66
    const/16 v1, 0x9

    #@68
    const-string v2, "hsupa"

    #@6a
    aput-object v2, v0, v1

    #@6c
    const/16 v1, 0xa

    #@6e
    const-string v2, "hspa"

    #@70
    aput-object v2, v0, v1

    #@72
    const/16 v1, 0xb

    #@74
    const-string v2, "iden"

    #@76
    aput-object v2, v0, v1

    #@78
    const/16 v1, 0xc

    #@7a
    const-string v2, "evdo_b"

    #@7c
    aput-object v2, v0, v1

    #@7e
    const/16 v1, 0xd

    #@80
    const-string/jumbo v2, "lte"

    #@83
    aput-object v2, v0, v1

    #@85
    const/16 v1, 0xe

    #@87
    const-string v2, "ehrpd"

    #@89
    aput-object v2, v0, v1

    #@8b
    const/16 v1, 0xf

    #@8d
    const-string/jumbo v2, "other"

    #@90
    aput-object v2, v0, v1

    #@92
    sput-object v0, Landroid/os/BatteryStats;->DATA_CONNECTION_NAMES:[Ljava/lang/String;

    #@94
    .line 856
    const/16 v0, 0x13

    #@96
    new-array v0, v0, [Landroid/os/BatteryStats$BitDescription;

    #@98
    new-instance v1, Landroid/os/BatteryStats$BitDescription;

    #@9a
    const/high16 v2, 0x8

    #@9c
    const-string/jumbo v3, "plugged"

    #@9f
    invoke-direct {v1, v2, v3}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@a2
    aput-object v1, v0, v8

    #@a4
    new-instance v1, Landroid/os/BatteryStats$BitDescription;

    #@a6
    const/high16 v2, 0x10

    #@a8
    const-string/jumbo v3, "screen"

    #@ab
    invoke-direct {v1, v2, v3}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@ae
    aput-object v1, v0, v9

    #@b0
    new-instance v1, Landroid/os/BatteryStats$BitDescription;

    #@b2
    const/high16 v2, 0x1000

    #@b4
    const-string v3, "gps"

    #@b6
    invoke-direct {v1, v2, v3}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@b9
    aput-object v1, v0, v10

    #@bb
    new-instance v1, Landroid/os/BatteryStats$BitDescription;

    #@bd
    const/high16 v2, 0x4

    #@bf
    const-string/jumbo v3, "phone_in_call"

    #@c2
    invoke-direct {v1, v2, v3}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@c5
    aput-object v1, v0, v11

    #@c7
    new-instance v1, Landroid/os/BatteryStats$BitDescription;

    #@c9
    const/high16 v2, 0x800

    #@cb
    const-string/jumbo v3, "phone_scanning"

    #@ce
    invoke-direct {v1, v2, v3}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@d1
    aput-object v1, v0, v6

    #@d3
    const/4 v1, 0x5

    #@d4
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@d6
    const/high16 v3, 0x2

    #@d8
    const-string/jumbo v4, "wifi"

    #@db
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@de
    aput-object v2, v0, v1

    #@e0
    const/4 v1, 0x6

    #@e1
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@e3
    const/high16 v3, 0x400

    #@e5
    const-string/jumbo v4, "wifi_running"

    #@e8
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@eb
    aput-object v2, v0, v1

    #@ed
    const/4 v1, 0x7

    #@ee
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@f0
    const/high16 v3, 0x200

    #@f2
    const-string/jumbo v4, "wifi_full_lock"

    #@f5
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@f8
    aput-object v2, v0, v1

    #@fa
    const/16 v1, 0x8

    #@fc
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@fe
    const/high16 v3, 0x100

    #@100
    const-string/jumbo v4, "wifi_scan"

    #@103
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@106
    aput-object v2, v0, v1

    #@108
    const/16 v1, 0x9

    #@10a
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@10c
    const/high16 v3, 0x80

    #@10e
    const-string/jumbo v4, "wifi_multicast"

    #@111
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@114
    aput-object v2, v0, v1

    #@116
    const/16 v1, 0xa

    #@118
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@11a
    const/high16 v3, 0x1

    #@11c
    const-string v4, "bluetooth"

    #@11e
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@121
    aput-object v2, v0, v1

    #@123
    const/16 v1, 0xb

    #@125
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@127
    const/high16 v3, 0x40

    #@129
    const-string v4, "audio"

    #@12b
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@12e
    aput-object v2, v0, v1

    #@130
    const/16 v1, 0xc

    #@132
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@134
    const/high16 v3, 0x20

    #@136
    const-string/jumbo v4, "video"

    #@139
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@13c
    aput-object v2, v0, v1

    #@13e
    const/16 v1, 0xd

    #@140
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@142
    const/high16 v3, 0x4000

    #@144
    const-string/jumbo v4, "wake_lock"

    #@147
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@14a
    aput-object v2, v0, v1

    #@14c
    const/16 v1, 0xe

    #@14e
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@150
    const/high16 v3, 0x2000

    #@152
    const-string/jumbo v4, "sensor"

    #@155
    invoke-direct {v2, v3, v4}, Landroid/os/BatteryStats$BitDescription;-><init>(ILjava/lang/String;)V

    #@158
    aput-object v2, v0, v1

    #@15a
    const/16 v1, 0xf

    #@15c
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@15e
    const/16 v3, 0xf

    #@160
    const-string v4, "brightness"

    #@162
    sget-object v5, Landroid/os/BatteryStats;->SCREEN_BRIGHTNESS_NAMES:[Ljava/lang/String;

    #@164
    invoke-direct {v2, v3, v8, v4, v5}, Landroid/os/BatteryStats$BitDescription;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    #@167
    aput-object v2, v0, v1

    #@169
    const/16 v1, 0x10

    #@16b
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@16d
    const/16 v3, 0xf0

    #@16f
    const-string/jumbo v4, "signal_strength"

    #@172
    sget-object v5, Landroid/telephony/SignalStrength;->SIGNAL_STRENGTH_NAMES:[Ljava/lang/String;

    #@174
    invoke-direct {v2, v3, v6, v4, v5}, Landroid/os/BatteryStats$BitDescription;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    #@177
    aput-object v2, v0, v1

    #@179
    const/16 v1, 0x11

    #@17b
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@17d
    const/16 v3, 0xf00

    #@17f
    const/16 v4, 0x8

    #@181
    const-string/jumbo v5, "phone_state"

    #@184
    new-array v6, v6, [Ljava/lang/String;

    #@186
    const-string v7, "in"

    #@188
    aput-object v7, v6, v8

    #@18a
    const-string/jumbo v7, "out"

    #@18d
    aput-object v7, v6, v9

    #@18f
    const-string v7, "emergency"

    #@191
    aput-object v7, v6, v10

    #@193
    const-string/jumbo v7, "off"

    #@196
    aput-object v7, v6, v11

    #@198
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/os/BatteryStats$BitDescription;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    #@19b
    aput-object v2, v0, v1

    #@19d
    const/16 v1, 0x12

    #@19f
    new-instance v2, Landroid/os/BatteryStats$BitDescription;

    #@1a1
    const v3, 0xf000

    #@1a4
    const/16 v4, 0xc

    #@1a6
    const-string v5, "data_conn"

    #@1a8
    sget-object v6, Landroid/os/BatteryStats;->DATA_CONNECTION_NAMES:[Ljava/lang/String;

    #@1aa
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/os/BatteryStats$BitDescription;-><init>(IILjava/lang/String;[Ljava/lang/String;)V

    #@1ad
    aput-object v2, v0, v1

    #@1af
    sput-object v0, Landroid/os/BatteryStats;->HISTORY_STATE_DESCRIPTIONS:[Landroid/os/BatteryStats$BitDescription;

    #@1b1
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    const/16 v1, 0x20

    #@7
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@a
    iput-object v0, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@c
    .line 151
    new-instance v0, Ljava/util/Formatter;

    #@e
    iget-object v1, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0, v1}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    #@13
    iput-object v0, p0, Landroid/os/BatteryStats;->mFormatter:Ljava/util/Formatter;

    #@15
    .line 2051
    return-void
.end method

.method private static computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J
    .registers 12
    .parameter "timer"
    .parameter "batteryRealtime"
    .parameter "which"

    #@0
    .prologue
    .line 1103
    if-eqz p0, :cond_e

    #@2
    .line 1105
    invoke-virtual {p0, p1, p2, p3}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@5
    move-result-wide v0

    #@6
    .line 1106
    .local v0, totalTimeMicros:J
    const-wide/16 v4, 0x1f4

    #@8
    add-long/2addr v4, v0

    #@9
    const-wide/16 v6, 0x3e8

    #@b
    div-long v2, v4, v6

    #@d
    .line 1109
    .end local v0           #totalTimeMicros:J
    :goto_d
    return-wide v2

    #@e
    :cond_e
    const-wide/16 v2, 0x0

    #@10
    goto :goto_d
.end method

.method private static final varargs dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 11
    .parameter "pw"
    .parameter "uid"
    .parameter "category"
    .parameter "type"
    .parameter "args"

    #@0
    .prologue
    const/16 v5, 0x2c

    #@2
    .line 1182
    const/4 v4, 0x5

    #@3
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(I)V

    #@6
    invoke-virtual {p0, v5}, Ljava/io/PrintWriter;->print(C)V

    #@9
    .line 1183
    invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(I)V

    #@c
    invoke-virtual {p0, v5}, Ljava/io/PrintWriter;->print(C)V

    #@f
    .line 1184
    invoke-virtual {p0, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    invoke-virtual {p0, v5}, Ljava/io/PrintWriter;->print(C)V

    #@15
    .line 1185
    invoke-virtual {p0, p3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@18
    .line 1187
    move-object v1, p4

    #@19
    .local v1, arr$:[Ljava/lang/Object;
    array-length v3, v1

    #@1a
    .local v3, len$:I
    const/4 v2, 0x0

    #@1b
    .local v2, i$:I
    :goto_1b
    if-ge v2, v3, :cond_28

    #@1d
    aget-object v0, v1, v2

    #@1f
    .line 1188
    .local v0, arg:Ljava/lang/Object;
    invoke-virtual {p0, v5}, Ljava/io/PrintWriter;->print(C)V

    #@22
    .line 1189
    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@25
    .line 1187
    add-int/lit8 v2, v2, 0x1

    #@27
    goto :goto_1b

    #@28
    .line 1191
    .end local v0           #arg:Ljava/lang/Object;
    :cond_28
    const/16 v4, 0xa

    #@2a
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(C)V

    #@2d
    .line 1192
    return-void
.end method

.method private final formatBytesLocked(J)Ljava/lang/String;
    .registers 11
    .parameter "bytes"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 1086
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@4
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    #@7
    .line 1088
    const-wide/16 v0, 0x400

    #@9
    cmp-long v0, p1, v0

    #@b
    if-gez v0, :cond_21

    #@d
    .line 1089
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    const-string v1, "B"

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 1098
    :goto_20
    return-object v0

    #@21
    .line 1090
    :cond_21
    const-wide/32 v0, 0x100000

    #@24
    cmp-long v0, p1, v0

    #@26
    if-gez v0, :cond_42

    #@28
    .line 1091
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatter:Ljava/util/Formatter;

    #@2a
    const-string v1, "%.2fKB"

    #@2c
    new-array v2, v2, [Ljava/lang/Object;

    #@2e
    long-to-double v3, p1

    #@2f
    const-wide/high16 v5, 0x4090

    #@31
    div-double/2addr v3, v5

    #@32
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@35
    move-result-object v3

    #@36
    aput-object v3, v2, v7

    #@38
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@3b
    .line 1092
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    goto :goto_20

    #@42
    .line 1093
    :cond_42
    const-wide/32 v0, 0x40000000

    #@45
    cmp-long v0, p1, v0

    #@47
    if-gez v0, :cond_63

    #@49
    .line 1094
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatter:Ljava/util/Formatter;

    #@4b
    const-string v1, "%.2fMB"

    #@4d
    new-array v2, v2, [Ljava/lang/Object;

    #@4f
    long-to-double v3, p1

    #@50
    const-wide/high16 v5, 0x4130

    #@52
    div-double/2addr v3, v5

    #@53
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@56
    move-result-object v3

    #@57
    aput-object v3, v2, v7

    #@59
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@5c
    .line 1095
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    goto :goto_20

    #@63
    .line 1097
    :cond_63
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatter:Ljava/util/Formatter;

    #@65
    const-string v1, "%.2fGB"

    #@67
    new-array v2, v2, [Ljava/lang/Object;

    #@69
    long-to-double v3, p1

    #@6a
    const-wide/high16 v5, 0x41d0

    #@6c
    div-double/2addr v3, v5

    #@6d
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@70
    move-result-object v3

    #@71
    aput-object v3, v2, v7

    #@73
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@76
    .line 1098
    iget-object v0, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    goto :goto_20
.end method

.method private final formatRatioLocked(JJ)Ljava/lang/String;
    .registers 11
    .parameter "num"
    .parameter "den"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1076
    const-wide/16 v1, 0x0

    #@3
    cmp-long v1, p3, v1

    #@5
    if-nez v1, :cond_a

    #@7
    .line 1077
    const-string v1, "---%"

    #@9
    .line 1082
    :goto_9
    return-object v1

    #@a
    .line 1079
    :cond_a
    long-to-float v1, p1

    #@b
    long-to-float v2, p3

    #@c
    div-float/2addr v1, v2

    #@d
    const/high16 v2, 0x42c8

    #@f
    mul-float v0, v1, v2

    #@11
    .line 1080
    .local v0, perc:F
    iget-object v1, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@13
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@16
    .line 1081
    iget-object v1, p0, Landroid/os/BatteryStats;->mFormatter:Ljava/util/Formatter;

    #@18
    const-string v2, "%.1f%%"

    #@1a
    const/4 v3, 0x1

    #@1b
    new-array v3, v3, [Ljava/lang/Object;

    #@1d
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@20
    move-result-object v4

    #@21
    aput-object v4, v3, v5

    #@23
    invoke-virtual {v1, v2, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@26
    .line 1082
    iget-object v1, p0, Landroid/os/BatteryStats;->mFormatBuilder:Ljava/lang/StringBuilder;

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    goto :goto_9
.end method

.method private static final formatTime(Ljava/lang/StringBuilder;J)V
    .registers 9
    .parameter "sb"
    .parameter "time"

    #@0
    .prologue
    const-wide/16 v2, 0x64

    #@2
    .line 1062
    div-long v0, p1, v2

    #@4
    .line 1063
    .local v0, sec:J
    invoke-static {p0, v0, v1}, Landroid/os/BatteryStats;->formatTimeRaw(Ljava/lang/StringBuilder;J)V

    #@7
    .line 1064
    mul-long/2addr v2, v0

    #@8
    sub-long v2, p1, v2

    #@a
    const-wide/16 v4, 0xa

    #@c
    mul-long/2addr v2, v4

    #@d
    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    .line 1065
    const-string/jumbo v2, "ms "

    #@13
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 1066
    return-void
.end method

.method private static final formatTimeMs(Ljava/lang/StringBuilder;J)V
    .registers 7
    .parameter "sb"
    .parameter "time"

    #@0
    .prologue
    const-wide/16 v2, 0x3e8

    #@2
    .line 1069
    div-long v0, p1, v2

    #@4
    .line 1070
    .local v0, sec:J
    invoke-static {p0, v0, v1}, Landroid/os/BatteryStats;->formatTimeRaw(Ljava/lang/StringBuilder;J)V

    #@7
    .line 1071
    mul-long/2addr v2, v0

    #@8
    sub-long v2, p1, v2

    #@a
    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d
    .line 1072
    const-string/jumbo v2, "ms "

    #@10
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 1073
    return-void
.end method

.method private static final formatTimeRaw(Ljava/lang/StringBuilder;J)V
    .registers 15
    .parameter "out"
    .parameter "seconds"

    #@0
    .prologue
    .line 1034
    const-wide/32 v8, 0x15180

    #@3
    div-long v0, p1, v8

    #@5
    .line 1035
    .local v0, days:J
    const-wide/16 v8, 0x0

    #@7
    cmp-long v8, v0, v8

    #@9
    if-eqz v8, :cond_13

    #@b
    .line 1036
    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e
    .line 1037
    const-string v8, "d "

    #@10
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 1039
    :cond_13
    const-wide/16 v8, 0x3c

    #@15
    mul-long/2addr v8, v0

    #@16
    const-wide/16 v10, 0x3c

    #@18
    mul-long/2addr v8, v10

    #@19
    const-wide/16 v10, 0x18

    #@1b
    mul-long v6, v8, v10

    #@1d
    .line 1041
    .local v6, used:J
    sub-long v8, p1, v6

    #@1f
    const-wide/16 v10, 0xe10

    #@21
    div-long v2, v8, v10

    #@23
    .line 1042
    .local v2, hours:J
    const-wide/16 v8, 0x0

    #@25
    cmp-long v8, v2, v8

    #@27
    if-nez v8, :cond_2f

    #@29
    const-wide/16 v8, 0x0

    #@2b
    cmp-long v8, v6, v8

    #@2d
    if-eqz v8, :cond_37

    #@2f
    .line 1043
    :cond_2f
    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@32
    .line 1044
    const-string v8, "h "

    #@34
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 1046
    :cond_37
    const-wide/16 v8, 0x3c

    #@39
    mul-long/2addr v8, v2

    #@3a
    const-wide/16 v10, 0x3c

    #@3c
    mul-long/2addr v8, v10

    #@3d
    add-long/2addr v6, v8

    #@3e
    .line 1048
    sub-long v8, p1, v6

    #@40
    const-wide/16 v10, 0x3c

    #@42
    div-long v4, v8, v10

    #@44
    .line 1049
    .local v4, mins:J
    const-wide/16 v8, 0x0

    #@46
    cmp-long v8, v4, v8

    #@48
    if-nez v8, :cond_50

    #@4a
    const-wide/16 v8, 0x0

    #@4c
    cmp-long v8, v6, v8

    #@4e
    if-eqz v8, :cond_59

    #@50
    .line 1050
    :cond_50
    invoke-virtual {p0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@53
    .line 1051
    const-string/jumbo v8, "m "

    #@56
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    .line 1053
    :cond_59
    const-wide/16 v8, 0x3c

    #@5b
    mul-long/2addr v8, v4

    #@5c
    add-long/2addr v6, v8

    #@5d
    .line 1055
    const-wide/16 v8, 0x0

    #@5f
    cmp-long v8, p1, v8

    #@61
    if-nez v8, :cond_69

    #@63
    const-wide/16 v8, 0x0

    #@65
    cmp-long v8, v6, v8

    #@67
    if-eqz v8, :cond_74

    #@69
    .line 1056
    :cond_69
    sub-long v8, p1, v6

    #@6b
    invoke-virtual {p0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6e
    .line 1057
    const-string/jumbo v8, "s "

    #@71
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    .line 1059
    :cond_74
    return-void
.end method

.method static printBitDescriptions(Ljava/io/PrintWriter;II[Landroid/os/BatteryStats$BitDescription;)V
    .registers 10
    .parameter "pw"
    .parameter "oldval"
    .parameter "newval"
    .parameter "descriptions"

    #@0
    .prologue
    .line 2025
    xor-int v1, p1, p2

    #@2
    .line 2026
    .local v1, diff:I
    if-nez v1, :cond_5

    #@4
    .line 2046
    :cond_4
    return-void

    #@5
    .line 2027
    :cond_5
    const/4 v2, 0x0

    #@6
    .local v2, i:I
    :goto_6
    array-length v4, p3

    #@7
    if-ge v2, v4, :cond_4

    #@9
    .line 2028
    aget-object v0, p3, v2

    #@b
    .line 2029
    .local v0, bd:Landroid/os/BatteryStats$BitDescription;
    iget v4, v0, Landroid/os/BatteryStats$BitDescription;->mask:I

    #@d
    and-int/2addr v4, v1

    #@e
    if-eqz v4, :cond_23

    #@10
    .line 2030
    iget v4, v0, Landroid/os/BatteryStats$BitDescription;->shift:I

    #@12
    if-gez v4, :cond_29

    #@14
    .line 2031
    iget v4, v0, Landroid/os/BatteryStats$BitDescription;->mask:I

    #@16
    and-int/2addr v4, p2

    #@17
    if-eqz v4, :cond_26

    #@19
    const-string v4, " +"

    #@1b
    :goto_1b
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e
    .line 2032
    iget-object v4, v0, Landroid/os/BatteryStats$BitDescription;->name:Ljava/lang/String;

    #@20
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    .line 2027
    :cond_23
    :goto_23
    add-int/lit8 v2, v2, 0x1

    #@25
    goto :goto_6

    #@26
    .line 2031
    :cond_26
    const-string v4, " -"

    #@28
    goto :goto_1b

    #@29
    .line 2034
    :cond_29
    const-string v4, " "

    #@2b
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e
    .line 2035
    iget-object v4, v0, Landroid/os/BatteryStats$BitDescription;->name:Ljava/lang/String;

    #@30
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    .line 2036
    const-string v4, "="

    #@35
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    .line 2037
    iget v4, v0, Landroid/os/BatteryStats$BitDescription;->mask:I

    #@3a
    and-int/2addr v4, p2

    #@3b
    iget v5, v0, Landroid/os/BatteryStats$BitDescription;->shift:I

    #@3d
    shr-int v3, v4, v5

    #@3f
    .line 2038
    .local v3, val:I
    iget-object v4, v0, Landroid/os/BatteryStats$BitDescription;->values:[Ljava/lang/String;

    #@41
    if-eqz v4, :cond_52

    #@43
    if-ltz v3, :cond_52

    #@45
    iget-object v4, v0, Landroid/os/BatteryStats$BitDescription;->values:[Ljava/lang/String;

    #@47
    array-length v4, v4

    #@48
    if-ge v3, v4, :cond_52

    #@4a
    .line 2039
    iget-object v4, v0, Landroid/os/BatteryStats$BitDescription;->values:[Ljava/lang/String;

    #@4c
    aget-object v4, v4, v3

    #@4e
    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51
    goto :goto_23

    #@52
    .line 2041
    :cond_52
    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    #@55
    goto :goto_23
.end method

.method private static final printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "sb"
    .parameter "timer"
    .parameter "batteryRealtime"
    .parameter "name"
    .parameter "which"
    .parameter "linePrefix"

    #@0
    .prologue
    .line 1125
    if-eqz p1, :cond_2f

    #@2
    .line 1126
    invoke-static {p1, p2, p3, p5}, Landroid/os/BatteryStats;->computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J

    #@5
    move-result-wide v1

    #@6
    .line 1128
    .local v1, totalTimeMillis:J
    invoke-virtual {p1, p5}, Landroid/os/BatteryStats$Timer;->getCountLocked(I)I

    #@9
    move-result v0

    #@a
    .line 1129
    .local v0, count:I
    const-wide/16 v3, 0x0

    #@c
    cmp-long v3, v1, v3

    #@e
    if-eqz v3, :cond_2f

    #@10
    .line 1130
    invoke-virtual {p0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 1131
    invoke-static {p0, v1, v2}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@16
    .line 1132
    if-eqz p4, :cond_20

    #@18
    .line 1133
    invoke-virtual {p0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    .line 1134
    const/16 v3, 0x20

    #@1d
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@20
    .line 1136
    :cond_20
    const/16 v3, 0x28

    #@22
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    .line 1137
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    .line 1138
    const-string v3, " times)"

    #@2a
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 1139
    const-string p6, ", "

    #@2f
    .line 1142
    .end local v0           #count:I
    .end local v1           #totalTimeMillis:J
    .end local p6
    :cond_2f
    return-object p6
.end method

.method private static final printWakeLockCheckin(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "sb"
    .parameter "timer"
    .parameter "now"
    .parameter "name"
    .parameter "which"
    .parameter "linePrefix"

    #@0
    .prologue
    .line 1158
    const-wide/16 v1, 0x0

    #@2
    .line 1159
    .local v1, totalTimeMicros:J
    const/4 v0, 0x0

    #@3
    .line 1160
    .local v0, count:I
    if-eqz p1, :cond_d

    #@5
    .line 1161
    invoke-virtual {p1, p2, p3, p5}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@8
    move-result-wide v1

    #@9
    .line 1162
    invoke-virtual {p1, p5}, Landroid/os/BatteryStats$Timer;->getCountLocked(I)I

    #@c
    move-result v0

    #@d
    .line 1164
    :cond_d
    invoke-virtual {p0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 1165
    const-wide/16 v3, 0x1f4

    #@12
    add-long/2addr v3, v1

    #@13
    const-wide/16 v5, 0x3e8

    #@15
    div-long/2addr v3, v5

    #@16
    invoke-virtual {p0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@19
    .line 1166
    const/16 v3, 0x2c

    #@1b
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    .line 1167
    if-eqz p4, :cond_3c

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, ","

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    :goto_33
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 1168
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    .line 1169
    const-string v3, ","

    #@3b
    return-object v3

    #@3c
    .line 1167
    :cond_3c
    const-string v3, ""

    #@3e
    goto :goto_33
.end method


# virtual methods
.method public abstract computeBatteryRealtime(JI)J
.end method

.method public abstract computeBatteryUptime(JI)J
.end method

.method public abstract computeRealtime(JI)J
.end method

.method public abstract computeUptime(JI)J
.end method

.method public final dumpCheckinLocked(Ljava/io/PrintWriter;II)V
    .registers 106
    .parameter "pw"
    .parameter "which"
    .parameter "reqUid"

    #@0
    .prologue
    .line 1200
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v8

    #@4
    const-wide/16 v97, 0x3e8

    #@6
    mul-long v46, v8, v97

    #@8
    .line 1201
    .local v46, rawUptime:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v8

    #@c
    const-wide/16 v97, 0x3e8

    #@e
    mul-long v44, v8, v97

    #@10
    .line 1202
    .local v44, rawRealtime:J
    move-object/from16 v0, p0

    #@12
    move-wide/from16 v1, v46

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getBatteryUptime(J)J

    #@17
    move-result-wide v13

    #@18
    .line 1203
    .local v13, batteryUptime:J
    move-object/from16 v0, p0

    #@1a
    move-wide/from16 v1, v44

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getBatteryRealtime(J)J

    #@1f
    move-result-wide v6

    #@20
    .line 1204
    .local v6, batteryRealtime:J
    move-object/from16 v0, p0

    #@22
    move-wide/from16 v1, v46

    #@24
    move/from16 v3, p2

    #@26
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeBatteryUptime(JI)J

    #@29
    move-result-wide v88

    #@2a
    .line 1205
    .local v88, whichBatteryUptime:J
    move-object/from16 v0, p0

    #@2c
    move-wide/from16 v1, v44

    #@2e
    move/from16 v3, p2

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeBatteryRealtime(JI)J

    #@33
    move-result-wide v86

    #@34
    .line 1206
    .local v86, whichBatteryRealtime:J
    move-object/from16 v0, p0

    #@36
    move-wide/from16 v1, v44

    #@38
    move/from16 v3, p2

    #@3a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeRealtime(JI)J

    #@3d
    move-result-wide v66

    #@3e
    .line 1207
    .local v66, totalRealtime:J
    move-object/from16 v0, p0

    #@40
    move-wide/from16 v1, v46

    #@42
    move/from16 v3, p2

    #@44
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeUptime(JI)J

    #@47
    move-result-wide v70

    #@48
    .line 1208
    .local v70, totalUptime:J
    move-object/from16 v0, p0

    #@4a
    move/from16 v1, p2

    #@4c
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getScreenOnTime(JI)J

    #@4f
    move-result-wide v52

    #@50
    .line 1209
    .local v52, screenOnTime:J
    move-object/from16 v0, p0

    #@52
    move/from16 v1, p2

    #@54
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getPhoneOnTime(JI)J

    #@57
    move-result-wide v40

    #@58
    .line 1210
    .local v40, phoneOnTime:J
    move-object/from16 v0, p0

    #@5a
    move/from16 v1, p2

    #@5c
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getWifiOnTime(JI)J

    #@5f
    move-result-wide v90

    #@60
    .line 1211
    .local v90, wifiOnTime:J
    move-object/from16 v0, p0

    #@62
    move/from16 v1, p2

    #@64
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getGlobalWifiRunningTime(JI)J

    #@67
    move-result-wide v92

    #@68
    .line 1212
    .local v92, wifiRunningTime:J
    move-object/from16 v0, p0

    #@6a
    move/from16 v1, p2

    #@6c
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getBluetoothOnTime(JI)J

    #@6f
    move-result-wide v15

    #@70
    .line 1214
    .local v15, bluetoothOnTime:J
    new-instance v4, Ljava/lang/StringBuilder;

    #@72
    const/16 v5, 0x80

    #@74
    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@77
    .line 1216
    .local v4, sb:Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    #@7a
    move-result-object v78

    #@7b
    .line 1217
    .local v78, uidStats:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid;>;"
    invoke-virtual/range {v78 .. v78}, Landroid/util/SparseArray;->size()I

    #@7e
    move-result v11

    #@7f
    .line 1219
    .local v11, NU:I
    sget-object v5, Landroid/os/BatteryStats;->STAT_NAMES:[Ljava/lang/String;

    #@81
    aget-object v17, v5, p2

    #@83
    .line 1222
    .local v17, category:Ljava/lang/String;
    const/4 v8, 0x0

    #@84
    const-string v9, "bt"

    #@86
    const/4 v5, 0x5

    #@87
    new-array v0, v5, [Ljava/lang/Object;

    #@89
    move-object/from16 v97, v0

    #@8b
    const/16 v98, 0x0

    #@8d
    if-nez p2, :cond_145

    #@8f
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getStartCount()I

    #@92
    move-result v5

    #@93
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@96
    move-result-object v5

    #@97
    :goto_97
    aput-object v5, v97, v98

    #@99
    const/4 v5, 0x1

    #@9a
    const-wide/16 v98, 0x3e8

    #@9c
    div-long v98, v86, v98

    #@9e
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a1
    move-result-object v98

    #@a2
    aput-object v98, v97, v5

    #@a4
    const/4 v5, 0x2

    #@a5
    const-wide/16 v98, 0x3e8

    #@a7
    div-long v98, v88, v98

    #@a9
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@ac
    move-result-object v98

    #@ad
    aput-object v98, v97, v5

    #@af
    const/4 v5, 0x3

    #@b0
    const-wide/16 v98, 0x3e8

    #@b2
    div-long v98, v66, v98

    #@b4
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b7
    move-result-object v98

    #@b8
    aput-object v98, v97, v5

    #@ba
    const/4 v5, 0x4

    #@bb
    const-wide/16 v98, 0x3e8

    #@bd
    div-long v98, v70, v98

    #@bf
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c2
    move-result-object v98

    #@c3
    aput-object v98, v97, v5

    #@c5
    move-object/from16 v0, p1

    #@c7
    move-object/from16 v1, v17

    #@c9
    move-object/from16 v2, v97

    #@cb
    invoke-static {v0, v8, v1, v9, v2}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@ce
    .line 1228
    const-wide/16 v50, 0x0

    #@d0
    .line 1229
    .local v50, rxTotal:J
    const-wide/16 v74, 0x0

    #@d2
    .line 1230
    .local v74, txTotal:J
    const-wide/16 v24, 0x0

    #@d4
    .line 1231
    .local v24, fullWakeLockTimeTotal:J
    const-wide/16 v37, 0x0

    #@d6
    .line 1233
    .local v37, partialWakeLockTimeTotal:J
    const/16 v33, 0x0

    #@d8
    .local v33, iu:I
    :goto_d8
    move/from16 v0, v33

    #@da
    if-ge v0, v11, :cond_14c

    #@dc
    .line 1234
    move-object/from16 v0, v78

    #@de
    move/from16 v1, v33

    #@e0
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@e3
    move-result-object v76

    #@e4
    check-cast v76, Landroid/os/BatteryStats$Uid;

    #@e6
    .line 1235
    .local v76, u:Landroid/os/BatteryStats$Uid;
    move-object/from16 v0, v76

    #@e8
    move/from16 v1, p2

    #@ea
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesReceived(I)J

    #@ed
    move-result-wide v8

    #@ee
    add-long v50, v50, v8

    #@f0
    .line 1236
    move-object/from16 v0, v76

    #@f2
    move/from16 v1, p2

    #@f4
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesSent(I)J

    #@f7
    move-result-wide v8

    #@f8
    add-long v74, v74, v8

    #@fa
    .line 1238
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->getWakelockStats()Ljava/util/Map;

    #@fd
    move-result-object v84

    #@fe
    .line 1239
    .local v84, wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v84 .. v84}, Ljava/util/Map;->size()I

    #@101
    move-result v5

    #@102
    if-lez v5, :cond_149

    #@104
    .line 1241
    invoke-interface/range {v84 .. v84}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@107
    move-result-object v5

    #@108
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@10b
    move-result-object v31

    #@10c
    .local v31, i$:Ljava/util/Iterator;
    :cond_10c
    :goto_10c
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@10f
    move-result v5

    #@110
    if-eqz v5, :cond_149

    #@112
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@115
    move-result-object v23

    #@116
    check-cast v23, Ljava/util/Map$Entry;

    #@118
    .line 1242
    .local v23, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@11b
    move-result-object v96

    #@11c
    check-cast v96, Landroid/os/BatteryStats$Uid$Wakelock;

    #@11e
    .line 1244
    .local v96, wl:Landroid/os/BatteryStats$Uid$Wakelock;
    const/4 v5, 0x1

    #@11f
    move-object/from16 v0, v96

    #@121
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@124
    move-result-object v26

    #@125
    .line 1245
    .local v26, fullWakeTimer:Landroid/os/BatteryStats$Timer;
    if-eqz v26, :cond_131

    #@127
    .line 1246
    move-object/from16 v0, v26

    #@129
    move/from16 v1, p2

    #@12b
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@12e
    move-result-wide v8

    #@12f
    add-long v24, v24, v8

    #@131
    .line 1249
    :cond_131
    const/4 v5, 0x0

    #@132
    move-object/from16 v0, v96

    #@134
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@137
    move-result-object v39

    #@138
    .line 1250
    .local v39, partialWakeTimer:Landroid/os/BatteryStats$Timer;
    if-eqz v39, :cond_10c

    #@13a
    .line 1251
    move-object/from16 v0, v39

    #@13c
    move/from16 v1, p2

    #@13e
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@141
    move-result-wide v8

    #@142
    add-long v37, v37, v8

    #@144
    goto :goto_10c

    #@145
    .line 1222
    .end local v23           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v24           #fullWakeLockTimeTotal:J
    .end local v26           #fullWakeTimer:Landroid/os/BatteryStats$Timer;
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v33           #iu:I
    .end local v37           #partialWakeLockTimeTotal:J
    .end local v39           #partialWakeTimer:Landroid/os/BatteryStats$Timer;
    .end local v50           #rxTotal:J
    .end local v74           #txTotal:J
    .end local v76           #u:Landroid/os/BatteryStats$Uid;
    .end local v84           #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v96           #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    :cond_145
    const-string v5, "N/A"

    #@147
    goto/16 :goto_97

    #@149
    .line 1233
    .restart local v24       #fullWakeLockTimeTotal:J
    .restart local v33       #iu:I
    .restart local v37       #partialWakeLockTimeTotal:J
    .restart local v50       #rxTotal:J
    .restart local v74       #txTotal:J
    .restart local v76       #u:Landroid/os/BatteryStats$Uid;
    .restart local v84       #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    :cond_149
    add-int/lit8 v33, v33, 0x1

    #@14b
    goto :goto_d8

    #@14c
    .line 1259
    .end local v76           #u:Landroid/os/BatteryStats$Uid;
    .end local v84           #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    :cond_14c
    const/4 v5, 0x0

    #@14d
    const-string/jumbo v8, "m"

    #@150
    const/16 v9, 0xa

    #@152
    new-array v9, v9, [Ljava/lang/Object;

    #@154
    const/16 v97, 0x0

    #@156
    const-wide/16 v98, 0x3e8

    #@158
    div-long v98, v52, v98

    #@15a
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@15d
    move-result-object v98

    #@15e
    aput-object v98, v9, v97

    #@160
    const/16 v97, 0x1

    #@162
    const-wide/16 v98, 0x3e8

    #@164
    div-long v98, v40, v98

    #@166
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@169
    move-result-object v98

    #@16a
    aput-object v98, v9, v97

    #@16c
    const/16 v97, 0x2

    #@16e
    const-wide/16 v98, 0x3e8

    #@170
    div-long v98, v90, v98

    #@172
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@175
    move-result-object v98

    #@176
    aput-object v98, v9, v97

    #@178
    const/16 v97, 0x3

    #@17a
    const-wide/16 v98, 0x3e8

    #@17c
    div-long v98, v92, v98

    #@17e
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@181
    move-result-object v98

    #@182
    aput-object v98, v9, v97

    #@184
    const/16 v97, 0x4

    #@186
    const-wide/16 v98, 0x3e8

    #@188
    div-long v98, v15, v98

    #@18a
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@18d
    move-result-object v98

    #@18e
    aput-object v98, v9, v97

    #@190
    const/16 v97, 0x5

    #@192
    invoke-static/range {v50 .. v51}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@195
    move-result-object v98

    #@196
    aput-object v98, v9, v97

    #@198
    const/16 v97, 0x6

    #@19a
    invoke-static/range {v74 .. v75}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@19d
    move-result-object v98

    #@19e
    aput-object v98, v9, v97

    #@1a0
    const/16 v97, 0x7

    #@1a2
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1a5
    move-result-object v98

    #@1a6
    aput-object v98, v9, v97

    #@1a8
    const/16 v97, 0x8

    #@1aa
    invoke-static/range {v37 .. v38}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1ad
    move-result-object v98

    #@1ae
    aput-object v98, v9, v97

    #@1b0
    const/16 v97, 0x9

    #@1b2
    move-object/from16 v0, p0

    #@1b4
    move/from16 v1, p2

    #@1b6
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getInputEventCount(I)I

    #@1b9
    move-result v98

    #@1ba
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bd
    move-result-object v98

    #@1be
    aput-object v98, v9, v97

    #@1c0
    move-object/from16 v0, p1

    #@1c2
    move-object/from16 v1, v17

    #@1c4
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@1c7
    .line 1266
    const/4 v5, 0x5

    #@1c8
    new-array v12, v5, [Ljava/lang/Object;

    #@1ca
    .line 1267
    .local v12, args:[Ljava/lang/Object;
    const/16 v30, 0x0

    #@1cc
    .local v30, i:I
    :goto_1cc
    const/4 v5, 0x5

    #@1cd
    move/from16 v0, v30

    #@1cf
    if-ge v0, v5, :cond_1e8

    #@1d1
    .line 1268
    move-object/from16 v0, p0

    #@1d3
    move/from16 v1, v30

    #@1d5
    move/from16 v2, p2

    #@1d7
    invoke-virtual {v0, v1, v6, v7, v2}, Landroid/os/BatteryStats;->getScreenBrightnessTime(IJI)J

    #@1da
    move-result-wide v8

    #@1db
    const-wide/16 v97, 0x3e8

    #@1dd
    div-long v8, v8, v97

    #@1df
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1e2
    move-result-object v5

    #@1e3
    aput-object v5, v12, v30

    #@1e5
    .line 1267
    add-int/lit8 v30, v30, 0x1

    #@1e7
    goto :goto_1cc

    #@1e8
    .line 1270
    :cond_1e8
    const/4 v5, 0x0

    #@1e9
    const-string v8, "br"

    #@1eb
    move-object/from16 v0, p1

    #@1ed
    move-object/from16 v1, v17

    #@1ef
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@1f2
    .line 1273
    sget v5, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@1f4
    new-array v12, v5, [Ljava/lang/Object;

    #@1f6
    .line 1274
    const/16 v30, 0x0

    #@1f8
    :goto_1f8
    sget v5, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@1fa
    move/from16 v0, v30

    #@1fc
    if-ge v0, v5, :cond_215

    #@1fe
    .line 1275
    move-object/from16 v0, p0

    #@200
    move/from16 v1, v30

    #@202
    move/from16 v2, p2

    #@204
    invoke-virtual {v0, v1, v6, v7, v2}, Landroid/os/BatteryStats;->getPhoneSignalStrengthTime(IJI)J

    #@207
    move-result-wide v8

    #@208
    const-wide/16 v97, 0x3e8

    #@20a
    div-long v8, v8, v97

    #@20c
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@20f
    move-result-object v5

    #@210
    aput-object v5, v12, v30

    #@212
    .line 1274
    add-int/lit8 v30, v30, 0x1

    #@214
    goto :goto_1f8

    #@215
    .line 1277
    :cond_215
    const/4 v5, 0x0

    #@216
    const-string/jumbo v8, "sgt"

    #@219
    move-object/from16 v0, p1

    #@21b
    move-object/from16 v1, v17

    #@21d
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@220
    .line 1278
    const/4 v5, 0x0

    #@221
    const-string/jumbo v8, "sst"

    #@224
    const/4 v9, 0x1

    #@225
    new-array v9, v9, [Ljava/lang/Object;

    #@227
    const/16 v97, 0x0

    #@229
    move-object/from16 v0, p0

    #@22b
    move/from16 v1, p2

    #@22d
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats;->getPhoneSignalScanningTime(JI)J

    #@230
    move-result-wide v98

    #@231
    const-wide/16 v100, 0x3e8

    #@233
    div-long v98, v98, v100

    #@235
    invoke-static/range {v98 .. v99}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@238
    move-result-object v98

    #@239
    aput-object v98, v9, v97

    #@23b
    move-object/from16 v0, p1

    #@23d
    move-object/from16 v1, v17

    #@23f
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@242
    .line 1280
    const/16 v30, 0x0

    #@244
    :goto_244
    sget v5, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@246
    move/from16 v0, v30

    #@248
    if-ge v0, v5, :cond_25d

    #@24a
    .line 1281
    move-object/from16 v0, p0

    #@24c
    move/from16 v1, v30

    #@24e
    move/from16 v2, p2

    #@250
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getPhoneSignalStrengthCount(II)I

    #@253
    move-result v5

    #@254
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@257
    move-result-object v5

    #@258
    aput-object v5, v12, v30

    #@25a
    .line 1280
    add-int/lit8 v30, v30, 0x1

    #@25c
    goto :goto_244

    #@25d
    .line 1283
    :cond_25d
    const/4 v5, 0x0

    #@25e
    const-string/jumbo v8, "sgc"

    #@261
    move-object/from16 v0, p1

    #@263
    move-object/from16 v1, v17

    #@265
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@268
    .line 1286
    const/16 v5, 0x10

    #@26a
    new-array v12, v5, [Ljava/lang/Object;

    #@26c
    .line 1287
    const/16 v30, 0x0

    #@26e
    :goto_26e
    const/16 v5, 0x10

    #@270
    move/from16 v0, v30

    #@272
    if-ge v0, v5, :cond_28b

    #@274
    .line 1288
    move-object/from16 v0, p0

    #@276
    move/from16 v1, v30

    #@278
    move/from16 v2, p2

    #@27a
    invoke-virtual {v0, v1, v6, v7, v2}, Landroid/os/BatteryStats;->getPhoneDataConnectionTime(IJI)J

    #@27d
    move-result-wide v8

    #@27e
    const-wide/16 v97, 0x3e8

    #@280
    div-long v8, v8, v97

    #@282
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@285
    move-result-object v5

    #@286
    aput-object v5, v12, v30

    #@288
    .line 1287
    add-int/lit8 v30, v30, 0x1

    #@28a
    goto :goto_26e

    #@28b
    .line 1290
    :cond_28b
    const/4 v5, 0x0

    #@28c
    const-string v8, "dct"

    #@28e
    move-object/from16 v0, p1

    #@290
    move-object/from16 v1, v17

    #@292
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@295
    .line 1291
    const/16 v30, 0x0

    #@297
    :goto_297
    const/16 v5, 0x10

    #@299
    move/from16 v0, v30

    #@29b
    if-ge v0, v5, :cond_2b0

    #@29d
    .line 1292
    move-object/from16 v0, p0

    #@29f
    move/from16 v1, v30

    #@2a1
    move/from16 v2, p2

    #@2a3
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getPhoneDataConnectionCount(II)I

    #@2a6
    move-result v5

    #@2a7
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2aa
    move-result-object v5

    #@2ab
    aput-object v5, v12, v30

    #@2ad
    .line 1291
    add-int/lit8 v30, v30, 0x1

    #@2af
    goto :goto_297

    #@2b0
    .line 1294
    :cond_2b0
    const/4 v5, 0x0

    #@2b1
    const-string v8, "dcc"

    #@2b3
    move-object/from16 v0, p1

    #@2b5
    move-object/from16 v1, v17

    #@2b7
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@2ba
    .line 1296
    const/4 v5, 0x3

    #@2bb
    move/from16 v0, p2

    #@2bd
    if-ne v0, v5, :cond_2e5

    #@2bf
    .line 1297
    const/4 v5, 0x0

    #@2c0
    const-string/jumbo v8, "lv"

    #@2c3
    const/4 v9, 0x2

    #@2c4
    new-array v9, v9, [Ljava/lang/Object;

    #@2c6
    const/16 v97, 0x0

    #@2c8
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeStartLevel()I

    #@2cb
    move-result v98

    #@2cc
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2cf
    move-result-object v98

    #@2d0
    aput-object v98, v9, v97

    #@2d2
    const/16 v97, 0x1

    #@2d4
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeCurrentLevel()I

    #@2d7
    move-result v98

    #@2d8
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2db
    move-result-object v98

    #@2dc
    aput-object v98, v9, v97

    #@2de
    move-object/from16 v0, p1

    #@2e0
    move-object/from16 v1, v17

    #@2e2
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@2e5
    .line 1301
    :cond_2e5
    const/4 v5, 0x3

    #@2e6
    move/from16 v0, p2

    #@2e8
    if-ne v0, v5, :cond_384

    #@2ea
    .line 1302
    const/4 v5, 0x0

    #@2eb
    const-string v8, "dc"

    #@2ed
    const/4 v9, 0x4

    #@2ee
    new-array v9, v9, [Ljava/lang/Object;

    #@2f0
    const/16 v97, 0x0

    #@2f2
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeStartLevel()I

    #@2f5
    move-result v98

    #@2f6
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeCurrentLevel()I

    #@2f9
    move-result v99

    #@2fa
    sub-int v98, v98, v99

    #@2fc
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2ff
    move-result-object v98

    #@300
    aput-object v98, v9, v97

    #@302
    const/16 v97, 0x1

    #@304
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeStartLevel()I

    #@307
    move-result v98

    #@308
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeCurrentLevel()I

    #@30b
    move-result v99

    #@30c
    sub-int v98, v98, v99

    #@30e
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@311
    move-result-object v98

    #@312
    aput-object v98, v9, v97

    #@314
    const/16 v97, 0x2

    #@316
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOn()I

    #@319
    move-result v98

    #@31a
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31d
    move-result-object v98

    #@31e
    aput-object v98, v9, v97

    #@320
    const/16 v97, 0x3

    #@322
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOff()I

    #@325
    move-result v98

    #@326
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@329
    move-result-object v98

    #@32a
    aput-object v98, v9, v97

    #@32c
    move-object/from16 v0, p1

    #@32e
    move-object/from16 v1, v17

    #@330
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@333
    .line 1312
    :goto_333
    if-gez p3, :cond_3c3

    #@335
    .line 1313
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getKernelWakelockStats()Ljava/util/Map;

    #@338
    move-result-object v34

    #@339
    .line 1314
    .local v34, kernelWakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->size()I

    #@33c
    move-result v5

    #@33d
    if-lez v5, :cond_3c3

    #@33f
    .line 1315
    invoke-interface/range {v34 .. v34}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@342
    move-result-object v5

    #@343
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@346
    move-result-object v31

    #@347
    .restart local v31       #i$:Ljava/util/Iterator;
    :goto_347
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@34a
    move-result v5

    #@34b
    if-eqz v5, :cond_3c3

    #@34d
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@350
    move-result-object v20

    #@351
    check-cast v20, Ljava/util/Map$Entry;

    #@353
    .line 1316
    .local v20, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    const/4 v5, 0x0

    #@354
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@357
    .line 1317
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@35a
    move-result-object v5

    #@35b
    check-cast v5, Landroid/os/BatteryStats$Timer;

    #@35d
    const/4 v8, 0x0

    #@35e
    const-string v10, ""

    #@360
    move/from16 v9, p2

    #@362
    invoke-static/range {v4 .. v10}, Landroid/os/BatteryStats;->printWakeLockCheckin(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@365
    .line 1319
    const/4 v5, 0x0

    #@366
    const-string/jumbo v8, "kwl"

    #@369
    const/4 v9, 0x2

    #@36a
    new-array v9, v9, [Ljava/lang/Object;

    #@36c
    const/16 v97, 0x0

    #@36e
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@371
    move-result-object v98

    #@372
    aput-object v98, v9, v97

    #@374
    const/16 v97, 0x1

    #@376
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@379
    move-result-object v98

    #@37a
    aput-object v98, v9, v97

    #@37c
    move-object/from16 v0, p1

    #@37e
    move-object/from16 v1, v17

    #@380
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@383
    goto :goto_347

    #@384
    .line 1307
    .end local v20           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v34           #kernelWakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    :cond_384
    const/4 v5, 0x0

    #@385
    const-string v8, "dc"

    #@387
    const/4 v9, 0x4

    #@388
    new-array v9, v9, [Ljava/lang/Object;

    #@38a
    const/16 v97, 0x0

    #@38c
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getLowDischargeAmountSinceCharge()I

    #@38f
    move-result v98

    #@390
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@393
    move-result-object v98

    #@394
    aput-object v98, v9, v97

    #@396
    const/16 v97, 0x1

    #@398
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getHighDischargeAmountSinceCharge()I

    #@39b
    move-result v98

    #@39c
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39f
    move-result-object v98

    #@3a0
    aput-object v98, v9, v97

    #@3a2
    const/16 v97, 0x2

    #@3a4
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOn()I

    #@3a7
    move-result v98

    #@3a8
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3ab
    move-result-object v98

    #@3ac
    aput-object v98, v9, v97

    #@3ae
    const/16 v97, 0x3

    #@3b0
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOff()I

    #@3b3
    move-result v98

    #@3b4
    invoke-static/range {v98 .. v98}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b7
    move-result-object v98

    #@3b8
    aput-object v98, v9, v97

    #@3ba
    move-object/from16 v0, p1

    #@3bc
    move-object/from16 v1, v17

    #@3be
    invoke-static {v0, v5, v1, v8, v9}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@3c1
    goto/16 :goto_333

    #@3c3
    .line 1325
    :cond_3c3
    const/16 v33, 0x0

    #@3c5
    :goto_3c5
    move/from16 v0, v33

    #@3c7
    if-ge v0, v11, :cond_6be

    #@3c9
    .line 1326
    move-object/from16 v0, v78

    #@3cb
    move/from16 v1, v33

    #@3cd
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@3d0
    move-result v77

    #@3d1
    .line 1327
    .local v77, uid:I
    if-ltz p3, :cond_3dc

    #@3d3
    move/from16 v0, v77

    #@3d5
    move/from16 v1, p3

    #@3d7
    if-eq v0, v1, :cond_3dc

    #@3d9
    .line 1325
    :cond_3d9
    add-int/lit8 v33, v33, 0x1

    #@3db
    goto :goto_3c5

    #@3dc
    .line 1330
    :cond_3dc
    move-object/from16 v0, v78

    #@3de
    move/from16 v1, v33

    #@3e0
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@3e3
    move-result-object v76

    #@3e4
    check-cast v76, Landroid/os/BatteryStats$Uid;

    #@3e6
    .line 1332
    .restart local v76       #u:Landroid/os/BatteryStats$Uid;
    move-object/from16 v0, v76

    #@3e8
    move/from16 v1, p2

    #@3ea
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesReceived(I)J

    #@3ed
    move-result-wide v48

    #@3ee
    .line 1333
    .local v48, rx:J
    move-object/from16 v0, v76

    #@3f0
    move/from16 v1, p2

    #@3f2
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesSent(I)J

    #@3f5
    move-result-wide v72

    #@3f6
    .line 1334
    .local v72, tx:J
    move-object/from16 v0, v76

    #@3f8
    move/from16 v1, p2

    #@3fa
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Uid;->getFullWifiLockTime(JI)J

    #@3fd
    move-result-wide v27

    #@3fe
    .line 1335
    .local v27, fullWifiLockOnTime:J
    move-object/from16 v0, v76

    #@400
    move/from16 v1, p2

    #@402
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Uid;->getWifiScanTime(JI)J

    #@405
    move-result-wide v94

    #@406
    .line 1336
    .local v94, wifiScanTime:J
    move-object/from16 v0, v76

    #@408
    move/from16 v1, p2

    #@40a
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Uid;->getWifiRunningTime(JI)J

    #@40d
    move-result-wide v79

    #@40e
    .line 1338
    .local v79, uidWifiRunningTime:J
    const-wide/16 v8, 0x0

    #@410
    cmp-long v5, v48, v8

    #@412
    if-gtz v5, :cond_41a

    #@414
    const-wide/16 v8, 0x0

    #@416
    cmp-long v5, v72, v8

    #@418
    if-lez v5, :cond_437

    #@41a
    :cond_41a
    const-string/jumbo v5, "nt"

    #@41d
    const/4 v8, 0x2

    #@41e
    new-array v8, v8, [Ljava/lang/Object;

    #@420
    const/4 v9, 0x0

    #@421
    invoke-static/range {v48 .. v49}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@424
    move-result-object v97

    #@425
    aput-object v97, v8, v9

    #@427
    const/4 v9, 0x1

    #@428
    invoke-static/range {v72 .. v73}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@42b
    move-result-object v97

    #@42c
    aput-object v97, v8, v9

    #@42e
    move-object/from16 v0, p1

    #@430
    move/from16 v1, v77

    #@432
    move-object/from16 v2, v17

    #@434
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@437
    .line 1340
    :cond_437
    const-wide/16 v8, 0x0

    #@439
    cmp-long v5, v27, v8

    #@43b
    if-nez v5, :cond_449

    #@43d
    const-wide/16 v8, 0x0

    #@43f
    cmp-long v5, v94, v8

    #@441
    if-nez v5, :cond_449

    #@443
    const-wide/16 v8, 0x0

    #@445
    cmp-long v5, v79, v8

    #@447
    if-eqz v5, :cond_46d

    #@449
    .line 1342
    :cond_449
    const-string/jumbo v5, "wfl"

    #@44c
    const/4 v8, 0x3

    #@44d
    new-array v8, v8, [Ljava/lang/Object;

    #@44f
    const/4 v9, 0x0

    #@450
    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@453
    move-result-object v97

    #@454
    aput-object v97, v8, v9

    #@456
    const/4 v9, 0x1

    #@457
    invoke-static/range {v94 .. v95}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@45a
    move-result-object v97

    #@45b
    aput-object v97, v8, v9

    #@45d
    const/4 v9, 0x2

    #@45e
    invoke-static/range {v79 .. v80}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@461
    move-result-object v97

    #@462
    aput-object v97, v8, v9

    #@464
    move-object/from16 v0, p1

    #@466
    move/from16 v1, v77

    #@468
    move-object/from16 v2, v17

    #@46a
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@46d
    .line 1346
    :cond_46d
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->hasUserActivity()Z

    #@470
    move-result v5

    #@471
    if-eqz v5, :cond_4a3

    #@473
    .line 1347
    const/4 v5, 0x3

    #@474
    new-array v12, v5, [Ljava/lang/Object;

    #@476
    .line 1348
    const/16 v29, 0x0

    #@478
    .line 1349
    .local v29, hasData:Z
    const/16 v30, 0x0

    #@47a
    :goto_47a
    const/4 v5, 0x3

    #@47b
    move/from16 v0, v30

    #@47d
    if-ge v0, v5, :cond_496

    #@47f
    .line 1350
    move-object/from16 v0, v76

    #@481
    move/from16 v1, v30

    #@483
    move/from16 v2, p2

    #@485
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats$Uid;->getUserActivityCount(II)I

    #@488
    move-result v83

    #@489
    .line 1351
    .local v83, val:I
    invoke-static/range {v83 .. v83}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48c
    move-result-object v5

    #@48d
    aput-object v5, v12, v30

    #@48f
    .line 1352
    if-eqz v83, :cond_493

    #@491
    const/16 v29, 0x1

    #@493
    .line 1349
    :cond_493
    add-int/lit8 v30, v30, 0x1

    #@495
    goto :goto_47a

    #@496
    .line 1354
    .end local v83           #val:I
    :cond_496
    if-eqz v29, :cond_4a3

    #@498
    .line 1355
    const/4 v5, 0x0

    #@499
    const-string/jumbo v8, "ua"

    #@49c
    move-object/from16 v0, p1

    #@49e
    move-object/from16 v1, v17

    #@4a0
    invoke-static {v0, v5, v1, v8, v12}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@4a3
    .line 1359
    .end local v29           #hasData:Z
    :cond_4a3
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->getWakelockStats()Ljava/util/Map;

    #@4a6
    move-result-object v84

    #@4a7
    .line 1360
    .restart local v84       #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v84 .. v84}, Ljava/util/Map;->size()I

    #@4aa
    move-result v5

    #@4ab
    if-lez v5, :cond_520

    #@4ad
    .line 1362
    invoke-interface/range {v84 .. v84}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@4b0
    move-result-object v5

    #@4b1
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@4b4
    move-result-object v31

    #@4b5
    .restart local v31       #i$:Ljava/util/Iterator;
    :cond_4b5
    :goto_4b5
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@4b8
    move-result v5

    #@4b9
    if-eqz v5, :cond_520

    #@4bb
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4be
    move-result-object v23

    #@4bf
    check-cast v23, Ljava/util/Map$Entry;

    #@4c1
    .line 1363
    .restart local v23       #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@4c4
    move-result-object v96

    #@4c5
    check-cast v96, Landroid/os/BatteryStats$Uid$Wakelock;

    #@4c7
    .line 1364
    .restart local v96       #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    const-string v10, ""

    #@4c9
    .line 1365
    .local v10, linePrefix:Ljava/lang/String;
    const/4 v5, 0x0

    #@4ca
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@4cd
    .line 1366
    const/4 v5, 0x1

    #@4ce
    move-object/from16 v0, v96

    #@4d0
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@4d3
    move-result-object v5

    #@4d4
    const-string v8, "f"

    #@4d6
    move/from16 v9, p2

    #@4d8
    invoke-static/range {v4 .. v10}, Landroid/os/BatteryStats;->printWakeLockCheckin(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@4db
    move-result-object v10

    #@4dc
    .line 1368
    const/4 v5, 0x0

    #@4dd
    move-object/from16 v0, v96

    #@4df
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@4e2
    move-result-object v5

    #@4e3
    const-string/jumbo v8, "p"

    #@4e6
    move/from16 v9, p2

    #@4e8
    invoke-static/range {v4 .. v10}, Landroid/os/BatteryStats;->printWakeLockCheckin(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@4eb
    move-result-object v10

    #@4ec
    .line 1370
    const/4 v5, 0x2

    #@4ed
    move-object/from16 v0, v96

    #@4ef
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@4f2
    move-result-object v5

    #@4f3
    const-string/jumbo v8, "w"

    #@4f6
    move/from16 v9, p2

    #@4f8
    invoke-static/range {v4 .. v10}, Landroid/os/BatteryStats;->printWakeLockCheckin(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@4fb
    move-result-object v10

    #@4fc
    .line 1374
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    #@4ff
    move-result v5

    #@500
    if-lez v5, :cond_4b5

    #@502
    .line 1375
    const-string/jumbo v5, "wl"

    #@505
    const/4 v8, 0x2

    #@506
    new-array v8, v8, [Ljava/lang/Object;

    #@508
    const/4 v9, 0x0

    #@509
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@50c
    move-result-object v97

    #@50d
    aput-object v97, v8, v9

    #@50f
    const/4 v9, 0x1

    #@510
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@513
    move-result-object v97

    #@514
    aput-object v97, v8, v9

    #@516
    move-object/from16 v0, p1

    #@518
    move/from16 v1, v77

    #@51a
    move-object/from16 v2, v17

    #@51c
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@51f
    goto :goto_4b5

    #@520
    .line 1380
    .end local v10           #linePrefix:Ljava/lang/String;
    .end local v23           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v96           #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    :cond_520
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->getSensorStats()Ljava/util/Map;

    #@523
    move-result-object v56

    #@524
    .line 1381
    .local v56, sensors:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    invoke-interface/range {v56 .. v56}, Ljava/util/Map;->size()I

    #@527
    move-result v5

    #@528
    if-lez v5, :cond_597

    #@52a
    .line 1383
    invoke-interface/range {v56 .. v56}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@52d
    move-result-object v5

    #@52e
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@531
    move-result-object v31

    #@532
    .restart local v31       #i$:Ljava/util/Iterator;
    :cond_532
    :goto_532
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@535
    move-result v5

    #@536
    if-eqz v5, :cond_597

    #@538
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53b
    move-result-object v19

    #@53c
    check-cast v19, Ljava/util/Map$Entry;

    #@53e
    .line 1384
    .local v19, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@541
    move-result-object v54

    #@542
    check-cast v54, Landroid/os/BatteryStats$Uid$Sensor;

    #@544
    .line 1385
    .local v54, se:Landroid/os/BatteryStats$Uid$Sensor;
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@547
    move-result-object v5

    #@548
    check-cast v5, Ljava/lang/Integer;

    #@54a
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@54d
    move-result v55

    #@54e
    .line 1386
    .local v55, sensorNumber:I
    invoke-virtual/range {v54 .. v54}, Landroid/os/BatteryStats$Uid$Sensor;->getSensorTime()Landroid/os/BatteryStats$Timer;

    #@551
    move-result-object v65

    #@552
    .line 1387
    .local v65, timer:Landroid/os/BatteryStats$Timer;
    if-eqz v65, :cond_532

    #@554
    .line 1389
    move-object/from16 v0, v65

    #@556
    move/from16 v1, p2

    #@558
    invoke-virtual {v0, v6, v7, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@55b
    move-result-wide v8

    #@55c
    const-wide/16 v97, 0x1f4

    #@55e
    add-long v8, v8, v97

    #@560
    const-wide/16 v97, 0x3e8

    #@562
    div-long v68, v8, v97

    #@564
    .line 1390
    .local v68, totalTime:J
    move-object/from16 v0, v65

    #@566
    move/from16 v1, p2

    #@568
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Timer;->getCountLocked(I)I

    #@56b
    move-result v18

    #@56c
    .line 1391
    .local v18, count:I
    const-wide/16 v8, 0x0

    #@56e
    cmp-long v5, v68, v8

    #@570
    if-eqz v5, :cond_532

    #@572
    .line 1392
    const-string/jumbo v5, "sr"

    #@575
    const/4 v8, 0x3

    #@576
    new-array v8, v8, [Ljava/lang/Object;

    #@578
    const/4 v9, 0x0

    #@579
    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@57c
    move-result-object v97

    #@57d
    aput-object v97, v8, v9

    #@57f
    const/4 v9, 0x1

    #@580
    invoke-static/range {v68 .. v69}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@583
    move-result-object v97

    #@584
    aput-object v97, v8, v9

    #@586
    const/4 v9, 0x2

    #@587
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@58a
    move-result-object v97

    #@58b
    aput-object v97, v8, v9

    #@58d
    move-object/from16 v0, p1

    #@58f
    move/from16 v1, v77

    #@591
    move-object/from16 v2, v17

    #@593
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@596
    goto :goto_532

    #@597
    .line 1398
    .end local v18           #count:I
    .end local v19           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v54           #se:Landroid/os/BatteryStats$Uid$Sensor;
    .end local v55           #sensorNumber:I
    .end local v65           #timer:Landroid/os/BatteryStats$Timer;
    .end local v68           #totalTime:J
    :cond_597
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->getProcessStats()Ljava/util/Map;

    #@59a
    move-result-object v42

    #@59b
    .line 1399
    .local v42, processStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    invoke-interface/range {v42 .. v42}, Ljava/util/Map;->size()I

    #@59e
    move-result v5

    #@59f
    if-lez v5, :cond_615

    #@5a1
    .line 1401
    invoke-interface/range {v42 .. v42}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@5a4
    move-result-object v5

    #@5a5
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5a8
    move-result-object v31

    #@5a9
    .restart local v31       #i$:Ljava/util/Iterator;
    :cond_5a9
    :goto_5a9
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@5ac
    move-result v5

    #@5ad
    if-eqz v5, :cond_615

    #@5af
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@5b2
    move-result-object v22

    #@5b3
    check-cast v22, Ljava/util/Map$Entry;

    #@5b5
    .line 1402
    .local v22, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@5b8
    move-result-object v43

    #@5b9
    check-cast v43, Landroid/os/BatteryStats$Uid$Proc;

    #@5bb
    .line 1404
    .local v43, ps:Landroid/os/BatteryStats$Uid$Proc;
    move-object/from16 v0, v43

    #@5bd
    move/from16 v1, p2

    #@5bf
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getUserTime(I)J

    #@5c2
    move-result-wide v81

    #@5c3
    .line 1405
    .local v81, userTime:J
    move-object/from16 v0, v43

    #@5c5
    move/from16 v1, p2

    #@5c7
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getSystemTime(I)J

    #@5ca
    move-result-wide v63

    #@5cb
    .line 1406
    .local v63, systemTime:J
    move-object/from16 v0, v43

    #@5cd
    move/from16 v1, p2

    #@5cf
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getStarts(I)I

    #@5d2
    move-result v62

    #@5d3
    .line 1408
    .local v62, starts:I
    const-wide/16 v8, 0x0

    #@5d5
    cmp-long v5, v81, v8

    #@5d7
    if-nez v5, :cond_5e1

    #@5d9
    const-wide/16 v8, 0x0

    #@5db
    cmp-long v5, v63, v8

    #@5dd
    if-nez v5, :cond_5e1

    #@5df
    if-eqz v62, :cond_5a9

    #@5e1
    .line 1409
    :cond_5e1
    const-string/jumbo v5, "pr"

    #@5e4
    const/4 v8, 0x4

    #@5e5
    new-array v8, v8, [Ljava/lang/Object;

    #@5e7
    const/4 v9, 0x0

    #@5e8
    invoke-interface/range {v22 .. v22}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@5eb
    move-result-object v97

    #@5ec
    aput-object v97, v8, v9

    #@5ee
    const/4 v9, 0x1

    #@5ef
    const-wide/16 v97, 0xa

    #@5f1
    mul-long v97, v97, v81

    #@5f3
    invoke-static/range {v97 .. v98}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5f6
    move-result-object v97

    #@5f7
    aput-object v97, v8, v9

    #@5f9
    const/4 v9, 0x2

    #@5fa
    const-wide/16 v97, 0xa

    #@5fc
    mul-long v97, v97, v63

    #@5fe
    invoke-static/range {v97 .. v98}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@601
    move-result-object v97

    #@602
    aput-object v97, v8, v9

    #@604
    const/4 v9, 0x3

    #@605
    invoke-static/range {v62 .. v62}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@608
    move-result-object v97

    #@609
    aput-object v97, v8, v9

    #@60b
    move-object/from16 v0, p1

    #@60d
    move/from16 v1, v77

    #@60f
    move-object/from16 v2, v17

    #@611
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@614
    goto :goto_5a9

    #@615
    .line 1418
    .end local v22           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    .end local v31           #i$:Ljava/util/Iterator;
    .end local v43           #ps:Landroid/os/BatteryStats$Uid$Proc;
    .end local v62           #starts:I
    .end local v63           #systemTime:J
    .end local v81           #userTime:J
    :cond_615
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid;->getPackageStats()Ljava/util/Map;

    #@618
    move-result-object v36

    #@619
    .line 1419
    .local v36, packageStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    invoke-interface/range {v36 .. v36}, Ljava/util/Map;->size()I

    #@61c
    move-result v5

    #@61d
    if-lez v5, :cond_3d9

    #@61f
    .line 1421
    invoke-interface/range {v36 .. v36}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@622
    move-result-object v5

    #@623
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@626
    move-result-object v31

    #@627
    :cond_627
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    #@62a
    move-result v5

    #@62b
    if-eqz v5, :cond_3d9

    #@62d
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@630
    move-result-object v21

    #@631
    check-cast v21, Ljava/util/Map$Entry;

    #@633
    .line 1423
    .local v21, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@636
    move-result-object v43

    #@637
    check-cast v43, Landroid/os/BatteryStats$Uid$Pkg;

    #@639
    .line 1424
    .local v43, ps:Landroid/os/BatteryStats$Uid$Pkg;
    move-object/from16 v0, v43

    #@63b
    move/from16 v1, p2

    #@63d
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg;->getWakeups(I)I

    #@640
    move-result v85

    #@641
    .line 1425
    .local v85, wakeups:I
    invoke-virtual/range {v43 .. v43}, Landroid/os/BatteryStats$Uid$Pkg;->getServiceStats()Ljava/util/Map;

    #@644
    move-result-object v58

    #@645
    .line 1427
    .local v58, serviceStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    invoke-interface/range {v58 .. v58}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@648
    move-result-object v5

    #@649
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@64c
    move-result-object v32

    #@64d
    .local v32, i$:Ljava/util/Iterator;
    :cond_64d
    :goto_64d
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    #@650
    move-result v5

    #@651
    if-eqz v5, :cond_627

    #@653
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@656
    move-result-object v57

    #@657
    check-cast v57, Ljava/util/Map$Entry;

    #@659
    .line 1428
    .local v57, sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    invoke-interface/range {v57 .. v57}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@65c
    move-result-object v59

    #@65d
    check-cast v59, Landroid/os/BatteryStats$Uid$Pkg$Serv;

    #@65f
    .line 1429
    .local v59, ss:Landroid/os/BatteryStats$Uid$Pkg$Serv;
    move-object/from16 v0, v59

    #@661
    move/from16 v1, p2

    #@663
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getStartTime(JI)J

    #@666
    move-result-wide v60

    #@667
    .line 1430
    .local v60, startTime:J
    move-object/from16 v0, v59

    #@669
    move/from16 v1, p2

    #@66b
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getStarts(I)I

    #@66e
    move-result v62

    #@66f
    .line 1431
    .restart local v62       #starts:I
    move-object/from16 v0, v59

    #@671
    move/from16 v1, p2

    #@673
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getLaunches(I)I

    #@676
    move-result v35

    #@677
    .line 1432
    .local v35, launches:I
    const-wide/16 v8, 0x0

    #@679
    cmp-long v5, v60, v8

    #@67b
    if-nez v5, :cond_681

    #@67d
    if-nez v62, :cond_681

    #@67f
    if-eqz v35, :cond_64d

    #@681
    .line 1433
    :cond_681
    const-string v5, "apk"

    #@683
    const/4 v8, 0x6

    #@684
    new-array v8, v8, [Ljava/lang/Object;

    #@686
    const/4 v9, 0x0

    #@687
    invoke-static/range {v85 .. v85}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68a
    move-result-object v97

    #@68b
    aput-object v97, v8, v9

    #@68d
    const/4 v9, 0x1

    #@68e
    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@691
    move-result-object v97

    #@692
    aput-object v97, v8, v9

    #@694
    const/4 v9, 0x2

    #@695
    invoke-interface/range {v57 .. v57}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@698
    move-result-object v97

    #@699
    aput-object v97, v8, v9

    #@69b
    const/4 v9, 0x3

    #@69c
    const-wide/16 v97, 0x3e8

    #@69e
    div-long v97, v60, v97

    #@6a0
    invoke-static/range {v97 .. v98}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@6a3
    move-result-object v97

    #@6a4
    aput-object v97, v8, v9

    #@6a6
    const/4 v9, 0x4

    #@6a7
    invoke-static/range {v62 .. v62}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6aa
    move-result-object v97

    #@6ab
    aput-object v97, v8, v9

    #@6ad
    const/4 v9, 0x5

    #@6ae
    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b1
    move-result-object v97

    #@6b2
    aput-object v97, v8, v9

    #@6b4
    move-object/from16 v0, p1

    #@6b6
    move/from16 v1, v77

    #@6b8
    move-object/from16 v2, v17

    #@6ba
    invoke-static {v0, v1, v2, v5, v8}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@6bd
    goto :goto_64d

    #@6be
    .line 1445
    .end local v21           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    .end local v27           #fullWifiLockOnTime:J
    .end local v32           #i$:Ljava/util/Iterator;
    .end local v35           #launches:I
    .end local v36           #packageStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    .end local v42           #processStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    .end local v43           #ps:Landroid/os/BatteryStats$Uid$Pkg;
    .end local v48           #rx:J
    .end local v56           #sensors:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    .end local v57           #sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    .end local v58           #serviceStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    .end local v59           #ss:Landroid/os/BatteryStats$Uid$Pkg$Serv;
    .end local v60           #startTime:J
    .end local v62           #starts:I
    .end local v72           #tx:J
    .end local v76           #u:Landroid/os/BatteryStats$Uid;
    .end local v77           #uid:I
    .end local v79           #uidWifiRunningTime:J
    .end local v84           #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v85           #wakeups:I
    .end local v94           #wifiScanTime:J
    :cond_6be
    return-void
.end method

.method public dumpCheckinLocked(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/util/List;)V
    .registers 27
    .parameter "pw"
    .parameter "args"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 2240
    .local p3, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->prepareForDumpLocked()V

    #@3
    .line 2242
    const/4 v11, 0x0

    #@4
    .line 2244
    .local v11, isUnpluggedOnly:Z
    move-object/from16 v8, p2

    #@6
    .local v8, arr$:[Ljava/lang/String;
    array-length v13, v8

    #@7
    .local v13, len$:I
    const/4 v10, 0x0

    #@8
    .local v10, i$:I
    :goto_8
    if-ge v10, v13, :cond_1a

    #@a
    aget-object v7, v8, v10

    #@c
    .line 2245
    .local v7, arg:Ljava/lang/String;
    const-string v19, "-u"

    #@e
    move-object/from16 v0, v19

    #@10
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v19

    #@14
    if-eqz v19, :cond_17

    #@16
    .line 2247
    const/4 v11, 0x1

    #@17
    .line 2244
    :cond_17
    add-int/lit8 v10, v10, 0x1

    #@19
    goto :goto_8

    #@1a
    .line 2251
    .end local v7           #arg:Ljava/lang/String;
    :cond_1a
    if-eqz p3, :cond_b6

    #@1c
    .line 2252
    new-instance v18, Landroid/util/SparseArray;

    #@1e
    invoke-direct/range {v18 .. v18}, Landroid/util/SparseArray;-><init>()V

    #@21
    .line 2253
    .local v18, uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    const/4 v9, 0x0

    #@22
    .local v9, i:I
    :goto_22
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    #@25
    move-result v19

    #@26
    move/from16 v0, v19

    #@28
    if-ge v9, v0, :cond_5a

    #@2a
    .line 2254
    move-object/from16 v0, p3

    #@2c
    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@2f
    move-result-object v6

    #@30
    check-cast v6, Landroid/content/pm/ApplicationInfo;

    #@32
    .line 2255
    .local v6, ai:Landroid/content/pm/ApplicationInfo;
    iget v0, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@34
    move/from16 v19, v0

    #@36
    invoke-virtual/range {v18 .. v19}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v15

    #@3a
    check-cast v15, Ljava/util/ArrayList;

    #@3c
    .line 2256
    .local v15, pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v15, :cond_4e

    #@3e
    .line 2257
    new-instance v15, Ljava/util/ArrayList;

    #@40
    .end local v15           #pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    #@43
    .line 2258
    .restart local v15       #pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget v0, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    #@45
    move/from16 v19, v0

    #@47
    move-object/from16 v0, v18

    #@49
    move/from16 v1, v19

    #@4b
    invoke-virtual {v0, v1, v15}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@4e
    .line 2260
    :cond_4e
    iget-object v0, v6, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@50
    move-object/from16 v19, v0

    #@52
    move-object/from16 v0, v19

    #@54
    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@57
    .line 2253
    add-int/lit8 v9, v9, 0x1

    #@59
    goto :goto_22

    #@5a
    .line 2262
    .end local v6           #ai:Landroid/content/pm/ApplicationInfo;
    .end local v15           #pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5a
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    #@5d
    move-result-object v17

    #@5e
    .line 2263
    .local v17, uidStats:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid;>;"
    invoke-virtual/range {v17 .. v17}, Landroid/util/SparseArray;->size()I

    #@61
    move-result v5

    #@62
    .line 2264
    .local v5, NU:I
    const/16 v19, 0x2

    #@64
    move/from16 v0, v19

    #@66
    new-array v14, v0, [Ljava/lang/String;

    #@68
    .line 2265
    .local v14, lineArgs:[Ljava/lang/String;
    const/4 v9, 0x0

    #@69
    :goto_69
    if-ge v9, v5, :cond_b6

    #@6b
    .line 2266
    move-object/from16 v0, v17

    #@6d
    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->keyAt(I)I

    #@70
    move-result v16

    #@71
    .line 2267
    .local v16, uid:I
    move-object/from16 v0, v18

    #@73
    move/from16 v1, v16

    #@75
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@78
    move-result-object v15

    #@79
    check-cast v15, Ljava/util/ArrayList;

    #@7b
    .line 2268
    .restart local v15       #pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v15, :cond_b3

    #@7d
    .line 2269
    const/4 v12, 0x0

    #@7e
    .local v12, j:I
    :goto_7e
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    #@81
    move-result v19

    #@82
    move/from16 v0, v19

    #@84
    if-ge v12, v0, :cond_b3

    #@86
    .line 2270
    const/16 v19, 0x0

    #@88
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@8b
    move-result-object v20

    #@8c
    aput-object v20, v14, v19

    #@8e
    .line 2271
    const/16 v20, 0x1

    #@90
    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@93
    move-result-object v19

    #@94
    check-cast v19, Ljava/lang/String;

    #@96
    aput-object v19, v14, v20

    #@98
    .line 2272
    const/16 v20, 0x0

    #@9a
    const-string v21, "i"

    #@9c
    const-string/jumbo v22, "uid"

    #@9f
    move-object/from16 v19, v14

    #@a1
    check-cast v19, [Ljava/lang/Object;

    #@a3
    move-object/from16 v0, p1

    #@a5
    move/from16 v1, v20

    #@a7
    move-object/from16 v2, v21

    #@a9
    move-object/from16 v3, v22

    #@ab
    move-object/from16 v4, v19

    #@ad
    invoke-static {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->dumpLine(Ljava/io/PrintWriter;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    #@b0
    .line 2269
    add-int/lit8 v12, v12, 0x1

    #@b2
    goto :goto_7e

    #@b3
    .line 2265
    .end local v12           #j:I
    :cond_b3
    add-int/lit8 v9, v9, 0x1

    #@b5
    goto :goto_69

    #@b6
    .line 2278
    .end local v5           #NU:I
    .end local v9           #i:I
    .end local v14           #lineArgs:[Ljava/lang/String;
    .end local v15           #pkgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16           #uid:I
    .end local v17           #uidStats:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid;>;"
    .end local v18           #uids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :cond_b6
    if-eqz v11, :cond_c8

    #@b8
    .line 2279
    const/16 v19, 0x3

    #@ba
    const/16 v20, -0x1

    #@bc
    move-object/from16 v0, p0

    #@be
    move-object/from16 v1, p1

    #@c0
    move/from16 v2, v19

    #@c2
    move/from16 v3, v20

    #@c4
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->dumpCheckinLocked(Ljava/io/PrintWriter;II)V

    #@c7
    .line 2285
    :goto_c7
    return-void

    #@c8
    .line 2282
    :cond_c8
    const/16 v19, 0x0

    #@ca
    const/16 v20, -0x1

    #@cc
    move-object/from16 v0, p0

    #@ce
    move-object/from16 v1, p1

    #@d0
    move/from16 v2, v19

    #@d2
    move/from16 v3, v20

    #@d4
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->dumpCheckinLocked(Ljava/io/PrintWriter;II)V

    #@d7
    .line 2283
    const/16 v19, 0x3

    #@d9
    const/16 v20, -0x1

    #@db
    move-object/from16 v0, p0

    #@dd
    move-object/from16 v1, p1

    #@df
    move/from16 v2, v19

    #@e1
    move/from16 v3, v20

    #@e3
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->dumpCheckinLocked(Ljava/io/PrintWriter;II)V

    #@e6
    goto :goto_c7
.end method

.method public dumpLocked(Ljava/io/PrintWriter;)V
    .registers 29
    .parameter "pw"

    #@0
    .prologue
    .line 2177
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->prepareForDumpLocked()V

    #@3
    .line 2179
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getHistoryBaseTime()J

    #@6
    move-result-wide v21

    #@7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@a
    move-result-wide v23

    #@b
    add-long v10, v21, v23

    #@d
    .line 2181
    .local v10, now:J
    new-instance v16, Landroid/os/BatteryStats$HistoryItem;

    #@f
    invoke-direct/range {v16 .. v16}, Landroid/os/BatteryStats$HistoryItem;-><init>()V

    #@12
    .line 2182
    .local v16, rec:Landroid/os/BatteryStats$HistoryItem;
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingHistoryLocked()Z

    #@15
    move-result v21

    #@16
    if-eqz v21, :cond_44

    #@18
    .line 2183
    const-string v21, "Battery History:"

    #@1a
    move-object/from16 v0, p1

    #@1c
    move-object/from16 v1, v21

    #@1e
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@21
    .line 2184
    new-instance v7, Landroid/os/BatteryStats$HistoryPrinter;

    #@23
    invoke-direct {v7}, Landroid/os/BatteryStats$HistoryPrinter;-><init>()V

    #@26
    .line 2185
    .local v7, hprinter:Landroid/os/BatteryStats$HistoryPrinter;
    :goto_26
    move-object/from16 v0, p0

    #@28
    move-object/from16 v1, v16

    #@2a
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    #@2d
    move-result v21

    #@2e
    if-eqz v21, :cond_38

    #@30
    .line 2186
    move-object/from16 v0, p1

    #@32
    move-object/from16 v1, v16

    #@34
    invoke-virtual {v7, v0, v1, v10, v11}, Landroid/os/BatteryStats$HistoryPrinter;->printNextItem(Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;J)V

    #@37
    goto :goto_26

    #@38
    .line 2188
    :cond_38
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingHistoryLocked()V

    #@3b
    .line 2189
    const-string v21, ""

    #@3d
    move-object/from16 v0, p1

    #@3f
    move-object/from16 v1, v21

    #@41
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@44
    .line 2192
    .end local v7           #hprinter:Landroid/os/BatteryStats$HistoryPrinter;
    :cond_44
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->startIteratingOldHistoryLocked()Z

    #@47
    move-result v21

    #@48
    if-eqz v21, :cond_76

    #@4a
    .line 2193
    const-string v21, "Old battery History:"

    #@4c
    move-object/from16 v0, p1

    #@4e
    move-object/from16 v1, v21

    #@50
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53
    .line 2194
    new-instance v7, Landroid/os/BatteryStats$HistoryPrinter;

    #@55
    invoke-direct {v7}, Landroid/os/BatteryStats$HistoryPrinter;-><init>()V

    #@58
    .line 2195
    .restart local v7       #hprinter:Landroid/os/BatteryStats$HistoryPrinter;
    :goto_58
    move-object/from16 v0, p0

    #@5a
    move-object/from16 v1, v16

    #@5c
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getNextOldHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z

    #@5f
    move-result v21

    #@60
    if-eqz v21, :cond_6a

    #@62
    .line 2196
    move-object/from16 v0, p1

    #@64
    move-object/from16 v1, v16

    #@66
    invoke-virtual {v7, v0, v1, v10, v11}, Landroid/os/BatteryStats$HistoryPrinter;->printNextItem(Ljava/io/PrintWriter;Landroid/os/BatteryStats$HistoryItem;J)V

    #@69
    goto :goto_58

    #@6a
    .line 2198
    :cond_6a
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->finishIteratingOldHistoryLocked()V

    #@6d
    .line 2199
    const-string v21, ""

    #@6f
    move-object/from16 v0, p1

    #@71
    move-object/from16 v1, v21

    #@73
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@76
    .line 2202
    .end local v7           #hprinter:Landroid/os/BatteryStats$HistoryPrinter;
    :cond_76
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    #@79
    move-result-object v20

    #@7a
    .line 2203
    .local v20, uidStats:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid;>;"
    invoke-virtual/range {v20 .. v20}, Landroid/util/SparseArray;->size()I

    #@7d
    move-result v5

    #@7e
    .line 2204
    .local v5, NU:I
    const/4 v6, 0x0

    #@7f
    .line 2205
    .local v6, didPid:Z
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@82
    move-result-wide v12

    #@83
    .line 2206
    .local v12, nowRealtime:J
    const/4 v8, 0x0

    #@84
    .local v8, i:I
    :goto_84
    if-ge v8, v5, :cond_fb

    #@86
    .line 2207
    move-object/from16 v0, v20

    #@88
    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@8b
    move-result-object v19

    #@8c
    check-cast v19, Landroid/os/BatteryStats$Uid;

    #@8e
    .line 2208
    .local v19, uid:Landroid/os/BatteryStats$Uid;
    invoke-virtual/range {v19 .. v19}, Landroid/os/BatteryStats$Uid;->getPidStats()Landroid/util/SparseArray;

    #@91
    move-result-object v15

    #@92
    .line 2209
    .local v15, pids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid$Pid;>;"
    if-eqz v15, :cond_f8

    #@94
    .line 2210
    const/4 v9, 0x0

    #@95
    .local v9, j:I
    :goto_95
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    #@98
    move-result v21

    #@99
    move/from16 v0, v21

    #@9b
    if-ge v9, v0, :cond_f8

    #@9d
    .line 2211
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@a0
    move-result-object v14

    #@a1
    check-cast v14, Landroid/os/BatteryStats$Uid$Pid;

    #@a3
    .line 2212
    .local v14, pid:Landroid/os/BatteryStats$Uid$Pid;
    if-nez v6, :cond_af

    #@a5
    .line 2213
    const-string v21, "Per-PID Stats:"

    #@a7
    move-object/from16 v0, p1

    #@a9
    move-object/from16 v1, v21

    #@ab
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ae
    .line 2214
    const/4 v6, 0x1

    #@af
    .line 2216
    :cond_af
    iget-wide v0, v14, Landroid/os/BatteryStats$Uid$Pid;->mWakeSum:J

    #@b1
    move-wide/from16 v23, v0

    #@b3
    iget-wide v0, v14, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@b5
    move-wide/from16 v21, v0

    #@b7
    const-wide/16 v25, 0x0

    #@b9
    cmp-long v21, v21, v25

    #@bb
    if-eqz v21, :cond_f5

    #@bd
    iget-wide v0, v14, Landroid/os/BatteryStats$Uid$Pid;->mWakeStart:J

    #@bf
    move-wide/from16 v21, v0

    #@c1
    sub-long v21, v12, v21

    #@c3
    :goto_c3
    add-long v17, v23, v21

    #@c5
    .line 2218
    .local v17, time:J
    const-string v21, "  PID "

    #@c7
    move-object/from16 v0, p1

    #@c9
    move-object/from16 v1, v21

    #@cb
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ce
    invoke-virtual {v15, v9}, Landroid/util/SparseArray;->keyAt(I)I

    #@d1
    move-result v21

    #@d2
    move-object/from16 v0, p1

    #@d4
    move/from16 v1, v21

    #@d6
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@d9
    .line 2219
    const-string v21, " wake time: "

    #@db
    move-object/from16 v0, p1

    #@dd
    move-object/from16 v1, v21

    #@df
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e2
    .line 2220
    move-wide/from16 v0, v17

    #@e4
    move-object/from16 v2, p1

    #@e6
    invoke-static {v0, v1, v2}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@e9
    .line 2221
    const-string v21, ""

    #@eb
    move-object/from16 v0, p1

    #@ed
    move-object/from16 v1, v21

    #@ef
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f2
    .line 2210
    add-int/lit8 v9, v9, 0x1

    #@f4
    goto :goto_95

    #@f5
    .line 2216
    .end local v17           #time:J
    :cond_f5
    const-wide/16 v21, 0x0

    #@f7
    goto :goto_c3

    #@f8
    .line 2206
    .end local v9           #j:I
    .end local v14           #pid:Landroid/os/BatteryStats$Uid$Pid;
    :cond_f8
    add-int/lit8 v8, v8, 0x1

    #@fa
    goto :goto_84

    #@fb
    .line 2225
    .end local v15           #pids:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid$Pid;>;"
    .end local v19           #uid:Landroid/os/BatteryStats$Uid;
    :cond_fb
    if-eqz v6, :cond_106

    #@fd
    .line 2226
    const-string v21, ""

    #@ff
    move-object/from16 v0, p1

    #@101
    move-object/from16 v1, v21

    #@103
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@106
    .line 2229
    :cond_106
    const-string v21, "Statistics since last charge:"

    #@108
    move-object/from16 v0, p1

    #@10a
    move-object/from16 v1, v21

    #@10c
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@10f
    .line 2230
    new-instance v21, Ljava/lang/StringBuilder;

    #@111
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@114
    const-string v22, "  System starts: "

    #@116
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v21

    #@11a
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getStartCount()I

    #@11d
    move-result v22

    #@11e
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@121
    move-result-object v21

    #@122
    const-string v22, ", currently on battery: "

    #@124
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v21

    #@128
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getIsOnBattery()Z

    #@12b
    move-result v22

    #@12c
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v21

    #@130
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@133
    move-result-object v21

    #@134
    move-object/from16 v0, p1

    #@136
    move-object/from16 v1, v21

    #@138
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@13b
    .line 2232
    const-string v21, ""

    #@13d
    const/16 v22, 0x0

    #@13f
    const/16 v23, -0x1

    #@141
    move-object/from16 v0, p0

    #@143
    move-object/from16 v1, p1

    #@145
    move-object/from16 v2, v21

    #@147
    move/from16 v3, v22

    #@149
    move/from16 v4, v23

    #@14b
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->dumpLocked(Ljava/io/PrintWriter;Ljava/lang/String;II)V

    #@14e
    .line 2233
    const-string v21, ""

    #@150
    move-object/from16 v0, p1

    #@152
    move-object/from16 v1, v21

    #@154
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@157
    .line 2234
    const-string v21, "Statistics since last unplugged:"

    #@159
    move-object/from16 v0, p1

    #@15b
    move-object/from16 v1, v21

    #@15d
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@160
    .line 2235
    const-string v21, ""

    #@162
    const/16 v22, 0x3

    #@164
    const/16 v23, -0x1

    #@166
    move-object/from16 v0, p0

    #@168
    move-object/from16 v1, p1

    #@16a
    move-object/from16 v2, v21

    #@16c
    move/from16 v3, v22

    #@16e
    move/from16 v4, v23

    #@170
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->dumpLocked(Ljava/io/PrintWriter;Ljava/lang/String;II)V

    #@173
    .line 2236
    return-void
.end method

.method public final dumpLocked(Ljava/io/PrintWriter;Ljava/lang/String;II)V
    .registers 136
    .parameter "pw"
    .parameter "prefix"
    .parameter "which"
    .parameter "reqUid"

    #@0
    .prologue
    .line 1462
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v5

    #@4
    const-wide/16 v15, 0x3e8

    #@6
    mul-long v70, v5, v15

    #@8
    .line 1463
    .local v70, rawUptime:J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v5

    #@c
    const-wide/16 v15, 0x3e8

    #@e
    mul-long v68, v5, v15

    #@10
    .line 1464
    .local v68, rawRealtime:J
    move-object/from16 v0, p0

    #@12
    move-wide/from16 v1, v70

    #@14
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getBatteryUptime(J)J

    #@17
    move-result-wide v33

    #@18
    .line 1465
    .local v33, batteryUptime:J
    move-object/from16 v0, p0

    #@1a
    move-wide/from16 v1, v68

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getBatteryRealtime(J)J

    #@1f
    move-result-wide v13

    #@20
    .line 1467
    .local v13, batteryRealtime:J
    move-object/from16 v0, p0

    #@22
    move-wide/from16 v1, v70

    #@24
    move/from16 v3, p3

    #@26
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeBatteryUptime(JI)J

    #@29
    move-result-wide v122

    #@2a
    .line 1468
    .local v122, whichBatteryUptime:J
    move-object/from16 v0, p0

    #@2c
    move-wide/from16 v1, v68

    #@2e
    move/from16 v3, p3

    #@30
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeBatteryRealtime(JI)J

    #@33
    move-result-wide v120

    #@34
    .line 1469
    .local v120, whichBatteryRealtime:J
    move-object/from16 v0, p0

    #@36
    move-wide/from16 v1, v68

    #@38
    move/from16 v3, p3

    #@3a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeRealtime(JI)J

    #@3d
    move-result-wide v99

    #@3e
    .line 1470
    .local v99, totalRealtime:J
    move-object/from16 v0, p0

    #@40
    move-wide/from16 v1, v70

    #@42
    move/from16 v3, p3

    #@44
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats;->computeUptime(JI)J

    #@47
    move-result-wide v103

    #@48
    .line 1472
    .local v103, totalUptime:J
    new-instance v11, Ljava/lang/StringBuilder;

    #@4a
    const/16 v5, 0x80

    #@4c
    invoke-direct {v11, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@4f
    .line 1474
    .local v11, sb:Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getUidStats()Landroid/util/SparseArray;

    #@52
    move-result-object v112

    #@53
    .line 1475
    .local v112, uidStats:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/BatteryStats$Uid;>;"
    invoke-virtual/range {v112 .. v112}, Landroid/util/SparseArray;->size()I

    #@56
    move-result v31

    #@57
    .line 1477
    .local v31, NU:I
    const/4 v5, 0x0

    #@58
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@5b
    .line 1478
    move-object/from16 v0, p2

    #@5d
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    .line 1479
    const-string v5, "  Time on battery: "

    #@62
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    .line 1480
    const-wide/16 v5, 0x3e8

    #@67
    div-long v5, v120, v5

    #@69
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@6c
    const-string v5, "("

    #@6e
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 1481
    move-object/from16 v0, p0

    #@73
    move-wide/from16 v1, v120

    #@75
    move-wide/from16 v3, v99

    #@77
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 1482
    const-string v5, ") realtime, "

    #@80
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    .line 1483
    const-wide/16 v5, 0x3e8

    #@85
    div-long v5, v122, v5

    #@87
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@8a
    .line 1484
    const-string v5, "("

    #@8c
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-object/from16 v0, p0

    #@91
    move-wide/from16 v1, v122

    #@93
    move-wide/from16 v3, v99

    #@95
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@98
    move-result-object v5

    #@99
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    .line 1485
    const-string v5, ") uptime"

    #@9e
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    .line 1486
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a4
    move-result-object v5

    #@a5
    move-object/from16 v0, p1

    #@a7
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@aa
    .line 1487
    const/4 v5, 0x0

    #@ab
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@ae
    .line 1488
    move-object/from16 v0, p2

    #@b0
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    .line 1489
    const-string v5, "  Total run time: "

    #@b5
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    .line 1490
    const-wide/16 v5, 0x3e8

    #@ba
    div-long v5, v99, v5

    #@bc
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@bf
    .line 1491
    const-string/jumbo v5, "realtime, "

    #@c2
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    .line 1492
    const-wide/16 v5, 0x3e8

    #@c7
    div-long v5, v103, v5

    #@c9
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@cc
    .line 1493
    const-string/jumbo v5, "uptime, "

    #@cf
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    .line 1494
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d5
    move-result-object v5

    #@d6
    move-object/from16 v0, p1

    #@d8
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@db
    .line 1496
    move-object/from16 v0, p0

    #@dd
    move/from16 v1, p3

    #@df
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getScreenOnTime(JI)J

    #@e2
    move-result-wide v74

    #@e3
    .line 1497
    .local v74, screenOnTime:J
    move-object/from16 v0, p0

    #@e5
    move/from16 v1, p3

    #@e7
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getPhoneOnTime(JI)J

    #@ea
    move-result-wide v64

    #@eb
    .line 1498
    .local v64, phoneOnTime:J
    move-object/from16 v0, p0

    #@ed
    move/from16 v1, p3

    #@ef
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getGlobalWifiRunningTime(JI)J

    #@f2
    move-result-wide v126

    #@f3
    .line 1499
    .local v126, wifiRunningTime:J
    move-object/from16 v0, p0

    #@f5
    move/from16 v1, p3

    #@f7
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getWifiOnTime(JI)J

    #@fa
    move-result-wide v124

    #@fb
    .line 1500
    .local v124, wifiOnTime:J
    move-object/from16 v0, p0

    #@fd
    move/from16 v1, p3

    #@ff
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getBluetoothOnTime(JI)J

    #@102
    move-result-wide v35

    #@103
    .line 1501
    .local v35, bluetoothOnTime:J
    const/4 v5, 0x0

    #@104
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@107
    .line 1502
    move-object/from16 v0, p2

    #@109
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    .line 1503
    const-string v5, "  Screen on: "

    #@10e
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    const-wide/16 v5, 0x3e8

    #@113
    div-long v5, v74, v5

    #@115
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@118
    .line 1504
    const-string v5, "("

    #@11a
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-object/from16 v0, p0

    #@11f
    move-wide/from16 v1, v74

    #@121
    move-wide/from16 v3, v120

    #@123
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    .line 1505
    const-string v5, "), Input events: "

    #@12c
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-object/from16 v0, p0

    #@131
    move/from16 v1, p3

    #@133
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats;->getInputEventCount(I)I

    #@136
    move-result v5

    #@137
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13a
    .line 1506
    const-string v5, ", Active phone call: "

    #@13c
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13f
    const-wide/16 v5, 0x3e8

    #@141
    div-long v5, v64, v5

    #@143
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@146
    .line 1507
    const-string v5, "("

    #@148
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-object/from16 v0, p0

    #@14d
    move-wide/from16 v1, v64

    #@14f
    move-wide/from16 v3, v120

    #@151
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@154
    move-result-object v5

    #@155
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    .line 1508
    const-string v5, ")"

    #@15a
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    .line 1509
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v5

    #@161
    move-object/from16 v0, p1

    #@163
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@166
    .line 1510
    const/4 v5, 0x0

    #@167
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@16a
    .line 1511
    move-object/from16 v0, p2

    #@16c
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    .line 1512
    const-string v5, "  Screen brightnesses: "

    #@171
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    .line 1513
    const/16 v38, 0x0

    #@176
    .line 1514
    .local v38, didOne:Z
    const/16 v53, 0x0

    #@178
    .local v53, i:I
    :goto_178
    const/4 v5, 0x5

    #@179
    move/from16 v0, v53

    #@17b
    if-ge v0, v5, :cond_1c4

    #@17d
    .line 1515
    move-object/from16 v0, p0

    #@17f
    move/from16 v1, v53

    #@181
    move/from16 v2, p3

    #@183
    invoke-virtual {v0, v1, v13, v14, v2}, Landroid/os/BatteryStats;->getScreenBrightnessTime(IJI)J

    #@186
    move-result-wide v91

    #@187
    .line 1516
    .local v91, time:J
    const-wide/16 v5, 0x0

    #@189
    cmp-long v5, v91, v5

    #@18b
    if-nez v5, :cond_190

    #@18d
    .line 1514
    :goto_18d
    add-int/lit8 v53, v53, 0x1

    #@18f
    goto :goto_178

    #@190
    .line 1519
    :cond_190
    if-eqz v38, :cond_197

    #@192
    const-string v5, ", "

    #@194
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    .line 1520
    :cond_197
    const/16 v38, 0x1

    #@199
    .line 1521
    sget-object v5, Landroid/os/BatteryStats;->SCREEN_BRIGHTNESS_NAMES:[Ljava/lang/String;

    #@19b
    aget-object v5, v5, v53

    #@19d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    .line 1522
    const-string v5, " "

    #@1a2
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    .line 1523
    const-wide/16 v5, 0x3e8

    #@1a7
    div-long v5, v91, v5

    #@1a9
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@1ac
    .line 1524
    const-string v5, "("

    #@1ae
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    .line 1525
    move-object/from16 v0, p0

    #@1b3
    move-wide/from16 v1, v91

    #@1b5
    move-wide/from16 v3, v74

    #@1b7
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@1ba
    move-result-object v5

    #@1bb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    .line 1526
    const-string v5, ")"

    #@1c0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c3
    goto :goto_18d

    #@1c4
    .line 1528
    .end local v91           #time:J
    :cond_1c4
    if-nez v38, :cond_1cb

    #@1c6
    const-string v5, "No activity"

    #@1c8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    .line 1529
    :cond_1cb
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ce
    move-result-object v5

    #@1cf
    move-object/from16 v0, p1

    #@1d1
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d4
    .line 1532
    const-wide/16 v72, 0x0

    #@1d6
    .line 1533
    .local v72, rxTotal:J
    const-wide/16 v107, 0x0

    #@1d8
    .line 1534
    .local v107, txTotal:J
    const-wide/16 v46, 0x0

    #@1da
    .line 1535
    .local v46, fullWakeLockTimeTotalMicros:J
    const-wide/16 v62, 0x0

    #@1dc
    .line 1537
    .local v62, partialWakeLockTimeTotalMicros:J
    new-instance v93, Landroid/os/BatteryStats$1;

    #@1de
    move-object/from16 v0, v93

    #@1e0
    move-object/from16 v1, p0

    #@1e2
    invoke-direct {v0, v1}, Landroid/os/BatteryStats$1;-><init>(Landroid/os/BatteryStats;)V

    #@1e5
    .line 1552
    .local v93, timerComparator:Ljava/util/Comparator;,"Ljava/util/Comparator<Landroid/os/BatteryStats$TimerEntry;>;"
    if-gez p4, :cond_282

    #@1e7
    .line 1553
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getKernelWakelockStats()Ljava/util/Map;

    #@1ea
    move-result-object v57

    #@1eb
    .line 1554
    .local v57, kernelWakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    invoke-interface/range {v57 .. v57}, Ljava/util/Map;->size()I

    #@1ee
    move-result v5

    #@1ef
    if-lez v5, :cond_282

    #@1f1
    .line 1555
    new-instance v94, Ljava/util/ArrayList;

    #@1f3
    invoke-direct/range {v94 .. v94}, Ljava/util/ArrayList;-><init>()V

    #@1f6
    .line 1556
    .local v94, timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/BatteryStats$TimerEntry;>;"
    invoke-interface/range {v57 .. v57}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1f9
    move-result-object v5

    #@1fa
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1fd
    move-result-object v54

    #@1fe
    .local v54, i$:Ljava/util/Iterator;
    :cond_1fe
    :goto_1fe
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@201
    move-result v5

    #@202
    if-eqz v5, :cond_22e

    #@204
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@207
    move-result-object v41

    #@208
    check-cast v41, Ljava/util/Map$Entry;

    #@20a
    .line 1557
    .local v41, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    invoke-interface/range {v41 .. v41}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@20d
    move-result-object v8

    #@20e
    check-cast v8, Landroid/os/BatteryStats$Timer;

    #@210
    .line 1558
    .local v8, timer:Landroid/os/BatteryStats$Timer;
    move/from16 v0, p3

    #@212
    invoke-static {v8, v13, v14, v0}, Landroid/os/BatteryStats;->computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J

    #@215
    move-result-wide v9

    #@216
    .line 1559
    .local v9, totalTimeMillis:J
    const-wide/16 v5, 0x0

    #@218
    cmp-long v5, v9, v5

    #@21a
    if-lez v5, :cond_1fe

    #@21c
    .line 1560
    new-instance v5, Landroid/os/BatteryStats$TimerEntry;

    #@21e
    invoke-interface/range {v41 .. v41}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@221
    move-result-object v6

    #@222
    check-cast v6, Ljava/lang/String;

    #@224
    const/4 v7, 0x0

    #@225
    invoke-direct/range {v5 .. v10}, Landroid/os/BatteryStats$TimerEntry;-><init>(Ljava/lang/String;ILandroid/os/BatteryStats$Timer;J)V

    #@228
    move-object/from16 v0, v94

    #@22a
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@22d
    goto :goto_1fe

    #@22e
    .line 1563
    .end local v8           #timer:Landroid/os/BatteryStats$Timer;
    .end local v9           #totalTimeMillis:J
    .end local v41           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    :cond_22e
    move-object/from16 v0, v94

    #@230
    move-object/from16 v1, v93

    #@232
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@235
    .line 1564
    const/16 v53, 0x0

    #@237
    :goto_237
    invoke-virtual/range {v94 .. v94}, Ljava/util/ArrayList;->size()I

    #@23a
    move-result v5

    #@23b
    move/from16 v0, v53

    #@23d
    if-ge v0, v5, :cond_282

    #@23f
    .line 1565
    move-object/from16 v0, v94

    #@241
    move/from16 v1, v53

    #@243
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@246
    move-result-object v8

    #@247
    check-cast v8, Landroid/os/BatteryStats$TimerEntry;

    #@249
    .line 1566
    .local v8, timer:Landroid/os/BatteryStats$TimerEntry;
    const-string v17, ": "

    #@24b
    .line 1567
    .local v17, linePrefix:Ljava/lang/String;
    const/4 v5, 0x0

    #@24c
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@24f
    .line 1568
    move-object/from16 v0, p2

    #@251
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    .line 1569
    const-string v5, "  Kernel Wake lock "

    #@256
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@259
    .line 1570
    iget-object v5, v8, Landroid/os/BatteryStats$TimerEntry;->mName:Ljava/lang/String;

    #@25b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25e
    .line 1571
    iget-object v12, v8, Landroid/os/BatteryStats$TimerEntry;->mTimer:Landroid/os/BatteryStats$Timer;

    #@260
    const/4 v15, 0x0

    #@261
    move/from16 v16, p3

    #@263
    invoke-static/range {v11 .. v17}, Landroid/os/BatteryStats;->printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@266
    move-result-object v17

    #@267
    .line 1573
    const-string v5, ": "

    #@269
    move-object/from16 v0, v17

    #@26b
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26e
    move-result v5

    #@26f
    if-nez v5, :cond_27f

    #@271
    .line 1574
    const-string v5, " realtime"

    #@273
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@276
    .line 1576
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@279
    move-result-object v5

    #@27a
    move-object/from16 v0, p1

    #@27c
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@27f
    .line 1564
    :cond_27f
    add-int/lit8 v53, v53, 0x1

    #@281
    goto :goto_237

    #@282
    .line 1582
    .end local v8           #timer:Landroid/os/BatteryStats$TimerEntry;
    .end local v17           #linePrefix:Ljava/lang/String;
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v57           #kernelWakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Timer;>;"
    .end local v94           #timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/BatteryStats$TimerEntry;>;"
    :cond_282
    new-instance v94, Ljava/util/ArrayList;

    #@284
    invoke-direct/range {v94 .. v94}, Ljava/util/ArrayList;-><init>()V

    #@287
    .line 1584
    .restart local v94       #timers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/BatteryStats$TimerEntry;>;"
    const/16 v56, 0x0

    #@289
    .local v56, iu:I
    :goto_289
    move/from16 v0, v56

    #@28b
    move/from16 v1, v31

    #@28d
    if-ge v0, v1, :cond_31a

    #@28f
    .line 1585
    move-object/from16 v0, v112

    #@291
    move/from16 v1, v56

    #@293
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@296
    move-result-object v109

    #@297
    check-cast v109, Landroid/os/BatteryStats$Uid;

    #@299
    .line 1586
    .local v109, u:Landroid/os/BatteryStats$Uid;
    move-object/from16 v0, v109

    #@29b
    move/from16 v1, p3

    #@29d
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesReceived(I)J

    #@2a0
    move-result-wide v5

    #@2a1
    add-long v72, v72, v5

    #@2a3
    .line 1587
    move-object/from16 v0, v109

    #@2a5
    move/from16 v1, p3

    #@2a7
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesSent(I)J

    #@2aa
    move-result-wide v5

    #@2ab
    add-long v107, v107, v5

    #@2ad
    .line 1589
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getWakelockStats()Ljava/util/Map;

    #@2b0
    move-result-object v118

    #@2b1
    .line 1590
    .local v118, wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v118 .. v118}, Ljava/util/Map;->size()I

    #@2b4
    move-result v5

    #@2b5
    if-lez v5, :cond_316

    #@2b7
    .line 1592
    invoke-interface/range {v118 .. v118}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@2ba
    move-result-object v5

    #@2bb
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2be
    move-result-object v54

    #@2bf
    .restart local v54       #i$:Ljava/util/Iterator;
    :cond_2bf
    :goto_2bf
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@2c2
    move-result v5

    #@2c3
    if-eqz v5, :cond_316

    #@2c5
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c8
    move-result-object v44

    #@2c9
    check-cast v44, Ljava/util/Map$Entry;

    #@2cb
    .line 1593
    .local v44, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v44 .. v44}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@2ce
    move-result-object v130

    #@2cf
    check-cast v130, Landroid/os/BatteryStats$Uid$Wakelock;

    #@2d1
    .line 1595
    .local v130, wl:Landroid/os/BatteryStats$Uid$Wakelock;
    const/4 v5, 0x1

    #@2d2
    move-object/from16 v0, v130

    #@2d4
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@2d7
    move-result-object v48

    #@2d8
    .line 1596
    .local v48, fullWakeTimer:Landroid/os/BatteryStats$Timer;
    if-eqz v48, :cond_2e4

    #@2da
    .line 1597
    move-object/from16 v0, v48

    #@2dc
    move/from16 v1, p3

    #@2de
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@2e1
    move-result-wide v5

    #@2e2
    add-long v46, v46, v5

    #@2e4
    .line 1601
    :cond_2e4
    const/4 v5, 0x0

    #@2e5
    move-object/from16 v0, v130

    #@2e7
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@2ea
    move-result-object v21

    #@2eb
    .line 1602
    .local v21, partialWakeTimer:Landroid/os/BatteryStats$Timer;
    if-eqz v21, :cond_2bf

    #@2ed
    .line 1603
    move-object/from16 v0, v21

    #@2ef
    move/from16 v1, p3

    #@2f1
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@2f4
    move-result-wide v22

    #@2f5
    .line 1605
    .local v22, totalTimeMicros:J
    const-wide/16 v5, 0x0

    #@2f7
    cmp-long v5, v22, v5

    #@2f9
    if-lez v5, :cond_2bf

    #@2fb
    .line 1606
    if-gez p4, :cond_313

    #@2fd
    .line 1610
    new-instance v18, Landroid/os/BatteryStats$TimerEntry;

    #@2ff
    invoke-interface/range {v44 .. v44}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@302
    move-result-object v19

    #@303
    check-cast v19, Ljava/lang/String;

    #@305
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getUid()I

    #@308
    move-result v20

    #@309
    invoke-direct/range {v18 .. v23}, Landroid/os/BatteryStats$TimerEntry;-><init>(Ljava/lang/String;ILandroid/os/BatteryStats$Timer;J)V

    #@30c
    move-object/from16 v0, v94

    #@30e
    move-object/from16 v1, v18

    #@310
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@313
    .line 1613
    :cond_313
    add-long v62, v62, v22

    #@315
    goto :goto_2bf

    #@316
    .line 1584
    .end local v21           #partialWakeTimer:Landroid/os/BatteryStats$Timer;
    .end local v22           #totalTimeMicros:J
    .end local v44           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v48           #fullWakeTimer:Landroid/os/BatteryStats$Timer;
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v130           #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    :cond_316
    add-int/lit8 v56, v56, 0x1

    #@318
    goto/16 :goto_289

    #@31a
    .line 1620
    .end local v109           #u:Landroid/os/BatteryStats$Uid;
    .end local v118           #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    :cond_31a
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@31d
    .line 1621
    const-string v5, "  Total received: "

    #@31f
    move-object/from16 v0, p1

    #@321
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@324
    move-object/from16 v0, p0

    #@326
    move-wide/from16 v1, v72

    #@328
    invoke-direct {v0, v1, v2}, Landroid/os/BatteryStats;->formatBytesLocked(J)Ljava/lang/String;

    #@32b
    move-result-object v5

    #@32c
    move-object/from16 v0, p1

    #@32e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@331
    .line 1622
    const-string v5, ", Total sent: "

    #@333
    move-object/from16 v0, p1

    #@335
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@338
    move-object/from16 v0, p0

    #@33a
    move-wide/from16 v1, v107

    #@33c
    invoke-direct {v0, v1, v2}, Landroid/os/BatteryStats;->formatBytesLocked(J)Ljava/lang/String;

    #@33f
    move-result-object v5

    #@340
    move-object/from16 v0, p1

    #@342
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@345
    .line 1623
    const/4 v5, 0x0

    #@346
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@349
    .line 1624
    move-object/from16 v0, p2

    #@34b
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34e
    .line 1625
    const-string v5, "  Total full wakelock time: "

    #@350
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@353
    const-wide/16 v5, 0x1f4

    #@355
    add-long v5, v5, v46

    #@357
    const-wide/16 v15, 0x3e8

    #@359
    div-long/2addr v5, v15

    #@35a
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@35d
    .line 1627
    const-string v5, ", Total partial wakelock time: "

    #@35f
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@362
    const-wide/16 v5, 0x1f4

    #@364
    add-long v5, v5, v62

    #@366
    const-wide/16 v15, 0x3e8

    #@368
    div-long/2addr v5, v15

    #@369
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@36c
    .line 1629
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36f
    move-result-object v5

    #@370
    move-object/from16 v0, p1

    #@372
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@375
    .line 1631
    const/4 v5, 0x0

    #@376
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@379
    .line 1632
    move-object/from16 v0, p2

    #@37b
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37e
    .line 1633
    const-string v5, "  Signal levels: "

    #@380
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@383
    .line 1634
    const/16 v38, 0x0

    #@385
    .line 1635
    const/16 v53, 0x0

    #@387
    :goto_387
    sget v5, Landroid/telephony/SignalStrength;->NUM_SIGNAL_STRENGTH_BINS:I

    #@389
    move/from16 v0, v53

    #@38b
    if-ge v0, v5, :cond_3e7

    #@38d
    .line 1636
    move-object/from16 v0, p0

    #@38f
    move/from16 v1, v53

    #@391
    move/from16 v2, p3

    #@393
    invoke-virtual {v0, v1, v13, v14, v2}, Landroid/os/BatteryStats;->getPhoneSignalStrengthTime(IJI)J

    #@396
    move-result-wide v91

    #@397
    .line 1637
    .restart local v91       #time:J
    const-wide/16 v5, 0x0

    #@399
    cmp-long v5, v91, v5

    #@39b
    if-nez v5, :cond_3a0

    #@39d
    .line 1635
    :goto_39d
    add-int/lit8 v53, v53, 0x1

    #@39f
    goto :goto_387

    #@3a0
    .line 1640
    :cond_3a0
    if-eqz v38, :cond_3a7

    #@3a2
    const-string v5, ", "

    #@3a4
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a7
    .line 1641
    :cond_3a7
    const/16 v38, 0x1

    #@3a9
    .line 1642
    sget-object v5, Landroid/telephony/SignalStrength;->SIGNAL_STRENGTH_NAMES:[Ljava/lang/String;

    #@3ab
    aget-object v5, v5, v53

    #@3ad
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b0
    .line 1643
    const-string v5, " "

    #@3b2
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b5
    .line 1644
    const-wide/16 v5, 0x3e8

    #@3b7
    div-long v5, v91, v5

    #@3b9
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@3bc
    .line 1645
    const-string v5, "("

    #@3be
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c1
    .line 1646
    move-object/from16 v0, p0

    #@3c3
    move-wide/from16 v1, v91

    #@3c5
    move-wide/from16 v3, v120

    #@3c7
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@3ca
    move-result-object v5

    #@3cb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ce
    .line 1647
    const-string v5, ") "

    #@3d0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d3
    .line 1648
    move-object/from16 v0, p0

    #@3d5
    move/from16 v1, v53

    #@3d7
    move/from16 v2, p3

    #@3d9
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getPhoneSignalStrengthCount(II)I

    #@3dc
    move-result v5

    #@3dd
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e0
    .line 1649
    const-string/jumbo v5, "x"

    #@3e3
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e6
    goto :goto_39d

    #@3e7
    .line 1651
    .end local v91           #time:J
    :cond_3e7
    if-nez v38, :cond_3ee

    #@3e9
    const-string v5, "No activity"

    #@3eb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ee
    .line 1652
    :cond_3ee
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f1
    move-result-object v5

    #@3f2
    move-object/from16 v0, p1

    #@3f4
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3f7
    .line 1654
    const/4 v5, 0x0

    #@3f8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@3fb
    .line 1655
    move-object/from16 v0, p2

    #@3fd
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@400
    .line 1656
    const-string v5, "  Signal scanning time: "

    #@402
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@405
    .line 1657
    move-object/from16 v0, p0

    #@407
    move/from16 v1, p3

    #@409
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats;->getPhoneSignalScanningTime(JI)J

    #@40c
    move-result-wide v5

    #@40d
    const-wide/16 v15, 0x3e8

    #@40f
    div-long/2addr v5, v15

    #@410
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@413
    .line 1658
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@416
    move-result-object v5

    #@417
    move-object/from16 v0, p1

    #@419
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@41c
    .line 1660
    const/4 v5, 0x0

    #@41d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@420
    .line 1661
    move-object/from16 v0, p2

    #@422
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@425
    .line 1662
    const-string v5, "  Radio types: "

    #@427
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42a
    .line 1663
    const/16 v38, 0x0

    #@42c
    .line 1664
    const/16 v53, 0x0

    #@42e
    :goto_42e
    const/16 v5, 0x10

    #@430
    move/from16 v0, v53

    #@432
    if-ge v0, v5, :cond_48e

    #@434
    .line 1665
    move-object/from16 v0, p0

    #@436
    move/from16 v1, v53

    #@438
    move/from16 v2, p3

    #@43a
    invoke-virtual {v0, v1, v13, v14, v2}, Landroid/os/BatteryStats;->getPhoneDataConnectionTime(IJI)J

    #@43d
    move-result-wide v91

    #@43e
    .line 1666
    .restart local v91       #time:J
    const-wide/16 v5, 0x0

    #@440
    cmp-long v5, v91, v5

    #@442
    if-nez v5, :cond_447

    #@444
    .line 1664
    :goto_444
    add-int/lit8 v53, v53, 0x1

    #@446
    goto :goto_42e

    #@447
    .line 1669
    :cond_447
    if-eqz v38, :cond_44e

    #@449
    const-string v5, ", "

    #@44b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44e
    .line 1670
    :cond_44e
    const/16 v38, 0x1

    #@450
    .line 1671
    sget-object v5, Landroid/os/BatteryStats;->DATA_CONNECTION_NAMES:[Ljava/lang/String;

    #@452
    aget-object v5, v5, v53

    #@454
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@457
    .line 1672
    const-string v5, " "

    #@459
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45c
    .line 1673
    const-wide/16 v5, 0x3e8

    #@45e
    div-long v5, v91, v5

    #@460
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@463
    .line 1674
    const-string v5, "("

    #@465
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@468
    .line 1675
    move-object/from16 v0, p0

    #@46a
    move-wide/from16 v1, v91

    #@46c
    move-wide/from16 v3, v120

    #@46e
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@471
    move-result-object v5

    #@472
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@475
    .line 1676
    const-string v5, ") "

    #@477
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47a
    .line 1677
    move-object/from16 v0, p0

    #@47c
    move/from16 v1, v53

    #@47e
    move/from16 v2, p3

    #@480
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats;->getPhoneDataConnectionCount(II)I

    #@483
    move-result v5

    #@484
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@487
    .line 1678
    const-string/jumbo v5, "x"

    #@48a
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48d
    goto :goto_444

    #@48e
    .line 1680
    .end local v91           #time:J
    :cond_48e
    if-nez v38, :cond_495

    #@490
    const-string v5, "No activity"

    #@492
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@495
    .line 1681
    :cond_495
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@498
    move-result-object v5

    #@499
    move-object/from16 v0, p1

    #@49b
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@49e
    .line 1683
    const/4 v5, 0x0

    #@49f
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@4a2
    .line 1684
    move-object/from16 v0, p2

    #@4a4
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a7
    .line 1685
    const-string v5, "  Radio data uptime when unplugged: "

    #@4a9
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ac
    .line 1686
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getRadioDataUptime()J

    #@4af
    move-result-wide v5

    #@4b0
    const-wide/16 v15, 0x3e8

    #@4b2
    div-long/2addr v5, v15

    #@4b3
    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4b6
    .line 1687
    const-string v5, " ms"

    #@4b8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bb
    .line 1688
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4be
    move-result-object v5

    #@4bf
    move-object/from16 v0, p1

    #@4c1
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@4c4
    .line 1690
    const/4 v5, 0x0

    #@4c5
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@4c8
    .line 1691
    move-object/from16 v0, p2

    #@4ca
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cd
    .line 1692
    const-string v5, "  Wifi on: "

    #@4cf
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d2
    const-wide/16 v5, 0x3e8

    #@4d4
    div-long v5, v124, v5

    #@4d6
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@4d9
    .line 1693
    const-string v5, "("

    #@4db
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4de
    move-object/from16 v0, p0

    #@4e0
    move-wide/from16 v1, v124

    #@4e2
    move-wide/from16 v3, v120

    #@4e4
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@4e7
    move-result-object v5

    #@4e8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4eb
    .line 1694
    const-string v5, "), Wifi running: "

    #@4ed
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f0
    const-wide/16 v5, 0x3e8

    #@4f2
    div-long v5, v126, v5

    #@4f4
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@4f7
    .line 1695
    const-string v5, "("

    #@4f9
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fc
    move-object/from16 v0, p0

    #@4fe
    move-wide/from16 v1, v126

    #@500
    move-wide/from16 v3, v120

    #@502
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@505
    move-result-object v5

    #@506
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@509
    .line 1696
    const-string v5, "), Bluetooth on: "

    #@50b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50e
    const-wide/16 v5, 0x3e8

    #@510
    div-long v5, v35, v5

    #@512
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@515
    .line 1697
    const-string v5, "("

    #@517
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51a
    move-object/from16 v0, p0

    #@51c
    move-wide/from16 v1, v35

    #@51e
    move-wide/from16 v3, v120

    #@520
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@523
    move-result-object v5

    #@524
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@527
    .line 1698
    const-string v5, ")"

    #@529
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52c
    .line 1699
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52f
    move-result-object v5

    #@530
    move-object/from16 v0, p1

    #@532
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@535
    .line 1701
    const-string v5, " "

    #@537
    move-object/from16 v0, p1

    #@539
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@53c
    .line 1703
    const/4 v5, 0x3

    #@53d
    move/from16 v0, p3

    #@53f
    if-ne v0, v5, :cond_63b

    #@541
    .line 1704
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getIsOnBattery()Z

    #@544
    move-result v5

    #@545
    if-eqz v5, :cond_609

    #@547
    .line 1705
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@54a
    const-string v5, "  Device is currently unplugged"

    #@54c
    move-object/from16 v0, p1

    #@54e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@551
    .line 1706
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@554
    const-string v5, "    Discharge cycle start level: "

    #@556
    move-object/from16 v0, p1

    #@558
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@55b
    .line 1707
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeStartLevel()I

    #@55e
    move-result v5

    #@55f
    move-object/from16 v0, p1

    #@561
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@564
    .line 1708
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@567
    const-string v5, "    Discharge cycle current level: "

    #@569
    move-object/from16 v0, p1

    #@56b
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56e
    .line 1709
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeCurrentLevel()I

    #@571
    move-result v5

    #@572
    move-object/from16 v0, p1

    #@574
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@577
    .line 1717
    :goto_577
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@57a
    const-string v5, "    Amount discharged while screen on: "

    #@57c
    move-object/from16 v0, p1

    #@57e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@581
    .line 1718
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOn()I

    #@584
    move-result v5

    #@585
    move-object/from16 v0, p1

    #@587
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@58a
    .line 1719
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@58d
    const-string v5, "    Amount discharged while screen off: "

    #@58f
    move-object/from16 v0, p1

    #@591
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@594
    .line 1720
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOff()I

    #@597
    move-result v5

    #@598
    move-object/from16 v0, p1

    #@59a
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@59d
    .line 1721
    const-string v5, " "

    #@59f
    move-object/from16 v0, p1

    #@5a1
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5a4
    .line 1735
    :goto_5a4
    invoke-virtual/range {v94 .. v94}, Ljava/util/ArrayList;->size()I

    #@5a7
    move-result v5

    #@5a8
    if-lez v5, :cond_69c

    #@5aa
    .line 1736
    move-object/from16 v0, v94

    #@5ac
    move-object/from16 v1, v93

    #@5ae
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@5b1
    .line 1737
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5b4
    const-string v5, "  All partial wake locks:"

    #@5b6
    move-object/from16 v0, p1

    #@5b8
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@5bb
    .line 1738
    const/16 v53, 0x0

    #@5bd
    :goto_5bd
    invoke-virtual/range {v94 .. v94}, Ljava/util/ArrayList;->size()I

    #@5c0
    move-result v5

    #@5c1
    move/from16 v0, v53

    #@5c3
    if-ge v0, v5, :cond_696

    #@5c5
    .line 1739
    move-object/from16 v0, v94

    #@5c7
    move/from16 v1, v53

    #@5c9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5cc
    move-result-object v8

    #@5cd
    check-cast v8, Landroid/os/BatteryStats$TimerEntry;

    #@5cf
    .line 1740
    .restart local v8       #timer:Landroid/os/BatteryStats$TimerEntry;
    const/4 v5, 0x0

    #@5d0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@5d3
    .line 1741
    const-string v5, "  Wake lock #"

    #@5d5
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d8
    .line 1742
    iget v5, v8, Landroid/os/BatteryStats$TimerEntry;->mId:I

    #@5da
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5dd
    .line 1743
    const-string v5, " "

    #@5df
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e2
    .line 1744
    iget-object v5, v8, Landroid/os/BatteryStats$TimerEntry;->mName:Ljava/lang/String;

    #@5e4
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e7
    .line 1745
    iget-object v0, v8, Landroid/os/BatteryStats$TimerEntry;->mTimer:Landroid/os/BatteryStats$Timer;

    #@5e9
    move-object/from16 v25, v0

    #@5eb
    const/16 v28, 0x0

    #@5ed
    const-string v30, ": "

    #@5ef
    move-object/from16 v24, v11

    #@5f1
    move-wide/from16 v26, v13

    #@5f3
    move/from16 v29, p3

    #@5f5
    invoke-static/range {v24 .. v30}, Landroid/os/BatteryStats;->printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@5f8
    .line 1746
    const-string v5, " realtime"

    #@5fa
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5fd
    .line 1747
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@600
    move-result-object v5

    #@601
    move-object/from16 v0, p1

    #@603
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@606
    .line 1738
    add-int/lit8 v53, v53, 0x1

    #@608
    goto :goto_5bd

    #@609
    .line 1711
    .end local v8           #timer:Landroid/os/BatteryStats$TimerEntry;
    :cond_609
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@60c
    const-string v5, "  Device is currently plugged into power"

    #@60e
    move-object/from16 v0, p1

    #@610
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@613
    .line 1712
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@616
    const-string v5, "    Last discharge cycle start level: "

    #@618
    move-object/from16 v0, p1

    #@61a
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@61d
    .line 1713
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeStartLevel()I

    #@620
    move-result v5

    #@621
    move-object/from16 v0, p1

    #@623
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@626
    .line 1714
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@629
    const-string v5, "    Last discharge cycle end level: "

    #@62b
    move-object/from16 v0, p1

    #@62d
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@630
    .line 1715
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeCurrentLevel()I

    #@633
    move-result v5

    #@634
    move-object/from16 v0, p1

    #@636
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@639
    goto/16 :goto_577

    #@63b
    .line 1723
    :cond_63b
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63e
    const-string v5, "  Device battery use since last full charge"

    #@640
    move-object/from16 v0, p1

    #@642
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@645
    .line 1724
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@648
    const-string v5, "    Amount discharged (lower bound): "

    #@64a
    move-object/from16 v0, p1

    #@64c
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64f
    .line 1725
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getLowDischargeAmountSinceCharge()I

    #@652
    move-result v5

    #@653
    move-object/from16 v0, p1

    #@655
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@658
    .line 1726
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@65b
    const-string v5, "    Amount discharged (upper bound): "

    #@65d
    move-object/from16 v0, p1

    #@65f
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@662
    .line 1727
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getHighDischargeAmountSinceCharge()I

    #@665
    move-result v5

    #@666
    move-object/from16 v0, p1

    #@668
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@66b
    .line 1728
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@66e
    const-string v5, "    Amount discharged while screen on: "

    #@670
    move-object/from16 v0, p1

    #@672
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@675
    .line 1729
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOnSinceCharge()I

    #@678
    move-result v5

    #@679
    move-object/from16 v0, p1

    #@67b
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@67e
    .line 1730
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@681
    const-string v5, "    Amount discharged while screen off: "

    #@683
    move-object/from16 v0, p1

    #@685
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@688
    .line 1731
    invoke-virtual/range {p0 .. p0}, Landroid/os/BatteryStats;->getDischargeAmountScreenOffSinceCharge()I

    #@68b
    move-result v5

    #@68c
    move-object/from16 v0, p1

    #@68e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(I)V

    #@691
    .line 1732
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@694
    goto/16 :goto_5a4

    #@696
    .line 1749
    :cond_696
    invoke-virtual/range {v94 .. v94}, Ljava/util/ArrayList;->clear()V

    #@699
    .line 1750
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    #@69c
    .line 1753
    :cond_69c
    const/16 v56, 0x0

    #@69e
    :goto_69e
    move/from16 v0, v56

    #@6a0
    move/from16 v1, v31

    #@6a2
    if-ge v0, v1, :cond_c78

    #@6a4
    .line 1754
    move-object/from16 v0, v112

    #@6a6
    move/from16 v1, v56

    #@6a8
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    #@6ab
    move-result v110

    #@6ac
    .line 1755
    .local v110, uid:I
    if-ltz p4, :cond_6bd

    #@6ae
    move/from16 v0, v110

    #@6b0
    move/from16 v1, p4

    #@6b2
    if-eq v0, v1, :cond_6bd

    #@6b4
    const/16 v5, 0x3e8

    #@6b6
    move/from16 v0, v110

    #@6b8
    if-eq v0, v5, :cond_6bd

    #@6ba
    .line 1753
    :cond_6ba
    :goto_6ba
    add-int/lit8 v56, v56, 0x1

    #@6bc
    goto :goto_69e

    #@6bd
    .line 1759
    :cond_6bd
    move-object/from16 v0, v112

    #@6bf
    move/from16 v1, v56

    #@6c1
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    #@6c4
    move-result-object v109

    #@6c5
    check-cast v109, Landroid/os/BatteryStats$Uid;

    #@6c7
    .line 1761
    .restart local v109       #u:Landroid/os/BatteryStats$Uid;
    new-instance v5, Ljava/lang/StringBuilder;

    #@6c9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6cc
    move-object/from16 v0, p2

    #@6ce
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d1
    move-result-object v5

    #@6d2
    const-string v6, "  #"

    #@6d4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d7
    move-result-object v5

    #@6d8
    move/from16 v0, v110

    #@6da
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6dd
    move-result-object v5

    #@6de
    const-string v6, ":"

    #@6e0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e3
    move-result-object v5

    #@6e4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e7
    move-result-object v5

    #@6e8
    move-object/from16 v0, p1

    #@6ea
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@6ed
    .line 1762
    const/16 v111, 0x0

    #@6ef
    .line 1764
    .local v111, uidActivity:Z
    move-object/from16 v0, v109

    #@6f1
    move/from16 v1, p3

    #@6f3
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesReceived(I)J

    #@6f6
    move-result-wide v87

    #@6f7
    .line 1765
    .local v87, tcpReceived:J
    move-object/from16 v0, v109

    #@6f9
    move/from16 v1, p3

    #@6fb
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid;->getTcpBytesSent(I)J

    #@6fe
    move-result-wide v89

    #@6ff
    .line 1766
    .local v89, tcpSent:J
    move-object/from16 v0, v109

    #@701
    move/from16 v1, p3

    #@703
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Uid;->getFullWifiLockTime(JI)J

    #@706
    move-result-wide v49

    #@707
    .line 1767
    .local v49, fullWifiLockOnTime:J
    move-object/from16 v0, v109

    #@709
    move/from16 v1, p3

    #@70b
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Uid;->getWifiScanTime(JI)J

    #@70e
    move-result-wide v128

    #@70f
    .line 1768
    .local v128, wifiScanTime:J
    move-object/from16 v0, v109

    #@711
    move/from16 v1, p3

    #@713
    invoke-virtual {v0, v13, v14, v1}, Landroid/os/BatteryStats$Uid;->getWifiRunningTime(JI)J

    #@716
    move-result-wide v113

    #@717
    .line 1770
    .local v113, uidWifiRunningTime:J
    const-wide/16 v5, 0x0

    #@719
    cmp-long v5, v87, v5

    #@71b
    if-nez v5, :cond_723

    #@71d
    const-wide/16 v5, 0x0

    #@71f
    cmp-long v5, v89, v5

    #@721
    if-eqz v5, :cond_755

    #@723
    .line 1771
    :cond_723
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@726
    const-string v5, "    Network: "

    #@728
    move-object/from16 v0, p1

    #@72a
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72d
    .line 1772
    move-object/from16 v0, p0

    #@72f
    move-wide/from16 v1, v87

    #@731
    invoke-direct {v0, v1, v2}, Landroid/os/BatteryStats;->formatBytesLocked(J)Ljava/lang/String;

    #@734
    move-result-object v5

    #@735
    move-object/from16 v0, p1

    #@737
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@73a
    const-string v5, " received, "

    #@73c
    move-object/from16 v0, p1

    #@73e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@741
    .line 1773
    move-object/from16 v0, p0

    #@743
    move-wide/from16 v1, v89

    #@745
    invoke-direct {v0, v1, v2}, Landroid/os/BatteryStats;->formatBytesLocked(J)Ljava/lang/String;

    #@748
    move-result-object v5

    #@749
    move-object/from16 v0, p1

    #@74b
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@74e
    const-string v5, " sent"

    #@750
    move-object/from16 v0, p1

    #@752
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@755
    .line 1776
    :cond_755
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->hasUserActivity()Z

    #@758
    move-result v5

    #@759
    if-eqz v5, :cond_7a2

    #@75b
    .line 1777
    const/16 v52, 0x0

    #@75d
    .line 1778
    .local v52, hasData:Z
    const/16 v53, 0x0

    #@75f
    :goto_75f
    const/4 v5, 0x3

    #@760
    move/from16 v0, v53

    #@762
    if-ge v0, v5, :cond_797

    #@764
    .line 1779
    move-object/from16 v0, v109

    #@766
    move/from16 v1, v53

    #@768
    move/from16 v2, p3

    #@76a
    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryStats$Uid;->getUserActivityCount(II)I

    #@76d
    move-result v117

    #@76e
    .line 1780
    .local v117, val:I
    if-eqz v117, :cond_78e

    #@770
    .line 1781
    if-nez v52, :cond_791

    #@772
    .line 1782
    const/4 v5, 0x0

    #@773
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@776
    .line 1783
    const-string v5, "    User activity: "

    #@778
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77b
    .line 1784
    const/16 v52, 0x1

    #@77d
    .line 1788
    :goto_77d
    move/from16 v0, v117

    #@77f
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@782
    .line 1789
    const-string v5, " "

    #@784
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@787
    .line 1790
    sget-object v5, Landroid/os/BatteryStats$Uid;->USER_ACTIVITY_TYPES:[Ljava/lang/String;

    #@789
    aget-object v5, v5, v53

    #@78b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78e
    .line 1778
    :cond_78e
    add-int/lit8 v53, v53, 0x1

    #@790
    goto :goto_75f

    #@791
    .line 1786
    :cond_791
    const-string v5, ", "

    #@793
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@796
    goto :goto_77d

    #@797
    .line 1793
    .end local v117           #val:I
    :cond_797
    if-eqz v52, :cond_7a2

    #@799
    .line 1794
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79c
    move-result-object v5

    #@79d
    move-object/from16 v0, p1

    #@79f
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7a2
    .line 1798
    .end local v52           #hasData:Z
    :cond_7a2
    const-wide/16 v5, 0x0

    #@7a4
    cmp-long v5, v49, v5

    #@7a6
    if-nez v5, :cond_7b4

    #@7a8
    const-wide/16 v5, 0x0

    #@7aa
    cmp-long v5, v128, v5

    #@7ac
    if-nez v5, :cond_7b4

    #@7ae
    const-wide/16 v5, 0x0

    #@7b0
    cmp-long v5, v113, v5

    #@7b2
    if-eqz v5, :cond_839

    #@7b4
    .line 1800
    :cond_7b4
    const/4 v5, 0x0

    #@7b5
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@7b8
    .line 1801
    move-object/from16 v0, p2

    #@7ba
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7bd
    const-string v5, "    Wifi Running: "

    #@7bf
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c2
    .line 1802
    const-wide/16 v5, 0x3e8

    #@7c4
    div-long v5, v113, v5

    #@7c6
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@7c9
    .line 1803
    const-string v5, "("

    #@7cb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7ce
    move-object/from16 v0, p0

    #@7d0
    move-wide/from16 v1, v113

    #@7d2
    move-wide/from16 v3, v120

    #@7d4
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@7d7
    move-result-object v5

    #@7d8
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7db
    .line 1804
    const-string v5, ")\n"

    #@7dd
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e0
    .line 1805
    move-object/from16 v0, p2

    #@7e2
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e5
    const-string v5, "    Full Wifi Lock: "

    #@7e7
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7ea
    .line 1806
    const-wide/16 v5, 0x3e8

    #@7ec
    div-long v5, v49, v5

    #@7ee
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@7f1
    .line 1807
    const-string v5, "("

    #@7f3
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f6
    move-object/from16 v0, p0

    #@7f8
    move-wide/from16 v1, v49

    #@7fa
    move-wide/from16 v3, v120

    #@7fc
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@7ff
    move-result-object v5

    #@800
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@803
    .line 1808
    const-string v5, ")\n"

    #@805
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@808
    .line 1809
    move-object/from16 v0, p2

    #@80a
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80d
    const-string v5, "    Wifi Scan: "

    #@80f
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@812
    .line 1810
    const-wide/16 v5, 0x3e8

    #@814
    div-long v5, v128, v5

    #@816
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@819
    .line 1811
    const-string v5, "("

    #@81b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81e
    move-object/from16 v0, p0

    #@820
    move-wide/from16 v1, v128

    #@822
    move-wide/from16 v3, v120

    #@824
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/os/BatteryStats;->formatRatioLocked(JJ)Ljava/lang/String;

    #@827
    move-result-object v5

    #@828
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82b
    .line 1812
    const-string v5, ")"

    #@82d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@830
    .line 1813
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@833
    move-result-object v5

    #@834
    move-object/from16 v0, p1

    #@836
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@839
    .line 1816
    :cond_839
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getWakelockStats()Ljava/util/Map;

    #@83c
    move-result-object v118

    #@83d
    .line 1817
    .restart local v118       #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v118 .. v118}, Ljava/util/Map;->size()I

    #@840
    move-result v5

    #@841
    if-lez v5, :cond_973

    #@843
    .line 1818
    const-wide/16 v95, 0x0

    #@845
    .local v95, totalFull:J
    const-wide/16 v97, 0x0

    #@847
    .local v97, totalPartial:J
    const-wide/16 v105, 0x0

    #@849
    .line 1819
    .local v105, totalWindow:J
    const/16 v37, 0x0

    #@84b
    .line 1821
    .local v37, count:I
    invoke-interface/range {v118 .. v118}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@84e
    move-result-object v5

    #@84f
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@852
    move-result-object v54

    #@853
    .restart local v54       #i$:Ljava/util/Iterator;
    :goto_853
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@856
    move-result v5

    #@857
    if-eqz v5, :cond_8f8

    #@859
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@85c
    move-result-object v44

    #@85d
    check-cast v44, Ljava/util/Map$Entry;

    #@85f
    .line 1822
    .restart local v44       #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    invoke-interface/range {v44 .. v44}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@862
    move-result-object v130

    #@863
    check-cast v130, Landroid/os/BatteryStats$Uid$Wakelock;

    #@865
    .line 1823
    .restart local v130       #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    const-string v17, ": "

    #@867
    .line 1824
    .restart local v17       #linePrefix:Ljava/lang/String;
    const/4 v5, 0x0

    #@868
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@86b
    .line 1825
    move-object/from16 v0, p2

    #@86d
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@870
    .line 1826
    const-string v5, "    Wake lock "

    #@872
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@875
    .line 1827
    invoke-interface/range {v44 .. v44}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@878
    move-result-object v5

    #@879
    check-cast v5, Ljava/lang/String;

    #@87b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87e
    .line 1828
    const/4 v5, 0x1

    #@87f
    move-object/from16 v0, v130

    #@881
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@884
    move-result-object v12

    #@885
    const-string v15, "full"

    #@887
    move/from16 v16, p3

    #@889
    invoke-static/range {v11 .. v17}, Landroid/os/BatteryStats;->printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@88c
    move-result-object v17

    #@88d
    .line 1830
    const/4 v5, 0x0

    #@88e
    move-object/from16 v0, v130

    #@890
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@893
    move-result-object v12

    #@894
    const-string/jumbo v15, "partial"

    #@897
    move/from16 v16, p3

    #@899
    invoke-static/range {v11 .. v17}, Landroid/os/BatteryStats;->printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@89c
    move-result-object v17

    #@89d
    .line 1832
    const/4 v5, 0x2

    #@89e
    move-object/from16 v0, v130

    #@8a0
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@8a3
    move-result-object v12

    #@8a4
    const-string/jumbo v15, "window"

    #@8a7
    move/from16 v16, p3

    #@8a9
    invoke-static/range {v11 .. v17}, Landroid/os/BatteryStats;->printWakeLock(Ljava/lang/StringBuilder;Landroid/os/BatteryStats$Timer;JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    #@8ac
    move-result-object v17

    #@8ad
    .line 1834
    const-string v5, ": "

    #@8af
    move-object/from16 v0, v17

    #@8b1
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8b4
    move-result v5

    #@8b5
    if-nez v5, :cond_8c9

    #@8b7
    .line 1835
    const-string v5, " realtime"

    #@8b9
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8bc
    .line 1837
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8bf
    move-result-object v5

    #@8c0
    move-object/from16 v0, p1

    #@8c2
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@8c5
    .line 1838
    const/16 v111, 0x1

    #@8c7
    .line 1839
    add-int/lit8 v37, v37, 0x1

    #@8c9
    .line 1841
    :cond_8c9
    const/4 v5, 0x1

    #@8ca
    move-object/from16 v0, v130

    #@8cc
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@8cf
    move-result-object v5

    #@8d0
    move/from16 v0, p3

    #@8d2
    invoke-static {v5, v13, v14, v0}, Landroid/os/BatteryStats;->computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J

    #@8d5
    move-result-wide v5

    #@8d6
    add-long v95, v95, v5

    #@8d8
    .line 1843
    const/4 v5, 0x0

    #@8d9
    move-object/from16 v0, v130

    #@8db
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@8de
    move-result-object v5

    #@8df
    move/from16 v0, p3

    #@8e1
    invoke-static {v5, v13, v14, v0}, Landroid/os/BatteryStats;->computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J

    #@8e4
    move-result-wide v5

    #@8e5
    add-long v97, v97, v5

    #@8e7
    .line 1845
    const/4 v5, 0x2

    #@8e8
    move-object/from16 v0, v130

    #@8ea
    invoke-virtual {v0, v5}, Landroid/os/BatteryStats$Uid$Wakelock;->getWakeTime(I)Landroid/os/BatteryStats$Timer;

    #@8ed
    move-result-object v5

    #@8ee
    move/from16 v0, p3

    #@8f0
    invoke-static {v5, v13, v14, v0}, Landroid/os/BatteryStats;->computeWakeLock(Landroid/os/BatteryStats$Timer;JI)J

    #@8f3
    move-result-wide v5

    #@8f4
    add-long v105, v105, v5

    #@8f6
    .line 1847
    goto/16 :goto_853

    #@8f8
    .line 1848
    .end local v17           #linePrefix:Ljava/lang/String;
    .end local v44           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v130           #wl:Landroid/os/BatteryStats$Uid$Wakelock;
    :cond_8f8
    const/4 v5, 0x1

    #@8f9
    move/from16 v0, v37

    #@8fb
    if-le v0, v5, :cond_973

    #@8fd
    .line 1849
    const-wide/16 v5, 0x0

    #@8ff
    cmp-long v5, v95, v5

    #@901
    if-nez v5, :cond_90f

    #@903
    const-wide/16 v5, 0x0

    #@905
    cmp-long v5, v97, v5

    #@907
    if-nez v5, :cond_90f

    #@909
    const-wide/16 v5, 0x0

    #@90b
    cmp-long v5, v105, v5

    #@90d
    if-eqz v5, :cond_973

    #@90f
    .line 1850
    :cond_90f
    const/4 v5, 0x0

    #@910
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@913
    .line 1851
    move-object/from16 v0, p2

    #@915
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@918
    .line 1852
    const-string v5, "    TOTAL wake: "

    #@91a
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91d
    .line 1853
    const/16 v59, 0x0

    #@91f
    .line 1854
    .local v59, needComma:Z
    const-wide/16 v5, 0x0

    #@921
    cmp-long v5, v95, v5

    #@923
    if-eqz v5, :cond_931

    #@925
    .line 1855
    const/16 v59, 0x1

    #@927
    .line 1856
    move-wide/from16 v0, v95

    #@929
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@92c
    .line 1857
    const-string v5, "full"

    #@92e
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@931
    .line 1859
    :cond_931
    const-wide/16 v5, 0x0

    #@933
    cmp-long v5, v97, v5

    #@935
    if-eqz v5, :cond_94b

    #@937
    .line 1860
    if-eqz v59, :cond_93e

    #@939
    .line 1861
    const-string v5, ", "

    #@93b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93e
    .line 1863
    :cond_93e
    const/16 v59, 0x1

    #@940
    .line 1864
    move-wide/from16 v0, v97

    #@942
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@945
    .line 1865
    const-string/jumbo v5, "partial"

    #@948
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94b
    .line 1867
    :cond_94b
    const-wide/16 v5, 0x0

    #@94d
    cmp-long v5, v105, v5

    #@94f
    if-eqz v5, :cond_965

    #@951
    .line 1868
    if-eqz v59, :cond_958

    #@953
    .line 1869
    const-string v5, ", "

    #@955
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@958
    .line 1871
    :cond_958
    const/16 v59, 0x1

    #@95a
    .line 1872
    move-wide/from16 v0, v105

    #@95c
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@95f
    .line 1873
    const-string/jumbo v5, "window"

    #@962
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@965
    .line 1875
    :cond_965
    const-string v5, " realtime"

    #@967
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96a
    .line 1876
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96d
    move-result-object v5

    #@96e
    move-object/from16 v0, p1

    #@970
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@973
    .line 1881
    .end local v37           #count:I
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v59           #needComma:Z
    .end local v95           #totalFull:J
    .end local v97           #totalPartial:J
    .end local v105           #totalWindow:J
    :cond_973
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getSensorStats()Ljava/util/Map;

    #@976
    move-result-object v78

    #@977
    .line 1882
    .local v78, sensors:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    invoke-interface/range {v78 .. v78}, Ljava/util/Map;->size()I

    #@97a
    move-result v5

    #@97b
    if-lez v5, :cond_a15

    #@97d
    .line 1884
    invoke-interface/range {v78 .. v78}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@980
    move-result-object v5

    #@981
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@984
    move-result-object v54

    #@985
    .restart local v54       #i$:Ljava/util/Iterator;
    :goto_985
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@988
    move-result v5

    #@989
    if-eqz v5, :cond_a15

    #@98b
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@98e
    move-result-object v40

    #@98f
    check-cast v40, Ljava/util/Map$Entry;

    #@991
    .line 1885
    .local v40, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    invoke-interface/range {v40 .. v40}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@994
    move-result-object v76

    #@995
    check-cast v76, Landroid/os/BatteryStats$Uid$Sensor;

    #@997
    .line 1886
    .local v76, se:Landroid/os/BatteryStats$Uid$Sensor;
    invoke-interface/range {v40 .. v40}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@99a
    move-result-object v5

    #@99b
    check-cast v5, Ljava/lang/Integer;

    #@99d
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@9a0
    move-result v77

    #@9a1
    .line 1887
    .local v77, sensorNumber:I
    const/4 v5, 0x0

    #@9a2
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@9a5
    .line 1888
    move-object/from16 v0, p2

    #@9a7
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9aa
    .line 1889
    const-string v5, "    Sensor "

    #@9ac
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9af
    .line 1890
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid$Sensor;->getHandle()I

    #@9b2
    move-result v51

    #@9b3
    .line 1891
    .local v51, handle:I
    const/16 v5, -0x2710

    #@9b5
    move/from16 v0, v51

    #@9b7
    if-ne v0, v5, :cond_a03

    #@9b9
    .line 1892
    const-string v5, "GPS"

    #@9bb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9be
    .line 1896
    :goto_9be
    const-string v5, ": "

    #@9c0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c3
    .line 1898
    invoke-virtual/range {v76 .. v76}, Landroid/os/BatteryStats$Uid$Sensor;->getSensorTime()Landroid/os/BatteryStats$Timer;

    #@9c6
    move-result-object v8

    #@9c7
    .line 1899
    .local v8, timer:Landroid/os/BatteryStats$Timer;
    if-eqz v8, :cond_a0f

    #@9c9
    .line 1901
    move/from16 v0, p3

    #@9cb
    invoke-virtual {v8, v13, v14, v0}, Landroid/os/BatteryStats$Timer;->getTotalTimeLocked(JI)J

    #@9ce
    move-result-wide v5

    #@9cf
    const-wide/16 v15, 0x1f4

    #@9d1
    add-long/2addr v5, v15

    #@9d2
    const-wide/16 v15, 0x3e8

    #@9d4
    div-long v101, v5, v15

    #@9d6
    .line 1903
    .local v101, totalTime:J
    move/from16 v0, p3

    #@9d8
    invoke-virtual {v8, v0}, Landroid/os/BatteryStats$Timer;->getCountLocked(I)I

    #@9db
    move-result v37

    #@9dc
    .line 1905
    .restart local v37       #count:I
    const-wide/16 v5, 0x0

    #@9de
    cmp-long v5, v101, v5

    #@9e0
    if-eqz v5, :cond_a09

    #@9e2
    .line 1906
    move-wide/from16 v0, v101

    #@9e4
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@9e7
    .line 1907
    const-string/jumbo v5, "realtime ("

    #@9ea
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9ed
    .line 1908
    move/from16 v0, v37

    #@9ef
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9f2
    .line 1909
    const-string v5, " times)"

    #@9f4
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f7
    .line 1917
    .end local v37           #count:I
    .end local v101           #totalTime:J
    :goto_9f7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9fa
    move-result-object v5

    #@9fb
    move-object/from16 v0, p1

    #@9fd
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@a00
    .line 1918
    const/16 v111, 0x1

    #@a02
    .line 1919
    goto :goto_985

    #@a03
    .line 1894
    .end local v8           #timer:Landroid/os/BatteryStats$Timer;
    :cond_a03
    move/from16 v0, v51

    #@a05
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a08
    goto :goto_9be

    #@a09
    .line 1911
    .restart local v8       #timer:Landroid/os/BatteryStats$Timer;
    .restart local v37       #count:I
    .restart local v101       #totalTime:J
    :cond_a09
    const-string v5, "(not used)"

    #@a0b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0e
    goto :goto_9f7

    #@a0f
    .line 1914
    .end local v37           #count:I
    .end local v101           #totalTime:J
    :cond_a0f
    const-string v5, "(not used)"

    #@a11
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a14
    goto :goto_9f7

    #@a15
    .line 1922
    .end local v8           #timer:Landroid/os/BatteryStats$Timer;
    .end local v40           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    .end local v51           #handle:I
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v76           #se:Landroid/os/BatteryStats$Uid$Sensor;
    .end local v77           #sensorNumber:I
    :cond_a15
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getProcessStats()Ljava/util/Map;

    #@a18
    move-result-object v66

    #@a19
    .line 1923
    .local v66, processStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    invoke-interface/range {v66 .. v66}, Ljava/util/Map;->size()I

    #@a1c
    move-result v5

    #@a1d
    if-lez v5, :cond_b54

    #@a1f
    .line 1925
    invoke-interface/range {v66 .. v66}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@a22
    move-result-object v5

    #@a23
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a26
    move-result-object v54

    #@a27
    .restart local v54       #i$:Ljava/util/Iterator;
    :cond_a27
    :goto_a27
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@a2a
    move-result v5

    #@a2b
    if-eqz v5, :cond_b54

    #@a2d
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a30
    move-result-object v43

    #@a31
    check-cast v43, Ljava/util/Map$Entry;

    #@a33
    .line 1926
    .local v43, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    invoke-interface/range {v43 .. v43}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@a36
    move-result-object v67

    #@a37
    check-cast v67, Landroid/os/BatteryStats$Uid$Proc;

    #@a39
    .line 1932
    .local v67, ps:Landroid/os/BatteryStats$Uid$Proc;
    move-object/from16 v0, v67

    #@a3b
    move/from16 v1, p3

    #@a3d
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getUserTime(I)J

    #@a40
    move-result-wide v115

    #@a41
    .line 1933
    .local v115, userTime:J
    move-object/from16 v0, v67

    #@a43
    move/from16 v1, p3

    #@a45
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getSystemTime(I)J

    #@a48
    move-result-wide v85

    #@a49
    .line 1934
    .local v85, systemTime:J
    move-object/from16 v0, v67

    #@a4b
    move/from16 v1, p3

    #@a4d
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getStarts(I)I

    #@a50
    move-result v84

    #@a51
    .line 1935
    .local v84, starts:I
    if-nez p3, :cond_b34

    #@a53
    invoke-virtual/range {v67 .. v67}, Landroid/os/BatteryStats$Uid$Proc;->countExcessivePowers()I

    #@a56
    move-result v60

    #@a57
    .line 1938
    .local v60, numExcessive:I
    :goto_a57
    const-wide/16 v5, 0x0

    #@a59
    cmp-long v5, v115, v5

    #@a5b
    if-nez v5, :cond_a67

    #@a5d
    const-wide/16 v5, 0x0

    #@a5f
    cmp-long v5, v85, v5

    #@a61
    if-nez v5, :cond_a67

    #@a63
    if-nez v84, :cond_a67

    #@a65
    if-eqz v60, :cond_a27

    #@a67
    .line 1940
    :cond_a67
    const/4 v5, 0x0

    #@a68
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@a6b
    .line 1941
    move-object/from16 v0, p2

    #@a6d
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a70
    const-string v5, "    Proc "

    #@a72
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a75
    .line 1942
    invoke-interface/range {v43 .. v43}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@a78
    move-result-object v5

    #@a79
    check-cast v5, Ljava/lang/String;

    #@a7b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7e
    const-string v5, ":\n"

    #@a80
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a83
    .line 1943
    move-object/from16 v0, p2

    #@a85
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a88
    const-string v5, "      CPU: "

    #@a8a
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8d
    .line 1944
    move-wide/from16 v0, v115

    #@a8f
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTime(Ljava/lang/StringBuilder;J)V

    #@a92
    const-string/jumbo v5, "usr + "

    #@a95
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a98
    .line 1945
    move-wide/from16 v0, v85

    #@a9a
    invoke-static {v11, v0, v1}, Landroid/os/BatteryStats;->formatTime(Ljava/lang/StringBuilder;J)V

    #@a9d
    const-string/jumbo v5, "krn"

    #@aa0
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa3
    .line 1946
    if-eqz v84, :cond_abe

    #@aa5
    .line 1947
    const-string v5, "\n"

    #@aa7
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aaa
    move-object/from16 v0, p2

    #@aac
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aaf
    const-string v5, "      "

    #@ab1
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab4
    .line 1948
    move/from16 v0, v84

    #@ab6
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab9
    const-string v5, " proc starts"

    #@abb
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@abe
    .line 1950
    :cond_abe
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac1
    move-result-object v5

    #@ac2
    move-object/from16 v0, p1

    #@ac4
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@ac7
    .line 1951
    const/16 v39, 0x0

    #@ac9
    .local v39, e:I
    :goto_ac9
    move/from16 v0, v39

    #@acb
    move/from16 v1, v60

    #@acd
    if-ge v0, v1, :cond_b50

    #@acf
    .line 1952
    move-object/from16 v0, v67

    #@ad1
    move/from16 v1, v39

    #@ad3
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Proc;->getExcessivePower(I)Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;

    #@ad6
    move-result-object v45

    #@ad7
    .line 1953
    .local v45, ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    if-eqz v45, :cond_b31

    #@ad9
    .line 1954
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@adc
    const-string v5, "      * Killed for "

    #@ade
    move-object/from16 v0, p1

    #@ae0
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ae3
    .line 1955
    move-object/from16 v0, v45

    #@ae5
    iget v5, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@ae7
    const/4 v6, 0x1

    #@ae8
    if-ne v5, v6, :cond_b38

    #@aea
    .line 1956
    const-string/jumbo v5, "wake lock"

    #@aed
    move-object/from16 v0, p1

    #@aef
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af2
    .line 1962
    :goto_af2
    const-string v5, " use: "

    #@af4
    move-object/from16 v0, p1

    #@af6
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af9
    .line 1963
    move-object/from16 v0, v45

    #@afb
    iget-wide v5, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@afd
    move-object/from16 v0, p1

    #@aff
    invoke-static {v5, v6, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@b02
    .line 1964
    const-string v5, " over "

    #@b04
    move-object/from16 v0, p1

    #@b06
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b09
    .line 1965
    move-object/from16 v0, v45

    #@b0b
    iget-wide v5, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@b0d
    move-object/from16 v0, p1

    #@b0f
    invoke-static {v5, v6, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    #@b12
    .line 1966
    const-string v5, " ("

    #@b14
    move-object/from16 v0, p1

    #@b16
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b19
    .line 1967
    move-object/from16 v0, v45

    #@b1b
    iget-wide v5, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->usedTime:J

    #@b1d
    const-wide/16 v15, 0x64

    #@b1f
    mul-long/2addr v5, v15

    #@b20
    move-object/from16 v0, v45

    #@b22
    iget-wide v15, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->overTime:J

    #@b24
    div-long/2addr v5, v15

    #@b25
    move-object/from16 v0, p1

    #@b27
    invoke-virtual {v0, v5, v6}, Ljava/io/PrintWriter;->print(J)V

    #@b2a
    .line 1968
    const-string v5, "%)"

    #@b2c
    move-object/from16 v0, p1

    #@b2e
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b31
    .line 1951
    :cond_b31
    add-int/lit8 v39, v39, 0x1

    #@b33
    goto :goto_ac9

    #@b34
    .line 1935
    .end local v39           #e:I
    .end local v45           #ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    .end local v60           #numExcessive:I
    :cond_b34
    const/16 v60, 0x0

    #@b36
    goto/16 :goto_a57

    #@b38
    .line 1957
    .restart local v39       #e:I
    .restart local v45       #ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    .restart local v60       #numExcessive:I
    :cond_b38
    move-object/from16 v0, v45

    #@b3a
    iget v5, v0, Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;->type:I

    #@b3c
    const/4 v6, 0x2

    #@b3d
    if-ne v5, v6, :cond_b47

    #@b3f
    .line 1958
    const-string v5, "cpu"

    #@b41
    move-object/from16 v0, p1

    #@b43
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b46
    goto :goto_af2

    #@b47
    .line 1960
    :cond_b47
    const-string/jumbo v5, "unknown"

    #@b4a
    move-object/from16 v0, p1

    #@b4c
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b4f
    goto :goto_af2

    #@b50
    .line 1971
    .end local v45           #ew:Landroid/os/BatteryStats$Uid$Proc$ExcessivePower;
    :cond_b50
    const/16 v111, 0x1

    #@b52
    goto/16 :goto_a27

    #@b54
    .line 1976
    .end local v39           #e:I
    .end local v43           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    .end local v54           #i$:Ljava/util/Iterator;
    .end local v60           #numExcessive:I
    .end local v67           #ps:Landroid/os/BatteryStats$Uid$Proc;
    .end local v84           #starts:I
    .end local v85           #systemTime:J
    .end local v115           #userTime:J
    :cond_b54
    invoke-virtual/range {v109 .. v109}, Landroid/os/BatteryStats$Uid;->getPackageStats()Ljava/util/Map;

    #@b57
    move-result-object v61

    #@b58
    .line 1977
    .local v61, packageStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    invoke-interface/range {v61 .. v61}, Ljava/util/Map;->size()I

    #@b5b
    move-result v5

    #@b5c
    if-lez v5, :cond_c6a

    #@b5e
    .line 1979
    invoke-interface/range {v61 .. v61}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@b61
    move-result-object v5

    #@b62
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@b65
    move-result-object v54

    #@b66
    :goto_b66
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->hasNext()Z

    #@b69
    move-result v5

    #@b6a
    if-eqz v5, :cond_c6a

    #@b6c
    invoke-interface/range {v54 .. v54}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@b6f
    move-result-object v42

    #@b70
    check-cast v42, Ljava/util/Map$Entry;

    #@b72
    .line 1980
    .local v42, ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b75
    const-string v5, "    Apk "

    #@b77
    move-object/from16 v0, p1

    #@b79
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b7c
    invoke-interface/range {v42 .. v42}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@b7f
    move-result-object v5

    #@b80
    check-cast v5, Ljava/lang/String;

    #@b82
    move-object/from16 v0, p1

    #@b84
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b87
    const-string v5, ":"

    #@b89
    move-object/from16 v0, p1

    #@b8b
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@b8e
    .line 1981
    const/16 v32, 0x0

    #@b90
    .line 1982
    .local v32, apkActivity:Z
    invoke-interface/range {v42 .. v42}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@b93
    move-result-object v67

    #@b94
    check-cast v67, Landroid/os/BatteryStats$Uid$Pkg;

    #@b96
    .line 1983
    .local v67, ps:Landroid/os/BatteryStats$Uid$Pkg;
    move-object/from16 v0, v67

    #@b98
    move/from16 v1, p3

    #@b9a
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg;->getWakeups(I)I

    #@b9d
    move-result v119

    #@b9e
    .line 1984
    .local v119, wakeups:I
    if-eqz v119, :cond_bba

    #@ba0
    .line 1985
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ba3
    const-string v5, "      "

    #@ba5
    move-object/from16 v0, p1

    #@ba7
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@baa
    .line 1986
    move-object/from16 v0, p1

    #@bac
    move/from16 v1, v119

    #@bae
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    #@bb1
    const-string v5, " wakeup alarms"

    #@bb3
    move-object/from16 v0, p1

    #@bb5
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@bb8
    .line 1987
    const/16 v32, 0x1

    #@bba
    .line 1989
    :cond_bba
    invoke-virtual/range {v67 .. v67}, Landroid/os/BatteryStats$Uid$Pkg;->getServiceStats()Ljava/util/Map;

    #@bbd
    move-result-object v80

    #@bbe
    .line 1990
    .local v80, serviceStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    invoke-interface/range {v80 .. v80}, Ljava/util/Map;->size()I

    #@bc1
    move-result v5

    #@bc2
    if-lez v5, :cond_c5a

    #@bc4
    .line 1992
    invoke-interface/range {v80 .. v80}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@bc7
    move-result-object v5

    #@bc8
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@bcb
    move-result-object v55

    #@bcc
    .local v55, i$:Ljava/util/Iterator;
    :cond_bcc
    :goto_bcc
    invoke-interface/range {v55 .. v55}, Ljava/util/Iterator;->hasNext()Z

    #@bcf
    move-result v5

    #@bd0
    if-eqz v5, :cond_c5a

    #@bd2
    invoke-interface/range {v55 .. v55}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@bd5
    move-result-object v79

    #@bd6
    check-cast v79, Ljava/util/Map$Entry;

    #@bd8
    .line 1993
    .local v79, sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    invoke-interface/range {v79 .. v79}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@bdb
    move-result-object v81

    #@bdc
    check-cast v81, Landroid/os/BatteryStats$Uid$Pkg$Serv;

    #@bde
    .line 1994
    .local v81, ss:Landroid/os/BatteryStats$Uid$Pkg$Serv;
    move-object/from16 v0, v81

    #@be0
    move-wide/from16 v1, v33

    #@be2
    move/from16 v3, p3

    #@be4
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getStartTime(JI)J

    #@be7
    move-result-wide v82

    #@be8
    .line 1995
    .local v82, startTime:J
    move-object/from16 v0, v81

    #@bea
    move/from16 v1, p3

    #@bec
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getStarts(I)I

    #@bef
    move-result v84

    #@bf0
    .line 1996
    .restart local v84       #starts:I
    move-object/from16 v0, v81

    #@bf2
    move/from16 v1, p3

    #@bf4
    invoke-virtual {v0, v1}, Landroid/os/BatteryStats$Uid$Pkg$Serv;->getLaunches(I)I

    #@bf7
    move-result v58

    #@bf8
    .line 1997
    .local v58, launches:I
    const-wide/16 v5, 0x0

    #@bfa
    cmp-long v5, v82, v5

    #@bfc
    if-nez v5, :cond_c02

    #@bfe
    if-nez v84, :cond_c02

    #@c00
    if-eqz v58, :cond_bcc

    #@c02
    .line 1998
    :cond_c02
    const/4 v5, 0x0

    #@c03
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    #@c06
    .line 1999
    move-object/from16 v0, p2

    #@c08
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0b
    const-string v5, "      Service "

    #@c0d
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c10
    .line 2000
    invoke-interface/range {v79 .. v79}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@c13
    move-result-object v5

    #@c14
    check-cast v5, Ljava/lang/String;

    #@c16
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c19
    const-string v5, ":\n"

    #@c1b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1e
    .line 2001
    move-object/from16 v0, p2

    #@c20
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c23
    const-string v5, "        Created for: "

    #@c25
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c28
    .line 2002
    const-wide/16 v5, 0x3e8

    #@c2a
    div-long v5, v82, v5

    #@c2c
    invoke-static {v11, v5, v6}, Landroid/os/BatteryStats;->formatTimeMs(Ljava/lang/StringBuilder;J)V

    #@c2f
    .line 2003
    const-string v5, " uptime\n"

    #@c31
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c34
    .line 2004
    move-object/from16 v0, p2

    #@c36
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c39
    const-string v5, "        Starts: "

    #@c3b
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3e
    .line 2005
    move/from16 v0, v84

    #@c40
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c43
    .line 2006
    const-string v5, ", launches: "

    #@c45
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c48
    move/from16 v0, v58

    #@c4a
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c4d
    .line 2007
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c50
    move-result-object v5

    #@c51
    move-object/from16 v0, p1

    #@c53
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c56
    .line 2008
    const/16 v32, 0x1

    #@c58
    goto/16 :goto_bcc

    #@c5a
    .line 2012
    .end local v55           #i$:Ljava/util/Iterator;
    .end local v58           #launches:I
    .end local v79           #sent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    .end local v81           #ss:Landroid/os/BatteryStats$Uid$Pkg$Serv;
    .end local v82           #startTime:J
    .end local v84           #starts:I
    :cond_c5a
    if-nez v32, :cond_c66

    #@c5c
    .line 2013
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c5f
    const-string v5, "      (nothing executed)"

    #@c61
    move-object/from16 v0, p1

    #@c63
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c66
    .line 2015
    :cond_c66
    const/16 v111, 0x1

    #@c68
    .line 2016
    goto/16 :goto_b66

    #@c6a
    .line 2018
    .end local v32           #apkActivity:Z
    .end local v42           #ent:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    .end local v67           #ps:Landroid/os/BatteryStats$Uid$Pkg;
    .end local v80           #serviceStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg$Serv;>;"
    .end local v119           #wakeups:I
    :cond_c6a
    if-nez v111, :cond_6ba

    #@c6c
    .line 2019
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c6f
    const-string v5, "    (nothing executed)"

    #@c71
    move-object/from16 v0, p1

    #@c73
    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@c76
    goto/16 :goto_6ba

    #@c78
    .line 2022
    .end local v49           #fullWifiLockOnTime:J
    .end local v61           #packageStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Pkg;>;"
    .end local v66           #processStats:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Proc;>;"
    .end local v78           #sensors:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Integer;+Landroid/os/BatteryStats$Uid$Sensor;>;"
    .end local v87           #tcpReceived:J
    .end local v89           #tcpSent:J
    .end local v109           #u:Landroid/os/BatteryStats$Uid;
    .end local v110           #uid:I
    .end local v111           #uidActivity:Z
    .end local v113           #uidWifiRunningTime:J
    .end local v118           #wakelocks:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;+Landroid/os/BatteryStats$Uid$Wakelock;>;"
    .end local v128           #wifiScanTime:J
    :cond_c78
    return-void
.end method

.method public abstract finishIteratingHistoryLocked()V
.end method

.method public abstract finishIteratingOldHistoryLocked()V
.end method

.method public abstract getBatteryRealtime(J)J
.end method

.method public abstract getBatteryUptime(J)J
.end method

.method public abstract getBluetoothOnTime(JI)J
.end method

.method public abstract getCpuSpeedSteps()I
.end method

.method public abstract getDischargeAmountScreenOff()I
.end method

.method public abstract getDischargeAmountScreenOffSinceCharge()I
.end method

.method public abstract getDischargeAmountScreenOn()I
.end method

.method public abstract getDischargeAmountScreenOnSinceCharge()I
.end method

.method public abstract getDischargeCurrentLevel()I
.end method

.method public abstract getDischargeStartLevel()I
.end method

.method public abstract getGlobalWifiRunningTime(JI)J
.end method

.method public abstract getHighDischargeAmountSinceCharge()I
.end method

.method public abstract getHistoryBaseTime()J
.end method

.method public abstract getInputEventCount(I)I
.end method

.method public abstract getIsOnBattery()Z
.end method

.method public abstract getKernelWakelockStats()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Landroid/os/BatteryStats$Timer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLowDischargeAmountSinceCharge()I
.end method

.method public abstract getNextHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z
.end method

.method public abstract getNextOldHistoryLocked(Landroid/os/BatteryStats$HistoryItem;)Z
.end method

.method public abstract getPhoneDataConnectionCount(II)I
.end method

.method public abstract getPhoneDataConnectionTime(IJI)J
.end method

.method public abstract getPhoneOnTime(JI)J
.end method

.method public abstract getPhoneSignalScanningTime(JI)J
.end method

.method public abstract getPhoneSignalStrengthCount(II)I
.end method

.method public abstract getPhoneSignalStrengthTime(IJI)J
.end method

.method public abstract getRadioDataUptime()J
.end method

.method public getRadioDataUptimeMs()J
    .registers 5

    #@0
    .prologue
    .line 932
    invoke-virtual {p0}, Landroid/os/BatteryStats;->getRadioDataUptime()J

    #@3
    move-result-wide v0

    #@4
    const-wide/16 v2, 0x3e8

    #@6
    div-long/2addr v0, v2

    #@7
    return-wide v0
.end method

.method public abstract getScreenBrightnessTime(IJI)J
.end method

.method public abstract getScreenOnTime(JI)J
.end method

.method public abstract getStartCount()I
.end method

.method public abstract getUidStats()Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<+",
            "Landroid/os/BatteryStats$Uid;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getWifiOnTime(JI)J
.end method

.method public prepareForDumpLocked()V
    .registers 1

    #@0
    .prologue
    .line 2049
    return-void
.end method

.method public abstract startIteratingHistoryLocked()Z
.end method

.method public abstract startIteratingOldHistoryLocked()Z
.end method
