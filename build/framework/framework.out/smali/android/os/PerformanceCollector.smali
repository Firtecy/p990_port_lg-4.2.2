.class public Landroid/os/PerformanceCollector;
.super Ljava/lang/Object;
.source "PerformanceCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/PerformanceCollector$PerformanceResultsWriter;
    }
.end annotation


# static fields
.field public static final METRIC_KEY_CPU_TIME:Ljava/lang/String; = "cpu_time"

.field public static final METRIC_KEY_EXECUTION_TIME:Ljava/lang/String; = "execution_time"

.field public static final METRIC_KEY_GC_INVOCATION_COUNT:Ljava/lang/String; = "gc_invocation_count"

.field public static final METRIC_KEY_GLOBAL_ALLOC_COUNT:Ljava/lang/String; = "global_alloc_count"

.field public static final METRIC_KEY_GLOBAL_ALLOC_SIZE:Ljava/lang/String; = "global_alloc_size"

.field public static final METRIC_KEY_GLOBAL_FREED_COUNT:Ljava/lang/String; = "global_freed_count"

.field public static final METRIC_KEY_GLOBAL_FREED_SIZE:Ljava/lang/String; = "global_freed_size"

.field public static final METRIC_KEY_ITERATIONS:Ljava/lang/String; = "iterations"

.field public static final METRIC_KEY_JAVA_ALLOCATED:Ljava/lang/String; = "java_allocated"

.field public static final METRIC_KEY_JAVA_FREE:Ljava/lang/String; = "java_free"

.field public static final METRIC_KEY_JAVA_PRIVATE_DIRTY:Ljava/lang/String; = "java_private_dirty"

.field public static final METRIC_KEY_JAVA_PSS:Ljava/lang/String; = "java_pss"

.field public static final METRIC_KEY_JAVA_SHARED_DIRTY:Ljava/lang/String; = "java_shared_dirty"

.field public static final METRIC_KEY_JAVA_SIZE:Ljava/lang/String; = "java_size"

.field public static final METRIC_KEY_LABEL:Ljava/lang/String; = "label"

.field public static final METRIC_KEY_NATIVE_ALLOCATED:Ljava/lang/String; = "native_allocated"

.field public static final METRIC_KEY_NATIVE_FREE:Ljava/lang/String; = "native_free"

.field public static final METRIC_KEY_NATIVE_PRIVATE_DIRTY:Ljava/lang/String; = "native_private_dirty"

.field public static final METRIC_KEY_NATIVE_PSS:Ljava/lang/String; = "native_pss"

.field public static final METRIC_KEY_NATIVE_SHARED_DIRTY:Ljava/lang/String; = "native_shared_dirty"

.field public static final METRIC_KEY_NATIVE_SIZE:Ljava/lang/String; = "native_size"

.field public static final METRIC_KEY_OTHER_PRIVATE_DIRTY:Ljava/lang/String; = "other_private_dirty"

.field public static final METRIC_KEY_OTHER_PSS:Ljava/lang/String; = "other_pss"

.field public static final METRIC_KEY_OTHER_SHARED_DIRTY:Ljava/lang/String; = "other_shared_dirty"

.field public static final METRIC_KEY_PRE_RECEIVED_TRANSACTIONS:Ljava/lang/String; = "pre_received_transactions"

.field public static final METRIC_KEY_PRE_SENT_TRANSACTIONS:Ljava/lang/String; = "pre_sent_transactions"

.field public static final METRIC_KEY_RECEIVED_TRANSACTIONS:Ljava/lang/String; = "received_transactions"

.field public static final METRIC_KEY_SENT_TRANSACTIONS:Ljava/lang/String; = "sent_transactions"


# instance fields
.field private mCpuTime:J

.field private mExecTime:J

.field private mPerfMeasurement:Landroid/os/Bundle;

.field private mPerfSnapshot:Landroid/os/Bundle;

.field private mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

.field private mSnapshotCpuTime:J

.field private mSnapshotExecTime:J


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 288
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 289
    return-void
.end method

.method public constructor <init>(Landroid/os/PerformanceCollector$PerformanceResultsWriter;)V
    .registers 2
    .parameter "writer"

    #@0
    .prologue
    .line 291
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 292
    invoke-virtual {p0, p1}, Landroid/os/PerformanceCollector;->setPerformanceResultsWriter(Landroid/os/PerformanceCollector$PerformanceResultsWriter;)V

    #@6
    .line 293
    return-void
.end method

.method private endPerformanceSnapshot()V
    .registers 27

    #@0
    .prologue
    .line 483
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    #@3
    move-result-wide v22

    #@4
    move-object/from16 v0, p0

    #@6
    iget-wide v0, v0, Landroid/os/PerformanceCollector;->mSnapshotCpuTime:J

    #@8
    move-wide/from16 v24, v0

    #@a
    sub-long v22, v22, v24

    #@c
    move-wide/from16 v0, v22

    #@e
    move-object/from16 v2, p0

    #@10
    iput-wide v0, v2, Landroid/os/PerformanceCollector;->mSnapshotCpuTime:J

    #@12
    .line 484
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@15
    move-result-wide v22

    #@16
    move-object/from16 v0, p0

    #@18
    iget-wide v0, v0, Landroid/os/PerformanceCollector;->mSnapshotExecTime:J

    #@1a
    move-wide/from16 v24, v0

    #@1c
    sub-long v22, v22, v24

    #@1e
    move-wide/from16 v0, v22

    #@20
    move-object/from16 v2, p0

    #@22
    iput-wide v0, v2, Landroid/os/PerformanceCollector;->mSnapshotExecTime:J

    #@24
    .line 486
    invoke-static {}, Landroid/os/PerformanceCollector;->stopAllocCounting()V

    #@27
    .line 488
    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    #@2a
    move-result-wide v22

    #@2b
    const-wide/16 v24, 0x400

    #@2d
    div-long v19, v22, v24

    #@2f
    .line 489
    .local v19, nativeMax:J
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    #@32
    move-result-wide v22

    #@33
    const-wide/16 v24, 0x400

    #@35
    div-long v15, v22, v24

    #@37
    .line 490
    .local v15, nativeAllocated:J
    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    #@3a
    move-result-wide v22

    #@3b
    const-wide/16 v24, 0x400

    #@3d
    div-long v17, v22, v24

    #@3f
    .line 492
    .local v17, nativeFree:J
    new-instance v14, Landroid/os/Debug$MemoryInfo;

    #@41
    invoke-direct {v14}, Landroid/os/Debug$MemoryInfo;-><init>()V

    #@44
    .line 493
    .local v14, memInfo:Landroid/os/Debug$MemoryInfo;
    invoke-static {v14}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    #@47
    .line 495
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@4a
    move-result-object v21

    #@4b
    .line 497
    .local v21, runtime:Ljava/lang/Runtime;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Runtime;->totalMemory()J

    #@4e
    move-result-wide v22

    #@4f
    const-wide/16 v24, 0x400

    #@51
    div-long v10, v22, v24

    #@53
    .line 498
    .local v10, dalvikMax:J
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Runtime;->freeMemory()J

    #@56
    move-result-wide v22

    #@57
    const-wide/16 v24, 0x400

    #@59
    div-long v8, v22, v24

    #@5b
    .line 499
    .local v8, dalvikFree:J
    sub-long v6, v10, v8

    #@5d
    .line 502
    .local v6, dalvikAllocated:J
    invoke-static {}, Landroid/os/PerformanceCollector;->getBinderCounts()Landroid/os/Bundle;

    #@60
    move-result-object v5

    #@61
    .line 503
    .local v5, binderCounts:Landroid/os/Bundle;
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@64
    move-result-object v22

    #@65
    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@68
    move-result-object v12

    #@69
    .local v12, i$:Ljava/util/Iterator;
    :goto_69
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@6c
    move-result v22

    #@6d
    if-eqz v22, :cond_87

    #@6f
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@72
    move-result-object v13

    #@73
    check-cast v13, Ljava/lang/String;

    #@75
    .line 504
    .local v13, key:Ljava/lang/String;
    move-object/from16 v0, p0

    #@77
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@79
    move-object/from16 v22, v0

    #@7b
    invoke-virtual {v5, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@7e
    move-result-wide v23

    #@7f
    move-object/from16 v0, v22

    #@81
    move-wide/from16 v1, v23

    #@83
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@86
    goto :goto_69

    #@87
    .line 508
    .end local v13           #key:Ljava/lang/String;
    :cond_87
    invoke-static {}, Landroid/os/PerformanceCollector;->getAllocCounts()Landroid/os/Bundle;

    #@8a
    move-result-object v4

    #@8b
    .line 509
    .local v4, allocCounts:Landroid/os/Bundle;
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@8e
    move-result-object v22

    #@8f
    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@92
    move-result-object v12

    #@93
    :goto_93
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@96
    move-result v22

    #@97
    if-eqz v22, :cond_b1

    #@99
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@9c
    move-result-object v13

    #@9d
    check-cast v13, Ljava/lang/String;

    #@9f
    .line 510
    .restart local v13       #key:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a1
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@a3
    move-object/from16 v22, v0

    #@a5
    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@a8
    move-result-wide v23

    #@a9
    move-object/from16 v0, v22

    #@ab
    move-wide/from16 v1, v23

    #@ad
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@b0
    goto :goto_93

    #@b1
    .line 513
    .end local v13           #key:Ljava/lang/String;
    :cond_b1
    move-object/from16 v0, p0

    #@b3
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@b5
    move-object/from16 v22, v0

    #@b7
    const-string v23, "execution_time"

    #@b9
    move-object/from16 v0, p0

    #@bb
    iget-wide v0, v0, Landroid/os/PerformanceCollector;->mSnapshotExecTime:J

    #@bd
    move-wide/from16 v24, v0

    #@bf
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@c2
    .line 514
    move-object/from16 v0, p0

    #@c4
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@c6
    move-object/from16 v22, v0

    #@c8
    const-string v23, "cpu_time"

    #@ca
    move-object/from16 v0, p0

    #@cc
    iget-wide v0, v0, Landroid/os/PerformanceCollector;->mSnapshotCpuTime:J

    #@ce
    move-wide/from16 v24, v0

    #@d0
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@d3
    .line 516
    move-object/from16 v0, p0

    #@d5
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@d7
    move-object/from16 v22, v0

    #@d9
    const-string/jumbo v23, "native_size"

    #@dc
    move-object/from16 v0, v22

    #@de
    move-object/from16 v1, v23

    #@e0
    move-wide/from16 v2, v19

    #@e2
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@e5
    .line 517
    move-object/from16 v0, p0

    #@e7
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@e9
    move-object/from16 v22, v0

    #@eb
    const-string/jumbo v23, "native_allocated"

    #@ee
    move-object/from16 v0, v22

    #@f0
    move-object/from16 v1, v23

    #@f2
    move-wide v2, v15

    #@f3
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@f6
    .line 518
    move-object/from16 v0, p0

    #@f8
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@fa
    move-object/from16 v22, v0

    #@fc
    const-string/jumbo v23, "native_free"

    #@ff
    move-object/from16 v0, v22

    #@101
    move-object/from16 v1, v23

    #@103
    move-wide/from16 v2, v17

    #@105
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@108
    .line 519
    move-object/from16 v0, p0

    #@10a
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@10c
    move-object/from16 v22, v0

    #@10e
    const-string/jumbo v23, "native_pss"

    #@111
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->nativePss:I

    #@113
    move/from16 v24, v0

    #@115
    move/from16 v0, v24

    #@117
    int-to-long v0, v0

    #@118
    move-wide/from16 v24, v0

    #@11a
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@11d
    .line 520
    move-object/from16 v0, p0

    #@11f
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@121
    move-object/from16 v22, v0

    #@123
    const-string/jumbo v23, "native_private_dirty"

    #@126
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    #@128
    move/from16 v24, v0

    #@12a
    move/from16 v0, v24

    #@12c
    int-to-long v0, v0

    #@12d
    move-wide/from16 v24, v0

    #@12f
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@132
    .line 521
    move-object/from16 v0, p0

    #@134
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@136
    move-object/from16 v22, v0

    #@138
    const-string/jumbo v23, "native_shared_dirty"

    #@13b
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->nativeSharedDirty:I

    #@13d
    move/from16 v24, v0

    #@13f
    move/from16 v0, v24

    #@141
    int-to-long v0, v0

    #@142
    move-wide/from16 v24, v0

    #@144
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@147
    .line 523
    move-object/from16 v0, p0

    #@149
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@14b
    move-object/from16 v22, v0

    #@14d
    const-string/jumbo v23, "java_size"

    #@150
    move-object/from16 v0, v22

    #@152
    move-object/from16 v1, v23

    #@154
    invoke-virtual {v0, v1, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@157
    .line 524
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@15b
    move-object/from16 v22, v0

    #@15d
    const-string/jumbo v23, "java_allocated"

    #@160
    move-object/from16 v0, v22

    #@162
    move-object/from16 v1, v23

    #@164
    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@167
    .line 525
    move-object/from16 v0, p0

    #@169
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@16b
    move-object/from16 v22, v0

    #@16d
    const-string/jumbo v23, "java_free"

    #@170
    move-object/from16 v0, v22

    #@172
    move-object/from16 v1, v23

    #@174
    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@177
    .line 526
    move-object/from16 v0, p0

    #@179
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@17b
    move-object/from16 v22, v0

    #@17d
    const-string/jumbo v23, "java_pss"

    #@180
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    #@182
    move/from16 v24, v0

    #@184
    move/from16 v0, v24

    #@186
    int-to-long v0, v0

    #@187
    move-wide/from16 v24, v0

    #@189
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@18c
    .line 527
    move-object/from16 v0, p0

    #@18e
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@190
    move-object/from16 v22, v0

    #@192
    const-string/jumbo v23, "java_private_dirty"

    #@195
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    #@197
    move/from16 v24, v0

    #@199
    move/from16 v0, v24

    #@19b
    int-to-long v0, v0

    #@19c
    move-wide/from16 v24, v0

    #@19e
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1a1
    .line 528
    move-object/from16 v0, p0

    #@1a3
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@1a5
    move-object/from16 v22, v0

    #@1a7
    const-string/jumbo v23, "java_shared_dirty"

    #@1aa
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->dalvikSharedDirty:I

    #@1ac
    move/from16 v24, v0

    #@1ae
    move/from16 v0, v24

    #@1b0
    int-to-long v0, v0

    #@1b1
    move-wide/from16 v24, v0

    #@1b3
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1b6
    .line 530
    move-object/from16 v0, p0

    #@1b8
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@1ba
    move-object/from16 v22, v0

    #@1bc
    const-string/jumbo v23, "other_pss"

    #@1bf
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->otherPss:I

    #@1c1
    move/from16 v24, v0

    #@1c3
    move/from16 v0, v24

    #@1c5
    int-to-long v0, v0

    #@1c6
    move-wide/from16 v24, v0

    #@1c8
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1cb
    .line 531
    move-object/from16 v0, p0

    #@1cd
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@1cf
    move-object/from16 v22, v0

    #@1d1
    const-string/jumbo v23, "other_private_dirty"

    #@1d4
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    #@1d6
    move/from16 v24, v0

    #@1d8
    move/from16 v0, v24

    #@1da
    int-to-long v0, v0

    #@1db
    move-wide/from16 v24, v0

    #@1dd
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1e0
    .line 532
    move-object/from16 v0, p0

    #@1e2
    iget-object v0, v0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@1e4
    move-object/from16 v22, v0

    #@1e6
    const-string/jumbo v23, "other_shared_dirty"

    #@1e9
    iget v0, v14, Landroid/os/Debug$MemoryInfo;->otherSharedDirty:I

    #@1eb
    move/from16 v24, v0

    #@1ed
    move/from16 v0, v24

    #@1ef
    int-to-long v0, v0

    #@1f0
    move-wide/from16 v24, v0

    #@1f2
    invoke-virtual/range {v22 .. v25}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1f5
    .line 533
    return-void
.end method

.method private static getAllocCounts()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 567
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 568
    .local v0, results:Landroid/os/Bundle;
    const-string v1, "global_alloc_count"

    #@7
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocCount()I

    #@a
    move-result v2

    #@b
    int-to-long v2, v2

    #@c
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@f
    .line 569
    const-string v1, "global_alloc_size"

    #@11
    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    #@14
    move-result v2

    #@15
    int-to-long v2, v2

    #@16
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@19
    .line 570
    const-string v1, "global_freed_count"

    #@1b
    invoke-static {}, Landroid/os/Debug;->getGlobalFreedCount()I

    #@1e
    move-result v2

    #@1f
    int-to-long v2, v2

    #@20
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@23
    .line 571
    const-string v1, "global_freed_size"

    #@25
    invoke-static {}, Landroid/os/Debug;->getGlobalFreedSize()I

    #@28
    move-result v2

    #@29
    int-to-long v2, v2

    #@2a
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@2d
    .line 572
    const-string v1, "gc_invocation_count"

    #@2f
    invoke-static {}, Landroid/os/Debug;->getGlobalGcInvocationCount()I

    #@32
    move-result v2

    #@33
    int-to-long v2, v2

    #@34
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@37
    .line 573
    return-object v0
.end method

.method private static getBinderCounts()Landroid/os/Bundle;
    .registers 4

    #@0
    .prologue
    .line 582
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    .line 583
    .local v0, results:Landroid/os/Bundle;
    const-string/jumbo v1, "sent_transactions"

    #@8
    invoke-static {}, Landroid/os/Debug;->getBinderSentTransactions()I

    #@b
    move-result v2

    #@c
    int-to-long v2, v2

    #@d
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@10
    .line 584
    const-string/jumbo v1, "received_transactions"

    #@13
    invoke-static {}, Landroid/os/Debug;->getBinderReceivedTransactions()I

    #@16
    move-result v2

    #@17
    int-to-long v2, v2

    #@18
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1b
    .line 585
    return-object v0
.end method

.method private static startAllocCounting()V
    .registers 1

    #@0
    .prologue
    .line 543
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@7
    .line 544
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    #@e
    .line 545
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@15
    .line 547
    invoke-static {}, Landroid/os/Debug;->resetAllCounts()V

    #@18
    .line 550
    invoke-static {}, Landroid/os/Debug;->startAllocCounting()V

    #@1b
    .line 551
    return-void
.end method

.method private startPerformanceSnapshot()V
    .registers 8

    #@0
    .prologue
    .line 456
    new-instance v3, Landroid/os/Bundle;

    #@2
    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    #@5
    iput-object v3, p0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@7
    .line 459
    invoke-static {}, Landroid/os/PerformanceCollector;->getBinderCounts()Landroid/os/Bundle;

    #@a
    move-result-object v0

    #@b
    .line 460
    .local v0, binderCounts:Landroid/os/Bundle;
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@e
    move-result-object v3

    #@f
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@12
    move-result-object v1

    #@13
    .local v1, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_3d

    #@19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Ljava/lang/String;

    #@1f
    .line 461
    .local v2, key:Ljava/lang/String;
    iget-object v3, p0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string/jumbo v5, "pre_"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    #@38
    move-result-wide v5

    #@39
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@3c
    goto :goto_13

    #@3d
    .line 467
    .end local v2           #key:Ljava/lang/String;
    :cond_3d
    invoke-static {}, Landroid/os/PerformanceCollector;->startAllocCounting()V

    #@40
    .line 472
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@43
    move-result-wide v3

    #@44
    iput-wide v3, p0, Landroid/os/PerformanceCollector;->mSnapshotExecTime:J

    #@46
    .line 473
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    #@49
    move-result-wide v3

    #@4a
    iput-wide v3, p0, Landroid/os/PerformanceCollector;->mSnapshotCpuTime:J

    #@4c
    .line 474
    return-void
.end method

.method private static stopAllocCounting()V
    .registers 1

    #@0
    .prologue
    .line 557
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@7
    .line 558
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/lang/Runtime;->runFinalization()V

    #@e
    .line 559
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    #@15
    .line 560
    invoke-static {}, Landroid/os/Debug;->stopAllocCounting()V

    #@18
    .line 561
    return-void
.end method


# virtual methods
.method public addIteration(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 7
    .parameter "label"

    #@0
    .prologue
    .line 388
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    #@3
    move-result-wide v1

    #@4
    iget-wide v3, p0, Landroid/os/PerformanceCollector;->mCpuTime:J

    #@6
    sub-long/2addr v1, v3

    #@7
    iput-wide v1, p0, Landroid/os/PerformanceCollector;->mCpuTime:J

    #@9
    .line 389
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v1

    #@d
    iget-wide v3, p0, Landroid/os/PerformanceCollector;->mExecTime:J

    #@f
    sub-long/2addr v1, v3

    #@10
    iput-wide v1, p0, Landroid/os/PerformanceCollector;->mExecTime:J

    #@12
    .line 391
    new-instance v0, Landroid/os/Bundle;

    #@14
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@17
    .line 392
    .local v0, iteration:Landroid/os/Bundle;
    const-string/jumbo v1, "label"

    #@1a
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 393
    const-string v1, "execution_time"

    #@1f
    iget-wide v2, p0, Landroid/os/PerformanceCollector;->mExecTime:J

    #@21
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@24
    .line 394
    const-string v1, "cpu_time"

    #@26
    iget-wide v2, p0, Landroid/os/PerformanceCollector;->mCpuTime:J

    #@28
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@2b
    .line 395
    iget-object v1, p0, Landroid/os/PerformanceCollector;->mPerfMeasurement:Landroid/os/Bundle;

    #@2d
    const-string/jumbo v2, "iterations"

    #@30
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@37
    .line 397
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3a
    move-result-wide v1

    #@3b
    iput-wide v1, p0, Landroid/os/PerformanceCollector;->mExecTime:J

    #@3d
    .line 398
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    #@40
    move-result-wide v1

    #@41
    iput-wide v1, p0, Landroid/os/PerformanceCollector;->mCpuTime:J

    #@43
    .line 399
    return-object v0
.end method

.method public addMeasurement(Ljava/lang/String;F)V
    .registers 4
    .parameter "label"
    .parameter "value"

    #@0
    .prologue
    .line 436
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 437
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@6
    invoke-interface {v0, p1, p2}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeMeasurement(Ljava/lang/String;F)V

    #@9
    .line 438
    :cond_9
    return-void
.end method

.method public addMeasurement(Ljava/lang/String;J)V
    .registers 5
    .parameter "label"
    .parameter "value"

    #@0
    .prologue
    .line 425
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 426
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@6
    invoke-interface {v0, p1, p2, p3}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeMeasurement(Ljava/lang/String;J)V

    #@9
    .line 427
    :cond_9
    return-void
.end method

.method public addMeasurement(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "label"
    .parameter "value"

    #@0
    .prologue
    .line 447
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 448
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@6
    invoke-interface {v0, p1, p2}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeMeasurement(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 449
    :cond_9
    return-void
.end method

.method public beginSnapshot(Ljava/lang/String;)V
    .registers 3
    .parameter "label"

    #@0
    .prologue
    .line 306
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 307
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@6
    invoke-interface {v0, p1}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeBeginSnapshot(Ljava/lang/String;)V

    #@9
    .line 308
    :cond_9
    invoke-direct {p0}, Landroid/os/PerformanceCollector;->startPerformanceSnapshot()V

    #@c
    .line 309
    return-void
.end method

.method public endSnapshot()Landroid/os/Bundle;
    .registers 3

    #@0
    .prologue
    .line 350
    invoke-direct {p0}, Landroid/os/PerformanceCollector;->endPerformanceSnapshot()V

    #@3
    .line 351
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 352
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@9
    iget-object v1, p0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@b
    invoke-interface {v0, v1}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeEndSnapshot(Landroid/os/Bundle;)V

    #@e
    .line 353
    :cond_e
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfSnapshot:Landroid/os/Bundle;

    #@10
    return-object v0
.end method

.method public setPerformanceResultsWriter(Landroid/os/PerformanceCollector$PerformanceResultsWriter;)V
    .registers 2
    .parameter "writer"

    #@0
    .prologue
    .line 296
    iput-object p1, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    .line 297
    return-void
.end method

.method public startTiming(Ljava/lang/String;)V
    .registers 5
    .parameter "label"

    #@0
    .prologue
    .line 363
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 364
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@6
    invoke-interface {v0, p1}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeStartTiming(Ljava/lang/String;)V

    #@9
    .line 365
    :cond_9
    new-instance v0, Landroid/os/Bundle;

    #@b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@e
    iput-object v0, p0, Landroid/os/PerformanceCollector;->mPerfMeasurement:Landroid/os/Bundle;

    #@10
    .line 366
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfMeasurement:Landroid/os/Bundle;

    #@12
    const-string/jumbo v1, "iterations"

    #@15
    new-instance v2, Ljava/util/ArrayList;

    #@17
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@1a
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    #@1d
    .line 368
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@20
    move-result-wide v0

    #@21
    iput-wide v0, p0, Landroid/os/PerformanceCollector;->mExecTime:J

    #@23
    .line 369
    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    #@26
    move-result-wide v0

    #@27
    iput-wide v0, p0, Landroid/os/PerformanceCollector;->mCpuTime:J

    #@29
    .line 370
    return-void
.end method

.method public stopTiming(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "label"

    #@0
    .prologue
    .line 412
    invoke-virtual {p0, p1}, Landroid/os/PerformanceCollector;->addIteration(Ljava/lang/String;)Landroid/os/Bundle;

    #@3
    .line 413
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 414
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfWriter:Landroid/os/PerformanceCollector$PerformanceResultsWriter;

    #@9
    iget-object v1, p0, Landroid/os/PerformanceCollector;->mPerfMeasurement:Landroid/os/Bundle;

    #@b
    invoke-interface {v0, v1}, Landroid/os/PerformanceCollector$PerformanceResultsWriter;->writeStopTiming(Landroid/os/Bundle;)V

    #@e
    .line 415
    :cond_e
    iget-object v0, p0, Landroid/os/PerformanceCollector;->mPerfMeasurement:Landroid/os/Bundle;

    #@10
    return-object v0
.end method
