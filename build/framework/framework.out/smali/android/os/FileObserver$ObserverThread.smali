.class Landroid/os/FileObserver$ObserverThread;
.super Ljava/lang/Thread;
.source "FileObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/FileObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ObserverThread"
.end annotation


# instance fields
.field private m_fd:I

.field private m_observers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 83
    const-string v0, "FileObserver"

    #@2
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    .line 79
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@c
    .line 84
    invoke-direct {p0}, Landroid/os/FileObserver$ObserverThread;->init()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/os/FileObserver$ObserverThread;->m_fd:I

    #@12
    .line 85
    return-void
.end method

.method private native init()I
.end method

.method private native observe(I)V
.end method

.method private native startWatching(ILjava/lang/String;I)I
.end method

.method private native stopWatching(II)V
.end method


# virtual methods
.method public onEvent(IILjava/lang/String;)V
    .registers 11
    .parameter "wfd"
    .parameter "mask"
    .parameter "path"

    #@0
    .prologue
    .line 110
    const/4 v1, 0x0

    #@1
    .line 112
    .local v1, observer:Landroid/os/FileObserver;
    iget-object v5, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@3
    monitor-enter v5

    #@4
    .line 113
    :try_start_4
    iget-object v4, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v6

    #@a
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v3

    #@e
    check-cast v3, Ljava/lang/ref/WeakReference;

    #@10
    .line 114
    .local v3, weak:Ljava/lang/ref/WeakReference;
    if-eqz v3, :cond_25

    #@12
    .line 115
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@15
    move-result-object v4

    #@16
    move-object v0, v4

    #@17
    check-cast v0, Landroid/os/FileObserver;

    #@19
    move-object v1, v0

    #@1a
    .line 116
    if-nez v1, :cond_25

    #@1c
    .line 117
    iget-object v4, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@1e
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v6

    #@22
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    .line 120
    :cond_25
    monitor-exit v5
    :try_end_26
    .catchall {:try_start_4 .. :try_end_26} :catchall_2c

    #@26
    .line 123
    if-eqz v1, :cond_2b

    #@28
    .line 125
    :try_start_28
    invoke-virtual {v1, p2, p3}, Landroid/os/FileObserver;->onEvent(ILjava/lang/String;)V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_2b} :catch_2f

    #@2b
    .line 130
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 120
    .end local v3           #weak:Ljava/lang/ref/WeakReference;
    :catchall_2c
    move-exception v4

    #@2d
    :try_start_2d
    monitor-exit v5
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_2c

    #@2e
    throw v4

    #@2f
    .line 126
    .restart local v3       #weak:Ljava/lang/ref/WeakReference;
    :catch_2f
    move-exception v2

    #@30
    .line 127
    .local v2, throwable:Ljava/lang/Throwable;
    const-string v4, "FileObserver"

    #@32
    new-instance v5, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v6, "Unhandled exception in FileObserver "

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v4, v5, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_2b
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/os/FileObserver$ObserverThread;->m_fd:I

    #@2
    invoke-direct {p0, v0}, Landroid/os/FileObserver$ObserverThread;->observe(I)V

    #@5
    .line 89
    return-void
.end method

.method public startWatching(Ljava/lang/String;ILandroid/os/FileObserver;)I
    .registers 9
    .parameter "path"
    .parameter "mask"
    .parameter "observer"

    #@0
    .prologue
    .line 92
    iget v2, p0, Landroid/os/FileObserver$ObserverThread;->m_fd:I

    #@2
    invoke-direct {p0, v2, p1, p2}, Landroid/os/FileObserver$ObserverThread;->startWatching(ILjava/lang/String;I)I

    #@5
    move-result v1

    #@6
    .line 94
    .local v1, wfd:I
    new-instance v0, Ljava/lang/Integer;

    #@8
    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    #@b
    .line 95
    .local v0, i:Ljava/lang/Integer;
    if-ltz v1, :cond_1b

    #@d
    .line 96
    iget-object v3, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@f
    monitor-enter v3

    #@10
    .line 97
    :try_start_10
    iget-object v2, p0, Landroid/os/FileObserver$ObserverThread;->m_observers:Ljava/util/HashMap;

    #@12
    new-instance v4, Ljava/lang/ref/WeakReference;

    #@14
    invoke-direct {v4, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@17
    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 98
    monitor-exit v3
    :try_end_1b
    .catchall {:try_start_10 .. :try_end_1b} :catchall_20

    #@1b
    .line 101
    :cond_1b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@1e
    move-result v2

    #@1f
    return v2

    #@20
    .line 98
    :catchall_20
    move-exception v2

    #@21
    :try_start_21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method public stopWatching(I)V
    .registers 3
    .parameter "descriptor"

    #@0
    .prologue
    .line 105
    iget v0, p0, Landroid/os/FileObserver$ObserverThread;->m_fd:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/os/FileObserver$ObserverThread;->stopWatching(II)V

    #@5
    .line 106
    return-void
.end method
