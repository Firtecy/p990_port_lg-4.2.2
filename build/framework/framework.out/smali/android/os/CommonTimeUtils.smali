.class Landroid/os/CommonTimeUtils;
.super Ljava/lang/Object;
.source "CommonTimeUtils.java"


# static fields
.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x4

.field public static final ERROR_DEAD_OBJECT:I = -0x7

.field public static final SUCCESS:I


# instance fields
.field private mInterfaceDesc:Ljava/lang/String;

.field private mRemote:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;Ljava/lang/String;)V
    .registers 3
    .parameter "remote"
    .parameter "interfaceDesc"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    iput-object p1, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@5
    .line 44
    iput-object p2, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@7
    .line 45
    return-void
.end method


# virtual methods
.method public transactGetInt(II)I
    .registers 9
    .parameter "method_code"
    .parameter "error_ret_val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 49
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 50
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 55
    .local v1, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v4, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 56
    iget-object v4, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v5, 0x0

    #@10
    invoke-interface {v4, p1, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@13
    .line 58
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v2

    #@17
    .line 59
    .local v2, res:I
    if-nez v2, :cond_24

    #@19
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_26

    #@1c
    move-result v3

    #@1d
    .line 62
    .local v3, ret_val:I
    :goto_1d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 63
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 66
    return v3

    #@24
    .end local v3           #ret_val:I
    :cond_24
    move v3, p2

    #@25
    .line 59
    goto :goto_1d

    #@26
    .line 62
    .end local v2           #res:I
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 63
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v4
.end method

.method public transactGetLong(IJ)J
    .registers 11
    .parameter "method_code"
    .parameter "error_ret_val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 91
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 92
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 97
    .local v1, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v5, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 98
    iget-object v5, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v6, 0x0

    #@10
    invoke-interface {v5, p1, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@13
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v2

    #@17
    .line 101
    .local v2, res:I
    if-nez v2, :cond_24

    #@19
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_26

    #@1c
    move-result-wide v3

    #@1d
    .line 104
    .local v3, ret_val:J
    :goto_1d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 105
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 108
    return-wide v3

    #@24
    .end local v3           #ret_val:J
    :cond_24
    move-wide v3, p2

    #@25
    .line 101
    goto :goto_1d

    #@26
    .line 104
    .end local v2           #res:I
    :catchall_26
    move-exception v5

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 105
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v5
.end method

.method public transactGetSockaddr(I)Ljava/net/InetSocketAddress;
    .registers 24
    .parameter "method_code"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v9

    #@4
    .line 176
    .local v9, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v12

    #@8
    .line 177
    .local v12, reply:Landroid/os/Parcel;
    const/4 v14, 0x0

    #@9
    .line 181
    .local v14, ret_val:Ljava/net/InetSocketAddress;
    :try_start_9
    move-object/from16 v0, p0

    #@b
    iget-object v0, v0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@d
    move-object/from16 v17, v0

    #@f
    move-object/from16 v0, v17

    #@11
    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@14
    .line 182
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@18
    move-object/from16 v17, v0

    #@1a
    const/16 v18, 0x0

    #@1c
    move-object/from16 v0, v17

    #@1e
    move/from16 v1, p1

    #@20
    move/from16 v2, v18

    #@22
    invoke-interface {v0, v1, v9, v12, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@25
    .line 184
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v13

    #@29
    .line 185
    .local v13, res:I
    if-nez v13, :cond_92

    #@2b
    .line 187
    const/4 v11, 0x0

    #@2c
    .line 188
    .local v11, port:I
    const/4 v8, 0x0

    #@2d
    .line 190
    .local v8, addrStr:Ljava/lang/String;
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v16

    #@31
    .line 192
    .local v16, type:I
    sget v17, Llibcore/io/OsConstants;->AF_INET:I

    #@33
    move/from16 v0, v17

    #@35
    move/from16 v1, v16

    #@37
    if-ne v0, v1, :cond_99

    #@39
    .line 193
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v3

    #@3d
    .line 194
    .local v3, addr:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v11

    #@41
    .line 195
    const-string v17, "%d.%d.%d.%d"

    #@43
    const/16 v18, 0x4

    #@45
    move/from16 v0, v18

    #@47
    new-array v0, v0, [Ljava/lang/Object;

    #@49
    move-object/from16 v18, v0

    #@4b
    const/16 v19, 0x0

    #@4d
    shr-int/lit8 v20, v3, 0x18

    #@4f
    move/from16 v0, v20

    #@51
    and-int/lit16 v0, v0, 0xff

    #@53
    move/from16 v20, v0

    #@55
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@58
    move-result-object v20

    #@59
    aput-object v20, v18, v19

    #@5b
    const/16 v19, 0x1

    #@5d
    shr-int/lit8 v20, v3, 0x10

    #@5f
    move/from16 v0, v20

    #@61
    and-int/lit16 v0, v0, 0xff

    #@63
    move/from16 v20, v0

    #@65
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v20

    #@69
    aput-object v20, v18, v19

    #@6b
    const/16 v19, 0x2

    #@6d
    shr-int/lit8 v20, v3, 0x8

    #@6f
    move/from16 v0, v20

    #@71
    and-int/lit16 v0, v0, 0xff

    #@73
    move/from16 v20, v0

    #@75
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@78
    move-result-object v20

    #@79
    aput-object v20, v18, v19

    #@7b
    const/16 v19, 0x3

    #@7d
    and-int/lit16 v0, v3, 0xff

    #@7f
    move/from16 v20, v0

    #@81
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@84
    move-result-object v20

    #@85
    aput-object v20, v18, v19

    #@87
    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    .line 217
    .end local v3           #addr:I
    :cond_8b
    :goto_8b
    if-eqz v8, :cond_92

    #@8d
    .line 218
    new-instance v14, Ljava/net/InetSocketAddress;

    #@8f
    .end local v14           #ret_val:Ljava/net/InetSocketAddress;
    invoke-direct {v14, v8, v11}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_92
    .catchall {:try_start_9 .. :try_end_92} :catchall_13d

    #@92
    .line 223
    .end local v8           #addrStr:Ljava/lang/String;
    .end local v11           #port:I
    .end local v16           #type:I
    .restart local v14       #ret_val:Ljava/net/InetSocketAddress;
    :cond_92
    invoke-virtual {v12}, Landroid/os/Parcel;->recycle()V

    #@95
    .line 224
    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    #@98
    .line 227
    return-object v14

    #@99
    .line 199
    .restart local v8       #addrStr:Ljava/lang/String;
    .restart local v11       #port:I
    .restart local v16       #type:I
    :cond_99
    :try_start_99
    sget v17, Llibcore/io/OsConstants;->AF_INET6:I

    #@9b
    move/from16 v0, v17

    #@9d
    move/from16 v1, v16

    #@9f
    if-ne v0, v1, :cond_8b

    #@a1
    .line 200
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@a4
    move-result v4

    #@a5
    .line 201
    .local v4, addr1:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@a8
    move-result v5

    #@a9
    .line 202
    .local v5, addr2:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@ac
    move-result v6

    #@ad
    .line 203
    .local v6, addr3:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@b0
    move-result v7

    #@b1
    .line 205
    .local v7, addr4:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@b4
    move-result v11

    #@b5
    .line 207
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@b8
    move-result v10

    #@b9
    .line 208
    .local v10, flowinfo:I
    invoke-virtual {v12}, Landroid/os/Parcel;->readInt()I

    #@bc
    move-result v15

    #@bd
    .line 210
    .local v15, scope_id:I
    const-string v17, "[%04X:%04X:%04X:%04X:%04X:%04X:%04X:%04X]"

    #@bf
    const/16 v18, 0x8

    #@c1
    move/from16 v0, v18

    #@c3
    new-array v0, v0, [Ljava/lang/Object;

    #@c5
    move-object/from16 v18, v0

    #@c7
    const/16 v19, 0x0

    #@c9
    shr-int/lit8 v20, v4, 0x10

    #@cb
    const v21, 0xffff

    #@ce
    and-int v20, v20, v21

    #@d0
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d3
    move-result-object v20

    #@d4
    aput-object v20, v18, v19

    #@d6
    const/16 v19, 0x1

    #@d8
    const v20, 0xffff

    #@db
    and-int v20, v20, v4

    #@dd
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e0
    move-result-object v20

    #@e1
    aput-object v20, v18, v19

    #@e3
    const/16 v19, 0x2

    #@e5
    shr-int/lit8 v20, v5, 0x10

    #@e7
    const v21, 0xffff

    #@ea
    and-int v20, v20, v21

    #@ec
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ef
    move-result-object v20

    #@f0
    aput-object v20, v18, v19

    #@f2
    const/16 v19, 0x3

    #@f4
    const v20, 0xffff

    #@f7
    and-int v20, v20, v5

    #@f9
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fc
    move-result-object v20

    #@fd
    aput-object v20, v18, v19

    #@ff
    const/16 v19, 0x4

    #@101
    shr-int/lit8 v20, v6, 0x10

    #@103
    const v21, 0xffff

    #@106
    and-int v20, v20, v21

    #@108
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10b
    move-result-object v20

    #@10c
    aput-object v20, v18, v19

    #@10e
    const/16 v19, 0x5

    #@110
    const v20, 0xffff

    #@113
    and-int v20, v20, v6

    #@115
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@118
    move-result-object v20

    #@119
    aput-object v20, v18, v19

    #@11b
    const/16 v19, 0x6

    #@11d
    shr-int/lit8 v20, v7, 0x10

    #@11f
    const v21, 0xffff

    #@122
    and-int v20, v20, v21

    #@124
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@127
    move-result-object v20

    #@128
    aput-object v20, v18, v19

    #@12a
    const/16 v19, 0x7

    #@12c
    const v20, 0xffff

    #@12f
    and-int v20, v20, v7

    #@131
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@134
    move-result-object v20

    #@135
    aput-object v20, v18, v19

    #@137
    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_13a
    .catchall {:try_start_99 .. :try_end_13a} :catchall_13d

    #@13a
    move-result-object v8

    #@13b
    goto/16 :goto_8b

    #@13d
    .line 223
    .end local v4           #addr1:I
    .end local v5           #addr2:I
    .end local v6           #addr3:I
    .end local v7           #addr4:I
    .end local v8           #addrStr:Ljava/lang/String;
    .end local v10           #flowinfo:I
    .end local v11           #port:I
    .end local v13           #res:I
    .end local v14           #ret_val:Ljava/net/InetSocketAddress;
    .end local v15           #scope_id:I
    .end local v16           #type:I
    :catchall_13d
    move-exception v17

    #@13e
    invoke-virtual {v12}, Landroid/os/Parcel;->recycle()V

    #@141
    .line 224
    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    #@144
    throw v17
.end method

.method public transactGetString(ILjava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "method_code"
    .parameter "error_ret_val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 133
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 134
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 139
    .local v1, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v4, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 140
    iget-object v4, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v5, 0x0

    #@10
    invoke-interface {v4, p1, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@13
    .line 142
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@16
    move-result v2

    #@17
    .line 143
    .local v2, res:I
    if-nez v2, :cond_24

    #@19
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_26

    #@1c
    move-result-object v3

    #@1d
    .line 146
    .local v3, ret_val:Ljava/lang/String;
    :goto_1d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 147
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 150
    return-object v3

    #@24
    .end local v3           #ret_val:Ljava/lang/String;
    :cond_24
    move-object v3, p2

    #@25
    .line 143
    goto :goto_1d

    #@26
    .line 146
    .end local v2           #res:I
    :catchall_26
    move-exception v4

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 147
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v4
.end method

.method public transactSetInt(II)I
    .registers 8
    .parameter "method_code"
    .parameter "val"

    #@0
    .prologue
    .line 70
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 71
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 74
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 75
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 76
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x0

    #@13
    invoke-interface {v3, p1, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 78
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_19} :catch_21

    #@19
    move-result v3

    #@1a
    .line 84
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 85
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    :goto_20
    return v3

    #@21
    .line 80
    :catch_21
    move-exception v1

    #@22
    .line 81
    .local v1, e:Landroid/os/RemoteException;
    const/4 v3, -0x7

    #@23
    .line 84
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 85
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    goto :goto_20

    #@2a
    .line 84
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 85
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public transactSetLong(IJ)I
    .registers 9
    .parameter "method_code"
    .parameter "val"

    #@0
    .prologue
    .line 112
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 113
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 116
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 117
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@10
    .line 118
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x0

    #@13
    invoke-interface {v3, p1, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 120
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_19} :catch_21

    #@19
    move-result v3

    #@1a
    .line 126
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    :goto_20
    return v3

    #@21
    .line 122
    :catch_21
    move-exception v1

    #@22
    .line 123
    .local v1, e:Landroid/os/RemoteException;
    const/4 v3, -0x7

    #@23
    .line 126
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    goto :goto_20

    #@2a
    .line 126
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 127
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public transactSetSockaddr(ILjava/net/InetSocketAddress;)I
    .registers 17
    .parameter "method_code"
    .parameter "addr"

    #@0
    .prologue
    .line 231
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v4

    #@4
    .line 232
    .local v4, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v8

    #@8
    .line 233
    .local v8, reply:Landroid/os/Parcel;
    const/4 v9, -0x1

    #@9
    .line 236
    .local v9, ret_val:I
    :try_start_9
    iget-object v12, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@b
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 238
    if-nez p2, :cond_26

    #@10
    .line 239
    const/4 v12, 0x0

    #@11
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 275
    :goto_14
    iget-object v12, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@16
    const/4 v13, 0x0

    #@17
    invoke-interface {v12, p1, v4, v8, v13}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 276
    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_9 .. :try_end_1d} :catchall_be
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_1d} :catch_64

    #@1d
    move-result v9

    #@1e
    .line 282
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 283
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@24
    :goto_24
    move v12, v9

    #@25
    .line 286
    :goto_25
    return v12

    #@26
    .line 241
    :cond_26
    const/4 v12, 0x1

    #@27
    :try_start_27
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 242
    invoke-virtual/range {p2 .. p2}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    #@2d
    move-result-object v1

    #@2e
    .line 243
    .local v1, a:Ljava/net/InetAddress;
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    #@31
    move-result-object v3

    #@32
    .line 244
    .local v3, b:[B
    invoke-virtual/range {p2 .. p2}, Ljava/net/InetSocketAddress;->getPort()I

    #@35
    move-result v7

    #@36
    .line 246
    .local v7, p:I
    instance-of v12, v1, Ljava/net/Inet4Address;

    #@38
    if-eqz v12, :cond_6d

    #@3a
    .line 247
    const/4 v12, 0x0

    #@3b
    aget-byte v12, v3, v12

    #@3d
    and-int/lit16 v12, v12, 0xff

    #@3f
    shl-int/lit8 v12, v12, 0x18

    #@41
    const/4 v13, 0x1

    #@42
    aget-byte v13, v3, v13

    #@44
    and-int/lit16 v13, v13, 0xff

    #@46
    shl-int/lit8 v13, v13, 0x10

    #@48
    or-int/2addr v12, v13

    #@49
    const/4 v13, 0x2

    #@4a
    aget-byte v13, v3, v13

    #@4c
    and-int/lit16 v13, v13, 0xff

    #@4e
    shl-int/lit8 v13, v13, 0x8

    #@50
    or-int/2addr v12, v13

    #@51
    const/4 v13, 0x3

    #@52
    aget-byte v13, v3, v13

    #@54
    and-int/lit16 v13, v13, 0xff

    #@56
    or-int v10, v12, v13

    #@58
    .line 252
    .local v10, v4addr:I
    sget v12, Llibcore/io/OsConstants;->AF_INET:I

    #@5a
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 253
    invoke-virtual {v4, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@60
    .line 254
    invoke-virtual {v4, v7}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_63
    .catchall {:try_start_27 .. :try_end_63} :catchall_be
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_63} :catch_64

    #@63
    goto :goto_14

    #@64
    .line 278
    .end local v1           #a:Ljava/net/InetAddress;
    .end local v3           #b:[B
    .end local v7           #p:I
    .end local v10           #v4addr:I
    :catch_64
    move-exception v5

    #@65
    .line 279
    .local v5, e:Landroid/os/RemoteException;
    const/4 v9, -0x7

    #@66
    .line 282
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    #@69
    .line 283
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@6c
    goto :goto_24

    #@6d
    .line 256
    .end local v5           #e:Landroid/os/RemoteException;
    .restart local v1       #a:Ljava/net/InetAddress;
    .restart local v3       #b:[B
    .restart local v7       #p:I
    :cond_6d
    :try_start_6d
    instance-of v12, v1, Ljava/net/Inet6Address;

    #@6f
    if-eqz v12, :cond_c6

    #@71
    .line 258
    move-object v0, v1

    #@72
    check-cast v0, Ljava/net/Inet6Address;

    #@74
    move-object v11, v0

    #@75
    .line 259
    .local v11, v6:Ljava/net/Inet6Address;
    sget v12, Llibcore/io/OsConstants;->AF_INET6:I

    #@77
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@7a
    .line 260
    const/4 v6, 0x0

    #@7b
    .local v6, i:I
    :goto_7b
    const/4 v12, 0x4

    #@7c
    if-ge v6, v12, :cond_ae

    #@7e
    .line 261
    mul-int/lit8 v12, v6, 0x4

    #@80
    add-int/lit8 v12, v12, 0x0

    #@82
    aget-byte v12, v3, v12

    #@84
    and-int/lit16 v12, v12, 0xff

    #@86
    shl-int/lit8 v12, v12, 0x18

    #@88
    mul-int/lit8 v13, v6, 0x4

    #@8a
    add-int/lit8 v13, v13, 0x1

    #@8c
    aget-byte v13, v3, v13

    #@8e
    and-int/lit16 v13, v13, 0xff

    #@90
    shl-int/lit8 v13, v13, 0x10

    #@92
    or-int/2addr v12, v13

    #@93
    mul-int/lit8 v13, v6, 0x4

    #@95
    add-int/lit8 v13, v13, 0x2

    #@97
    aget-byte v13, v3, v13

    #@99
    and-int/lit16 v13, v13, 0xff

    #@9b
    shl-int/lit8 v13, v13, 0x8

    #@9d
    or-int/2addr v12, v13

    #@9e
    mul-int/lit8 v13, v6, 0x4

    #@a0
    add-int/lit8 v13, v13, 0x3

    #@a2
    aget-byte v13, v3, v13

    #@a4
    and-int/lit16 v13, v13, 0xff

    #@a6
    or-int v2, v12, v13

    #@a8
    .line 265
    .local v2, aword:I
    invoke-virtual {v4, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    .line 260
    add-int/lit8 v6, v6, 0x1

    #@ad
    goto :goto_7b

    #@ae
    .line 267
    .end local v2           #aword:I
    :cond_ae
    invoke-virtual {v4, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@b1
    .line 268
    const/4 v12, 0x0

    #@b2
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@b5
    .line 269
    invoke-virtual {v11}, Ljava/net/Inet6Address;->getScopeId()I

    #@b8
    move-result v12

    #@b9
    invoke-virtual {v4, v12}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_bc
    .catchall {:try_start_6d .. :try_end_bc} :catchall_be
    .catch Landroid/os/RemoteException; {:try_start_6d .. :try_end_bc} :catch_64

    #@bc
    goto/16 :goto_14

    #@be
    .line 282
    .end local v1           #a:Ljava/net/InetAddress;
    .end local v3           #b:[B
    .end local v6           #i:I
    .end local v7           #p:I
    .end local v11           #v6:Ljava/net/Inet6Address;
    :catchall_be
    move-exception v12

    #@bf
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    #@c2
    .line 283
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@c5
    throw v12

    #@c6
    .line 271
    .restart local v1       #a:Ljava/net/InetAddress;
    .restart local v3       #b:[B
    .restart local v7       #p:I
    :cond_c6
    const/4 v12, -0x4

    #@c7
    .line 282
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    #@ca
    .line 283
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@cd
    goto/16 :goto_25
.end method

.method public transactSetString(ILjava/lang/String;)I
    .registers 8
    .parameter "method_code"
    .parameter "val"

    #@0
    .prologue
    .line 154
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 155
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 158
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 159
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 160
    iget-object v3, p0, Landroid/os/CommonTimeUtils;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x0

    #@13
    invoke-interface {v3, p1, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@16
    .line 162
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_2a
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_19} :catch_21

    #@19
    move-result v3

    #@1a
    .line 168
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 169
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    :goto_20
    return v3

    #@21
    .line 164
    :catch_21
    move-exception v1

    #@22
    .line 165
    .local v1, e:Landroid/os/RemoteException;
    const/4 v3, -0x7

    #@23
    .line 168
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 169
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    goto :goto_20

    #@2a
    .line 168
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 169
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method
