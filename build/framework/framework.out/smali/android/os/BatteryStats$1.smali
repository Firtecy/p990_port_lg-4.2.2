.class Landroid/os/BatteryStats$1;
.super Ljava/lang/Object;
.source "BatteryStats.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/os/BatteryStats;->dumpLocked(Ljava/io/PrintWriter;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/os/BatteryStats$TimerEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/os/BatteryStats;


# direct methods
.method constructor <init>(Landroid/os/BatteryStats;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1537
    iput-object p1, p0, Landroid/os/BatteryStats$1;->this$0:Landroid/os/BatteryStats;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public compare(Landroid/os/BatteryStats$TimerEntry;Landroid/os/BatteryStats$TimerEntry;)I
    .registers 8
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 1540
    iget-wide v0, p1, Landroid/os/BatteryStats$TimerEntry;->mTime:J

    #@2
    .line 1541
    .local v0, lhsTime:J
    iget-wide v2, p2, Landroid/os/BatteryStats$TimerEntry;->mTime:J

    #@4
    .line 1542
    .local v2, rhsTime:J
    cmp-long v4, v0, v2

    #@6
    if-gez v4, :cond_a

    #@8
    .line 1543
    const/4 v4, 0x1

    #@9
    .line 1548
    :goto_9
    return v4

    #@a
    .line 1545
    :cond_a
    cmp-long v4, v0, v2

    #@c
    if-lez v4, :cond_10

    #@e
    .line 1546
    const/4 v4, -0x1

    #@f
    goto :goto_9

    #@10
    .line 1548
    :cond_10
    const/4 v4, 0x0

    #@11
    goto :goto_9
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1537
    check-cast p1, Landroid/os/BatteryStats$TimerEntry;

    #@2
    .end local p1
    check-cast p2, Landroid/os/BatteryStats$TimerEntry;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/os/BatteryStats$1;->compare(Landroid/os/BatteryStats$TimerEntry;Landroid/os/BatteryStats$TimerEntry;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
