.class public final Landroid/os/BatteryStats$BitDescription;
.super Ljava/lang/Object;
.source "BatteryStats.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/BatteryStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BitDescription"
.end annotation


# instance fields
.field public final mask:I

.field public final name:Ljava/lang/String;

.field public final shift:I

.field public final values:[Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;[Ljava/lang/String;)V
    .registers 5
    .parameter "mask"
    .parameter "shift"
    .parameter "name"
    .parameter "values"

    #@0
    .prologue
    .line 720
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 721
    iput p1, p0, Landroid/os/BatteryStats$BitDescription;->mask:I

    #@5
    .line 722
    iput p2, p0, Landroid/os/BatteryStats$BitDescription;->shift:I

    #@7
    .line 723
    iput-object p3, p0, Landroid/os/BatteryStats$BitDescription;->name:Ljava/lang/String;

    #@9
    .line 724
    iput-object p4, p0, Landroid/os/BatteryStats$BitDescription;->values:[Ljava/lang/String;

    #@b
    .line 725
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "mask"
    .parameter "name"

    #@0
    .prologue
    .line 713
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 714
    iput p1, p0, Landroid/os/BatteryStats$BitDescription;->mask:I

    #@5
    .line 715
    const/4 v0, -0x1

    #@6
    iput v0, p0, Landroid/os/BatteryStats$BitDescription;->shift:I

    #@8
    .line 716
    iput-object p2, p0, Landroid/os/BatteryStats$BitDescription;->name:Ljava/lang/String;

    #@a
    .line 717
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/os/BatteryStats$BitDescription;->values:[Ljava/lang/String;

    #@d
    .line 718
    return-void
.end method
