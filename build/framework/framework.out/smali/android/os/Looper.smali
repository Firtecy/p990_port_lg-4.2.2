.class public Landroid/os/Looper;
.super Ljava/lang/Object;
.source "Looper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Looper"

.field private static sMainLooper:Landroid/os/Looper;

.field static final sThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/os/Looper;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mLogging:Landroid/util/Printer;

.field final mQueue:Landroid/os/MessageQueue;

.field volatile mRun:Z

.field final mThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 57
    new-instance v0, Ljava/lang/ThreadLocal;

    #@2
    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    #@5
    sput-object v0, Landroid/os/Looper;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@7
    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter "quitAllowed"

    #@0
    .prologue
    .line 188
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 189
    new-instance v0, Landroid/os/MessageQueue;

    #@5
    invoke-direct {v0, p1}, Landroid/os/MessageQueue;-><init>(Z)V

    #@8
    iput-object v0, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@a
    .line 190
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/os/Looper;->mRun:Z

    #@d
    .line 191
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Landroid/os/Looper;->mThread:Ljava/lang/Thread;

    #@13
    .line 192
    return-void
.end method

.method public static getMainLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 102
    const-class v1, Landroid/os/Looper;

    #@2
    monitor-enter v1

    #@3
    .line 103
    :try_start_3
    sget-object v0, Landroid/os/Looper;->sMainLooper:Landroid/os/Looper;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 104
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public static loop()V
    .registers 11

    #@0
    .prologue
    .line 112
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v3

    #@4
    .line 113
    .local v3, me:Landroid/os/Looper;
    if-nez v3, :cond_e

    #@6
    .line 114
    new-instance v8, Ljava/lang/RuntimeException;

    #@8
    const-string v9, "No Looper; Looper.prepare() wasn\'t called on this thread."

    #@a
    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v8

    #@e
    .line 116
    :cond_e
    iget-object v7, v3, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@10
    .line 120
    .local v7, queue:Landroid/os/MessageQueue;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@13
    .line 121
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@16
    move-result-wide v0

    #@17
    .line 124
    .local v0, ident:J
    :goto_17
    invoke-virtual {v7}, Landroid/os/MessageQueue;->next()Landroid/os/Message;

    #@1a
    move-result-object v4

    #@1b
    .line 125
    .local v4, msg:Landroid/os/Message;
    if-nez v4, :cond_1e

    #@1d
    .line 127
    return-void

    #@1e
    .line 131
    :cond_1e
    iget-object v2, v3, Landroid/os/Looper;->mLogging:Landroid/util/Printer;

    #@20
    .line 132
    .local v2, logging:Landroid/util/Printer;
    if-eqz v2, :cond_52

    #@22
    .line 133
    new-instance v8, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v9, ">>>>> Dispatching to "

    #@29
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    iget-object v9, v4, Landroid/os/Message;->target:Landroid/os/Handler;

    #@2f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    const-string v9, " "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    iget-object v9, v4, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@3b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    const-string v9, ": "

    #@41
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v8

    #@45
    iget v9, v4, Landroid/os/Message;->what:I

    #@47
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-interface {v2, v8}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@52
    .line 137
    :cond_52
    iget-object v8, v4, Landroid/os/Message;->target:Landroid/os/Handler;

    #@54
    invoke-virtual {v8, v4}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    #@57
    .line 139
    if-eqz v2, :cond_7d

    #@59
    .line 140
    new-instance v8, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v9, "<<<<< Finished to "

    #@60
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v8

    #@64
    iget-object v9, v4, Landroid/os/Message;->target:Landroid/os/Handler;

    #@66
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v8

    #@6a
    const-string v9, " "

    #@6c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v8

    #@70
    iget-object v9, v4, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v8

    #@7a
    invoke-interface {v2, v8}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@7d
    .line 145
    :cond_7d
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@80
    move-result-wide v5

    #@81
    .line 146
    .local v5, newIdent:J
    cmp-long v8, v0, v5

    #@83
    if-eqz v8, :cond_db

    #@85
    .line 147
    const-string v8, "Looper"

    #@87
    new-instance v9, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v10, "Thread identity changed from 0x"

    #@8e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v9

    #@92
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@95
    move-result-object v10

    #@96
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v9

    #@9a
    const-string v10, " to 0x"

    #@9c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-static {v5, v6}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    const-string v10, " while dispatching to "

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    iget-object v10, v4, Landroid/os/Message;->target:Landroid/os/Handler;

    #@b0
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@b7
    move-result-object v10

    #@b8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v9

    #@bc
    const-string v10, " "

    #@be
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v9

    #@c2
    iget-object v10, v4, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@c4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v9

    #@c8
    const-string v10, " what="

    #@ca
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v9

    #@ce
    iget v10, v4, Landroid/os/Message;->what:I

    #@d0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v9

    #@d4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v9

    #@d8
    invoke-static {v8, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    .line 154
    :cond_db
    invoke-virtual {v4}, Landroid/os/Message;->recycle()V

    #@de
    goto/16 :goto_17
.end method

.method public static myLooper()Landroid/os/Looper;
    .registers 1

    #@0
    .prologue
    .line 163
    sget-object v0, Landroid/os/Looper;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/Looper;

    #@8
    return-object v0
.end method

.method public static myQueue()Landroid/os/MessageQueue;
    .registers 1

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    iget-object v0, v0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@6
    return-object v0
.end method

.method public static prepare()V
    .registers 1

    #@0
    .prologue
    .line 73
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Landroid/os/Looper;->prepare(Z)V

    #@4
    .line 74
    return-void
.end method

.method private static prepare(Z)V
    .registers 3
    .parameter "quitAllowed"

    #@0
    .prologue
    .line 77
    sget-object v0, Landroid/os/Looper;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_10

    #@8
    .line 78
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string v1, "Only one Looper may be created per thread"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 80
    :cond_10
    sget-object v0, Landroid/os/Looper;->sThreadLocal:Ljava/lang/ThreadLocal;

    #@12
    new-instance v1, Landroid/os/Looper;

    #@14
    invoke-direct {v1, p0}, Landroid/os/Looper;-><init>(Z)V

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@1a
    .line 81
    return-void
.end method

.method public static prepareMainLooper()V
    .registers 3

    #@0
    .prologue
    .line 90
    const/4 v0, 0x0

    #@1
    invoke-static {v0}, Landroid/os/Looper;->prepare(Z)V

    #@4
    .line 91
    const-class v1, Landroid/os/Looper;

    #@6
    monitor-enter v1

    #@7
    .line 92
    :try_start_7
    sget-object v0, Landroid/os/Looper;->sMainLooper:Landroid/os/Looper;

    #@9
    if-eqz v0, :cond_16

    #@b
    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    const-string v2, "The main Looper has already been prepared."

    #@f
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 96
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_13

    #@15
    throw v0

    #@16
    .line 95
    :cond_16
    :try_start_16
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@19
    move-result-object v0

    #@1a
    sput-object v0, Landroid/os/Looper;->sMainLooper:Landroid/os/Looper;

    #@1c
    .line 96
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_16 .. :try_end_1d} :catchall_13

    #@1d
    .line 97
    return-void
.end method


# virtual methods
.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 10
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 258
    invoke-static {p1, p2}, Landroid/util/PrefixPrinter;->create(Landroid/util/Printer;Ljava/lang/String;)Landroid/util/Printer;

    #@3
    move-result-object p1

    #@4
    .line 259
    invoke-virtual {p0}, Landroid/os/Looper;->toString()Ljava/lang/String;

    #@7
    move-result-object v4

    #@8
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@b
    .line 260
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string/jumbo v5, "mRun="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    iget-boolean v5, p0, Landroid/os/Looper;->mRun:Z

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@24
    .line 261
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string/jumbo v5, "mThread="

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    iget-object v5, p0, Landroid/os/Looper;->mThread:Ljava/lang/Thread;

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v4

    #@3a
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3d
    .line 262
    new-instance v4, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string/jumbo v5, "mQueue="

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    iget-object v4, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@4b
    if-eqz v4, :cond_95

    #@4d
    iget-object v4, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@4f
    :goto_4f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@5a
    .line 263
    iget-object v4, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@5c
    if-eqz v4, :cond_b5

    #@5e
    .line 264
    iget-object v5, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@60
    monitor-enter v5

    #@61
    .line 265
    :try_start_61
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@64
    move-result-wide v2

    #@65
    .line 266
    .local v2, now:J
    iget-object v4, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@67
    iget-object v0, v4, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@69
    .line 267
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x0

    #@6a
    .line 268
    .local v1, n:I
    :goto_6a
    if-eqz v0, :cond_98

    #@6c
    .line 269
    new-instance v4, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v6, "  Message "

    #@73
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    const-string v6, ": "

    #@7d
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v4

    #@81
    invoke-virtual {v0, v2, v3}, Landroid/os/Message;->toString(J)Ljava/lang/String;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@90
    .line 270
    add-int/lit8 v1, v1, 0x1

    #@92
    .line 271
    iget-object v0, v0, Landroid/os/Message;->next:Landroid/os/Message;
    :try_end_94
    .catchall {:try_start_61 .. :try_end_94} :catchall_b6

    #@94
    goto :goto_6a

    #@95
    .line 262
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #n:I
    .end local v2           #now:J
    :cond_95
    const-string v4, "(null"

    #@97
    goto :goto_4f

    #@98
    .line 273
    .restart local v0       #msg:Landroid/os/Message;
    .restart local v1       #n:I
    .restart local v2       #now:J
    :cond_98
    :try_start_98
    new-instance v4, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v6, "(Total messages: "

    #@9f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v4

    #@a3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v4

    #@a7
    const-string v6, ")"

    #@a9
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v4

    #@ad
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v4

    #@b1
    invoke-interface {p1, v4}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@b4
    .line 274
    monitor-exit v5

    #@b5
    .line 276
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #n:I
    .end local v2           #now:J
    :cond_b5
    return-void

    #@b6
    .line 274
    :catchall_b6
    move-exception v4

    #@b7
    monitor-exit v5
    :try_end_b8
    .catchall {:try_start_98 .. :try_end_b8} :catchall_b6

    #@b8
    throw v4
.end method

.method public getQueue()Landroid/os/MessageQueue;
    .registers 2

    #@0
    .prologue
    .line 254
    iget-object v0, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@2
    return-object v0
.end method

.method public getThread()Ljava/lang/Thread;
    .registers 2

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/os/Looper;->mThread:Ljava/lang/Thread;

    #@2
    return-object v0
.end method

.method public final postSyncBarrier()I
    .registers 4

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5
    move-result-wide v1

    #@6
    invoke-virtual {v0, v1, v2}, Landroid/os/MessageQueue;->enqueueSyncBarrier(J)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public quit()V
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0}, Landroid/os/MessageQueue;->quit()V

    #@5
    .line 201
    return-void
.end method

.method public final removeSyncBarrier(I)V
    .registers 3
    .parameter "token"

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/MessageQueue;->removeSyncBarrier(I)V

    #@5
    .line 243
    return-void
.end method

.method public setMessageLogging(Landroid/util/Printer;)V
    .registers 2
    .parameter "printer"

    #@0
    .prologue
    .line 176
    iput-object p1, p0, Landroid/os/Looper;->mLogging:Landroid/util/Printer;

    #@2
    .line 177
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Looper{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@e
    move-result v1

    #@f
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string/jumbo v1, "}"

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v0

    #@22
    return-object v0
.end method
