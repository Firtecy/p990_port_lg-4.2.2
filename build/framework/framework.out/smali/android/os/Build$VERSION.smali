.class public Landroid/os/Build$VERSION;
.super Ljava/lang/Object;
.source "Build.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/Build;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VERSION"
.end annotation


# static fields
.field public static final CODENAME:Ljava/lang/String;

.field public static final INCREMENTAL:Ljava/lang/String;

.field public static final RELEASE:Ljava/lang/String;

.field public static final RESOURCES_SDK_INT:I

.field public static final SDK:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SDK_INT:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 85
    const-string/jumbo v1, "ro.build.version.incremental"

    #@4
    invoke-static {v1}, Landroid/os/Build;->access$000(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    sput-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    #@a
    .line 90
    const-string/jumbo v1, "ro.build.version.release"

    #@d
    invoke-static {v1}, Landroid/os/Build;->access$000(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    #@13
    .line 99
    const-string/jumbo v1, "ro.build.version.sdk"

    #@16
    invoke-static {v1}, Landroid/os/Build;->access$000(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    sput-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    #@1c
    .line 105
    const-string/jumbo v1, "ro.build.version.sdk"

    #@1f
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@22
    move-result v1

    #@23
    sput v1, Landroid/os/Build$VERSION;->SDK_INT:I

    #@25
    .line 112
    const-string/jumbo v1, "ro.build.version.codename"

    #@28
    invoke-static {v1}, Landroid/os/Build;->access$000(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    sput-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@2e
    .line 120
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    #@30
    const-string v2, "REL"

    #@32
    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v2

    #@38
    if-eqz v2, :cond_3e

    #@3a
    :goto_3a
    add-int/2addr v0, v1

    #@3b
    sput v0, Landroid/os/Build$VERSION;->RESOURCES_SDK_INT:I

    #@3d
    return-void

    #@3e
    :cond_3e
    const/4 v0, 0x1

    #@3f
    goto :goto_3a
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
