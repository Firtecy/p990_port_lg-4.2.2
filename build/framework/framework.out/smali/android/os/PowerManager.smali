.class public final Landroid/os/PowerManager;
.super Ljava/lang/Object;
.source "PowerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/PowerManager$WakeLock;
    }
.end annotation


# static fields
.field public static final ACQUIRE_CAUSES_WAKEUP:I = 0x10000000

.field public static final BRIGHTNESS_OFF:I = 0x0

.field public static final BRIGHTNESS_ON:I = 0xff

.field public static final FULL_WAKE_LOCK:I = 0x1a
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final GO_TO_SLEEP_REASON_DEVICE_ADMIN:I = 0x1

.field public static final GO_TO_SLEEP_REASON_PROX_SENSOR:I = 0x3

.field public static final GO_TO_SLEEP_REASON_TIMEOUT:I = 0x2

.field public static final GO_TO_SLEEP_REASON_USER:I = 0x0

.field public static final ON_AFTER_RELEASE:I = 0x20000000

.field public static final PARTIAL_WAKE_LOCK:I = 0x1

.field public static final PROXIMITY_SCREEN_OFF_WAKE_LOCK:I = 0x20

.field public static final SCREEN_BRIGHT_WAKE_LOCK:I = 0xa
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SCREEN_DIM_WAKE_LOCK:I = 0x6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "PowerManager"

.field public static final USER_ACTIVITY_EVENT_BUTTON:I = 0x1

.field public static final USER_ACTIVITY_EVENT_OTHER:I = 0x0

.field public static final USER_ACTIVITY_EVENT_TOUCH:I = 0x2

.field public static final USER_ACTIVITY_FLAG_NO_CHANGE_LIGHTS:I = 0x1

.field public static final WAIT_FOR_PROXIMITY_NEGATIVE:I = 0x1

.field public static final WAKE_LOCK_LEVEL_MASK:I = 0xffff


# instance fields
.field final mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field final mService:Landroid/os/IPowerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/IPowerManager;Landroid/os/Handler;)V
    .registers 4
    .parameter "context"
    .parameter "service"
    .parameter "handler"

    #@0
    .prologue
    .line 308
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 309
    iput-object p1, p0, Landroid/os/PowerManager;->mContext:Landroid/content/Context;

    #@5
    .line 310
    iput-object p2, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@7
    .line 311
    iput-object p3, p0, Landroid/os/PowerManager;->mHandler:Landroid/os/Handler;

    #@9
    .line 312
    return-void
.end method

.method public static useScreenAutoBrightnessAdjustmentFeature()Z
    .registers 2

    #@0
    .prologue
    .line 351
    const-string/jumbo v0, "persist.power.useautobrightadj"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static useTwilightAdjustmentFeature()Z
    .registers 2

    #@0
    .prologue
    .line 360
    const-string/jumbo v0, "persist.power.usetwilightadj"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static validateWakeLockParameters(ILjava/lang/String;)V
    .registers 4
    .parameter "levelAndFlags"
    .parameter "tag"

    #@0
    .prologue
    .line 423
    const v0, 0xffff

    #@3
    and-int/2addr v0, p0

    #@4
    sparse-switch v0, :sswitch_data_1a

    #@7
    .line 431
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string v1, "Must specify a valid wake lock level."

    #@b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 433
    :sswitch_f
    if-nez p1, :cond_19

    #@11
    .line 434
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v1, "The tag must not be null."

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 436
    :cond_19
    return-void

    #@1a
    .line 423
    :sswitch_data_1a
    .sparse-switch
        0x1 -> :sswitch_f
        0x6 -> :sswitch_f
        0xa -> :sswitch_f
        0x1a -> :sswitch_f
        0x20 -> :sswitch_f
    .end sparse-switch
.end method


# virtual methods
.method public getDefaultScreenBrightnessSetting()I
    .registers 3

    #@0
    .prologue
    .line 341
    iget-object v0, p0, Landroid/os/PowerManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10e0027

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getLockState()Z
    .registers 4

    #@0
    .prologue
    .line 667
    const-string v1, "PowerManager"

    #@2
    const-string v2, "[SKT Lock&Wipe] getLockState()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 669
    :try_start_7
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@9
    invoke-interface {v1}, Landroid/os/IPowerManager;->getLockState()Z
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_e

    #@c
    move-result v1

    #@d
    .line 672
    :goto_d
    return v1

    #@e
    .line 670
    :catch_e
    move-exception v0

    #@f
    .line 671
    .local v0, ex:Landroid/os/RemoteException;
    const-string v1, "PowerManager"

    #@11
    const-string v2, "getLockState() : exception!"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 672
    const/4 v1, 0x0

    #@17
    goto :goto_d
.end method

.method public getMaximumScreenBrightnessSetting()I
    .registers 3

    #@0
    .prologue
    .line 332
    iget-object v0, p0, Landroid/os/PowerManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10e0026

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getMinimumScreenBrightnessSetting()I
    .registers 3

    #@0
    .prologue
    .line 321
    iget-object v0, p0, Landroid/os/PowerManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v0

    #@6
    const v1, 0x10e0025

    #@9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getNextTimeout()J
    .registers 6

    #@0
    .prologue
    .line 943
    const-wide/16 v1, -0x1

    #@2
    .line 945
    .local v1, nextTimeout:J
    :try_start_2
    iget-object v3, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@4
    invoke-interface {v3}, Landroid/os/IPowerManager;->getNextTimeout()J
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_7} :catch_a

    #@7
    move-result-wide v1

    #@8
    move-wide v3, v1

    #@9
    .line 950
    :goto_9
    return-wide v3

    #@a
    .line 946
    :catch_a
    move-exception v0

    #@b
    .line 947
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "PowerManager"

    #@d
    const-string v4, "getNextTimeout() : exception"

    #@f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 948
    const-wide/16 v3, 0x0

    #@14
    goto :goto_9
.end method

.method public goToSleep(J)V
    .registers 5
    .parameter "time"

    #@0
    .prologue
    .line 489
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, p1, p2, v1}, Landroid/os/IPowerManager;->goToSleep(JI)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_7

    #@6
    .line 492
    :goto_6
    return-void

    #@7
    .line 490
    :catch_7
    move-exception v0

    #@8
    goto :goto_6
.end method

.method public hideLocked()V
    .registers 5

    #@0
    .prologue
    .line 648
    const-string v1, "KR"

    #@2
    const-string/jumbo v2, "ro.build.target_country"

    #@5
    const-string v3, "COM"

    #@7
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_2e

    #@11
    const-string v1, "SKT"

    #@13
    const-string/jumbo v2, "ro.build.target_operator"

    #@16
    const-string v3, "COM"

    #@18
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2e

    #@22
    .line 649
    const-string v1, "PowerManager"

    #@24
    const-string v2, "[SKT Lock&Wipe] hideLocked()"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 651
    :try_start_29
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2b
    invoke-interface {v1}, Landroid/os/IPowerManager;->hideLocked()V
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_2e} :catch_2f

    #@2e
    .line 656
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 652
    :catch_2f
    move-exception v0

    #@30
    .line 653
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PowerManager"

    #@32
    const-string v2, "hideLocked() : exception!"

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_2e
.end method

.method public isLcdOn()Z
    .registers 5

    #@0
    .prologue
    .line 971
    const/4 v1, 0x0

    #@1
    .line 973
    .local v1, lcdOn:Z
    :try_start_1
    iget-object v2, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@3
    invoke-interface {v2}, Landroid/os/IPowerManager;->isLcdOn()Z
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_8

    #@6
    move-result v1

    #@7
    .line 977
    :goto_7
    return v1

    #@8
    .line 974
    :catch_8
    move-exception v0

    #@9
    .line 975
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "PowerManager"

    #@b
    const-string v3, "isLcdOn() : exception!"

    #@d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    goto :goto_7
.end method

.method public isScreenOn()Z
    .registers 3

    #@0
    .prologue
    .line 594
    :try_start_0
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v1}, Landroid/os/IPowerManager;->isScreenOn()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 596
    :goto_6
    return v1

    #@7
    .line 595
    :catch_7
    move-exception v0

    #@8
    .line 596
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isWakeLockLevelSupported(I)Z
    .registers 4
    .parameter "level"

    #@0
    .prologue
    .line 573
    :try_start_0
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IPowerManager;->isWakeLockLevelSupported(I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 575
    :goto_6
    return v1

    #@7
    .line 574
    :catch_7
    move-exception v0

    #@8
    .line 575
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public nap(J)V
    .registers 4
    .parameter "time"

    #@0
    .prologue
    .line 541
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/os/IPowerManager;->nap(J)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 544
    :goto_5
    return-void

    #@6
    .line 542
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
    .registers 4
    .parameter "levelAndFlags"
    .parameter "tag"

    #@0
    .prologue
    .line 417
    invoke-static {p1, p2}, Landroid/os/PowerManager;->validateWakeLockParameters(ILjava/lang/String;)V

    #@3
    .line 418
    new-instance v0, Landroid/os/PowerManager$WakeLock;

    #@5
    invoke-direct {v0, p0, p1, p2}, Landroid/os/PowerManager$WakeLock;-><init>(Landroid/os/PowerManager;ILjava/lang/String;)V

    #@8
    return-object v0
.end method

.method public perflockBoostExt(II)V
    .registers 4
    .parameter "perfLevel"
    .parameter "ms"

    #@0
    .prologue
    .line 931
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/os/IPowerManager;->perflockBoostExt(II)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 934
    :goto_5
    return-void

    #@6
    .line 932
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public reboot(Ljava/lang/String;)V
    .registers 5
    .parameter "reason"

    #@0
    .prologue
    .line 611
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v2, 0x1

    #@4
    invoke-interface {v0, v1, p1, v2}, Landroid/os/IPowerManager;->reboot(ZLjava/lang/String;Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 614
    :goto_7
    return-void

    #@8
    .line 612
    :catch_8
    move-exception v0

    #@9
    goto :goto_7
.end method

.method public recoverBacklightBrightness(I)V
    .registers 3
    .parameter "brightness"

    #@0
    .prologue
    .line 959
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v0, p1}, Landroid/os/IPowerManager;->recoverBacklightBrightness(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 962
    :goto_5
    return-void

    #@6
    .line 960
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public setBacklightBrightness(I)V
    .registers 3
    .parameter "brightness"

    #@0
    .prologue
    .line 558
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v0, p1}, Landroid/os/IPowerManager;->setTemporaryScreenBrightnessSettingOverride(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 561
    :goto_5
    return-void

    #@6
    .line 559
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public showLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    .line 630
    const-string v1, "KR"

    #@2
    const-string/jumbo v2, "ro.build.target_country"

    #@5
    const-string v3, "COM"

    #@7
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v1

    #@f
    if-eqz v1, :cond_27

    #@11
    const-string v1, "SKT"

    #@13
    const-string/jumbo v2, "ro.build.target_operator"

    #@16
    const-string v3, "COM"

    #@18
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_27

    #@22
    .line 633
    :try_start_22
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@24
    invoke-interface {v1, p1, p2, p3}, Landroid/os/IPowerManager;->showLocked(ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_27} :catch_28

    #@27
    .line 638
    :cond_27
    :goto_27
    return-void

    #@28
    .line 634
    :catch_28
    move-exception v0

    #@29
    .line 635
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "PowerManager"

    #@2b
    const-string/jumbo v2, "showLocked() : exception!"

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_27
.end method

.method public userActivity(JZ)V
    .registers 7
    .parameter "when"
    .parameter "noChangeLights"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 464
    :try_start_1
    iget-object v1, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@3
    const/4 v2, 0x0

    #@4
    if-eqz p3, :cond_7

    #@6
    const/4 v0, 0x1

    #@7
    :cond_7
    invoke-interface {v1, p1, p2, v2, v0}, Landroid/os/IPowerManager;->userActivity(JII)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_b

    #@a
    .line 468
    :goto_a
    return-void

    #@b
    .line 466
    :catch_b
    move-exception v0

    #@c
    goto :goto_a
.end method

.method public wakeUp(J)V
    .registers 4
    .parameter "time"

    #@0
    .prologue
    .line 513
    :try_start_0
    iget-object v0, p0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/os/IPowerManager;->wakeUp(J)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 516
    :goto_5
    return-void

    #@6
    .line 514
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method
