.class public abstract Landroid/os/IUserManager$Stub;
.super Landroid/os/Binder;
.source "IUserManager.java"

# interfaces
.implements Landroid/os/IUserManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/IUserManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/IUserManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.IUserManager"

.field static final TRANSACTION_createUser:I = 0x1

.field static final TRANSACTION_getUserHandle:I = 0xc

.field static final TRANSACTION_getUserIcon:I = 0x5

.field static final TRANSACTION_getUserInfo:I = 0x7

.field static final TRANSACTION_getUserSerialNumber:I = 0xb

.field static final TRANSACTION_getUsers:I = 0x6

.field static final TRANSACTION_isGuestEnabled:I = 0x9

.field static final TRANSACTION_removeUser:I = 0x2

.field static final TRANSACTION_setGuestEnabled:I = 0x8

.field static final TRANSACTION_setUserIcon:I = 0x4

.field static final TRANSACTION_setUserName:I = 0x3

.field static final TRANSACTION_wipeUser:I = 0xa


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.os.IUserManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/IUserManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/IUserManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.os.IUserManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/IUserManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/os/IUserManager;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/os/IUserManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/IUserManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 41
    sparse-switch p1, :sswitch_data_13a

    #@5
    .line 192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v5

    #@9
    :goto_9
    return v5

    #@a
    .line 45
    :sswitch_a
    const-string v4, "android.os.IUserManager"

    #@c
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 50
    :sswitch_10
    const-string v6, "android.os.IUserManager"

    #@12
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 54
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v1

    #@1d
    .line 55
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Landroid/os/IUserManager$Stub;->createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;

    #@20
    move-result-object v2

    #@21
    .line 56
    .local v2, _result:Landroid/content/pm/UserInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24
    .line 57
    if-eqz v2, :cond_2d

    #@26
    .line 58
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    .line 59
    invoke-virtual {v2, p3, v5}, Landroid/content/pm/UserInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    goto :goto_9

    #@2d
    .line 62
    :cond_2d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@30
    goto :goto_9

    #@31
    .line 68
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    .end local v2           #_result:Landroid/content/pm/UserInfo;
    :sswitch_31
    const-string v6, "android.os.IUserManager"

    #@33
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39
    move-result v0

    #@3a
    .line 71
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->removeUser(I)Z

    #@3d
    move-result v2

    #@3e
    .line 72
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@41
    .line 73
    if-eqz v2, :cond_44

    #@43
    move v4, v5

    #@44
    :cond_44
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    goto :goto_9

    #@48
    .line 78
    .end local v0           #_arg0:I
    .end local v2           #_result:Z
    :sswitch_48
    const-string v4, "android.os.IUserManager"

    #@4a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4d
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v0

    #@51
    .line 82
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@54
    move-result-object v1

    #@55
    .line 83
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/os/IUserManager$Stub;->setUserName(ILjava/lang/String;)V

    #@58
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5b
    goto :goto_9

    #@5c
    .line 89
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Ljava/lang/String;
    :sswitch_5c
    const-string v4, "android.os.IUserManager"

    #@5e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@64
    move-result v0

    #@65
    .line 93
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@68
    move-result v4

    #@69
    if-eqz v4, :cond_7a

    #@6b
    .line 94
    sget-object v4, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6d
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@70
    move-result-object v1

    #@71
    check-cast v1, Landroid/graphics/Bitmap;

    #@73
    .line 99
    .local v1, _arg1:Landroid/graphics/Bitmap;
    :goto_73
    invoke-virtual {p0, v0, v1}, Landroid/os/IUserManager$Stub;->setUserIcon(ILandroid/graphics/Bitmap;)V

    #@76
    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@79
    goto :goto_9

    #@7a
    .line 97
    .end local v1           #_arg1:Landroid/graphics/Bitmap;
    :cond_7a
    const/4 v1, 0x0

    #@7b
    .restart local v1       #_arg1:Landroid/graphics/Bitmap;
    goto :goto_73

    #@7c
    .line 105
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Landroid/graphics/Bitmap;
    :sswitch_7c
    const-string v6, "android.os.IUserManager"

    #@7e
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@81
    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@84
    move-result v0

    #@85
    .line 108
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->getUserIcon(I)Landroid/graphics/Bitmap;

    #@88
    move-result-object v2

    #@89
    .line 109
    .local v2, _result:Landroid/graphics/Bitmap;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    .line 110
    if-eqz v2, :cond_96

    #@8e
    .line 111
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@91
    .line 112
    invoke-virtual {v2, p3, v5}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    #@94
    goto/16 :goto_9

    #@96
    .line 115
    :cond_96
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@99
    goto/16 :goto_9

    #@9b
    .line 121
    .end local v0           #_arg0:I
    .end local v2           #_result:Landroid/graphics/Bitmap;
    :sswitch_9b
    const-string v6, "android.os.IUserManager"

    #@9d
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a3
    move-result v6

    #@a4
    if-eqz v6, :cond_b3

    #@a6
    move v0, v5

    #@a7
    .line 124
    .local v0, _arg0:Z
    :goto_a7
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->getUsers(Z)Ljava/util/List;

    #@aa
    move-result-object v3

    #@ab
    .line 125
    .local v3, _result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ae
    .line 126
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    #@b1
    goto/16 :goto_9

    #@b3
    .end local v0           #_arg0:Z
    .end local v3           #_result:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :cond_b3
    move v0, v4

    #@b4
    .line 123
    goto :goto_a7

    #@b5
    .line 131
    :sswitch_b5
    const-string v6, "android.os.IUserManager"

    #@b7
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ba
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bd
    move-result v0

    #@be
    .line 134
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->getUserInfo(I)Landroid/content/pm/UserInfo;

    #@c1
    move-result-object v2

    #@c2
    .line 135
    .local v2, _result:Landroid/content/pm/UserInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c5
    .line 136
    if-eqz v2, :cond_cf

    #@c7
    .line 137
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    .line 138
    invoke-virtual {v2, p3, v5}, Landroid/content/pm/UserInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@cd
    goto/16 :goto_9

    #@cf
    .line 141
    :cond_cf
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@d2
    goto/16 :goto_9

    #@d4
    .line 147
    .end local v0           #_arg0:I
    .end local v2           #_result:Landroid/content/pm/UserInfo;
    :sswitch_d4
    const-string v6, "android.os.IUserManager"

    #@d6
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d9
    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@dc
    move-result v6

    #@dd
    if-eqz v6, :cond_e8

    #@df
    move v0, v5

    #@e0
    .line 150
    .local v0, _arg0:Z
    :goto_e0
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->setGuestEnabled(Z)V

    #@e3
    .line 151
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e6
    goto/16 :goto_9

    #@e8
    .end local v0           #_arg0:Z
    :cond_e8
    move v0, v4

    #@e9
    .line 149
    goto :goto_e0

    #@ea
    .line 156
    :sswitch_ea
    const-string v6, "android.os.IUserManager"

    #@ec
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ef
    .line 157
    invoke-virtual {p0}, Landroid/os/IUserManager$Stub;->isGuestEnabled()Z

    #@f2
    move-result v2

    #@f3
    .line 158
    .local v2, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f6
    .line 159
    if-eqz v2, :cond_f9

    #@f8
    move v4, v5

    #@f9
    :cond_f9
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@fc
    goto/16 :goto_9

    #@fe
    .line 164
    .end local v2           #_result:Z
    :sswitch_fe
    const-string v4, "android.os.IUserManager"

    #@100
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@103
    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v0

    #@107
    .line 167
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->wipeUser(I)V

    #@10a
    .line 168
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10d
    goto/16 :goto_9

    #@10f
    .line 173
    .end local v0           #_arg0:I
    :sswitch_10f
    const-string v4, "android.os.IUserManager"

    #@111
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@114
    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@117
    move-result v0

    #@118
    .line 176
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->getUserSerialNumber(I)I

    #@11b
    move-result v2

    #@11c
    .line 177
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@11f
    .line 178
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@122
    goto/16 :goto_9

    #@124
    .line 183
    .end local v0           #_arg0:I
    .end local v2           #_result:I
    :sswitch_124
    const-string v4, "android.os.IUserManager"

    #@126
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@129
    .line 185
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12c
    move-result v0

    #@12d
    .line 186
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IUserManager$Stub;->getUserHandle(I)I

    #@130
    move-result v2

    #@131
    .line 187
    .restart local v2       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@134
    .line 188
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@137
    goto/16 :goto_9

    #@139
    .line 41
    nop

    #@13a
    :sswitch_data_13a
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_31
        0x3 -> :sswitch_48
        0x4 -> :sswitch_5c
        0x5 -> :sswitch_7c
        0x6 -> :sswitch_9b
        0x7 -> :sswitch_b5
        0x8 -> :sswitch_d4
        0x9 -> :sswitch_ea
        0xa -> :sswitch_fe
        0xb -> :sswitch_10f
        0xc -> :sswitch_124
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
