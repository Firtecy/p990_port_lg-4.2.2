.class public final Landroid/os/ParcelUuid;
.super Ljava/lang/Object;
.source "ParcelUuid.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/ParcelUuid;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mUuid:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 110
    new-instance v0, Landroid/os/ParcelUuid$1;

    #@2
    invoke-direct {v0}, Landroid/os/ParcelUuid$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/ParcelUuid;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;)V
    .registers 2
    .parameter "uuid"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    iput-object p1, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@5
    .line 38
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;
    .registers 3
    .parameter "uuid"

    #@0
    .prologue
    .line 52
    new-instance v0, Landroid/os/ParcelUuid;

    #@2
    invoke-static {p0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@9
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 125
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 93
    if-nez p1, :cond_4

    #@3
    .line 107
    :cond_3
    :goto_3
    return v1

    #@4
    .line 97
    :cond_4
    if-ne p0, p1, :cond_8

    #@6
    .line 98
    const/4 v1, 0x1

    #@7
    goto :goto_3

    #@8
    .line 101
    :cond_8
    instance-of v2, p1, Landroid/os/ParcelUuid;

    #@a
    if-eqz v2, :cond_3

    #@c
    move-object v0, p1

    #@d
    .line 105
    check-cast v0, Landroid/os/ParcelUuid;

    #@f
    .line 107
    .local v0, that:Landroid/os/ParcelUuid;
    iget-object v1, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@11
    iget-object v2, v0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@13
    invoke-virtual {v1, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    goto :goto_3
.end method

.method public getUuid()Ljava/util/UUID;
    .registers 2

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@2
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@2
    invoke-virtual {v0}, Ljava/util/UUID;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@2
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@2
    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    #@5
    move-result-wide v0

    #@6
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@9
    .line 130
    iget-object v0, p0, Landroid/os/ParcelUuid;->mUuid:Ljava/util/UUID;

    #@b
    invoke-virtual {v0}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@e
    move-result-wide v0

    #@f
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@12
    .line 131
    return-void
.end method
