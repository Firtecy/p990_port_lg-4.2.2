.class public final Landroid/os/UserHandle;
.super Ljava/lang/Object;
.source "UserHandle.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ALL:Landroid/os/UserHandle; = null

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/UserHandle;",
            ">;"
        }
    .end annotation
.end field

.field public static final CURRENT:Landroid/os/UserHandle; = null

.field public static final CURRENT_OR_SELF:Landroid/os/UserHandle; = null

.field public static final MU_ENABLED:Z = true

.field public static final OWNER:Landroid/os/UserHandle; = null

.field public static final PER_USER_RANGE:I = 0x186a0

.field public static final USER_ALL:I = -0x1

.field public static final USER_CURRENT:I = -0x2

.field public static final USER_CURRENT_OR_SELF:I = -0x3

.field public static final USER_NULL:I = -0x2710

.field public static final USER_OWNER:I


# instance fields
.field final mHandle:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 32
    new-instance v0, Landroid/os/UserHandle;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@6
    sput-object v0, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@8
    .line 38
    new-instance v0, Landroid/os/UserHandle;

    #@a
    const/4 v1, -0x2

    #@b
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@e
    sput-object v0, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@10
    .line 48
    new-instance v0, Landroid/os/UserHandle;

    #@12
    const/4 v1, -0x3

    #@13
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@16
    sput-object v0, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    #@18
    .line 57
    new-instance v0, Landroid/os/UserHandle;

    #@1a
    const/4 v1, 0x0

    #@1b
    invoke-direct {v0, v1}, Landroid/os/UserHandle;-><init>(I)V

    #@1e
    sput-object v0, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    #@20
    .line 236
    new-instance v0, Landroid/os/UserHandle$1;

    #@22
    invoke-direct {v0}, Landroid/os/UserHandle$1;-><init>()V

    #@25
    sput-object v0, Landroid/os/UserHandle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@27
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 164
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 165
    iput p1, p0, Landroid/os/UserHandle;->mHandle:I

    #@5
    .line 166
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 257
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/os/UserHandle;->mHandle:I

    #@9
    .line 259
    return-void
.end method

.method public static final getAppId(I)I
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 142
    const v0, 0x186a0

    #@3
    rem-int v0, p0, v0

    #@5
    return v0
.end method

.method public static final getCallingUserId()I
    .registers 1

    #@0
    .prologue
    .line 122
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static final getSharedAppGid(I)I
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 150
    const v0, 0xc350

    #@3
    const v1, 0x186a0

    #@6
    rem-int v1, p0, v1

    #@8
    add-int/2addr v0, v1

    #@9
    add-int/lit16 v0, v0, -0x2710

    #@b
    return v0
.end method

.method public static final getUid(II)I
    .registers 4
    .parameter "userId"
    .parameter "appId"

    #@0
    .prologue
    const v1, 0x186a0

    #@3
    .line 131
    mul-int v0, p0, v1

    #@5
    rem-int v1, p1, v1

    #@7
    add-int/2addr v0, v1

    #@8
    return v0
.end method

.method public static final getUserId(I)I
    .registers 2
    .parameter "uid"

    #@0
    .prologue
    .line 114
    const v0, 0x186a0

    #@3
    div-int v0, p0, v0

    #@5
    return v0
.end method

.method public static isApp(I)Z
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 100
    if-lez p0, :cond_10

    #@3
    .line 101
    invoke-static {p0}, Landroid/os/UserHandle;->getAppId(I)I

    #@6
    move-result v0

    #@7
    .line 102
    .local v0, appId:I
    const/16 v2, 0x2710

    #@9
    if-lt v0, v2, :cond_10

    #@b
    const/16 v2, 0x4e1f

    #@d
    if-gt v0, v2, :cond_10

    #@f
    const/4 v1, 0x1

    #@10
    .line 104
    .end local v0           #appId:I
    :cond_10
    return v1
.end method

.method public static final isIsolated(I)Z
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 90
    if-lez p0, :cond_12

    #@3
    .line 91
    invoke-static {p0}, Landroid/os/UserHandle;->getAppId(I)I

    #@6
    move-result v0

    #@7
    .line 92
    .local v0, appId:I
    const v2, 0x182b8

    #@a
    if-lt v0, v2, :cond_12

    #@c
    const v2, 0x1869f

    #@f
    if-gt v0, v2, :cond_12

    #@11
    const/4 v1, 0x1

    #@12
    .line 94
    .end local v0           #appId:I
    :cond_12
    return v1
.end method

.method public static final isSameApp(II)Z
    .registers 4
    .parameter "uid1"
    .parameter "uid2"

    #@0
    .prologue
    .line 85
    invoke-static {p0}, Landroid/os/UserHandle;->getAppId(I)I

    #@3
    move-result v0

    #@4
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static final isSameUser(II)Z
    .registers 4
    .parameter "uid1"
    .parameter "uid2"

    #@0
    .prologue
    .line 73
    invoke-static {p0}, Landroid/os/UserHandle;->getUserId(I)I

    #@3
    move-result v0

    #@4
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public static final myUserId()I
    .registers 1

    #@0
    .prologue
    .line 160
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Landroid/os/UserHandle;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 232
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 233
    .local v0, h:I
    const/16 v1, -0x2710

    #@6
    if-eq v0, v1, :cond_e

    #@8
    new-instance v1, Landroid/os/UserHandle;

    #@a
    invoke-direct {v1, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@d
    :goto_d
    return-object v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method public static writeToParcel(Landroid/os/UserHandle;Landroid/os/Parcel;)V
    .registers 3
    .parameter "h"
    .parameter "out"

    #@0
    .prologue
    .line 213
    if-eqz p0, :cond_7

    #@2
    .line 214
    const/4 v0, 0x0

    #@3
    invoke-virtual {p0, p1, v0}, Landroid/os/UserHandle;->writeToParcel(Landroid/os/Parcel;I)V

    #@6
    .line 218
    :goto_6
    return-void

    #@7
    .line 216
    :cond_7
    const/16 v0, -0x2710

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    goto :goto_6
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 196
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "obj"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 181
    if-eqz p1, :cond_e

    #@3
    .line 182
    :try_start_3
    move-object v0, p1

    #@4
    check-cast v0, Landroid/os/UserHandle;

    #@6
    move-object v1, v0

    #@7
    .line 183
    .local v1, other:Landroid/os/UserHandle;
    iget v3, p0, Landroid/os/UserHandle;->mHandle:I

    #@9
    iget v4, v1, Landroid/os/UserHandle;->mHandle:I
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_b} :catch_f

    #@b
    if-ne v3, v4, :cond_e

    #@d
    const/4 v2, 0x1

    #@e
    .line 187
    .end local v1           #other:Landroid/os/UserHandle;
    :cond_e
    :goto_e
    return v2

    #@f
    .line 185
    :catch_f
    move-exception v3

    #@10
    goto :goto_e
.end method

.method public getIdentifier()I
    .registers 2

    #@0
    .prologue
    .line 170
    iget v0, p0, Landroid/os/UserHandle;->mHandle:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 192
    iget v0, p0, Landroid/os/UserHandle;->mHandle:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "UserHandle{"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/os/UserHandle;->mHandle:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string/jumbo v1, "}"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 200
    iget v0, p0, Landroid/os/UserHandle;->mHandle:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 201
    return-void
.end method
