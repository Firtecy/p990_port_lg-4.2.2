.class public final Landroid/os/Bundle;
.super Ljava/lang/Object;
.source "Bundle.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public static final EMPTY:Landroid/os/Bundle; = null

.field private static final LOG_TAG:Ljava/lang/String; = "Bundle"


# instance fields
.field private mAllowFds:Z

.field private mClassLoader:Ljava/lang/ClassLoader;

.field private mFdsKnown:Z

.field private mHasFds:Z

.field mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mParcelledData:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 39
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@5
    sput-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    #@7
    .line 40
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    #@9
    new-instance v1, Ljava/util/HashMap;

    #@b
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@e
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    #@11
    move-result-object v1

    #@12
    iput-object v1, v0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@14
    .line 1587
    new-instance v0, Landroid/os/Bundle$1;

    #@16
    invoke-direct {v0}, Landroid/os/Bundle$1;-><init>()V

    #@19
    sput-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1b
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 46
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@7
    .line 53
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    .line 55
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v1, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 68
    new-instance v0, Ljava/util/HashMap;

    #@12
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@15
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@17
    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@21
    .line 70
    return-void
.end method

.method public constructor <init>(I)V
    .registers 4
    .parameter "capacity"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 104
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 46
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@7
    .line 53
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    .line 55
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v1, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 105
    new-instance v0, Ljava/util/HashMap;

    #@12
    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    #@15
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@17
    .line 106
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@21
    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .registers 7
    .parameter "b"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 46
    iput-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@8
    .line 53
    iput-object v3, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@a
    .line 55
    iput-boolean v4, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v0, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 116
    iget-object v0, p1, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@12
    if-eqz v0, :cond_46

    #@14
    .line 117
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@17
    move-result-object v0

    #@18
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@1a
    .line 118
    iget-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@1c
    iget-object v1, p1, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@1e
    iget-object v2, p1, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@20
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    #@23
    move-result v2

    #@24
    invoke-virtual {v0, v1, v4, v2}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@27
    .line 119
    iget-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@29
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    #@2c
    .line 124
    :goto_2c
    iget-object v0, p1, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@2e
    if-eqz v0, :cond_49

    #@30
    .line 125
    new-instance v0, Ljava/util/HashMap;

    #@32
    iget-object v1, p1, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@34
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    #@37
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@39
    .line 130
    :goto_39
    iget-boolean v0, p1, Landroid/os/Bundle;->mHasFds:Z

    #@3b
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@3d
    .line 131
    iget-boolean v0, p1, Landroid/os/Bundle;->mFdsKnown:Z

    #@3f
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@41
    .line 132
    iget-object v0, p1, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@43
    iput-object v0, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@45
    .line 133
    return-void

    #@46
    .line 121
    :cond_46
    iput-object v3, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@48
    goto :goto_2c

    #@49
    .line 127
    :cond_49
    iput-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@4b
    goto :goto_39
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcelledData"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 46
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@7
    .line 53
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    .line 55
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v1, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 79
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->readFromParcel(Landroid/os/Parcel;)V

    #@13
    .line 80
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcelledData"
    .parameter "length"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 46
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@7
    .line 53
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    .line 55
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v1, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 83
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->readFromParcelInner(Landroid/os/Parcel;I)V

    #@13
    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/ClassLoader;)V
    .registers 4
    .parameter "loader"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 46
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@7
    .line 53
    iput-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    .line 55
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@c
    .line 56
    iput-boolean v1, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 57
    iput-boolean v1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@10
    .line 94
    new-instance v0, Ljava/util/HashMap;

    #@12
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@15
    iput-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@17
    .line 95
    iput-object p1, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@19
    .line 96
    return-void
.end method

.method public static forPair(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 142
    new-instance v0, Landroid/os/Bundle;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    #@6
    .line 143
    .local v0, b:Landroid/os/Bundle;
    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 144
    return-object v0
.end method

.method private typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V
    .registers 11
    .parameter "key"
    .parameter "value"
    .parameter "className"
    .parameter "e"

    #@0
    .prologue
    .line 791
    const-string v4, "<null>"

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move-object v5, p4

    #@7
    invoke-direct/range {v0 .. v5}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@a
    .line 792
    return-void
.end method

.method private typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V
    .registers 9
    .parameter "key"
    .parameter "value"
    .parameter "className"
    .parameter "defaultValue"
    .parameter "e"

    #@0
    .prologue
    .line 775
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 776
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "Key "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 777
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 778
    const-string v1, " expected "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 779
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 780
    const-string v1, " but value was a "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 781
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 782
    const-string v1, ".  The default value "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 783
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    .line 784
    const-string v1, " was returned."

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 785
    const-string v1, "Bundle"

    #@34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 786
    const-string v1, "Bundle"

    #@3d
    const-string v2, "Attempt to cast generated internal exception:"

    #@3f
    invoke-static {v1, v2, p5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@42
    .line 787
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 257
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 258
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@8
    .line 259
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@b
    .line 260
    const/4 v0, 0x1

    #@c
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@e
    .line 261
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 204
    new-instance v0, Landroid/os/Bundle;

    #@2
    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@5
    return-object v0
.end method

.method public containsKey(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 272
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public describeContents()I
    .registers 3

    #@0
    .prologue
    .line 1602
    const/4 v0, 0x0

    #@1
    .line 1603
    .local v0, mask:I
    invoke-virtual {p0}, Landroid/os/Bundle;->hasFileDescriptors()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_9

    #@7
    .line 1604
    or-int/lit8 v0, v0, 0x1

    #@9
    .line 1606
    :cond_9
    return v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 282
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 283
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 768
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 769
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .registers 10
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 803
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 804
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 805
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 812
    .end local p2
    :goto_b
    return p2

    #@c
    .line 809
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Boolean;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result p2

    #@14
    goto :goto_b

    #@15
    .line 810
    :catch_15
    move-exception v6

    #@16
    .line 811
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Boolean"

    #@18
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getBooleanArray(Ljava/lang/String;)[Z
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1350
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1351
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1352
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1359
    :goto_d
    return-object v3

    #@e
    .line 1356
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [Z

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [Z
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1357
    :catch_15
    move-exception v1

    #@16
    .line 1358
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "byte[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1359
    goto :goto_d
.end method

.method public getBundle(Ljava/lang/String;)Landroid/os/Bundle;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1151
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1152
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1153
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1160
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1157
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Landroid/os/Bundle;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1158
    :catch_11
    move-exception v0

    #@12
    .line 1159
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "Bundle"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1160
    goto :goto_d
.end method

.method public getByte(Ljava/lang/String;)B
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 824
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 825
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getByte(Ljava/lang/String;B)Ljava/lang/Byte;
    .registers 9
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 837
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 838
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    .line 839
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_10

    #@b
    .line 840
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@e
    move-result-object v2

    #@f
    .line 846
    .end local v2           #o:Ljava/lang/Object;
    :goto_f
    return-object v2

    #@10
    .line 843
    .restart local v2       #o:Ljava/lang/Object;
    :cond_10
    :try_start_10
    check-cast v2, Ljava/lang/Byte;
    :try_end_12
    .catch Ljava/lang/ClassCastException; {:try_start_10 .. :try_end_12} :catch_13

    #@12
    goto :goto_f

    #@13
    .line 844
    :catch_13
    move-exception v5

    #@14
    .line 845
    .local v5, e:Ljava/lang/ClassCastException;
    const-string v3, "Byte"

    #@16
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@19
    move-result-object v4

    #@1a
    move-object v0, p0

    #@1b
    move-object v1, p1

    #@1c
    invoke-direct/range {v0 .. v5}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@1f
    .line 846
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@22
    move-result-object v2

    #@23
    goto :goto_f
.end method

.method public getByteArray(Ljava/lang/String;)[B
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1372
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1373
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1374
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1381
    :goto_d
    return-object v3

    #@e
    .line 1378
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [B

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [B
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1379
    :catch_15
    move-exception v1

    #@16
    .line 1380
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "byte[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1381
    goto :goto_d
.end method

.method public getChar(Ljava/lang/String;)C
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 858
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 859
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getChar(Ljava/lang/String;C)C

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getChar(Ljava/lang/String;C)C
    .registers 10
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 871
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 872
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 873
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 880
    .end local p2
    :goto_b
    return p2

    #@c
    .line 877
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Character;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result p2

    #@14
    goto :goto_b

    #@15
    .line 878
    :catch_15
    move-exception v6

    #@16
    .line 879
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Character"

    #@18
    invoke-static {p2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getCharArray(Ljava/lang/String;)[C
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1416
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1417
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1418
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1425
    :goto_d
    return-object v3

    #@e
    .line 1422
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [C

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [C
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1423
    :catch_15
    move-exception v1

    #@16
    .line 1424
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "char[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1425
    goto :goto_d
.end method

.method public getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1107
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1108
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1109
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1116
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1113
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/lang/CharSequence;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1114
    :catch_11
    move-exception v0

    #@12
    .line 1115
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "CharSequence"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1116
    goto :goto_d
.end method

.method public getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 6
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1129
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 1130
    iget-object v2, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    .line 1131
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_c

    #@b
    .line 1138
    .end local v1           #o:Ljava/lang/Object;
    .end local p2
    :goto_b
    return-object p2

    #@c
    .line 1135
    .restart local v1       #o:Ljava/lang/Object;
    .restart local p2
    :cond_c
    :try_start_c
    check-cast v1, Ljava/lang/CharSequence;
    :try_end_e
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_e} :catch_10

    #@e
    .end local v1           #o:Ljava/lang/Object;
    move-object p2, v1

    #@f
    goto :goto_b

    #@10
    .line 1136
    .restart local v1       #o:Ljava/lang/Object;
    :catch_10
    move-exception v0

    #@11
    .line 1137
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v2, "CharSequence"

    #@13
    invoke-direct {p0, p1, v1, v2, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@16
    goto :goto_b
.end method

.method public getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1548
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1549
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1550
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1557
    :goto_d
    return-object v3

    #@e
    .line 1554
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [Ljava/lang/CharSequence;

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [Ljava/lang/CharSequence;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1555
    :catch_15
    move-exception v1

    #@16
    .line 1556
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "CharSequence[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1557
    goto :goto_d
.end method

.method public getCharSequenceArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1328
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1329
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1330
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1337
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1334
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/util/ArrayList;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1335
    :catch_11
    move-exception v0

    #@12
    .line 1336
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "ArrayList<CharSequence>"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1337
    goto :goto_d
.end method

.method public getClassLoader()Ljava/lang/ClassLoader;
    .registers 2

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@2
    return-object v0
.end method

.method public getDouble(Ljava/lang/String;)D
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 1028
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 1029
    const-wide/16 v0, 0x0

    #@5
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getDouble(Ljava/lang/String;D)D
    .registers 11
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1041
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 1042
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 1043
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 1050
    .end local p2
    :goto_b
    return-wide p2

    #@c
    .line 1047
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Double;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result-wide p2

    #@14
    goto :goto_b

    #@15
    .line 1048
    :catch_15
    move-exception v6

    #@16
    .line 1049
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Double"

    #@18
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getDoubleArray(Ljava/lang/String;)[D
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1504
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1505
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1506
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1513
    :goto_d
    return-object v3

    #@e
    .line 1510
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [D

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [D
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1511
    :catch_15
    move-exception v1

    #@16
    .line 1512
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "double[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1513
    goto :goto_d
.end method

.method public getFloat(Ljava/lang/String;)F
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 994
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 995
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getFloat(Ljava/lang/String;F)F
    .registers 10
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1007
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 1008
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 1009
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 1016
    .end local p2
    :goto_b
    return p2

    #@c
    .line 1013
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Float;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result p2

    #@14
    goto :goto_b

    #@15
    .line 1014
    :catch_15
    move-exception v6

    #@16
    .line 1015
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Float"

    #@18
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getFloatArray(Ljava/lang/String;)[F
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1482
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1483
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1484
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1491
    :goto_d
    return-object v3

    #@e
    .line 1488
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [F

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [F
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1489
    :catch_15
    move-exception v1

    #@16
    .line 1490
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "float[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1491
    goto :goto_d
.end method

.method public getIBinder(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 6
    .parameter "key"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1574
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1575
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1576
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1583
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1580
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Landroid/os/IBinder;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1581
    :catch_11
    move-exception v0

    #@12
    .line 1582
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "IBinder"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1583
    goto :goto_d
.end method

.method public getInt(Ljava/lang/String;)I
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 926
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 927
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getInt(Ljava/lang/String;I)I
    .registers 10
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 939
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 940
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 941
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 948
    .end local p2
    :goto_b
    return p2

    #@c
    .line 945
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Integer;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result p2

    #@14
    goto :goto_b

    #@15
    .line 946
    :catch_15
    move-exception v6

    #@16
    .line 947
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Integer"

    #@18
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getIntArray(Ljava/lang/String;)[I
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1438
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1439
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1440
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1447
    :goto_d
    return-object v3

    #@e
    .line 1444
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [I

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [I
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1445
    :catch_15
    move-exception v1

    #@16
    .line 1446
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "int[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1447
    goto :goto_d
.end method

.method public getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1284
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1285
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1286
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1293
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1290
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/util/ArrayList;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1291
    :catch_11
    move-exception v0

    #@12
    .line 1292
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "ArrayList<Integer>"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1293
    goto :goto_d
.end method

.method public getLong(Ljava/lang/String;)J
    .registers 4
    .parameter "key"

    #@0
    .prologue
    .line 960
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 961
    const-wide/16 v0, 0x0

    #@5
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@8
    move-result-wide v0

    #@9
    return-wide v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .registers 11
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 973
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 974
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 975
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 982
    .end local p2
    :goto_b
    return-wide p2

    #@c
    .line 979
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Long;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result-wide p2

    #@14
    goto :goto_b

    #@15
    .line 980
    :catch_15
    move-exception v6

    #@16
    .line 981
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Long"

    #@18
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getLongArray(Ljava/lang/String;)[J
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1460
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1461
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1462
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1469
    :goto_d
    return-object v3

    #@e
    .line 1466
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [J

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [J
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1467
    :catch_15
    move-exception v1

    #@16
    .line 1468
    .local v1, e:Ljava/lang/ClassCastException;
    const-string/jumbo v3, "long[]"

    #@19
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1c
    move-object v3, v4

    #@1d
    .line 1469
    goto :goto_d
.end method

.method public getPairValue()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 157
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 158
    iget-object v4, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v4}, Ljava/util/Map;->size()I

    #@9
    move-result v2

    #@a
    .line 159
    .local v2, size:I
    const/4 v4, 0x1

    #@b
    if-le v2, v4, :cond_14

    #@d
    .line 160
    const-string v4, "Bundle"

    #@f
    const-string v5, "getPairValue() used on Bundle with multiple pairs."

    #@11
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 162
    :cond_14
    if-nez v2, :cond_18

    #@16
    move-object v1, v3

    #@17
    .line 170
    :goto_17
    return-object v1

    #@18
    .line 165
    :cond_18
    iget-object v4, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@1a
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    #@1d
    move-result-object v4

    #@1e
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@21
    move-result-object v4

    #@22
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@25
    move-result-object v1

    #@26
    .line 167
    .local v1, o:Ljava/lang/Object;
    :try_start_26
    check-cast v1, Ljava/lang/String;
    :try_end_28
    .catch Ljava/lang/ClassCastException; {:try_start_26 .. :try_end_28} :catch_29

    #@28
    goto :goto_17

    #@29
    .line 168
    :catch_29
    move-exception v0

    #@2a
    .line 169
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v4, "getPairValue()"

    #@2c
    const-string v5, "String"

    #@2e
    invoke-direct {p0, v4, v1, v5, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@31
    move-object v1, v3

    #@32
    .line 170
    goto :goto_17
.end method

.method public getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1173
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1174
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1175
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1182
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1179
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Landroid/os/Parcelable;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1180
    :catch_11
    move-exception v0

    #@12
    .line 1181
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "Parcelable"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1182
    goto :goto_d
.end method

.method public getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1195
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1196
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1197
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1204
    :goto_d
    return-object v3

    #@e
    .line 1201
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [Landroid/os/Parcelable;

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [Landroid/os/Parcelable;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1202
    :catch_15
    move-exception v1

    #@16
    .line 1203
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "Parcelable[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1204
    goto :goto_d
.end method

.method public getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1217
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1218
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1219
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1226
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1223
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/util/ArrayList;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1224
    :catch_11
    move-exception v0

    #@12
    .line 1225
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "ArrayList"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1226
    goto :goto_d
.end method

.method public getSerializable(Ljava/lang/String;)Ljava/io/Serializable;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1262
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1263
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1264
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1271
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1268
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/io/Serializable;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1269
    :catch_11
    move-exception v0

    #@12
    .line 1270
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "Serializable"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1271
    goto :goto_d
.end method

.method public getShort(Ljava/lang/String;)S
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 892
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 893
    const/4 v0, 0x0

    #@4
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->getShort(Ljava/lang/String;S)S

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getShort(Ljava/lang/String;S)S
    .registers 10
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 905
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 906
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v3

    #@9
    .line 907
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_c

    #@b
    .line 914
    .end local p2
    :goto_b
    return p2

    #@c
    .line 911
    .restart local p2
    :cond_c
    :try_start_c
    move-object v0, v3

    #@d
    check-cast v0, Ljava/lang/Short;

    #@f
    move-object v1, v0

    #@10
    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_13} :catch_15

    #@13
    move-result p2

    #@14
    goto :goto_b

    #@15
    .line 912
    :catch_15
    move-exception v6

    #@16
    .line 913
    .local v6, e:Ljava/lang/ClassCastException;
    const-string v4, "Short"

    #@18
    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@1b
    move-result-object v5

    #@1c
    move-object v1, p0

    #@1d
    move-object v2, p1

    #@1e
    invoke-direct/range {v1 .. v6}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassCastException;)V

    #@21
    goto :goto_b
.end method

.method public getShortArray(Ljava/lang/String;)[S
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1394
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1395
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1396
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1403
    :goto_d
    return-object v3

    #@e
    .line 1400
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [S

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [S
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1401
    :catch_15
    move-exception v1

    #@16
    .line 1402
    .local v1, e:Ljava/lang/ClassCastException;
    const-string/jumbo v3, "short[]"

    #@19
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1c
    move-object v3, v4

    #@1d
    .line 1403
    goto :goto_d
.end method

.method public getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<TT;>;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1240
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1241
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1242
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1249
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1246
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Landroid/util/SparseArray;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1247
    :catch_11
    move-exception v0

    #@12
    .line 1248
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "SparseArray"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1249
    goto :goto_d
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "key"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1063
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1064
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1065
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1072
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1069
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/lang/String;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1070
    :catch_11
    move-exception v0

    #@12
    .line 1071
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "String"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1072
    goto :goto_d
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 1085
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 1086
    iget-object v2, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    .line 1087
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_c

    #@b
    .line 1094
    .end local v1           #o:Ljava/lang/Object;
    .end local p2
    :goto_b
    return-object p2

    #@c
    .line 1091
    .restart local v1       #o:Ljava/lang/Object;
    .restart local p2
    :cond_c
    :try_start_c
    check-cast v1, Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/ClassCastException; {:try_start_c .. :try_end_e} :catch_10

    #@e
    .end local v1           #o:Ljava/lang/Object;
    move-object p2, v1

    #@f
    goto :goto_b

    #@10
    .line 1092
    .restart local v1       #o:Ljava/lang/Object;
    :catch_10
    move-exception v0

    #@11
    .line 1093
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v2, "String"

    #@13
    invoke-direct {p0, p1, v1, v2, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@16
    goto :goto_b
.end method

.method public getStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .registers 7
    .parameter "key"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1526
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1527
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 1528
    .local v2, o:Ljava/lang/Object;
    if-nez v2, :cond_e

    #@c
    move-object v3, v4

    #@d
    .line 1535
    :goto_d
    return-object v3

    #@e
    .line 1532
    :cond_e
    :try_start_e
    move-object v0, v2

    #@f
    check-cast v0, [Ljava/lang/String;

    #@11
    move-object v3, v0

    #@12
    check-cast v3, [Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_14} :catch_15

    #@14
    goto :goto_d

    #@15
    .line 1533
    :catch_15
    move-exception v1

    #@16
    .line 1534
    .local v1, e:Ljava/lang/ClassCastException;
    const-string v3, "String[]"

    #@18
    invoke-direct {p0, p1, v2, v3, v1}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@1b
    move-object v3, v4

    #@1c
    .line 1535
    goto :goto_d
.end method

.method public getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 6
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1306
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@4
    .line 1307
    iget-object v3, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@6
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    .line 1308
    .local v1, o:Ljava/lang/Object;
    if-nez v1, :cond_e

    #@c
    move-object v1, v2

    #@d
    .line 1315
    .end local v1           #o:Ljava/lang/Object;
    :goto_d
    return-object v1

    #@e
    .line 1312
    .restart local v1       #o:Ljava/lang/Object;
    :cond_e
    :try_start_e
    check-cast v1, Ljava/util/ArrayList;
    :try_end_10
    .catch Ljava/lang/ClassCastException; {:try_start_e .. :try_end_10} :catch_11

    #@10
    goto :goto_d

    #@11
    .line 1313
    :catch_11
    move-exception v0

    #@12
    .line 1314
    .local v0, e:Ljava/lang/ClassCastException;
    const-string v3, "ArrayList<String>"

    #@14
    invoke-direct {p0, p1, v1, v3, v0}, Landroid/os/Bundle;->typeWarning(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/ClassCastException;)V

    #@17
    move-object v1, v2

    #@18
    .line 1315
    goto :goto_d
.end method

.method public hasFileDescriptors()Z
    .registers 9

    #@0
    .prologue
    .line 325
    iget-boolean v7, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@2
    if-nez v7, :cond_17

    #@4
    .line 326
    const/4 v2, 0x0

    #@5
    .line 328
    .local v2, fdFound:Z
    iget-object v7, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@7
    if-eqz v7, :cond_1a

    #@9
    .line 329
    iget-object v7, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@b
    invoke-virtual {v7}, Landroid/os/Parcel;->hasFileDescriptors()Z

    #@e
    move-result v7

    #@f
    if-eqz v7, :cond_12

    #@11
    .line 330
    const/4 v2, 0x1

    #@12
    .line 381
    :cond_12
    :goto_12
    iput-boolean v2, p0, Landroid/os/Bundle;->mHasFds:Z

    #@14
    .line 382
    const/4 v7, 0x1

    #@15
    iput-boolean v7, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@17
    .line 384
    .end local v2           #fdFound:Z
    :cond_17
    iget-boolean v7, p0, Landroid/os/Bundle;->mHasFds:Z

    #@19
    return v7

    #@1a
    .line 334
    .restart local v2       #fdFound:Z
    :cond_1a
    iget-object v7, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@1c
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@1f
    move-result-object v7

    #@20
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v3

    #@24
    .line 335
    .local v3, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :cond_24
    :goto_24
    if-nez v2, :cond_12

    #@26
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@29
    move-result v7

    #@2a
    if-eqz v7, :cond_12

    #@2c
    .line 336
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2f
    move-result-object v7

    #@30
    check-cast v7, Ljava/util/Map$Entry;

    #@32
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@35
    move-result-object v5

    #@36
    .line 337
    .local v5, obj:Ljava/lang/Object;
    instance-of v7, v5, Landroid/os/Parcelable;

    #@38
    if-eqz v7, :cond_46

    #@3a
    .line 338
    check-cast v5, Landroid/os/Parcelable;

    #@3c
    .end local v5           #obj:Ljava/lang/Object;
    invoke-interface {v5}, Landroid/os/Parcelable;->describeContents()I

    #@3f
    move-result v7

    #@40
    and-int/lit8 v7, v7, 0x1

    #@42
    if-eqz v7, :cond_24

    #@44
    .line 340
    const/4 v2, 0x1

    #@45
    .line 341
    goto :goto_12

    #@46
    .line 343
    .restart local v5       #obj:Ljava/lang/Object;
    :cond_46
    instance-of v7, v5, [Landroid/os/Parcelable;

    #@48
    if-eqz v7, :cond_63

    #@4a
    .line 344
    check-cast v5, [Landroid/os/Parcelable;

    #@4c
    .end local v5           #obj:Ljava/lang/Object;
    move-object v0, v5

    #@4d
    check-cast v0, [Landroid/os/Parcelable;

    #@4f
    .line 345
    .local v0, array:[Landroid/os/Parcelable;
    array-length v7, v0

    #@50
    add-int/lit8 v4, v7, -0x1

    #@52
    .local v4, n:I
    :goto_52
    if-ltz v4, :cond_24

    #@54
    .line 346
    aget-object v7, v0, v4

    #@56
    invoke-interface {v7}, Landroid/os/Parcelable;->describeContents()I

    #@59
    move-result v7

    #@5a
    and-int/lit8 v7, v7, 0x1

    #@5c
    if-eqz v7, :cond_60

    #@5e
    .line 348
    const/4 v2, 0x1

    #@5f
    .line 349
    goto :goto_24

    #@60
    .line 345
    :cond_60
    add-int/lit8 v4, v4, -0x1

    #@62
    goto :goto_52

    #@63
    .line 352
    .end local v0           #array:[Landroid/os/Parcelable;
    .end local v4           #n:I
    .restart local v5       #obj:Ljava/lang/Object;
    :cond_63
    instance-of v7, v5, Landroid/util/SparseArray;

    #@65
    if-eqz v7, :cond_85

    #@67
    move-object v1, v5

    #@68
    .line 353
    check-cast v1, Landroid/util/SparseArray;

    #@6a
    .line 355
    .local v1, array:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/Parcelable;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    #@6d
    move-result v7

    #@6e
    add-int/lit8 v4, v7, -0x1

    #@70
    .restart local v4       #n:I
    :goto_70
    if-ltz v4, :cond_24

    #@72
    .line 356
    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@75
    move-result-object v7

    #@76
    check-cast v7, Landroid/os/Parcelable;

    #@78
    invoke-interface {v7}, Landroid/os/Parcelable;->describeContents()I

    #@7b
    move-result v7

    #@7c
    and-int/lit8 v7, v7, 0x1

    #@7e
    if-eqz v7, :cond_82

    #@80
    .line 358
    const/4 v2, 0x1

    #@81
    .line 359
    goto :goto_24

    #@82
    .line 355
    :cond_82
    add-int/lit8 v4, v4, -0x1

    #@84
    goto :goto_70

    #@85
    .line 362
    .end local v1           #array:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/Parcelable;>;"
    .end local v4           #n:I
    :cond_85
    instance-of v7, v5, Ljava/util/ArrayList;

    #@87
    if-eqz v7, :cond_24

    #@89
    move-object v0, v5

    #@8a
    .line 363
    check-cast v0, Ljava/util/ArrayList;

    #@8c
    .line 366
    .local v0, array:Ljava/util/ArrayList;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8f
    move-result v7

    #@90
    if-lez v7, :cond_24

    #@92
    const/4 v7, 0x0

    #@93
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@96
    move-result-object v7

    #@97
    instance-of v7, v7, Landroid/os/Parcelable;

    #@99
    if-eqz v7, :cond_24

    #@9b
    .line 368
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@9e
    move-result v7

    #@9f
    add-int/lit8 v4, v7, -0x1

    #@a1
    .restart local v4       #n:I
    :goto_a1
    if-ltz v4, :cond_24

    #@a3
    .line 369
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a6
    move-result-object v6

    #@a7
    check-cast v6, Landroid/os/Parcelable;

    #@a9
    .line 370
    .local v6, p:Landroid/os/Parcelable;
    if-eqz v6, :cond_b6

    #@ab
    invoke-interface {v6}, Landroid/os/Parcelable;->describeContents()I

    #@ae
    move-result v7

    #@af
    and-int/lit8 v7, v7, 0x1

    #@b1
    if-eqz v7, :cond_b6

    #@b3
    .line 372
    const/4 v2, 0x1

    #@b4
    .line 373
    goto/16 :goto_24

    #@b6
    .line 368
    :cond_b6
    add-int/lit8 v4, v4, -0x1

    #@b8
    goto :goto_a1
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 250
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public isParcelled()Z
    .registers 2

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public keySet()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 317
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 318
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public putAll(Landroid/os/Bundle;)V
    .registers 4
    .parameter "map"

    #@0
    .prologue
    .line 302
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 303
    invoke-virtual {p1}, Landroid/os/Bundle;->unparcel()V

    #@6
    .line 304
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@8
    iget-object v1, p1, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@a
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    #@d
    .line 307
    iget-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@f
    iget-boolean v1, p1, Landroid/os/Bundle;->mHasFds:Z

    #@11
    or-int/2addr v0, v1

    #@12
    iput-boolean v0, p0, Landroid/os/Bundle;->mHasFds:Z

    #@14
    .line 308
    iget-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@16
    if-eqz v0, :cond_20

    #@18
    iget-boolean v0, p1, Landroid/os/Bundle;->mFdsKnown:Z

    #@1a
    if-eqz v0, :cond_20

    #@1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@1f
    .line 309
    return-void

    #@20
    .line 308
    :cond_20
    const/4 v0, 0x0

    #@21
    goto :goto_1d
.end method

.method public putBoolean(Ljava/lang/String;Z)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 395
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 396
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 397
    return-void
.end method

.method public putBooleanArray(Ljava/lang/String;[Z)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 620
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 621
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 622
    return-void
.end method

.method public putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 740
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 741
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 742
    return-void
.end method

.method public putByte(Ljava/lang/String;B)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 407
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 408
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 409
    return-void
.end method

.method public putByteArray(Ljava/lang/String;[B)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 632
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 633
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 634
    return-void
.end method

.method public putChar(Ljava/lang/String;C)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 419
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 420
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 421
    return-void
.end method

.method public putCharArray(Ljava/lang/String;[C)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 656
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 657
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 658
    return-void
.end method

.method public putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 503
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 504
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 505
    return-void
.end method

.method public putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 728
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 729
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 730
    return-void
.end method

.method public putCharSequenceArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 596
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/CharSequence;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 597
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 598
    return-void
.end method

.method public putDouble(Ljava/lang/String;D)V
    .registers 6
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 479
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 480
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 481
    return-void
.end method

.method public putDoubleArray(Ljava/lang/String;[D)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 704
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 705
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 706
    return-void
.end method

.method public putFloat(Ljava/lang/String;F)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 467
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 468
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 469
    return-void
.end method

.method public putFloatArray(Ljava/lang/String;[F)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 692
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 693
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 694
    return-void
.end method

.method public putIBinder(Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 4
    .parameter "key"
    .parameter "value"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 756
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 757
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 758
    return-void
.end method

.method public putInt(Ljava/lang/String;I)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 443
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 444
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 445
    return-void
.end method

.method public putIntArray(Ljava/lang/String;[I)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 668
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 669
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 670
    return-void
.end method

.method public putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 572
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 573
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 574
    return-void
.end method

.method public putLong(Ljava/lang/String;J)V
    .registers 6
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 455
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 456
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 457
    return-void
.end method

.method public putLongArray(Ljava/lang/String;[J)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 680
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 681
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 682
    return-void
.end method

.method public putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 515
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 516
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 517
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@b
    .line 518
    return-void
.end method

.method public putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 529
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 530
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 531
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@b
    .line 532
    return-void
.end method

.method public putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<+",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 544
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<+Landroid/os/Parcelable;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 545
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 546
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@b
    .line 547
    return-void
.end method

.method public putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 608
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 609
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 610
    return-void
.end method

.method public putShort(Ljava/lang/String;S)V
    .registers 5
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 431
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 432
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@8
    move-result-object v1

    #@9
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 433
    return-void
.end method

.method public putShortArray(Ljava/lang/String;[S)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 644
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 645
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 646
    return-void
.end method

.method public putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<+",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 559
    .local p2, value:Landroid/util/SparseArray;,"Landroid/util/SparseArray<+Landroid/os/Parcelable;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 560
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 561
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@b
    .line 562
    return-void
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 491
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 492
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 493
    return-void
.end method

.method public putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .registers 4
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 716
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 717
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 718
    return-void
.end method

.method public putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 4
    .parameter "key"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 584
    .local p2, value:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 585
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 586
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "parcel"

    #@0
    .prologue
    .line 1647
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 1648
    .local v0, length:I
    if-gez v0, :cond_1f

    #@6
    .line 1649
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "Bad length in parcel: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v1

    #@1f
    .line 1651
    :cond_1f
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->readFromParcelInner(Landroid/os/Parcel;I)V

    #@22
    .line 1652
    return-void
.end method

.method readFromParcelInner(Landroid/os/Parcel;I)V
    .registers 11
    .parameter "parcel"
    .parameter "length"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1655
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v0

    #@5
    .line 1656
    .local v0, magic:I
    const v4, 0x4c444e42

    #@8
    if-eq v0, v4, :cond_34

    #@a
    .line 1658
    new-instance v4, Ljava/lang/RuntimeException;

    #@c
    invoke-direct {v4}, Ljava/lang/RuntimeException;-><init>()V

    #@f
    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 1659
    .local v3, st:Ljava/lang/String;
    const-string v4, "Bundle"

    #@15
    const-string/jumbo v5, "readBundle: bad magic number"

    #@18
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1660
    const-string v4, "Bundle"

    #@1d
    new-instance v5, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string/jumbo v6, "readBundle: trace = "

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1664
    .end local v3           #st:Ljava/lang/String;
    :cond_34
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@37
    move-result v1

    #@38
    .line 1665
    .local v1, offset:I
    add-int v4, v1, p2

    #@3a
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    #@3d
    .line 1667
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@40
    move-result-object v2

    #@41
    .line 1668
    .local v2, p:Landroid/os/Parcel;
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    #@44
    .line 1669
    invoke-virtual {v2, p1, v1, p2}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V

    #@47
    .line 1670
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    #@4a
    .line 1672
    iput-object v2, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@4c
    .line 1673
    invoke-virtual {v2}, Landroid/os/Parcel;->hasFileDescriptors()Z

    #@4f
    move-result v4

    #@50
    iput-boolean v4, p0, Landroid/os/Bundle;->mHasFds:Z

    #@52
    .line 1674
    const/4 v4, 0x1

    #@53
    iput-boolean v4, p0, Landroid/os/Bundle;->mFdsKnown:Z

    #@55
    .line 1675
    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 292
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 293
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    .line 294
    return-void
.end method

.method public setAllowFds(Z)Z
    .registers 3
    .parameter "allowFds"

    #@0
    .prologue
    .line 193
    iget-boolean v0, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@2
    .line 194
    .local v0, orig:Z
    iput-boolean p1, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@4
    .line 195
    return v0
.end method

.method public setClassLoader(Ljava/lang/ClassLoader;)V
    .registers 2
    .parameter "loader"

    #@0
    .prologue
    .line 181
    iput-object p1, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@2
    .line 182
    return-void
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 241
    invoke-virtual {p0}, Landroid/os/Bundle;->unparcel()V

    #@3
    .line 242
    iget-object v0, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@5
    invoke-interface {v0}, Ljava/util/Map;->size()I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1679
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@3
    if-eqz v0, :cond_26

    #@5
    .line 1680
    new-instance v0, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v1, "Bundle[mParcelledData.dataSize="

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    iget-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@12
    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    #@15
    move-result v1

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v1, "]"

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_46

    #@23
    move-result-object v0

    #@24
    .line 1683
    :goto_24
    monitor-exit p0

    #@25
    return-object v0

    #@26
    :cond_26
    :try_start_26
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v1, "Bundle["

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v0

    #@31
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@33
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, "]"

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_44
    .catchall {:try_start_26 .. :try_end_44} :catchall_46

    #@44
    move-result-object v0

    #@45
    goto :goto_24

    #@46
    .line 1679
    :catchall_46
    move-exception v0

    #@47
    monitor-exit p0

    #@48
    throw v0
.end method

.method declared-synchronized unparcel()V
    .registers 5

    #@0
    .prologue
    .line 212
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2c

    #@3
    if-nez v1, :cond_7

    #@5
    .line 226
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 216
    :cond_7
    :try_start_7
    iget-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@9
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    .line 217
    .local v0, N:I
    if-ltz v0, :cond_5

    #@f
    .line 220
    iget-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@11
    if-nez v1, :cond_1a

    #@13
    .line 221
    new-instance v1, Ljava/util/HashMap;

    #@15
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v1, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@1a
    .line 223
    :cond_1a
    iget-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@1c
    iget-object v2, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@1e
    iget-object v3, p0, Landroid/os/Bundle;->mClassLoader:Ljava/lang/ClassLoader;

    #@20
    invoke-virtual {v1, v2, v0, v3}, Landroid/os/Parcel;->readMapInternal(Ljava/util/Map;ILjava/lang/ClassLoader;)V

    #@23
    .line 224
    iget-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 225
    const/4 v1, 0x0

    #@29
    iput-object v1, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;
    :try_end_2b
    .catchall {:try_start_7 .. :try_end_2b} :catchall_2c

    #@2b
    goto :goto_5

    #@2c
    .line 212
    .end local v0           #N:I
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit p0

    #@2e
    throw v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    .line 1615
    iget-boolean v4, p0, Landroid/os/Bundle;->mAllowFds:Z

    #@2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->pushAllowFds(Z)Z

    #@5
    move-result v2

    #@6
    .line 1617
    .local v2, oldAllowFds:Z
    :try_start_6
    iget-object v4, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@8
    if-eqz v4, :cond_23

    #@a
    .line 1618
    iget-object v4, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@c
    invoke-virtual {v4}, Landroid/os/Parcel;->dataSize()I

    #@f
    move-result v0

    #@10
    .line 1619
    .local v0, length:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 1620
    const v4, 0x4c444e42

    #@16
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 1621
    iget-object v4, p0, Landroid/os/Bundle;->mParcelledData:Landroid/os/Parcel;

    #@1b
    const/4 v5, 0x0

    #@1c
    invoke-virtual {p1, v4, v5, v0}, Landroid/os/Parcel;->appendFrom(Landroid/os/Parcel;II)V
    :try_end_1f
    .catchall {:try_start_6 .. :try_end_1f} :catchall_48

    #@1f
    .line 1637
    :goto_1f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->restoreAllowFds(Z)V

    #@22
    .line 1639
    return-void

    #@23
    .line 1623
    .end local v0           #length:I
    :cond_23
    const/4 v4, -0x1

    #@24
    :try_start_24
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 1624
    const v4, 0x4c444e42

    #@2a
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 1626
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@30
    move-result v3

    #@31
    .line 1627
    .local v3, oldPos:I
    iget-object v4, p0, Landroid/os/Bundle;->mMap:Ljava/util/Map;

    #@33
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeMapInternal(Ljava/util/Map;)V

    #@36
    .line 1628
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    #@39
    move-result v1

    #@3a
    .line 1631
    .local v1, newPos:I
    add-int/lit8 v4, v3, -0x8

    #@3c
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    #@3f
    .line 1632
    sub-int v0, v1, v3

    #@41
    .line 1633
    .restart local v0       #length:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@44
    .line 1634
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->setDataPosition(I)V
    :try_end_47
    .catchall {:try_start_24 .. :try_end_47} :catchall_48

    #@47
    goto :goto_1f

    #@48
    .line 1637
    .end local v0           #length:I
    .end local v1           #newPos:I
    .end local v3           #oldPos:I
    :catchall_48
    move-exception v4

    #@49
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->restoreAllowFds(Z)V

    #@4c
    throw v4
.end method
