.class public Landroid/os/WorkSource;
.super Ljava/lang/Object;
.source "WorkSource.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/WorkSource;",
            ">;"
        }
    .end annotation
.end field

.field static sGoneWork:Landroid/os/WorkSource;

.field static sNewbWork:Landroid/os/WorkSource;

.field static final sTmpWorkSource:Landroid/os/WorkSource;


# instance fields
.field mNum:I

.field mUids:[I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 21
    new-instance v0, Landroid/os/WorkSource;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/os/WorkSource;-><init>(I)V

    #@6
    sput-object v0, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@8
    .line 334
    new-instance v0, Landroid/os/WorkSource$1;

    #@a
    invoke-direct {v0}, Landroid/os/WorkSource$1;-><init>()V

    #@d
    sput-object v0, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@6
    .line 36
    return-void
.end method

.method public constructor <init>(I)V
    .registers 5
    .parameter "uid"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 57
    iput v2, p0, Landroid/os/WorkSource;->mNum:I

    #@7
    .line 58
    const/4 v0, 0x2

    #@8
    new-array v0, v0, [I

    #@a
    aput p1, v0, v1

    #@c
    aput v1, v0, v2

    #@e
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@10
    .line 59
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@9
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@f
    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/os/WorkSource;)V
    .registers 3
    .parameter "orig"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    if-nez p1, :cond_9

    #@5
    .line 44
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@8
    .line 53
    :goto_8
    return-void

    #@9
    .line 47
    :cond_9
    iget v0, p1, Landroid/os/WorkSource;->mNum:I

    #@b
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@d
    .line 48
    iget-object v0, p1, Landroid/os/WorkSource;->mUids:[I

    #@f
    if-eqz v0, :cond_1c

    #@11
    .line 49
    iget-object v0, p1, Landroid/os/WorkSource;->mUids:[I

    #@13
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, [I

    #@19
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@1b
    goto :goto_8

    #@1c
    .line 51
    :cond_1c
    const/4 v0, 0x0

    #@1d
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@1f
    goto :goto_8
.end method

.method private addLocked(I)V
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 293
    iget-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@3
    if-nez v1, :cond_12

    #@5
    .line 294
    const/4 v1, 0x4

    #@6
    new-array v1, v1, [I

    #@8
    iput-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@a
    .line 295
    iget-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@c
    aput p1, v1, v3

    #@e
    .line 296
    const/4 v1, 0x1

    #@f
    iput v1, p0, Landroid/os/WorkSource;->mNum:I

    #@11
    .line 307
    :goto_11
    return-void

    #@12
    .line 299
    :cond_12
    iget v1, p0, Landroid/os/WorkSource;->mNum:I

    #@14
    iget-object v2, p0, Landroid/os/WorkSource;->mUids:[I

    #@16
    array-length v2, v2

    #@17
    if-lt v1, v2, :cond_2a

    #@19
    .line 300
    iget v1, p0, Landroid/os/WorkSource;->mNum:I

    #@1b
    mul-int/lit8 v1, v1, 0x3

    #@1d
    div-int/lit8 v1, v1, 0x2

    #@1f
    new-array v0, v1, [I

    #@21
    .line 301
    .local v0, newuids:[I
    iget-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@23
    iget v2, p0, Landroid/os/WorkSource;->mNum:I

    #@25
    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@28
    .line 302
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@2a
    .line 305
    .end local v0           #newuids:[I
    :cond_2a
    iget-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@2c
    iget v2, p0, Landroid/os/WorkSource;->mNum:I

    #@2e
    aput p1, v1, v2

    #@30
    .line 306
    iget v1, p0, Landroid/os/WorkSource;->mNum:I

    #@32
    add-int/lit8 v1, v1, 0x1

    #@34
    iput v1, p0, Landroid/os/WorkSource;->mNum:I

    #@36
    goto :goto_11
.end method

.method private updateLocked(Landroid/os/WorkSource;ZZ)Z
    .registers 16
    .parameter "other"
    .parameter "set"
    .parameter "returnNewbs"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 224
    iget v0, p0, Landroid/os/WorkSource;->mNum:I

    #@3
    .line 225
    .local v0, N1:I
    iget-object v7, p0, Landroid/os/WorkSource;->mUids:[I

    #@5
    .line 226
    .local v7, uids1:[I
    iget v1, p1, Landroid/os/WorkSource;->mNum:I

    #@7
    .line 227
    .local v1, N2:I
    iget-object v8, p1, Landroid/os/WorkSource;->mUids:[I

    #@9
    .line 228
    .local v8, uids2:[I
    const/4 v2, 0x0

    #@a
    .line 229
    .local v2, changed:Z
    const/4 v3, 0x0

    #@b
    .line 230
    .local v3, i1:I
    const/4 v4, 0x0

    #@c
    .local v4, i2:I
    :goto_c
    if-ge v4, v1, :cond_ae

    #@e
    .line 231
    if-ge v3, v0, :cond_16

    #@10
    aget v9, v8, v4

    #@12
    aget v10, v7, v3

    #@14
    if-ge v9, v10, :cond_6a

    #@16
    .line 233
    :cond_16
    const/4 v2, 0x1

    #@17
    .line 234
    if-nez v7, :cond_36

    #@19
    .line 235
    const/4 v9, 0x4

    #@1a
    new-array v7, v9, [I

    #@1c
    .line 236
    aget v9, v8, v4

    #@1e
    aput v9, v7, v11

    #@20
    .line 247
    :goto_20
    if-eqz p3, :cond_2f

    #@22
    .line 248
    sget-object v9, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@24
    if-nez v9, :cond_62

    #@26
    .line 249
    new-instance v9, Landroid/os/WorkSource;

    #@28
    aget v10, v8, v4

    #@2a
    invoke-direct {v9, v10}, Landroid/os/WorkSource;-><init>(I)V

    #@2d
    sput-object v9, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@2f
    .line 254
    :cond_2f
    :goto_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    .line 255
    add-int/lit8 v3, v3, 0x1

    #@33
    .line 230
    :cond_33
    :goto_33
    add-int/lit8 v4, v4, 0x1

    #@35
    goto :goto_c

    #@36
    .line 237
    :cond_36
    array-length v9, v7

    #@37
    if-lt v3, v9, :cond_54

    #@39
    .line 238
    array-length v9, v7

    #@3a
    mul-int/lit8 v9, v9, 0x3

    #@3c
    div-int/lit8 v9, v9, 0x2

    #@3e
    new-array v5, v9, [I

    #@40
    .line 239
    .local v5, newuids:[I
    if-lez v3, :cond_45

    #@42
    invoke-static {v7, v11, v5, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@45
    .line 240
    :cond_45
    if-ge v3, v0, :cond_4e

    #@47
    add-int/lit8 v9, v3, 0x1

    #@49
    sub-int v10, v0, v3

    #@4b
    invoke-static {v7, v3, v5, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@4e
    .line 241
    :cond_4e
    move-object v7, v5

    #@4f
    .line 242
    aget v9, v8, v4

    #@51
    aput v9, v7, v3

    #@53
    goto :goto_20

    #@54
    .line 244
    .end local v5           #newuids:[I
    :cond_54
    if-ge v3, v0, :cond_5d

    #@56
    add-int/lit8 v9, v3, 0x1

    #@58
    sub-int v10, v0, v3

    #@5a
    invoke-static {v7, v3, v7, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@5d
    .line 245
    :cond_5d
    aget v9, v8, v4

    #@5f
    aput v9, v7, v3

    #@61
    goto :goto_20

    #@62
    .line 251
    :cond_62
    sget-object v9, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@64
    aget v10, v8, v4

    #@66
    invoke-direct {v9, v10}, Landroid/os/WorkSource;->addLocked(I)V

    #@69
    goto :goto_2f

    #@6a
    .line 257
    :cond_6a
    if-nez p2, :cond_77

    #@6c
    .line 260
    :cond_6c
    add-int/lit8 v3, v3, 0x1

    #@6e
    .line 261
    if-ge v3, v0, :cond_33

    #@70
    aget v9, v8, v4

    #@72
    aget v10, v7, v3

    #@74
    if-ge v9, v10, :cond_6c

    #@76
    goto :goto_33

    #@77
    .line 264
    :cond_77
    move v6, v3

    #@78
    .line 265
    .local v6, start:I
    :goto_78
    if-ge v3, v0, :cond_98

    #@7a
    aget v9, v8, v4

    #@7c
    aget v10, v7, v3

    #@7e
    if-le v9, v10, :cond_98

    #@80
    .line 266
    sget-object v9, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@82
    if-nez v9, :cond_90

    #@84
    .line 267
    new-instance v9, Landroid/os/WorkSource;

    #@86
    aget v10, v7, v3

    #@88
    invoke-direct {v9, v10}, Landroid/os/WorkSource;-><init>(I)V

    #@8b
    sput-object v9, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@8d
    .line 271
    :goto_8d
    add-int/lit8 v3, v3, 0x1

    #@8f
    goto :goto_78

    #@90
    .line 269
    :cond_90
    sget-object v9, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@92
    aget v10, v7, v3

    #@94
    invoke-direct {v9, v10}, Landroid/os/WorkSource;->addLocked(I)V

    #@97
    goto :goto_8d

    #@98
    .line 273
    :cond_98
    if-ge v6, v3, :cond_a3

    #@9a
    .line 274
    sub-int v9, v3, v6

    #@9c
    invoke-static {v7, v3, v7, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9f
    .line 275
    sub-int v9, v3, v6

    #@a1
    sub-int/2addr v0, v9

    #@a2
    .line 276
    move v3, v6

    #@a3
    .line 279
    :cond_a3
    if-ge v3, v0, :cond_33

    #@a5
    aget v9, v8, v3

    #@a7
    aget v10, v7, v3

    #@a9
    if-ne v9, v10, :cond_33

    #@ab
    .line 280
    add-int/lit8 v3, v3, 0x1

    #@ad
    goto :goto_33

    #@ae
    .line 286
    .end local v6           #start:I
    :cond_ae
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@b0
    .line 287
    iput-object v7, p0, Landroid/os/WorkSource;->mUids:[I

    #@b2
    .line 289
    return v2
.end method


# virtual methods
.method public add(I)Z
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    .line 185
    sget-object v1, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@2
    monitor-enter v1

    #@3
    .line 186
    :try_start_3
    sget-object v0, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@5
    iget-object v0, v0, Landroid/os/WorkSource;->mUids:[I

    #@7
    const/4 v2, 0x0

    #@8
    aput p1, v0, v2

    #@a
    .line 187
    sget-object v0, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@c
    const/4 v2, 0x0

    #@d
    const/4 v3, 0x0

    #@e
    invoke-direct {p0, v0, v2, v3}, Landroid/os/WorkSource;->updateLocked(Landroid/os/WorkSource;ZZ)Z

    #@11
    move-result v0

    #@12
    monitor-exit v1

    #@13
    return v0

    #@14
    .line 188
    :catchall_14
    move-exception v0

    #@15
    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    #@16
    throw v0
.end method

.method public add(Landroid/os/WorkSource;)Z
    .registers 5
    .parameter "other"

    #@0
    .prologue
    .line 169
    sget-object v1, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@2
    monitor-enter v1

    #@3
    .line 170
    const/4 v0, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    :try_start_5
    invoke-direct {p0, p1, v0, v2}, Landroid/os/WorkSource;->updateLocked(Landroid/os/WorkSource;ZZ)Z

    #@8
    move-result v0

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 171
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method public addReturningNewbs(I)Landroid/os/WorkSource;
    .registers 6
    .parameter "uid"

    #@0
    .prologue
    .line 193
    sget-object v1, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@2
    monitor-enter v1

    #@3
    .line 194
    const/4 v0, 0x0

    #@4
    :try_start_4
    sput-object v0, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@6
    .line 195
    sget-object v0, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@8
    iget-object v0, v0, Landroid/os/WorkSource;->mUids:[I

    #@a
    const/4 v2, 0x0

    #@b
    aput p1, v0, v2

    #@d
    .line 196
    sget-object v0, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@f
    const/4 v2, 0x0

    #@10
    const/4 v3, 0x1

    #@11
    invoke-direct {p0, v0, v2, v3}, Landroid/os/WorkSource;->updateLocked(Landroid/os/WorkSource;ZZ)Z

    #@14
    .line 197
    sget-object v0, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@16
    monitor-exit v1

    #@17
    return-object v0

    #@18
    .line 198
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_4 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public addReturningNewbs(Landroid/os/WorkSource;)Landroid/os/WorkSource;
    .registers 5
    .parameter "other"

    #@0
    .prologue
    .line 176
    sget-object v1, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@2
    monitor-enter v1

    #@3
    .line 177
    const/4 v0, 0x0

    #@4
    :try_start_4
    sput-object v0, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@6
    .line 178
    const/4 v0, 0x0

    #@7
    const/4 v2, 0x1

    #@8
    invoke-direct {p0, p1, v0, v2}, Landroid/os/WorkSource;->updateLocked(Landroid/os/WorkSource;ZZ)Z

    #@b
    .line 179
    sget-object v0, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@d
    monitor-exit v1

    #@e
    return-object v0

    #@f
    .line 180
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_4 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public clear()V
    .registers 2

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@3
    .line 81
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 311
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public diff(Landroid/os/WorkSource;)Z
    .registers 9
    .parameter "other"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 103
    iget v0, p0, Landroid/os/WorkSource;->mNum:I

    #@3
    .line 104
    .local v0, N:I
    iget v5, p1, Landroid/os/WorkSource;->mNum:I

    #@5
    if-eq v0, v5, :cond_8

    #@7
    .line 114
    :cond_7
    :goto_7
    return v4

    #@8
    .line 107
    :cond_8
    iget-object v2, p0, Landroid/os/WorkSource;->mUids:[I

    #@a
    .line 108
    .local v2, uids1:[I
    iget-object v3, p1, Landroid/os/WorkSource;->mUids:[I

    #@c
    .line 109
    .local v3, uids2:[I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_18

    #@f
    .line 110
    aget v5, v2, v1

    #@11
    aget v6, v3, v1

    #@13
    if-ne v5, v6, :cond_7

    #@15
    .line 109
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_d

    #@18
    .line 114
    :cond_18
    const/4 v4, 0x0

    #@19
    goto :goto_7
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 85
    instance-of v0, p1, Landroid/os/WorkSource;

    #@2
    if-eqz v0, :cond_e

    #@4
    check-cast p1, Landroid/os/WorkSource;

    #@6
    .end local p1
    invoke-virtual {p0, p1}, Landroid/os/WorkSource;->diff(Landroid/os/WorkSource;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public get(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@2
    aget v0, v0, p1

    #@4
    return v0
.end method

.method public hashCode()I
    .registers 5

    #@0
    .prologue
    .line 90
    const/4 v1, 0x0

    #@1
    .line 91
    .local v1, result:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    iget v2, p0, Landroid/os/WorkSource;->mNum:I

    #@4
    if-ge v0, v2, :cond_14

    #@6
    .line 92
    shl-int/lit8 v2, v1, 0x4

    #@8
    ushr-int/lit8 v3, v1, 0x1c

    #@a
    or-int/2addr v2, v3

    #@b
    iget-object v3, p0, Landroid/os/WorkSource;->mUids:[I

    #@d
    aget v3, v3, v0

    #@f
    xor-int v1, v2, v3

    #@11
    .line 91
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_2

    #@14
    .line 94
    :cond_14
    return v1
.end method

.method public remove(Landroid/os/WorkSource;)Z
    .registers 11
    .parameter "other"

    #@0
    .prologue
    .line 202
    iget v0, p0, Landroid/os/WorkSource;->mNum:I

    #@2
    .line 203
    .local v0, N1:I
    iget-object v5, p0, Landroid/os/WorkSource;->mUids:[I

    #@4
    .line 204
    .local v5, uids1:[I
    iget v1, p1, Landroid/os/WorkSource;->mNum:I

    #@6
    .line 205
    .local v1, N2:I
    iget-object v6, p1, Landroid/os/WorkSource;->mUids:[I

    #@8
    .line 206
    .local v6, uids2:[I
    const/4 v2, 0x0

    #@9
    .line 207
    .local v2, changed:Z
    const/4 v3, 0x0

    #@a
    .line 208
    .local v3, i1:I
    const/4 v4, 0x0

    #@b
    .local v4, i2:I
    :goto_b
    if-ge v4, v1, :cond_2e

    #@d
    if-ge v3, v0, :cond_2e

    #@f
    .line 209
    aget v7, v6, v4

    #@11
    aget v8, v5, v3

    #@13
    if-ne v7, v8, :cond_20

    #@15
    .line 210
    add-int/lit8 v0, v0, -0x1

    #@17
    .line 211
    if-ge v3, v0, :cond_20

    #@19
    add-int/lit8 v7, v3, 0x1

    #@1b
    sub-int v8, v0, v3

    #@1d
    invoke-static {v5, v7, v5, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@20
    .line 213
    :cond_20
    :goto_20
    if-ge v3, v0, :cond_2b

    #@22
    aget v7, v6, v4

    #@24
    aget v8, v5, v3

    #@26
    if-le v7, v8, :cond_2b

    #@28
    .line 214
    add-int/lit8 v3, v3, 0x1

    #@2a
    goto :goto_20

    #@2b
    .line 208
    :cond_2b
    add-int/lit8 v4, v4, 0x1

    #@2d
    goto :goto_b

    #@2e
    .line 218
    :cond_2e
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@30
    .line 220
    return v2
.end method

.method public set(I)V
    .registers 4
    .parameter "uid"

    #@0
    .prologue
    .line 141
    const/4 v0, 0x1

    #@1
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@3
    .line 142
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@5
    if-nez v0, :cond_c

    #@7
    const/4 v0, 0x2

    #@8
    new-array v0, v0, [I

    #@a
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@c
    .line 143
    :cond_c
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@e
    const/4 v1, 0x0

    #@f
    aput p1, v0, v1

    #@11
    .line 144
    return-void
.end method

.method public set(Landroid/os/WorkSource;)V
    .registers 6
    .parameter "other"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 123
    if-nez p1, :cond_6

    #@3
    .line 124
    iput v3, p0, Landroid/os/WorkSource;->mNum:I

    #@5
    .line 137
    :goto_5
    return-void

    #@6
    .line 127
    :cond_6
    iget v0, p1, Landroid/os/WorkSource;->mNum:I

    #@8
    iput v0, p0, Landroid/os/WorkSource;->mNum:I

    #@a
    .line 128
    iget-object v0, p1, Landroid/os/WorkSource;->mUids:[I

    #@c
    if-eqz v0, :cond_2e

    #@e
    .line 129
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@10
    if-eqz v0, :cond_23

    #@12
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@14
    array-length v0, v0

    #@15
    iget v1, p0, Landroid/os/WorkSource;->mNum:I

    #@17
    if-lt v0, v1, :cond_23

    #@19
    .line 130
    iget-object v0, p1, Landroid/os/WorkSource;->mUids:[I

    #@1b
    iget-object v1, p0, Landroid/os/WorkSource;->mUids:[I

    #@1d
    iget v2, p0, Landroid/os/WorkSource;->mNum:I

    #@1f
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@22
    goto :goto_5

    #@23
    .line 132
    :cond_23
    iget-object v0, p1, Landroid/os/WorkSource;->mUids:[I

    #@25
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@28
    move-result-object v0

    #@29
    check-cast v0, [I

    #@2b
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@2d
    goto :goto_5

    #@2e
    .line 135
    :cond_2e
    const/4 v0, 0x0

    #@2f
    iput-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@31
    goto :goto_5
.end method

.method public setReturningDiffs(Landroid/os/WorkSource;)[Landroid/os/WorkSource;
    .registers 6
    .parameter "other"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 148
    sget-object v2, Landroid/os/WorkSource;->sTmpWorkSource:Landroid/os/WorkSource;

    #@3
    monitor-enter v2

    #@4
    .line 149
    const/4 v1, 0x0

    #@5
    :try_start_5
    sput-object v1, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@7
    .line 150
    const/4 v1, 0x0

    #@8
    sput-object v1, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@a
    .line 151
    const/4 v1, 0x1

    #@b
    const/4 v3, 0x1

    #@c
    invoke-direct {p0, p1, v1, v3}, Landroid/os/WorkSource;->updateLocked(Landroid/os/WorkSource;ZZ)Z

    #@f
    .line 152
    sget-object v1, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@11
    if-nez v1, :cond_17

    #@13
    sget-object v1, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@15
    if-eqz v1, :cond_26

    #@17
    .line 153
    :cond_17
    const/4 v1, 0x2

    #@18
    new-array v0, v1, [Landroid/os/WorkSource;

    #@1a
    .line 154
    .local v0, diffs:[Landroid/os/WorkSource;
    const/4 v1, 0x0

    #@1b
    sget-object v3, Landroid/os/WorkSource;->sNewbWork:Landroid/os/WorkSource;

    #@1d
    aput-object v3, v0, v1

    #@1f
    .line 155
    const/4 v1, 0x1

    #@20
    sget-object v3, Landroid/os/WorkSource;->sGoneWork:Landroid/os/WorkSource;

    #@22
    aput-object v3, v0, v1

    #@24
    .line 156
    monitor-exit v2

    #@25
    .line 158
    .end local v0           #diffs:[Landroid/os/WorkSource;
    :goto_25
    return-object v0

    #@26
    :cond_26
    monitor-exit v2

    #@27
    goto :goto_25

    #@28
    .line 159
    :catchall_28
    move-exception v1

    #@29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_5 .. :try_end_2a} :catchall_28

    #@2a
    throw v1
.end method

.method public size()I
    .registers 2

    #@0
    .prologue
    .line 68
    iget v0, p0, Landroid/os/WorkSource;->mNum:I

    #@2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 323
    .local v1, result:Ljava/lang/StringBuilder;
    const-string/jumbo v2, "{WorkSource: uids=["

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 324
    const/4 v0, 0x0

    #@c
    .local v0, i:I
    :goto_c
    iget v2, p0, Landroid/os/WorkSource;->mNum:I

    #@e
    if-ge v0, v2, :cond_21

    #@10
    .line 325
    if-eqz v0, :cond_17

    #@12
    .line 326
    const-string v2, ", "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 328
    :cond_17
    iget-object v2, p0, Landroid/os/WorkSource;->mUids:[I

    #@19
    aget v2, v2, v0

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    .line 324
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_c

    #@21
    .line 330
    :cond_21
    const-string v2, "]}"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 331
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    return-object v2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 316
    iget v0, p0, Landroid/os/WorkSource;->mNum:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 317
    iget-object v0, p0, Landroid/os/WorkSource;->mUids:[I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    #@a
    .line 318
    return-void
.end method
