.class public Landroid/os/ParcelFileDescriptor;
.super Ljava/lang/Object;
.source "ParcelFileDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;,
        Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field public static final MODE_APPEND:I = 0x2000000

.field public static final MODE_CREATE:I = 0x8000000

.field public static final MODE_READ_ONLY:I = 0x10000000

.field public static final MODE_READ_WRITE:I = 0x30000000

.field public static final MODE_TRUNCATE:I = 0x4000000

.field public static final MODE_WORLD_READABLE:I = 0x1

.field public static final MODE_WORLD_WRITEABLE:I = 0x2

.field public static final MODE_WRITE_ONLY:I = 0x20000000


# instance fields
.field private volatile mClosed:Z

.field private final mFileDescriptor:Ljava/io/FileDescriptor;

.field private final mGuard:Ldalvik/system/CloseGuard;

.field private final mWrapped:Landroid/os/ParcelFileDescriptor;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 437
    new-instance v0, Landroid/os/ParcelFileDescriptor$1;

    #@2
    invoke-direct {v0}, Landroid/os/ParcelFileDescriptor$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/ParcelFileDescriptor;)V
    .registers 4
    .parameter "descriptor"

    #@0
    .prologue
    .line 399
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 400
    iput-object p1, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@b
    .line 401
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@d
    iget-object v0, v0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@f
    iput-object v0, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@11
    .line 402
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@13
    const-string v1, "close"

    #@15
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@18
    .line 403
    return-void
.end method

.method public constructor <init>(Ljava/io/FileDescriptor;)V
    .registers 4
    .parameter "descriptor"

    #@0
    .prologue
    .line 406
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@9
    .line 407
    if-nez p1, :cond_13

    #@b
    .line 408
    new-instance v0, Ljava/lang/NullPointerException;

    #@d
    const-string v1, "descriptor must not be null"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 410
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@16
    .line 411
    iput-object p1, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@18
    .line 412
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@1a
    const-string v1, "close"

    #@1c
    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    #@1f
    .line 413
    return-void
.end method

.method public static adoptFd(I)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "fd"

    #@0
    .prologue
    .line 178
    invoke-static {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptorFromFdNoDup(I)Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 179
    .local v0, fdesc:Ljava/io/FileDescriptor;
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@9
    return-object v1
.end method

.method public static createPipe()[Landroid/os/ParcelFileDescriptor;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 222
    new-array v0, v2, [Ljava/io/FileDescriptor;

    #@5
    .line 223
    .local v0, fds:[Ljava/io/FileDescriptor;
    invoke-static {v0}, Landroid/os/ParcelFileDescriptor;->createPipeNative([Ljava/io/FileDescriptor;)V

    #@8
    .line 224
    new-array v1, v2, [Landroid/os/ParcelFileDescriptor;

    #@a
    .line 225
    .local v1, pfds:[Landroid/os/ParcelFileDescriptor;
    new-instance v2, Landroid/os/ParcelFileDescriptor;

    #@c
    aget-object v3, v0, v4

    #@e
    invoke-direct {v2, v3}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@11
    aput-object v2, v1, v4

    #@13
    .line 226
    new-instance v2, Landroid/os/ParcelFileDescriptor;

    #@15
    aget-object v3, v0, v5

    #@17
    invoke-direct {v2, v3}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@1a
    aput-object v2, v1, v5

    #@1c
    .line 227
    return-object v1
.end method

.method private static native createPipeNative([Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public static dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "orig"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 135
    invoke-static {p0}, Landroid/os/Parcel;->dupFileDescriptor(Ljava/io/FileDescriptor;)Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 136
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@8
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public static fromData([BLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 7
    .parameter "data"
    .parameter "name"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 244
    if-nez p0, :cond_5

    #@4
    .line 251
    :cond_4
    :goto_4
    return-object v2

    #@5
    .line 245
    :cond_5
    new-instance v1, Landroid/os/MemoryFile;

    #@7
    array-length v3, p0

    #@8
    invoke-direct {v1, p1, v3}, Landroid/os/MemoryFile;-><init>(Ljava/lang/String;I)V

    #@b
    .line 246
    .local v1, file:Landroid/os/MemoryFile;
    array-length v3, p0

    #@c
    if-lez v3, :cond_12

    #@e
    .line 247
    array-length v3, p0

    #@f
    invoke-virtual {v1, p0, v4, v4, v3}, Landroid/os/MemoryFile;->writeBytes([BIII)V

    #@12
    .line 249
    :cond_12
    invoke-virtual {v1}, Landroid/os/MemoryFile;->deactivate()V

    #@15
    .line 250
    invoke-virtual {v1}, Landroid/os/MemoryFile;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@18
    move-result-object v0

    #@19
    .line 251
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_4

    #@1b
    new-instance v2, Landroid/os/ParcelFileDescriptor;

    #@1d
    invoke-direct {v2, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@20
    goto :goto_4
.end method

.method public static fromDatagramSocket(Ljava/net/DatagramSocket;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "datagramSocket"

    #@0
    .prologue
    .line 212
    invoke-virtual {p0}, Ljava/net/DatagramSocket;->getFileDescriptor$()Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 213
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@8
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public static fromFd(I)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    invoke-static {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptorFromFd(I)Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 161
    .local v0, fdesc:Ljava/io/FileDescriptor;
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@9
    return-object v1
.end method

.method public static fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;
    .registers 3
    .parameter "socket"

    #@0
    .prologue
    .line 198
    invoke-virtual {p0}, Ljava/net/Socket;->getFileDescriptor$()Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    .line 199
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_c

    #@6
    new-instance v1, Landroid/os/ParcelFileDescriptor;

    #@8
    invoke-direct {v1, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method private native getFdNative()I
.end method

.method private static native getFileDescriptorFromFd(I)Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native getFileDescriptorFromFdNoDup(I)Ljava/io/FileDescriptor;
.end method

.method public static open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    .registers 7
    .parameter "file"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 110
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 111
    .local v1, path:Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    #@7
    move-result-object v2

    #@8
    .line 112
    .local v2, security:Ljava/lang/SecurityManager;
    if-eqz v2, :cond_15

    #@a
    .line 113
    invoke-virtual {v2, v1}, Ljava/lang/SecurityManager;->checkRead(Ljava/lang/String;)V

    #@d
    .line 114
    const/high16 v3, 0x2000

    #@f
    and-int/2addr v3, p1

    #@10
    if-eqz v3, :cond_15

    #@12
    .line 115
    invoke-virtual {v2, v1}, Ljava/lang/SecurityManager;->checkWrite(Ljava/lang/String;)V

    #@15
    .line 119
    :cond_15
    const/high16 v3, 0x3000

    #@17
    and-int/2addr v3, p1

    #@18
    if-nez v3, :cond_22

    #@1a
    .line 120
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1c
    const-string v4, "Must specify MODE_READ_ONLY, MODE_WRITE_ONLY, or MODE_READ_WRITE"

    #@1e
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@21
    throw v3

    #@22
    .line 124
    :cond_22
    invoke-static {v1, p1}, Landroid/os/Parcel;->openFileDescriptor(Ljava/lang/String;I)Ljava/io/FileDescriptor;

    #@25
    move-result-object v0

    #@26
    .line 125
    .local v0, fd:Ljava/io/FileDescriptor;
    if-eqz v0, :cond_2e

    #@28
    new-instance v3, Landroid/os/ParcelFileDescriptor;

    #@2a
    invoke-direct {v3, v0}, Landroid/os/ParcelFileDescriptor;-><init>(Ljava/io/FileDescriptor;)V

    #@2d
    :goto_2d
    return-object v3

    #@2e
    :cond_2e
    const/4 v3, 0x0

    #@2f
    goto :goto_2d
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 321
    iget-boolean v0, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 332
    :goto_4
    return-void

    #@5
    .line 322
    :cond_5
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@8
    .line 323
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@a
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->close()V

    #@d
    .line 325
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 328
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@13
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    #@16
    goto :goto_4

    #@17
    .line 330
    :cond_17
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@19
    invoke-static {v0}, Landroid/os/Parcel;->closeFileDescriptor(Ljava/io/FileDescriptor;)V

    #@1c
    goto :goto_4
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 417
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public detachFd()I
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 296
    iget-boolean v2, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@3
    if-eqz v2, :cond_d

    #@5
    .line 297
    new-instance v2, Ljava/lang/IllegalStateException;

    #@7
    const-string v3, "Already closed"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 299
    :cond_d
    iget-object v2, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@f
    if-eqz v2, :cond_20

    #@11
    .line 300
    iget-object v2, p0, Landroid/os/ParcelFileDescriptor;->mWrapped:Landroid/os/ParcelFileDescriptor;

    #@13
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    #@16
    move-result v0

    #@17
    .line 301
    .local v0, fd:I
    iput-boolean v3, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@19
    .line 302
    iget-object v2, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@1b
    invoke-virtual {v2}, Ldalvik/system/CloseGuard;->close()V

    #@1e
    move v1, v0

    #@1f
    .line 309
    .end local v0           #fd:I
    .local v1, fd:I
    :goto_1f
    return v1

    #@20
    .line 305
    .end local v1           #fd:I
    :cond_20
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFd()I

    #@23
    move-result v0

    #@24
    .line 306
    .restart local v0       #fd:I
    iput-boolean v3, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@26
    .line 307
    iget-object v2, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@28
    invoke-virtual {v2}, Ldalvik/system/CloseGuard;->close()V

    #@2b
    .line 308
    iget-object v2, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@2d
    invoke-static {v2}, Landroid/os/Parcel;->clearFileDescriptor(Ljava/io/FileDescriptor;)V

    #@30
    move v1, v0

    #@31
    .line 309
    .end local v0           #fd:I
    .restart local v1       #fd:I
    goto :goto_1f
.end method

.method public dup()Landroid/os/ParcelFileDescriptor;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 387
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 388
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mGuard:Ldalvik/system/CloseGuard;

    #@6
    invoke-virtual {v0}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    #@9
    .line 391
    :cond_9
    :try_start_9
    iget-boolean v0, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@b
    if-nez v0, :cond_10

    #@d
    .line 392
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_14

    #@10
    .line 395
    :cond_10
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@13
    .line 397
    return-void

    #@14
    .line 395
    :catchall_14
    move-exception v0

    #@15
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@18
    throw v0
.end method

.method public getFd()I
    .registers 3

    #@0
    .prologue
    .line 282
    iget-boolean v0, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string v1, "Already closed"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 285
    :cond_c
    invoke-direct {p0}, Landroid/os/ParcelFileDescriptor;->getFdNative()I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public getFileDescriptor()Ljava/io/FileDescriptor;
    .registers 2

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@2
    return-object v0
.end method

.method public native getStatSize()J
.end method

.method public native seekTo(J)J
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string/jumbo v1, "{ParcelFileDescriptor: "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    iget-object v1, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string/jumbo v1, "}"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 427
    iget-object v0, p0, Landroid/os/ParcelFileDescriptor;->mFileDescriptor:Ljava/io/FileDescriptor;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFileDescriptor(Ljava/io/FileDescriptor;)V

    #@5
    .line 428
    and-int/lit8 v0, p2, 0x1

    #@7
    if-eqz v0, :cond_10

    #@9
    iget-boolean v0, p0, Landroid/os/ParcelFileDescriptor;->mClosed:Z

    #@b
    if-nez v0, :cond_10

    #@d
    .line 430
    :try_start_d
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_11

    #@10
    .line 435
    :cond_10
    :goto_10
    return-void

    #@11
    .line 431
    :catch_11
    move-exception v0

    #@12
    goto :goto_10
.end method
