.class Landroid/os/AsyncTask$InternalHandler;
.super Landroid/os/Handler;
.source "AsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 636
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@3
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/AsyncTask$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 636
    invoke-direct {p0}, Landroid/os/AsyncTask$InternalHandler;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 640
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/AsyncTask$AsyncTaskResult;

    #@4
    .line 641
    .local v0, result:Landroid/os/AsyncTask$AsyncTaskResult;
    iget v1, p1, Landroid/os/Message;->what:I

    #@6
    packed-switch v1, :pswitch_data_1e

    #@9
    .line 650
    :goto_9
    return-void

    #@a
    .line 644
    :pswitch_a
    iget-object v1, v0, Landroid/os/AsyncTask$AsyncTaskResult;->mTask:Landroid/os/AsyncTask;

    #@c
    iget-object v2, v0, Landroid/os/AsyncTask$AsyncTaskResult;->mData:[Ljava/lang/Object;

    #@e
    const/4 v3, 0x0

    #@f
    aget-object v2, v2, v3

    #@11
    invoke-static {v1, v2}, Landroid/os/AsyncTask;->access$600(Landroid/os/AsyncTask;Ljava/lang/Object;)V

    #@14
    goto :goto_9

    #@15
    .line 647
    :pswitch_15
    iget-object v1, v0, Landroid/os/AsyncTask$AsyncTaskResult;->mTask:Landroid/os/AsyncTask;

    #@17
    iget-object v2, v0, Landroid/os/AsyncTask$AsyncTaskResult;->mData:[Ljava/lang/Object;

    #@19
    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    #@1c
    goto :goto_9

    #@1d
    .line 641
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_a
        :pswitch_15
    .end packed-switch
.end method
