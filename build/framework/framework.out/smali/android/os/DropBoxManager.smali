.class public Landroid/os/DropBoxManager;
.super Ljava/lang/Object;
.source "DropBoxManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/DropBoxManager$Entry;
    }
.end annotation


# static fields
.field public static final ACTION_DROPBOX_ENTRY_ADDED:Ljava/lang/String; = "android.intent.action.DROPBOX_ENTRY_ADDED"

.field public static final EXTRA_TAG:Ljava/lang/String; = "tag"

.field public static final EXTRA_TIME:Ljava/lang/String; = "time"

.field private static final HAS_BYTE_ARRAY:I = 0x8

.field public static final IS_EMPTY:I = 0x1

.field public static final IS_GZIPPED:I = 0x4

.field public static final IS_TEXT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "DropBoxManager"


# instance fields
.field private final mService:Lcom/android/internal/os/IDropBoxManagerService;


# direct methods
.method protected constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 261
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@6
    return-void
.end method

.method public constructor <init>(Lcom/android/internal/os/IDropBoxManagerService;)V
    .registers 2
    .parameter "service"

    #@0
    .prologue
    .line 254
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    iput-object p1, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@5
    return-void
.end method


# virtual methods
.method public addData(Ljava/lang/String;[BI)V
    .registers 11
    .parameter "tag"
    .parameter "data"
    .parameter "flags"

    #@0
    .prologue
    .line 283
    if-nez p2, :cond_a

    #@2
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "data == null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 284
    :cond_a
    :try_start_a
    iget-object v6, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@c
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@e
    const-wide/16 v2, 0x0

    #@10
    move-object v1, p1

    #@11
    move-object v4, p2

    #@12
    move v5, p3

    #@13
    invoke-direct/range {v0 .. v5}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;J[BI)V

    #@16
    invoke-interface {v6, v0}, Lcom/android/internal/os/IDropBoxManagerService;->add(Landroid/os/DropBoxManager$Entry;)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_19} :catch_1a

    #@19
    .line 285
    :goto_19
    return-void

    #@1a
    .line 284
    :catch_1a
    move-exception v0

    #@1b
    goto :goto_19
.end method

.method public addFile(Ljava/lang/String;Ljava/io/File;I)V
    .registers 10
    .parameter "tag"
    .parameter "file"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 297
    if-nez p2, :cond_a

    #@2
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string v2, "file == null"

    #@6
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v1

    #@a
    .line 298
    :cond_a
    new-instance v0, Landroid/os/DropBoxManager$Entry;

    #@c
    const-wide/16 v2, 0x0

    #@e
    move-object v1, p1

    #@f
    move-object v4, p2

    #@10
    move v5, p3

    #@11
    invoke-direct/range {v0 .. v5}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;JLjava/io/File;I)V

    #@14
    .line 300
    .local v0, entry:Landroid/os/DropBoxManager$Entry;
    :try_start_14
    iget-object v1, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@16
    invoke-interface {v1, v0}, Lcom/android/internal/os/IDropBoxManagerService;->add(Landroid/os/DropBoxManager$Entry;)V
    :try_end_19
    .catchall {:try_start_14 .. :try_end_19} :catchall_22
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_19} :catch_1d

    #@19
    .line 304
    invoke-virtual {v0}, Landroid/os/DropBoxManager$Entry;->close()V

    #@1c
    .line 306
    :goto_1c
    return-void

    #@1d
    .line 301
    :catch_1d
    move-exception v1

    #@1e
    .line 304
    invoke-virtual {v0}, Landroid/os/DropBoxManager$Entry;->close()V

    #@21
    goto :goto_1c

    #@22
    :catchall_22
    move-exception v1

    #@23
    invoke-virtual {v0}, Landroid/os/DropBoxManager$Entry;->close()V

    #@26
    throw v1
.end method

.method public addText(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "tag"
    .parameter "data"

    #@0
    .prologue
    .line 272
    :try_start_0
    iget-object v0, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@2
    new-instance v1, Landroid/os/DropBoxManager$Entry;

    #@4
    const-wide/16 v2, 0x0

    #@6
    invoke-direct {v1, p1, v2, v3, p2}, Landroid/os/DropBoxManager$Entry;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    #@9
    invoke-interface {v0, v1}, Lcom/android/internal/os/IDropBoxManagerService;->add(Landroid/os/DropBoxManager$Entry;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 273
    :goto_c
    return-void

    #@d
    .line 272
    :catch_d
    move-exception v0

    #@e
    goto :goto_c
.end method

.method public getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    .registers 6
    .parameter "tag"
    .parameter "msec"

    #@0
    .prologue
    .line 330
    :try_start_0
    iget-object v1, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@2
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/os/IDropBoxManagerService;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    :goto_6
    return-object v1

    #@7
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method

.method public isTagEnabled(Ljava/lang/String;)Z
    .registers 4
    .parameter "tag"

    #@0
    .prologue
    .line 317
    :try_start_0
    iget-object v1, p0, Landroid/os/DropBoxManager;->mService:Lcom/android/internal/os/IDropBoxManagerService;

    #@2
    invoke-interface {v1, p1}, Lcom/android/internal/os/IDropBoxManagerService;->isTagEnabled(Ljava/lang/String;)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    :goto_6
    return v1

    #@7
    :catch_7
    move-exception v0

    #@8
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@9
    goto :goto_6
.end method
