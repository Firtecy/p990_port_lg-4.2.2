.class public Landroid/os/HandlerThread;
.super Ljava/lang/Thread;
.source "HandlerThread.java"


# instance fields
.field mLooper:Landroid/os/Looper;

.field mPriority:I

.field mTid:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 29
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@3
    .line 25
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/os/HandlerThread;->mTid:I

    #@6
    .line 30
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/os/HandlerThread;->mPriority:I

    #@9
    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter "name"
    .parameter "priority"

    #@0
    .prologue
    .line 40
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@3
    .line 25
    const/4 v0, -0x1

    #@4
    iput v0, p0, Landroid/os/HandlerThread;->mTid:I

    #@6
    .line 41
    iput p2, p0, Landroid/os/HandlerThread;->mPriority:I

    #@8
    .line 42
    return-void
.end method


# virtual methods
.method public getLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/os/HandlerThread;->isAlive()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 72
    const/4 v0, 0x0

    #@7
    .line 84
    :goto_7
    return-object v0

    #@8
    .line 76
    :cond_8
    monitor-enter p0

    #@9
    .line 77
    :goto_9
    :try_start_9
    invoke-virtual {p0}, Landroid/os/HandlerThread;->isAlive()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_19

    #@f
    iget-object v0, p0, Landroid/os/HandlerThread;->mLooper:Landroid/os/Looper;
    :try_end_11
    .catchall {:try_start_9 .. :try_end_11} :catchall_1d

    #@11
    if-nez v0, :cond_19

    #@13
    .line 79
    :try_start_13
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_1d
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_16} :catch_17

    #@16
    goto :goto_9

    #@17
    .line 80
    :catch_17
    move-exception v0

    #@18
    goto :goto_9

    #@19
    .line 83
    :cond_19
    :try_start_19
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_1d

    #@1a
    .line 84
    iget-object v0, p0, Landroid/os/HandlerThread;->mLooper:Landroid/os/Looper;

    #@1c
    goto :goto_7

    #@1d
    .line 83
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method public getThreadId()I
    .registers 2

    #@0
    .prologue
    .line 106
    iget v0, p0, Landroid/os/HandlerThread;->mTid:I

    #@2
    return v0
.end method

.method protected onLooperPrepared()V
    .registers 1

    #@0
    .prologue
    .line 49
    return-void
.end method

.method public quit()Z
    .registers 3

    #@0
    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    .line 95
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_b

    #@6
    .line 96
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@9
    .line 97
    const/4 v1, 0x1

    #@a
    .line 99
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 52
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/os/HandlerThread;->mTid:I

    #@6
    .line 53
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@9
    .line 54
    monitor-enter p0

    #@a
    .line 55
    :try_start_a
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Landroid/os/HandlerThread;->mLooper:Landroid/os/Looper;

    #@10
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@13
    .line 57
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_23

    #@14
    .line 58
    iget v0, p0, Landroid/os/HandlerThread;->mPriority:I

    #@16
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@19
    .line 59
    invoke-virtual {p0}, Landroid/os/HandlerThread;->onLooperPrepared()V

    #@1c
    .line 60
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@1f
    .line 61
    const/4 v0, -0x1

    #@20
    iput v0, p0, Landroid/os/HandlerThread;->mTid:I

    #@22
    .line 62
    return-void

    #@23
    .line 57
    :catchall_23
    move-exception v0

    #@24
    :try_start_24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    #@25
    throw v0
.end method
