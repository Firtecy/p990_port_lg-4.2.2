.class public Landroid/os/Build;
.super Ljava/lang/Object;
.source "Build.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Build$VERSION_CODES;,
        Landroid/os/Build$VERSION;
    }
.end annotation


# static fields
.field public static final BOARD:Ljava/lang/String; = null

.field public static final BOOTLOADER:Ljava/lang/String; = null

.field public static final BRAND:Ljava/lang/String; = null

.field public static final CPU_ABI:Ljava/lang/String; = null

.field public static final CPU_ABI2:Ljava/lang/String; = null

.field public static final DEVICE:Ljava/lang/String; = null

.field public static final DISPLAY:Ljava/lang/String; = null

.field public static final FINGERPRINT:Ljava/lang/String; = null

.field public static final HARDWARE:Ljava/lang/String; = null

.field public static final HOST:Ljava/lang/String; = null

.field public static final ID:Ljava/lang/String; = null

#the value of this static final field might be set in the static constructor
.field public static final IS_DEBUGGABLE:Z = false

.field public static final MANUFACTURER:Ljava/lang/String; = null

.field public static final MODEL:Ljava/lang/String; = null

.field public static final PRODUCT:Ljava/lang/String; = null

.field public static final RADIO:Ljava/lang/String; = null
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SERIAL:Ljava/lang/String; = null

.field public static final TAGS:Ljava/lang/String; = null

#the value of this static final field might be set in the static constructor
.field public static final TIME:J = 0x0L

.field public static final TYPE:Ljava/lang/String; = null

.field public static final UNKNOWN:Ljava/lang/String; = "unknown"

.field public static final USER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 29
    const-string/jumbo v2, "ro.build.id"

    #@5
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    sput-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    #@b
    .line 32
    const-string/jumbo v2, "ro.build.display.id"

    #@e
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    sput-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    #@14
    .line 35
    const-string/jumbo v2, "ro.product.name"

    #@17
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    sput-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    #@1d
    .line 38
    const-string/jumbo v2, "ro.product.device"

    #@20
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    sput-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    #@26
    .line 41
    const-string/jumbo v2, "ro.product.board"

    #@29
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    sput-object v2, Landroid/os/Build;->BOARD:Ljava/lang/String;

    #@2f
    .line 44
    const-string/jumbo v2, "ro.product.cpu.abi"

    #@32
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    sput-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    #@38
    .line 47
    const-string/jumbo v2, "ro.product.cpu.abi2"

    #@3b
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v2

    #@3f
    sput-object v2, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    #@41
    .line 50
    const-string/jumbo v2, "ro.product.manufacturer"

    #@44
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    sput-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    #@4a
    .line 53
    const-string/jumbo v2, "ro.product.brand"

    #@4d
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    sput-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    #@53
    .line 56
    const-string/jumbo v2, "ro.product.model"

    #@56
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    sput-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    #@5c
    .line 59
    const-string/jumbo v2, "ro.bootloader"

    #@5f
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    sput-object v2, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    #@65
    .line 70
    const-string v2, "gsm.version.baseband"

    #@67
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    sput-object v2, Landroid/os/Build;->RADIO:Ljava/lang/String;

    #@6d
    .line 73
    const-string/jumbo v2, "ro.hardware"

    #@70
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    sput-object v2, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    #@76
    .line 76
    const-string/jumbo v2, "ro.serialno"

    #@79
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v2

    #@7d
    sput-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    #@7f
    .line 437
    const-string/jumbo v2, "ro.build.type"

    #@82
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    sput-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@88
    .line 440
    const-string/jumbo v2, "ro.build.tags"

    #@8b
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@8e
    move-result-object v2

    #@8f
    sput-object v2, Landroid/os/Build;->TAGS:Ljava/lang/String;

    #@91
    .line 443
    const-string/jumbo v2, "ro.build.fingerprint"

    #@94
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@97
    move-result-object v2

    #@98
    sput-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    #@9a
    .line 446
    const-string/jumbo v2, "ro.build.date.utc"

    #@9d
    invoke-static {v2}, Landroid/os/Build;->getLong(Ljava/lang/String;)J

    #@a0
    move-result-wide v2

    #@a1
    const-wide/16 v4, 0x3e8

    #@a3
    mul-long/2addr v2, v4

    #@a4
    sput-wide v2, Landroid/os/Build;->TIME:J

    #@a6
    .line 447
    const-string/jumbo v2, "ro.build.user"

    #@a9
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@ac
    move-result-object v2

    #@ad
    sput-object v2, Landroid/os/Build;->USER:Ljava/lang/String;

    #@af
    .line 448
    const-string/jumbo v2, "ro.build.host"

    #@b2
    invoke-static {v2}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@b5
    move-result-object v2

    #@b6
    sput-object v2, Landroid/os/Build;->HOST:Ljava/lang/String;

    #@b8
    .line 454
    const-string/jumbo v2, "ro.debuggable"

    #@bb
    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@be
    move-result v2

    #@bf
    if-ne v2, v0, :cond_c4

    #@c1
    :goto_c1
    sput-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    #@c3
    return-void

    #@c4
    :cond_c4
    move v0, v1

    #@c5
    goto :goto_c1
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 129
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    invoke-static {p0}, Landroid/os/Build;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static getLong(Ljava/lang/String;)J
    .registers 4
    .parameter "property"

    #@0
    .prologue
    .line 471
    :try_start_0
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-wide v1

    #@8
    .line 473
    :goto_8
    return-wide v1

    #@9
    .line 472
    :catch_9
    move-exception v0

    #@a
    .line 473
    .local v0, e:Ljava/lang/NumberFormatException;
    const-wide/16 v1, -0x1

    #@c
    goto :goto_8
.end method

.method public static getRadioVersion()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 462
    const-string v0, "gsm.version.baseband"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method private static getString(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "property"

    #@0
    .prologue
    .line 466
    const-string/jumbo v0, "unknown"

    #@3
    invoke-static {p0, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method
