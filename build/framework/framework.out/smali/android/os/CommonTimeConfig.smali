.class public Landroid/os/CommonTimeConfig;
.super Ljava/lang/Object;
.source "CommonTimeConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/CommonTimeConfig$OnServerDiedListener;
    }
.end annotation


# static fields
.field public static final ERROR:I = -0x1

.field public static final ERROR_BAD_VALUE:I = -0x4

.field public static final ERROR_DEAD_OBJECT:I = -0x7

.field public static final INVALID_GROUP_ID:J = -0x1L

.field private static final METHOD_FORCE_NETWORKLESS_MASTER_MODE:I = 0x11

.field private static final METHOD_GET_AUTO_DISABLE:I = 0xf

.field private static final METHOD_GET_CLIENT_SYNC_INTERVAL:I = 0xb

.field private static final METHOD_GET_INTERFACE_BINDING:I = 0x7

.field private static final METHOD_GET_MASTER_ANNOUNCE_INTERVAL:I = 0x9

.field private static final METHOD_GET_MASTER_ELECTION_ENDPOINT:I = 0x3

.field private static final METHOD_GET_MASTER_ELECTION_GROUP_ID:I = 0x5

.field private static final METHOD_GET_MASTER_ELECTION_PRIORITY:I = 0x1

.field private static final METHOD_GET_PANIC_THRESHOLD:I = 0xd

.field private static final METHOD_SET_AUTO_DISABLE:I = 0x10

.field private static final METHOD_SET_CLIENT_SYNC_INTERVAL:I = 0xc

.field private static final METHOD_SET_INTERFACE_BINDING:I = 0x8

.field private static final METHOD_SET_MASTER_ANNOUNCE_INTERVAL:I = 0xa

.field private static final METHOD_SET_MASTER_ELECTION_ENDPOINT:I = 0x4

.field private static final METHOD_SET_MASTER_ELECTION_GROUP_ID:I = 0x6

.field private static final METHOD_SET_MASTER_ELECTION_PRIORITY:I = 0x2

.field private static final METHOD_SET_PANIC_THRESHOLD:I = 0xe

.field public static final SERVICE_NAME:Ljava/lang/String; = "common_time.config"

.field public static final SUCCESS:I


# instance fields
.field private mDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private mInterfaceDesc:Ljava/lang/String;

.field private final mListenerLock:Ljava/lang/Object;

.field private mRemote:Landroid/os/IBinder;

.field private mServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

.field private mUtils:Landroid/os/CommonTimeUtils;


# direct methods
.method public constructor <init>()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 415
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mListenerLock:Ljava/lang/Object;

    #@b
    .line 416
    iput-object v1, p0, Landroid/os/CommonTimeConfig;->mServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

    #@d
    .line 418
    iput-object v1, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@f
    .line 419
    const-string v0, ""

    #@11
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mInterfaceDesc:Ljava/lang/String;

    #@13
    .line 422
    new-instance v0, Landroid/os/CommonTimeConfig$1;

    #@15
    invoke-direct {v0, p0}, Landroid/os/CommonTimeConfig$1;-><init>(Landroid/os/CommonTimeConfig;)V

    #@18
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@1a
    .line 66
    const-string v0, "common_time.config"

    #@1c
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@22
    .line 67
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@24
    if-nez v0, :cond_2c

    #@26
    .line 68
    new-instance v0, Landroid/os/RemoteException;

    #@28
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@2b
    throw v0

    #@2c
    .line 70
    :cond_2c
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@2e
    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mInterfaceDesc:Ljava/lang/String;

    #@34
    .line 71
    new-instance v0, Landroid/os/CommonTimeUtils;

    #@36
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@38
    iget-object v2, p0, Landroid/os/CommonTimeConfig;->mInterfaceDesc:Ljava/lang/String;

    #@3a
    invoke-direct {v0, v1, v2}, Landroid/os/CommonTimeUtils;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    #@3d
    iput-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@3f
    .line 72
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@41
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@43
    const/4 v2, 0x0

    #@44
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@47
    .line 73
    return-void
.end method

.method static synthetic access$000(Landroid/os/CommonTimeConfig;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/os/CommonTimeConfig;)Landroid/os/CommonTimeConfig$OnServerDiedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 31
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

    #@2
    return-object v0
.end method

.method private checkDeadServer()Z
    .registers 2

    #@0
    .prologue
    .line 407
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@6
    if-nez v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static create()Landroid/os/CommonTimeConfig;
    .registers 2

    #@0
    .prologue
    .line 82
    :try_start_0
    new-instance v1, Landroid/os/CommonTimeConfig;

    #@2
    invoke-direct {v1}, Landroid/os/CommonTimeConfig;-><init>()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 88
    .local v1, retVal:Landroid/os/CommonTimeConfig;
    :goto_5
    return-object v1

    #@6
    .line 84
    .end local v1           #retVal:Landroid/os/CommonTimeConfig;
    :catch_6
    move-exception v0

    #@7
    .line 85
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@8
    .restart local v1       #retVal:Landroid/os/CommonTimeConfig;
    goto :goto_5
.end method

.method private throwOnDeadServer()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 411
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 412
    new-instance v0, Landroid/os/RemoteException;

    #@8
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@b
    throw v0

    #@c
    .line 413
    :cond_c
    return-void
.end method


# virtual methods
.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 404
    invoke-virtual {p0}, Landroid/os/CommonTimeConfig;->release()V

    #@3
    return-void
.end method

.method public forceNetworklessMasterMode()I
    .registers 7

    #@0
    .prologue
    .line 360
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 361
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v2

    #@8
    .line 364
    .local v2, reply:Landroid/os/Parcel;
    :try_start_8
    iget-object v3, p0, Landroid/os/CommonTimeConfig;->mInterfaceDesc:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 365
    iget-object v3, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x11

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 367
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_29
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_18} :catch_20

    #@18
    move-result v3

    #@19
    .line 373
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@1c
    .line 374
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1f
    :goto_1f
    return v3

    #@20
    .line 369
    :catch_20
    move-exception v1

    #@21
    .line 370
    .local v1, e:Landroid/os/RemoteException;
    const/4 v3, -0x7

    #@22
    .line 373
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 374
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    goto :goto_1f

    #@29
    .line 373
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_29
    move-exception v3

    #@2a
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 374
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v3
.end method

.method public getAutoDisable()Z
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 322
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@4
    .line 323
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@6
    const/16 v2, 0xf

    #@8
    invoke-virtual {v1, v2, v0}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@b
    move-result v1

    #@c
    if-ne v0, v1, :cond_f

    #@e
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getClientSyncInterval()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 266
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 267
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0xb

    #@7
    const/4 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getInterfaceBinding()Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 202
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@4
    .line 204
    iget-object v2, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@6
    const/4 v3, 0x7

    #@7
    invoke-virtual {v2, v3, v1}, Landroid/os/CommonTimeUtils;->transactGetString(ILjava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 206
    .local v0, ifaceName:Ljava/lang/String;
    if-eqz v0, :cond_14

    #@d
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_14

    #@13
    move-object v0, v1

    #@14
    .line 209
    .end local v0           #ifaceName:Ljava/lang/String;
    :cond_14
    return-object v0
.end method

.method public getMasterAnnounceInterval()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 240
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0x9

    #@7
    const/4 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getMasterElectionEndpoint()Ljava/net/InetSocketAddress;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 147
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 148
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/4 v1, 0x3

    #@6
    invoke-virtual {v0, v1}, Landroid/os/CommonTimeUtils;->transactGetSockaddr(I)Ljava/net/InetSocketAddress;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getMasterElectionGroupId()J
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 176
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/4 v1, 0x5

    #@6
    const-wide/16 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/CommonTimeUtils;->transactGetLong(IJ)J

    #@b
    move-result-wide v0

    #@c
    return-wide v0
.end method

.method public getMasterElectionPriority()B
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 121
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/4 v1, 0x1

    #@6
    const/4 v2, -0x1

    #@7
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@a
    move-result v0

    #@b
    int-to-byte v0, v0

    #@c
    return v0
.end method

.method public getPanicThreshold()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 294
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->throwOnDeadServer()V

    #@3
    .line 295
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0xd

    #@7
    const/4 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public release()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 101
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@3
    if-eqz v0, :cond_f

    #@5
    .line 103
    :try_start_5
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@7
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@9
    const/4 v2, 0x0

    #@a
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_d
    .catch Ljava/util/NoSuchElementException; {:try_start_5 .. :try_end_d} :catch_12

    #@d
    .line 106
    :goto_d
    iput-object v3, p0, Landroid/os/CommonTimeConfig;->mRemote:Landroid/os/IBinder;

    #@f
    .line 108
    :cond_f
    iput-object v3, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@11
    .line 109
    return-void

    #@12
    .line 105
    :catch_12
    move-exception v0

    #@13
    goto :goto_d
.end method

.method public setAutoDisable(Z)I
    .registers 5
    .parameter "autoDisable"

    #@0
    .prologue
    .line 338
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 339
    const/4 v0, -0x7

    #@7
    .line 341
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/16 v2, 0x10

    #@c
    if-eqz p1, :cond_14

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-virtual {v1, v2, v0}, Landroid/os/CommonTimeUtils;->transactSetInt(II)I

    #@12
    move-result v0

    #@13
    goto :goto_7

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_f
.end method

.method public setClientSyncInterval(I)I
    .registers 4
    .parameter "interval"

    #@0
    .prologue
    .line 279
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 280
    const/4 v0, -0x7

    #@7
    .line 281
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/16 v1, 0xc

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetInt(II)I

    #@f
    move-result v0

    #@10
    goto :goto_7
.end method

.method public setMasterAnnounceInterval(I)I
    .registers 4
    .parameter "interval"

    #@0
    .prologue
    .line 252
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 253
    const/4 v0, -0x7

    #@7
    .line 254
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/16 v1, 0xa

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetInt(II)I

    #@f
    move-result v0

    #@10
    goto :goto_7
.end method

.method public setMasterElectionEndpoint(Ljava/net/InetSocketAddress;)I
    .registers 4
    .parameter "ep"

    #@0
    .prologue
    .line 162
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 163
    const/4 v0, -0x7

    #@7
    .line 164
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/4 v1, 0x4

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetSockaddr(ILjava/net/InetSocketAddress;)I

    #@e
    move-result v0

    #@f
    goto :goto_7
.end method

.method public setMasterElectionGroupId(J)I
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 187
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 188
    const/4 v0, -0x7

    #@7
    .line 189
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/4 v1, 0x6

    #@b
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/CommonTimeUtils;->transactSetLong(IJ)I

    #@e
    move-result v0

    #@f
    goto :goto_7
.end method

.method public setMasterElectionPriority(B)I
    .registers 4
    .parameter "priority"

    #@0
    .prologue
    .line 133
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 134
    const/4 v0, -0x7

    #@7
    .line 135
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/4 v1, 0x2

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetInt(II)I

    #@e
    move-result v0

    #@f
    goto :goto_7
.end method

.method public setNetworkBinding(Ljava/lang/String;)I
    .registers 4
    .parameter "ifaceName"

    #@0
    .prologue
    .line 223
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 224
    const/4 v0, -0x7

    #@7
    .line 226
    .end local p1
    :goto_7
    return v0

    #@8
    .restart local p1
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/16 v1, 0x8

    #@c
    if-nez p1, :cond_10

    #@e
    const-string p1, ""

    #@10
    .end local p1
    :cond_10
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetString(ILjava/lang/String;)I

    #@13
    move-result v0

    #@14
    goto :goto_7
.end method

.method public setPanicThreshold(I)I
    .registers 4
    .parameter "threshold"

    #@0
    .prologue
    .line 309
    invoke-direct {p0}, Landroid/os/CommonTimeConfig;->checkDeadServer()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 310
    const/4 v0, -0x7

    #@7
    .line 311
    :goto_7
    return v0

    #@8
    :cond_8
    iget-object v0, p0, Landroid/os/CommonTimeConfig;->mUtils:Landroid/os/CommonTimeUtils;

    #@a
    const/16 v1, 0xe

    #@c
    invoke-virtual {v0, v1, p1}, Landroid/os/CommonTimeUtils;->transactSetInt(II)I

    #@f
    move-result v0

    #@10
    goto :goto_7
.end method

.method public setServerDiedListener(Landroid/os/CommonTimeConfig$OnServerDiedListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 399
    iget-object v1, p0, Landroid/os/CommonTimeConfig;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 400
    :try_start_3
    iput-object p1, p0, Landroid/os/CommonTimeConfig;->mServerDiedListener:Landroid/os/CommonTimeConfig$OnServerDiedListener;

    #@5
    .line 401
    monitor-exit v1

    #@6
    .line 402
    return-void

    #@7
    .line 401
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method
