.class public Landroid/os/MessageQueue;
.super Ljava/lang/Object;
.source "MessageQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/MessageQueue$IdleHandler;
    }
.end annotation


# instance fields
.field private mBlocked:Z

.field private final mIdleHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/MessageQueue$IdleHandler;",
            ">;"
        }
    .end annotation
.end field

.field mMessages:Landroid/os/Message;

.field private mNextBarrierToken:I

.field private mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

.field private mPtr:I

.field private final mQuitAllowed:Z

.field private mQuiting:Z


# direct methods
.method constructor <init>(Z)V
    .registers 3
    .parameter "quitAllowed"

    #@0
    .prologue
    .line 103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@a
    .line 104
    iput-boolean p1, p0, Landroid/os/MessageQueue;->mQuitAllowed:Z

    #@c
    .line 105
    invoke-direct {p0}, Landroid/os/MessageQueue;->nativeInit()V

    #@f
    .line 106
    return-void
.end method

.method private native nativeDestroy()V
.end method

.method private native nativeInit()V
.end method

.method private native nativePollOnce(II)V
.end method

.method private native nativeWake(I)V
.end method


# virtual methods
.method public final addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V
    .registers 4
    .parameter "handler"

    #@0
    .prologue
    .line 82
    if-nez p1, :cond_a

    #@2
    .line 83
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "Can\'t add a null IdleHandler"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 85
    :cond_a
    monitor-enter p0

    #@b
    .line 86
    :try_start_b
    iget-object v0, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@10
    .line 87
    monitor-exit p0

    #@11
    .line 88
    return-void

    #@12
    .line 87
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_b .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method final enqueueMessage(Landroid/os/Message;J)Z
    .registers 11
    .parameter "msg"
    .parameter "when"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 284
    invoke-virtual {p1}, Landroid/os/Message;->isInUse()Z

    #@5
    move-result v5

    #@6
    if-eqz v5, :cond_21

    #@8
    .line 285
    new-instance v4, Landroid/util/AndroidRuntimeException;

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    const-string v6, " This message is already in use."

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v5

    #@1d
    invoke-direct {v4, v5}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@20
    throw v4

    #@21
    .line 287
    :cond_21
    iget-object v5, p1, Landroid/os/Message;->target:Landroid/os/Handler;

    #@23
    if-nez v5, :cond_2d

    #@25
    .line 288
    new-instance v4, Landroid/util/AndroidRuntimeException;

    #@27
    const-string v5, "Message must have a target."

    #@29
    invoke-direct {v4, v5}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    #@2c
    throw v4

    #@2d
    .line 292
    :cond_2d
    monitor-enter p0

    #@2e
    .line 293
    :try_start_2e
    iget-boolean v5, p0, Landroid/os/MessageQueue;->mQuiting:Z

    #@30
    if-eqz v5, :cond_57

    #@32
    .line 294
    new-instance v0, Ljava/lang/RuntimeException;

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    iget-object v5, p1, Landroid/os/Message;->target:Landroid/os/Handler;

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, " sending message to a Handler on a dead thread"

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-direct {v0, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@4c
    .line 296
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v4, "MessageQueue"

    #@4e
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@55
    .line 297
    monitor-exit p0

    #@56
    .line 330
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_56
    return v1

    #@57
    .line 300
    :cond_57
    iput-wide p2, p1, Landroid/os/Message;->when:J

    #@59
    .line 301
    iget-object v2, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@5b
    .line 302
    .local v2, p:Landroid/os/Message;
    if-eqz v2, :cond_69

    #@5d
    const-wide/16 v5, 0x0

    #@5f
    cmp-long v5, p2, v5

    #@61
    if-eqz v5, :cond_69

    #@63
    iget-wide v5, v2, Landroid/os/Message;->when:J

    #@65
    cmp-long v5, p2, v5

    #@67
    if-gez v5, :cond_79

    #@69
    .line 304
    :cond_69
    iput-object v2, p1, Landroid/os/Message;->next:Landroid/os/Message;

    #@6b
    .line 305
    iput-object p1, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@6d
    .line 306
    iget-boolean v1, p0, Landroid/os/MessageQueue;->mBlocked:Z

    #@6f
    .line 326
    .local v1, needWake:Z
    :goto_6f
    monitor-exit p0
    :try_end_70
    .catchall {:try_start_2e .. :try_end_70} :catchall_98

    #@70
    .line 327
    if-eqz v1, :cond_77

    #@72
    .line 328
    iget v5, p0, Landroid/os/MessageQueue;->mPtr:I

    #@74
    invoke-direct {p0, v5}, Landroid/os/MessageQueue;->nativeWake(I)V

    #@77
    :cond_77
    move v1, v4

    #@78
    .line 330
    goto :goto_56

    #@79
    .line 311
    .end local v1           #needWake:Z
    :cond_79
    :try_start_79
    iget-boolean v5, p0, Landroid/os/MessageQueue;->mBlocked:Z

    #@7b
    if-eqz v5, :cond_88

    #@7d
    iget-object v5, v2, Landroid/os/Message;->target:Landroid/os/Handler;

    #@7f
    if-nez v5, :cond_88

    #@81
    invoke-virtual {p1}, Landroid/os/Message;->isAsynchronous()Z

    #@84
    move-result v5

    #@85
    if-eqz v5, :cond_88

    #@87
    move v1, v4

    #@88
    .line 314
    .restart local v1       #needWake:Z
    :cond_88
    :goto_88
    move-object v3, v2

    #@89
    .line 315
    .local v3, prev:Landroid/os/Message;
    iget-object v2, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@8b
    .line 316
    if-eqz v2, :cond_93

    #@8d
    iget-wide v5, v2, Landroid/os/Message;->when:J

    #@8f
    cmp-long v5, p2, v5

    #@91
    if-gez v5, :cond_9b

    #@93
    .line 323
    :cond_93
    iput-object v2, p1, Landroid/os/Message;->next:Landroid/os/Message;

    #@95
    .line 324
    iput-object p1, v3, Landroid/os/Message;->next:Landroid/os/Message;

    #@97
    goto :goto_6f

    #@98
    .line 326
    .end local v1           #needWake:Z
    .end local v2           #p:Landroid/os/Message;
    .end local v3           #prev:Landroid/os/Message;
    :catchall_98
    move-exception v4

    #@99
    monitor-exit p0
    :try_end_9a
    .catchall {:try_start_79 .. :try_end_9a} :catchall_98

    #@9a
    throw v4

    #@9b
    .line 319
    .restart local v1       #needWake:Z
    .restart local v2       #p:Landroid/os/Message;
    .restart local v3       #prev:Landroid/os/Message;
    :cond_9b
    if-eqz v1, :cond_88

    #@9d
    :try_start_9d
    invoke-virtual {v2}, Landroid/os/Message;->isAsynchronous()Z
    :try_end_a0
    .catchall {:try_start_9d .. :try_end_a0} :catchall_98

    #@a0
    move-result v5

    #@a1
    if-eqz v5, :cond_88

    #@a3
    .line 320
    const/4 v1, 0x0

    #@a4
    goto :goto_88
.end method

.method final enqueueSyncBarrier(J)I
    .registers 9
    .parameter "when"

    #@0
    .prologue
    .line 230
    monitor-enter p0

    #@1
    .line 231
    :try_start_1
    iget v3, p0, Landroid/os/MessageQueue;->mNextBarrierToken:I

    #@3
    add-int/lit8 v4, v3, 0x1

    #@5
    iput v4, p0, Landroid/os/MessageQueue;->mNextBarrierToken:I

    #@7
    .line 232
    .local v3, token:I
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 233
    .local v0, msg:Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->arg1:I

    #@d
    .line 235
    const/4 v2, 0x0

    #@e
    .line 236
    .local v2, prev:Landroid/os/Message;
    iget-object v1, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@10
    .line 237
    .local v1, p:Landroid/os/Message;
    const-wide/16 v4, 0x0

    #@12
    cmp-long v4, p1, v4

    #@14
    if-eqz v4, :cond_22

    #@16
    .line 238
    :goto_16
    if-eqz v1, :cond_22

    #@18
    iget-wide v4, v1, Landroid/os/Message;->when:J

    #@1a
    cmp-long v4, v4, p1

    #@1c
    if-gtz v4, :cond_22

    #@1e
    .line 239
    move-object v2, v1

    #@1f
    .line 240
    iget-object v1, v1, Landroid/os/Message;->next:Landroid/os/Message;

    #@21
    goto :goto_16

    #@22
    .line 243
    :cond_22
    if-eqz v2, :cond_2a

    #@24
    .line 244
    iput-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@26
    .line 245
    iput-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@28
    .line 250
    :goto_28
    monitor-exit p0

    #@29
    return v3

    #@2a
    .line 247
    :cond_2a
    iput-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@2c
    .line 248
    iput-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@2e
    goto :goto_28

    #@2f
    .line 251
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #p:Landroid/os/Message;
    .end local v2           #prev:Landroid/os/Message;
    .end local v3           #token:I
    :catchall_2f
    move-exception v4

    #@30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_1 .. :try_end_31} :catchall_2f

    #@31
    throw v4
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 111
    :try_start_0
    invoke-direct {p0}, Landroid/os/MessageQueue;->nativeDestroy()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 113
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 115
    return-void

    #@7
    .line 113
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method final hasMessages(Landroid/os/Handler;ILjava/lang/Object;)Z
    .registers 7
    .parameter "h"
    .parameter "what"
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 334
    if-nez p1, :cond_4

    #@3
    .line 346
    :goto_3
    return v1

    #@4
    .line 338
    :cond_4
    monitor-enter p0

    #@5
    .line 339
    :try_start_5
    iget-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@7
    .line 340
    .local v0, p:Landroid/os/Message;
    :goto_7
    if-eqz v0, :cond_20

    #@9
    .line 341
    iget-object v2, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@b
    if-ne v2, p1, :cond_1d

    #@d
    iget v2, v0, Landroid/os/Message;->what:I

    #@f
    if-ne v2, p2, :cond_1d

    #@11
    if-eqz p3, :cond_17

    #@13
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15
    if-ne v2, p3, :cond_1d

    #@17
    .line 342
    :cond_17
    const/4 v1, 0x1

    #@18
    monitor-exit p0

    #@19
    goto :goto_3

    #@1a
    .line 347
    .end local v0           #p:Landroid/os/Message;
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_5 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1

    #@1d
    .line 344
    .restart local v0       #p:Landroid/os/Message;
    :cond_1d
    :try_start_1d
    iget-object v0, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@1f
    goto :goto_7

    #@20
    .line 346
    :cond_20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1d .. :try_end_21} :catchall_1a

    #@21
    goto :goto_3
.end method

.method final hasMessages(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)Z
    .registers 7
    .parameter "h"
    .parameter "r"
    .parameter "object"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 351
    if-nez p1, :cond_4

    #@3
    .line 363
    :goto_3
    return v1

    #@4
    .line 355
    :cond_4
    monitor-enter p0

    #@5
    .line 356
    :try_start_5
    iget-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@7
    .line 357
    .local v0, p:Landroid/os/Message;
    :goto_7
    if-eqz v0, :cond_20

    #@9
    .line 358
    iget-object v2, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@b
    if-ne v2, p1, :cond_1d

    #@d
    iget-object v2, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@f
    if-ne v2, p2, :cond_1d

    #@11
    if-eqz p3, :cond_17

    #@13
    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@15
    if-ne v2, p3, :cond_1d

    #@17
    .line 359
    :cond_17
    const/4 v1, 0x1

    #@18
    monitor-exit p0

    #@19
    goto :goto_3

    #@1a
    .line 364
    .end local v0           #p:Landroid/os/Message;
    :catchall_1a
    move-exception v1

    #@1b
    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_5 .. :try_end_1c} :catchall_1a

    #@1c
    throw v1

    #@1d
    .line 361
    .restart local v0       #p:Landroid/os/Message;
    :cond_1d
    :try_start_1d
    iget-object v0, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@1f
    goto :goto_7

    #@20
    .line 363
    :cond_20
    monitor-exit p0
    :try_end_21
    .catchall {:try_start_1d .. :try_end_21} :catchall_1a

    #@21
    goto :goto_3
.end method

.method final next()Landroid/os/Message;
    .registers 15

    #@0
    .prologue
    .line 118
    const/4 v7, -0x1

    #@1
    .line 119
    .local v7, pendingIdleHandlerCount:I
    const/4 v4, 0x0

    #@2
    .line 122
    .local v4, nextPollTimeoutMillis:I
    :goto_2
    if-eqz v4, :cond_7

    #@4
    .line 123
    invoke-static {}, Landroid/os/Binder;->flushPendingCommands()V

    #@7
    .line 125
    :cond_7
    iget v10, p0, Landroid/os/MessageQueue;->mPtr:I

    #@9
    invoke-direct {p0, v10, v4}, Landroid/os/MessageQueue;->nativePollOnce(II)V

    #@c
    .line 127
    monitor-enter p0

    #@d
    .line 128
    :try_start_d
    iget-boolean v10, p0, Landroid/os/MessageQueue;->mQuiting:Z

    #@f
    if-eqz v10, :cond_14

    #@11
    .line 129
    const/4 v3, 0x0

    #@12
    monitor-exit p0

    #@13
    .line 158
    :goto_13
    return-object v3

    #@14
    .line 133
    :cond_14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v5

    #@18
    .line 134
    .local v5, now:J
    const/4 v8, 0x0

    #@19
    .line 135
    .local v8, prevMsg:Landroid/os/Message;
    iget-object v3, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@1b
    .line 136
    .local v3, msg:Landroid/os/Message;
    if-eqz v3, :cond_2c

    #@1d
    iget-object v10, v3, Landroid/os/Message;->target:Landroid/os/Handler;

    #@1f
    if-nez v10, :cond_2c

    #@21
    .line 139
    :cond_21
    move-object v8, v3

    #@22
    .line 140
    iget-object v3, v3, Landroid/os/Message;->next:Landroid/os/Message;

    #@24
    .line 141
    if-eqz v3, :cond_2c

    #@26
    invoke-virtual {v3}, Landroid/os/Message;->isAsynchronous()Z

    #@29
    move-result v10

    #@2a
    if-eqz v10, :cond_21

    #@2c
    .line 143
    :cond_2c
    if-eqz v3, :cond_73

    #@2e
    .line 144
    iget-wide v10, v3, Landroid/os/Message;->when:J

    #@30
    cmp-long v10, v5, v10

    #@32
    if-gez v10, :cond_5d

    #@34
    .line 146
    iget-wide v10, v3, Landroid/os/Message;->when:J

    #@36
    sub-long/2addr v10, v5

    #@37
    const-wide/32 v12, 0x7fffffff

    #@3a
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    #@3d
    move-result-wide v10

    #@3e
    long-to-int v4, v10

    #@3f
    .line 168
    :goto_3f
    if-gez v7, :cond_53

    #@41
    iget-object v10, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@43
    if-eqz v10, :cond_4d

    #@45
    iget-object v10, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@47
    iget-wide v10, v10, Landroid/os/Message;->when:J

    #@49
    cmp-long v10, v5, v10

    #@4b
    if-gez v10, :cond_53

    #@4d
    .line 170
    :cond_4d
    iget-object v10, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@4f
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@52
    move-result v7

    #@53
    .line 172
    :cond_53
    if-gtz v7, :cond_75

    #@55
    .line 174
    const/4 v10, 0x1

    #@56
    iput-boolean v10, p0, Landroid/os/MessageQueue;->mBlocked:Z

    #@58
    .line 175
    monitor-exit p0

    #@59
    goto :goto_2

    #@5a
    .line 182
    .end local v3           #msg:Landroid/os/Message;
    .end local v5           #now:J
    .end local v8           #prevMsg:Landroid/os/Message;
    :catchall_5a
    move-exception v10

    #@5b
    monitor-exit p0
    :try_end_5c
    .catchall {:try_start_d .. :try_end_5c} :catchall_5a

    #@5c
    throw v10

    #@5d
    .line 149
    .restart local v3       #msg:Landroid/os/Message;
    .restart local v5       #now:J
    .restart local v8       #prevMsg:Landroid/os/Message;
    :cond_5d
    const/4 v10, 0x0

    #@5e
    :try_start_5e
    iput-boolean v10, p0, Landroid/os/MessageQueue;->mBlocked:Z

    #@60
    .line 150
    if-eqz v8, :cond_6e

    #@62
    .line 151
    iget-object v10, v3, Landroid/os/Message;->next:Landroid/os/Message;

    #@64
    iput-object v10, v8, Landroid/os/Message;->next:Landroid/os/Message;

    #@66
    .line 155
    :goto_66
    const/4 v10, 0x0

    #@67
    iput-object v10, v3, Landroid/os/Message;->next:Landroid/os/Message;

    #@69
    .line 157
    invoke-virtual {v3}, Landroid/os/Message;->markInUse()V

    #@6c
    .line 158
    monitor-exit p0

    #@6d
    goto :goto_13

    #@6e
    .line 153
    :cond_6e
    iget-object v10, v3, Landroid/os/Message;->next:Landroid/os/Message;

    #@70
    iput-object v10, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@72
    goto :goto_66

    #@73
    .line 162
    :cond_73
    const/4 v4, -0x1

    #@74
    goto :goto_3f

    #@75
    .line 178
    :cond_75
    iget-object v10, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@77
    if-nez v10, :cond_82

    #@79
    .line 179
    const/4 v10, 0x4

    #@7a
    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    #@7d
    move-result v10

    #@7e
    new-array v10, v10, [Landroid/os/MessageQueue$IdleHandler;

    #@80
    iput-object v10, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@82
    .line 181
    :cond_82
    iget-object v10, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@84
    iget-object v11, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@86
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@89
    move-result-object v10

    #@8a
    check-cast v10, [Landroid/os/MessageQueue$IdleHandler;

    #@8c
    iput-object v10, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@8e
    .line 182
    monitor-exit p0
    :try_end_8f
    .catchall {:try_start_5e .. :try_end_8f} :catchall_5a

    #@8f
    .line 186
    const/4 v0, 0x0

    #@90
    .local v0, i:I
    :goto_90
    if-ge v0, v7, :cond_b8

    #@92
    .line 187
    iget-object v10, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@94
    aget-object v1, v10, v0

    #@96
    .line 188
    .local v1, idler:Landroid/os/MessageQueue$IdleHandler;
    iget-object v10, p0, Landroid/os/MessageQueue;->mPendingIdleHandlers:[Landroid/os/MessageQueue$IdleHandler;

    #@98
    const/4 v11, 0x0

    #@99
    aput-object v11, v10, v0

    #@9b
    .line 190
    const/4 v2, 0x0

    #@9c
    .line 192
    .local v2, keep:Z
    :try_start_9c
    invoke-interface {v1}, Landroid/os/MessageQueue$IdleHandler;->queueIdle()Z
    :try_end_9f
    .catch Ljava/lang/Throwable; {:try_start_9c .. :try_end_9f} :catch_ac

    #@9f
    move-result v2

    #@a0
    .line 197
    :goto_a0
    if-nez v2, :cond_a9

    #@a2
    .line 198
    monitor-enter p0

    #@a3
    .line 199
    :try_start_a3
    iget-object v10, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@a5
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@a8
    .line 200
    monitor-exit p0
    :try_end_a9
    .catchall {:try_start_a3 .. :try_end_a9} :catchall_b5

    #@a9
    .line 186
    :cond_a9
    add-int/lit8 v0, v0, 0x1

    #@ab
    goto :goto_90

    #@ac
    .line 193
    :catch_ac
    move-exception v9

    #@ad
    .line 194
    .local v9, t:Ljava/lang/Throwable;
    const-string v10, "MessageQueue"

    #@af
    const-string v11, "IdleHandler threw exception"

    #@b1
    invoke-static {v10, v11, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    goto :goto_a0

    #@b5
    .line 200
    .end local v9           #t:Ljava/lang/Throwable;
    :catchall_b5
    move-exception v10

    #@b6
    :try_start_b6
    monitor-exit p0
    :try_end_b7
    .catchall {:try_start_b6 .. :try_end_b7} :catchall_b5

    #@b7
    throw v10

    #@b8
    .line 205
    .end local v1           #idler:Landroid/os/MessageQueue$IdleHandler;
    .end local v2           #keep:Z
    :cond_b8
    const/4 v7, 0x0

    #@b9
    .line 209
    const/4 v4, 0x0

    #@ba
    goto/16 :goto_2
.end method

.method final quit()V
    .registers 3

    #@0
    .prologue
    .line 214
    iget-boolean v0, p0, Landroid/os/MessageQueue;->mQuitAllowed:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 215
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "Main thread not allowed to quit."

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 218
    :cond_c
    monitor-enter p0

    #@d
    .line 219
    :try_start_d
    iget-boolean v0, p0, Landroid/os/MessageQueue;->mQuiting:Z

    #@f
    if-eqz v0, :cond_13

    #@11
    .line 220
    monitor-exit p0

    #@12
    .line 225
    :goto_12
    return-void

    #@13
    .line 222
    :cond_13
    const/4 v0, 0x1

    #@14
    iput-boolean v0, p0, Landroid/os/MessageQueue;->mQuiting:Z

    #@16
    .line 223
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_d .. :try_end_17} :catchall_1d

    #@17
    .line 224
    iget v0, p0, Landroid/os/MessageQueue;->mPtr:I

    #@19
    invoke-direct {p0, v0}, Landroid/os/MessageQueue;->nativeWake(I)V

    #@1c
    goto :goto_12

    #@1d
    .line 223
    :catchall_1d
    move-exception v0

    #@1e
    :try_start_1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method final removeCallbacksAndMessages(Landroid/os/Handler;Ljava/lang/Object;)V
    .registers 7
    .parameter "h"
    .parameter "object"

    #@0
    .prologue
    .line 436
    if-nez p1, :cond_3

    #@2
    .line 466
    :goto_2
    return-void

    #@3
    .line 440
    :cond_3
    monitor-enter p0

    #@4
    .line 441
    :try_start_4
    iget-object v2, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@6
    .line 445
    .local v2, p:Landroid/os/Message;
    :goto_6
    if-eqz v2, :cond_1b

    #@8
    iget-object v3, v2, Landroid/os/Message;->target:Landroid/os/Handler;

    #@a
    if-ne v3, p1, :cond_1b

    #@c
    if-eqz p2, :cond_12

    #@e
    iget-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10
    if-ne v3, p2, :cond_1b

    #@12
    .line 446
    :cond_12
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@14
    .line 447
    .local v0, n:Landroid/os/Message;
    iput-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@16
    .line 448
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    #@19
    .line 449
    move-object v2, v0

    #@1a
    .line 450
    goto :goto_6

    #@1b
    .line 453
    .end local v0           #n:Landroid/os/Message;
    :cond_1b
    :goto_1b
    if-eqz v2, :cond_38

    #@1d
    .line 454
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@1f
    .line 455
    .restart local v0       #n:Landroid/os/Message;
    if-eqz v0, :cond_36

    #@21
    .line 456
    iget-object v3, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@23
    if-ne v3, p1, :cond_36

    #@25
    if-eqz p2, :cond_2b

    #@27
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29
    if-ne v3, p2, :cond_36

    #@2b
    .line 457
    :cond_2b
    iget-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@2d
    .line 458
    .local v1, nn:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    #@30
    .line 459
    iput-object v1, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@32
    goto :goto_1b

    #@33
    .line 465
    .end local v0           #n:Landroid/os/Message;
    .end local v1           #nn:Landroid/os/Message;
    .end local v2           #p:Landroid/os/Message;
    :catchall_33
    move-exception v3

    #@34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_4 .. :try_end_35} :catchall_33

    #@35
    throw v3

    #@36
    .line 463
    .restart local v0       #n:Landroid/os/Message;
    .restart local v2       #p:Landroid/os/Message;
    :cond_36
    move-object v2, v0

    #@37
    .line 464
    goto :goto_1b

    #@38
    .line 465
    .end local v0           #n:Landroid/os/Message;
    :cond_38
    :try_start_38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_38 .. :try_end_39} :catchall_33

    #@39
    goto :goto_2
.end method

.method public final removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V
    .registers 3
    .parameter "handler"

    #@0
    .prologue
    .line 98
    monitor-enter p0

    #@1
    .line 99
    :try_start_1
    iget-object v0, p0, Landroid/os/MessageQueue;->mIdleHandlers:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@6
    .line 100
    monitor-exit p0

    #@7
    .line 101
    return-void

    #@8
    .line 100
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method final removeMessages(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "what"
    .parameter "object"

    #@0
    .prologue
    .line 368
    if-nez p1, :cond_3

    #@2
    .line 399
    :goto_2
    return-void

    #@3
    .line 372
    :cond_3
    monitor-enter p0

    #@4
    .line 373
    :try_start_4
    iget-object v2, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@6
    .line 377
    .local v2, p:Landroid/os/Message;
    :goto_6
    if-eqz v2, :cond_1f

    #@8
    iget-object v3, v2, Landroid/os/Message;->target:Landroid/os/Handler;

    #@a
    if-ne v3, p1, :cond_1f

    #@c
    iget v3, v2, Landroid/os/Message;->what:I

    #@e
    if-ne v3, p2, :cond_1f

    #@10
    if-eqz p3, :cond_16

    #@12
    iget-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14
    if-ne v3, p3, :cond_1f

    #@16
    .line 378
    :cond_16
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@18
    .line 379
    .local v0, n:Landroid/os/Message;
    iput-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@1a
    .line 380
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    #@1d
    .line 381
    move-object v2, v0

    #@1e
    .line 382
    goto :goto_6

    #@1f
    .line 385
    .end local v0           #n:Landroid/os/Message;
    :cond_1f
    :goto_1f
    if-eqz v2, :cond_40

    #@21
    .line 386
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@23
    .line 387
    .restart local v0       #n:Landroid/os/Message;
    if-eqz v0, :cond_3e

    #@25
    .line 388
    iget-object v3, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@27
    if-ne v3, p1, :cond_3e

    #@29
    iget v3, v0, Landroid/os/Message;->what:I

    #@2b
    if-ne v3, p2, :cond_3e

    #@2d
    if-eqz p3, :cond_33

    #@2f
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@31
    if-ne v3, p3, :cond_3e

    #@33
    .line 390
    :cond_33
    iget-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@35
    .line 391
    .local v1, nn:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    #@38
    .line 392
    iput-object v1, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@3a
    goto :goto_1f

    #@3b
    .line 398
    .end local v0           #n:Landroid/os/Message;
    .end local v1           #nn:Landroid/os/Message;
    .end local v2           #p:Landroid/os/Message;
    :catchall_3b
    move-exception v3

    #@3c
    monitor-exit p0
    :try_end_3d
    .catchall {:try_start_4 .. :try_end_3d} :catchall_3b

    #@3d
    throw v3

    #@3e
    .line 396
    .restart local v0       #n:Landroid/os/Message;
    .restart local v2       #p:Landroid/os/Message;
    :cond_3e
    move-object v2, v0

    #@3f
    .line 397
    goto :goto_1f

    #@40
    .line 398
    .end local v0           #n:Landroid/os/Message;
    :cond_40
    :try_start_40
    monitor-exit p0
    :try_end_41
    .catchall {:try_start_40 .. :try_end_41} :catchall_3b

    #@41
    goto :goto_2
.end method

.method final removeMessages(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V
    .registers 8
    .parameter "h"
    .parameter "r"
    .parameter "object"

    #@0
    .prologue
    .line 402
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 433
    :cond_4
    :goto_4
    return-void

    #@5
    .line 406
    :cond_5
    monitor-enter p0

    #@6
    .line 407
    :try_start_6
    iget-object v2, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@8
    .line 411
    .local v2, p:Landroid/os/Message;
    :goto_8
    if-eqz v2, :cond_21

    #@a
    iget-object v3, v2, Landroid/os/Message;->target:Landroid/os/Handler;

    #@c
    if-ne v3, p1, :cond_21

    #@e
    iget-object v3, v2, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@10
    if-ne v3, p2, :cond_21

    #@12
    if-eqz p3, :cond_18

    #@14
    iget-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16
    if-ne v3, p3, :cond_21

    #@18
    .line 412
    :cond_18
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@1a
    .line 413
    .local v0, n:Landroid/os/Message;
    iput-object v0, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@1c
    .line 414
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    #@1f
    .line 415
    move-object v2, v0

    #@20
    .line 416
    goto :goto_8

    #@21
    .line 419
    .end local v0           #n:Landroid/os/Message;
    :cond_21
    :goto_21
    if-eqz v2, :cond_42

    #@23
    .line 420
    iget-object v0, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@25
    .line 421
    .restart local v0       #n:Landroid/os/Message;
    if-eqz v0, :cond_40

    #@27
    .line 422
    iget-object v3, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@29
    if-ne v3, p1, :cond_40

    #@2b
    iget-object v3, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2d
    if-ne v3, p2, :cond_40

    #@2f
    if-eqz p3, :cond_35

    #@31
    iget-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@33
    if-ne v3, p3, :cond_40

    #@35
    .line 424
    :cond_35
    iget-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@37
    .line 425
    .local v1, nn:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->recycle()V

    #@3a
    .line 426
    iput-object v1, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@3c
    goto :goto_21

    #@3d
    .line 432
    .end local v0           #n:Landroid/os/Message;
    .end local v1           #nn:Landroid/os/Message;
    .end local v2           #p:Landroid/os/Message;
    :catchall_3d
    move-exception v3

    #@3e
    monitor-exit p0
    :try_end_3f
    .catchall {:try_start_6 .. :try_end_3f} :catchall_3d

    #@3f
    throw v3

    #@40
    .line 430
    .restart local v0       #n:Landroid/os/Message;
    .restart local v2       #p:Landroid/os/Message;
    :cond_40
    move-object v2, v0

    #@41
    .line 431
    goto :goto_21

    #@42
    .line 432
    .end local v0           #n:Landroid/os/Message;
    :cond_42
    :try_start_42
    monitor-exit p0
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_3d

    #@43
    goto :goto_4
.end method

.method final removeSyncBarrier(I)V
    .registers 7
    .parameter "token"

    #@0
    .prologue
    .line 258
    monitor-enter p0

    #@1
    .line 259
    const/4 v2, 0x0

    #@2
    .line 260
    .local v2, prev:Landroid/os/Message;
    :try_start_2
    iget-object v1, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@4
    .line 261
    .local v1, p:Landroid/os/Message;
    :goto_4
    if-eqz v1, :cond_12

    #@6
    iget-object v3, v1, Landroid/os/Message;->target:Landroid/os/Handler;

    #@8
    if-nez v3, :cond_e

    #@a
    iget v3, v1, Landroid/os/Message;->arg1:I

    #@c
    if-eq v3, p1, :cond_12

    #@e
    .line 262
    :cond_e
    move-object v2, v1

    #@f
    .line 263
    iget-object v1, v1, Landroid/os/Message;->next:Landroid/os/Message;

    #@11
    goto :goto_4

    #@12
    .line 265
    :cond_12
    if-nez v1, :cond_1f

    #@14
    .line 266
    new-instance v3, Ljava/lang/IllegalStateException;

    #@16
    const-string v4, "The specified message queue synchronization  barrier token has not been posted or has already been removed."

    #@18
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v3

    #@1c
    .line 277
    .end local v1           #p:Landroid/os/Message;
    :catchall_1c
    move-exception v3

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_2 .. :try_end_1e} :catchall_1c

    #@1e
    throw v3

    #@1f
    .line 269
    .restart local v1       #p:Landroid/os/Message;
    :cond_1f
    if-eqz v2, :cond_32

    #@21
    .line 270
    :try_start_21
    iget-object v3, v1, Landroid/os/Message;->next:Landroid/os/Message;

    #@23
    iput-object v3, v2, Landroid/os/Message;->next:Landroid/os/Message;

    #@25
    .line 271
    const/4 v0, 0x0

    #@26
    .line 276
    .local v0, needWake:Z
    :goto_26
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    #@29
    .line 277
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_1c

    #@2a
    .line 278
    if-eqz v0, :cond_31

    #@2c
    .line 279
    iget v3, p0, Landroid/os/MessageQueue;->mPtr:I

    #@2e
    invoke-direct {p0, v3}, Landroid/os/MessageQueue;->nativeWake(I)V

    #@31
    .line 281
    :cond_31
    return-void

    #@32
    .line 273
    .end local v0           #needWake:Z
    :cond_32
    :try_start_32
    iget-object v3, v1, Landroid/os/Message;->next:Landroid/os/Message;

    #@34
    iput-object v3, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@36
    .line 274
    iget-object v3, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@38
    if-eqz v3, :cond_40

    #@3a
    iget-object v3, p0, Landroid/os/MessageQueue;->mMessages:Landroid/os/Message;

    #@3c
    iget-object v3, v3, Landroid/os/Message;->target:Landroid/os/Handler;
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_1c

    #@3e
    if-eqz v3, :cond_42

    #@40
    :cond_40
    const/4 v0, 0x1

    #@41
    .restart local v0       #needWake:Z
    :goto_41
    goto :goto_26

    #@42
    .end local v0           #needWake:Z
    :cond_42
    const/4 v0, 0x0

    #@43
    goto :goto_41
.end method
