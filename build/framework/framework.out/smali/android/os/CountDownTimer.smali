.class public abstract Landroid/os/CountDownTimer;
.super Ljava/lang/Object;
.source "CountDownTimer.java"


# static fields
.field private static final MSG:I = 0x1


# instance fields
.field private final mCountdownInterval:J

.field private mHandler:Landroid/os/Handler;

.field private final mMillisInFuture:J

.field private mStopTimeInFuture:J


# direct methods
.method public constructor <init>(JJ)V
    .registers 6
    .parameter "millisInFuture"
    .parameter "countDownInterval"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 109
    new-instance v0, Landroid/os/CountDownTimer$1;

    #@5
    invoke-direct {v0, p0}, Landroid/os/CountDownTimer$1;-><init>(Landroid/os/CountDownTimer;)V

    #@8
    iput-object v0, p0, Landroid/os/CountDownTimer;->mHandler:Landroid/os/Handler;

    #@a
    .line 68
    iput-wide p1, p0, Landroid/os/CountDownTimer;->mMillisInFuture:J

    #@c
    .line 69
    iput-wide p3, p0, Landroid/os/CountDownTimer;->mCountdownInterval:J

    #@e
    .line 70
    return-void
.end method

.method static synthetic access$000(Landroid/os/CountDownTimer;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-wide v0, p0, Landroid/os/CountDownTimer;->mStopTimeInFuture:J

    #@2
    return-wide v0
.end method

.method static synthetic access$100(Landroid/os/CountDownTimer;)J
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-wide v0, p0, Landroid/os/CountDownTimer;->mCountdownInterval:J

    #@2
    return-wide v0
.end method


# virtual methods
.method public final cancel()V
    .registers 3

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/os/CountDownTimer;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6
    .line 77
    return-void
.end method

.method public abstract onFinish()V
.end method

.method public abstract onTick(J)V
.end method

.method public final declared-synchronized start()Landroid/os/CountDownTimer;
    .registers 6

    #@0
    .prologue
    .line 83
    monitor-enter p0

    #@1
    :try_start_1
    iget-wide v1, p0, Landroid/os/CountDownTimer;->mMillisInFuture:J

    #@3
    const-wide/16 v3, 0x0

    #@5
    cmp-long v1, v1, v3

    #@7
    if-gtz v1, :cond_f

    #@9
    .line 84
    invoke-virtual {p0}, Landroid/os/CountDownTimer;->onFinish()V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_26

    #@c
    move-object v0, p0

    #@d
    .line 89
    .end local p0
    .local v0, this:Landroid/os/CountDownTimer;
    :goto_d
    monitor-exit p0

    #@e
    return-object v0

    #@f
    .line 87
    .end local v0           #this:Landroid/os/CountDownTimer;
    .restart local p0
    :cond_f
    :try_start_f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@12
    move-result-wide v1

    #@13
    iget-wide v3, p0, Landroid/os/CountDownTimer;->mMillisInFuture:J

    #@15
    add-long/2addr v1, v3

    #@16
    iput-wide v1, p0, Landroid/os/CountDownTimer;->mStopTimeInFuture:J

    #@18
    .line 88
    iget-object v1, p0, Landroid/os/CountDownTimer;->mHandler:Landroid/os/Handler;

    #@1a
    iget-object v2, p0, Landroid/os/CountDownTimer;->mHandler:Landroid/os/Handler;

    #@1c
    const/4 v3, 0x1

    #@1d
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_24
    .catchall {:try_start_f .. :try_end_24} :catchall_26

    #@24
    move-object v0, p0

    #@25
    .line 89
    .end local p0
    .restart local v0       #this:Landroid/os/CountDownTimer;
    goto :goto_d

    #@26
    .line 83
    .end local v0           #this:Landroid/os/CountDownTimer;
    .restart local p0
    :catchall_26
    move-exception v1

    #@27
    monitor-exit p0

    #@28
    throw v1
.end method
