.class public Landroid/os/Environment;
.super Ljava/lang/Object;
.source "Environment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Environment$UserEnvironment;
    }
.end annotation


# static fields
.field private static final DATA_DIRECTORY:Ljava/io/File; = null

.field public static DIRECTORY_ALARMS:Ljava/lang/String; = null

.field public static DIRECTORY_ANDROID:Ljava/lang/String; = null

.field public static DIRECTORY_DCIM:Ljava/lang/String; = null

.field public static DIRECTORY_DOWNLOADS:Ljava/lang/String; = null

.field public static DIRECTORY_MOVIES:Ljava/lang/String; = null

.field public static DIRECTORY_MUSIC:Ljava/lang/String; = null

.field public static DIRECTORY_NOTIFICATIONS:Ljava/lang/String; = null

.field public static DIRECTORY_PICTURES:Ljava/lang/String; = null

.field public static DIRECTORY_PODCASTS:Ljava/lang/String; = null

.field public static DIRECTORY_RINGTONES:Ljava/lang/String; = null

.field private static final DOWNLOAD_CACHE_DIRECTORY:Ljava/io/File; = null

.field private static final ENV_EMULATED_STORAGE_SOURCE:Ljava/lang/String; = "EMULATED_STORAGE_SOURCE"

.field private static final ENV_EMULATED_STORAGE_TARGET:Ljava/lang/String; = "EMULATED_STORAGE_TARGET"

.field private static final ENV_EXTERNAL_STORAGE:Ljava/lang/String; = "EXTERNAL_STORAGE"

.field private static final ENV_MEDIA_STORAGE:Ljava/lang/String; = "MEDIA_STORAGE"

.field public static final MEDIA_BAD_REMOVAL:Ljava/lang/String; = "bad_removal"

.field public static final MEDIA_CHECKING:Ljava/lang/String; = "checking"

.field public static final MEDIA_MOUNTED:Ljava/lang/String; = "mounted"

.field public static final MEDIA_MOUNTED_READ_ONLY:Ljava/lang/String; = "mounted_ro"

.field public static final MEDIA_NOFS:Ljava/lang/String; = "nofs"

.field public static final MEDIA_REMOVED:Ljava/lang/String; = "removed"

.field public static final MEDIA_SHARED:Ljava/lang/String; = "shared"

.field public static final MEDIA_UNMOUNTABLE:Ljava/lang/String; = "unmountable"

.field public static final MEDIA_UNMOUNTED:Ljava/lang/String; = "unmounted"

.field private static final ROOT_DIRECTORY:Ljava/io/File; = null

.field private static final SECURE_DATA_DIRECTORY:Ljava/io/File; = null

.field private static final SYSTEM_PROPERTY_EFS_ENABLED:Ljava/lang/String; = "persist.security.efs.enabled"

.field private static final TAG:Ljava/lang/String; = "Environment"

.field private static sCurrentUser:Landroid/os/Environment$UserEnvironment;

.field private static final sLock:Ljava/lang/Object;

.field private static volatile sPrimaryVolume:Landroid/os/storage/StorageVolume;
    .annotation build Lcom/android/internal/annotations/GuardedBy;
        value = "sLock"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 41
    const-string v0, "Android"

    #@2
    sput-object v0, Landroid/os/Environment;->DIRECTORY_ANDROID:Ljava/lang/String;

    #@4
    .line 43
    const-string v0, "ANDROID_ROOT"

    #@6
    const-string v1, "/system"

    #@8
    invoke-static {v0, v1}, Landroid/os/Environment;->getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    #@b
    move-result-object v0

    #@c
    sput-object v0, Landroid/os/Environment;->ROOT_DIRECTORY:Ljava/io/File;

    #@e
    .line 50
    new-instance v0, Ljava/lang/Object;

    #@10
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@13
    sput-object v0, Landroid/os/Environment;->sLock:Ljava/lang/Object;

    #@15
    .line 74
    invoke-static {}, Landroid/os/Environment;->initForCurrentUser()V

    #@18
    .line 246
    const-string v0, "ANDROID_DATA"

    #@1a
    const-string v1, "/data"

    #@1c
    invoke-static {v0, v1}, Landroid/os/Environment;->getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    #@1f
    move-result-object v0

    #@20
    sput-object v0, Landroid/os/Environment;->DATA_DIRECTORY:Ljava/io/File;

    #@22
    .line 252
    const-string v0, "ANDROID_SECURE_DATA"

    #@24
    const-string v1, "/data/secure"

    #@26
    invoke-static {v0, v1}, Landroid/os/Environment;->getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    #@29
    move-result-object v0

    #@2a
    sput-object v0, Landroid/os/Environment;->SECURE_DATA_DIRECTORY:Ljava/io/File;

    #@2c
    .line 255
    const-string v0, "DOWNLOAD_CACHE"

    #@2e
    const-string v1, "/cache"

    #@30
    invoke-static {v0, v1}, Landroid/os/Environment;->getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    #@33
    move-result-object v0

    #@34
    sput-object v0, Landroid/os/Environment;->DOWNLOAD_CACHE_DIRECTORY:Ljava/io/File;

    #@36
    .line 356
    const-string v0, "Music"

    #@38
    sput-object v0, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    #@3a
    .line 368
    const-string v0, "Podcasts"

    #@3c
    sput-object v0, Landroid/os/Environment;->DIRECTORY_PODCASTS:Ljava/lang/String;

    #@3e
    .line 380
    const-string v0, "Ringtones"

    #@40
    sput-object v0, Landroid/os/Environment;->DIRECTORY_RINGTONES:Ljava/lang/String;

    #@42
    .line 392
    const-string v0, "Alarms"

    #@44
    sput-object v0, Landroid/os/Environment;->DIRECTORY_ALARMS:Ljava/lang/String;

    #@46
    .line 404
    const-string v0, "Notifications"

    #@48
    sput-object v0, Landroid/os/Environment;->DIRECTORY_NOTIFICATIONS:Ljava/lang/String;

    #@4a
    .line 412
    const-string v0, "Pictures"

    #@4c
    sput-object v0, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    #@4e
    .line 420
    const-string v0, "Movies"

    #@50
    sput-object v0, Landroid/os/Environment;->DIRECTORY_MOVIES:Ljava/lang/String;

    #@52
    .line 430
    const-string v0, "Download"

    #@54
    sput-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    #@56
    .line 437
    const-string v0, "DCIM"

    #@58
    sput-object v0, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    #@5a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    return-void
.end method

.method static synthetic access$000(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-static {p0, p1}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static varargs buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;
    .registers 8
    .parameter "base"
    .parameter "segments"

    #@0
    .prologue
    .line 647
    move-object v1, p0

    #@1
    .line 648
    .local v1, cur:Ljava/io/File;
    move-object v0, p1

    #@2
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@3
    .local v4, len$:I
    const/4 v3, 0x0

    #@4
    .local v3, i$:I
    move-object v2, v1

    #@5
    .end local v1           #cur:Ljava/io/File;
    .local v2, cur:Ljava/io/File;
    :goto_5
    if-ge v3, v4, :cond_1a

    #@7
    aget-object v5, v0, v3

    #@9
    .line 649
    .local v5, segment:Ljava/lang/String;
    if-nez v2, :cond_14

    #@b
    .line 650
    new-instance v1, Ljava/io/File;

    #@d
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@10
    .line 648
    .end local v2           #cur:Ljava/io/File;
    .restart local v1       #cur:Ljava/io/File;
    :goto_10
    add-int/lit8 v3, v3, 0x1

    #@12
    move-object v2, v1

    #@13
    .end local v1           #cur:Ljava/io/File;
    .restart local v2       #cur:Ljava/io/File;
    goto :goto_5

    #@14
    .line 652
    :cond_14
    new-instance v1, Ljava/io/File;

    #@16
    invoke-direct {v1, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@19
    .end local v2           #cur:Ljava/io/File;
    .restart local v1       #cur:Ljava/io/File;
    goto :goto_10

    #@1a
    .line 655
    .end local v1           #cur:Ljava/io/File;
    .end local v5           #segment:Ljava/lang/String;
    .restart local v2       #cur:Ljava/io/File;
    :cond_1a
    return-object v2
.end method

.method public static getDataDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 261
    sget-object v0, Landroid/os/Environment;->DATA_DIRECTORY:Ljava/io/File;

    #@2
    return-object v0
.end method

.method static getDirectory(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter "variableName"
    .parameter "defaultPath"

    #@0
    .prologue
    .line 636
    invoke-static {p0}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 637
    .local v0, path:Ljava/lang/String;
    if-nez v0, :cond_c

    #@6
    new-instance v1, Ljava/io/File;

    #@8
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    :goto_b
    return-object v1

    #@c
    :cond_c
    new-instance v1, Ljava/io/File;

    #@e
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    goto :goto_b
.end method

.method public static getDownloadCacheDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 530
    sget-object v0, Landroid/os/Environment;->DOWNLOAD_CACHE_DIRECTORY:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public static getEmulatedStorageObbSource()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 344
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "EMULATED_STORAGE_SOURCE"

    #@4
    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const-string/jumbo v2, "obb"

    #@b
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    return-object v0
.end method

.method public static getEmulatedStorageSource(I)Ljava/io/File;
    .registers 4
    .parameter "userId"

    #@0
    .prologue
    .line 338
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "EMULATED_STORAGE_SOURCE"

    #@4
    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    return-object v0
.end method

.method public static getExternalStorageAndroidDataDir()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 477
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 478
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAndroidDataDir()Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageAppCacheDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 522
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 523
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppCacheDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageAppDataDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 486
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 487
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppDataDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageAppFilesDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 513
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 514
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppFilesDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageAppMediaDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 495
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 496
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppMediaDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "packageName"

    #@0
    .prologue
    .line 504
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 505
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageAppObbDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 316
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 317
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageDirectoryForSystem_UID()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 322
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@2
    invoke-virtual {v0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 468
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 469
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0, p0}, Landroid/os/Environment$UserEnvironment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static getExternalStorageState()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 594
    :try_start_0
    const-string/jumbo v3, "mount"

    #@3
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@6
    move-result-object v3

    #@7
    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@a
    move-result-object v0

    #@b
    .line 596
    .local v0, mountService:Landroid/os/storage/IMountService;
    invoke-static {}, Landroid/os/Environment;->getPrimaryVolume()Landroid/os/storage/StorageVolume;

    #@e
    move-result-object v1

    #@f
    .line 597
    .local v1, primary:Landroid/os/storage/StorageVolume;
    invoke-virtual {v1}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-interface {v0, v3}, Landroid/os/storage/IMountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_16} :catch_18

    #@16
    move-result-object v3

    #@17
    .line 600
    .end local v1           #primary:Landroid/os/storage/StorageVolume;
    :goto_17
    return-object v3

    #@18
    .line 598
    :catch_18
    move-exception v2

    #@19
    .line 599
    .local v2, rex:Landroid/os/RemoteException;
    const-string v3, "Environment"

    #@1b
    new-instance v4, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v5, "Failed to read external storage state; assuming REMOVED: "

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 600
    const-string/jumbo v3, "removed"

    #@34
    goto :goto_17
.end method

.method public static getLegacyExternalStorageDirectory()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 327
    new-instance v0, Ljava/io/File;

    #@2
    const-string v1, "EXTERNAL_STORAGE"

    #@4
    invoke-static {v1}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@b
    return-object v0
.end method

.method public static getLegacyExternalStorageObbDirectory()Ljava/io/File;
    .registers 4

    #@0
    .prologue
    .line 332
    invoke-static {}, Landroid/os/Environment;->getLegacyExternalStorageDirectory()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    const/4 v1, 0x2

    #@5
    new-array v1, v1, [Ljava/lang/String;

    #@7
    const/4 v2, 0x0

    #@8
    sget-object v3, Landroid/os/Environment;->DIRECTORY_ANDROID:Ljava/lang/String;

    #@a
    aput-object v3, v1, v2

    #@c
    const/4 v2, 0x1

    #@d
    const-string/jumbo v3, "obb"

    #@10
    aput-object v3, v1, v2

    #@12
    invoke-static {v0, v1}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    #@15
    move-result-object v0

    #@16
    return-object v0
.end method

.method public static getMediaStorageDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 221
    invoke-static {}, Landroid/os/Environment;->throwIfSystem()V

    #@3
    .line 222
    sget-object v0, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@5
    invoke-virtual {v0}, Landroid/os/Environment$UserEnvironment;->getMediaStorageDirectory()Ljava/io/File;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method private static getPrimaryVolume()Landroid/os/storage/StorageVolume;
    .registers 6

    #@0
    .prologue
    .line 56
    sget-object v3, Landroid/os/Environment;->sPrimaryVolume:Landroid/os/storage/StorageVolume;

    #@2
    if-nez v3, :cond_21

    #@4
    .line 57
    sget-object v4, Landroid/os/Environment;->sLock:Ljava/lang/Object;

    #@6
    monitor-enter v4

    #@7
    .line 58
    :try_start_7
    sget-object v3, Landroid/os/Environment;->sPrimaryVolume:Landroid/os/storage/StorageVolume;
    :try_end_9
    .catchall {:try_start_7 .. :try_end_9} :catchall_2d

    #@9
    if-nez v3, :cond_20

    #@b
    .line 60
    :try_start_b
    const-string/jumbo v3, "mount"

    #@e
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@11
    move-result-object v3

    #@12
    invoke-static {v3}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@15
    move-result-object v1

    #@16
    .line 62
    .local v1, mountService:Landroid/os/storage/IMountService;
    invoke-interface {v1}, Landroid/os/storage/IMountService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@19
    move-result-object v2

    #@1a
    .line 63
    .local v2, volumes:[Landroid/os/storage/StorageVolume;
    invoke-static {v2}, Landroid/os/storage/StorageManager;->getPrimaryVolume([Landroid/os/storage/StorageVolume;)Landroid/os/storage/StorageVolume;

    #@1d
    move-result-object v3

    #@1e
    sput-object v3, Landroid/os/Environment;->sPrimaryVolume:Landroid/os/storage/StorageVolume;
    :try_end_20
    .catchall {:try_start_b .. :try_end_20} :catchall_2d
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_20} :catch_24

    #@20
    .line 68
    .end local v2           #volumes:[Landroid/os/storage/StorageVolume;
    :cond_20
    :goto_20
    :try_start_20
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_2d

    #@21
    .line 70
    :cond_21
    sget-object v3, Landroid/os/Environment;->sPrimaryVolume:Landroid/os/storage/StorageVolume;

    #@23
    return-object v3

    #@24
    .line 64
    :catch_24
    move-exception v0

    #@25
    .line 65
    .local v0, e:Ljava/lang/Exception;
    :try_start_25
    const-string v3, "Environment"

    #@27
    const-string v5, "couldn\'t talk to MountService"

    #@29
    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    goto :goto_20

    #@2d
    .line 68
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_2d
    move-exception v3

    #@2e
    monitor-exit v4
    :try_end_2f
    .catchall {:try_start_25 .. :try_end_2f} :catchall_2d

    #@2f
    throw v3
.end method

.method public static getRootDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 181
    sget-object v0, Landroid/os/Environment;->ROOT_DIRECTORY:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public static getSecureDataDirectory()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 207
    invoke-static {}, Landroid/os/Environment;->isEncryptedFilesystemEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 208
    sget-object v0, Landroid/os/Environment;->SECURE_DATA_DIRECTORY:Ljava/io/File;

    #@8
    .line 210
    :goto_8
    return-object v0

    #@9
    :cond_9
    sget-object v0, Landroid/os/Environment;->DATA_DIRECTORY:Ljava/io/File;

    #@b
    goto :goto_8
.end method

.method public static getSystemSecureDirectory()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 192
    invoke-static {}, Landroid/os/Environment;->isEncryptedFilesystemEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_11

    #@6
    .line 193
    new-instance v0, Ljava/io/File;

    #@8
    sget-object v1, Landroid/os/Environment;->SECURE_DATA_DIRECTORY:Ljava/io/File;

    #@a
    const-string/jumbo v2, "system"

    #@d
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@10
    .line 195
    :goto_10
    return-object v0

    #@11
    :cond_11
    new-instance v0, Ljava/io/File;

    #@13
    sget-object v1, Landroid/os/Environment;->DATA_DIRECTORY:Ljava/io/File;

    #@15
    const-string/jumbo v2, "system"

    #@18
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@1b
    goto :goto_10
.end method

.method public static getUserSystemDirectory(I)Ljava/io/File;
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 233
    new-instance v0, Ljava/io/File;

    #@2
    new-instance v1, Ljava/io/File;

    #@4
    invoke-static {}, Landroid/os/Environment;->getSystemSecureDirectory()Ljava/io/File;

    #@7
    move-result-object v2

    #@8
    const-string/jumbo v3, "users"

    #@b
    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@e
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@15
    return-object v0
.end method

.method public static initForCurrentUser()V
    .registers 3

    #@0
    .prologue
    .line 79
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    .line 80
    .local v0, userId:I
    new-instance v1, Landroid/os/Environment$UserEnvironment;

    #@6
    invoke-direct {v1, v0}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    #@9
    sput-object v1, Landroid/os/Environment;->sCurrentUser:Landroid/os/Environment$UserEnvironment;

    #@b
    .line 82
    sget-object v2, Landroid/os/Environment;->sLock:Ljava/lang/Object;

    #@d
    monitor-enter v2

    #@e
    .line 83
    const/4 v1, 0x0

    #@f
    :try_start_f
    sput-object v1, Landroid/os/Environment;->sPrimaryVolume:Landroid/os/storage/StorageVolume;

    #@11
    .line 84
    monitor-exit v2

    #@12
    .line 85
    return-void

    #@13
    .line 84
    :catchall_13
    move-exception v1

    #@14
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_f .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method public static isEncryptedFilesystemEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 243
    const-string/jumbo v0, "persist.security.efs.enabled"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static isExternalStorageEmulated()Z
    .registers 2

    #@0
    .prologue
    .line 631
    invoke-static {}, Landroid/os/Environment;->getPrimaryVolume()Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v0

    #@4
    .line 632
    .local v0, primary:Landroid/os/storage/StorageVolume;
    if-eqz v0, :cond_e

    #@6
    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->isEmulated()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method public static isExternalStorageRemovable()Z
    .registers 2

    #@0
    .prologue
    .line 613
    invoke-static {}, Landroid/os/Environment;->getPrimaryVolume()Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v0

    #@4
    .line 614
    .local v0, primary:Landroid/os/storage/StorageVolume;
    if-eqz v0, :cond_e

    #@6
    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method private static throwIfSystem()V
    .registers 3

    #@0
    .prologue
    .line 641
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0x3e8

    #@6
    if-ne v0, v1, :cond_14

    #@8
    .line 642
    const-string v0, "Environment"

    #@a
    const-string v1, "Static storage paths aren\'t available from AID_SYSTEM"

    #@c
    new-instance v2, Ljava/lang/Throwable;

    #@e
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@11
    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    .line 644
    :cond_14
    return-void
.end method
