.class public final enum Landroid/os/SystemService$State;
.super Ljava/lang/Enum;
.source "SystemService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/SystemService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/os/SystemService$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/os/SystemService$State;

.field public static final enum RESTARTING:Landroid/os/SystemService$State;

.field public static final enum RUNNING:Landroid/os/SystemService$State;

.field public static final enum STOPPED:Landroid/os/SystemService$State;

.field public static final enum STOPPING:Landroid/os/SystemService$State;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v5, 0x2

    #@2
    const/4 v4, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 39
    new-instance v0, Landroid/os/SystemService$State;

    #@6
    const-string v1, "RUNNING"

    #@8
    const-string/jumbo v2, "running"

    #@b
    invoke-direct {v0, v1, v3, v2}, Landroid/os/SystemService$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@e
    sput-object v0, Landroid/os/SystemService$State;->RUNNING:Landroid/os/SystemService$State;

    #@10
    .line 40
    new-instance v0, Landroid/os/SystemService$State;

    #@12
    const-string v1, "STOPPING"

    #@14
    const-string/jumbo v2, "stopping"

    #@17
    invoke-direct {v0, v1, v4, v2}, Landroid/os/SystemService$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@1a
    sput-object v0, Landroid/os/SystemService$State;->STOPPING:Landroid/os/SystemService$State;

    #@1c
    .line 41
    new-instance v0, Landroid/os/SystemService$State;

    #@1e
    const-string v1, "STOPPED"

    #@20
    const-string/jumbo v2, "stopped"

    #@23
    invoke-direct {v0, v1, v5, v2}, Landroid/os/SystemService$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@26
    sput-object v0, Landroid/os/SystemService$State;->STOPPED:Landroid/os/SystemService$State;

    #@28
    .line 42
    new-instance v0, Landroid/os/SystemService$State;

    #@2a
    const-string v1, "RESTARTING"

    #@2c
    const-string/jumbo v2, "restarting"

    #@2f
    invoke-direct {v0, v1, v6, v2}, Landroid/os/SystemService$State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    #@32
    sput-object v0, Landroid/os/SystemService$State;->RESTARTING:Landroid/os/SystemService$State;

    #@34
    .line 38
    const/4 v0, 0x4

    #@35
    new-array v0, v0, [Landroid/os/SystemService$State;

    #@37
    sget-object v1, Landroid/os/SystemService$State;->RUNNING:Landroid/os/SystemService$State;

    #@39
    aput-object v1, v0, v3

    #@3b
    sget-object v1, Landroid/os/SystemService$State;->STOPPING:Landroid/os/SystemService$State;

    #@3d
    aput-object v1, v0, v4

    #@3f
    sget-object v1, Landroid/os/SystemService$State;->STOPPED:Landroid/os/SystemService$State;

    #@41
    aput-object v1, v0, v5

    #@43
    sget-object v1, Landroid/os/SystemService$State;->RESTARTING:Landroid/os/SystemService$State;

    #@45
    aput-object v1, v0, v6

    #@47
    sput-object v0, Landroid/os/SystemService$State;->$VALUES:[Landroid/os/SystemService$State;

    #@49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter "state"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 45
    invoke-static {}, Landroid/os/SystemService;->access$000()Ljava/util/HashMap;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p3, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/os/SystemService$State;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 38
    const-class v0, Landroid/os/SystemService$State;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/SystemService$State;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/os/SystemService$State;
    .registers 1

    #@0
    .prologue
    .line 38
    sget-object v0, Landroid/os/SystemService$State;->$VALUES:[Landroid/os/SystemService$State;

    #@2
    invoke-virtual {v0}, [Landroid/os/SystemService$State;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/os/SystemService$State;

    #@8
    return-object v0
.end method
