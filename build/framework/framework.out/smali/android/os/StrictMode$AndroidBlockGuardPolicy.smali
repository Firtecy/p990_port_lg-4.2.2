.class Landroid/os/StrictMode$AndroidBlockGuardPolicy;
.super Ljava/lang/Object;
.source "StrictMode.java"

# interfaces
.implements Ldalvik/system/BlockGuard$Policy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AndroidBlockGuardPolicy"
.end annotation


# instance fields
.field private final mLastViolationTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPolicyMask:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "policyMask"

    #@0
    .prologue
    .line 1061
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1059
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mLastViolationTime:Ljava/util/HashMap;

    #@a
    .line 1062
    iput p1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@c
    .line 1063
    return-void
.end method


# virtual methods
.method public getPolicyMask()I
    .registers 2

    #@0
    .prologue
    .line 1072
    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    return v0
.end method

.method handleViolation(Landroid/os/StrictMode$ViolationInfo;)V
    .registers 24
    .parameter "info"

    #@0
    .prologue
    .line 1240
    if-eqz p1, :cond_18

    #@2
    move-object/from16 v0, p1

    #@4
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@6
    move-object/from16 v19, v0

    #@8
    if-eqz v19, :cond_18

    #@a
    move-object/from16 v0, p1

    #@c
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@e
    move-object/from16 v19, v0

    #@10
    move-object/from16 v0, v19

    #@12
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@14
    move-object/from16 v19, v0

    #@16
    if-nez v19, :cond_21

    #@18
    .line 1241
    :cond_18
    const-string v19, "StrictMode"

    #@1a
    const-string/jumbo v20, "unexpected null stacktrace"

    #@1d
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1342
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1245
    :cond_21
    invoke-static {}, Landroid/os/StrictMode;->access$800()Z

    #@24
    move-result v19

    #@25
    if-eqz v19, :cond_45

    #@27
    const-string v19, "StrictMode"

    #@29
    new-instance v20, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v21, "handleViolation; policy="

    #@30
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v20

    #@34
    move-object/from16 v0, p1

    #@36
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@38
    move/from16 v21, v0

    #@3a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v20

    #@3e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v20

    #@42
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 1247
    :cond_45
    move-object/from16 v0, p1

    #@47
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@49
    move/from16 v19, v0

    #@4b
    move/from16 v0, v19

    #@4d
    and-int/lit16 v0, v0, 0x100

    #@4f
    move/from16 v19, v0

    #@51
    if-eqz v19, :cond_b5

    #@53
    .line 1248
    invoke-static {}, Landroid/os/StrictMode;->access$900()Ljava/lang/ThreadLocal;

    #@56
    move-result-object v19

    #@57
    invoke-virtual/range {v19 .. v19}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5a
    move-result-object v18

    #@5b
    check-cast v18, Ljava/util/ArrayList;

    #@5d
    .line 1249
    .local v18, violations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    if-nez v18, :cond_9e

    #@5f
    .line 1250
    new-instance v18, Ljava/util/ArrayList;

    #@61
    .end local v18           #violations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    const/16 v19, 0x1

    #@63
    invoke-direct/range {v18 .. v19}, Ljava/util/ArrayList;-><init>(I)V

    #@66
    .line 1251
    .restart local v18       #violations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    invoke-static {}, Landroid/os/StrictMode;->access$900()Ljava/lang/ThreadLocal;

    #@69
    move-result-object v19

    #@6a
    move-object/from16 v0, v19

    #@6c
    move-object/from16 v1, v18

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@71
    .line 1256
    :cond_71
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@74
    move-result-object v6

    #@75
    .local v6, i$:Ljava/util/Iterator;
    :cond_75
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@78
    move-result v19

    #@79
    if-eqz v19, :cond_ac

    #@7b
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7e
    move-result-object v12

    #@7f
    check-cast v12, Landroid/os/StrictMode$ViolationInfo;

    #@81
    .line 1257
    .local v12, previous:Landroid/os/StrictMode$ViolationInfo;
    move-object/from16 v0, p1

    #@83
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@85
    move-object/from16 v19, v0

    #@87
    move-object/from16 v0, v19

    #@89
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@8b
    move-object/from16 v19, v0

    #@8d
    iget-object v0, v12, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@8f
    move-object/from16 v20, v0

    #@91
    move-object/from16 v0, v20

    #@93
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@95
    move-object/from16 v20, v0

    #@97
    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v19

    #@9b
    if-eqz v19, :cond_75

    #@9d
    goto :goto_20

    #@9e
    .line 1252
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v12           #previous:Landroid/os/StrictMode$ViolationInfo;
    :cond_9e
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    #@a1
    move-result v19

    #@a2
    const/16 v20, 0x5

    #@a4
    move/from16 v0, v19

    #@a6
    move/from16 v1, v20

    #@a8
    if-lt v0, v1, :cond_71

    #@aa
    goto/16 :goto_20

    #@ac
    .line 1262
    .restart local v6       #i$:Ljava/util/Iterator;
    :cond_ac
    move-object/from16 v0, v18

    #@ae
    move-object/from16 v1, p1

    #@b0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@b3
    goto/16 :goto_20

    #@b5
    .line 1267
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v18           #violations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    :cond_b5
    invoke-virtual/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->hashCode()I

    #@b8
    move-result v19

    #@b9
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bc
    move-result-object v4

    #@bd
    .line 1268
    .local v4, crashFingerprint:Ljava/lang/Integer;
    const-wide/16 v8, 0x0

    #@bf
    .line 1269
    .local v8, lastViolationTime:J
    move-object/from16 v0, p0

    #@c1
    iget-object v0, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mLastViolationTime:Ljava/util/HashMap;

    #@c3
    move-object/from16 v19, v0

    #@c5
    move-object/from16 v0, v19

    #@c7
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@ca
    move-result v19

    #@cb
    if-eqz v19, :cond_df

    #@cd
    .line 1270
    move-object/from16 v0, p0

    #@cf
    iget-object v0, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mLastViolationTime:Ljava/util/HashMap;

    #@d1
    move-object/from16 v19, v0

    #@d3
    move-object/from16 v0, v19

    #@d5
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d8
    move-result-object v19

    #@d9
    check-cast v19, Ljava/lang/Long;

    #@db
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    #@de
    move-result-wide v8

    #@df
    .line 1272
    :cond_df
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e2
    move-result-wide v10

    #@e3
    .line 1273
    .local v10, now:J
    move-object/from16 v0, p0

    #@e5
    iget-object v0, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mLastViolationTime:Ljava/util/HashMap;

    #@e7
    move-object/from16 v19, v0

    #@e9
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@ec
    move-result-object v20

    #@ed
    move-object/from16 v0, v19

    #@ef
    move-object/from16 v1, v20

    #@f1
    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@f4
    .line 1274
    const-wide/16 v19, 0x0

    #@f6
    cmp-long v19, v8, v19

    #@f8
    if-nez v19, :cond_1b7

    #@fa
    const-wide v14, 0x7fffffffffffffffL

    #@ff
    .line 1277
    .local v14, timeSinceLastViolationMillis:J
    :goto_ff
    move-object/from16 v0, p1

    #@101
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@103
    move/from16 v19, v0

    #@105
    and-int/lit8 v19, v19, 0x10

    #@107
    if-eqz v19, :cond_151

    #@109
    const-wide/16 v19, 0x3e8

    #@10b
    cmp-long v19, v14, v19

    #@10d
    if-lez v19, :cond_151

    #@10f
    .line 1279
    move-object/from16 v0, p1

    #@111
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@113
    move/from16 v19, v0

    #@115
    const/16 v20, -0x1

    #@117
    move/from16 v0, v19

    #@119
    move/from16 v1, v20

    #@11b
    if-eq v0, v1, :cond_1bb

    #@11d
    .line 1280
    const-string v19, "StrictMode"

    #@11f
    new-instance v20, Ljava/lang/StringBuilder;

    #@121
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@124
    const-string v21, "StrictMode policy violation; ~duration="

    #@126
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v20

    #@12a
    move-object/from16 v0, p1

    #@12c
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@12e
    move/from16 v21, v0

    #@130
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v20

    #@134
    const-string v21, " ms: "

    #@136
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v20

    #@13a
    move-object/from16 v0, p1

    #@13c
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@13e
    move-object/from16 v21, v0

    #@140
    move-object/from16 v0, v21

    #@142
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@144
    move-object/from16 v21, v0

    #@146
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v20

    #@14a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v20

    #@14e
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 1291
    :cond_151
    :goto_151
    const/16 v17, 0x0

    #@153
    .line 1293
    .local v17, violationMaskSubset:I
    move-object/from16 v0, p1

    #@155
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@157
    move/from16 v19, v0

    #@159
    and-int/lit8 v19, v19, 0x20

    #@15b
    if-eqz v19, :cond_165

    #@15d
    const-wide/16 v19, 0x7530

    #@15f
    cmp-long v19, v14, v19

    #@161
    if-lez v19, :cond_165

    #@163
    .line 1295
    or-int/lit8 v17, v17, 0x20

    #@165
    .line 1298
    :cond_165
    move-object/from16 v0, p1

    #@167
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@169
    move/from16 v19, v0

    #@16b
    move/from16 v0, v19

    #@16d
    and-int/lit16 v0, v0, 0x80

    #@16f
    move/from16 v19, v0

    #@171
    if-eqz v19, :cond_17f

    #@173
    const-wide/16 v19, 0x0

    #@175
    cmp-long v19, v8, v19

    #@177
    if-nez v19, :cond_17f

    #@179
    .line 1299
    move/from16 v0, v17

    #@17b
    or-int/lit16 v0, v0, 0x80

    #@17d
    move/from16 v17, v0

    #@17f
    .line 1302
    :cond_17f
    if-eqz v17, :cond_1fe

    #@181
    .line 1303
    move-object/from16 v0, p1

    #@183
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@185
    move-object/from16 v19, v0

    #@187
    move-object/from16 v0, v19

    #@189
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    #@18b
    move-object/from16 v19, v0

    #@18d
    invoke-static/range {v19 .. v19}, Landroid/os/StrictMode;->access$1000(Ljava/lang/String;)I

    #@190
    move-result v16

    #@191
    .line 1304
    .local v16, violationBit:I
    or-int v17, v17, v16

    #@193
    .line 1305
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@196
    move-result v13

    #@197
    .line 1307
    .local v13, savedPolicyMask:I
    move-object/from16 v0, p1

    #@199
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@19b
    move/from16 v19, v0

    #@19d
    move/from16 v0, v19

    #@19f
    and-int/lit16 v0, v0, 0xbf0

    #@1a1
    move/from16 v19, v0

    #@1a3
    const/16 v20, 0x80

    #@1a5
    move/from16 v0, v19

    #@1a7
    move/from16 v1, v20

    #@1a9
    if-ne v0, v1, :cond_1e1

    #@1ab
    const/4 v7, 0x1

    #@1ac
    .line 1308
    .local v7, justDropBox:Z
    :goto_1ac
    if-eqz v7, :cond_1e3

    #@1ae
    .line 1315
    move/from16 v0, v17

    #@1b0
    move-object/from16 v1, p1

    #@1b2
    invoke-static {v0, v1}, Landroid/os/StrictMode;->access$1100(ILandroid/os/StrictMode$ViolationInfo;)V

    #@1b5
    goto/16 :goto_20

    #@1b7
    .line 1274
    .end local v7           #justDropBox:Z
    .end local v13           #savedPolicyMask:I
    .end local v14           #timeSinceLastViolationMillis:J
    .end local v16           #violationBit:I
    .end local v17           #violationMaskSubset:I
    :cond_1b7
    sub-long v14, v10, v8

    #@1b9
    goto/16 :goto_ff

    #@1bb
    .line 1283
    .restart local v14       #timeSinceLastViolationMillis:J
    :cond_1bb
    const-string v19, "StrictMode"

    #@1bd
    new-instance v20, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    const-string v21, "StrictMode policy violation: "

    #@1c4
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v20

    #@1c8
    move-object/from16 v0, p1

    #@1ca
    iget-object v0, v0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@1cc
    move-object/from16 v21, v0

    #@1ce
    move-object/from16 v0, v21

    #@1d0
    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@1d2
    move-object/from16 v21, v0

    #@1d4
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v20

    #@1d8
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1db
    move-result-object v20

    #@1dc
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1df
    goto/16 :goto_151

    #@1e1
    .line 1307
    .restart local v13       #savedPolicyMask:I
    .restart local v16       #violationBit:I
    .restart local v17       #violationMaskSubset:I
    :cond_1e1
    const/4 v7, 0x0

    #@1e2
    goto :goto_1ac

    #@1e3
    .line 1325
    .restart local v7       #justDropBox:Z
    :cond_1e3
    const/16 v19, 0x0

    #@1e5
    :try_start_1e5
    invoke-static/range {v19 .. v19}, Landroid/os/StrictMode;->access$1200(I)V

    #@1e8
    .line 1327
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@1eb
    move-result-object v19

    #@1ec
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->getApplicationObject()Landroid/os/IBinder;

    #@1ef
    move-result-object v20

    #@1f0
    move-object/from16 v0, v19

    #@1f2
    move-object/from16 v1, v20

    #@1f4
    move/from16 v2, v17

    #@1f6
    move-object/from16 v3, p1

    #@1f8
    invoke-interface {v0, v1, v2, v3}, Landroid/app/IActivityManager;->handleApplicationStrictModeViolation(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V
    :try_end_1fb
    .catchall {:try_start_1e5 .. :try_end_1fb} :catchall_21a
    .catch Landroid/os/RemoteException; {:try_start_1e5 .. :try_end_1fb} :catch_20d

    #@1fb
    .line 1335
    :goto_1fb
    invoke-static {v13}, Landroid/os/StrictMode;->access$1200(I)V

    #@1fe
    .line 1339
    .end local v7           #justDropBox:Z
    .end local v13           #savedPolicyMask:I
    .end local v16           #violationBit:I
    :cond_1fe
    move-object/from16 v0, p1

    #@200
    iget v0, v0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@202
    move/from16 v19, v0

    #@204
    and-int/lit8 v19, v19, 0x40

    #@206
    if-eqz v19, :cond_20

    #@208
    .line 1340
    invoke-static/range {p1 .. p1}, Landroid/os/StrictMode;->access$1300(Landroid/os/StrictMode$ViolationInfo;)V

    #@20b
    goto/16 :goto_20

    #@20d
    .line 1331
    .restart local v7       #justDropBox:Z
    .restart local v13       #savedPolicyMask:I
    .restart local v16       #violationBit:I
    :catch_20d
    move-exception v5

    #@20e
    .line 1332
    .local v5, e:Landroid/os/RemoteException;
    :try_start_20e
    const-string v19, "StrictMode"

    #@210
    const-string v20, "RemoteException trying to handle StrictMode violation"

    #@212
    move-object/from16 v0, v19

    #@214
    move-object/from16 v1, v20

    #@216
    invoke-static {v0, v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_219
    .catchall {:try_start_20e .. :try_end_219} :catchall_21a

    #@219
    goto :goto_1fb

    #@21a
    .line 1335
    .end local v5           #e:Landroid/os/RemoteException;
    :catchall_21a
    move-exception v19

    #@21b
    invoke-static {v13}, Landroid/os/StrictMode;->access$1200(I)V

    #@21e
    throw v19
.end method

.method handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V
    .registers 8
    .parameter "info"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 1151
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@4
    move-result-object v0

    #@5
    .line 1169
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_f

    #@7
    iget v3, p1, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@9
    and-int/lit16 v3, v3, 0xbf0

    #@b
    const/16 v4, 0x40

    #@d
    if-ne v3, v4, :cond_16

    #@f
    .line 1171
    :cond_f
    const/4 v3, -0x1

    #@10
    iput v3, p1, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@12
    .line 1172
    invoke-virtual {p0, p1}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolation(Landroid/os/StrictMode$ViolationInfo;)V

    #@15
    .line 1232
    :cond_15
    :goto_15
    return-void

    #@16
    .line 1176
    :cond_16
    invoke-static {}, Landroid/os/StrictMode;->access$500()Ljava/lang/ThreadLocal;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Ljava/util/ArrayList;

    #@20
    .line 1177
    .local v1, records:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v3

    #@24
    const/16 v4, 0xa

    #@26
    if-ge v3, v4, :cond_15

    #@28
    .line 1181
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2b
    .line 1182
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v3

    #@2f
    if-gt v3, v5, :cond_15

    #@31
    .line 1189
    iget v3, p1, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@33
    and-int/lit16 v3, v3, 0x800

    #@35
    if-eqz v3, :cond_5b

    #@37
    invoke-static {}, Landroid/os/StrictMode;->access$600()Landroid/util/Singleton;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    #@3e
    move-result-object v3

    #@3f
    check-cast v3, Landroid/view/IWindowManager;

    #@41
    move-object v2, v3

    #@42
    .line 1191
    .local v2, windowManager:Landroid/view/IWindowManager;
    :goto_42
    if-eqz v2, :cond_48

    #@44
    .line 1193
    const/4 v3, 0x1

    #@45
    :try_start_45
    invoke-interface {v2, v3}, Landroid/view/IWindowManager;->showStrictModeViolation(Z)V
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_48} :catch_5d

    #@48
    .line 1207
    :cond_48
    :goto_48
    invoke-static {}, Landroid/os/StrictMode;->access$700()Ljava/lang/ThreadLocal;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@4f
    move-result-object v3

    #@50
    check-cast v3, Landroid/os/Handler;

    #@52
    new-instance v4, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;

    #@54
    invoke-direct {v4, p0, v2, v1}, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;-><init>(Landroid/os/StrictMode$AndroidBlockGuardPolicy;Landroid/view/IWindowManager;Ljava/util/ArrayList;)V

    #@57
    invoke-virtual {v3, v4}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    #@5a
    goto :goto_15

    #@5b
    .line 1189
    .end local v2           #windowManager:Landroid/view/IWindowManager;
    :cond_5b
    const/4 v2, 0x0

    #@5c
    goto :goto_42

    #@5d
    .line 1194
    .restart local v2       #windowManager:Landroid/view/IWindowManager;
    :catch_5d
    move-exception v3

    #@5e
    goto :goto_48
.end method

.method onCustomSlowCall(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1090
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    and-int/lit8 v1, v1, 0x8

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1099
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1093
    :cond_7
    invoke-static {}, Landroid/os/StrictMode;->access$400()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_6

    #@d
    .line 1096
    new-instance v0, Landroid/os/StrictMode$StrictModeCustomViolation;

    #@f
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@11
    invoke-direct {v0, v1, p1}, Landroid/os/StrictMode$StrictModeCustomViolation;-><init>(ILjava/lang/String;)V

    #@14
    .line 1097
    .local v0, e:Ldalvik/system/BlockGuard$BlockGuardPolicyException;
    invoke-virtual {v0}, Ldalvik/system/BlockGuard$BlockGuardPolicyException;->fillInStackTrace()Ljava/lang/Throwable;

    #@17
    .line 1098
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Ldalvik/system/BlockGuard$BlockGuardPolicyException;)V

    #@1a
    goto :goto_6
.end method

.method public onNetwork()V
    .registers 3

    #@0
    .prologue
    .line 1116
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    and-int/lit8 v1, v1, 0x4

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1128
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1119
    :cond_7
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@9
    and-int/lit16 v1, v1, 0x200

    #@b
    if-eqz v1, :cond_13

    #@d
    .line 1120
    new-instance v1, Landroid/os/NetworkOnMainThreadException;

    #@f
    invoke-direct {v1}, Landroid/os/NetworkOnMainThreadException;-><init>()V

    #@12
    throw v1

    #@13
    .line 1122
    :cond_13
    invoke-static {}, Landroid/os/StrictMode;->access$400()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_6

    #@19
    .line 1125
    new-instance v0, Landroid/os/StrictMode$StrictModeNetworkViolation;

    #@1b
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@1d
    invoke-direct {v0, v1}, Landroid/os/StrictMode$StrictModeNetworkViolation;-><init>(I)V

    #@20
    .line 1126
    .local v0, e:Ldalvik/system/BlockGuard$BlockGuardPolicyException;
    invoke-virtual {v0}, Ldalvik/system/BlockGuard$BlockGuardPolicyException;->fillInStackTrace()Ljava/lang/Throwable;

    #@23
    .line 1127
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Ldalvik/system/BlockGuard$BlockGuardPolicyException;)V

    #@26
    goto :goto_6
.end method

.method public onReadFromDisk()V
    .registers 3

    #@0
    .prologue
    .line 1103
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    and-int/lit8 v1, v1, 0x2

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1112
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1106
    :cond_7
    invoke-static {}, Landroid/os/StrictMode;->access$400()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_6

    #@d
    .line 1109
    new-instance v0, Landroid/os/StrictMode$StrictModeDiskReadViolation;

    #@f
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@11
    invoke-direct {v0, v1}, Landroid/os/StrictMode$StrictModeDiskReadViolation;-><init>(I)V

    #@14
    .line 1110
    .local v0, e:Ldalvik/system/BlockGuard$BlockGuardPolicyException;
    invoke-virtual {v0}, Ldalvik/system/BlockGuard$BlockGuardPolicyException;->fillInStackTrace()Ljava/lang/Throwable;

    #@17
    .line 1111
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Ldalvik/system/BlockGuard$BlockGuardPolicyException;)V

    #@1a
    goto :goto_6
.end method

.method public onWriteToDisk()V
    .registers 3

    #@0
    .prologue
    .line 1077
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    and-int/lit8 v1, v1, 0x1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 1086
    :cond_6
    :goto_6
    return-void

    #@7
    .line 1080
    :cond_7
    invoke-static {}, Landroid/os/StrictMode;->access$400()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_6

    #@d
    .line 1083
    new-instance v0, Landroid/os/StrictMode$StrictModeDiskWriteViolation;

    #@f
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@11
    invoke-direct {v0, v1}, Landroid/os/StrictMode$StrictModeDiskWriteViolation;-><init>(I)V

    #@14
    .line 1084
    .local v0, e:Ldalvik/system/BlockGuard$BlockGuardPolicyException;
    invoke-virtual {v0}, Ldalvik/system/BlockGuard$BlockGuardPolicyException;->fillInStackTrace()Ljava/lang/Throwable;

    #@17
    .line 1085
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Ldalvik/system/BlockGuard$BlockGuardPolicyException;)V

    #@1a
    goto :goto_6
.end method

.method public setPolicyMask(I)V
    .registers 2
    .parameter "policyMask"

    #@0
    .prologue
    .line 1131
    iput p1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@2
    .line 1132
    return-void
.end method

.method startHandlingViolationException(Ldalvik/system/BlockGuard$BlockGuardPolicyException;)V
    .registers 5
    .parameter "e"

    #@0
    .prologue
    .line 1140
    new-instance v0, Landroid/os/StrictMode$ViolationInfo;

    #@2
    invoke-virtual {p1}, Ldalvik/system/BlockGuard$BlockGuardPolicyException;->getPolicy()I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, p1, v1}, Landroid/os/StrictMode$ViolationInfo;-><init>(Ljava/lang/Throwable;I)V

    #@9
    .line 1141
    .local v0, info:Landroid/os/StrictMode$ViolationInfo;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@c
    move-result-wide v1

    #@d
    iput-wide v1, v0, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@f
    .line 1142
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V

    #@12
    .line 1143
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 1067
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "AndroidBlockGuardPolicy; mPolicyMask="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mPolicyMask:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    return-object v0
.end method
