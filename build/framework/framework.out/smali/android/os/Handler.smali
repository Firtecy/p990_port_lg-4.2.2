.class public Landroid/os/Handler;
.super Ljava/lang/Object;
.source "Handler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Handler$1;,
        Landroid/os/Handler$BlockingRunnable;,
        Landroid/os/Handler$MessengerImpl;,
        Landroid/os/Handler$Callback;
    }
.end annotation


# static fields
.field private static final FIND_POTENTIAL_LEAKS:Z = false

.field private static final TAG:Ljava/lang/String; = "Handler"


# instance fields
.field final mAsynchronous:Z

.field final mCallback:Landroid/os/Handler$Callback;

.field final mLooper:Landroid/os/Looper;

.field mMessenger:Landroid/os/IMessenger;

.field final mQueue:Landroid/os/MessageQueue;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    .line 111
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    #@5
    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler$Callback;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 125
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    #@4
    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler$Callback;Z)V
    .registers 5
    .parameter "callback"
    .parameter "async"

    #@0
    .prologue
    .line 185
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 195
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@9
    .line 196
    iget-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@b
    if-nez v0, :cond_15

    #@d
    .line 197
    new-instance v0, Ljava/lang/RuntimeException;

    #@f
    const-string v1, "Can\'t create handler inside thread that has not called Looper.prepare()"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 200
    :cond_15
    iget-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@17
    iget-object v0, v0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@19
    iput-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@1b
    .line 201
    iput-object p1, p0, Landroid/os/Handler;->mCallback:Landroid/os/Handler$Callback;

    #@1d
    .line 202
    iput-boolean p2, p0, Landroid/os/Handler;->mAsynchronous:Z

    #@1f
    .line 203
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .registers 4
    .parameter "looper"

    #@0
    .prologue
    .line 134
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    invoke-direct {p0, p1, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@5
    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
    .registers 4
    .parameter "looper"
    .parameter "callback"

    #@0
    .prologue
    .line 145
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    #@4
    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V
    .registers 5
    .parameter "looper"
    .parameter "callback"
    .parameter "async"

    #@0
    .prologue
    .line 224
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 225
    iput-object p1, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@5
    .line 226
    iget-object v0, p1, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@7
    iput-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@9
    .line 227
    iput-object p2, p0, Landroid/os/Handler;->mCallback:Landroid/os/Handler$Callback;

    #@b
    .line 228
    iput-boolean p3, p0, Landroid/os/Handler;->mAsynchronous:Z

    #@d
    .line 229
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 3
    .parameter "async"

    #@0
    .prologue
    .line 165
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;Z)V

    #@4
    .line 166
    return-void
.end method

.method private enqueueMessage(Landroid/os/MessageQueue;Landroid/os/Message;J)Z
    .registers 6
    .parameter "queue"
    .parameter "msg"
    .parameter "uptimeMillis"

    #@0
    .prologue
    .line 614
    iput-object p0, p2, Landroid/os/Message;->target:Landroid/os/Handler;

    #@2
    .line 615
    iget-boolean v0, p0, Landroid/os/Handler;->mAsynchronous:Z

    #@4
    if-eqz v0, :cond_a

    #@6
    .line 616
    const/4 v0, 0x1

    #@7
    invoke-virtual {p2, v0}, Landroid/os/Message;->setAsynchronous(Z)V

    #@a
    .line 618
    :cond_a
    invoke-virtual {p1, p2, p3, p4}, Landroid/os/MessageQueue;->enqueueMessage(Landroid/os/Message;J)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method private static getPostMessage(Ljava/lang/Runnable;)Landroid/os/Message;
    .registers 2
    .parameter "r"

    #@0
    .prologue
    .line 712
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 713
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@6
    .line 714
    return-object v0
.end method

.method private static getPostMessage(Ljava/lang/Runnable;Ljava/lang/Object;)Landroid/os/Message;
    .registers 3
    .parameter "r"
    .parameter "token"

    #@0
    .prologue
    .line 718
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 719
    .local v0, m:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@6
    .line 720
    iput-object p0, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@8
    .line 721
    return-object v0
.end method

.method private static handleCallback(Landroid/os/Message;)V
    .registers 2
    .parameter "message"

    #@0
    .prologue
    .line 725
    iget-object v0, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    #@5
    .line 726
    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 91
    iget-object v0, p1, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_8

    #@4
    .line 92
    invoke-static {p1}, Landroid/os/Handler;->handleCallback(Landroid/os/Message;)V

    #@7
    .line 101
    :cond_7
    :goto_7
    return-void

    #@8
    .line 94
    :cond_8
    iget-object v0, p0, Landroid/os/Handler;->mCallback:Landroid/os/Handler$Callback;

    #@a
    if-eqz v0, :cond_14

    #@c
    .line 95
    iget-object v0, p0, Landroid/os/Handler;->mCallback:Landroid/os/Handler$Callback;

    #@e
    invoke-interface {v0, p1}, Landroid/os/Handler$Callback;->handleMessage(Landroid/os/Message;)Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_7

    #@14
    .line 99
    :cond_14
    invoke-virtual {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    #@17
    goto :goto_7
.end method

.method public final dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 6
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 680
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    const-string v1, " @ "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@16
    move-result-wide v1

    #@17
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@22
    .line 681
    iget-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@24
    if-nez v0, :cond_3e

    #@26
    .line 682
    new-instance v0, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    const-string/jumbo v1, "looper uninitialized"

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@3d
    .line 686
    :goto_3d
    return-void

    #@3e
    .line 684
    :cond_3e
    iget-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@40
    new-instance v1, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    const-string v2, "  "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v0, p1, v1}, Landroid/os/Looper;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@56
    goto :goto_3d
.end method

.method final getIMessenger()Landroid/os/IMessenger;
    .registers 4

    #@0
    .prologue
    .line 696
    iget-object v1, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    monitor-enter v1

    #@3
    .line 697
    :try_start_3
    iget-object v0, p0, Landroid/os/Handler;->mMessenger:Landroid/os/IMessenger;

    #@5
    if-eqz v0, :cond_b

    #@7
    .line 698
    iget-object v0, p0, Landroid/os/Handler;->mMessenger:Landroid/os/IMessenger;

    #@9
    monitor-exit v1

    #@a
    .line 701
    :goto_a
    return-object v0

    #@b
    .line 700
    :cond_b
    new-instance v0, Landroid/os/Handler$MessengerImpl;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-direct {v0, p0, v2}, Landroid/os/Handler$MessengerImpl;-><init>(Landroid/os/Handler;Landroid/os/Handler$1;)V

    #@11
    iput-object v0, p0, Landroid/os/Handler;->mMessenger:Landroid/os/IMessenger;

    #@13
    .line 701
    iget-object v0, p0, Landroid/os/Handler;->mMessenger:Landroid/os/IMessenger;

    #@15
    monitor-exit v1

    #@16
    goto :goto_a

    #@17
    .line 702
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public final getLooper()Landroid/os/Looper;
    .registers 2

    #@0
    .prologue
    .line 676
    iget-object v0, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@2
    return-object v0
.end method

.method public getMessageName(Landroid/os/Message;)Ljava/lang/String;
    .registers 4
    .parameter "message"

    #@0
    .prologue
    .line 240
    iget-object v0, p1, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 241
    iget-object v0, p1, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@6
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@d
    move-result-object v0

    #@e
    .line 243
    :goto_e
    return-object v0

    #@f
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v1, "0x"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    iget v1, p1, Landroid/os/Message;->what:I

    #@1c
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    goto :goto_e
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 85
    return-void
.end method

.method public final hasCallbacks(Ljava/lang/Runnable;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 670
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p0, p1, v1}, Landroid/os/MessageQueue;->hasMessages(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final hasMessages(I)Z
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 652
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p0, p1, v1}, Landroid/os/MessageQueue;->hasMessages(Landroid/os/Handler;ILjava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final hasMessages(ILjava/lang/Object;)Z
    .registers 4
    .parameter "what"
    .parameter "object"

    #@0
    .prologue
    .line 660
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Landroid/os/MessageQueue;->hasMessages(Landroid/os/Handler;ILjava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final obtainMessage()Landroid/os/Message;
    .registers 2

    #@0
    .prologue
    .line 253
    invoke-static {p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final obtainMessage(I)Landroid/os/Message;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 264
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final obtainMessage(III)Landroid/os/Message;
    .registers 5
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 292
    invoke-static {p0, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
    .registers 6
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 307
    invoke-static {p0, p1, p2, p3, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
    .registers 4
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 278
    invoke-static {p0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public final post(Ljava/lang/Runnable;)Z
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 323
    invoke-static {p1}, Landroid/os/Handler;->getPostMessage(Ljava/lang/Runnable;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    const-wide/16 v1, 0x0

    #@6
    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public final postAtFrontOfQueue(Ljava/lang/Runnable;)Z
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 411
    invoke-static {p1}, Landroid/os/Handler;->getPostMessage(Ljava/lang/Runnable;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final postAtTime(Ljava/lang/Runnable;J)Z
    .registers 5
    .parameter "r"
    .parameter "uptimeMillis"

    #@0
    .prologue
    .line 345
    invoke-static {p1}, Landroid/os/Handler;->getPostMessage(Ljava/lang/Runnable;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z
    .registers 6
    .parameter "r"
    .parameter "token"
    .parameter "uptimeMillis"

    #@0
    .prologue
    .line 369
    invoke-static {p1, p2}, Landroid/os/Handler;->getPostMessage(Ljava/lang/Runnable;Ljava/lang/Object;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p3, p4}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final postDelayed(Ljava/lang/Runnable;J)Z
    .registers 5
    .parameter "r"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 391
    invoke-static {p1}, Landroid/os/Handler;->getPostMessage(Ljava/lang/Runnable;)Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final removeCallbacks(Ljava/lang/Runnable;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 471
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p0, p1, v1}, Landroid/os/MessageQueue;->removeMessages(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V

    #@6
    .line 472
    return-void
.end method

.method public final removeCallbacks(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .registers 4
    .parameter "r"
    .parameter "token"

    #@0
    .prologue
    .line 481
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Landroid/os/MessageQueue;->removeMessages(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;)V

    #@5
    .line 482
    return-void
.end method

.method public final removeCallbacksAndMessages(Ljava/lang/Object;)V
    .registers 3
    .parameter "token"

    #@0
    .prologue
    .line 644
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0, p0, p1}, Landroid/os/MessageQueue;->removeCallbacksAndMessages(Landroid/os/Handler;Ljava/lang/Object;)V

    #@5
    .line 645
    return-void
.end method

.method public final removeMessages(I)V
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 626
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p0, p1, v1}, Landroid/os/MessageQueue;->removeMessages(Landroid/os/Handler;ILjava/lang/Object;)V

    #@6
    .line 627
    return-void
.end method

.method public final removeMessages(ILjava/lang/Object;)V
    .registers 4
    .parameter "what"
    .parameter "object"

    #@0
    .prologue
    .line 635
    iget-object v0, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    invoke-virtual {v0, p0, p1, p2}, Landroid/os/MessageQueue;->removeMessages(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5
    .line 636
    return-void
.end method

.method public final runWithScissors(Ljava/lang/Runnable;J)Z
    .registers 7
    .parameter "r"
    .parameter "timeout"

    #@0
    .prologue
    .line 450
    if-nez p1, :cond_b

    #@2
    .line 451
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "runnable must not be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 453
    :cond_b
    const-wide/16 v1, 0x0

    #@d
    cmp-long v1, p2, v1

    #@f
    if-gez v1, :cond_1a

    #@11
    .line 454
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string/jumbo v2, "timeout must be non-negative"

    #@16
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v1

    #@1a
    .line 457
    :cond_1a
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@1d
    move-result-object v1

    #@1e
    iget-object v2, p0, Landroid/os/Handler;->mLooper:Landroid/os/Looper;

    #@20
    if-ne v1, v2, :cond_27

    #@22
    .line 458
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    #@25
    .line 459
    const/4 v1, 0x1

    #@26
    .line 463
    :goto_26
    return v1

    #@27
    .line 462
    :cond_27
    new-instance v0, Landroid/os/Handler$BlockingRunnable;

    #@29
    invoke-direct {v0, p1}, Landroid/os/Handler$BlockingRunnable;-><init>(Ljava/lang/Runnable;)V

    #@2c
    .line 463
    .local v0, br:Landroid/os/Handler$BlockingRunnable;
    invoke-virtual {v0, p0, p2, p3}, Landroid/os/Handler$BlockingRunnable;->postAndWait(Landroid/os/Handler;J)Z

    #@2f
    move-result v1

    #@30
    goto :goto_26
.end method

.method public final sendEmptyMessage(I)Z
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 507
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final sendEmptyMessageAtTime(IJ)Z
    .registers 6
    .parameter "what"
    .parameter "uptimeMillis"

    #@0
    .prologue
    .line 536
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 537
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 538
    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@9
    move-result v1

    #@a
    return v1
.end method

.method public final sendEmptyMessageDelayed(IJ)Z
    .registers 6
    .parameter "what"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 520
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 521
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@6
    .line 522
    invoke-virtual {p0, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@9
    move-result v1

    #@a
    return v1
.end method

.method public final sendMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 495
    const-wide/16 v0, 0x0

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 603
    iget-object v1, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    .line 604
    .local v1, queue:Landroid/os/MessageQueue;
    if-nez v1, :cond_27

    #@4
    .line 605
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, " sendMessageAtTime() called with no mQueue"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    .line 607
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "Looper"

    #@1e
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    .line 608
    const/4 v2, 0x0

    #@26
    .line 610
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_26
    return v2

    #@27
    :cond_27
    const-wide/16 v2, 0x0

    #@29
    invoke-direct {p0, v1, p1, v2, v3}, Landroid/os/Handler;->enqueueMessage(Landroid/os/MessageQueue;Landroid/os/Message;J)Z

    #@2c
    move-result v2

    #@2d
    goto :goto_26
.end method

.method public sendMessageAtTime(Landroid/os/Message;J)Z
    .registers 8
    .parameter "msg"
    .parameter "uptimeMillis"

    #@0
    .prologue
    .line 580
    iget-object v1, p0, Landroid/os/Handler;->mQueue:Landroid/os/MessageQueue;

    #@2
    .line 581
    .local v1, queue:Landroid/os/MessageQueue;
    if-nez v1, :cond_27

    #@4
    .line 582
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, " sendMessageAtTime() called with no mQueue"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    .line 584
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v2, "Looper"

    #@1e
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@25
    .line 585
    const/4 v2, 0x0

    #@26
    .line 587
    .end local v0           #e:Ljava/lang/RuntimeException;
    :goto_26
    return v2

    #@27
    :cond_27
    invoke-direct {p0, v1, p1, p2, p3}, Landroid/os/Handler;->enqueueMessage(Landroid/os/MessageQueue;Landroid/os/Message;J)Z

    #@2a
    move-result v2

    #@2b
    goto :goto_26
.end method

.method public final sendMessageDelayed(Landroid/os/Message;J)Z
    .registers 6
    .parameter "msg"
    .parameter "delayMillis"

    #@0
    .prologue
    .line 555
    const-wide/16 v0, 0x0

    #@2
    cmp-long v0, p2, v0

    #@4
    if-gez v0, :cond_8

    #@6
    .line 556
    const-wide/16 p2, 0x0

    #@8
    .line 558
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@b
    move-result-wide v0

    #@c
    add-long/2addr v0, p2

    #@d
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 690
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Handler ("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, ") {"

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@20
    move-result v1

    #@21
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string/jumbo v1, "}"

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    return-object v0
.end method
