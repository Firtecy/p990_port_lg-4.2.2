.class public abstract Landroid/os/IOemExtendedApi3LM$Stub;
.super Landroid/os/Binder;
.source "IOemExtendedApi3LM.java"

# interfaces
.implements Landroid/os/IOemExtendedApi3LM;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/IOemExtendedApi3LM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/IOemExtendedApi3LM$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.IOemExtendedApi3LM"

.field static final TRANSACTION_clear:I = 0x2

.field static final TRANSACTION_getFelicaState:I = 0x3

.field static final TRANSACTION_getInfraredState:I = 0x7

.field static final TRANSACTION_getOneSegState:I = 0x5

.field static final TRANSACTION_getVersion:I = 0x1

.field static final TRANSACTION_setEmergencyLock:I = 0x9

.field static final TRANSACTION_setFelicaState:I = 0x4

.field static final TRANSACTION_setInfraredState:I = 0x8

.field static final TRANSACTION_setOneSegState:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.os.IOemExtendedApi3LM"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/IOemExtendedApi3LM$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/IOemExtendedApi3LM;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.os.IOemExtendedApi3LM"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/IOemExtendedApi3LM;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/os/IOemExtendedApi3LM;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/os/IOemExtendedApi3LM$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/IOemExtendedApi3LM$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_b8

    #@5
    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v4

    #@9
    :goto_9
    return v4

    #@a
    .line 43
    :sswitch_a
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p0}, Landroid/os/IOemExtendedApi3LM$Stub;->getVersion()I

    #@18
    move-result v3

    #@19
    .line 50
    .local v3, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c
    .line 51
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    goto :goto_9

    #@20
    .line 56
    .end local v3           #_result:I
    :sswitch_20
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@22
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 57
    invoke-virtual {p0}, Landroid/os/IOemExtendedApi3LM$Stub;->clear()V

    #@28
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b
    goto :goto_9

    #@2c
    .line 63
    :sswitch_2c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@2e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 64
    invoke-virtual {p0}, Landroid/os/IOemExtendedApi3LM$Stub;->getFelicaState()I

    #@34
    move-result v3

    #@35
    .line 65
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 66
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    goto :goto_9

    #@3c
    .line 71
    .end local v3           #_result:I
    :sswitch_3c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@3e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v0

    #@45
    .line 74
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IOemExtendedApi3LM$Stub;->setFelicaState(I)V

    #@48
    .line 75
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b
    goto :goto_9

    #@4c
    .line 80
    .end local v0           #_arg0:I
    :sswitch_4c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@4e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 81
    invoke-virtual {p0}, Landroid/os/IOemExtendedApi3LM$Stub;->getOneSegState()I

    #@54
    move-result v3

    #@55
    .line 82
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58
    .line 83
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_9

    #@5c
    .line 88
    .end local v3           #_result:I
    :sswitch_5c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@5e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@64
    move-result v0

    #@65
    .line 91
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IOemExtendedApi3LM$Stub;->setOneSegState(I)V

    #@68
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6b
    goto :goto_9

    #@6c
    .line 97
    .end local v0           #_arg0:I
    :sswitch_6c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@6e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 98
    invoke-virtual {p0}, Landroid/os/IOemExtendedApi3LM$Stub;->getInfraredState()I

    #@74
    move-result v3

    #@75
    .line 99
    .restart local v3       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    .line 100
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@7b
    goto :goto_9

    #@7c
    .line 105
    .end local v3           #_result:I
    :sswitch_7c
    const-string v5, "android.os.IOemExtendedApi3LM"

    #@7e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@81
    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@84
    move-result v0

    #@85
    .line 108
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IOemExtendedApi3LM$Stub;->setInfraredState(I)V

    #@88
    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8b
    goto/16 :goto_9

    #@8d
    .line 114
    .end local v0           #_arg0:I
    :sswitch_8d
    const-string v6, "android.os.IOemExtendedApi3LM"

    #@8f
    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@92
    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@95
    move-result v6

    #@96
    if-eqz v6, :cond_b3

    #@98
    move v0, v4

    #@99
    .line 118
    .local v0, _arg0:Z
    :goto_99
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9c
    move-result-object v1

    #@9d
    .line 120
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v6

    #@a1
    if-eqz v6, :cond_b5

    #@a3
    move v2, v4

    #@a4
    .line 121
    .local v2, _arg2:Z
    :goto_a4
    invoke-virtual {p0, v0, v1, v2}, Landroid/os/IOemExtendedApi3LM$Stub;->setEmergencyLock(ZLjava/lang/String;Z)Z

    #@a7
    move-result v3

    #@a8
    .line 122
    .local v3, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@ab
    .line 123
    if-eqz v3, :cond_ae

    #@ad
    move v5, v4

    #@ae
    :cond_ae
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@b1
    goto/16 :goto_9

    #@b3
    .end local v0           #_arg0:Z
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Z
    .end local v3           #_result:Z
    :cond_b3
    move v0, v5

    #@b4
    .line 116
    goto :goto_99

    #@b5
    .restart local v0       #_arg0:Z
    .restart local v1       #_arg1:Ljava/lang/String;
    :cond_b5
    move v2, v5

    #@b6
    .line 120
    goto :goto_a4

    #@b7
    .line 39
    nop

    #@b8
    :sswitch_data_b8
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_20
        0x3 -> :sswitch_2c
        0x4 -> :sswitch_3c
        0x5 -> :sswitch_4c
        0x6 -> :sswitch_5c
        0x7 -> :sswitch_6c
        0x8 -> :sswitch_7c
        0x9 -> :sswitch_8d
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
