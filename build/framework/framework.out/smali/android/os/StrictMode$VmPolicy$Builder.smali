.class public final Landroid/os/StrictMode$VmPolicy$Builder;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode$VmPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mClassInstanceLimit:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mClassInstanceLimitNeedCow:Z

.field private mMask:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 578
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 576
    iput-boolean v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimitNeedCow:Z

    #@6
    .line 579
    iput v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@8
    .line 580
    return-void
.end method

.method public constructor <init>(Landroid/os/StrictMode$VmPolicy;)V
    .registers 3
    .parameter "base"

    #@0
    .prologue
    .line 585
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 576
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimitNeedCow:Z

    #@6
    .line 586
    iget v0, p1, Landroid/os/StrictMode$VmPolicy;->mask:I

    #@8
    iput v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@a
    .line 587
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimitNeedCow:Z

    #@d
    .line 588
    iget-object v0, p1, Landroid/os/StrictMode$VmPolicy;->classInstanceLimit:Ljava/util/HashMap;

    #@f
    iput-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@11
    .line 589
    return-void
.end method

.method private enable(I)Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 3
    .parameter "bit"

    #@0
    .prologue
    .line 695
    iget v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@5
    .line 696
    return-object p0
.end method


# virtual methods
.method public build()Landroid/os/StrictMode$VmPolicy;
    .registers 5

    #@0
    .prologue
    .line 709
    iget v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@2
    if-eqz v0, :cond_d

    #@4
    iget v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@6
    and-int/lit16 v0, v0, 0xf0

    #@8
    if-nez v0, :cond_d

    #@a
    .line 712
    invoke-virtual {p0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    #@d
    .line 714
    :cond_d
    new-instance v1, Landroid/os/StrictMode$VmPolicy;

    #@f
    iget v2, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@11
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@13
    if-eqz v0, :cond_1c

    #@15
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@17
    :goto_17
    const/4 v3, 0x0

    #@18
    invoke-direct {v1, v2, v0, v3}, Landroid/os/StrictMode$VmPolicy;-><init>(ILjava/util/HashMap;Landroid/os/StrictMode$1;)V

    #@1b
    return-object v1

    #@1c
    :cond_1c
    invoke-static {}, Landroid/os/StrictMode;->access$100()Ljava/util/HashMap;

    #@1f
    move-result-object v0

    #@20
    goto :goto_17
.end method

.method public detectActivityLeaks()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 619
    const/16 v0, 0x800

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectAll()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 630
    const/16 v0, 0x2e00

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectLeakedClosableObjects()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 656
    const/16 v0, 0x400

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectLeakedRegistrationObjects()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 665
    const/16 v0, 0x2000

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public detectLeakedSqlLiteObjects()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 644
    const/16 v0, 0x200

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyDeath()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 674
    const/16 v0, 0x40

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyDropBox()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 691
    const/16 v0, 0x80

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 2

    #@0
    .prologue
    .line 681
    const/16 v0, 0x10

    #@2
    invoke-direct {p0, v0}, Landroid/os/StrictMode$VmPolicy$Builder;->enable(I)Landroid/os/StrictMode$VmPolicy$Builder;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public setClassInstanceLimit(Ljava/lang/Class;I)Landroid/os/StrictMode$VmPolicy$Builder;
    .registers 5
    .parameter "klass"
    .parameter "instanceLimit"

    #@0
    .prologue
    .line 596
    if-nez p1, :cond_b

    #@2
    .line 597
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "klass == null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 599
    :cond_b
    iget-boolean v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimitNeedCow:Z

    #@d
    if-eqz v0, :cond_43

    #@f
    .line 600
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@11
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_26

    #@17
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@19
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v0

    #@1d
    check-cast v0, Ljava/lang/Integer;

    #@1f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@22
    move-result v0

    #@23
    if-ne v0, p2, :cond_26

    #@25
    .line 612
    :goto_25
    return-object p0

    #@26
    .line 605
    :cond_26
    const/4 v0, 0x0

    #@27
    iput-boolean v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimitNeedCow:Z

    #@29
    .line 606
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@2b
    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Ljava/util/HashMap;

    #@31
    iput-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@33
    .line 610
    :cond_33
    :goto_33
    iget v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@35
    or-int/lit16 v0, v0, 0x1000

    #@37
    iput v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mMask:I

    #@39
    .line 611
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@3b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@42
    goto :goto_25

    #@43
    .line 607
    :cond_43
    iget-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@45
    if-nez v0, :cond_33

    #@47
    .line 608
    new-instance v0, Ljava/util/HashMap;

    #@49
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@4c
    iput-object v0, p0, Landroid/os/StrictMode$VmPolicy$Builder;->mClassInstanceLimit:Ljava/util/HashMap;

    #@4e
    goto :goto_33
.end method
