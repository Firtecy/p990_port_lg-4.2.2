.class public Landroid/os/CommonClock;
.super Ljava/lang/Object;
.source "CommonClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/CommonClock$TimelineChangedListener;,
        Landroid/os/CommonClock$OnServerDiedListener;,
        Landroid/os/CommonClock$OnTimelineChangedListener;
    }
.end annotation


# static fields
.field public static final ERROR_ESTIMATE_UNKNOWN:I = 0x7fffffff

.field public static final INVALID_TIMELINE_ID:J = 0x0L

.field private static final METHOD_CBK_ON_TIMELINE_CHANGED:I = 0x1

.field private static final METHOD_COMMON_TIME_TO_LOCAL_TIME:I = 0x2

.field private static final METHOD_GET_COMMON_FREQ:I = 0x5

.field private static final METHOD_GET_COMMON_TIME:I = 0x4

.field private static final METHOD_GET_ESTIMATED_ERROR:I = 0x8

.field private static final METHOD_GET_LOCAL_FREQ:I = 0x7

.field private static final METHOD_GET_LOCAL_TIME:I = 0x6

.field private static final METHOD_GET_MASTER_ADDRESS:I = 0xb

.field private static final METHOD_GET_STATE:I = 0xa

.field private static final METHOD_GET_TIMELINE_ID:I = 0x9

.field private static final METHOD_IS_COMMON_TIME_VALID:I = 0x1

.field private static final METHOD_LOCAL_TIME_TO_COMMON_TIME:I = 0x3

.field private static final METHOD_REGISTER_LISTENER:I = 0xc

.field private static final METHOD_UNREGISTER_LISTENER:I = 0xd

.field public static final SERVICE_NAME:Ljava/lang/String; = "common_time.clock"

.field public static final STATE_CLIENT:I = 0x1

.field public static final STATE_INITIAL:I = 0x0

.field public static final STATE_INVALID:I = -0x1

.field public static final STATE_MASTER:I = 0x2

.field public static final STATE_RONIN:I = 0x3

.field public static final STATE_WAIT_FOR_ELECTION:I = 0x4

.field public static final TIME_NOT_SYNCED:J = -0x1L


# instance fields
.field private mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

.field private mDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private mInterfaceDesc:Ljava/lang/String;

.field private final mListenerLock:Ljava/lang/Object;

.field private mRemote:Landroid/os/IBinder;

.field private mServerDiedListener:Landroid/os/CommonClock$OnServerDiedListener;

.field private mTimelineChangedListener:Landroid/os/CommonClock$OnTimelineChangedListener;

.field private mUtils:Landroid/os/CommonTimeUtils;


# direct methods
.method public constructor <init>()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 121
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 306
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/os/CommonClock;->mListenerLock:Ljava/lang/Object;

    #@b
    .line 307
    iput-object v1, p0, Landroid/os/CommonClock;->mTimelineChangedListener:Landroid/os/CommonClock$OnTimelineChangedListener;

    #@d
    .line 308
    iput-object v1, p0, Landroid/os/CommonClock;->mServerDiedListener:Landroid/os/CommonClock$OnServerDiedListener;

    #@f
    .line 310
    iput-object v1, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@11
    .line 311
    const-string v0, ""

    #@13
    iput-object v0, p0, Landroid/os/CommonClock;->mInterfaceDesc:Ljava/lang/String;

    #@15
    .line 314
    new-instance v0, Landroid/os/CommonClock$1;

    #@17
    invoke-direct {v0, p0}, Landroid/os/CommonClock$1;-><init>(Landroid/os/CommonClock;)V

    #@1a
    iput-object v0, p0, Landroid/os/CommonClock;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@1c
    .line 344
    iput-object v1, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@1e
    .line 122
    const-string v0, "common_time.clock"

    #@20
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@26
    .line 123
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@28
    if-nez v0, :cond_30

    #@2a
    .line 124
    new-instance v0, Landroid/os/RemoteException;

    #@2c
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@2f
    throw v0

    #@30
    .line 126
    :cond_30
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@32
    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Landroid/os/CommonClock;->mInterfaceDesc:Ljava/lang/String;

    #@38
    .line 127
    new-instance v0, Landroid/os/CommonTimeUtils;

    #@3a
    iget-object v1, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@3c
    iget-object v2, p0, Landroid/os/CommonClock;->mInterfaceDesc:Ljava/lang/String;

    #@3e
    invoke-direct {v0, v1, v2}, Landroid/os/CommonTimeUtils;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    #@41
    iput-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@43
    .line 128
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@45
    iget-object v1, p0, Landroid/os/CommonClock;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@47
    const/4 v2, 0x0

    #@48
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@4b
    .line 129
    invoke-direct {p0}, Landroid/os/CommonClock;->registerTimelineChangeListener()V

    #@4e
    .line 130
    return-void
.end method

.method static synthetic access$000(Landroid/os/CommonClock;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/os/CommonClock;->mListenerLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Landroid/os/CommonClock;)Landroid/os/CommonClock$OnServerDiedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/os/CommonClock;->mServerDiedListener:Landroid/os/CommonClock$OnServerDiedListener;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/os/CommonClock;)Landroid/os/CommonClock$OnTimelineChangedListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 41
    iget-object v0, p0, Landroid/os/CommonClock;->mTimelineChangedListener:Landroid/os/CommonClock$OnTimelineChangedListener;

    #@2
    return-object v0
.end method

.method public static create()Landroid/os/CommonClock;
    .registers 2

    #@0
    .prologue
    .line 139
    :try_start_0
    new-instance v1, Landroid/os/CommonClock;

    #@2
    invoke-direct {v1}, Landroid/os/CommonClock;-><init>()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 145
    .local v1, retVal:Landroid/os/CommonClock;
    :goto_5
    return-object v1

    #@6
    .line 141
    .end local v1           #retVal:Landroid/os/CommonClock;
    :catch_6
    move-exception v0

    #@7
    .line 142
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@8
    .restart local v1       #retVal:Landroid/os/CommonClock;
    goto :goto_5
.end method

.method private registerTimelineChangeListener()V
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v8, 0x0

    #@2
    .line 347
    iget-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@4
    if-eqz v5, :cond_7

    #@6
    .line 377
    :cond_6
    :goto_6
    return-void

    #@7
    .line 350
    :cond_7
    const/4 v3, 0x0

    #@8
    .line 351
    .local v3, success:Z
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@b
    move-result-object v0

    #@c
    .line 352
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@f
    move-result-object v2

    #@10
    .line 353
    .local v2, reply:Landroid/os/Parcel;
    new-instance v5, Landroid/os/CommonClock$TimelineChangedListener;

    #@12
    invoke-direct {v5, p0, v8}, Landroid/os/CommonClock$TimelineChangedListener;-><init>(Landroid/os/CommonClock;Landroid/os/CommonClock$1;)V

    #@15
    iput-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@17
    .line 356
    :try_start_17
    iget-object v5, p0, Landroid/os/CommonClock;->mInterfaceDesc:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@1c
    .line 357
    iget-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@1e
    invoke-virtual {v0, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@21
    .line 358
    iget-object v5, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v6, 0xc

    #@25
    const/4 v7, 0x0

    #@26
    invoke-interface {v5, v6, v0, v2, v7}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 359
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_2c
    .catchall {:try_start_17 .. :try_end_2c} :catchall_4a
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_2c} :catch_41

    #@2c
    move-result v5

    #@2d
    if-nez v5, :cond_3f

    #@2f
    const/4 v3, 0x1

    #@30
    .line 365
    :goto_30
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 366
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 372
    :goto_36
    if-nez v3, :cond_6

    #@38
    .line 373
    iput-object v8, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@3a
    .line 374
    iput-object v8, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@3c
    .line 375
    iput-object v8, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@3e
    goto :goto_6

    #@3f
    :cond_3f
    move v3, v4

    #@40
    .line 359
    goto :goto_30

    #@41
    .line 361
    :catch_41
    move-exception v1

    #@42
    .line 362
    .local v1, e:Landroid/os/RemoteException;
    const/4 v3, 0x0

    #@43
    .line 365
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 366
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@49
    goto :goto_36

    #@4a
    .line 365
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_4a
    move-exception v4

    #@4b
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@4e
    .line 366
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@51
    throw v4
.end method

.method private throwOnDeadServer()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 302
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@2
    if-eqz v0, :cond_8

    #@4
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@6
    if-nez v0, :cond_e

    #@8
    .line 303
    :cond_8
    new-instance v0, Landroid/os/RemoteException;

    #@a
    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    #@d
    throw v0

    #@e
    .line 304
    :cond_e
    return-void
.end method

.method private unregisterTimelineChangeListener()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 380
    iget-object v2, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@3
    if-nez v2, :cond_6

    #@5
    .line 397
    :goto_5
    return-void

    #@6
    .line 383
    :cond_6
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v0

    #@a
    .line 384
    .local v0, data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v1

    #@e
    .line 387
    .local v1, reply:Landroid/os/Parcel;
    :try_start_e
    iget-object v2, p0, Landroid/os/CommonClock;->mInterfaceDesc:Ljava/lang/String;

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@13
    .line 388
    iget-object v2, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 389
    iget-object v2, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@1a
    const/16 v3, 0xd

    #@1c
    const/4 v4, 0x0

    #@1d
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_20
    .catchall {:try_start_e .. :try_end_20} :catchall_33
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_20} :catch_29

    #@20
    .line 393
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 394
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 395
    iput-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@28
    goto :goto_5

    #@29
    .line 391
    :catch_29
    move-exception v2

    #@2a
    .line 393
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 394
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 395
    iput-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@32
    goto :goto_5

    #@33
    .line 393
    :catchall_33
    move-exception v2

    #@34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 394
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    .line 395
    iput-object v5, p0, Landroid/os/CommonClock;->mCallbackTgt:Landroid/os/CommonClock$TimelineChangedListener;

    #@3c
    throw v2
.end method


# virtual methods
.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 299
    invoke-virtual {p0}, Landroid/os/CommonClock;->release()V

    #@3
    return-void
.end method

.method public getEstimatedError()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 198
    invoke-direct {p0}, Landroid/os/CommonClock;->throwOnDeadServer()V

    #@3
    .line 199
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0x8

    #@7
    const v2, 0x7fffffff

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getMasterAddr()Ljava/net/InetSocketAddress;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 239
    invoke-direct {p0}, Landroid/os/CommonClock;->throwOnDeadServer()V

    #@3
    .line 240
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0xb

    #@7
    invoke-virtual {v0, v1}, Landroid/os/CommonTimeUtils;->transactGetSockaddr(I)Ljava/net/InetSocketAddress;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getState()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 226
    invoke-direct {p0}, Landroid/os/CommonClock;->throwOnDeadServer()V

    #@3
    .line 227
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0xa

    #@7
    const/4 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2}, Landroid/os/CommonTimeUtils;->transactGetInt(II)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getTime()J
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    invoke-direct {p0}, Landroid/os/CommonClock;->throwOnDeadServer()V

    #@3
    .line 180
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/4 v1, 0x4

    #@6
    const-wide/16 v2, -0x1

    #@8
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/CommonTimeUtils;->transactGetLong(IJ)J

    #@b
    move-result-wide v0

    #@c
    return-wide v0
.end method

.method public getTimelineId()J
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 212
    invoke-direct {p0}, Landroid/os/CommonClock;->throwOnDeadServer()V

    #@3
    .line 213
    iget-object v0, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@5
    const/16 v1, 0x9

    #@7
    const-wide/16 v2, 0x0

    #@9
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/CommonTimeUtils;->transactGetLong(IJ)J

    #@c
    move-result-wide v0

    #@d
    return-wide v0
.end method

.method public release()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 158
    invoke-direct {p0}, Landroid/os/CommonClock;->unregisterTimelineChangeListener()V

    #@4
    .line 159
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 161
    :try_start_8
    iget-object v0, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@a
    iget-object v1, p0, Landroid/os/CommonClock;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_10
    .catch Ljava/util/NoSuchElementException; {:try_start_8 .. :try_end_10} :catch_15

    #@10
    .line 164
    :goto_10
    iput-object v3, p0, Landroid/os/CommonClock;->mRemote:Landroid/os/IBinder;

    #@12
    .line 166
    :cond_12
    iput-object v3, p0, Landroid/os/CommonClock;->mUtils:Landroid/os/CommonTimeUtils;

    #@14
    .line 167
    return-void

    #@15
    .line 163
    :catch_15
    move-exception v0

    #@16
    goto :goto_10
.end method

.method public setServerDiedListener(Landroid/os/CommonClock$OnServerDiedListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 294
    iget-object v1, p0, Landroid/os/CommonClock;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 295
    :try_start_3
    iput-object p1, p0, Landroid/os/CommonClock;->mServerDiedListener:Landroid/os/CommonClock$OnServerDiedListener;

    #@5
    .line 296
    monitor-exit v1

    #@6
    .line 297
    return-void

    #@7
    .line 296
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setTimelineChangedListener(Landroid/os/CommonClock$OnTimelineChangedListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 266
    iget-object v1, p0, Landroid/os/CommonClock;->mListenerLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 267
    :try_start_3
    iput-object p1, p0, Landroid/os/CommonClock;->mTimelineChangedListener:Landroid/os/CommonClock$OnTimelineChangedListener;

    #@5
    .line 268
    monitor-exit v1

    #@6
    .line 269
    return-void

    #@7
    .line 268
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method
