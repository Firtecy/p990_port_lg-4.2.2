.class final Landroid/os/StrictMode$InstanceTracker;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InstanceTracker"
.end annotation


# static fields
.field private static final sInstanceCounts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mKlass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2192
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .registers 8
    .parameter "instance"

    #@0
    .prologue
    .line 2197
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 2198
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6
    move-result-object v2

    #@7
    iput-object v2, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@9
    .line 2200
    sget-object v3, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@b
    monitor-enter v3

    #@c
    .line 2201
    :try_start_c
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@e
    iget-object v4, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@10
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    move-result-object v1

    #@14
    check-cast v1, Ljava/lang/Integer;

    #@16
    .line 2202
    .local v1, value:Ljava/lang/Integer;
    if-eqz v1, :cond_2b

    #@18
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@1b
    move-result v2

    #@1c
    add-int/lit8 v0, v2, 0x1

    #@1e
    .line 2203
    .local v0, newValue:I
    :goto_1e
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@20
    iget-object v4, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@22
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v5

    #@26
    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 2204
    monitor-exit v3

    #@2a
    .line 2205
    return-void

    #@2b
    .line 2202
    .end local v0           #newValue:I
    :cond_2b
    const/4 v0, 0x1

    #@2c
    goto :goto_1e

    #@2d
    .line 2204
    .end local v1           #value:Ljava/lang/Integer;
    :catchall_2d
    move-exception v2

    #@2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_c .. :try_end_2f} :catchall_2d

    #@2f
    throw v2
.end method

.method public static getInstanceCount(Ljava/lang/Class;)I
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)I"
        }
    .end annotation

    #@0
    .prologue
    .line 2227
    .local p0, klass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 2228
    :try_start_3
    sget-object v1, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@5
    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Ljava/lang/Integer;

    #@b
    .line 2229
    .local v0, value:Ljava/lang/Integer;
    if-eqz v0, :cond_13

    #@d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@10
    move-result v1

    #@11
    :goto_11
    monitor-exit v2

    #@12
    return v1

    #@13
    :cond_13
    const/4 v1, 0x0

    #@14
    goto :goto_11

    #@15
    .line 2230
    .end local v0           #value:Ljava/lang/Integer;
    :catchall_15
    move-exception v1

    #@16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method


# virtual methods
.method protected finalize()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 2210
    :try_start_0
    sget-object v3, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@2
    monitor-enter v3
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_32

    #@3
    .line 2211
    :try_start_3
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@5
    iget-object v4, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@7
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Ljava/lang/Integer;

    #@d
    .line 2212
    .local v1, value:Ljava/lang/Integer;
    if-eqz v1, :cond_22

    #@f
    .line 2213
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@12
    move-result v2

    #@13
    add-int/lit8 v0, v2, -0x1

    #@15
    .line 2214
    .local v0, newValue:I
    if-lez v0, :cond_27

    #@17
    .line 2215
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@19
    iget-object v4, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 2220
    .end local v0           #newValue:I
    :cond_22
    :goto_22
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_2f

    #@23
    .line 2222
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@26
    .line 2224
    return-void

    #@27
    .line 2217
    .restart local v0       #newValue:I
    :cond_27
    :try_start_27
    sget-object v2, Landroid/os/StrictMode$InstanceTracker;->sInstanceCounts:Ljava/util/HashMap;

    #@29
    iget-object v4, p0, Landroid/os/StrictMode$InstanceTracker;->mKlass:Ljava/lang/Class;

    #@2b
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2e
    goto :goto_22

    #@2f
    .line 2220
    .end local v0           #newValue:I
    .end local v1           #value:Ljava/lang/Integer;
    :catchall_2f
    move-exception v2

    #@30
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_27 .. :try_end_31} :catchall_2f

    #@31
    :try_start_31
    throw v2
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_32

    #@32
    .line 2222
    :catchall_32
    move-exception v2

    #@33
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@36
    throw v2
.end method
