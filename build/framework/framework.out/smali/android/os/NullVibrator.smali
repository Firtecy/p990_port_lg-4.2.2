.class public Landroid/os/NullVibrator;
.super Landroid/os/Vibrator;
.source "NullVibrator.java"


# static fields
.field private static final sInstance:Landroid/os/NullVibrator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    new-instance v0, Landroid/os/NullVibrator;

    #@2
    invoke-direct {v0}, Landroid/os/NullVibrator;-><init>()V

    #@5
    sput-object v0, Landroid/os/NullVibrator;->sInstance:Landroid/os/NullVibrator;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Landroid/os/Vibrator;-><init>()V

    #@3
    .line 30
    return-void
.end method

.method public static getInstance()Landroid/os/NullVibrator;
    .registers 1

    #@0
    .prologue
    .line 33
    sget-object v0, Landroid/os/NullVibrator;->sInstance:Landroid/os/NullVibrator;

    #@2
    return-object v0
.end method


# virtual methods
.method public cancel()V
    .registers 1

    #@0
    .prologue
    .line 54
    return-void
.end method

.method public hasVibrator()Z
    .registers 2

    #@0
    .prologue
    .line 38
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public vibrate(J)V
    .registers 3
    .parameter "milliseconds"

    #@0
    .prologue
    .line 43
    return-void
.end method

.method public vibrate([JI)V
    .registers 4
    .parameter "pattern"
    .parameter "repeat"

    #@0
    .prologue
    .line 47
    array-length v0, p1

    #@1
    if-lt p2, v0, :cond_9

    #@3
    .line 48
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@5
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@8
    throw v0

    #@9
    .line 50
    :cond_9
    return-void
.end method
