.class public final Landroid/os/LatencyTimer;
.super Ljava/lang/Object;
.source "LatencyTimer.java"


# instance fields
.field final TAG:Ljava/lang/String;

.field final mSampleSize:I

.field final mScaleFactor:I

.field volatile store:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[J>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "sampleSize"
    .parameter "scaleFactor"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    const-string v0, "LatencyTimer"

    #@5
    iput-object v0, p0, Landroid/os/LatencyTimer;->TAG:Ljava/lang/String;

    #@7
    .line 42
    new-instance v0, Ljava/util/HashMap;

    #@9
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@c
    iput-object v0, p0, Landroid/os/LatencyTimer;->store:Ljava/util/HashMap;

    #@e
    .line 51
    if-nez p2, :cond_11

    #@10
    .line 52
    const/4 p2, 0x1

    #@11
    .line 54
    :cond_11
    iput p2, p0, Landroid/os/LatencyTimer;->mScaleFactor:I

    #@13
    .line 55
    iput p1, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@15
    .line 56
    return-void
.end method

.method private getArray(Ljava/lang/String;)[J
    .registers 8
    .parameter "tag"

    #@0
    .prologue
    .line 81
    iget-object v2, p0, Landroid/os/LatencyTimer;->store:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, [J

    #@8
    .line 82
    .local v1, data:[J
    if-nez v1, :cond_2b

    #@a
    .line 83
    iget-object v3, p0, Landroid/os/LatencyTimer;->store:Ljava/util/HashMap;

    #@c
    monitor-enter v3

    #@d
    .line 84
    :try_start_d
    iget-object v2, p0, Landroid/os/LatencyTimer;->store:Ljava/util/HashMap;

    #@f
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    move-result-object v2

    #@13
    move-object v0, v2

    #@14
    check-cast v0, [J

    #@16
    move-object v1, v0

    #@17
    .line 85
    if-nez v1, :cond_2a

    #@19
    .line 86
    iget v2, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    new-array v1, v2, [J

    #@1f
    .line 87
    iget-object v2, p0, Landroid/os/LatencyTimer;->store:Ljava/util/HashMap;

    #@21
    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 88
    iget v2, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@26
    const-wide/16 v4, 0x0

    #@28
    aput-wide v4, v1, v2

    #@2a
    .line 90
    :cond_2a
    monitor-exit v3

    #@2b
    .line 92
    :cond_2b
    return-object v1

    #@2c
    .line 90
    :catchall_2c
    move-exception v2

    #@2d
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_d .. :try_end_2e} :catchall_2c

    #@2e
    throw v2
.end method


# virtual methods
.method public sample(Ljava/lang/String;J)V
    .registers 18
    .parameter "tag"
    .parameter "delta"

    #@0
    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/os/LatencyTimer;->getArray(Ljava/lang/String;)[J

    #@3
    move-result-object v1

    #@4
    .line 68
    .local v1, array:[J
    iget v9, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@6
    aget-wide v10, v1, v9

    #@8
    const-wide/16 v12, 0x1

    #@a
    add-long/2addr v12, v10

    #@b
    aput-wide v12, v1, v9

    #@d
    long-to-int v5, v10

    #@e
    .line 69
    .local v5, index:I
    aput-wide p2, v1, v5

    #@10
    .line 70
    iget v9, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@12
    aget-wide v9, v1, v9

    #@14
    iget v11, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@16
    int-to-long v11, v11

    #@17
    cmp-long v9, v9, v11

    #@19
    if-nez v9, :cond_54

    #@1b
    .line 71
    const-wide/16 v7, 0x0

    #@1d
    .line 72
    .local v7, totalDelta:J
    move-object v0, v1

    #@1e
    .local v0, arr$:[J
    array-length v6, v0

    #@1f
    .local v6, len$:I
    const/4 v4, 0x0

    #@20
    .local v4, i$:I
    :goto_20
    if-ge v4, v6, :cond_2d

    #@22
    aget-wide v2, v0, v4

    #@24
    .line 73
    .local v2, d:J
    iget v9, p0, Landroid/os/LatencyTimer;->mScaleFactor:I

    #@26
    int-to-long v9, v9

    #@27
    div-long v9, v2, v9

    #@29
    add-long/2addr v7, v9

    #@2a
    .line 72
    add-int/lit8 v4, v4, 0x1

    #@2c
    goto :goto_20

    #@2d
    .line 75
    .end local v2           #d:J
    :cond_2d
    iget v9, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@2f
    const-wide/16 v10, 0x0

    #@31
    aput-wide v10, v1, v9

    #@33
    .line 76
    const-string v9, "LatencyTimer"

    #@35
    new-instance v10, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v10

    #@3e
    const-string v11, " average = "

    #@40
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v10

    #@44
    iget v11, p0, Landroid/os/LatencyTimer;->mSampleSize:I

    #@46
    int-to-long v11, v11

    #@47
    div-long v11, v7, v11

    #@49
    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v10

    #@4d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v10

    #@51
    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 78
    .end local v0           #arr$:[J
    .end local v4           #i$:I
    .end local v6           #len$:I
    .end local v7           #totalDelta:J
    :cond_54
    return-void
.end method
