.class public final Landroid/os/UEventObserver$UEvent;
.super Ljava/lang/Object;
.source "UEventObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/UEventObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UEvent"
.end annotation


# instance fields
.field private final mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 9
    .parameter "message"

    #@0
    .prologue
    .line 130
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 128
    new-instance v4, Ljava/util/HashMap;

    #@5
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v4, p0, Landroid/os/UEventObserver$UEvent;->mMap:Ljava/util/HashMap;

    #@a
    .line 131
    const/4 v3, 0x0

    #@b
    .line 132
    .local v3, offset:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v2

    #@f
    .line 134
    .local v2, length:I
    :goto_f
    if-ge v3, v2, :cond_1e

    #@11
    .line 135
    const/16 v4, 0x3d

    #@13
    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    #@16
    move-result v1

    #@17
    .line 136
    .local v1, equals:I
    const/4 v4, 0x0

    #@18
    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    #@1b
    move-result v0

    #@1c
    .line 137
    .local v0, at:I
    if-gez v0, :cond_1f

    #@1e
    .line 147
    .end local v0           #at:I
    .end local v1           #equals:I
    :cond_1e
    return-void

    #@1f
    .line 139
    .restart local v0       #at:I
    .restart local v1       #equals:I
    :cond_1f
    if-le v1, v3, :cond_32

    #@21
    if-ge v1, v0, :cond_32

    #@23
    .line 141
    iget-object v4, p0, Landroid/os/UEventObserver$UEvent;->mMap:Ljava/util/HashMap;

    #@25
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    add-int/lit8 v6, v1, 0x1

    #@2b
    invoke-virtual {p1, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    .line 145
    :cond_32
    add-int/lit8 v3, v0, 0x1

    #@34
    .line 146
    goto :goto_f
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "key"

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/os/UEventObserver$UEvent;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 154
    iget-object v1, p0, Landroid/os/UEventObserver$UEvent;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    .line 155
    .local v0, result:Ljava/lang/String;
    if-nez v0, :cond_b

    #@a
    .end local p2
    :goto_a
    return-object p2

    #@b
    .restart local p2
    :cond_b
    move-object p2, v0

    #@c
    goto :goto_a
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 159
    iget-object v0, p0, Landroid/os/UEventObserver$UEvent;->mMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
