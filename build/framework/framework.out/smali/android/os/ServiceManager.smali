.class public final Landroid/os/ServiceManager;
.super Ljava/lang/Object;
.source "ServiceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceManager"

.field private static sCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field private static sServiceManager:Landroid/os/IServiceManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/os/ServiceManager;->sCache:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static addService(Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 5
    .parameter "name"
    .parameter "service"

    #@0
    .prologue
    .line 72
    :try_start_0
    invoke-static {}, Landroid/os/ServiceManager;->getIServiceManager()Landroid/os/IServiceManager;

    #@3
    move-result-object v1

    #@4
    const/4 v2, 0x0

    #@5
    invoke-interface {v1, p0, p1, v2}, Landroid/os/IServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;Z)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_8} :catch_9

    #@8
    .line 76
    :goto_8
    return-void

    #@9
    .line 73
    :catch_9
    move-exception v0

    #@a
    .line 74
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "ServiceManager"

    #@c
    const-string v2, "error in addService"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    goto :goto_8
.end method

.method public static addService(Ljava/lang/String;Landroid/os/IBinder;Z)V
    .registers 6
    .parameter "name"
    .parameter "service"
    .parameter "allowIsolated"

    #@0
    .prologue
    .line 89
    :try_start_0
    invoke-static {}, Landroid/os/ServiceManager;->getIServiceManager()Landroid/os/IServiceManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1, p0, p1, p2}, Landroid/os/IServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 93
    :goto_7
    return-void

    #@8
    .line 90
    :catch_8
    move-exception v0

    #@9
    .line 91
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "ServiceManager"

    #@b
    const-string v2, "error in addService"

    #@d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    goto :goto_7
.end method

.method public static checkService(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 101
    :try_start_0
    sget-object v2, Landroid/os/ServiceManager;->sCache:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/os/IBinder;

    #@8
    .line 102
    .local v1, service:Landroid/os/IBinder;
    if-eqz v1, :cond_b

    #@a
    .line 109
    .end local v1           #service:Landroid/os/IBinder;
    :goto_a
    return-object v1

    #@b
    .line 105
    .restart local v1       #service:Landroid/os/IBinder;
    :cond_b
    invoke-static {}, Landroid/os/ServiceManager;->getIServiceManager()Landroid/os/IServiceManager;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2, p0}, Landroid/os/IServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 107
    .end local v1           #service:Landroid/os/IBinder;
    :catch_14
    move-exception v0

    #@15
    .line 108
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "ServiceManager"

    #@17
    const-string v3, "error in checkService"

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 109
    const/4 v1, 0x0

    #@1d
    goto :goto_a
.end method

.method private static getIServiceManager()Landroid/os/IServiceManager;
    .registers 1

    #@0
    .prologue
    .line 34
    sget-object v0, Landroid/os/ServiceManager;->sServiceManager:Landroid/os/IServiceManager;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 35
    sget-object v0, Landroid/os/ServiceManager;->sServiceManager:Landroid/os/IServiceManager;

    #@6
    .line 40
    :goto_6
    return-object v0

    #@7
    .line 39
    :cond_7
    invoke-static {}, Lcom/android/internal/os/BinderInternal;->getContextObject()Landroid/os/IBinder;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Landroid/os/ServiceManagerNative;->asInterface(Landroid/os/IBinder;)Landroid/os/IServiceManager;

    #@e
    move-result-object v0

    #@f
    sput-object v0, Landroid/os/ServiceManager;->sServiceManager:Landroid/os/IServiceManager;

    #@11
    .line 40
    sget-object v0, Landroid/os/ServiceManager;->sServiceManager:Landroid/os/IServiceManager;

    #@13
    goto :goto_6
.end method

.method public static getService(Ljava/lang/String;)Landroid/os/IBinder;
    .registers 5
    .parameter "name"

    #@0
    .prologue
    .line 51
    :try_start_0
    sget-object v2, Landroid/os/ServiceManager;->sCache:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/os/IBinder;

    #@8
    .line 52
    .local v1, service:Landroid/os/IBinder;
    if-eqz v1, :cond_b

    #@a
    .line 60
    .end local v1           #service:Landroid/os/IBinder;
    :goto_a
    return-object v1

    #@b
    .line 55
    .restart local v1       #service:Landroid/os/IBinder;
    :cond_b
    invoke-static {}, Landroid/os/ServiceManager;->getIServiceManager()Landroid/os/IServiceManager;

    #@e
    move-result-object v2

    #@f
    invoke-interface {v2, p0}, Landroid/os/IServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    goto :goto_a

    #@14
    .line 57
    .end local v1           #service:Landroid/os/IBinder;
    :catch_14
    move-exception v0

    #@15
    .line 58
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "ServiceManager"

    #@17
    const-string v3, "error in getService"

    #@19
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c
    .line 60
    const/4 v1, 0x0

    #@1d
    goto :goto_a
.end method

.method public static initServiceCache(Ljava/util/Map;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/IBinder;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 134
    .local p0, cache:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Landroid/os/IBinder;>;"
    sget-object v0, Landroid/os/ServiceManager;->sCache:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    #@a
    const-string/jumbo v1, "setServiceCache may only be called once"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 137
    :cond_11
    sget-object v0, Landroid/os/ServiceManager;->sCache:Ljava/util/HashMap;

    #@13
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    #@16
    .line 138
    return-void
.end method

.method public static listServices()[Ljava/lang/String;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    :try_start_0
    invoke-static {}, Landroid/os/ServiceManager;->getIServiceManager()Landroid/os/IServiceManager;

    #@3
    move-result-object v1

    #@4
    invoke-interface {v1}, Landroid/os/IServiceManager;->listServices()[Ljava/lang/String;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_9

    #@7
    move-result-object v1

    #@8
    .line 121
    .local v0, e:Landroid/os/RemoteException;
    :goto_8
    return-object v1

    #@9
    .line 119
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_9
    move-exception v0

    #@a
    .line 120
    .restart local v0       #e:Landroid/os/RemoteException;
    const-string v1, "ServiceManager"

    #@c
    const-string v2, "error in listServices"

    #@e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@11
    .line 121
    const/4 v1, 0x0

    #@12
    goto :goto_8
.end method
