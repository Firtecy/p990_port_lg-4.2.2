.class public final Landroid/os/StrictMode;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/StrictMode$InstanceTracker;,
        Landroid/os/StrictMode$InstanceCountViolation;,
        Landroid/os/StrictMode$ViolationInfo;,
        Landroid/os/StrictMode$ThreadSpanState;,
        Landroid/os/StrictMode$Span;,
        Landroid/os/StrictMode$LogStackTrace;,
        Landroid/os/StrictMode$AndroidCloseGuardReporter;,
        Landroid/os/StrictMode$AndroidBlockGuardPolicy;,
        Landroid/os/StrictMode$StrictModeCustomViolation;,
        Landroid/os/StrictMode$StrictModeDiskWriteViolation;,
        Landroid/os/StrictMode$StrictModeDiskReadViolation;,
        Landroid/os/StrictMode$StrictModeNetworkViolation;,
        Landroid/os/StrictMode$StrictModeViolation;,
        Landroid/os/StrictMode$VmPolicy;,
        Landroid/os/StrictMode$ThreadPolicy;
    }
.end annotation


# static fields
.field private static final ALL_THREAD_DETECT_BITS:I = 0xf

.field private static final ALL_VM_DETECT_BITS:I = 0x3e00

.field public static final DETECT_CUSTOM:I = 0x8

.field public static final DETECT_DISK_READ:I = 0x2

.field public static final DETECT_DISK_WRITE:I = 0x1

.field public static final DETECT_NETWORK:I = 0x4

.field public static final DETECT_VM_ACTIVITY_LEAKS:I = 0x800

.field public static final DETECT_VM_CLOSABLE_LEAKS:I = 0x400

.field public static final DETECT_VM_CURSOR_LEAKS:I = 0x200

.field private static final DETECT_VM_INSTANCE_LEAKS:I = 0x1000

.field public static final DETECT_VM_REGISTRATION_LEAKS:I = 0x2000

.field public static final DISABLE_PROPERTY:Ljava/lang/String; = "persist.sys.strictmode.disable"

.field private static final EMPTY_CLASS_LIMIT_MAP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

#the value of this static final field might be set in the static constructor
.field private static final IS_ENG_BUILD:Z = false

#the value of this static final field might be set in the static constructor
.field private static final IS_USER_BUILD:Z = false

#the value of this static final field might be set in the static constructor
.field private static final LOG_V:Z = false

.field private static final MAX_OFFENSES_PER_LOOP:I = 0xa

.field private static final MAX_SPAN_TAGS:I = 0x14

.field private static final MIN_DIALOG_INTERVAL_MS:J = 0x7530L

.field private static final MIN_LOG_INTERVAL_MS:J = 0x3e8L

.field private static final NO_OP_SPAN:Landroid/os/StrictMode$Span; = null

.field public static final PENALTY_DEATH:I = 0x40

.field public static final PENALTY_DEATH_ON_NETWORK:I = 0x200

.field public static final PENALTY_DIALOG:I = 0x20

.field public static final PENALTY_DROPBOX:I = 0x80

.field public static final PENALTY_FLASH:I = 0x800

.field public static final PENALTY_GATHER:I = 0x100

.field public static final PENALTY_LOG:I = 0x10

.field private static final TAG:Ljava/lang/String; = "StrictMode"

.field private static final THREAD_PENALTY_MASK:I = 0xbf0

.field public static final VISUAL_PROPERTY:Ljava/lang/String; = "persist.sys.strictmode.visual"

.field private static final VM_PENALTY_MASK:I = 0xd0

.field private static final gatheredViolations:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/StrictMode$ViolationInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sDropboxCallsInFlight:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final sExpectedActivityInstanceCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sIsIdlerRegistered:Z

.field private static sLastInstanceCountCheckMillis:J

.field private static final sLastVmViolationTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final sProcessIdleHandler:Landroid/os/MessageQueue$IdleHandler;

.field private static final sThisThreadSpanState:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/os/StrictMode$ThreadSpanState;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sVmPolicy:Landroid/os/StrictMode$VmPolicy;

.field private static volatile sVmPolicyMask:I

.field private static sWindowManager:Landroid/util/Singleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Singleton",
            "<",
            "Landroid/view/IWindowManager;",
            ">;"
        }
    .end annotation
.end field

.field private static final threadHandler:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private static final violationsBeingTimed:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/StrictMode$ViolationInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 116
    const-string v0, "StrictMode"

    #@3
    const/4 v1, 0x2

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    sput-boolean v0, Landroid/os/StrictMode;->LOG_V:Z

    #@a
    .line 118
    const-string/jumbo v0, "user"

    #@d
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v0

    #@13
    sput-boolean v0, Landroid/os/StrictMode;->IS_USER_BUILD:Z

    #@15
    .line 119
    const-string v0, "eng"

    #@17
    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v0

    #@1d
    sput-boolean v0, Landroid/os/StrictMode;->IS_ENG_BUILD:Z

    #@1f
    .line 279
    new-instance v0, Ljava/util/HashMap;

    #@21
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@24
    sput-object v0, Landroid/os/StrictMode;->EMPTY_CLASS_LIMIT_MAP:Ljava/util/HashMap;

    #@26
    .line 286
    sput v2, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@28
    .line 287
    sget-object v0, Landroid/os/StrictMode$VmPolicy;->LAX:Landroid/os/StrictMode$VmPolicy;

    #@2a
    sput-object v0, Landroid/os/StrictMode;->sVmPolicy:Landroid/os/StrictMode$VmPolicy;

    #@2c
    .line 293
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@2e
    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@31
    sput-object v0, Landroid/os/StrictMode;->sDropboxCallsInFlight:Ljava/util/concurrent/atomic/AtomicInteger;

    #@33
    .line 726
    new-instance v0, Landroid/os/StrictMode$1;

    #@35
    invoke-direct {v0}, Landroid/os/StrictMode$1;-><init>()V

    #@38
    sput-object v0, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@3a
    .line 1035
    new-instance v0, Landroid/os/StrictMode$2;

    #@3c
    invoke-direct {v0}, Landroid/os/StrictMode$2;-><init>()V

    #@3f
    sput-object v0, Landroid/os/StrictMode;->violationsBeingTimed:Ljava/lang/ThreadLocal;

    #@41
    .line 1043
    new-instance v0, Landroid/os/StrictMode$3;

    #@43
    invoke-direct {v0}, Landroid/os/StrictMode$3;-><init>()V

    #@46
    sput-object v0, Landroid/os/StrictMode;->threadHandler:Ljava/lang/ThreadLocal;

    #@48
    .line 1436
    const-wide/16 v0, 0x0

    #@4a
    sput-wide v0, Landroid/os/StrictMode;->sLastInstanceCountCheckMillis:J

    #@4c
    .line 1437
    sput-boolean v2, Landroid/os/StrictMode;->sIsIdlerRegistered:Z

    #@4e
    .line 1438
    new-instance v0, Landroid/os/StrictMode$5;

    #@50
    invoke-direct {v0}, Landroid/os/StrictMode$5;-><init>()V

    #@53
    sput-object v0, Landroid/os/StrictMode;->sProcessIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    #@55
    .line 1556
    new-instance v0, Ljava/util/HashMap;

    #@57
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5a
    sput-object v0, Landroid/os/StrictMode;->sLastVmViolationTime:Ljava/util/HashMap;

    #@5c
    .line 1763
    new-instance v0, Landroid/os/StrictMode$6;

    #@5e
    invoke-direct {v0}, Landroid/os/StrictMode$6;-><init>()V

    #@61
    sput-object v0, Landroid/os/StrictMode;->NO_OP_SPAN:Landroid/os/StrictMode$Span;

    #@63
    .line 1787
    new-instance v0, Landroid/os/StrictMode$7;

    #@65
    invoke-direct {v0}, Landroid/os/StrictMode$7;-><init>()V

    #@68
    sput-object v0, Landroid/os/StrictMode;->sThisThreadSpanState:Ljava/lang/ThreadLocal;

    #@6a
    .line 1794
    new-instance v0, Landroid/os/StrictMode$8;

    #@6c
    invoke-direct {v0}, Landroid/os/StrictMode$8;-><init>()V

    #@6f
    sput-object v0, Landroid/os/StrictMode;->sWindowManager:Landroid/util/Singleton;

    #@71
    .line 1890
    new-instance v0, Ljava/util/HashMap;

    #@73
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@76
    sput-object v0, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@78
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 295
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->EMPTY_CLASS_LIMIT_MAP:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-static {p0}, Landroid/os/StrictMode;->parseViolationFromMessage(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(ILandroid/os/StrictMode$ViolationInfo;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 114
    invoke-static {p0, p1}, Landroid/os/StrictMode;->dropboxViolationAsync(ILandroid/os/StrictMode$ViolationInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-static {p0}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Landroid/os/StrictMode$ViolationInfo;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-static {p0}, Landroid/os/StrictMode;->executeDeathPenalty(Landroid/os/StrictMode$ViolationInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$1400()Ljava/util/concurrent/atomic/AtomicInteger;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->sDropboxCallsInFlight:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    return-object v0
.end method

.method static synthetic access$1500()J
    .registers 2

    #@0
    .prologue
    .line 114
    sget-wide v0, Landroid/os/StrictMode;->sLastInstanceCountCheckMillis:J

    #@2
    return-wide v0
.end method

.method static synthetic access$1502(J)J
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    sput-wide p0, Landroid/os/StrictMode;->sLastInstanceCountCheckMillis:J

    #@2
    return-wide p0
.end method

.method static synthetic access$2200()Ljava/lang/ThreadLocal;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->sThisThreadSpanState:Ljava/lang/ThreadLocal;

    #@2
    return-object v0
.end method

.method static synthetic access$400()Z
    .registers 1

    #@0
    .prologue
    .line 114
    invoke-static {}, Landroid/os/StrictMode;->tooManyViolationsThisLoop()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500()Ljava/lang/ThreadLocal;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->violationsBeingTimed:Ljava/lang/ThreadLocal;

    #@2
    return-object v0
.end method

.method static synthetic access$600()Landroid/util/Singleton;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->sWindowManager:Landroid/util/Singleton;

    #@2
    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/ThreadLocal;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->threadHandler:Ljava/lang/ThreadLocal;

    #@2
    return-object v0
.end method

.method static synthetic access$800()Z
    .registers 1

    #@0
    .prologue
    .line 114
    sget-boolean v0, Landroid/os/StrictMode;->LOG_V:Z

    #@2
    return v0
.end method

.method static synthetic access$900()Ljava/lang/ThreadLocal;
    .registers 1

    #@0
    .prologue
    .line 114
    sget-object v0, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@2
    return-object v0
.end method

.method public static allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;
    .registers 4

    #@0
    .prologue
    .line 883
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@3
    move-result v1

    #@4
    .line 884
    .local v1, oldPolicyMask:I
    and-int/lit8 v0, v1, -0x3

    #@6
    .line 885
    .local v0, newPolicyMask:I
    if-eq v0, v1, :cond_b

    #@8
    .line 886
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@b
    .line 888
    :cond_b
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-direct {v2, v1, v3}, Landroid/os/StrictMode$ThreadPolicy;-><init>(ILandroid/os/StrictMode$1;)V

    #@11
    return-object v2
.end method

.method public static allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;
    .registers 4

    #@0
    .prologue
    .line 864
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@3
    move-result v1

    #@4
    .line 865
    .local v1, oldPolicyMask:I
    and-int/lit8 v0, v1, -0x4

    #@6
    .line 866
    .local v0, newPolicyMask:I
    if-eq v0, v1, :cond_b

    #@8
    .line 867
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@b
    .line 869
    :cond_b
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy;

    #@d
    const/4 v3, 0x0

    #@e
    invoke-direct {v2, v1, v3}, Landroid/os/StrictMode$ThreadPolicy;-><init>(ILandroid/os/StrictMode$1;)V

    #@11
    return-object v2
.end method

.method private static amTheSystemServerProcess()Z
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 897
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@4
    move-result v7

    #@5
    const/16 v8, 0x3e8

    #@7
    if-eq v7, v8, :cond_a

    #@9
    .line 911
    .local v0, arr$:[Ljava/lang/StackTraceElement;
    .local v2, i$:I
    .local v3, len$:I
    .local v4, stack:Ljava/lang/Throwable;
    :cond_9
    :goto_9
    return v6

    #@a
    .line 903
    .end local v0           #arr$:[Ljava/lang/StackTraceElement;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v4           #stack:Ljava/lang/Throwable;
    :cond_a
    new-instance v4, Ljava/lang/Throwable;

    #@c
    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    #@f
    .line 904
    .restart local v4       #stack:Ljava/lang/Throwable;
    invoke-virtual {v4}, Ljava/lang/Throwable;->fillInStackTrace()Ljava/lang/Throwable;

    #@12
    .line 905
    invoke-virtual {v4}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@15
    move-result-object v0

    #@16
    .restart local v0       #arr$:[Ljava/lang/StackTraceElement;
    array-length v3, v0

    #@17
    .restart local v3       #len$:I
    const/4 v2, 0x0

    #@18
    .restart local v2       #i$:I
    :goto_18
    if-ge v2, v3, :cond_9

    #@1a
    aget-object v5, v0, v2

    #@1c
    .line 906
    .local v5, ste:Ljava/lang/StackTraceElement;
    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 907
    .local v1, clsName:Ljava/lang/String;
    if-eqz v1, :cond_2c

    #@22
    const-string v7, "com.android.server."

    #@24
    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_2c

    #@2a
    .line 908
    const/4 v6, 0x1

    #@2b
    goto :goto_9

    #@2c
    .line 905
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_18
.end method

.method static clearGatheredViolations()V
    .registers 2

    #@0
    .prologue
    .line 1411
    sget-object v0, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@6
    .line 1412
    return-void
.end method

.method public static conditionallyCheckInstanceCounts()V
    .registers 10

    #@0
    .prologue
    .line 1418
    invoke-static {}, Landroid/os/StrictMode;->getVmPolicy()Landroid/os/StrictMode$VmPolicy;

    #@3
    move-result-object v6

    #@4
    .line 1419
    .local v6, policy:Landroid/os/StrictMode$VmPolicy;
    iget-object v8, v6, Landroid/os/StrictMode$VmPolicy;->classInstanceLimit:Ljava/util/HashMap;

    #@6
    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    #@9
    move-result v8

    #@a
    if-nez v8, :cond_d

    #@c
    .line 1434
    :cond_c
    return-void

    #@d
    .line 1422
    :cond_d
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8}, Ljava/lang/Runtime;->gc()V

    #@14
    .line 1424
    iget-object v8, v6, Landroid/os/StrictMode$VmPolicy;->classInstanceLimit:Ljava/util/HashMap;

    #@16
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@19
    move-result-object v8

    #@1a
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1d
    move-result-object v1

    #@1e
    .local v1, i$:Ljava/util/Iterator;
    :cond_1e
    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@21
    move-result v8

    #@22
    if-eqz v8, :cond_c

    #@24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Ljava/util/Map$Entry;

    #@2a
    .line 1425
    .local v0, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Class;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@2d
    move-result-object v4

    #@2e
    check-cast v4, Ljava/lang/Class;

    #@30
    .line 1426
    .local v4, klass:Ljava/lang/Class;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@33
    move-result-object v8

    #@34
    check-cast v8, Ljava/lang/Integer;

    #@36
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    #@39
    move-result v5

    #@3a
    .line 1427
    .local v5, limit:I
    const/4 v8, 0x0

    #@3b
    invoke-static {v4, v8}, Ldalvik/system/VMDebug;->countInstancesOfClass(Ljava/lang/Class;Z)J

    #@3e
    move-result-wide v2

    #@3f
    .line 1428
    .local v2, instances:J
    int-to-long v8, v5

    #@40
    cmp-long v8, v2, v8

    #@42
    if-lez v8, :cond_1e

    #@44
    .line 1431
    new-instance v7, Landroid/os/StrictMode$InstanceCountViolation;

    #@46
    invoke-direct {v7, v4, v2, v3, v5}, Landroid/os/StrictMode$InstanceCountViolation;-><init>(Ljava/lang/Class;JI)V

    #@49
    .line 1432
    .local v7, tr:Ljava/lang/Throwable;
    invoke-virtual {v7}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    #@4c
    move-result-object v8

    #@4d
    invoke-static {v8, v7}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@50
    goto :goto_1e
.end method

.method public static conditionallyEnableDebugLogging()Z
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 920
    const-string/jumbo v6, "persist.sys.strictmode.visual"

    #@5
    invoke-static {v6, v5}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_25

    #@b
    invoke-static {}, Landroid/os/StrictMode;->amTheSystemServerProcess()Z

    #@e
    move-result v6

    #@f
    if-nez v6, :cond_25

    #@11
    move v0, v4

    #@12
    .line 923
    .local v0, doFlashes:Z
    :goto_12
    const-string/jumbo v6, "persist.sys.strictmode.disable"

    #@15
    invoke-static {v6, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@18
    move-result v2

    #@19
    .line 927
    .local v2, suppress:Z
    if-nez v0, :cond_27

    #@1b
    sget-boolean v6, Landroid/os/StrictMode;->IS_USER_BUILD:Z

    #@1d
    if-nez v6, :cond_21

    #@1f
    if-eqz v2, :cond_27

    #@21
    .line 928
    :cond_21
    invoke-static {v5}, Landroid/os/StrictMode;->setCloseGuardEnabled(Z)V

    #@24
    .line 967
    :goto_24
    return v5

    #@25
    .end local v0           #doFlashes:Z
    .end local v2           #suppress:Z
    :cond_25
    move v0, v5

    #@26
    .line 920
    goto :goto_12

    #@27
    .line 935
    .restart local v0       #doFlashes:Z
    .restart local v2       #suppress:Z
    :cond_27
    sget-boolean v6, Landroid/os/StrictMode;->IS_ENG_BUILD:Z

    #@29
    if-eqz v6, :cond_2c

    #@2b
    .line 936
    const/4 v0, 0x1

    #@2c
    .line 940
    :cond_2c
    const/4 v3, 0x7

    #@2d
    .line 945
    .local v3, threadPolicyMask:I
    sget-boolean v6, Landroid/os/StrictMode;->IS_USER_BUILD:Z

    #@2f
    if-nez v6, :cond_35

    #@31
    if-nez v2, :cond_35

    #@33
    .line 946
    or-int/lit16 v3, v3, 0x80

    #@35
    .line 948
    :cond_35
    if-eqz v0, :cond_39

    #@37
    .line 949
    or-int/lit16 v3, v3, 0x800

    #@39
    .line 952
    :cond_39
    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@3c
    .line 957
    sget-boolean v6, Landroid/os/StrictMode;->IS_USER_BUILD:Z

    #@3e
    if-nez v6, :cond_42

    #@40
    if-eqz v2, :cond_47

    #@42
    .line 958
    :cond_42
    invoke-static {v5}, Landroid/os/StrictMode;->setCloseGuardEnabled(Z)V

    #@45
    :goto_45
    move v5, v4

    #@46
    .line 967
    goto :goto_24

    #@47
    .line 960
    :cond_47
    new-instance v5, Landroid/os/StrictMode$VmPolicy$Builder;

    #@49
    invoke-direct {v5}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    #@4c
    invoke-virtual {v5}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    #@4f
    move-result-object v5

    #@50
    invoke-virtual {v5}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyDropBox()Landroid/os/StrictMode$VmPolicy$Builder;

    #@53
    move-result-object v1

    #@54
    .line 961
    .local v1, policyBuilder:Landroid/os/StrictMode$VmPolicy$Builder;
    sget-boolean v5, Landroid/os/StrictMode;->IS_ENG_BUILD:Z

    #@56
    if-eqz v5, :cond_5b

    #@58
    .line 962
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    #@5b
    .line 964
    :cond_5b
    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v5}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    #@62
    .line 965
    invoke-static {}, Landroid/os/StrictMode;->vmClosableObjectLeaksEnabled()Z

    #@65
    move-result v5

    #@66
    invoke-static {v5}, Landroid/os/StrictMode;->setCloseGuardEnabled(Z)V

    #@69
    goto :goto_45
.end method

.method public static decrementExpectedActivityCount(Ljava/lang/Class;)V
    .registers 12
    .parameter "klass"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 1925
    if-nez p0, :cond_4

    #@3
    .line 1968
    :cond_3
    :goto_3
    return-void

    #@4
    .line 1930
    :cond_4
    const-class v8, Landroid/os/StrictMode;

    #@6
    monitor-enter v8

    #@7
    .line 1931
    :try_start_7
    sget-object v9, Landroid/os/StrictMode;->sVmPolicy:Landroid/os/StrictMode$VmPolicy;

    #@9
    iget v9, v9, Landroid/os/StrictMode$VmPolicy;->mask:I

    #@b
    and-int/lit16 v9, v9, 0x800

    #@d
    if-nez v9, :cond_14

    #@f
    .line 1932
    monitor-exit v8

    #@10
    goto :goto_3

    #@11
    .line 1946
    :catchall_11
    move-exception v7

    #@12
    monitor-exit v8
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_11

    #@13
    throw v7

    #@14
    .line 1935
    :cond_14
    :try_start_14
    sget-object v9, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@16
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v1

    #@1a
    check-cast v1, Ljava/lang/Integer;

    #@1c
    .line 1936
    .local v1, expected:Ljava/lang/Integer;
    if-eqz v1, :cond_24

    #@1e
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@21
    move-result v9

    #@22
    if-nez v9, :cond_52

    #@24
    :cond_24
    move v5, v7

    #@25
    .line 1937
    .local v5, newExpected:I
    :goto_25
    if-nez v5, :cond_59

    #@27
    .line 1938
    sget-object v9, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@29
    invoke-virtual {v9, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 1945
    :goto_2c
    add-int/lit8 v4, v5, 0x1

    #@2e
    .line 1946
    .local v4, limit:I
    monitor-exit v8
    :try_end_2f
    .catchall {:try_start_14 .. :try_end_2f} :catchall_11

    #@2f
    .line 1949
    invoke-static {p0}, Landroid/os/StrictMode$InstanceTracker;->getInstanceCount(Ljava/lang/Class;)I

    #@32
    move-result v0

    #@33
    .line 1950
    .local v0, actual:I
    if-le v0, v4, :cond_3

    #@35
    .line 1961
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8}, Ljava/lang/Runtime;->gc()V

    #@3c
    .line 1963
    invoke-static {p0, v7}, Ldalvik/system/VMDebug;->countInstancesOfClass(Ljava/lang/Class;Z)J

    #@3f
    move-result-wide v2

    #@40
    .line 1964
    .local v2, instances:J
    int-to-long v7, v4

    #@41
    cmp-long v7, v2, v7

    #@43
    if-lez v7, :cond_3

    #@45
    .line 1965
    new-instance v6, Landroid/os/StrictMode$InstanceCountViolation;

    #@47
    invoke-direct {v6, p0, v2, v3, v4}, Landroid/os/StrictMode$InstanceCountViolation;-><init>(Ljava/lang/Class;JI)V

    #@4a
    .line 1966
    .local v6, tr:Ljava/lang/Throwable;
    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    #@4d
    move-result-object v7

    #@4e
    invoke-static {v7, v6}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@51
    goto :goto_3

    #@52
    .line 1936
    .end local v0           #actual:I
    .end local v2           #instances:J
    .end local v4           #limit:I
    .end local v5           #newExpected:I
    .end local v6           #tr:Ljava/lang/Throwable;
    :cond_52
    :try_start_52
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@55
    move-result v9

    #@56
    add-int/lit8 v5, v9, -0x1

    #@58
    goto :goto_25

    #@59
    .line 1940
    .restart local v5       #newExpected:I
    :cond_59
    sget-object v9, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@5b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v10

    #@5f
    invoke-virtual {v9, p0, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_62
    .catchall {:try_start_52 .. :try_end_62} :catchall_11

    #@62
    goto :goto_2c
.end method

.method private static dropboxViolationAsync(ILandroid/os/StrictMode$ViolationInfo;)V
    .registers 6
    .parameter "violationMaskSubset"
    .parameter "info"

    #@0
    .prologue
    .line 1360
    sget-object v1, Landroid/os/StrictMode;->sDropboxCallsInFlight:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    #@5
    move-result v0

    #@6
    .line 1361
    .local v0, outstanding:I
    const/16 v1, 0x14

    #@8
    if-le v0, v1, :cond_10

    #@a
    .line 1364
    sget-object v1, Landroid/os/StrictMode;->sDropboxCallsInFlight:Ljava/util/concurrent/atomic/AtomicInteger;

    #@c
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    #@f
    .line 1390
    :goto_f
    return-void

    #@10
    .line 1368
    :cond_10
    sget-boolean v1, Landroid/os/StrictMode;->LOG_V:Z

    #@12
    if-eqz v1, :cond_2c

    #@14
    const-string v1, "StrictMode"

    #@16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "Dropboxing async; in-flight="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1370
    :cond_2c
    new-instance v1, Landroid/os/StrictMode$4;

    #@2e
    const-string v2, "callActivityManagerForStrictModeDropbox"

    #@30
    invoke-direct {v1, v2, p0, p1}, Landroid/os/StrictMode$4;-><init>(Ljava/lang/String;ILandroid/os/StrictMode$ViolationInfo;)V

    #@33
    invoke-virtual {v1}, Landroid/os/StrictMode$4;->start()V

    #@36
    goto :goto_f
.end method

.method public static enableDeathOnNetwork()V
    .registers 3

    #@0
    .prologue
    .line 977
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@3
    move-result v1

    #@4
    .line 978
    .local v1, oldPolicy:I
    or-int/lit8 v2, v1, 0x4

    #@6
    or-int/lit16 v0, v2, 0x200

    #@8
    .line 979
    .local v0, newPolicy:I
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@b
    .line 980
    return-void
.end method

.method public static enableDefaults()V
    .registers 1

    #@0
    .prologue
    .line 1496
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@2
    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    #@5
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    #@10
    move-result-object v0

    #@11
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    #@14
    .line 1500
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    #@16
    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    #@19
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    #@24
    move-result-object v0

    #@25
    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    #@28
    .line 1504
    return-void
.end method

.method public static enterCriticalSpan(Ljava/lang/String;)Landroid/os/StrictMode$Span;
    .registers 7
    .parameter "name"

    #@0
    .prologue
    .line 1816
    sget-boolean v3, Landroid/os/StrictMode;->IS_USER_BUILD:Z

    #@2
    if-eqz v3, :cond_7

    #@4
    .line 1817
    sget-object v0, Landroid/os/StrictMode;->NO_OP_SPAN:Landroid/os/StrictMode$Span;

    #@6
    .line 1844
    :goto_6
    return-object v0

    #@7
    .line 1819
    :cond_7
    if-eqz p0, :cond_f

    #@9
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_18

    #@f
    .line 1820
    :cond_f
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@11
    const-string/jumbo v4, "name must be non-null and non-empty"

    #@14
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3

    #@18
    .line 1822
    :cond_18
    sget-object v3, Landroid/os/StrictMode;->sThisThreadSpanState:Ljava/lang/ThreadLocal;

    #@1a
    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@1d
    move-result-object v2

    #@1e
    check-cast v2, Landroid/os/StrictMode$ThreadSpanState;

    #@20
    .line 1823
    .local v2, state:Landroid/os/StrictMode$ThreadSpanState;
    const/4 v0, 0x0

    #@21
    .line 1824
    .local v0, span:Landroid/os/StrictMode$Span;
    monitor-enter v2

    #@22
    .line 1825
    :try_start_22
    iget-object v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mFreeListHead:Landroid/os/StrictMode$Span;

    #@24
    if-eqz v3, :cond_89

    #@26
    .line 1826
    iget-object v0, v2, Landroid/os/StrictMode$ThreadSpanState;->mFreeListHead:Landroid/os/StrictMode$Span;

    #@28
    .line 1827
    invoke-static {v0}, Landroid/os/StrictMode$Span;->access$1800(Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@2b
    move-result-object v3

    #@2c
    iput-object v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mFreeListHead:Landroid/os/StrictMode$Span;

    #@2e
    .line 1828
    iget v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mFreeListSize:I

    #@30
    add-int/lit8 v3, v3, -0x1

    #@32
    iput v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mFreeListSize:I

    #@34
    .line 1833
    :goto_34
    invoke-static {v0, p0}, Landroid/os/StrictMode$Span;->access$1902(Landroid/os/StrictMode$Span;Ljava/lang/String;)Ljava/lang/String;

    #@37
    .line 1834
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3a
    move-result-wide v3

    #@3b
    invoke-static {v0, v3, v4}, Landroid/os/StrictMode$Span;->access$2002(Landroid/os/StrictMode$Span;J)J

    #@3e
    .line 1835
    iget-object v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mActiveHead:Landroid/os/StrictMode$Span;

    #@40
    invoke-static {v0, v3}, Landroid/os/StrictMode$Span;->access$1802(Landroid/os/StrictMode$Span;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@43
    .line 1836
    const/4 v3, 0x0

    #@44
    invoke-static {v0, v3}, Landroid/os/StrictMode$Span;->access$2102(Landroid/os/StrictMode$Span;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@47
    .line 1837
    iput-object v0, v2, Landroid/os/StrictMode$ThreadSpanState;->mActiveHead:Landroid/os/StrictMode$Span;

    #@49
    .line 1838
    iget v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@4b
    add-int/lit8 v3, v3, 0x1

    #@4d
    iput v3, v2, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@4f
    .line 1839
    invoke-static {v0}, Landroid/os/StrictMode$Span;->access$1800(Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@52
    move-result-object v3

    #@53
    if-eqz v3, :cond_5c

    #@55
    .line 1840
    invoke-static {v0}, Landroid/os/StrictMode$Span;->access$1800(Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@58
    move-result-object v3

    #@59
    invoke-static {v3, v0}, Landroid/os/StrictMode$Span;->access$2102(Landroid/os/StrictMode$Span;Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@5c
    .line 1842
    :cond_5c
    sget-boolean v3, Landroid/os/StrictMode;->LOG_V:Z

    #@5e
    if-eqz v3, :cond_84

    #@60
    const-string v3, "StrictMode"

    #@62
    new-instance v4, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v5, "Span enter="

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    const-string v5, "; size="

    #@73
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    iget v5, v2, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v4

    #@81
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 1843
    :cond_84
    monitor-exit v2

    #@85
    goto :goto_6

    #@86
    :catchall_86
    move-exception v3

    #@87
    monitor-exit v2
    :try_end_88
    .catchall {:try_start_22 .. :try_end_88} :catchall_86

    #@88
    throw v3

    #@89
    .line 1831
    :cond_89
    :try_start_89
    new-instance v1, Landroid/os/StrictMode$Span;

    #@8b
    invoke-direct {v1, v2}, Landroid/os/StrictMode$Span;-><init>(Landroid/os/StrictMode$ThreadSpanState;)V
    :try_end_8e
    .catchall {:try_start_89 .. :try_end_8e} :catchall_86

    #@8e
    .end local v0           #span:Landroid/os/StrictMode$Span;
    .local v1, span:Landroid/os/StrictMode$Span;
    move-object v0, v1

    #@8f
    .end local v1           #span:Landroid/os/StrictMode$Span;
    .restart local v0       #span:Landroid/os/StrictMode$Span;
    goto :goto_34
.end method

.method private static executeDeathPenalty(Landroid/os/StrictMode$ViolationInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 1346
    iget-object v1, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@2
    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    #@4
    invoke-static {v1}, Landroid/os/StrictMode;->parseViolationFromMessage(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    .line 1347
    .local v0, violationBit:I
    new-instance v1, Landroid/os/StrictMode$StrictModeViolation;

    #@a
    iget v2, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@c
    const/4 v3, 0x0

    #@d
    invoke-direct {v1, v2, v0, v3}, Landroid/os/StrictMode$StrictModeViolation;-><init>(IILjava/lang/String;)V

    #@10
    throw v1
.end method

.method public static getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;
    .registers 3

    #@0
    .prologue
    .line 850
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy;

    #@2
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@5
    move-result v1

    #@6
    const/4 v2, 0x0

    #@7
    invoke-direct {v0, v1, v2}, Landroid/os/StrictMode$ThreadPolicy;-><init>(ILandroid/os/StrictMode$1;)V

    #@a
    return-object v0
.end method

.method public static getThreadPolicyMask()I
    .registers 1

    #@0
    .prologue
    .line 839
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@3
    move-result-object v0

    #@4
    invoke-interface {v0}, Ldalvik/system/BlockGuard$Policy;->getPolicyMask()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public static getVmPolicy()Landroid/os/StrictMode$VmPolicy;
    .registers 2

    #@0
    .prologue
    .line 1482
    const-class v1, Landroid/os/StrictMode;

    #@2
    monitor-enter v1

    #@3
    .line 1483
    :try_start_3
    sget-object v0, Landroid/os/StrictMode;->sVmPolicy:Landroid/os/StrictMode$VmPolicy;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 1484
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method static hasGatheredViolations()Z
    .registers 1

    #@0
    .prologue
    .line 1402
    sget-object v0, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public static incrementExpectedActivityCount(Ljava/lang/Class;)V
    .registers 5
    .parameter "klass"

    #@0
    .prologue
    .line 1906
    if-nez p0, :cond_3

    #@2
    .line 1919
    :goto_2
    return-void

    #@3
    .line 1910
    :cond_3
    const-class v3, Landroid/os/StrictMode;

    #@5
    monitor-enter v3

    #@6
    .line 1911
    :try_start_6
    sget-object v2, Landroid/os/StrictMode;->sVmPolicy:Landroid/os/StrictMode$VmPolicy;

    #@8
    iget v2, v2, Landroid/os/StrictMode$VmPolicy;->mask:I

    #@a
    and-int/lit16 v2, v2, 0x800

    #@c
    if-nez v2, :cond_13

    #@e
    .line 1912
    monitor-exit v3

    #@f
    goto :goto_2

    #@10
    .line 1918
    :catchall_10
    move-exception v2

    #@11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_10

    #@12
    throw v2

    #@13
    .line 1915
    :cond_13
    :try_start_13
    sget-object v2, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@15
    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Ljava/lang/Integer;

    #@1b
    .line 1916
    .local v0, expected:Ljava/lang/Integer;
    if-nez v0, :cond_29

    #@1d
    const/4 v2, 0x1

    #@1e
    :goto_1e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v1

    #@22
    .line 1917
    .local v1, newExpected:Ljava/lang/Integer;
    sget-object v2, Landroid/os/StrictMode;->sExpectedActivityInstanceCount:Ljava/util/HashMap;

    #@24
    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 1918
    monitor-exit v3

    #@28
    goto :goto_2

    #@29
    .line 1916
    .end local v1           #newExpected:Ljava/lang/Integer;
    :cond_29
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_2c
    .catchall {:try_start_13 .. :try_end_2c} :catchall_10

    #@2c
    move-result v2

    #@2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_1e
.end method

.method public static noteDiskRead()V
    .registers 2

    #@0
    .prologue
    .line 1869
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@3
    move-result-object v0

    #@4
    .line 1870
    .local v0, policy:Ldalvik/system/BlockGuard$Policy;
    instance-of v1, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1875
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    :goto_8
    return-void

    #@9
    .line 1874
    .restart local v0       #policy:Ldalvik/system/BlockGuard$Policy;
    :cond_9
    check-cast v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@b
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    invoke-virtual {v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->onReadFromDisk()V

    #@e
    goto :goto_8
.end method

.method public static noteDiskWrite()V
    .registers 2

    #@0
    .prologue
    .line 1881
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@3
    move-result-object v0

    #@4
    .line 1882
    .local v0, policy:Ldalvik/system/BlockGuard$Policy;
    instance-of v1, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1887
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    :goto_8
    return-void

    #@9
    .line 1886
    .restart local v0       #policy:Ldalvik/system/BlockGuard$Policy;
    :cond_9
    check-cast v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@b
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    invoke-virtual {v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->onWriteToDisk()V

    #@e
    goto :goto_8
.end method

.method public static noteSlowCall(Ljava/lang/String;)V
    .registers 3
    .parameter "name"

    #@0
    .prologue
    .line 1857
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@3
    move-result-object v0

    #@4
    .line 1858
    .local v0, policy:Ldalvik/system/BlockGuard$Policy;
    instance-of v1, v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1863
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    :goto_8
    return-void

    #@9
    .line 1862
    .restart local v0       #policy:Ldalvik/system/BlockGuard$Policy;
    :cond_9
    check-cast v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@b
    .end local v0           #policy:Ldalvik/system/BlockGuard$Policy;
    invoke-virtual {v0, p0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->onCustomSlowCall(Ljava/lang/String;)V

    #@e
    goto :goto_8
.end method

.method private static onBinderStrictModePolicyChange(I)V
    .registers 1
    .parameter "newPolicy"

    #@0
    .prologue
    .line 1686
    invoke-static {p0}, Landroid/os/StrictMode;->setBlockGuardPolicy(I)V

    #@3
    .line 1687
    return-void
.end method

.method public static onIntentReceiverLeaked(Ljava/lang/Throwable;)V
    .registers 2
    .parameter "originStack"

    #@0
    .prologue
    .line 1545
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4
    .line 1546
    return-void
.end method

.method public static onServiceConnectionLeaked(Ljava/lang/Throwable;)V
    .registers 2
    .parameter "originStack"

    #@0
    .prologue
    .line 1552
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4
    .line 1553
    return-void
.end method

.method public static onSqliteObjectLeaked(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 2
    .parameter "message"
    .parameter "originStack"

    #@0
    .prologue
    .line 1531
    invoke-static {p0, p1}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3
    .line 1532
    return-void
.end method

.method public static onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 23
    .parameter "message"
    .parameter "originStack"

    #@0
    .prologue
    .line 1562
    sget v17, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@2
    move/from16 v0, v17

    #@4
    and-int/lit16 v0, v0, 0x80

    #@6
    move/from16 v17, v0

    #@8
    if-eqz v17, :cond_a5

    #@a
    const/4 v11, 0x1

    #@b
    .line 1563
    .local v11, penaltyDropbox:Z
    :goto_b
    sget v17, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@d
    and-int/lit8 v17, v17, 0x40

    #@f
    if-eqz v17, :cond_a8

    #@11
    const/4 v10, 0x1

    #@12
    .line 1564
    .local v10, penaltyDeath:Z
    :goto_12
    sget v17, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@14
    and-int/lit8 v17, v17, 0x10

    #@16
    if-eqz v17, :cond_ab

    #@18
    const/4 v12, 0x1

    #@19
    .line 1565
    .local v12, penaltyLog:Z
    :goto_19
    new-instance v5, Landroid/os/StrictMode$ViolationInfo;

    #@1b
    sget v17, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@1d
    move-object/from16 v0, p1

    #@1f
    move/from16 v1, v17

    #@21
    invoke-direct {v5, v0, v1}, Landroid/os/StrictMode$ViolationInfo;-><init>(Ljava/lang/Throwable;I)V

    #@24
    .line 1568
    .local v5, info:Landroid/os/StrictMode$ViolationInfo;
    const/16 v17, 0x0

    #@26
    move/from16 v0, v17

    #@28
    iput v0, v5, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@2a
    .line 1569
    const/16 v17, 0x0

    #@2c
    move-object/from16 v0, v17

    #@2e
    iput-object v0, v5, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@30
    .line 1570
    const/16 v17, 0x0

    #@32
    move-object/from16 v0, v17

    #@34
    iput-object v0, v5, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@36
    .line 1572
    invoke-virtual {v5}, Landroid/os/StrictMode$ViolationInfo;->hashCode()I

    #@39
    move-result v17

    #@3a
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3d
    move-result-object v4

    #@3e
    .line 1573
    .local v4, fingerprint:Ljava/lang/Integer;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@41
    move-result-wide v8

    #@42
    .line 1574
    .local v8, now:J
    const-wide/16 v6, 0x0

    #@44
    .line 1575
    .local v6, lastViolationTime:J
    const-wide v14, 0x7fffffffffffffffL

    #@49
    .line 1576
    .local v14, timeSinceLastViolationMillis:J
    sget-object v18, Landroid/os/StrictMode;->sLastVmViolationTime:Ljava/util/HashMap;

    #@4b
    monitor-enter v18

    #@4c
    .line 1577
    :try_start_4c
    sget-object v17, Landroid/os/StrictMode;->sLastVmViolationTime:Ljava/util/HashMap;

    #@4e
    move-object/from16 v0, v17

    #@50
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@53
    move-result v17

    #@54
    if-eqz v17, :cond_66

    #@56
    .line 1578
    sget-object v17, Landroid/os/StrictMode;->sLastVmViolationTime:Ljava/util/HashMap;

    #@58
    move-object/from16 v0, v17

    #@5a
    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5d
    move-result-object v17

    #@5e
    check-cast v17, Ljava/lang/Long;

    #@60
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    #@63
    move-result-wide v6

    #@64
    .line 1579
    sub-long v14, v8, v6

    #@66
    .line 1581
    :cond_66
    const-wide/16 v19, 0x3e8

    #@68
    cmp-long v17, v14, v19

    #@6a
    if-lez v17, :cond_79

    #@6c
    .line 1582
    sget-object v17, Landroid/os/StrictMode;->sLastVmViolationTime:Ljava/util/HashMap;

    #@6e
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@71
    move-result-object v19

    #@72
    move-object/from16 v0, v17

    #@74
    move-object/from16 v1, v19

    #@76
    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@79
    .line 1584
    :cond_79
    monitor-exit v18
    :try_end_7a
    .catchall {:try_start_4c .. :try_end_7a} :catchall_ae

    #@7a
    .line 1586
    if-eqz v12, :cond_8d

    #@7c
    const-wide/16 v17, 0x3e8

    #@7e
    cmp-long v17, v14, v17

    #@80
    if-lez v17, :cond_8d

    #@82
    .line 1587
    const-string v17, "StrictMode"

    #@84
    move-object/from16 v0, v17

    #@86
    move-object/from16 v1, p0

    #@88
    move-object/from16 v2, p1

    #@8a
    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8d
    .line 1590
    :cond_8d
    sget v17, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@8f
    move/from16 v0, v17

    #@91
    and-int/lit16 v0, v0, 0x3e00

    #@93
    move/from16 v17, v0

    #@95
    move/from16 v0, v17

    #@97
    or-int/lit16 v0, v0, 0x80

    #@99
    move/from16 v16, v0

    #@9b
    .line 1592
    .local v16, violationMaskSubset:I
    if-eqz v11, :cond_b1

    #@9d
    if-nez v10, :cond_b1

    #@9f
    .line 1596
    move/from16 v0, v16

    #@a1
    invoke-static {v0, v5}, Landroid/os/StrictMode;->dropboxViolationAsync(ILandroid/os/StrictMode$ViolationInfo;)V

    #@a4
    .line 1630
    :cond_a4
    :goto_a4
    return-void

    #@a5
    .line 1562
    .end local v4           #fingerprint:Ljava/lang/Integer;
    .end local v5           #info:Landroid/os/StrictMode$ViolationInfo;
    .end local v6           #lastViolationTime:J
    .end local v8           #now:J
    .end local v10           #penaltyDeath:Z
    .end local v11           #penaltyDropbox:Z
    .end local v12           #penaltyLog:Z
    .end local v14           #timeSinceLastViolationMillis:J
    .end local v16           #violationMaskSubset:I
    :cond_a5
    const/4 v11, 0x0

    #@a6
    goto/16 :goto_b

    #@a8
    .line 1563
    .restart local v11       #penaltyDropbox:Z
    :cond_a8
    const/4 v10, 0x0

    #@a9
    goto/16 :goto_12

    #@ab
    .line 1564
    .restart local v10       #penaltyDeath:Z
    :cond_ab
    const/4 v12, 0x0

    #@ac
    goto/16 :goto_19

    #@ae
    .line 1584
    .restart local v4       #fingerprint:Ljava/lang/Integer;
    .restart local v5       #info:Landroid/os/StrictMode$ViolationInfo;
    .restart local v6       #lastViolationTime:J
    .restart local v8       #now:J
    .restart local v12       #penaltyLog:Z
    .restart local v14       #timeSinceLastViolationMillis:J
    :catchall_ae
    move-exception v17

    #@af
    :try_start_af
    monitor-exit v18
    :try_end_b0
    .catchall {:try_start_af .. :try_end_b0} :catchall_ae

    #@b0
    throw v17

    #@b1
    .line 1600
    .restart local v16       #violationMaskSubset:I
    :cond_b1
    if-eqz v11, :cond_d6

    #@b3
    const-wide/16 v17, 0x0

    #@b5
    cmp-long v17, v6, v17

    #@b7
    if-nez v17, :cond_d6

    #@b9
    .line 1605
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@bc
    move-result v13

    #@bd
    .line 1611
    .local v13, savedPolicyMask:I
    const/16 v17, 0x0

    #@bf
    :try_start_bf
    invoke-static/range {v17 .. v17}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@c2
    .line 1613
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@c5
    move-result-object v17

    #@c6
    invoke-static {}, Lcom/android/internal/os/RuntimeInit;->getApplicationObject()Landroid/os/IBinder;

    #@c9
    move-result-object v18

    #@ca
    move-object/from16 v0, v17

    #@cc
    move-object/from16 v1, v18

    #@ce
    move/from16 v2, v16

    #@d0
    invoke-interface {v0, v1, v2, v5}, Landroid/app/IActivityManager;->handleApplicationStrictModeViolation(Landroid/os/IBinder;ILandroid/os/StrictMode$ViolationInfo;)V
    :try_end_d3
    .catchall {:try_start_bf .. :try_end_d3} :catchall_f9
    .catch Landroid/os/RemoteException; {:try_start_bf .. :try_end_d3} :catch_ec

    #@d3
    .line 1621
    :goto_d3
    invoke-static {v13}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@d6
    .line 1625
    .end local v13           #savedPolicyMask:I
    :cond_d6
    if-eqz v10, :cond_a4

    #@d8
    .line 1626
    sget-object v17, Ljava/lang/System;->err:Ljava/io/PrintStream;

    #@da
    const-string v18, "StrictMode VmPolicy violation with POLICY_DEATH; shutting down."

    #@dc
    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@df
    .line 1627
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@e2
    move-result v17

    #@e3
    invoke-static/range {v17 .. v17}, Landroid/os/Process;->killProcess(I)V

    #@e6
    .line 1628
    const/16 v17, 0xa

    #@e8
    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    #@eb
    goto :goto_a4

    #@ec
    .line 1617
    .restart local v13       #savedPolicyMask:I
    :catch_ec
    move-exception v3

    #@ed
    .line 1618
    .local v3, e:Landroid/os/RemoteException;
    :try_start_ed
    const-string v17, "StrictMode"

    #@ef
    const-string v18, "RemoteException trying to handle StrictMode violation"

    #@f1
    move-object/from16 v0, v17

    #@f3
    move-object/from16 v1, v18

    #@f5
    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f8
    .catchall {:try_start_ed .. :try_end_f8} :catchall_f9

    #@f8
    goto :goto_d3

    #@f9
    .line 1621
    .end local v3           #e:Landroid/os/RemoteException;
    :catchall_f9
    move-exception v17

    #@fa
    invoke-static {v13}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@fd
    throw v17
.end method

.method public static onWebViewMethodCalledOnWrongThread(Ljava/lang/Throwable;)V
    .registers 2
    .parameter "originStack"

    #@0
    .prologue
    .line 1538
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0}, Landroid/os/StrictMode;->onVmPolicyViolation(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4
    .line 1539
    return-void
.end method

.method private static parsePolicyFromMessage(Ljava/lang/String;)I
    .registers 6
    .parameter "message"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 996
    if-eqz p0, :cond_c

    #@3
    const-string/jumbo v4, "policy="

    #@6
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_d

    #@c
    .line 1007
    :cond_c
    :goto_c
    return v3

    #@d
    .line 999
    :cond_d
    const/16 v4, 0x20

    #@f
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    #@12
    move-result v2

    #@13
    .line 1000
    .local v2, spaceIndex:I
    const/4 v4, -0x1

    #@14
    if-eq v2, v4, :cond_c

    #@16
    .line 1003
    const/4 v4, 0x7

    #@17
    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    .line 1005
    .local v1, policyString:Ljava/lang/String;
    :try_start_1b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_22
    .catch Ljava/lang/NumberFormatException; {:try_start_1b .. :try_end_22} :catch_24

    #@22
    move-result v3

    #@23
    goto :goto_c

    #@24
    .line 1006
    :catch_24
    move-exception v0

    #@25
    .line 1007
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_c
.end method

.method private static parseViolationFromMessage(Ljava/lang/String;)I
    .registers 9
    .parameter "message"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1015
    if-nez p0, :cond_5

    #@4
    .line 1031
    :cond_4
    :goto_4
    return v5

    #@5
    .line 1018
    :cond_5
    const-string/jumbo v6, "violation="

    #@8
    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@b
    move-result v3

    #@c
    .line 1019
    .local v3, violationIndex:I
    if-eq v3, v7, :cond_4

    #@e
    .line 1022
    const-string/jumbo v6, "violation="

    #@11
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    #@14
    move-result v6

    #@15
    add-int v2, v3, v6

    #@17
    .line 1023
    .local v2, numberStartIndex:I
    const/16 v6, 0x20

    #@19
    invoke-virtual {p0, v6, v2}, Ljava/lang/String;->indexOf(II)I

    #@1c
    move-result v1

    #@1d
    .line 1024
    .local v1, numberEndIndex:I
    if-ne v1, v7, :cond_23

    #@1f
    .line 1025
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@22
    move-result v1

    #@23
    .line 1027
    :cond_23
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    .line 1029
    .local v4, violationString:Ljava/lang/String;
    :try_start_27
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_2e
    .catch Ljava/lang/NumberFormatException; {:try_start_27 .. :try_end_2e} :catch_30

    #@2e
    move-result v5

    #@2f
    goto :goto_4

    #@30
    .line 1030
    :catch_30
    move-exception v0

    #@31
    .line 1031
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_4
.end method

.method static readAndHandleBinderCallViolations(Landroid/os/Parcel;)V
    .registers 14
    .parameter "p"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 1658
    new-instance v7, Ljava/io/StringWriter;

    #@4
    invoke-direct {v7}, Ljava/io/StringWriter;-><init>()V

    #@7
    .line 1659
    .local v7, sw:Ljava/io/StringWriter;
    new-instance v10, Landroid/os/StrictMode$LogStackTrace;

    #@9
    const/4 v11, 0x0

    #@a
    invoke-direct {v10, v11}, Landroid/os/StrictMode$LogStackTrace;-><init>(Landroid/os/StrictMode$1;)V

    #@d
    new-instance v11, Ljava/io/PrintWriter;

    #@f
    invoke-direct {v11, v7}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    #@12
    invoke-virtual {v10, v11}, Landroid/os/StrictMode$LogStackTrace;->printStackTrace(Ljava/io/PrintWriter;)V

    #@15
    .line 1660
    invoke-virtual {v7}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    .line 1662
    .local v4, ourStack:Ljava/lang/String;
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    #@1c
    move-result v6

    #@1d
    .line 1663
    .local v6, policyMask:I
    and-int/lit16 v10, v6, 0x100

    #@1f
    if-eqz v10, :cond_7b

    #@21
    move v0, v8

    #@22
    .line 1665
    .local v0, currentlyGathering:Z
    :goto_22
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v3

    #@26
    .line 1666
    .local v3, numViolations:I
    const/4 v1, 0x0

    #@27
    .local v1, i:I
    :goto_27
    if-ge v1, v3, :cond_7f

    #@29
    .line 1667
    sget-boolean v10, Landroid/os/StrictMode;->LOG_V:Z

    #@2b
    if-eqz v10, :cond_46

    #@2d
    const-string v10, "StrictMode"

    #@2f
    new-instance v11, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string/jumbo v12, "strict mode violation stacks read from binder call.  i="

    #@37
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v11

    #@3b
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v11

    #@3f
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v11

    #@43
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 1668
    :cond_46
    new-instance v2, Landroid/os/StrictMode$ViolationInfo;

    #@48
    if-nez v0, :cond_7d

    #@4a
    move v10, v8

    #@4b
    :goto_4b
    invoke-direct {v2, p0, v10}, Landroid/os/StrictMode$ViolationInfo;-><init>(Landroid/os/Parcel;Z)V

    #@4e
    .line 1669
    .local v2, info:Landroid/os/StrictMode$ViolationInfo;
    new-instance v10, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    iget-object v11, v2, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@55
    iget-object v12, v11, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@57
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v10

    #@5b
    const-string v12, "# via Binder call with stack:\n"

    #@5d
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v10

    #@61
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v10

    #@65
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v10

    #@69
    iput-object v10, v11, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@6b
    .line 1670
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@6e
    move-result-object v5

    #@6f
    .line 1671
    .local v5, policy:Ldalvik/system/BlockGuard$Policy;
    instance-of v10, v5, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@71
    if-eqz v10, :cond_78

    #@73
    .line 1672
    check-cast v5, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@75
    .end local v5           #policy:Ldalvik/system/BlockGuard$Policy;
    invoke-virtual {v5, v2}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V

    #@78
    .line 1666
    :cond_78
    add-int/lit8 v1, v1, 0x1

    #@7a
    goto :goto_27

    #@7b
    .end local v0           #currentlyGathering:Z
    .end local v1           #i:I
    .end local v2           #info:Landroid/os/StrictMode$ViolationInfo;
    .end local v3           #numViolations:I
    :cond_7b
    move v0, v9

    #@7c
    .line 1663
    goto :goto_22

    #@7d
    .restart local v0       #currentlyGathering:Z
    .restart local v1       #i:I
    .restart local v3       #numViolations:I
    :cond_7d
    move v10, v9

    #@7e
    .line 1668
    goto :goto_4b

    #@7f
    .line 1675
    :cond_7f
    return-void
.end method

.method private static setBlockGuardPolicy(I)V
    .registers 4
    .parameter "policyMask"

    #@0
    .prologue
    .line 765
    if-nez p0, :cond_8

    #@2
    .line 766
    sget-object v2, Ldalvik/system/BlockGuard;->LAX_POLICY:Ldalvik/system/BlockGuard$Policy;

    #@4
    invoke-static {v2}, Ldalvik/system/BlockGuard;->setThreadPolicy(Ldalvik/system/BlockGuard$Policy;)V

    #@7
    .line 776
    :goto_7
    return-void

    #@8
    .line 769
    :cond_8
    invoke-static {}, Ldalvik/system/BlockGuard;->getThreadPolicy()Ldalvik/system/BlockGuard$Policy;

    #@b
    move-result-object v1

    #@c
    .line 770
    .local v1, policy:Ldalvik/system/BlockGuard$Policy;
    instance-of v2, v1, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@e
    if-nez v2, :cond_19

    #@10
    .line 771
    new-instance v2, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@12
    invoke-direct {v2, p0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;-><init>(I)V

    #@15
    invoke-static {v2}, Ldalvik/system/BlockGuard;->setThreadPolicy(Ldalvik/system/BlockGuard$Policy;)V

    #@18
    goto :goto_7

    #@19
    :cond_19
    move-object v0, v1

    #@1a
    .line 773
    check-cast v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@1c
    .line 774
    .local v0, androidPolicy:Landroid/os/StrictMode$AndroidBlockGuardPolicy;
    invoke-virtual {v0, p0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->setPolicyMask(I)V

    #@1f
    goto :goto_7
.end method

.method private static setCloseGuardEnabled(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 780
    invoke-static {}, Ldalvik/system/CloseGuard;->getReporter()Ldalvik/system/CloseGuard$Reporter;

    #@3
    move-result-object v0

    #@4
    instance-of v0, v0, Landroid/os/StrictMode$AndroidCloseGuardReporter;

    #@6
    if-nez v0, :cond_11

    #@8
    .line 781
    new-instance v0, Landroid/os/StrictMode$AndroidCloseGuardReporter;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {v0, v1}, Landroid/os/StrictMode$AndroidCloseGuardReporter;-><init>(Landroid/os/StrictMode$1;)V

    #@e
    invoke-static {v0}, Ldalvik/system/CloseGuard;->setReporter(Ldalvik/system/CloseGuard$Reporter;)V

    #@11
    .line 783
    :cond_11
    invoke-static {p0}, Ldalvik/system/CloseGuard;->setEnabled(Z)V

    #@14
    .line 784
    return-void
.end method

.method public static setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V
    .registers 2
    .parameter "policy"

    #@0
    .prologue
    .line 748
    iget v0, p0, Landroid/os/StrictMode$ThreadPolicy;->mask:I

    #@2
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicyMask(I)V

    #@5
    .line 749
    return-void
.end method

.method private static setThreadPolicyMask(I)V
    .registers 1
    .parameter "policyMask"

    #@0
    .prologue
    .line 757
    invoke-static {p0}, Landroid/os/StrictMode;->setBlockGuardPolicy(I)V

    #@3
    .line 760
    invoke-static {p0}, Landroid/os/Binder;->setThreadStrictModePolicy(I)V

    #@6
    .line 761
    return-void
.end method

.method public static setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V
    .registers 5
    .parameter "policy"

    #@0
    .prologue
    .line 1458
    const-class v3, Landroid/os/StrictMode;

    #@2
    monitor-enter v3

    #@3
    .line 1459
    :try_start_3
    sput-object p0, Landroid/os/StrictMode;->sVmPolicy:Landroid/os/StrictMode$VmPolicy;

    #@5
    .line 1460
    iget v2, p0, Landroid/os/StrictMode$VmPolicy;->mask:I

    #@7
    sput v2, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@9
    .line 1461
    invoke-static {}, Landroid/os/StrictMode;->vmClosableObjectLeaksEnabled()Z

    #@c
    move-result v2

    #@d
    invoke-static {v2}, Landroid/os/StrictMode;->setCloseGuardEnabled(Z)V

    #@10
    .line 1463
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@13
    move-result-object v0

    #@14
    .line 1464
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_2e

    #@16
    .line 1465
    iget-object v1, v0, Landroid/os/Looper;->mQueue:Landroid/os/MessageQueue;

    #@18
    .line 1466
    .local v1, mq:Landroid/os/MessageQueue;
    iget-object v2, p0, Landroid/os/StrictMode$VmPolicy;->classInstanceLimit:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_26

    #@20
    sget v2, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@22
    and-int/lit16 v2, v2, 0xd0

    #@24
    if-nez v2, :cond_30

    #@26
    .line 1468
    :cond_26
    sget-object v2, Landroid/os/StrictMode;->sProcessIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    #@28
    invoke-virtual {v1, v2}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@2b
    .line 1469
    const/4 v2, 0x0

    #@2c
    sput-boolean v2, Landroid/os/StrictMode;->sIsIdlerRegistered:Z

    #@2e
    .line 1475
    .end local v1           #mq:Landroid/os/MessageQueue;
    :cond_2e
    :goto_2e
    monitor-exit v3

    #@2f
    .line 1476
    return-void

    #@30
    .line 1470
    .restart local v1       #mq:Landroid/os/MessageQueue;
    :cond_30
    sget-boolean v2, Landroid/os/StrictMode;->sIsIdlerRegistered:Z

    #@32
    if-nez v2, :cond_2e

    #@34
    .line 1471
    sget-object v2, Landroid/os/StrictMode;->sProcessIdleHandler:Landroid/os/MessageQueue$IdleHandler;

    #@36
    invoke-virtual {v1, v2}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@39
    .line 1472
    const/4 v2, 0x1

    #@3a
    sput-boolean v2, Landroid/os/StrictMode;->sIsIdlerRegistered:Z

    #@3c
    goto :goto_2e

    #@3d
    .line 1475
    .end local v0           #looper:Landroid/os/Looper;
    .end local v1           #mq:Landroid/os/MessageQueue;
    :catchall_3d
    move-exception v2

    #@3e
    monitor-exit v3
    :try_end_3f
    .catchall {:try_start_3 .. :try_end_3f} :catchall_3d

    #@3f
    throw v2
.end method

.method private static tooManyViolationsThisLoop()Z
    .registers 2

    #@0
    .prologue
    .line 1050
    sget-object v0, Landroid/os/StrictMode;->violationsBeingTimed:Ljava/lang/ThreadLocal;

    #@2
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/ArrayList;

    #@8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v0

    #@c
    const/16 v1, 0xa

    #@e
    if-lt v0, v1, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public static trackActivity(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "instance"

    #@0
    .prologue
    .line 1899
    new-instance v0, Landroid/os/StrictMode$InstanceTracker;

    #@2
    invoke-direct {v0, p0}, Landroid/os/StrictMode$InstanceTracker;-><init>(Ljava/lang/Object;)V

    #@5
    return-object v0
.end method

.method public static vmClosableObjectLeaksEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 1517
    sget v0, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@2
    and-int/lit16 v0, v0, 0x400

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public static vmRegistrationLeaksEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 1524
    sget v0, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@2
    and-int/lit16 v0, v0, 0x2000

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public static vmSqliteObjectLeaksEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 1510
    sget v0, Landroid/os/StrictMode;->sVmPolicyMask:I

    #@2
    and-int/lit16 v0, v0, 0x200

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method static writeGatheredViolationsToParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "p"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1636
    sget-object v2, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@3
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Ljava/util/ArrayList;

    #@9
    .line 1637
    .local v1, violations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/StrictMode$ViolationInfo;>;"
    if-nez v1, :cond_15

    #@b
    .line 1638
    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 1647
    :goto_e
    sget-object v2, Landroid/os/StrictMode;->gatheredViolations:Ljava/lang/ThreadLocal;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    #@14
    .line 1648
    return-void

    #@15
    .line 1640
    :cond_15
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@18
    move-result v2

    #@19
    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 1641
    const/4 v0, 0x0

    #@1d
    .local v0, i:I
    :goto_1d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@20
    move-result v2

    #@21
    if-ge v0, v2, :cond_2f

    #@23
    .line 1642
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@26
    move-result-object v2

    #@27
    check-cast v2, Landroid/os/StrictMode$ViolationInfo;

    #@29
    invoke-virtual {v2, p0, v3}, Landroid/os/StrictMode$ViolationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2c
    .line 1641
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1d

    #@2f
    .line 1644
    :cond_2f
    sget-boolean v2, Landroid/os/StrictMode;->LOG_V:Z

    #@31
    if-eqz v2, :cond_50

    #@33
    const-string v2, "StrictMode"

    #@35
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string/jumbo v4, "wrote violations to response parcel; num="

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@44
    move-result v4

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1645
    :cond_50
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@53
    goto :goto_e
.end method
