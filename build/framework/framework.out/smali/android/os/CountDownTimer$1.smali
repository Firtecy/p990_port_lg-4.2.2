.class Landroid/os/CountDownTimer$1;
.super Landroid/os/Handler;
.source "CountDownTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/CountDownTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/os/CountDownTimer;


# direct methods
.method constructor <init>(Landroid/os/CountDownTimer;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 109
    iput-object p1, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v12, 0x0

    #@2
    .line 114
    iget-object v7, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@4
    monitor-enter v7

    #@5
    .line 115
    :try_start_5
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@7
    invoke-static {v6}, Landroid/os/CountDownTimer;->access$000(Landroid/os/CountDownTimer;)J

    #@a
    move-result-wide v8

    #@b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@e
    move-result-wide v10

    #@f
    sub-long v4, v8, v10

    #@11
    .line 117
    .local v4, millisLeft:J
    cmp-long v6, v4, v12

    #@13
    if-gtz v6, :cond_1c

    #@15
    .line 118
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@17
    invoke-virtual {v6}, Landroid/os/CountDownTimer;->onFinish()V

    #@1a
    .line 135
    :goto_1a
    monitor-exit v7

    #@1b
    .line 136
    return-void

    #@1c
    .line 119
    :cond_1c
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@1e
    invoke-static {v6}, Landroid/os/CountDownTimer;->access$100(Landroid/os/CountDownTimer;)J

    #@21
    move-result-wide v8

    #@22
    cmp-long v6, v4, v8

    #@24
    if-gez v6, :cond_32

    #@26
    .line 121
    const/4 v6, 0x1

    #@27
    invoke-virtual {p0, v6}, Landroid/os/CountDownTimer$1;->obtainMessage(I)Landroid/os/Message;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {p0, v6, v4, v5}, Landroid/os/CountDownTimer$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2e
    goto :goto_1a

    #@2f
    .line 135
    .end local v4           #millisLeft:J
    :catchall_2f
    move-exception v6

    #@30
    monitor-exit v7
    :try_end_31
    .catchall {:try_start_5 .. :try_end_31} :catchall_2f

    #@31
    throw v6

    #@32
    .line 123
    .restart local v4       #millisLeft:J
    :cond_32
    :try_start_32
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@35
    move-result-wide v2

    #@36
    .line 124
    .local v2, lastTickStart:J
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@38
    invoke-virtual {v6, v4, v5}, Landroid/os/CountDownTimer;->onTick(J)V

    #@3b
    .line 127
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@3d
    invoke-static {v6}, Landroid/os/CountDownTimer;->access$100(Landroid/os/CountDownTimer;)J

    #@40
    move-result-wide v8

    #@41
    add-long/2addr v8, v2

    #@42
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@45
    move-result-wide v10

    #@46
    sub-long v0, v8, v10

    #@48
    .line 131
    .local v0, delay:J
    :goto_48
    cmp-long v6, v0, v12

    #@4a
    if-gez v6, :cond_54

    #@4c
    iget-object v6, p0, Landroid/os/CountDownTimer$1;->this$0:Landroid/os/CountDownTimer;

    #@4e
    invoke-static {v6}, Landroid/os/CountDownTimer;->access$100(Landroid/os/CountDownTimer;)J

    #@51
    move-result-wide v8

    #@52
    add-long/2addr v0, v8

    #@53
    goto :goto_48

    #@54
    .line 133
    :cond_54
    const/4 v6, 0x1

    #@55
    invoke-virtual {p0, v6}, Landroid/os/CountDownTimer$1;->obtainMessage(I)Landroid/os/Message;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {p0, v6, v0, v1}, Landroid/os/CountDownTimer$1;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_5c
    .catchall {:try_start_32 .. :try_end_5c} :catchall_2f

    #@5c
    goto :goto_1a
.end method
