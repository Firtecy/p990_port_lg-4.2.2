.class public final Landroid/os/CancellationSignal;
.super Ljava/lang/Object;
.source "CancellationSignal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/CancellationSignal$1;,
        Landroid/os/CancellationSignal$Transport;,
        Landroid/os/CancellationSignal$OnCancelListener;
    }
.end annotation


# instance fields
.field private mCancelInProgress:Z

.field private mIsCanceled:Z

.field private mOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

.field private mRemote:Landroid/os/ICancellationSignal;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    return-void
.end method

.method public static createTransport()Landroid/os/ICancellationSignal;
    .registers 2

    #@0
    .prologue
    .line 174
    new-instance v0, Landroid/os/CancellationSignal$Transport;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/os/CancellationSignal$Transport;-><init>(Landroid/os/CancellationSignal$1;)V

    #@6
    return-object v0
.end method

.method public static fromTransport(Landroid/os/ICancellationSignal;)Landroid/os/CancellationSignal;
    .registers 2
    .parameter "transport"

    #@0
    .prologue
    .line 186
    instance-of v0, p0, Landroid/os/CancellationSignal$Transport;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 187
    check-cast p0, Landroid/os/CancellationSignal$Transport;

    #@6
    .end local p0
    iget-object v0, p0, Landroid/os/CancellationSignal$Transport;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@8
    .line 189
    :goto_8
    return-object v0

    #@9
    .restart local p0
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private waitForCancelFinishedLocked()V
    .registers 2

    #@0
    .prologue
    .line 157
    :goto_0
    iget-boolean v0, p0, Landroid/os/CancellationSignal;->mCancelInProgress:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 159
    :try_start_4
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_7} :catch_8

    #@7
    goto :goto_0

    #@8
    .line 160
    :catch_8
    move-exception v0

    #@9
    goto :goto_0

    #@a
    .line 163
    :cond_a
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 5

    #@0
    .prologue
    .line 66
    monitor-enter p0

    #@1
    .line 67
    :try_start_1
    iget-boolean v2, p0, Landroid/os/CancellationSignal;->mIsCanceled:Z

    #@3
    if-eqz v2, :cond_7

    #@5
    .line 68
    monitor-exit p0

    #@6
    .line 92
    :goto_6
    return-void

    #@7
    .line 70
    :cond_7
    const/4 v2, 0x1

    #@8
    iput-boolean v2, p0, Landroid/os/CancellationSignal;->mIsCanceled:Z

    #@a
    .line 71
    const/4 v2, 0x1

    #@b
    iput-boolean v2, p0, Landroid/os/CancellationSignal;->mCancelInProgress:Z

    #@d
    .line 72
    iget-object v0, p0, Landroid/os/CancellationSignal;->mOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    #@f
    .line 73
    .local v0, listener:Landroid/os/CancellationSignal$OnCancelListener;
    iget-object v1, p0, Landroid/os/CancellationSignal;->mRemote:Landroid/os/ICancellationSignal;

    #@11
    .line 74
    .local v1, remote:Landroid/os/ICancellationSignal;
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_28

    #@12
    .line 77
    if-eqz v0, :cond_17

    #@14
    .line 78
    :try_start_14
    invoke-interface {v0}, Landroid/os/CancellationSignal$OnCancelListener;->onCancel()V
    :try_end_17
    .catchall {:try_start_14 .. :try_end_17} :catchall_2b

    #@17
    .line 80
    :cond_17
    if-eqz v1, :cond_1c

    #@19
    .line 82
    :try_start_19
    invoke-interface {v1}, Landroid/os/ICancellationSignal;->cancel()V
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_2b
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_1c} :catch_38

    #@1c
    .line 87
    :cond_1c
    :goto_1c
    monitor-enter p0

    #@1d
    .line 88
    const/4 v2, 0x0

    #@1e
    :try_start_1e
    iput-boolean v2, p0, Landroid/os/CancellationSignal;->mCancelInProgress:Z

    #@20
    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@23
    .line 90
    monitor-exit p0

    #@24
    goto :goto_6

    #@25
    :catchall_25
    move-exception v2

    #@26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_1e .. :try_end_27} :catchall_25

    #@27
    throw v2

    #@28
    .line 74
    .end local v0           #listener:Landroid/os/CancellationSignal$OnCancelListener;
    .end local v1           #remote:Landroid/os/ICancellationSignal;
    :catchall_28
    move-exception v2

    #@29
    :try_start_29
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v2

    #@2b
    .line 87
    .restart local v0       #listener:Landroid/os/CancellationSignal$OnCancelListener;
    .restart local v1       #remote:Landroid/os/ICancellationSignal;
    :catchall_2b
    move-exception v2

    #@2c
    monitor-enter p0

    #@2d
    .line 88
    const/4 v3, 0x0

    #@2e
    :try_start_2e
    iput-boolean v3, p0, Landroid/os/CancellationSignal;->mCancelInProgress:Z

    #@30
    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    #@33
    .line 90
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_2e .. :try_end_34} :catchall_35

    #@34
    throw v2

    #@35
    :catchall_35
    move-exception v2

    #@36
    :try_start_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v2

    #@38
    .line 83
    :catch_38
    move-exception v2

    #@39
    goto :goto_1c
.end method

.method public isCanceled()Z
    .registers 2

    #@0
    .prologue
    .line 43
    monitor-enter p0

    #@1
    .line 44
    :try_start_1
    iget-boolean v0, p0, Landroid/os/CancellationSignal;->mIsCanceled:Z

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 45
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 111
    monitor-enter p0

    #@1
    .line 112
    :try_start_1
    invoke-direct {p0}, Landroid/os/CancellationSignal;->waitForCancelFinishedLocked()V

    #@4
    .line 114
    iget-object v0, p0, Landroid/os/CancellationSignal;->mOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    #@6
    if-ne v0, p1, :cond_a

    #@8
    .line 115
    monitor-exit p0

    #@9
    .line 123
    :goto_9
    return-void

    #@a
    .line 117
    :cond_a
    iput-object p1, p0, Landroid/os/CancellationSignal;->mOnCancelListener:Landroid/os/CancellationSignal$OnCancelListener;

    #@c
    .line 118
    iget-boolean v0, p0, Landroid/os/CancellationSignal;->mIsCanceled:Z

    #@e
    if-eqz v0, :cond_12

    #@10
    if-nez p1, :cond_17

    #@12
    .line 119
    :cond_12
    monitor-exit p0

    #@13
    goto :goto_9

    #@14
    .line 121
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v0

    #@17
    :cond_17
    :try_start_17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_14

    #@18
    .line 122
    invoke-interface {p1}, Landroid/os/CancellationSignal$OnCancelListener;->onCancel()V

    #@1b
    goto :goto_9
.end method

.method public setRemote(Landroid/os/ICancellationSignal;)V
    .registers 3
    .parameter "remote"

    #@0
    .prologue
    .line 139
    monitor-enter p0

    #@1
    .line 140
    :try_start_1
    invoke-direct {p0}, Landroid/os/CancellationSignal;->waitForCancelFinishedLocked()V

    #@4
    .line 142
    iget-object v0, p0, Landroid/os/CancellationSignal;->mRemote:Landroid/os/ICancellationSignal;

    #@6
    if-ne v0, p1, :cond_a

    #@8
    .line 143
    monitor-exit p0

    #@9
    .line 154
    :goto_9
    return-void

    #@a
    .line 145
    :cond_a
    iput-object p1, p0, Landroid/os/CancellationSignal;->mRemote:Landroid/os/ICancellationSignal;

    #@c
    .line 146
    iget-boolean v0, p0, Landroid/os/CancellationSignal;->mIsCanceled:Z

    #@e
    if-eqz v0, :cond_12

    #@10
    if-nez p1, :cond_17

    #@12
    .line 147
    :cond_12
    monitor-exit p0

    #@13
    goto :goto_9

    #@14
    .line 149
    :catchall_14
    move-exception v0

    #@15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_14

    #@16
    throw v0

    #@17
    :cond_17
    :try_start_17
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_14

    #@18
    .line 151
    :try_start_18
    invoke-interface {p1}, Landroid/os/ICancellationSignal;->cancel()V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_1b} :catch_1c

    #@1b
    goto :goto_9

    #@1c
    .line 152
    :catch_1c
    move-exception v0

    #@1d
    goto :goto_9
.end method

.method public throwIfCanceled()V
    .registers 2

    #@0
    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/os/CancellationSignal;->isCanceled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    .line 55
    new-instance v0, Landroid/os/OperationCanceledException;

    #@8
    invoke-direct {v0}, Landroid/os/OperationCanceledException;-><init>()V

    #@b
    throw v0

    #@c
    .line 57
    :cond_c
    return-void
.end method
