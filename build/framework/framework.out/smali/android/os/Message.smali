.class public final Landroid/os/Message;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field static final FLAGS_TO_CLEAR_ON_COPY_FROM:I = 0x1

.field static final FLAG_ASYNCHRONOUS:I = 0x2

.field static final FLAG_IN_USE:I = 0x1

.field private static final MAX_POOL_SIZE:I = 0x32

.field private static sPool:Landroid/os/Message;

.field private static sPoolSize:I

.field private static final sPoolSync:Ljava/lang/Object;


# instance fields
.field public arg1:I

.field public arg2:I

.field callback:Ljava/lang/Runnable;

.field data:Landroid/os/Bundle;

.field flags:I

.field next:Landroid/os/Message;

.field public obj:Ljava/lang/Object;

.field public replyTo:Landroid/os/Messenger;

.field target:Landroid/os/Handler;

.field public what:I

.field when:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 96
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    #@7
    .line 98
    const/4 v0, 0x0

    #@8
    sput v0, Landroid/os/Message;->sPoolSize:I

    #@a
    .line 464
    new-instance v0, Landroid/os/Message$1;

    #@c
    invoke-direct {v0}, Landroid/os/Message$1;-><init>()V

    #@f
    sput-object v0, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    #@11
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 428
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 429
    return-void
.end method

.method static synthetic access$000(Landroid/os/Message;Landroid/os/Parcel;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/os/Message;->readFromParcel(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public static obtain()Landroid/os/Message;
    .registers 3

    #@0
    .prologue
    .line 107
    sget-object v2, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 108
    :try_start_3
    sget-object v1, Landroid/os/Message;->sPool:Landroid/os/Message;

    #@5
    if-eqz v1, :cond_18

    #@7
    .line 109
    sget-object v0, Landroid/os/Message;->sPool:Landroid/os/Message;

    #@9
    .line 110
    .local v0, m:Landroid/os/Message;
    iget-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@b
    sput-object v1, Landroid/os/Message;->sPool:Landroid/os/Message;

    #@d
    .line 111
    const/4 v1, 0x0

    #@e
    iput-object v1, v0, Landroid/os/Message;->next:Landroid/os/Message;

    #@10
    .line 112
    sget v1, Landroid/os/Message;->sPoolSize:I

    #@12
    add-int/lit8 v1, v1, -0x1

    #@14
    sput v1, Landroid/os/Message;->sPoolSize:I

    #@16
    .line 113
    monitor-exit v2

    #@17
    .line 116
    .end local v0           #m:Landroid/os/Message;
    :goto_17
    return-object v0

    #@18
    .line 115
    :cond_18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1f

    #@19
    .line 116
    new-instance v0, Landroid/os/Message;

    #@1b
    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    #@1e
    goto :goto_17

    #@1f
    .line 115
    .restart local v0       #m:Landroid/os/Message;
    :catchall_1f
    move-exception v1

    #@20
    :try_start_20
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    #@21
    throw v1
.end method

.method public static obtain(Landroid/os/Handler;)Landroid/os/Message;
    .registers 2
    .parameter "h"

    #@0
    .prologue
    .line 147
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 148
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 150
    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;I)Landroid/os/Message;
    .registers 3
    .parameter "h"
    .parameter "what"

    #@0
    .prologue
    .line 176
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 177
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 178
    iput p1, v0, Landroid/os/Message;->what:I

    #@8
    .line 180
    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;III)Landroid/os/Message;
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 211
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 212
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 213
    iput p1, v0, Landroid/os/Message;->what:I

    #@8
    .line 214
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 215
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 217
    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "obj"

    #@0
    .prologue
    .line 233
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 234
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 235
    iput p1, v0, Landroid/os/Message;->what:I

    #@8
    .line 236
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 237
    iput p3, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 238
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e
    .line 240
    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    .registers 4
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 192
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 193
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 194
    iput p1, v0, Landroid/os/Message;->what:I

    #@8
    .line 195
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 197
    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;
    .registers 3
    .parameter "h"
    .parameter "callback"

    #@0
    .prologue
    .line 161
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 162
    .local v0, m:Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@6
    .line 163
    iput-object p1, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@8
    .line 165
    return-object v0
.end method

.method public static obtain(Landroid/os/Message;)Landroid/os/Message;
    .registers 4
    .parameter "orig"

    #@0
    .prologue
    .line 126
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 127
    .local v0, m:Landroid/os/Message;
    iget v1, p0, Landroid/os/Message;->what:I

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 128
    iget v1, p0, Landroid/os/Message;->arg1:I

    #@a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 129
    iget v1, p0, Landroid/os/Message;->arg2:I

    #@e
    iput v1, v0, Landroid/os/Message;->arg2:I

    #@10
    .line 130
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14
    .line 131
    iget-object v1, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@16
    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@18
    .line 132
    iget-object v1, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@1a
    if-eqz v1, :cond_25

    #@1c
    .line 133
    new-instance v1, Landroid/os/Bundle;

    #@1e
    iget-object v2, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@20
    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@23
    iput-object v1, v0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@25
    .line 135
    :cond_25
    iget-object v1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@27
    iput-object v1, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@29
    .line 136
    iget-object v1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2b
    iput-object v1, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2d
    .line 138
    return-object v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 507
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/os/Message;->what:I

    #@6
    .line 508
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/os/Message;->arg1:I

    #@c
    .line 509
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/os/Message;->arg2:I

    #@12
    .line 510
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_26

    #@18
    .line 511
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1b
    move-result-object v0

    #@1c
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@23
    move-result-object v0

    #@24
    iput-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26
    .line 513
    :cond_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@29
    move-result-wide v0

    #@2a
    iput-wide v0, p0, Landroid/os/Message;->when:J

    #@2c
    .line 514
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    #@2f
    move-result-object v0

    #@30
    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@32
    .line 515
    invoke-static {p1}, Landroid/os/Messenger;->readMessengerOrNullFromParcel(Landroid/os/Parcel;)Landroid/os/Messenger;

    #@35
    move-result-object v0

    #@36
    iput-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@38
    .line 516
    return-void
.end method


# virtual methods
.method clearForRecycle()V
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 406
    iput v0, p0, Landroid/os/Message;->flags:I

    #@4
    .line 407
    iput v0, p0, Landroid/os/Message;->what:I

    #@6
    .line 408
    iput v0, p0, Landroid/os/Message;->arg1:I

    #@8
    .line 409
    iput v0, p0, Landroid/os/Message;->arg2:I

    #@a
    .line 410
    iput-object v2, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    .line 411
    iput-object v2, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@e
    .line 412
    const-wide/16 v0, 0x0

    #@10
    iput-wide v0, p0, Landroid/os/Message;->when:J

    #@12
    .line 413
    iput-object v2, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@14
    .line 414
    iput-object v2, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@16
    .line 415
    iput-object v2, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@18
    .line 416
    return-void
.end method

.method public copyFrom(Landroid/os/Message;)V
    .registers 3
    .parameter "o"

    #@0
    .prologue
    .line 266
    iget v0, p1, Landroid/os/Message;->flags:I

    #@2
    and-int/lit8 v0, v0, -0x2

    #@4
    iput v0, p0, Landroid/os/Message;->flags:I

    #@6
    .line 267
    iget v0, p1, Landroid/os/Message;->what:I

    #@8
    iput v0, p0, Landroid/os/Message;->what:I

    #@a
    .line 268
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@c
    iput v0, p0, Landroid/os/Message;->arg1:I

    #@e
    .line 269
    iget v0, p1, Landroid/os/Message;->arg2:I

    #@10
    iput v0, p0, Landroid/os/Message;->arg2:I

    #@12
    .line 270
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@14
    iput-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@16
    .line 271
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@18
    iput-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@1a
    .line 273
    iget-object v0, p1, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@1c
    if-eqz v0, :cond_29

    #@1e
    .line 274
    iget-object v0, p1, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@20
    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    #@23
    move-result-object v0

    #@24
    check-cast v0, Landroid/os/Bundle;

    #@26
    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@28
    .line 278
    :goto_28
    return-void

    #@29
    .line 276
    :cond_29
    const/4 v0, 0x0

    #@2a
    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@2c
    goto :goto_28
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 478
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCallback()Ljava/lang/Runnable;
    .registers 2

    #@0
    .prologue
    .line 312
    iget-object v0, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method public getData()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 328
    new-instance v0, Landroid/os/Bundle;

    #@6
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@9
    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@b
    .line 331
    :cond_b
    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@d
    return-object v0
.end method

.method public getTarget()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public getWhen()J
    .registers 3

    #@0
    .prologue
    .line 284
    iget-wide v0, p0, Landroid/os/Message;->when:J

    #@2
    return-wide v0
.end method

.method public isAsynchronous()Z
    .registers 2

    #@0
    .prologue
    .line 379
    iget v0, p0, Landroid/os/Message;->flags:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method isInUse()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 419
    iget v1, p0, Landroid/os/Message;->flags:I

    #@3
    and-int/lit8 v1, v1, 0x1

    #@5
    if-ne v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method markInUse()V
    .registers 2

    #@0
    .prologue
    .line 423
    iget v0, p0, Landroid/os/Message;->flags:I

    #@2
    or-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Landroid/os/Message;->flags:I

    #@6
    .line 424
    return-void
.end method

.method public peekData()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 342
    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/os/Message;->clearForRecycle()V

    #@3
    .line 251
    sget-object v1, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    #@5
    monitor-enter v1

    #@6
    .line 252
    :try_start_6
    sget v0, Landroid/os/Message;->sPoolSize:I

    #@8
    const/16 v2, 0x32

    #@a
    if-ge v0, v2, :cond_18

    #@c
    .line 253
    sget-object v0, Landroid/os/Message;->sPool:Landroid/os/Message;

    #@e
    iput-object v0, p0, Landroid/os/Message;->next:Landroid/os/Message;

    #@10
    .line 254
    sput-object p0, Landroid/os/Message;->sPool:Landroid/os/Message;

    #@12
    .line 255
    sget v0, Landroid/os/Message;->sPoolSize:I

    #@14
    add-int/lit8 v0, v0, 0x1

    #@16
    sput v0, Landroid/os/Message;->sPoolSize:I

    #@18
    .line 257
    :cond_18
    monitor-exit v1

    #@19
    .line 258
    return-void

    #@1a
    .line 257
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_6 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public sendToTarget()V
    .registers 2

    #@0
    .prologue
    .line 360
    iget-object v0, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@2
    invoke-virtual {v0, p0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@5
    .line 361
    return-void
.end method

.method public setAsynchronous(Z)V
    .registers 3
    .parameter "async"

    #@0
    .prologue
    .line 398
    if-eqz p1, :cond_9

    #@2
    .line 399
    iget v0, p0, Landroid/os/Message;->flags:I

    #@4
    or-int/lit8 v0, v0, 0x2

    #@6
    iput v0, p0, Landroid/os/Message;->flags:I

    #@8
    .line 403
    :goto_8
    return-void

    #@9
    .line 401
    :cond_9
    iget v0, p0, Landroid/os/Message;->flags:I

    #@b
    and-int/lit8 v0, v0, -0x3

    #@d
    iput v0, p0, Landroid/os/Message;->flags:I

    #@f
    goto :goto_8
.end method

.method public setData(Landroid/os/Bundle;)V
    .registers 2
    .parameter "data"

    #@0
    .prologue
    .line 352
    iput-object p1, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@2
    .line 353
    return-void
.end method

.method public setTarget(Landroid/os/Handler;)V
    .registers 2
    .parameter "target"

    #@0
    .prologue
    .line 288
    iput-object p1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    #@2
    .line 289
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 432
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/os/Message;->toString(J)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method toString(J)Ljava/lang/String;
    .registers 6
    .parameter "now"

    #@0
    .prologue
    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 438
    .local v0, b:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{ what="

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 439
    iget v1, p0, Landroid/os/Message;->what:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    .line 441
    const-string v1, " when="

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 442
    iget-wide v1, p0, Landroid/os/Message;->when:J

    #@17
    sub-long/2addr v1, p1

    #@18
    invoke-static {v1, v2, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    #@1b
    .line 444
    iget v1, p0, Landroid/os/Message;->arg1:I

    #@1d
    if-eqz v1, :cond_29

    #@1f
    .line 445
    const-string v1, " arg1="

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    .line 446
    iget v1, p0, Landroid/os/Message;->arg1:I

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    .line 449
    :cond_29
    iget v1, p0, Landroid/os/Message;->arg2:I

    #@2b
    if-eqz v1, :cond_37

    #@2d
    .line 450
    const-string v1, " arg2="

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 451
    iget v1, p0, Landroid/os/Message;->arg2:I

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    .line 454
    :cond_37
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@39
    if-eqz v1, :cond_45

    #@3b
    .line 455
    const-string v1, " obj="

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    .line 456
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@45
    .line 459
    :cond_45
    const-string v1, " }"

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    .line 461
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 482
    iget-object v2, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    #@2
    if-eqz v2, :cond_c

    #@4
    .line 483
    new-instance v2, Ljava/lang/RuntimeException;

    #@6
    const-string v3, "Can\'t marshal callbacks across processes."

    #@8
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 486
    :cond_c
    iget v2, p0, Landroid/os/Message;->what:I

    #@e
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 487
    iget v2, p0, Landroid/os/Message;->arg1:I

    #@13
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 488
    iget v2, p0, Landroid/os/Message;->arg2:I

    #@18
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1b
    .line 489
    iget-object v2, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    if-eqz v2, :cond_43

    #@1f
    .line 491
    :try_start_1f
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v1, Landroid/os/Parcelable;

    #@23
    .line 492
    .local v1, p:Landroid/os/Parcelable;
    const/4 v2, 0x1

    #@24
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 493
    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
    :try_end_2a
    .catch Ljava/lang/ClassCastException; {:try_start_1f .. :try_end_2a} :catch_3a

    #@2a
    .line 501
    .end local v1           #p:Landroid/os/Parcelable;
    :goto_2a
    iget-wide v2, p0, Landroid/os/Message;->when:J

    #@2c
    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    #@2f
    .line 502
    iget-object v2, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    #@31
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    #@34
    .line 503
    iget-object v2, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    #@36
    invoke-static {v2, p1}, Landroid/os/Messenger;->writeMessengerOrNullToParcel(Landroid/os/Messenger;Landroid/os/Parcel;)V

    #@39
    .line 504
    return-void

    #@3a
    .line 494
    :catch_3a
    move-exception v0

    #@3b
    .line 495
    .local v0, e:Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/RuntimeException;

    #@3d
    const-string v3, "Can\'t marshal non-Parcelable objects across processes."

    #@3f
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@42
    throw v2

    #@43
    .line 499
    .end local v0           #e:Ljava/lang/ClassCastException;
    :cond_43
    const/4 v2, 0x0

    #@44
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    goto :goto_2a
.end method
