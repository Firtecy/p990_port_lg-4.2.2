.class Landroid/os/storage/StorageManager$StorageEvent;
.super Ljava/lang/Object;
.source "StorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/storage/StorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StorageEvent"
.end annotation


# static fields
.field static final EVENT_OBB_STATE_CHANGED:I = 0x3

.field static final EVENT_STORAGE_STATE_CHANGED:I = 0x2

.field static final EVENT_UMS_CONNECTION_CHANGED:I = 0x1


# instance fields
.field private mMessage:Landroid/os/Message;

.field final synthetic this$0:Landroid/os/storage/StorageManager;


# direct methods
.method public constructor <init>(Landroid/os/storage/StorageManager;I)V
    .registers 4
    .parameter
    .parameter "what"

    #@0
    .prologue
    .line 213
    iput-object p1, p0, Landroid/os/storage/StorageManager$StorageEvent;->this$0:Landroid/os/storage/StorageManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 214
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    iput-object v0, p0, Landroid/os/storage/StorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    #@b
    .line 215
    iget-object v0, p0, Landroid/os/storage/StorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    #@d
    iput p2, v0, Landroid/os/Message;->what:I

    #@f
    .line 216
    iget-object v0, p0, Landroid/os/storage/StorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    #@11
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 217
    return-void
.end method


# virtual methods
.method public getMessage()Landroid/os/Message;
    .registers 2

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/os/storage/StorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    #@2
    return-object v0
.end method
