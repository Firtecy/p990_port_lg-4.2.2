.class public abstract Landroid/os/storage/IMountService$Stub;
.super Landroid/os/Binder;
.source "IMountService.java"

# interfaces
.implements Landroid/os/storage/IMountService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/storage/IMountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/storage/IMountService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "IMountService"

.field static final TRANSACTION_changeEncryptionPassword:I = 0x1d

.field static final TRANSACTION_createSecureContainer:I = 0xb

.field static final TRANSACTION_decryptStorage:I = 0x1b

.field static final TRANSACTION_destroySecureContainer:I = 0xd

.field static final TRANSACTION_encryptStorage:I = 0x1c

.field static final TRANSACTION_finalizeSecureContainer:I = 0xc

.field static final TRANSACTION_finishMediaUpdate:I = 0x15

.field static final TRANSACTION_fixPermissionsSecureContainer:I = 0x22

.field static final TRANSACTION_formatVolume:I = 0x8

.field static final TRANSACTION_getBackupSecureContainerList:I = 0x23

.field static final TRANSACTION_getEncryptionState:I = 0x20

.field static final TRANSACTION_getMountedObbPath:I = 0x19

.field static final TRANSACTION_getSecureContainerFilesystemPath:I = 0x1f

.field static final TRANSACTION_getSecureContainerList:I = 0x13

.field static final TRANSACTION_getSecureContainerPath:I = 0x12

.field static final TRANSACTION_getStorageUsers:I = 0x9

.field static final TRANSACTION_getVolumeList:I = 0x1e

.field static final TRANSACTION_getVolumeState:I = 0xa

.field static final TRANSACTION_isExternalStorageEmulated:I = 0x1a

.field static final TRANSACTION_isObbMounted:I = 0x18

.field static final TRANSACTION_isSecureContainerMounted:I = 0x10

.field static final TRANSACTION_isUsbMassStorageConnected:I = 0x3

.field static final TRANSACTION_isUsbMassStorageEnabled:I = 0x5

.field static final TRANSACTION_mountNetStorage:I = 0x24

.field static final TRANSACTION_mountObb:I = 0x16

.field static final TRANSACTION_mountSecureContainer:I = 0xe

.field static final TRANSACTION_mountVolume:I = 0x6

.field static final TRANSACTION_registerListener:I = 0x1

.field static final TRANSACTION_renameSecureContainer:I = 0x11

.field static final TRANSACTION_setUsbMassStorageEnabled:I = 0x4

.field static final TRANSACTION_shutdown:I = 0x14

.field static final TRANSACTION_unencryptStorage:I = 0x26

.field static final TRANSACTION_unmountNetStorage:I = 0x25

.field static final TRANSACTION_unmountObb:I = 0x17

.field static final TRANSACTION_unmountSecureContainer:I = 0xf

.field static final TRANSACTION_unmountVolume:I = 0x7

.field static final TRANSACTION_unregisterListener:I = 0x2

.field static final TRANSACTION_verifyEncryptionPassword:I = 0x21


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 930
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 931
    const-string v0, "IMountService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/storage/IMountService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 932
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 919
    if-nez p0, :cond_4

    #@2
    .line 920
    const/4 v0, 0x0

    #@3
    .line 926
    :goto_3
    return-object v0

    #@4
    .line 922
    :cond_4
    const-string v1, "IMountService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 923
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/storage/IMountService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 924
    check-cast v0, Landroid/os/storage/IMountService;

    #@12
    goto :goto_3

    #@13
    .line 926
    :cond_13
    new-instance v0, Landroid/os/storage/IMountService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/storage/IMountService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 935
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 46
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 941
    sparse-switch p1, :sswitch_data_4e8

    #@3
    .line 1310
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v4

    #@7
    :goto_7
    return v4

    #@8
    .line 943
    :sswitch_8
    const-string v4, "IMountService"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 944
    const/4 v4, 0x1

    #@10
    goto :goto_7

    #@11
    .line 947
    :sswitch_11
    const-string v4, "IMountService"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 949
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v4}, Landroid/os/storage/IMountServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountServiceListener;

    #@1f
    move-result-object v30

    #@20
    .line 950
    .local v30, listener:Landroid/os/storage/IMountServiceListener;
    move-object/from16 v0, p0

    #@22
    move-object/from16 v1, v30

    #@24
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->registerListener(Landroid/os/storage/IMountServiceListener;)V

    #@27
    .line 951
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    .line 952
    const/4 v4, 0x1

    #@2b
    goto :goto_7

    #@2c
    .line 955
    .end local v30           #listener:Landroid/os/storage/IMountServiceListener;
    :sswitch_2c
    const-string v4, "IMountService"

    #@2e
    move-object/from16 v0, p2

    #@30
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@33
    .line 957
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@36
    move-result-object v4

    #@37
    invoke-static {v4}, Landroid/os/storage/IMountServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountServiceListener;

    #@3a
    move-result-object v30

    #@3b
    .line 958
    .restart local v30       #listener:Landroid/os/storage/IMountServiceListener;
    move-object/from16 v0, p0

    #@3d
    move-object/from16 v1, v30

    #@3f
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->unregisterListener(Landroid/os/storage/IMountServiceListener;)V

    #@42
    .line 959
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@45
    .line 960
    const/4 v4, 0x1

    #@46
    goto :goto_7

    #@47
    .line 963
    .end local v30           #listener:Landroid/os/storage/IMountServiceListener;
    :sswitch_47
    const-string v4, "IMountService"

    #@49
    move-object/from16 v0, p2

    #@4b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4e
    .line 964
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->isUsbMassStorageConnected()Z

    #@51
    move-result v37

    #@52
    .line 965
    .local v37, result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@55
    .line 966
    if-eqz v37, :cond_5f

    #@57
    const/4 v4, 0x1

    #@58
    :goto_58
    move-object/from16 v0, p3

    #@5a
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 967
    const/4 v4, 0x1

    #@5e
    goto :goto_7

    #@5f
    .line 966
    :cond_5f
    const/4 v4, 0x0

    #@60
    goto :goto_58

    #@61
    .line 970
    .end local v37           #result:Z
    :sswitch_61
    const-string v4, "IMountService"

    #@63
    move-object/from16 v0, p2

    #@65
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@68
    .line 972
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_7c

    #@6e
    const/16 v25, 0x1

    #@70
    .line 973
    .local v25, enable:Z
    :goto_70
    move-object/from16 v0, p0

    #@72
    move/from16 v1, v25

    #@74
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->setUsbMassStorageEnabled(Z)V

    #@77
    .line 974
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@7a
    .line 975
    const/4 v4, 0x1

    #@7b
    goto :goto_7

    #@7c
    .line 972
    .end local v25           #enable:Z
    :cond_7c
    const/16 v25, 0x0

    #@7e
    goto :goto_70

    #@7f
    .line 978
    :sswitch_7f
    const-string v4, "IMountService"

    #@81
    move-object/from16 v0, p2

    #@83
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 979
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->isUsbMassStorageEnabled()Z

    #@89
    move-result v37

    #@8a
    .line 980
    .restart local v37       #result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    .line 981
    if-eqz v37, :cond_98

    #@8f
    const/4 v4, 0x1

    #@90
    :goto_90
    move-object/from16 v0, p3

    #@92
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@95
    .line 982
    const/4 v4, 0x1

    #@96
    goto/16 :goto_7

    #@98
    .line 981
    :cond_98
    const/4 v4, 0x0

    #@99
    goto :goto_90

    #@9a
    .line 985
    .end local v37           #result:Z
    :sswitch_9a
    const-string v4, "IMountService"

    #@9c
    move-object/from16 v0, p2

    #@9e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 987
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a4
    move-result-object v18

    #@a5
    .line 988
    .local v18, mountPoint:Ljava/lang/String;
    move-object/from16 v0, p0

    #@a7
    move-object/from16 v1, v18

    #@a9
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->mountVolume(Ljava/lang/String;)I

    #@ac
    move-result v38

    #@ad
    .line 989
    .local v38, resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@b0
    .line 990
    move-object/from16 v0, p3

    #@b2
    move/from16 v1, v38

    #@b4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@b7
    .line 991
    const/4 v4, 0x1

    #@b8
    goto/16 :goto_7

    #@ba
    .line 994
    .end local v18           #mountPoint:Ljava/lang/String;
    .end local v38           #resultCode:I
    :sswitch_ba
    const-string v4, "IMountService"

    #@bc
    move-object/from16 v0, p2

    #@be
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c1
    .line 996
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c4
    move-result-object v18

    #@c5
    .line 997
    .restart local v18       #mountPoint:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@c8
    move-result v4

    #@c9
    if-eqz v4, :cond_e6

    #@cb
    const/16 v27, 0x1

    #@cd
    .line 998
    .local v27, force:Z
    :goto_cd
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@d0
    move-result v4

    #@d1
    if-eqz v4, :cond_e9

    #@d3
    const/16 v36, 0x1

    #@d5
    .line 999
    .local v36, removeEncrypt:Z
    :goto_d5
    move-object/from16 v0, p0

    #@d7
    move-object/from16 v1, v18

    #@d9
    move/from16 v2, v27

    #@db
    move/from16 v3, v36

    #@dd
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/storage/IMountService$Stub;->unmountVolume(Ljava/lang/String;ZZ)V

    #@e0
    .line 1000
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@e3
    .line 1001
    const/4 v4, 0x1

    #@e4
    goto/16 :goto_7

    #@e6
    .line 997
    .end local v27           #force:Z
    .end local v36           #removeEncrypt:Z
    :cond_e6
    const/16 v27, 0x0

    #@e8
    goto :goto_cd

    #@e9
    .line 998
    .restart local v27       #force:Z
    :cond_e9
    const/16 v36, 0x0

    #@eb
    goto :goto_d5

    #@ec
    .line 1004
    .end local v18           #mountPoint:Ljava/lang/String;
    .end local v27           #force:Z
    :sswitch_ec
    const-string v4, "IMountService"

    #@ee
    move-object/from16 v0, p2

    #@f0
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f3
    .line 1006
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f6
    move-result-object v18

    #@f7
    .line 1007
    .restart local v18       #mountPoint:Ljava/lang/String;
    move-object/from16 v0, p0

    #@f9
    move-object/from16 v1, v18

    #@fb
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->formatVolume(Ljava/lang/String;)I

    #@fe
    move-result v37

    #@ff
    .line 1008
    .local v37, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@102
    .line 1009
    move-object/from16 v0, p3

    #@104
    move/from16 v1, v37

    #@106
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@109
    .line 1010
    const/4 v4, 0x1

    #@10a
    goto/16 :goto_7

    #@10c
    .line 1013
    .end local v18           #mountPoint:Ljava/lang/String;
    .end local v37           #result:I
    :sswitch_10c
    const-string v4, "IMountService"

    #@10e
    move-object/from16 v0, p2

    #@110
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@113
    .line 1015
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@116
    move-result-object v34

    #@117
    .line 1016
    .local v34, path:Ljava/lang/String;
    move-object/from16 v0, p0

    #@119
    move-object/from16 v1, v34

    #@11b
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->getStorageUsers(Ljava/lang/String;)[I

    #@11e
    move-result-object v35

    #@11f
    .line 1017
    .local v35, pids:[I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    .line 1018
    move-object/from16 v0, p3

    #@124
    move-object/from16 v1, v35

    #@126
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@129
    .line 1019
    const/4 v4, 0x1

    #@12a
    goto/16 :goto_7

    #@12c
    .line 1022
    .end local v34           #path:Ljava/lang/String;
    .end local v35           #pids:[I
    :sswitch_12c
    const-string v4, "IMountService"

    #@12e
    move-object/from16 v0, p2

    #@130
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@133
    .line 1024
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@136
    move-result-object v18

    #@137
    .line 1025
    .restart local v18       #mountPoint:Ljava/lang/String;
    move-object/from16 v0, p0

    #@139
    move-object/from16 v1, v18

    #@13b
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@13e
    move-result-object v39

    #@13f
    .line 1026
    .local v39, state:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@142
    .line 1027
    move-object/from16 v0, p3

    #@144
    move-object/from16 v1, v39

    #@146
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@149
    .line 1028
    const/4 v4, 0x1

    #@14a
    goto/16 :goto_7

    #@14c
    .line 1031
    .end local v18           #mountPoint:Ljava/lang/String;
    .end local v39           #state:Ljava/lang/String;
    :sswitch_14c
    const-string v4, "IMountService"

    #@14e
    move-object/from16 v0, p2

    #@150
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@153
    .line 1033
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@156
    move-result-object v5

    #@157
    .line 1035
    .local v5, id:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15a
    move-result v6

    #@15b
    .line 1037
    .local v6, sizeMb:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15e
    move-result-object v7

    #@15f
    .line 1039
    .local v7, fstype:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@162
    move-result-object v8

    #@163
    .line 1041
    .local v8, key:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@166
    move-result v9

    #@167
    .line 1043
    .local v9, ownerUid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@16a
    move-result v4

    #@16b
    if-eqz v4, :cond_181

    #@16d
    const/4 v10, 0x1

    #@16e
    .local v10, external:Z
    :goto_16e
    move-object/from16 v4, p0

    #@170
    .line 1044
    invoke-virtual/range {v4 .. v10}, Landroid/os/storage/IMountService$Stub;->createSecureContainer(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IZ)I

    #@173
    move-result v38

    #@174
    .line 1046
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@177
    .line 1047
    move-object/from16 v0, p3

    #@179
    move/from16 v1, v38

    #@17b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@17e
    .line 1048
    const/4 v4, 0x1

    #@17f
    goto/16 :goto_7

    #@181
    .line 1043
    .end local v10           #external:Z
    .end local v38           #resultCode:I
    :cond_181
    const/4 v10, 0x0

    #@182
    goto :goto_16e

    #@183
    .line 1051
    .end local v5           #id:Ljava/lang/String;
    .end local v6           #sizeMb:I
    .end local v7           #fstype:Ljava/lang/String;
    .end local v8           #key:Ljava/lang/String;
    .end local v9           #ownerUid:I
    :sswitch_183
    const-string v4, "IMountService"

    #@185
    move-object/from16 v0, p2

    #@187
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18a
    .line 1053
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18d
    move-result-object v5

    #@18e
    .line 1054
    .restart local v5       #id:Ljava/lang/String;
    move-object/from16 v0, p0

    #@190
    invoke-virtual {v0, v5}, Landroid/os/storage/IMountService$Stub;->finalizeSecureContainer(Ljava/lang/String;)I

    #@193
    move-result v38

    #@194
    .line 1055
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@197
    .line 1056
    move-object/from16 v0, p3

    #@199
    move/from16 v1, v38

    #@19b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@19e
    .line 1057
    const/4 v4, 0x1

    #@19f
    goto/16 :goto_7

    #@1a1
    .line 1060
    .end local v5           #id:Ljava/lang/String;
    .end local v38           #resultCode:I
    :sswitch_1a1
    const-string v4, "IMountService"

    #@1a3
    move-object/from16 v0, p2

    #@1a5
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a8
    .line 1062
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1ab
    move-result-object v5

    #@1ac
    .line 1064
    .restart local v5       #id:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1af
    move-result v4

    #@1b0
    if-eqz v4, :cond_1c9

    #@1b2
    const/16 v27, 0x1

    #@1b4
    .line 1065
    .restart local v27       #force:Z
    :goto_1b4
    move-object/from16 v0, p0

    #@1b6
    move/from16 v1, v27

    #@1b8
    invoke-virtual {v0, v5, v1}, Landroid/os/storage/IMountService$Stub;->destroySecureContainer(Ljava/lang/String;Z)I

    #@1bb
    move-result v38

    #@1bc
    .line 1066
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bf
    .line 1067
    move-object/from16 v0, p3

    #@1c1
    move/from16 v1, v38

    #@1c3
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1c6
    .line 1068
    const/4 v4, 0x1

    #@1c7
    goto/16 :goto_7

    #@1c9
    .line 1064
    .end local v27           #force:Z
    .end local v38           #resultCode:I
    :cond_1c9
    const/16 v27, 0x0

    #@1cb
    goto :goto_1b4

    #@1cc
    .line 1071
    .end local v5           #id:Ljava/lang/String;
    :sswitch_1cc
    const-string v4, "IMountService"

    #@1ce
    move-object/from16 v0, p2

    #@1d0
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d3
    .line 1073
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d6
    move-result-object v5

    #@1d7
    .line 1075
    .restart local v5       #id:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1da
    move-result-object v8

    #@1db
    .line 1077
    .restart local v8       #key:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1de
    move-result v9

    #@1df
    .line 1078
    .restart local v9       #ownerUid:I
    move-object/from16 v0, p0

    #@1e1
    invoke-virtual {v0, v5, v8, v9}, Landroid/os/storage/IMountService$Stub;->mountSecureContainer(Ljava/lang/String;Ljava/lang/String;I)I

    #@1e4
    move-result v38

    #@1e5
    .line 1079
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e8
    .line 1080
    move-object/from16 v0, p3

    #@1ea
    move/from16 v1, v38

    #@1ec
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1ef
    .line 1081
    const/4 v4, 0x1

    #@1f0
    goto/16 :goto_7

    #@1f2
    .line 1084
    .end local v5           #id:Ljava/lang/String;
    .end local v8           #key:Ljava/lang/String;
    .end local v9           #ownerUid:I
    .end local v38           #resultCode:I
    :sswitch_1f2
    const-string v4, "IMountService"

    #@1f4
    move-object/from16 v0, p2

    #@1f6
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f9
    .line 1086
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1fc
    move-result-object v5

    #@1fd
    .line 1088
    .restart local v5       #id:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@200
    move-result v4

    #@201
    if-eqz v4, :cond_21a

    #@203
    const/16 v27, 0x1

    #@205
    .line 1089
    .restart local v27       #force:Z
    :goto_205
    move-object/from16 v0, p0

    #@207
    move/from16 v1, v27

    #@209
    invoke-virtual {v0, v5, v1}, Landroid/os/storage/IMountService$Stub;->unmountSecureContainer(Ljava/lang/String;Z)I

    #@20c
    move-result v38

    #@20d
    .line 1090
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@210
    .line 1091
    move-object/from16 v0, p3

    #@212
    move/from16 v1, v38

    #@214
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@217
    .line 1092
    const/4 v4, 0x1

    #@218
    goto/16 :goto_7

    #@21a
    .line 1088
    .end local v27           #force:Z
    .end local v38           #resultCode:I
    :cond_21a
    const/16 v27, 0x0

    #@21c
    goto :goto_205

    #@21d
    .line 1095
    .end local v5           #id:Ljava/lang/String;
    :sswitch_21d
    const-string v4, "IMountService"

    #@21f
    move-object/from16 v0, p2

    #@221
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@224
    .line 1097
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@227
    move-result-object v5

    #@228
    .line 1098
    .restart local v5       #id:Ljava/lang/String;
    move-object/from16 v0, p0

    #@22a
    invoke-virtual {v0, v5}, Landroid/os/storage/IMountService$Stub;->isSecureContainerMounted(Ljava/lang/String;)Z

    #@22d
    move-result v40

    #@22e
    .line 1099
    .local v40, status:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@231
    .line 1100
    if-eqz v40, :cond_23c

    #@233
    const/4 v4, 0x1

    #@234
    :goto_234
    move-object/from16 v0, p3

    #@236
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@239
    .line 1101
    const/4 v4, 0x1

    #@23a
    goto/16 :goto_7

    #@23c
    .line 1100
    :cond_23c
    const/4 v4, 0x0

    #@23d
    goto :goto_234

    #@23e
    .line 1104
    .end local v5           #id:Ljava/lang/String;
    .end local v40           #status:Z
    :sswitch_23e
    const-string v4, "IMountService"

    #@240
    move-object/from16 v0, p2

    #@242
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@245
    .line 1106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@248
    move-result-object v33

    #@249
    .line 1108
    .local v33, oldId:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24c
    move-result-object v32

    #@24d
    .line 1109
    .local v32, newId:Ljava/lang/String;
    move-object/from16 v0, p0

    #@24f
    move-object/from16 v1, v33

    #@251
    move-object/from16 v2, v32

    #@253
    invoke-virtual {v0, v1, v2}, Landroid/os/storage/IMountService$Stub;->renameSecureContainer(Ljava/lang/String;Ljava/lang/String;)I

    #@256
    move-result v38

    #@257
    .line 1110
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@25a
    .line 1111
    move-object/from16 v0, p3

    #@25c
    move/from16 v1, v38

    #@25e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@261
    .line 1112
    const/4 v4, 0x1

    #@262
    goto/16 :goto_7

    #@264
    .line 1115
    .end local v32           #newId:Ljava/lang/String;
    .end local v33           #oldId:Ljava/lang/String;
    .end local v38           #resultCode:I
    :sswitch_264
    const-string v4, "IMountService"

    #@266
    move-object/from16 v0, p2

    #@268
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@26b
    .line 1117
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@26e
    move-result-object v5

    #@26f
    .line 1118
    .restart local v5       #id:Ljava/lang/String;
    move-object/from16 v0, p0

    #@271
    invoke-virtual {v0, v5}, Landroid/os/storage/IMountService$Stub;->getSecureContainerPath(Ljava/lang/String;)Ljava/lang/String;

    #@274
    move-result-object v34

    #@275
    .line 1119
    .restart local v34       #path:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@278
    .line 1120
    move-object/from16 v0, p3

    #@27a
    move-object/from16 v1, v34

    #@27c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@27f
    .line 1121
    const/4 v4, 0x1

    #@280
    goto/16 :goto_7

    #@282
    .line 1124
    .end local v5           #id:Ljava/lang/String;
    .end local v34           #path:Ljava/lang/String;
    :sswitch_282
    const-string v4, "IMountService"

    #@284
    move-object/from16 v0, p2

    #@286
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@289
    .line 1125
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->getSecureContainerList()[Ljava/lang/String;

    #@28c
    move-result-object v29

    #@28d
    .line 1126
    .local v29, ids:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@290
    .line 1127
    move-object/from16 v0, p3

    #@292
    move-object/from16 v1, v29

    #@294
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@297
    .line 1128
    const/4 v4, 0x1

    #@298
    goto/16 :goto_7

    #@29a
    .line 1131
    .end local v29           #ids:[Ljava/lang/String;
    :sswitch_29a
    const-string v4, "IMountService"

    #@29c
    move-object/from16 v0, p2

    #@29e
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a1
    .line 1133
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2a4
    move-result-object v4

    #@2a5
    invoke-static {v4}, Landroid/os/storage/IMountShutdownObserver$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountShutdownObserver;

    #@2a8
    move-result-object v15

    #@2a9
    .line 1135
    .local v15, observer:Landroid/os/storage/IMountShutdownObserver;
    move-object/from16 v0, p0

    #@2ab
    invoke-virtual {v0, v15}, Landroid/os/storage/IMountService$Stub;->shutdown(Landroid/os/storage/IMountShutdownObserver;)V

    #@2ae
    .line 1136
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b1
    .line 1137
    const/4 v4, 0x1

    #@2b2
    goto/16 :goto_7

    #@2b4
    .line 1140
    .end local v15           #observer:Landroid/os/storage/IMountShutdownObserver;
    :sswitch_2b4
    const-string v4, "IMountService"

    #@2b6
    move-object/from16 v0, p2

    #@2b8
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2bb
    .line 1141
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->finishMediaUpdate()V

    #@2be
    .line 1142
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c1
    .line 1143
    const/4 v4, 0x1

    #@2c2
    goto/16 :goto_7

    #@2c4
    .line 1146
    :sswitch_2c4
    const-string v4, "IMountService"

    #@2c6
    move-object/from16 v0, p2

    #@2c8
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2cb
    .line 1147
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2ce
    move-result-object v12

    #@2cf
    .line 1148
    .local v12, rawPath:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d2
    move-result-object v13

    #@2d3
    .line 1149
    .local v13, canonicalPath:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d6
    move-result-object v8

    #@2d7
    .line 1151
    .restart local v8       #key:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@2da
    move-result-object v4

    #@2db
    invoke-static {v4}, Landroid/os/storage/IObbActionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IObbActionListener;

    #@2de
    move-result-object v15

    #@2df
    .line 1153
    .local v15, observer:Landroid/os/storage/IObbActionListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2e2
    move-result v16

    #@2e3
    .local v16, nonce:I
    move-object/from16 v11, p0

    #@2e5
    move-object v14, v8

    #@2e6
    .line 1154
    invoke-virtual/range {v11 .. v16}, Landroid/os/storage/IMountService$Stub;->mountObb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/IObbActionListener;I)V

    #@2e9
    .line 1155
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ec
    .line 1156
    const/4 v4, 0x1

    #@2ed
    goto/16 :goto_7

    #@2ef
    .line 1159
    .end local v8           #key:Ljava/lang/String;
    .end local v12           #rawPath:Ljava/lang/String;
    .end local v13           #canonicalPath:Ljava/lang/String;
    .end local v15           #observer:Landroid/os/storage/IObbActionListener;
    .end local v16           #nonce:I
    :sswitch_2ef
    const-string v4, "IMountService"

    #@2f1
    move-object/from16 v0, p2

    #@2f3
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f6
    .line 1161
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2f9
    move-result-object v26

    #@2fa
    .line 1163
    .local v26, filename:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2fd
    move-result v4

    #@2fe
    if-eqz v4, :cond_31f

    #@300
    const/16 v27, 0x1

    #@302
    .line 1165
    .restart local v27       #force:Z
    :goto_302
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@305
    move-result-object v4

    #@306
    invoke-static {v4}, Landroid/os/storage/IObbActionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IObbActionListener;

    #@309
    move-result-object v15

    #@30a
    .line 1167
    .restart local v15       #observer:Landroid/os/storage/IObbActionListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@30d
    move-result v16

    #@30e
    .line 1168
    .restart local v16       #nonce:I
    move-object/from16 v0, p0

    #@310
    move-object/from16 v1, v26

    #@312
    move/from16 v2, v27

    #@314
    move/from16 v3, v16

    #@316
    invoke-virtual {v0, v1, v2, v15, v3}, Landroid/os/storage/IMountService$Stub;->unmountObb(Ljava/lang/String;ZLandroid/os/storage/IObbActionListener;I)V

    #@319
    .line 1169
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@31c
    .line 1170
    const/4 v4, 0x1

    #@31d
    goto/16 :goto_7

    #@31f
    .line 1163
    .end local v15           #observer:Landroid/os/storage/IObbActionListener;
    .end local v16           #nonce:I
    .end local v27           #force:Z
    :cond_31f
    const/16 v27, 0x0

    #@321
    goto :goto_302

    #@322
    .line 1173
    .end local v26           #filename:Ljava/lang/String;
    :sswitch_322
    const-string v4, "IMountService"

    #@324
    move-object/from16 v0, p2

    #@326
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@329
    .line 1175
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@32c
    move-result-object v26

    #@32d
    .line 1176
    .restart local v26       #filename:Ljava/lang/String;
    move-object/from16 v0, p0

    #@32f
    move-object/from16 v1, v26

    #@331
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->isObbMounted(Ljava/lang/String;)Z

    #@334
    move-result v40

    #@335
    .line 1177
    .restart local v40       #status:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@338
    .line 1178
    if-eqz v40, :cond_343

    #@33a
    const/4 v4, 0x1

    #@33b
    :goto_33b
    move-object/from16 v0, p3

    #@33d
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@340
    .line 1179
    const/4 v4, 0x1

    #@341
    goto/16 :goto_7

    #@343
    .line 1178
    :cond_343
    const/4 v4, 0x0

    #@344
    goto :goto_33b

    #@345
    .line 1182
    .end local v26           #filename:Ljava/lang/String;
    .end local v40           #status:Z
    :sswitch_345
    const-string v4, "IMountService"

    #@347
    move-object/from16 v0, p2

    #@349
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34c
    .line 1184
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34f
    move-result-object v26

    #@350
    .line 1185
    .restart local v26       #filename:Ljava/lang/String;
    move-object/from16 v0, p0

    #@352
    move-object/from16 v1, v26

    #@354
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->getMountedObbPath(Ljava/lang/String;)Ljava/lang/String;

    #@357
    move-result-object v31

    #@358
    .line 1186
    .local v31, mountedPath:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@35b
    .line 1187
    move-object/from16 v0, p3

    #@35d
    move-object/from16 v1, v31

    #@35f
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@362
    .line 1188
    const/4 v4, 0x1

    #@363
    goto/16 :goto_7

    #@365
    .line 1191
    .end local v26           #filename:Ljava/lang/String;
    .end local v31           #mountedPath:Ljava/lang/String;
    :sswitch_365
    const-string v4, "IMountService"

    #@367
    move-object/from16 v0, p2

    #@369
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36c
    .line 1192
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->isExternalStorageEmulated()Z

    #@36f
    move-result v24

    #@370
    .line 1193
    .local v24, emulated:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@373
    .line 1194
    if-eqz v24, :cond_37e

    #@375
    const/4 v4, 0x1

    #@376
    :goto_376
    move-object/from16 v0, p3

    #@378
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@37b
    .line 1195
    const/4 v4, 0x1

    #@37c
    goto/16 :goto_7

    #@37e
    .line 1194
    :cond_37e
    const/4 v4, 0x0

    #@37f
    goto :goto_376

    #@380
    .line 1198
    .end local v24           #emulated:Z
    :sswitch_380
    const-string v4, "IMountService"

    #@382
    move-object/from16 v0, p2

    #@384
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@387
    .line 1199
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@38a
    move-result-object v23

    #@38b
    .line 1200
    .local v23, password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@38d
    move-object/from16 v1, v23

    #@38f
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->decryptStorage(Ljava/lang/String;)I

    #@392
    move-result v37

    #@393
    .line 1201
    .restart local v37       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@396
    .line 1202
    move-object/from16 v0, p3

    #@398
    move/from16 v1, v37

    #@39a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39d
    .line 1203
    const/4 v4, 0x1

    #@39e
    goto/16 :goto_7

    #@3a0
    .line 1206
    .end local v23           #password:Ljava/lang/String;
    .end local v37           #result:I
    :sswitch_3a0
    const-string v4, "IMountService"

    #@3a2
    move-object/from16 v0, p2

    #@3a4
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3a7
    .line 1207
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3aa
    move-result-object v23

    #@3ab
    .line 1208
    .restart local v23       #password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3ad
    move-object/from16 v1, v23

    #@3af
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->encryptStorage(Ljava/lang/String;)I

    #@3b2
    move-result v37

    #@3b3
    .line 1209
    .restart local v37       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b6
    .line 1210
    move-object/from16 v0, p3

    #@3b8
    move/from16 v1, v37

    #@3ba
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3bd
    .line 1211
    const/4 v4, 0x1

    #@3be
    goto/16 :goto_7

    #@3c0
    .line 1215
    .end local v23           #password:Ljava/lang/String;
    .end local v37           #result:I
    :sswitch_3c0
    const-string v4, "IMountService"

    #@3c2
    move-object/from16 v0, p2

    #@3c4
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c7
    .line 1216
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ca
    move-result-object v23

    #@3cb
    .line 1217
    .restart local v23       #password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3cd
    move-object/from16 v1, v23

    #@3cf
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->unencryptStorage(Ljava/lang/String;)I

    #@3d2
    move-result v37

    #@3d3
    .line 1218
    .restart local v37       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3d6
    .line 1219
    move-object/from16 v0, p3

    #@3d8
    move/from16 v1, v37

    #@3da
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3dd
    .line 1220
    const/4 v4, 0x1

    #@3de
    goto/16 :goto_7

    #@3e0
    .line 1224
    .end local v23           #password:Ljava/lang/String;
    .end local v37           #result:I
    :sswitch_3e0
    const-string v4, "IMountService"

    #@3e2
    move-object/from16 v0, p2

    #@3e4
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3e7
    .line 1225
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ea
    move-result-object v23

    #@3eb
    .line 1226
    .restart local v23       #password:Ljava/lang/String;
    move-object/from16 v0, p0

    #@3ed
    move-object/from16 v1, v23

    #@3ef
    invoke-virtual {v0, v1}, Landroid/os/storage/IMountService$Stub;->changeEncryptionPassword(Ljava/lang/String;)I

    #@3f2
    move-result v37

    #@3f3
    .line 1227
    .restart local v37       #result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f6
    .line 1228
    move-object/from16 v0, p3

    #@3f8
    move/from16 v1, v37

    #@3fa
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3fd
    .line 1229
    const/4 v4, 0x1

    #@3fe
    goto/16 :goto_7

    #@400
    .line 1232
    .end local v23           #password:Ljava/lang/String;
    .end local v37           #result:I
    :sswitch_400
    const-string v4, "IMountService"

    #@402
    move-object/from16 v0, p2

    #@404
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@407
    .line 1233
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@40a
    move-result-object v37

    #@40b
    .line 1234
    .local v37, result:[Landroid/os/storage/StorageVolume;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@40e
    .line 1235
    const/4 v4, 0x1

    #@40f
    move-object/from16 v0, p3

    #@411
    move-object/from16 v1, v37

    #@413
    invoke-virtual {v0, v1, v4}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@416
    .line 1236
    const/4 v4, 0x1

    #@417
    goto/16 :goto_7

    #@419
    .line 1239
    .end local v37           #result:[Landroid/os/storage/StorageVolume;
    :sswitch_419
    const-string v4, "IMountService"

    #@41b
    move-object/from16 v0, p2

    #@41d
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@420
    .line 1241
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@423
    move-result-object v5

    #@424
    .line 1242
    .restart local v5       #id:Ljava/lang/String;
    move-object/from16 v0, p0

    #@426
    invoke-virtual {v0, v5}, Landroid/os/storage/IMountService$Stub;->getSecureContainerFilesystemPath(Ljava/lang/String;)Ljava/lang/String;

    #@429
    move-result-object v34

    #@42a
    .line 1243
    .restart local v34       #path:Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@42d
    .line 1244
    move-object/from16 v0, p3

    #@42f
    move-object/from16 v1, v34

    #@431
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@434
    .line 1245
    const/4 v4, 0x1

    #@435
    goto/16 :goto_7

    #@437
    .line 1248
    .end local v5           #id:Ljava/lang/String;
    .end local v34           #path:Ljava/lang/String;
    :sswitch_437
    const-string v4, "IMountService"

    #@439
    move-object/from16 v0, p2

    #@43b
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43e
    .line 1249
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->getEncryptionState()I

    #@441
    move-result v37

    #@442
    .line 1250
    .local v37, result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@445
    .line 1251
    move-object/from16 v0, p3

    #@447
    move/from16 v1, v37

    #@449
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@44c
    .line 1252
    const/4 v4, 0x1

    #@44d
    goto/16 :goto_7

    #@44f
    .line 1255
    .end local v37           #result:I
    :sswitch_44f
    const-string v4, "IMountService"

    #@451
    move-object/from16 v0, p2

    #@453
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@456
    .line 1257
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@459
    move-result-object v5

    #@45a
    .line 1259
    .restart local v5       #id:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@45d
    move-result v28

    #@45e
    .line 1261
    .local v28, gid:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@461
    move-result-object v26

    #@462
    .line 1262
    .restart local v26       #filename:Ljava/lang/String;
    move-object/from16 v0, p0

    #@464
    move/from16 v1, v28

    #@466
    move-object/from16 v2, v26

    #@468
    invoke-virtual {v0, v5, v1, v2}, Landroid/os/storage/IMountService$Stub;->fixPermissionsSecureContainer(Ljava/lang/String;ILjava/lang/String;)I

    #@46b
    move-result v38

    #@46c
    .line 1263
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@46f
    .line 1264
    move-object/from16 v0, p3

    #@471
    move/from16 v1, v38

    #@473
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@476
    .line 1265
    const/4 v4, 0x1

    #@477
    goto/16 :goto_7

    #@479
    .line 1270
    .end local v5           #id:Ljava/lang/String;
    .end local v26           #filename:Ljava/lang/String;
    .end local v28           #gid:I
    .end local v38           #resultCode:I
    :sswitch_479
    const-string v4, "IMountService"

    #@47b
    move-object/from16 v0, p2

    #@47d
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@480
    .line 1271
    invoke-virtual/range {p0 .. p0}, Landroid/os/storage/IMountService$Stub;->getBackupSecureContainerList()[Ljava/lang/String;

    #@483
    move-result-object v29

    #@484
    .line 1272
    .restart local v29       #ids:[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@487
    .line 1273
    move-object/from16 v0, p3

    #@489
    move-object/from16 v1, v29

    #@48b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@48e
    .line 1274
    const/4 v4, 0x1

    #@48f
    goto/16 :goto_7

    #@491
    .line 1279
    .end local v29           #ids:[Ljava/lang/String;
    :sswitch_491
    const-string v4, "IMountService"

    #@493
    move-object/from16 v0, p2

    #@495
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@498
    .line 1286
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@49b
    move-result-object v18

    #@49c
    .line 1287
    .restart local v18       #mountPoint:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@49f
    move-result-object v19

    #@4a0
    .line 1288
    .local v19, type:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a3
    move-result-object v20

    #@4a4
    .line 1289
    .local v20, ip:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a7
    move-result-object v21

    #@4a8
    .line 1290
    .local v21, remotepath:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4ab
    move-result-object v22

    #@4ac
    .line 1291
    .local v22, userid:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4af
    move-result-object v23

    #@4b0
    .restart local v23       #password:Ljava/lang/String;
    move-object/from16 v17, p0

    #@4b2
    .line 1292
    invoke-virtual/range {v17 .. v23}, Landroid/os/storage/IMountService$Stub;->mountNetStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@4b5
    move-result v38

    #@4b6
    .line 1293
    .restart local v38       #resultCode:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b9
    .line 1294
    move-object/from16 v0, p3

    #@4bb
    move/from16 v1, v38

    #@4bd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4c0
    .line 1295
    const/4 v4, 0x1

    #@4c1
    goto/16 :goto_7

    #@4c3
    .line 1298
    .end local v18           #mountPoint:Ljava/lang/String;
    .end local v19           #type:Ljava/lang/String;
    .end local v20           #ip:Ljava/lang/String;
    .end local v21           #remotepath:Ljava/lang/String;
    .end local v22           #userid:Ljava/lang/String;
    .end local v23           #password:Ljava/lang/String;
    .end local v38           #resultCode:I
    :sswitch_4c3
    const-string v4, "IMountService"

    #@4c5
    move-object/from16 v0, p2

    #@4c7
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ca
    .line 1300
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4cd
    move-result-object v18

    #@4ce
    .line 1302
    .restart local v18       #mountPoint:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4d1
    move-result v4

    #@4d2
    if-eqz v4, :cond_4e5

    #@4d4
    const/16 v27, 0x1

    #@4d6
    .line 1303
    .restart local v27       #force:Z
    :goto_4d6
    move-object/from16 v0, p0

    #@4d8
    move-object/from16 v1, v18

    #@4da
    move/from16 v2, v27

    #@4dc
    invoke-virtual {v0, v1, v2}, Landroid/os/storage/IMountService$Stub;->unmountNetStorage(Ljava/lang/String;Z)I

    #@4df
    .line 1304
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e2
    .line 1305
    const/4 v4, 0x1

    #@4e3
    goto/16 :goto_7

    #@4e5
    .line 1302
    .end local v27           #force:Z
    :cond_4e5
    const/16 v27, 0x0

    #@4e7
    goto :goto_4d6

    #@4e8
    .line 941
    :sswitch_data_4e8
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_47
        0x4 -> :sswitch_61
        0x5 -> :sswitch_7f
        0x6 -> :sswitch_9a
        0x7 -> :sswitch_ba
        0x8 -> :sswitch_ec
        0x9 -> :sswitch_10c
        0xa -> :sswitch_12c
        0xb -> :sswitch_14c
        0xc -> :sswitch_183
        0xd -> :sswitch_1a1
        0xe -> :sswitch_1cc
        0xf -> :sswitch_1f2
        0x10 -> :sswitch_21d
        0x11 -> :sswitch_23e
        0x12 -> :sswitch_264
        0x13 -> :sswitch_282
        0x14 -> :sswitch_29a
        0x15 -> :sswitch_2b4
        0x16 -> :sswitch_2c4
        0x17 -> :sswitch_2ef
        0x18 -> :sswitch_322
        0x19 -> :sswitch_345
        0x1a -> :sswitch_365
        0x1b -> :sswitch_380
        0x1c -> :sswitch_3a0
        0x1d -> :sswitch_3e0
        0x1e -> :sswitch_400
        0x1f -> :sswitch_419
        0x20 -> :sswitch_437
        0x22 -> :sswitch_44f
        0x23 -> :sswitch_479
        0x24 -> :sswitch_491
        0x25 -> :sswitch_4c3
        0x26 -> :sswitch_3c0
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
