.class public Landroid/os/storage/StorageManager;
.super Ljava/lang/Object;
.source "StorageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/storage/StorageManager$1;,
        Landroid/os/storage/StorageManager$ListenerDelegate;,
        Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;,
        Landroid/os/storage/StorageManager$UmsConnectionChangedStorageEvent;,
        Landroid/os/storage/StorageManager$StorageEvent;,
        Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;,
        Landroid/os/storage/StorageManager$ObbListenerDelegate;,
        Landroid/os/storage/StorageManager$ObbActionListener;,
        Landroid/os/storage/StorageManager$MountServiceBinderListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "StorageManager"


# instance fields
.field private mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/storage/StorageManager$ListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final mMountService:Landroid/os/storage/IMountService;

.field private final mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

.field mTgtLooper:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .registers 4
    .parameter "tgtLooper"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 311
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@a
    .line 85
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    #@c
    const/4 v1, 0x0

    #@d
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    #@10
    iput-object v0, p0, Landroid/os/storage/StorageManager;->mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

    #@12
    .line 106
    new-instance v0, Landroid/os/storage/StorageManager$ObbActionListener;

    #@14
    const/4 v1, 0x0

    #@15
    invoke-direct {v0, p0, v1}, Landroid/os/storage/StorageManager$ObbActionListener;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/StorageManager$1;)V

    #@18
    iput-object v0, p0, Landroid/os/storage/StorageManager;->mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

    #@1a
    .line 312
    const-string/jumbo v0, "mount"

    #@1d
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@20
    move-result-object v0

    #@21
    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@27
    .line 313
    iget-object v0, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@29
    if-nez v0, :cond_33

    #@2b
    .line 314
    const-string v0, "StorageManager"

    #@2d
    const-string v1, "Unable to connect to mount service! - is it running yet?"

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 318
    :goto_32
    return-void

    #@33
    .line 317
    :cond_33
    iput-object p1, p0, Landroid/os/storage/StorageManager;->mTgtLooper:Landroid/os/Looper;

    #@35
    goto :goto_32
.end method

.method static synthetic access$000(Landroid/os/storage/StorageManager;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Landroid/os/storage/StorageManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Landroid/os/storage/StorageManager;->getNextNonce()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static from(Landroid/content/Context;)Landroid/os/storage/StorageManager;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 296
    const-string/jumbo v0, "storage"

    #@3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/os/storage/StorageManager;

    #@9
    return-object v0
.end method

.method private getNextNonce()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget-object v0, p0, Landroid/os/storage/StorageManager;->mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

    #@2
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static getPrimaryVolume([Landroid/os/storage/StorageVolume;)Landroid/os/storage/StorageVolume;
    .registers 7
    .parameter "volumes"

    #@0
    .prologue
    .line 605
    move-object v0, p0

    #@1
    .local v0, arr$:[Landroid/os/storage/StorageVolume;
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_11

    #@5
    aget-object v3, v0, v1

    #@7
    .line 606
    .local v3, volume:Landroid/os/storage/StorageVolume;
    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->isPrimary()Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_e

    #@d
    .line 611
    .end local v3           #volume:Landroid/os/storage/StorageVolume;
    :goto_d
    return-object v3

    #@e
    .line 605
    .restart local v3       #volume:Landroid/os/storage/StorageVolume;
    :cond_e
    add-int/lit8 v1, v1, 0x1

    #@10
    goto :goto_3

    #@11
    .line 610
    .end local v3           #volume:Landroid/os/storage/StorageVolume;
    :cond_11
    const-string v4, "StorageManager"

    #@13
    const-string v5, "No primary storage defined"

    #@15
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 611
    const/4 v3, 0x0

    #@19
    goto :goto_d
.end method


# virtual methods
.method public disableUsbMassStorage()V
    .registers 4

    #@0
    .prologue
    .line 399
    :try_start_0
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-interface {v1, v2}, Landroid/os/storage/IMountService;->setUsbMassStorageEnabled(Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6} :catch_7

    #@6
    .line 403
    :goto_6
    return-void

    #@7
    .line 400
    :catch_7
    move-exception v0

    #@8
    .line 401
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "StorageManager"

    #@a
    const-string v2, "Failed to disable UMS"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    goto :goto_6
.end method

.method public enableUsbMassStorage()V
    .registers 4

    #@0
    .prologue
    .line 386
    :try_start_0
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-interface {v1, v2}, Landroid/os/storage/IMountService;->setUsbMassStorageEnabled(Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6} :catch_7

    #@6
    .line 390
    :goto_6
    return-void

    #@7
    .line 387
    :catch_7
    move-exception v0

    #@8
    .line 388
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "StorageManager"

    #@a
    const-string v2, "Failed to enable UMS"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    goto :goto_6
.end method

.method public getMountedObbPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "rawPath"

    #@0
    .prologue
    .line 537
    const-string/jumbo v1, "rawPath cannot be null"

    #@3
    invoke-static {p1, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 540
    :try_start_6
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@8
    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getMountedObbPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result-object v1

    #@c
    .line 545
    :goto_c
    return-object v1

    #@d
    .line 541
    :catch_d
    move-exception v0

    #@e
    .line 542
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "StorageManager"

    #@10
    const-string v2, "Failed to find mounted path for OBB"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 545
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public getPrimaryVolume()Landroid/os/storage/StorageVolume;
    .registers 2

    #@0
    .prologue
    .line 600
    invoke-virtual {p0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Landroid/os/storage/StorageManager;->getPrimaryVolume([Landroid/os/storage/StorageVolume;)Landroid/os/storage/StorageVolume;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getVolumeList()[Landroid/os/storage/StorageVolume;
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 567
    iget-object v5, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@3
    if-nez v5, :cond_8

    #@5
    new-array v4, v6, [Landroid/os/storage/StorageVolume;

    #@7
    .line 579
    :cond_7
    :goto_7
    return-object v4

    #@8
    .line 569
    :cond_8
    :try_start_8
    iget-object v5, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@a
    invoke-interface {v5}, Landroid/os/storage/IMountService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@d
    move-result-object v3

    #@e
    .line 570
    .local v3, list:[Landroid/os/Parcelable;
    if-nez v3, :cond_14

    #@10
    const/4 v5, 0x0

    #@11
    new-array v4, v5, [Landroid/os/storage/StorageVolume;

    #@13
    goto :goto_7

    #@14
    .line 571
    :cond_14
    array-length v2, v3

    #@15
    .line 572
    .local v2, length:I
    new-array v4, v2, [Landroid/os/storage/StorageVolume;

    #@17
    .line 573
    .local v4, result:[Landroid/os/storage/StorageVolume;
    const/4 v1, 0x0

    #@18
    .local v1, i:I
    :goto_18
    if-ge v1, v2, :cond_7

    #@1a
    .line 574
    aget-object v5, v3, v1

    #@1c
    check-cast v5, Landroid/os/storage/StorageVolume;

    #@1e
    aput-object v5, v4, v1
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_20} :catch_23

    #@20
    .line 573
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_18

    #@23
    .line 577
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #list:[Landroid/os/Parcelable;
    .end local v4           #result:[Landroid/os/storage/StorageVolume;
    :catch_23
    move-exception v0

    #@24
    .line 578
    .local v0, e:Landroid/os/RemoteException;
    const-string v5, "StorageManager"

    #@26
    const-string v6, "Failed to get volume list"

    #@28
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    .line 579
    const/4 v4, 0x0

    #@2c
    goto :goto_7
.end method

.method public getVolumePaths()[Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 588
    invoke-virtual {p0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@3
    move-result-object v3

    #@4
    .line 589
    .local v3, volumes:[Landroid/os/storage/StorageVolume;
    if-nez v3, :cond_8

    #@6
    const/4 v2, 0x0

    #@7
    .line 595
    :cond_7
    return-object v2

    #@8
    .line 590
    :cond_8
    array-length v0, v3

    #@9
    .line 591
    .local v0, count:I
    new-array v2, v0, [Ljava/lang/String;

    #@b
    .line 592
    .local v2, paths:[Ljava/lang/String;
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    if-ge v1, v0, :cond_7

    #@e
    .line 593
    aget-object v4, v3, v1

    #@10
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    aput-object v4, v2, v1

    #@16
    .line 592
    add-int/lit8 v1, v1, 0x1

    #@18
    goto :goto_c
.end method

.method public getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "mountPoint"

    #@0
    .prologue
    .line 553
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@2
    if-nez v1, :cond_8

    #@4
    const-string/jumbo v1, "removed"

    #@7
    .line 558
    :goto_7
    return-object v1

    #@8
    .line 555
    :cond_8
    :try_start_8
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@a
    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_f

    #@d
    move-result-object v1

    #@e
    goto :goto_7

    #@f
    .line 556
    :catch_f
    move-exception v0

    #@10
    .line 557
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "StorageManager"

    #@12
    const-string v2, "Failed to get volume state"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 558
    const/4 v1, 0x0

    #@18
    goto :goto_7
.end method

.method public isObbMounted(Ljava/lang/String;)Z
    .registers 5
    .parameter "rawPath"

    #@0
    .prologue
    .line 516
    const-string/jumbo v1, "rawPath cannot be null"

    #@3
    invoke-static {p1, v1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 519
    :try_start_6
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@8
    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->isObbMounted(Ljava/lang/String;)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    #@b
    move-result v1

    #@c
    .line 524
    :goto_c
    return v1

    #@d
    .line 520
    :catch_d
    move-exception v0

    #@e
    .line 521
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "StorageManager"

    #@10
    const-string v2, "Failed to check if OBB is mounted"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 524
    const/4 v1, 0x0

    #@16
    goto :goto_c
.end method

.method public isUsbMassStorageConnected()Z
    .registers 4

    #@0
    .prologue
    .line 413
    :try_start_0
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@2
    invoke-interface {v1}, Landroid/os/storage/IMountService;->isUsbMassStorageConnected()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 417
    :goto_6
    return v1

    #@7
    .line 414
    :catch_7
    move-exception v0

    #@8
    .line 415
    .local v0, ex:Ljava/lang/Exception;
    const-string v1, "StorageManager"

    #@a
    const-string v2, "Failed to get UMS connection state"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 417
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public isUsbMassStorageEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 428
    :try_start_0
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@2
    invoke-interface {v1}, Landroid/os/storage/IMountService;->isUsbMassStorageEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 432
    :goto_6
    return v1

    #@7
    .line 429
    :catch_7
    move-exception v0

    #@8
    .line 430
    .local v0, rex:Landroid/os/RemoteException;
    const-string v1, "StorageManager"

    #@a
    const-string v2, "Failed to get UMS enable state"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 432
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public mountObb(Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/OnObbStateChangeListener;)Z
    .registers 11
    .parameter "rawPath"
    .parameter "key"
    .parameter "listener"

    #@0
    .prologue
    .line 457
    const-string/jumbo v0, "rawPath cannot be null"

    #@3
    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 458
    const-string/jumbo v0, "listener cannot be null"

    #@9
    invoke-static {p3, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 461
    :try_start_c
    new-instance v0, Ljava/io/File;

    #@e
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@11
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    .line 462
    .local v2, canonicalPath:Ljava/lang/String;
    iget-object v0, p0, Landroid/os/storage/StorageManager;->mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

    #@17
    invoke-virtual {v0, p3}, Landroid/os/storage/StorageManager$ObbActionListener;->addListener(Landroid/os/storage/OnObbStateChangeListener;)I

    #@1a
    move-result v5

    #@1b
    .line 463
    .local v5, nonce:I
    iget-object v0, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@1d
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

    #@1f
    move-object v1, p1

    #@20
    move-object v3, p2

    #@21
    invoke-interface/range {v0 .. v5}, Landroid/os/storage/IMountService;->mountObb(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/IObbActionListener;I)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_24} :catch_26
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_24} :catch_40

    #@24
    .line 464
    const/4 v0, 0x1

    #@25
    .line 471
    .end local v2           #canonicalPath:Ljava/lang/String;
    .end local v5           #nonce:I
    :goto_25
    return v0

    #@26
    .line 465
    :catch_26
    move-exception v6

    #@27
    .line 466
    .local v6, e:Ljava/io/IOException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@29
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "Failed to resolve path: "

    #@30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-direct {v0, v1, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3f
    throw v0

    #@40
    .line 467
    .end local v6           #e:Ljava/io/IOException;
    :catch_40
    move-exception v6

    #@41
    .line 468
    .local v6, e:Landroid/os/RemoteException;
    const-string v0, "StorageManager"

    #@43
    const-string v1, "Failed to mount OBB"

    #@45
    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    .line 471
    const/4 v0, 0x0

    #@49
    goto :goto_25
.end method

.method public registerListener(Landroid/os/storage/StorageEventListener;)V
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 329
    if-nez p1, :cond_3

    #@2
    .line 345
    :goto_2
    return-void

    #@3
    .line 333
    :cond_3
    iget-object v2, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@5
    monitor-enter v2

    #@6
    .line 334
    :try_start_6
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;
    :try_end_8
    .catchall {:try_start_6 .. :try_end_8} :catchall_25

    #@8
    if-nez v1, :cond_19

    #@a
    .line 336
    :try_start_a
    new-instance v1, Landroid/os/storage/StorageManager$MountServiceBinderListener;

    #@c
    const/4 v3, 0x0

    #@d
    invoke-direct {v1, p0, v3}, Landroid/os/storage/StorageManager$MountServiceBinderListener;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/StorageManager$1;)V

    #@10
    iput-object v1, p0, Landroid/os/storage/StorageManager;->mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;

    #@12
    .line 337
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@14
    iget-object v3, p0, Landroid/os/storage/StorageManager;->mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;

    #@16
    invoke-interface {v1, v3}, Landroid/os/storage/IMountService;->registerListener(Landroid/os/storage/IMountServiceListener;)V
    :try_end_19
    .catchall {:try_start_a .. :try_end_19} :catchall_25
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_19} :catch_28

    #@19
    .line 343
    :cond_19
    :try_start_19
    iget-object v1, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@1b
    new-instance v3, Landroid/os/storage/StorageManager$ListenerDelegate;

    #@1d
    invoke-direct {v3, p0, p1}, Landroid/os/storage/StorageManager$ListenerDelegate;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/StorageEventListener;)V

    #@20
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@23
    .line 344
    monitor-exit v2

    #@24
    goto :goto_2

    #@25
    :catchall_25
    move-exception v1

    #@26
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_19 .. :try_end_27} :catchall_25

    #@27
    throw v1

    #@28
    .line 338
    :catch_28
    move-exception v0

    #@29
    .line 339
    .local v0, rex:Landroid/os/RemoteException;
    :try_start_29
    const-string v1, "StorageManager"

    #@2b
    const-string v3, "Register mBinderListener failed"

    #@2d
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 340
    monitor-exit v2
    :try_end_31
    .catchall {:try_start_29 .. :try_end_31} :catchall_25

    #@31
    goto :goto_2
.end method

.method public unmountObb(Ljava/lang/String;ZLandroid/os/storage/OnObbStateChangeListener;)Z
    .registers 8
    .parameter "rawPath"
    .parameter "force"
    .parameter "listener"

    #@0
    .prologue
    .line 495
    const-string/jumbo v2, "rawPath cannot be null"

    #@3
    invoke-static {p1, v2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    .line 496
    const-string/jumbo v2, "listener cannot be null"

    #@9
    invoke-static {p3, v2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    .line 499
    :try_start_c
    iget-object v2, p0, Landroid/os/storage/StorageManager;->mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

    #@e
    invoke-virtual {v2, p3}, Landroid/os/storage/StorageManager$ObbActionListener;->addListener(Landroid/os/storage/OnObbStateChangeListener;)I

    #@11
    move-result v1

    #@12
    .line 500
    .local v1, nonce:I
    iget-object v2, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@14
    iget-object v3, p0, Landroid/os/storage/StorageManager;->mObbActionListener:Landroid/os/storage/StorageManager$ObbActionListener;

    #@16
    invoke-interface {v2, p1, p2, v3, v1}, Landroid/os/storage/IMountService;->unmountObb(Ljava/lang/String;ZLandroid/os/storage/IObbActionListener;I)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_19} :catch_1b

    #@19
    .line 501
    const/4 v2, 0x1

    #@1a
    .line 506
    .end local v1           #nonce:I
    :goto_1a
    return v2

    #@1b
    .line 502
    :catch_1b
    move-exception v0

    #@1c
    .line 503
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "StorageManager"

    #@1e
    const-string v3, "Failed to mount OBB"

    #@20
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    .line 506
    const/4 v2, 0x0

    #@24
    goto :goto_1a
.end method

.method public unregisterListener(Landroid/os/storage/StorageEventListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 355
    if-nez p1, :cond_3

    #@2
    .line 377
    :goto_2
    return-void

    #@3
    .line 359
    :cond_3
    iget-object v5, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@5
    monitor-enter v5

    #@6
    .line 360
    :try_start_6
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@8
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@b
    move-result v3

    #@c
    .line 361
    .local v3, size:I
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    if-ge v0, v3, :cond_22

    #@f
    .line 362
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@11
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/os/storage/StorageManager$ListenerDelegate;

    #@17
    .line 363
    .local v1, l:Landroid/os/storage/StorageManager$ListenerDelegate;
    invoke-virtual {v1}, Landroid/os/storage/StorageManager$ListenerDelegate;->getListener()Landroid/os/storage/StorageEventListener;

    #@1a
    move-result-object v4

    #@1b
    if-ne v4, p1, :cond_3a

    #@1d
    .line 364
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@1f
    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@22
    .line 368
    .end local v1           #l:Landroid/os/storage/StorageManager$ListenerDelegate;
    :cond_22
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mListeners:Ljava/util/List;

    #@24
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@27
    move-result v4

    #@28
    if-nez v4, :cond_35

    #@2a
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;
    :try_end_2c
    .catchall {:try_start_6 .. :try_end_2c} :catchall_37

    #@2c
    if-eqz v4, :cond_35

    #@2e
    .line 370
    :try_start_2e
    iget-object v4, p0, Landroid/os/storage/StorageManager;->mMountService:Landroid/os/storage/IMountService;

    #@30
    iget-object v6, p0, Landroid/os/storage/StorageManager;->mBinderListener:Landroid/os/storage/StorageManager$MountServiceBinderListener;

    #@32
    invoke-interface {v4, v6}, Landroid/os/storage/IMountService;->unregisterListener(Landroid/os/storage/IMountServiceListener;)V
    :try_end_35
    .catchall {:try_start_2e .. :try_end_35} :catchall_37
    .catch Landroid/os/RemoteException; {:try_start_2e .. :try_end_35} :catch_3d

    #@35
    .line 376
    :cond_35
    :try_start_35
    monitor-exit v5

    #@36
    goto :goto_2

    #@37
    .end local v0           #i:I
    .end local v3           #size:I
    :catchall_37
    move-exception v4

    #@38
    monitor-exit v5
    :try_end_39
    .catchall {:try_start_35 .. :try_end_39} :catchall_37

    #@39
    throw v4

    #@3a
    .line 361
    .restart local v0       #i:I
    .restart local v1       #l:Landroid/os/storage/StorageManager$ListenerDelegate;
    .restart local v3       #size:I
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_d

    #@3d
    .line 371
    .end local v1           #l:Landroid/os/storage/StorageManager$ListenerDelegate;
    :catch_3d
    move-exception v2

    #@3e
    .line 372
    .local v2, rex:Landroid/os/RemoteException;
    :try_start_3e
    const-string v4, "StorageManager"

    #@40
    const-string v6, "Unregister mBinderListener failed"

    #@42
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 373
    monitor-exit v5
    :try_end_46
    .catchall {:try_start_3e .. :try_end_46} :catchall_37

    #@46
    goto :goto_2
.end method
