.class Landroid/os/storage/StorageManager$ObbActionListener;
.super Landroid/os/storage/IObbActionListener$Stub;
.source "StorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/storage/StorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObbActionListener"
.end annotation


# instance fields
.field private mListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/storage/StorageManager$ObbListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/os/storage/StorageManager;


# direct methods
.method private constructor <init>(Landroid/os/storage/StorageManager;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 108
    iput-object p1, p0, Landroid/os/storage/StorageManager$ObbActionListener;->this$0:Landroid/os/storage/StorageManager;

    #@2
    invoke-direct {p0}, Landroid/os/storage/IObbActionListener$Stub;-><init>()V

    #@5
    .line 109
    new-instance v0, Landroid/util/SparseArray;

    #@7
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@a
    iput-object v0, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/storage/StorageManager;Landroid/os/storage/StorageManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 108
    invoke-direct {p0, p1}, Landroid/os/storage/StorageManager$ObbActionListener;-><init>(Landroid/os/storage/StorageManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public addListener(Landroid/os/storage/OnObbStateChangeListener;)I
    .registers 6
    .parameter "listener"

    #@0
    .prologue
    .line 128
    new-instance v0, Landroid/os/storage/StorageManager$ObbListenerDelegate;

    #@2
    iget-object v1, p0, Landroid/os/storage/StorageManager$ObbActionListener;->this$0:Landroid/os/storage/StorageManager;

    #@4
    invoke-direct {v0, v1, p1}, Landroid/os/storage/StorageManager$ObbListenerDelegate;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/OnObbStateChangeListener;)V

    #@7
    .line 130
    .local v0, delegate:Landroid/os/storage/StorageManager$ObbListenerDelegate;
    iget-object v2, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@9
    monitor-enter v2

    #@a
    .line 131
    :try_start_a
    iget-object v1, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@c
    invoke-static {v0}, Landroid/os/storage/StorageManager$ObbListenerDelegate;->access$200(Landroid/os/storage/StorageManager$ObbListenerDelegate;)I

    #@f
    move-result v3

    #@10
    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@13
    .line 132
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_a .. :try_end_14} :catchall_19

    #@14
    .line 134
    invoke-static {v0}, Landroid/os/storage/StorageManager$ObbListenerDelegate;->access$200(Landroid/os/storage/StorageManager$ObbListenerDelegate;)I

    #@17
    move-result v1

    #@18
    return v1

    #@19
    .line 132
    :catchall_19
    move-exception v1

    #@1a
    :try_start_1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method

.method public onObbResult(Ljava/lang/String;II)V
    .registers 7
    .parameter "filename"
    .parameter "nonce"
    .parameter "status"

    #@0
    .prologue
    .line 115
    iget-object v2, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@2
    monitor-enter v2

    #@3
    .line 116
    :try_start_3
    iget-object v1, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@5
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Landroid/os/storage/StorageManager$ObbListenerDelegate;

    #@b
    .line 117
    .local v0, delegate:Landroid/os/storage/StorageManager$ObbListenerDelegate;
    if-eqz v0, :cond_12

    #@d
    .line 118
    iget-object v1, p0, Landroid/os/storage/StorageManager$ObbActionListener;->mListeners:Landroid/util/SparseArray;

    #@f
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    #@12
    .line 120
    :cond_12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_19

    #@13
    .line 122
    if-eqz v0, :cond_18

    #@15
    .line 123
    invoke-virtual {v0, p1, p3}, Landroid/os/storage/StorageManager$ObbListenerDelegate;->sendObbStateChanged(Ljava/lang/String;I)V

    #@18
    .line 125
    :cond_18
    return-void

    #@19
    .line 120
    .end local v0           #delegate:Landroid/os/storage/StorageManager$ObbListenerDelegate;
    :catchall_19
    move-exception v1

    #@1a
    :try_start_1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1a .. :try_end_1b} :catchall_19

    #@1b
    throw v1
.end method
