.class public Landroid/os/storage/StorageVolume;
.super Ljava/lang/Object;
.source "StorageVolume.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/storage/StorageVolume;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_STORAGE_VOLUME:Ljava/lang/String; = "storage_volume"


# instance fields
.field private final mAllowMassStorage:Z

.field private final mDescriptionId:I

.field private final mEmulated:Z

.field private final mMaxFileSize:J

.field private final mMtpReserveSpace:I

.field private final mOwner:Landroid/os/UserHandle;

.field private final mPath:Ljava/io/File;

.field private final mPrimary:Z

.field private final mRemovable:Z

.field private mStorageId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 223
    new-instance v0, Landroid/os/storage/StorageVolume$1;

    #@2
    invoke-direct {v0}, Landroid/os/storage/StorageVolume$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/storage/StorageVolume;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 6
    .parameter "in"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/os/storage/StorageVolume;->mStorageId:I

    #@b
    .line 70
    new-instance v0, Ljava/io/File;

    #@d
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@14
    iput-object v0, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@16
    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@1c
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_55

    #@22
    move v0, v1

    #@23
    :goto_23
    iput-boolean v0, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@25
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_57

    #@2b
    move v0, v1

    #@2c
    :goto_2c
    iput-boolean v0, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@2e
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v0

    #@32
    if-eqz v0, :cond_59

    #@34
    move v0, v1

    #@35
    :goto_35
    iput-boolean v0, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@37
    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v0

    #@3b
    iput v0, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@3d
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v0

    #@41
    if-eqz v0, :cond_5b

    #@43
    :goto_43
    iput-boolean v1, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@45
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@48
    move-result-wide v0

    #@49
    iput-wide v0, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@4b
    .line 78
    const/4 v0, 0x0

    #@4c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@4f
    move-result-object v0

    #@50
    check-cast v0, Landroid/os/UserHandle;

    #@52
    iput-object v0, p0, Landroid/os/storage/StorageVolume;->mOwner:Landroid/os/UserHandle;

    #@54
    .line 79
    return-void

    #@55
    :cond_55
    move v0, v2

    #@56
    .line 72
    goto :goto_23

    #@57
    :cond_57
    move v0, v2

    #@58
    .line 73
    goto :goto_2c

    #@59
    :cond_59
    move v0, v2

    #@5a
    .line 74
    goto :goto_35

    #@5b
    :cond_5b
    move v1, v2

    #@5c
    .line 76
    goto :goto_43
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/os/storage/StorageVolume$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/os/storage/StorageVolume;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method public constructor <init>(Ljava/io/File;IZZZIZJLandroid/os/UserHandle;)V
    .registers 11
    .parameter "path"
    .parameter "descriptionId"
    .parameter "primary"
    .parameter "removable"
    .parameter "emulated"
    .parameter "mtpReserveSpace"
    .parameter "allowMassStorage"
    .parameter "maxFileSize"
    .parameter "owner"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    iput-object p1, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@5
    .line 58
    iput p2, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@7
    .line 59
    iput-boolean p3, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@9
    .line 60
    iput-boolean p4, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@b
    .line 61
    iput-boolean p5, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@d
    .line 62
    iput p6, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@f
    .line 63
    iput-boolean p7, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@11
    .line 64
    iput-wide p8, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@13
    .line 65
    iput-object p10, p0, Landroid/os/storage/StorageVolume;->mOwner:Landroid/os/UserHandle;

    #@15
    .line 66
    return-void
.end method

.method public static fromTemplate(Landroid/os/storage/StorageVolume;Ljava/io/File;Landroid/os/UserHandle;)Landroid/os/storage/StorageVolume;
    .registers 14
    .parameter "template"
    .parameter "path"
    .parameter "owner"

    #@0
    .prologue
    .line 82
    new-instance v0, Landroid/os/storage/StorageVolume;

    #@2
    iget v2, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@4
    iget-boolean v3, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@6
    iget-boolean v4, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@8
    iget-boolean v5, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@a
    iget v6, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@c
    iget-boolean v7, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@e
    iget-wide v8, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@10
    move-object v1, p1

    #@11
    move-object v10, p2

    #@12
    invoke-direct/range {v0 .. v10}, Landroid/os/storage/StorageVolume;-><init>(Ljava/io/File;IZZZIZJLandroid/os/UserHandle;)V

    #@15
    return-object v0
.end method


# virtual methods
.method public allowMassStorage()Z
    .registers 2

    #@0
    .prologue
    .line 176
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@2
    return v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 237
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "obj"

    #@0
    .prologue
    .line 194
    instance-of v1, p1, Landroid/os/storage/StorageVolume;

    #@2
    if-eqz v1, :cond_14

    #@4
    iget-object v1, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@6
    if-eqz v1, :cond_14

    #@8
    move-object v0, p1

    #@9
    .line 195
    check-cast v0, Landroid/os/storage/StorageVolume;

    #@b
    .line 196
    .local v0, volume:Landroid/os/storage/StorageVolume;
    iget-object v1, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@d
    iget-object v2, v0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@f
    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    .line 198
    .end local v0           #volume:Landroid/os/storage/StorageVolume;
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method public getDescription(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    iget v1, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@6
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getDescriptionId()I
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@2
    return v0
.end method

.method public getMaxFileSize()J
    .registers 3

    #@0
    .prologue
    .line 185
    iget-wide v0, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@2
    return-wide v0
.end method

.method public getMtpReserveSpace()I
    .registers 2

    #@0
    .prologue
    .line 167
    iget v0, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@2
    return v0
.end method

.method public getOwner()Landroid/os/UserHandle;
    .registers 2

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mOwner:Landroid/os/UserHandle;

    #@2
    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPathFile()Ljava/io/File;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public getStorageId()I
    .registers 2

    #@0
    .prologue
    .line 142
    iget v0, p0, Landroid/os/storage/StorageVolume;->mStorageId:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@2
    invoke-virtual {v0}, Ljava/io/File;->hashCode()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isEmulated()Z
    .registers 2

    #@0
    .prologue
    .line 132
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@2
    return v0
.end method

.method public isPrimary()Z
    .registers 2

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@2
    return v0
.end method

.method public isRemovable()Z
    .registers 2

    #@0
    .prologue
    .line 123
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@2
    return v0
.end method

.method public setStorageId(I)V
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 151
    add-int/lit8 v0, p1, 0x1

    #@2
    shl-int/lit8 v0, v0, 0x10

    #@4
    add-int/lit8 v0, v0, 0x1

    #@6
    iput v0, p0, Landroid/os/storage/StorageVolume;->mStorageId:I

    #@8
    .line 152
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const-string v1, "StorageVolume ["

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@7
    .line 209
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string/jumbo v1, "mStorageId="

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget v2, p0, Landroid/os/storage/StorageVolume;->mStorageId:I

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    .line 210
    const-string v1, " mPath="

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    .line 211
    const-string v1, " mDescriptionId="

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget v2, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    .line 212
    const-string v1, " mPrimary="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    iget-boolean v2, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34
    .line 213
    const-string v1, " mRemovable="

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget-boolean v2, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3f
    .line 214
    const-string v1, " mEmulated="

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget-boolean v2, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    .line 215
    const-string v1, " mMtpReserveSpace="

    #@4c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    iget v2, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    .line 216
    const-string v1, " mAllowMassStorage="

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    iget-boolean v2, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@5d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@60
    .line 217
    const-string v1, " mMaxFileSize="

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    iget-wide v2, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@68
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6b
    .line 218
    const-string v1, " mOwner="

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    iget-object v2, p0, Landroid/os/storage/StorageVolume;->mOwner:Landroid/os/UserHandle;

    #@73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@76
    .line 219
    const-string v1, "]"

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    .line 220
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v1

    #@7f
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 242
    iget v0, p0, Landroid/os/storage/StorageVolume;->mStorageId:I

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@7
    .line 243
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mPath:Ljava/io/File;

    #@9
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 244
    iget v0, p0, Landroid/os/storage/StorageVolume;->mDescriptionId:I

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 245
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mPrimary:Z

    #@17
    if-eqz v0, :cond_44

    #@19
    move v0, v1

    #@1a
    :goto_1a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 246
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mRemovable:Z

    #@1f
    if-eqz v0, :cond_46

    #@21
    move v0, v1

    #@22
    :goto_22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@25
    .line 247
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mEmulated:Z

    #@27
    if-eqz v0, :cond_48

    #@29
    move v0, v1

    #@2a
    :goto_2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 248
    iget v0, p0, Landroid/os/storage/StorageVolume;->mMtpReserveSpace:I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 249
    iget-boolean v0, p0, Landroid/os/storage/StorageVolume;->mAllowMassStorage:Z

    #@34
    if-eqz v0, :cond_4a

    #@36
    :goto_36
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@39
    .line 250
    iget-wide v0, p0, Landroid/os/storage/StorageVolume;->mMaxFileSize:J

    #@3b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@3e
    .line 251
    iget-object v0, p0, Landroid/os/storage/StorageVolume;->mOwner:Landroid/os/UserHandle;

    #@40
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@43
    .line 252
    return-void

    #@44
    :cond_44
    move v0, v2

    #@45
    .line 245
    goto :goto_1a

    #@46
    :cond_46
    move v0, v2

    #@47
    .line 246
    goto :goto_22

    #@48
    :cond_48
    move v0, v2

    #@49
    .line 247
    goto :goto_2a

    #@4a
    :cond_4a
    move v1, v2

    #@4b
    .line 249
    goto :goto_36
.end method
