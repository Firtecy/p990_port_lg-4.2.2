.class Landroid/os/storage/StorageManager$ListenerDelegate$1;
.super Landroid/os/Handler;
.source "StorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/os/storage/StorageManager$ListenerDelegate;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/StorageEventListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/os/storage/StorageManager$ListenerDelegate;

.field final synthetic val$this$0:Landroid/os/storage/StorageManager;


# direct methods
.method constructor <init>(Landroid/os/storage/StorageManager$ListenerDelegate;Landroid/os/Looper;Landroid/os/storage/StorageManager;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    #@0
    .prologue
    .line 261
    iput-object p1, p0, Landroid/os/storage/StorageManager$ListenerDelegate$1;->this$1:Landroid/os/storage/StorageManager$ListenerDelegate;

    #@2
    iput-object p3, p0, Landroid/os/storage/StorageManager$ListenerDelegate$1;->val$this$0:Landroid/os/storage/StorageManager;

    #@4
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 264
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2
    check-cast v0, Landroid/os/storage/StorageManager$StorageEvent;

    #@4
    .line 266
    .local v0, e:Landroid/os/storage/StorageManager$StorageEvent;
    iget v2, p1, Landroid/os/Message;->what:I

    #@6
    const/4 v3, 0x1

    #@7
    if-ne v2, v3, :cond_16

    #@9
    move-object v1, v0

    #@a
    .line 267
    check-cast v1, Landroid/os/storage/StorageManager$UmsConnectionChangedStorageEvent;

    #@c
    .line 268
    .local v1, ev:Landroid/os/storage/StorageManager$UmsConnectionChangedStorageEvent;
    iget-object v2, p0, Landroid/os/storage/StorageManager$ListenerDelegate$1;->this$1:Landroid/os/storage/StorageManager$ListenerDelegate;

    #@e
    iget-object v2, v2, Landroid/os/storage/StorageManager$ListenerDelegate;->mStorageEventListener:Landroid/os/storage/StorageEventListener;

    #@10
    iget-boolean v3, v1, Landroid/os/storage/StorageManager$UmsConnectionChangedStorageEvent;->available:Z

    #@12
    invoke-virtual {v2, v3}, Landroid/os/storage/StorageEventListener;->onUsbMassStorageConnectionChanged(Z)V

    #@15
    .line 275
    .end local v1           #ev:Landroid/os/storage/StorageManager$UmsConnectionChangedStorageEvent;
    :goto_15
    return-void

    #@16
    .line 269
    :cond_16
    iget v2, p1, Landroid/os/Message;->what:I

    #@18
    const/4 v3, 0x2

    #@19
    if-ne v2, v3, :cond_2c

    #@1b
    move-object v1, v0

    #@1c
    .line 270
    check-cast v1, Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;

    #@1e
    .line 271
    .local v1, ev:Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;
    iget-object v2, p0, Landroid/os/storage/StorageManager$ListenerDelegate$1;->this$1:Landroid/os/storage/StorageManager$ListenerDelegate;

    #@20
    iget-object v2, v2, Landroid/os/storage/StorageManager$ListenerDelegate;->mStorageEventListener:Landroid/os/storage/StorageEventListener;

    #@22
    iget-object v3, v1, Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;->path:Ljava/lang/String;

    #@24
    iget-object v4, v1, Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;->oldState:Ljava/lang/String;

    #@26
    iget-object v5, v1, Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;->newState:Ljava/lang/String;

    #@28
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/storage/StorageEventListener;->onStorageStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    goto :goto_15

    #@2c
    .line 273
    .end local v1           #ev:Landroid/os/storage/StorageManager$StorageStateChangedStorageEvent;
    :cond_2c
    const-string v2, "StorageManager"

    #@2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v4, "Unsupported event "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    iget v4, p1, Landroid/os/Message;->what:I

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_15
.end method
