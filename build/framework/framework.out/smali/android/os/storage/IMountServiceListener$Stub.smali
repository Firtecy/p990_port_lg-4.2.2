.class public abstract Landroid/os/storage/IMountServiceListener$Stub;
.super Landroid/os/Binder;
.source "IMountServiceListener.java"

# interfaces
.implements Landroid/os/storage/IMountServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/storage/IMountServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/storage/IMountServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "IMountServiceListener"

.field static final TRANSACTION_onStorageStateChanged:I = 0x2

.field static final TRANSACTION_onUsbMassStorageConnectionChanged:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 38
    const-string v0, "IMountServiceListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/storage/IMountServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 39
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountServiceListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 46
    if-nez p0, :cond_4

    #@2
    .line 47
    const/4 v0, 0x0

    #@3
    .line 53
    :goto_3
    return-object v0

    #@4
    .line 49
    :cond_4
    const-string v1, "IMountServiceListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 50
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/storage/IMountServiceListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 51
    check-cast v0, Landroid/os/storage/IMountServiceListener;

    #@12
    goto :goto_3

    #@13
    .line 53
    :cond_13
    new-instance v0, Landroid/os/storage/IMountServiceListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/storage/IMountServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 57
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 11
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 63
    sparse-switch p1, :sswitch_data_3c

    #@4
    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v4

    #@8
    :goto_8
    return v4

    #@9
    .line 65
    :sswitch_9
    const-string v5, "IMountServiceListener"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 69
    :sswitch_f
    const-string v5, "IMountServiceListener"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v5

    #@18
    if-eqz v5, :cond_22

    #@1a
    move v0, v4

    #@1b
    .line 72
    .local v0, connected:Z
    :goto_1b
    invoke-virtual {p0, v0}, Landroid/os/storage/IMountServiceListener$Stub;->onUsbMassStorageConnectionChanged(Z)V

    #@1e
    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21
    goto :goto_8

    #@22
    .line 71
    .end local v0           #connected:Z
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_1b

    #@24
    .line 77
    :sswitch_24
    const-string v5, "IMountServiceListener"

    #@26
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@29
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    .line 81
    .local v3, path:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    .line 83
    .local v2, oldState:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    .line 84
    .local v1, newState:Ljava/lang/String;
    invoke-virtual {p0, v3, v2, v1}, Landroid/os/storage/IMountServiceListener$Stub;->onStorageStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    goto :goto_8

    #@3c
    .line 63
    :sswitch_data_3c
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_24
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
