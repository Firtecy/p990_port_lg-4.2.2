.class Landroid/os/storage/StorageManager$ObbListenerDelegate$1;
.super Landroid/os/Handler;
.source "StorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/os/storage/StorageManager$ObbListenerDelegate;-><init>(Landroid/os/storage/StorageManager;Landroid/os/storage/OnObbStateChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/os/storage/StorageManager$ObbListenerDelegate;

.field final synthetic val$this$0:Landroid/os/storage/StorageManager;


# direct methods
.method constructor <init>(Landroid/os/storage/StorageManager$ObbListenerDelegate;Landroid/os/Looper;Landroid/os/storage/StorageManager;)V
    .registers 4
    .parameter
    .parameter "x0"
    .parameter

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Landroid/os/storage/StorageManager$ObbListenerDelegate$1;->this$1:Landroid/os/storage/StorageManager$ObbListenerDelegate;

    #@2
    iput-object p3, p0, Landroid/os/storage/StorageManager$ObbListenerDelegate$1;->val$this$0:Landroid/os/storage/StorageManager;

    #@4
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@7
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 157
    iget-object v3, p0, Landroid/os/storage/StorageManager$ObbListenerDelegate$1;->this$1:Landroid/os/storage/StorageManager$ObbListenerDelegate;

    #@2
    invoke-virtual {v3}, Landroid/os/storage/StorageManager$ObbListenerDelegate;->getListener()Landroid/os/storage/OnObbStateChangeListener;

    #@5
    move-result-object v0

    #@6
    .line 158
    .local v0, changeListener:Landroid/os/storage/OnObbStateChangeListener;
    if-nez v0, :cond_9

    #@8
    .line 170
    :goto_8
    return-void

    #@9
    .line 162
    :cond_9
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    check-cast v1, Landroid/os/storage/StorageManager$StorageEvent;

    #@d
    .line 164
    .local v1, e:Landroid/os/storage/StorageManager$StorageEvent;
    iget v3, p1, Landroid/os/Message;->what:I

    #@f
    const/4 v4, 0x3

    #@10
    if-ne v3, v4, :cond_1d

    #@12
    move-object v2, v1

    #@13
    .line 165
    check-cast v2, Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;

    #@15
    .line 166
    .local v2, ev:Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;
    iget-object v3, v2, Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;->path:Ljava/lang/String;

    #@17
    iget v4, v2, Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;->state:I

    #@19
    invoke-virtual {v0, v3, v4}, Landroid/os/storage/OnObbStateChangeListener;->onObbStateChange(Ljava/lang/String;I)V

    #@1c
    goto :goto_8

    #@1d
    .line 168
    .end local v2           #ev:Landroid/os/storage/StorageManager$ObbStateChangedStorageEvent;
    :cond_1d
    const-string v3, "StorageManager"

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "Unsupported event "

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    iget v5, p1, Landroid/os/Message;->what:I

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_8
.end method
