.class public final Landroid/os/FactoryTest;
.super Ljava/lang/Object;
.source "FactoryTest.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static isLongPressOnPowerOffEnabled()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 33
    const-string v1, "factory.long_press_power_off"

    #@3
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_a

    #@9
    const/4 v0, 0x1

    #@a
    :cond_a
    return v0
.end method
