.class Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;
.super Ljava/lang/Object;
.source "StrictMode.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/os/StrictMode$AndroidBlockGuardPolicy;

.field final synthetic val$records:Ljava/util/ArrayList;

.field final synthetic val$windowManager:Landroid/view/IWindowManager;


# direct methods
.method constructor <init>(Landroid/os/StrictMode$AndroidBlockGuardPolicy;Landroid/view/IWindowManager;Ljava/util/ArrayList;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 1207
    iput-object p1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->this$0:Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@2
    iput-object p2, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$windowManager:Landroid/view/IWindowManager;

    #@4
    iput-object p3, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$records:Ljava/util/ArrayList;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    #@0
    .prologue
    .line 1209
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    .line 1215
    .local v0, loopFinishTime:J
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$windowManager:Landroid/view/IWindowManager;

    #@6
    if-eqz v4, :cond_e

    #@8
    .line 1217
    :try_start_8
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$windowManager:Landroid/view/IWindowManager;

    #@a
    const/4 v5, 0x0

    #@b
    invoke-interface {v4, v5}, Landroid/view/IWindowManager;->showStrictModeViolation(Z)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_e} :catch_38

    #@e
    .line 1222
    :cond_e
    :goto_e
    const/4 v2, 0x0

    #@f
    .local v2, n:I
    :goto_f
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$records:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v4

    #@15
    if-ge v2, v4, :cond_32

    #@17
    .line 1223
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$records:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/os/StrictMode$ViolationInfo;

    #@1f
    .line 1224
    .local v3, v:Landroid/os/StrictMode$ViolationInfo;
    add-int/lit8 v4, v2, 0x1

    #@21
    iput v4, v3, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    #@23
    .line 1225
    iget-wide v4, v3, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@25
    sub-long v4, v0, v4

    #@27
    long-to-int v4, v4

    #@28
    iput v4, v3, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@2a
    .line 1227
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->this$0:Landroid/os/StrictMode$AndroidBlockGuardPolicy;

    #@2c
    invoke-virtual {v4, v3}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolation(Landroid/os/StrictMode$ViolationInfo;)V

    #@2f
    .line 1222
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_f

    #@32
    .line 1229
    .end local v3           #v:Landroid/os/StrictMode$ViolationInfo;
    :cond_32
    iget-object v4, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$1;->val$records:Ljava/util/ArrayList;

    #@34
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    #@37
    .line 1230
    return-void

    #@38
    .line 1218
    .end local v2           #n:I
    :catch_38
    move-exception v4

    #@39
    goto :goto_e
.end method
