.class public final Landroid/os/BatteryStats$HistoryItem;
.super Ljava/lang/Object;
.source "BatteryStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/BatteryStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HistoryItem"
.end annotation


# static fields
.field public static final CMD_NULL:B = 0x0t

.field public static final CMD_OVERFLOW:B = 0x3t

.field public static final CMD_START:B = 0x2t

.field public static final CMD_UPDATE:B = 0x1t

.field static final DEBUG:Z = false

.field static final DELTA_BATTERY_LEVEL_FLAG:I = 0x100000

.field static final DELTA_CMD_MASK:I = 0x3

.field static final DELTA_CMD_SHIFT:I = 0x12

.field static final DELTA_STATE_FLAG:I = 0x200000

.field static final DELTA_STATE_MASK:I = -0x400000

.field static final DELTA_TIME_ABS:I = 0x3fffd

.field static final DELTA_TIME_INT:I = 0x3fffe

.field static final DELTA_TIME_LONG:I = 0x3ffff

.field static final DELTA_TIME_MASK:I = 0x3ffff

.field public static final MOST_INTERESTING_STATES:I = 0x101c0000

.field public static final STATE_AUDIO_ON_FLAG:I = 0x400000

.field public static final STATE_BATTERY_PLUGGED_FLAG:I = 0x80000

.field public static final STATE_BLUETOOTH_ON_FLAG:I = 0x10000

.field public static final STATE_BRIGHTNESS_MASK:I = 0xf

.field public static final STATE_BRIGHTNESS_SHIFT:I = 0x0

.field public static final STATE_DATA_CONNECTION_MASK:I = 0xf000

.field public static final STATE_DATA_CONNECTION_SHIFT:I = 0xc

.field public static final STATE_GPS_ON_FLAG:I = 0x10000000

.field public static final STATE_PHONE_IN_CALL_FLAG:I = 0x40000

.field public static final STATE_PHONE_SCANNING_FLAG:I = 0x8000000

.field public static final STATE_PHONE_STATE_MASK:I = 0xf00

.field public static final STATE_PHONE_STATE_SHIFT:I = 0x8

.field public static final STATE_SCREEN_ON_FLAG:I = 0x100000

.field public static final STATE_SENSOR_ON_FLAG:I = 0x20000000

.field public static final STATE_SIGNAL_STRENGTH_MASK:I = 0xf0

.field public static final STATE_SIGNAL_STRENGTH_SHIFT:I = 0x4

.field public static final STATE_VIDEO_ON_FLAG:I = 0x200000

.field public static final STATE_WAKE_LOCK_FLAG:I = 0x40000000

.field public static final STATE_WIFI_FULL_LOCK_FLAG:I = 0x2000000

.field public static final STATE_WIFI_MULTICAST_ON_FLAG:I = 0x800000

.field public static final STATE_WIFI_ON_FLAG:I = 0x20000

.field public static final STATE_WIFI_RUNNING_FLAG:I = 0x4000000

.field public static final STATE_WIFI_SCAN_FLAG:I = 0x1000000

.field static final TAG:Ljava/lang/String; = "HistoryItem"


# instance fields
.field public batteryHealth:B

.field public batteryLevel:B

.field public batteryPlugType:B

.field public batteryStatus:B

.field public batteryTemperature:C

.field public batteryVoltage:C

.field public cmd:B

.field public next:Landroid/os/BatteryStats$HistoryItem;

.field public states:I

.field public time:J


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 478
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 428
    const/4 v0, 0x0

    #@4
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@6
    .line 479
    return-void
.end method

.method public constructor <init>(JLandroid/os/Parcel;)V
    .registers 5
    .parameter "time"
    .parameter "src"

    #@0
    .prologue
    .line 481
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 428
    const/4 v0, 0x0

    #@4
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@6
    .line 482
    iput-wide p1, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@8
    .line 483
    invoke-direct {p0, p3}, Landroid/os/BatteryStats$HistoryItem;->readFromParcel(Landroid/os/Parcel;)V

    #@b
    .line 484
    return-void
.end method

.method private buildBatteryLevelInt()I
    .registers 4

    #@0
    .prologue
    .line 596
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@2
    shl-int/lit8 v0, v0, 0x18

    #@4
    const/high16 v1, -0x100

    #@6
    and-int/2addr v0, v1

    #@7
    iget-char v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@9
    shl-int/lit8 v1, v1, 0xe

    #@b
    const v2, 0xffc000

    #@e
    and-int/2addr v1, v2

    #@f
    or-int/2addr v0, v1

    #@10
    iget-char v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@12
    and-int/lit16 v1, v1, 0x3fff

    #@14
    or-int/2addr v0, v1

    #@15
    return v0
.end method

.method private buildStateInt()I
    .registers 4

    #@0
    .prologue
    .line 602
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@2
    shl-int/lit8 v0, v0, 0x1c

    #@4
    const/high16 v1, -0x1000

    #@6
    and-int/2addr v0, v1

    #@7
    iget-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@9
    shl-int/lit8 v1, v1, 0x18

    #@b
    const/high16 v2, 0xf00

    #@d
    and-int/2addr v1, v2

    #@e
    or-int/2addr v0, v1

    #@f
    iget-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@11
    shl-int/lit8 v1, v1, 0x16

    #@13
    const/high16 v2, 0xc0

    #@15
    and-int/2addr v1, v2

    #@16
    or-int/2addr v0, v1

    #@17
    iget v1, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@19
    const v2, 0x3fffff

    #@1c
    and-int/2addr v1, v2

    #@1d
    or-int/2addr v0, v1

    #@1e
    return v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .registers 5
    .parameter "src"

    #@0
    .prologue
    const v2, 0xffff

    #@3
    .line 505
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    .line 506
    .local v0, bat:I
    and-int/lit16 v1, v0, 0xff

    #@9
    int-to-byte v1, v1

    #@a
    iput-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@c
    .line 507
    shr-int/lit8 v1, v0, 0x8

    #@e
    and-int/lit16 v1, v1, 0xff

    #@10
    int-to-byte v1, v1

    #@11
    iput-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@13
    .line 508
    shr-int/lit8 v1, v0, 0x10

    #@15
    and-int/lit8 v1, v1, 0xf

    #@17
    int-to-byte v1, v1

    #@18
    iput-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@1a
    .line 509
    shr-int/lit8 v1, v0, 0x14

    #@1c
    and-int/lit8 v1, v1, 0xf

    #@1e
    int-to-byte v1, v1

    #@1f
    iput-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@21
    .line 510
    shr-int/lit8 v1, v0, 0x18

    #@23
    and-int/lit8 v1, v1, 0xf

    #@25
    int-to-byte v1, v1

    #@26
    iput-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@28
    .line 511
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    .line 512
    and-int v1, v0, v2

    #@2e
    int-to-char v1, v1

    #@2f
    iput-char v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@31
    .line 513
    shr-int/lit8 v1, v0, 0x10

    #@33
    and-int/2addr v1, v2

    #@34
    int-to-char v1, v1

    #@35
    iput-char v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@37
    .line 514
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v1

    #@3b
    iput v1, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@3d
    .line 515
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 661
    const-wide/16 v0, 0x0

    #@3
    iput-wide v0, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@5
    .line 662
    iput-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@7
    .line 663
    iput-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@9
    .line 664
    iput-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@b
    .line 665
    iput-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@d
    .line 666
    iput-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@f
    .line 667
    iput-char v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@11
    .line 668
    iput-char v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@13
    .line 669
    iput v2, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@15
    .line 670
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 487
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readDelta(Landroid/os/Parcel;)V
    .registers 14
    .parameter "src"

    #@0
    .prologue
    const v11, 0x3fffff

    #@3
    const v7, 0x3fffd

    #@6
    const/high16 v10, -0x40

    #@8
    .line 609
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@b
    move-result v4

    #@c
    .line 610
    .local v4, firstToken:I
    const v6, 0x3ffff

    #@f
    and-int v3, v4, v6

    #@11
    .line 611
    .local v3, deltaTimeToken:I
    shr-int/lit8 v6, v4, 0x12

    #@13
    and-int/lit8 v6, v6, 0x3

    #@15
    int-to-byte v6, v6

    #@16
    iput-byte v6, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@18
    .line 615
    if-ge v3, v7, :cond_62

    #@1a
    .line 616
    iget-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@1c
    int-to-long v8, v3

    #@1d
    add-long/2addr v6, v8

    #@1e
    iput-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@20
    .line 631
    :goto_20
    const/high16 v6, 0x10

    #@22
    and-int/2addr v6, v4

    #@23
    if-eqz v6, :cond_3c

    #@25
    .line 632
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    .line 633
    .local v0, batteryLevelInt:I
    shr-int/lit8 v6, v0, 0x18

    #@2b
    and-int/lit16 v6, v6, 0xff

    #@2d
    int-to-byte v6, v6

    #@2e
    iput-byte v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@30
    .line 634
    shr-int/lit8 v6, v0, 0xe

    #@32
    and-int/lit16 v6, v6, 0x3ff

    #@34
    int-to-char v6, v6

    #@35
    iput-char v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@37
    .line 635
    and-int/lit16 v6, v0, 0x3fff

    #@39
    int-to-char v6, v6

    #@3a
    iput-char v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@3c
    .line 643
    .end local v0           #batteryLevelInt:I
    :cond_3c
    const/high16 v6, 0x20

    #@3e
    and-int/2addr v6, v4

    #@3f
    if-eqz v6, :cond_88

    #@41
    .line 644
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v5

    #@45
    .line 645
    .local v5, stateInt:I
    and-int v6, v4, v10

    #@47
    and-int v7, v5, v11

    #@49
    or-int/2addr v6, v7

    #@4a
    iput v6, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@4c
    .line 646
    shr-int/lit8 v6, v5, 0x1c

    #@4e
    and-int/lit8 v6, v6, 0xf

    #@50
    int-to-byte v6, v6

    #@51
    iput-byte v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@53
    .line 647
    shr-int/lit8 v6, v5, 0x18

    #@55
    and-int/lit8 v6, v6, 0xf

    #@57
    int-to-byte v6, v6

    #@58
    iput-byte v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@5a
    .line 648
    shr-int/lit8 v6, v5, 0x16

    #@5c
    and-int/lit8 v6, v6, 0x3

    #@5e
    int-to-byte v6, v6

    #@5f
    iput-byte v6, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@61
    .line 658
    .end local v5           #stateInt:I
    :goto_61
    return-void

    #@62
    .line 617
    :cond_62
    if-ne v3, v7, :cond_6e

    #@64
    .line 618
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@67
    move-result-wide v6

    #@68
    iput-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@6a
    .line 619
    invoke-direct {p0, p1}, Landroid/os/BatteryStats$HistoryItem;->readFromParcel(Landroid/os/Parcel;)V

    #@6d
    goto :goto_61

    #@6e
    .line 621
    :cond_6e
    const v6, 0x3fffe

    #@71
    if-ne v3, v6, :cond_7e

    #@73
    .line 622
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@76
    move-result v1

    #@77
    .line 623
    .local v1, delta:I
    iget-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@79
    int-to-long v8, v1

    #@7a
    add-long/2addr v6, v8

    #@7b
    iput-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@7d
    goto :goto_20

    #@7e
    .line 626
    .end local v1           #delta:I
    :cond_7e
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@81
    move-result-wide v1

    #@82
    .line 628
    .local v1, delta:J
    iget-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@84
    add-long/2addr v6, v1

    #@85
    iput-wide v6, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@87
    goto :goto_20

    #@88
    .line 656
    .end local v1           #delta:J
    :cond_88
    and-int v6, v4, v10

    #@8a
    iget v7, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@8c
    and-int/2addr v7, v11

    #@8d
    or-int/2addr v6, v7

    #@8e
    iput v6, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@90
    goto :goto_61
.end method

.method public same(Landroid/os/BatteryStats$HistoryItem;)Z
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 697
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@2
    iget-byte v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@4
    if-ne v0, v1, :cond_2c

    #@6
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@8
    iget-byte v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@a
    if-ne v0, v1, :cond_2c

    #@c
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@e
    iget-byte v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@10
    if-ne v0, v1, :cond_2c

    #@12
    iget-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@14
    iget-byte v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@16
    if-ne v0, v1, :cond_2c

    #@18
    iget-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@1a
    iget-char v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@1c
    if-ne v0, v1, :cond_2c

    #@1e
    iget-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@20
    iget-char v1, p1, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@22
    if-ne v0, v1, :cond_2c

    #@24
    iget v0, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@26
    iget v1, p1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@28
    if-ne v0, v1, :cond_2c

    #@2a
    const/4 v0, 0x1

    #@2b
    :goto_2b
    return v0

    #@2c
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_2b
.end method

.method public setTo(JBLandroid/os/BatteryStats$HistoryItem;)V
    .registers 6
    .parameter "time"
    .parameter "cmd"
    .parameter "o"

    #@0
    .prologue
    .line 685
    iput-wide p1, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@2
    .line 686
    iput-byte p3, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@4
    .line 687
    iget-byte v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@6
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@8
    .line 688
    iget-byte v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@a
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@c
    .line 689
    iget-byte v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@e
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@10
    .line 690
    iget-byte v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@12
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@14
    .line 691
    iget-char v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@16
    iput-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@18
    .line 692
    iget-char v0, p4, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@1a
    iput-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@1c
    .line 693
    iget v0, p4, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@1e
    iput v0, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@20
    .line 694
    return-void
.end method

.method public setTo(Landroid/os/BatteryStats$HistoryItem;)V
    .registers 4
    .parameter "o"

    #@0
    .prologue
    .line 673
    iget-wide v0, p1, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@2
    iput-wide v0, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@4
    .line 674
    iget-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@6
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@8
    .line 675
    iget-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@a
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@c
    .line 676
    iget-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@e
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@10
    .line 677
    iget-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@12
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@14
    .line 678
    iget-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@16
    iput-byte v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@18
    .line 679
    iget-char v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@1a
    iput-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@1c
    .line 680
    iget-char v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@1e
    iput-char v0, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@20
    .line 681
    iget v0, p1, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@22
    iput v0, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@24
    .line 682
    return-void
.end method

.method public writeDelta(Landroid/os/Parcel;Landroid/os/BatteryStats$HistoryItem;)V
    .registers 18
    .parameter "dest"
    .parameter "last"

    #@0
    .prologue
    .line 532
    if-eqz p2, :cond_9

    #@2
    move-object/from16 v0, p2

    #@4
    iget-byte v11, v0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@6
    const/4 v12, 0x1

    #@7
    if-eq v11, v12, :cond_18

    #@9
    .line 533
    :cond_9
    const v11, 0x3fffd

    #@c
    move-object/from16 v0, p1

    #@e
    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 534
    const/4 v11, 0x0

    #@12
    move-object/from16 v0, p1

    #@14
    invoke-virtual {p0, v0, v11}, Landroid/os/BatteryStats$HistoryItem;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 593
    :cond_17
    :goto_17
    return-void

    #@18
    .line 538
    :cond_18
    iget-wide v11, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@1a
    move-object/from16 v0, p2

    #@1c
    iget-wide v13, v0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@1e
    sub-long v3, v11, v13

    #@20
    .line 539
    .local v3, deltaTime:J
    invoke-direct/range {p2 .. p2}, Landroid/os/BatteryStats$HistoryItem;->buildBatteryLevelInt()I

    #@23
    move-result v7

    #@24
    .line 540
    .local v7, lastBatteryLevelInt:I
    invoke-direct/range {p2 .. p2}, Landroid/os/BatteryStats$HistoryItem;->buildStateInt()I

    #@27
    move-result v8

    #@28
    .line 543
    .local v8, lastStateInt:I
    const-wide/16 v11, 0x0

    #@2a
    cmp-long v11, v3, v11

    #@2c
    if-ltz v11, :cond_35

    #@2e
    const-wide/32 v11, 0x7fffffff

    #@31
    cmp-long v11, v3, v11

    #@33
    if-lez v11, :cond_80

    #@35
    .line 544
    :cond_35
    const v5, 0x3ffff

    #@38
    .line 550
    .local v5, deltaTimeToken:I
    :goto_38
    iget-byte v11, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@3a
    shl-int/lit8 v11, v11, 0x12

    #@3c
    or-int/2addr v11, v5

    #@3d
    iget v12, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@3f
    const/high16 v13, -0x40

    #@41
    and-int/2addr v12, v13

    #@42
    or-int v6, v11, v12

    #@44
    .line 553
    .local v6, firstToken:I
    invoke-direct {p0}, Landroid/os/BatteryStats$HistoryItem;->buildBatteryLevelInt()I

    #@47
    move-result v1

    #@48
    .line 554
    .local v1, batteryLevelInt:I
    if-eq v1, v7, :cond_8d

    #@4a
    const/4 v2, 0x1

    #@4b
    .line 555
    .local v2, batteryLevelIntChanged:Z
    :goto_4b
    if-eqz v2, :cond_50

    #@4d
    .line 556
    const/high16 v11, 0x10

    #@4f
    or-int/2addr v6, v11

    #@50
    .line 558
    :cond_50
    invoke-direct {p0}, Landroid/os/BatteryStats$HistoryItem;->buildStateInt()I

    #@53
    move-result v9

    #@54
    .line 559
    .local v9, stateInt:I
    if-eq v9, v8, :cond_8f

    #@56
    const/4 v10, 0x1

    #@57
    .line 560
    .local v10, stateIntChanged:Z
    :goto_57
    if-eqz v10, :cond_5c

    #@59
    .line 561
    const/high16 v11, 0x20

    #@5b
    or-int/2addr v6, v11

    #@5c
    .line 563
    :cond_5c
    move-object/from16 v0, p1

    #@5e
    invoke-virtual {v0, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@61
    .line 567
    const v11, 0x3fffe

    #@64
    if-lt v5, v11, :cond_71

    #@66
    .line 568
    const v11, 0x3fffe

    #@69
    if-ne v5, v11, :cond_91

    #@6b
    .line 570
    long-to-int v11, v3

    #@6c
    move-object/from16 v0, p1

    #@6e
    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeInt(I)V

    #@71
    .line 576
    :cond_71
    :goto_71
    if-eqz v2, :cond_78

    #@73
    .line 577
    move-object/from16 v0, p1

    #@75
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@78
    .line 584
    :cond_78
    if-eqz v10, :cond_17

    #@7a
    .line 585
    move-object/from16 v0, p1

    #@7c
    invoke-virtual {v0, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_17

    #@80
    .line 545
    .end local v1           #batteryLevelInt:I
    .end local v2           #batteryLevelIntChanged:Z
    .end local v5           #deltaTimeToken:I
    .end local v6           #firstToken:I
    .end local v9           #stateInt:I
    .end local v10           #stateIntChanged:Z
    :cond_80
    const-wide/32 v11, 0x3fffd

    #@83
    cmp-long v11, v3, v11

    #@85
    if-ltz v11, :cond_8b

    #@87
    .line 546
    const v5, 0x3fffe

    #@8a
    .restart local v5       #deltaTimeToken:I
    goto :goto_38

    #@8b
    .line 548
    .end local v5           #deltaTimeToken:I
    :cond_8b
    long-to-int v5, v3

    #@8c
    .restart local v5       #deltaTimeToken:I
    goto :goto_38

    #@8d
    .line 554
    .restart local v1       #batteryLevelInt:I
    .restart local v6       #firstToken:I
    :cond_8d
    const/4 v2, 0x0

    #@8e
    goto :goto_4b

    #@8f
    .line 559
    .restart local v2       #batteryLevelIntChanged:Z
    .restart local v9       #stateInt:I
    :cond_8f
    const/4 v10, 0x0

    #@90
    goto :goto_57

    #@91
    .line 573
    .restart local v10       #stateIntChanged:Z
    :cond_91
    move-object/from16 v0, p1

    #@93
    invoke-virtual {v0, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    #@96
    goto :goto_71
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 7
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 491
    iget-wide v1, p0, Landroid/os/BatteryStats$HistoryItem;->time:J

    #@2
    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@5
    .line 492
    iget-byte v1, p0, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    #@7
    and-int/lit16 v1, v1, 0xff

    #@9
    iget-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    #@b
    shl-int/lit8 v2, v2, 0x8

    #@d
    const v3, 0xff00

    #@10
    and-int/2addr v2, v3

    #@11
    or-int/2addr v1, v2

    #@12
    iget-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    #@14
    shl-int/lit8 v2, v2, 0x10

    #@16
    const/high16 v3, 0xf

    #@18
    and-int/2addr v2, v3

    #@19
    or-int/2addr v1, v2

    #@1a
    iget-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    #@1c
    shl-int/lit8 v2, v2, 0x14

    #@1e
    const/high16 v3, 0xf0

    #@20
    and-int/2addr v2, v3

    #@21
    or-int/2addr v1, v2

    #@22
    iget-byte v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    #@24
    shl-int/lit8 v2, v2, 0x18

    #@26
    const/high16 v3, 0xf00

    #@28
    and-int/2addr v2, v3

    #@29
    or-int v0, v1, v2

    #@2b
    .line 497
    .local v0, bat:I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 498
    iget-char v1, p0, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:C

    #@30
    const v2, 0xffff

    #@33
    and-int/2addr v1, v2

    #@34
    iget-char v2, p0, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    #@36
    shl-int/lit8 v2, v2, 0x10

    #@38
    const/high16 v3, -0x1

    #@3a
    and-int/2addr v2, v3

    #@3b
    or-int v0, v1, v2

    #@3d
    .line 500
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@40
    .line 501
    iget v1, p0, Landroid/os/BatteryStats$HistoryItem;->states:I

    #@42
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@45
    .line 502
    return-void
.end method
