.class public final Landroid/os/Trace;
.super Ljava/lang/Object;
.source "Trace.java"


# static fields
.field public static final PROPERTY_TRACE_TAG_ENABLEFLAGS:Ljava/lang/String; = "debug.atrace.tags.enableflags"

.field private static final TAG:Ljava/lang/String; = "Trace"

.field public static final TRACE_FLAGS_START_BIT:I = 0x1

.field public static final TRACE_TAGS:[Ljava/lang/String; = null

.field public static final TRACE_TAG_ACTIVITY_MANAGER:J = 0x40L

.field public static final TRACE_TAG_ALWAYS:J = 0x1L

.field public static final TRACE_TAG_AUDIO:J = 0x100L

.field public static final TRACE_TAG_CAMERA:J = 0x400L

.field public static final TRACE_TAG_GRAPHICS:J = 0x2L

.field public static final TRACE_TAG_INPUT:J = 0x4L

.field public static final TRACE_TAG_NEVER:J = 0x0L

.field private static final TRACE_TAG_NOT_READY:J = -0x8000000000000000L

.field public static final TRACE_TAG_SYNC_MANAGER:J = 0x80L

.field public static final TRACE_TAG_VIDEO:J = 0x200L

.field public static final TRACE_TAG_VIEW:J = 0x8L

.field public static final TRACE_TAG_WEBVIEW:J = 0x10L

.field public static final TRACE_TAG_WINDOW_MANAGER:J = 0x20L

.field private static volatile sEnabledTags:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 50
    const/16 v0, 0xa

    #@2
    new-array v0, v0, [Ljava/lang/String;

    #@4
    const/4 v1, 0x0

    #@5
    const-string v2, "Graphics"

    #@7
    aput-object v2, v0, v1

    #@9
    const/4 v1, 0x1

    #@a
    const-string v2, "Input"

    #@c
    aput-object v2, v0, v1

    #@e
    const/4 v1, 0x2

    #@f
    const-string v2, "View"

    #@11
    aput-object v2, v0, v1

    #@13
    const/4 v1, 0x3

    #@14
    const-string v2, "WebView"

    #@16
    aput-object v2, v0, v1

    #@18
    const/4 v1, 0x4

    #@19
    const-string v2, "Window Manager"

    #@1b
    aput-object v2, v0, v1

    #@1d
    const/4 v1, 0x5

    #@1e
    const-string v2, "Activity Manager"

    #@20
    aput-object v2, v0, v1

    #@22
    const/4 v1, 0x6

    #@23
    const-string v2, "Sync Manager"

    #@25
    aput-object v2, v0, v1

    #@27
    const/4 v1, 0x7

    #@28
    const-string v2, "Audio"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/16 v1, 0x8

    #@2e
    const-string v2, "Video"

    #@30
    aput-object v2, v0, v1

    #@32
    const/16 v1, 0x9

    #@34
    const-string v2, "Camera"

    #@36
    aput-object v2, v0, v1

    #@38
    sput-object v0, Landroid/os/Trace;->TRACE_TAGS:[Ljava/lang/String;

    #@3a
    .line 58
    const-wide/high16 v0, -0x8000

    #@3c
    sput-wide v0, Landroid/os/Trace;->sEnabledTags:J

    #@3e
    .line 74
    new-instance v0, Landroid/os/Trace$1;

    #@40
    invoke-direct {v0}, Landroid/os/Trace$1;-><init>()V

    #@43
    invoke-static {v0}, Landroid/os/SystemProperties;->addChangeCallback(Ljava/lang/Runnable;)V

    #@46
    .line 79
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    return-void
.end method

.method static synthetic access$000()J
    .registers 2

    #@0
    .prologue
    .line 31
    invoke-static {}, Landroid/os/Trace;->cacheEnabledTags()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method private static cacheEnabledTags()J
    .registers 5

    #@0
    .prologue
    .line 99
    invoke-static {}, Landroid/os/Trace;->nativeGetEnabledTags()J

    #@3
    move-result-wide v0

    #@4
    .line 101
    .local v0, tags:J
    const-wide/high16 v2, -0x8000

    #@6
    cmp-long v2, v0, v2

    #@8
    if-nez v2, :cond_22

    #@a
    .line 102
    const-string v2, "Trace"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Unexpected value from nativeGetEnabledTags: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 105
    :cond_22
    sput-wide v0, Landroid/os/Trace;->sEnabledTags:J

    #@24
    .line 106
    return-wide v0
.end method

.method public static isTagEnabled(J)Z
    .registers 8
    .parameter "traceTag"

    #@0
    .prologue
    .line 116
    sget-wide v0, Landroid/os/Trace;->sEnabledTags:J

    #@2
    .line 117
    .local v0, tags:J
    const-wide/high16 v2, -0x8000

    #@4
    cmp-long v2, v0, v2

    #@6
    if-nez v2, :cond_c

    #@8
    .line 118
    invoke-static {}, Landroid/os/Trace;->cacheEnabledTags()J

    #@b
    move-result-wide v0

    #@c
    .line 120
    :cond_c
    and-long v2, v0, p0

    #@e
    const-wide/16 v4, 0x0

    #@10
    cmp-long v2, v2, v4

    #@12
    if-eqz v2, :cond_16

    #@14
    const/4 v2, 0x1

    #@15
    :goto_15
    return v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_15
.end method

.method private static native nativeGetEnabledTags()J
.end method

.method private static native nativeTraceBegin(JLjava/lang/String;)V
.end method

.method private static native nativeTraceCounter(JLjava/lang/String;I)V
.end method

.method private static native nativeTraceEnd(J)V
.end method

.method public static traceBegin(JLjava/lang/String;)V
    .registers 4
    .parameter "traceTag"
    .parameter "methodName"

    #@0
    .prologue
    .line 144
    invoke-static {p0, p1}, Landroid/os/Trace;->isTagEnabled(J)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 145
    invoke-static {p0, p1, p2}, Landroid/os/Trace;->nativeTraceBegin(JLjava/lang/String;)V

    #@9
    .line 147
    :cond_9
    return-void
.end method

.method public static traceCounter(JLjava/lang/String;I)V
    .registers 5
    .parameter "traceTag"
    .parameter "counterName"
    .parameter "counterValue"

    #@0
    .prologue
    .line 131
    invoke-static {p0, p1}, Landroid/os/Trace;->isTagEnabled(J)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 132
    invoke-static {p0, p1, p2, p3}, Landroid/os/Trace;->nativeTraceCounter(JLjava/lang/String;I)V

    #@9
    .line 134
    :cond_9
    return-void
.end method

.method public static traceEnd(J)V
    .registers 3
    .parameter "traceTag"

    #@0
    .prologue
    .line 156
    invoke-static {p0, p1}, Landroid/os/Trace;->isTagEnabled(J)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 157
    invoke-static {p0, p1}, Landroid/os/Trace;->nativeTraceEnd(J)V

    #@9
    .line 159
    :cond_9
    return-void
.end method
