.class public final Landroid/os/PowerManager$WakeLock;
.super Ljava/lang/Object;
.source "PowerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/PowerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "WakeLock"
.end annotation


# instance fields
.field private mCount:I

.field private final mFlags:I

.field private mHeld:Z

.field private mRefCounted:Z

.field private final mReleaser:Ljava/lang/Runnable;

.field private final mTag:Ljava/lang/String;

.field private final mToken:Landroid/os/IBinder;

.field private mWorkSource:Landroid/os/WorkSource;

.field final synthetic this$0:Landroid/os/PowerManager;


# direct methods
.method constructor <init>(Landroid/os/PowerManager;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter "flags"
    .parameter "tag"

    #@0
    .prologue
    .line 708
    iput-object p1, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 698
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mRefCounted:Z

    #@8
    .line 702
    new-instance v0, Landroid/os/PowerManager$WakeLock$1;

    #@a
    invoke-direct {v0, p0}, Landroid/os/PowerManager$WakeLock$1;-><init>(Landroid/os/PowerManager$WakeLock;)V

    #@d
    iput-object v0, p0, Landroid/os/PowerManager$WakeLock;->mReleaser:Ljava/lang/Runnable;

    #@f
    .line 709
    iput p2, p0, Landroid/os/PowerManager$WakeLock;->mFlags:I

    #@11
    .line 710
    iput-object p3, p0, Landroid/os/PowerManager$WakeLock;->mTag:Ljava/lang/String;

    #@13
    .line 711
    new-instance v0, Landroid/os/Binder;

    #@15
    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    #@18
    iput-object v0, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@1a
    .line 712
    return-void
.end method

.method private acquireLocked()V
    .registers 6

    #@0
    .prologue
    .line 777
    iget-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mRefCounted:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget v0, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@6
    add-int/lit8 v1, v0, 0x1

    #@8
    iput v1, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@a
    if-nez v0, :cond_27

    #@c
    .line 784
    :cond_c
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@e
    iget-object v0, v0, Landroid/os/PowerManager;->mHandler:Landroid/os/Handler;

    #@10
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mReleaser:Ljava/lang/Runnable;

    #@12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@15
    .line 786
    :try_start_15
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@17
    iget-object v0, v0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@19
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@1b
    iget v2, p0, Landroid/os/PowerManager$WakeLock;->mFlags:I

    #@1d
    iget-object v3, p0, Landroid/os/PowerManager$WakeLock;->mTag:Ljava/lang/String;

    #@1f
    iget-object v4, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@21
    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IPowerManager;->acquireWakeLock(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_24} :catch_28

    #@24
    .line 789
    :goto_24
    const/4 v0, 0x1

    #@25
    iput-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z

    #@27
    .line 791
    :cond_27
    return-void

    #@28
    .line 787
    :catch_28
    move-exception v0

    #@29
    goto :goto_24
.end method


# virtual methods
.method public acquire()V
    .registers 3

    #@0
    .prologue
    .line 754
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 755
    :try_start_3
    invoke-direct {p0}, Landroid/os/PowerManager$WakeLock;->acquireLocked()V

    #@6
    .line 756
    monitor-exit v1

    #@7
    .line 757
    return-void

    #@8
    .line 756
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public acquire(J)V
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 770
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 771
    :try_start_3
    invoke-direct {p0}, Landroid/os/PowerManager$WakeLock;->acquireLocked()V

    #@6
    .line 772
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@8
    iget-object v0, v0, Landroid/os/PowerManager;->mHandler:Landroid/os/Handler;

    #@a
    iget-object v2, p0, Landroid/os/PowerManager$WakeLock;->mReleaser:Ljava/lang/Runnable;

    #@c
    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@f
    .line 773
    monitor-exit v1

    #@10
    .line 774
    return-void

    #@11
    .line 773
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method protected finalize()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 716
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 717
    :try_start_3
    iget-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z

    #@5
    if-eqz v0, :cond_2b

    #@7
    .line 718
    const-string v0, "PowerManager"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "WakeLock finalized while still held: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Landroid/os/PowerManager$WakeLock;->mTag:Ljava/lang/String;

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v0, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_2d

    #@21
    .line 720
    :try_start_21
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@23
    iget-object v0, v0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@25
    iget-object v2, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@27
    const/4 v3, 0x0

    #@28
    invoke-interface {v0, v2, v3}, Landroid/os/IPowerManager;->releaseWakeLock(Landroid/os/IBinder;I)V
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_2d
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_2b} :catch_30

    #@2b
    .line 724
    :cond_2b
    :goto_2b
    :try_start_2b
    monitor-exit v1

    #@2c
    .line 725
    return-void

    #@2d
    .line 724
    :catchall_2d
    move-exception v0

    #@2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2b .. :try_end_2f} :catchall_2d

    #@2f
    throw v0

    #@30
    .line 721
    :catch_30
    move-exception v0

    #@31
    goto :goto_2b
.end method

.method public isHeld()Z
    .registers 3

    #@0
    .prologue
    .line 842
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 843
    :try_start_3
    iget-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 844
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public release()V
    .registers 2

    #@0
    .prologue
    .line 802
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/os/PowerManager$WakeLock;->release(I)V

    #@4
    .line 803
    return-void
.end method

.method public release(I)V
    .registers 6
    .parameter "flags"

    #@0
    .prologue
    .line 819
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 820
    :try_start_3
    iget-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mRefCounted:Z

    #@5
    if-eqz v0, :cond_f

    #@7
    iget v0, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@9
    add-int/lit8 v0, v0, -0x1

    #@b
    iput v0, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@d
    if-nez v0, :cond_28

    #@f
    .line 821
    :cond_f
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@11
    iget-object v0, v0, Landroid/os/PowerManager;->mHandler:Landroid/os/Handler;

    #@13
    iget-object v2, p0, Landroid/os/PowerManager$WakeLock;->mReleaser:Ljava/lang/Runnable;

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@18
    .line 822
    iget-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_47

    #@1a
    if-eqz v0, :cond_28

    #@1c
    .line 824
    :try_start_1c
    iget-object v0, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@1e
    iget-object v0, v0, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@20
    iget-object v2, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@22
    invoke-interface {v0, v2, p1}, Landroid/os/IPowerManager;->releaseWakeLock(Landroid/os/IBinder;I)V
    :try_end_25
    .catchall {:try_start_1c .. :try_end_25} :catchall_47
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_25} :catch_4c

    #@25
    .line 827
    :goto_25
    const/4 v0, 0x0

    #@26
    :try_start_26
    iput-boolean v0, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z

    #@28
    .line 830
    :cond_28
    iget v0, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@2a
    if-gez v0, :cond_4a

    #@2c
    .line 831
    new-instance v0, Ljava/lang/RuntimeException;

    #@2e
    new-instance v2, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v3, "WakeLock under-locked "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    iget-object v3, p0, Landroid/os/PowerManager$WakeLock;->mTag:Ljava/lang/String;

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@46
    throw v0

    #@47
    .line 833
    :catchall_47
    move-exception v0

    #@48
    monitor-exit v1
    :try_end_49
    .catchall {:try_start_26 .. :try_end_49} :catchall_47

    #@49
    throw v0

    #@4a
    :cond_4a
    :try_start_4a
    monitor-exit v1
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_47

    #@4b
    .line 834
    return-void

    #@4c
    .line 825
    :catch_4c
    move-exception v0

    #@4d
    goto :goto_25
.end method

.method public setReferenceCounted(Z)V
    .registers 4
    .parameter "value"

    #@0
    .prologue
    .line 741
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 742
    :try_start_3
    iput-boolean p1, p0, Landroid/os/PowerManager$WakeLock;->mRefCounted:Z

    #@5
    .line 743
    monitor-exit v1

    #@6
    .line 744
    return-void

    #@7
    .line 743
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public setWorkSource(Landroid/os/WorkSource;)V
    .registers 7
    .parameter "ws"

    #@0
    .prologue
    .line 859
    iget-object v2, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v2

    #@3
    .line 860
    if-eqz p1, :cond_c

    #@5
    :try_start_5
    invoke-virtual {p1}, Landroid/os/WorkSource;->size()I

    #@8
    move-result v1

    #@9
    if-nez v1, :cond_c

    #@b
    .line 861
    const/4 p1, 0x0

    #@c
    .line 865
    :cond_c
    if-nez p1, :cond_2b

    #@e
    .line 866
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@10
    if-eqz v1, :cond_29

    #@12
    const/4 v0, 0x1

    #@13
    .line 867
    .local v0, changed:Z
    :goto_13
    const/4 v1, 0x0

    #@14
    iput-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@16
    .line 878
    :cond_16
    :goto_16
    if-eqz v0, :cond_27

    #@18
    iget-boolean v1, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_38

    #@1a
    if-eqz v1, :cond_27

    #@1c
    .line 880
    :try_start_1c
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->this$0:Landroid/os/PowerManager;

    #@1e
    iget-object v1, v1, Landroid/os/PowerManager;->mService:Landroid/os/IPowerManager;

    #@20
    iget-object v3, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@22
    iget-object v4, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@24
    invoke-interface {v1, v3, v4}, Landroid/os/IPowerManager;->updateWakeLockWorkSource(Landroid/os/IBinder;Landroid/os/WorkSource;)V
    :try_end_27
    .catchall {:try_start_1c .. :try_end_27} :catchall_38
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_27} :catch_49

    #@27
    .line 884
    :cond_27
    :goto_27
    :try_start_27
    monitor-exit v2

    #@28
    .line 885
    return-void

    #@29
    .line 866
    .end local v0           #changed:Z
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_13

    #@2b
    .line 868
    :cond_2b
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@2d
    if-nez v1, :cond_3b

    #@2f
    .line 869
    const/4 v0, 0x1

    #@30
    .line 870
    .restart local v0       #changed:Z
    new-instance v1, Landroid/os/WorkSource;

    #@32
    invoke-direct {v1, p1}, Landroid/os/WorkSource;-><init>(Landroid/os/WorkSource;)V

    #@35
    iput-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@37
    goto :goto_16

    #@38
    .line 884
    .end local v0           #changed:Z
    :catchall_38
    move-exception v1

    #@39
    monitor-exit v2
    :try_end_3a
    .catchall {:try_start_27 .. :try_end_3a} :catchall_38

    #@3a
    throw v1

    #@3b
    .line 872
    :cond_3b
    :try_start_3b
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@3d
    invoke-virtual {v1, p1}, Landroid/os/WorkSource;->diff(Landroid/os/WorkSource;)Z

    #@40
    move-result v0

    #@41
    .line 873
    .restart local v0       #changed:Z
    if-eqz v0, :cond_16

    #@43
    .line 874
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mWorkSource:Landroid/os/WorkSource;

    #@45
    invoke-virtual {v1, p1}, Landroid/os/WorkSource;->set(Landroid/os/WorkSource;)V
    :try_end_48
    .catchall {:try_start_3b .. :try_end_48} :catchall_38

    #@48
    goto :goto_16

    #@49
    .line 881
    :catch_49
    move-exception v1

    #@4a
    goto :goto_27
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 889
    iget-object v1, p0, Landroid/os/PowerManager$WakeLock;->mToken:Landroid/os/IBinder;

    #@2
    monitor-enter v1

    #@3
    .line 890
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "WakeLock{"

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    #@11
    move-result v2

    #@12
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    const-string v2, " held="

    #@1c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    iget-boolean v2, p0, Landroid/os/PowerManager$WakeLock;->mHeld:Z

    #@22
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    const-string v2, ", refCount="

    #@28
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v0

    #@2c
    iget v2, p0, Landroid/os/PowerManager$WakeLock;->mCount:I

    #@2e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    const-string/jumbo v2, "}"

    #@35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    monitor-exit v1

    #@3e
    return-object v0

    #@3f
    .line 893
    :catchall_3f
    move-exception v0

    #@40
    monitor-exit v1
    :try_end_41
    .catchall {:try_start_3 .. :try_end_41} :catchall_3f

    #@41
    throw v0
.end method
