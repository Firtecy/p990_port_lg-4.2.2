.class public Landroid/os/DropBoxManager$Entry;
.super Ljava/lang/Object;
.source "DropBoxManager.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/DropBoxManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/DropBoxManager$Entry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mData:[B

.field private final mFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private final mFlags:I

.field private final mTag:Ljava/lang/String;

.field private final mTimeMillis:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 222
    new-instance v0, Landroid/os/DropBoxManager$Entry$1;

    #@2
    invoke-direct {v0}, Landroid/os/DropBoxManager$Entry$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/DropBoxManager$Entry;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .registers 6
    .parameter "tag"
    .parameter "millis"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 99
    if-nez p1, :cond_f

    #@6
    new-instance v0, Ljava/lang/NullPointerException;

    #@8
    const-string/jumbo v1, "tag == null"

    #@b
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@e
    throw v0

    #@f
    .line 101
    :cond_f
    iput-object p1, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@11
    .line 102
    iput-wide p2, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@13
    .line 103
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@15
    .line 104
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@17
    .line 105
    const/4 v0, 0x1

    #@18
    iput v0, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@1a
    .line 106
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLandroid/os/ParcelFileDescriptor;I)V
    .registers 9
    .parameter "tag"
    .parameter "millis"
    .parameter "data"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 141
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 142
    if-nez p1, :cond_10

    #@7
    new-instance v0, Ljava/lang/NullPointerException;

    #@9
    const-string/jumbo v1, "tag == null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 143
    :cond_10
    and-int/lit8 v2, p5, 0x1

    #@12
    if-eqz v2, :cond_32

    #@14
    move v2, v0

    #@15
    :goto_15
    if-nez p4, :cond_34

    #@17
    :goto_17
    if-eq v2, v0, :cond_36

    #@19
    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Bad flags: "

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    :cond_32
    move v2, v1

    #@33
    .line 143
    goto :goto_15

    #@34
    :cond_34
    move v0, v1

    #@35
    goto :goto_17

    #@36
    .line 147
    :cond_36
    iput-object p1, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@38
    .line 148
    iput-wide p2, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@3a
    .line 149
    const/4 v0, 0x0

    #@3b
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@3d
    .line 150
    iput-object p4, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@3f
    .line 151
    iput p5, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@41
    .line 152
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/io/File;I)V
    .registers 9
    .parameter "tag"
    .parameter "millis"
    .parameter "data"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 158
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 159
    if-nez p1, :cond_e

    #@5
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string/jumbo v1, "tag == null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 160
    :cond_e
    and-int/lit8 v0, p5, 0x1

    #@10
    if-eqz v0, :cond_2b

    #@12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "Bad flags: "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v0

    #@2b
    .line 162
    :cond_2b
    iput-object p1, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@2d
    .line 163
    iput-wide p2, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@2f
    .line 164
    const/4 v0, 0x0

    #@30
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@32
    .line 165
    const/high16 v0, 0x1000

    #@34
    invoke-static {p4, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@37
    move-result-object v0

    #@38
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@3a
    .line 166
    iput p5, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@3c
    .line 167
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .registers 7
    .parameter "tag"
    .parameter "millis"
    .parameter "text"

    #@0
    .prologue
    .line 109
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 110
    if-nez p1, :cond_e

    #@5
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string/jumbo v1, "tag == null"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 111
    :cond_e
    if-nez p4, :cond_19

    #@10
    new-instance v0, Ljava/lang/NullPointerException;

    #@12
    const-string/jumbo v1, "text == null"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 113
    :cond_19
    iput-object p1, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@1b
    .line 114
    iput-wide p2, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@1d
    .line 115
    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    #@20
    move-result-object v0

    #@21
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@23
    .line 116
    const/4 v0, 0x0

    #@24
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@26
    .line 117
    const/4 v0, 0x2

    #@27
    iput v0, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@29
    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J[BI)V
    .registers 9
    .parameter "tag"
    .parameter "millis"
    .parameter "data"
    .parameter "flags"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 124
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 125
    if-nez p1, :cond_10

    #@7
    new-instance v0, Ljava/lang/NullPointerException;

    #@9
    const-string/jumbo v1, "tag == null"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 126
    :cond_10
    and-int/lit8 v2, p5, 0x1

    #@12
    if-eqz v2, :cond_32

    #@14
    move v2, v0

    #@15
    :goto_15
    if-nez p4, :cond_34

    #@17
    :goto_17
    if-eq v2, v0, :cond_36

    #@19
    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Bad flags: "

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@31
    throw v0

    #@32
    :cond_32
    move v2, v1

    #@33
    .line 126
    goto :goto_15

    #@34
    :cond_34
    move v0, v1

    #@35
    goto :goto_17

    #@36
    .line 130
    :cond_36
    iput-object p1, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@38
    .line 131
    iput-wide p2, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@3a
    .line 132
    iput-object p4, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@3c
    .line 133
    const/4 v0, 0x0

    #@3d
    iput-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@3f
    .line 134
    iput p5, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@41
    .line 135
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 171
    :try_start_0
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@2
    if-eqz v0, :cond_9

    #@4
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@6
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_9} :catch_a

    #@9
    .line 172
    :cond_9
    :goto_9
    return-void

    #@a
    .line 171
    :catch_a
    move-exception v0

    #@b
    goto :goto_9
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@2
    if-eqz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public getFlags()I
    .registers 2

    #@0
    .prologue
    .line 181
    iget v0, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@2
    and-int/lit8 v0, v0, -0x5

    #@4
    return v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 212
    iget-object v1, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@2
    if-eqz v1, :cond_18

    #@4
    .line 213
    new-instance v0, Ljava/io/ByteArrayInputStream;

    #@6
    iget-object v1, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@8
    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    #@b
    .line 219
    .local v0, is:Ljava/io/InputStream;
    :goto_b
    iget v1, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@d
    and-int/lit8 v1, v1, 0x4

    #@f
    if-eqz v1, :cond_17

    #@11
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    #@13
    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    #@16
    move-object v0, v1

    #@17
    .end local v0           #is:Ljava/io/InputStream;
    :cond_17
    :goto_17
    return-object v0

    #@18
    .line 214
    :cond_18
    iget-object v1, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@1a
    if-eqz v1, :cond_24

    #@1c
    .line 215
    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    #@1e
    iget-object v1, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@20
    invoke-direct {v0, v1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    #@23
    .restart local v0       #is:Ljava/io/InputStream;
    goto :goto_b

    #@24
    .line 217
    .end local v0           #is:Ljava/io/InputStream;
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_17
.end method

.method public getTag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 175
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getText(I)Ljava/lang/String;
    .registers 11
    .parameter "maxBytes"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 188
    iget v6, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@4
    and-int/lit8 v6, v6, 0x2

    #@6
    if-nez v6, :cond_9

    #@8
    .line 205
    :cond_8
    :goto_8
    return-object v5

    #@9
    .line 189
    :cond_9
    iget-object v6, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@b
    if-eqz v6, :cond_1c

    #@d
    new-instance v5, Ljava/lang/String;

    #@f
    iget-object v6, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@11
    iget-object v7, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@13
    array-length v7, v7

    #@14
    invoke-static {p1, v7}, Ljava/lang/Math;->min(II)I

    #@17
    move-result v7

    #@18
    invoke-direct {v5, v6, v8, v7}, Ljava/lang/String;-><init>([BII)V

    #@1b
    goto :goto_8

    #@1c
    .line 191
    :cond_1c
    const/4 v2, 0x0

    #@1d
    .line 193
    .local v2, is:Ljava/io/InputStream;
    :try_start_1d
    invoke-virtual {p0}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_51
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_48

    #@20
    move-result-object v2

    #@21
    .line 194
    if-nez v2, :cond_2b

    #@23
    .line 205
    if-eqz v2, :cond_8

    #@25
    :try_start_25
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_28} :catch_29

    #@28
    goto :goto_8

    #@29
    :catch_29
    move-exception v6

    #@2a
    goto :goto_8

    #@2b
    .line 195
    :cond_2b
    :try_start_2b
    new-array v0, p1, [B

    #@2d
    .line 196
    .local v0, buf:[B
    const/4 v4, 0x0

    #@2e
    .line 197
    .local v4, readBytes:I
    const/4 v3, 0x0

    #@2f
    .line 198
    .local v3, n:I
    :goto_2f
    if-ltz v3, :cond_3b

    #@31
    add-int/2addr v4, v3

    #@32
    if-ge v4, p1, :cond_3b

    #@34
    .line 199
    sub-int v6, p1, v4

    #@36
    invoke-virtual {v2, v0, v4, v6}, Ljava/io/InputStream;->read([BII)I

    #@39
    move-result v3

    #@3a
    goto :goto_2f

    #@3b
    .line 201
    :cond_3b
    new-instance v6, Ljava/lang/String;

    #@3d
    const/4 v7, 0x0

    #@3e
    invoke-direct {v6, v0, v7, v4}, Ljava/lang/String;-><init>([BII)V
    :try_end_41
    .catchall {:try_start_2b .. :try_end_41} :catchall_51
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_41} :catch_48

    #@41
    .line 205
    if-eqz v2, :cond_46

    #@43
    :try_start_43
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_46} :catch_58

    #@46
    :cond_46
    :goto_46
    move-object v5, v6

    #@47
    goto :goto_8

    #@48
    .line 202
    .end local v0           #buf:[B
    .end local v3           #n:I
    .end local v4           #readBytes:I
    :catch_48
    move-exception v1

    #@49
    .line 205
    .local v1, e:Ljava/io/IOException;
    if-eqz v2, :cond_8

    #@4b
    :try_start_4b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_4e} :catch_4f

    #@4e
    goto :goto_8

    #@4f
    :catch_4f
    move-exception v6

    #@50
    goto :goto_8

    #@51
    .end local v1           #e:Ljava/io/IOException;
    :catchall_51
    move-exception v5

    #@52
    if-eqz v2, :cond_57

    #@54
    :try_start_54
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_57} :catch_5a

    #@57
    :cond_57
    :goto_57
    throw v5

    #@58
    .restart local v0       #buf:[B
    .restart local v3       #n:I
    .restart local v4       #readBytes:I
    :catch_58
    move-exception v5

    #@59
    goto :goto_46

    #@5a
    .end local v0           #buf:[B
    .end local v3           #n:I
    .end local v4           #readBytes:I
    :catch_5a
    move-exception v6

    #@5b
    goto :goto_57
.end method

.method public getTimeMillis()J
    .registers 3

    #@0
    .prologue
    .line 178
    iget-wide v0, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@2
    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mTag:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 242
    iget-wide v0, p0, Landroid/os/DropBoxManager$Entry;->mTimeMillis:J

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@a
    .line 243
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@c
    if-eqz v0, :cond_1b

    #@e
    .line 244
    iget v0, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@10
    and-int/lit8 v0, v0, -0x9

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 245
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mFileDescriptor:Landroid/os/ParcelFileDescriptor;

    #@17
    invoke-virtual {v0, p1, p2}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 250
    :goto_1a
    return-void

    #@1b
    .line 247
    :cond_1b
    iget v0, p0, Landroid/os/DropBoxManager$Entry;->mFlags:I

    #@1d
    or-int/lit8 v0, v0, 0x8

    #@1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 248
    iget-object v0, p0, Landroid/os/DropBoxManager$Entry;->mData:[B

    #@24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    #@27
    goto :goto_1a
.end method
