.class final Landroid/os/CancellationSignal$Transport;
.super Landroid/os/ICancellationSignal$Stub;
.source "CancellationSignal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/CancellationSignal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Transport"
.end annotation


# instance fields
.field final mCancellationSignal:Landroid/os/CancellationSignal;


# direct methods
.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 202
    invoke-direct {p0}, Landroid/os/ICancellationSignal$Stub;-><init>()V

    #@3
    .line 203
    new-instance v0, Landroid/os/CancellationSignal;

    #@5
    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    #@8
    iput-object v0, p0, Landroid/os/CancellationSignal$Transport;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@a
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/CancellationSignal$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 202
    invoke-direct {p0}, Landroid/os/CancellationSignal$Transport;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/os/CancellationSignal$Transport;->mCancellationSignal:Landroid/os/CancellationSignal;

    #@2
    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    #@5
    .line 208
    return-void
.end method
