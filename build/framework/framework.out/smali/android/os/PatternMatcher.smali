.class public Landroid/os/PatternMatcher;
.super Ljava/lang/Object;
.source "PatternMatcher.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/PatternMatcher;",
            ">;"
        }
    .end annotation
.end field

.field public static final PATTERN_LITERAL:I = 0x0

.field public static final PATTERN_PREFIX:I = 0x1

.field public static final PATTERN_SIMPLE_GLOB:I = 0x2


# instance fields
.field private final mPattern:Ljava/lang/String;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 98
    new-instance v0, Landroid/os/PatternMatcher$1;

    #@2
    invoke-direct {v0}, Landroid/os/PatternMatcher$1;-><init>()V

    #@5
    sput-object v0, Landroid/os/PatternMatcher;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 93
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@9
    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/os/PatternMatcher;->mType:I

    #@f
    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter "pattern"
    .parameter "type"

    #@0
    .prologue
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-object p1, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@5
    .line 53
    iput p2, p0, Landroid/os/PatternMatcher;->mType:I

    #@7
    .line 54
    return-void
.end method

.method static matchPattern(Ljava/lang/String;Ljava/lang/String;I)Z
    .registers 16
    .parameter "pattern"
    .parameter "match"
    .parameter "type"

    #@0
    .prologue
    const/16 v12, 0x5c

    #@2
    const/16 v11, 0x2a

    #@4
    const/16 v10, 0x2e

    #@6
    const/4 v7, 0x1

    #@7
    const/4 v8, 0x0

    #@8
    .line 110
    if-nez p1, :cond_b

    #@a
    .line 195
    :cond_a
    :goto_a
    return v8

    #@b
    .line 111
    :cond_b
    if-nez p2, :cond_12

    #@d
    .line 112
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v8

    #@11
    goto :goto_a

    #@12
    .line 113
    :cond_12
    if-ne p2, v7, :cond_19

    #@14
    .line 114
    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@17
    move-result v8

    #@18
    goto :goto_a

    #@19
    .line 115
    :cond_19
    const/4 v9, 0x2

    #@1a
    if-ne p2, v9, :cond_a

    #@1c
    .line 119
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@1f
    move-result v1

    #@20
    .line 120
    .local v1, NP:I
    if-gtz v1, :cond_2c

    #@22
    .line 121
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@25
    move-result v9

    #@26
    if-gtz v9, :cond_2a

    #@28
    :goto_28
    move v8, v7

    #@29
    goto :goto_a

    #@2a
    :cond_2a
    move v7, v8

    #@2b
    goto :goto_28

    #@2c
    .line 123
    :cond_2c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2f
    move-result v0

    #@30
    .line 124
    .local v0, NM:I
    const/4 v5, 0x0

    #@31
    .local v5, ip:I
    const/4 v4, 0x0

    #@32
    .line 125
    .local v4, im:I
    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    #@35
    move-result v6

    #@36
    .line 126
    .local v6, nextChar:C
    :goto_36
    if-ge v5, v1, :cond_b0

    #@38
    if-ge v4, v0, :cond_b0

    #@3a
    .line 127
    move v2, v6

    #@3b
    .line 128
    .local v2, c:C
    add-int/lit8 v5, v5, 0x1

    #@3d
    .line 129
    if-ge v5, v1, :cond_5d

    #@3f
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@42
    move-result v6

    #@43
    .line 130
    :goto_43
    if-ne v2, v12, :cond_5f

    #@45
    move v3, v7

    #@46
    .line 131
    .local v3, escaped:Z
    :goto_46
    if-eqz v3, :cond_51

    #@48
    .line 132
    move v2, v6

    #@49
    .line 133
    add-int/lit8 v5, v5, 0x1

    #@4b
    .line 134
    if-ge v5, v1, :cond_61

    #@4d
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@50
    move-result v6

    #@51
    .line 136
    :cond_51
    :goto_51
    if-ne v6, v11, :cond_a5

    #@53
    .line 137
    if-nez v3, :cond_8f

    #@55
    if-ne v2, v10, :cond_8f

    #@57
    .line 138
    add-int/lit8 v9, v1, -0x1

    #@59
    if-lt v5, v9, :cond_63

    #@5b
    move v8, v7

    #@5c
    .line 141
    goto :goto_a

    #@5d
    .end local v3           #escaped:Z
    :cond_5d
    move v6, v8

    #@5e
    .line 129
    goto :goto_43

    #@5f
    :cond_5f
    move v3, v8

    #@60
    .line 130
    goto :goto_46

    #@61
    .restart local v3       #escaped:Z
    :cond_61
    move v6, v8

    #@62
    .line 134
    goto :goto_51

    #@63
    .line 143
    :cond_63
    add-int/lit8 v5, v5, 0x1

    #@65
    .line 144
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@68
    move-result v6

    #@69
    .line 147
    if-ne v6, v12, :cond_73

    #@6b
    .line 148
    add-int/lit8 v5, v5, 0x1

    #@6d
    .line 149
    if-ge v5, v1, :cond_86

    #@6f
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@72
    move-result v6

    #@73
    .line 152
    :cond_73
    :goto_73
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@76
    move-result v9

    #@77
    if-ne v9, v6, :cond_88

    #@79
    .line 157
    :goto_79
    if-eq v4, v0, :cond_a

    #@7b
    .line 162
    add-int/lit8 v5, v5, 0x1

    #@7d
    .line 163
    if-ge v5, v1, :cond_8d

    #@7f
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@82
    move-result v6

    #@83
    .line 164
    :goto_83
    add-int/lit8 v4, v4, 0x1

    #@85
    goto :goto_36

    #@86
    :cond_86
    move v6, v8

    #@87
    .line 149
    goto :goto_73

    #@88
    .line 155
    :cond_88
    add-int/lit8 v4, v4, 0x1

    #@8a
    .line 156
    if-lt v4, v0, :cond_73

    #@8c
    goto :goto_79

    #@8d
    :cond_8d
    move v6, v8

    #@8e
    .line 163
    goto :goto_83

    #@8f
    .line 168
    :cond_8f
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@92
    move-result v9

    #@93
    if-eq v9, v2, :cond_9e

    #@95
    .line 173
    :goto_95
    add-int/lit8 v5, v5, 0x1

    #@97
    .line 174
    if-ge v5, v1, :cond_a3

    #@99
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@9c
    move-result v6

    #@9d
    :goto_9d
    goto :goto_36

    #@9e
    .line 171
    :cond_9e
    add-int/lit8 v4, v4, 0x1

    #@a0
    .line 172
    if-lt v4, v0, :cond_8f

    #@a2
    goto :goto_95

    #@a3
    :cond_a3
    move v6, v8

    #@a4
    .line 174
    goto :goto_9d

    #@a5
    .line 177
    :cond_a5
    if-eq v2, v10, :cond_ad

    #@a7
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@aa
    move-result v9

    #@ab
    if-ne v9, v2, :cond_a

    #@ad
    .line 178
    :cond_ad
    add-int/lit8 v4, v4, 0x1

    #@af
    goto :goto_36

    #@b0
    .line 182
    .end local v2           #c:C
    .end local v3           #escaped:Z
    :cond_b0
    if-lt v5, v1, :cond_b7

    #@b2
    if-lt v4, v0, :cond_b7

    #@b4
    move v8, v7

    #@b5
    .line 184
    goto/16 :goto_a

    #@b7
    .line 190
    :cond_b7
    add-int/lit8 v9, v1, -0x2

    #@b9
    if-ne v5, v9, :cond_a

    #@bb
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    #@be
    move-result v9

    #@bf
    if-ne v9, v10, :cond_a

    #@c1
    add-int/lit8 v9, v5, 0x1

    #@c3
    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    #@c6
    move-result v9

    #@c7
    if-ne v9, v11, :cond_a

    #@c9
    move v8, v7

    #@ca
    .line 192
    goto/16 :goto_a
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 85
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final getPath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public final getType()I
    .registers 2

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/os/PatternMatcher;->mType:I

    #@2
    return v0
.end method

.method public match(Ljava/lang/String;)Z
    .registers 4
    .parameter "str"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@2
    iget v1, p0, Landroid/os/PatternMatcher;->mType:I

    #@4
    invoke-static {v0, p1, v1}, Landroid/os/PatternMatcher;->matchPattern(Ljava/lang/String;Ljava/lang/String;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 69
    const-string v0, "? "

    #@2
    .line 70
    .local v0, type:Ljava/lang/String;
    iget v1, p0, Landroid/os/PatternMatcher;->mType:I

    #@4
    packed-switch v1, :pswitch_data_32

    #@7
    .line 81
    :goto_7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "PatternMatcher{"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    iget-object v2, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string/jumbo v2, "}"

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    return-object v1

    #@28
    .line 72
    :pswitch_28
    const-string v0, "LITERAL: "

    #@2a
    .line 73
    goto :goto_7

    #@2b
    .line 75
    :pswitch_2b
    const-string v0, "PREFIX: "

    #@2d
    .line 76
    goto :goto_7

    #@2e
    .line 78
    :pswitch_2e
    const-string v0, "GLOB: "

    #@30
    goto :goto_7

    #@31
    .line 70
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 89
    iget-object v0, p0, Landroid/os/PatternMatcher;->mPattern:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 90
    iget v0, p0, Landroid/os/PatternMatcher;->mType:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 91
    return-void
.end method
