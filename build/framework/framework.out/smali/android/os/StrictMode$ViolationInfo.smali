.class public Landroid/os/StrictMode$ViolationInfo;
.super Ljava/lang/Object;
.source "StrictMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViolationInfo"
.end annotation


# instance fields
.field public broadcastIntentAction:Ljava/lang/String;

.field public final crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

.field public durationMillis:I

.field public numAnimationsRunning:I

.field public numInstances:J

.field public final policy:I

.field public tags:[Ljava/lang/String;

.field public violationNumThisLoop:I

.field public violationUptimeMillis:J


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2033
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 1991
    const/4 v0, -0x1

    #@5
    iput v0, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@7
    .line 1996
    iput v2, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@9
    .line 2028
    const-wide/16 v0, -0x1

    #@b
    iput-wide v0, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@d
    .line 2034
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@10
    .line 2035
    iput v2, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@12
    .line 2036
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 2094
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/os/StrictMode$ViolationInfo;-><init>(Landroid/os/Parcel;Z)V

    #@4
    .line 2095
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Z)V
    .registers 6
    .parameter "in"
    .parameter "unsetGatheringBit"

    #@0
    .prologue
    .line 2103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1991
    const/4 v1, -0x1

    #@4
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@6
    .line 1996
    const/4 v1, 0x0

    #@7
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@9
    .line 2028
    const-wide/16 v1, -0x1

    #@b
    iput-wide v1, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@d
    .line 2104
    new-instance v1, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@f
    invoke-direct {v1, p1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Landroid/os/Parcel;)V

    #@12
    iput-object v1, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@14
    .line 2105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 2106
    .local v0, rawPolicy:I
    if-eqz p2, :cond_49

    #@1a
    .line 2107
    and-int/lit16 v1, v0, -0x101

    #@1c
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@1e
    .line 2111
    :goto_1e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v1

    #@22
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@24
    .line 2112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v1

    #@28
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    #@2a
    .line 2113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v1

    #@2e
    iput v1, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@30
    .line 2114
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@33
    move-result-wide v1

    #@34
    iput-wide v1, p0, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@36
    .line 2115
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@39
    move-result-wide v1

    #@3a
    iput-wide v1, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@3c
    .line 2116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3f
    move-result-object v1

    #@40
    iput-object v1, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@42
    .line 2117
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    iput-object v1, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@48
    .line 2118
    return-void

    #@49
    .line 2109
    :cond_49
    iput v0, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@4b
    goto :goto_1e
.end method

.method public constructor <init>(Ljava/lang/Throwable;I)V
    .registers 10
    .parameter "tr"
    .parameter "policy"

    #@0
    .prologue
    .line 2041
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1991
    const/4 v5, -0x1

    #@4
    iput v5, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@6
    .line 1996
    const/4 v5, 0x0

    #@7
    iput v5, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@9
    .line 2028
    const-wide/16 v5, -0x1

    #@b
    iput-wide v5, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@d
    .line 2042
    new-instance v5, Landroid/app/ApplicationErrorReport$CrashInfo;

    #@f
    invoke-direct {v5, p1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>(Ljava/lang/Throwable;)V

    #@12
    iput-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@14
    .line 2043
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v5

    #@18
    iput-wide v5, p0, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@1a
    .line 2044
    iput p2, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@1c
    .line 2045
    invoke-static {}, Landroid/animation/ValueAnimator;->getCurrentAnimationsCount()I

    #@1f
    move-result v5

    #@20
    iput v5, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@22
    .line 2046
    invoke-static {}, Landroid/app/ActivityThread;->getIntentBeingBroadcast()Landroid/content/Intent;

    #@25
    move-result-object v0

    #@26
    .line 2047
    .local v0, broadcastIntent:Landroid/content/Intent;
    if-eqz v0, :cond_2e

    #@28
    .line 2048
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    iput-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@2e
    .line 2050
    :cond_2e
    invoke-static {}, Landroid/os/StrictMode;->access$2200()Ljava/lang/ThreadLocal;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    #@35
    move-result-object v4

    #@36
    check-cast v4, Landroid/os/StrictMode$ThreadSpanState;

    #@38
    .line 2051
    .local v4, state:Landroid/os/StrictMode$ThreadSpanState;
    instance-of v5, p1, Landroid/os/StrictMode$InstanceCountViolation;

    #@3a
    if-eqz v5, :cond_42

    #@3c
    .line 2052
    check-cast p1, Landroid/os/StrictMode$InstanceCountViolation;

    #@3e
    .end local p1
    iget-wide v5, p1, Landroid/os/StrictMode$InstanceCountViolation;->mInstances:J

    #@40
    iput-wide v5, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@42
    .line 2054
    :cond_42
    monitor-enter v4

    #@43
    .line 2055
    :try_start_43
    iget v3, v4, Landroid/os/StrictMode$ThreadSpanState;->mActiveSize:I

    #@45
    .line 2056
    .local v3, spanActiveCount:I
    const/16 v5, 0x14

    #@47
    if-le v3, v5, :cond_4b

    #@49
    .line 2057
    const/16 v3, 0x14

    #@4b
    .line 2059
    :cond_4b
    if-eqz v3, :cond_67

    #@4d
    .line 2060
    new-array v5, v3, [Ljava/lang/String;

    #@4f
    iput-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@51
    .line 2061
    iget-object v2, v4, Landroid/os/StrictMode$ThreadSpanState;->mActiveHead:Landroid/os/StrictMode$Span;

    #@53
    .line 2062
    .local v2, iter:Landroid/os/StrictMode$Span;
    const/4 v1, 0x0

    #@54
    .line 2063
    .local v1, index:I
    :goto_54
    if-eqz v2, :cond_67

    #@56
    if-ge v1, v3, :cond_67

    #@58
    .line 2064
    iget-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@5a
    invoke-static {v2}, Landroid/os/StrictMode$Span;->access$1900(Landroid/os/StrictMode$Span;)Ljava/lang/String;

    #@5d
    move-result-object v6

    #@5e
    aput-object v6, v5, v1

    #@60
    .line 2065
    add-int/lit8 v1, v1, 0x1

    #@62
    .line 2066
    invoke-static {v2}, Landroid/os/StrictMode$Span;->access$1800(Landroid/os/StrictMode$Span;)Landroid/os/StrictMode$Span;

    #@65
    move-result-object v2

    #@66
    goto :goto_54

    #@67
    .line 2069
    .end local v1           #index:I
    .end local v2           #iter:Landroid/os/StrictMode$Span;
    :cond_67
    monitor-exit v4

    #@68
    .line 2070
    return-void

    #@69
    .line 2069
    .end local v3           #spanActiveCount:I
    :catchall_69
    move-exception v5

    #@6a
    monitor-exit v4
    :try_end_6b
    .catchall {:try_start_43 .. :try_end_6b} :catchall_69

    #@6b
    throw v5
.end method


# virtual methods
.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .registers 13
    .parameter "pw"
    .parameter "prefix"

    #@0
    .prologue
    .line 2140
    iget-object v6, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@2
    invoke-virtual {v6, p1, p2}, Landroid/app/ApplicationErrorReport$CrashInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    #@5
    .line 2141
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    const-string/jumbo v7, "policy: "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    iget v7, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@22
    .line 2142
    iget v6, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@24
    const/4 v7, -0x1

    #@25
    if-eq v6, v7, :cond_43

    #@27
    .line 2143
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    const-string v7, "durationMillis: "

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    iget v7, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@43
    .line 2145
    :cond_43
    iget-wide v6, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@45
    const-wide/16 v8, -0x1

    #@47
    cmp-long v6, v6, v8

    #@49
    if-eqz v6, :cond_68

    #@4b
    .line 2146
    new-instance v6, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    const-string/jumbo v7, "numInstances: "

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    iget-wide v7, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@5d
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v6

    #@65
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@68
    .line 2148
    :cond_68
    iget v6, p0, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    #@6a
    if-eqz v6, :cond_89

    #@6c
    .line 2149
    new-instance v6, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    const-string/jumbo v7, "violationNumThisLoop: "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    iget v7, p0, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v6

    #@86
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@89
    .line 2151
    :cond_89
    iget v6, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@8b
    if-eqz v6, :cond_aa

    #@8d
    .line 2152
    new-instance v6, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    const-string/jumbo v7, "numAnimationsRunning: "

    #@99
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v6

    #@9d
    iget v7, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@9f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v6

    #@a7
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@aa
    .line 2154
    :cond_aa
    new-instance v6, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v6

    #@b3
    const-string/jumbo v7, "violationUptimeMillis: "

    #@b6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v6

    #@ba
    iget-wide v7, p0, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@bc
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v6

    #@c0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v6

    #@c4
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@c7
    .line 2155
    iget-object v6, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@c9
    if-eqz v6, :cond_e7

    #@cb
    .line 2156
    new-instance v6, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v6

    #@d4
    const-string v7, "broadcastIntentAction: "

    #@d6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v6

    #@da
    iget-object v7, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@dc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v6

    #@e0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v6

    #@e4
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@e7
    .line 2158
    :cond_e7
    iget-object v6, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@e9
    if-eqz v6, :cond_120

    #@eb
    .line 2159
    const/4 v2, 0x0

    #@ec
    .line 2160
    .local v2, index:I
    iget-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@ee
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@ef
    .local v4, len$:I
    const/4 v1, 0x0

    #@f0
    .local v1, i$:I
    move v3, v2

    #@f1
    .end local v2           #index:I
    .local v3, index:I
    :goto_f1
    if-ge v1, v4, :cond_120

    #@f3
    aget-object v5, v0, v1

    #@f5
    .line 2161
    .local v5, tag:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@f7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@fa
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v6

    #@fe
    const-string/jumbo v7, "tag["

    #@101
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v6

    #@105
    add-int/lit8 v2, v3, 0x1

    #@107
    .end local v3           #index:I
    .restart local v2       #index:I
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v6

    #@10b
    const-string v7, "]: "

    #@10d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v6

    #@111
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v6

    #@115
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v6

    #@119
    invoke-interface {p1, v6}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    #@11c
    .line 2160
    add-int/lit8 v1, v1, 0x1

    #@11e
    move v3, v2

    #@11f
    .end local v2           #index:I
    .restart local v3       #index:I
    goto :goto_f1

    #@120
    .line 2164
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v3           #index:I
    .end local v4           #len$:I
    .end local v5           #tag:Ljava/lang/String;
    :cond_120
    return-void
.end method

.method public hashCode()I
    .registers 8

    #@0
    .prologue
    .line 2074
    const/16 v3, 0x11

    #@2
    .line 2075
    .local v3, result:I
    iget-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@4
    iget-object v5, v5, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    #@6
    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    #@9
    move-result v5

    #@a
    add-int/lit16 v3, v5, 0x275

    #@c
    .line 2076
    iget v5, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@e
    if-eqz v5, :cond_12

    #@10
    .line 2077
    mul-int/lit8 v3, v3, 0x25

    #@12
    .line 2079
    :cond_12
    iget-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@14
    if-eqz v5, :cond_20

    #@16
    .line 2080
    mul-int/lit8 v5, v3, 0x25

    #@18
    iget-object v6, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@1a
    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    #@1d
    move-result v6

    #@1e
    add-int v3, v5, v6

    #@20
    .line 2082
    :cond_20
    iget-object v5, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@22
    if-eqz v5, :cond_37

    #@24
    .line 2083
    iget-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@26
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@27
    .local v2, len$:I
    const/4 v1, 0x0

    #@28
    .local v1, i$:I
    :goto_28
    if-ge v1, v2, :cond_37

    #@2a
    aget-object v4, v0, v1

    #@2c
    .line 2084
    .local v4, tag:Ljava/lang/String;
    mul-int/lit8 v5, v3, 0x25

    #@2e
    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    #@31
    move-result v6

    #@32
    add-int v3, v5, v6

    #@34
    .line 2083
    add-int/lit8 v1, v1, 0x1

    #@36
    goto :goto_28

    #@37
    .line 2087
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    .end local v4           #tag:Ljava/lang/String;
    :cond_37
    return v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 2124
    iget-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/app/ApplicationErrorReport$CrashInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@5
    .line 2125
    iget v0, p0, Landroid/os/StrictMode$ViolationInfo;->policy:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 2126
    iget v0, p0, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 2127
    iget v0, p0, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 2128
    iget v0, p0, Landroid/os/StrictMode$ViolationInfo;->numAnimationsRunning:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 2129
    iget-wide v0, p0, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    #@1b
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@1e
    .line 2130
    iget-wide v0, p0, Landroid/os/StrictMode$ViolationInfo;->numInstances:J

    #@20
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    #@23
    .line 2131
    iget-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->broadcastIntentAction:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 2132
    iget-object v0, p0, Landroid/os/StrictMode$ViolationInfo;->tags:[Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@2d
    .line 2133
    return-void
.end method
