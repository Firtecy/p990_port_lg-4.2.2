.class public abstract Landroid/os/IDeviceManager3LM$Stub;
.super Landroid/os/Binder;
.source "IDeviceManager3LM.java"

# interfaces
.implements Landroid/os/IDeviceManager3LM;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/IDeviceManager3LM;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/IDeviceManager3LM$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.IDeviceManager3LM"

.field static final TRANSACTION_addApn:I = 0x34

.field static final TRANSACTION_blockAdb:I = 0x22

.field static final TRANSACTION_blockScreenshot:I = 0x32

.field static final TRANSACTION_blockTethering:I = 0x31

.field static final TRANSACTION_blockUsb:I = 0x27

.field static final TRANSACTION_checkAppUninstallPolicies:I = 0x12

.field static final TRANSACTION_checkPackagePermission:I = 0x8

.field static final TRANSACTION_checkSignature:I = 0x25

.field static final TRANSACTION_clear:I = 0x1

.field static final TRANSACTION_clearApn:I = 0x35

.field static final TRANSACTION_clearApplicationUserData:I = 0x21

.field static final TRANSACTION_clearPackagePermissions:I = 0x2d

.field static final TRANSACTION_completePackageScan:I = 0x30

.field static final TRANSACTION_deletePackage:I = 0x14

.field static final TRANSACTION_disablePackage:I = 0xa

.field static final TRANSACTION_enablePackage:I = 0xb

.field static final TRANSACTION_getBluetoothEnabled:I = 0x6

.field static final TRANSACTION_getMultiUserEnabled:I = 0x38

.field static final TRANSACTION_getNotificationText:I = 0x39

.field static final TRANSACTION_getPackageScanner:I = 0x2f

.field static final TRANSACTION_getVersion:I = 0x2

.field static final TRANSACTION_installPackage:I = 0x13

.field static final TRANSACTION_isAdminLocked:I = 0x29

.field static final TRANSACTION_isPackageDisabled:I = 0xd

.field static final TRANSACTION_isSsidAllowed:I = 0x24

.field static final TRANSACTION_keyStoreChangePassword:I = 0x1c

.field static final TRANSACTION_keyStoreContains:I = 0x1a

.field static final TRANSACTION_keyStoreDeleteKey:I = 0x17

.field static final TRANSACTION_keyStoreGetKey:I = 0x16

.field static final TRANSACTION_keyStorePutKey:I = 0x15

.field static final TRANSACTION_keyStoreReset:I = 0x1b

.field static final TRANSACTION_keyStoreSetPassword:I = 0x18

.field static final TRANSACTION_keyStoreUnlock:I = 0x19

.field static final TRANSACTION_lockAdmin:I = 0x28

.field static final TRANSACTION_lockApn:I = 0x33

.field static final TRANSACTION_notification:I = 0x26

.field static final TRANSACTION_putSettingsSecureInt:I = 0x1e

.field static final TRANSACTION_putSettingsSecureString:I = 0x1d

.field static final TRANSACTION_restoreDefaultApns:I = 0x36

.field static final TRANSACTION_setAllowedPackages:I = 0xc

.field static final TRANSACTION_setAppInstallPermissionPolicies:I = 0xf

.field static final TRANSACTION_setAppInstallPkgNamePolicies:I = 0xe

.field static final TRANSACTION_setAppInstallPubkeyPolicies:I = 0x10

.field static final TRANSACTION_setAppUninstallPkgNamePolicies:I = 0x11

.field static final TRANSACTION_setBluetoothEnabled:I = 0x5

.field static final TRANSACTION_setBootLock:I = 0x3

.field static final TRANSACTION_setLocationProviderEnabled:I = 0x2b

.field static final TRANSACTION_setMultiUserEnabled:I = 0x37

.field static final TRANSACTION_setNfcState:I = 0x1f

.field static final TRANSACTION_setNotificationText:I = 0x7

.field static final TRANSACTION_setOtaDelay:I = 0x2a

.field static final TRANSACTION_setPackagePermission:I = 0x9

.field static final TRANSACTION_setPackageScanner:I = 0x2e

.field static final TRANSACTION_setSecureClipboard:I = 0x2c

.field static final TRANSACTION_setSsidFilter:I = 0x23

.field static final TRANSACTION_setWifiState:I = 0x20

.field static final TRANSACTION_unlock:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.os.IDeviceManager3LM"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/IDeviceManager3LM;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.os.IDeviceManager3LM"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/IDeviceManager3LM;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/os/IDeviceManager3LM;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/os/IDeviceManager3LM$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/IDeviceManager3LM$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 14
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_4fc

    #@5
    .line 597
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 43
    :sswitch_a
    const-string v7, "android.os.IDeviceManager3LM"

    #@c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v7, "android.os.IDeviceManager3LM"

    #@12
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->clear()V

    #@18
    .line 50
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    goto :goto_9

    #@1c
    .line 55
    :sswitch_1c
    const-string v7, "android.os.IDeviceManager3LM"

    #@1e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 56
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->getVersion()I

    #@24
    move-result v4

    #@25
    .line 57
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@28
    .line 58
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@2b
    goto :goto_9

    #@2c
    .line 63
    .end local v4           #_result:I
    :sswitch_2c
    const-string v8, "android.os.IDeviceManager3LM"

    #@2e
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v8

    #@35
    if-eqz v8, :cond_3f

    #@37
    move v0, v6

    #@38
    .line 66
    .local v0, _arg0:Z
    :goto_38
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setBootLock(Z)V

    #@3b
    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_9

    #@3f
    .end local v0           #_arg0:Z
    :cond_3f
    move v0, v7

    #@40
    .line 65
    goto :goto_38

    #@41
    .line 72
    :sswitch_41
    const-string v7, "android.os.IDeviceManager3LM"

    #@43
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 73
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->unlock()V

    #@49
    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4c
    goto :goto_9

    #@4d
    .line 79
    :sswitch_4d
    const-string v8, "android.os.IDeviceManager3LM"

    #@4f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@52
    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@55
    move-result v8

    #@56
    if-eqz v8, :cond_60

    #@58
    move v0, v6

    #@59
    .line 82
    .restart local v0       #_arg0:Z
    :goto_59
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setBluetoothEnabled(Z)V

    #@5c
    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5f
    goto :goto_9

    #@60
    .end local v0           #_arg0:Z
    :cond_60
    move v0, v7

    #@61
    .line 81
    goto :goto_59

    #@62
    .line 88
    :sswitch_62
    const-string v8, "android.os.IDeviceManager3LM"

    #@64
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@67
    .line 89
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->getBluetoothEnabled()Z

    #@6a
    move-result v4

    #@6b
    .line 90
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6e
    .line 91
    if-eqz v4, :cond_71

    #@70
    move v7, v6

    #@71
    :cond_71
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    goto :goto_9

    #@75
    .line 96
    .end local v4           #_result:Z
    :sswitch_75
    const-string v7, "android.os.IDeviceManager3LM"

    #@77
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a
    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7d
    move-result-object v0

    #@7e
    .line 99
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setNotificationText(Ljava/lang/String;)V

    #@81
    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@84
    goto :goto_9

    #@85
    .line 105
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_85
    const-string v8, "android.os.IDeviceManager3LM"

    #@87
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8a
    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    .line 109
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@91
    move-result-object v2

    #@92
    .line 110
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->checkPackagePermission(Ljava/lang/String;Ljava/lang/String;)Z

    #@95
    move-result v4

    #@96
    .line 111
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@99
    .line 112
    if-eqz v4, :cond_9c

    #@9b
    move v7, v6

    #@9c
    :cond_9c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@9f
    goto/16 :goto_9

    #@a1
    .line 117
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_a1
    const-string v8, "android.os.IDeviceManager3LM"

    #@a3
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a9
    move-result-object v0

    #@aa
    .line 121
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ad
    move-result-object v2

    #@ae
    .line 123
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b1
    move-result v8

    #@b2
    if-eqz v8, :cond_c4

    #@b4
    move v3, v6

    #@b5
    .line 124
    .local v3, _arg2:Z
    :goto_b5
    invoke-virtual {p0, v0, v2, v3}, Landroid/os/IDeviceManager3LM$Stub;->setPackagePermission(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@b8
    move-result v4

    #@b9
    .line 125
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@bc
    .line 126
    if-eqz v4, :cond_bf

    #@be
    move v7, v6

    #@bf
    :cond_bf
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@c2
    goto/16 :goto_9

    #@c4
    .end local v3           #_arg2:Z
    .end local v4           #_result:Z
    :cond_c4
    move v3, v7

    #@c5
    .line 123
    goto :goto_b5

    #@c6
    .line 131
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_c6
    const-string v7, "android.os.IDeviceManager3LM"

    #@c8
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cb
    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ce
    move-result-object v0

    #@cf
    .line 134
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->disablePackage(Ljava/lang/String;)V

    #@d2
    .line 135
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d5
    goto/16 :goto_9

    #@d7
    .line 140
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_d7
    const-string v7, "android.os.IDeviceManager3LM"

    #@d9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@df
    move-result-object v0

    #@e0
    .line 143
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->enablePackage(Ljava/lang/String;)V

    #@e3
    .line 144
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e6
    goto/16 :goto_9

    #@e8
    .line 149
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_e8
    const-string v8, "android.os.IDeviceManager3LM"

    #@ea
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ed
    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@f0
    move-result-object v8

    #@f1
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@f4
    move-result-object v5

    #@f5
    .line 152
    .local v5, cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@f8
    move-result-object v0

    #@f9
    .line 153
    .local v0, _arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setAllowedPackages(Ljava/util/Map;)Z

    #@fc
    move-result v4

    #@fd
    .line 154
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@100
    .line 155
    if-eqz v4, :cond_103

    #@102
    move v7, v6

    #@103
    :cond_103
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@106
    goto/16 :goto_9

    #@108
    .line 160
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v4           #_result:Z
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_108
    const-string v8, "android.os.IDeviceManager3LM"

    #@10a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10d
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@110
    move-result-object v0

    #@111
    .line 163
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->isPackageDisabled(Ljava/lang/String;)Z

    #@114
    move-result v4

    #@115
    .line 164
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@118
    .line 165
    if-eqz v4, :cond_11b

    #@11a
    move v7, v6

    #@11b
    :cond_11b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@11e
    goto/16 :goto_9

    #@120
    .line 170
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_120
    const-string v8, "android.os.IDeviceManager3LM"

    #@122
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@125
    .line 172
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@128
    move-result-object v8

    #@129
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@12c
    move-result-object v5

    #@12d
    .line 173
    .restart local v5       #cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@130
    move-result-object v0

    #@131
    .line 174
    .local v0, _arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setAppInstallPkgNamePolicies(Ljava/util/Map;)Z

    #@134
    move-result v4

    #@135
    .line 175
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@138
    .line 176
    if-eqz v4, :cond_13b

    #@13a
    move v7, v6

    #@13b
    :cond_13b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@13e
    goto/16 :goto_9

    #@140
    .line 181
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v4           #_result:Z
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_140
    const-string v8, "android.os.IDeviceManager3LM"

    #@142
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@145
    .line 183
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@148
    move-result-object v8

    #@149
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@14c
    move-result-object v5

    #@14d
    .line 184
    .restart local v5       #cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@150
    move-result-object v0

    #@151
    .line 185
    .restart local v0       #_arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setAppInstallPermissionPolicies(Ljava/util/Map;)Z

    #@154
    move-result v4

    #@155
    .line 186
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@158
    .line 187
    if-eqz v4, :cond_15b

    #@15a
    move v7, v6

    #@15b
    :cond_15b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@15e
    goto/16 :goto_9

    #@160
    .line 192
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v4           #_result:Z
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_160
    const-string v8, "android.os.IDeviceManager3LM"

    #@162
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@165
    .line 194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@168
    move-result-object v8

    #@169
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@16c
    move-result-object v5

    #@16d
    .line 195
    .restart local v5       #cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@170
    move-result-object v0

    #@171
    .line 196
    .restart local v0       #_arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setAppInstallPubkeyPolicies(Ljava/util/Map;)Z

    #@174
    move-result v4

    #@175
    .line 197
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@178
    .line 198
    if-eqz v4, :cond_17b

    #@17a
    move v7, v6

    #@17b
    :cond_17b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@17e
    goto/16 :goto_9

    #@180
    .line 203
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v4           #_result:Z
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_180
    const-string v8, "android.os.IDeviceManager3LM"

    #@182
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@185
    .line 205
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@188
    move-result-object v8

    #@189
    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@18c
    move-result-object v5

    #@18d
    .line 206
    .restart local v5       #cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@190
    move-result-object v0

    #@191
    .line 207
    .restart local v0       #_arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setAppUninstallPkgNamePolicies(Ljava/util/Map;)Z

    #@194
    move-result v4

    #@195
    .line 208
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@198
    .line 209
    if-eqz v4, :cond_19b

    #@19a
    move v7, v6

    #@19b
    :cond_19b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@19e
    goto/16 :goto_9

    #@1a0
    .line 214
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v4           #_result:Z
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_1a0
    const-string v8, "android.os.IDeviceManager3LM"

    #@1a2
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1a5
    .line 216
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1a8
    move-result-object v0

    #@1a9
    .line 217
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->checkAppUninstallPolicies(Ljava/lang/String;)Z

    #@1ac
    move-result v4

    #@1ad
    .line 218
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b0
    .line 219
    if-eqz v4, :cond_1b3

    #@1b2
    move v7, v6

    #@1b3
    :cond_1b3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1b6
    goto/16 :goto_9

    #@1b8
    .line 224
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_1b8
    const-string v7, "android.os.IDeviceManager3LM"

    #@1ba
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1bd
    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c0
    move-result-object v0

    #@1c1
    .line 227
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->installPackage(Ljava/lang/String;)V

    #@1c4
    .line 228
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1c7
    goto/16 :goto_9

    #@1c9
    .line 233
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_1c9
    const-string v8, "android.os.IDeviceManager3LM"

    #@1cb
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ce
    .line 235
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d1
    move-result-object v0

    #@1d2
    .line 237
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d5
    move-result v8

    #@1d6
    if-eqz v8, :cond_1e1

    #@1d8
    move v2, v6

    #@1d9
    .line 238
    .local v2, _arg1:Z
    :goto_1d9
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->deletePackage(Ljava/lang/String;Z)V

    #@1dc
    .line 239
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1df
    goto/16 :goto_9

    #@1e1
    .end local v2           #_arg1:Z
    :cond_1e1
    move v2, v7

    #@1e2
    .line 237
    goto :goto_1d9

    #@1e3
    .line 244
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_1e3
    const-string v8, "android.os.IDeviceManager3LM"

    #@1e5
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1e8
    .line 246
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1eb
    move-result-object v0

    #@1ec
    .line 248
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1ef
    move-result-object v2

    #@1f0
    .line 249
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->keyStorePutKey(Ljava/lang/String;Ljava/lang/String;)Z

    #@1f3
    move-result v4

    #@1f4
    .line 250
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f7
    .line 251
    if-eqz v4, :cond_1fa

    #@1f9
    move v7, v6

    #@1fa
    :cond_1fa
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@1fd
    goto/16 :goto_9

    #@1ff
    .line 256
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_1ff
    const-string v7, "android.os.IDeviceManager3LM"

    #@201
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@204
    .line 258
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@207
    move-result-object v0

    #@208
    .line 259
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreGetKey(Ljava/lang/String;)Ljava/lang/String;

    #@20b
    move-result-object v4

    #@20c
    .line 260
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@20f
    .line 261
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@212
    goto/16 :goto_9

    #@214
    .line 266
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Ljava/lang/String;
    :sswitch_214
    const-string v8, "android.os.IDeviceManager3LM"

    #@216
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@219
    .line 268
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@21c
    move-result-object v0

    #@21d
    .line 269
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreDeleteKey(Ljava/lang/String;)Z

    #@220
    move-result v4

    #@221
    .line 270
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@224
    .line 271
    if-eqz v4, :cond_227

    #@226
    move v7, v6

    #@227
    :cond_227
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@22a
    goto/16 :goto_9

    #@22c
    .line 276
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_22c
    const-string v7, "android.os.IDeviceManager3LM"

    #@22e
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@231
    .line 278
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@234
    move-result-object v0

    #@235
    .line 279
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreSetPassword(Ljava/lang/String;)V

    #@238
    .line 280
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23b
    goto/16 :goto_9

    #@23d
    .line 285
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_23d
    const-string v8, "android.os.IDeviceManager3LM"

    #@23f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@242
    .line 287
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@245
    move-result-object v0

    #@246
    .line 288
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreUnlock(Ljava/lang/String;)Z

    #@249
    move-result v4

    #@24a
    .line 289
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@24d
    .line 290
    if-eqz v4, :cond_250

    #@24f
    move v7, v6

    #@250
    :cond_250
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@253
    goto/16 :goto_9

    #@255
    .line 295
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_255
    const-string v8, "android.os.IDeviceManager3LM"

    #@257
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25a
    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@25d
    move-result-object v0

    #@25e
    .line 298
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreContains(Ljava/lang/String;)Z

    #@261
    move-result v4

    #@262
    .line 299
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@265
    .line 300
    if-eqz v4, :cond_268

    #@267
    move v7, v6

    #@268
    :cond_268
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@26b
    goto/16 :goto_9

    #@26d
    .line 305
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_26d
    const-string v8, "android.os.IDeviceManager3LM"

    #@26f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@272
    .line 306
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreReset()Z

    #@275
    move-result v4

    #@276
    .line 307
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@279
    .line 308
    if-eqz v4, :cond_27c

    #@27b
    move v7, v6

    #@27c
    :cond_27c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@27f
    goto/16 :goto_9

    #@281
    .line 313
    .end local v4           #_result:Z
    :sswitch_281
    const-string v8, "android.os.IDeviceManager3LM"

    #@283
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@286
    .line 315
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@289
    move-result-object v0

    #@28a
    .line 317
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28d
    move-result-object v2

    #@28e
    .line 318
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->keyStoreChangePassword(Ljava/lang/String;Ljava/lang/String;)Z

    #@291
    move-result v4

    #@292
    .line 319
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@295
    .line 320
    if-eqz v4, :cond_298

    #@297
    move v7, v6

    #@298
    :cond_298
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@29b
    goto/16 :goto_9

    #@29d
    .line 325
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_29d
    const-string v8, "android.os.IDeviceManager3LM"

    #@29f
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a2
    .line 327
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a5
    move-result-object v0

    #@2a6
    .line 329
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a9
    move-result-object v2

    #@2aa
    .line 330
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->putSettingsSecureString(Ljava/lang/String;Ljava/lang/String;)Z

    #@2ad
    move-result v4

    #@2ae
    .line 331
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b1
    .line 332
    if-eqz v4, :cond_2b4

    #@2b3
    move v7, v6

    #@2b4
    :cond_2b4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@2b7
    goto/16 :goto_9

    #@2b9
    .line 337
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_2b9
    const-string v8, "android.os.IDeviceManager3LM"

    #@2bb
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2be
    .line 339
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c1
    move-result-object v0

    #@2c2
    .line 341
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2c5
    move-result v2

    #@2c6
    .line 342
    .local v2, _arg1:I
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->putSettingsSecureInt(Ljava/lang/String;I)Z

    #@2c9
    move-result v4

    #@2ca
    .line 343
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2cd
    .line 344
    if-eqz v4, :cond_2d0

    #@2cf
    move v7, v6

    #@2d0
    :cond_2d0
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@2d3
    goto/16 :goto_9

    #@2d5
    .line 349
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v4           #_result:Z
    :sswitch_2d5
    const-string v7, "android.os.IDeviceManager3LM"

    #@2d7
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2da
    .line 351
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2dd
    move-result v0

    #@2de
    .line 352
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setNfcState(I)V

    #@2e1
    .line 353
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e4
    goto/16 :goto_9

    #@2e6
    .line 358
    .end local v0           #_arg0:I
    :sswitch_2e6
    const-string v7, "android.os.IDeviceManager3LM"

    #@2e8
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2eb
    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2ee
    move-result v0

    #@2ef
    .line 361
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setWifiState(I)V

    #@2f2
    .line 362
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2f5
    goto/16 :goto_9

    #@2f7
    .line 367
    .end local v0           #_arg0:I
    :sswitch_2f7
    const-string v8, "android.os.IDeviceManager3LM"

    #@2f9
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2fc
    .line 369
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2ff
    move-result-object v0

    #@300
    .line 370
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->clearApplicationUserData(Ljava/lang/String;)Z

    #@303
    move-result v4

    #@304
    .line 371
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@307
    .line 372
    if-eqz v4, :cond_30a

    #@309
    move v7, v6

    #@30a
    :cond_30a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@30d
    goto/16 :goto_9

    #@30f
    .line 377
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_30f
    const-string v8, "android.os.IDeviceManager3LM"

    #@311
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@314
    .line 379
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@317
    move-result v8

    #@318
    if-eqz v8, :cond_323

    #@31a
    move v0, v6

    #@31b
    .line 380
    .local v0, _arg0:Z
    :goto_31b
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->blockAdb(Z)V

    #@31e
    .line 381
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@321
    goto/16 :goto_9

    #@323
    .end local v0           #_arg0:Z
    :cond_323
    move v0, v7

    #@324
    .line 379
    goto :goto_31b

    #@325
    .line 386
    :sswitch_325
    const-string v7, "android.os.IDeviceManager3LM"

    #@327
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@32a
    .line 388
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@32d
    move-result-object v1

    #@32e
    .line 389
    .local v1, _arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v1}, Landroid/os/IDeviceManager3LM$Stub;->setSsidFilter(Ljava/util/List;)V

    #@331
    .line 390
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@334
    goto/16 :goto_9

    #@336
    .line 395
    .end local v1           #_arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_336
    const-string v8, "android.os.IDeviceManager3LM"

    #@338
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@33b
    .line 397
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33e
    move-result-object v0

    #@33f
    .line 398
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->isSsidAllowed(Ljava/lang/String;)Z

    #@342
    move-result v4

    #@343
    .line 399
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@346
    .line 400
    if-eqz v4, :cond_349

    #@348
    move v7, v6

    #@349
    :cond_349
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@34c
    goto/16 :goto_9

    #@34e
    .line 405
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:Z
    :sswitch_34e
    const-string v8, "android.os.IDeviceManager3LM"

    #@350
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@353
    .line 407
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@356
    move-result v0

    #@357
    .line 408
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->checkSignature(I)Z

    #@35a
    move-result v4

    #@35b
    .line 409
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35e
    .line 410
    if-eqz v4, :cond_361

    #@360
    move v7, v6

    #@361
    :cond_361
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@364
    goto/16 :goto_9

    #@366
    .line 415
    .end local v0           #_arg0:I
    .end local v4           #_result:Z
    :sswitch_366
    const-string v7, "android.os.IDeviceManager3LM"

    #@368
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36b
    .line 417
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@36e
    move-result v0

    #@36f
    .line 419
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@372
    move-result v2

    #@373
    .line 421
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@376
    move-result v3

    #@377
    .line 422
    .local v3, _arg2:I
    invoke-virtual {p0, v0, v2, v3}, Landroid/os/IDeviceManager3LM$Stub;->notification(III)V

    #@37a
    .line 423
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@37d
    goto/16 :goto_9

    #@37f
    .line 428
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    :sswitch_37f
    const-string v8, "android.os.IDeviceManager3LM"

    #@381
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@384
    .line 430
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@387
    move-result v8

    #@388
    if-eqz v8, :cond_393

    #@38a
    move v0, v6

    #@38b
    .line 431
    .local v0, _arg0:Z
    :goto_38b
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->blockUsb(Z)V

    #@38e
    .line 432
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@391
    goto/16 :goto_9

    #@393
    .end local v0           #_arg0:Z
    :cond_393
    move v0, v7

    #@394
    .line 430
    goto :goto_38b

    #@395
    .line 437
    :sswitch_395
    const-string v8, "android.os.IDeviceManager3LM"

    #@397
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39a
    .line 439
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@39d
    move-result v8

    #@39e
    if-eqz v8, :cond_3a9

    #@3a0
    move v0, v6

    #@3a1
    .line 440
    .restart local v0       #_arg0:Z
    :goto_3a1
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->lockAdmin(Z)V

    #@3a4
    .line 441
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a7
    goto/16 :goto_9

    #@3a9
    .end local v0           #_arg0:Z
    :cond_3a9
    move v0, v7

    #@3aa
    .line 439
    goto :goto_3a1

    #@3ab
    .line 446
    :sswitch_3ab
    const-string v8, "android.os.IDeviceManager3LM"

    #@3ad
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b0
    .line 447
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->isAdminLocked()Z

    #@3b3
    move-result v4

    #@3b4
    .line 448
    .restart local v4       #_result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b7
    .line 449
    if-eqz v4, :cond_3ba

    #@3b9
    move v7, v6

    #@3ba
    :cond_3ba
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@3bd
    goto/16 :goto_9

    #@3bf
    .line 454
    .end local v4           #_result:Z
    :sswitch_3bf
    const-string v7, "android.os.IDeviceManager3LM"

    #@3c1
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3c4
    .line 456
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3c7
    move-result v0

    #@3c8
    .line 457
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setOtaDelay(I)V

    #@3cb
    .line 458
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3ce
    goto/16 :goto_9

    #@3d0
    .line 463
    .end local v0           #_arg0:I
    :sswitch_3d0
    const-string v8, "android.os.IDeviceManager3LM"

    #@3d2
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3d5
    .line 465
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3d8
    move-result-object v0

    #@3d9
    .line 467
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3dc
    move-result v8

    #@3dd
    if-eqz v8, :cond_3e8

    #@3df
    move v2, v6

    #@3e0
    .line 468
    .local v2, _arg1:Z
    :goto_3e0
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->setLocationProviderEnabled(Ljava/lang/String;Z)V

    #@3e3
    .line 469
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e6
    goto/16 :goto_9

    #@3e8
    .end local v2           #_arg1:Z
    :cond_3e8
    move v2, v7

    #@3e9
    .line 467
    goto :goto_3e0

    #@3ea
    .line 474
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_3ea
    const-string v7, "android.os.IDeviceManager3LM"

    #@3ec
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3ef
    .line 476
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    #@3f2
    move-result-object v1

    #@3f3
    .line 477
    .restart local v1       #_arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v1}, Landroid/os/IDeviceManager3LM$Stub;->setSecureClipboard(Ljava/util/List;)V

    #@3f6
    .line 478
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f9
    goto/16 :goto_9

    #@3fb
    .line 483
    .end local v1           #_arg0:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_3fb
    const-string v7, "android.os.IDeviceManager3LM"

    #@3fd
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@400
    .line 484
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->clearPackagePermissions()V

    #@403
    .line 485
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@406
    goto/16 :goto_9

    #@408
    .line 490
    :sswitch_408
    const-string v8, "android.os.IDeviceManager3LM"

    #@40a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40d
    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@410
    move-result-object v0

    #@411
    .line 494
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@414
    move-result v8

    #@415
    if-eqz v8, :cond_424

    #@417
    move v2, v6

    #@418
    .line 496
    .restart local v2       #_arg1:Z
    :goto_418
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@41b
    move-result v3

    #@41c
    .line 497
    .restart local v3       #_arg2:I
    invoke-virtual {p0, v0, v2, v3}, Landroid/os/IDeviceManager3LM$Stub;->setPackageScanner(Ljava/lang/String;ZI)V

    #@41f
    .line 498
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@422
    goto/16 :goto_9

    #@424
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:I
    :cond_424
    move v2, v7

    #@425
    .line 494
    goto :goto_418

    #@426
    .line 503
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_426
    const-string v7, "android.os.IDeviceManager3LM"

    #@428
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@42b
    .line 504
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->getPackageScanner()[Ljava/lang/String;

    #@42e
    move-result-object v4

    #@42f
    .line 505
    .local v4, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@432
    .line 506
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@435
    goto/16 :goto_9

    #@437
    .line 511
    .end local v4           #_result:[Ljava/lang/String;
    :sswitch_437
    const-string v7, "android.os.IDeviceManager3LM"

    #@439
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43c
    .line 513
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43f
    move-result v0

    #@440
    .line 515
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@443
    move-result v2

    #@444
    .line 516
    .local v2, _arg1:I
    invoke-virtual {p0, v0, v2}, Landroid/os/IDeviceManager3LM$Stub;->completePackageScan(II)V

    #@447
    .line 517
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@44a
    goto/16 :goto_9

    #@44c
    .line 522
    .end local v0           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_44c
    const-string v8, "android.os.IDeviceManager3LM"

    #@44e
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@451
    .line 524
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@454
    move-result v8

    #@455
    if-eqz v8, :cond_460

    #@457
    move v0, v6

    #@458
    .line 525
    .local v0, _arg0:Z
    :goto_458
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->blockTethering(Z)V

    #@45b
    .line 526
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@45e
    goto/16 :goto_9

    #@460
    .end local v0           #_arg0:Z
    :cond_460
    move v0, v7

    #@461
    .line 524
    goto :goto_458

    #@462
    .line 531
    :sswitch_462
    const-string v8, "android.os.IDeviceManager3LM"

    #@464
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@467
    .line 533
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@46a
    move-result v8

    #@46b
    if-eqz v8, :cond_476

    #@46d
    move v0, v6

    #@46e
    .line 534
    .restart local v0       #_arg0:Z
    :goto_46e
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->blockScreenshot(Z)V

    #@471
    .line 535
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@474
    goto/16 :goto_9

    #@476
    .end local v0           #_arg0:Z
    :cond_476
    move v0, v7

    #@477
    .line 533
    goto :goto_46e

    #@478
    .line 540
    :sswitch_478
    const-string v8, "android.os.IDeviceManager3LM"

    #@47a
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@47d
    .line 542
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@480
    move-result v8

    #@481
    if-eqz v8, :cond_48c

    #@483
    move v0, v6

    #@484
    .line 543
    .restart local v0       #_arg0:Z
    :goto_484
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->lockApn(Z)V

    #@487
    .line 544
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48a
    goto/16 :goto_9

    #@48c
    .end local v0           #_arg0:Z
    :cond_48c
    move v0, v7

    #@48d
    .line 542
    goto :goto_484

    #@48e
    .line 549
    :sswitch_48e
    const-string v7, "android.os.IDeviceManager3LM"

    #@490
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@493
    .line 551
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@496
    move-result-object v7

    #@497
    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    #@49a
    move-result-object v5

    #@49b
    .line 552
    .restart local v5       #cl:Ljava/lang/ClassLoader;
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    #@49e
    move-result-object v0

    #@49f
    .line 553
    .local v0, _arg0:Ljava/util/Map;
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->addApn(Ljava/util/Map;)V

    #@4a2
    .line 554
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a5
    goto/16 :goto_9

    #@4a7
    .line 559
    .end local v0           #_arg0:Ljava/util/Map;
    .end local v5           #cl:Ljava/lang/ClassLoader;
    :sswitch_4a7
    const-string v7, "android.os.IDeviceManager3LM"

    #@4a9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4ac
    .line 560
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->clearApn()V

    #@4af
    .line 561
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4b2
    goto/16 :goto_9

    #@4b4
    .line 566
    :sswitch_4b4
    const-string v7, "android.os.IDeviceManager3LM"

    #@4b6
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4b9
    .line 567
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->restoreDefaultApns()V

    #@4bc
    .line 568
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4bf
    goto/16 :goto_9

    #@4c1
    .line 573
    :sswitch_4c1
    const-string v8, "android.os.IDeviceManager3LM"

    #@4c3
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c6
    .line 575
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4c9
    move-result v8

    #@4ca
    if-eqz v8, :cond_4d5

    #@4cc
    move v0, v6

    #@4cd
    .line 576
    .local v0, _arg0:Z
    :goto_4cd
    invoke-virtual {p0, v0}, Landroid/os/IDeviceManager3LM$Stub;->setMultiUserEnabled(Z)V

    #@4d0
    .line 577
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4d3
    goto/16 :goto_9

    #@4d5
    .end local v0           #_arg0:Z
    :cond_4d5
    move v0, v7

    #@4d6
    .line 575
    goto :goto_4cd

    #@4d7
    .line 582
    :sswitch_4d7
    const-string v8, "android.os.IDeviceManager3LM"

    #@4d9
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4dc
    .line 583
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->getMultiUserEnabled()Z

    #@4df
    move-result v4

    #@4e0
    .line 584
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e3
    .line 585
    if-eqz v4, :cond_4e6

    #@4e5
    move v7, v6

    #@4e6
    :cond_4e6
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@4e9
    goto/16 :goto_9

    #@4eb
    .line 590
    .end local v4           #_result:Z
    :sswitch_4eb
    const-string v7, "android.os.IDeviceManager3LM"

    #@4ed
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4f0
    .line 591
    invoke-virtual {p0}, Landroid/os/IDeviceManager3LM$Stub;->getNotificationText()Ljava/lang/String;

    #@4f3
    move-result-object v4

    #@4f4
    .line 592
    .local v4, _result:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4f7
    .line 593
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4fa
    goto/16 :goto_9

    #@4fc
    .line 39
    :sswitch_data_4fc
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_2c
        0x4 -> :sswitch_41
        0x5 -> :sswitch_4d
        0x6 -> :sswitch_62
        0x7 -> :sswitch_75
        0x8 -> :sswitch_85
        0x9 -> :sswitch_a1
        0xa -> :sswitch_c6
        0xb -> :sswitch_d7
        0xc -> :sswitch_e8
        0xd -> :sswitch_108
        0xe -> :sswitch_120
        0xf -> :sswitch_140
        0x10 -> :sswitch_160
        0x11 -> :sswitch_180
        0x12 -> :sswitch_1a0
        0x13 -> :sswitch_1b8
        0x14 -> :sswitch_1c9
        0x15 -> :sswitch_1e3
        0x16 -> :sswitch_1ff
        0x17 -> :sswitch_214
        0x18 -> :sswitch_22c
        0x19 -> :sswitch_23d
        0x1a -> :sswitch_255
        0x1b -> :sswitch_26d
        0x1c -> :sswitch_281
        0x1d -> :sswitch_29d
        0x1e -> :sswitch_2b9
        0x1f -> :sswitch_2d5
        0x20 -> :sswitch_2e6
        0x21 -> :sswitch_2f7
        0x22 -> :sswitch_30f
        0x23 -> :sswitch_325
        0x24 -> :sswitch_336
        0x25 -> :sswitch_34e
        0x26 -> :sswitch_366
        0x27 -> :sswitch_37f
        0x28 -> :sswitch_395
        0x29 -> :sswitch_3ab
        0x2a -> :sswitch_3bf
        0x2b -> :sswitch_3d0
        0x2c -> :sswitch_3ea
        0x2d -> :sswitch_3fb
        0x2e -> :sswitch_408
        0x2f -> :sswitch_426
        0x30 -> :sswitch_437
        0x31 -> :sswitch_44c
        0x32 -> :sswitch_462
        0x33 -> :sswitch_478
        0x34 -> :sswitch_48e
        0x35 -> :sswitch_4a7
        0x36 -> :sswitch_4b4
        0x37 -> :sswitch_4c1
        0x38 -> :sswitch_4d7
        0x39 -> :sswitch_4eb
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
