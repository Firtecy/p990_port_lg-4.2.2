.class public Landroid/os/SystemService;
.super Ljava/lang/Object;
.source "SystemService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/SystemService$State;
    }
.end annotation


# static fields
.field private static sPropertyLock:Ljava/lang/Object;

.field private static sStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/SystemService$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    #@3
    move-result-object v0

    #@4
    sput-object v0, Landroid/os/SystemService;->sStates:Ljava/util/HashMap;

    #@6
    .line 49
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    sput-object v0, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@d
    .line 52
    new-instance v0, Landroid/os/SystemService$1;

    #@f
    invoke-direct {v0}, Landroid/os/SystemService$1;-><init>()V

    #@12
    invoke-static {v0}, Landroid/os/SystemProperties;->addChangeCallback(Ljava/lang/Runnable;)V

    #@15
    .line 60
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .registers 1

    #@0
    .prologue
    .line 31
    sget-object v0, Landroid/os/SystemService;->sStates:Ljava/util/HashMap;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .registers 1

    #@0
    .prologue
    .line 31
    sget-object v0, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public static getState(Ljava/lang/String;)Landroid/os/SystemService$State;
    .registers 5
    .parameter "service"

    #@0
    .prologue
    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "init.svc."

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 82
    .local v0, rawState:Ljava/lang/String;
    sget-object v2, Landroid/os/SystemService;->sStates:Ljava/util/HashMap;

    #@19
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1c
    move-result-object v1

    #@1d
    check-cast v1, Landroid/os/SystemService$State;

    #@1f
    .line 83
    .local v1, state:Landroid/os/SystemService$State;
    if-eqz v1, :cond_22

    #@21
    .line 86
    .end local v1           #state:Landroid/os/SystemService$State;
    :goto_21
    return-object v1

    #@22
    .restart local v1       #state:Landroid/os/SystemService$State;
    :cond_22
    sget-object v1, Landroid/os/SystemService$State;->STOPPED:Landroid/os/SystemService$State;

    #@24
    goto :goto_21
.end method

.method public static isRunning(Ljava/lang/String;)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 101
    sget-object v0, Landroid/os/SystemService$State;->RUNNING:Landroid/os/SystemService$State;

    #@2
    invoke-static {p0}, Landroid/os/SystemService;->getState(Ljava/lang/String;)Landroid/os/SystemService$State;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/os/SystemService$State;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public static isStopped(Ljava/lang/String;)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 94
    sget-object v0, Landroid/os/SystemService$State;->STOPPED:Landroid/os/SystemService$State;

    #@2
    invoke-static {p0}, Landroid/os/SystemService;->getState(Ljava/lang/String;)Landroid/os/SystemService$State;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/os/SystemService$State;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public static restart(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 74
    const-string v0, "ctl.restart"

    #@2
    invoke-static {v0, p0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 75
    return-void
.end method

.method public static start(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 64
    const-string v0, "ctl.start"

    #@2
    invoke-static {v0, p0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 65
    return-void
.end method

.method public static stop(Ljava/lang/String;)V
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 69
    const-string v0, "ctl.stop"

    #@2
    invoke-static {v0, p0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@5
    .line 70
    return-void
.end method

.method public static varargs waitForAnyStopped([Ljava/lang/String;)V
    .registers 8
    .parameter "services"

    #@0
    .prologue
    .line 135
    :goto_0
    sget-object v5, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@2
    monitor-enter v5

    #@3
    .line 136
    move-object v0, p0

    #@4
    .local v0, arr$:[Ljava/lang/String;
    :try_start_4
    array-length v2, v0

    #@5
    .local v2, len$:I
    const/4 v1, 0x0

    #@6
    .local v1, i$:I
    :goto_6
    if-ge v1, v2, :cond_1b

    #@8
    aget-object v3, v0, v1

    #@a
    .line 137
    .local v3, service:Ljava/lang/String;
    sget-object v4, Landroid/os/SystemService$State;->STOPPED:Landroid/os/SystemService$State;

    #@c
    invoke-static {v3}, Landroid/os/SystemService;->getState(Ljava/lang/String;)Landroid/os/SystemService$State;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v4, v6}, Landroid/os/SystemService$State;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v4

    #@14
    if-eqz v4, :cond_18

    #@16
    .line 138
    monitor-exit v5
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_22

    #@17
    return-void

    #@18
    .line 136
    :cond_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_6

    #@1b
    .line 143
    .end local v3           #service:Ljava/lang/String;
    :cond_1b
    :try_start_1b
    sget-object v4, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@1d
    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_22
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_20} :catch_25

    #@20
    .line 146
    :goto_20
    :try_start_20
    monitor-exit v5

    #@21
    goto :goto_0

    #@22
    .end local v1           #i$:I
    .end local v2           #len$:I
    :catchall_22
    move-exception v4

    #@23
    monitor-exit v5
    :try_end_24
    .catchall {:try_start_20 .. :try_end_24} :catchall_22

    #@24
    throw v4

    #@25
    .line 144
    .restart local v1       #i$:I
    .restart local v2       #len$:I
    :catch_25
    move-exception v4

    #@26
    goto :goto_20
.end method

.method public static waitForState(Ljava/lang/String;Landroid/os/SystemService$State;J)V
    .registers 11
    .parameter "service"
    .parameter "state"
    .parameter "timeoutMillis"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    #@0
    .prologue
    .line 109
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v3

    #@4
    add-long v1, v3, p2

    #@6
    .line 111
    .local v1, endMillis:J
    :goto_6
    sget-object v4, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@8
    monitor-enter v4

    #@9
    .line 112
    :try_start_9
    invoke-static {p0}, Landroid/os/SystemService;->getState(Ljava/lang/String;)Landroid/os/SystemService$State;

    #@c
    move-result-object v0

    #@d
    .line 113
    .local v0, currentState:Landroid/os/SystemService$State;
    invoke-virtual {p1, v0}, Landroid/os/SystemService$State;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v3

    #@11
    if-eqz v3, :cond_15

    #@13
    .line 114
    monitor-exit v4

    #@14
    return-void

    #@15
    .line 117
    :cond_15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@18
    move-result-wide v5

    #@19
    cmp-long v3, v5, v1

    #@1b
    if-ltz v3, :cond_58

    #@1d
    .line 118
    new-instance v3, Ljava/util/concurrent/TimeoutException;

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "Service "

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    const-string v6, " currently "

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    const-string v6, "; waited "

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v5

    #@42
    const-string/jumbo v6, "ms for "

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    invoke-direct {v3, v5}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    #@54
    throw v3

    #@55
    .line 126
    .end local v0           #currentState:Landroid/os/SystemService$State;
    :catchall_55
    move-exception v3

    #@56
    monitor-exit v4
    :try_end_57
    .catchall {:try_start_9 .. :try_end_57} :catchall_55

    #@57
    throw v3

    #@58
    .line 123
    .restart local v0       #currentState:Landroid/os/SystemService$State;
    :cond_58
    :try_start_58
    sget-object v3, Landroid/os/SystemService;->sPropertyLock:Ljava/lang/Object;

    #@5a
    invoke-virtual {v3, p2, p3}, Ljava/lang/Object;->wait(J)V
    :try_end_5d
    .catchall {:try_start_58 .. :try_end_5d} :catchall_55
    .catch Ljava/lang/InterruptedException; {:try_start_58 .. :try_end_5d} :catch_5f

    #@5d
    .line 126
    :goto_5d
    :try_start_5d
    monitor-exit v4
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_55

    #@5e
    goto :goto_6

    #@5f
    .line 124
    :catch_5f
    move-exception v3

    #@60
    goto :goto_5d
.end method
