.class public abstract Landroid/os/IPowerManager$Stub;
.super Landroid/os/Binder;
.source "IPowerManager.java"

# interfaces
.implements Landroid/os/IPowerManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/IPowerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/IPowerManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.os.IPowerManager"

.field static final TRANSACTION_acquireWakeLock:I = 0x1

.field static final TRANSACTION_crash:I = 0xd

.field static final TRANSACTION_getLockState:I = 0x15

.field static final TRANSACTION_getNextTimeout:I = 0x18

.field static final TRANSACTION_getWakeLockFlags:I = 0x1b

.field static final TRANSACTION_goToSleep:I = 0x8

.field static final TRANSACTION_hideLocked:I = 0x14

.field static final TRANSACTION_isLcdOn:I = 0x19

.field static final TRANSACTION_isScreenOn:I = 0xa

.field static final TRANSACTION_isWakeLockLevelSupported:I = 0x5

.field static final TRANSACTION_nap:I = 0x9

.field static final TRANSACTION_perflockBoostExt:I = 0x17

.field static final TRANSACTION_reboot:I = 0xb

.field static final TRANSACTION_recoverBacklightBrightness:I = 0x3

.field static final TRANSACTION_releaseWakeLock:I = 0x2

.field static final TRANSACTION_setAttentionLight:I = 0x12

.field static final TRANSACTION_setBattery:I = 0x16

.field static final TRANSACTION_setMaximumScreenOffTimeoutFromDeviceAdmin:I = 0xf

.field static final TRANSACTION_setStayOnSetting:I = 0xe

.field static final TRANSACTION_setTemporaryScreenAutoBrightnessAdjustmentSettingOverride:I = 0x11

.field static final TRANSACTION_setTemporaryScreenBrightnessSettingOverride:I = 0x10

.field static final TRANSACTION_showLocked:I = 0x13

.field static final TRANSACTION_shutdown:I = 0xc

.field static final TRANSACTION_turnOffThermald:I = 0x1a

.field static final TRANSACTION_updateWakeLockWorkSource:I = 0x4

.field static final TRANSACTION_userActivity:I = 0x6

.field static final TRANSACTION_wakeUp:I = 0x7


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.os.IPowerManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/IPowerManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.os.IPowerManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/os/IPowerManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/os/IPowerManager;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/os/IPowerManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/os/IPowerManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 19
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 39
    sparse-switch p1, :sswitch_data_2e8

    #@3
    .line 338
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v1

    #@7
    :goto_7
    return v1

    #@8
    .line 43
    :sswitch_8
    const-string v1, "android.os.IPowerManager"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 44
    const/4 v1, 0x1

    #@10
    goto :goto_7

    #@11
    .line 48
    :sswitch_11
    const-string v1, "android.os.IPowerManager"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v2

    #@1c
    .line 52
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v3

    #@20
    .line 54
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    .line 56
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_3c

    #@2a
    .line 57
    sget-object v1, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2c
    move-object/from16 v0, p2

    #@2e
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31
    move-result-object v5

    #@32
    check-cast v5, Landroid/os/WorkSource;

    #@34
    .line 62
    .local v5, _arg3:Landroid/os/WorkSource;
    :goto_34
    invoke-virtual {p0, v2, v3, v4, v5}, Landroid/os/IPowerManager$Stub;->acquireWakeLock(Landroid/os/IBinder;ILjava/lang/String;Landroid/os/WorkSource;)V

    #@37
    .line 63
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    .line 64
    const/4 v1, 0x1

    #@3b
    goto :goto_7

    #@3c
    .line 60
    .end local v5           #_arg3:Landroid/os/WorkSource;
    :cond_3c
    const/4 v5, 0x0

    #@3d
    .restart local v5       #_arg3:Landroid/os/WorkSource;
    goto :goto_34

    #@3e
    .line 68
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v5           #_arg3:Landroid/os/WorkSource;
    :sswitch_3e
    const-string v1, "android.os.IPowerManager"

    #@40
    move-object/from16 v0, p2

    #@42
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@45
    .line 70
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@48
    move-result-object v2

    #@49
    .line 72
    .restart local v2       #_arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@4c
    move-result v3

    #@4d
    .line 73
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/os/IPowerManager$Stub;->releaseWakeLock(Landroid/os/IBinder;I)V

    #@50
    .line 74
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@53
    .line 75
    const/4 v1, 0x1

    #@54
    goto :goto_7

    #@55
    .line 79
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:I
    :sswitch_55
    const-string v1, "android.os.IPowerManager"

    #@57
    move-object/from16 v0, p2

    #@59
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 81
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v2

    #@60
    .line 82
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->recoverBacklightBrightness(I)V

    #@63
    .line 83
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@66
    .line 84
    const/4 v1, 0x1

    #@67
    goto :goto_7

    #@68
    .line 88
    .end local v2           #_arg0:I
    :sswitch_68
    const-string v1, "android.os.IPowerManager"

    #@6a
    move-object/from16 v0, p2

    #@6c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6f
    .line 90
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@72
    move-result-object v2

    #@73
    .line 92
    .local v2, _arg0:Landroid/os/IBinder;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@76
    move-result v1

    #@77
    if-eqz v1, :cond_8c

    #@79
    .line 93
    sget-object v1, Landroid/os/WorkSource;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7b
    move-object/from16 v0, p2

    #@7d
    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@80
    move-result-object v3

    #@81
    check-cast v3, Landroid/os/WorkSource;

    #@83
    .line 98
    .local v3, _arg1:Landroid/os/WorkSource;
    :goto_83
    invoke-virtual {p0, v2, v3}, Landroid/os/IPowerManager$Stub;->updateWakeLockWorkSource(Landroid/os/IBinder;Landroid/os/WorkSource;)V

    #@86
    .line 99
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@89
    .line 100
    const/4 v1, 0x1

    #@8a
    goto/16 :goto_7

    #@8c
    .line 96
    .end local v3           #_arg1:Landroid/os/WorkSource;
    :cond_8c
    const/4 v3, 0x0

    #@8d
    .restart local v3       #_arg1:Landroid/os/WorkSource;
    goto :goto_83

    #@8e
    .line 104
    .end local v2           #_arg0:Landroid/os/IBinder;
    .end local v3           #_arg1:Landroid/os/WorkSource;
    :sswitch_8e
    const-string v1, "android.os.IPowerManager"

    #@90
    move-object/from16 v0, p2

    #@92
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@95
    .line 106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@98
    move-result v2

    #@99
    .line 107
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->isWakeLockLevelSupported(I)Z

    #@9c
    move-result v12

    #@9d
    .line 108
    .local v12, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@a0
    .line 109
    if-eqz v12, :cond_ab

    #@a2
    const/4 v1, 0x1

    #@a3
    :goto_a3
    move-object/from16 v0, p3

    #@a5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@a8
    .line 110
    const/4 v1, 0x1

    #@a9
    goto/16 :goto_7

    #@ab
    .line 109
    :cond_ab
    const/4 v1, 0x0

    #@ac
    goto :goto_a3

    #@ad
    .line 114
    .end local v2           #_arg0:I
    .end local v12           #_result:Z
    :sswitch_ad
    const-string v1, "android.os.IPowerManager"

    #@af
    move-object/from16 v0, p2

    #@b1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b4
    .line 116
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@b7
    move-result-wide v10

    #@b8
    .line 118
    .local v10, _arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@bb
    move-result v3

    #@bc
    .line 120
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@bf
    move-result v4

    #@c0
    .line 121
    .local v4, _arg2:I
    invoke-virtual {p0, v10, v11, v3, v4}, Landroid/os/IPowerManager$Stub;->userActivity(JII)V

    #@c3
    .line 122
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@c6
    .line 123
    const/4 v1, 0x1

    #@c7
    goto/16 :goto_7

    #@c9
    .line 127
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v10           #_arg0:J
    :sswitch_c9
    const-string v1, "android.os.IPowerManager"

    #@cb
    move-object/from16 v0, p2

    #@cd
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d0
    .line 129
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@d3
    move-result-wide v10

    #@d4
    .line 130
    .restart local v10       #_arg0:J
    invoke-virtual {p0, v10, v11}, Landroid/os/IPowerManager$Stub;->wakeUp(J)V

    #@d7
    .line 131
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@da
    .line 132
    const/4 v1, 0x1

    #@db
    goto/16 :goto_7

    #@dd
    .line 136
    .end local v10           #_arg0:J
    :sswitch_dd
    const-string v1, "android.os.IPowerManager"

    #@df
    move-object/from16 v0, p2

    #@e1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e4
    .line 138
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@e7
    move-result-wide v10

    #@e8
    .line 140
    .restart local v10       #_arg0:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@eb
    move-result v3

    #@ec
    .line 141
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v10, v11, v3}, Landroid/os/IPowerManager$Stub;->goToSleep(JI)V

    #@ef
    .line 142
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@f2
    .line 143
    const/4 v1, 0x1

    #@f3
    goto/16 :goto_7

    #@f5
    .line 147
    .end local v3           #_arg1:I
    .end local v10           #_arg0:J
    :sswitch_f5
    const-string v1, "android.os.IPowerManager"

    #@f7
    move-object/from16 v0, p2

    #@f9
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fc
    .line 149
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@ff
    move-result-wide v10

    #@100
    .line 150
    .restart local v10       #_arg0:J
    invoke-virtual {p0, v10, v11}, Landroid/os/IPowerManager$Stub;->nap(J)V

    #@103
    .line 151
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@106
    .line 152
    const/4 v1, 0x1

    #@107
    goto/16 :goto_7

    #@109
    .line 156
    .end local v10           #_arg0:J
    :sswitch_109
    const-string v1, "android.os.IPowerManager"

    #@10b
    move-object/from16 v0, p2

    #@10d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@110
    .line 157
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->isScreenOn()Z

    #@113
    move-result v12

    #@114
    .line 158
    .restart local v12       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@117
    .line 159
    if-eqz v12, :cond_122

    #@119
    const/4 v1, 0x1

    #@11a
    :goto_11a
    move-object/from16 v0, p3

    #@11c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@11f
    .line 160
    const/4 v1, 0x1

    #@120
    goto/16 :goto_7

    #@122
    .line 159
    :cond_122
    const/4 v1, 0x0

    #@123
    goto :goto_11a

    #@124
    .line 164
    .end local v12           #_result:Z
    :sswitch_124
    const-string v1, "android.os.IPowerManager"

    #@126
    move-object/from16 v0, p2

    #@128
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12b
    .line 166
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@12e
    move-result v1

    #@12f
    if-eqz v1, :cond_146

    #@131
    const/4 v2, 0x1

    #@132
    .line 168
    .local v2, _arg0:Z
    :goto_132
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@135
    move-result-object v3

    #@136
    .line 170
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@139
    move-result v1

    #@13a
    if-eqz v1, :cond_148

    #@13c
    const/4 v4, 0x1

    #@13d
    .line 171
    .local v4, _arg2:Z
    :goto_13d
    invoke-virtual {p0, v2, v3, v4}, Landroid/os/IPowerManager$Stub;->reboot(ZLjava/lang/String;Z)V

    #@140
    .line 172
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@143
    .line 173
    const/4 v1, 0x1

    #@144
    goto/16 :goto_7

    #@146
    .line 166
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Z
    :cond_146
    const/4 v2, 0x0

    #@147
    goto :goto_132

    #@148
    .line 170
    .restart local v2       #_arg0:Z
    .restart local v3       #_arg1:Ljava/lang/String;
    :cond_148
    const/4 v4, 0x0

    #@149
    goto :goto_13d

    #@14a
    .line 177
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:Ljava/lang/String;
    :sswitch_14a
    const-string v1, "android.os.IPowerManager"

    #@14c
    move-object/from16 v0, p2

    #@14e
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@151
    .line 179
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@154
    move-result v1

    #@155
    if-eqz v1, :cond_168

    #@157
    const/4 v2, 0x1

    #@158
    .line 181
    .restart local v2       #_arg0:Z
    :goto_158
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@15b
    move-result v1

    #@15c
    if-eqz v1, :cond_16a

    #@15e
    const/4 v3, 0x1

    #@15f
    .line 182
    .local v3, _arg1:Z
    :goto_15f
    invoke-virtual {p0, v2, v3}, Landroid/os/IPowerManager$Stub;->shutdown(ZZ)V

    #@162
    .line 183
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@165
    .line 184
    const/4 v1, 0x1

    #@166
    goto/16 :goto_7

    #@168
    .line 179
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:Z
    :cond_168
    const/4 v2, 0x0

    #@169
    goto :goto_158

    #@16a
    .line 181
    .restart local v2       #_arg0:Z
    :cond_16a
    const/4 v3, 0x0

    #@16b
    goto :goto_15f

    #@16c
    .line 188
    .end local v2           #_arg0:Z
    :sswitch_16c
    const-string v1, "android.os.IPowerManager"

    #@16e
    move-object/from16 v0, p2

    #@170
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@173
    .line 190
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@176
    move-result-object v2

    #@177
    .line 191
    .local v2, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->crash(Ljava/lang/String;)V

    #@17a
    .line 192
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@17d
    .line 193
    const/4 v1, 0x1

    #@17e
    goto/16 :goto_7

    #@180
    .line 197
    .end local v2           #_arg0:Ljava/lang/String;
    :sswitch_180
    const-string v1, "android.os.IPowerManager"

    #@182
    move-object/from16 v0, p2

    #@184
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@187
    .line 199
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@18a
    move-result v2

    #@18b
    .line 200
    .local v2, _arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->setStayOnSetting(I)V

    #@18e
    .line 201
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@191
    .line 202
    const/4 v1, 0x1

    #@192
    goto/16 :goto_7

    #@194
    .line 206
    .end local v2           #_arg0:I
    :sswitch_194
    const-string v1, "android.os.IPowerManager"

    #@196
    move-object/from16 v0, p2

    #@198
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19b
    .line 208
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@19e
    move-result v2

    #@19f
    .line 209
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->setMaximumScreenOffTimeoutFromDeviceAdmin(I)V

    #@1a2
    .line 210
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    .line 211
    const/4 v1, 0x1

    #@1a6
    goto/16 :goto_7

    #@1a8
    .line 215
    .end local v2           #_arg0:I
    :sswitch_1a8
    const-string v1, "android.os.IPowerManager"

    #@1aa
    move-object/from16 v0, p2

    #@1ac
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1af
    .line 217
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b2
    move-result v2

    #@1b3
    .line 218
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->setTemporaryScreenBrightnessSettingOverride(I)V

    #@1b6
    .line 219
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b9
    .line 220
    const/4 v1, 0x1

    #@1ba
    goto/16 :goto_7

    #@1bc
    .line 224
    .end local v2           #_arg0:I
    :sswitch_1bc
    const-string v1, "android.os.IPowerManager"

    #@1be
    move-object/from16 v0, p2

    #@1c0
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c3
    .line 226
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    #@1c6
    move-result v2

    #@1c7
    .line 227
    .local v2, _arg0:F
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V

    #@1ca
    .line 228
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cd
    .line 229
    const/4 v1, 0x1

    #@1ce
    goto/16 :goto_7

    #@1d0
    .line 233
    .end local v2           #_arg0:F
    :sswitch_1d0
    const-string v1, "android.os.IPowerManager"

    #@1d2
    move-object/from16 v0, p2

    #@1d4
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d7
    .line 235
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1da
    move-result v1

    #@1db
    if-eqz v1, :cond_1eb

    #@1dd
    const/4 v2, 0x1

    #@1de
    .line 237
    .local v2, _arg0:Z
    :goto_1de
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1e1
    move-result v3

    #@1e2
    .line 238
    .local v3, _arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/os/IPowerManager$Stub;->setAttentionLight(ZI)V

    #@1e5
    .line 239
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e8
    .line 240
    const/4 v1, 0x1

    #@1e9
    goto/16 :goto_7

    #@1eb
    .line 235
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:I
    :cond_1eb
    const/4 v2, 0x0

    #@1ec
    goto :goto_1de

    #@1ed
    .line 244
    :sswitch_1ed
    const-string v1, "android.os.IPowerManager"

    #@1ef
    move-object/from16 v0, p2

    #@1f1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f4
    .line 246
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1f7
    move-result v1

    #@1f8
    if-eqz v1, :cond_20c

    #@1fa
    const/4 v2, 0x1

    #@1fb
    .line 248
    .restart local v2       #_arg0:Z
    :goto_1fb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1fe
    move-result-object v3

    #@1ff
    .line 250
    .local v3, _arg1:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@202
    move-result-object v4

    #@203
    .line 251
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Landroid/os/IPowerManager$Stub;->showLocked(ZLjava/lang/String;Ljava/lang/String;)V

    #@206
    .line 252
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@209
    .line 253
    const/4 v1, 0x1

    #@20a
    goto/16 :goto_7

    #@20c
    .line 246
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    :cond_20c
    const/4 v2, 0x0

    #@20d
    goto :goto_1fb

    #@20e
    .line 257
    :sswitch_20e
    const-string v1, "android.os.IPowerManager"

    #@210
    move-object/from16 v0, p2

    #@212
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@215
    .line 258
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->hideLocked()V

    #@218
    .line 259
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@21b
    .line 260
    const/4 v1, 0x1

    #@21c
    goto/16 :goto_7

    #@21e
    .line 264
    :sswitch_21e
    const-string v1, "android.os.IPowerManager"

    #@220
    move-object/from16 v0, p2

    #@222
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@225
    .line 265
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->getLockState()Z

    #@228
    move-result v12

    #@229
    .line 266
    .restart local v12       #_result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@22c
    .line 267
    if-eqz v12, :cond_237

    #@22e
    const/4 v1, 0x1

    #@22f
    :goto_22f
    move-object/from16 v0, p3

    #@231
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@234
    .line 268
    const/4 v1, 0x1

    #@235
    goto/16 :goto_7

    #@237
    .line 267
    :cond_237
    const/4 v1, 0x0

    #@238
    goto :goto_22f

    #@239
    .line 272
    .end local v12           #_result:Z
    :sswitch_239
    const-string v1, "android.os.IPowerManager"

    #@23b
    move-object/from16 v0, p2

    #@23d
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@240
    .line 274
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@243
    move-result v1

    #@244
    if-eqz v1, :cond_270

    #@246
    const/4 v2, 0x1

    #@247
    .line 276
    .restart local v2       #_arg0:Z
    :goto_247
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@24a
    move-result v3

    #@24b
    .line 278
    .local v3, _arg1:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@24e
    move-result v4

    #@24f
    .line 280
    .local v4, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@252
    move-result v1

    #@253
    if-eqz v1, :cond_272

    #@255
    const/4 v5, 0x1

    #@256
    .line 282
    .local v5, _arg3:Z
    :goto_256
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@259
    move-result v6

    #@25a
    .line 284
    .local v6, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@25d
    move-result v7

    #@25e
    .line 286
    .local v7, _arg5:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@261
    move-result v8

    #@262
    .line 288
    .local v8, _arg6:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@265
    move-result-object v9

    #@266
    .local v9, _arg7:Ljava/lang/String;
    move-object v1, p0

    #@267
    .line 289
    invoke-virtual/range {v1 .. v9}, Landroid/os/IPowerManager$Stub;->setBattery(ZIIZIIILjava/lang/String;)V

    #@26a
    .line 290
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@26d
    .line 291
    const/4 v1, 0x1

    #@26e
    goto/16 :goto_7

    #@270
    .line 274
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Z
    .end local v6           #_arg4:I
    .end local v7           #_arg5:I
    .end local v8           #_arg6:I
    .end local v9           #_arg7:Ljava/lang/String;
    :cond_270
    const/4 v2, 0x0

    #@271
    goto :goto_247

    #@272
    .line 280
    .restart local v2       #_arg0:Z
    .restart local v3       #_arg1:I
    .restart local v4       #_arg2:I
    :cond_272
    const/4 v5, 0x0

    #@273
    goto :goto_256

    #@274
    .line 295
    .end local v2           #_arg0:Z
    .end local v3           #_arg1:I
    .end local v4           #_arg2:I
    :sswitch_274
    const-string v1, "android.os.IPowerManager"

    #@276
    move-object/from16 v0, p2

    #@278
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@27b
    .line 297
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@27e
    move-result v2

    #@27f
    .line 299
    .local v2, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@282
    move-result v3

    #@283
    .line 300
    .restart local v3       #_arg1:I
    invoke-virtual {p0, v2, v3}, Landroid/os/IPowerManager$Stub;->perflockBoostExt(II)V

    #@286
    .line 301
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@289
    .line 302
    const/4 v1, 0x1

    #@28a
    goto/16 :goto_7

    #@28c
    .line 306
    .end local v2           #_arg0:I
    .end local v3           #_arg1:I
    :sswitch_28c
    const-string v1, "android.os.IPowerManager"

    #@28e
    move-object/from16 v0, p2

    #@290
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@293
    .line 307
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->getNextTimeout()J

    #@296
    move-result-wide v12

    #@297
    .line 308
    .local v12, _result:J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@29a
    .line 309
    move-object/from16 v0, p3

    #@29c
    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    #@29f
    .line 310
    const/4 v1, 0x1

    #@2a0
    goto/16 :goto_7

    #@2a2
    .line 314
    .end local v12           #_result:J
    :sswitch_2a2
    const-string v1, "android.os.IPowerManager"

    #@2a4
    move-object/from16 v0, p2

    #@2a6
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2a9
    .line 315
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->isLcdOn()Z

    #@2ac
    move-result v12

    #@2ad
    .line 316
    .local v12, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2b0
    .line 317
    if-eqz v12, :cond_2bb

    #@2b2
    const/4 v1, 0x1

    #@2b3
    :goto_2b3
    move-object/from16 v0, p3

    #@2b5
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2b8
    .line 318
    const/4 v1, 0x1

    #@2b9
    goto/16 :goto_7

    #@2bb
    .line 317
    :cond_2bb
    const/4 v1, 0x0

    #@2bc
    goto :goto_2b3

    #@2bd
    .line 322
    .end local v12           #_result:Z
    :sswitch_2bd
    const-string v1, "android.os.IPowerManager"

    #@2bf
    move-object/from16 v0, p2

    #@2c1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c4
    .line 323
    invoke-virtual {p0}, Landroid/os/IPowerManager$Stub;->turnOffThermald()V

    #@2c7
    .line 324
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ca
    .line 325
    const/4 v1, 0x1

    #@2cb
    goto/16 :goto_7

    #@2cd
    .line 329
    :sswitch_2cd
    const-string v1, "android.os.IPowerManager"

    #@2cf
    move-object/from16 v0, p2

    #@2d1
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d4
    .line 331
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@2d7
    move-result v2

    #@2d8
    .line 332
    .restart local v2       #_arg0:I
    invoke-virtual {p0, v2}, Landroid/os/IPowerManager$Stub;->getWakeLockFlags(I)I

    #@2db
    move-result v12

    #@2dc
    .line 333
    .local v12, _result:I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@2df
    .line 334
    move-object/from16 v0, p3

    #@2e1
    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    #@2e4
    .line 335
    const/4 v1, 0x1

    #@2e5
    goto/16 :goto_7

    #@2e7
    .line 39
    nop

    #@2e8
    :sswitch_data_2e8
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_3e
        0x3 -> :sswitch_55
        0x4 -> :sswitch_68
        0x5 -> :sswitch_8e
        0x6 -> :sswitch_ad
        0x7 -> :sswitch_c9
        0x8 -> :sswitch_dd
        0x9 -> :sswitch_f5
        0xa -> :sswitch_109
        0xb -> :sswitch_124
        0xc -> :sswitch_14a
        0xd -> :sswitch_16c
        0xe -> :sswitch_180
        0xf -> :sswitch_194
        0x10 -> :sswitch_1a8
        0x11 -> :sswitch_1bc
        0x12 -> :sswitch_1d0
        0x13 -> :sswitch_1ed
        0x14 -> :sswitch_20e
        0x15 -> :sswitch_21e
        0x16 -> :sswitch_239
        0x17 -> :sswitch_274
        0x18 -> :sswitch_28c
        0x19 -> :sswitch_2a2
        0x1a -> :sswitch_2bd
        0x1b -> :sswitch_2cd
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
