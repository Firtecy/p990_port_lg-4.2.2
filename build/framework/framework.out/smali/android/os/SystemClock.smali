.class public final Landroid/os/SystemClock;
.super Ljava/lang/Object;
.source "SystemClock.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 97
    return-void
.end method

.method public static native currentThreadTimeMicro()J
.end method

.method public static native currentThreadTimeMillis()J
.end method

.method public static native currentTimeMicro()J
.end method

.method public static native elapsedRealtime()J
.end method

.method public static native elapsedRealtimeNanos()J
.end method

.method public static native setCurrentTimeMillis(J)Z
.end method

.method public static sleep(J)V
    .registers 12
    .parameter "ms"

    #@0
    .prologue
    .line 110
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v4

    #@4
    .line 111
    .local v4, start:J
    move-wide v0, p0

    #@5
    .line 112
    .local v0, duration:J
    const/4 v3, 0x0

    #@6
    .line 115
    .local v3, interrupted:Z
    :cond_6
    :try_start_6
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_9} :catch_21

    #@9
    .line 120
    :goto_9
    add-long v6, v4, p0

    #@b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v8

    #@f
    sub-long v0, v6, v8

    #@11
    .line 121
    const-wide/16 v6, 0x0

    #@13
    cmp-long v6, v0, v6

    #@15
    if-gtz v6, :cond_6

    #@17
    .line 123
    if-eqz v3, :cond_20

    #@19
    .line 127
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@1c
    move-result-object v6

    #@1d
    invoke-virtual {v6}, Ljava/lang/Thread;->interrupt()V

    #@20
    .line 129
    :cond_20
    return-void

    #@21
    .line 117
    :catch_21
    move-exception v2

    #@22
    .line 118
    .local v2, e:Ljava/lang/InterruptedException;
    const/4 v3, 0x1

    #@23
    goto :goto_9
.end method

.method public static native uptimeMillis()J
.end method
