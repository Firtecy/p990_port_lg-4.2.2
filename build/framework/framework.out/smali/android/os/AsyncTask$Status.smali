.class public final enum Landroid/os/AsyncTask$Status;
.super Ljava/lang/Enum;
.source "AsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/os/AsyncTask$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/os/AsyncTask$Status;

.field public static final enum FINISHED:Landroid/os/AsyncTask$Status;

.field public static final enum PENDING:Landroid/os/AsyncTask$Status;

.field public static final enum RUNNING:Landroid/os/AsyncTask$Status;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 256
    new-instance v0, Landroid/os/AsyncTask$Status;

    #@5
    const-string v1, "PENDING"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/os/AsyncTask$Status;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    #@c
    .line 260
    new-instance v0, Landroid/os/AsyncTask$Status;

    #@e
    const-string v1, "RUNNING"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/os/AsyncTask$Status;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    #@15
    .line 264
    new-instance v0, Landroid/os/AsyncTask$Status;

    #@17
    const-string v1, "FINISHED"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/os/AsyncTask$Status;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    #@1e
    .line 252
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/os/AsyncTask$Status;

    #@21
    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/os/AsyncTask$Status;->$VALUES:[Landroid/os/AsyncTask$Status;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/os/AsyncTask$Status;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 252
    const-class v0, Landroid/os/AsyncTask$Status;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/os/AsyncTask$Status;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/os/AsyncTask$Status;
    .registers 1

    #@0
    .prologue
    .line 252
    sget-object v0, Landroid/os/AsyncTask$Status;->$VALUES:[Landroid/os/AsyncTask$Status;

    #@2
    invoke-virtual {v0}, [Landroid/os/AsyncTask$Status;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/os/AsyncTask$Status;

    #@8
    return-object v0
.end method
