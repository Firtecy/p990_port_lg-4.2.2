.class public abstract Landroid/os/ServiceManagerNative;
.super Landroid/os/Binder;
.source "ServiceManagerNative.java"

# interfaces
.implements Landroid/os/IServiceManager;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 49
    const-string v0, "android.os.IServiceManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/os/ServiceManagerNative;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 50
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/os/IServiceManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 35
    if-nez p0, :cond_4

    #@2
    .line 36
    const/4 v0, 0x0

    #@3
    .line 44
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 38
    :cond_4
    const-string v1, "android.os.IServiceManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/os/IServiceManager;

    #@c
    .line 40
    .local v0, in:Landroid/os/IServiceManager;
    if-nez v0, :cond_3

    #@e
    .line 44
    new-instance v0, Landroid/os/ServiceManagerProxy;

    #@10
    .end local v0           #in:Landroid/os/IServiceManager;
    invoke-direct {v0, p0}, Landroid/os/ServiceManagerProxy;-><init>(Landroid/os/IBinder;)V

    #@13
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 105
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 55
    packed-switch p1, :pswitch_data_64

    #@5
    :goto_5
    :pswitch_5
    move v5, v6

    #@6
    .line 100
    :goto_6
    return v5

    #@7
    .line 57
    :pswitch_7
    :try_start_7
    const-string v7, "android.os.IServiceManager"

    #@9
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@c
    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    .line 59
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/os/ServiceManagerNative;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@13
    move-result-object v4

    #@14
    .line 60
    .local v4, service:Landroid/os/IBinder;
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@17
    goto :goto_6

    #@18
    .line 97
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #service:Landroid/os/IBinder;
    :catch_18
    move-exception v5

    #@19
    goto :goto_5

    #@1a
    .line 65
    :pswitch_1a
    const-string v7, "android.os.IServiceManager"

    #@1c
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1f
    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    .line 67
    .restart local v3       #name:Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/os/ServiceManagerNative;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@26
    move-result-object v4

    #@27
    .line 68
    .restart local v4       #service:Landroid/os/IBinder;
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2a
    goto :goto_6

    #@2b
    .line 73
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #service:Landroid/os/IBinder;
    :pswitch_2b
    const-string v7, "android.os.IServiceManager"

    #@2d
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    .line 75
    .restart local v3       #name:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@37
    move-result-object v4

    #@38
    .line 76
    .restart local v4       #service:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3b
    move-result v7

    #@3c
    if-eqz v7, :cond_43

    #@3e
    move v0, v5

    #@3f
    .line 77
    .local v0, allowIsolated:Z
    :goto_3f
    invoke-virtual {p0, v3, v4, v0}, Landroid/os/ServiceManagerNative;->addService(Ljava/lang/String;Landroid/os/IBinder;Z)V

    #@42
    goto :goto_6

    #@43
    .end local v0           #allowIsolated:Z
    :cond_43
    move v0, v6

    #@44
    .line 76
    goto :goto_3f

    #@45
    .line 82
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #service:Landroid/os/IBinder;
    :pswitch_45
    const-string v7, "android.os.IServiceManager"

    #@47
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a
    .line 83
    invoke-virtual {p0}, Landroid/os/ServiceManagerNative;->listServices()[Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    .line 84
    .local v2, list:[Ljava/lang/String;
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@51
    goto :goto_6

    #@52
    .line 89
    .end local v2           #list:[Ljava/lang/String;
    :pswitch_52
    const-string v7, "android.os.IServiceManager"

    #@54
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@57
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@5a
    move-result-object v7

    #@5b
    invoke-static {v7}, Landroid/os/IPermissionController$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPermissionController;

    #@5e
    move-result-object v1

    #@5f
    .line 93
    .local v1, controller:Landroid/os/IPermissionController;
    invoke-virtual {p0, v1}, Landroid/os/ServiceManagerNative;->setPermissionController(Landroid/os/IPermissionController;)V
    :try_end_62
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_62} :catch_18

    #@62
    goto :goto_6

    #@63
    .line 55
    nop

    #@64
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1a
        :pswitch_2b
        :pswitch_45
        :pswitch_5
        :pswitch_52
    .end packed-switch
.end method
