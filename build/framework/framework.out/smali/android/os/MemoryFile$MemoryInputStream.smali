.class Landroid/os/MemoryFile$MemoryInputStream;
.super Ljava/io/InputStream;
.source "MemoryFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/MemoryFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoryInputStream"
.end annotation


# instance fields
.field private mMark:I

.field private mOffset:I

.field private mSingleByte:[B

.field final synthetic this$0:Landroid/os/MemoryFile;


# direct methods
.method private constructor <init>(Landroid/os/MemoryFile;)V
    .registers 3
    .parameter

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 266
    iput-object p1, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@3
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@6
    .line 268
    iput v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mMark:I

    #@8
    .line 269
    iput v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@a
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/MemoryFile;Landroid/os/MemoryFile$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 266
    invoke-direct {p0, p1}, Landroid/os/MemoryFile$MemoryInputStream;-><init>(Landroid/os/MemoryFile;)V

    #@3
    return-void
.end method


# virtual methods
.method public available()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    iget v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@2
    iget-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@4
    invoke-static {v1}, Landroid/os/MemoryFile;->access$200(Landroid/os/MemoryFile;)I

    #@7
    move-result v1

    #@8
    if-lt v0, v1, :cond_c

    #@a
    .line 275
    const/4 v0, 0x0

    #@b
    .line 277
    :goto_b
    return v0

    #@c
    :cond_c
    iget-object v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@e
    invoke-static {v0}, Landroid/os/MemoryFile;->access$200(Landroid/os/MemoryFile;)I

    #@11
    move-result v0

    #@12
    iget v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@14
    sub-int/2addr v0, v1

    #@15
    goto :goto_b
.end method

.method public mark(I)V
    .registers 3
    .parameter "readlimit"

    #@0
    .prologue
    .line 287
    iget v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@2
    iput v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mMark:I

    #@4
    .line 288
    return-void
.end method

.method public markSupported()Z
    .registers 2

    #@0
    .prologue
    .line 282
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 297
    iget-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mSingleByte:[B

    #@4
    if-nez v1, :cond_a

    #@6
    .line 298
    new-array v1, v2, [B

    #@8
    iput-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mSingleByte:[B

    #@a
    .line 300
    :cond_a
    iget-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mSingleByte:[B

    #@c
    invoke-virtual {p0, v1, v3, v2}, Landroid/os/MemoryFile$MemoryInputStream;->read([BII)I

    #@f
    move-result v0

    #@10
    .line 301
    .local v0, result:I
    if-eq v0, v2, :cond_14

    #@12
    .line 302
    const/4 v1, -0x1

    #@13
    .line 304
    :goto_13
    return v1

    #@14
    :cond_14
    iget-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mSingleByte:[B

    #@16
    aget-byte v1, v1, v3

    #@18
    goto :goto_13
.end method

.method public read([BII)I
    .registers 7
    .parameter "buffer"
    .parameter "offset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 309
    if-ltz p2, :cond_9

    #@2
    if-ltz p3, :cond_9

    #@4
    add-int v1, p2, p3

    #@6
    array-length v2, p1

    #@7
    if-le v1, v2, :cond_f

    #@9
    .line 312
    :cond_9
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@b
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@e
    throw v1

    #@f
    .line 314
    :cond_f
    invoke-virtual {p0}, Landroid/os/MemoryFile$MemoryInputStream;->available()I

    #@12
    move-result v1

    #@13
    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    #@16
    move-result p3

    #@17
    .line 315
    const/4 v1, 0x1

    #@18
    if-ge p3, v1, :cond_1c

    #@1a
    .line 316
    const/4 v0, -0x1

    #@1b
    .line 322
    :cond_1b
    :goto_1b
    return v0

    #@1c
    .line 318
    :cond_1c
    iget-object v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@1e
    iget v2, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@20
    invoke-virtual {v1, p1, v2, p2, p3}, Landroid/os/MemoryFile;->readBytes([BIII)I

    #@23
    move-result v0

    #@24
    .line 319
    .local v0, result:I
    if-lez v0, :cond_1b

    #@26
    .line 320
    iget v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@28
    add-int/2addr v1, v0

    #@29
    iput v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@2b
    goto :goto_1b
.end method

.method public reset()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 292
    iget v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mMark:I

    #@2
    iput v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@4
    .line 293
    return-void
.end method

.method public skip(J)J
    .registers 7
    .parameter "n"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 327
    iget v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@2
    int-to-long v0, v0

    #@3
    add-long/2addr v0, p1

    #@4
    iget-object v2, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@6
    invoke-static {v2}, Landroid/os/MemoryFile;->access$200(Landroid/os/MemoryFile;)I

    #@9
    move-result v2

    #@a
    int-to-long v2, v2

    #@b
    cmp-long v0, v0, v2

    #@d
    if-lez v0, :cond_19

    #@f
    .line 328
    iget-object v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->this$0:Landroid/os/MemoryFile;

    #@11
    invoke-static {v0}, Landroid/os/MemoryFile;->access$200(Landroid/os/MemoryFile;)I

    #@14
    move-result v0

    #@15
    iget v1, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@17
    sub-int/2addr v0, v1

    #@18
    int-to-long p1, v0

    #@19
    .line 330
    :cond_19
    iget v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@1b
    int-to-long v0, v0

    #@1c
    add-long/2addr v0, p1

    #@1d
    long-to-int v0, v0

    #@1e
    iput v0, p0, Landroid/os/MemoryFile$MemoryInputStream;->mOffset:I

    #@20
    .line 331
    return-wide p1
.end method
