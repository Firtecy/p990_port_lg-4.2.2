.class public Landroid/os/UserManager;
.super Ljava/lang/Object;
.source "UserManager.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mService:Landroid/os/IUserManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    const-string v0, "UserManager"

    #@2
    sput-object v0, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/IUserManager;)V
    .registers 3
    .parameter "context"
    .parameter "service"

    #@0
    .prologue
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    iput-object p2, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@5
    .line 41
    iput-object p1, p0, Landroid/os/UserManager;->mContext:Landroid/content/Context;

    #@7
    .line 42
    return-void
.end method

.method public static getMaxSupportedUsers()I
    .registers 3

    #@0
    .prologue
    .line 333
    sget-object v0, Landroid/os/Build;->ID:Ljava/lang/String;

    #@2
    const-string v1, "JVP"

    #@4
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    .line 334
    :goto_b
    return v0

    #@c
    :cond_c
    const-string v0, "fw.max_users"

    #@e
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@11
    move-result-object v1

    #@12
    const v2, 0x10e0039

    #@15
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    #@18
    move-result v1

    #@19
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@1c
    move-result v0

    #@1d
    goto :goto_b
.end method

.method public static supportsMultipleUsers()Z
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 50
    invoke-static {}, Landroid/os/UserManager;->getMaxSupportedUsers()I

    #@4
    move-result v1

    #@5
    if-le v1, v0, :cond_8

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method


# virtual methods
.method public createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;
    .registers 6
    .parameter "name"
    .parameter "flags"

    #@0
    .prologue
    .line 175
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/os/IUserManager;->createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 178
    :goto_6
    return-object v1

    #@7
    .line 176
    :catch_7
    move-exception v0

    #@8
    .line 177
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not create a user"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 178
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getSerialNumberForUser(Landroid/os/UserHandle;)J
    .registers 4
    .parameter "user"

    #@0
    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/os/UserManager;->getUserSerialNumber(I)I

    #@7
    move-result v0

    #@8
    int-to-long v0, v0

    #@9
    return-wide v0
.end method

.method public getUserCount()I
    .registers 3

    #@0
    .prologue
    .line 186
    invoke-virtual {p0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    .line 187
    .local v0, users:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    if-eqz v0, :cond_b

    #@6
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@9
    move-result v1

    #@a
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x1

    #@c
    goto :goto_a
.end method

.method public getUserForSerialNumber(J)Landroid/os/UserHandle;
    .registers 5
    .parameter "serialNumber"

    #@0
    .prologue
    .line 158
    long-to-int v1, p1

    #@1
    invoke-virtual {p0, v1}, Landroid/os/UserManager;->getUserHandle(I)I

    #@4
    move-result v0

    #@5
    .line 159
    .local v0, ident:I
    if-ltz v0, :cond_d

    #@7
    new-instance v1, Landroid/os/UserHandle;

    #@9
    invoke-direct {v1, v0}, Landroid/os/UserHandle;-><init>(I)V

    #@c
    :goto_c
    return-object v1

    #@d
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_c
.end method

.method public getUserHandle()I
    .registers 2

    #@0
    .prologue
    .line 59
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getUserHandle(I)I
    .registers 6
    .parameter "userSerialNumber"

    #@0
    .prologue
    .line 365
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->getUserHandle(I)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 369
    :goto_6
    return v1

    #@7
    .line 366
    :catch_7
    move-exception v0

    #@8
    .line 367
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Could not get userHandle for user "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 369
    const/4 v1, -0x1

    #@21
    goto :goto_6
.end method

.method public getUserIcon(I)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 274
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->getUserIcon(I)Landroid/graphics/Bitmap;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 277
    :goto_6
    return-object v1

    #@7
    .line 275
    :catch_7
    move-exception v0

    #@8
    .line 276
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not get the user icon "

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 277
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getUserInfo(I)Landroid/content/pm/UserInfo;
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 128
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 131
    :goto_6
    return-object v1

    #@7
    .line 129
    :catch_7
    move-exception v0

    #@8
    .line 130
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not get user info"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 131
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public getUserName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 70
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-virtual {p0}, Landroid/os/UserManager;->getUserHandle()I

    #@5
    move-result v2

    #@6
    invoke-interface {v1, v2}, Landroid/os/IUserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    #@9
    move-result-object v1

    #@a
    iget-object v1, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_c} :catch_d

    #@c
    .line 73
    :goto_c
    return-object v1

    #@d
    .line 71
    :catch_d
    move-exception v0

    #@e
    .line 72
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@10
    const-string v2, "Could not get user name"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    .line 73
    const-string v1, ""

    #@17
    goto :goto_c
.end method

.method public getUserSerialNumber(I)I
    .registers 6
    .parameter "userHandle"

    #@0
    .prologue
    .line 347
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->getUserSerialNumber(I)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 351
    :goto_6
    return v1

    #@7
    .line 348
    :catch_7
    move-exception v0

    #@8
    .line 349
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Could not get serial number for user "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 351
    const/4 v1, -0x1

    #@21
    goto :goto_6
.end method

.method public getUsers()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 198
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-interface {v1, v2}, Landroid/os/IUserManager;->getUsers(Z)Ljava/util/List;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_8

    #@6
    move-result-object v1

    #@7
    .line 201
    :goto_7
    return-object v1

    #@8
    .line 199
    :catch_8
    move-exception v0

    #@9
    .line 200
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@b
    const-string v2, "Could not get user list"

    #@d
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@10
    .line 201
    const/4 v1, 0x0

    #@11
    goto :goto_7
.end method

.method public getUsers(Z)Ljava/util/List;
    .registers 5
    .parameter "excludeDying"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 214
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->getUsers(Z)Ljava/util/List;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    .line 217
    :goto_6
    return-object v1

    #@7
    .line 215
    :catch_7
    move-exception v0

    #@8
    .line 216
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not get user list"

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 217
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public isGuestEnabled()Z
    .registers 4

    #@0
    .prologue
    .line 304
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1}, Landroid/os/IUserManager;->isGuestEnabled()Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 307
    :goto_6
    return v1

    #@7
    .line 305
    :catch_7
    move-exception v0

    #@8
    .line 306
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not retrieve guest enabled state"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 307
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public isUserAGoat()Z
    .registers 2

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public isUserRunning(Landroid/os/UserHandle;)Z
    .registers 7
    .parameter "user"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 96
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@4
    move-result-object v2

    #@5
    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    #@8
    move-result v3

    #@9
    const/4 v4, 0x0

    #@a
    invoke-interface {v2, v3, v4}, Landroid/app/IActivityManager;->isUserRunning(IZ)Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 99
    :goto_e
    return v1

    #@f
    .line 98
    :catch_f
    move-exception v0

    #@10
    .line 99
    .local v0, e:Landroid/os/RemoteException;
    goto :goto_e
.end method

.method public isUserRunningOrStopping(Landroid/os/UserHandle;)Z
    .registers 6
    .parameter "user"

    #@0
    .prologue
    .line 112
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    #@7
    move-result v2

    #@8
    const/4 v3, 0x1

    #@9
    invoke-interface {v1, v2, v3}, Landroid/app/IActivityManager;->isUserRunning(IZ)Z
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_c} :catch_e

    #@c
    move-result v1

    #@d
    .line 115
    :goto_d
    return v1

    #@e
    .line 114
    :catch_e
    move-exception v0

    #@f
    .line 115
    .local v0, e:Landroid/os/RemoteException;
    const/4 v1, 0x0

    #@10
    goto :goto_d
.end method

.method public removeUser(I)Z
    .registers 5
    .parameter "userHandle"

    #@0
    .prologue
    .line 229
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->removeUser(I)Z
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result v1

    #@6
    .line 232
    :goto_6
    return v1

    #@7
    .line 230
    :catch_7
    move-exception v0

    #@8
    .line 231
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@a
    const-string v2, "Could not remove user "

    #@c
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f
    .line 232
    const/4 v1, 0x0

    #@10
    goto :goto_6
.end method

.method public setGuestEnabled(Z)V
    .registers 6
    .parameter "enable"

    #@0
    .prologue
    .line 290
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->setGuestEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 294
    :goto_5
    return-void

    #@6
    .line 291
    :catch_6
    move-exception v0

    #@7
    .line 292
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Could not change guest account availability to "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_5
.end method

.method public setUserIcon(ILandroid/graphics/Bitmap;)V
    .registers 6
    .parameter "userHandle"
    .parameter "icon"

    #@0
    .prologue
    .line 260
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/os/IUserManager;->setUserIcon(ILandroid/graphics/Bitmap;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 264
    :goto_5
    return-void

    #@6
    .line 261
    :catch_6
    move-exception v0

    #@7
    .line 262
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@9
    const-string v2, "Could not set the user icon "

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public setUserName(ILjava/lang/String;)V
    .registers 6
    .parameter "userHandle"
    .parameter "name"

    #@0
    .prologue
    .line 246
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1, p2}, Landroid/os/IUserManager;->setUserName(ILjava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 250
    :goto_5
    return-void

    #@6
    .line 247
    :catch_6
    move-exception v0

    #@7
    .line 248
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@9
    const-string v2, "Could not set the user name "

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public wipeUser(I)V
    .registers 6
    .parameter "userHandle"

    #@0
    .prologue
    .line 319
    :try_start_0
    iget-object v1, p0, Landroid/os/UserManager;->mService:Landroid/os/IUserManager;

    #@2
    invoke-interface {v1, p1}, Landroid/os/IUserManager;->wipeUser(I)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 323
    :goto_5
    return-void

    #@6
    .line 320
    :catch_6
    move-exception v0

    #@7
    .line 321
    .local v0, re:Landroid/os/RemoteException;
    sget-object v1, Landroid/os/UserManager;->TAG:Ljava/lang/String;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Could not wipe user "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    goto :goto_5
.end method
