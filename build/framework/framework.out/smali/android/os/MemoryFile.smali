.class public Landroid/os/MemoryFile;
.super Ljava/lang/Object;
.source "MemoryFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/MemoryFile$1;,
        Landroid/os/MemoryFile$MemoryOutputStream;,
        Landroid/os/MemoryFile$MemoryInputStream;
    }
.end annotation


# static fields
.field private static final PROT_READ:I = 0x1

.field private static final PROT_WRITE:I = 0x2

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAddress:I

.field private mAllowPurging:Z

.field private mFD:Ljava/io/FileDescriptor;

.field private mLength:I

.field private mOwnsRegion:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 38
    const-string v0, "MemoryFile"

    #@2
    sput-object v0, Landroid/os/MemoryFile;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method public constructor <init>(Ljava/io/FileDescriptor;II)V
    .registers 8
    .parameter "fd"
    .parameter "length"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 375
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 60
    iput-boolean v3, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@7
    .line 62
    iput-boolean v1, p0, Landroid/os/MemoryFile;->mOwnsRegion:Z

    #@9
    .line 376
    if-nez p1, :cond_13

    #@b
    .line 377
    new-instance v0, Ljava/lang/NullPointerException;

    #@d
    const-string v1, "File descriptor is null."

    #@f
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 379
    :cond_13
    invoke-static {p1}, Landroid/os/MemoryFile;->isMemoryFile(Ljava/io/FileDescriptor;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_21

    #@19
    .line 380
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1b
    const-string v1, "Not a memory file."

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 383
    :cond_21
    iput-object p1, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@23
    .line 384
    iput p2, p0, Landroid/os/MemoryFile;->mLength:I

    #@25
    .line 385
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@27
    invoke-static {v0, p2, v1}, Landroid/os/MemoryFile;->native_mmap(Ljava/io/FileDescriptor;II)I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/os/MemoryFile;->mAddress:I

    #@2d
    .line 386
    sget-object v0, Landroid/os/MemoryFile;->TAG:Ljava/lang/String;

    #@2f
    new-instance v1, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v2, "MemoryFile() mAddress = "

    #@36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v1

    #@3a
    iget v2, p0, Landroid/os/MemoryFile;->mAddress:I

    #@3c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v1

    #@40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 387
    iput-boolean v3, p0, Landroid/os/MemoryFile;->mOwnsRegion:Z

    #@49
    .line 389
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 5
    .parameter "name"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 72
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 60
    iput-boolean v1, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@6
    .line 62
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Landroid/os/MemoryFile;->mOwnsRegion:Z

    #@9
    .line 73
    iput p2, p0, Landroid/os/MemoryFile;->mLength:I

    #@b
    .line 74
    invoke-static {p1, p2}, Landroid/os/MemoryFile;->native_open(Ljava/lang/String;I)Ljava/io/FileDescriptor;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@11
    .line 75
    if-lez p2, :cond_1d

    #@13
    .line 76
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@15
    const/4 v1, 0x3

    #@16
    invoke-static {v0, p2, v1}, Landroid/os/MemoryFile;->native_mmap(Ljava/io/FileDescriptor;II)I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/os/MemoryFile;->mAddress:I

    #@1c
    .line 80
    :goto_1c
    return-void

    #@1d
    .line 78
    :cond_1d
    iput v1, p0, Landroid/os/MemoryFile;->mAddress:I

    #@1f
    goto :goto_1c
.end method

.method static synthetic access$200(Landroid/os/MemoryFile;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@2
    return v0
.end method

.method public static getSize(Ljava/io/FileDescriptor;)I
    .registers 2
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    invoke-static {p0}, Landroid/os/MemoryFile;->native_get_size(Ljava/io/FileDescriptor;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private isClosed()Z
    .registers 2

    #@0
    .prologue
    .line 122
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@2
    invoke-virtual {v0}, Ljava/io/FileDescriptor;->valid()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isDeactivated()Z
    .registers 2

    #@0
    .prologue
    .line 115
    iget v0, p0, Landroid/os/MemoryFile;->mAddress:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public static isMemoryFile(Ljava/io/FileDescriptor;)Z
    .registers 2
    .parameter "fd"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 398
    invoke-static {p0}, Landroid/os/MemoryFile;->native_get_size(Ljava/io/FileDescriptor;)I

    #@3
    move-result v0

    #@4
    if-ltz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private static native native_close(Ljava/io/FileDescriptor;)V
.end method

.method private static native native_get_size(Ljava/io/FileDescriptor;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_mmap(Ljava/io/FileDescriptor;II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_munmap(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_open(Ljava/lang/String;I)Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_pin(Ljava/io/FileDescriptor;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_read(Ljava/io/FileDescriptor;I[BIIIZ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native native_write(Ljava/io/FileDescriptor;I[BIIIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method


# virtual methods
.method public declared-synchronized allowPurging(Z)Z
    .registers 5
    .parameter "allowPurging"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 161
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v1, p0, Landroid/os/MemoryFile;->mOwnsRegion:Z

    #@3
    if-nez v1, :cond_10

    #@5
    .line 162
    new-instance v1, Ljava/io/IOException;

    #@7
    const-string v2, "Only the owner can make ashmem regions purgable."

    #@9
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@c
    throw v1
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    #@d
    .line 161
    :catchall_d
    move-exception v1

    #@e
    monitor-exit p0

    #@f
    throw v1

    #@10
    .line 166
    :cond_10
    :try_start_10
    iget-boolean v0, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@12
    .line 167
    .local v0, oldValue:Z
    if-eq v0, p1, :cond_1e

    #@14
    .line 168
    iget-object v2, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@16
    if-nez p1, :cond_20

    #@18
    const/4 v1, 0x1

    #@19
    :goto_19
    invoke-static {v2, v1}, Landroid/os/MemoryFile;->native_pin(Ljava/io/FileDescriptor;Z)V

    #@1c
    .line 169
    iput-boolean p1, p0, Landroid/os/MemoryFile;->mAllowPurging:Z
    :try_end_1e
    .catchall {:try_start_10 .. :try_end_1e} :catchall_d

    #@1e
    .line 171
    :cond_1e
    monitor-exit p0

    #@1f
    return v0

    #@20
    .line 168
    :cond_20
    const/4 v1, 0x0

    #@21
    goto :goto_19
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/os/MemoryFile;->deactivate()V

    #@3
    .line 88
    invoke-direct {p0}, Landroid/os/MemoryFile;->isClosed()Z

    #@6
    move-result v0

    #@7
    if-nez v0, :cond_e

    #@9
    .line 89
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@b
    invoke-static {v0}, Landroid/os/MemoryFile;->native_close(Ljava/io/FileDescriptor;)V

    #@e
    .line 91
    :cond_e
    return-void
.end method

.method deactivate()V
    .registers 4

    #@0
    .prologue
    .line 101
    invoke-direct {p0}, Landroid/os/MemoryFile;->isDeactivated()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_10

    #@6
    .line 103
    :try_start_6
    iget v1, p0, Landroid/os/MemoryFile;->mAddress:I

    #@8
    iget v2, p0, Landroid/os/MemoryFile;->mLength:I

    #@a
    invoke-static {v1, v2}, Landroid/os/MemoryFile;->native_munmap(II)V

    #@d
    .line 104
    const/4 v1, 0x0

    #@e
    iput v1, p0, Landroid/os/MemoryFile;->mAddress:I
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_10} :catch_11

    #@10
    .line 109
    :cond_10
    :goto_10
    return-void

    #@11
    .line 105
    :catch_11
    move-exception v0

    #@12
    .line 106
    .local v0, ex:Ljava/io/IOException;
    sget-object v1, Landroid/os/MemoryFile;->TAG:Ljava/lang/String;

    #@14
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_10
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 127
    invoke-direct {p0}, Landroid/os/MemoryFile;->isClosed()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_10

    #@6
    .line 128
    sget-object v0, Landroid/os/MemoryFile;->TAG:Ljava/lang/String;

    #@8
    const-string v1, "MemoryFile.finalize() called while ashmem still open"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 129
    invoke-virtual {p0}, Landroid/os/MemoryFile;->close()V

    #@10
    .line 131
    :cond_10
    return-void
.end method

.method public getFileDescriptor()Ljava/io/FileDescriptor;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 251
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@2
    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .registers 3

    #@0
    .prologue
    .line 180
    new-instance v0, Landroid/os/MemoryFile$MemoryInputStream;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/os/MemoryFile$MemoryInputStream;-><init>(Landroid/os/MemoryFile;Landroid/os/MemoryFile$1;)V

    #@6
    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .registers 3

    #@0
    .prologue
    .line 189
    new-instance v0, Landroid/os/MemoryFile$MemoryOutputStream;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/os/MemoryFile$MemoryOutputStream;-><init>(Landroid/os/MemoryFile;Landroid/os/MemoryFile$1;)V

    #@6
    return-object v0
.end method

.method public isPurgingAllowed()Z
    .registers 2

    #@0
    .prologue
    .line 148
    iget-boolean v0, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@2
    return v0
.end method

.method public length()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@2
    return v0
.end method

.method public readBytes([BIII)I
    .registers 12
    .parameter "buffer"
    .parameter "srcOffset"
    .parameter "destOffset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    invoke-direct {p0}, Landroid/os/MemoryFile;->isDeactivated()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 206
    new-instance v0, Ljava/io/IOException;

    #@8
    const-string v1, "Can\'t read from deactivated memory file."

    #@a
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 208
    :cond_e
    if-ltz p3, :cond_24

    #@10
    array-length v0, p1

    #@11
    if-gt p3, v0, :cond_24

    #@13
    if-ltz p4, :cond_24

    #@15
    array-length v0, p1

    #@16
    sub-int/2addr v0, p3

    #@17
    if-gt p4, v0, :cond_24

    #@19
    if-ltz p2, :cond_24

    #@1b
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@1d
    if-gt p2, v0, :cond_24

    #@1f
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@21
    sub-int/2addr v0, p2

    #@22
    if-le p4, v0, :cond_2a

    #@24
    .line 212
    :cond_24
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@26
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@29
    throw v0

    #@2a
    .line 214
    :cond_2a
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@2c
    iget v1, p0, Landroid/os/MemoryFile;->mAddress:I

    #@2e
    iget-boolean v6, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@30
    move-object v2, p1

    #@31
    move v3, p2

    #@32
    move v4, p3

    #@33
    move v5, p4

    #@34
    invoke-static/range {v0 .. v6}, Landroid/os/MemoryFile;->native_read(Ljava/io/FileDescriptor;I[BIIIZ)I

    #@37
    move-result v0

    #@38
    return v0
.end method

.method public writeBytes([BIII)V
    .registers 12
    .parameter "buffer"
    .parameter "srcOffset"
    .parameter "destOffset"
    .parameter "count"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 229
    invoke-direct {p0}, Landroid/os/MemoryFile;->isDeactivated()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    .line 230
    new-instance v0, Ljava/io/IOException;

    #@8
    const-string v1, "Can\'t write to deactivated memory file."

    #@a
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 232
    :cond_e
    if-ltz p2, :cond_24

    #@10
    array-length v0, p1

    #@11
    if-gt p2, v0, :cond_24

    #@13
    if-ltz p4, :cond_24

    #@15
    array-length v0, p1

    #@16
    sub-int/2addr v0, p2

    #@17
    if-gt p4, v0, :cond_24

    #@19
    if-ltz p3, :cond_24

    #@1b
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@1d
    if-gt p3, v0, :cond_24

    #@1f
    iget v0, p0, Landroid/os/MemoryFile;->mLength:I

    #@21
    sub-int/2addr v0, p3

    #@22
    if-le p4, v0, :cond_2a

    #@24
    .line 236
    :cond_24
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@26
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@29
    throw v0

    #@2a
    .line 238
    :cond_2a
    iget-object v0, p0, Landroid/os/MemoryFile;->mFD:Ljava/io/FileDescriptor;

    #@2c
    iget v1, p0, Landroid/os/MemoryFile;->mAddress:I

    #@2e
    iget-boolean v6, p0, Landroid/os/MemoryFile;->mAllowPurging:Z

    #@30
    move-object v2, p1

    #@31
    move v3, p2

    #@32
    move v4, p3

    #@33
    move v5, p4

    #@34
    invoke-static/range {v0 .. v6}, Landroid/os/MemoryFile;->native_write(Ljava/io/FileDescriptor;I[BIIIZ)V

    #@37
    .line 239
    return-void
.end method
