.class public Landroid/os/Broadcaster;
.super Ljava/lang/Object;
.source "Broadcaster.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/Broadcaster$1;,
        Landroid/os/Broadcaster$Registration;
    }
.end annotation


# instance fields
.field private mReg:Landroid/os/Broadcaster$Registration;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 24
    return-void
.end method


# virtual methods
.method public broadcast(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    .line 173
    monitor-enter p0

    #@1
    .line 174
    :try_start_1
    iget-object v9, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@3
    if-nez v9, :cond_7

    #@5
    .line 175
    monitor-exit p0

    #@6
    .line 200
    :goto_6
    return-void

    #@7
    .line 178
    :cond_7
    iget v4, p1, Landroid/os/Message;->what:I

    #@9
    .line 179
    .local v4, senderWhat:I
    iget-object v5, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@b
    .line 180
    .local v5, start:Landroid/os/Broadcaster$Registration;
    move-object v3, v5

    #@c
    .line 182
    .local v3, r:Landroid/os/Broadcaster$Registration;
    :cond_c
    iget v9, v3, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@e
    if-lt v9, v4, :cond_2f

    #@10
    .line 187
    :goto_10
    iget v9, v3, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@12
    if-ne v9, v4, :cond_34

    #@14
    .line 188
    iget-object v7, v3, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@16
    .line 189
    .local v7, targets:[Landroid/os/Handler;
    iget-object v8, v3, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@18
    .line 190
    .local v8, whats:[I
    array-length v2, v7

    #@19
    .line 191
    .local v2, n:I
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    if-ge v0, v2, :cond_34

    #@1c
    .line 192
    aget-object v6, v7, v0

    #@1e
    .line 193
    .local v6, target:Landroid/os/Handler;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@21
    move-result-object v1

    #@22
    .line 194
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    #@25
    .line 195
    aget v9, v8, v0

    #@27
    iput v9, v1, Landroid/os/Message;->what:I

    #@29
    .line 196
    invoke-virtual {v6, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2c
    .line 191
    add-int/lit8 v0, v0, 0x1

    #@2e
    goto :goto_1a

    #@2f
    .line 185
    .end local v0           #i:I
    .end local v1           #m:Landroid/os/Message;
    .end local v2           #n:I
    .end local v6           #target:Landroid/os/Handler;
    .end local v7           #targets:[Landroid/os/Handler;
    .end local v8           #whats:[I
    :cond_2f
    iget-object v3, v3, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@31
    .line 186
    if-ne v3, v5, :cond_c

    #@33
    goto :goto_10

    #@34
    .line 199
    :cond_34
    monitor-exit p0

    #@35
    goto :goto_6

    #@36
    .end local v3           #r:Landroid/os/Broadcaster$Registration;
    .end local v4           #senderWhat:I
    .end local v5           #start:Landroid/os/Broadcaster$Registration;
    :catchall_36
    move-exception v9

    #@37
    monitor-exit p0
    :try_end_38
    .catchall {:try_start_1 .. :try_end_38} :catchall_36

    #@38
    throw v9
.end method

.method public cancelRequest(ILandroid/os/Handler;I)V
    .registers 14
    .parameter "senderWhat"
    .parameter "target"
    .parameter "targetWhat"

    #@0
    .prologue
    .line 101
    monitor-enter p0

    #@1
    .line 102
    :try_start_1
    iget-object v4, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@3
    .line 103
    .local v4, start:Landroid/os/Broadcaster$Registration;
    move-object v2, v4

    #@4
    .line 105
    .local v2, r:Landroid/os/Broadcaster$Registration;
    if-nez v2, :cond_8

    #@6
    .line 106
    monitor-exit p0

    #@7
    .line 141
    :goto_7
    return-void

    #@8
    .line 110
    :cond_8
    iget v7, v2, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@a
    if-lt v7, p1, :cond_55

    #@c
    .line 116
    :goto_c
    iget v7, v2, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@e
    if-ne v7, p1, :cond_50

    #@10
    .line 117
    iget-object v5, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@12
    .line 118
    .local v5, targets:[Landroid/os/Handler;
    iget-object v6, v2, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@14
    .line 119
    .local v6, whats:[I
    array-length v1, v5

    #@15
    .line 120
    .local v1, oldLen:I
    const/4 v0, 0x0

    #@16
    .local v0, i:I
    :goto_16
    if-ge v0, v1, :cond_50

    #@18
    .line 121
    aget-object v7, v5, v0

    #@1a
    if-ne v7, p2, :cond_5a

    #@1c
    aget v7, v6, v0

    #@1e
    if-ne v7, p3, :cond_5a

    #@20
    .line 122
    add-int/lit8 v7, v1, -0x1

    #@22
    new-array v7, v7, [Landroid/os/Handler;

    #@24
    iput-object v7, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@26
    .line 123
    add-int/lit8 v7, v1, -0x1

    #@28
    new-array v7, v7, [I

    #@2a
    iput-object v7, v2, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@2c
    .line 124
    if-lez v0, :cond_3c

    #@2e
    .line 125
    const/4 v7, 0x0

    #@2f
    iget-object v8, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@31
    const/4 v9, 0x0

    #@32
    invoke-static {v5, v7, v8, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@35
    .line 126
    const/4 v7, 0x0

    #@36
    iget-object v8, v2, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@38
    const/4 v9, 0x0

    #@39
    invoke-static {v6, v7, v8, v9, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3c
    .line 129
    :cond_3c
    sub-int v7, v1, v0

    #@3e
    add-int/lit8 v3, v7, -0x1

    #@40
    .line 130
    .local v3, remainingLen:I
    if-eqz v3, :cond_50

    #@42
    .line 131
    add-int/lit8 v7, v0, 0x1

    #@44
    iget-object v8, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@46
    invoke-static {v5, v7, v8, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@49
    .line 133
    add-int/lit8 v7, v0, 0x1

    #@4b
    iget-object v8, v2, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@4d
    invoke-static {v6, v7, v8, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@50
    .line 140
    .end local v0           #i:I
    .end local v1           #oldLen:I
    .end local v3           #remainingLen:I
    .end local v5           #targets:[Landroid/os/Handler;
    .end local v6           #whats:[I
    :cond_50
    monitor-exit p0

    #@51
    goto :goto_7

    #@52
    .end local v2           #r:Landroid/os/Broadcaster$Registration;
    .end local v4           #start:Landroid/os/Broadcaster$Registration;
    :catchall_52
    move-exception v7

    #@53
    monitor-exit p0
    :try_end_54
    .catchall {:try_start_1 .. :try_end_54} :catchall_52

    #@54
    throw v7

    #@55
    .line 113
    .restart local v2       #r:Landroid/os/Broadcaster$Registration;
    .restart local v4       #start:Landroid/os/Broadcaster$Registration;
    :cond_55
    :try_start_55
    iget-object v2, v2, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;
    :try_end_57
    .catchall {:try_start_55 .. :try_end_57} :catchall_52

    #@57
    .line 114
    if-ne v2, v4, :cond_8

    #@59
    goto :goto_c

    #@5a
    .line 120
    .restart local v0       #i:I
    .restart local v1       #oldLen:I
    .restart local v5       #targets:[Landroid/os/Handler;
    .restart local v6       #whats:[I
    :cond_5a
    add-int/lit8 v0, v0, 0x1

    #@5c
    goto :goto_16
.end method

.method public dumpRegistrations()V
    .registers 8

    #@0
    .prologue
    .line 148
    monitor-enter p0

    #@1
    .line 149
    :try_start_1
    iget-object v3, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@3
    .line 150
    .local v3, start:Landroid/os/Broadcaster$Registration;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "Broadcaster "

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, " {"

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@21
    .line 151
    if-eqz v3, :cond_75

    #@23
    .line 152
    move-object v2, v3

    #@24
    .line 154
    .local v2, r:Landroid/os/Broadcaster$Registration;
    :cond_24
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@26
    new-instance v5, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v6, "    senderWhat="

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    iget v6, v2, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@3e
    .line 155
    iget-object v4, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@40
    array-length v1, v4

    #@41
    .line 156
    .local v1, n:I
    const/4 v0, 0x0

    #@42
    .local v0, i:I
    :goto_42
    if-ge v0, v1, :cond_71

    #@44
    .line 157
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@46
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v6, "        ["

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    iget-object v6, v2, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@53
    aget v6, v6, v0

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, "] "

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    iget-object v6, v2, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@61
    aget-object v6, v6, v0

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@6e
    .line 156
    add-int/lit8 v0, v0, 0x1

    #@70
    goto :goto_42

    #@71
    .line 160
    :cond_71
    iget-object v2, v2, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@73
    .line 161
    if-ne v2, v3, :cond_24

    #@75
    .line 163
    .end local v0           #i:I
    .end local v1           #n:I
    .end local v2           #r:Landroid/os/Broadcaster$Registration;
    :cond_75
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    #@77
    const-string/jumbo v5, "}"

    #@7a
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    #@7d
    .line 164
    monitor-exit p0

    #@7e
    .line 165
    return-void

    #@7f
    .line 164
    .end local v3           #start:Landroid/os/Broadcaster$Registration;
    :catchall_7f
    move-exception v4

    #@80
    monitor-exit p0
    :try_end_81
    .catchall {:try_start_1 .. :try_end_81} :catchall_7f

    #@81
    throw v4
.end method

.method public request(ILandroid/os/Handler;I)V
    .registers 15
    .parameter "senderWhat"
    .parameter "target"
    .parameter "targetWhat"

    #@0
    .prologue
    .line 34
    monitor-enter p0

    #@1
    .line 35
    const/4 v4, 0x0

    #@2
    .line 36
    .local v4, r:Landroid/os/Broadcaster$Registration;
    :try_start_2
    iget-object v8, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@4
    if-nez v8, :cond_2b

    #@6
    .line 37
    new-instance v5, Landroid/os/Broadcaster$Registration;

    #@8
    const/4 v8, 0x0

    #@9
    invoke-direct {v5, p0, v8}, Landroid/os/Broadcaster$Registration;-><init>(Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_6b

    #@c
    .line 38
    .end local v4           #r:Landroid/os/Broadcaster$Registration;
    .local v5, r:Landroid/os/Broadcaster$Registration;
    :try_start_c
    iput p1, v5, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@e
    .line 39
    const/4 v8, 0x1

    #@f
    new-array v8, v8, [Landroid/os/Handler;

    #@11
    iput-object v8, v5, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@13
    .line 40
    const/4 v8, 0x1

    #@14
    new-array v8, v8, [I

    #@16
    iput-object v8, v5, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@18
    .line 41
    iget-object v8, v5, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@1a
    const/4 v9, 0x0

    #@1b
    aput-object p2, v8, v9

    #@1d
    .line 42
    iget-object v8, v5, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@1f
    const/4 v9, 0x0

    #@20
    aput p3, v8, v9

    #@22
    .line 43
    iput-object v5, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@24
    .line 44
    iput-object v5, v5, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@26
    .line 45
    iput-object v5, v5, Landroid/os/Broadcaster$Registration;->prev:Landroid/os/Broadcaster$Registration;
    :try_end_28
    .catchall {:try_start_c .. :try_end_28} :catchall_a5

    #@28
    move-object v4, v5

    #@29
    .line 93
    .end local v5           #r:Landroid/os/Broadcaster$Registration;
    .restart local v4       #r:Landroid/os/Broadcaster$Registration;
    :goto_29
    :try_start_29
    monitor-exit p0

    #@2a
    .line 94
    :goto_2a
    return-void

    #@2b
    .line 48
    :cond_2b
    iget-object v7, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@2d
    .line 49
    .local v7, start:Landroid/os/Broadcaster$Registration;
    move-object v4, v7

    #@2e
    .line 51
    :cond_2e
    iget v8, v4, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@30
    if-lt v8, p1, :cond_6e

    #@32
    .line 57
    :goto_32
    iget v8, v4, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@34
    if-eq v8, p1, :cond_73

    #@36
    .line 60
    new-instance v6, Landroid/os/Broadcaster$Registration;

    #@38
    const/4 v8, 0x0

    #@39
    invoke-direct {v6, p0, v8}, Landroid/os/Broadcaster$Registration;-><init>(Landroid/os/Broadcaster;Landroid/os/Broadcaster$1;)V

    #@3c
    .line 61
    .local v6, reg:Landroid/os/Broadcaster$Registration;
    iput p1, v6, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@3e
    .line 62
    const/4 v8, 0x1

    #@3f
    new-array v8, v8, [Landroid/os/Handler;

    #@41
    iput-object v8, v6, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@43
    .line 63
    const/4 v8, 0x1

    #@44
    new-array v8, v8, [I

    #@46
    iput-object v8, v6, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@48
    .line 64
    iput-object v4, v6, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@4a
    .line 65
    iget-object v8, v4, Landroid/os/Broadcaster$Registration;->prev:Landroid/os/Broadcaster$Registration;

    #@4c
    iput-object v8, v6, Landroid/os/Broadcaster$Registration;->prev:Landroid/os/Broadcaster$Registration;

    #@4e
    .line 66
    iget-object v8, v4, Landroid/os/Broadcaster$Registration;->prev:Landroid/os/Broadcaster$Registration;

    #@50
    iput-object v6, v8, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@52
    .line 67
    iput-object v6, v4, Landroid/os/Broadcaster$Registration;->prev:Landroid/os/Broadcaster$Registration;

    #@54
    .line 69
    iget-object v8, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@56
    if-ne v4, v8, :cond_60

    #@58
    iget v8, v4, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@5a
    iget v9, v6, Landroid/os/Broadcaster$Registration;->senderWhat:I

    #@5c
    if-le v8, v9, :cond_60

    #@5e
    .line 70
    iput-object v6, p0, Landroid/os/Broadcaster;->mReg:Landroid/os/Broadcaster$Registration;

    #@60
    .line 73
    :cond_60
    move-object v4, v6

    #@61
    .line 74
    const/4 v1, 0x0

    #@62
    .line 90
    .end local v6           #reg:Landroid/os/Broadcaster$Registration;
    .local v1, n:I
    :goto_62
    iget-object v8, v4, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@64
    aput-object p2, v8, v1

    #@66
    .line 91
    iget-object v8, v4, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@68
    aput p3, v8, v1

    #@6a
    goto :goto_29

    #@6b
    .line 93
    .end local v1           #n:I
    .end local v7           #start:Landroid/os/Broadcaster$Registration;
    :catchall_6b
    move-exception v8

    #@6c
    :goto_6c
    monitor-exit p0
    :try_end_6d
    .catchall {:try_start_29 .. :try_end_6d} :catchall_6b

    #@6d
    throw v8

    #@6e
    .line 54
    .restart local v7       #start:Landroid/os/Broadcaster$Registration;
    :cond_6e
    :try_start_6e
    iget-object v4, v4, Landroid/os/Broadcaster$Registration;->next:Landroid/os/Broadcaster$Registration;

    #@70
    .line 55
    if-ne v4, v7, :cond_2e

    #@72
    goto :goto_32

    #@73
    .line 76
    :cond_73
    iget-object v8, v4, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@75
    array-length v1, v8

    #@76
    .line 77
    .restart local v1       #n:I
    iget-object v2, v4, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@78
    .line 78
    .local v2, oldTargets:[Landroid/os/Handler;
    iget-object v3, v4, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@7a
    .line 80
    .local v3, oldWhats:[I
    const/4 v0, 0x0

    #@7b
    .local v0, i:I
    :goto_7b
    if-ge v0, v1, :cond_8a

    #@7d
    .line 81
    aget-object v8, v2, v0

    #@7f
    if-ne v8, p2, :cond_87

    #@81
    aget v8, v3, v0

    #@83
    if-ne v8, p3, :cond_87

    #@85
    .line 82
    monitor-exit p0

    #@86
    goto :goto_2a

    #@87
    .line 80
    :cond_87
    add-int/lit8 v0, v0, 0x1

    #@89
    goto :goto_7b

    #@8a
    .line 85
    :cond_8a
    add-int/lit8 v8, v1, 0x1

    #@8c
    new-array v8, v8, [Landroid/os/Handler;

    #@8e
    iput-object v8, v4, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@90
    .line 86
    const/4 v8, 0x0

    #@91
    iget-object v9, v4, Landroid/os/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    #@93
    const/4 v10, 0x0

    #@94
    invoke-static {v2, v8, v9, v10, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@97
    .line 87
    add-int/lit8 v8, v1, 0x1

    #@99
    new-array v8, v8, [I

    #@9b
    iput-object v8, v4, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@9d
    .line 88
    const/4 v8, 0x0

    #@9e
    iget-object v9, v4, Landroid/os/Broadcaster$Registration;->targetWhats:[I

    #@a0
    const/4 v10, 0x0

    #@a1
    invoke-static {v3, v8, v9, v10, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_a4
    .catchall {:try_start_6e .. :try_end_a4} :catchall_6b

    #@a4
    goto :goto_62

    #@a5
    .line 93
    .end local v0           #i:I
    .end local v1           #n:I
    .end local v2           #oldTargets:[Landroid/os/Handler;
    .end local v3           #oldWhats:[I
    .end local v4           #r:Landroid/os/Broadcaster$Registration;
    .end local v7           #start:Landroid/os/Broadcaster$Registration;
    .restart local v5       #r:Landroid/os/Broadcaster$Registration;
    :catchall_a5
    move-exception v8

    #@a6
    move-object v4, v5

    #@a7
    .end local v5           #r:Landroid/os/Broadcaster$Registration;
    .restart local v4       #r:Landroid/os/Broadcaster$Registration;
    goto :goto_6c
.end method
