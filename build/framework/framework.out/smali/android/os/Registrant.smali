.class public Landroid/os/Registrant;
.super Ljava/lang/Object;
.source "Registrant.java"


# instance fields
.field refH:Ljava/lang/ref/WeakReference;

.field userObj:Ljava/lang/Object;

.field what:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 5
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/os/Registrant;->refH:Ljava/lang/ref/WeakReference;

    #@a
    .line 32
    iput p2, p0, Landroid/os/Registrant;->what:I

    #@c
    .line 33
    iput-object p3, p0, Landroid/os/Registrant;->userObj:Ljava/lang/Object;

    #@e
    .line 34
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 39
    iput-object v0, p0, Landroid/os/Registrant;->refH:Ljava/lang/ref/WeakReference;

    #@3
    .line 40
    iput-object v0, p0, Landroid/os/Registrant;->userObj:Ljava/lang/Object;

    #@5
    .line 41
    return-void
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 114
    iget-object v0, p0, Landroid/os/Registrant;->refH:Ljava/lang/ref/WeakReference;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 115
    const/4 v0, 0x0

    #@5
    .line 117
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/os/Registrant;->refH:Ljava/lang/ref/WeakReference;

    #@8
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/os/Handler;

    #@e
    goto :goto_5
.end method

.method internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .registers 7
    .parameter "result"
    .parameter "exception"

    #@0
    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/os/Registrant;->getHandler()Landroid/os/Handler;

    #@3
    move-result-object v0

    #@4
    .line 75
    .local v0, h:Landroid/os/Handler;
    if-nez v0, :cond_a

    #@6
    .line 76
    invoke-virtual {p0}, Landroid/os/Registrant;->clear()V

    #@9
    .line 86
    :goto_9
    return-void

    #@a
    .line 78
    :cond_a
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@d
    move-result-object v1

    #@e
    .line 80
    .local v1, msg:Landroid/os/Message;
    iget v2, p0, Landroid/os/Registrant;->what:I

    #@10
    iput v2, v1, Landroid/os/Message;->what:I

    #@12
    .line 82
    new-instance v2, Landroid/os/AsyncResult;

    #@14
    iget-object v3, p0, Landroid/os/Registrant;->userObj:Ljava/lang/Object;

    #@16
    invoke-direct {v2, v3, p1, p2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@19
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1b
    .line 84
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1e
    goto :goto_9
.end method

.method public messageForRegistrant()Landroid/os/Message;
    .registers 4

    #@0
    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/os/Registrant;->getHandler()Landroid/os/Handler;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, h:Landroid/os/Handler;
    if-nez v0, :cond_b

    #@6
    .line 98
    invoke-virtual {p0}, Landroid/os/Registrant;->clear()V

    #@9
    .line 100
    const/4 v1, 0x0

    #@a
    .line 107
    :goto_a
    return-object v1

    #@b
    .line 102
    :cond_b
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@e
    move-result-object v1

    #@f
    .line 104
    .local v1, msg:Landroid/os/Message;
    iget v2, p0, Landroid/os/Registrant;->what:I

    #@11
    iput v2, v1, Landroid/os/Message;->what:I

    #@13
    .line 105
    iget-object v2, p0, Landroid/os/Registrant;->userObj:Ljava/lang/Object;

    #@15
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@17
    goto :goto_a
.end method

.method public notifyException(Ljava/lang/Throwable;)V
    .registers 3
    .parameter "exception"

    #@0
    .prologue
    .line 58
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0, p1}, Landroid/os/Registrant;->internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 59
    return-void
.end method

.method public notifyRegistrant()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 46
    invoke-virtual {p0, v0, v0}, Landroid/os/Registrant;->internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 47
    return-void
.end method

.method public notifyRegistrant(Landroid/os/AsyncResult;)V
    .registers 4
    .parameter "ar"

    #@0
    .prologue
    .line 67
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@2
    iget-object v1, p1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/os/Registrant;->internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@7
    .line 68
    return-void
.end method

.method public notifyResult(Ljava/lang/Object;)V
    .registers 3
    .parameter "result"

    #@0
    .prologue
    .line 52
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/os/Registrant;->internalNotifyRegistrant(Ljava/lang/Object;Ljava/lang/Throwable;)V

    #@4
    .line 53
    return-void
.end method
