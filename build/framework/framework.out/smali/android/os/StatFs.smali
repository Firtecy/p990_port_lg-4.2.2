.class public Landroid/os/StatFs;
.super Ljava/lang/Object;
.source "StatFs.java"


# instance fields
.field private mStat:Llibcore/io/StructStatFs;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    invoke-static {p1}, Landroid/os/StatFs;->doStat(Ljava/lang/String;)Llibcore/io/StructStatFs;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@9
    .line 40
    return-void
.end method

.method private static doStat(Ljava/lang/String;)Llibcore/io/StructStatFs;
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 44
    :try_start_0
    sget-object v1, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    #@2
    invoke-interface {v1, p0}, Llibcore/io/Os;->statfs(Ljava/lang/String;)Llibcore/io/StructStatFs;
    :try_end_5
    .catch Llibcore/io/ErrnoException; {:try_start_0 .. :try_end_5} :catch_7

    #@5
    move-result-object v1

    #@6
    return-object v1

    #@7
    .line 45
    :catch_7
    move-exception v0

    #@8
    .line 46
    .local v0, e:Llibcore/io/ErrnoException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "Invalid path: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@20
    throw v1
.end method


# virtual methods
.method public getAvailableBlocks()I
    .registers 3

    #@0
    .prologue
    .line 90
    iget-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@2
    iget-wide v0, v0, Llibcore/io/StructStatFs;->f_bavail:J

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public getBlockCount()I
    .registers 3

    #@0
    .prologue
    .line 72
    iget-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@2
    iget-wide v0, v0, Llibcore/io/StructStatFs;->f_blocks:J

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public getBlockSize()I
    .registers 3

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@2
    iget-wide v0, v0, Llibcore/io/StructStatFs;->f_bsize:J

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public getFreeBlocks()I
    .registers 3

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@2
    iget-wide v0, v0, Llibcore/io/StructStatFs;->f_bfree:J

    #@4
    long-to-int v0, v0

    #@5
    return v0
.end method

.method public restat(Ljava/lang/String;)V
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 56
    invoke-static {p1}, Landroid/os/StatFs;->doStat(Ljava/lang/String;)Llibcore/io/StructStatFs;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/os/StatFs;->mStat:Llibcore/io/StructStatFs;

    #@6
    .line 57
    return-void
.end method
