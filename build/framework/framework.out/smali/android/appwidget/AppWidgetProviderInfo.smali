.class public Landroid/appwidget/AppWidgetProviderInfo;
.super Ljava/lang/Object;
.source "AppWidgetProviderInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final RESIZE_BOTH:I = 0x3

.field public static final RESIZE_HORIZONTAL:I = 0x1

.field public static final RESIZE_NONE:I = 0x0

.field public static final RESIZE_VERTICAL:I = 0x2

.field public static final WIDGET_CATEGORY_HOME_SCREEN:I = 0x1

.field public static final WIDGET_CATEGORY_KEYGUARD:I = 0x2


# instance fields
.field public autoAdvanceViewId:I

.field public configure:Landroid/content/ComponentName;

.field public icon:I

.field public initialKeyguardLayout:I

.field public initialLayout:I

.field public label:Ljava/lang/String;

.field public minHeight:I

.field public minResizeHeight:I

.field public minResizeWidth:I

.field public minWidth:I

.field public previewImage:I

.field public provider:Landroid/content/ComponentName;

.field public resizeMode:I

.field public updatePeriodMillis:I

.field public widgetCategory:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 282
    new-instance v0, Landroid/appwidget/AppWidgetProviderInfo$1;

    #@2
    invoke-direct {v0}, Landroid/appwidget/AppWidgetProviderInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/appwidget/AppWidgetProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 198
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 199
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 204
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 206
    new-instance v0, Landroid/content/ComponentName;

    #@b
    invoke-direct {v0, p1}, Landroid/content/ComponentName;-><init>(Landroid/os/Parcel;)V

    #@e
    iput-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@10
    .line 208
    :cond_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@16
    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@1c
    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@22
    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v0

    #@26
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@28
    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@2e
    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@31
    move-result v0

    #@32
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@34
    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@3a
    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_47

    #@40
    .line 216
    new-instance v0, Landroid/content/ComponentName;

    #@42
    invoke-direct {v0, p1}, Landroid/content/ComponentName;-><init>(Landroid/os/Parcel;)V

    #@45
    iput-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@47
    .line 218
    :cond_47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    iput-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@4d
    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v0

    #@51
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    #@53
    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@56
    move-result v0

    #@57
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    #@59
    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5c
    move-result v0

    #@5d
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@5f
    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@62
    move-result v0

    #@63
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@65
    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@68
    move-result v0

    #@69
    iput v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@6b
    .line 224
    return-void
.end method


# virtual methods
.method public clone()Landroid/appwidget/AppWidgetProviderInfo;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 256
    new-instance v0, Landroid/appwidget/AppWidgetProviderInfo;

    #@3
    invoke-direct {v0}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    #@6
    .line 257
    .local v0, that:Landroid/appwidget/AppWidgetProviderInfo;
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@8
    if-nez v1, :cond_4b

    #@a
    move-object v1, v2

    #@b
    :goto_b
    iput-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@d
    .line 258
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@f
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@11
    .line 259
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@13
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@15
    .line 260
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@17
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@19
    .line 261
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@1b
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@1d
    .line 262
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@1f
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@21
    .line 263
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@23
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@25
    .line 264
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@27
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@29
    .line 265
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@2b
    if-nez v1, :cond_52

    #@2d
    move-object v1, v2

    #@2e
    :goto_2e
    iput-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@30
    .line 266
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@32
    if-nez v1, :cond_59

    #@34
    :goto_34
    iput-object v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@36
    .line 267
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    #@38
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    #@3a
    .line 268
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    #@3c
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    #@3e
    .line 269
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@40
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@42
    .line 270
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@44
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@46
    .line 271
    iget v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@48
    iput v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@4a
    .line 272
    return-object v0

    #@4b
    .line 257
    :cond_4b
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@4d
    invoke-virtual {v1}, Landroid/content/ComponentName;->clone()Landroid/content/ComponentName;

    #@50
    move-result-object v1

    #@51
    goto :goto_b

    #@52
    .line 265
    :cond_52
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@54
    invoke-virtual {v1}, Landroid/content/ComponentName;->clone()Landroid/content/ComponentName;

    #@57
    move-result-object v1

    #@58
    goto :goto_2e

    #@59
    .line 266
    :cond_59
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@5b
    const/4 v2, 0x0

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    goto :goto_34
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetProviderInfo;->clone()Landroid/appwidget/AppWidgetProviderInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 276
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "AppWidgetProviderInfo(provider="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ")"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 227
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@4
    if-eqz v0, :cond_5c

    #@6
    .line 228
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@9
    .line 229
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@b
    invoke-virtual {v0, p1, p2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@e
    .line 233
    :goto_e
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 234
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@15
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 235
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@1a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 236
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 237
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->updatePeriodMillis:I

    #@24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 238
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@29
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 239
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@2e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 240
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@33
    if-eqz v0, :cond_60

    #@35
    .line 241
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 242
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    #@3a
    invoke-virtual {v0, p1, p2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@3d
    .line 246
    :goto_3d
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@3f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@42
    .line 247
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    #@44
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@47
    .line 248
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    #@49
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    .line 249
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    #@4e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@51
    .line 250
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    #@53
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@56
    .line 251
    iget v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    #@58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    .line 252
    return-void

    #@5c
    .line 231
    :cond_5c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@5f
    goto :goto_e

    #@60
    .line 244
    :cond_60
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@63
    goto :goto_3d
.end method
