.class Landroid/appwidget/AppWidgetHost$Callbacks;
.super Lcom/android/internal/appwidget/IAppWidgetHost$Stub;
.source "AppWidgetHost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/appwidget/AppWidgetHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Callbacks"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/appwidget/AppWidgetHost;


# direct methods
.method constructor <init>(Landroid/appwidget/AppWidgetHost;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 59
    iput-object p1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@2
    invoke-direct {p0}, Lcom/android/internal/appwidget/IAppWidgetHost$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public providerChanged(ILandroid/appwidget/AppWidgetProviderInfo;)V
    .registers 6
    .parameter "appWidgetId"
    .parameter "info"

    #@0
    .prologue
    .line 72
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@2
    invoke-static {v1}, Landroid/appwidget/AppWidgetHost;->access$000(Landroid/appwidget/AppWidgetHost;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_e

    #@8
    if-eqz p2, :cond_e

    #@a
    .line 73
    invoke-virtual {p2}, Landroid/appwidget/AppWidgetProviderInfo;->clone()Landroid/appwidget/AppWidgetProviderInfo;

    #@d
    move-result-object p2

    #@e
    .line 75
    :cond_e
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@10
    iget-object v1, v1, Landroid/appwidget/AppWidgetHost;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v2, 0x2

    #@13
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 76
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@19
    .line 77
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1b
    .line 78
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1e
    .line 79
    return-void
.end method

.method public providersChanged()V
    .registers 4

    #@0
    .prologue
    .line 82
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@2
    iget-object v1, v1, Landroid/appwidget/AppWidgetHost;->mHandler:Landroid/os/Handler;

    #@4
    const/4 v2, 0x3

    #@5
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 83
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@c
    .line 84
    return-void
.end method

.method public updateAppWidget(ILandroid/widget/RemoteViews;)V
    .registers 6
    .parameter "appWidgetId"
    .parameter "views"

    #@0
    .prologue
    .line 61
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@2
    invoke-static {v1}, Landroid/appwidget/AppWidgetHost;->access$000(Landroid/appwidget/AppWidgetHost;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_17

    #@8
    if-eqz p2, :cond_17

    #@a
    .line 62
    invoke-virtual {p2}, Landroid/widget/RemoteViews;->clone()Landroid/widget/RemoteViews;

    #@d
    move-result-object p2

    #@e
    .line 63
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@10
    invoke-static {v1}, Landroid/appwidget/AppWidgetHost;->access$100(Landroid/appwidget/AppWidgetHost;)Landroid/os/UserHandle;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {p2, v1}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@17
    .line 65
    :cond_17
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@19
    iget-object v1, v1, Landroid/appwidget/AppWidgetHost;->mHandler:Landroid/os/Handler;

    #@1b
    const/4 v2, 0x1

    #@1c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v0

    #@20
    .line 66
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@22
    .line 67
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    .line 68
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@27
    .line 69
    return-void
.end method

.method public viewDataChanged(II)V
    .registers 6
    .parameter "appWidgetId"
    .parameter "viewId"

    #@0
    .prologue
    .line 87
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost$Callbacks;->this$0:Landroid/appwidget/AppWidgetHost;

    #@2
    iget-object v1, v1, Landroid/appwidget/AppWidgetHost;->mHandler:Landroid/os/Handler;

    #@4
    const/4 v2, 0x4

    #@5
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 88
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 89
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@d
    .line 90
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@10
    .line 91
    return-void
.end method
