.class public Landroid/appwidget/AppWidgetHostView;
.super Landroid/widget/FrameLayout;
.source "AppWidgetHostView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;
    }
.end annotation


# static fields
.field static final CROSSFADE:Z = false

.field static final FADE_DURATION:I = 0x3e8

.field static final LOGD:Z = false

.field static final TAG:Ljava/lang/String; = "AppWidgetHostView"

.field static final VIEW_MODE_CONTENT:I = 0x1

.field static final VIEW_MODE_DEFAULT:I = 0x3

.field static final VIEW_MODE_ERROR:I = 0x2

.field static final VIEW_MODE_NOINIT:I

.field static final sInflaterFilter:Landroid/view/LayoutInflater$Filter;


# instance fields
.field mAppWidgetId:I

.field mContext:Landroid/content/Context;

.field mFadeStartTime:J

.field mInfo:Landroid/appwidget/AppWidgetProviderInfo;

.field mLayoutId:I

.field mOld:Landroid/graphics/Bitmap;

.field mOldPaint:Landroid/graphics/Paint;

.field private mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

.field mRemoteContext:Landroid/content/Context;

.field private mUser:Landroid/os/UserHandle;

.field mView:Landroid/view/View;

.field mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 72
    new-instance v0, Landroid/appwidget/AppWidgetHostView$1;

    #@2
    invoke-direct {v0}, Landroid/appwidget/AppWidgetHostView$1;-><init>()V

    #@5
    sput-object v0, Landroid/appwidget/AppWidgetHostView;->sInflaterFilter:Landroid/view/LayoutInflater$Filter;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 96
    const/high16 v0, 0x10a

    #@2
    const v1, 0x10a0001

    #@5
    invoke-direct {p0, p1, v0, v1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;II)V

    #@8
    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .registers 6
    .parameter "context"
    .parameter "animationIn"
    .parameter "animationOut"

    #@0
    .prologue
    .line 116
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    #@3
    .line 84
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@6
    .line 85
    const/4 v0, -0x1

    #@7
    iput v0, p0, Landroid/appwidget/AppWidgetHostView;->mLayoutId:I

    #@9
    .line 86
    const-wide/16 v0, -0x1

    #@b
    iput-wide v0, p0, Landroid/appwidget/AppWidgetHostView;->mFadeStartTime:J

    #@d
    .line 88
    new-instance v0, Landroid/graphics/Paint;

    #@f
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@12
    iput-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mOldPaint:Landroid/graphics/Paint;

    #@14
    .line 117
    iput-object p1, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@16
    .line 118
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mUser:Landroid/os/UserHandle;

    #@1c
    .line 121
    const/4 v0, 0x1

    #@1d
    invoke-virtual {p0, v0}, Landroid/appwidget/AppWidgetHostView;->setIsRootNamespace(Z)V

    #@20
    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 5
    .parameter "context"
    .parameter "handler"

    #@0
    .prologue
    .line 103
    const/high16 v0, 0x10a

    #@2
    const v1, 0x10a0001

    #@5
    invoke-direct {p0, p1, v0, v1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;II)V

    #@8
    .line 104
    iput-object p2, p0, Landroid/appwidget/AppWidgetHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@a
    .line 105
    return-void
.end method

.method private generateId()I
    .registers 3

    #@0
    .prologue
    .line 223
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHostView;->getId()I

    #@3
    move-result v0

    #@4
    .line 224
    .local v0, id:I
    const/4 v1, -0x1

    #@5
    if-ne v0, v1, :cond_9

    #@7
    iget v0, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@9
    .end local v0           #id:I
    :cond_9
    return v0
.end method

.method public static getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 9
    .parameter "context"
    .parameter "component"
    .parameter "padding"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 177
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@4
    move-result-object v2

    #@5
    .line 180
    .local v2, packageManager:Landroid/content/pm/PackageManager;
    if-nez p2, :cond_44

    #@7
    .line 181
    new-instance p2, Landroid/graphics/Rect;

    #@9
    .end local p2
    invoke-direct {p2, v4, v4, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    #@c
    .line 187
    .restart local p2
    :goto_c
    :try_start_c
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@f
    move-result-object v4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_14
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_14} :catch_48

    #@14
    move-result-object v0

    #@15
    .line 193
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    #@17
    const/16 v5, 0xe

    #@19
    if-lt v4, v5, :cond_43

    #@1b
    .line 194
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1e
    move-result-object v3

    #@1f
    .line 195
    .local v3, r:Landroid/content/res/Resources;
    const v4, 0x1050050

    #@22
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@25
    move-result v4

    #@26
    iput v4, p2, Landroid/graphics/Rect;->left:I

    #@28
    .line 197
    const v4, 0x1050052

    #@2b
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@2e
    move-result v4

    #@2f
    iput v4, p2, Landroid/graphics/Rect;->right:I

    #@31
    .line 199
    const v4, 0x1050051

    #@34
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@37
    move-result v4

    #@38
    iput v4, p2, Landroid/graphics/Rect;->top:I

    #@3a
    .line 201
    const v4, 0x1050053

    #@3d
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@40
    move-result v4

    #@41
    iput v4, p2, Landroid/graphics/Rect;->bottom:I

    #@43
    .line 204
    .end local v0           #appInfo:Landroid/content/pm/ApplicationInfo;
    .end local v3           #r:Landroid/content/res/Resources;
    :cond_43
    :goto_43
    return-object p2

    #@44
    .line 183
    :cond_44
    invoke-virtual {p2, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    #@47
    goto :goto_c

    #@48
    .line 188
    :catch_48
    move-exception v1

    #@49
    .line 190
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_43
.end method

.method private getRemoteContext(Landroid/widget/RemoteViews;)Landroid/content/Context;
    .registers 7
    .parameter "views"

    #@0
    .prologue
    .line 471
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 472
    .local v1, packageName:Ljava/lang/String;
    if-nez v1, :cond_9

    #@6
    iget-object v2, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@8
    .line 480
    :goto_8
    return-object v2

    #@9
    .line 476
    :cond_9
    :try_start_9
    iget-object v2, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@b
    const/4 v3, 0x4

    #@c
    iget-object v4, p0, Landroid/appwidget/AppWidgetHostView;->mUser:Landroid/os/UserHandle;

    #@e
    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_11} :catch_13

    #@11
    move-result-object v2

    #@12
    goto :goto_8

    #@13
    .line 478
    :catch_13
    move-exception v0

    #@14
    .line 479
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "AppWidgetHostView"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Package name "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, " not found"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 480
    iget-object v2, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@34
    goto :goto_8
.end method


# virtual methods
.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 229
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-direct {p0}, Landroid/appwidget/AppWidgetHostView;->generateId()I

    #@3
    move-result v3

    #@4
    invoke-virtual {p1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v2

    #@8
    check-cast v2, Landroid/os/Parcelable;

    #@a
    .line 231
    .local v2, parcelable:Landroid/os/Parcelable;
    const/4 v1, 0x0

    #@b
    .line 232
    .local v1, jail:Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;
    if-eqz v2, :cond_14

    #@d
    instance-of v3, v2, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;

    #@f
    if-eqz v3, :cond_14

    #@11
    move-object v1, v2

    #@12
    .line 233
    check-cast v1, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;

    #@14
    .line 236
    :cond_14
    if-nez v1, :cond_1c

    #@16
    new-instance v1, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;

    #@18
    .end local v1           #jail:Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;
    const/4 v3, 0x0

    #@19
    invoke-direct {v1, v3}, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;-><init>(Landroid/appwidget/AppWidgetHostView$1;)V

    #@1c
    .line 239
    .restart local v1       #jail:Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;
    :cond_1c
    :try_start_1c
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1f} :catch_20

    #@1f
    .line 244
    :goto_1f
    return-void

    #@20
    .line 240
    :catch_20
    move-exception v0

    #@21
    .line 241
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "AppWidgetHostView"

    #@23
    new-instance v3, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v5, "failed to restoreInstanceState for widget id: "

    #@2a
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    iget v5, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@30
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    const-string v5, ", "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    iget-object v3, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@3c
    if-nez v3, :cond_4d

    #@3e
    const-string/jumbo v3, "null"

    #@41
    :goto_41
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v4, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4c
    goto :goto_1f

    #@4d
    :cond_4d
    iget-object v3, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@4f
    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@51
    goto :goto_41
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 217
    .local p1, container:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    new-instance v0, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;-><init>(Landroid/appwidget/AppWidgetHostView$1;)V

    #@6
    .line 218
    .local v0, jail:Landroid/appwidget/AppWidgetHostView$ParcelableSparseArray;
    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->dispatchSaveInstanceState(Landroid/util/SparseArray;)V

    #@9
    .line 219
    invoke-direct {p0}, Landroid/appwidget/AppWidgetHostView;->generateId()I

    #@c
    move-result v1

    #@d
    invoke-virtual {p1, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@10
    .line 220
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .registers 6
    .parameter "canvas"
    .parameter "child"
    .parameter "drawingTime"

    #@0
    .prologue
    .line 519
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetHostView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 327
    iget-object v1, p0, Landroid/appwidget/AppWidgetHostView;->mRemoteContext:Landroid/content/Context;

    #@2
    if-eqz v1, :cond_c

    #@4
    iget-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mRemoteContext:Landroid/content/Context;

    #@6
    .line 328
    .local v0, context:Landroid/content/Context;
    :goto_6
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    #@8
    invoke-direct {v1, v0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@b
    return-object v1

    #@c
    .line 327
    .end local v0           #context:Landroid/content/Context;
    :cond_c
    iget-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@e
    goto :goto_6
.end method

.method public getAppWidgetId()I
    .registers 2

    #@0
    .prologue
    .line 208
    iget v0, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@2
    return v0
.end method

.method public getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;
    .registers 2

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@2
    return-object v0
.end method

.method protected getDefaultView()Landroid/view/View;
    .registers 15

    #@0
    .prologue
    .line 546
    const/4 v1, 0x0

    #@1
    .line 547
    .local v1, defaultView:Landroid/view/View;
    const/4 v3, 0x0

    #@2
    .line 550
    .local v3, exception:Ljava/lang/Exception;
    :try_start_2
    iget-object v10, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@4
    if-eqz v10, :cond_8a

    #@6
    .line 551
    iget-object v10, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@8
    iget-object v11, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@a
    iget-object v11, v11, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@c
    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@f
    move-result-object v11

    #@10
    const/4 v12, 0x4

    #@11
    iget-object v13, p0, Landroid/appwidget/AppWidgetHostView;->mUser:Landroid/os/UserHandle;

    #@13
    invoke-virtual {v10, v11, v12, v13}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    #@16
    move-result-object v9

    #@17
    .line 553
    .local v9, theirContext:Landroid/content/Context;
    iput-object v9, p0, Landroid/appwidget/AppWidgetHostView;->mRemoteContext:Landroid/content/Context;

    #@19
    .line 554
    const-string/jumbo v10, "layout_inflater"

    #@1c
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Landroid/view/LayoutInflater;

    #@22
    .line 556
    .local v4, inflater:Landroid/view/LayoutInflater;
    invoke-virtual {v4, v9}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@25
    move-result-object v4

    #@26
    .line 557
    sget-object v10, Landroid/appwidget/AppWidgetHostView;->sInflaterFilter:Landroid/view/LayoutInflater$Filter;

    #@28
    invoke-virtual {v4, v10}, Landroid/view/LayoutInflater;->setFilter(Landroid/view/LayoutInflater$Filter;)V

    #@2b
    .line 558
    iget-object v10, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@2d
    invoke-static {v10}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@30
    move-result-object v7

    #@31
    .line 559
    .local v7, manager:Landroid/appwidget/AppWidgetManager;
    iget v10, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@33
    invoke-virtual {v7, v10}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    #@36
    move-result-object v8

    #@37
    .line 561
    .local v8, options:Landroid/os/Bundle;
    iget-object v10, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@39
    iget v6, v10, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    #@3b
    .line 562
    .local v6, layoutId:I
    const-string v10, "appWidgetCategory"

    #@3d
    invoke-virtual {v8, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@40
    move-result v10

    #@41
    if-eqz v10, :cond_52

    #@43
    .line 563
    const-string v10, "appWidgetCategory"

    #@45
    invoke-virtual {v8, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@48
    move-result v0

    #@49
    .line 564
    .local v0, category:I
    const/4 v10, 0x2

    #@4a
    if-ne v0, v10, :cond_52

    #@4c
    .line 565
    iget-object v10, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@4e
    iget v5, v10, Landroid/appwidget/AppWidgetProviderInfo;->initialKeyguardLayout:I

    #@50
    .line 568
    .local v5, kgLayoutId:I
    if-nez v5, :cond_88

    #@52
    .line 571
    .end local v0           #category:I
    .end local v5           #kgLayoutId:I
    :cond_52
    :goto_52
    const/4 v10, 0x0

    #@53
    invoke-virtual {v4, v6, p0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_56
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_56} :catch_92
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_56} :catch_95

    #@56
    move-result-object v1

    #@57
    .line 581
    .end local v4           #inflater:Landroid/view/LayoutInflater;
    .end local v6           #layoutId:I
    .end local v7           #manager:Landroid/appwidget/AppWidgetManager;
    .end local v8           #options:Landroid/os/Bundle;
    .end local v9           #theirContext:Landroid/content/Context;
    :goto_57
    if-eqz v3, :cond_81

    #@59
    .line 582
    const-string v10, "AppWidgetHostView"

    #@5b
    new-instance v11, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v12, "Error inflating AppWidget "

    #@62
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v11

    #@66
    iget-object v12, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@68
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v11

    #@6c
    const-string v12, ": "

    #@6e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v11

    #@72
    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@75
    move-result-object v12

    #@76
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v11

    #@7a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v11

    #@7e
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 585
    :cond_81
    if-nez v1, :cond_87

    #@83
    .line 587
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHostView;->getErrorView()Landroid/view/View;

    #@86
    move-result-object v1

    #@87
    .line 590
    :cond_87
    return-object v1

    #@88
    .restart local v0       #category:I
    .restart local v4       #inflater:Landroid/view/LayoutInflater;
    .restart local v5       #kgLayoutId:I
    .restart local v6       #layoutId:I
    .restart local v7       #manager:Landroid/appwidget/AppWidgetManager;
    .restart local v8       #options:Landroid/os/Bundle;
    .restart local v9       #theirContext:Landroid/content/Context;
    :cond_88
    move v6, v5

    #@89
    .line 568
    goto :goto_52

    #@8a
    .line 573
    .end local v0           #category:I
    .end local v4           #inflater:Landroid/view/LayoutInflater;
    .end local v5           #kgLayoutId:I
    .end local v6           #layoutId:I
    .end local v7           #manager:Landroid/appwidget/AppWidgetManager;
    .end local v8           #options:Landroid/os/Bundle;
    .end local v9           #theirContext:Landroid/content/Context;
    :cond_8a
    :try_start_8a
    const-string v10, "AppWidgetHostView"

    #@8c
    const-string v11, "can\'t inflate defaultView because mInfo is missing"

    #@8e
    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_91
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8a .. :try_end_91} :catch_92
    .catch Ljava/lang/RuntimeException; {:try_start_8a .. :try_end_91} :catch_95

    #@91
    goto :goto_57

    #@92
    .line 575
    :catch_92
    move-exception v2

    #@93
    .line 576
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object v3, v2

    #@94
    .line 579
    goto :goto_57

    #@95
    .line 577
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_95
    move-exception v2

    #@96
    .line 578
    .local v2, e:Ljava/lang/RuntimeException;
    move-object v3, v2

    #@97
    goto :goto_57
.end method

.method protected getErrorView()Landroid/view/View;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 597
    new-instance v0, Landroid/widget/TextView;

    #@3
    iget-object v1, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@5
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    #@8
    .line 598
    .local v0, tv:Landroid/widget/TextView;
    const v1, 0x1040498

    #@b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@e
    .line 600
    const/16 v1, 0x7f

    #@10
    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    #@13
    move-result v1

    #@14
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    #@17
    .line 601
    return-object v0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 606
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    #@3
    .line 607
    const-class v0, Landroid/appwidget/AppWidgetHostView;

    #@5
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    #@c
    .line 608
    return-void
.end method

.method protected prepareView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 529
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    #@4
    move-result-object v0

    #@5
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    #@7
    .line 530
    .local v0, requested:Landroid/widget/FrameLayout$LayoutParams;
    if-nez v0, :cond_e

    #@9
    .line 531
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    #@b
    .end local v0           #requested:Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    #@e
    .line 535
    .restart local v0       #requested:Landroid/widget/FrameLayout$LayoutParams;
    :cond_e
    const/16 v1, 0x11

    #@10
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    #@12
    .line 536
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    #@15
    .line 537
    return-void
.end method

.method resetAppWidget(Landroid/appwidget/AppWidgetProviderInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 336
    iput-object p1, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@2
    .line 337
    const/4 v0, 0x0

    #@3
    iput v0, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@5
    .line 338
    const/4 v0, 0x0

    #@6
    invoke-virtual {p0, v0}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    #@9
    .line 339
    return-void
.end method

.method public setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V
    .registers 8
    .parameter "appWidgetId"
    .parameter "info"

    #@0
    .prologue
    .line 146
    iput p1, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@2
    .line 147
    iput-object p2, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@4
    .line 151
    if-eqz p2, :cond_1f

    #@6
    .line 153
    iget-object v1, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@8
    iget-object v2, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@a
    const/4 v3, 0x0

    #@b
    invoke-static {v1, v2, v3}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@e
    move-result-object v0

    #@f
    .line 154
    .local v0, padding:Landroid/graphics/Rect;
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@11
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@13
    iget v3, v0, Landroid/graphics/Rect;->right:I

    #@15
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    #@17
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/appwidget/AppWidgetHostView;->setPadding(IIII)V

    #@1a
    .line 155
    iget-object v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    #@1c
    invoke-virtual {p0, v1}, Landroid/appwidget/AppWidgetHostView;->setContentDescription(Ljava/lang/CharSequence;)V

    #@1f
    .line 157
    .end local v0           #padding:Landroid/graphics/Rect;
    :cond_1f
    return-void
.end method

.method public setOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V
    .registers 2
    .parameter "handler"

    #@0
    .prologue
    .line 137
    iput-object p1, p0, Landroid/appwidget/AppWidgetHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@2
    .line 138
    return-void
.end method

.method public setUserId(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 126
    new-instance v0, Landroid/os/UserHandle;

    #@2
    invoke-direct {v0, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@5
    iput-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mUser:Landroid/os/UserHandle;

    #@7
    .line 127
    return-void
.end method

.method public updateAppWidget(Landroid/widget/RemoteViews;)V
    .registers 11
    .parameter "remoteViews"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v8, 0x2

    #@2
    .line 349
    const/4 v4, 0x0

    #@3
    .line 350
    .local v4, recycled:Z
    const/4 v0, 0x0

    #@4
    .line 351
    .local v0, content:Landroid/view/View;
    const/4 v2, 0x0

    #@5
    .line 372
    .local v2, exception:Ljava/lang/Exception;
    if-nez p1, :cond_3d

    #@7
    .line 373
    iget v5, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@9
    if-ne v5, v6, :cond_c

    #@b
    .line 442
    :cond_b
    :goto_b
    return-void

    #@c
    .line 377
    :cond_c
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHostView;->getDefaultView()Landroid/view/View;

    #@f
    move-result-object v0

    #@10
    .line 378
    const/4 v5, -0x1

    #@11
    iput v5, p0, Landroid/appwidget/AppWidgetHostView;->mLayoutId:I

    #@13
    .line 379
    iput v6, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@15
    .line 413
    :goto_15
    if-nez v0, :cond_29

    #@17
    .line 414
    iget v5, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@19
    if-eq v5, v8, :cond_b

    #@1b
    .line 418
    const-string v5, "AppWidgetHostView"

    #@1d
    const-string/jumbo v6, "updateAppWidget couldn\'t find any view, using error view"

    #@20
    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    .line 419
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHostView;->getErrorView()Landroid/view/View;

    #@26
    move-result-object v0

    #@27
    .line 420
    iput v8, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@29
    .line 423
    :cond_29
    if-nez v4, :cond_31

    #@2b
    .line 424
    invoke-virtual {p0, v0}, Landroid/appwidget/AppWidgetHostView;->prepareView(Landroid/view/View;)V

    #@2e
    .line 425
    invoke-virtual {p0, v0}, Landroid/appwidget/AppWidgetHostView;->addView(Landroid/view/View;)V

    #@31
    .line 428
    :cond_31
    iget-object v5, p0, Landroid/appwidget/AppWidgetHostView;->mView:Landroid/view/View;

    #@33
    if-eq v5, v0, :cond_b

    #@35
    .line 429
    iget-object v5, p0, Landroid/appwidget/AppWidgetHostView;->mView:Landroid/view/View;

    #@37
    invoke-virtual {p0, v5}, Landroid/appwidget/AppWidgetHostView;->removeView(Landroid/view/View;)V

    #@3a
    .line 430
    iput-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mView:Landroid/view/View;

    #@3c
    goto :goto_b

    #@3d
    .line 383
    :cond_3d
    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetHostView;->getRemoteContext(Landroid/widget/RemoteViews;)Landroid/content/Context;

    #@40
    move-result-object v5

    #@41
    iput-object v5, p0, Landroid/appwidget/AppWidgetHostView;->mRemoteContext:Landroid/content/Context;

    #@43
    .line 384
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    #@46
    move-result v3

    #@47
    .line 388
    .local v3, layoutId:I
    if-nez v0, :cond_59

    #@49
    iget v5, p0, Landroid/appwidget/AppWidgetHostView;->mLayoutId:I

    #@4b
    if-ne v3, v5, :cond_59

    #@4d
    .line 390
    :try_start_4d
    iget-object v5, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@4f
    iget-object v6, p0, Landroid/appwidget/AppWidgetHostView;->mView:Landroid/view/View;

    #@51
    iget-object v7, p0, Landroid/appwidget/AppWidgetHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@53
    invoke-virtual {p1, v5, v6, v7}, Landroid/widget/RemoteViews;->reapply(Landroid/content/Context;Landroid/view/View;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@56
    .line 391
    iget-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mView:Landroid/view/View;
    :try_end_58
    .catch Ljava/lang/RuntimeException; {:try_start_4d .. :try_end_58} :catch_69

    #@58
    .line 392
    const/4 v4, 0x1

    #@59
    .line 400
    :cond_59
    :goto_59
    if-nez v0, :cond_63

    #@5b
    .line 402
    :try_start_5b
    iget-object v5, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@5d
    iget-object v6, p0, Landroid/appwidget/AppWidgetHostView;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@5f
    invoke-virtual {p1, v5, p0, v6}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/widget/RemoteViews$OnClickHandler;)Landroid/view/View;
    :try_end_62
    .catch Ljava/lang/RuntimeException; {:try_start_5b .. :try_end_62} :catch_6c

    #@62
    move-result-object v0

    #@63
    .line 409
    :cond_63
    :goto_63
    iput v3, p0, Landroid/appwidget/AppWidgetHostView;->mLayoutId:I

    #@65
    .line 410
    const/4 v5, 0x1

    #@66
    iput v5, p0, Landroid/appwidget/AppWidgetHostView;->mViewMode:I

    #@68
    goto :goto_15

    #@69
    .line 394
    :catch_69
    move-exception v1

    #@6a
    .line 395
    .local v1, e:Ljava/lang/RuntimeException;
    move-object v2, v1

    #@6b
    goto :goto_59

    #@6c
    .line 404
    .end local v1           #e:Ljava/lang/RuntimeException;
    :catch_6c
    move-exception v1

    #@6d
    .line 405
    .restart local v1       #e:Ljava/lang/RuntimeException;
    move-object v2, v1

    #@6e
    goto :goto_63
.end method

.method public updateAppWidgetOptions(Landroid/os/Bundle;)V
    .registers 4
    .parameter "options"

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@5
    move-result-object v0

    #@6
    iget v1, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@8
    invoke-virtual {v0, v1, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidgetOptions(ILandroid/os/Bundle;)V

    #@b
    .line 319
    return-void
.end method

.method public updateAppWidgetSize(Landroid/os/Bundle;IIII)V
    .registers 13
    .parameter "newOptions"
    .parameter "minWidth"
    .parameter "minHeight"
    .parameter "maxWidth"
    .parameter "maxHeight"

    #@0
    .prologue
    .line 263
    const/4 v6, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Landroid/appwidget/AppWidgetHostView;->updateAppWidgetSize(Landroid/os/Bundle;IIIIZ)V

    #@a
    .line 264
    return-void
.end method

.method public updateAppWidgetSize(Landroid/os/Bundle;IIIIZ)V
    .registers 20
    .parameter "newOptions"
    .parameter "minWidth"
    .parameter "minHeight"
    .parameter "maxWidth"
    .parameter "maxHeight"
    .parameter "ignorePadding"

    #@0
    .prologue
    .line 271
    if-nez p1, :cond_7

    #@2
    .line 272
    new-instance p1, Landroid/os/Bundle;

    #@4
    .end local p1
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    #@7
    .line 275
    .restart local p1
    :cond_7
    new-instance v7, Landroid/graphics/Rect;

    #@9
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    #@c
    .line 276
    .local v7, padding:Landroid/graphics/Rect;
    iget-object v11, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@e
    if-eqz v11, :cond_1a

    #@10
    .line 277
    iget-object v11, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@12
    iget-object v12, p0, Landroid/appwidget/AppWidgetHostView;->mInfo:Landroid/appwidget/AppWidgetProviderInfo;

    #@14
    iget-object v12, v12, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    #@16
    invoke-static {v11, v12, v7}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    #@19
    move-result-object v7

    #@1a
    .line 279
    :cond_1a
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHostView;->getResources()Landroid/content/res/Resources;

    #@1d
    move-result-object v11

    #@1e
    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@21
    move-result-object v11

    #@22
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    #@24
    .line 281
    .local v0, density:F
    iget v11, v7, Landroid/graphics/Rect;->left:I

    #@26
    iget v12, v7, Landroid/graphics/Rect;->right:I

    #@28
    add-int/2addr v11, v12

    #@29
    int-to-float v11, v11

    #@2a
    div-float/2addr v11, v0

    #@2b
    float-to-int v9, v11

    #@2c
    .line 282
    .local v9, xPaddingDips:I
    iget v11, v7, Landroid/graphics/Rect;->top:I

    #@2e
    iget v12, v7, Landroid/graphics/Rect;->bottom:I

    #@30
    add-int/2addr v11, v12

    #@31
    int-to-float v11, v11

    #@32
    div-float/2addr v11, v0

    #@33
    float-to-int v10, v11

    #@34
    .line 284
    .local v10, yPaddingDips:I
    if-eqz p6, :cond_90

    #@36
    const/4 v11, 0x0

    #@37
    :goto_37
    sub-int v5, p2, v11

    #@39
    .line 285
    .local v5, newMinWidth:I
    if-eqz p6, :cond_92

    #@3b
    const/4 v11, 0x0

    #@3c
    :goto_3c
    sub-int v4, p3, v11

    #@3e
    .line 286
    .local v4, newMinHeight:I
    if-eqz p6, :cond_41

    #@40
    const/4 v9, 0x0

    #@41
    .end local v9           #xPaddingDips:I
    :cond_41
    sub-int v3, p4, v9

    #@43
    .line 287
    .local v3, newMaxWidth:I
    if-eqz p6, :cond_46

    #@45
    const/4 v10, 0x0

    #@46
    .end local v10           #yPaddingDips:I
    :cond_46
    sub-int v2, p5, v10

    #@48
    .line 289
    .local v2, newMaxHeight:I
    iget-object v11, p0, Landroid/appwidget/AppWidgetHostView;->mContext:Landroid/content/Context;

    #@4a
    invoke-static {v11}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@4d
    move-result-object v8

    #@4e
    .line 292
    .local v8, widgetManager:Landroid/appwidget/AppWidgetManager;
    iget v11, p0, Landroid/appwidget/AppWidgetHostView;->mAppWidgetId:I

    #@50
    invoke-virtual {v8, v11}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    #@53
    move-result-object v6

    #@54
    .line 293
    .local v6, oldOptions:Landroid/os/Bundle;
    const/4 v1, 0x0

    #@55
    .line 294
    .local v1, needsUpdate:Z
    const-string v11, "appWidgetMinWidth"

    #@57
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@5a
    move-result v11

    #@5b
    if-ne v5, v11, :cond_75

    #@5d
    const-string v11, "appWidgetMinHeight"

    #@5f
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@62
    move-result v11

    #@63
    if-ne v4, v11, :cond_75

    #@65
    const-string v11, "appWidgetMaxWidth"

    #@67
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@6a
    move-result v11

    #@6b
    if-ne v3, v11, :cond_75

    #@6d
    const-string v11, "appWidgetMaxHeight"

    #@6f
    invoke-virtual {v6, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@72
    move-result v11

    #@73
    if-eq v2, v11, :cond_76

    #@75
    .line 298
    :cond_75
    const/4 v1, 0x1

    #@76
    .line 301
    :cond_76
    if-eqz v1, :cond_8f

    #@78
    .line 302
    const-string v11, "appWidgetMinWidth"

    #@7a
    invoke-virtual {p1, v11, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@7d
    .line 303
    const-string v11, "appWidgetMinHeight"

    #@7f
    invoke-virtual {p1, v11, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@82
    .line 304
    const-string v11, "appWidgetMaxWidth"

    #@84
    invoke-virtual {p1, v11, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@87
    .line 305
    const-string v11, "appWidgetMaxHeight"

    #@89
    invoke-virtual {p1, v11, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@8c
    .line 306
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetHostView;->updateAppWidgetOptions(Landroid/os/Bundle;)V

    #@8f
    .line 308
    :cond_8f
    return-void

    #@90
    .end local v1           #needsUpdate:Z
    .end local v2           #newMaxHeight:I
    .end local v3           #newMaxWidth:I
    .end local v4           #newMinHeight:I
    .end local v5           #newMinWidth:I
    .end local v6           #oldOptions:Landroid/os/Bundle;
    .end local v8           #widgetManager:Landroid/appwidget/AppWidgetManager;
    .restart local v9       #xPaddingDips:I
    .restart local v10       #yPaddingDips:I
    :cond_90
    move v11, v9

    #@91
    .line 284
    goto :goto_37

    #@92
    .restart local v5       #newMinWidth:I
    :cond_92
    move v11, v10

    #@93
    .line 285
    goto :goto_3c
.end method

.method viewDataChanged(I)V
    .registers 7
    .parameter "viewId"

    #@0
    .prologue
    .line 449
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetHostView;->findViewById(I)Landroid/view/View;

    #@3
    move-result-object v3

    #@4
    .line 450
    .local v3, v:Landroid/view/View;
    if-eqz v3, :cond_1b

    #@6
    instance-of v4, v3, Landroid/widget/AdapterView;

    #@8
    if-eqz v4, :cond_1b

    #@a
    move-object v1, v3

    #@b
    .line 451
    check-cast v1, Landroid/widget/AdapterView;

    #@d
    .line 452
    .local v1, adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    invoke-virtual {v1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    #@10
    move-result-object v0

    #@11
    .line 453
    .local v0, adapter:Landroid/widget/Adapter;
    instance-of v4, v0, Landroid/widget/BaseAdapter;

    #@13
    if-eqz v4, :cond_1c

    #@15
    move-object v2, v0

    #@16
    .line 454
    check-cast v2, Landroid/widget/BaseAdapter;

    #@18
    .line 455
    .local v2, baseAdapter:Landroid/widget/BaseAdapter;
    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    #@1b
    .line 463
    .end local v0           #adapter:Landroid/widget/Adapter;
    .end local v1           #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    .end local v2           #baseAdapter:Landroid/widget/BaseAdapter;
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 456
    .restart local v0       #adapter:Landroid/widget/Adapter;
    .restart local v1       #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    :cond_1c
    if-nez v0, :cond_1b

    #@1e
    instance-of v4, v1, Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;

    #@20
    if-eqz v4, :cond_1b

    #@22
    .line 460
    check-cast v1, Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;

    #@24
    .end local v1           #adapterView:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    invoke-interface {v1}, Landroid/widget/RemoteViewsAdapter$RemoteAdapterConnectionCallback;->deferNotifyDataSetChanged()V

    #@27
    goto :goto_1b
.end method
