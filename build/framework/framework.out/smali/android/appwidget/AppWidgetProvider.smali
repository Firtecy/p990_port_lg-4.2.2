.class public Landroid/appwidget/AppWidgetProvider;
.super Landroid/content/BroadcastReceiver;
.source "AppWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 47
    return-void
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .registers 5
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetId"
    .parameter "newOptions"

    #@0
    .prologue
    .line 133
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .registers 3
    .parameter "context"
    .parameter "appWidgetIds"

    #@0
    .prologue
    .line 149
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 183
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 168
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 60
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 61
    .local v0, action:Ljava/lang/String;
    const-string v5, "android.appwidget.action.APPWIDGET_UPDATE"

    #@6
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v5

    #@a
    if-eqz v5, :cond_25

    #@c
    .line 62
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@f
    move-result-object v3

    #@10
    .line 63
    .local v3, extras:Landroid/os/Bundle;
    if-eqz v3, :cond_24

    #@12
    .line 64
    const-string v5, "appWidgetIds"

    #@14
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    #@17
    move-result-object v2

    #@18
    .line 65
    .local v2, appWidgetIds:[I
    if-eqz v2, :cond_24

    #@1a
    array-length v5, v2

    #@1b
    if-lez v5, :cond_24

    #@1d
    .line 66
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {p0, p1, v5, v2}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    #@24
    .line 93
    .end local v2           #appWidgetIds:[I
    .end local v3           #extras:Landroid/os/Bundle;
    :cond_24
    :goto_24
    return-void

    #@25
    .line 70
    :cond_25
    const-string v5, "android.appwidget.action.APPWIDGET_DELETED"

    #@27
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_4b

    #@2d
    .line 71
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@30
    move-result-object v3

    #@31
    .line 72
    .restart local v3       #extras:Landroid/os/Bundle;
    if-eqz v3, :cond_24

    #@33
    const-string v5, "appWidgetId"

    #@35
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@38
    move-result v5

    #@39
    if-eqz v5, :cond_24

    #@3b
    .line 73
    const-string v5, "appWidgetId"

    #@3d
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@40
    move-result v1

    #@41
    .line 74
    .local v1, appWidgetId:I
    const/4 v5, 0x1

    #@42
    new-array v5, v5, [I

    #@44
    const/4 v6, 0x0

    #@45
    aput v1, v5, v6

    #@47
    invoke-virtual {p0, p1, v5}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    #@4a
    goto :goto_24

    #@4b
    .line 77
    .end local v1           #appWidgetId:I
    .end local v3           #extras:Landroid/os/Bundle;
    :cond_4b
    const-string v5, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    #@4d
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@50
    move-result v5

    #@51
    if-eqz v5, :cond_7d

    #@53
    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@56
    move-result-object v3

    #@57
    .line 79
    .restart local v3       #extras:Landroid/os/Bundle;
    if-eqz v3, :cond_24

    #@59
    const-string v5, "appWidgetId"

    #@5b
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@5e
    move-result v5

    #@5f
    if-eqz v5, :cond_24

    #@61
    const-string v5, "appWidgetOptions"

    #@63
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    #@66
    move-result v5

    #@67
    if-eqz v5, :cond_24

    #@69
    .line 81
    const-string v5, "appWidgetId"

    #@6b
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@6e
    move-result v1

    #@6f
    .line 82
    .restart local v1       #appWidgetId:I
    const-string v5, "appWidgetOptions"

    #@71
    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    #@74
    move-result-object v4

    #@75
    .line 83
    .local v4, widgetExtras:Landroid/os/Bundle;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {p0, p1, v5, v1, v4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    #@7c
    goto :goto_24

    #@7d
    .line 87
    .end local v1           #appWidgetId:I
    .end local v3           #extras:Landroid/os/Bundle;
    .end local v4           #widgetExtras:Landroid/os/Bundle;
    :cond_7d
    const-string v5, "android.appwidget.action.APPWIDGET_ENABLED"

    #@7f
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@82
    move-result v5

    #@83
    if-eqz v5, :cond_89

    #@85
    .line 88
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    #@88
    goto :goto_24

    #@89
    .line 90
    :cond_89
    const-string v5, "android.appwidget.action.APPWIDGET_DISABLED"

    #@8b
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v5

    #@8f
    if-eqz v5, :cond_24

    #@91
    .line 91
    invoke-virtual {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    #@94
    goto :goto_24
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 4
    .parameter "context"
    .parameter "appWidgetManager"
    .parameter "appWidgetIds"

    #@0
    .prologue
    .line 114
    return-void
.end method
