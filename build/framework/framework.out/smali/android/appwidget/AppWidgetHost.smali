.class public Landroid/appwidget/AppWidgetHost;
.super Ljava/lang/Object;
.source "AppWidgetHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/appwidget/AppWidgetHost$UpdateHandler;,
        Landroid/appwidget/AppWidgetHost$Callbacks;
    }
.end annotation


# static fields
.field static final HANDLE_PROVIDERS_CHANGED:I = 0x3

.field static final HANDLE_PROVIDER_CHANGED:I = 0x2

.field static final HANDLE_UPDATE:I = 0x1

.field static final HANDLE_VIEW_DATA_CHANGED:I = 0x4

.field static sService:Lcom/android/internal/appwidget/IAppWidgetService;

.field static final sServiceLock:Ljava/lang/Object;


# instance fields
.field mCallbacks:Landroid/appwidget/AppWidgetHost$Callbacks;

.field mContext:Landroid/content/Context;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field mHandler:Landroid/os/Handler;

.field mHostId:I

.field private mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

.field mPackageName:Ljava/lang/String;

.field private mUser:Landroid/os/UserHandle;

.field final mViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/appwidget/AppWidgetHostView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 52
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/appwidget/AppWidgetHost;->sServiceLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .parameter "context"
    .parameter "hostId"

    #@0
    .prologue
    .line 131
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@4
    move-result-object v1

    #@5
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;ILandroid/widget/RemoteViews$OnClickHandler;Landroid/os/Looper;)V

    #@8
    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/widget/RemoteViews$OnClickHandler;Landroid/os/Looper;)V
    .registers 6
    .parameter "context"
    .parameter "hostId"
    .parameter "handler"
    .parameter "looper"

    #@0
    .prologue
    .line 137
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 124
    new-instance v0, Landroid/appwidget/AppWidgetHost$Callbacks;

    #@5
    invoke-direct {v0, p0}, Landroid/appwidget/AppWidgetHost$Callbacks;-><init>(Landroid/appwidget/AppWidgetHost;)V

    #@8
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mCallbacks:Landroid/appwidget/AppWidgetHost$Callbacks;

    #@a
    .line 125
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@11
    .line 138
    iput-object p1, p0, Landroid/appwidget/AppWidgetHost;->mContext:Landroid/content/Context;

    #@13
    .line 139
    iput p2, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@15
    .line 140
    iput-object p3, p0, Landroid/appwidget/AppWidgetHost;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@17
    .line 141
    new-instance v0, Landroid/appwidget/AppWidgetHost$UpdateHandler;

    #@19
    invoke-direct {v0, p0, p4}, Landroid/appwidget/AppWidgetHost$UpdateHandler;-><init>(Landroid/appwidget/AppWidgetHost;Landroid/os/Looper;)V

    #@1c
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mHandler:Landroid/os/Handler;

    #@1e
    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@28
    .line 143
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mUser:Landroid/os/UserHandle;

    #@2e
    .line 144
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->bindService()V

    #@31
    .line 145
    return-void
.end method

.method static synthetic access$000(Landroid/appwidget/AppWidgetHost;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/appwidget/AppWidgetHost;->isLocalBinder()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Landroid/appwidget/AppWidgetHost;)Landroid/os/UserHandle;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/appwidget/AppWidgetHost;->mUser:Landroid/os/UserHandle;

    #@2
    return-object v0
.end method

.method public static allocateAppWidgetIdForSystem(I)I
    .registers 6
    .parameter "hostId"

    #@0
    .prologue
    .line 251
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->checkCallerIsSystem()V

    #@3
    .line 253
    :try_start_3
    sget-object v3, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@5
    if-nez v3, :cond_a

    #@7
    .line 254
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->bindService()V

    #@a
    .line 256
    :cond_a
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    #@11
    move-result-object v2

    #@12
    .line 258
    .local v2, systemContext:Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 259
    .local v1, packageName:Ljava/lang/String;
    sget-object v3, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@18
    invoke-interface {v3, v1, p0}, Lcom/android/internal/appwidget/IAppWidgetService;->allocateAppWidgetId(Ljava/lang/String;I)I
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_1b} :catch_1d

    #@1b
    move-result v3

    #@1c
    return v3

    #@1d
    .line 260
    .end local v1           #packageName:Ljava/lang/String;
    .end local v2           #systemContext:Landroid/content/Context;
    :catch_1d
    move-exception v0

    #@1e
    .line 261
    .local v0, e:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@20
    const-string/jumbo v4, "system server dead?"

    #@23
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@26
    throw v3
.end method

.method private static bindService()V
    .registers 3

    #@0
    .prologue
    .line 153
    sget-object v2, Landroid/appwidget/AppWidgetHost;->sServiceLock:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 154
    :try_start_3
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@5
    if-nez v1, :cond_13

    #@7
    .line 155
    const-string v1, "appwidget"

    #@9
    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    .line 156
    .local v0, b:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/appwidget/IAppWidgetService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/appwidget/IAppWidgetService;

    #@10
    move-result-object v1

    #@11
    sput-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@13
    .line 158
    :cond_13
    monitor-exit v2

    #@14
    .line 159
    return-void

    #@15
    .line 158
    :catchall_15
    move-exception v1

    #@16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method private static checkCallerIsSystem()V
    .registers 4

    #@0
    .prologue
    .line 282
    invoke-static {}, Landroid/os/Process;->myUid()I

    #@3
    move-result v0

    #@4
    .line 283
    .local v0, uid:I
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    #@7
    move-result v1

    #@8
    const/16 v2, 0x3e8

    #@a
    if-eq v1, v2, :cond_e

    #@c
    if-nez v0, :cond_f

    #@e
    .line 284
    :cond_e
    return-void

    #@f
    .line 286
    :cond_f
    new-instance v1, Ljava/lang/SecurityException;

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "Disallowed call for uid "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    #@27
    throw v1
.end method

.method public static deleteAllHosts()V
    .registers 3

    #@0
    .prologue
    .line 351
    :try_start_0
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@2
    invoke-interface {v1}, Lcom/android/internal/appwidget/IAppWidgetService;->deleteAllHosts()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 356
    return-void

    #@6
    .line 353
    :catch_6
    move-exception v0

    #@7
    .line 354
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@9
    const-string/jumbo v2, "system server dead?"

    #@c
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@f
    throw v1
.end method

.method public static deleteAppWidgetIdForSystem(I)V
    .registers 4
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 313
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->checkCallerIsSystem()V

    #@3
    .line 315
    :try_start_3
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@5
    if-nez v1, :cond_a

    #@7
    .line 316
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->bindService()V

    #@a
    .line 318
    :cond_a
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@c
    invoke-interface {v1, p0}, Lcom/android/internal/appwidget/IAppWidgetService;->deleteAppWidgetId(I)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_f} :catch_10

    #@f
    .line 322
    return-void

    #@10
    .line 319
    :catch_10
    move-exception v0

    #@11
    .line 320
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@13
    const-string/jumbo v2, "system server dead?"

    #@16
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@19
    throw v1
.end method

.method private isLocalBinder()Z
    .registers 3

    #@0
    .prologue
    .line 290
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@3
    move-result v0

    #@4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@7
    move-result v1

    #@8
    if-ne v0, v1, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method


# virtual methods
.method public allocateAppWidgetId()I
    .registers 5

    #@0
    .prologue
    .line 234
    :try_start_0
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@2
    if-nez v1, :cond_c

    #@4
    .line 235
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@c
    .line 237
    :cond_c
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@e
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@10
    iget v3, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@12
    invoke-interface {v1, v2, v3}, Lcom/android/internal/appwidget/IAppWidgetService;->allocateAppWidgetId(Ljava/lang/String;I)I
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_15} :catch_17

    #@15
    move-result v1

    #@16
    return v1

    #@17
    .line 239
    :catch_17
    move-exception v0

    #@18
    .line 240
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@1a
    const-string/jumbo v2, "system server dead?"

    #@1d
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@20
    throw v1
.end method

.method protected clearViews()V
    .registers 2

    #@0
    .prologue
    .line 452
    iget-object v0, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 453
    return-void
.end method

.method public final createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .registers 10
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "appWidget"

    #@0
    .prologue
    .line 364
    invoke-virtual {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetHost;->onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    #@3
    move-result-object v1

    #@4
    .line 365
    .local v1, view:Landroid/appwidget/AppWidgetHostView;
    iget-object v3, p0, Landroid/appwidget/AppWidgetHost;->mUser:Landroid/os/UserHandle;

    #@6
    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    #@9
    move-result v3

    #@a
    invoke-virtual {v1, v3}, Landroid/appwidget/AppWidgetHostView;->setUserId(I)V

    #@d
    .line 366
    iget-object v3, p0, Landroid/appwidget/AppWidgetHost;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@f
    invoke-virtual {v1, v3}, Landroid/appwidget/AppWidgetHostView;->setOnClickHandler(Landroid/widget/RemoteViews$OnClickHandler;)V

    #@12
    .line 367
    invoke-virtual {v1, p2, p3}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    #@15
    .line 368
    iget-object v4, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@17
    monitor-enter v4

    #@18
    .line 369
    :try_start_18
    iget-object v3, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@1a
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    .line 370
    monitor-exit v4
    :try_end_22
    .catchall {:try_start_18 .. :try_end_22} :catchall_33

    #@22
    .line 373
    :try_start_22
    sget-object v3, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@24
    invoke-interface {v3, p2}, Lcom/android/internal/appwidget/IAppWidgetService;->getAppWidgetViews(I)Landroid/widget/RemoteViews;

    #@27
    move-result-object v2

    #@28
    .line 374
    .local v2, views:Landroid/widget/RemoteViews;
    if-eqz v2, :cond_2f

    #@2a
    .line 375
    iget-object v3, p0, Landroid/appwidget/AppWidgetHost;->mUser:Landroid/os/UserHandle;

    #@2c
    invoke-virtual {v2, v3}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V
    :try_end_2f
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_2f} :catch_36

    #@2f
    .line 380
    :cond_2f
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    #@32
    .line 382
    return-object v1

    #@33
    .line 370
    .end local v2           #views:Landroid/widget/RemoteViews;
    :catchall_33
    move-exception v3

    #@34
    :try_start_34
    monitor-exit v4
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    #@35
    throw v3

    #@36
    .line 377
    :catch_36
    move-exception v0

    #@37
    .line 378
    .local v0, e:Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/RuntimeException;

    #@39
    const-string/jumbo v4, "system server dead?"

    #@3c
    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@3f
    throw v3
.end method

.method public deleteAppWidgetId(I)V
    .registers 6
    .parameter "appWidgetId"

    #@0
    .prologue
    .line 297
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 298
    :try_start_3
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_1d

    #@c
    .line 300
    :try_start_c
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@e
    invoke-interface {v1, p1}, Lcom/android/internal/appwidget/IAppWidgetService;->deleteAppWidgetId(I)V
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_1d
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_11} :catch_13

    #@11
    .line 305
    :try_start_11
    monitor-exit v2

    #@12
    .line 306
    return-void

    #@13
    .line 302
    :catch_13
    move-exception v0

    #@14
    .line 303
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@16
    const-string/jumbo v3, "system server dead?"

    #@19
    invoke-direct {v1, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@1c
    throw v1

    #@1d
    .line 305
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_1d
    move-exception v1

    #@1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_11 .. :try_end_1f} :catchall_1d

    #@1f
    throw v1
.end method

.method public deleteHost()V
    .registers 4

    #@0
    .prologue
    .line 334
    :try_start_0
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@2
    iget v2, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@4
    invoke-interface {v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService;->deleteHost(I)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_8

    #@7
    .line 339
    return-void

    #@8
    .line 336
    :catch_8
    move-exception v0

    #@9
    .line 337
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@b
    const-string/jumbo v2, "system server dead?"

    #@e
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@11
    throw v1
.end method

.method public getAppWidgetIds()[I
    .registers 4

    #@0
    .prologue
    .line 272
    :try_start_0
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@2
    if-nez v1, :cond_7

    #@4
    .line 273
    invoke-static {}, Landroid/appwidget/AppWidgetHost;->bindService()V

    #@7
    .line 275
    :cond_7
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@9
    iget v2, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@b
    invoke-interface {v1, v2}, Lcom/android/internal/appwidget/IAppWidgetService;->getAppWidgetIdsForHost(I)[I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_10

    #@e
    move-result-object v1

    #@f
    return-object v1

    #@10
    .line 276
    :catch_10
    move-exception v0

    #@11
    .line 277
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@13
    const-string/jumbo v2, "system server dead?"

    #@16
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@19
    throw v1
.end method

.method protected onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .registers 6
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "appWidget"

    #@0
    .prologue
    .line 391
    new-instance v0, Landroid/appwidget/AppWidgetHostView;

    #@2
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mOnClickHandler:Landroid/widget/RemoteViews$OnClickHandler;

    #@4
    invoke-direct {v0, p1, v1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;Landroid/widget/RemoteViews$OnClickHandler;)V

    #@7
    return-object v0
.end method

.method protected onProviderChanged(ILandroid/appwidget/AppWidgetProviderInfo;)V
    .registers 7
    .parameter "appWidgetId"
    .parameter "appWidget"

    #@0
    .prologue
    .line 403
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@2
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@4
    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@7
    move-result v1

    #@8
    iput v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    #@a
    .line 405
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@c
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@e
    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@11
    move-result v1

    #@12
    iput v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    #@14
    .line 407
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@16
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@18
    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@1b
    move-result v1

    #@1c
    iput v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    #@1e
    .line 409
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@20
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    #@22
    invoke-static {v1, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    #@25
    move-result v1

    #@26
    iput v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    #@28
    .line 412
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2a
    monitor-enter v2

    #@2b
    .line 413
    :try_start_2b
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@37
    .line 414
    .local v0, v:Landroid/appwidget/AppWidgetHostView;
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_2b .. :try_end_38} :catchall_3e

    #@38
    .line 415
    if-eqz v0, :cond_3d

    #@3a
    .line 416
    invoke-virtual {v0, p2}, Landroid/appwidget/AppWidgetHostView;->resetAppWidget(Landroid/appwidget/AppWidgetProviderInfo;)V

    #@3d
    .line 418
    :cond_3d
    return-void

    #@3e
    .line 414
    .end local v0           #v:Landroid/appwidget/AppWidgetHostView;
    :catchall_3e
    move-exception v1

    #@3f
    :try_start_3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    #@40
    throw v1
.end method

.method protected onProvidersChanged()V
    .registers 1

    #@0
    .prologue
    .line 426
    return-void
.end method

.method public setUserId(I)V
    .registers 3
    .parameter "userId"

    #@0
    .prologue
    .line 149
    new-instance v0, Landroid/os/UserHandle;

    #@2
    invoke-direct {v0, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@5
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mUser:Landroid/os/UserHandle;

    #@7
    .line 150
    return-void
.end method

.method public startListening()V
    .registers 2

    #@0
    .prologue
    .line 166
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/appwidget/AppWidgetHost;->startListeningAsUser(I)V

    #@7
    .line 167
    return-void
.end method

.method public startListeningAsUser(I)V
    .registers 12
    .parameter "userId"

    #@0
    .prologue
    .line 176
    new-instance v4, Ljava/util/ArrayList;

    #@2
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 179
    .local v4, updatedViews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/widget/RemoteViews;>;"
    :try_start_5
    iget-object v0, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@7
    if-nez v0, :cond_11

    #@9
    .line 180
    iget-object v0, p0, Landroid/appwidget/AppWidgetHost;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@11
    .line 182
    :cond_11
    sget-object v0, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@13
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mCallbacks:Landroid/appwidget/AppWidgetHost$Callbacks;

    #@15
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mPackageName:Ljava/lang/String;

    #@17
    iget v3, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@19
    move v5, p1

    #@1a
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/appwidget/IAppWidgetService;->startListeningAsUser(Lcom/android/internal/appwidget/IAppWidgetHost;Ljava/lang/String;ILjava/util/List;I)[I
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_1d} :catch_44

    #@1d
    move-result-object v9

    #@1e
    .line 189
    .local v9, updatedIds:[I
    array-length v6, v9

    #@1f
    .line 190
    .local v6, N:I
    const/4 v8, 0x0

    #@20
    .local v8, i:I
    :goto_20
    if-ge v8, v6, :cond_4e

    #@22
    .line 191
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    if-eqz v0, :cond_36

    #@28
    .line 192
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Landroid/widget/RemoteViews;

    #@2e
    new-instance v1, Landroid/os/UserHandle;

    #@30
    invoke-direct {v1, p1}, Landroid/os/UserHandle;-><init>(I)V

    #@33
    invoke-virtual {v0, v1}, Landroid/widget/RemoteViews;->setUser(Landroid/os/UserHandle;)V

    #@36
    .line 194
    :cond_36
    aget v1, v9, v8

    #@38
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    check-cast v0, Landroid/widget/RemoteViews;

    #@3e
    invoke-virtual {p0, v1, v0}, Landroid/appwidget/AppWidgetHost;->updateAppWidgetView(ILandroid/widget/RemoteViews;)V

    #@41
    .line 190
    add-int/lit8 v8, v8, 0x1

    #@43
    goto :goto_20

    #@44
    .line 185
    .end local v6           #N:I
    .end local v8           #i:I
    .end local v9           #updatedIds:[I
    :catch_44
    move-exception v7

    #@45
    .line 186
    .local v7, e:Landroid/os/RemoteException;
    new-instance v0, Ljava/lang/RuntimeException;

    #@47
    const-string/jumbo v1, "system server dead?"

    #@4a
    invoke-direct {v0, v1, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@4d
    throw v0

    #@4e
    .line 196
    .end local v7           #e:Landroid/os/RemoteException;
    .restart local v6       #N:I
    .restart local v8       #i:I
    .restart local v9       #updatedIds:[I
    :cond_4e
    return-void
.end method

.method public stopListening()V
    .registers 5

    #@0
    .prologue
    .line 204
    :try_start_0
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@2
    iget v2, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@4
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    #@7
    move-result v3

    #@8
    invoke-interface {v1, v2, v3}, Lcom/android/internal/appwidget/IAppWidgetService;->stopListeningAsUser(II)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 209
    return-void

    #@c
    .line 206
    :catch_c
    move-exception v0

    #@d
    .line 207
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@f
    const-string/jumbo v2, "system server dead?"

    #@12
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@15
    throw v1
.end method

.method public stopListeningAsUser(I)V
    .registers 5
    .parameter "userId"

    #@0
    .prologue
    .line 218
    :try_start_0
    sget-object v1, Landroid/appwidget/AppWidgetHost;->sService:Lcom/android/internal/appwidget/IAppWidgetService;

    #@2
    iget v2, p0, Landroid/appwidget/AppWidgetHost;->mHostId:I

    #@4
    invoke-interface {v1, v2, p1}, Lcom/android/internal/appwidget/IAppWidgetService;->stopListeningAsUser(II)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_7} :catch_b

    #@7
    .line 224
    invoke-virtual {p0}, Landroid/appwidget/AppWidgetHost;->clearViews()V

    #@a
    .line 225
    return-void

    #@b
    .line 220
    :catch_b
    move-exception v0

    #@c
    .line 221
    .local v0, e:Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    #@e
    const-string/jumbo v2, "system server dead?"

    #@11
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@14
    throw v1
.end method

.method updateAppWidgetView(ILandroid/widget/RemoteViews;)V
    .registers 7
    .parameter "appWidgetId"
    .parameter "views"

    #@0
    .prologue
    .line 430
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 431
    :try_start_3
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@f
    .line 432
    .local v0, v:Landroid/appwidget/AppWidgetHostView;
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_16

    #@10
    .line 433
    if-eqz v0, :cond_15

    #@12
    .line 434
    invoke-virtual {v0, p2}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    #@15
    .line 436
    :cond_15
    return-void

    #@16
    .line 432
    .end local v0           #v:Landroid/appwidget/AppWidgetHostView;
    :catchall_16
    move-exception v1

    #@17
    :try_start_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method viewDataChanged(II)V
    .registers 7
    .parameter "appWidgetId"
    .parameter "viewId"

    #@0
    .prologue
    .line 440
    iget-object v2, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 441
    :try_start_3
    iget-object v1, p0, Landroid/appwidget/AppWidgetHost;->mViews:Ljava/util/HashMap;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/appwidget/AppWidgetHostView;

    #@f
    .line 442
    .local v0, v:Landroid/appwidget/AppWidgetHostView;
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_16

    #@10
    .line 443
    if-eqz v0, :cond_15

    #@12
    .line 444
    invoke-virtual {v0, p2}, Landroid/appwidget/AppWidgetHostView;->viewDataChanged(I)V

    #@15
    .line 446
    :cond_15
    return-void

    #@16
    .line 442
    .end local v0           #v:Landroid/appwidget/AppWidgetHostView;
    :catchall_16
    move-exception v1

    #@17
    :try_start_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method
