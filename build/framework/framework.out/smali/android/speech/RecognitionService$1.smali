.class Landroid/speech/RecognitionService$1;
.super Landroid/os/Handler;
.source "RecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/RecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/speech/RecognitionService;


# direct methods
.method constructor <init>(Landroid/speech/RecognitionService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Landroid/speech/RecognitionService$1;->this$0:Landroid/speech/RecognitionService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 76
    iget v1, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v1, :pswitch_data_2e

    #@5
    .line 91
    :goto_5
    return-void

    #@6
    .line 78
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/speech/RecognitionService$StartListeningArgs;

    #@a
    .line 79
    .local v0, args:Landroid/speech/RecognitionService$StartListeningArgs;
    iget-object v1, p0, Landroid/speech/RecognitionService$1;->this$0:Landroid/speech/RecognitionService;

    #@c
    iget-object v2, v0, Landroid/speech/RecognitionService$StartListeningArgs;->mIntent:Landroid/content/Intent;

    #@e
    iget-object v3, v0, Landroid/speech/RecognitionService$StartListeningArgs;->mListener:Landroid/speech/IRecognitionListener;

    #@10
    invoke-static {v1, v2, v3}, Landroid/speech/RecognitionService;->access$000(Landroid/speech/RecognitionService;Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V

    #@13
    goto :goto_5

    #@14
    .line 82
    .end local v0           #args:Landroid/speech/RecognitionService$StartListeningArgs;
    :pswitch_14
    iget-object v2, p0, Landroid/speech/RecognitionService$1;->this$0:Landroid/speech/RecognitionService;

    #@16
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    check-cast v1, Landroid/speech/IRecognitionListener;

    #@1a
    invoke-static {v2, v1}, Landroid/speech/RecognitionService;->access$100(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;)V

    #@1d
    goto :goto_5

    #@1e
    .line 85
    :pswitch_1e
    iget-object v2, p0, Landroid/speech/RecognitionService$1;->this$0:Landroid/speech/RecognitionService;

    #@20
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    check-cast v1, Landroid/speech/IRecognitionListener;

    #@24
    invoke-static {v2, v1}, Landroid/speech/RecognitionService;->access$200(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;)V

    #@27
    goto :goto_5

    #@28
    .line 88
    :pswitch_28
    iget-object v1, p0, Landroid/speech/RecognitionService$1;->this$0:Landroid/speech/RecognitionService;

    #@2a
    invoke-static {v1}, Landroid/speech/RecognitionService;->access$300(Landroid/speech/RecognitionService;)V

    #@2d
    goto :goto_5

    #@2e
    .line 76
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_14
        :pswitch_1e
        :pswitch_28
    .end packed-switch
.end method
