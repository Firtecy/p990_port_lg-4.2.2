.class Landroid/speech/tts/PlaybackSynthesisCallback;
.super Landroid/speech/tts/AbstractSynthesisCallback;
.source "PlaybackSynthesisCallback.java"


# static fields
.field private static final DBG:Z = false

.field private static final MIN_AUDIO_BUFFER_SIZE:I = 0x2000

.field private static final TAG:Ljava/lang/String; = "PlaybackSynthesisRequest"


# instance fields
.field private final mAudioTrackHandler:Landroid/speech/tts/AudioPlaybackHandler;

.field private final mCallerIdentity:Ljava/lang/Object;

.field private final mDispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

.field private volatile mDone:Z

.field private mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

.field private final mLogger:Landroid/speech/tts/EventLogger;

.field private final mPan:F

.field private final mStateLock:Ljava/lang/Object;

.field private mStopped:Z

.field private final mStreamType:I

.field private final mVolume:F


# direct methods
.method constructor <init>(IFFLandroid/speech/tts/AudioPlaybackHandler;Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/speech/tts/EventLogger;)V
    .registers 10
    .parameter "streamType"
    .parameter "volume"
    .parameter "pan"
    .parameter "audioTrackHandler"
    .parameter "dispatcher"
    .parameter "callerIdentity"
    .parameter "logger"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    invoke-direct {p0}, Landroid/speech/tts/AbstractSynthesisCallback;-><init>()V

    #@4
    .line 52
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@b
    .line 57
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@e
    .line 61
    iput-boolean v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStopped:Z

    #@10
    .line 63
    iput-boolean v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDone:Z

    #@12
    .line 72
    iput p1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStreamType:I

    #@14
    .line 73
    iput p2, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mVolume:F

    #@16
    .line 74
    iput p3, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mPan:F

    #@18
    .line 75
    iput-object p4, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mAudioTrackHandler:Landroid/speech/tts/AudioPlaybackHandler;

    #@1a
    .line 76
    iput-object p5, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

    #@1c
    .line 77
    iput-object p6, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mCallerIdentity:Ljava/lang/Object;

    #@1e
    .line 78
    iput-object p7, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@20
    .line 79
    return-void
.end method


# virtual methods
.method public audioAvailable([BII)I
    .registers 11
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 171
    invoke-virtual {p0}, Landroid/speech/tts/PlaybackSynthesisCallback;->getMaxBufferSize()I

    #@5
    move-result v5

    #@6
    if-gt p3, v5, :cond_a

    #@8
    if-gtz p3, :cond_29

    #@a
    .line 172
    :cond_a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "buffer is too large or of zero length ("

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, " bytes)"

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v3

    #@29
    .line 176
    :cond_29
    const/4 v2, 0x0

    #@2a
    .line 177
    .local v2, item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    iget-object v5, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@2c
    monitor-enter v5

    #@2d
    .line 178
    :try_start_2d
    iget-object v6, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@2f
    if-eqz v6, :cond_35

    #@31
    iget-boolean v6, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStopped:Z

    #@33
    if-eqz v6, :cond_38

    #@35
    .line 179
    :cond_35
    monitor-exit v5

    #@36
    move v3, v4

    #@37
    .line 198
    :goto_37
    return v3

    #@38
    .line 181
    :cond_38
    iget-object v2, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@3a
    .line 182
    monitor-exit v5
    :try_end_3b
    .catchall {:try_start_2d .. :try_end_3b} :catchall_49

    #@3b
    .line 185
    new-array v0, p3, [B

    #@3d
    .line 186
    .local v0, bufferCopy:[B
    invoke-static {p1, p2, v0, v3, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@40
    .line 191
    :try_start_40
    invoke-virtual {v2, v0}, Landroid/speech/tts/SynthesisPlaybackQueueItem;->put([B)V
    :try_end_43
    .catch Ljava/lang/InterruptedException; {:try_start_40 .. :try_end_43} :catch_4c

    #@43
    .line 196
    iget-object v4, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@45
    invoke-virtual {v4}, Landroid/speech/tts/EventLogger;->onEngineDataReceived()V

    #@48
    goto :goto_37

    #@49
    .line 182
    .end local v0           #bufferCopy:[B
    :catchall_49
    move-exception v3

    #@4a
    :try_start_4a
    monitor-exit v5
    :try_end_4b
    .catchall {:try_start_4a .. :try_end_4b} :catchall_49

    #@4b
    throw v3

    #@4c
    .line 192
    .restart local v0       #bufferCopy:[B
    :catch_4c
    move-exception v1

    #@4d
    .local v1, ie:Ljava/lang/InterruptedException;
    move v3, v4

    #@4e
    .line 193
    goto :goto_37
.end method

.method public done()I
    .registers 6

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 205
    const/4 v0, 0x0

    #@2
    .line 206
    .local v0, item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    iget-object v2, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@4
    monitor-enter v2

    #@5
    .line 207
    :try_start_5
    iget-boolean v3, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDone:Z

    #@7
    if-eqz v3, :cond_12

    #@9
    .line 208
    const-string v3, "PlaybackSynthesisRequest"

    #@b
    const-string v4, "Duplicate call to done()"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 209
    monitor-exit v2

    #@11
    .line 224
    :goto_11
    return v1

    #@12
    .line 212
    :cond_12
    const/4 v3, 0x1

    #@13
    iput-boolean v3, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDone:Z

    #@15
    .line 214
    iget-object v3, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@17
    if-nez v3, :cond_1e

    #@19
    .line 215
    monitor-exit v2

    #@1a
    goto :goto_11

    #@1b
    .line 219
    :catchall_1b
    move-exception v1

    #@1c
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_5 .. :try_end_1d} :catchall_1b

    #@1d
    throw v1

    #@1e
    .line 218
    :cond_1e
    :try_start_1e
    iget-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@20
    .line 219
    monitor-exit v2
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_1b

    #@21
    .line 221
    invoke-virtual {v0}, Landroid/speech/tts/SynthesisPlaybackQueueItem;->done()V

    #@24
    .line 222
    iget-object v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@26
    invoke-virtual {v1}, Landroid/speech/tts/EventLogger;->onEngineComplete()V

    #@29
    .line 224
    const/4 v1, 0x0

    #@2a
    goto :goto_11
.end method

.method public error()V
    .registers 2

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@2
    invoke-virtual {v0}, Landroid/speech/tts/EventLogger;->onError()V

    #@5
    .line 233
    const/4 v0, 0x1

    #@6
    invoke-virtual {p0, v0}, Landroid/speech/tts/PlaybackSynthesisCallback;->stopImpl(Z)V

    #@9
    .line 234
    return-void
.end method

.method public getMaxBufferSize()I
    .registers 2

    #@0
    .prologue
    .line 128
    const/16 v0, 0x2000

    #@2
    return v0
.end method

.method isDone()Z
    .registers 2

    #@0
    .prologue
    .line 133
    iget-boolean v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDone:Z

    #@2
    return v0
.end method

.method public start(III)I
    .registers 16
    .parameter "sampleRateInHz"
    .parameter "audioFormat"
    .parameter "channelCount"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 143
    invoke-static {p3}, Landroid/speech/tts/BlockingAudioTrack;->getChannelConfig(I)I

    #@4
    move-result v10

    #@5
    .line 144
    .local v10, channelConfig:I
    if-nez v10, :cond_20

    #@7
    .line 145
    const-string v2, "PlaybackSynthesisRequest"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Unsupported number of channels :"

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 161
    :goto_1f
    return v1

    #@20
    .line 149
    :cond_20
    iget-object v11, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@22
    monitor-enter v11

    #@23
    .line 150
    :try_start_23
    iget-boolean v2, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStopped:Z

    #@25
    if-eqz v2, :cond_2c

    #@27
    .line 152
    monitor-exit v11

    #@28
    goto :goto_1f

    #@29
    .line 159
    :catchall_29
    move-exception v1

    #@2a
    monitor-exit v11
    :try_end_2b
    .catchall {:try_start_23 .. :try_end_2b} :catchall_29

    #@2b
    throw v1

    #@2c
    .line 154
    :cond_2c
    :try_start_2c
    new-instance v0, Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@2e
    iget v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStreamType:I

    #@30
    iget v5, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mVolume:F

    #@32
    iget v6, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mPan:F

    #@34
    iget-object v7, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

    #@36
    iget-object v8, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mCallerIdentity:Ljava/lang/Object;

    #@38
    iget-object v9, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@3a
    move v2, p1

    #@3b
    move v3, p2

    #@3c
    move v4, p3

    #@3d
    invoke-direct/range {v0 .. v9}, Landroid/speech/tts/SynthesisPlaybackQueueItem;-><init>(IIIIFFLandroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/speech/tts/EventLogger;)V

    #@40
    .line 157
    .local v0, item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    iget-object v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mAudioTrackHandler:Landroid/speech/tts/AudioPlaybackHandler;

    #@42
    invoke-virtual {v1, v0}, Landroid/speech/tts/AudioPlaybackHandler;->enqueue(Landroid/speech/tts/PlaybackQueueItem;)V

    #@45
    .line 158
    iput-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@47
    .line 159
    monitor-exit v11
    :try_end_48
    .catchall {:try_start_2c .. :try_end_48} :catchall_29

    #@48
    .line 161
    const/4 v1, 0x0

    #@49
    goto :goto_1f
.end method

.method stop()V
    .registers 2

    #@0
    .prologue
    .line 83
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/speech/tts/PlaybackSynthesisCallback;->stopImpl(Z)V

    #@4
    .line 84
    return-void
.end method

.method stopImpl(Z)V
    .registers 6
    .parameter "wasError"

    #@0
    .prologue
    .line 90
    iget-object v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@2
    invoke-virtual {v1}, Landroid/speech/tts/EventLogger;->onStopped()V

    #@5
    .line 93
    iget-object v2, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@7
    monitor-enter v2

    #@8
    .line 94
    :try_start_8
    iget-boolean v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStopped:Z

    #@a
    if-eqz v1, :cond_16

    #@c
    .line 95
    const-string v1, "PlaybackSynthesisRequest"

    #@e
    const-string/jumbo v3, "stop() called twice"

    #@11
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 96
    monitor-exit v2

    #@15
    .line 122
    :cond_15
    :goto_15
    return-void

    #@16
    .line 99
    :cond_16
    iget-object v0, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mItem:Landroid/speech/tts/SynthesisPlaybackQueueItem;

    #@18
    .line 100
    .local v0, item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    const/4 v1, 0x1

    #@19
    iput-boolean v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mStopped:Z

    #@1b
    .line 101
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_8 .. :try_end_1c} :catchall_22

    #@1c
    .line 103
    if-eqz v0, :cond_25

    #@1e
    .line 108
    invoke-virtual {v0, p1}, Landroid/speech/tts/SynthesisPlaybackQueueItem;->stop(Z)V

    #@21
    goto :goto_15

    #@22
    .line 101
    .end local v0           #item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    :catchall_22
    move-exception v1

    #@23
    :try_start_23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_23 .. :try_end_24} :catchall_22

    #@24
    throw v1

    #@25
    .line 115
    .restart local v0       #item:Landroid/speech/tts/SynthesisPlaybackQueueItem;
    :cond_25
    iget-object v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mLogger:Landroid/speech/tts/EventLogger;

    #@27
    invoke-virtual {v1}, Landroid/speech/tts/EventLogger;->onWriteData()V

    #@2a
    .line 117
    if-eqz p1, :cond_15

    #@2c
    .line 119
    iget-object v1, p0, Landroid/speech/tts/PlaybackSynthesisCallback;->mDispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

    #@2e
    invoke-interface {v1}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnError()V

    #@31
    goto :goto_15
.end method
