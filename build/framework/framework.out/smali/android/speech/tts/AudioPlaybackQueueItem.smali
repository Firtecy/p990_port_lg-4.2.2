.class Landroid/speech/tts/AudioPlaybackQueueItem;
.super Landroid/speech/tts/PlaybackQueueItem;
.source "AudioPlaybackQueueItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TTS.AudioQueueItem"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDone:Landroid/os/ConditionVariable;

.field private volatile mFinished:Z

.field private mPlayer:Landroid/media/MediaPlayer;

.field private final mStreamType:I

.field private final mUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/content/Context;Landroid/net/Uri;I)V
    .registers 7
    .parameter "dispatcher"
    .parameter "callerIdentity"
    .parameter "context"
    .parameter "uri"
    .parameter "streamType"

    #@0
    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/speech/tts/PlaybackQueueItem;-><init>(Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;)V

    #@3
    .line 41
    iput-object p3, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mContext:Landroid/content/Context;

    #@5
    .line 42
    iput-object p4, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mUri:Landroid/net/Uri;

    #@7
    .line 43
    iput p5, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mStreamType:I

    #@9
    .line 45
    new-instance v0, Landroid/os/ConditionVariable;

    #@b
    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    #@e
    iput-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mDone:Landroid/os/ConditionVariable;

    #@10
    .line 46
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@13
    .line 47
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mFinished:Z

    #@16
    .line 48
    return-void
.end method

.method static synthetic access$000(Landroid/speech/tts/AudioPlaybackQueueItem;)Landroid/os/ConditionVariable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mDone:Landroid/os/ConditionVariable;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/speech/tts/AudioPlaybackQueueItem;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 25
    iput-boolean p1, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mFinished:Z

    #@2
    return p1
.end method

.method private finish()V
    .registers 2

    #@0
    .prologue
    .line 94
    :try_start_0
    iget-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@2
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_b

    #@5
    .line 98
    :goto_5
    iget-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@7
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    #@a
    .line 99
    return-void

    #@b
    .line 95
    :catch_b
    move-exception v0

    #@c
    goto :goto_5
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 51
    invoke-virtual {p0}, Landroid/speech/tts/AudioPlaybackQueueItem;->getDispatcher()Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

    #@3
    move-result-object v0

    #@4
    .line 53
    .local v0, dispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;
    invoke-interface {v0}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnStart()V

    #@7
    .line 54
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mContext:Landroid/content/Context;

    #@9
    iget-object v3, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mUri:Landroid/net/Uri;

    #@b
    invoke-static {v2, v3}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    #@e
    move-result-object v2

    #@f
    iput-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@11
    .line 55
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@13
    if-nez v2, :cond_19

    #@15
    .line 56
    invoke-interface {v0}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnError()V

    #@18
    .line 90
    :goto_18
    return-void

    #@19
    .line 61
    :cond_19
    :try_start_19
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@1b
    new-instance v3, Landroid/speech/tts/AudioPlaybackQueueItem$1;

    #@1d
    invoke-direct {v3, p0}, Landroid/speech/tts/AudioPlaybackQueueItem$1;-><init>(Landroid/speech/tts/AudioPlaybackQueueItem;)V

    #@20
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    #@23
    .line 69
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@25
    new-instance v3, Landroid/speech/tts/AudioPlaybackQueueItem$2;

    #@27
    invoke-direct {v3, p0}, Landroid/speech/tts/AudioPlaybackQueueItem$2;-><init>(Landroid/speech/tts/AudioPlaybackQueueItem;)V

    #@2a
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    #@2d
    .line 76
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@2f
    iget v3, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mStreamType:I

    #@31
    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    #@34
    .line 77
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mPlayer:Landroid/media/MediaPlayer;

    #@36
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    #@39
    .line 78
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mDone:Landroid/os/ConditionVariable;

    #@3b
    invoke-virtual {v2}, Landroid/os/ConditionVariable;->block()V

    #@3e
    .line 79
    invoke-direct {p0}, Landroid/speech/tts/AudioPlaybackQueueItem;->finish()V
    :try_end_41
    .catch Ljava/lang/IllegalArgumentException; {:try_start_19 .. :try_end_41} :catch_49

    #@41
    .line 85
    :goto_41
    iget-boolean v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mFinished:Z

    #@43
    if-eqz v2, :cond_57

    #@45
    .line 86
    invoke-interface {v0}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnDone()V

    #@48
    goto :goto_18

    #@49
    .line 80
    :catch_49
    move-exception v1

    #@4a
    .line 81
    .local v1, ex:Ljava/lang/IllegalArgumentException;
    const-string v2, "TTS.AudioQueueItem"

    #@4c
    const-string v3, "MediaPlayer failed"

    #@4e
    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    .line 82
    iget-object v2, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mDone:Landroid/os/ConditionVariable;

    #@53
    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    #@56
    goto :goto_41

    #@57
    .line 88
    .end local v1           #ex:Ljava/lang/IllegalArgumentException;
    :cond_57
    invoke-interface {v0}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnError()V

    #@5a
    goto :goto_18
.end method

.method stop(Z)V
    .registers 3
    .parameter "isError"

    #@0
    .prologue
    .line 103
    iget-object v0, p0, Landroid/speech/tts/AudioPlaybackQueueItem;->mDone:Landroid/os/ConditionVariable;

    #@2
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    #@5
    .line 104
    return-void
.end method
