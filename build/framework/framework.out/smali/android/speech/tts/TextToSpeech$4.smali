.class Landroid/speech/tts/TextToSpeech$4;
.super Ljava/lang/Object;
.source "TextToSpeech.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/speech/tts/TextToSpeech;->playSilence(JILjava/util/HashMap;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/speech/tts/TextToSpeech$Action",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/speech/tts/TextToSpeech;

.field final synthetic val$durationInMs:J

.field final synthetic val$params:Ljava/util/HashMap;

.field final synthetic val$queueMode:I


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeech;JILjava/util/HashMap;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    #@0
    .prologue
    .line 880
    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$4;->this$0:Landroid/speech/tts/TextToSpeech;

    #@2
    iput-wide p2, p0, Landroid/speech/tts/TextToSpeech$4;->val$durationInMs:J

    #@4
    iput p4, p0, Landroid/speech/tts/TextToSpeech$4;->val$queueMode:I

    #@6
    iput-object p5, p0, Landroid/speech/tts/TextToSpeech$4;->val$params:Ljava/util/HashMap;

    #@8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@b
    return-void
.end method


# virtual methods
.method public run(Landroid/speech/tts/ITextToSpeechService;)Ljava/lang/Integer;
    .registers 8
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 883
    iget-object v0, p0, Landroid/speech/tts/TextToSpeech$4;->this$0:Landroid/speech/tts/TextToSpeech;

    #@2
    invoke-static {v0}, Landroid/speech/tts/TextToSpeech;->access$100(Landroid/speech/tts/TextToSpeech;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    iget-wide v2, p0, Landroid/speech/tts/TextToSpeech$4;->val$durationInMs:J

    #@8
    iget v4, p0, Landroid/speech/tts/TextToSpeech$4;->val$queueMode:I

    #@a
    iget-object v0, p0, Landroid/speech/tts/TextToSpeech$4;->this$0:Landroid/speech/tts/TextToSpeech;

    #@c
    iget-object v5, p0, Landroid/speech/tts/TextToSpeech$4;->val$params:Ljava/util/HashMap;

    #@e
    invoke-static {v0, v5}, Landroid/speech/tts/TextToSpeech;->access$500(Landroid/speech/tts/TextToSpeech;Ljava/util/HashMap;)Landroid/os/Bundle;

    #@11
    move-result-object v5

    #@12
    move-object v0, p1

    #@13
    invoke-interface/range {v0 .. v5}, Landroid/speech/tts/ITextToSpeechService;->playSilence(Landroid/os/IBinder;JILandroid/os/Bundle;)I

    #@16
    move-result v0

    #@17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v0

    #@1b
    return-object v0
.end method

.method public bridge synthetic run(Landroid/speech/tts/ITextToSpeechService;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 880
    invoke-virtual {p0, p1}, Landroid/speech/tts/TextToSpeech$4;->run(Landroid/speech/tts/ITextToSpeechService;)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
