.class Landroid/speech/tts/TtsEngines$EngineInfoComparator;
.super Ljava/lang/Object;
.source "TtsEngines.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TtsEngines;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EngineInfoComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/speech/tts/TextToSpeech$EngineInfo;",
        ">;"
    }
.end annotation


# static fields
.field static INSTANCE:Landroid/speech/tts/TtsEngines$EngineInfoComparator;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 259
    new-instance v0, Landroid/speech/tts/TtsEngines$EngineInfoComparator;

    #@2
    invoke-direct {v0}, Landroid/speech/tts/TtsEngines$EngineInfoComparator;-><init>()V

    #@5
    sput-object v0, Landroid/speech/tts/TtsEngines$EngineInfoComparator;->INSTANCE:Landroid/speech/tts/TtsEngines$EngineInfoComparator;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 257
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/speech/tts/TextToSpeech$EngineInfo;Landroid/speech/tts/TextToSpeech$EngineInfo;)I
    .registers 5
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    .line 268
    iget-boolean v0, p1, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-boolean v0, p2, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@6
    if-nez v0, :cond_a

    #@8
    .line 269
    const/4 v0, -0x1

    #@9
    .line 278
    :goto_9
    return v0

    #@a
    .line 270
    :cond_a
    iget-boolean v0, p2, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@c
    if-eqz v0, :cond_14

    #@e
    iget-boolean v0, p1, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@10
    if-nez v0, :cond_14

    #@12
    .line 271
    const/4 v0, 0x1

    #@13
    goto :goto_9

    #@14
    .line 278
    :cond_14
    iget v0, p2, Landroid/speech/tts/TextToSpeech$EngineInfo;->priority:I

    #@16
    iget v1, p1, Landroid/speech/tts/TextToSpeech$EngineInfo;->priority:I

    #@18
    sub-int/2addr v0, v1

    #@19
    goto :goto_9
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 256
    check-cast p1, Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@2
    .end local p1
    check-cast p2, Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Landroid/speech/tts/TtsEngines$EngineInfoComparator;->compare(Landroid/speech/tts/TextToSpeech$EngineInfo;Landroid/speech/tts/TextToSpeech$EngineInfo;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
