.class Landroid/speech/tts/TextToSpeechService$SynthHandler;
.super Landroid/os/Handler;
.source "TextToSpeechService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SynthHandler"
.end annotation


# instance fields
.field private mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method public constructor <init>(Landroid/speech/tts/TextToSpeechService;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 262
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    .line 263
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 260
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@8
    .line 264
    return-void
.end method

.method static synthetic access$100(Landroid/speech/tts/TextToSpeechService$SynthHandler;Landroid/speech/tts/TextToSpeechService$SpeechItem;)Landroid/speech/tts/TextToSpeechService$SpeechItem;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 258
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->setCurrentSpeechItem(Landroid/speech/tts/TextToSpeechService$SpeechItem;)Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private declared-synchronized getCurrentSpeechItem()Landroid/speech/tts/TextToSpeechService$SpeechItem;
    .registers 2

    #@0
    .prologue
    .line 267
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return-object v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method private declared-synchronized maybeRemoveCurrentSpeechItem(Ljava/lang/Object;)Landroid/speech/tts/TextToSpeechService$SpeechItem;
    .registers 4
    .parameter "callerIdentity"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 277
    monitor-enter p0

    #@2
    :try_start_2
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@4
    if-eqz v1, :cond_13

    #@6
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@8
    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    if-ne v1, p1, :cond_13

    #@e
    .line 279
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@10
    .line 280
    .local v0, current:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    const/4 v1, 0x0

    #@11
    iput-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    :try_end_13
    .catchall {:try_start_2 .. :try_end_13} :catchall_15

    #@13
    .line 284
    .end local v0           #current:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    :cond_13
    monitor-exit p0

    #@14
    return-object v0

    #@15
    .line 277
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0

    #@17
    throw v1
.end method

.method private declared-synchronized setCurrentSpeechItem(Landroid/speech/tts/TextToSpeechService$SpeechItem;)Landroid/speech/tts/TextToSpeechService$SpeechItem;
    .registers 4
    .parameter "speechItem"

    #@0
    .prologue
    .line 271
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@3
    .line 272
    .local v0, old:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->mCurrentSpeechItem:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_7

    #@5
    .line 273
    monitor-exit p0

    #@6
    return-object v0

    #@7
    .line 271
    .end local v0           #old:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    :catchall_7
    move-exception v1

    #@8
    monitor-exit p0

    #@9
    throw v1
.end method


# virtual methods
.method public enqueueSpeechItem(ILandroid/speech/tts/TextToSpeechService$SpeechItem;)I
    .registers 8
    .parameter "queueMode"
    .parameter "speechItem"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 309
    invoke-virtual {p2}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->isValid()Z

    #@4
    move-result v3

    #@5
    if-nez v3, :cond_b

    #@7
    .line 310
    invoke-virtual {p2}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->dispatchOnError()V

    #@a
    .line 338
    :goto_a
    return v2

    #@b
    .line 314
    :cond_b
    if-nez p1, :cond_2b

    #@d
    .line 315
    invoke-virtual {p2}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {p0, v3}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->stopForApp(Ljava/lang/Object;)I

    #@14
    .line 319
    :cond_14
    :goto_14
    new-instance v1, Landroid/speech/tts/TextToSpeechService$SynthHandler$1;

    #@16
    invoke-direct {v1, p0, p2}, Landroid/speech/tts/TextToSpeechService$SynthHandler$1;-><init>(Landroid/speech/tts/TextToSpeechService$SynthHandler;Landroid/speech/tts/TextToSpeechService$SpeechItem;)V

    #@19
    .line 327
    .local v1, runnable:Ljava/lang/Runnable;
    invoke-static {p0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    #@1c
    move-result-object v0

    #@1d
    .line 332
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p2}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@20
    move-result-object v3

    #@21
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@23
    .line 333
    invoke-virtual {p0, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->sendMessage(Landroid/os/Message;)Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_32

    #@29
    .line 334
    const/4 v2, 0x0

    #@2a
    goto :goto_a

    #@2b
    .line 316
    .end local v0           #msg:Landroid/os/Message;
    .end local v1           #runnable:Ljava/lang/Runnable;
    :cond_2b
    const/4 v3, 0x2

    #@2c
    if-ne p1, v3, :cond_14

    #@2e
    .line 317
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->stopAll()I

    #@31
    goto :goto_14

    #@32
    .line 336
    .restart local v0       #msg:Landroid/os/Message;
    .restart local v1       #runnable:Ljava/lang/Runnable;
    :cond_32
    const-string v3, "TextToSpeechService"

    #@34
    const-string v4, "SynthThread has quit"

    #@36
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 337
    invoke-virtual {p2}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->dispatchOnError()V

    #@3c
    goto :goto_a
.end method

.method public isSpeaking()Z
    .registers 2

    #@0
    .prologue
    .line 288
    invoke-direct {p0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->getCurrentSpeechItem()Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public quit()V
    .registers 3

    #@0
    .prologue
    .line 293
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->getLooper()Landroid/os/Looper;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    #@7
    .line 295
    const/4 v1, 0x0

    #@8
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->setCurrentSpeechItem(Landroid/speech/tts/TextToSpeechService$SpeechItem;)Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@b
    move-result-object v0

    #@c
    .line 296
    .local v0, current:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    if-eqz v0, :cond_11

    #@e
    .line 297
    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->stop()V

    #@11
    .line 301
    :cond_11
    return-void
.end method

.method public stopAll()I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 374
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->setCurrentSpeechItem(Landroid/speech/tts/TextToSpeechService$SpeechItem;)Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@4
    move-result-object v0

    #@5
    .line 375
    .local v0, current:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    if-eqz v0, :cond_a

    #@7
    .line 376
    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->stop()V

    #@a
    .line 379
    :cond_a
    invoke-virtual {p0, v1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@d
    .line 381
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@f
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$200(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/AudioPlaybackHandler;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Landroid/speech/tts/AudioPlaybackHandler;->stop()V

    #@16
    .line 383
    const/4 v1, 0x0

    #@17
    return v1
.end method

.method public stopForApp(Ljava/lang/Object;)I
    .registers 4
    .parameter "callerIdentity"

    #@0
    .prologue
    .line 349
    if-nez p1, :cond_4

    #@2
    .line 350
    const/4 v1, -0x1

    #@3
    .line 369
    :goto_3
    return v1

    #@4
    .line 353
    :cond_4
    invoke-virtual {p0, p1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@7
    .line 361
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->maybeRemoveCurrentSpeechItem(Ljava/lang/Object;)Landroid/speech/tts/TextToSpeechService$SpeechItem;

    #@a
    move-result-object v0

    #@b
    .line 362
    .local v0, current:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    if-eqz v0, :cond_10

    #@d
    .line 363
    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->stop()V

    #@10
    .line 367
    :cond_10
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthHandler;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@12
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$200(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/AudioPlaybackHandler;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1, p1}, Landroid/speech/tts/AudioPlaybackHandler;->stopForApp(Ljava/lang/Object;)V

    #@19
    .line 369
    const/4 v1, 0x0

    #@1a
    goto :goto_3
.end method
