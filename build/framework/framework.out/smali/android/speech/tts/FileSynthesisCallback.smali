.class Landroid/speech/tts/FileSynthesisCallback;
.super Landroid/speech/tts/AbstractSynthesisCallback;
.source "FileSynthesisCallback.java"


# static fields
.field private static final DBG:Z = false

.field private static final MAX_AUDIO_BUFFER_SIZE:I = 0x2000

.field private static final TAG:Ljava/lang/String; = "FileSynthesisRequest"

.field private static final WAV_FORMAT_PCM:S = 0x1s

.field private static final WAV_HEADER_LENGTH:I = 0x2c


# instance fields
.field private mAudioFormat:I

.field private mChannelCount:I

.field private mDone:Z

.field private mFile:Ljava/io/RandomAccessFile;

.field private final mFileName:Ljava/io/File;

.field private mSampleRateInHz:I

.field private final mStateLock:Ljava/lang/Object;

.field private mStopped:Z


# direct methods
.method constructor <init>(Ljava/io/File;)V
    .registers 4
    .parameter "fileName"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 50
    invoke-direct {p0}, Landroid/speech/tts/AbstractSynthesisCallback;-><init>()V

    #@4
    .line 41
    new-instance v0, Ljava/lang/Object;

    #@6
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@9
    iput-object v0, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@b
    .line 47
    iput-boolean v1, p0, Landroid/speech/tts/FileSynthesisCallback;->mStopped:Z

    #@d
    .line 48
    iput-boolean v1, p0, Landroid/speech/tts/FileSynthesisCallback;->mDone:Z

    #@f
    .line 51
    iput-object p1, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@11
    .line 52
    return-void
.end method

.method private cleanUp()V
    .registers 2

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->closeFileAndWidenPermissions()V

    #@3
    .line 67
    iget-object v0, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 68
    iget-object v0, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@9
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    #@c
    .line 70
    :cond_c
    return-void
.end method

.method private closeFileAndWidenPermissions()V
    .registers 7

    #@0
    .prologue
    .line 77
    :try_start_0
    iget-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@2
    if-eqz v2, :cond_c

    #@4
    .line 78
    iget-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V

    #@9
    .line 79
    const/4 v2, 0x0

    #@a
    iput-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_c} :catch_1a

    #@c
    .line 92
    :cond_c
    :goto_c
    :try_start_c
    iget-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@e
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    const/16 v3, 0x1b6

    #@14
    const/4 v4, -0x1

    #@15
    const/4 v5, -0x1

    #@16
    invoke-static {v2, v3, v4, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_19
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_19} :catch_40

    #@19
    .line 96
    :goto_19
    return-void

    #@1a
    .line 81
    :catch_1a
    move-exception v0

    #@1b
    .line 82
    .local v0, ex:Ljava/io/IOException;
    const-string v2, "FileSynthesisRequest"

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Failed to close "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, ": "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_c

    #@40
    .line 93
    .end local v0           #ex:Ljava/io/IOException;
    :catch_40
    move-exception v1

    #@41
    .line 94
    .local v1, se:Ljava/lang/SecurityException;
    const-string v2, "FileSynthesisRequest"

    #@43
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "Security exception setting rw permissions on : "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_19
.end method

.method private makeWavHeader(IIII)[B
    .registers 14
    .parameter "sampleRateInHz"
    .parameter "audioFormat"
    .parameter "channelCount"
    .parameter "dataLength"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v8, 0x4

    #@2
    .line 232
    const/4 v7, 0x3

    #@3
    if-ne p2, v7, :cond_60

    #@5
    move v5, v6

    #@6
    .line 233
    .local v5, sampleSizeInBytes:I
    :goto_6
    mul-int v7, p1, v5

    #@8
    mul-int v2, v7, p3

    #@a
    .line 234
    .local v2, byteRate:I
    mul-int v7, v5, p3

    #@c
    int-to-short v1, v7

    #@d
    .line 235
    .local v1, blockAlign:S
    mul-int/lit8 v7, v5, 0x8

    #@f
    int-to-short v0, v7

    #@10
    .line 237
    .local v0, bitsPerSample:S
    const/16 v7, 0x2c

    #@12
    new-array v4, v7, [B

    #@14
    .line 238
    .local v4, headerBuf:[B
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@17
    move-result-object v3

    #@18
    .line 239
    .local v3, header:Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    #@1a
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@1d
    .line 241
    new-array v7, v8, [B

    #@1f
    fill-array-data v7, :array_62

    #@22
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@25
    .line 242
    add-int/lit8 v7, p4, 0x2c

    #@27
    add-int/lit8 v7, v7, -0x8

    #@29
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@2c
    .line 243
    new-array v7, v8, [B

    #@2e
    fill-array-data v7, :array_68

    #@31
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@34
    .line 244
    new-array v7, v8, [B

    #@36
    fill-array-data v7, :array_6e

    #@39
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@3c
    .line 245
    const/16 v7, 0x10

    #@3e
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@41
    .line 246
    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@44
    .line 247
    int-to-short v6, p3

    #@45
    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@48
    .line 248
    invoke-virtual {v3, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@4b
    .line 249
    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@4e
    .line 250
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@51
    .line 251
    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    #@54
    .line 252
    new-array v6, v8, [B

    #@56
    fill-array-data v6, :array_74

    #@59
    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    #@5c
    .line 253
    invoke-virtual {v3, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@5f
    .line 255
    return-object v4

    #@60
    .line 232
    .end local v0           #bitsPerSample:S
    .end local v1           #blockAlign:S
    .end local v2           #byteRate:I
    .end local v3           #header:Ljava/nio/ByteBuffer;
    .end local v4           #headerBuf:[B
    .end local v5           #sampleSizeInBytes:I
    :cond_60
    const/4 v5, 0x2

    #@61
    goto :goto_6

    #@62
    .line 241
    :array_62
    .array-data 0x1
        0x52t
        0x49t
        0x46t
        0x46t
    .end array-data

    #@68
    .line 243
    :array_68
    .array-data 0x1
        0x57t
        0x41t
        0x56t
        0x45t
    .end array-data

    #@6e
    .line 244
    :array_6e
    .array-data 0x1
        0x66t
        0x6dt
        0x74t
        0x20t
    .end array-data

    #@74
    .line 252
    :array_74
    .array-data 0x1
        0x64t
        0x61t
        0x74t
        0x61t
    .end array-data
.end method

.method private maybeCleanupExistingFile(Ljava/io/File;)Z
    .registers 5
    .parameter "file"

    #@0
    .prologue
    .line 102
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_44

    #@6
    .line 103
    const-string v0, "FileSynthesisRequest"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "File "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, " exists, deleting."

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 104
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    #@27
    move-result v0

    #@28
    if-nez v0, :cond_44

    #@2a
    .line 105
    const-string v0, "FileSynthesisRequest"

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "Failed to delete "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 106
    const/4 v0, 0x0

    #@43
    .line 110
    :goto_43
    return v0

    #@44
    :cond_44
    const/4 v0, 0x1

    #@45
    goto :goto_43
.end method


# virtual methods
.method public audioAvailable([BII)I
    .registers 10
    .parameter "buffer"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 166
    iget-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 167
    :try_start_4
    iget-boolean v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mStopped:Z

    #@6
    if-eqz v3, :cond_a

    #@8
    .line 169
    monitor-exit v2

    #@9
    .line 181
    :goto_9
    return v1

    #@a
    .line 171
    :cond_a
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@c
    if-nez v3, :cond_1a

    #@e
    .line 172
    const-string v3, "FileSynthesisRequest"

    #@10
    const-string v4, "File not open"

    #@12
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 173
    monitor-exit v2

    #@16
    goto :goto_9

    #@17
    .line 183
    :catchall_17
    move-exception v1

    #@18
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_4 .. :try_end_19} :catchall_17

    #@19
    throw v1

    #@1a
    .line 176
    :cond_1a
    :try_start_1a
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@1c
    invoke-virtual {v3, p1, p2, p3}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_17
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1f} :catch_22

    #@1f
    .line 177
    const/4 v1, 0x0

    #@20
    :try_start_20
    monitor-exit v2

    #@21
    goto :goto_9

    #@22
    .line 178
    :catch_22
    move-exception v0

    #@23
    .line 179
    .local v0, ex:Ljava/io/IOException;
    const-string v3, "FileSynthesisRequest"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "Failed to write to "

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    iget-object v5, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    const-string v5, ": "

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 180
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@4a
    .line 181
    monitor-exit v2
    :try_end_4b
    .catchall {:try_start_20 .. :try_end_4b} :catchall_17

    #@4b
    goto :goto_9
.end method

.method public done()I
    .registers 9

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 189
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 190
    :try_start_4
    iget-boolean v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mDone:Z

    #@6
    if-eqz v4, :cond_a

    #@8
    .line 194
    monitor-exit v3

    #@9
    .line 216
    :goto_9
    return v2

    #@a
    .line 196
    :cond_a
    iget-boolean v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mStopped:Z

    #@c
    if-eqz v4, :cond_13

    #@e
    .line 198
    monitor-exit v3

    #@f
    goto :goto_9

    #@10
    .line 218
    :catchall_10
    move-exception v2

    #@11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_4 .. :try_end_12} :catchall_10

    #@12
    throw v2

    #@13
    .line 200
    :cond_13
    :try_start_13
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@15
    if-nez v4, :cond_20

    #@17
    .line 201
    const-string v4, "FileSynthesisRequest"

    #@19
    const-string v5, "File not open"

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 202
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_13 .. :try_end_1f} :catchall_10

    #@1f
    goto :goto_9

    #@20
    .line 206
    :cond_20
    :try_start_20
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@22
    const-wide/16 v5, 0x0

    #@24
    invoke-virtual {v4, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V

    #@27
    .line 207
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@29
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->length()J

    #@2c
    move-result-wide v4

    #@2d
    const-wide/16 v6, 0x2c

    #@2f
    sub-long/2addr v4, v6

    #@30
    long-to-int v0, v4

    #@31
    .line 208
    .local v0, dataLength:I
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@33
    iget v5, p0, Landroid/speech/tts/FileSynthesisCallback;->mSampleRateInHz:I

    #@35
    iget v6, p0, Landroid/speech/tts/FileSynthesisCallback;->mAudioFormat:I

    #@37
    iget v7, p0, Landroid/speech/tts/FileSynthesisCallback;->mChannelCount:I

    #@39
    invoke-direct {p0, v5, v6, v7, v0}, Landroid/speech/tts/FileSynthesisCallback;->makeWavHeader(IIII)[B

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v4, v5}, Ljava/io/RandomAccessFile;->write([B)V

    #@40
    .line 210
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->closeFileAndWidenPermissions()V

    #@43
    .line 211
    const/4 v4, 0x1

    #@44
    iput-boolean v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mDone:Z
    :try_end_46
    .catchall {:try_start_20 .. :try_end_46} :catchall_10
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_46} :catch_49

    #@46
    .line 212
    const/4 v2, 0x0

    #@47
    :try_start_47
    monitor-exit v3

    #@48
    goto :goto_9

    #@49
    .line 213
    .end local v0           #dataLength:I
    :catch_49
    move-exception v1

    #@4a
    .line 214
    .local v1, ex:Ljava/io/IOException;
    const-string v4, "FileSynthesisRequest"

    #@4c
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "Failed to write to "

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    iget-object v6, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@59
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    const-string v6, ": "

    #@5f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v5

    #@63
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v5

    #@6b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 215
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@71
    .line 216
    monitor-exit v3
    :try_end_72
    .catchall {:try_start_47 .. :try_end_72} :catchall_10

    #@72
    goto :goto_9
.end method

.method public error()V
    .registers 3

    #@0
    .prologue
    .line 224
    iget-object v1, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 225
    :try_start_3
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@6
    .line 226
    monitor-exit v1

    #@7
    .line 227
    return-void

    #@8
    .line 226
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method public getMaxBufferSize()I
    .registers 2

    #@0
    .prologue
    .line 116
    const/16 v0, 0x2000

    #@2
    return v0
.end method

.method isDone()Z
    .registers 2

    #@0
    .prologue
    .line 121
    iget-boolean v0, p0, Landroid/speech/tts/FileSynthesisCallback;->mDone:Z

    #@2
    return v0
.end method

.method public start(III)I
    .registers 10
    .parameter "sampleRateInHz"
    .parameter "audioFormat"
    .parameter "channelCount"

    #@0
    .prologue
    const/4 v1, -0x1

    #@1
    .line 130
    iget-object v2, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 131
    :try_start_4
    iget-boolean v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mStopped:Z

    #@6
    if-eqz v3, :cond_a

    #@8
    .line 133
    monitor-exit v2

    #@9
    .line 155
    :goto_9
    return v1

    #@a
    .line 135
    :cond_a
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@c
    if-eqz v3, :cond_1c

    #@e
    .line 136
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@11
    .line 137
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@13
    const-string v3, "FileSynthesisRequest.start() called twice"

    #@15
    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v1

    #@19
    .line 157
    :catchall_19
    move-exception v1

    #@1a
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_19

    #@1b
    throw v1

    #@1c
    .line 140
    :cond_1c
    :try_start_1c
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@1e
    invoke-direct {p0, v3}, Landroid/speech/tts/FileSynthesisCallback;->maybeCleanupExistingFile(Ljava/io/File;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_26

    #@24
    .line 141
    monitor-exit v2

    #@25
    goto :goto_9

    #@26
    .line 144
    :cond_26
    iput p1, p0, Landroid/speech/tts/FileSynthesisCallback;->mSampleRateInHz:I

    #@28
    .line 145
    iput p2, p0, Landroid/speech/tts/FileSynthesisCallback;->mAudioFormat:I

    #@2a
    .line 146
    iput p3, p0, Landroid/speech/tts/FileSynthesisCallback;->mChannelCount:I
    :try_end_2c
    .catchall {:try_start_1c .. :try_end_2c} :catchall_19

    #@2c
    .line 148
    :try_start_2c
    new-instance v3, Ljava/io/RandomAccessFile;

    #@2e
    iget-object v4, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@30
    const-string/jumbo v5, "rw"

    #@33
    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@36
    iput-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@38
    .line 150
    iget-object v3, p0, Landroid/speech/tts/FileSynthesisCallback;->mFile:Ljava/io/RandomAccessFile;

    #@3a
    const/16 v4, 0x2c

    #@3c
    new-array v4, v4, [B

    #@3e
    invoke-virtual {v3, v4}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_41
    .catchall {:try_start_2c .. :try_end_41} :catchall_19
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_41} :catch_44

    #@41
    .line 151
    const/4 v1, 0x0

    #@42
    :try_start_42
    monitor-exit v2

    #@43
    goto :goto_9

    #@44
    .line 152
    :catch_44
    move-exception v0

    #@45
    .line 153
    .local v0, ex:Ljava/io/IOException;
    const-string v3, "FileSynthesisRequest"

    #@47
    new-instance v4, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v5, "Failed to open "

    #@4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v4

    #@52
    iget-object v5, p0, Landroid/speech/tts/FileSynthesisCallback;->mFileName:Ljava/io/File;

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    const-string v5, ": "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v4

    #@62
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v4

    #@66
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 154
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@6c
    .line 155
    monitor-exit v2
    :try_end_6d
    .catchall {:try_start_42 .. :try_end_6d} :catchall_19

    #@6d
    goto :goto_9
.end method

.method stop()V
    .registers 3

    #@0
    .prologue
    .line 56
    iget-object v1, p0, Landroid/speech/tts/FileSynthesisCallback;->mStateLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 57
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Landroid/speech/tts/FileSynthesisCallback;->mStopped:Z

    #@6
    .line 58
    invoke-direct {p0}, Landroid/speech/tts/FileSynthesisCallback;->cleanUp()V

    #@9
    .line 59
    monitor-exit v1

    #@a
    .line 60
    return-void

    #@b
    .line 59
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method
