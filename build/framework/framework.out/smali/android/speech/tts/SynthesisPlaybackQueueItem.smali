.class final Landroid/speech/tts/SynthesisPlaybackQueueItem;
.super Landroid/speech/tts/PlaybackQueueItem;
.source "SynthesisPlaybackQueueItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final MAX_UNCONSUMED_AUDIO_MS:J = 0x1f4L

.field private static final TAG:Ljava/lang/String; = "TTS.SynthQueueItem"


# instance fields
.field private final mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

.field private final mDataBufferList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mDone:Z

.field private volatile mIsError:Z

.field private final mListLock:Ljava/util/concurrent/locks/Lock;

.field private final mLogger:Landroid/speech/tts/EventLogger;

.field private final mNotFull:Ljava/util/concurrent/locks/Condition;

.field private final mReadReady:Ljava/util/concurrent/locks/Condition;

.field private volatile mStopped:Z

.field private mUnconsumedBytes:I


# direct methods
.method constructor <init>(IIIIFFLandroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;Landroid/speech/tts/EventLogger;)V
    .registers 18
    .parameter "streamType"
    .parameter "sampleRate"
    .parameter "audioFormat"
    .parameter "channelCount"
    .parameter "volume"
    .parameter "pan"
    .parameter "dispatcher"
    .parameter "callerIdentity"
    .parameter "logger"

    #@0
    .prologue
    .line 70
    move-object/from16 v0, p8

    #@2
    invoke-direct {p0, p7, v0}, Landroid/speech/tts/PlaybackQueueItem;-><init>(Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;Ljava/lang/Object;)V

    #@5
    .line 44
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    #@7
    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    #@a
    iput-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@c
    .line 45
    iget-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@e
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mReadReady:Ljava/util/concurrent/locks/Condition;

    #@14
    .line 46
    iget-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@16
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mNotFull:Ljava/util/concurrent/locks/Condition;

    #@1c
    .line 49
    new-instance v1, Ljava/util/LinkedList;

    #@1e
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    #@21
    iput-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDataBufferList:Ljava/util/LinkedList;

    #@23
    .line 72
    const/4 v1, 0x0

    #@24
    iput v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@26
    .line 74
    const/4 v1, 0x0

    #@27
    iput-boolean v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z

    #@29
    .line 75
    const/4 v1, 0x0

    #@2a
    iput-boolean v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDone:Z

    #@2c
    .line 76
    const/4 v1, 0x0

    #@2d
    iput-boolean v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mIsError:Z

    #@2f
    .line 78
    new-instance v1, Landroid/speech/tts/BlockingAudioTrack;

    #@31
    move v2, p1

    #@32
    move v3, p2

    #@33
    move v4, p3

    #@34
    move v5, p4

    #@35
    move v6, p5

    #@36
    move v7, p6

    #@37
    invoke-direct/range {v1 .. v7}, Landroid/speech/tts/BlockingAudioTrack;-><init>(IIIIFF)V

    #@3a
    iput-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@3c
    .line 80
    move-object/from16 v0, p9

    #@3e
    iput-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mLogger:Landroid/speech/tts/EventLogger;

    #@40
    .line 81
    return-void
.end method

.method private take()[B
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 205
    :try_start_1
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@3
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@6
    .line 209
    :goto_6
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDataBufferList:Ljava/util/LinkedList;

    #@8
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_23

    #@e
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z

    #@10
    if-nez v2, :cond_23

    #@12
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDone:Z

    #@14
    if-nez v2, :cond_23

    #@16
    .line 210
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mReadReady:Ljava/util/concurrent/locks/Condition;

    #@18
    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1c

    #@1b
    goto :goto_6

    #@1c
    .line 236
    :catchall_1c
    move-exception v1

    #@1d
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@1f
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@22
    throw v1

    #@23
    .line 215
    :cond_23
    :try_start_23
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z
    :try_end_25
    .catchall {:try_start_23 .. :try_end_25} :catchall_1c

    #@25
    if-eqz v2, :cond_2d

    #@27
    .line 236
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@29
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@2c
    :goto_2c
    return-object v1

    #@2d
    .line 220
    :cond_2d
    :try_start_2d
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDataBufferList:Ljava/util/LinkedList;

    #@2f
    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;
    :try_end_35
    .catchall {:try_start_2d .. :try_end_35} :catchall_1c

    #@35
    .line 224
    .local v0, entry:Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;
    if-nez v0, :cond_3d

    #@37
    .line 236
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@39
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@3c
    goto :goto_2c

    #@3d
    .line 228
    :cond_3d
    :try_start_3d
    iget v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@3f
    iget-object v2, v0, Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;->mBytes:[B

    #@41
    array-length v2, v2

    #@42
    sub-int/2addr v1, v2

    #@43
    iput v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@45
    .line 232
    iget-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mNotFull:Ljava/util/concurrent/locks/Condition;

    #@47
    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V

    #@4a
    .line 234
    iget-object v1, v0, Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;->mBytes:[B
    :try_end_4c
    .catchall {:try_start_3d .. :try_end_4c} :catchall_1c

    #@4c
    .line 236
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@4e
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@51
    goto :goto_2c
.end method


# virtual methods
.method done()V
    .registers 3

    #@0
    .prologue
    .line 156
    :try_start_0
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@2
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@5
    .line 159
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDone:Z

    #@8
    .line 164
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mReadReady:Ljava/util/concurrent/locks/Condition;

    #@a
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    #@d
    .line 170
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mNotFull:Ljava/util/concurrent/locks/Condition;

    #@f
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_12
    .catchall {:try_start_0 .. :try_end_12} :catchall_18

    #@12
    .line 172
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@14
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@17
    .line 174
    return-void

    #@18
    .line 172
    :catchall_18
    move-exception v0

    #@19
    iget-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@1b
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@1e
    throw v0
.end method

.method put([B)V
    .registers 6
    .parameter "buffer"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    :try_start_0
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@2
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@5
    .line 180
    const-wide/16 v0, 0x0

    #@7
    .line 183
    .local v0, unconsumedAudioMs:J
    :goto_7
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@9
    iget v3, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@b
    invoke-virtual {v2, v3}, Landroid/speech/tts/BlockingAudioTrack;->getAudioLengthMs(I)J

    #@e
    move-result-wide v0

    #@f
    const-wide/16 v2, 0x1f4

    #@11
    cmp-long v2, v0, v2

    #@13
    if-lez v2, :cond_26

    #@15
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z

    #@17
    if-nez v2, :cond_26

    #@19
    .line 184
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mNotFull:Ljava/util/concurrent/locks/Condition;

    #@1b
    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1e
    .catchall {:try_start_0 .. :try_end_1e} :catchall_1f

    #@1e
    goto :goto_7

    #@1f
    .line 199
    .end local v0           #unconsumedAudioMs:J
    :catchall_1f
    move-exception v2

    #@20
    iget-object v3, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@22
    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@25
    throw v2

    #@26
    .line 191
    .restart local v0       #unconsumedAudioMs:J
    :cond_26
    :try_start_26
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z
    :try_end_28
    .catchall {:try_start_26 .. :try_end_28} :catchall_1f

    #@28
    if-eqz v2, :cond_30

    #@2a
    .line 199
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@2c
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@2f
    .line 201
    :goto_2f
    return-void

    #@30
    .line 195
    :cond_30
    :try_start_30
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mDataBufferList:Ljava/util/LinkedList;

    #@32
    new-instance v3, Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;

    #@34
    invoke-direct {v3, p1}, Landroid/speech/tts/SynthesisPlaybackQueueItem$ListEntry;-><init>([B)V

    #@37
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@3a
    .line 196
    iget v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@3c
    array-length v3, p1

    #@3d
    add-int/2addr v2, v3

    #@3e
    iput v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mUnconsumedBytes:I

    #@40
    .line 197
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mReadReady:Ljava/util/concurrent/locks/Condition;

    #@42
    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_45
    .catchall {:try_start_30 .. :try_end_45} :catchall_1f

    #@45
    .line 199
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@47
    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@4a
    goto :goto_2f
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/speech/tts/SynthesisPlaybackQueueItem;->getDispatcher()Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;

    #@3
    move-result-object v1

    #@4
    .line 87
    .local v1, dispatcher:Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;
    invoke-interface {v1}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnStart()V

    #@7
    .line 90
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@9
    invoke-virtual {v2}, Landroid/speech/tts/BlockingAudioTrack;->init()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_13

    #@f
    .line 91
    invoke-interface {v1}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnError()V

    #@12
    .line 122
    :goto_12
    return-void

    #@13
    .line 96
    :cond_13
    const/4 v0, 0x0

    #@14
    .line 104
    .local v0, buffer:[B
    :goto_14
    :try_start_14
    invoke-direct {p0}, Landroid/speech/tts/SynthesisPlaybackQueueItem;->take()[B

    #@17
    move-result-object v0

    #@18
    if-eqz v0, :cond_26

    #@1a
    .line 105
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@1c
    invoke-virtual {v2, v0}, Landroid/speech/tts/BlockingAudioTrack;->write([B)I

    #@1f
    .line 106
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mLogger:Landroid/speech/tts/EventLogger;

    #@21
    invoke-virtual {v2}, Landroid/speech/tts/EventLogger;->onAudioDataWritten()V
    :try_end_24
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_24} :catch_25

    #@24
    goto :goto_14

    #@25
    .line 109
    :catch_25
    move-exception v2

    #@26
    .line 113
    :cond_26
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@28
    invoke-virtual {v2}, Landroid/speech/tts/BlockingAudioTrack;->waitAndRelease()V

    #@2b
    .line 115
    iget-boolean v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mIsError:Z

    #@2d
    if-eqz v2, :cond_38

    #@2f
    .line 116
    invoke-interface {v1}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnError()V

    #@32
    .line 121
    :goto_32
    iget-object v2, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mLogger:Landroid/speech/tts/EventLogger;

    #@34
    invoke-virtual {v2}, Landroid/speech/tts/EventLogger;->onWriteData()V

    #@37
    goto :goto_12

    #@38
    .line 118
    :cond_38
    invoke-interface {v1}, Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;->dispatchOnDone()V

    #@3b
    goto :goto_32
.end method

.method stop(Z)V
    .registers 4
    .parameter "isError"

    #@0
    .prologue
    .line 127
    :try_start_0
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@2
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    #@5
    .line 130
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mStopped:Z

    #@8
    .line 131
    iput-boolean p1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mIsError:Z

    #@a
    .line 136
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mReadReady:Ljava/util/concurrent/locks/Condition;

    #@c
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    #@f
    .line 143
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mNotFull:Ljava/util/concurrent/locks/Condition;

    #@11
    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_14
    .catchall {:try_start_0 .. :try_end_14} :catchall_1f

    #@14
    .line 145
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@16
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@19
    .line 151
    iget-object v0, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mAudioTrack:Landroid/speech/tts/BlockingAudioTrack;

    #@1b
    invoke-virtual {v0}, Landroid/speech/tts/BlockingAudioTrack;->stop()V

    #@1e
    .line 152
    return-void

    #@1f
    .line 145
    :catchall_1f
    move-exception v0

    #@20
    iget-object v1, p0, Landroid/speech/tts/SynthesisPlaybackQueueItem;->mListLock:Ljava/util/concurrent/locks/Lock;

    #@22
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    #@25
    throw v0
.end method
