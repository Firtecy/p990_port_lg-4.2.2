.class abstract Landroid/speech/tts/TextToSpeechService$SpeechItem;
.super Ljava/lang/Object;
.source "TextToSpeechService.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeechService$UtteranceProgressDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "SpeechItem"
.end annotation


# instance fields
.field private final mCallerIdentity:Ljava/lang/Object;

.field private final mCallerPid:I

.field private final mCallerUid:I

.field protected final mParams:Landroid/os/Bundle;

.field private mStarted:Z

.field private mStopped:Z

.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method public constructor <init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter "caller"
    .parameter "callerUid"
    .parameter "callerPid"
    .parameter "params"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 404
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 401
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStarted:Z

    #@8
    .line 402
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStopped:Z

    #@a
    .line 405
    iput-object p2, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerIdentity:Ljava/lang/Object;

    #@c
    .line 406
    iput-object p5, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@e
    .line 407
    iput p3, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerUid:I

    #@10
    .line 408
    iput p4, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerPid:I

    #@12
    .line 409
    return-void
.end method


# virtual methods
.method public dispatchOnDone()V
    .registers 4

    #@0
    .prologue
    .line 457
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getUtteranceId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 458
    .local v0, utteranceId:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@6
    .line 459
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@8
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$300(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$CallbackMap;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2, v0}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->dispatchOnDone(Ljava/lang/Object;Ljava/lang/String;)V

    #@13
    .line 461
    :cond_13
    return-void
.end method

.method public dispatchOnError()V
    .registers 4

    #@0
    .prologue
    .line 473
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getUtteranceId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 474
    .local v0, utteranceId:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@6
    .line 475
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@8
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$300(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$CallbackMap;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2, v0}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->dispatchOnError(Ljava/lang/Object;Ljava/lang/String;)V

    #@13
    .line 477
    :cond_13
    return-void
.end method

.method public dispatchOnStart()V
    .registers 4

    #@0
    .prologue
    .line 465
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getUtteranceId()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 466
    .local v0, utteranceId:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@6
    .line 467
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@8
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$300(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$CallbackMap;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getCallerIdentity()Ljava/lang/Object;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v1, v2, v0}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->dispatchOnStart(Ljava/lang/Object;Ljava/lang/String;)V

    #@13
    .line 469
    :cond_13
    return-void
.end method

.method public getCallerIdentity()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 412
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerIdentity:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method public getCallerPid()I
    .registers 2

    #@0
    .prologue
    .line 484
    iget v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerPid:I

    #@2
    return v0
.end method

.method public getCallerUid()I
    .registers 2

    #@0
    .prologue
    .line 480
    iget v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mCallerUid:I

    #@2
    return v0
.end method

.method protected getFloatParam(Ljava/lang/String;F)F
    .registers 4
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 520
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method protected getIntParam(Ljava/lang/String;I)I
    .registers 4
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 516
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@a
    move-result p2

    #@b
    goto :goto_4
.end method

.method public getPan()F
    .registers 3

    #@0
    .prologue
    .line 504
    const-string/jumbo v0, "pan"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getFloatParam(Ljava/lang/String;F)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getStreamType()I
    .registers 3

    #@0
    .prologue
    .line 496
    const-string/jumbo v0, "streamType"

    #@3
    const/4 v1, 0x3

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getIntParam(Ljava/lang/String;I)I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method protected getStringParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "key"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 512
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return-object p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mParams:Landroid/os/Bundle;

    #@7
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object p2

    #@b
    goto :goto_4
.end method

.method public getUtteranceId()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 508
    const-string/jumbo v0, "utteranceId"

    #@3
    const/4 v1, 0x0

    #@4
    invoke-virtual {p0, v0, v1}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getStringParam(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public getVolume()F
    .registers 3

    #@0
    .prologue
    .line 500
    const-string/jumbo v0, "volume"

    #@3
    const/high16 v1, 0x3f80

    #@5
    invoke-virtual {p0, v0, v1}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->getFloatParam(Ljava/lang/String;F)F

    #@8
    move-result v0

    #@9
    return v0
.end method

.method protected declared-synchronized isStopped()Z
    .registers 2

    #@0
    .prologue
    .line 488
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStopped:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public abstract isValid()Z
.end method

.method public play()I
    .registers 3

    #@0
    .prologue
    .line 430
    monitor-enter p0

    #@1
    .line 431
    :try_start_1
    iget-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStarted:Z

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 432
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "play() called twice"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 435
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    #@10
    throw v0

    #@11
    .line 434
    :cond_11
    const/4 v0, 0x1

    #@12
    :try_start_12
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStarted:Z

    #@14
    .line 435
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_e

    #@15
    .line 436
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->playImpl()I

    #@18
    move-result v0

    #@19
    return v0
.end method

.method protected abstract playImpl()I
.end method

.method public stop()V
    .registers 3

    #@0
    .prologue
    .line 446
    monitor-enter p0

    #@1
    .line 447
    :try_start_1
    iget-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStopped:Z

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 448
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "stop() called twice"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 451
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    #@10
    throw v0

    #@11
    .line 450
    :cond_11
    const/4 v0, 0x1

    #@12
    :try_start_12
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SpeechItem;->mStopped:Z

    #@14
    .line 451
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_e

    #@15
    .line 452
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SpeechItem;->stopImpl()V

    #@18
    .line 453
    return-void
.end method

.method protected abstract stopImpl()V
.end method
