.class Landroid/speech/tts/EventLogger;
.super Ljava/lang/Object;
.source "EventLogger.java"


# instance fields
.field private final mCallerPid:I

.field private final mCallerUid:I

.field private volatile mEngineCompleteTime:J

.field private volatile mEngineStartTime:J

.field private volatile mError:Z

.field private mLogWritten:Z

.field private mPlaybackStartTime:J

.field private final mReceivedTime:J

.field private final mRequest:Landroid/speech/tts/SynthesisRequest;

.field private volatile mRequestProcessingStartTime:J

.field private final mServiceApp:Ljava/lang/String;

.field private volatile mStopped:Z


# direct methods
.method constructor <init>(Landroid/speech/tts/SynthesisRequest;IILjava/lang/String;)V
    .registers 8
    .parameter "request"
    .parameter "callerUid"
    .parameter "callerPid"
    .parameter "serviceApp"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const-wide/16 v0, -0x1

    #@3
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 38
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mPlaybackStartTime:J

    #@8
    .line 39
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mRequestProcessingStartTime:J

    #@a
    .line 40
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineStartTime:J

    #@c
    .line 41
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineCompleteTime:J

    #@e
    .line 43
    iput-boolean v2, p0, Landroid/speech/tts/EventLogger;->mError:Z

    #@10
    .line 44
    iput-boolean v2, p0, Landroid/speech/tts/EventLogger;->mStopped:Z

    #@12
    .line 45
    iput-boolean v2, p0, Landroid/speech/tts/EventLogger;->mLogWritten:Z

    #@14
    .line 48
    iput-object p1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@16
    .line 49
    iput p2, p0, Landroid/speech/tts/EventLogger;->mCallerUid:I

    #@18
    .line 50
    iput p3, p0, Landroid/speech/tts/EventLogger;->mCallerPid:I

    #@1a
    .line 51
    iput-object p4, p0, Landroid/speech/tts/EventLogger;->mServiceApp:Ljava/lang/String;

    #@1c
    .line 52
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@1f
    move-result-wide v0

    #@20
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mReceivedTime:J

    #@22
    .line 53
    return-void
.end method

.method private getLocaleString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x2d

    #@2
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@6
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getLanguage()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    #@d
    .line 165
    .local v0, sb:Ljava/lang/StringBuilder;
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@f
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getCountry()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_3d

    #@19
    .line 166
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1c
    .line 167
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@1e
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getCountry()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 169
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@27
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getVariant()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_3d

    #@31
    .line 170
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@34
    .line 171
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@36
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getVariant()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    .line 175
    :cond_3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    return-object v1
.end method

.method private getUtteranceLength()I
    .registers 3

    #@0
    .prologue
    .line 155
    iget-object v1, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@2
    invoke-virtual {v1}, Landroid/speech/tts/SynthesisRequest;->getText()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 156
    .local v0, utterance:Ljava/lang/String;
    if-nez v0, :cond_a

    #@8
    const/4 v1, 0x0

    #@9
    :goto_9
    return v1

    #@a
    :cond_a
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@d
    move-result v1

    #@e
    goto :goto_9
.end method


# virtual methods
.method public onAudioDataWritten()V
    .registers 5

    #@0
    .prologue
    .line 91
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mPlaybackStartTime:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_e

    #@8
    .line 92
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v0

    #@c
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mPlaybackStartTime:J

    #@e
    .line 94
    :cond_e
    return-void
.end method

.method public onEngineComplete()V
    .registers 3

    #@0
    .prologue
    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineCompleteTime:J

    #@6
    .line 80
    return-void
.end method

.method public onEngineDataReceived()V
    .registers 5

    #@0
    .prologue
    .line 69
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineStartTime:J

    #@2
    const-wide/16 v2, -0x1

    #@4
    cmp-long v0, v0, v2

    #@6
    if-nez v0, :cond_e

    #@8
    .line 70
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@b
    move-result-wide v0

    #@c
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineStartTime:J

    #@e
    .line 72
    :cond_e
    return-void
.end method

.method public onError()V
    .registers 2

    #@0
    .prologue
    .line 109
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/speech/tts/EventLogger;->mError:Z

    #@3
    .line 110
    return-void
.end method

.method public onRequestProcessingStart()V
    .registers 3

    #@0
    .prologue
    .line 61
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    iput-wide v0, p0, Landroid/speech/tts/EventLogger;->mRequestProcessingStartTime:J

    #@6
    .line 62
    return-void
.end method

.method public onStopped()V
    .registers 2

    #@0
    .prologue
    .line 101
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/speech/tts/EventLogger;->mStopped:Z

    #@3
    .line 102
    return-void
.end method

.method public onWriteData()V
    .registers 16

    #@0
    .prologue
    const-wide/16 v2, -0x1

    #@2
    .line 117
    iget-boolean v0, p0, Landroid/speech/tts/EventLogger;->mLogWritten:Z

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 148
    :cond_6
    :goto_6
    return-void

    #@7
    .line 120
    :cond_7
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/speech/tts/EventLogger;->mLogWritten:Z

    #@a
    .line 123
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@d
    move-result-wide v13

    #@e
    .line 126
    .local v13, completionTime:J
    iget-boolean v0, p0, Landroid/speech/tts/EventLogger;->mError:Z

    #@10
    if-nez v0, :cond_1e

    #@12
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mPlaybackStartTime:J

    #@14
    cmp-long v0, v0, v2

    #@16
    if-eqz v0, :cond_1e

    #@18
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineCompleteTime:J

    #@1a
    cmp-long v0, v0, v2

    #@1c
    if-nez v0, :cond_3c

    #@1e
    .line 127
    :cond_1e
    iget-object v0, p0, Landroid/speech/tts/EventLogger;->mServiceApp:Ljava/lang/String;

    #@20
    iget v1, p0, Landroid/speech/tts/EventLogger;->mCallerUid:I

    #@22
    iget v2, p0, Landroid/speech/tts/EventLogger;->mCallerPid:I

    #@24
    invoke-direct {p0}, Landroid/speech/tts/EventLogger;->getUtteranceLength()I

    #@27
    move-result v3

    #@28
    invoke-direct {p0}, Landroid/speech/tts/EventLogger;->getLocaleString()Ljava/lang/String;

    #@2b
    move-result-object v4

    #@2c
    iget-object v5, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@2e
    invoke-virtual {v5}, Landroid/speech/tts/SynthesisRequest;->getSpeechRate()I

    #@31
    move-result v5

    #@32
    iget-object v6, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@34
    invoke-virtual {v6}, Landroid/speech/tts/SynthesisRequest;->getPitch()I

    #@37
    move-result v6

    #@38
    invoke-static/range {v0 .. v6}, Landroid/speech/tts/EventLogTags;->writeTtsSpeakFailure(Ljava/lang/String;IIILjava/lang/String;II)V

    #@3b
    goto :goto_6

    #@3c
    .line 136
    :cond_3c
    iget-boolean v0, p0, Landroid/speech/tts/EventLogger;->mStopped:Z

    #@3e
    if-nez v0, :cond_6

    #@40
    .line 140
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mPlaybackStartTime:J

    #@42
    iget-wide v2, p0, Landroid/speech/tts/EventLogger;->mReceivedTime:J

    #@44
    sub-long v11, v0, v2

    #@46
    .line 141
    .local v11, audioLatency:J
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineStartTime:J

    #@48
    iget-wide v2, p0, Landroid/speech/tts/EventLogger;->mRequestProcessingStartTime:J

    #@4a
    sub-long v7, v0, v2

    #@4c
    .line 142
    .local v7, engineLatency:J
    iget-wide v0, p0, Landroid/speech/tts/EventLogger;->mEngineCompleteTime:J

    #@4e
    iget-wide v2, p0, Landroid/speech/tts/EventLogger;->mRequestProcessingStartTime:J

    #@50
    sub-long v9, v0, v2

    #@52
    .line 144
    .local v9, engineTotal:J
    iget-object v0, p0, Landroid/speech/tts/EventLogger;->mServiceApp:Ljava/lang/String;

    #@54
    iget v1, p0, Landroid/speech/tts/EventLogger;->mCallerUid:I

    #@56
    iget v2, p0, Landroid/speech/tts/EventLogger;->mCallerPid:I

    #@58
    invoke-direct {p0}, Landroid/speech/tts/EventLogger;->getUtteranceLength()I

    #@5b
    move-result v3

    #@5c
    invoke-direct {p0}, Landroid/speech/tts/EventLogger;->getLocaleString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    iget-object v5, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@62
    invoke-virtual {v5}, Landroid/speech/tts/SynthesisRequest;->getSpeechRate()I

    #@65
    move-result v5

    #@66
    iget-object v6, p0, Landroid/speech/tts/EventLogger;->mRequest:Landroid/speech/tts/SynthesisRequest;

    #@68
    invoke-virtual {v6}, Landroid/speech/tts/SynthesisRequest;->getPitch()I

    #@6b
    move-result v6

    #@6c
    invoke-static/range {v0 .. v12}, Landroid/speech/tts/EventLogTags;->writeTtsSpeakSuccess(Ljava/lang/String;IIILjava/lang/String;IIJJJ)V

    #@6f
    goto :goto_6
.end method
