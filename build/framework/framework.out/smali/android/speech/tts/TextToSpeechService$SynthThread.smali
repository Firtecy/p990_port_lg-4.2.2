.class Landroid/speech/tts/TextToSpeechService$SynthThread;
.super Landroid/os/HandlerThread;
.source "TextToSpeechService.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SynthThread"
.end annotation


# instance fields
.field private mFirstIdle:Z

.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method public constructor <init>(Landroid/speech/tts/TextToSpeechService;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 232
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$SynthThread;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    .line 233
    const-string v0, "SynthThread"

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    #@8
    .line 230
    const/4 v0, 0x1

    #@9
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SynthThread;->mFirstIdle:Z

    #@b
    .line 234
    return-void
.end method

.method private broadcastTtsQueueProcessingCompleted()V
    .registers 3

    #@0
    .prologue
    .line 252
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 254
    .local v0, i:Landroid/content/Intent;
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$SynthThread;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@9
    invoke-virtual {v1, v0}, Landroid/speech/tts/TextToSpeechService;->sendBroadcast(Landroid/content/Intent;)V

    #@c
    .line 255
    return-void
.end method


# virtual methods
.method protected onLooperPrepared()V
    .registers 2

    #@0
    .prologue
    .line 238
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeechService$SynthThread;->getLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Landroid/os/Looper;->getQueue()Landroid/os/MessageQueue;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {v0, p0}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    #@b
    .line 239
    return-void
.end method

.method public queueIdle()Z
    .registers 2

    #@0
    .prologue
    .line 243
    iget-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SynthThread;->mFirstIdle:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 244
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/speech/tts/TextToSpeechService$SynthThread;->mFirstIdle:Z

    #@7
    .line 248
    :goto_7
    const/4 v0, 0x1

    #@8
    return v0

    #@9
    .line 246
    :cond_9
    invoke-direct {p0}, Landroid/speech/tts/TextToSpeechService$SynthThread;->broadcastTtsQueueProcessingCompleted()V

    #@c
    goto :goto_7
.end method
