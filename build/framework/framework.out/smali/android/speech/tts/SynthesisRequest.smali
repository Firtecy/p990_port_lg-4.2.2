.class public final Landroid/speech/tts/SynthesisRequest;
.super Ljava/lang/Object;
.source "SynthesisRequest.java"


# instance fields
.field private mCountry:Ljava/lang/String;

.field private mLanguage:Ljava/lang/String;

.field private final mParams:Landroid/os/Bundle;

.field private mPitch:I

.field private mSpeechRate:I

.field private final mText:Ljava/lang/String;

.field private mVariant:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 4
    .parameter "text"
    .parameter "params"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    iput-object p1, p0, Landroid/speech/tts/SynthesisRequest;->mText:Ljava/lang/String;

    #@5
    .line 48
    new-instance v0, Landroid/os/Bundle;

    #@7
    invoke-direct {v0, p2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    #@a
    iput-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mParams:Landroid/os/Bundle;

    #@c
    .line 49
    return-void
.end method


# virtual methods
.method public getCountry()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mCountry:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mLanguage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getParams()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mParams:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public getPitch()I
    .registers 2

    #@0
    .prologue
    .line 90
    iget v0, p0, Landroid/speech/tts/SynthesisRequest;->mPitch:I

    #@2
    return v0
.end method

.method public getSpeechRate()I
    .registers 2

    #@0
    .prologue
    .line 83
    iget v0, p0, Landroid/speech/tts/SynthesisRequest;->mSpeechRate:I

    #@2
    return v0
.end method

.method public getText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getVariant()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Landroid/speech/tts/SynthesisRequest;->mVariant:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method setLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "language"
    .parameter "country"
    .parameter "variant"

    #@0
    .prologue
    .line 104
    iput-object p1, p0, Landroid/speech/tts/SynthesisRequest;->mLanguage:Ljava/lang/String;

    #@2
    .line 105
    iput-object p2, p0, Landroid/speech/tts/SynthesisRequest;->mCountry:Ljava/lang/String;

    #@4
    .line 106
    iput-object p3, p0, Landroid/speech/tts/SynthesisRequest;->mVariant:Ljava/lang/String;

    #@6
    .line 107
    return-void
.end method

.method setPitch(I)V
    .registers 2
    .parameter "pitch"

    #@0
    .prologue
    .line 120
    iput p1, p0, Landroid/speech/tts/SynthesisRequest;->mPitch:I

    #@2
    .line 121
    return-void
.end method

.method setSpeechRate(I)V
    .registers 2
    .parameter "speechRate"

    #@0
    .prologue
    .line 113
    iput p1, p0, Landroid/speech/tts/SynthesisRequest;->mSpeechRate:I

    #@2
    .line 114
    return-void
.end method
