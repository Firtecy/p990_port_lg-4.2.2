.class public abstract Landroid/speech/tts/ITextToSpeechService$Stub;
.super Landroid/os/Binder;
.source "ITextToSpeechService.java"

# interfaces
.implements Landroid/speech/tts/ITextToSpeechService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/ITextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/tts/ITextToSpeechService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.speech.tts.ITextToSpeechService"

.field static final TRANSACTION_getFeaturesForLanguage:I = 0x9

.field static final TRANSACTION_getLanguage:I = 0x7

.field static final TRANSACTION_isLanguageAvailable:I = 0x8

.field static final TRANSACTION_isSpeaking:I = 0x5

.field static final TRANSACTION_loadLanguage:I = 0xa

.field static final TRANSACTION_playAudio:I = 0x3

.field static final TRANSACTION_playSilence:I = 0x4

.field static final TRANSACTION_setCallback:I = 0xb

.field static final TRANSACTION_speak:I = 0x1

.field static final TRANSACTION_stop:I = 0x6

.field static final TRANSACTION_synthesizeToFile:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/speech/tts/ITextToSpeechService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/speech/tts/ITextToSpeechService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.speech.tts.ITextToSpeechService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/speech/tts/ITextToSpeechService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/speech/tts/ITextToSpeechService;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/speech/tts/ITextToSpeechService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/speech/tts/ITextToSpeechService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_17a

    #@4
    .line 219
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v7

    #@8
    :goto_8
    return v7

    #@9
    .line 47
    :sswitch_9
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v1

    #@18
    .line 56
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    .line 58
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v4

    #@20
    .line 60
    .local v4, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_39

    #@26
    .line 61
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@28
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2b
    move-result-object v5

    #@2c
    check-cast v5, Landroid/os/Bundle;

    #@2e
    .line 66
    .local v5, _arg3:Landroid/os/Bundle;
    :goto_2e
    invoke-virtual {p0, v1, v2, v4, v5}, Landroid/speech/tts/ITextToSpeechService$Stub;->speak(Landroid/os/IBinder;Ljava/lang/String;ILandroid/os/Bundle;)I

    #@31
    move-result v6

    #@32
    .line 67
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@35
    .line 68
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    goto :goto_8

    #@39
    .line 64
    .end local v5           #_arg3:Landroid/os/Bundle;
    .end local v6           #_result:I
    :cond_39
    const/4 v5, 0x0

    #@3a
    .restart local v5       #_arg3:Landroid/os/Bundle;
    goto :goto_2e

    #@3b
    .line 73
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/Bundle;
    :sswitch_3b
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@3d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@43
    move-result-object v1

    #@44
    .line 77
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    .line 79
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    .line 81
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v0

    #@50
    if-eqz v0, :cond_65

    #@52
    .line 82
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@54
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@57
    move-result-object v5

    #@58
    check-cast v5, Landroid/os/Bundle;

    #@5a
    .line 87
    .restart local v5       #_arg3:Landroid/os/Bundle;
    :goto_5a
    invoke-virtual {p0, v1, v2, v4, v5}, Landroid/speech/tts/ITextToSpeechService$Stub;->synthesizeToFile(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I

    #@5d
    move-result v6

    #@5e
    .line 88
    .restart local v6       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@61
    .line 89
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    goto :goto_8

    #@65
    .line 85
    .end local v5           #_arg3:Landroid/os/Bundle;
    .end local v6           #_result:I
    :cond_65
    const/4 v5, 0x0

    #@66
    .restart local v5       #_arg3:Landroid/os/Bundle;
    goto :goto_5a

    #@67
    .line 94
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v5           #_arg3:Landroid/os/Bundle;
    :sswitch_67
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@69
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@6f
    move-result-object v1

    #@70
    .line 98
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v0

    #@74
    if-eqz v0, :cond_9c

    #@76
    .line 99
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    #@78
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7b
    move-result-object v2

    #@7c
    check-cast v2, Landroid/net/Uri;

    #@7e
    .line 105
    .local v2, _arg1:Landroid/net/Uri;
    :goto_7e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v4

    #@82
    .line 107
    .local v4, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@85
    move-result v0

    #@86
    if-eqz v0, :cond_9e

    #@88
    .line 108
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@8a
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8d
    move-result-object v5

    #@8e
    check-cast v5, Landroid/os/Bundle;

    #@90
    .line 113
    .restart local v5       #_arg3:Landroid/os/Bundle;
    :goto_90
    invoke-virtual {p0, v1, v2, v4, v5}, Landroid/speech/tts/ITextToSpeechService$Stub;->playAudio(Landroid/os/IBinder;Landroid/net/Uri;ILandroid/os/Bundle;)I

    #@93
    move-result v6

    #@94
    .line 114
    .restart local v6       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@97
    .line 115
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@9a
    goto/16 :goto_8

    #@9c
    .line 102
    .end local v2           #_arg1:Landroid/net/Uri;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/Bundle;
    .end local v6           #_result:I
    :cond_9c
    const/4 v2, 0x0

    #@9d
    .restart local v2       #_arg1:Landroid/net/Uri;
    goto :goto_7e

    #@9e
    .line 111
    .restart local v4       #_arg2:I
    :cond_9e
    const/4 v5, 0x0

    #@9f
    .restart local v5       #_arg3:Landroid/os/Bundle;
    goto :goto_90

    #@a0
    .line 120
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:Landroid/net/Uri;
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/Bundle;
    :sswitch_a0
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@a2
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a5
    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@a8
    move-result-object v1

    #@a9
    .line 124
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    #@ac
    move-result-wide v2

    #@ad
    .line 126
    .local v2, _arg1:J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b0
    move-result v4

    #@b1
    .line 128
    .restart local v4       #_arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b4
    move-result v0

    #@b5
    if-eqz v0, :cond_cc

    #@b7
    .line 129
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b9
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@bc
    move-result-object v5

    #@bd
    check-cast v5, Landroid/os/Bundle;

    #@bf
    .restart local v5       #_arg3:Landroid/os/Bundle;
    :goto_bf
    move-object v0, p0

    #@c0
    .line 134
    invoke-virtual/range {v0 .. v5}, Landroid/speech/tts/ITextToSpeechService$Stub;->playSilence(Landroid/os/IBinder;JILandroid/os/Bundle;)I

    #@c3
    move-result v6

    #@c4
    .line 135
    .restart local v6       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c7
    .line 136
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@ca
    goto/16 :goto_8

    #@cc
    .line 132
    .end local v5           #_arg3:Landroid/os/Bundle;
    .end local v6           #_result:I
    :cond_cc
    const/4 v5, 0x0

    #@cd
    .restart local v5       #_arg3:Landroid/os/Bundle;
    goto :goto_bf

    #@ce
    .line 141
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v2           #_arg1:J
    .end local v4           #_arg2:I
    .end local v5           #_arg3:Landroid/os/Bundle;
    :sswitch_ce
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@d0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d3
    .line 142
    invoke-virtual {p0}, Landroid/speech/tts/ITextToSpeechService$Stub;->isSpeaking()Z

    #@d6
    move-result v6

    #@d7
    .line 143
    .local v6, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@da
    .line 144
    if-eqz v6, :cond_e2

    #@dc
    move v0, v7

    #@dd
    :goto_dd
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@e0
    goto/16 :goto_8

    #@e2
    :cond_e2
    const/4 v0, 0x0

    #@e3
    goto :goto_dd

    #@e4
    .line 149
    .end local v6           #_result:Z
    :sswitch_e4
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@e6
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@e9
    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ec
    move-result-object v1

    #@ed
    .line 152
    .restart local v1       #_arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v1}, Landroid/speech/tts/ITextToSpeechService$Stub;->stop(Landroid/os/IBinder;)I

    #@f0
    move-result v6

    #@f1
    .line 153
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f4
    .line 154
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@f7
    goto/16 :goto_8

    #@f9
    .line 159
    .end local v1           #_arg0:Landroid/os/IBinder;
    .end local v6           #_result:I
    :sswitch_f9
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@fb
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@fe
    .line 160
    invoke-virtual {p0}, Landroid/speech/tts/ITextToSpeechService$Stub;->getLanguage()[Ljava/lang/String;

    #@101
    move-result-object v6

    #@102
    .line 161
    .local v6, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@105
    .line 162
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@108
    goto/16 :goto_8

    #@10a
    .line 167
    .end local v6           #_result:[Ljava/lang/String;
    :sswitch_10a
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@10c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@10f
    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@112
    move-result-object v1

    #@113
    .line 171
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@116
    move-result-object v2

    #@117
    .line 173
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@11a
    move-result-object v4

    #@11b
    .line 174
    .local v4, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v4}, Landroid/speech/tts/ITextToSpeechService$Stub;->isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@11e
    move-result v6

    #@11f
    .line 175
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@122
    .line 176
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@125
    goto/16 :goto_8

    #@127
    .line 181
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v6           #_result:I
    :sswitch_127
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@129
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12c
    .line 183
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12f
    move-result-object v1

    #@130
    .line 185
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@133
    move-result-object v2

    #@134
    .line 187
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@137
    move-result-object v4

    #@138
    .line 188
    .restart local v4       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v4}, Landroid/speech/tts/ITextToSpeechService$Stub;->getFeaturesForLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    #@13b
    move-result-object v6

    #@13c
    .line 189
    .local v6, _result:[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13f
    .line 190
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@142
    goto/16 :goto_8

    #@144
    .line 195
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v6           #_result:[Ljava/lang/String;
    :sswitch_144
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@146
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@149
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14c
    move-result-object v1

    #@14d
    .line 199
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@150
    move-result-object v2

    #@151
    .line 201
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@154
    move-result-object v4

    #@155
    .line 202
    .restart local v4       #_arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v4}, Landroid/speech/tts/ITextToSpeechService$Stub;->loadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@158
    move-result v6

    #@159
    .line 203
    .local v6, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@15c
    .line 204
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    #@15f
    goto/16 :goto_8

    #@161
    .line 209
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v4           #_arg2:Ljava/lang/String;
    .end local v6           #_result:I
    :sswitch_161
    const-string v0, "android.speech.tts.ITextToSpeechService"

    #@163
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@166
    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@169
    move-result-object v1

    #@16a
    .line 213
    .local v1, _arg0:Landroid/os/IBinder;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@16d
    move-result-object v0

    #@16e
    invoke-static {v0}, Landroid/speech/tts/ITextToSpeechCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/speech/tts/ITextToSpeechCallback;

    #@171
    move-result-object v2

    #@172
    .line 214
    .local v2, _arg1:Landroid/speech/tts/ITextToSpeechCallback;
    invoke-virtual {p0, v1, v2}, Landroid/speech/tts/ITextToSpeechService$Stub;->setCallback(Landroid/os/IBinder;Landroid/speech/tts/ITextToSpeechCallback;)V

    #@175
    .line 215
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@178
    goto/16 :goto_8

    #@17a
    .line 43
    :sswitch_data_17a
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_3b
        0x3 -> :sswitch_67
        0x4 -> :sswitch_a0
        0x5 -> :sswitch_ce
        0x6 -> :sswitch_e4
        0x7 -> :sswitch_f9
        0x8 -> :sswitch_10a
        0x9 -> :sswitch_127
        0xa -> :sswitch_144
        0xb -> :sswitch_161
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
