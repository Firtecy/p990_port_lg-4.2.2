.class Landroid/speech/tts/TextToSpeechService$1;
.super Landroid/speech/tts/ITextToSpeechService$Stub;
.source "TextToSpeechService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeechService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 728
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    invoke-direct {p0}, Landroid/speech/tts/ITextToSpeechService$Stub;-><init>()V

    #@5
    return-void
.end method

.method private varargs checkNonNull([Ljava/lang/Object;)Z
    .registers 7
    .parameter "args"

    #@0
    .prologue
    .line 849
    move-object v0, p1

    #@1
    .local v0, arr$:[Ljava/lang/Object;
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_e

    #@5
    aget-object v3, v0, v1

    #@7
    .line 850
    .local v3, o:Ljava/lang/Object;
    if-nez v3, :cond_b

    #@9
    const/4 v4, 0x0

    #@a
    .line 852
    .end local v3           #o:Ljava/lang/Object;
    :goto_a
    return v4

    #@b
    .line 849
    .restart local v3       #o:Ljava/lang/Object;
    :cond_b
    add-int/lit8 v1, v1, 0x1

    #@d
    goto :goto_3

    #@e
    .line 852
    .end local v3           #o:Ljava/lang/Object;
    :cond_e
    const/4 v4, 0x1

    #@f
    goto :goto_a
.end method

.method private intern(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 845
    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method


# virtual methods
.method public getFeaturesForLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 7
    .parameter "lang"
    .parameter "country"
    .parameter "variant"

    #@0
    .prologue
    .line 809
    iget-object v2, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    invoke-virtual {v2, p1, p2, p3}, Landroid/speech/tts/TextToSpeechService;->onGetFeaturesForLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    #@5
    move-result-object v0

    #@6
    .line 810
    .local v0, features:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    #@7
    .line 811
    .local v1, featuresArray:[Ljava/lang/String;
    if-eqz v0, :cond_13

    #@9
    .line 812
    invoke-interface {v0}, Ljava/util/Set;->size()I

    #@c
    move-result v2

    #@d
    new-array v1, v2, [Ljava/lang/String;

    #@f
    .line 813
    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@12
    .line 817
    :goto_12
    return-object v1

    #@13
    .line 815
    :cond_13
    const/4 v2, 0x0

    #@14
    new-array v1, v2, [Ljava/lang/String;

    #@16
    goto :goto_12
.end method

.method public getLanguage()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 791
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeechService;->onGetLanguage()[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "lang"
    .parameter "country"
    .parameter "variant"

    #@0
    .prologue
    .line 800
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p1, v0, v1

    #@6
    invoke-direct {p0, v0}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    .line 801
    const/4 v0, -0x1

    #@d
    .line 804
    :goto_d
    return v0

    #@e
    :cond_e
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@10
    invoke-virtual {v0, p1, p2, p3}, Landroid/speech/tts/TextToSpeechService;->onIsLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    goto :goto_d
.end method

.method public isSpeaking()Z
    .registers 2

    #@0
    .prologue
    .line 777
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    invoke-static {v0}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->isSpeaking()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_18

    #@c
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@e
    invoke-static {v0}, Landroid/speech/tts/TextToSpeechService;->access$200(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/AudioPlaybackHandler;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Landroid/speech/tts/AudioPlaybackHandler;->isSpeaking()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1a

    #@18
    :cond_18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public loadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter "lang"
    .parameter "country"
    .parameter "variant"

    #@0
    .prologue
    .line 826
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p1, v0, v1

    #@6
    invoke-direct {p0, v0}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    .line 827
    const/4 v0, -0x1

    #@d
    .line 830
    :goto_d
    return v0

    #@e
    :cond_e
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@10
    invoke-virtual {v0, p1, p2, p3}, Landroid/speech/tts/TextToSpeechService;->onLoadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@13
    move-result v0

    #@14
    goto :goto_d
.end method

.method public playAudio(Landroid/os/IBinder;Landroid/net/Uri;ILandroid/os/Bundle;)I
    .registers 12
    .parameter "caller"
    .parameter "audioUri"
    .parameter "queueMode"
    .parameter "params"

    #@0
    .prologue
    .line 755
    const/4 v1, 0x3

    #@1
    new-array v1, v1, [Ljava/lang/Object;

    #@3
    const/4 v2, 0x0

    #@4
    aput-object p1, v1, v2

    #@6
    const/4 v2, 0x1

    #@7
    aput-object p2, v1, v2

    #@9
    const/4 v2, 0x2

    #@a
    aput-object p4, v1, v2

    #@c
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_14

    #@12
    .line 756
    const/4 v1, -0x1

    #@13
    .line 761
    :goto_13
    return v1

    #@14
    .line 759
    :cond_14
    new-instance v0, Landroid/speech/tts/TextToSpeechService$AudioSpeechItem;

    #@16
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@18
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1b
    move-result v3

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1f
    move-result v4

    #@20
    move-object v2, p1

    #@21
    move-object v5, p4

    #@22
    move-object v6, p2

    #@23
    invoke-direct/range {v0 .. v6}, Landroid/speech/tts/TextToSpeechService$AudioSpeechItem;-><init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;Landroid/net/Uri;)V

    #@26
    .line 761
    .local v0, item:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@28
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p3, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->enqueueSpeechItem(ILandroid/speech/tts/TextToSpeechService$SpeechItem;)I

    #@2f
    move-result v1

    #@30
    goto :goto_13
.end method

.method public playSilence(Landroid/os/IBinder;JILandroid/os/Bundle;)I
    .registers 14
    .parameter "caller"
    .parameter "duration"
    .parameter "queueMode"
    .parameter "params"

    #@0
    .prologue
    .line 766
    const/4 v1, 0x2

    #@1
    new-array v1, v1, [Ljava/lang/Object;

    #@3
    const/4 v2, 0x0

    #@4
    aput-object p1, v1, v2

    #@6
    const/4 v2, 0x1

    #@7
    aput-object p5, v1, v2

    #@9
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_11

    #@f
    .line 767
    const/4 v1, -0x1

    #@10
    .line 772
    :goto_10
    return v1

    #@11
    .line 770
    :cond_11
    new-instance v0, Landroid/speech/tts/TextToSpeechService$SilenceSpeechItem;

    #@13
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@15
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@18
    move-result v3

    #@19
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1c
    move-result v4

    #@1d
    move-object v2, p1

    #@1e
    move-object v5, p5

    #@1f
    move-wide v6, p2

    #@20
    invoke-direct/range {v0 .. v7}, Landroid/speech/tts/TextToSpeechService$SilenceSpeechItem;-><init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;J)V

    #@23
    .line 772
    .local v0, item:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@25
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p4, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->enqueueSpeechItem(ILandroid/speech/tts/TextToSpeechService$SpeechItem;)I

    #@2c
    move-result v1

    #@2d
    goto :goto_10
.end method

.method public setCallback(Landroid/os/IBinder;Landroid/speech/tts/ITextToSpeechCallback;)V
    .registers 5
    .parameter "caller"
    .parameter "cb"

    #@0
    .prologue
    .line 836
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p1, v0, v1

    #@6
    invoke-direct {p0, v0}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_d

    #@c
    .line 841
    :goto_c
    return-void

    #@d
    .line 840
    :cond_d
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@f
    invoke-static {v0}, Landroid/speech/tts/TextToSpeechService;->access$300(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$CallbackMap;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p1, p2}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->setCallback(Landroid/os/IBinder;Landroid/speech/tts/ITextToSpeechCallback;)V

    #@16
    goto :goto_c
.end method

.method public speak(Landroid/os/IBinder;Ljava/lang/String;ILandroid/os/Bundle;)I
    .registers 12
    .parameter "caller"
    .parameter "text"
    .parameter "queueMode"
    .parameter "params"

    #@0
    .prologue
    .line 731
    const/4 v1, 0x3

    #@1
    new-array v1, v1, [Ljava/lang/Object;

    #@3
    const/4 v2, 0x0

    #@4
    aput-object p1, v1, v2

    #@6
    const/4 v2, 0x1

    #@7
    aput-object p2, v1, v2

    #@9
    const/4 v2, 0x2

    #@a
    aput-object p4, v1, v2

    #@c
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_14

    #@12
    .line 732
    const/4 v1, -0x1

    #@13
    .line 737
    :goto_13
    return v1

    #@14
    .line 735
    :cond_14
    new-instance v0, Landroid/speech/tts/TextToSpeechService$SynthesisSpeechItem;

    #@16
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@18
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@1b
    move-result v3

    #@1c
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@1f
    move-result v4

    #@20
    move-object v2, p1

    #@21
    move-object v5, p4

    #@22
    move-object v6, p2

    #@23
    invoke-direct/range {v0 .. v6}, Landroid/speech/tts/TextToSpeechService$SynthesisSpeechItem;-><init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;Ljava/lang/String;)V

    #@26
    .line 737
    .local v0, item:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@28
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p3, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->enqueueSpeechItem(ILandroid/speech/tts/TextToSpeechService$SpeechItem;)I

    #@2f
    move-result v1

    #@30
    goto :goto_13
.end method

.method public stop(Landroid/os/IBinder;)I
    .registers 4
    .parameter "caller"

    #@0
    .prologue
    .line 782
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    aput-object p1, v0, v1

    #@6
    invoke-direct {p0, v0}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    .line 783
    const/4 v0, -0x1

    #@d
    .line 786
    :goto_d
    return v0

    #@e
    :cond_e
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@10
    invoke-static {v0}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->stopForApp(Ljava/lang/Object;)I

    #@17
    move-result v0

    #@18
    goto :goto_d
.end method

.method public synthesizeToFile(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I
    .registers 14
    .parameter "caller"
    .parameter "text"
    .parameter "filename"
    .parameter "params"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 743
    const/4 v1, 0x4

    #@2
    new-array v1, v1, [Ljava/lang/Object;

    #@4
    const/4 v2, 0x0

    #@5
    aput-object p1, v1, v2

    #@7
    aput-object p2, v1, v8

    #@9
    const/4 v2, 0x2

    #@a
    aput-object p3, v1, v2

    #@c
    const/4 v2, 0x3

    #@d
    aput-object p4, v1, v2

    #@f
    invoke-direct {p0, v1}, Landroid/speech/tts/TextToSpeechService$1;->checkNonNull([Ljava/lang/Object;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_17

    #@15
    .line 744
    const/4 v1, -0x1

    #@16
    .line 750
    :goto_16
    return v1

    #@17
    .line 747
    :cond_17
    new-instance v7, Ljava/io/File;

    #@19
    invoke-direct {v7, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@1c
    .line 748
    .local v7, file:Ljava/io/File;
    new-instance v0, Landroid/speech/tts/TextToSpeechService$SynthesisToFileSpeechItem;

    #@1e
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@20
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@23
    move-result v3

    #@24
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    #@27
    move-result v4

    #@28
    move-object v2, p1

    #@29
    move-object v5, p4

    #@2a
    move-object v6, p2

    #@2b
    invoke-direct/range {v0 .. v7}, Landroid/speech/tts/TextToSpeechService$SynthesisToFileSpeechItem;-><init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;Ljava/lang/String;Ljava/io/File;)V

    #@2e
    .line 750
    .local v0, item:Landroid/speech/tts/TextToSpeechService$SpeechItem;
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$1;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@30
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, v8, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->enqueueSpeechItem(ILandroid/speech/tts/TextToSpeechService$SpeechItem;)I

    #@37
    move-result v1

    #@38
    goto :goto_16
.end method
