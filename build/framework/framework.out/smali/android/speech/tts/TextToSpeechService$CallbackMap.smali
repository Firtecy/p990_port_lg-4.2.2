.class Landroid/speech/tts/TextToSpeechService$CallbackMap;
.super Landroid/os/RemoteCallbackList;
.source "TextToSpeechService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallbackMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Landroid/speech/tts/ITextToSpeechCallback;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCallerToCallback:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/speech/tts/ITextToSpeechCallback;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method private constructor <init>(Landroid/speech/tts/TextToSpeechService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 856
    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@2
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@5
    .line 857
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Landroid/speech/tts/TextToSpeechService;Landroid/speech/tts/TextToSpeechService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 856
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$CallbackMap;-><init>(Landroid/speech/tts/TextToSpeechService;)V

    #@3
    return-void
.end method

.method private getCallbackFor(Ljava/lang/Object;)Landroid/speech/tts/ITextToSpeechCallback;
    .registers 6
    .parameter "caller"

    #@0
    .prologue
    .line 925
    move-object v0, p1

    #@1
    check-cast v0, Landroid/os/IBinder;

    #@3
    .line 926
    .local v0, asBinder:Landroid/os/IBinder;
    iget-object v3, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@5
    monitor-enter v3

    #@6
    .line 927
    :try_start_6
    iget-object v2, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@8
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    check-cast v1, Landroid/speech/tts/ITextToSpeechCallback;

    #@e
    .line 928
    .local v1, cb:Landroid/speech/tts/ITextToSpeechCallback;
    monitor-exit v3

    #@f
    .line 930
    return-object v1

    #@10
    .line 928
    .end local v1           #cb:Landroid/speech/tts/ITextToSpeechCallback;
    :catchall_10
    move-exception v2

    #@11
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_10

    #@12
    throw v2
.end method


# virtual methods
.method public dispatchOnDone(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 8
    .parameter "callerIdentity"
    .parameter "utteranceId"

    #@0
    .prologue
    .line 876
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->getCallbackFor(Ljava/lang/Object;)Landroid/speech/tts/ITextToSpeechCallback;

    #@3
    move-result-object v0

    #@4
    .line 877
    .local v0, cb:Landroid/speech/tts/ITextToSpeechCallback;
    if-nez v0, :cond_7

    #@6
    .line 883
    :goto_6
    return-void

    #@7
    .line 879
    :cond_7
    :try_start_7
    invoke-interface {v0, p2}, Landroid/speech/tts/ITextToSpeechCallback;->onDone(Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_6

    #@b
    .line 880
    :catch_b
    move-exception v1

    #@c
    .line 881
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "TextToSpeechService"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Callback onDone failed: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_6
.end method

.method public dispatchOnError(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 8
    .parameter "callerIdentity"
    .parameter "utteranceId"

    #@0
    .prologue
    .line 897
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->getCallbackFor(Ljava/lang/Object;)Landroid/speech/tts/ITextToSpeechCallback;

    #@3
    move-result-object v0

    #@4
    .line 898
    .local v0, cb:Landroid/speech/tts/ITextToSpeechCallback;
    if-nez v0, :cond_7

    #@6
    .line 904
    :goto_6
    return-void

    #@7
    .line 900
    :cond_7
    :try_start_7
    invoke-interface {v0, p2}, Landroid/speech/tts/ITextToSpeechCallback;->onError(Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_6

    #@b
    .line 901
    :catch_b
    move-exception v1

    #@c
    .line 902
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "TextToSpeechService"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Callback onError failed: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_6
.end method

.method public dispatchOnStart(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 8
    .parameter "callerIdentity"
    .parameter "utteranceId"

    #@0
    .prologue
    .line 886
    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->getCallbackFor(Ljava/lang/Object;)Landroid/speech/tts/ITextToSpeechCallback;

    #@3
    move-result-object v0

    #@4
    .line 887
    .local v0, cb:Landroid/speech/tts/ITextToSpeechCallback;
    if-nez v0, :cond_7

    #@6
    .line 894
    :goto_6
    return-void

    #@7
    .line 889
    :cond_7
    :try_start_7
    invoke-interface {v0, p2}, Landroid/speech/tts/ITextToSpeechCallback;->onStart(Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_b

    #@a
    goto :goto_6

    #@b
    .line 890
    :catch_b
    move-exception v1

    #@c
    .line 891
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "TextToSpeechService"

    #@e
    new-instance v3, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v4, "Callback onStart failed: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_6
.end method

.method public kill()V
    .registers 3

    #@0
    .prologue
    .line 917
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 918
    :try_start_3
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@8
    .line 919
    invoke-super {p0}, Landroid/os/RemoteCallbackList;->kill()V

    #@b
    .line 920
    monitor-exit v1

    #@c
    .line 921
    return-void

    #@d
    .line 920
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 856
    check-cast p1, Landroid/speech/tts/ITextToSpeechCallback;

    #@2
    .end local p1
    invoke-virtual {p0, p1, p2}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->onCallbackDied(Landroid/speech/tts/ITextToSpeechCallback;Ljava/lang/Object;)V

    #@5
    return-void
.end method

.method public onCallbackDied(Landroid/speech/tts/ITextToSpeechCallback;Ljava/lang/Object;)V
    .registers 6
    .parameter "callback"
    .parameter "cookie"

    #@0
    .prologue
    .line 908
    move-object v0, p2

    #@1
    check-cast v0, Landroid/os/IBinder;

    #@3
    .line 909
    .local v0, caller:Landroid/os/IBinder;
    iget-object v2, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@5
    monitor-enter v2

    #@6
    .line 910
    :try_start_6
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@8
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    .line 911
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_6 .. :try_end_c} :catchall_16

    #@c
    .line 912
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->this$0:Landroid/speech/tts/TextToSpeechService;

    #@e
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->access$700(Landroid/speech/tts/TextToSpeechService;)Landroid/speech/tts/TextToSpeechService$SynthHandler;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, v0}, Landroid/speech/tts/TextToSpeechService$SynthHandler;->stopForApp(Ljava/lang/Object;)I

    #@15
    .line 913
    return-void

    #@16
    .line 911
    :catchall_16
    move-exception v1

    #@17
    :try_start_17
    monitor-exit v2
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v1
.end method

.method public setCallback(Landroid/os/IBinder;Landroid/speech/tts/ITextToSpeechCallback;)V
    .registers 6
    .parameter "caller"
    .parameter "cb"

    #@0
    .prologue
    .line 861
    iget-object v2, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 863
    if-eqz p2, :cond_19

    #@5
    .line 864
    :try_start_5
    invoke-virtual {p0, p2, p1}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    #@8
    .line 865
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@a
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v0

    #@e
    check-cast v0, Landroid/speech/tts/ITextToSpeechCallback;

    #@10
    .line 869
    .local v0, old:Landroid/speech/tts/ITextToSpeechCallback;
    :goto_10
    if-eqz v0, :cond_17

    #@12
    if-eq v0, p2, :cond_17

    #@14
    .line 870
    invoke-virtual {p0, v0}, Landroid/speech/tts/TextToSpeechService$CallbackMap;->unregister(Landroid/os/IInterface;)Z

    #@17
    .line 872
    :cond_17
    monitor-exit v2

    #@18
    .line 873
    return-void

    #@19
    .line 867
    .end local v0           #old:Landroid/speech/tts/ITextToSpeechCallback;
    :cond_19
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$CallbackMap;->mCallerToCallback:Ljava/util/HashMap;

    #@1b
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/speech/tts/ITextToSpeechCallback;

    #@21
    .restart local v0       #old:Landroid/speech/tts/ITextToSpeechCallback;
    goto :goto_10

    #@22
    .line 872
    .end local v0           #old:Landroid/speech/tts/ITextToSpeechCallback;
    :catchall_22
    move-exception v1

    #@23
    monitor-exit v2
    :try_end_24
    .catchall {:try_start_5 .. :try_end_24} :catchall_22

    #@24
    throw v1
.end method
