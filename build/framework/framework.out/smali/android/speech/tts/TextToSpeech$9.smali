.class Landroid/speech/tts/TextToSpeech$9;
.super Ljava/lang/Object;
.source "TextToSpeech.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/speech/tts/TextToSpeech$Action",
        "<",
        "Ljava/util/Locale;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/speech/tts/TextToSpeech;


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeech;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1059
    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$9;->this$0:Landroid/speech/tts/TextToSpeech;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Landroid/speech/tts/ITextToSpeechService;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1059
    invoke-virtual {p0, p1}, Landroid/speech/tts/TextToSpeech$9;->run(Landroid/speech/tts/ITextToSpeechService;)Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public run(Landroid/speech/tts/ITextToSpeechService;)Ljava/util/Locale;
    .registers 7
    .parameter "service"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1062
    invoke-interface {p1}, Landroid/speech/tts/ITextToSpeechService;->getLanguage()[Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 1063
    .local v0, locStrings:[Ljava/lang/String;
    if-eqz v0, :cond_19

    #@6
    array-length v1, v0

    #@7
    const/4 v2, 0x3

    #@8
    if-ne v1, v2, :cond_19

    #@a
    .line 1064
    new-instance v1, Ljava/util/Locale;

    #@c
    const/4 v2, 0x0

    #@d
    aget-object v2, v0, v2

    #@f
    const/4 v3, 0x1

    #@10
    aget-object v3, v0, v3

    #@12
    const/4 v4, 0x2

    #@13
    aget-object v4, v0, v4

    #@15
    invoke-direct {v1, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1066
    :goto_18
    return-object v1

    #@19
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_18
.end method
