.class Landroid/speech/tts/BlockingAudioTrack;
.super Ljava/lang/Object;
.source "BlockingAudioTrack.java"


# static fields
.field private static final DBG:Z = false

.field private static final MAX_PROGRESS_WAIT_MS:J = 0x9c4L

.field private static final MAX_SLEEP_TIME_MS:J = 0x9c4L

.field private static final MIN_AUDIO_BUFFER_SIZE:I = 0x2000

.field private static final MIN_SLEEP_TIME_MS:J = 0x14L

.field private static final TAG:Ljava/lang/String; = "TTS.BlockingAudioTrack"


# instance fields
.field private mAudioBufferSize:I

.field private final mAudioFormat:I

.field private mAudioTrack:Landroid/media/AudioTrack;

.field private mAudioTrackLock:Ljava/lang/Object;

.field private final mBytesPerFrame:I

.field private mBytesWritten:I

.field private final mChannelCount:I

.field private mIsShortUtterance:Z

.field private final mPan:F

.field private final mSampleRateInHz:I

.field private volatile mStopped:Z

.field private final mStreamType:I

.field private final mVolume:F


# direct methods
.method constructor <init>(IIIIFF)V
    .registers 10
    .parameter "streamType"
    .parameter "sampleRate"
    .parameter "audioFormat"
    .parameter "channelCount"
    .parameter "volume"
    .parameter "pan"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 78
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 68
    iput v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@6
    .line 72
    new-instance v0, Ljava/lang/Object;

    #@8
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@b
    iput-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@d
    .line 79
    iput p1, p0, Landroid/speech/tts/BlockingAudioTrack;->mStreamType:I

    #@f
    .line 80
    iput p2, p0, Landroid/speech/tts/BlockingAudioTrack;->mSampleRateInHz:I

    #@11
    .line 81
    iput p3, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioFormat:I

    #@13
    .line 82
    iput p4, p0, Landroid/speech/tts/BlockingAudioTrack;->mChannelCount:I

    #@15
    .line 83
    iput p5, p0, Landroid/speech/tts/BlockingAudioTrack;->mVolume:F

    #@17
    .line 84
    iput p6, p0, Landroid/speech/tts/BlockingAudioTrack;->mPan:F

    #@19
    .line 86
    iget v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioFormat:I

    #@1b
    invoke-static {v0}, Landroid/speech/tts/BlockingAudioTrack;->getBytesPerFrame(I)I

    #@1e
    move-result v0

    #@1f
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mChannelCount:I

    #@21
    mul-int/2addr v0, v1

    #@22
    iput v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesPerFrame:I

    #@24
    .line 87
    iput-boolean v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mIsShortUtterance:Z

    #@26
    .line 88
    iput v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioBufferSize:I

    #@28
    .line 89
    iput v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@2a
    .line 91
    const/4 v0, 0x0

    #@2b
    iput-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@2d
    .line 92
    iput-boolean v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@2f
    .line 93
    return-void
.end method

.method private blockUntilCompletion(Landroid/media/AudioTrack;)V
    .registers 16
    .parameter "audioTrack"

    #@0
    .prologue
    const-wide/16 v4, 0x9c4

    #@2
    .line 280
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@4
    iget v3, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesPerFrame:I

    #@6
    div-int v10, v2, v3

    #@8
    .line 282
    .local v10, lengthInFrames:I
    const/4 v11, -0x1

    #@9
    .line 283
    .local v11, previousPosition:I
    const/4 v8, 0x0

    #@a
    .line 284
    .local v8, currentPosition:I
    const-wide/16 v6, 0x0

    #@c
    .line 287
    .local v6, blockedTimeMs:J
    :goto_c
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    #@f
    move-result v8

    #@10
    if-ge v8, v10, :cond_3b

    #@12
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getPlayState()I

    #@15
    move-result v2

    #@16
    const/4 v3, 0x3

    #@17
    if-ne v2, v3, :cond_3b

    #@19
    iget-boolean v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@1b
    if-nez v2, :cond_3b

    #@1d
    .line 289
    sub-int v2, v10, v8

    #@1f
    mul-int/lit16 v2, v2, 0x3e8

    #@21
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getSampleRate()I

    #@24
    move-result v3

    #@25
    div-int/2addr v2, v3

    #@26
    int-to-long v0, v2

    #@27
    .line 291
    .local v0, estimatedTimeMs:J
    const-wide/16 v2, 0x14

    #@29
    invoke-static/range {v0 .. v5}, Landroid/speech/tts/BlockingAudioTrack;->clip(JJJ)J

    #@2c
    move-result-wide v12

    #@2d
    .line 296
    .local v12, sleepTimeMs:J
    if-ne v8, v11, :cond_3c

    #@2f
    .line 299
    add-long/2addr v6, v12

    #@30
    .line 301
    cmp-long v2, v6, v4

    #@32
    if-lez v2, :cond_3e

    #@34
    .line 302
    const-string v2, "TTS.BlockingAudioTrack"

    #@36
    const-string v3, "Waited unsuccessfully for 2500ms for AudioTrack to make progress, Aborting"

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 322
    .end local v0           #estimatedTimeMs:J
    .end local v12           #sleepTimeMs:J
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 307
    .restart local v0       #estimatedTimeMs:J
    .restart local v12       #sleepTimeMs:J
    :cond_3c
    const-wide/16 v6, 0x0

    #@3e
    .line 309
    :cond_3e
    move v11, v8

    #@3f
    .line 317
    :try_start_3f
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V
    :try_end_42
    .catch Ljava/lang/InterruptedException; {:try_start_3f .. :try_end_42} :catch_43

    #@42
    goto :goto_c

    #@43
    .line 318
    :catch_43
    move-exception v9

    #@44
    .line 319
    .local v9, ie:Ljava/lang/InterruptedException;
    goto :goto_3b
.end method

.method private blockUntilDone(Landroid/media/AudioTrack;)V
    .registers 3
    .parameter "audioTrack"

    #@0
    .prologue
    .line 244
    iget v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@2
    if-gtz v0, :cond_5

    #@4
    .line 264
    :goto_4
    return-void

    #@5
    .line 248
    :cond_5
    iget-boolean v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mIsShortUtterance:Z

    #@7
    if-eqz v0, :cond_d

    #@9
    .line 260
    invoke-direct {p0}, Landroid/speech/tts/BlockingAudioTrack;->blockUntilEstimatedCompletion()V

    #@c
    goto :goto_4

    #@d
    .line 262
    :cond_d
    invoke-direct {p0, p1}, Landroid/speech/tts/BlockingAudioTrack;->blockUntilCompletion(Landroid/media/AudioTrack;)V

    #@10
    goto :goto_4
.end method

.method private blockUntilEstimatedCompletion()V
    .registers 6

    #@0
    .prologue
    .line 267
    iget v3, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@2
    iget v4, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesPerFrame:I

    #@4
    div-int v2, v3, v4

    #@6
    .line 268
    .local v2, lengthInFrames:I
    mul-int/lit16 v3, v2, 0x3e8

    #@8
    iget v4, p0, Landroid/speech/tts/BlockingAudioTrack;->mSampleRateInHz:I

    #@a
    div-int/2addr v3, v4

    #@b
    int-to-long v0, v3

    #@c
    .line 273
    .local v0, estimatedTimeMs:J
    :try_start_c
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_f} :catch_10

    #@f
    .line 277
    :goto_f
    return-void

    #@10
    .line 274
    :catch_10
    move-exception v3

    #@11
    goto :goto_f
.end method

.method private static clip(FFF)F
    .registers 4
    .parameter "value"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 354
    cmpl-float v0, p0, p2

    #@2
    if-lez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    cmpg-float v0, p0, p1

    #@7
    if-gez v0, :cond_b

    #@9
    move p2, p1

    #@a
    goto :goto_4

    #@b
    :cond_b
    move p2, p0

    #@c
    goto :goto_4
.end method

.method private static final clip(JJJ)J
    .registers 7
    .parameter "value"
    .parameter "min"
    .parameter "max"

    #@0
    .prologue
    .line 342
    cmp-long v0, p0, p2

    #@2
    if-gez v0, :cond_5

    #@4
    .line 350
    .end local p2
    :goto_4
    return-wide p2

    #@5
    .line 346
    .restart local p2
    :cond_5
    cmp-long v0, p0, p4

    #@7
    if-lez v0, :cond_b

    #@9
    move-wide p2, p4

    #@a
    .line 347
    goto :goto_4

    #@b
    :cond_b
    move-wide p2, p0

    #@c
    .line 350
    goto :goto_4
.end method

.method private createStreamingAudioTrack()Landroid/media/AudioTrack;
    .registers 9

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 212
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mChannelCount:I

    #@3
    invoke-static {v1}, Landroid/speech/tts/BlockingAudioTrack;->getChannelConfig(I)I

    #@6
    move-result v3

    #@7
    .line 214
    .local v3, channelConfig:I
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mSampleRateInHz:I

    #@9
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioFormat:I

    #@b
    invoke-static {v1, v3, v2}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    #@e
    move-result v7

    #@f
    .line 216
    .local v7, minBufferSizeInBytes:I
    const/16 v1, 0x2000

    #@11
    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    #@14
    move-result v5

    #@15
    .line 218
    .local v5, bufferSizeInBytes:I
    new-instance v0, Landroid/media/AudioTrack;

    #@17
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mStreamType:I

    #@19
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mSampleRateInHz:I

    #@1b
    iget v4, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioFormat:I

    #@1d
    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    #@20
    .line 220
    .local v0, audioTrack:Landroid/media/AudioTrack;
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    #@23
    move-result v1

    #@24
    if-eq v1, v6, :cond_32

    #@26
    .line 221
    const-string v1, "TTS.BlockingAudioTrack"

    #@28
    const-string v2, "Unable to create audio track."

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 222
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    #@30
    .line 223
    const/4 v0, 0x0

    #@31
    .line 229
    .end local v0           #audioTrack:Landroid/media/AudioTrack;
    :goto_31
    return-object v0

    #@32
    .line 226
    .restart local v0       #audioTrack:Landroid/media/AudioTrack;
    :cond_32
    iput v5, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioBufferSize:I

    #@34
    .line 228
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mVolume:F

    #@36
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mPan:F

    #@38
    invoke-static {v0, v1, v2}, Landroid/speech/tts/BlockingAudioTrack;->setupVolume(Landroid/media/AudioTrack;FF)V

    #@3b
    goto :goto_31
.end method

.method private static getBytesPerFrame(I)I
    .registers 3
    .parameter "audioFormat"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 233
    const/4 v1, 0x3

    #@2
    if-ne p0, v1, :cond_6

    #@4
    .line 234
    const/4 v0, 0x1

    #@5
    .line 239
    :cond_5
    :goto_5
    return v0

    #@6
    .line 235
    :cond_6
    if-eq p0, v0, :cond_5

    #@8
    .line 239
    const/4 v0, -0x1

    #@9
    goto :goto_5
.end method

.method static getChannelConfig(I)I
    .registers 2
    .parameter "channelCount"

    #@0
    .prologue
    .line 176
    const/4 v0, 0x1

    #@1
    if-ne p0, v0, :cond_5

    #@3
    .line 177
    const/4 v0, 0x4

    #@4
    .line 182
    :goto_4
    return v0

    #@5
    .line 178
    :cond_5
    const/4 v0, 0x2

    #@6
    if-ne p0, v0, :cond_b

    #@8
    .line 179
    const/16 v0, 0xc

    #@a
    goto :goto_4

    #@b
    .line 182
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_4
.end method

.method private static setupVolume(Landroid/media/AudioTrack;FF)V
    .registers 10
    .parameter "audioTrack"
    .parameter "volume"
    .parameter "pan"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/high16 v5, 0x3f80

    #@3
    .line 325
    invoke-static {p1, v6, v5}, Landroid/speech/tts/BlockingAudioTrack;->clip(FFF)F

    #@6
    move-result v1

    #@7
    .line 326
    .local v1, vol:F
    const/high16 v4, -0x4080

    #@9
    invoke-static {p2, v4, v5}, Landroid/speech/tts/BlockingAudioTrack;->clip(FFF)F

    #@c
    move-result v0

    #@d
    .line 328
    .local v0, panning:F
    move v2, v1

    #@e
    .line 329
    .local v2, volLeft:F
    move v3, v1

    #@f
    .line 330
    .local v3, volRight:F
    cmpl-float v4, v0, v6

    #@11
    if-lez v4, :cond_24

    #@13
    .line 331
    sub-float v4, v5, v0

    #@15
    mul-float/2addr v2, v4

    #@16
    .line 336
    :cond_16
    :goto_16
    invoke-virtual {p0, v2, v3}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    #@19
    move-result v4

    #@1a
    if-eqz v4, :cond_23

    #@1c
    .line 337
    const-string v4, "TTS.BlockingAudioTrack"

    #@1e
    const-string v5, "Failed to set volume"

    #@20
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 339
    :cond_23
    return-void

    #@24
    .line 332
    :cond_24
    cmpg-float v4, v0, v6

    #@26
    if-gez v4, :cond_16

    #@28
    .line 333
    add-float v4, v5, v0

    #@2a
    mul-float/2addr v3, v4

    #@2b
    goto :goto_16
.end method

.method private static writeToAudioTrack(Landroid/media/AudioTrack;[B)I
    .registers 6
    .parameter "audioTrack"
    .parameter "bytes"

    #@0
    .prologue
    .line 193
    invoke-virtual {p0}, Landroid/media/AudioTrack;->getPlayState()I

    #@3
    move-result v2

    #@4
    const/4 v3, 0x3

    #@5
    if-eq v2, v3, :cond_a

    #@7
    .line 195
    invoke-virtual {p0}, Landroid/media/AudioTrack;->play()V

    #@a
    .line 198
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 199
    .local v0, count:I
    :goto_b
    array-length v2, p1

    #@c
    if-ge v0, v2, :cond_15

    #@e
    .line 202
    array-length v2, p1

    #@f
    invoke-virtual {p0, p1, v0, v2}, Landroid/media/AudioTrack;->write([BII)I

    #@12
    move-result v1

    #@13
    .line 203
    .local v1, written:I
    if-gtz v1, :cond_16

    #@15
    .line 208
    .end local v1           #written:I
    :cond_15
    return v0

    #@16
    .line 206
    .restart local v1       #written:I
    :cond_16
    add-int/2addr v0, v1

    #@17
    .line 207
    goto :goto_b
.end method


# virtual methods
.method getAudioLengthMs(I)J
    .registers 7
    .parameter "numBytes"

    #@0
    .prologue
    .line 186
    iget v3, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesPerFrame:I

    #@2
    div-int v2, p1, v3

    #@4
    .line 187
    .local v2, unconsumedFrames:I
    mul-int/lit16 v3, v2, 0x3e8

    #@6
    iget v4, p0, Landroid/speech/tts/BlockingAudioTrack;->mSampleRateInHz:I

    #@8
    div-int/2addr v3, v4

    #@9
    int-to-long v0, v3

    #@a
    .line 189
    .local v0, estimatedTimeMs:J
    return-wide v0
.end method

.method public init()Z
    .registers 4

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Landroid/speech/tts/BlockingAudioTrack;->createStreamingAudioTrack()Landroid/media/AudioTrack;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, track:Landroid/media/AudioTrack;
    iget-object v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@6
    monitor-enter v2

    #@7
    .line 98
    :try_start_7
    iput-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@9
    .line 99
    monitor-exit v2

    #@a
    .line 101
    if-nez v0, :cond_11

    #@c
    .line 102
    const/4 v1, 0x0

    #@d
    .line 104
    :goto_d
    return v1

    #@e
    .line 99
    :catchall_e
    move-exception v1

    #@f
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_e

    #@10
    throw v1

    #@11
    .line 104
    :cond_11
    const/4 v1, 0x1

    #@12
    goto :goto_d
.end method

.method public stop()V
    .registers 3

    #@0
    .prologue
    .line 109
    iget-object v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 110
    :try_start_3
    iget-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@5
    if-eqz v0, :cond_c

    #@7
    .line 111
    iget-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@9
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    #@c
    .line 113
    :cond_c
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@f
    .line 114
    monitor-exit v1

    #@10
    .line 115
    return-void

    #@11
    .line 114
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method public waitAndRelease()V
    .registers 4

    #@0
    .prologue
    .line 133
    const/4 v0, 0x0

    #@1
    .line 134
    .local v0, track:Landroid/media/AudioTrack;
    iget-object v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 135
    :try_start_4
    iget-object v0, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@6
    .line 136
    monitor-exit v2

    #@7
    .line 137
    if-nez v0, :cond_d

    #@9
    .line 172
    :goto_9
    return-void

    #@a
    .line 136
    :catchall_a
    move-exception v1

    #@b
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_4 .. :try_end_c} :catchall_a

    #@c
    throw v1

    #@d
    .line 148
    :cond_d
    iget v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@f
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioBufferSize:I

    #@11
    if-ge v1, v2, :cond_1d

    #@13
    iget-boolean v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@15
    if-nez v1, :cond_1d

    #@17
    .line 154
    const/4 v1, 0x1

    #@18
    iput-boolean v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mIsShortUtterance:Z

    #@1a
    .line 155
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    #@1d
    .line 159
    :cond_1d
    iget-boolean v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@1f
    if-nez v1, :cond_26

    #@21
    .line 161
    iget-object v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@23
    invoke-direct {p0, v1}, Landroid/speech/tts/BlockingAudioTrack;->blockUntilDone(Landroid/media/AudioTrack;)V

    #@26
    .line 168
    :cond_26
    iget-object v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@28
    monitor-enter v2

    #@29
    .line 169
    const/4 v1, 0x0

    #@2a
    :try_start_2a
    iput-object v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@2c
    .line 170
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_31

    #@2d
    .line 171
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    #@30
    goto :goto_9

    #@31
    .line 170
    :catchall_31
    move-exception v1

    #@32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    #@33
    throw v1
.end method

.method public write([B)I
    .registers 6
    .parameter "data"

    #@0
    .prologue
    .line 118
    const/4 v1, 0x0

    #@1
    .line 119
    .local v1, track:Landroid/media/AudioTrack;
    iget-object v3, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrackLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 120
    :try_start_4
    iget-object v1, p0, Landroid/speech/tts/BlockingAudioTrack;->mAudioTrack:Landroid/media/AudioTrack;

    #@6
    .line 121
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_f

    #@7
    .line 123
    if-eqz v1, :cond_d

    #@9
    iget-boolean v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mStopped:Z

    #@b
    if-eqz v2, :cond_12

    #@d
    .line 124
    :cond_d
    const/4 v0, -0x1

    #@e
    .line 129
    :goto_e
    return v0

    #@f
    .line 121
    :catchall_f
    move-exception v2

    #@10
    :try_start_10
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    #@11
    throw v2

    #@12
    .line 126
    :cond_12
    invoke-static {v1, p1}, Landroid/speech/tts/BlockingAudioTrack;->writeToAudioTrack(Landroid/media/AudioTrack;[B)I

    #@15
    move-result v0

    #@16
    .line 128
    .local v0, bytesWritten:I
    iget v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@18
    add-int/2addr v2, v0

    #@19
    iput v2, p0, Landroid/speech/tts/BlockingAudioTrack;->mBytesWritten:I

    #@1b
    goto :goto_e
.end method
