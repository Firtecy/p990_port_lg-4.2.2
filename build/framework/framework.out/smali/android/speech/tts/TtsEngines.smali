.class public Landroid/speech/tts/TtsEngines;
.super Ljava/lang/Object;
.source "TtsEngines.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/tts/TtsEngines$EngineInfoComparator;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final LOCALE_DELIMITER:Ljava/lang/String; = "-"

.field private static final TAG:Ljava/lang/String; = "TtsEngines"

.field private static final XML_TAG_NAME:Ljava/lang/String; = "tts-engine"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "ctx"

    #@0
    .prologue
    .line 65
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    iput-object p1, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@5
    .line 67
    return-void
.end method

.method private getDefaultLocale()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 359
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v1

    #@4
    .line 365
    .local v1, locale:Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 366
    .local v0, defaultLocale:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_18

    #@e
    .line 367
    const-string v2, "TtsEngines"

    #@10
    const-string v3, "Default locale is empty."

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 368
    const-string v2, ""

    #@17
    .line 382
    :goto_17
    return-object v2

    #@18
    .line 371
    :cond_18
    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1f
    move-result v2

    #@20
    if-nez v2, :cond_64

    #@22
    .line 372
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, "-"

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    .line 378
    invoke-virtual {v1}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@44
    move-result v2

    #@45
    if-nez v2, :cond_62

    #@47
    .line 379
    new-instance v2, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "-"

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v1}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    :cond_62
    move-object v2, v0

    #@63
    .line 382
    goto :goto_17

    #@64
    :cond_64
    move-object v2, v0

    #@65
    .line 376
    goto :goto_17
.end method

.method private getEngineInfo(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/speech/tts/TextToSpeech$EngineInfo;
    .registers 7
    .parameter "resolve"
    .parameter "pm"

    #@0
    .prologue
    .line 239
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@2
    .line 240
    .local v2, service:Landroid/content/pm/ServiceInfo;
    if-eqz v2, :cond_31

    #@4
    .line 241
    new-instance v0, Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@6
    invoke-direct {v0}, Landroid/speech/tts/TextToSpeech$EngineInfo;-><init>()V

    #@9
    .line 244
    .local v0, engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    iget-object v3, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@b
    iput-object v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    #@d
    .line 245
    invoke-virtual {v2, p2}, Landroid/content/pm/ServiceInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    #@10
    move-result-object v1

    #@11
    .line 246
    .local v1, label:Ljava/lang/CharSequence;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_2c

    #@17
    iget-object v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    #@19
    :goto_19
    iput-object v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->label:Ljava/lang/String;

    #@1b
    .line 247
    invoke-virtual {v2}, Landroid/content/pm/ServiceInfo;->getIconResource()I

    #@1e
    move-result v3

    #@1f
    iput v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->icon:I

    #@21
    .line 248
    iget v3, p1, Landroid/content/pm/ResolveInfo;->priority:I

    #@23
    iput v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->priority:I

    #@25
    .line 249
    invoke-direct {p0, v2}, Landroid/speech/tts/TtsEngines;->isSystemEngine(Landroid/content/pm/ServiceInfo;)Z

    #@28
    move-result v3

    #@29
    iput-boolean v3, v0, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@2b
    .line 253
    .end local v0           #engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    .end local v1           #label:Ljava/lang/CharSequence;
    :goto_2b
    return-object v0

    #@2c
    .line 246
    .restart local v0       #engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    .restart local v1       #label:Ljava/lang/CharSequence;
    :cond_2c
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    goto :goto_19

    #@31
    .line 253
    .end local v0           #engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    .end local v1           #label:Ljava/lang/CharSequence;
    :cond_31
    const/4 v0, 0x0

    #@32
    goto :goto_2b
.end method

.method private getV1Locale()Ljava/lang/String;
    .registers 8

    #@0
    .prologue
    .line 334
    iget-object v5, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    .line 336
    .local v1, cr:Landroid/content/ContentResolver;
    const-string/jumbo v5, "tts_default_lang"

    #@9
    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    .line 337
    .local v2, lang:Ljava/lang/String;
    const-string/jumbo v5, "tts_default_country"

    #@10
    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 338
    .local v0, country:Ljava/lang/String;
    const-string/jumbo v5, "tts_default_variant"

    #@17
    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    .line 340
    .local v4, variant:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1e
    move-result v5

    #@1f
    if-eqz v5, :cond_26

    #@21
    .line 341
    invoke-direct {p0}, Landroid/speech/tts/TtsEngines;->getDefaultLocale()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    .line 355
    :cond_25
    :goto_25
    return-object v3

    #@26
    .line 344
    :cond_26
    move-object v3, v2

    #@27
    .line 345
    .local v3, v1Locale:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2a
    move-result v5

    #@2b
    if-nez v5, :cond_25

    #@2d
    .line 346
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    const-string v6, "-"

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v3

    #@44
    .line 351
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@47
    move-result v5

    #@48
    if-nez v5, :cond_25

    #@4a
    .line 352
    new-instance v5, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    const-string v6, "-"

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    goto :goto_25
.end method

.method private isSystemEngine(Landroid/content/pm/ServiceInfo;)Z
    .registers 4
    .parameter "info"

    #@0
    .prologue
    .line 140
    iget-object v0, p1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2
    .line 141
    .local v0, appInfo:Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_c

    #@4
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    #@6
    and-int/lit8 v1, v1, 0x1

    #@8
    if-eqz v1, :cond_c

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method private static parseEnginePrefFromList(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "prefValue"
    .parameter "engineName"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 392
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4
    move-result v7

    #@5
    if-eqz v7, :cond_8

    #@7
    .line 407
    :cond_7
    :goto_7
    return-object v6

    #@8
    .line 396
    :cond_8
    const-string v7, ","

    #@a
    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    .line 398
    .local v4, prefValues:[Ljava/lang/String;
    move-object v0, v4

    #@f
    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    #@10
    .local v3, len$:I
    const/4 v2, 0x0

    #@11
    .local v2, i$:I
    :goto_11
    if-ge v2, v3, :cond_7

    #@13
    aget-object v5, v0, v2

    #@15
    .line 399
    .local v5, value:Ljava/lang/String;
    const/16 v7, 0x3a

    #@17
    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(I)I

    #@1a
    move-result v1

    #@1b
    .line 400
    .local v1, delimiter:I
    if-lez v1, :cond_2f

    #@1d
    .line 401
    const/4 v7, 0x0

    #@1e
    invoke-virtual {v5, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_2f

    #@28
    .line 402
    add-int/lit8 v6, v1, 0x1

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    goto :goto_7

    #@2f
    .line 398
    :cond_2f
    add-int/lit8 v2, v2, 0x1

    #@31
    goto :goto_11
.end method

.method public static parseLocalePref(Ljava/lang/String;)[Ljava/lang/String;
    .registers 6
    .parameter "pref"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 314
    const/4 v2, 0x3

    #@2
    new-array v0, v2, [Ljava/lang/String;

    #@4
    const-string v2, ""

    #@6
    aput-object v2, v0, v4

    #@8
    const/4 v2, 0x1

    #@9
    const-string v3, ""

    #@b
    aput-object v3, v0, v2

    #@d
    const/4 v2, 0x2

    #@e
    const-string v3, ""

    #@10
    aput-object v3, v0, v2

    #@12
    .line 315
    .local v0, returnVal:[Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@15
    move-result v2

    #@16
    if-nez v2, :cond_22

    #@18
    .line 316
    const-string v2, "-"

    #@1a
    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    .line 317
    .local v1, split:[Ljava/lang/String;
    array-length v2, v1

    #@1f
    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@22
    .line 323
    .end local v1           #split:[Ljava/lang/String;
    :cond_22
    return-object v0
.end method

.method private settingsActivityFromServiceInfo(Landroid/content/pm/ServiceInfo;Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .registers 14
    .parameter "si"
    .parameter "pm"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 191
    const/4 v3, 0x0

    #@2
    .line 193
    .local v3, parser:Landroid/content/res/XmlResourceParser;
    :try_start_2
    const-string v8, "android.speech.tts"

    #@4
    invoke-virtual {p1, p2, v8}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    #@7
    move-result-object v3

    #@8
    .line 194
    if-nez v3, :cond_29

    #@a
    .line 195
    const-string v8, "TtsEngines"

    #@c
    new-instance v9, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v10, "No meta-data found for :"

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v9

    #@1b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v9

    #@1f
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22
    .catchall {:try_start_2 .. :try_end_22} :catchall_109
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_22} :catch_92
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_22} :catch_b3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_22} :catch_de

    #@22
    .line 232
    if-eqz v3, :cond_27

    #@24
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@27
    :cond_27
    move-object v5, v7

    #@28
    :cond_28
    :goto_28
    return-object v5

    #@29
    .line 199
    :cond_29
    :try_start_29
    iget-object v8, p1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@2b
    invoke-virtual {p2, v8}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@2e
    move-result-object v4

    #@2f
    .line 202
    .local v4, res:Landroid/content/res/Resources;
    :cond_2f
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->next()I

    #@32
    move-result v6

    #@33
    .local v6, type:I
    const/4 v8, 0x1

    #@34
    if-eq v6, v8, :cond_8b

    #@36
    .line 203
    const/4 v8, 0x2

    #@37
    if-ne v6, v8, :cond_2f

    #@39
    .line 204
    const-string/jumbo v8, "tts-engine"

    #@3c
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v8

    #@44
    if-nez v8, :cond_73

    #@46
    .line 205
    const-string v8, "TtsEngines"

    #@48
    new-instance v9, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v10, "Package "

    #@4f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v9

    #@57
    const-string v10, " uses unknown tag :"

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@60
    move-result-object v10

    #@61
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v9

    #@69
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6c
    .catchall {:try_start_29 .. :try_end_6c} :catchall_109
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_29 .. :try_end_6c} :catch_92
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_29 .. :try_end_6c} :catch_b3
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_6c} :catch_de

    #@6c
    .line 232
    if-eqz v3, :cond_71

    #@6e
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@71
    :cond_71
    move-object v5, v7

    #@72
    goto :goto_28

    #@73
    .line 210
    :cond_73
    :try_start_73
    invoke-static {v3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@76
    move-result-object v1

    #@77
    .line 211
    .local v1, attrs:Landroid/util/AttributeSet;
    sget-object v8, Lcom/android/internal/R$styleable;->TextToSpeechEngine:[I

    #@79
    invoke-virtual {v4, v1, v8}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@7c
    move-result-object v0

    #@7d
    .line 213
    .local v0, array:Landroid/content/res/TypedArray;
    const/4 v8, 0x0

    #@7e
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    .line 215
    .local v5, settings:Ljava/lang/String;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_85
    .catchall {:try_start_73 .. :try_end_85} :catchall_109
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_73 .. :try_end_85} :catch_92
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_73 .. :try_end_85} :catch_b3
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_85} :catch_de

    #@85
    .line 232
    if-eqz v3, :cond_28

    #@87
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@8a
    goto :goto_28

    #@8b
    .line 232
    .end local v0           #array:Landroid/content/res/TypedArray;
    .end local v1           #attrs:Landroid/util/AttributeSet;
    .end local v5           #settings:Ljava/lang/String;
    :cond_8b
    if-eqz v3, :cond_90

    #@8d
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@90
    :cond_90
    move-object v5, v7

    #@91
    goto :goto_28

    #@92
    .line 222
    .end local v4           #res:Landroid/content/res/Resources;
    .end local v6           #type:I
    :catch_92
    move-exception v2

    #@93
    .line 223
    .local v2, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_93
    const-string v8, "TtsEngines"

    #@95
    new-instance v9, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v10, "Could not load resources for : "

    #@9c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v9

    #@a0
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v9

    #@a8
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ab
    .catchall {:try_start_93 .. :try_end_ab} :catchall_109

    #@ab
    .line 232
    if-eqz v3, :cond_b0

    #@ad
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@b0
    :cond_b0
    move-object v5, v7

    #@b1
    goto/16 :goto_28

    #@b3
    .line 225
    .end local v2           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_b3
    move-exception v2

    #@b4
    .line 226
    .local v2, e:Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_b4
    const-string v8, "TtsEngines"

    #@b6
    new-instance v9, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v10, "Error parsing metadata for "

    #@bd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v9

    #@c1
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v9

    #@c5
    const-string v10, ":"

    #@c7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v9

    #@cb
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v9

    #@cf
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v9

    #@d3
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d6
    .catchall {:try_start_b4 .. :try_end_d6} :catchall_109

    #@d6
    .line 232
    if-eqz v3, :cond_db

    #@d8
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@db
    :cond_db
    move-object v5, v7

    #@dc
    goto/16 :goto_28

    #@de
    .line 228
    .end local v2           #e:Lorg/xmlpull/v1/XmlPullParserException;
    :catch_de
    move-exception v2

    #@df
    .line 229
    .local v2, e:Ljava/io/IOException;
    :try_start_df
    const-string v8, "TtsEngines"

    #@e1
    new-instance v9, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v10, "Error parsing metadata for "

    #@e8
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v9

    #@ec
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v9

    #@f0
    const-string v10, ":"

    #@f2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v9

    #@f6
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v9

    #@fa
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v9

    #@fe
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_101
    .catchall {:try_start_df .. :try_end_101} :catchall_109

    #@101
    .line 232
    if-eqz v3, :cond_106

    #@103
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@106
    :cond_106
    move-object v5, v7

    #@107
    goto/16 :goto_28

    #@109
    .line 232
    .end local v2           #e:Ljava/io/IOException;
    :catchall_109
    move-exception v7

    #@10a
    if-eqz v3, :cond_10f

    #@10c
    .line 233
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    #@10f
    :cond_10f
    throw v7
.end method

.method private updateValueInCommaSeparatedList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 16
    .parameter "list"
    .parameter "key"
    .parameter "newValue"

    #@0
    .prologue
    const/16 v11, 0x2c

    #@2
    const/16 v10, 0x3a

    #@4
    .line 434
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 435
    .local v6, newPrefList:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c
    move-result v9

    #@d
    if-eqz v9, :cond_1f

    #@f
    .line 437
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 475
    :cond_1a
    :goto_1a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v9

    #@1e
    return-object v9

    #@1f
    .line 439
    :cond_1f
    const-string v9, ","

    #@21
    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@24
    move-result-object v7

    #@25
    .line 441
    .local v7, prefValues:[Ljava/lang/String;
    const/4 v2, 0x1

    #@26
    .line 443
    .local v2, first:Z
    const/4 v3, 0x0

    #@27
    .line 444
    .local v3, found:Z
    move-object v0, v7

    #@28
    .local v0, arr$:[Ljava/lang/String;
    array-length v5, v0

    #@29
    .local v5, len$:I
    const/4 v4, 0x0

    #@2a
    .local v4, i$:I
    :goto_2a
    if-ge v4, v5, :cond_60

    #@2c
    aget-object v8, v0, v4

    #@2e
    .line 445
    .local v8, value:Ljava/lang/String;
    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(I)I

    #@31
    move-result v1

    #@32
    .line 446
    .local v1, delimiter:I
    if-lez v1, :cond_4e

    #@34
    .line 447
    const/4 v9, 0x0

    #@35
    invoke-virtual {v8, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v9

    #@3d
    if-eqz v9, :cond_55

    #@3f
    .line 448
    if-eqz v2, :cond_51

    #@41
    .line 449
    const/4 v2, 0x0

    #@42
    .line 453
    :goto_42
    const/4 v3, 0x1

    #@43
    .line 454
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v9

    #@47
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 444
    :cond_4e
    :goto_4e
    add-int/lit8 v4, v4, 0x1

    #@50
    goto :goto_2a

    #@51
    .line 451
    :cond_51
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@54
    goto :goto_42

    #@55
    .line 456
    :cond_55
    if-eqz v2, :cond_5c

    #@57
    .line 457
    const/4 v2, 0x0

    #@58
    .line 462
    :goto_58
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    goto :goto_4e

    #@5c
    .line 459
    :cond_5c
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@5f
    goto :goto_58

    #@60
    .line 467
    .end local v1           #delimiter:I
    .end local v8           #value:Ljava/lang/String;
    :cond_60
    if-nez v3, :cond_1a

    #@62
    .line 470
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@65
    .line 471
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v9

    #@69
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v9

    #@6d
    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    goto :goto_1a
.end method


# virtual methods
.method public getDefaultEngine()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 75
    iget-object v1, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "tts_default_synth"

    #@9
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    .line 77
    .local v0, engine:Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/speech/tts/TtsEngines;->isEngineInstalled(Ljava/lang/String;)Z

    #@10
    move-result v1

    #@11
    if-eqz v1, :cond_14

    #@13
    .end local v0           #engine:Ljava/lang/String;
    :goto_13
    return-object v0

    #@14
    .restart local v0       #engine:Ljava/lang/String;
    :cond_14
    invoke-virtual {p0}, Landroid/speech/tts/TtsEngines;->getHighestRankedEngineName()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    goto :goto_13
.end method

.method public getEngineInfo(Ljava/lang/String;)Landroid/speech/tts/TextToSpeech$EngineInfo;
    .registers 7
    .parameter "packageName"

    #@0
    .prologue
    .line 99
    iget-object v3, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v1

    #@6
    .line 100
    .local v1, pm:Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v3, "android.intent.action.TTS_SERVICE"

    #@a
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 101
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 102
    const/high16 v3, 0x1

    #@12
    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    #@15
    move-result-object v2

    #@16
    .line 107
    .local v2, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_2b

    #@18
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@1b
    move-result v3

    #@1c
    const/4 v4, 0x1

    #@1d
    if-ne v3, v4, :cond_2b

    #@1f
    .line 108
    const/4 v3, 0x0

    #@20
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@23
    move-result-object v3

    #@24
    check-cast v3, Landroid/content/pm/ResolveInfo;

    #@26
    invoke-direct {p0, v3, v1}, Landroid/speech/tts/TtsEngines;->getEngineInfo(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@29
    move-result-object v3

    #@2a
    .line 111
    :goto_2a
    return-object v3

    #@2b
    :cond_2b
    const/4 v3, 0x0

    #@2c
    goto :goto_2a
.end method

.method public getEngines()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/speech/tts/TextToSpeech$EngineInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 120
    iget-object v7, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v4

    #@6
    .line 121
    .local v4, pm:Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    #@8
    const-string v7, "android.intent.action.TTS_SERVICE"

    #@a
    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 122
    .local v3, intent:Landroid/content/Intent;
    const/high16 v7, 0x1

    #@f
    invoke-virtual {v4, v3, v7}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    #@12
    move-result-object v6

    #@13
    .line 124
    .local v6, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v6, :cond_1a

    #@15
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    #@18
    move-result-object v1

    #@19
    .line 136
    :goto_19
    return-object v1

    #@1a
    .line 126
    :cond_1a
    new-instance v1, Ljava/util/ArrayList;

    #@1c
    invoke-interface {v6}, Ljava/util/List;->size()I

    #@1f
    move-result v7

    #@20
    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    #@23
    .line 128
    .local v1, engines:Ljava/util/List;,"Ljava/util/List<Landroid/speech/tts/TextToSpeech$EngineInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v2

    #@27
    .local v2, i$:Ljava/util/Iterator;
    :cond_27
    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v7

    #@2b
    if-eqz v7, :cond_3d

    #@2d
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v5

    #@31
    check-cast v5, Landroid/content/pm/ResolveInfo;

    #@33
    .line 129
    .local v5, resolveInfo:Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v5, v4}, Landroid/speech/tts/TtsEngines;->getEngineInfo(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@36
    move-result-object v0

    #@37
    .line 130
    .local v0, engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    if-eqz v0, :cond_27

    #@39
    .line 131
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3c
    goto :goto_27

    #@3d
    .line 134
    .end local v0           #engine:Landroid/speech/tts/TextToSpeech$EngineInfo;
    .end local v5           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_3d
    sget-object v7, Landroid/speech/tts/TtsEngines$EngineInfoComparator;->INSTANCE:Landroid/speech/tts/TtsEngines$EngineInfoComparator;

    #@3f
    invoke-static {v1, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@42
    goto :goto_19
.end method

.method public getHighestRankedEngineName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 85
    invoke-virtual {p0}, Landroid/speech/tts/TtsEngines;->getEngines()Ljava/util/List;

    #@4
    move-result-object v0

    #@5
    .line 87
    .local v0, engines:Ljava/util/List;,"Ljava/util/List<Landroid/speech/tts/TextToSpeech$EngineInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@8
    move-result v1

    #@9
    if-lez v1, :cond_1e

    #@b
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v1

    #@f
    check-cast v1, Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@11
    iget-boolean v1, v1, Landroid/speech/tts/TextToSpeech$EngineInfo;->system:Z

    #@13
    if-eqz v1, :cond_1e

    #@15
    .line 88
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@1b
    iget-object v1, v1, Landroid/speech/tts/TextToSpeech$EngineInfo;->name:Ljava/lang/String;

    #@1d
    .line 91
    :goto_1d
    return-object v1

    #@1e
    :cond_1e
    const/4 v1, 0x0

    #@1f
    goto :goto_1d
.end method

.method public getLocalePrefForEngine(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "engineName"

    #@0
    .prologue
    .line 294
    iget-object v1, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string/jumbo v2, "tts_default_locale"

    #@9
    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    invoke-static {v1, p1}, Landroid/speech/tts/TtsEngines;->parseEnginePrefFromList(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 298
    .local v0, locale:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_1b

    #@17
    .line 300
    invoke-direct {p0}, Landroid/speech/tts/TtsEngines;->getV1Locale()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 305
    :cond_1b
    return-object v0
.end method

.method public getSettingsIntent(Ljava/lang/String;)Landroid/content/Intent;
    .registers 10
    .parameter "engine"

    #@0
    .prologue
    .line 159
    iget-object v6, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@5
    move-result-object v2

    #@6
    .line 160
    .local v2, pm:Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/Intent;

    #@8
    const-string v6, "android.intent.action.TTS_SERVICE"

    #@a
    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 161
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 162
    const v6, 0x10080

    #@13
    invoke-virtual {v2, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    #@16
    move-result-object v3

    #@17
    .line 167
    .local v3, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v3, :cond_3a

    #@19
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@1c
    move-result v6

    #@1d
    const/4 v7, 0x1

    #@1e
    if-ne v6, v7, :cond_3a

    #@20
    .line 168
    const/4 v6, 0x0

    #@21
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v6

    #@25
    check-cast v6, Landroid/content/pm/ResolveInfo;

    #@27
    iget-object v4, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@29
    .line 169
    .local v4, service:Landroid/content/pm/ServiceInfo;
    if-eqz v4, :cond_3a

    #@2b
    .line 170
    invoke-direct {p0, v4, v2}, Landroid/speech/tts/TtsEngines;->settingsActivityFromServiceInfo(Landroid/content/pm/ServiceInfo;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    .line 171
    .local v5, settings:Ljava/lang/String;
    if-eqz v5, :cond_3a

    #@31
    .line 172
    new-instance v0, Landroid/content/Intent;

    #@33
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@36
    .line 173
    .local v0, i:Landroid/content/Intent;
    invoke-virtual {v0, p1, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@39
    .line 179
    .end local v0           #i:Landroid/content/Intent;
    .end local v4           #service:Landroid/content/pm/ServiceInfo;
    .end local v5           #settings:Ljava/lang/String;
    :goto_39
    return-object v0

    #@3a
    :cond_3a
    const/4 v0, 0x0

    #@3b
    goto :goto_39
.end method

.method public isEngineInstalled(Ljava/lang/String;)Z
    .registers 4
    .parameter "engine"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 148
    if-nez p1, :cond_4

    #@3
    .line 152
    :cond_3
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p0, p1}, Landroid/speech/tts/TtsEngines;->getEngineInfo(Ljava/lang/String;)Landroid/speech/tts/TextToSpeech$EngineInfo;

    #@7
    move-result-object v1

    #@8
    if-eqz v1, :cond_3

    #@a
    const/4 v0, 0x1

    #@b
    goto :goto_3
.end method

.method public declared-synchronized updateLocalePrefForEngine(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "name"
    .parameter "newLocale"

    #@0
    .prologue
    .line 411
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v2

    #@7
    const-string/jumbo v3, "tts_default_locale"

    #@a
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    .line 418
    .local v1, prefList:Ljava/lang/String;
    invoke-direct {p0, v1, p1, p2}, Landroid/speech/tts/TtsEngines;->updateValueInCommaSeparatedList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 423
    .local v0, newPrefList:Ljava/lang/String;
    iget-object v2, p0, Landroid/speech/tts/TtsEngines;->mContext:Landroid/content/Context;

    #@14
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17
    move-result-object v2

    #@18
    const-string/jumbo v3, "tts_default_locale"

    #@1b
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_24

    #@22
    .line 425
    monitor-exit p0

    #@23
    return-void

    #@24
    .line 411
    .end local v0           #newPrefList:Ljava/lang/String;
    .end local v1           #prefList:Ljava/lang/String;
    :catchall_24
    move-exception v2

    #@25
    monitor-exit p0

    #@26
    throw v2
.end method
