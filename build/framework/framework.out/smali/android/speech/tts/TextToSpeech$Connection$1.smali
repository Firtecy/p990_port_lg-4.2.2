.class Landroid/speech/tts/TextToSpeech$Connection$1;
.super Landroid/speech/tts/ITextToSpeechCallback$Stub;
.source "TextToSpeech.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeech$Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/speech/tts/TextToSpeech$Connection;


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeech$Connection;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1259
    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$Connection$1;->this$1:Landroid/speech/tts/TextToSpeech$Connection;

    #@2
    invoke-direct {p0}, Landroid/speech/tts/ITextToSpeechCallback$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .registers 4
    .parameter "utteranceId"

    #@0
    .prologue
    .line 1262
    iget-object v1, p0, Landroid/speech/tts/TextToSpeech$Connection$1;->this$1:Landroid/speech/tts/TextToSpeech$Connection;

    #@2
    iget-object v1, v1, Landroid/speech/tts/TextToSpeech$Connection;->this$0:Landroid/speech/tts/TextToSpeech;

    #@4
    invoke-static {v1}, Landroid/speech/tts/TextToSpeech;->access$800(Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/UtteranceProgressListener;

    #@7
    move-result-object v0

    #@8
    .line 1263
    .local v0, listener:Landroid/speech/tts/UtteranceProgressListener;
    if-eqz v0, :cond_d

    #@a
    .line 1264
    invoke-virtual {v0, p1}, Landroid/speech/tts/UtteranceProgressListener;->onDone(Ljava/lang/String;)V

    #@d
    .line 1266
    :cond_d
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .registers 4
    .parameter "utteranceId"

    #@0
    .prologue
    .line 1270
    iget-object v1, p0, Landroid/speech/tts/TextToSpeech$Connection$1;->this$1:Landroid/speech/tts/TextToSpeech$Connection;

    #@2
    iget-object v1, v1, Landroid/speech/tts/TextToSpeech$Connection;->this$0:Landroid/speech/tts/TextToSpeech;

    #@4
    invoke-static {v1}, Landroid/speech/tts/TextToSpeech;->access$800(Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/UtteranceProgressListener;

    #@7
    move-result-object v0

    #@8
    .line 1271
    .local v0, listener:Landroid/speech/tts/UtteranceProgressListener;
    if-eqz v0, :cond_d

    #@a
    .line 1272
    invoke-virtual {v0, p1}, Landroid/speech/tts/UtteranceProgressListener;->onError(Ljava/lang/String;)V

    #@d
    .line 1274
    :cond_d
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .registers 4
    .parameter "utteranceId"

    #@0
    .prologue
    .line 1278
    iget-object v1, p0, Landroid/speech/tts/TextToSpeech$Connection$1;->this$1:Landroid/speech/tts/TextToSpeech$Connection;

    #@2
    iget-object v1, v1, Landroid/speech/tts/TextToSpeech$Connection;->this$0:Landroid/speech/tts/TextToSpeech;

    #@4
    invoke-static {v1}, Landroid/speech/tts/TextToSpeech;->access$800(Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/UtteranceProgressListener;

    #@7
    move-result-object v0

    #@8
    .line 1279
    .local v0, listener:Landroid/speech/tts/UtteranceProgressListener;
    if-eqz v0, :cond_d

    #@a
    .line 1280
    invoke-virtual {v0, p1}, Landroid/speech/tts/UtteranceProgressListener;->onStart(Ljava/lang/String;)V

    #@d
    .line 1282
    :cond_d
    return-void
.end method
