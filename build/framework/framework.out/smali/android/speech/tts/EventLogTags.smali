.class public Landroid/speech/tts/EventLogTags;
.super Ljava/lang/Object;
.source "EventLogTags.java"


# static fields
.field public static final TTS_SPEAK_FAILURE:I = 0x128e2

.field public static final TTS_SPEAK_SUCCESS:I = 0x128e1


# direct methods
.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static writeTtsSpeakFailure(Ljava/lang/String;IIILjava/lang/String;II)V
    .registers 11
    .parameter "engine"
    .parameter "callerUid"
    .parameter "callerPid"
    .parameter "length"
    .parameter "locale"
    .parameter "rate"
    .parameter "pitch"

    #@0
    .prologue
    .line 24
    const v0, 0x128e2

    #@3
    const/4 v1, 0x7

    #@4
    new-array v1, v1, [Ljava/lang/Object;

    #@6
    const/4 v2, 0x0

    #@7
    aput-object p0, v1, v2

    #@9
    const/4 v2, 0x1

    #@a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d
    move-result-object v3

    #@e
    aput-object v3, v1, v2

    #@10
    const/4 v2, 0x2

    #@11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x3

    #@18
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    aput-object v3, v1, v2

    #@1e
    const/4 v2, 0x4

    #@1f
    aput-object p4, v1, v2

    #@21
    const/4 v2, 0x5

    #@22
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@25
    move-result-object v3

    #@26
    aput-object v3, v1, v2

    #@28
    const/4 v2, 0x6

    #@29
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    aput-object v3, v1, v2

    #@2f
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@32
    .line 25
    return-void
.end method

.method public static writeTtsSpeakSuccess(Ljava/lang/String;IIILjava/lang/String;IIJJJ)V
    .registers 17
    .parameter "engine"
    .parameter "callerUid"
    .parameter "callerPid"
    .parameter "length"
    .parameter "locale"
    .parameter "rate"
    .parameter "pitch"
    .parameter "engineLatency"
    .parameter "engineTotal"
    .parameter "audioLatency"

    #@0
    .prologue
    .line 20
    const v0, 0x128e1

    #@3
    const/16 v1, 0xa

    #@5
    new-array v1, v1, [Ljava/lang/Object;

    #@7
    const/4 v2, 0x0

    #@8
    aput-object p0, v1, v2

    #@a
    const/4 v2, 0x1

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v3

    #@f
    aput-object v3, v1, v2

    #@11
    const/4 v2, 0x2

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v3

    #@16
    aput-object v3, v1, v2

    #@18
    const/4 v2, 0x3

    #@19
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v3

    #@1d
    aput-object v3, v1, v2

    #@1f
    const/4 v2, 0x4

    #@20
    aput-object p4, v1, v2

    #@22
    const/4 v2, 0x5

    #@23
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    const/4 v2, 0x6

    #@2a
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v3

    #@2e
    aput-object v3, v1, v2

    #@30
    const/4 v2, 0x7

    #@31
    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@34
    move-result-object v3

    #@35
    aput-object v3, v1, v2

    #@37
    const/16 v2, 0x8

    #@39
    invoke-static {p9, p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3c
    move-result-object v3

    #@3d
    aput-object v3, v1, v2

    #@3f
    const/16 v2, 0x9

    #@41
    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v3

    #@45
    aput-object v3, v1, v2

    #@47
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    #@4a
    .line 21
    return-void
.end method
