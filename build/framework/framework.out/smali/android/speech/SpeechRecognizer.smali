.class public Landroid/speech/SpeechRecognizer;
.super Ljava/lang/Object;
.source "SpeechRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/SpeechRecognizer$InternalListener;,
        Landroid/speech/SpeechRecognizer$Connection;
    }
.end annotation


# static fields
.field public static final CONFIDENCE_SCORES:Ljava/lang/String; = "confidence_scores"

.field private static final DBG:Z = false

.field public static final ERROR_AUDIO:I = 0x3

.field public static final ERROR_CLIENT:I = 0x5

.field public static final ERROR_INSUFFICIENT_PERMISSIONS:I = 0x9

.field public static final ERROR_NETWORK:I = 0x2

.field public static final ERROR_NETWORK_TIMEOUT:I = 0x1

.field public static final ERROR_NO_MATCH:I = 0x7

.field public static final ERROR_RECOGNIZER_BUSY:I = 0x8

.field public static final ERROR_SERVER:I = 0x4

.field public static final ERROR_SPEECH_TIMEOUT:I = 0x6

.field private static final MSG_CANCEL:I = 0x3

.field private static final MSG_CHANGE_LISTENER:I = 0x4

.field private static final MSG_START:I = 0x1

.field private static final MSG_STOP:I = 0x2

.field public static final RESULTS_RECOGNITION:Ljava/lang/String; = "results_recognition"

.field private static final TAG:Ljava/lang/String; = "SpeechRecognizer"


# instance fields
.field private mConnection:Landroid/speech/SpeechRecognizer$Connection;

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private final mListener:Landroid/speech/SpeechRecognizer$InternalListener;

.field private final mPendingTasks:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Landroid/speech/IRecognitionService;

.field private final mServiceComponent:Landroid/content/ComponentName;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .registers 5
    .parameter "context"
    .parameter "serviceComponent"

    #@0
    .prologue
    .line 153
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 120
    new-instance v0, Landroid/speech/SpeechRecognizer$1;

    #@5
    invoke-direct {v0, p0}, Landroid/speech/SpeechRecognizer$1;-><init>(Landroid/speech/SpeechRecognizer;)V

    #@8
    iput-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@a
    .line 144
    new-instance v0, Ljava/util/LinkedList;

    #@c
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@f
    iput-object v0, p0, Landroid/speech/SpeechRecognizer;->mPendingTasks:Ljava/util/Queue;

    #@11
    .line 147
    new-instance v0, Landroid/speech/SpeechRecognizer$InternalListener;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-direct {v0, p0, v1}, Landroid/speech/SpeechRecognizer$InternalListener;-><init>(Landroid/speech/SpeechRecognizer;Landroid/speech/SpeechRecognizer$1;)V

    #@17
    iput-object v0, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@19
    .line 154
    iput-object p1, p0, Landroid/speech/SpeechRecognizer;->mContext:Landroid/content/Context;

    #@1b
    .line 155
    iput-object p2, p0, Landroid/speech/SpeechRecognizer;->mServiceComponent:Landroid/content/ComponentName;

    #@1d
    .line 156
    return-void
.end method

.method static synthetic access$000(Landroid/speech/SpeechRecognizer;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/speech/SpeechRecognizer;->handleStartListening(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/speech/SpeechRecognizer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/speech/SpeechRecognizer;->handleStopMessage()V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/speech/SpeechRecognizer;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/speech/SpeechRecognizer;->handleCancelMessage()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/speech/SpeechRecognizer;Landroid/speech/RecognitionListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/speech/SpeechRecognizer;->handleChangeListener(Landroid/speech/RecognitionListener;)V

    #@3
    return-void
.end method

.method static synthetic access$502(Landroid/speech/SpeechRecognizer;Landroid/speech/IRecognitionService;)Landroid/speech/IRecognitionService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Landroid/speech/SpeechRecognizer;)Ljava/util/Queue;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mPendingTasks:Ljava/util/Queue;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/speech/SpeechRecognizer;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/speech/SpeechRecognizer;Landroid/speech/SpeechRecognizer$Connection;)Landroid/speech/SpeechRecognizer$Connection;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    iput-object p1, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@2
    return-object p1
.end method

.method private static checkIsCalledFromMainThread()V
    .registers 2

    #@0
    .prologue
    .line 318
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3
    move-result-object v0

    #@4
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@7
    move-result-object v1

    #@8
    if-eq v0, v1, :cond_12

    #@a
    .line 319
    new-instance v0, Ljava/lang/RuntimeException;

    #@c
    const-string v1, "SpeechRecognizer should be used only from the application\'s main thread"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 322
    :cond_12
    return-void
.end method

.method private checkOpenConnection()Z
    .registers 3

    #@0
    .prologue
    .line 375
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@2
    if-eqz v0, :cond_6

    #@4
    .line 376
    const/4 v0, 0x1

    #@5
    .line 380
    :goto_5
    return v0

    #@6
    .line 378
    :cond_6
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@8
    const/4 v1, 0x5

    #@9
    invoke-virtual {v0, v1}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@c
    .line 379
    const-string v0, "SpeechRecognizer"

    #@e
    const-string/jumbo v1, "not connected to the recognition service"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 380
    const/4 v0, 0x0

    #@15
    goto :goto_5
.end method

.method public static createSpeechRecognizer(Landroid/content/Context;)Landroid/speech/SpeechRecognizer;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 206
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/speech/SpeechRecognizer;->createSpeechRecognizer(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/speech/SpeechRecognizer;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static createSpeechRecognizer(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/speech/SpeechRecognizer;
    .registers 4
    .parameter "context"
    .parameter "serviceComponent"

    #@0
    .prologue
    .line 227
    if-nez p0, :cond_a

    #@2
    .line 228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string v1, "Context cannot be null)"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 230
    :cond_a
    invoke-static {}, Landroid/speech/SpeechRecognizer;->checkIsCalledFromMainThread()V

    #@d
    .line 231
    new-instance v0, Landroid/speech/SpeechRecognizer;

    #@f
    invoke-direct {v0, p0, p1}, Landroid/speech/SpeechRecognizer;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    #@12
    return-object v0
.end method

.method private handleCancelMessage()V
    .registers 4

    #@0
    .prologue
    .line 362
    invoke-direct {p0}, Landroid/speech/SpeechRecognizer;->checkOpenConnection()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 372
    :goto_6
    return-void

    #@7
    .line 366
    :cond_7
    :try_start_7
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@9
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@b
    invoke-interface {v1, v2}, Landroid/speech/IRecognitionService;->cancel(Landroid/speech/IRecognitionListener;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_f

    #@e
    goto :goto_6

    #@f
    .line 368
    :catch_f
    move-exception v0

    #@10
    .line 369
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SpeechRecognizer"

    #@12
    const-string v2, "cancel() failed"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 370
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@19
    const/4 v2, 0x5

    #@1a
    invoke-virtual {v1, v2}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@1d
    goto :goto_6
.end method

.method private handleChangeListener(Landroid/speech/RecognitionListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@2
    invoke-static {v0, p1}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1002(Landroid/speech/SpeechRecognizer$InternalListener;Landroid/speech/RecognitionListener;)Landroid/speech/RecognitionListener;

    #@5
    .line 387
    return-void
.end method

.method private handleStartListening(Landroid/content/Intent;)V
    .registers 5
    .parameter "recognizerIntent"

    #@0
    .prologue
    .line 334
    invoke-direct {p0}, Landroid/speech/SpeechRecognizer;->checkOpenConnection()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 344
    :goto_6
    return-void

    #@7
    .line 338
    :cond_7
    :try_start_7
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@9
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@b
    invoke-interface {v1, p1, v2}, Landroid/speech/IRecognitionService;->startListening(Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_f

    #@e
    goto :goto_6

    #@f
    .line 340
    :catch_f
    move-exception v0

    #@10
    .line 341
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SpeechRecognizer"

    #@12
    const-string/jumbo v2, "startListening() failed"

    #@15
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 342
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@1a
    const/4 v2, 0x5

    #@1b
    invoke-virtual {v1, v2}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@1e
    goto :goto_6
.end method

.method private handleStopMessage()V
    .registers 4

    #@0
    .prologue
    .line 348
    invoke-direct {p0}, Landroid/speech/SpeechRecognizer;->checkOpenConnection()Z

    #@3
    move-result v1

    #@4
    if-nez v1, :cond_7

    #@6
    .line 358
    :goto_6
    return-void

    #@7
    .line 352
    :cond_7
    :try_start_7
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@9
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@b
    invoke-interface {v1, v2}, Landroid/speech/IRecognitionService;->stopListening(Landroid/speech/IRecognitionListener;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_e} :catch_f

    #@e
    goto :goto_6

    #@f
    .line 354
    :catch_f
    move-exception v0

    #@10
    .line 355
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "SpeechRecognizer"

    #@12
    const-string/jumbo v2, "stopListening() failed"

    #@15
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@18
    .line 356
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@1a
    const/4 v2, 0x5

    #@1b
    invoke-virtual {v1, v2}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@1e
    goto :goto_6
.end method

.method public static isRecognitionAvailable(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 191
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@4
    move-result-object v2

    #@5
    new-instance v3, Landroid/content/Intent;

    #@7
    const-string v4, "android.speech.RecognitionService"

    #@9
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    #@f
    move-result-object v0

    #@10
    .line 193
    .local v0, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_19

    #@12
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_19

    #@18
    const/4 v1, 0x1

    #@19
    :cond_19
    return v1
.end method

.method private putMessage(Landroid/os/Message;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 325
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 326
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mPendingTasks:Ljava/util/Queue;

    #@6
    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    #@9
    .line 330
    :goto_9
    return-void

    #@a
    .line 328
    :cond_a
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@c
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@f
    goto :goto_9
.end method


# virtual methods
.method public cancel()V
    .registers 3

    #@0
    .prologue
    .line 313
    invoke-static {}, Landroid/speech/SpeechRecognizer;->checkIsCalledFromMainThread()V

    #@3
    .line 314
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x3

    #@6
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-direct {p0, v0}, Landroid/speech/SpeechRecognizer;->putMessage(Landroid/os/Message;)V

    #@d
    .line 315
    return-void
.end method

.method public destroy()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 393
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 394
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mContext:Landroid/content/Context;

    #@7
    iget-object v1, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    #@c
    .line 396
    :cond_c
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mPendingTasks:Ljava/util/Queue;

    #@e
    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    #@11
    .line 397
    iput-object v2, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@13
    .line 398
    iput-object v2, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@15
    .line 399
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@17
    invoke-static {v0, v2}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1002(Landroid/speech/SpeechRecognizer$InternalListener;Landroid/speech/RecognitionListener;)Landroid/speech/RecognitionListener;

    #@1a
    .line 400
    return-void
.end method

.method public setRecognitionListener(Landroid/speech/RecognitionListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 243
    invoke-static {}, Landroid/speech/SpeechRecognizer;->checkIsCalledFromMainThread()V

    #@3
    .line 244
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x4

    #@6
    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-direct {p0, v0}, Landroid/speech/SpeechRecognizer;->putMessage(Landroid/os/Message;)V

    #@d
    .line 245
    return-void
.end method

.method public startListening(Landroid/content/Intent;)V
    .registers 9
    .parameter "recognizerIntent"

    #@0
    .prologue
    const/4 v6, 0x5

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x0

    #@3
    .line 257
    if-nez p1, :cond_d

    #@5
    .line 258
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@7
    const-string v3, "intent must not be null"

    #@9
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v2

    #@d
    .line 260
    :cond_d
    invoke-static {}, Landroid/speech/SpeechRecognizer;->checkIsCalledFromMainThread()V

    #@10
    .line 261
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@12
    if-nez v2, :cond_6f

    #@14
    .line 262
    new-instance v2, Landroid/speech/SpeechRecognizer$Connection;

    #@16
    invoke-direct {v2, p0, v4}, Landroid/speech/SpeechRecognizer$Connection;-><init>(Landroid/speech/SpeechRecognizer;Landroid/speech/SpeechRecognizer$1;)V

    #@19
    iput-object v2, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@1b
    .line 264
    new-instance v1, Landroid/content/Intent;

    #@1d
    const-string v2, "android.speech.RecognitionService"

    #@1f
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@22
    .line 266
    .local v1, serviceIntent:Landroid/content/Intent;
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mServiceComponent:Landroid/content/ComponentName;

    #@24
    if-nez v2, :cond_69

    #@26
    .line 267
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v2

    #@2c
    const-string/jumbo v3, "voice_recognition_service"

    #@2f
    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 270
    .local v0, serviceComponent:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_47

    #@39
    .line 271
    const-string v2, "SpeechRecognizer"

    #@3b
    const-string/jumbo v3, "no selected voice recognition service"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 272
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@43
    invoke-virtual {v2, v6}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@46
    .line 290
    .end local v0           #serviceComponent:Ljava/lang/String;
    .end local v1           #serviceIntent:Landroid/content/Intent;
    :goto_46
    return-void

    #@47
    .line 276
    .restart local v0       #serviceComponent:Ljava/lang/String;
    .restart local v1       #serviceIntent:Landroid/content/Intent;
    :cond_47
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@4e
    .line 281
    .end local v0           #serviceComponent:Ljava/lang/String;
    :goto_4e
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mContext:Landroid/content/Context;

    #@50
    iget-object v3, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@52
    invoke-virtual {v2, v1, v3, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@55
    move-result v2

    #@56
    if-nez v2, :cond_6f

    #@58
    .line 282
    const-string v2, "SpeechRecognizer"

    #@5a
    const-string v3, "bind to recognition service failed"

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 283
    iput-object v4, p0, Landroid/speech/SpeechRecognizer;->mConnection:Landroid/speech/SpeechRecognizer$Connection;

    #@61
    .line 284
    iput-object v4, p0, Landroid/speech/SpeechRecognizer;->mService:Landroid/speech/IRecognitionService;

    #@63
    .line 285
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mListener:Landroid/speech/SpeechRecognizer$InternalListener;

    #@65
    invoke-virtual {v2, v6}, Landroid/speech/SpeechRecognizer$InternalListener;->onError(I)V

    #@68
    goto :goto_46

    #@69
    .line 278
    :cond_69
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mServiceComponent:Landroid/content/ComponentName;

    #@6b
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@6e
    goto :goto_4e

    #@6f
    .line 289
    .end local v1           #serviceIntent:Landroid/content/Intent;
    :cond_6f
    iget-object v2, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@71
    invoke-static {v2, v5, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    #@74
    move-result-object v2

    #@75
    invoke-direct {p0, v2}, Landroid/speech/SpeechRecognizer;->putMessage(Landroid/os/Message;)V

    #@78
    goto :goto_46
.end method

.method public stopListening()V
    .registers 3

    #@0
    .prologue
    .line 303
    invoke-static {}, Landroid/speech/SpeechRecognizer;->checkIsCalledFromMainThread()V

    #@3
    .line 304
    iget-object v0, p0, Landroid/speech/SpeechRecognizer;->mHandler:Landroid/os/Handler;

    #@5
    const/4 v1, 0x2

    #@6
    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    invoke-direct {p0, v0}, Landroid/speech/SpeechRecognizer;->putMessage(Landroid/os/Message;)V

    #@d
    .line 305
    return-void
.end method
