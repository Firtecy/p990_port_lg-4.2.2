.class public abstract Landroid/speech/IRecognitionListener$Stub;
.super Landroid/os/Binder;
.source "IRecognitionListener.java"

# interfaces
.implements Landroid/speech/IRecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/IRecognitionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/IRecognitionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.speech.IRecognitionListener"

.field static final TRANSACTION_onBeginningOfSpeech:I = 0x2

.field static final TRANSACTION_onBufferReceived:I = 0x4

.field static final TRANSACTION_onEndOfSpeech:I = 0x5

.field static final TRANSACTION_onError:I = 0x6

.field static final TRANSACTION_onEvent:I = 0x9

.field static final TRANSACTION_onPartialResults:I = 0x8

.field static final TRANSACTION_onReadyForSpeech:I = 0x1

.field static final TRANSACTION_onResults:I = 0x7

.field static final TRANSACTION_onRmsChanged:I = 0x3


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 21
    const-string v0, "android.speech.IRecognitionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/speech/IRecognitionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/speech/IRecognitionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 29
    if-nez p0, :cond_4

    #@2
    .line 30
    const/4 v0, 0x0

    #@3
    .line 36
    :goto_3
    return-object v0

    #@4
    .line 32
    :cond_4
    const-string v1, "android.speech.IRecognitionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 33
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/speech/IRecognitionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 34
    check-cast v0, Landroid/speech/IRecognitionListener;

    #@12
    goto :goto_3

    #@13
    .line 36
    :cond_13
    new-instance v0, Landroid/speech/IRecognitionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/speech/IRecognitionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 44
    sparse-switch p1, :sswitch_data_b2

    #@4
    .line 142
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 48
    :sswitch_9
    const-string v3, "android.speech.IRecognitionListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 53
    :sswitch_f
    const-string v3, "android.speech.IRecognitionListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_26

    #@1a
    .line 56
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/os/Bundle;

    #@22
    .line 61
    .local v0, _arg0:Landroid/os/Bundle;
    :goto_22
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onReadyForSpeech(Landroid/os/Bundle;)V

    #@25
    goto :goto_8

    #@26
    .line 59
    .end local v0           #_arg0:Landroid/os/Bundle;
    :cond_26
    const/4 v0, 0x0

    #@27
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_22

    #@28
    .line 66
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_28
    const-string v3, "android.speech.IRecognitionListener"

    #@2a
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 67
    invoke-virtual {p0}, Landroid/speech/IRecognitionListener$Stub;->onBeginningOfSpeech()V

    #@30
    goto :goto_8

    #@31
    .line 72
    :sswitch_31
    const-string v3, "android.speech.IRecognitionListener"

    #@33
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@36
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    #@39
    move-result v0

    #@3a
    .line 75
    .local v0, _arg0:F
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onRmsChanged(F)V

    #@3d
    goto :goto_8

    #@3e
    .line 80
    .end local v0           #_arg0:F
    :sswitch_3e
    const-string v3, "android.speech.IRecognitionListener"

    #@40
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@43
    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    #@46
    move-result-object v0

    #@47
    .line 83
    .local v0, _arg0:[B
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onBufferReceived([B)V

    #@4a
    goto :goto_8

    #@4b
    .line 88
    .end local v0           #_arg0:[B
    :sswitch_4b
    const-string v3, "android.speech.IRecognitionListener"

    #@4d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 89
    invoke-virtual {p0}, Landroid/speech/IRecognitionListener$Stub;->onEndOfSpeech()V

    #@53
    goto :goto_8

    #@54
    .line 94
    :sswitch_54
    const-string v3, "android.speech.IRecognitionListener"

    #@56
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@59
    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5c
    move-result v0

    #@5d
    .line 97
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onError(I)V

    #@60
    goto :goto_8

    #@61
    .line 102
    .end local v0           #_arg0:I
    :sswitch_61
    const-string v3, "android.speech.IRecognitionListener"

    #@63
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66
    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v3

    #@6a
    if-eqz v3, :cond_78

    #@6c
    .line 105
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@6e
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@71
    move-result-object v0

    #@72
    check-cast v0, Landroid/os/Bundle;

    #@74
    .line 110
    .local v0, _arg0:Landroid/os/Bundle;
    :goto_74
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onResults(Landroid/os/Bundle;)V

    #@77
    goto :goto_8

    #@78
    .line 108
    .end local v0           #_arg0:Landroid/os/Bundle;
    :cond_78
    const/4 v0, 0x0

    #@79
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_74

    #@7a
    .line 115
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_7a
    const-string v3, "android.speech.IRecognitionListener"

    #@7c
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7f
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@82
    move-result v3

    #@83
    if-eqz v3, :cond_92

    #@85
    .line 118
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@87
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@8a
    move-result-object v0

    #@8b
    check-cast v0, Landroid/os/Bundle;

    #@8d
    .line 123
    .restart local v0       #_arg0:Landroid/os/Bundle;
    :goto_8d
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionListener$Stub;->onPartialResults(Landroid/os/Bundle;)V

    #@90
    goto/16 :goto_8

    #@92
    .line 121
    .end local v0           #_arg0:Landroid/os/Bundle;
    :cond_92
    const/4 v0, 0x0

    #@93
    .restart local v0       #_arg0:Landroid/os/Bundle;
    goto :goto_8d

    #@94
    .line 128
    .end local v0           #_arg0:Landroid/os/Bundle;
    :sswitch_94
    const-string v3, "android.speech.IRecognitionListener"

    #@96
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@99
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@9c
    move-result v0

    #@9d
    .line 132
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v3

    #@a1
    if-eqz v3, :cond_b0

    #@a3
    .line 133
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a5
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@a8
    move-result-object v1

    #@a9
    check-cast v1, Landroid/os/Bundle;

    #@ab
    .line 138
    .local v1, _arg1:Landroid/os/Bundle;
    :goto_ab
    invoke-virtual {p0, v0, v1}, Landroid/speech/IRecognitionListener$Stub;->onEvent(ILandroid/os/Bundle;)V

    #@ae
    goto/16 :goto_8

    #@b0
    .line 136
    .end local v1           #_arg1:Landroid/os/Bundle;
    :cond_b0
    const/4 v1, 0x0

    #@b1
    .restart local v1       #_arg1:Landroid/os/Bundle;
    goto :goto_ab

    #@b2
    .line 44
    :sswitch_data_b2
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_28
        0x3 -> :sswitch_31
        0x4 -> :sswitch_3e
        0x5 -> :sswitch_4b
        0x6 -> :sswitch_54
        0x7 -> :sswitch_61
        0x8 -> :sswitch_7a
        0x9 -> :sswitch_94
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
