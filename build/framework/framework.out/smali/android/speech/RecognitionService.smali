.class public abstract Landroid/speech/RecognitionService;
.super Landroid/app/Service;
.source "RecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/RecognitionService$RecognitionServiceBinder;,
        Landroid/speech/RecognitionService$Callback;,
        Landroid/speech/RecognitionService$StartListeningArgs;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final MSG_CANCEL:I = 0x3

.field private static final MSG_RESET:I = 0x4

.field private static final MSG_START_LISTENING:I = 0x1

.field private static final MSG_STOP_LISTENING:I = 0x2

.field public static final SERVICE_INTERFACE:Ljava/lang/String; = "android.speech.RecognitionService"

.field public static final SERVICE_META_DATA:Ljava/lang/String; = "android.speech"

.field private static final TAG:Ljava/lang/String; = "RecognitionService"


# instance fields
.field private mBinder:Landroid/speech/RecognitionService$RecognitionServiceBinder;

.field private mCurrentCallback:Landroid/speech/RecognitionService$Callback;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 57
    new-instance v0, Landroid/speech/RecognitionService$RecognitionServiceBinder;

    #@5
    invoke-direct {v0, p0}, Landroid/speech/RecognitionService$RecognitionServiceBinder;-><init>(Landroid/speech/RecognitionService;)V

    #@8
    iput-object v0, p0, Landroid/speech/RecognitionService;->mBinder:Landroid/speech/RecognitionService$RecognitionServiceBinder;

    #@a
    .line 63
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@d
    .line 73
    new-instance v0, Landroid/speech/RecognitionService$1;

    #@f
    invoke-direct {v0, p0}, Landroid/speech/RecognitionService$1;-><init>(Landroid/speech/RecognitionService;)V

    #@12
    iput-object v0, p0, Landroid/speech/RecognitionService;->mHandler:Landroid/os/Handler;

    #@14
    .line 307
    return-void
.end method

.method static synthetic access$000(Landroid/speech/RecognitionService;Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/speech/RecognitionService;->dispatchStartListening(Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/speech/RecognitionService;->dispatchStopListening(Landroid/speech/IRecognitionListener;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/speech/RecognitionService;->dispatchCancel(Landroid/speech/IRecognitionListener;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/speech/RecognitionService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/speech/RecognitionService;->dispatchClearCallback()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/speech/RecognitionService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 36
    iget-object v0, p0, Landroid/speech/RecognitionService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/speech/RecognitionService;->checkPermissions(Landroid/speech/IRecognitionListener;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private checkPermissions(Landroid/speech/IRecognitionListener;)Z
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 160
    const-string v1, "android.permission.RECORD_AUDIO"

    #@2
    invoke-virtual {p0, v1}, Landroid/speech/RecognitionService;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_a

    #@8
    .line 162
    const/4 v1, 0x1

    #@9
    .line 170
    :goto_9
    return v1

    #@a
    .line 165
    :cond_a
    :try_start_a
    const-string v1, "RecognitionService"

    #@c
    const-string v2, "call for recognition service without RECORD_AUDIO permissions"

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 166
    const/16 v1, 0x9

    #@13
    invoke-interface {p1, v1}, Landroid/speech/IRecognitionListener;->onError(I)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_16} :catch_18

    #@16
    .line 170
    :goto_16
    const/4 v1, 0x0

    #@17
    goto :goto_9

    #@18
    .line 167
    :catch_18
    move-exception v0

    #@19
    .line 168
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "RecognitionService"

    #@1b
    const-string/jumbo v2, "sending ERROR_INSUFFICIENT_PERMISSIONS message failed"

    #@1e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_16
.end method

.method private dispatchCancel(Landroid/speech/IRecognitionListener;)V
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 135
    :goto_4
    return-void

    #@5
    .line 128
    :cond_5
    iget-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@7
    invoke-static {v0}, Landroid/speech/RecognitionService$Callback;->access$500(Landroid/speech/RecognitionService$Callback;)Landroid/speech/IRecognitionListener;

    #@a
    move-result-object v0

    #@b
    invoke-interface {v0}, Landroid/speech/IRecognitionListener;->asBinder()Landroid/os/IBinder;

    #@e
    move-result-object v0

    #@f
    invoke-interface {p1}, Landroid/speech/IRecognitionListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v1

    #@13
    if-eq v0, v1, :cond_1d

    #@15
    .line 129
    const-string v0, "RecognitionService"

    #@17
    const-string v1, "cancel called by client who did not call startListening - ignoring"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_4

    #@1d
    .line 131
    :cond_1d
    iget-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@1f
    invoke-virtual {p0, v0}, Landroid/speech/RecognitionService;->onCancel(Landroid/speech/RecognitionService$Callback;)V

    #@22
    .line 132
    const/4 v0, 0x0

    #@23
    iput-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@25
    goto :goto_4
.end method

.method private dispatchClearCallback()V
    .registers 2

    #@0
    .prologue
    .line 138
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@3
    .line 139
    return-void
.end method

.method private dispatchStartListening(Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V
    .registers 6
    .parameter "intent"
    .parameter "listener"

    #@0
    .prologue
    .line 95
    iget-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@2
    if-nez v1, :cond_12

    #@4
    .line 97
    new-instance v1, Landroid/speech/RecognitionService$Callback;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-direct {v1, p0, p2, v2}, Landroid/speech/RecognitionService$Callback;-><init>(Landroid/speech/RecognitionService;Landroid/speech/IRecognitionListener;Landroid/speech/RecognitionService$1;)V

    #@a
    iput-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@c
    .line 98
    iget-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@e
    invoke-virtual {p0, p1, v1}, Landroid/speech/RecognitionService;->onStartListening(Landroid/content/Intent;Landroid/speech/RecognitionService$Callback;)V

    #@11
    .line 107
    :goto_11
    return-void

    #@12
    .line 101
    :cond_12
    const/16 v1, 0x8

    #@14
    :try_start_14
    invoke-interface {p2, v1}, Landroid/speech/IRecognitionListener;->onError(I)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_17} :catch_1f

    #@17
    .line 105
    :goto_17
    const-string v1, "RecognitionService"

    #@19
    const-string v2, "concurrent startListening received - ignoring this call"

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    goto :goto_11

    #@1f
    .line 102
    :catch_1f
    move-exception v0

    #@20
    .line 103
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RecognitionService"

    #@22
    const-string/jumbo v2, "onError call from startListening failed"

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_17
.end method

.method private dispatchStopListening(Landroid/speech/IRecognitionListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 111
    :try_start_0
    iget-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@2
    if-nez v1, :cond_11

    #@4
    .line 112
    const/4 v1, 0x5

    #@5
    invoke-interface {p1, v1}, Landroid/speech/IRecognitionListener;->onError(I)V

    #@8
    .line 113
    const-string v1, "RecognitionService"

    #@a
    const-string/jumbo v2, "stopListening called with no preceding startListening - ignoring"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 123
    :goto_10
    return-void

    #@11
    .line 114
    :cond_11
    iget-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@13
    invoke-static {v1}, Landroid/speech/RecognitionService$Callback;->access$500(Landroid/speech/RecognitionService$Callback;)Landroid/speech/IRecognitionListener;

    #@16
    move-result-object v1

    #@17
    invoke-interface {v1}, Landroid/speech/IRecognitionListener;->asBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v1

    #@1b
    invoke-interface {p1}, Landroid/speech/IRecognitionListener;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v2

    #@1f
    if-eq v1, v2, :cond_39

    #@21
    .line 115
    const/16 v1, 0x8

    #@23
    invoke-interface {p1, v1}, Landroid/speech/IRecognitionListener;->onError(I)V

    #@26
    .line 116
    const-string v1, "RecognitionService"

    #@28
    const-string/jumbo v2, "stopListening called by other caller than startListening - ignoring"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_2e} :catch_2f

    #@2e
    goto :goto_10

    #@2f
    .line 120
    :catch_2f
    move-exception v0

    #@30
    .line 121
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "RecognitionService"

    #@32
    const-string/jumbo v2, "onError call from stopListening failed"

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_10

    #@39
    .line 118
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_39
    :try_start_39
    iget-object v1, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@3b
    invoke-virtual {p0, v1}, Landroid/speech/RecognitionService;->onStopListening(Landroid/speech/RecognitionService$Callback;)V
    :try_end_3e
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_3e} :catch_2f

    #@3e
    goto :goto_10
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Landroid/speech/RecognitionService;->mBinder:Landroid/speech/RecognitionService$RecognitionServiceBinder;

    #@2
    return-object v0
.end method

.method protected abstract onCancel(Landroid/speech/RecognitionService$Callback;)V
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 204
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/speech/RecognitionService;->mCurrentCallback:Landroid/speech/RecognitionService$Callback;

    #@3
    .line 205
    iget-object v0, p0, Landroid/speech/RecognitionService;->mBinder:Landroid/speech/RecognitionService$RecognitionServiceBinder;

    #@5
    invoke-virtual {v0}, Landroid/speech/RecognitionService$RecognitionServiceBinder;->clearReference()V

    #@8
    .line 206
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@b
    .line 207
    return-void
.end method

.method protected abstract onStartListening(Landroid/content/Intent;Landroid/speech/RecognitionService$Callback;)V
.end method

.method protected abstract onStopListening(Landroid/speech/RecognitionService$Callback;)V
.end method
