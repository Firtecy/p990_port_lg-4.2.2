.class public abstract Landroid/speech/IRecognitionService$Stub;
.super Landroid/os/Binder;
.source "IRecognitionService.java"

# interfaces
.implements Landroid/speech/IRecognitionService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/IRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/IRecognitionService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.speech.IRecognitionService"

.field static final TRANSACTION_cancel:I = 0x3

.field static final TRANSACTION_startListening:I = 0x1

.field static final TRANSACTION_stopListening:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 23
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 24
    const-string v0, "android.speech.IRecognitionService"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/speech/IRecognitionService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 25
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/speech/IRecognitionService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 32
    if-nez p0, :cond_4

    #@2
    .line 33
    const/4 v0, 0x0

    #@3
    .line 39
    :goto_3
    return-object v0

    #@4
    .line 35
    :cond_4
    const-string v1, "android.speech.IRecognitionService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 36
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/speech/IRecognitionService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 37
    check-cast v0, Landroid/speech/IRecognitionService;

    #@12
    goto :goto_3

    #@13
    .line 39
    :cond_13
    new-instance v0, Landroid/speech/IRecognitionService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/speech/IRecognitionService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 43
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 47
    sparse-switch p1, :sswitch_data_52

    #@4
    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 51
    :sswitch_9
    const-string v3, "android.speech.IRecognitionService"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 56
    :sswitch_f
    const-string v3, "android.speech.IRecognitionService"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v3

    #@18
    if-eqz v3, :cond_2e

    #@1a
    .line 59
    sget-object v3, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Landroid/content/Intent;

    #@22
    .line 65
    .local v0, _arg0:Landroid/content/Intent;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@25
    move-result-object v3

    #@26
    invoke-static {v3}, Landroid/speech/IRecognitionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/speech/IRecognitionListener;

    #@29
    move-result-object v1

    #@2a
    .line 66
    .local v1, _arg1:Landroid/speech/IRecognitionListener;
    invoke-virtual {p0, v0, v1}, Landroid/speech/IRecognitionService$Stub;->startListening(Landroid/content/Intent;Landroid/speech/IRecognitionListener;)V

    #@2d
    goto :goto_8

    #@2e
    .line 62
    .end local v0           #_arg0:Landroid/content/Intent;
    .end local v1           #_arg1:Landroid/speech/IRecognitionListener;
    :cond_2e
    const/4 v0, 0x0

    #@2f
    .restart local v0       #_arg0:Landroid/content/Intent;
    goto :goto_22

    #@30
    .line 71
    .end local v0           #_arg0:Landroid/content/Intent;
    :sswitch_30
    const-string v3, "android.speech.IRecognitionService"

    #@32
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@35
    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@38
    move-result-object v3

    #@39
    invoke-static {v3}, Landroid/speech/IRecognitionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/speech/IRecognitionListener;

    #@3c
    move-result-object v0

    #@3d
    .line 74
    .local v0, _arg0:Landroid/speech/IRecognitionListener;
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionService$Stub;->stopListening(Landroid/speech/IRecognitionListener;)V

    #@40
    goto :goto_8

    #@41
    .line 79
    .end local v0           #_arg0:Landroid/speech/IRecognitionListener;
    :sswitch_41
    const-string v3, "android.speech.IRecognitionService"

    #@43
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@46
    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@49
    move-result-object v3

    #@4a
    invoke-static {v3}, Landroid/speech/IRecognitionListener$Stub;->asInterface(Landroid/os/IBinder;)Landroid/speech/IRecognitionListener;

    #@4d
    move-result-object v0

    #@4e
    .line 82
    .restart local v0       #_arg0:Landroid/speech/IRecognitionListener;
    invoke-virtual {p0, v0}, Landroid/speech/IRecognitionService$Stub;->cancel(Landroid/speech/IRecognitionListener;)V

    #@51
    goto :goto_8

    #@52
    .line 47
    :sswitch_data_52
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_30
        0x3 -> :sswitch_41
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
