.class public final Landroid/speech/srec/MicrophoneInputStream;
.super Ljava/io/InputStream;
.source "MicrophoneInputStream.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MicrophoneInputStream"


# instance fields
.field private mAudioRecord:I

.field private mOneByte:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const-string/jumbo v0, "srec_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 34
    return-void
.end method

.method public constructor <init>(II)V
    .registers 7
    .parameter "sampleRate"
    .parameter "fifoDepth"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@3
    .line 37
    const/4 v1, 0x0

    #@4
    iput v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@6
    .line 38
    const/4 v1, 0x1

    #@7
    new-array v1, v1, [B

    #@9
    iput-object v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mOneByte:[B

    #@b
    .line 47
    invoke-static {p1, p2}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordNew(II)I

    #@e
    move-result v1

    #@f
    iput v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@11
    .line 48
    iget v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@13
    if-nez v1, :cond_1d

    #@15
    new-instance v1, Ljava/io/IOException;

    #@17
    const-string v2, "AudioRecord constructor failed - busy?"

    #@19
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v1

    #@1d
    .line 49
    :cond_1d
    iget v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@1f
    invoke-static {v1}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordStart(I)I

    #@22
    move-result v0

    #@23
    .line 50
    .local v0, status:I
    if-eqz v0, :cond_41

    #@25
    .line 51
    invoke-virtual {p0}, Landroid/speech/srec/MicrophoneInputStream;->close()V

    #@28
    .line 52
    new-instance v1, Ljava/io/IOException;

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "AudioRecord start failed: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@40
    throw v1

    #@41
    .line 54
    :cond_41
    return-void
.end method

.method private static native AudioRecordDelete(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native AudioRecordNew(II)I
.end method

.method private static native AudioRecordRead(I[BII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private static native AudioRecordStart(I)I
.end method

.method private static native AudioRecordStop(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method


# virtual methods
.method public close()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 81
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@3
    if-eqz v0, :cond_11

    #@5
    .line 83
    :try_start_5
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@7
    invoke-static {v0}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordStop(I)V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_16

    #@a
    .line 86
    :try_start_a
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@c
    invoke-static {v0}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordDelete(I)V
    :try_end_f
    .catchall {:try_start_a .. :try_end_f} :catchall_12

    #@f
    .line 88
    iput v2, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@11
    .line 92
    :cond_11
    return-void

    #@12
    .line 88
    :catchall_12
    move-exception v0

    #@13
    iput v2, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@15
    throw v0

    #@16
    .line 85
    :catchall_16
    move-exception v0

    #@17
    .line 86
    :try_start_17
    iget v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@19
    invoke-static {v1}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordDelete(I)V
    :try_end_1c
    .catchall {:try_start_17 .. :try_end_1c} :catchall_1f

    #@1c
    .line 88
    iput v2, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@1e
    throw v0

    #@1f
    :catchall_1f
    move-exception v0

    #@20
    iput v2, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@22
    throw v0
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 97
    invoke-virtual {p0}, Landroid/speech/srec/MicrophoneInputStream;->close()V

    #@7
    .line 98
    new-instance v0, Ljava/io/IOException;

    #@9
    const-string/jumbo v1, "someone forgot to close MicrophoneInputStream"

    #@c
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 100
    :cond_10
    return-void
.end method

.method public read()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 58
    iget v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@4
    if-nez v1, :cond_f

    #@6
    new-instance v1, Ljava/lang/IllegalStateException;

    #@8
    const-string/jumbo v2, "not open"

    #@b
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@e
    throw v1

    #@f
    .line 59
    :cond_f
    iget v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@11
    iget-object v2, p0, Landroid/speech/srec/MicrophoneInputStream;->mOneByte:[B

    #@13
    invoke-static {v1, v2, v3, v4}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordRead(I[BII)I

    #@16
    move-result v0

    #@17
    .line 60
    .local v0, rtn:I
    if-ne v0, v4, :cond_20

    #@19
    iget-object v1, p0, Landroid/speech/srec/MicrophoneInputStream;->mOneByte:[B

    #@1b
    aget-byte v1, v1, v3

    #@1d
    and-int/lit16 v1, v1, 0xff

    #@1f
    :goto_1f
    return v1

    #@20
    :cond_20
    const/4 v1, -0x1

    #@21
    goto :goto_1f
.end method

.method public read([B)I
    .registers 5
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@2
    if-nez v0, :cond_d

    #@4
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "not open"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 66
    :cond_d
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@f
    const/4 v1, 0x0

    #@10
    array-length v2, p1

    #@11
    invoke-static {v0, p1, v1, v2}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordRead(I[BII)I

    #@14
    move-result v0

    #@15
    return v0
.end method

.method public read([BII)I
    .registers 6
    .parameter "b"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@2
    if-nez v0, :cond_d

    #@4
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    const-string/jumbo v1, "not open"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 73
    :cond_d
    iget v0, p0, Landroid/speech/srec/MicrophoneInputStream;->mAudioRecord:I

    #@f
    invoke-static {v0, p1, p2, p3}, Landroid/speech/srec/MicrophoneInputStream;->AudioRecordRead(I[BII)I

    #@12
    move-result v0

    #@13
    return v0
.end method
