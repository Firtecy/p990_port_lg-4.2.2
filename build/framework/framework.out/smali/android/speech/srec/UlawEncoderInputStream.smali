.class public final Landroid/speech/srec/UlawEncoderInputStream;
.super Ljava/io/InputStream;
.source "UlawEncoderInputStream.java"


# static fields
.field private static final MAX_ULAW:I = 0x2000

.field private static final SCALE_BITS:I = 0x10

.field private static final TAG:Ljava/lang/String; = "UlawEncoderInputStream"


# instance fields
.field private final mBuf:[B

.field private mBufCount:I

.field private mIn:Ljava/io/InputStream;

.field private mMax:I

.field private final mOneByte:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .registers 5
    .parameter "in"
    .parameter "max"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 135
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    #@4
    .line 41
    iput v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mMax:I

    #@6
    .line 43
    const/16 v0, 0x400

    #@8
    new-array v0, v0, [B

    #@a
    iput-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@c
    .line 44
    iput v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@e
    .line 46
    const/4 v0, 0x1

    #@f
    new-array v0, v0, [B

    #@11
    iput-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mOneByte:[B

    #@13
    .line 136
    iput-object p1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@15
    .line 137
    iput p2, p0, Landroid/speech/srec/UlawEncoderInputStream;->mMax:I

    #@17
    .line 138
    return-void
.end method

.method public static encode([BI[BIII)V
    .registers 14
    .parameter "pcmBuf"
    .parameter "pcmOffset"
    .parameter "ulawBuf"
    .parameter "ulawOffset"
    .parameter "length"
    .parameter "max"

    #@0
    .prologue
    .line 76
    if-gtz p5, :cond_4

    #@2
    const/16 p5, 0x2000

    #@4
    .line 78
    :cond_4
    const/high16 v6, 0x2000

    #@6
    div-int v0, v6, p5

    #@8
    .line 80
    .local v0, coef:I
    const/4 v1, 0x0

    #@9
    .local v1, i:I
    move v5, p3

    #@a
    .end local p3
    .local v5, ulawOffset:I
    move v3, p1

    #@b
    .end local p1
    .local v3, pcmOffset:I
    :goto_b
    if-ge v1, p4, :cond_e9

    #@d
    .line 81
    add-int/lit8 p1, v3, 0x1

    #@f
    .end local v3           #pcmOffset:I
    .restart local p1
    aget-byte v6, p0, v3

    #@11
    and-int/lit16 v6, v6, 0xff

    #@13
    add-int/lit8 v3, p1, 0x1

    #@15
    .end local p1
    .restart local v3       #pcmOffset:I
    aget-byte v7, p0, p1

    #@17
    shl-int/lit8 v7, v7, 0x8

    #@19
    add-int v2, v6, v7

    #@1b
    .line 82
    .local v2, pcm:I
    mul-int v6, v2, v0

    #@1d
    shr-int/lit8 v2, v6, 0x10

    #@1f
    .line 85
    if-ltz v2, :cond_89

    #@21
    .line 86
    if-gtz v2, :cond_2e

    #@23
    const/16 v4, 0xff

    #@25
    .line 108
    .local v4, ulaw:I
    :goto_25
    add-int/lit8 p3, v5, 0x1

    #@27
    .end local v5           #ulawOffset:I
    .restart local p3
    int-to-byte v6, v4

    #@28
    aput-byte v6, p2, v5

    #@2a
    .line 80
    add-int/lit8 v1, v1, 0x1

    #@2c
    move v5, p3

    #@2d
    .end local p3
    .restart local v5       #ulawOffset:I
    goto :goto_b

    #@2e
    .line 86
    .end local v4           #ulaw:I
    :cond_2e
    const/16 v6, 0x1e

    #@30
    if-gt v2, v6, :cond_39

    #@32
    rsub-int/lit8 v6, v2, 0x1e

    #@34
    shr-int/lit8 v6, v6, 0x1

    #@36
    add-int/lit16 v4, v6, 0xf0

    #@38
    goto :goto_25

    #@39
    :cond_39
    const/16 v6, 0x5e

    #@3b
    if-gt v2, v6, :cond_44

    #@3d
    rsub-int/lit8 v6, v2, 0x5e

    #@3f
    shr-int/lit8 v6, v6, 0x2

    #@41
    add-int/lit16 v4, v6, 0xe0

    #@43
    goto :goto_25

    #@44
    :cond_44
    const/16 v6, 0xde

    #@46
    if-gt v2, v6, :cond_4f

    #@48
    rsub-int v6, v2, 0xde

    #@4a
    shr-int/lit8 v6, v6, 0x3

    #@4c
    add-int/lit16 v4, v6, 0xd0

    #@4e
    goto :goto_25

    #@4f
    :cond_4f
    const/16 v6, 0x1de

    #@51
    if-gt v2, v6, :cond_5a

    #@53
    rsub-int v6, v2, 0x1de

    #@55
    shr-int/lit8 v6, v6, 0x4

    #@57
    add-int/lit16 v4, v6, 0xc0

    #@59
    goto :goto_25

    #@5a
    :cond_5a
    const/16 v6, 0x3de

    #@5c
    if-gt v2, v6, :cond_65

    #@5e
    rsub-int v6, v2, 0x3de

    #@60
    shr-int/lit8 v6, v6, 0x5

    #@62
    add-int/lit16 v4, v6, 0xb0

    #@64
    goto :goto_25

    #@65
    :cond_65
    const/16 v6, 0x7de

    #@67
    if-gt v2, v6, :cond_70

    #@69
    rsub-int v6, v2, 0x7de

    #@6b
    shr-int/lit8 v6, v6, 0x6

    #@6d
    add-int/lit16 v4, v6, 0xa0

    #@6f
    goto :goto_25

    #@70
    :cond_70
    const/16 v6, 0xfde

    #@72
    if-gt v2, v6, :cond_7b

    #@74
    rsub-int v6, v2, 0xfde

    #@76
    shr-int/lit8 v6, v6, 0x7

    #@78
    add-int/lit16 v4, v6, 0x90

    #@7a
    goto :goto_25

    #@7b
    :cond_7b
    const/16 v6, 0x1fde

    #@7d
    if-gt v2, v6, :cond_86

    #@7f
    rsub-int v6, v2, 0x1fde

    #@81
    shr-int/lit8 v6, v6, 0x8

    #@83
    add-int/lit16 v4, v6, 0x80

    #@85
    goto :goto_25

    #@86
    :cond_86
    const/16 v4, 0x80

    #@88
    goto :goto_25

    #@89
    .line 97
    :cond_89
    const/4 v6, -0x1

    #@8a
    if-gt v6, v2, :cond_8f

    #@8c
    const/16 v4, 0x7f

    #@8e
    .restart local v4       #ulaw:I
    :goto_8e
    goto :goto_25

    #@8f
    .end local v4           #ulaw:I
    :cond_8f
    const/16 v6, -0x1f

    #@91
    if-gt v6, v2, :cond_9a

    #@93
    add-int/lit8 v6, v2, 0x1f

    #@95
    shr-int/lit8 v6, v6, 0x1

    #@97
    add-int/lit8 v4, v6, 0x70

    #@99
    goto :goto_8e

    #@9a
    :cond_9a
    const/16 v6, -0x5f

    #@9c
    if-gt v6, v2, :cond_a5

    #@9e
    add-int/lit8 v6, v2, 0x5f

    #@a0
    shr-int/lit8 v6, v6, 0x2

    #@a2
    add-int/lit8 v4, v6, 0x60

    #@a4
    goto :goto_8e

    #@a5
    :cond_a5
    const/16 v6, -0xdf

    #@a7
    if-gt v6, v2, :cond_b0

    #@a9
    add-int/lit16 v6, v2, 0xdf

    #@ab
    shr-int/lit8 v6, v6, 0x3

    #@ad
    add-int/lit8 v4, v6, 0x50

    #@af
    goto :goto_8e

    #@b0
    :cond_b0
    const/16 v6, -0x1df

    #@b2
    if-gt v6, v2, :cond_bb

    #@b4
    add-int/lit16 v6, v2, 0x1df

    #@b6
    shr-int/lit8 v6, v6, 0x4

    #@b8
    add-int/lit8 v4, v6, 0x40

    #@ba
    goto :goto_8e

    #@bb
    :cond_bb
    const/16 v6, -0x3df

    #@bd
    if-gt v6, v2, :cond_c6

    #@bf
    add-int/lit16 v6, v2, 0x3df

    #@c1
    shr-int/lit8 v6, v6, 0x5

    #@c3
    add-int/lit8 v4, v6, 0x30

    #@c5
    goto :goto_8e

    #@c6
    :cond_c6
    const/16 v6, -0x7df

    #@c8
    if-gt v6, v2, :cond_d1

    #@ca
    add-int/lit16 v6, v2, 0x7df

    #@cc
    shr-int/lit8 v6, v6, 0x6

    #@ce
    add-int/lit8 v4, v6, 0x20

    #@d0
    goto :goto_8e

    #@d1
    :cond_d1
    const/16 v6, -0xfdf

    #@d3
    if-gt v6, v2, :cond_dc

    #@d5
    add-int/lit16 v6, v2, 0xfdf

    #@d7
    shr-int/lit8 v6, v6, 0x7

    #@d9
    add-int/lit8 v4, v6, 0x10

    #@db
    goto :goto_8e

    #@dc
    :cond_dc
    const/16 v6, -0x1fdf

    #@de
    if-gt v6, v2, :cond_e7

    #@e0
    add-int/lit16 v6, v2, 0x1fdf

    #@e2
    shr-int/lit8 v6, v6, 0x8

    #@e4
    add-int/lit8 v4, v6, 0x0

    #@e6
    goto :goto_8e

    #@e7
    :cond_e7
    const/4 v4, 0x0

    #@e8
    goto :goto_8e

    #@e9
    .line 110
    .end local v2           #pcm:I
    :cond_e9
    return-void
.end method

.method public static maxAbsPcm([BII)I
    .registers 9
    .parameter "pcmBuf"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 121
    const/4 v1, 0x0

    #@1
    .line 122
    .local v1, max:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    move v2, p1

    #@3
    .end local p1
    .local v2, offset:I
    :goto_3
    if-ge v0, p2, :cond_1c

    #@5
    .line 123
    add-int/lit8 p1, v2, 0x1

    #@7
    .end local v2           #offset:I
    .restart local p1
    aget-byte v4, p0, v2

    #@9
    and-int/lit16 v4, v4, 0xff

    #@b
    add-int/lit8 v2, p1, 0x1

    #@d
    .end local p1
    .restart local v2       #offset:I
    aget-byte v5, p0, p1

    #@f
    shl-int/lit8 v5, v5, 0x8

    #@11
    add-int v3, v4, v5

    #@13
    .line 124
    .local v3, pcm:I
    if-gez v3, :cond_16

    #@15
    neg-int v3, v3

    #@16
    .line 125
    :cond_16
    if-le v3, v1, :cond_19

    #@18
    move v1, v3

    #@19
    .line 122
    :cond_19
    add-int/lit8 v0, v0, 0x1

    #@1b
    goto :goto_3

    #@1c
    .line 127
    .end local v3           #pcm:I
    :cond_1c
    return v1
.end method


# virtual methods
.method public available()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@2
    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@8
    add-int/2addr v0, v1

    #@9
    div-int/lit8 v0, v0, 0x2

    #@b
    return v0
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 176
    iget-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 177
    iget-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@6
    .line 178
    .local v0, in:Ljava/io/InputStream;
    const/4 v1, 0x0

    #@7
    iput-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@9
    .line 179
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    #@c
    .line 181
    .end local v0           #in:Ljava/io/InputStream;
    :cond_c
    return-void
.end method

.method public read()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    .line 169
    iget-object v2, p0, Landroid/speech/srec/UlawEncoderInputStream;->mOneByte:[B

    #@4
    const/4 v3, 0x1

    #@5
    invoke-virtual {p0, v2, v4, v3}, Landroid/speech/srec/UlawEncoderInputStream;->read([BII)I

    #@8
    move-result v0

    #@9
    .line 170
    .local v0, n:I
    if-ne v0, v1, :cond_c

    #@b
    .line 171
    :goto_b
    return v1

    #@c
    :cond_c
    iget-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mOneByte:[B

    #@e
    aget-byte v1, v1, v4

    #@10
    and-int/lit16 v1, v1, 0xff

    #@12
    goto :goto_b
.end method

.method public read([B)I
    .registers 4
    .parameter "buf"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 164
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1}, Landroid/speech/srec/UlawEncoderInputStream;->read([BII)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public read([BII)I
    .registers 13
    .parameter "buf"
    .parameter "offset"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 142
    iget-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@3
    if-nez v1, :cond_13

    #@5
    new-instance v0, Ljava/lang/IllegalStateException;

    #@7
    const-string/jumbo v1, "not open"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 148
    .local v4, n:I
    :cond_e
    iget v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@10
    add-int/2addr v1, v4

    #@11
    iput v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@13
    .line 145
    .end local v4           #n:I
    :cond_13
    iget v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@15
    const/4 v2, 0x2

    #@16
    if-ge v1, v2, :cond_31

    #@18
    .line 146
    iget-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mIn:Ljava/io/InputStream;

    #@1a
    iget-object v2, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@1c
    iget v3, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@1e
    mul-int/lit8 v5, p3, 0x2

    #@20
    iget-object v7, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@22
    array-length v7, v7

    #@23
    iget v8, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@25
    sub-int/2addr v7, v8

    #@26
    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    #@29
    move-result v5

    #@2a
    invoke-virtual {v1, v2, v3, v5}, Ljava/io/InputStream;->read([BII)I

    #@2d
    move-result v4

    #@2e
    .line 147
    .restart local v4       #n:I
    if-ne v4, v0, :cond_e

    #@30
    .line 159
    :goto_30
    return v0

    #@31
    .line 152
    .end local v4           #n:I
    :cond_31
    iget v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@33
    div-int/lit8 v0, v0, 0x2

    #@35
    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    #@38
    move-result v4

    #@39
    .line 153
    .restart local v4       #n:I
    iget-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@3b
    const/4 v1, 0x0

    #@3c
    iget v5, p0, Landroid/speech/srec/UlawEncoderInputStream;->mMax:I

    #@3e
    move-object v2, p1

    #@3f
    move v3, p2

    #@40
    invoke-static/range {v0 .. v5}, Landroid/speech/srec/UlawEncoderInputStream;->encode([BI[BIII)V

    #@43
    .line 156
    iget v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@45
    mul-int/lit8 v1, v4, 0x2

    #@47
    sub-int/2addr v0, v1

    #@48
    iput v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@4a
    .line 157
    const/4 v6, 0x0

    #@4b
    .local v6, i:I
    :goto_4b
    iget v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBufCount:I

    #@4d
    if-ge v6, v0, :cond_5d

    #@4f
    iget-object v0, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@51
    iget-object v1, p0, Landroid/speech/srec/UlawEncoderInputStream;->mBuf:[B

    #@53
    mul-int/lit8 v2, v4, 0x2

    #@55
    add-int/2addr v2, v6

    #@56
    aget-byte v1, v1, v2

    #@58
    aput-byte v1, v0, v6

    #@5a
    add-int/lit8 v6, v6, 0x1

    #@5c
    goto :goto_4b

    #@5d
    :cond_5d
    move v0, v4

    #@5e
    .line 159
    goto :goto_30
.end method
