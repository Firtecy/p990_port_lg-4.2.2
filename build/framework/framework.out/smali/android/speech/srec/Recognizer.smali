.class public final Landroid/speech/srec/Recognizer;
.super Ljava/lang/Object;
.source "Recognizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/speech/srec/Recognizer$Grammar;
    }
.end annotation


# static fields
.field public static final EVENT_END_OF_VOICING:I = 0x6

.field public static final EVENT_INCOMPLETE:I = 0x2

.field public static final EVENT_INVALID:I = 0x0

.field public static final EVENT_MAX_SPEECH:I = 0xc

.field public static final EVENT_NEED_MORE_AUDIO:I = 0xb

.field public static final EVENT_NO_MATCH:I = 0x1

.field public static final EVENT_RECOGNITION_RESULT:I = 0x8

.field public static final EVENT_RECOGNITION_TIMEOUT:I = 0xa

.field public static final EVENT_SPOKE_TOO_SOON:I = 0x7

.field public static final EVENT_STARTED:I = 0x3

.field public static final EVENT_START_OF_UTTERANCE_TIMEOUT:I = 0x9

.field public static final EVENT_START_OF_VOICING:I = 0x5

.field public static final EVENT_STOPPED:I = 0x4

.field public static final KEY_CONFIDENCE:Ljava/lang/String; = "conf"

.field public static final KEY_LITERAL:Ljava/lang/String; = "literal"

.field public static final KEY_MEANING:Ljava/lang/String; = "meaning"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mActiveGrammar:Landroid/speech/srec/Recognizer$Grammar;

.field private mPutAudioBuffer:[B

.field private mRecognizer:I

.field private mVocabulary:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 107
    const-string/jumbo v0, "srec_jni"

    #@3
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@6
    .line 110
    const-string v0, "Recognizer"

    #@8
    sput-object v0, Landroid/speech/srec/Recognizer;->TAG:Ljava/lang/String;

    #@a
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "configFile"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 165
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 128
    iput v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@7
    .line 131
    iput v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@9
    .line 134
    iput-object v1, p0, Landroid/speech/srec/Recognizer;->mActiveGrammar:Landroid/speech/srec/Recognizer$Grammar;

    #@b
    .line 320
    iput-object v1, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@d
    .line 166
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemInit()V

    #@10
    .line 167
    invoke-static {p1}, Landroid/speech/srec/Recognizer;->SR_SessionCreate(Ljava/lang/String;)V

    #@13
    .line 168
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_RecognizerCreate()I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@19
    .line 169
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@1b
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerSetup(I)V

    #@1e
    .line 170
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_VocabularyLoad()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@24
    .line 171
    return-void
.end method

.method private static native PMemInit()V
.end method

.method private static native PMemShutdown()V
.end method

.method private static native SR_AcousticStateGet(I)Ljava/lang/String;
.end method

.method private static native SR_AcousticStateReset(I)V
.end method

.method private static native SR_AcousticStateSet(ILjava/lang/String;)V
.end method

.method private static native SR_GrammarAddWordToSlot(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method private static native SR_GrammarAllowAll(I)V
.end method

.method private static native SR_GrammarAllowOnly(ILjava/lang/String;)V
.end method

.method private static native SR_GrammarCompile(I)V
.end method

.method private static native SR_GrammarCreate()I
.end method

.method private static native SR_GrammarDestroy(I)V
.end method

.method private static native SR_GrammarLoad(Ljava/lang/String;)I
.end method

.method private static native SR_GrammarResetAllSlots(I)V
.end method

.method private static native SR_GrammarSave(ILjava/lang/String;)V
.end method

.method private static native SR_GrammarSetupRecognizer(II)V
.end method

.method private static native SR_GrammarSetupVocabulary(II)V
.end method

.method private static native SR_GrammarUnsetupRecognizer(I)V
.end method

.method private static native SR_RecognizerActivateRule(IILjava/lang/String;I)V
.end method

.method private static native SR_RecognizerAdvance(I)I
.end method

.method private static native SR_RecognizerCheckGrammarConsistency(II)Z
.end method

.method private static native SR_RecognizerCreate()I
.end method

.method private static native SR_RecognizerDeactivateAllRules(I)V
.end method

.method private static native SR_RecognizerDeactivateRule(IILjava/lang/String;)V
.end method

.method private static native SR_RecognizerDestroy(I)V
.end method

.method private static native SR_RecognizerGetBoolParameter(ILjava/lang/String;)Z
.end method

.method private static native SR_RecognizerGetParameter(ILjava/lang/String;)Ljava/lang/String;
.end method

.method private static native SR_RecognizerGetSize_tParameter(ILjava/lang/String;)I
.end method

.method private static native SR_RecognizerHasSetupRules(I)Z
.end method

.method private static native SR_RecognizerIsActiveRule(IILjava/lang/String;)Z
.end method

.method private static native SR_RecognizerIsSetup(I)Z
.end method

.method private static native SR_RecognizerIsSignalClipping(I)Z
.end method

.method private static native SR_RecognizerIsSignalDCOffset(I)Z
.end method

.method private static native SR_RecognizerIsSignalNoisy(I)Z
.end method

.method private static native SR_RecognizerIsSignalTooFewSamples(I)Z
.end method

.method private static native SR_RecognizerIsSignalTooManySamples(I)Z
.end method

.method private static native SR_RecognizerIsSignalTooQuiet(I)Z
.end method

.method private static native SR_RecognizerPutAudio(I[BIIZ)I
.end method

.method private static native SR_RecognizerResultGetKeyCount(II)I
.end method

.method private static native SR_RecognizerResultGetKeyList(II)[Ljava/lang/String;
.end method

.method private static native SR_RecognizerResultGetSize(I)I
.end method

.method private static native SR_RecognizerResultGetValue(IILjava/lang/String;)Ljava/lang/String;
.end method

.method private static native SR_RecognizerResultGetWaveform(I)[B
.end method

.method private static native SR_RecognizerSetBoolParameter(ILjava/lang/String;Z)V
.end method

.method private static native SR_RecognizerSetParameter(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private static native SR_RecognizerSetSize_tParameter(ILjava/lang/String;I)V
.end method

.method private static native SR_RecognizerSetup(I)V
.end method

.method private static native SR_RecognizerSetupRule(IILjava/lang/String;)V
.end method

.method private static native SR_RecognizerStart(I)V
.end method

.method private static native SR_RecognizerStop(I)V
.end method

.method private static native SR_RecognizerUnsetup(I)V
.end method

.method private static native SR_SessionCreate(Ljava/lang/String;)V
.end method

.method private static native SR_SessionDestroy()V
.end method

.method private static native SR_VocabularyDestroy(I)V
.end method

.method private static native SR_VocabularyGetPronunciation(ILjava/lang/String;)Ljava/lang/String;
.end method

.method private static native SR_VocabularyLoad()I
.end method

.method static synthetic access$000(Ljava/lang/String;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-static {p0}, Landroid/speech/srec/Recognizer;->SR_GrammarLoad(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Landroid/speech/srec/Recognizer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 105
    iget v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@2
    return v0
.end method

.method static synthetic access$1000(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-static {p0}, Landroid/speech/srec/Recognizer;->SR_GrammarDestroy(I)V

    #@3
    return-void
.end method

.method static synthetic access$200(II)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 105
    invoke-static {p0, p1}, Landroid/speech/srec/Recognizer;->SR_GrammarSetupVocabulary(II)V

    #@3
    return-void
.end method

.method static synthetic access$300(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-static {p0}, Landroid/speech/srec/Recognizer;->SR_GrammarResetAllSlots(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 105
    invoke-static/range {p0 .. p5}, Landroid/speech/srec/Recognizer;->SR_GrammarAddWordToSlot(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 105
    invoke-static {p0}, Landroid/speech/srec/Recognizer;->SR_GrammarCompile(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/speech/srec/Recognizer;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 105
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    return v0
.end method

.method static synthetic access$700(II)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 105
    invoke-static {p0, p1}, Landroid/speech/srec/Recognizer;->SR_GrammarSetupRecognizer(II)V

    #@3
    return-void
.end method

.method static synthetic access$802(Landroid/speech/srec/Recognizer;Landroid/speech/srec/Recognizer$Grammar;)Landroid/speech/srec/Recognizer$Grammar;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 105
    iput-object p1, p0, Landroid/speech/srec/Recognizer;->mActiveGrammar:Landroid/speech/srec/Recognizer$Grammar;

    #@2
    return-object p1
.end method

.method static synthetic access$900(ILjava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 105
    invoke-static {p0, p1}, Landroid/speech/srec/Recognizer;->SR_GrammarSave(ILjava/lang/String;)V

    #@3
    return-void
.end method

.method public static eventToString(I)Ljava/lang/String;
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 572
    packed-switch p0, :pswitch_data_3e

    #@3
    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "EVENT_"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    :goto_16
    return-object v0

    #@17
    .line 574
    :pswitch_17
    const-string v0, "EVENT_INVALID"

    #@19
    goto :goto_16

    #@1a
    .line 576
    :pswitch_1a
    const-string v0, "EVENT_NO_MATCH"

    #@1c
    goto :goto_16

    #@1d
    .line 578
    :pswitch_1d
    const-string v0, "EVENT_INCOMPLETE"

    #@1f
    goto :goto_16

    #@20
    .line 580
    :pswitch_20
    const-string v0, "EVENT_STARTED"

    #@22
    goto :goto_16

    #@23
    .line 582
    :pswitch_23
    const-string v0, "EVENT_STOPPED"

    #@25
    goto :goto_16

    #@26
    .line 584
    :pswitch_26
    const-string v0, "EVENT_START_OF_VOICING"

    #@28
    goto :goto_16

    #@29
    .line 586
    :pswitch_29
    const-string v0, "EVENT_END_OF_VOICING"

    #@2b
    goto :goto_16

    #@2c
    .line 588
    :pswitch_2c
    const-string v0, "EVENT_SPOKE_TOO_SOON"

    #@2e
    goto :goto_16

    #@2f
    .line 590
    :pswitch_2f
    const-string v0, "EVENT_RECOGNITION_RESULT"

    #@31
    goto :goto_16

    #@32
    .line 592
    :pswitch_32
    const-string v0, "EVENT_START_OF_UTTERANCE_TIMEOUT"

    #@34
    goto :goto_16

    #@35
    .line 594
    :pswitch_35
    const-string v0, "EVENT_RECOGNITION_TIMEOUT"

    #@37
    goto :goto_16

    #@38
    .line 596
    :pswitch_38
    const-string v0, "EVENT_NEED_MORE_AUDIO"

    #@3a
    goto :goto_16

    #@3b
    .line 598
    :pswitch_3b
    const-string v0, "EVENT_MAX_SPEECH"

    #@3d
    goto :goto_16

    #@3e
    .line 572
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_29
        :pswitch_2c
        :pswitch_2f
        :pswitch_32
        :pswitch_35
        :pswitch_38
        :pswitch_3b
    .end packed-switch
.end method

.method public static getConfigDir(Ljava/util/Locale;)Ljava/lang/String;
    .registers 6
    .parameter "locale"

    #@0
    .prologue
    .line 146
    if-nez p0, :cond_4

    #@2
    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@4
    .line 147
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "/system/usr/srec/config/"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    const/16 v3, 0x5f

    #@15
    const/16 v4, 0x2e

    #@17
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    .line 149
    .local v0, dir:Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    #@29
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@2c
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_33

    #@32
    .line 150
    .end local v0           #dir:Ljava/lang/String;
    :goto_32
    return-object v0

    #@33
    .restart local v0       #dir:Ljava/lang/String;
    :cond_33
    const/4 v0, 0x0

    #@34
    goto :goto_32
.end method


# virtual methods
.method public advance()I
    .registers 2

    #@0
    .prologue
    .line 284
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerAdvance(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public destroy()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 404
    :try_start_1
    iget v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@3
    if-eqz v0, :cond_a

    #@5
    iget v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@7
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_VocabularyDestroy(I)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_62

    #@a
    .line 406
    :cond_a
    iput v2, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@c
    .line 408
    :try_start_c
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@e
    if-eqz v0, :cond_15

    #@10
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@12
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerUnsetup(I)V
    :try_end_15
    .catchall {:try_start_c .. :try_end_15} :catchall_3b

    #@15
    .line 411
    :cond_15
    :try_start_15
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@17
    if-eqz v0, :cond_1e

    #@19
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@1b
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerDestroy(I)V
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_2c

    #@1e
    .line 413
    :cond_1e
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@20
    .line 415
    :try_start_20
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_23
    .catchall {:try_start_20 .. :try_end_23} :catchall_27

    #@23
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@26
    .line 422
    return-void

    #@27
    .line 417
    :catchall_27
    move-exception v0

    #@28
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@2b
    throw v0

    #@2c
    .line 413
    :catchall_2c
    move-exception v0

    #@2d
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2f
    .line 415
    :try_start_2f
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_36

    #@32
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@35
    throw v0

    #@36
    :catchall_36
    move-exception v0

    #@37
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@3a
    throw v0

    #@3b
    .line 410
    :catchall_3b
    move-exception v0

    #@3c
    .line 411
    :try_start_3c
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@3e
    if-eqz v1, :cond_45

    #@40
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@42
    invoke-static {v1}, Landroid/speech/srec/Recognizer;->SR_RecognizerDestroy(I)V
    :try_end_45
    .catchall {:try_start_3c .. :try_end_45} :catchall_53

    #@45
    .line 413
    :cond_45
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@47
    .line 415
    :try_start_47
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_4a
    .catchall {:try_start_47 .. :try_end_4a} :catchall_4e

    #@4a
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@4d
    throw v0

    #@4e
    :catchall_4e
    move-exception v0

    #@4f
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@52
    throw v0

    #@53
    .line 413
    :catchall_53
    move-exception v0

    #@54
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@56
    .line 415
    :try_start_56
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_59
    .catchall {:try_start_56 .. :try_end_59} :catchall_5d

    #@59
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@5c
    throw v0

    #@5d
    :catchall_5d
    move-exception v0

    #@5e
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@61
    throw v0

    #@62
    .line 406
    :catchall_62
    move-exception v0

    #@63
    iput v2, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@65
    .line 408
    :try_start_65
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@67
    if-eqz v1, :cond_6e

    #@69
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@6b
    invoke-static {v1}, Landroid/speech/srec/Recognizer;->SR_RecognizerUnsetup(I)V
    :try_end_6e
    .catchall {:try_start_65 .. :try_end_6e} :catchall_94

    #@6e
    .line 411
    :cond_6e
    :try_start_6e
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@70
    if-eqz v1, :cond_77

    #@72
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@74
    invoke-static {v1}, Landroid/speech/srec/Recognizer;->SR_RecognizerDestroy(I)V
    :try_end_77
    .catchall {:try_start_6e .. :try_end_77} :catchall_85

    #@77
    .line 413
    :cond_77
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@79
    .line 415
    :try_start_79
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_7c
    .catchall {:try_start_79 .. :try_end_7c} :catchall_80

    #@7c
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@7f
    throw v0

    #@80
    :catchall_80
    move-exception v0

    #@81
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@84
    throw v0

    #@85
    .line 413
    :catchall_85
    move-exception v0

    #@86
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@88
    .line 415
    :try_start_88
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_8b
    .catchall {:try_start_88 .. :try_end_8b} :catchall_8f

    #@8b
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@8e
    throw v0

    #@8f
    :catchall_8f
    move-exception v0

    #@90
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@93
    throw v0

    #@94
    .line 410
    :catchall_94
    move-exception v0

    #@95
    .line 411
    :try_start_95
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@97
    if-eqz v1, :cond_9e

    #@99
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@9b
    invoke-static {v1}, Landroid/speech/srec/Recognizer;->SR_RecognizerDestroy(I)V
    :try_end_9e
    .catchall {:try_start_95 .. :try_end_9e} :catchall_ac

    #@9e
    .line 413
    :cond_9e
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@a0
    .line 415
    :try_start_a0
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_a3
    .catchall {:try_start_a0 .. :try_end_a3} :catchall_a7

    #@a3
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@a6
    throw v0

    #@a7
    :catchall_a7
    move-exception v0

    #@a8
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@ab
    throw v0

    #@ac
    .line 413
    :catchall_ac
    move-exception v0

    #@ad
    iput v2, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@af
    .line 415
    :try_start_af
    invoke-static {}, Landroid/speech/srec/Recognizer;->SR_SessionDestroy()V
    :try_end_b2
    .catchall {:try_start_af .. :try_end_b2} :catchall_b6

    #@b2
    .line 417
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@b5
    throw v0

    #@b6
    :catchall_b6
    move-exception v0

    #@b7
    invoke-static {}, Landroid/speech/srec/Recognizer;->PMemShutdown()V

    #@ba
    throw v0
.end method

.method protected finalize()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 428
    iget v0, p0, Landroid/speech/srec/Recognizer;->mVocabulary:I

    #@2
    if-nez v0, :cond_8

    #@4
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@6
    if-eqz v0, :cond_14

    #@8
    .line 429
    :cond_8
    invoke-virtual {p0}, Landroid/speech/srec/Recognizer;->destroy()V

    #@b
    .line 430
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    const-string/jumbo v1, "someone forgot to destroy Recognizer"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 432
    :cond_14
    return-void
.end method

.method public getAcousticState()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 396
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_AcousticStateGet(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getResult(ILjava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "index"
    .parameter "key"

    #@0
    .prologue
    .line 359
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/speech/srec/Recognizer;->SR_RecognizerResultGetValue(IILjava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getResultCount()I
    .registers 2

    #@0
    .prologue
    .line 330
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerResultGetSize(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getResultKeys(I)[Ljava/lang/String;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 342
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0, p1}, Landroid/speech/srec/Recognizer;->SR_RecognizerResultGetKeyList(II)[Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public putAudio([BIIZ)I
    .registers 6
    .parameter "buf"
    .parameter "offset"
    .parameter "length"
    .parameter "isLast"

    #@0
    .prologue
    .line 296
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/speech/srec/Recognizer;->SR_RecognizerPutAudio(I[BIIZ)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public putAudio(Ljava/io/InputStream;)V
    .registers 7
    .parameter "audio"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 306
    iget-object v1, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@3
    if-nez v1, :cond_b

    #@5
    const/16 v1, 0x200

    #@7
    new-array v1, v1, [B

    #@9
    iput-object v1, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@b
    .line 308
    :cond_b
    iget-object v1, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@d
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    #@10
    move-result v0

    #@11
    .line 310
    .local v0, nbytes:I
    const/4 v1, -0x1

    #@12
    if-ne v0, v1, :cond_1d

    #@14
    .line 311
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@16
    iget-object v2, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@18
    const/4 v3, 0x1

    #@19
    invoke-static {v1, v2, v4, v4, v3}, Landroid/speech/srec/Recognizer;->SR_RecognizerPutAudio(I[BIIZ)I

    #@1c
    .line 317
    :cond_1c
    return-void

    #@1d
    .line 314
    :cond_1d
    iget v1, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@1f
    iget-object v2, p0, Landroid/speech/srec/Recognizer;->mPutAudioBuffer:[B

    #@21
    invoke-static {v1, v2, v4, v0, v4}, Landroid/speech/srec/Recognizer;->SR_RecognizerPutAudio(I[BIIZ)I

    #@24
    move-result v1

    #@25
    if-eq v0, v1, :cond_1c

    #@27
    .line 315
    new-instance v1, Ljava/io/IOException;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "SR_RecognizerPutAudio failed nbytes="

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3f
    throw v1
.end method

.method public resetAcousticState()V
    .registers 2

    #@0
    .prologue
    .line 376
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_AcousticStateReset(I)V

    #@5
    .line 377
    return-void
.end method

.method public setAcousticState(Ljava/lang/String;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 386
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0, p1}, Landroid/speech/srec/Recognizer;->SR_AcousticStateSet(ILjava/lang/String;)V

    #@5
    .line 387
    return-void
.end method

.method public start()V
    .registers 5

    #@0
    .prologue
    .line 260
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    iget-object v1, p0, Landroid/speech/srec/Recognizer;->mActiveGrammar:Landroid/speech/srec/Recognizer$Grammar;

    #@4
    invoke-static {v1}, Landroid/speech/srec/Recognizer$Grammar;->access$1100(Landroid/speech/srec/Recognizer$Grammar;)I

    #@7
    move-result v1

    #@8
    const-string/jumbo v2, "trash"

    #@b
    const/4 v3, 0x1

    #@c
    invoke-static {v0, v1, v2, v3}, Landroid/speech/srec/Recognizer;->SR_RecognizerActivateRule(IILjava/lang/String;I)V

    #@f
    .line 261
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@11
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerStart(I)V

    #@14
    .line 262
    return-void
.end method

.method public stop()V
    .registers 4

    #@0
    .prologue
    .line 366
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->SR_RecognizerStop(I)V

    #@5
    .line 367
    iget v0, p0, Landroid/speech/srec/Recognizer;->mRecognizer:I

    #@7
    iget-object v1, p0, Landroid/speech/srec/Recognizer;->mActiveGrammar:Landroid/speech/srec/Recognizer$Grammar;

    #@9
    invoke-static {v1}, Landroid/speech/srec/Recognizer$Grammar;->access$1100(Landroid/speech/srec/Recognizer$Grammar;)I

    #@c
    move-result v1

    #@d
    const-string/jumbo v2, "trash"

    #@10
    invoke-static {v0, v1, v2}, Landroid/speech/srec/Recognizer;->SR_RecognizerDeactivateRule(IILjava/lang/String;)V

    #@13
    .line 368
    return-void
.end method
