.class public Landroid/speech/srec/WaveHeader;
.super Ljava/lang/Object;
.source "WaveHeader.java"


# static fields
.field public static final FORMAT_ALAW:S = 0x6s

.field public static final FORMAT_PCM:S = 0x1s

.field public static final FORMAT_ULAW:S = 0x7s

.field private static final HEADER_LENGTH:I = 0x2c

.field private static final TAG:Ljava/lang/String; = "WaveHeader"


# instance fields
.field private mBitsPerSample:S

.field private mFormat:S

.field private mNumBytes:I

.field private mNumChannels:S

.field private mSampleRate:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 62
    return-void
.end method

.method public constructor <init>(SSISI)V
    .registers 6
    .parameter "format"
    .parameter "numChannels"
    .parameter "sampleRate"
    .parameter "bitsPerSample"
    .parameter "numBytes"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    iput-short p1, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@5
    .line 75
    iput p3, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@7
    .line 76
    iput-short p2, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@9
    .line 77
    iput-short p4, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@b
    .line 78
    iput p5, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@d
    .line 79
    return-void
.end method

.method private static readId(Ljava/io/InputStream;Ljava/lang/String;)V
    .registers 6
    .parameter "in"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 211
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    if-ge v0, v1, :cond_2d

    #@7
    .line 212
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v1

    #@b
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@e
    move-result v2

    #@f
    if-eq v1, v2, :cond_2a

    #@11
    new-instance v1, Ljava/io/IOException;

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, " tag not present"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@29
    throw v1

    #@2a
    .line 211
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    #@2c
    goto :goto_1

    #@2d
    .line 214
    :cond_2d
    return-void
.end method

.method private static readInt(Ljava/io/InputStream;)I
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 217
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@7
    move-result v1

    #@8
    shl-int/lit8 v1, v1, 0x8

    #@a
    or-int/2addr v0, v1

    #@b
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@e
    move-result v1

    #@f
    shl-int/lit8 v1, v1, 0x10

    #@11
    or-int/2addr v0, v1

    #@12
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@15
    move-result v1

    #@16
    shl-int/lit8 v1, v1, 0x18

    #@18
    or-int/2addr v0, v1

    #@19
    return v0
.end method

.method private static readShort(Ljava/io/InputStream;)S
    .registers 3
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    #@7
    move-result v1

    #@8
    shl-int/lit8 v1, v1, 0x8

    #@a
    or-int/2addr v0, v1

    #@b
    int-to-short v0, v0

    #@c
    return v0
.end method

.method private static writeId(Ljava/io/OutputStream;Ljava/lang/String;)V
    .registers 4
    .parameter "out"
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 254
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    if-ge v0, v1, :cond_11

    #@7
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    #@a
    move-result v1

    #@b
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_1

    #@11
    .line 255
    :cond_11
    return-void
.end method

.method private static writeInt(Ljava/io/OutputStream;I)V
    .registers 3
    .parameter "out"
    .parameter "val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 258
    shr-int/lit8 v0, p1, 0x0

    #@2
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@5
    .line 259
    shr-int/lit8 v0, p1, 0x8

    #@7
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@a
    .line 260
    shr-int/lit8 v0, p1, 0x10

    #@c
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@f
    .line 261
    shr-int/lit8 v0, p1, 0x18

    #@11
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@14
    .line 262
    return-void
.end method

.method private static writeShort(Ljava/io/OutputStream;S)V
    .registers 3
    .parameter "out"
    .parameter "val"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 265
    shr-int/lit8 v0, p1, 0x0

    #@2
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@5
    .line 266
    shr-int/lit8 v0, p1, 0x8

    #@7
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    #@a
    .line 267
    return-void
.end method


# virtual methods
.method public getBitsPerSample()S
    .registers 2

    #@0
    .prologue
    .line 143
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@2
    return v0
.end method

.method public getFormat()S
    .registers 2

    #@0
    .prologue
    .line 87
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@2
    return v0
.end method

.method public getNumBytes()I
    .registers 2

    #@0
    .prologue
    .line 162
    iget v0, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@2
    return v0
.end method

.method public getNumChannels()S
    .registers 2

    #@0
    .prologue
    .line 106
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@2
    return v0
.end method

.method public getSampleRate()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@2
    return v0
.end method

.method public read(Ljava/io/InputStream;)I
    .registers 7
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    const-string v3, "RIFF"

    #@2
    invoke-static {p1, v3}, Landroid/speech/srec/WaveHeader;->readId(Ljava/io/InputStream;Ljava/lang/String;)V

    #@5
    .line 184
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readInt(Ljava/io/InputStream;)I

    #@8
    move-result v3

    #@9
    add-int/lit8 v2, v3, -0x24

    #@b
    .line 185
    .local v2, numBytes:I
    const-string v3, "WAVE"

    #@d
    invoke-static {p1, v3}, Landroid/speech/srec/WaveHeader;->readId(Ljava/io/InputStream;Ljava/lang/String;)V

    #@10
    .line 188
    const-string v3, "fmt "

    #@12
    invoke-static {p1, v3}, Landroid/speech/srec/WaveHeader;->readId(Ljava/io/InputStream;Ljava/lang/String;)V

    #@15
    .line 189
    const/16 v3, 0x10

    #@17
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readInt(Ljava/io/InputStream;)I

    #@1a
    move-result v4

    #@1b
    if-eq v3, v4, :cond_25

    #@1d
    new-instance v3, Ljava/io/IOException;

    #@1f
    const-string v4, "fmt chunk length not 16"

    #@21
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@24
    throw v3

    #@25
    .line 190
    :cond_25
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readShort(Ljava/io/InputStream;)S

    #@28
    move-result v3

    #@29
    iput-short v3, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@2b
    .line 191
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readShort(Ljava/io/InputStream;)S

    #@2e
    move-result v3

    #@2f
    iput-short v3, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@31
    .line 192
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readInt(Ljava/io/InputStream;)I

    #@34
    move-result v3

    #@35
    iput v3, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@37
    .line 193
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readInt(Ljava/io/InputStream;)I

    #@3a
    move-result v1

    #@3b
    .line 194
    .local v1, byteRate:I
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readShort(Ljava/io/InputStream;)S

    #@3e
    move-result v0

    #@3f
    .line 195
    .local v0, blockAlign:S
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readShort(Ljava/io/InputStream;)S

    #@42
    move-result v3

    #@43
    iput-short v3, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@45
    .line 196
    iget-short v3, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@47
    iget v4, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@49
    mul-int/2addr v3, v4

    #@4a
    iget-short v4, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@4c
    mul-int/2addr v3, v4

    #@4d
    div-int/lit8 v3, v3, 0x8

    #@4f
    if-eq v1, v3, :cond_59

    #@51
    .line 197
    new-instance v3, Ljava/io/IOException;

    #@53
    const-string v4, "fmt.ByteRate field inconsistent"

    #@55
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@58
    throw v3

    #@59
    .line 199
    :cond_59
    iget-short v3, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@5b
    iget-short v4, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@5d
    mul-int/2addr v3, v4

    #@5e
    div-int/lit8 v3, v3, 0x8

    #@60
    if-eq v0, v3, :cond_6a

    #@62
    .line 200
    new-instance v3, Ljava/io/IOException;

    #@64
    const-string v4, "fmt.BlockAlign field inconsistent"

    #@66
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@69
    throw v3

    #@6a
    .line 204
    :cond_6a
    const-string v3, "data"

    #@6c
    invoke-static {p1, v3}, Landroid/speech/srec/WaveHeader;->readId(Ljava/io/InputStream;Ljava/lang/String;)V

    #@6f
    .line 205
    invoke-static {p1}, Landroid/speech/srec/WaveHeader;->readInt(Ljava/io/InputStream;)I

    #@72
    move-result v3

    #@73
    iput v3, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@75
    .line 207
    const/16 v3, 0x2c

    #@77
    return v3
.end method

.method public setBitsPerSample(S)Landroid/speech/srec/WaveHeader;
    .registers 2
    .parameter "bitsPerSample"

    #@0
    .prologue
    .line 153
    iput-short p1, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@2
    .line 154
    return-object p0
.end method

.method public setFormat(S)Landroid/speech/srec/WaveHeader;
    .registers 2
    .parameter "format"

    #@0
    .prologue
    .line 97
    iput-short p1, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@2
    .line 98
    return-object p0
.end method

.method public setNumBytes(I)Landroid/speech/srec/WaveHeader;
    .registers 2
    .parameter "numBytes"

    #@0
    .prologue
    .line 171
    iput p1, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@2
    .line 172
    return-object p0
.end method

.method public setNumChannels(S)Landroid/speech/srec/WaveHeader;
    .registers 2
    .parameter "numChannels"

    #@0
    .prologue
    .line 115
    iput-short p1, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@2
    .line 116
    return-object p0
.end method

.method public setSampleRate(I)Landroid/speech/srec/WaveHeader;
    .registers 2
    .parameter "sampleRate"

    #@0
    .prologue
    .line 133
    iput p1, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@2
    .line 134
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 271
    const-string v0, "WaveHeader format=%d numChannels=%d sampleRate=%d bitsPerSample=%d numBytes=%d"

    #@2
    const/4 v1, 0x5

    #@3
    new-array v1, v1, [Ljava/lang/Object;

    #@5
    const/4 v2, 0x0

    #@6
    iget-short v3, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@8
    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v1, v2

    #@e
    const/4 v2, 0x1

    #@f
    iget-short v3, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@11
    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v1, v2

    #@17
    const/4 v2, 0x2

    #@18
    iget v3, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@1a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v3

    #@1e
    aput-object v3, v1, v2

    #@20
    const/4 v2, 0x3

    #@21
    iget-short v3, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@23
    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    #@26
    move-result-object v3

    #@27
    aput-object v3, v1, v2

    #@29
    const/4 v2, 0x4

    #@2a
    iget v3, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@2c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2f
    move-result-object v3

    #@30
    aput-object v3, v1, v2

    #@32
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@35
    move-result-object v0

    #@36
    return-object v0
.end method

.method public write(Ljava/io/OutputStream;)I
    .registers 4
    .parameter "out"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 232
    const-string v0, "RIFF"

    #@2
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeId(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@5
    .line 233
    iget v0, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@7
    add-int/lit8 v0, v0, 0x24

    #@9
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeInt(Ljava/io/OutputStream;I)V

    #@c
    .line 234
    const-string v0, "WAVE"

    #@e
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeId(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@11
    .line 237
    const-string v0, "fmt "

    #@13
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeId(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@16
    .line 238
    const/16 v0, 0x10

    #@18
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeInt(Ljava/io/OutputStream;I)V

    #@1b
    .line 239
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mFormat:S

    #@1d
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeShort(Ljava/io/OutputStream;S)V

    #@20
    .line 240
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@22
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeShort(Ljava/io/OutputStream;S)V

    #@25
    .line 241
    iget v0, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@27
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeInt(Ljava/io/OutputStream;I)V

    #@2a
    .line 242
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@2c
    iget v1, p0, Landroid/speech/srec/WaveHeader;->mSampleRate:I

    #@2e
    mul-int/2addr v0, v1

    #@2f
    iget-short v1, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@31
    mul-int/2addr v0, v1

    #@32
    div-int/lit8 v0, v0, 0x8

    #@34
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeInt(Ljava/io/OutputStream;I)V

    #@37
    .line 243
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mNumChannels:S

    #@39
    iget-short v1, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@3b
    mul-int/2addr v0, v1

    #@3c
    div-int/lit8 v0, v0, 0x8

    #@3e
    int-to-short v0, v0

    #@3f
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeShort(Ljava/io/OutputStream;S)V

    #@42
    .line 244
    iget-short v0, p0, Landroid/speech/srec/WaveHeader;->mBitsPerSample:S

    #@44
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeShort(Ljava/io/OutputStream;S)V

    #@47
    .line 247
    const-string v0, "data"

    #@49
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeId(Ljava/io/OutputStream;Ljava/lang/String;)V

    #@4c
    .line 248
    iget v0, p0, Landroid/speech/srec/WaveHeader;->mNumBytes:I

    #@4e
    invoke-static {p1, v0}, Landroid/speech/srec/WaveHeader;->writeInt(Ljava/io/OutputStream;I)V

    #@51
    .line 250
    const/16 v0, 0x2c

    #@53
    return v0
.end method
