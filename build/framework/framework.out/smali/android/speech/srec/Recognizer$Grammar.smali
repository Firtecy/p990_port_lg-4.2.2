.class public Landroid/speech/srec/Recognizer$Grammar;
.super Ljava/lang/Object;
.source "Recognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/srec/Recognizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Grammar"
.end annotation


# instance fields
.field private mGrammar:I

.field final synthetic this$0:Landroid/speech/srec/Recognizer;


# direct methods
.method public constructor <init>(Landroid/speech/srec/Recognizer;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter "g2gFileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    iput-object p1, p0, Landroid/speech/srec/Recognizer$Grammar;->this$0:Landroid/speech/srec/Recognizer;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 177
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@8
    .line 184
    invoke-static {p2}, Landroid/speech/srec/Recognizer;->access$000(Ljava/lang/String;)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@e
    .line 185
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@10
    invoke-static {p1}, Landroid/speech/srec/Recognizer;->access$100(Landroid/speech/srec/Recognizer;)I

    #@13
    move-result v1

    #@14
    invoke-static {v0, v1}, Landroid/speech/srec/Recognizer;->access$200(II)V

    #@17
    .line 186
    return-void
.end method

.method static synthetic access$1100(Landroid/speech/srec/Recognizer$Grammar;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 176
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    return v0
.end method


# virtual methods
.method public addWordToSlot(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 12
    .parameter "slot"
    .parameter "word"
    .parameter "pron"
    .parameter "weight"
    .parameter "tag"

    #@0
    .prologue
    .line 205
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-object v5, p5

    #@7
    invoke-static/range {v0 .. v5}, Landroid/speech/srec/Recognizer;->access$400(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@a
    .line 206
    return-void
.end method

.method public compile()V
    .registers 2

    #@0
    .prologue
    .line 212
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->access$500(I)V

    #@5
    .line 213
    return-void
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 238
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 239
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@6
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->access$1000(I)V

    #@9
    .line 240
    const/4 v0, 0x0

    #@a
    iput v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@c
    .line 242
    :cond_c
    return-void
.end method

.method protected finalize()V
    .registers 3

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    if-eqz v0, :cond_10

    #@4
    .line 249
    invoke-virtual {p0}, Landroid/speech/srec/Recognizer$Grammar;->destroy()V

    #@7
    .line 250
    new-instance v0, Ljava/lang/IllegalStateException;

    #@9
    const-string/jumbo v1, "someone forgot to destroy Grammar"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 252
    :cond_10
    return-void
.end method

.method public resetAllSlots()V
    .registers 2

    #@0
    .prologue
    .line 192
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    invoke-static {v0}, Landroid/speech/srec/Recognizer;->access$300(I)V

    #@5
    .line 193
    return-void
.end method

.method public save(Ljava/lang/String;)V
    .registers 3
    .parameter "g2gFileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 230
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    invoke-static {v0, p1}, Landroid/speech/srec/Recognizer;->access$900(ILjava/lang/String;)V

    #@5
    .line 231
    return-void
.end method

.method public setupRecognizer()V
    .registers 3

    #@0
    .prologue
    .line 219
    iget v0, p0, Landroid/speech/srec/Recognizer$Grammar;->mGrammar:I

    #@2
    iget-object v1, p0, Landroid/speech/srec/Recognizer$Grammar;->this$0:Landroid/speech/srec/Recognizer;

    #@4
    invoke-static {v1}, Landroid/speech/srec/Recognizer;->access$600(Landroid/speech/srec/Recognizer;)I

    #@7
    move-result v1

    #@8
    invoke-static {v0, v1}, Landroid/speech/srec/Recognizer;->access$700(II)V

    #@b
    .line 220
    iget-object v0, p0, Landroid/speech/srec/Recognizer$Grammar;->this$0:Landroid/speech/srec/Recognizer;

    #@d
    invoke-static {v0, p0}, Landroid/speech/srec/Recognizer;->access$802(Landroid/speech/srec/Recognizer;Landroid/speech/srec/Recognizer$Grammar;)Landroid/speech/srec/Recognizer$Grammar;

    #@10
    .line 221
    return-void
.end method
