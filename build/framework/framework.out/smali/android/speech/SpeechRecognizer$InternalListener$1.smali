.class Landroid/speech/SpeechRecognizer$InternalListener$1;
.super Landroid/os/Handler;
.source "SpeechRecognizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/SpeechRecognizer$InternalListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/speech/SpeechRecognizer$InternalListener;


# direct methods
.method constructor <init>(Landroid/speech/SpeechRecognizer$InternalListener;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 419
    iput-object p1, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 422
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@2
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_9

    #@8
    .line 454
    :goto_8
    return-void

    #@9
    .line 425
    :cond_9
    iget v0, p1, Landroid/os/Message;->what:I

    #@b
    packed-switch v0, :pswitch_data_92

    #@e
    goto :goto_8

    #@f
    .line 427
    :pswitch_f
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@11
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@14
    move-result-object v0

    #@15
    invoke-interface {v0}, Landroid/speech/RecognitionListener;->onBeginningOfSpeech()V

    #@18
    goto :goto_8

    #@19
    .line 430
    :pswitch_19
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@1b
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@1e
    move-result-object v1

    #@1f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@21
    check-cast v0, [B

    #@23
    check-cast v0, [B

    #@25
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onBufferReceived([B)V

    #@28
    goto :goto_8

    #@29
    .line 433
    :pswitch_29
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@2b
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@2e
    move-result-object v0

    #@2f
    invoke-interface {v0}, Landroid/speech/RecognitionListener;->onEndOfSpeech()V

    #@32
    goto :goto_8

    #@33
    .line 436
    :pswitch_33
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@35
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@38
    move-result-object v1

    #@39
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3b
    check-cast v0, Ljava/lang/Integer;

    #@3d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@40
    move-result v0

    #@41
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onError(I)V

    #@44
    goto :goto_8

    #@45
    .line 439
    :pswitch_45
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@47
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@4a
    move-result-object v1

    #@4b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d
    check-cast v0, Landroid/os/Bundle;

    #@4f
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onReadyForSpeech(Landroid/os/Bundle;)V

    #@52
    goto :goto_8

    #@53
    .line 442
    :pswitch_53
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@55
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@58
    move-result-object v1

    #@59
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5b
    check-cast v0, Landroid/os/Bundle;

    #@5d
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onResults(Landroid/os/Bundle;)V

    #@60
    goto :goto_8

    #@61
    .line 445
    :pswitch_61
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@63
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@66
    move-result-object v1

    #@67
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    check-cast v0, Landroid/os/Bundle;

    #@6b
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onPartialResults(Landroid/os/Bundle;)V

    #@6e
    goto :goto_8

    #@6f
    .line 448
    :pswitch_6f
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@71
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@74
    move-result-object v1

    #@75
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@77
    check-cast v0, Ljava/lang/Float;

    #@79
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    #@7c
    move-result v0

    #@7d
    invoke-interface {v1, v0}, Landroid/speech/RecognitionListener;->onRmsChanged(F)V

    #@80
    goto :goto_8

    #@81
    .line 451
    :pswitch_81
    iget-object v0, p0, Landroid/speech/SpeechRecognizer$InternalListener$1;->this$1:Landroid/speech/SpeechRecognizer$InternalListener;

    #@83
    invoke-static {v0}, Landroid/speech/SpeechRecognizer$InternalListener;->access$1000(Landroid/speech/SpeechRecognizer$InternalListener;)Landroid/speech/RecognitionListener;

    #@86
    move-result-object v1

    #@87
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@89
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8b
    check-cast v0, Landroid/os/Bundle;

    #@8d
    invoke-interface {v1, v2, v0}, Landroid/speech/RecognitionListener;->onEvent(ILandroid/os/Bundle;)V

    #@90
    goto/16 :goto_8

    #@92
    .line 425
    :pswitch_data_92
    .packed-switch 0x1
        :pswitch_f
        :pswitch_19
        :pswitch_29
        :pswitch_33
        :pswitch_45
        :pswitch_53
        :pswitch_61
        :pswitch_6f
        :pswitch_81
    .end packed-switch
.end method
