.class Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;
.super Ljava/lang/Object;
.source "SpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/textservice/SpellCheckerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SentenceLevelAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;,
        Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;
    }
.end annotation


# static fields
.field public static final EMPTY_SENTENCE_SUGGESTIONS_INFOS:[Landroid/view/textservice/SentenceSuggestionsInfo;

.field private static final EMPTY_SUGGESTIONS_INFO:Landroid/view/textservice/SuggestionsInfo;


# instance fields
.field private final mWordIterator:Landroid/text/method/WordIterator;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 334
    new-array v0, v2, [Landroid/view/textservice/SentenceSuggestionsInfo;

    #@3
    sput-object v0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->EMPTY_SENTENCE_SUGGESTIONS_INFOS:[Landroid/view/textservice/SentenceSuggestionsInfo;

    #@5
    .line 336
    new-instance v0, Landroid/view/textservice/SuggestionsInfo;

    #@7
    const/4 v1, 0x0

    #@8
    invoke-direct {v0, v2, v1}, Landroid/view/textservice/SuggestionsInfo;-><init>(I[Ljava/lang/String;)V

    #@b
    sput-object v0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->EMPTY_SUGGESTIONS_INFO:Landroid/view/textservice/SuggestionsInfo;

    #@d
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .registers 3
    .parameter "locale"

    #@0
    .prologue
    .line 366
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 367
    new-instance v0, Landroid/text/method/WordIterator;

    #@5
    invoke-direct {v0, p1}, Landroid/text/method/WordIterator;-><init>(Ljava/util/Locale;)V

    #@8
    iput-object v0, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->mWordIterator:Landroid/text/method/WordIterator;

    #@a
    .line 368
    return-void
.end method

.method static synthetic access$000(Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;Landroid/view/textservice/TextInfo;)Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 333
    invoke-direct {p0, p1}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->getSplitWords(Landroid/view/textservice/TextInfo;)Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private getSplitWords(Landroid/view/textservice/TextInfo;)Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;
    .registers 15
    .parameter "originalTextInfo"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, -0x1

    #@2
    .line 371
    iget-object v8, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->mWordIterator:Landroid/text/method/WordIterator;

    #@4
    .line 372
    .local v8, wordIterator:Landroid/text/method/WordIterator;
    invoke-virtual {p1}, Landroid/view/textservice/TextInfo;->getText()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 373
    .local v2, originalText:Ljava/lang/CharSequence;
    invoke-virtual {p1}, Landroid/view/textservice/TextInfo;->getCookie()I

    #@b
    move-result v0

    #@c
    .line 374
    .local v0, cookie:I
    const/4 v4, 0x0

    #@d
    .line 375
    .local v4, start:I
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@10
    move-result v1

    #@11
    .line 376
    .local v1, end:I
    new-instance v7, Ljava/util/ArrayList;

    #@13
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@16
    .line 377
    .local v7, wordItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;>;"
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@19
    move-result v10

    #@1a
    invoke-virtual {v8, v2, v12, v10}, Landroid/text/method/WordIterator;->setCharSequence(Ljava/lang/CharSequence;II)V

    #@1d
    .line 378
    invoke-virtual {v8, v12}, Landroid/text/method/WordIterator;->following(I)I

    #@20
    move-result v6

    #@21
    .line 379
    .local v6, wordEnd:I
    invoke-virtual {v8, v6}, Landroid/text/method/WordIterator;->getBeginning(I)I

    #@24
    move-result v9

    #@25
    .line 385
    .local v9, wordStart:I
    :goto_25
    if-gt v9, v1, :cond_4e

    #@27
    if-eq v6, v11, :cond_4e

    #@29
    if-eq v9, v11, :cond_4e

    #@2b
    .line 386
    if-ltz v6, :cond_48

    #@2d
    if-le v6, v9, :cond_48

    #@2f
    .line 387
    invoke-interface {v2, v9, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    #@32
    move-result-object v10

    #@33
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    .line 388
    .local v3, query:Ljava/lang/String;
    new-instance v5, Landroid/view/textservice/TextInfo;

    #@39
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    #@3c
    move-result v10

    #@3d
    invoke-direct {v5, v3, v0, v10}, Landroid/view/textservice/TextInfo;-><init>(Ljava/lang/String;II)V

    #@40
    .line 389
    .local v5, ti:Landroid/view/textservice/TextInfo;
    new-instance v10, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;

    #@42
    invoke-direct {v10, v5, v9, v6}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;-><init>(Landroid/view/textservice/TextInfo;II)V

    #@45
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@48
    .line 394
    .end local v3           #query:Ljava/lang/String;
    .end local v5           #ti:Landroid/view/textservice/TextInfo;
    :cond_48
    invoke-virtual {v8, v6}, Landroid/text/method/WordIterator;->following(I)I

    #@4b
    move-result v6

    #@4c
    .line 395
    if-ne v6, v11, :cond_54

    #@4e
    .line 400
    :cond_4e
    new-instance v10, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;

    #@50
    invoke-direct {v10, p1, v7}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;-><init>(Landroid/view/textservice/TextInfo;Ljava/util/ArrayList;)V

    #@53
    return-object v10

    #@54
    .line 398
    :cond_54
    invoke-virtual {v8, v6}, Landroid/text/method/WordIterator;->getBeginning(I)I

    #@57
    move-result v9

    #@58
    goto :goto_25
.end method

.method public static reconstructSuggestions(Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;[Landroid/view/textservice/SuggestionsInfo;)Landroid/view/textservice/SentenceSuggestionsInfo;
    .registers 15
    .parameter "originalTextInfoParams"
    .parameter "results"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 405
    if-eqz p1, :cond_6

    #@3
    array-length v12, p1

    #@4
    if-nez v12, :cond_7

    #@6
    .line 447
    :cond_6
    :goto_6
    return-object v11

    #@7
    .line 411
    :cond_7
    if-eqz p0, :cond_6

    #@9
    .line 417
    iget-object v11, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;->mOriginalTextInfo:Landroid/view/textservice/TextInfo;

    #@b
    invoke-virtual {v11}, Landroid/view/textservice/TextInfo;->getCookie()I

    #@e
    move-result v6

    #@f
    .line 418
    .local v6, originalCookie:I
    iget-object v11, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;->mOriginalTextInfo:Landroid/view/textservice/TextInfo;

    #@11
    invoke-virtual {v11}, Landroid/view/textservice/TextInfo;->getSequence()I

    #@14
    move-result v7

    #@15
    .line 421
    .local v7, originalSequence:I
    iget v8, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;->mSize:I

    #@17
    .line 422
    .local v8, querySize:I
    new-array v5, v8, [I

    #@19
    .line 423
    .local v5, offsets:[I
    new-array v4, v8, [I

    #@1b
    .line 424
    .local v4, lengths:[I
    new-array v9, v8, [Landroid/view/textservice/SuggestionsInfo;

    #@1d
    .line 425
    .local v9, reconstructedSuggestions:[Landroid/view/textservice/SuggestionsInfo;
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    if-ge v1, v8, :cond_56

    #@20
    .line 426
    iget-object v11, p0, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;->mItems:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;

    #@28
    .line 427
    .local v2, item:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;
    const/4 v10, 0x0

    #@29
    .line 428
    .local v10, result:Landroid/view/textservice/SuggestionsInfo;
    const/4 v3, 0x0

    #@2a
    .local v3, j:I
    :goto_2a
    array-length v11, p1

    #@2b
    if-ge v3, v11, :cond_41

    #@2d
    .line 429
    aget-object v0, p1, v3

    #@2f
    .line 430
    .local v0, cur:Landroid/view/textservice/SuggestionsInfo;
    if-eqz v0, :cond_50

    #@31
    invoke-virtual {v0}, Landroid/view/textservice/SuggestionsInfo;->getSequence()I

    #@34
    move-result v11

    #@35
    iget-object v12, v2, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;->mTextInfo:Landroid/view/textservice/TextInfo;

    #@37
    invoke-virtual {v12}, Landroid/view/textservice/TextInfo;->getSequence()I

    #@3a
    move-result v12

    #@3b
    if-ne v11, v12, :cond_50

    #@3d
    .line 431
    move-object v10, v0

    #@3e
    .line 432
    invoke-virtual {v10, v6, v7}, Landroid/view/textservice/SuggestionsInfo;->setCookieAndSequence(II)V

    #@41
    .line 436
    .end local v0           #cur:Landroid/view/textservice/SuggestionsInfo;
    :cond_41
    iget v11, v2, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;->mStart:I

    #@43
    aput v11, v5, v1

    #@45
    .line 437
    iget v11, v2, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;->mLength:I

    #@47
    aput v11, v4, v1

    #@49
    .line 438
    if-eqz v10, :cond_53

    #@4b
    .end local v10           #result:Landroid/view/textservice/SuggestionsInfo;
    :goto_4b
    aput-object v10, v9, v1

    #@4d
    .line 425
    add-int/lit8 v1, v1, 0x1

    #@4f
    goto :goto_1e

    #@50
    .line 428
    .restart local v0       #cur:Landroid/view/textservice/SuggestionsInfo;
    .restart local v10       #result:Landroid/view/textservice/SuggestionsInfo;
    :cond_50
    add-int/lit8 v3, v3, 0x1

    #@52
    goto :goto_2a

    #@53
    .line 438
    .end local v0           #cur:Landroid/view/textservice/SuggestionsInfo;
    :cond_53
    sget-object v10, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->EMPTY_SUGGESTIONS_INFO:Landroid/view/textservice/SuggestionsInfo;

    #@55
    goto :goto_4b

    #@56
    .line 447
    .end local v2           #item:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;
    .end local v3           #j:I
    .end local v10           #result:Landroid/view/textservice/SuggestionsInfo;
    :cond_56
    new-instance v11, Landroid/view/textservice/SentenceSuggestionsInfo;

    #@58
    invoke-direct {v11, v9, v5, v4}, Landroid/view/textservice/SentenceSuggestionsInfo;-><init>([Landroid/view/textservice/SuggestionsInfo;[I[I)V

    #@5b
    goto :goto_6
.end method
