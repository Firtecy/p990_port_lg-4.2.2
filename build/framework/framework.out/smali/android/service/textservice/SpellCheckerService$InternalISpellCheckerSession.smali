.class Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;
.super Lcom/android/internal/textservice/ISpellCheckerSession$Stub;
.source "SpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/textservice/SpellCheckerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InternalISpellCheckerSession"
.end annotation


# instance fields
.field private final mBundle:Landroid/os/Bundle;

.field private mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

.field private final mLocale:Ljava/lang/String;

.field private final mSession:Landroid/service/textservice/SpellCheckerService$Session;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;Landroid/service/textservice/SpellCheckerService$Session;)V
    .registers 5
    .parameter "locale"
    .parameter "listener"
    .parameter "bundle"
    .parameter "session"

    #@0
    .prologue
    .line 244
    invoke-direct {p0}, Lcom/android/internal/textservice/ISpellCheckerSession$Stub;-><init>()V

    #@3
    .line 245
    iput-object p2, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@5
    .line 246
    iput-object p4, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mSession:Landroid/service/textservice/SpellCheckerService$Session;

    #@7
    .line 247
    iput-object p1, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mLocale:Ljava/lang/String;

    #@9
    .line 248
    iput-object p3, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mBundle:Landroid/os/Bundle;

    #@b
    .line 249
    invoke-virtual {p4, p0}, Landroid/service/textservice/SpellCheckerService$Session;->setInternalISpellCheckerSession(Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;)V

    #@e
    .line 250
    return-void
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 304
    iget-object v0, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mBundle:Landroid/os/Bundle;

    #@2
    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 300
    iget-object v0, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mLocale:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public onCancel()V
    .registers 3

    #@0
    .prologue
    .line 278
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@3
    move-result v1

    #@4
    invoke-static {v1}, Landroid/os/Process;->getThreadPriority(I)I

    #@7
    move-result v0

    #@8
    .line 280
    .local v0, pri:I
    const/16 v1, 0xa

    #@a
    :try_start_a
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@d
    .line 281
    iget-object v1, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mSession:Landroid/service/textservice/SpellCheckerService$Session;

    #@f
    invoke-virtual {v1}, Landroid/service/textservice/SpellCheckerService$Session;->onCancel()V
    :try_end_12
    .catchall {:try_start_a .. :try_end_12} :catchall_16

    #@12
    .line 283
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@15
    .line 285
    return-void

    #@16
    .line 283
    :catchall_16
    move-exception v1

    #@17
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@1a
    throw v1
.end method

.method public onClose()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 289
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@4
    move-result v1

    #@5
    invoke-static {v1}, Landroid/os/Process;->getThreadPriority(I)I

    #@8
    move-result v0

    #@9
    .line 291
    .local v0, pri:I
    const/16 v1, 0xa

    #@b
    :try_start_b
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@e
    .line 292
    iget-object v1, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mSession:Landroid/service/textservice/SpellCheckerService$Session;

    #@10
    invoke-virtual {v1}, Landroid/service/textservice/SpellCheckerService$Session;->onClose()V
    :try_end_13
    .catchall {:try_start_b .. :try_end_13} :catchall_19

    #@13
    .line 294
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@16
    .line 295
    iput-object v2, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@18
    .line 297
    return-void

    #@19
    .line 294
    :catchall_19
    move-exception v1

    #@1a
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@1d
    .line 295
    iput-object v2, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@1f
    .line 294
    throw v1
.end method

.method public onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)V
    .registers 5
    .parameter "textInfos"
    .parameter "suggestionsLimit"

    #@0
    .prologue
    .line 270
    :try_start_0
    iget-object v0, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@2
    iget-object v1, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mSession:Landroid/service/textservice/SpellCheckerService$Session;

    #@4
    invoke-virtual {v1, p1, p2}, Landroid/service/textservice/SpellCheckerService$Session;->onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)[Landroid/view/textservice/SentenceSuggestionsInfo;

    #@7
    move-result-object v1

    #@8
    invoke-interface {v0, v1}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->onGetSentenceSuggestions([Landroid/view/textservice/SentenceSuggestionsInfo;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 274
    :goto_b
    return-void

    #@c
    .line 272
    :catch_c
    move-exception v0

    #@d
    goto :goto_b
.end method

.method public onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)V
    .registers 7
    .parameter "textInfos"
    .parameter "suggestionsLimit"
    .parameter "sequentialWords"

    #@0
    .prologue
    .line 255
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@3
    move-result v1

    #@4
    invoke-static {v1}, Landroid/os/Process;->getThreadPriority(I)I

    #@7
    move-result v0

    #@8
    .line 257
    .local v0, pri:I
    const/16 v1, 0xa

    #@a
    :try_start_a
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    #@d
    .line 258
    iget-object v1, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mListener:Lcom/android/internal/textservice/ISpellCheckerSessionListener;

    #@f
    iget-object v2, p0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->mSession:Landroid/service/textservice/SpellCheckerService$Session;

    #@11
    invoke-virtual {v2, p1, p2, p3}, Landroid/service/textservice/SpellCheckerService$Session;->onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;

    #@14
    move-result-object v2

    #@15
    invoke-interface {v1, v2}, Lcom/android/internal/textservice/ISpellCheckerSessionListener;->onGetSuggestions([Landroid/view/textservice/SuggestionsInfo;)V
    :try_end_18
    .catchall {:try_start_a .. :try_end_18} :catchall_1c
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_18} :catch_21

    #@18
    .line 263
    :goto_18
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@1b
    .line 265
    return-void

    #@1c
    .line 263
    :catchall_1c
    move-exception v1

    #@1d
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@20
    throw v1

    #@21
    .line 261
    :catch_21
    move-exception v1

    #@22
    goto :goto_18
.end method
