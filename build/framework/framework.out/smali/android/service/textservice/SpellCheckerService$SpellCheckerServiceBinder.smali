.class Landroid/service/textservice/SpellCheckerService$SpellCheckerServiceBinder;
.super Lcom/android/internal/textservice/ISpellCheckerService$Stub;
.source "SpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/textservice/SpellCheckerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SpellCheckerServiceBinder"
.end annotation


# instance fields
.field private final mInternalServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/service/textservice/SpellCheckerService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/service/textservice/SpellCheckerService;)V
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 311
    invoke-direct {p0}, Lcom/android/internal/textservice/ISpellCheckerService$Stub;-><init>()V

    #@3
    .line 312
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/service/textservice/SpellCheckerService$SpellCheckerServiceBinder;->mInternalServiceRef:Ljava/lang/ref/WeakReference;

    #@a
    .line 313
    return-void
.end method


# virtual methods
.method public getISpellCheckerSession(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;)Lcom/android/internal/textservice/ISpellCheckerSession;
    .registers 8
    .parameter "locale"
    .parameter "listener"
    .parameter "bundle"

    #@0
    .prologue
    .line 318
    iget-object v3, p0, Landroid/service/textservice/SpellCheckerService$SpellCheckerServiceBinder;->mInternalServiceRef:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    check-cast v1, Landroid/service/textservice/SpellCheckerService;

    #@8
    .line 319
    .local v1, service:Landroid/service/textservice/SpellCheckerService;
    if-nez v1, :cond_c

    #@a
    const/4 v0, 0x0

    #@b
    .line 324
    :goto_b
    return-object v0

    #@c
    .line 320
    :cond_c
    invoke-virtual {v1}, Landroid/service/textservice/SpellCheckerService;->createSession()Landroid/service/textservice/SpellCheckerService$Session;

    #@f
    move-result-object v2

    #@10
    .line 321
    .local v2, session:Landroid/service/textservice/SpellCheckerService$Session;
    new-instance v0, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;

    #@12
    invoke-direct {v0, p1, p2, p3, v2}, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;-><init>(Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;Landroid/os/Bundle;Landroid/service/textservice/SpellCheckerService$Session;)V

    #@15
    .line 323
    .local v0, internalSession:Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;
    invoke-virtual {v2}, Landroid/service/textservice/SpellCheckerService$Session;->onCreate()V

    #@18
    goto :goto_b
.end method
