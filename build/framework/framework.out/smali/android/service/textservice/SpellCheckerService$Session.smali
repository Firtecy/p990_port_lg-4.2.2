.class public abstract Landroid/service/textservice/SpellCheckerService$Session;
.super Ljava/lang/Object;
.source "SpellCheckerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/textservice/SpellCheckerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Session"
.end annotation


# instance fields
.field private mInternalSession:Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;

.field private volatile mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 99
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .registers 2

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Landroid/service/textservice/SpellCheckerService$Session;->mInternalSession:Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;

    #@2
    invoke-virtual {v0}, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->getBundle()Landroid/os/Bundle;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 225
    iget-object v0, p0, Landroid/service/textservice/SpellCheckerService$Session;->mInternalSession:Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;

    #@2
    invoke-virtual {v0}, Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;->getLocale()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public onCancel()V
    .registers 1

    #@0
    .prologue
    .line 211
    return-void
.end method

.method public onClose()V
    .registers 1

    #@0
    .prologue
    .line 219
    return-void
.end method

.method public abstract onCreate()V
.end method

.method public onGetSentenceSuggestionsMultiple([Landroid/view/textservice/TextInfo;I)[Landroid/view/textservice/SentenceSuggestionsInfo;
    .registers 14
    .parameter "textInfos"
    .parameter "suggestionsLimit"

    #@0
    .prologue
    .line 166
    if-eqz p1, :cond_5

    #@2
    array-length v9, p1

    #@3
    if-nez v9, :cond_8

    #@5
    .line 167
    :cond_5
    sget-object v6, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->EMPTY_SENTENCE_SUGGESTIONS_INFOS:[Landroid/view/textservice/SentenceSuggestionsInfo;

    #@7
    .line 202
    :cond_7
    :goto_7
    return-object v6

    #@8
    .line 173
    :cond_8
    iget-object v9, p0, Landroid/service/textservice/SpellCheckerService$Session;->mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@a
    if-nez v9, :cond_28

    #@c
    .line 174
    monitor-enter p0

    #@d
    .line 175
    :try_start_d
    iget-object v9, p0, Landroid/service/textservice/SpellCheckerService$Session;->mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@f
    if-nez v9, :cond_27

    #@11
    .line 176
    invoke-virtual {p0}, Landroid/service/textservice/SpellCheckerService$Session;->getLocale()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    .line 177
    .local v4, localeStr:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@18
    move-result v9

    #@19
    if-nez v9, :cond_27

    #@1b
    .line 178
    new-instance v9, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@1d
    new-instance v10, Ljava/util/Locale;

    #@1f
    invoke-direct {v10, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    #@22
    invoke-direct {v9, v10}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;-><init>(Ljava/util/Locale;)V

    #@25
    iput-object v9, p0, Landroid/service/textservice/SpellCheckerService$Session;->mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@27
    .line 181
    .end local v4           #localeStr:Ljava/lang/String;
    :cond_27
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_d .. :try_end_28} :catchall_2f

    #@28
    .line 183
    :cond_28
    iget-object v9, p0, Landroid/service/textservice/SpellCheckerService$Session;->mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@2a
    if-nez v9, :cond_32

    #@2c
    .line 184
    sget-object v6, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->EMPTY_SENTENCE_SUGGESTIONS_INFOS:[Landroid/view/textservice/SentenceSuggestionsInfo;

    #@2e
    goto :goto_7

    #@2f
    .line 181
    :catchall_2f
    move-exception v9

    #@30
    :try_start_30
    monitor-exit p0
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    #@31
    throw v9

    #@32
    .line 186
    :cond_32
    array-length v1, p1

    #@33
    .line 187
    .local v1, infosSize:I
    new-array v6, v1, [Landroid/view/textservice/SentenceSuggestionsInfo;

    #@35
    .line 188
    .local v6, retval:[Landroid/view/textservice/SentenceSuggestionsInfo;
    const/4 v0, 0x0

    #@36
    .local v0, i:I
    :goto_36
    if-ge v0, v1, :cond_7

    #@38
    .line 189
    iget-object v9, p0, Landroid/service/textservice/SpellCheckerService$Session;->mSentenceLevelAdapter:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;

    #@3a
    aget-object v10, p1, v0

    #@3c
    invoke-static {v9, v10}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->access$000(Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;Landroid/view/textservice/TextInfo;)Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;

    #@3f
    move-result-object v8

    #@40
    .line 191
    .local v8, textInfoParams:Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;
    iget-object v5, v8, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;->mItems:Ljava/util/ArrayList;

    #@42
    .line 193
    .local v5, mItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v2

    #@46
    .line 194
    .local v2, itemsSize:I
    new-array v7, v2, [Landroid/view/textservice/TextInfo;

    #@48
    .line 195
    .local v7, splitTextInfos:[Landroid/view/textservice/TextInfo;
    const/4 v3, 0x0

    #@49
    .local v3, j:I
    :goto_49
    if-ge v3, v2, :cond_58

    #@4b
    .line 196
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4e
    move-result-object v9

    #@4f
    check-cast v9, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;

    #@51
    iget-object v9, v9, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceWordItem;->mTextInfo:Landroid/view/textservice/TextInfo;

    #@53
    aput-object v9, v7, v3

    #@55
    .line 195
    add-int/lit8 v3, v3, 0x1

    #@57
    goto :goto_49

    #@58
    .line 198
    :cond_58
    const/4 v9, 0x1

    #@59
    invoke-virtual {p0, v7, p2, v9}, Landroid/service/textservice/SpellCheckerService$Session;->onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;

    #@5c
    move-result-object v9

    #@5d
    invoke-static {v8, v9}, Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter;->reconstructSuggestions(Landroid/service/textservice/SpellCheckerService$SentenceLevelAdapter$SentenceTextInfoParams;[Landroid/view/textservice/SuggestionsInfo;)Landroid/view/textservice/SentenceSuggestionsInfo;

    #@60
    move-result-object v9

    #@61
    aput-object v9, v6, v0

    #@63
    .line 188
    add-int/lit8 v0, v0, 0x1

    #@65
    goto :goto_36
.end method

.method public abstract onGetSuggestions(Landroid/view/textservice/TextInfo;I)Landroid/view/textservice/SuggestionsInfo;
.end method

.method public onGetSuggestionsMultiple([Landroid/view/textservice/TextInfo;IZ)[Landroid/view/textservice/SuggestionsInfo;
    .registers 10
    .parameter "textInfos"
    .parameter "suggestionsLimit"
    .parameter "sequentialWords"

    #@0
    .prologue
    .line 140
    array-length v1, p1

    #@1
    .line 141
    .local v1, length:I
    new-array v2, v1, [Landroid/view/textservice/SuggestionsInfo;

    #@3
    .line 142
    .local v2, retval:[Landroid/view/textservice/SuggestionsInfo;
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, v1, :cond_22

    #@6
    .line 143
    aget-object v3, p1, v0

    #@8
    invoke-virtual {p0, v3, p2}, Landroid/service/textservice/SpellCheckerService$Session;->onGetSuggestions(Landroid/view/textservice/TextInfo;I)Landroid/view/textservice/SuggestionsInfo;

    #@b
    move-result-object v3

    #@c
    aput-object v3, v2, v0

    #@e
    .line 144
    aget-object v3, v2, v0

    #@10
    aget-object v4, p1, v0

    #@12
    invoke-virtual {v4}, Landroid/view/textservice/TextInfo;->getCookie()I

    #@15
    move-result v4

    #@16
    aget-object v5, p1, v0

    #@18
    invoke-virtual {v5}, Landroid/view/textservice/TextInfo;->getSequence()I

    #@1b
    move-result v5

    #@1c
    invoke-virtual {v3, v4, v5}, Landroid/view/textservice/SuggestionsInfo;->setCookieAndSequence(II)V

    #@1f
    .line 142
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_4

    #@22
    .line 147
    :cond_22
    return-object v2
.end method

.method public final setInternalISpellCheckerSession(Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;)V
    .registers 2
    .parameter "session"

    #@0
    .prologue
    .line 107
    iput-object p1, p0, Landroid/service/textservice/SpellCheckerService$Session;->mInternalSession:Landroid/service/textservice/SpellCheckerService$InternalISpellCheckerSession;

    #@2
    .line 108
    return-void
.end method
