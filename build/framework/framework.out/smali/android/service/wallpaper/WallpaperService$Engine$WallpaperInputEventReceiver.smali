.class final Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "WallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/wallpaper/WallpaperService$Engine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "WallpaperInputEventReceiver"
.end annotation


# instance fields
.field final synthetic this$1:Landroid/service/wallpaper/WallpaperService$Engine;


# direct methods
.method public constructor <init>(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .registers 4
    .parameter
    .parameter "inputChannel"
    .parameter "looper"

    #@0
    .prologue
    .line 234
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->this$1:Landroid/service/wallpaper/WallpaperService$Engine;

    #@2
    .line 235
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@5
    .line 236
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 240
    const/4 v2, 0x0

    #@1
    .line 242
    .local v2, handled:Z
    :try_start_1
    instance-of v3, p1, Landroid/view/MotionEvent;

    #@3
    if-eqz v3, :cond_1b

    #@5
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@8
    move-result v3

    #@9
    and-int/lit8 v3, v3, 0x2

    #@b
    if-eqz v3, :cond_1b

    #@d
    .line 244
    move-object v0, p1

    #@e
    check-cast v0, Landroid/view/MotionEvent;

    #@10
    move-object v3, v0

    #@11
    invoke-static {v3}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    #@14
    move-result-object v1

    #@15
    .line 245
    .local v1, dup:Landroid/view/MotionEvent;
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->this$1:Landroid/service/wallpaper/WallpaperService$Engine;

    #@17
    invoke-static {v3, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->access$000(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/MotionEvent;)V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1f

    #@1a
    .line 246
    const/4 v2, 0x1

    #@1b
    .line 249
    .end local v1           #dup:Landroid/view/MotionEvent;
    :cond_1b
    invoke-virtual {p0, p1, v2}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@1e
    .line 251
    return-void

    #@1f
    .line 249
    :catchall_1f
    move-exception v3

    #@20
    invoke-virtual {p0, p1, v2}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@23
    throw v3
.end method
