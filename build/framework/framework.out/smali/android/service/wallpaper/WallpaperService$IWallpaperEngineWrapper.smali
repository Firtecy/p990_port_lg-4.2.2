.class Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;
.super Landroid/service/wallpaper/IWallpaperEngine$Stub;
.source "WallpaperService.java"

# interfaces
.implements Lcom/android/internal/os/HandlerCaller$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/wallpaper/WallpaperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IWallpaperEngineWrapper"
.end annotation


# instance fields
.field private final mCaller:Lcom/android/internal/os/HandlerCaller;

.field final mConnection:Landroid/service/wallpaper/IWallpaperConnection;

.field mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

.field final mIsPreview:Z

.field mReqHeight:I

.field mReqWidth:I

.field mShownReported:Z

.field final mWindowToken:Landroid/os/IBinder;

.field final mWindowType:I

.field final synthetic this$0:Landroid/service/wallpaper/WallpaperService;


# direct methods
.method constructor <init>(Landroid/service/wallpaper/WallpaperService;Landroid/service/wallpaper/WallpaperService;Landroid/service/wallpaper/IWallpaperConnection;Landroid/os/IBinder;IZII)V
    .registers 12
    .parameter
    .parameter "context"
    .parameter "conn"
    .parameter "windowToken"
    .parameter "windowType"
    .parameter "isPreview"
    .parameter "reqWidth"
    .parameter "reqHeight"

    #@0
    .prologue
    .line 983
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@2
    invoke-direct {p0}, Landroid/service/wallpaper/IWallpaperEngine$Stub;-><init>()V

    #@5
    .line 987
    new-instance v2, Lcom/android/internal/os/HandlerCaller;

    #@7
    invoke-static {p1}, Landroid/service/wallpaper/WallpaperService;->access$300(Landroid/service/wallpaper/WallpaperService;)Landroid/os/Looper;

    #@a
    move-result-object v1

    #@b
    if-eqz v1, :cond_30

    #@d
    invoke-static {p1}, Landroid/service/wallpaper/WallpaperService;->access$300(Landroid/service/wallpaper/WallpaperService;)Landroid/os/Looper;

    #@10
    move-result-object v1

    #@11
    :goto_11
    invoke-direct {v2, p2, v1, p0}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/internal/os/HandlerCaller$Callback;)V

    #@14
    iput-object v2, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@16
    .line 991
    iput-object p3, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    #@18
    .line 992
    iput-object p4, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowToken:Landroid/os/IBinder;

    #@1a
    .line 993
    iput p5, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowType:I

    #@1c
    .line 994
    iput-boolean p6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mIsPreview:Z

    #@1e
    .line 995
    iput p7, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    #@20
    .line 996
    iput p8, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    #@22
    .line 998
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@24
    const/16 v2, 0xa

    #@26
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@29
    move-result-object v0

    #@2a
    .line 999
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2c
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@2f
    .line 1000
    return-void

    #@30
    .line 987
    .end local v0           #msg:Landroid/os/Message;
    :cond_30
    invoke-virtual {p2}, Landroid/service/wallpaper/WallpaperService;->getMainLooper()Landroid/os/Looper;

    #@33
    move-result-object v1

    #@34
    goto :goto_11
.end method

.method static synthetic access$200(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)Lcom/android/internal/os/HandlerCaller;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 967
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    return-object v0
.end method


# virtual methods
.method public destroy()V
    .registers 4

    #@0
    .prologue
    .line 1041
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0x14

    #@4
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 1042
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 1043
    return-void
.end method

.method public dispatchPointer(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 1014
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 1015
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@6
    #calls: Landroid/service/wallpaper/WallpaperService$Engine;->dispatchPointer(Landroid/view/MotionEvent;)V
    invoke-static {v0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->access$000(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/MotionEvent;)V

    #@9
    .line 1019
    :goto_9
    return-void

    #@a
    .line 1017
    :cond_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    #@d
    goto :goto_9
.end method

.method public dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;)V
    .registers 13
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1024
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@6
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@8
    const/4 v6, 0x0

    #@9
    move-object v1, p1

    #@a
    move v2, p2

    #@b
    move v3, p3

    #@c
    move v4, p4

    #@d
    move-object v5, p5

    #@e
    invoke-virtual/range {v0 .. v6}, Lcom/android/internal/view/BaseIWindow;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)V

    #@11
    .line 1026
    :cond_11
    return-void
.end method

.method public executeMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "message"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 1046
    iget v8, p1, Landroid/os/Message;->what:I

    #@4
    sparse-switch v8, :sswitch_data_c2

    #@7
    .line 1112
    const-string v6, "WallpaperService"

    #@9
    new-instance v7, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v8, "Unknown message type "

    #@10
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    iget v8, p1, Landroid/os/Message;->what:I

    #@16
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v7

    #@1a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v7

    #@1e
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 1114
    :goto_21
    :sswitch_21
    return-void

    #@22
    .line 1049
    :sswitch_22
    :try_start_22
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    #@24
    invoke-interface {v6, p0}, Landroid/service/wallpaper/IWallpaperConnection;->attachEngine(Landroid/service/wallpaper/IWallpaperEngine;)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_27} :catch_3c

    #@27
    .line 1054
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@29
    invoke-virtual {v6}, Landroid/service/wallpaper/WallpaperService;->onCreateEngine()Landroid/service/wallpaper/WallpaperService$Engine;

    #@2c
    move-result-object v2

    #@2d
    .line 1055
    .local v2, engine:Landroid/service/wallpaper/WallpaperService$Engine;
    iput-object v2, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@2f
    .line 1056
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@31
    invoke-static {v6}, Landroid/service/wallpaper/WallpaperService;->access$400(Landroid/service/wallpaper/WallpaperService;)Ljava/util/ArrayList;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@38
    .line 1057
    invoke-virtual {v2, p0}, Landroid/service/wallpaper/WallpaperService$Engine;->attach(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)V

    #@3b
    goto :goto_21

    #@3c
    .line 1050
    .end local v2           #engine:Landroid/service/wallpaper/WallpaperService$Engine;
    :catch_3c
    move-exception v1

    #@3d
    .line 1051
    .local v1, e:Landroid/os/RemoteException;
    const-string v6, "WallpaperService"

    #@3f
    const-string v7, "Wallpaper host disappeared"

    #@41
    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    goto :goto_21

    #@45
    .line 1061
    .end local v1           #e:Landroid/os/RemoteException;
    :sswitch_45
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@47
    invoke-static {v6}, Landroid/service/wallpaper/WallpaperService;->access$400(Landroid/service/wallpaper/WallpaperService;)Ljava/util/ArrayList;

    #@4a
    move-result-object v6

    #@4b
    iget-object v7, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@4d
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@50
    .line 1062
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@52
    invoke-virtual {v6}, Landroid/service/wallpaper/WallpaperService$Engine;->detach()V

    #@55
    goto :goto_21

    #@56
    .line 1066
    :sswitch_56
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@58
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@5a
    iget v8, p1, Landroid/os/Message;->arg2:I

    #@5c
    invoke-virtual {v6, v7, v8}, Landroid/service/wallpaper/WallpaperService$Engine;->doDesiredSizeChanged(II)V

    #@5f
    goto :goto_21

    #@60
    .line 1070
    :sswitch_60
    iget-object v8, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@62
    invoke-virtual {v8, v6, v7, v7}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@65
    goto :goto_21

    #@66
    .line 1075
    :sswitch_66
    iget-object v8, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@68
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@6a
    if-eqz v9, :cond_70

    #@6c
    :goto_6c
    invoke-virtual {v8, v6}, Landroid/service/wallpaper/WallpaperService$Engine;->doVisibilityChanged(Z)V

    #@6f
    goto :goto_21

    #@70
    :cond_70
    move v6, v7

    #@71
    goto :goto_6c

    #@72
    .line 1078
    :sswitch_72
    iget-object v7, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@74
    invoke-virtual {v7, v6}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    #@77
    goto :goto_21

    #@78
    .line 1081
    :sswitch_78
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7a
    check-cast v0, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;

    #@7c
    .line 1082
    .local v0, cmd:Landroid/service/wallpaper/WallpaperService$WallpaperCommand;
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@7e
    invoke-virtual {v6, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->doCommand(Landroid/service/wallpaper/WallpaperService$WallpaperCommand;)V

    #@81
    goto :goto_21

    #@82
    .line 1085
    .end local v0           #cmd:Landroid/service/wallpaper/WallpaperService$WallpaperCommand;
    :sswitch_82
    iget v8, p1, Landroid/os/Message;->arg1:I

    #@84
    if-eqz v8, :cond_92

    #@86
    move v4, v6

    #@87
    .line 1086
    .local v4, reportDraw:Z
    :goto_87
    iget-object v8, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@89
    invoke-virtual {v8, v6, v7, v4}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@8c
    .line 1087
    iget-object v7, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@8e
    invoke-virtual {v7, v6}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    #@91
    goto :goto_21

    #@92
    .end local v4           #reportDraw:Z
    :cond_92
    move v4, v7

    #@93
    .line 1085
    goto :goto_87

    #@94
    .line 1093
    :sswitch_94
    const/4 v5, 0x0

    #@95
    .line 1094
    .local v5, skip:Z
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@97
    check-cast v3, Landroid/view/MotionEvent;

    #@99
    .line 1095
    .local v3, ev:Landroid/view/MotionEvent;
    invoke-virtual {v3}, Landroid/view/MotionEvent;->getAction()I

    #@9c
    move-result v6

    #@9d
    const/4 v7, 0x2

    #@9e
    if-ne v6, v7, :cond_b1

    #@a0
    .line 1096
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@a2
    iget-object v7, v6, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    #@a4
    monitor-enter v7

    #@a5
    .line 1097
    :try_start_a5
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@a7
    iget-object v6, v6, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@a9
    if-ne v6, v3, :cond_bd

    #@ab
    .line 1098
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@ad
    const/4 v8, 0x0

    #@ae
    iput-object v8, v6, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@b0
    .line 1103
    :goto_b0
    monitor-exit v7
    :try_end_b1
    .catchall {:try_start_a5 .. :try_end_b1} :catchall_bf

    #@b1
    .line 1105
    :cond_b1
    if-nez v5, :cond_b8

    #@b3
    .line 1107
    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mEngine:Landroid/service/wallpaper/WallpaperService$Engine;

    #@b5
    invoke-virtual {v6, v3}, Landroid/service/wallpaper/WallpaperService$Engine;->onTouchEvent(Landroid/view/MotionEvent;)V

    #@b8
    .line 1109
    :cond_b8
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    #@bb
    goto/16 :goto_21

    #@bd
    .line 1101
    :cond_bd
    const/4 v5, 0x1

    #@be
    goto :goto_b0

    #@bf
    .line 1103
    :catchall_bf
    move-exception v6

    #@c0
    :try_start_c0
    monitor-exit v7
    :try_end_c1
    .catchall {:try_start_c0 .. :try_end_c1} :catchall_bf

    #@c1
    throw v6

    #@c2
    .line 1046
    :sswitch_data_c2
    .sparse-switch
        0xa -> :sswitch_22
        0x14 -> :sswitch_45
        0x1e -> :sswitch_56
        0x2710 -> :sswitch_60
        0x271a -> :sswitch_66
        0x2724 -> :sswitch_72
        0x2729 -> :sswitch_78
        0x272e -> :sswitch_82
        0x2733 -> :sswitch_21
        0x2738 -> :sswitch_94
    .end sparse-switch
.end method

.method public reportShown()V
    .registers 4

    #@0
    .prologue
    .line 1029
    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mShownReported:Z

    #@2
    if-nez v1, :cond_c

    #@4
    .line 1030
    const/4 v1, 0x1

    #@5
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mShownReported:Z

    #@7
    .line 1032
    :try_start_7
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    #@9
    invoke-interface {v1, p0}, Landroid/service/wallpaper/IWallpaperConnection;->engineShown(Landroid/service/wallpaper/IWallpaperEngine;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_c} :catch_d

    #@c
    .line 1038
    :cond_c
    :goto_c
    return-void

    #@d
    .line 1033
    :catch_d
    move-exception v0

    #@e
    .line 1034
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "WallpaperService"

    #@10
    const-string v2, "Wallpaper host disappeared"

    #@12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@15
    goto :goto_c
.end method

.method public setDesiredSize(II)V
    .registers 6
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1003
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0x1e

    #@4
    invoke-virtual {v1, v2, p1, p2}, Lcom/android/internal/os/HandlerCaller;->obtainMessageII(III)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 1004
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 1005
    return-void
.end method

.method public setVisibility(Z)V
    .registers 6
    .parameter "visible"

    #@0
    .prologue
    .line 1008
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v3, 0x271a

    #@4
    if-eqz p1, :cond_11

    #@6
    const/4 v1, 0x1

    #@7
    :goto_7
    invoke-virtual {v2, v3, v1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageI(II)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 1010
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@d
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@10
    .line 1011
    return-void

    #@11
    .line 1008
    .end local v0           #msg:Landroid/os/Message;
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_7
.end method
