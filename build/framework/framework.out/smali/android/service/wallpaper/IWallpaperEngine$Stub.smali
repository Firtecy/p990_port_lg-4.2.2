.class public abstract Landroid/service/wallpaper/IWallpaperEngine$Stub;
.super Landroid/os/Binder;
.source "IWallpaperEngine.java"

# interfaces
.implements Landroid/service/wallpaper/IWallpaperEngine;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/wallpaper/IWallpaperEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/wallpaper/IWallpaperEngine$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.service.wallpaper.IWallpaperEngine"

.field static final TRANSACTION_destroy:I = 0x5

.field static final TRANSACTION_dispatchPointer:I = 0x3

.field static final TRANSACTION_dispatchWallpaperCommand:I = 0x4

.field static final TRANSACTION_setDesiredSize:I = 0x1

.field static final TRANSACTION_setVisibility:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 18
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/service/wallpaper/IWallpaperEngine;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 26
    if-nez p0, :cond_4

    #@2
    .line 27
    const/4 v0, 0x0

    #@3
    .line 33
    :goto_3
    return-object v0

    #@4
    .line 29
    :cond_4
    const-string v1, "android.service.wallpaper.IWallpaperEngine"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 30
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/service/wallpaper/IWallpaperEngine;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 31
    check-cast v0, Landroid/service/wallpaper/IWallpaperEngine;

    #@12
    goto :goto_3

    #@13
    .line 33
    :cond_13
    new-instance v0, Landroid/service/wallpaper/IWallpaperEngine$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/service/wallpaper/IWallpaperEngine$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 41
    sparse-switch p1, :sswitch_data_7e

    #@4
    .line 107
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v6

    #@8
    :goto_8
    return v6

    #@9
    .line 45
    :sswitch_9
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 50
    :sswitch_f
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@11
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v1

    #@18
    .line 54
    .local v1, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    .line 55
    .local v2, _arg1:I
    invoke-virtual {p0, v1, v2}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->setDesiredSize(II)V

    #@1f
    goto :goto_8

    #@20
    .line 60
    .end local v1           #_arg0:I
    .end local v2           #_arg1:I
    :sswitch_20
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@22
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25
    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_30

    #@2b
    move v1, v6

    #@2c
    .line 63
    .local v1, _arg0:Z
    :goto_2c
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->setVisibility(Z)V

    #@2f
    goto :goto_8

    #@30
    .line 62
    .end local v1           #_arg0:Z
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_2c

    #@32
    .line 68
    :sswitch_32
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@34
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@37
    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_49

    #@3d
    .line 71
    sget-object v0, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@3f
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@42
    move-result-object v1

    #@43
    check-cast v1, Landroid/view/MotionEvent;

    #@45
    .line 76
    .local v1, _arg0:Landroid/view/MotionEvent;
    :goto_45
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->dispatchPointer(Landroid/view/MotionEvent;)V

    #@48
    goto :goto_8

    #@49
    .line 74
    .end local v1           #_arg0:Landroid/view/MotionEvent;
    :cond_49
    const/4 v1, 0x0

    #@4a
    .restart local v1       #_arg0:Landroid/view/MotionEvent;
    goto :goto_45

    #@4b
    .line 81
    .end local v1           #_arg0:Landroid/view/MotionEvent;
    :sswitch_4b
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@4d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    .line 85
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v2

    #@58
    .line 87
    .restart local v2       #_arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v3

    #@5c
    .line 89
    .local v3, _arg2:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v4

    #@60
    .line 91
    .local v4, _arg3:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v0

    #@64
    if-eqz v0, :cond_73

    #@66
    .line 92
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@68
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@6b
    move-result-object v5

    #@6c
    check-cast v5, Landroid/os/Bundle;

    #@6e
    .local v5, _arg4:Landroid/os/Bundle;
    :goto_6e
    move-object v0, p0

    #@6f
    .line 97
    invoke-virtual/range {v0 .. v5}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->dispatchWallpaperCommand(Ljava/lang/String;IIILandroid/os/Bundle;)V

    #@72
    goto :goto_8

    #@73
    .line 95
    .end local v5           #_arg4:Landroid/os/Bundle;
    :cond_73
    const/4 v5, 0x0

    #@74
    .restart local v5       #_arg4:Landroid/os/Bundle;
    goto :goto_6e

    #@75
    .line 102
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:I
    .end local v3           #_arg2:I
    .end local v4           #_arg3:I
    .end local v5           #_arg4:Landroid/os/Bundle;
    :sswitch_75
    const-string v0, "android.service.wallpaper.IWallpaperEngine"

    #@77
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7a
    .line 103
    invoke-virtual {p0}, Landroid/service/wallpaper/IWallpaperEngine$Stub;->destroy()V

    #@7d
    goto :goto_8

    #@7e
    .line 41
    :sswitch_data_7e
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_20
        0x3 -> :sswitch_32
        0x4 -> :sswitch_4b
        0x5 -> :sswitch_75
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
