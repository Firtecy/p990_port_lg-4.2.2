.class public abstract Landroid/service/wallpaper/WallpaperService;
.super Landroid/app/Service;
.source "WallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/wallpaper/WallpaperService$IWallpaperServiceWrapper;,
        Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;,
        Landroid/service/wallpaper/WallpaperService$Engine;,
        Landroid/service/wallpaper/WallpaperService$WallpaperCommand;
    }
.end annotation


# static fields
.field private static CONFIG_FIXED_WALLPAPER_AVAILABLE_IN_GALLERY:Z = false

.field static final DEBUG:Z = false

.field private static final DO_ATTACH:I = 0xa

.field private static final DO_DETACH:I = 0x14

.field private static final DO_SET_DESIRED_SIZE:I = 0x1e

.field private static final MSG_TOUCH_EVENT:I = 0x2738

.field private static final MSG_UPDATE_SURFACE:I = 0x2710

.field private static final MSG_VISIBILITY_CHANGED:I = 0x271a

.field private static final MSG_WALLPAPER_COMMAND:I = 0x2729

.field private static final MSG_WALLPAPER_OFFSETS:I = 0x2724

.field private static final MSG_WINDOW_MOVED:I = 0x2733

.field private static final MSG_WINDOW_RESIZED:I = 0x272e

.field public static final SERVICE_INTERFACE:Ljava/lang/String; = "android.service.wallpaper.WallpaperService"

.field public static final SERVICE_META_DATA:Ljava/lang/String; = "android.service.wallpaper"

.field static final TAG:Ljava/lang/String; = "WallpaperService"


# instance fields
.field private final mActiveEngines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/service/wallpaper/WallpaperService$Engine;",
            ">;"
        }
    .end annotation
.end field

.field private mCallbackLooper:Landroid/os/Looper;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@a
    .line 1121
    return-void
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 69
    sget-boolean v0, Landroid/service/wallpaper/WallpaperService;->CONFIG_FIXED_WALLPAPER_AVAILABLE_IN_GALLERY:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/service/wallpaper/WallpaperService;)Landroid/os/Looper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService;->mCallbackLooper:Landroid/os/Looper;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Landroid/service/wallpaper/WallpaperService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "fd"
    .parameter "out"
    .parameter "args"

    #@0
    .prologue
    .line 1182
    const-string v2, "State of wallpaper "

    #@2
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5
    invoke-virtual {p2, p0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@8
    const-string v2, ":"

    #@a
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@d
    .line 1183
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v2

    #@14
    if-ge v1, v2, :cond_33

    #@16
    .line 1184
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@18
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/service/wallpaper/WallpaperService$Engine;

    #@1e
    .line 1185
    .local v0, engine:Landroid/service/wallpaper/WallpaperService$Engine;
    const-string v2, "  Engine "

    #@20
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@23
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@26
    const-string v2, ":"

    #@28
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2b
    .line 1186
    const-string v2, "    "

    #@2d
    invoke-virtual {v0, v2, p1, p2, p3}, Landroid/service/wallpaper/WallpaperService$Engine;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@30
    .line 1183
    add-int/lit8 v1, v1, 0x1

    #@32
    goto :goto_e

    #@33
    .line 1188
    .end local v0           #engine:Landroid/service/wallpaper/WallpaperService$Engine;
    :cond_33
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 1156
    new-instance v0, Landroid/service/wallpaper/WallpaperService$IWallpaperServiceWrapper;

    #@2
    invoke-direct {v0, p0, p0}, Landroid/service/wallpaper/WallpaperService$IWallpaperServiceWrapper;-><init>(Landroid/service/wallpaper/WallpaperService;Landroid/service/wallpaper/WallpaperService;)V

    #@5
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 1137
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 1138
    const/4 v0, 0x1

    #@4
    sput-boolean v0, Landroid/service/wallpaper/WallpaperService;->CONFIG_FIXED_WALLPAPER_AVAILABLE_IN_GALLERY:Z

    #@6
    .line 1139
    return-void
.end method

.method public abstract onCreateEngine()Landroid/service/wallpaper/WallpaperService$Engine;
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 1143
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 1144
    const/4 v0, 0x0

    #@4
    .local v0, i:I
    :goto_4
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@9
    move-result v1

    #@a
    if-ge v0, v1, :cond_1a

    #@c
    .line 1145
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    check-cast v1, Landroid/service/wallpaper/WallpaperService$Engine;

    #@14
    invoke-virtual {v1}, Landroid/service/wallpaper/WallpaperService$Engine;->detach()V

    #@17
    .line 1144
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_4

    #@1a
    .line 1147
    :cond_1a
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService;->mActiveEngines:Ljava/util/ArrayList;

    #@1c
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    #@1f
    .line 1148
    return-void
.end method

.method public setCallbackLooper(Landroid/os/Looper;)V
    .registers 2
    .parameter "looper"

    #@0
    .prologue
    .line 1169
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService;->mCallbackLooper:Landroid/os/Looper;

    #@2
    .line 1170
    return-void
.end method
