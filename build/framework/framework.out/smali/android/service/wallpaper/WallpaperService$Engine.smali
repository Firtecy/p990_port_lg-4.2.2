.class public Landroid/service/wallpaper/WallpaperService$Engine;
.super Ljava/lang/Object;
.source "WallpaperService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/wallpaper/WallpaperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Engine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;
    }
.end annotation


# instance fields
.field mCaller:Lcom/android/internal/os/HandlerCaller;

.field final mConfiguration:Landroid/content/res/Configuration;

.field mConnection:Landroid/service/wallpaper/IWallpaperConnection;

.field final mContentInsets:Landroid/graphics/Rect;

.field mCreated:Z

.field mCurHeight:I

.field mCurWidth:I

.field mCurWindowFlags:I

.field mCurWindowPrivateFlags:I

.field mDestroyed:Z

.field mDrawingAllowed:Z

.field mFixedSizeAllowed:Z

.field mFormat:I

.field mHeight:I

.field mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

.field mInitializing:Z

.field mInputChannel:Landroid/view/InputChannel;

.field mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

.field mIsCreating:Z

.field final mLayout:Landroid/view/WindowManager$LayoutParams;

.field final mLock:Ljava/lang/Object;

.field mOffsetMessageEnqueued:Z

.field mOffsetsChanged:Z

.field mPendingMove:Landroid/view/MotionEvent;

.field mPendingSync:Z

.field mPendingXOffset:F

.field mPendingXOffsetStep:F

.field mPendingYOffset:F

.field mPendingYOffsetStep:F

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field mReportedVisible:Z

.field mScreenOn:Z

.field mSession:Landroid/view/IWindowSession;

.field mSurfaceCreated:Z

.field final mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

.field mType:I

.field mVisible:Z

.field final mVisibleInsets:Landroid/graphics/Rect;

.field mWidth:I

.field final mWinFrame:Landroid/graphics/Rect;

.field final mWindow:Lcom/android/internal/view/BaseIWindow;

.field mWindowFlags:I

.field mWindowPrivateFlags:I

.field mWindowToken:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/service/wallpaper/WallpaperService;


# direct methods
.method public constructor <init>(Landroid/service/wallpaper/WallpaperService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 124
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 132
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    #@8
    .line 134
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenOn:Z

    #@a
    .line 151
    const/16 v0, 0x10

    #@c
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@e
    .line 152
    const/4 v0, 0x4

    #@f
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@11
    .line 154
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@13
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    #@15
    .line 155
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@17
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    #@19
    .line 156
    new-instance v0, Landroid/graphics/Rect;

    #@1b
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@1e
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisibleInsets:Landroid/graphics/Rect;

    #@20
    .line 157
    new-instance v0, Landroid/graphics/Rect;

    #@22
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@25
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrame:Landroid/graphics/Rect;

    #@27
    .line 158
    new-instance v0, Landroid/graphics/Rect;

    #@29
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@2c
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mContentInsets:Landroid/graphics/Rect;

    #@2e
    .line 159
    new-instance v0, Landroid/content/res/Configuration;

    #@30
    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    #@33
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConfiguration:Landroid/content/res/Configuration;

    #@35
    .line 161
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@37
    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    #@3a
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@3c
    .line 166
    new-instance v0, Ljava/lang/Object;

    #@3e
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@41
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    #@43
    .line 175
    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$1;

    #@45
    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$1;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    #@48
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReceiver:Landroid/content/BroadcastReceiver;

    #@4a
    .line 188
    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$2;

    #@4c
    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$2;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    #@4f
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@51
    .line 255
    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$3;

    #@53
    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$3;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    #@56
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@58
    return-void
.end method

.method static synthetic access$000(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 124
    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->dispatchPointer(Landroid/view/MotionEvent;)V

    #@3
    return-void
.end method

.method private dispatchPointer(Landroid/view/MotionEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 545
    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_28

    #@6
    .line 546
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    #@8
    monitor-enter v2

    #@9
    .line 547
    :try_start_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    #@c
    move-result v1

    #@d
    const/4 v3, 0x2

    #@e
    if-ne v1, v3, :cond_21

    #@10
    .line 548
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@12
    .line 552
    :goto_12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_9 .. :try_end_13} :catchall_25

    #@13
    .line 553
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@15
    const/16 v2, 0x2738

    #@17
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@1a
    move-result-object v0

    #@1b
    .line 554
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@1d
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@20
    .line 558
    .end local v0           #msg:Landroid/os/Message;
    :goto_20
    return-void

    #@21
    .line 550
    :cond_21
    const/4 v1, 0x0

    #@22
    :try_start_22
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@24
    goto :goto_12

    #@25
    .line 552
    :catchall_25
    move-exception v1

    #@26
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_22 .. :try_end_27} :catchall_25

    #@27
    throw v1

    #@28
    .line 556
    :cond_28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    #@2b
    goto :goto_20
.end method


# virtual methods
.method attach(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)V
    .registers 6
    .parameter "wrapper"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 775
    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@3
    if-eqz v1, :cond_6

    #@5
    .line 802
    :goto_5
    return-void

    #@6
    .line 779
    :cond_6
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@8
    .line 780
    #getter for: Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;
    invoke-static {p1}, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->access$200(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)Lcom/android/internal/os/HandlerCaller;

    #@b
    move-result-object v1

    #@c
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@e
    .line 781
    iget-object v1, p1, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    #@10
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    #@12
    .line 782
    iget-object v1, p1, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowToken:Landroid/os/IBinder;

    #@14
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowToken:Landroid/os/IBinder;

    #@16
    .line 783
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@18
    invoke-virtual {v1}, Lcom/android/internal/view/BaseSurfaceHolder;->setSizeFromLayout()V

    #@1b
    .line 784
    const/4 v1, 0x1

    #@1c
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    #@1e
    .line 785
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@20
    invoke-virtual {v1}, Landroid/service/wallpaper/WallpaperService;->getMainLooper()Landroid/os/Looper;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Landroid/view/WindowManagerGlobal;->getWindowSession(Landroid/os/Looper;)Landroid/view/IWindowSession;

    #@27
    move-result-object v1

    #@28
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@2a
    .line 787
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@2c
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@2e
    invoke-virtual {v1, v2}, Lcom/android/internal/view/BaseIWindow;->setSession(Landroid/view/IWindowSession;)V

    #@31
    .line 789
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@33
    const-string/jumbo v2, "power"

    #@36
    invoke-virtual {v1, v2}, Landroid/service/wallpaper/WallpaperService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Landroid/os/PowerManager;

    #@3c
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    #@3f
    move-result v1

    #@40
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenOn:Z

    #@42
    .line 791
    new-instance v0, Landroid/content/IntentFilter;

    #@44
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@47
    .line 792
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    #@49
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4c
    .line 793
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@4e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@51
    .line 794
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@53
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReceiver:Landroid/content/BroadcastReceiver;

    #@55
    invoke-virtual {v1, v2, v0}, Landroid/service/wallpaper/WallpaperService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@58
    .line 797
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@5a
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    #@5d
    .line 799
    iput-boolean v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    #@5f
    .line 800
    iput-boolean v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@61
    .line 801
    invoke-virtual {p0, v3, v3, v3}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@64
    goto :goto_5
.end method

.method detach()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    .line 922
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@4
    if-eqz v0, :cond_7

    #@6
    .line 964
    :cond_6
    :goto_6
    return-void

    #@7
    .line 926
    :cond_7
    const/4 v0, 0x1

    #@8
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@a
    .line 928
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 929
    iput-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    #@10
    .line 931
    invoke-virtual {p0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    #@13
    .line 934
    :cond_13
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportSurfaceDestroyed()V

    #@16
    .line 937
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    #@19
    .line 939
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@1b
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1d
    invoke-virtual {v0, v1}, Landroid/service/wallpaper/WallpaperService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@20
    .line 941
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@22
    if-eqz v0, :cond_6

    #@24
    .line 946
    :try_start_24
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    #@26
    if-eqz v0, :cond_30

    #@28
    .line 947
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    #@2a
    invoke-virtual {v0}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->dispose()V

    #@2d
    .line 948
    const/4 v0, 0x0

    #@2e
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    #@30
    .line 951
    :cond_30
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@32
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@34
    invoke-interface {v0, v1}, Landroid/view/IWindowSession;->remove(Landroid/view/IWindow;)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_37} :catch_4c

    #@37
    .line 954
    :goto_37
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@39
    iget-object v0, v0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@3b
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    #@3e
    .line 955
    iput-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@40
    .line 959
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@42
    if-eqz v0, :cond_6

    #@44
    .line 960
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@46
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@49
    .line 961
    iput-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@4b
    goto :goto_6

    #@4c
    .line 952
    :catch_4c
    move-exception v0

    #@4d
    goto :goto_37
.end method

.method doCommand(Landroid/service/wallpaper/WallpaperService$WallpaperCommand;)V
    .registers 10
    .parameter "cmd"

    #@0
    .prologue
    .line 890
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@2
    if-nez v0, :cond_25

    #@4
    .line 891
    iget-object v1, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->action:Ljava/lang/String;

    #@6
    iget v2, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->x:I

    #@8
    iget v3, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->y:I

    #@a
    iget v4, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->z:I

    #@c
    iget-object v5, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->extras:Landroid/os/Bundle;

    #@e
    iget-boolean v6, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->sync:Z

    #@10
    move-object v0, p0

    #@11
    invoke-virtual/range {v0 .. v6}, Landroid/service/wallpaper/WallpaperService$Engine;->onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    #@14
    move-result-object v7

    #@15
    .line 896
    .local v7, result:Landroid/os/Bundle;
    :goto_15
    iget-boolean v0, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->sync:Z

    #@17
    if-eqz v0, :cond_24

    #@19
    .line 899
    :try_start_19
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@1b
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@1d
    invoke-virtual {v1}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    #@20
    move-result-object v1

    #@21
    invoke-interface {v0, v1, v7}, Landroid/view/IWindowSession;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_24} :catch_27

    #@24
    .line 903
    :cond_24
    :goto_24
    return-void

    #@25
    .line 894
    .end local v7           #result:Landroid/os/Bundle;
    :cond_25
    const/4 v7, 0x0

    #@26
    .restart local v7       #result:Landroid/os/Bundle;
    goto :goto_15

    #@27
    .line 900
    :catch_27
    move-exception v0

    #@28
    goto :goto_24
.end method

.method doDesiredSizeChanged(II)V
    .registers 4
    .parameter "desiredWidth"
    .parameter "desiredHeight"

    #@0
    .prologue
    .line 805
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@2
    if-nez v0, :cond_13

    #@4
    .line 808
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@6
    iput p1, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    #@8
    .line 809
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@a
    iput p2, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    #@c
    .line 810
    invoke-virtual {p0, p1, p2}, Landroid/service/wallpaper/WallpaperService$Engine;->onDesiredSizeChanged(II)V

    #@f
    .line 811
    const/4 v0, 0x1

    #@10
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    #@13
    .line 813
    :cond_13
    return-void
.end method

.method doOffsetsChanged(Z)V
    .registers 15
    .parameter "always"

    #@0
    .prologue
    const/high16 v12, 0x3f00

    #@2
    const/4 v0, 0x0

    #@3
    .line 842
    iget-boolean v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@5
    if-eqz v10, :cond_8

    #@7
    .line 886
    :cond_7
    :goto_7
    return-void

    #@8
    .line 846
    :cond_8
    if-nez p1, :cond_e

    #@a
    iget-boolean v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetsChanged:Z

    #@c
    if-eqz v10, :cond_7

    #@e
    .line 855
    :cond_e
    iget-object v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    #@10
    monitor-enter v10

    #@11
    .line 856
    :try_start_11
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    #@13
    .line 857
    .local v1, xOffset:F
    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingYOffset:F

    #@15
    .line 858
    .local v2, yOffset:F
    iget v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    #@17
    .line 859
    .local v3, xOffsetStep:F
    iget v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingYOffsetStep:F

    #@19
    .line 860
    .local v4, yOffsetStep:F
    iget-boolean v9, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    #@1b
    .line 861
    .local v9, sync:Z
    const/4 v11, 0x0

    #@1c
    iput-boolean v11, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    #@1e
    .line 862
    const/4 v11, 0x0

    #@1f
    iput-boolean v11, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetMessageEnqueued:Z

    #@21
    .line 863
    monitor-exit v10
    :try_end_22
    .catchall {:try_start_11 .. :try_end_22} :catchall_5c

    #@22
    .line 865
    iget-boolean v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@24
    if-eqz v10, :cond_4c

    #@26
    .line 866
    iget-boolean v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@28
    if-eqz v10, :cond_63

    #@2a
    .line 869
    iget-object v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@2c
    iget v10, v10, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    #@2e
    iget v11, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@30
    sub-int v8, v10, v11

    #@32
    .line 870
    .local v8, availw:I
    if-lez v8, :cond_5f

    #@34
    int-to-float v10, v8

    #@35
    mul-float/2addr v10, v1

    #@36
    add-float/2addr v10, v12

    #@37
    float-to-int v10, v10

    #@38
    neg-int v5, v10

    #@39
    .line 871
    .local v5, xPixels:I
    :goto_39
    iget-object v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@3b
    iget v10, v10, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    #@3d
    iget v11, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@3f
    sub-int v7, v10, v11

    #@41
    .line 872
    .local v7, availh:I
    if-lez v7, :cond_61

    #@43
    int-to-float v0, v7

    #@44
    mul-float/2addr v0, v2

    #@45
    add-float/2addr v0, v12

    #@46
    float-to-int v0, v0

    #@47
    neg-int v6, v0

    #@48
    .local v6, yPixels:I
    :goto_48
    move-object v0, p0

    #@49
    .line 873
    invoke-virtual/range {v0 .. v6}, Landroid/service/wallpaper/WallpaperService$Engine;->onOffsetsChanged(FFFFII)V

    #@4c
    .line 879
    .end local v5           #xPixels:I
    .end local v6           #yPixels:I
    .end local v7           #availh:I
    .end local v8           #availw:I
    :cond_4c
    :goto_4c
    if-eqz v9, :cond_7

    #@4e
    .line 882
    :try_start_4e
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@50
    iget-object v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@52
    invoke-virtual {v10}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    #@55
    move-result-object v10

    #@56
    invoke-interface {v0, v10}, Landroid/view/IWindowSession;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    :try_end_59
    .catch Landroid/os/RemoteException; {:try_start_4e .. :try_end_59} :catch_5a

    #@59
    goto :goto_7

    #@5a
    .line 883
    :catch_5a
    move-exception v0

    #@5b
    goto :goto_7

    #@5c
    .line 863
    .end local v1           #xOffset:F
    .end local v2           #yOffset:F
    .end local v3           #xOffsetStep:F
    .end local v4           #yOffsetStep:F
    .end local v9           #sync:Z
    :catchall_5c
    move-exception v0

    #@5d
    :try_start_5d
    monitor-exit v10
    :try_end_5e
    .catchall {:try_start_5d .. :try_end_5e} :catchall_5c

    #@5e
    throw v0

    #@5f
    .restart local v1       #xOffset:F
    .restart local v2       #yOffset:F
    .restart local v3       #xOffsetStep:F
    .restart local v4       #yOffsetStep:F
    .restart local v8       #availw:I
    .restart local v9       #sync:Z
    :cond_5f
    move v5, v0

    #@60
    .line 870
    goto :goto_39

    #@61
    .restart local v5       #xPixels:I
    .restart local v7       #availh:I
    :cond_61
    move v6, v0

    #@62
    .line 872
    goto :goto_48

    #@63
    .line 875
    .end local v5           #xPixels:I
    .end local v7           #availh:I
    .end local v8           #availw:I
    :cond_63
    const/4 v0, 0x1

    #@64
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetsChanged:Z

    #@66
    goto :goto_4c
.end method

.method doVisibilityChanged(Z)V
    .registers 3
    .parameter "visible"

    #@0
    .prologue
    .line 816
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@2
    if-nez v0, :cond_9

    #@4
    .line 817
    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    #@6
    .line 818
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportVisibility()V

    #@9
    .line 820
    :cond_9
    return-void
.end method

.method protected dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter "prefix"
    .parameter "fd"
    .parameter "out"
    .parameter "args"

    #@0
    .prologue
    .line 505
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string/jumbo v0, "mInitializing="

    #@6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    #@b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@e
    .line 506
    const-string v0, " mDestroyed="

    #@10
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@15
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@18
    .line 507
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    const-string/jumbo v0, "mVisible="

    #@1e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    #@23
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@26
    .line 508
    const-string v0, " mScreenOn="

    #@28
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenOn:Z

    #@2d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@30
    .line 509
    const-string v0, " mReportedVisible="

    #@32
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@37
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@3a
    .line 510
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    const-string/jumbo v0, "mCreated="

    #@40
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@45
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@48
    .line 511
    const-string v0, " mSurfaceCreated="

    #@4a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@4f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@52
    .line 512
    const-string v0, " mIsCreating="

    #@54
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@57
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    #@59
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@5c
    .line 513
    const-string v0, " mDrawingAllowed="

    #@5e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@61
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDrawingAllowed:Z

    #@63
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@66
    .line 514
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@69
    const-string/jumbo v0, "mWidth="

    #@6c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6f
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    #@71
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@74
    .line 515
    const-string v0, " mCurWidth="

    #@76
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@79
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@7b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@7e
    .line 516
    const-string v0, " mHeight="

    #@80
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@83
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    #@85
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@88
    .line 517
    const-string v0, " mCurHeight="

    #@8a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8d
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@8f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@92
    .line 518
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@95
    const-string/jumbo v0, "mType="

    #@98
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9b
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    #@9d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@a0
    .line 519
    const-string v0, " mWindowFlags="

    #@a2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a5
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@a7
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@aa
    .line 520
    const-string v0, " mCurWindowFlags="

    #@ac
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@af
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    #@b1
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@b4
    .line 521
    const-string v0, " mWindowPrivateFlags="

    #@b6
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b9
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@bb
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    #@be
    .line 522
    const-string v0, " mCurWindowPrivateFlags="

    #@c0
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c3
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    #@c5
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    #@c8
    .line 523
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@cb
    const-string/jumbo v0, "mVisibleInsets="

    #@ce
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d1
    .line 524
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisibleInsets:Landroid/graphics/Rect;

    #@d3
    invoke-virtual {v0}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@da
    .line 525
    const-string v0, " mWinFrame="

    #@dc
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@df
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrame:Landroid/graphics/Rect;

    #@e1
    invoke-virtual {v0}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@e4
    move-result-object v0

    #@e5
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e8
    .line 526
    const-string v0, " mContentInsets="

    #@ea
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ed
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mContentInsets:Landroid/graphics/Rect;

    #@ef
    invoke-virtual {v0}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    #@f2
    move-result-object v0

    #@f3
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@f6
    .line 527
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f9
    const-string/jumbo v0, "mConfiguration="

    #@fc
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ff
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConfiguration:Landroid/content/res/Configuration;

    #@101
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@104
    .line 528
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@107
    const-string/jumbo v0, "mLayout="

    #@10a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10d
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@10f
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@112
    .line 529
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    #@114
    monitor-enter v1

    #@115
    .line 530
    :try_start_115
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@118
    const-string/jumbo v0, "mPendingXOffset="

    #@11b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11e
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    #@120
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(F)V

    #@123
    .line 531
    const-string v0, " mPendingXOffset="

    #@125
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@128
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    #@12a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(F)V

    #@12d
    .line 532
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@130
    const-string/jumbo v0, "mPendingXOffsetStep="

    #@133
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@136
    .line 533
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    #@138
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(F)V

    #@13b
    .line 534
    const-string v0, " mPendingXOffsetStep="

    #@13d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@140
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    #@142
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(F)V

    #@145
    .line 535
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@148
    const-string/jumbo v0, "mOffsetMessageEnqueued="

    #@14b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14e
    .line 536
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetMessageEnqueued:Z

    #@150
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@153
    .line 537
    const-string v0, " mPendingSync="

    #@155
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@158
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    #@15a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@15d
    .line 538
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@15f
    if-eqz v0, :cond_16f

    #@161
    .line 539
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@164
    const-string/jumbo v0, "mPendingMove="

    #@167
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@16a
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    #@16c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@16f
    .line 541
    :cond_16f
    monitor-exit v1

    #@170
    .line 542
    return-void

    #@171
    .line 541
    :catchall_171
    move-exception v0

    #@172
    monitor-exit v1
    :try_end_173
    .catchall {:try_start_115 .. :try_end_173} :catchall_171

    #@173
    throw v0
.end method

.method public getDesiredMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 342
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@2
    iget v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    #@4
    return v0
.end method

.method public getDesiredMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 333
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@2
    iget v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    #@4
    return v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .registers 2

    #@0
    .prologue
    .line 324
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2
    return-object v0
.end method

.method public isPreview()Z
    .registers 2

    #@0
    .prologue
    .line 360
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@2
    iget-boolean v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mIsPreview:Z

    #@4
    return v0
.end method

.method public isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 351
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@2
    return v0
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .registers 8
    .parameter "action"
    .parameter "x"
    .parameter "y"
    .parameter "z"
    .parameter "extras"
    .parameter "resultRequested"

    #@0
    .prologue
    .line 466
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .registers 2
    .parameter "surfaceHolder"

    #@0
    .prologue
    .line 408
    return-void
.end method

.method public onDesiredSizeChanged(II)V
    .registers 3
    .parameter "desiredWidth"
    .parameter "desiredHeight"

    #@0
    .prologue
    .line 474
    return-void
.end method

.method public onDestroy()V
    .registers 1

    #@0
    .prologue
    .line 416
    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .registers 7
    .parameter "xOffset"
    .parameter "yOffset"
    .parameter "xOffsetStep"
    .parameter "yOffsetStep"
    .parameter "xPixelOffset"
    .parameter "yPixelOffset"

    #@0
    .prologue
    .line 445
    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 5
    .parameter "holder"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 481
    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 2
    .parameter "holder"

    #@0
    .prologue
    .line 495
    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 2
    .parameter "holder"

    #@0
    .prologue
    .line 502
    return-void
.end method

.method public onSurfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    .registers 2
    .parameter "holder"

    #@0
    .prologue
    .line 488
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 434
    return-void
.end method

.method public onVisibilityChanged(Z)V
    .registers 2
    .parameter "visible"

    #@0
    .prologue
    .line 424
    return-void
.end method

.method reportSurfaceDestroyed()V
    .registers 7

    #@0
    .prologue
    .line 906
    iget-boolean v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@2
    if-eqz v5, :cond_28

    #@4
    .line 907
    const/4 v5, 0x0

    #@5
    iput-boolean v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@7
    .line 908
    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@9
    invoke-virtual {v5}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V

    #@c
    .line 909
    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@e
    invoke-virtual {v5}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@11
    move-result-object v2

    #@12
    .line 910
    .local v2, callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v2, :cond_23

    #@14
    .line 911
    move-object v0, v2

    #@15
    .local v0, arr$:[Landroid/view/SurfaceHolder$Callback;
    array-length v4, v0

    #@16
    .local v4, len$:I
    const/4 v3, 0x0

    #@17
    .local v3, i$:I
    :goto_17
    if-ge v3, v4, :cond_23

    #@19
    aget-object v1, v0, v3

    #@1b
    .line 912
    .local v1, c:Landroid/view/SurfaceHolder$Callback;
    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@1d
    invoke-interface {v1, v5}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    #@20
    .line 911
    add-int/lit8 v3, v3, 0x1

    #@22
    goto :goto_17

    #@23
    .line 917
    .end local v0           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v1           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_23
    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@25
    invoke-virtual {p0, v5}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    #@28
    .line 919
    .end local v2           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    :cond_28
    return-void
.end method

.method reportVisibility()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 823
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@3
    if-nez v2, :cond_1f

    #@5
    .line 824
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    #@7
    if-eqz v2, :cond_20

    #@9
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenOn:Z

    #@b
    if-eqz v2, :cond_20

    #@d
    const/4 v0, 0x1

    #@e
    .line 825
    .local v0, visible:Z
    :goto_e
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@10
    if-eq v2, v0, :cond_1f

    #@12
    .line 826
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@14
    .line 829
    if-eqz v0, :cond_1c

    #@16
    .line 833
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    #@19
    .line 834
    invoke-virtual {p0, v1, v1, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@1c
    .line 836
    :cond_1c
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    #@1f
    .line 839
    .end local v0           #visible:Z
    :cond_1f
    return-void

    #@20
    :cond_20
    move v0, v1

    #@21
    .line 824
    goto :goto_e
.end method

.method public setFixedSizeAllowed(Z)V
    .registers 2
    .parameter "allowed"

    #@0
    .prologue
    .line 400
    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mFixedSizeAllowed:Z

    #@2
    .line 401
    return-void
.end method

.method public setOffsetNotificationsEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 388
    if-eqz p1, :cond_11

    #@3
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@5
    or-int/lit8 v0, v0, 0x4

    #@7
    :goto_7
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@9
    .line 393
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 394
    invoke-virtual {p0, v1, v1, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@10
    .line 396
    :cond_10
    return-void

    #@11
    .line 388
    :cond_11
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@13
    and-int/lit8 v0, v0, -0x5

    #@15
    goto :goto_7
.end method

.method public setTouchEventsEnabled(Z)V
    .registers 4
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 371
    if-eqz p1, :cond_11

    #@3
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@5
    and-int/lit8 v0, v0, -0x11

    #@7
    :goto_7
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@9
    .line 374
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@b
    if-eqz v0, :cond_10

    #@d
    .line 375
    invoke-virtual {p0, v1, v1, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    #@10
    .line 377
    :cond_10
    return-void

    #@11
    .line 371
    :cond_11
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@13
    or-int/lit8 v0, v0, 0x10

    #@15
    goto :goto_7
.end method

.method updateSurface(ZZZ)V
    .registers 37
    .parameter "forceRelayout"
    .parameter "forceReport"
    .parameter "redrawNeeded"

    #@0
    .prologue
    .line 561
    move-object/from16 v0, p0

    #@2
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    #@4
    if-eqz v2, :cond_d

    #@6
    .line 562
    const-string v2, "WallpaperService"

    #@8
    const-string v3, "Ignoring updateSurface: destroyed"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 565
    :cond_d
    move-object/from16 v0, p0

    #@f
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@11
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedWidth()I

    #@14
    move-result v26

    #@15
    .line 566
    .local v26, myWidth:I
    if-gtz v26, :cond_19

    #@17
    const/16 v26, -0x1

    #@19
    .line 567
    :cond_19
    move-object/from16 v0, p0

    #@1b
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@1d
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedHeight()I

    #@20
    move-result v25

    #@21
    .line 568
    .local v25, myHeight:I
    if-gtz v25, :cond_25

    #@23
    const/16 v25, -0x1

    #@25
    .line 570
    :cond_25
    move-object/from16 v0, p0

    #@27
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@29
    if-nez v2, :cond_1d3

    #@2b
    const/16 v18, 0x1

    #@2d
    .line 571
    .local v18, creating:Z
    :goto_2d
    move-object/from16 v0, p0

    #@2f
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@31
    if-nez v2, :cond_1d7

    #@33
    const/16 v30, 0x1

    #@35
    .line 572
    .local v30, surfaceCreating:Z
    :goto_35
    move-object/from16 v0, p0

    #@37
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    #@39
    move-object/from16 v0, p0

    #@3b
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@3d
    invoke-virtual {v3}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedFormat()I

    #@40
    move-result v3

    #@41
    if-eq v2, v3, :cond_1db

    #@43
    const/16 v21, 0x1

    #@45
    .line 573
    .local v21, formatChanged:Z
    :goto_45
    move-object/from16 v0, p0

    #@47
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    #@49
    move/from16 v0, v26

    #@4b
    if-ne v2, v0, :cond_55

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    #@51
    move/from16 v0, v25

    #@53
    if-eq v2, v0, :cond_1df

    #@55
    :cond_55
    const/16 v29, 0x1

    #@57
    .line 574
    .local v29, sizeChanged:Z
    :goto_57
    move-object/from16 v0, p0

    #@59
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    #@5b
    move-object/from16 v0, p0

    #@5d
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@5f
    invoke-virtual {v3}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedType()I

    #@62
    move-result v3

    #@63
    if-eq v2, v3, :cond_1e3

    #@65
    const/16 v31, 0x1

    #@67
    .line 575
    .local v31, typeChanged:Z
    :goto_67
    move-object/from16 v0, p0

    #@69
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    #@6b
    move-object/from16 v0, p0

    #@6d
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@6f
    if-ne v2, v3, :cond_7b

    #@71
    move-object/from16 v0, p0

    #@73
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    #@75
    move-object/from16 v0, p0

    #@77
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@79
    if-eq v2, v3, :cond_1e7

    #@7b
    :cond_7b
    const/16 v20, 0x1

    #@7d
    .line 577
    .local v20, flagsChanged:Z
    :goto_7d
    if-nez p1, :cond_95

    #@7f
    if-nez v18, :cond_95

    #@81
    if-nez v30, :cond_95

    #@83
    if-nez v21, :cond_95

    #@85
    if-nez v29, :cond_95

    #@87
    if-nez v31, :cond_95

    #@89
    if-nez v20, :cond_95

    #@8b
    if-nez p3, :cond_95

    #@8d
    move-object/from16 v0, p0

    #@8f
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@91
    iget-boolean v2, v2, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mShownReported:Z

    #@93
    if-nez v2, :cond_1d2

    #@95
    .line 585
    :cond_95
    :try_start_95
    move/from16 v0, v26

    #@97
    move-object/from16 v1, p0

    #@99
    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    #@9b
    .line 586
    move/from16 v0, v25

    #@9d
    move-object/from16 v1, p0

    #@9f
    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    #@a1
    .line 587
    move-object/from16 v0, p0

    #@a3
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@a5
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedFormat()I

    #@a8
    move-result v2

    #@a9
    move-object/from16 v0, p0

    #@ab
    iput v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    #@ad
    .line 588
    move-object/from16 v0, p0

    #@af
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@b1
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedType()I

    #@b4
    move-result v2

    #@b5
    move-object/from16 v0, p0

    #@b7
    iput v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    #@b9
    .line 590
    move-object/from16 v0, p0

    #@bb
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@bd
    const/4 v3, 0x0

    #@be
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    #@c0
    .line 591
    move-object/from16 v0, p0

    #@c2
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@c4
    const/4 v3, 0x0

    #@c5
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    #@c7
    .line 592
    move-object/from16 v0, p0

    #@c9
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@cb
    move/from16 v0, v26

    #@cd
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    #@cf
    .line 593
    move-object/from16 v0, p0

    #@d1
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@d3
    move/from16 v0, v25

    #@d5
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    #@d7
    .line 595
    move-object/from16 v0, p0

    #@d9
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@db
    move-object/from16 v0, p0

    #@dd
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    #@df
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    #@e1
    .line 597
    move-object/from16 v0, p0

    #@e3
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@e5
    move-object/from16 v0, p0

    #@e7
    iput v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    #@e9
    .line 600
    move-object/from16 v0, p0

    #@eb
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@ed
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@f0
    move-result-object v2

    #@f1
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@f4
    move-result-object v27

    #@f5
    .line 601
    .local v27, name:Ljava/lang/String;
    const-string v2, "WallpaperService"

    #@f7
    new-instance v3, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    const-string/jumbo v4, "name : "

    #@ff
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v3

    #@103
    move-object/from16 v0, v27

    #@105
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v3

    #@10d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 602
    if-eqz v27, :cond_1eb

    #@112
    const-string v2, "com.lge.livewallpaper"

    #@114
    move-object/from16 v0, v27

    #@116
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@119
    move-result v2

    #@11a
    if-eqz v2, :cond_1eb

    #@11c
    .line 604
    move-object/from16 v0, p0

    #@11e
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@120
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@122
    or-int/lit16 v3, v3, 0x600

    #@124
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@126
    .line 605
    const-string v2, "WallpaperService"

    #@128
    const-string v3, "add fullscreen flag"

    #@12a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    .line 611
    :goto_12d
    move-object/from16 v0, p0

    #@12f
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@131
    move-object/from16 v0, p0

    #@133
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    #@135
    or-int/lit16 v3, v3, 0x200

    #@137
    or-int/lit16 v3, v3, 0x100

    #@139
    or-int/lit8 v3, v3, 0x8

    #@13b
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@13d
    .line 616
    move-object/from16 v0, p0

    #@13f
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@141
    move-object/from16 v0, p0

    #@143
    iput v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    #@145
    .line 617
    move-object/from16 v0, p0

    #@147
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@149
    move-object/from16 v0, p0

    #@14b
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    #@14d
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@14f
    .line 619
    move-object/from16 v0, p0

    #@151
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@153
    move-object/from16 v0, p0

    #@155
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    #@157
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->memoryType:I

    #@159
    .line 620
    move-object/from16 v0, p0

    #@15b
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowToken:Landroid/os/IBinder;

    #@161
    iput-object v3, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@163
    .line 622
    move-object/from16 v0, p0

    #@165
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@167
    if-nez v2, :cond_216

    #@169
    .line 623
    move-object/from16 v0, p0

    #@16b
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@16d
    move-object/from16 v0, p0

    #@16f
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@171
    iget v3, v3, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowType:I

    #@173
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@175
    .line 624
    move-object/from16 v0, p0

    #@177
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@179
    const v3, 0x800033

    #@17c
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@17e
    .line 625
    move-object/from16 v0, p0

    #@180
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@182
    move-object/from16 v0, p0

    #@184
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    #@186
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@189
    move-result-object v3

    #@18a
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@18d
    move-result-object v3

    #@18e
    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@191
    .line 627
    invoke-static {}, Landroid/service/wallpaper/WallpaperService;->access$100()Z

    #@194
    move-result v2

    #@195
    if-nez v2, :cond_1f6

    #@197
    .line 628
    move-object/from16 v0, p0

    #@199
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@19b
    const v3, 0x10301ec

    #@19e
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@1a0
    .line 634
    :goto_1a0
    new-instance v2, Landroid/view/InputChannel;

    #@1a2
    invoke-direct {v2}, Landroid/view/InputChannel;-><init>()V

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iput-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@1a9
    .line 635
    move-object/from16 v0, p0

    #@1ab
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@1b1
    move-object/from16 v0, p0

    #@1b3
    iget-object v4, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@1b5
    iget v4, v4, Lcom/android/internal/view/BaseIWindow;->mSeq:I

    #@1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v5, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1bb
    const/4 v6, 0x0

    #@1bc
    const/4 v7, 0x0

    #@1bd
    move-object/from16 v0, p0

    #@1bf
    iget-object v8, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mContentInsets:Landroid/graphics/Rect;

    #@1c1
    move-object/from16 v0, p0

    #@1c3
    iget-object v9, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@1c5
    invoke-interface/range {v2 .. v9}, Landroid/view/IWindowSession;->addToDisplay(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IILandroid/graphics/Rect;Landroid/view/InputChannel;)I

    #@1c8
    move-result v2

    #@1c9
    if-gez v2, :cond_1fe

    #@1cb
    .line 637
    const-string v2, "WallpaperService"

    #@1cd
    const-string v3, "Failed to add window while updating wallpaper surface."

    #@1cf
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1d2
    .line 771
    .end local v27           #name:Ljava/lang/String;
    :cond_1d2
    :goto_1d2
    return-void

    #@1d3
    .line 570
    .end local v18           #creating:Z
    .end local v20           #flagsChanged:Z
    .end local v21           #formatChanged:Z
    .end local v29           #sizeChanged:Z
    .end local v30           #surfaceCreating:Z
    .end local v31           #typeChanged:Z
    :cond_1d3
    const/16 v18, 0x0

    #@1d5
    goto/16 :goto_2d

    #@1d7
    .line 571
    .restart local v18       #creating:Z
    :cond_1d7
    const/16 v30, 0x0

    #@1d9
    goto/16 :goto_35

    #@1db
    .line 572
    .restart local v30       #surfaceCreating:Z
    :cond_1db
    const/16 v21, 0x0

    #@1dd
    goto/16 :goto_45

    #@1df
    .line 573
    .restart local v21       #formatChanged:Z
    :cond_1df
    const/16 v29, 0x0

    #@1e1
    goto/16 :goto_57

    #@1e3
    .line 574
    .restart local v29       #sizeChanged:Z
    :cond_1e3
    const/16 v31, 0x0

    #@1e5
    goto/16 :goto_67

    #@1e7
    .line 575
    .restart local v31       #typeChanged:Z
    :cond_1e7
    const/16 v20, 0x0

    #@1e9
    goto/16 :goto_7d

    #@1eb
    .line 607
    .restart local v20       #flagsChanged:Z
    .restart local v27       #name:Ljava/lang/String;
    :cond_1eb
    const-string v2, "WallpaperService"

    #@1ed
    const-string v3, "do not fullscreen flag"

    #@1ef
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f2
    goto/16 :goto_12d

    #@1f4
    .line 765
    .end local v27           #name:Ljava/lang/String;
    :catch_1f4
    move-exception v2

    #@1f5
    goto :goto_1d2

    #@1f6
    .line 631
    .restart local v27       #name:Ljava/lang/String;
    :cond_1f6
    move-object/from16 v0, p0

    #@1f8
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@1fa
    const/4 v3, 0x0

    #@1fb
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@1fd
    goto :goto_1a0

    #@1fe
    .line 640
    :cond_1fe
    const/4 v2, 0x1

    #@1ff
    move-object/from16 v0, p0

    #@201
    iput-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    #@203
    .line 642
    new-instance v2, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    #@205
    move-object/from16 v0, p0

    #@207
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputChannel:Landroid/view/InputChannel;

    #@209
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@20c
    move-result-object v4

    #@20d
    move-object/from16 v0, p0

    #@20f
    invoke-direct {v2, v0, v3, v4}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@212
    move-object/from16 v0, p0

    #@214
    iput-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    #@216
    .line 646
    :cond_216
    move-object/from16 v0, p0

    #@218
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@21a
    iget-object v2, v2, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@21c
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    #@21f
    .line 647
    const/4 v2, 0x1

    #@220
    move-object/from16 v0, p0

    #@222
    iput-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mDrawingAllowed:Z

    #@224
    .line 649
    move-object/from16 v0, p0

    #@226
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@228
    move-object/from16 v0, p0

    #@22a
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@22c
    move-object/from16 v0, p0

    #@22e
    iget-object v4, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@230
    iget v4, v4, Lcom/android/internal/view/BaseIWindow;->mSeq:I

    #@232
    move-object/from16 v0, p0

    #@234
    iget-object v5, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    #@236
    move-object/from16 v0, p0

    #@238
    iget v6, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    #@23a
    move-object/from16 v0, p0

    #@23c
    iget v7, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    #@23e
    const/4 v8, 0x0

    #@23f
    const/4 v9, 0x0

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v10, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrame:Landroid/graphics/Rect;

    #@244
    move-object/from16 v0, p0

    #@246
    iget-object v11, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mContentInsets:Landroid/graphics/Rect;

    #@248
    move-object/from16 v0, p0

    #@24a
    iget-object v12, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisibleInsets:Landroid/graphics/Rect;

    #@24c
    move-object/from16 v0, p0

    #@24e
    iget-object v13, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mConfiguration:Landroid/content/res/Configuration;

    #@250
    move-object/from16 v0, p0

    #@252
    iget-object v14, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@254
    iget-object v14, v14, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@256
    invoke-interface/range {v2 .. v14}, Landroid/view/IWindowSession;->relayout(Landroid/view/IWindow;ILandroid/view/WindowManager$LayoutParams;IIIILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I

    #@259
    move-result v28

    #@25a
    .line 657
    .local v28, relayoutResult:I
    move-object/from16 v0, p0

    #@25c
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrame:Landroid/graphics/Rect;

    #@25e
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    #@261
    move-result v32

    #@262
    .line 658
    .local v32, w:I
    move-object/from16 v0, p0

    #@264
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@266
    move/from16 v0, v32

    #@268
    if-eq v2, v0, :cond_272

    #@26a
    .line 659
    const/16 v29, 0x1

    #@26c
    .line 660
    move/from16 v0, v32

    #@26e
    move-object/from16 v1, p0

    #@270
    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@272
    .line 662
    :cond_272
    move-object/from16 v0, p0

    #@274
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrame:Landroid/graphics/Rect;

    #@276
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    #@279
    move-result v22

    #@27a
    .line 663
    .local v22, h:I
    move-object/from16 v0, p0

    #@27c
    iget v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@27e
    move/from16 v0, v22

    #@280
    if-eq v2, v0, :cond_28a

    #@282
    .line 664
    const/16 v29, 0x1

    #@284
    .line 665
    move/from16 v0, v22

    #@286
    move-object/from16 v1, p0

    #@288
    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@28a
    .line 668
    :cond_28a
    move-object/from16 v0, p0

    #@28c
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@28e
    move/from16 v0, v32

    #@290
    move/from16 v1, v22

    #@292
    invoke-virtual {v2, v0, v1}, Lcom/android/internal/view/BaseSurfaceHolder;->setSurfaceFrameSize(II)V

    #@295
    .line 669
    move-object/from16 v0, p0

    #@297
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@299
    iget-object v2, v2, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    #@29b
    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    #@29e
    .line 671
    move-object/from16 v0, p0

    #@2a0
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2a2
    iget-object v2, v2, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    #@2a4
    invoke-virtual {v2}, Landroid/view/Surface;->isValid()Z

    #@2a7
    move-result v2

    #@2a8
    if-nez v2, :cond_2af

    #@2aa
    .line 672
    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportSurfaceDestroyed()V
    :try_end_2ad
    .catch Landroid/os/RemoteException; {:try_start_95 .. :try_end_2ad} :catch_1f4

    #@2ad
    goto/16 :goto_1d2

    #@2af
    .line 677
    :cond_2af
    const/16 v19, 0x0

    #@2b1
    .line 680
    .local v19, didSurface:Z
    :try_start_2b1
    move-object/from16 v0, p0

    #@2b3
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2b5
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V

    #@2b8
    .line 682
    if-eqz v30, :cond_2ef

    #@2ba
    .line 683
    const/4 v2, 0x1

    #@2bb
    move-object/from16 v0, p0

    #@2bd
    iput-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    #@2bf
    .line 684
    const/16 v19, 0x1

    #@2c1
    .line 687
    move-object/from16 v0, p0

    #@2c3
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2c5
    move-object/from16 v0, p0

    #@2c7
    invoke-virtual {v0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    #@2ca
    .line 688
    move-object/from16 v0, p0

    #@2cc
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2ce
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@2d1
    move-result-object v17

    #@2d2
    .line 689
    .local v17, callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v17, :cond_2ef

    #@2d4
    .line 690
    move-object/from16 v15, v17

    #@2d6
    .local v15, arr$:[Landroid/view/SurfaceHolder$Callback;
    array-length v0, v15

    #@2d7
    move/from16 v24, v0

    #@2d9
    .local v24, len$:I
    const/16 v23, 0x0

    #@2db
    .local v23, i$:I
    :goto_2db
    move/from16 v0, v23

    #@2dd
    move/from16 v1, v24

    #@2df
    if-ge v0, v1, :cond_2ef

    #@2e1
    aget-object v16, v15, v23

    #@2e3
    .line 691
    .local v16, c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@2e5
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@2e7
    move-object/from16 v0, v16

    #@2e9
    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    #@2ec
    .line 690
    add-int/lit8 v23, v23, 0x1

    #@2ee
    goto :goto_2db

    #@2ef
    .line 696
    .end local v15           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v16           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v17           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v23           #i$:I
    .end local v24           #len$:I
    :cond_2ef
    if-nez v18, :cond_2f5

    #@2f1
    and-int/lit8 v2, v28, 0x2

    #@2f3
    if-eqz v2, :cond_34a

    #@2f5
    :cond_2f5
    const/4 v2, 0x1

    #@2f6
    :goto_2f6
    or-int p3, p3, v2

    #@2f8
    .line 699
    if-nez p2, :cond_302

    #@2fa
    if-nez v18, :cond_302

    #@2fc
    if-nez v30, :cond_302

    #@2fe
    if-nez v21, :cond_302

    #@300
    if-eqz v29, :cond_34c

    #@302
    .line 712
    :cond_302
    const/16 v19, 0x1

    #@304
    .line 713
    move-object/from16 v0, p0

    #@306
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@308
    move-object/from16 v0, p0

    #@30a
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    #@30c
    move-object/from16 v0, p0

    #@30e
    iget v4, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@310
    move-object/from16 v0, p0

    #@312
    iget v5, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@314
    move-object/from16 v0, p0

    #@316
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    #@319
    .line 715
    move-object/from16 v0, p0

    #@31b
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@31d
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@320
    move-result-object v17

    #@321
    .line 716
    .restart local v17       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v17, :cond_34c

    #@323
    .line 717
    move-object/from16 v15, v17

    #@325
    .restart local v15       #arr$:[Landroid/view/SurfaceHolder$Callback;
    array-length v0, v15

    #@326
    move/from16 v24, v0

    #@328
    .restart local v24       #len$:I
    const/16 v23, 0x0

    #@32a
    .restart local v23       #i$:I
    :goto_32a
    move/from16 v0, v23

    #@32c
    move/from16 v1, v24

    #@32e
    if-ge v0, v1, :cond_34c

    #@330
    aget-object v16, v15, v23

    #@332
    .line 718
    .restart local v16       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@334
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@336
    move-object/from16 v0, p0

    #@338
    iget v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    #@33a
    move-object/from16 v0, p0

    #@33c
    iget v4, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    #@33e
    move-object/from16 v0, p0

    #@340
    iget v5, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    #@342
    move-object/from16 v0, v16

    #@344
    invoke-interface {v0, v2, v3, v4, v5}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    #@347
    .line 717
    add-int/lit8 v23, v23, 0x1

    #@349
    goto :goto_32a

    #@34a
    .line 696
    .end local v15           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v16           #c:Landroid/view/SurfaceHolder$Callback;
    .end local v17           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v23           #i$:I
    .end local v24           #len$:I
    :cond_34a
    const/4 v2, 0x0

    #@34b
    goto :goto_2f6

    #@34c
    .line 724
    :cond_34c
    if-eqz p3, :cond_384

    #@34e
    .line 725
    move-object/from16 v0, p0

    #@350
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@352
    move-object/from16 v0, p0

    #@354
    invoke-virtual {v0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    #@357
    .line 726
    move-object/from16 v0, p0

    #@359
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@35b
    invoke-virtual {v2}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    #@35e
    move-result-object v17

    #@35f
    .line 727
    .restart local v17       #callbacks:[Landroid/view/SurfaceHolder$Callback;
    if-eqz v17, :cond_384

    #@361
    .line 728
    move-object/from16 v15, v17

    #@363
    .restart local v15       #arr$:[Landroid/view/SurfaceHolder$Callback;
    array-length v0, v15

    #@364
    move/from16 v24, v0

    #@366
    .restart local v24       #len$:I
    const/16 v23, 0x0

    #@368
    .restart local v23       #i$:I
    :goto_368
    move/from16 v0, v23

    #@36a
    move/from16 v1, v24

    #@36c
    if-ge v0, v1, :cond_384

    #@36e
    aget-object v16, v15, v23

    #@370
    .line 729
    .restart local v16       #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, v16

    #@372
    instance-of v2, v0, Landroid/view/SurfaceHolder$Callback2;

    #@374
    if-eqz v2, :cond_381

    #@376
    .line 730
    check-cast v16, Landroid/view/SurfaceHolder$Callback2;

    #@378
    .end local v16           #c:Landroid/view/SurfaceHolder$Callback;
    move-object/from16 v0, p0

    #@37a
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    #@37c
    move-object/from16 v0, v16

    #@37e
    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder$Callback2;->surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    #@381
    .line 728
    :cond_381
    add-int/lit8 v23, v23, 0x1

    #@383
    goto :goto_368

    #@384
    .line 737
    .end local v15           #arr$:[Landroid/view/SurfaceHolder$Callback;
    .end local v17           #callbacks:[Landroid/view/SurfaceHolder$Callback;
    .end local v23           #i$:I
    .end local v24           #len$:I
    :cond_384
    if-eqz v19, :cond_39e

    #@386
    move-object/from16 v0, p0

    #@388
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    #@38a
    if-nez v2, :cond_39e

    #@38c
    .line 743
    move-object/from16 v0, p0

    #@38e
    iget-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    #@390
    if-eqz v2, :cond_398

    #@392
    .line 750
    const/4 v2, 0x1

    #@393
    move-object/from16 v0, p0

    #@395
    invoke-virtual {v0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    #@398
    .line 754
    :cond_398
    const/4 v2, 0x0

    #@399
    move-object/from16 v0, p0

    #@39b
    invoke-virtual {v0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V
    :try_end_39e
    .catchall {:try_start_2b1 .. :try_end_39e} :catchall_3be

    #@39e
    .line 758
    :cond_39e
    const/4 v2, 0x0

    #@39f
    :try_start_39f
    move-object/from16 v0, p0

    #@3a1
    iput-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    #@3a3
    .line 759
    const/4 v2, 0x1

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    iput-boolean v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@3a8
    .line 760
    if-eqz p3, :cond_3b5

    #@3aa
    .line 761
    move-object/from16 v0, p0

    #@3ac
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@3ae
    move-object/from16 v0, p0

    #@3b0
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@3b2
    invoke-interface {v2, v3}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V

    #@3b5
    .line 763
    :cond_3b5
    move-object/from16 v0, p0

    #@3b7
    iget-object v2, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@3b9
    invoke-virtual {v2}, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->reportShown()V

    #@3bc
    goto/16 :goto_1d2

    #@3be
    .line 758
    :catchall_3be
    move-exception v2

    #@3bf
    const/4 v3, 0x0

    #@3c0
    move-object/from16 v0, p0

    #@3c2
    iput-boolean v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    #@3c4
    .line 759
    const/4 v3, 0x1

    #@3c5
    move-object/from16 v0, p0

    #@3c7
    iput-boolean v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    #@3c9
    .line 760
    if-eqz p3, :cond_3d6

    #@3cb
    .line 761
    move-object/from16 v0, p0

    #@3cd
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    #@3cf
    move-object/from16 v0, p0

    #@3d1
    iget-object v4, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    #@3d3
    invoke-interface {v3, v4}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;)V

    #@3d6
    .line 763
    :cond_3d6
    move-object/from16 v0, p0

    #@3d8
    iget-object v3, v0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    #@3da
    invoke-virtual {v3}, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->reportShown()V

    #@3dd
    .line 758
    throw v2
    :try_end_3de
    .catch Landroid/os/RemoteException; {:try_start_39f .. :try_end_3de} :catch_1f4
.end method
