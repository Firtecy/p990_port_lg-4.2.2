.class public abstract Landroid/service/dreams/IDreamManager$Stub;
.super Landroid/os/Binder;
.source "IDreamManager.java"

# interfaces
.implements Landroid/service/dreams/IDreamManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/dreams/IDreamManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/dreams/IDreamManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.service.dreams.IDreamManager"

.field static final TRANSACTION_awaken:I = 0x2

.field static final TRANSACTION_dream:I = 0x1

.field static final TRANSACTION_finishSelf:I = 0x8

.field static final TRANSACTION_getDefaultDreamComponent:I = 0x5

.field static final TRANSACTION_getDreamComponents:I = 0x4

.field static final TRANSACTION_isDreaming:I = 0x7

.field static final TRANSACTION_setDreamComponents:I = 0x3

.field static final TRANSACTION_testDream:I = 0x6


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 16
    const-string v0, "android.service.dreams.IDreamManager"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/service/dreams/IDreamManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/service/dreams/IDreamManager;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 24
    if-nez p0, :cond_4

    #@2
    .line 25
    const/4 v0, 0x0

    #@3
    .line 31
    :goto_3
    return-object v0

    #@4
    .line 27
    :cond_4
    const-string v1, "android.service.dreams.IDreamManager"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 28
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/service/dreams/IDreamManager;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 29
    check-cast v0, Landroid/service/dreams/IDreamManager;

    #@12
    goto :goto_3

    #@13
    .line 31
    :cond_13
    new-instance v0, Landroid/service/dreams/IDreamManager$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/service/dreams/IDreamManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 39
    sparse-switch p1, :sswitch_data_a6

    #@5
    .line 123
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v3

    #@9
    :goto_9
    return v3

    #@a
    .line 43
    :sswitch_a
    const-string v2, "android.service.dreams.IDreamManager"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 48
    :sswitch_10
    const-string v2, "android.service.dreams.IDreamManager"

    #@12
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p0}, Landroid/service/dreams/IDreamManager$Stub;->dream()V

    #@18
    .line 50
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    goto :goto_9

    #@1c
    .line 55
    :sswitch_1c
    const-string v2, "android.service.dreams.IDreamManager"

    #@1e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@21
    .line 56
    invoke-virtual {p0}, Landroid/service/dreams/IDreamManager$Stub;->awaken()V

    #@24
    .line 57
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27
    goto :goto_9

    #@28
    .line 62
    :sswitch_28
    const-string v2, "android.service.dreams.IDreamManager"

    #@2a
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2d
    .line 64
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2f
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@32
    move-result-object v0

    #@33
    check-cast v0, [Landroid/content/ComponentName;

    #@35
    .line 65
    .local v0, _arg0:[Landroid/content/ComponentName;
    invoke-virtual {p0, v0}, Landroid/service/dreams/IDreamManager$Stub;->setDreamComponents([Landroid/content/ComponentName;)V

    #@38
    .line 66
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3b
    goto :goto_9

    #@3c
    .line 71
    .end local v0           #_arg0:[Landroid/content/ComponentName;
    :sswitch_3c
    const-string v2, "android.service.dreams.IDreamManager"

    #@3e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 72
    invoke-virtual {p0}, Landroid/service/dreams/IDreamManager$Stub;->getDreamComponents()[Landroid/content/ComponentName;

    #@44
    move-result-object v1

    #@45
    .line 73
    .local v1, _result:[Landroid/content/ComponentName;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@48
    .line 74
    invoke-virtual {p3, v1, v3}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@4b
    goto :goto_9

    #@4c
    .line 79
    .end local v1           #_result:[Landroid/content/ComponentName;
    :sswitch_4c
    const-string v4, "android.service.dreams.IDreamManager"

    #@4e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@51
    .line 80
    invoke-virtual {p0}, Landroid/service/dreams/IDreamManager$Stub;->getDefaultDreamComponent()Landroid/content/ComponentName;

    #@54
    move-result-object v1

    #@55
    .line 81
    .local v1, _result:Landroid/content/ComponentName;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58
    .line 82
    if-eqz v1, :cond_61

    #@5a
    .line 83
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@5d
    .line 84
    invoke-virtual {v1, p3, v3}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@60
    goto :goto_9

    #@61
    .line 87
    :cond_61
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@64
    goto :goto_9

    #@65
    .line 93
    .end local v1           #_result:Landroid/content/ComponentName;
    :sswitch_65
    const-string v2, "android.service.dreams.IDreamManager"

    #@67
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6a
    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v2

    #@6e
    if-eqz v2, :cond_7f

    #@70
    .line 96
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@72
    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@75
    move-result-object v0

    #@76
    check-cast v0, Landroid/content/ComponentName;

    #@78
    .line 101
    .local v0, _arg0:Landroid/content/ComponentName;
    :goto_78
    invoke-virtual {p0, v0}, Landroid/service/dreams/IDreamManager$Stub;->testDream(Landroid/content/ComponentName;)V

    #@7b
    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    goto :goto_9

    #@7f
    .line 99
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :cond_7f
    const/4 v0, 0x0

    #@80
    .restart local v0       #_arg0:Landroid/content/ComponentName;
    goto :goto_78

    #@81
    .line 107
    .end local v0           #_arg0:Landroid/content/ComponentName;
    :sswitch_81
    const-string v4, "android.service.dreams.IDreamManager"

    #@83
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@86
    .line 108
    invoke-virtual {p0}, Landroid/service/dreams/IDreamManager$Stub;->isDreaming()Z

    #@89
    move-result v1

    #@8a
    .line 109
    .local v1, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8d
    .line 110
    if-eqz v1, :cond_90

    #@8f
    move v2, v3

    #@90
    :cond_90
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@93
    goto/16 :goto_9

    #@95
    .line 115
    .end local v1           #_result:Z
    :sswitch_95
    const-string v2, "android.service.dreams.IDreamManager"

    #@97
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9a
    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@9d
    move-result-object v0

    #@9e
    .line 118
    .local v0, _arg0:Landroid/os/IBinder;
    invoke-virtual {p0, v0}, Landroid/service/dreams/IDreamManager$Stub;->finishSelf(Landroid/os/IBinder;)V

    #@a1
    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a4
    goto/16 :goto_9

    #@a6
    .line 39
    :sswitch_data_a6
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_28
        0x4 -> :sswitch_3c
        0x5 -> :sswitch_4c
        0x6 -> :sswitch_65
        0x7 -> :sswitch_81
        0x8 -> :sswitch_95
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
