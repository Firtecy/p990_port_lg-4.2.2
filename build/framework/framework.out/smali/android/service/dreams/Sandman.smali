.class public final Landroid/service/dreams/Sandman;
.super Ljava/lang/Object;
.source "Sandman.java"


# static fields
.field private static final SOMNAMBULATOR_COMPONENT:Landroid/content/ComponentName; = null

.field private static final TAG:Ljava/lang/String; = "Sandman"


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 42
    new-instance v0, Landroid/content/ComponentName;

    #@2
    const-string v1, "com.android.systemui"

    #@4
    const-string v2, "com.android.systemui.Somnambulator"

    #@6
    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    sput-object v0, Landroid/service/dreams/Sandman;->SOMNAMBULATOR_COMPONENT:Landroid/content/ComponentName;

    #@b
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    return-void
.end method

.method private static isScreenSaverActivatedOnDock(Landroid/content/Context;)Z
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 117
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v3

    #@6
    const v4, 0x1110042

    #@9
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_1f

    #@f
    move v0, v1

    #@10
    .line 119
    .local v0, def:I
    :goto_10
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v3

    #@14
    const-string/jumbo v4, "screensaver_activate_on_dock"

    #@17
    const/4 v5, -0x2

    #@18
    invoke-static {v3, v4, v0, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_21

    #@1e
    :goto_1e
    return v1

    #@1f
    .end local v0           #def:I
    :cond_1f
    move v0, v2

    #@20
    .line 117
    goto :goto_10

    #@21
    .restart local v0       #def:I
    :cond_21
    move v1, v2

    #@22
    .line 119
    goto :goto_1e
.end method

.method private static isScreenSaverEnabled(Landroid/content/Context;)Z
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 109
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v3

    #@6
    const v4, 0x1110041

    #@9
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_1f

    #@f
    move v0, v1

    #@10
    .line 111
    .local v0, def:I
    :goto_10
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v3

    #@14
    const-string/jumbo v4, "screensaver_enabled"

    #@17
    const/4 v5, -0x2

    #@18
    invoke-static {v3, v4, v0, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_21

    #@1e
    :goto_1e
    return v1

    #@1f
    .end local v0           #def:I
    :cond_1f
    move v0, v2

    #@20
    .line 109
    goto :goto_10

    #@21
    .restart local v0       #def:I
    :cond_21
    move v1, v2

    #@22
    .line 111
    goto :goto_1e
.end method

.method public static shouldStartDockApp(Landroid/content/Context;Landroid/content/Intent;)Z
    .registers 4
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {p1, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    #@7
    move-result-object v0

    #@8
    .line 56
    .local v0, name:Landroid/content/ComponentName;
    if-eqz v0, :cond_14

    #@a
    sget-object v1, Landroid/service/dreams/Sandman;->SOMNAMBULATOR_COMPONENT:Landroid/content/ComponentName;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_14

    #@12
    const/4 v1, 0x1

    #@13
    :goto_13
    return v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method private static startDream(Landroid/content/Context;Z)V
    .registers 7
    .parameter "context"
    .parameter "docked"

    #@0
    .prologue
    .line 82
    :try_start_0
    const-string v3, "dreams"

    #@2
    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v3

    #@6
    invoke-static {v3}, Landroid/service/dreams/IDreamManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/dreams/IDreamManager;

    #@9
    move-result-object v0

    #@a
    .line 84
    .local v0, dreamManagerService:Landroid/service/dreams/IDreamManager;
    if-eqz v0, :cond_2e

    #@c
    invoke-interface {v0}, Landroid/service/dreams/IDreamManager;->isDreaming()Z

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_2e

    #@12
    .line 85
    if-eqz p1, :cond_2f

    #@14
    .line 86
    const-string v3, "Sandman"

    #@16
    const-string v4, "Activating dream while docked."

    #@18
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 93
    const-string/jumbo v3, "power"

    #@1e
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@21
    move-result-object v2

    #@22
    check-cast v2, Landroid/os/PowerManager;

    #@24
    .line 95
    .local v2, powerManager:Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@27
    move-result-wide v3

    #@28
    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->wakeUp(J)V

    #@2b
    .line 101
    .end local v2           #powerManager:Landroid/os/PowerManager;
    :goto_2b
    invoke-interface {v0}, Landroid/service/dreams/IDreamManager;->dream()V

    #@2e
    .line 106
    .end local v0           #dreamManagerService:Landroid/service/dreams/IDreamManager;
    :cond_2e
    :goto_2e
    return-void

    #@2f
    .line 97
    .restart local v0       #dreamManagerService:Landroid/service/dreams/IDreamManager;
    :cond_2f
    const-string v3, "Sandman"

    #@31
    const-string v4, "Activating dream by user request."

    #@33
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_36} :catch_37

    #@36
    goto :goto_2b

    #@37
    .line 103
    .end local v0           #dreamManagerService:Landroid/service/dreams/IDreamManager;
    :catch_37
    move-exception v1

    #@38
    .line 104
    .local v1, ex:Landroid/os/RemoteException;
    const-string v3, "Sandman"

    #@3a
    const-string v4, "Could not start dream when docked."

    #@3c
    invoke-static {v3, v4, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    goto :goto_2e
.end method

.method public static startDreamByUserRequest(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 63
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/service/dreams/Sandman;->startDream(Landroid/content/Context;Z)V

    #@4
    .line 64
    return-void
.end method

.method public static startDreamWhenDockedIfAppropriate(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 71
    invoke-static {p0}, Landroid/service/dreams/Sandman;->isScreenSaverEnabled(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_c

    #@6
    invoke-static {p0}, Landroid/service/dreams/Sandman;->isScreenSaverActivatedOnDock(Landroid/content/Context;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_14

    #@c
    .line 73
    :cond_c
    const-string v0, "Sandman"

    #@e
    const-string v1, "Dreams currently disabled for docks."

    #@10
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 78
    :goto_13
    return-void

    #@14
    .line 77
    :cond_14
    const/4 v0, 0x1

    #@15
    invoke-static {p0, v0}, Landroid/service/dreams/Sandman;->startDream(Landroid/content/Context;Z)V

    #@18
    goto :goto_13
.end method
