.class Landroid/service/dreams/IDreamManager$Stub$Proxy;
.super Ljava/lang/Object;
.source "IDreamManager.java"

# interfaces
.implements Landroid/service/dreams/IDreamManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/dreams/IDreamManager$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 129
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 130
    iput-object p1, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 131
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public awaken()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 156
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 157
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 159
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 160
    iget-object v2, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x2

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 161
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 164
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 165
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 167
    return-void

    #@1e
    .line 164
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 165
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public dream()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 143
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 145
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 146
    iget-object v2, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v3, 0x1

    #@10
    const/4 v4, 0x0

    #@11
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 147
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    .line 150
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 151
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 153
    return-void

    #@1e
    .line 150
    :catchall_1e
    move-exception v2

    #@1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 151
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    throw v2
.end method

.method public finishSelf(Landroid/os/IBinder;)V
    .registers 7
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 262
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 263
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 265
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 266
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@10
    .line 267
    iget-object v2, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x8

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 268
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 271
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 272
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 274
    return-void

    #@22
    .line 271
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 272
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public getDefaultDreamComponent()Landroid/content/ComponentName;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 202
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 203
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 206
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 207
    iget-object v3, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x5

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 209
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_2c

    #@1d
    .line 210
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1f
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@22
    move-result-object v2

    #@23
    check-cast v2, Landroid/content/ComponentName;
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_2e

    #@25
    .line 217
    .local v2, _result:Landroid/content/ComponentName;
    :goto_25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 218
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 220
    return-object v2

    #@2c
    .line 213
    .end local v2           #_result:Landroid/content/ComponentName;
    :cond_2c
    const/4 v2, 0x0

    #@2d
    .restart local v2       #_result:Landroid/content/ComponentName;
    goto :goto_25

    #@2e
    .line 217
    .end local v2           #_result:Landroid/content/ComponentName;
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 218
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public getDreamComponents()[Landroid/content/ComponentName;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 189
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 190
    iget-object v3, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 191
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 192
    sget-object v3, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    #@19
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, [Landroid/content/ComponentName;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_26

    #@1f
    .line 195
    .local v2, _result:[Landroid/content/ComponentName;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 196
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 198
    return-object v2

    #@26
    .line 195
    .end local v2           #_result:[Landroid/content/ComponentName;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 196
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 138
    const-string v0, "android.service.dreams.IDreamManager"

    #@2
    return-object v0
.end method

.method public isDreaming()Z
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 245
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 246
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 249
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.service.dreams.IDreamManager"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 250
    iget-object v3, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@10
    const/4 v4, 0x7

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 251
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 252
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_9 .. :try_end_1b} :catchall_26

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_1f

    #@1e
    const/4 v2, 0x1

    #@1f
    .line 255
    .local v2, _result:Z
    :cond_1f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 256
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 258
    return v2

    #@26
    .line 255
    .end local v2           #_result:Z
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 256
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public setDreamComponents([Landroid/content/ComponentName;)V
    .registers 7
    .parameter "componentNames"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 170
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 171
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 173
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 174
    const/4 v2, 0x0

    #@e
    invoke-virtual {v0, p1, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    #@11
    .line 175
    iget-object v2, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/4 v3, 0x3

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 176
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 179
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 180
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 182
    return-void

    #@22
    .line 179
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 180
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public testDream(Landroid/content/ComponentName;)V
    .registers 7
    .parameter "componentName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 225
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 227
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.service.dreams.IDreamManager"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 228
    if-eqz p1, :cond_28

    #@f
    .line 229
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 230
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 235
    :goto_17
    iget-object v2, p0, Landroid/service/dreams/IDreamManager$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x6

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 236
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 239
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 240
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 242
    return-void

    #@28
    .line 233
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 239
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 240
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method
