.class public Landroid/service/dreams/DreamService;
.super Landroid/app/Service;
.source "DreamService.java"

# interfaces
.implements Landroid/view/Window$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/dreams/DreamService$DreamServiceWrapper;
    }
.end annotation


# static fields
.field public static final DREAM_META_DATA:Ljava/lang/String; = "android.service.dream"

.field public static final DREAM_SERVICE:Ljava/lang/String; = "dreams"

.field public static final SERVICE_INTERFACE:Ljava/lang/String; = "android.service.dreams.DreamService"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mDebug:Z

.field private mFinished:Z

.field private mFullscreen:Z

.field private final mHandler:Landroid/os/Handler;

.field private mInteractive:Z

.field private mLcdOledConfig:Z

.field private mLowProfile:Z

.field private mSandman:Landroid/service/dreams/IDreamManager;

.field private mScreenBright:Z

.field private mWindow:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 128
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@4
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-class v1, Landroid/service/dreams/DreamService;

    #@b
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "["

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    const-string v1, "]"

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v0

    #@2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@31
    .line 152
    new-instance v0, Landroid/os/Handler;

    #@33
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@36
    iput-object v0, p0, Landroid/service/dreams/DreamService;->mHandler:Landroid/os/Handler;

    #@38
    .line 157
    iput-boolean v2, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@3a
    .line 158
    iput-boolean v2, p0, Landroid/service/dreams/DreamService;->mLowProfile:Z

    #@3c
    .line 159
    iput-boolean v2, p0, Landroid/service/dreams/DreamService;->mFullscreen:Z

    #@3e
    .line 160
    const/4 v0, 0x1

    #@3f
    iput-boolean v0, p0, Landroid/service/dreams/DreamService;->mScreenBright:Z

    #@41
    .line 163
    iput-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@43
    .line 166
    iput-boolean v2, p0, Landroid/service/dreams/DreamService;->mLcdOledConfig:Z

    #@45
    .line 785
    return-void
.end method

.method static synthetic access$100(Landroid/service/dreams/DreamService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Landroid/service/dreams/DreamService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 128
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@3
    return-void
.end method

.method static synthetic access$300(Landroid/service/dreams/DreamService;Landroid/os/IBinder;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 128
    invoke-direct {p0, p1}, Landroid/service/dreams/DreamService;->attach(Landroid/os/IBinder;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Landroid/service/dreams/DreamService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/service/dreams/DreamService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 128
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->detach()V

    #@3
    return-void
.end method

.method static synthetic access$600(Landroid/service/dreams/DreamService;)Landroid/view/Window;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Landroid/service/dreams/DreamService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 128
    invoke-direct {p0, p1}, Landroid/service/dreams/DreamService;->hideNavigationBar(Z)V

    #@3
    return-void
.end method

.method private applyFlags(III)I
    .registers 6
    .parameter "oldFlags"
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 763
    xor-int/lit8 v0, p3, -0x1

    #@2
    and-int/2addr v0, p1

    #@3
    and-int v1, p2, p3

    #@5
    or-int/2addr v0, v1

    #@6
    return v0
.end method

.method private applySystemUiVisibilityFlags(II)V
    .registers 5
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 756
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    if-nez v1, :cond_13

    #@4
    const/4 v0, 0x0

    #@5
    .line 757
    .local v0, v:Landroid/view/View;
    :goto_5
    if-eqz v0, :cond_12

    #@7
    .line 758
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    #@a
    move-result v1

    #@b
    invoke-direct {p0, v1, p1, p2}, Landroid/service/dreams/DreamService;->applyFlags(III)I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    #@12
    .line 760
    :cond_12
    return-void

    #@13
    .line 756
    .end local v0           #v:Landroid/view/View;
    :cond_13
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@15
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    goto :goto_5
.end method

.method private applyWindowFlags(II)V
    .registers 6
    .parameter "flags"
    .parameter "mask"

    #@0
    .prologue
    .line 742
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    if-eqz v1, :cond_22

    #@4
    .line 743
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@6
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@9
    move-result-object v0

    #@a
    .line 744
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@c
    invoke-direct {p0, v1, p1, p2}, Landroid/service/dreams/DreamService;->applyFlags(III)I

    #@f
    move-result v1

    #@10
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@12
    .line 745
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@14
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@17
    .line 746
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindowManager:Landroid/view/WindowManager;

    #@19
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@1b
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1e
    move-result-object v2

    #@1f
    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@22
    .line 748
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    :cond_22
    return-void
.end method

.method private final attach(Landroid/os/IBinder;)V
    .registers 11
    .parameter "windowToken"

    #@0
    .prologue
    const/16 v8, 0x7e7

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 638
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@6
    if-eqz v2, :cond_23

    #@8
    .line 639
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "attach() called when already attached with token="

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    iget-object v4, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 701
    :goto_22
    return-void

    #@23
    .line 643
    :cond_23
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@25
    if-eqz v2, :cond_47

    #@27
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@29
    new-instance v5, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v6, "Attached on thread "

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    #@3b
    move-result-wide v6

    #@3c
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v5

    #@44
    invoke-static {v2, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 645
    :cond_47
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mSandman:Landroid/service/dreams/IDreamManager;

    #@49
    if-nez v2, :cond_4e

    #@4b
    .line 646
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->loadSandman()V

    #@4e
    .line 648
    :cond_4e
    iput-object p1, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@50
    .line 649
    invoke-static {p0}, Lcom/android/internal/policy/PolicyManager;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@53
    move-result-object v2

    #@54
    iput-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@56
    .line 650
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@58
    invoke-virtual {v2, p0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    #@5b
    .line 651
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@5d
    invoke-virtual {v2, v4}, Landroid/view/Window;->requestFeature(I)Z

    #@60
    .line 652
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@62
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    #@64
    const/high16 v6, -0x100

    #@66
    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    #@69
    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    #@6c
    .line 653
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@6e
    const/4 v5, -0x1

    #@6f
    invoke-virtual {v2, v5}, Landroid/view/Window;->setFormat(I)V

    #@72
    .line 655
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@74
    if-eqz v2, :cond_8c

    #@76
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@78
    const-string v5, "Attaching window token: %s to window of type %s"

    #@7a
    const/4 v6, 0x2

    #@7b
    new-array v6, v6, [Ljava/lang/Object;

    #@7d
    aput-object p1, v6, v3

    #@7f
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@82
    move-result-object v7

    #@83
    aput-object v7, v6, v4

    #@85
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@88
    move-result-object v5

    #@89
    invoke-static {v2, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 658
    :cond_8c
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@8e
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@91
    move-result-object v0

    #@92
    .line 659
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    iput v8, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@94
    .line 660
    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@96
    .line 661
    const v2, 0x10301f1

    #@99
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@9b
    .line 662
    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@9d
    const v6, 0x490101

    #@a0
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mFullscreen:Z

    #@a2
    if-eqz v2, :cond_130

    #@a4
    const/16 v2, 0x400

    #@a6
    :goto_a6
    or-int/2addr v6, v2

    #@a7
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mScreenBright:Z

    #@a9
    if-eqz v2, :cond_133

    #@ab
    const/16 v2, 0x80

    #@ad
    :goto_ad
    or-int/2addr v2, v6

    #@ae
    or-int/2addr v2, v5

    #@af
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@b1
    .line 670
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@b3
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    #@b6
    .line 672
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@b8
    if-eqz v2, :cond_d4

    #@ba
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@bc
    new-instance v5, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v6, "Created and attached window: "

    #@c3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v5

    #@c7
    iget-object v6, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-static {v2, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 674
    :cond_d4
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@d6
    const/4 v5, 0x0

    #@d7
    const-string v6, "dream"

    #@d9
    invoke-virtual {v2, v5, p1, v6, v4}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;Z)V

    #@dc
    .line 675
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@de
    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    #@e1
    move-result-object v2

    #@e2
    iput-object v2, p0, Landroid/service/dreams/DreamService;->mWindowManager:Landroid/view/WindowManager;

    #@e4
    .line 677
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@e6
    if-eqz v2, :cond_108

    #@e8
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@ea
    new-instance v5, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v6, "Window added on thread "

    #@f1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v5

    #@f5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@f8
    move-result-object v6

    #@f9
    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    #@fc
    move-result-wide v6

    #@fd
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@100
    move-result-object v5

    #@101
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v5

    #@105
    invoke-static {v2, v5}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 679
    :cond_108
    :try_start_108
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mLowProfile:Z

    #@10a
    if-eqz v2, :cond_10d

    #@10c
    move v3, v4

    #@10d
    :cond_10d
    const/4 v2, 0x1

    #@10e
    invoke-direct {p0, v3, v2}, Landroid/service/dreams/DreamService;->applySystemUiVisibilityFlags(II)V

    #@111
    .line 682
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindowManager()Landroid/view/WindowManager;

    #@114
    move-result-object v2

    #@115
    iget-object v3, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@117
    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@11a
    move-result-object v3

    #@11b
    iget-object v4, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@11d
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@120
    move-result-object v4

    #@121
    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_124
    .catch Ljava/lang/Throwable; {:try_start_108 .. :try_end_124} :catch_136

    #@124
    .line 690
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mHandler:Landroid/os/Handler;

    #@126
    new-instance v3, Landroid/service/dreams/DreamService$1;

    #@128
    invoke-direct {v3, p0}, Landroid/service/dreams/DreamService$1;-><init>(Landroid/service/dreams/DreamService;)V

    #@12b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@12e
    goto/16 :goto_22

    #@130
    :cond_130
    move v2, v3

    #@131
    .line 662
    goto/16 :goto_a6

    #@133
    :cond_133
    move v2, v3

    #@134
    goto/16 :goto_ad

    #@136
    .line 683
    :catch_136
    move-exception v1

    #@137
    .line 684
    .local v1, t:Ljava/lang/Throwable;
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@139
    const-string v3, "Crashed adding window view"

    #@13b
    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@13e
    .line 685
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@141
    goto/16 :goto_22
.end method

.method private final detach()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 603
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 628
    :goto_5
    return-void

    #@6
    .line 609
    :cond_6
    :try_start_6
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->onDreamingStopped()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_9} :catch_37

    #@9
    .line 615
    :goto_9
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@b
    if-eqz v1, :cond_14

    #@d
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@f
    const-string v2, "detach(): Removing window from window manager"

    #@11
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 618
    :cond_14
    :try_start_14
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindowManager:Landroid/view/WindowManager;

    #@16
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@18
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@1b
    move-result-object v2

    #@1c
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    #@1f
    .line 620
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    const-string v4, "Dream"

    #@2f
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/WindowManagerGlobal;->closeAll(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_32} :catch_40

    #@32
    .line 626
    :goto_32
    iput-object v5, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@34
    .line 627
    iput-object v5, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@36
    goto :goto_5

    #@37
    .line 610
    :catch_37
    move-exception v0

    #@38
    .line 611
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@3a
    const-string v2, "Crashed in onDreamingStopped()"

    #@3c
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3f
    goto :goto_9

    #@40
    .line 622
    .end local v0           #t:Ljava/lang/Throwable;
    :catch_40
    move-exception v0

    #@41
    .line 623
    .restart local v0       #t:Ljava/lang/Throwable;
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@43
    const-string v2, "Crashed removing window view"

    #@45
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_32
.end method

.method private finishInternal()V
    .registers 5

    #@0
    .prologue
    .line 720
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v1, :cond_1e

    #@4
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "finishInternal() mFinished = "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    iget-boolean v3, p0, Landroid/service/dreams/DreamService;->mFinished:Z

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 721
    :cond_1e
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mFinished:Z

    #@20
    if-eqz v1, :cond_23

    #@22
    .line 735
    :goto_22
    return-void

    #@23
    .line 723
    :cond_23
    const/4 v1, 0x1

    #@24
    :try_start_24
    iput-boolean v1, p0, Landroid/service/dreams/DreamService;->mFinished:Z

    #@26
    .line 725
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mSandman:Landroid/service/dreams/IDreamManager;

    #@28
    if-eqz v1, :cond_3e

    #@2a
    .line 726
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mSandman:Landroid/service/dreams/IDreamManager;

    #@2c
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@2e
    invoke-interface {v1, v2}, Landroid/service/dreams/IDreamManager;->finishSelf(Landroid/os/IBinder;)V

    #@31
    .line 730
    :goto_31
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->stopSelf()V
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_24 .. :try_end_34} :catch_35

    #@34
    goto :goto_22

    #@35
    .line 732
    :catch_35
    move-exception v0

    #@36
    .line 733
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@38
    const-string v2, "Crashed in finishInternal()"

    #@3a
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3d
    goto :goto_22

    #@3e
    .line 728
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_3e
    :try_start_3e
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@40
    const-string v2, "No dream manager found"

    #@42
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_45
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_45} :catch_35

    #@45
    goto :goto_31
.end method

.method private getSystemUiVisibilityFlagValue(IZ)Z
    .registers 5
    .parameter "flag"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 751
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    if-nez v1, :cond_8

    #@4
    const/4 v0, 0x0

    #@5
    .line 752
    .local v0, v:Landroid/view/View;
    :goto_5
    if-nez v0, :cond_f

    #@7
    .end local p2
    :goto_7
    return p2

    #@8
    .line 751
    .end local v0           #v:Landroid/view/View;
    .restart local p2
    :cond_8
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@a
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@d
    move-result-object v0

    #@e
    goto :goto_5

    #@f
    .line 752
    .restart local v0       #v:Landroid/view/View;
    :cond_f
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    #@12
    move-result v1

    #@13
    and-int/2addr v1, p1

    #@14
    if-eqz v1, :cond_18

    #@16
    const/4 p2, 0x1

    #@17
    goto :goto_7

    #@18
    :cond_18
    const/4 p2, 0x0

    #@19
    goto :goto_7
.end method

.method private getWindowFlagValue(IZ)Z
    .registers 4
    .parameter "flag"
    .parameter "defaultValue"

    #@0
    .prologue
    .line 738
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    if-nez v0, :cond_5

    #@4
    .end local p2
    :goto_4
    return p2

    #@5
    .restart local p2
    :cond_5
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@7
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@a
    move-result-object v0

    #@b
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@d
    and-int/2addr v0, p1

    #@e
    if-eqz v0, :cond_12

    #@10
    const/4 p2, 0x1

    #@11
    goto :goto_4

    #@12
    :cond_12
    const/4 p2, 0x0

    #@13
    goto :goto_4
.end method

.method private hideNavigationBar(Z)V
    .registers 7
    .parameter "immediate"

    #@0
    .prologue
    .line 808
    const-wide/16 v0, 0xbb8

    #@2
    .line 810
    .local v0, delayMillis:J
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mLcdOledConfig:Z

    #@4
    if-nez v2, :cond_e

    #@6
    .line 811
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@8
    const-string v3, "hideNavigationBar() : not supported on non-OLED device !!"

    #@a
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 839
    :goto_d
    return-void

    #@e
    .line 815
    :cond_e
    if-eqz p1, :cond_12

    #@10
    .line 816
    const-wide/16 v0, 0x0

    #@12
    .line 818
    :cond_12
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@14
    if-eqz v2, :cond_38

    #@16
    iget-object v2, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "hideNavigationBar() : immediate = "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ", delayMillis = "

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 820
    :cond_38
    iget-object v2, p0, Landroid/service/dreams/DreamService;->mHandler:Landroid/os/Handler;

    #@3a
    new-instance v3, Landroid/service/dreams/DreamService$2;

    #@3c
    invoke-direct {v3, p0}, Landroid/service/dreams/DreamService$2;-><init>(Landroid/service/dreams/DreamService;)V

    #@3f
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@42
    goto :goto_d
.end method

.method private loadSandman()V
    .registers 2

    #@0
    .prologue
    .line 594
    const-string v0, "dreams"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Landroid/service/dreams/IDreamManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/dreams/IDreamManager;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Landroid/service/dreams/DreamService;->mSandman:Landroid/service/dreams/IDreamManager;

    #@c
    .line 595
    return-void
.end method

.method private safelyFinish()V
    .registers 4

    #@0
    .prologue
    .line 704
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v1, :cond_c

    #@4
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    const-string/jumbo v2, "safelyFinish()"

    #@9
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 706
    :cond_c
    :try_start_c
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->finish()V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_f} :catch_1e

    #@f
    .line 713
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mFinished:Z

    #@11
    if-nez v1, :cond_1d

    #@13
    .line 714
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@15
    const-string v2, "Bad dream, did not call super.finish()"

    #@17
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 715
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->finishInternal()V

    #@1d
    .line 717
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 707
    :catch_1e
    move-exception v0

    #@1f
    .line 708
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@21
    const-string v2, "Crashed in safelyFinish()"

    #@23
    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@26
    .line 709
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->finishInternal()V

    #@29
    goto :goto_1d
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 421
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@7
    .line 422
    return-void
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 243
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    if-nez v0, :cond_14

    #@4
    .line 244
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    const-string v1, "Finishing on genericMotionEvent"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 245
    :cond_f
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@12
    .line 246
    const/4 v0, 0x1

    #@13
    .line 248
    :goto_13
    return v0

    #@14
    :cond_14
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@16
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_13
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 5
    .parameter "event"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 181
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@3
    if-nez v1, :cond_14

    #@5
    .line 182
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@7
    if-eqz v1, :cond_10

    #@9
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@b
    const-string v2, "Finishing on keyEvent"

    #@d
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 183
    :cond_10
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@13
    .line 202
    :goto_13
    return v0

    #@14
    .line 185
    :cond_14
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@17
    move-result v1

    #@18
    const/4 v2, 0x4

    #@19
    if-ne v1, v2, :cond_2a

    #@1b
    .line 186
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@1d
    if-eqz v1, :cond_26

    #@1f
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@21
    const-string v2, "Finishing on back key"

    #@23
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 187
    :cond_26
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@29
    goto :goto_13

    #@2a
    .line 189
    :cond_2a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@2d
    move-result v1

    #@2e
    const/4 v2, 0x3

    #@2f
    if-ne v1, v2, :cond_40

    #@31
    .line 190
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@33
    if-eqz v1, :cond_3c

    #@35
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@37
    const-string v2, "Finishing on home key"

    #@39
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 191
    :cond_3c
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@3f
    goto :goto_13

    #@40
    .line 193
    :cond_40
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@43
    move-result v1

    #@44
    const/16 v2, 0x52

    #@46
    if-ne v1, v2, :cond_57

    #@48
    .line 194
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@4a
    if-eqz v1, :cond_53

    #@4c
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@4e
    const-string v2, "Finishing on menu key"

    #@50
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 195
    :cond_53
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@56
    goto :goto_13

    #@57
    .line 197
    :cond_57
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@5a
    move-result v1

    #@5b
    const/16 v2, 0x53

    #@5d
    if-ne v1, v2, :cond_6e

    #@5f
    .line 198
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@61
    if-eqz v1, :cond_6a

    #@63
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@65
    const-string v2, "Finishing on notification key"

    #@67
    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 199
    :cond_6a
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@6d
    goto :goto_13

    #@6e
    .line 202
    :cond_6e
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@70
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchKeyEvent(Landroid/view/KeyEvent;)Z

    #@73
    move-result v0

    #@74
    goto :goto_13
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 208
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    if-nez v0, :cond_14

    #@4
    .line 209
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    const-string v1, "Finishing on keyShortcutEvent"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 210
    :cond_f
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@12
    .line 211
    const/4 v0, 0x1

    #@13
    .line 213
    :goto_13
    return v0

    #@14
    :cond_14
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@16
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_13
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter "event"

    #@0
    .prologue
    .line 254
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 221
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    if-nez v0, :cond_14

    #@4
    .line 222
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    const-string v1, "Finishing on touchEvent"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 223
    :cond_f
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@12
    .line 224
    const/4 v0, 0x1

    #@13
    .line 226
    :goto_13
    return v0

    #@14
    :cond_14
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@16
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTouchEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_13
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 232
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    if-nez v0, :cond_14

    #@4
    .line 233
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    const-string v1, "Finishing on trackballEvent"

    #@c
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 234
    :cond_f
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->safelyFinish()V

    #@12
    .line 235
    const/4 v0, 0x1

    #@13
    .line 237
    :goto_13
    return v0

    #@14
    :cond_14
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@16
    invoke-virtual {v0, p1}, Landroid/view/Window;->superDispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    #@19
    move-result v0

    #@1a
    goto :goto_13
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6
    .parameter "fd"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 768
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    #@3
    .line 770
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    iget-object v1, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    const-string v1, ": "

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b
    .line 771
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@1d
    if-nez v0, :cond_72

    #@1f
    .line 772
    const-string/jumbo v0, "stopped"

    #@22
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@25
    .line 776
    :goto_25
    new-instance v0, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v1, "  window: "

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@3d
    .line 777
    const-string v0, "  flags:"

    #@3f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42
    .line 778
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->isInteractive()Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_4d

    #@48
    const-string v0, " interactive"

    #@4a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4d
    .line 779
    :cond_4d
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->isLowProfile()Z

    #@50
    move-result v0

    #@51
    if-eqz v0, :cond_58

    #@53
    const-string v0, " lowprofile"

    #@55
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@58
    .line 780
    :cond_58
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->isFullscreen()Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_63

    #@5e
    const-string v0, " fullscreen"

    #@60
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@63
    .line 781
    :cond_63
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->isScreenBright()Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_6e

    #@69
    const-string v0, " bright"

    #@6b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6e
    .line 782
    :cond_6e
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    #@71
    .line 783
    return-void

    #@72
    .line 774
    :cond_72
    new-instance v0, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string/jumbo v1, "running (token="

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v0

    #@7e
    iget-object v1, p0, Landroid/service/dreams/DreamService;->mWindowToken:Landroid/os/IBinder;

    #@80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v0

    #@84
    const-string v1, ")"

    #@86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v0

    #@8a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@91
    goto :goto_25
.end method

.method public findViewById(I)Landroid/view/View;
    .registers 3
    .parameter "id"

    #@0
    .prologue
    .line 433
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public final finish()V
    .registers 3

    #@0
    .prologue
    .line 575
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    const-string v1, "finish()"

    #@8
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 576
    :cond_b
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->finishInternal()V

    #@e
    .line 577
    return-void
.end method

.method public getWindow()Landroid/view/Window;
    .registers 2

    #@0
    .prologue
    .line 358
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindow:Landroid/view/Window;

    #@2
    return-object v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .registers 2

    #@0
    .prologue
    .line 348
    iget-object v0, p0, Landroid/service/dreams/DreamService;->mWindowManager:Landroid/view/WindowManager;

    #@2
    return-object v0
.end method

.method public isFullscreen()Z
    .registers 2

    #@0
    .prologue
    .line 505
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mFullscreen:Z

    #@2
    return v0
.end method

.method public isInteractive()Z
    .registers 2

    #@0
    .prologue
    .line 455
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    return v0
.end method

.method public isLowProfile()Z
    .registers 3

    #@0
    .prologue
    .line 483
    const/4 v0, 0x1

    #@1
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mLowProfile:Z

    #@3
    invoke-direct {p0, v0, v1}, Landroid/service/dreams/DreamService;->getSystemUiVisibilityFlagValue(IZ)Z

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public isScreenBright()Z
    .registers 3

    #@0
    .prologue
    .line 526
    const/16 v0, 0x80

    #@2
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mScreenBright:Z

    #@4
    invoke-direct {p0, v0, v1}, Landroid/service/dreams/DreamService;->getWindowFlagValue(IZ)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 337
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 332
    return-void
.end method

.method public onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 305
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 561
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_1d

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string/jumbo v2, "onBind() intent = "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 564
    :cond_1d
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getResources()Landroid/content/res/Resources;

    #@20
    move-result-object v0

    #@21
    const v1, 0x206002d

    #@24
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@27
    move-result v0

    #@28
    iput-boolean v0, p0, Landroid/service/dreams/DreamService;->mLcdOledConfig:Z

    #@2a
    .line 565
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@2c
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "OLEDMode, mLcdOledConfig : "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-boolean v2, p0, Landroid/service/dreams/DreamService;->mLcdOledConfig:Z

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 568
    new-instance v0, Landroid/service/dreams/DreamService$DreamServiceWrapper;

    #@46
    const/4 v1, 0x0

    #@47
    invoke-direct {v0, p0, v1}, Landroid/service/dreams/DreamService$DreamServiceWrapper;-><init>(Landroid/service/dreams/DreamService;Landroid/service/dreams/DreamService$1;)V

    #@4a
    return-object v0
.end method

.method public onContentChanged()V
    .registers 1

    #@0
    .prologue
    .line 295
    return-void
.end method

.method public onCreate()V
    .registers 5

    #@0
    .prologue
    .line 534
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_25

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string/jumbo v2, "onCreate() on thread "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    #@19
    move-result-wide v2

    #@1a
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 535
    :cond_25
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@28
    .line 536
    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .registers 4
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 266
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .registers 3
    .parameter "featureId"

    #@0
    .prologue
    .line 260
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 582
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "onDestroy()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 586
    :cond_c
    invoke-direct {p0}, Landroid/service/dreams/DreamService;->detach()V

    #@f
    .line 588
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@12
    .line 589
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 1

    #@0
    .prologue
    .line 310
    return-void
.end method

.method public onDreamingStarted()V
    .registers 3

    #@0
    .prologue
    .line 542
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "onDreamingStarted()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 545
    :cond_c
    const/4 v0, 0x1

    #@d
    invoke-direct {p0, v0}, Landroid/service/dreams/DreamService;->hideNavigationBar(Z)V

    #@10
    .line 547
    return-void
.end method

.method public onDreamingStopped()V
    .registers 3

    #@0
    .prologue
    .line 554
    iget-boolean v0, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/service/dreams/DreamService;->TAG:Ljava/lang/String;

    #@6
    const-string/jumbo v1, "onDreamingStopped()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 556
    :cond_c
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .registers 4
    .parameter "featureId"
    .parameter "item"

    #@0
    .prologue
    .line 284
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 4
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 278
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 3
    .parameter "featureId"
    .parameter "menu"

    #@0
    .prologue
    .line 315
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 5
    .parameter "featureId"
    .parameter "view"
    .parameter "menu"

    #@0
    .prologue
    .line 272
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onSearchRequested()Z
    .registers 2

    #@0
    .prologue
    .line 320
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .registers 2
    .parameter "attrs"

    #@0
    .prologue
    .line 290
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter "hasFocus"

    #@0
    .prologue
    .line 300
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 326
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public setContentView(I)V
    .registers 3
    .parameter "layoutResID"

    #@0
    .prologue
    .line 373
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(I)V

    #@7
    .line 374
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    #@0
    .prologue
    .line 389
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    #@7
    .line 390
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .parameter "view"
    .parameter "params"

    #@0
    .prologue
    .line 409
    invoke-virtual {p0}, Landroid/service/dreams/DreamService;->getWindow()Landroid/view/Window;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@7
    .line 410
    return-void
.end method

.method public setDebug(Z)V
    .registers 2
    .parameter "dbg"

    #@0
    .prologue
    .line 173
    iput-boolean p1, p0, Landroid/service/dreams/DreamService;->mDebug:Z

    #@2
    .line 174
    return-void
.end method

.method public setFullscreen(Z)V
    .registers 4
    .parameter "fullscreen"

    #@0
    .prologue
    .line 494
    iput-boolean p1, p0, Landroid/service/dreams/DreamService;->mFullscreen:Z

    #@2
    .line 495
    const/16 v0, 0x400

    #@4
    .line 496
    .local v0, flag:I
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mFullscreen:Z

    #@6
    if-eqz v1, :cond_d

    #@8
    move v1, v0

    #@9
    :goto_9
    invoke-direct {p0, v1, v0}, Landroid/service/dreams/DreamService;->applyWindowFlags(II)V

    #@c
    .line 497
    return-void

    #@d
    .line 496
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_9
.end method

.method public setInteractive(Z)V
    .registers 2
    .parameter "interactive"

    #@0
    .prologue
    .line 446
    iput-boolean p1, p0, Landroid/service/dreams/DreamService;->mInteractive:Z

    #@2
    .line 447
    return-void
.end method

.method public setLowProfile(Z)V
    .registers 2
    .parameter "lowProfile"

    #@0
    .prologue
    .line 474
    return-void
.end method

.method public setScreenBright(Z)V
    .registers 4
    .parameter "screenBright"

    #@0
    .prologue
    .line 514
    iput-boolean p1, p0, Landroid/service/dreams/DreamService;->mScreenBright:Z

    #@2
    .line 515
    const/16 v0, 0x80

    #@4
    .line 516
    .local v0, flag:I
    iget-boolean v1, p0, Landroid/service/dreams/DreamService;->mScreenBright:Z

    #@6
    if-eqz v1, :cond_d

    #@8
    move v1, v0

    #@9
    :goto_9
    invoke-direct {p0, v1, v0}, Landroid/service/dreams/DreamService;->applyWindowFlags(II)V

    #@c
    .line 517
    return-void

    #@d
    .line 516
    :cond_d
    const/4 v1, 0x0

    #@e
    goto :goto_9
.end method
