.class Landroid/accessibilityservice/UiTestAutomationBridge$1;
.super Ljava/lang/Object;
.source "UiTestAutomationBridge.java"

# interfaces
.implements Landroid/accessibilityservice/AccessibilityService$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/accessibilityservice/UiTestAutomationBridge;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/accessibilityservice/UiTestAutomationBridge;


# direct methods
.method constructor <init>(Landroid/accessibilityservice/UiTestAutomationBridge;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 137
    iput-object p1, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 150
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 152
    :goto_7
    :try_start_7
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@9
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    #@c
    move-result-object v2

    #@d
    invoke-static {v0, v2}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$102(Landroid/accessibilityservice/UiTestAutomationBridge;Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;

    #@10
    .line 153
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@12
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$200(Landroid/accessibilityservice/UiTestAutomationBridge;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_28

    #@18
    .line 154
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@1a
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@21
    .line 168
    :goto_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_7 .. :try_end_22} :catchall_40

    #@22
    .line 169
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@24
    invoke-virtual {v0, p1}, Landroid/accessibilityservice/UiTestAutomationBridge;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@27
    .line 170
    return-void

    #@28
    .line 157
    :cond_28
    :try_start_28
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2a
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$300(Landroid/accessibilityservice/UiTestAutomationBridge;)Z

    #@2d
    move-result v0

    #@2e
    if-nez v0, :cond_43

    #@30
    .line 158
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@32
    const/4 v2, 0x1

    #@33
    invoke-static {v0, v2}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$302(Landroid/accessibilityservice/UiTestAutomationBridge;Z)Z

    #@36
    .line 159
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@38
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@3f
    goto :goto_21

    #@40
    .line 168
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_28 .. :try_end_42} :catchall_40

    #@42
    throw v0

    #@43
    .line 163
    :cond_43
    :try_start_43
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@45
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_4c
    .catchall {:try_start_43 .. :try_end_4c} :catchall_40
    .catch Ljava/lang/InterruptedException; {:try_start_43 .. :try_end_4c} :catch_4d

    #@4c
    goto :goto_7

    #@4d
    .line 164
    :catch_4d
    move-exception v0

    #@4e
    goto :goto_7
.end method

.method public onGesture(I)Z
    .registers 3
    .parameter "gestureId"

    #@0
    .prologue
    .line 182
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public onInterrupt()V
    .registers 2

    #@0
    .prologue
    .line 145
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2
    invoke-virtual {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->onInterrupt()V

    #@5
    .line 146
    return-void
.end method

.method public onServiceConnected()V
    .registers 1

    #@0
    .prologue
    .line 141
    return-void
.end method

.method public onSetConnectionId(I)V
    .registers 4
    .parameter "connectionId"

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 175
    :try_start_7
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@9
    invoke-static {v0, p1}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$402(Landroid/accessibilityservice/UiTestAutomationBridge;I)I

    #@c
    .line 176
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge$1;->this$0:Landroid/accessibilityservice/UiTestAutomationBridge;

    #@e
    invoke-static {v0}, Landroid/accessibilityservice/UiTestAutomationBridge;->access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    #@15
    .line 177
    monitor-exit v1

    #@16
    .line 178
    return-void

    #@17
    .line 177
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_7 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method
