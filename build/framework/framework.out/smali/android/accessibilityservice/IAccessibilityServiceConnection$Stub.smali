.class public abstract Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;
.super Landroid/os/Binder;
.source "IAccessibilityServiceConnection.java"

# interfaces
.implements Landroid/accessibilityservice/IAccessibilityServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accessibilityservice/IAccessibilityServiceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.accessibilityservice.IAccessibilityServiceConnection"

.field static final TRANSACTION_findAccessibilityNodeInfoByAccessibilityId:I = 0x2

.field static final TRANSACTION_findAccessibilityNodeInfoByViewId:I = 0x4

.field static final TRANSACTION_findAccessibilityNodeInfosByText:I = 0x3

.field static final TRANSACTION_findFocus:I = 0x5

.field static final TRANSACTION_focusSearch:I = 0x6

.field static final TRANSACTION_getServiceInfo:I = 0x8

.field static final TRANSACTION_performAccessibilityAction:I = 0x7

.field static final TRANSACTION_performGlobalAction:I = 0x9

.field static final TRANSACTION_setServiceInfo:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@5
    invoke-virtual {p0, p0, v0}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 27
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 43
    sparse-switch p1, :sswitch_data_1de

    #@3
    .line 216
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@6
    move-result v2

    #@7
    :goto_7
    return v2

    #@8
    .line 47
    :sswitch_8
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    move-object/from16 v0, p3

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 48
    const/4 v2, 0x1

    #@10
    goto :goto_7

    #@11
    .line 52
    :sswitch_11
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18
    .line 54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_32

    #@1e
    .line 55
    sget-object v2, Landroid/accessibilityservice/AccessibilityServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    move-object/from16 v0, p2

    #@22
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@28
    .line 60
    .local v3, _arg0:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :goto_28
    move-object/from16 v0, p0

    #@2a
    invoke-virtual {v0, v3}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V

    #@2d
    .line 61
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@30
    .line 62
    const/4 v2, 0x1

    #@31
    goto :goto_7

    #@32
    .line 58
    .end local v3           #_arg0:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_32
    const/4 v3, 0x0

    #@33
    .restart local v3       #_arg0:Landroid/accessibilityservice/AccessibilityServiceInfo;
    goto :goto_28

    #@34
    .line 66
    .end local v3           #_arg0:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :sswitch_34
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@36
    move-object/from16 v0, p2

    #@38
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@3b
    .line 68
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v3

    #@3f
    .line 70
    .local v3, _arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@42
    move-result-wide v4

    #@43
    .line 72
    .local v4, _arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v6

    #@47
    .line 74
    .local v6, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@4e
    move-result-object v7

    #@4f
    .line 76
    .local v7, _arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@52
    move-result v8

    #@53
    .line 78
    .local v8, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@56
    move-result-wide v9

    #@57
    .local v9, _arg5:J
    move-object/from16 v2, p0

    #@59
    .line 79
    invoke-virtual/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->findAccessibilityNodeInfoByAccessibilityId(IJILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IJ)F

    #@5c
    move-result v21

    #@5d
    .line 80
    .local v21, _result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@60
    .line 81
    move-object/from16 v0, p3

    #@62
    move/from16 v1, v21

    #@64
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@67
    .line 82
    const/4 v2, 0x1

    #@68
    goto :goto_7

    #@69
    .line 86
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v8           #_arg4:I
    .end local v9           #_arg5:J
    .end local v21           #_result:F
    :sswitch_69
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@6b
    move-object/from16 v0, p2

    #@6d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 88
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v3

    #@74
    .line 90
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@77
    move-result-wide v4

    #@78
    .line 92
    .restart local v4       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7b
    move-result-object v6

    #@7c
    .line 94
    .local v6, _arg2:Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v7

    #@80
    .line 96
    .local v7, _arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@83
    move-result-object v2

    #@84
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@87
    move-result-object v8

    #@88
    .line 98
    .local v8, _arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@8b
    move-result-wide v9

    #@8c
    .restart local v9       #_arg5:J
    move-object/from16 v2, p0

    #@8e
    .line 99
    invoke-virtual/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->findAccessibilityNodeInfosByText(IJLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@91
    move-result v21

    #@92
    .line 100
    .restart local v21       #_result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@95
    .line 101
    move-object/from16 v0, p3

    #@97
    move/from16 v1, v21

    #@99
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@9c
    .line 102
    const/4 v2, 0x1

    #@9d
    goto/16 :goto_7

    #@9f
    .line 106
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:Ljava/lang/String;
    .end local v7           #_arg3:I
    .end local v8           #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v9           #_arg5:J
    .end local v21           #_result:F
    :sswitch_9f
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a1
    move-object/from16 v0, p2

    #@a3
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a6
    .line 108
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@a9
    move-result v3

    #@aa
    .line 110
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@ad
    move-result-wide v4

    #@ae
    .line 112
    .restart local v4       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b1
    move-result v6

    #@b2
    .line 114
    .local v6, _arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@b5
    move-result v7

    #@b6
    .line 116
    .restart local v7       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@b9
    move-result-object v2

    #@ba
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@bd
    move-result-object v8

    #@be
    .line 118
    .restart local v8       #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@c1
    move-result-wide v9

    #@c2
    .restart local v9       #_arg5:J
    move-object/from16 v2, p0

    #@c4
    .line 119
    invoke-virtual/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->findAccessibilityNodeInfoByViewId(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@c7
    move-result v21

    #@c8
    .line 120
    .restart local v21       #_result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@cb
    .line 121
    move-object/from16 v0, p3

    #@cd
    move/from16 v1, v21

    #@cf
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@d2
    .line 122
    const/4 v2, 0x1

    #@d3
    goto/16 :goto_7

    #@d5
    .line 126
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v8           #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v9           #_arg5:J
    .end local v21           #_result:F
    :sswitch_d5
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@d7
    move-object/from16 v0, p2

    #@d9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@dc
    .line 128
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@df
    move-result v3

    #@e0
    .line 130
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@e3
    move-result-wide v4

    #@e4
    .line 132
    .restart local v4       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@e7
    move-result v6

    #@e8
    .line 134
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@eb
    move-result v7

    #@ec
    .line 136
    .restart local v7       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@ef
    move-result-object v2

    #@f0
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@f3
    move-result-object v8

    #@f4
    .line 138
    .restart local v8       #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@f7
    move-result-wide v9

    #@f8
    .restart local v9       #_arg5:J
    move-object/from16 v2, p0

    #@fa
    .line 139
    invoke-virtual/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->findFocus(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@fd
    move-result v21

    #@fe
    .line 140
    .restart local v21       #_result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@101
    .line 141
    move-object/from16 v0, p3

    #@103
    move/from16 v1, v21

    #@105
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@108
    .line 142
    const/4 v2, 0x1

    #@109
    goto/16 :goto_7

    #@10b
    .line 146
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v8           #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v9           #_arg5:J
    .end local v21           #_result:F
    :sswitch_10b
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@10d
    move-object/from16 v0, p2

    #@10f
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@112
    .line 148
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@115
    move-result v3

    #@116
    .line 150
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@119
    move-result-wide v4

    #@11a
    .line 152
    .restart local v4       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@11d
    move-result v6

    #@11e
    .line 154
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@121
    move-result v7

    #@122
    .line 156
    .restart local v7       #_arg3:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@125
    move-result-object v2

    #@126
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@129
    move-result-object v8

    #@12a
    .line 158
    .restart local v8       #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@12d
    move-result-wide v9

    #@12e
    .restart local v9       #_arg5:J
    move-object/from16 v2, p0

    #@130
    .line 159
    invoke-virtual/range {v2 .. v10}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->focusSearch(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F

    #@133
    move-result v21

    #@134
    .line 160
    .restart local v21       #_result:F
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@137
    .line 161
    move-object/from16 v0, p3

    #@139
    move/from16 v1, v21

    #@13b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeFloat(F)V

    #@13e
    .line 162
    const/4 v2, 0x1

    #@13f
    goto/16 :goto_7

    #@141
    .line 166
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:I
    .end local v7           #_arg3:I
    .end local v8           #_arg4:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v9           #_arg5:J
    .end local v21           #_result:F
    :sswitch_141
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@143
    move-object/from16 v0, p2

    #@145
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@148
    .line 168
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@14b
    move-result v3

    #@14c
    .line 170
    .restart local v3       #_arg0:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@14f
    move-result-wide v4

    #@150
    .line 172
    .restart local v4       #_arg1:J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@153
    move-result v6

    #@154
    .line 174
    .restart local v6       #_arg2:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@157
    move-result v2

    #@158
    if-eqz v2, :cond_191

    #@15a
    .line 175
    sget-object v2, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    #@15c
    move-object/from16 v0, p2

    #@15e
    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@161
    move-result-object v7

    #@162
    check-cast v7, Landroid/os/Bundle;

    #@164
    .line 181
    .local v7, _arg3:Landroid/os/Bundle;
    :goto_164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@167
    move-result v8

    #@168
    .line 183
    .local v8, _arg4:I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@16b
    move-result-object v2

    #@16c
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;

    #@16f
    move-result-object v9

    #@170
    .line 185
    .local v9, _arg5:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    #@173
    move-result-wide v19

    #@174
    .local v19, _arg6:J
    move-object/from16 v11, p0

    #@176
    move v12, v3

    #@177
    move-wide v13, v4

    #@178
    move v15, v6

    #@179
    move-object/from16 v16, v7

    #@17b
    move/from16 v17, v8

    #@17d
    move-object/from16 v18, v9

    #@17f
    .line 186
    invoke-virtual/range {v11 .. v20}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->performAccessibilityAction(IJILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)Z

    #@182
    move-result v21

    #@183
    .line 187
    .local v21, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@186
    .line 188
    if-eqz v21, :cond_193

    #@188
    const/4 v2, 0x1

    #@189
    :goto_189
    move-object/from16 v0, p3

    #@18b
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18e
    .line 189
    const/4 v2, 0x1

    #@18f
    goto/16 :goto_7

    #@191
    .line 178
    .end local v7           #_arg3:Landroid/os/Bundle;
    .end local v8           #_arg4:I
    .end local v9           #_arg5:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v19           #_arg6:J
    .end local v21           #_result:Z
    :cond_191
    const/4 v7, 0x0

    #@192
    .restart local v7       #_arg3:Landroid/os/Bundle;
    goto :goto_164

    #@193
    .line 188
    .restart local v8       #_arg4:I
    .restart local v9       #_arg5:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .restart local v19       #_arg6:J
    .restart local v21       #_result:Z
    :cond_193
    const/4 v2, 0x0

    #@194
    goto :goto_189

    #@195
    .line 193
    .end local v3           #_arg0:I
    .end local v4           #_arg1:J
    .end local v6           #_arg2:I
    .end local v7           #_arg3:Landroid/os/Bundle;
    .end local v8           #_arg4:I
    .end local v9           #_arg5:Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;
    .end local v19           #_arg6:J
    .end local v21           #_result:Z
    :sswitch_195
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@197
    move-object/from16 v0, p2

    #@199
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@19c
    .line 194
    invoke-virtual/range {p0 .. p0}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@19f
    move-result-object v21

    #@1a0
    .line 195
    .local v21, _result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a3
    .line 196
    if-eqz v21, :cond_1b6

    #@1a5
    .line 197
    const/4 v2, 0x1

    #@1a6
    move-object/from16 v0, p3

    #@1a8
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1ab
    .line 198
    const/4 v2, 0x1

    #@1ac
    move-object/from16 v0, v21

    #@1ae
    move-object/from16 v1, p3

    #@1b0
    invoke-virtual {v0, v1, v2}, Landroid/accessibilityservice/AccessibilityServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1b3
    .line 203
    :goto_1b3
    const/4 v2, 0x1

    #@1b4
    goto/16 :goto_7

    #@1b6
    .line 201
    :cond_1b6
    const/4 v2, 0x0

    #@1b7
    move-object/from16 v0, p3

    #@1b9
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1bc
    goto :goto_1b3

    #@1bd
    .line 207
    .end local v21           #_result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :sswitch_1bd
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@1bf
    move-object/from16 v0, p2

    #@1c1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c4
    .line 209
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    #@1c7
    move-result v3

    #@1c8
    .line 210
    .restart local v3       #_arg0:I
    move-object/from16 v0, p0

    #@1ca
    invoke-virtual {v0, v3}, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;->performGlobalAction(I)Z

    #@1cd
    move-result v21

    #@1ce
    .line 211
    .local v21, _result:Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    #@1d1
    .line 212
    if-eqz v21, :cond_1dc

    #@1d3
    const/4 v2, 0x1

    #@1d4
    :goto_1d4
    move-object/from16 v0, p3

    #@1d6
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d9
    .line 213
    const/4 v2, 0x1

    #@1da
    goto/16 :goto_7

    #@1dc
    .line 212
    :cond_1dc
    const/4 v2, 0x0

    #@1dd
    goto :goto_1d4

    #@1de
    .line 43
    :sswitch_data_1de
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_34
        0x3 -> :sswitch_69
        0x4 -> :sswitch_9f
        0x5 -> :sswitch_d5
        0x6 -> :sswitch_10b
        0x7 -> :sswitch_141
        0x8 -> :sswitch_195
        0x9 -> :sswitch_1bd
        0x5f4e5446 -> :sswitch_8
    .end sparse-switch
.end method
