.class Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;
.super Ljava/lang/Object;
.source "IAccessibilityServiceConnection.java"

# interfaces
.implements Landroid/accessibilityservice/IAccessibilityServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 222
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 223
    iput-object p1, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 224
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 227
    iget-object v0, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public findAccessibilityNodeInfoByAccessibilityId(IJILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;IJ)F
    .registers 15
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "flags"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 272
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 273
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 276
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 277
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 278
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 279
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 280
    if-eqz p5, :cond_3a

    #@18
    invoke-interface {p5}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v3

    #@1c
    :goto_1c
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@1f
    .line 281
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@22
    .line 282
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 283
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v4, 0x2

    #@28
    const/4 v5, 0x0

    #@29
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 284
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 285
    invoke-virtual {v1}, Landroid/os/Parcel;->readFloat()F
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3c

    #@32
    move-result v2

    #@33
    .line 288
    .local v2, _result:F
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 289
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 291
    return v2

    #@3a
    .line 280
    .end local v2           #_result:F
    :cond_3a
    const/4 v3, 0x0

    #@3b
    goto :goto_1c

    #@3c
    .line 288
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 289
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public findAccessibilityNodeInfoByViewId(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 15
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "viewId"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 354
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 355
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 358
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 359
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 360
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 361
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 362
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 363
    if-eqz p6, :cond_3a

    #@1b
    invoke-interface {p6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v3

    #@1f
    :goto_1f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@22
    .line 364
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 365
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v4, 0x4

    #@28
    const/4 v5, 0x0

    #@29
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 366
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 367
    invoke-virtual {v1}, Landroid/os/Parcel;->readFloat()F
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3c

    #@32
    move-result v2

    #@33
    .line 370
    .local v2, _result:F
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 371
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 373
    return v2

    #@3a
    .line 363
    .end local v2           #_result:F
    :cond_3a
    const/4 v3, 0x0

    #@3b
    goto :goto_1f

    #@3c
    .line 370
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 371
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public findAccessibilityNodeInfosByText(IJLjava/lang/String;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 15
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "text"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 313
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 314
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 317
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 318
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 319
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 320
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 321
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 322
    if-eqz p6, :cond_3a

    #@1b
    invoke-interface {p6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v3

    #@1f
    :goto_1f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@22
    .line 323
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 324
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v4, 0x3

    #@28
    const/4 v5, 0x0

    #@29
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 325
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 326
    invoke-virtual {v1}, Landroid/os/Parcel;->readFloat()F
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3c

    #@32
    move-result v2

    #@33
    .line 329
    .local v2, _result:F
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 330
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 332
    return v2

    #@3a
    .line 322
    .end local v2           #_result:F
    :cond_3a
    const/4 v3, 0x0

    #@3b
    goto :goto_1f

    #@3c
    .line 329
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 330
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public findFocus(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 15
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "focusType"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 395
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 396
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 399
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 400
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 401
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 402
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 403
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 404
    if-eqz p6, :cond_3a

    #@1b
    invoke-interface {p6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v3

    #@1f
    :goto_1f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@22
    .line 405
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 406
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v4, 0x5

    #@28
    const/4 v5, 0x0

    #@29
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 407
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 408
    invoke-virtual {v1}, Landroid/os/Parcel;->readFloat()F
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3c

    #@32
    move-result v2

    #@33
    .line 411
    .local v2, _result:F
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 412
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 414
    return v2

    #@3a
    .line 404
    .end local v2           #_result:F
    :cond_3a
    const/4 v3, 0x0

    #@3b
    goto :goto_1f

    #@3c
    .line 411
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 412
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public focusSearch(IJIILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)F
    .registers 15
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "direction"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 436
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 437
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 440
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 441
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 442
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 443
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 444
    invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 445
    if-eqz p6, :cond_3a

    #@1b
    invoke-interface {p6}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@1e
    move-result-object v3

    #@1f
    :goto_1f
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@22
    .line 446
    invoke-virtual {v0, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    #@25
    .line 447
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@27
    const/4 v4, 0x6

    #@28
    const/4 v5, 0x0

    #@29
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 448
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@2f
    .line 449
    invoke-virtual {v1}, Landroid/os/Parcel;->readFloat()F
    :try_end_32
    .catchall {:try_start_8 .. :try_end_32} :catchall_3c

    #@32
    move-result v2

    #@33
    .line 452
    .local v2, _result:F
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@36
    .line 453
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 455
    return v2

    #@3a
    .line 445
    .end local v2           #_result:F
    :cond_3a
    const/4 v3, 0x0

    #@3b
    goto :goto_1f

    #@3c
    .line 452
    :catchall_3c
    move-exception v3

    #@3d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@40
    .line 453
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@43
    throw v3
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 231
    const-string v0, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@2
    return-object v0
.end method

.method public getServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 510
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 511
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 514
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 515
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 516
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 517
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v3

    #@1c
    if-eqz v3, :cond_2d

    #@1e
    .line 518
    sget-object v3, Landroid/accessibilityservice/AccessibilityServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@20
    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@23
    move-result-object v2

    #@24
    check-cast v2, Landroid/accessibilityservice/AccessibilityServiceInfo;
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2f

    #@26
    .line 525
    .local v2, _result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :goto_26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 526
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 528
    return-object v2

    #@2d
    .line 521
    .end local v2           #_result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_2d
    const/4 v2, 0x0

    #@2e
    .restart local v2       #_result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    goto :goto_26

    #@2f
    .line 525
    .end local v2           #_result:Landroid/accessibilityservice/AccessibilityServiceInfo;
    :catchall_2f
    move-exception v3

    #@30
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 526
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@36
    throw v3
.end method

.method public performAccessibilityAction(IJILandroid/os/Bundle;ILandroid/view/accessibility/IAccessibilityInteractionConnectionCallback;J)Z
    .registers 16
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"
    .parameter "interactionId"
    .parameter "callback"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 477
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 478
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 481
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 482
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 483
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    #@13
    .line 484
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 485
    if-eqz p5, :cond_47

    #@18
    .line 486
    const/4 v3, 0x1

    #@19
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 487
    const/4 v3, 0x0

    #@1d
    invoke-virtual {p5, v0, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 492
    :goto_20
    invoke-virtual {v0, p6}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 493
    if-eqz p7, :cond_54

    #@25
    invoke-interface {p7}, Landroid/view/accessibility/IAccessibilityInteractionConnectionCallback;->asBinder()Landroid/os/IBinder;

    #@28
    move-result-object v3

    #@29
    :goto_29
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2c
    .line 494
    invoke-virtual {v0, p8, p9}, Landroid/os/Parcel;->writeLong(J)V

    #@2f
    .line 495
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@31
    const/4 v4, 0x7

    #@32
    const/4 v5, 0x0

    #@33
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@36
    .line 496
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@39
    .line 497
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_3c
    .catchall {:try_start_8 .. :try_end_3c} :catchall_4c

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_56

    #@3f
    const/4 v2, 0x1

    #@40
    .line 500
    .local v2, _result:Z
    :goto_40
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@43
    .line 501
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@46
    .line 503
    return v2

    #@47
    .line 490
    .end local v2           #_result:Z
    :cond_47
    const/4 v3, 0x0

    #@48
    :try_start_48
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_4b
    .catchall {:try_start_48 .. :try_end_4b} :catchall_4c

    #@4b
    goto :goto_20

    #@4c
    .line 500
    :catchall_4c
    move-exception v3

    #@4d
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@50
    .line 501
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@53
    throw v3

    #@54
    .line 493
    :cond_54
    const/4 v3, 0x0

    #@55
    goto :goto_29

    #@56
    .line 497
    :cond_56
    const/4 v2, 0x0

    #@57
    goto :goto_40
.end method

.method public performGlobalAction(I)Z
    .registers 8
    .parameter "action"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 538
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@4
    move-result-object v0

    #@5
    .line 539
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v1

    #@9
    .line 542
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_9
    const-string v3, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@b
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@e
    .line 543
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 544
    iget-object v3, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@13
    const/16 v4, 0x9

    #@15
    const/4 v5, 0x0

    #@16
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 545
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 546
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_9 .. :try_end_1f} :catchall_2a

    #@1f
    move-result v3

    #@20
    if-eqz v3, :cond_23

    #@22
    const/4 v2, 0x1

    #@23
    .line 549
    .local v2, _result:Z
    :cond_23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 550
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 552
    return v2

    #@2a
    .line 549
    .end local v2           #_result:Z
    :catchall_2a
    move-exception v3

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 550
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v3
.end method

.method public setServiceInfo(Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    .registers 7
    .parameter "info"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 235
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 236
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 238
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "android.accessibilityservice.IAccessibilityServiceConnection"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 239
    if-eqz p1, :cond_28

    #@f
    .line 240
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 241
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Landroid/accessibilityservice/AccessibilityServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 246
    :goto_17
    iget-object v2, p0, Landroid/accessibilityservice/IAccessibilityServiceConnection$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x1

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 247
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 250
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 251
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 253
    return-void

    #@28
    .line 244
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 250
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 251
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method
