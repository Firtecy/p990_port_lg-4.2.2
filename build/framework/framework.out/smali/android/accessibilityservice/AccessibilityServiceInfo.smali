.class public Landroid/accessibilityservice/AccessibilityServiceInfo;
.super Ljava/lang/Object;
.source "AccessibilityServiceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT:I = 0x1

.field public static final FEEDBACK_ALL_MASK:I = -0x1

.field public static final FEEDBACK_AUDIBLE:I = 0x4

.field public static final FEEDBACK_BRAILLE:I = 0x20

.field public static final FEEDBACK_GENERIC:I = 0x10

.field public static final FEEDBACK_HAPTIC:I = 0x2

.field public static final FEEDBACK_SPOKEN:I = 0x1

.field public static final FEEDBACK_VISUAL:I = 0x8

.field public static final FLAG_INCLUDE_NOT_IMPORTANT_VIEWS:I = 0x2

.field public static final FLAG_REQUEST_TOUCH_EXPLORATION_MODE:I = 0x4

.field private static final TAG_ACCESSIBILITY_SERVICE:Ljava/lang/String; = "accessibility-service"


# instance fields
.field public eventTypes:I

.field public feedbackType:I

.field public flags:I

.field private mCanRetrieveWindowContent:Z

.field private mDescriptionResId:I

.field private mId:Ljava/lang/String;

.field private mNonLocalizedDescription:Ljava/lang/String;

.field private mResolveInfo:Landroid/content/pm/ResolveInfo;

.field private mSettingsActivityName:Ljava/lang/String;

.field public notificationTimeout:J

.field public packageNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 636
    new-instance v0, Landroid/accessibilityservice/AccessibilityServiceInfo$1;

    #@2
    invoke-direct {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo$1;-><init>()V

    #@5
    sput-object v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 259
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 261
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ResolveInfo;Landroid/content/Context;)V
    .registers 20
    .parameter "resolveInfo"
    .parameter "context"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 274
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 275
    move-object/from16 v0, p1

    #@5
    iget-object v12, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@7
    .line 276
    .local v12, serviceInfo:Landroid/content/pm/ServiceInfo;
    new-instance v14, Landroid/content/ComponentName;

    #@9
    iget-object v15, v12, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@b
    iget-object v0, v12, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    #@d
    move-object/from16 v16, v0

    #@f
    invoke-direct/range {v14 .. v16}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    invoke-virtual {v14}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    #@15
    move-result-object v14

    #@16
    move-object/from16 v0, p0

    #@18
    iput-object v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mId:Ljava/lang/String;

    #@1a
    .line 277
    move-object/from16 v0, p1

    #@1c
    move-object/from16 v1, p0

    #@1e
    iput-object v0, v1, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@20
    .line 279
    const/4 v9, 0x0

    #@21
    .line 282
    .local v9, parser:Landroid/content/res/XmlResourceParser;
    :try_start_21
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@24
    move-result-object v7

    #@25
    .line 283
    .local v7, packageManager:Landroid/content/pm/PackageManager;
    const-string v14, "android.accessibilityservice"

    #@27
    invoke-virtual {v12, v7, v14}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_2a
    .catchall {:try_start_21 .. :try_end_2a} :catchall_71
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_21 .. :try_end_2a} :catch_53

    #@2a
    move-result-object v9

    #@2b
    .line 285
    if-nez v9, :cond_33

    #@2d
    .line 340
    if-eqz v9, :cond_32

    #@2f
    .line 341
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@32
    .line 344
    :cond_32
    :goto_32
    return-void

    #@33
    .line 289
    :cond_33
    const/4 v13, 0x0

    #@34
    .line 290
    .local v13, type:I
    :goto_34
    const/4 v14, 0x1

    #@35
    if-eq v13, v14, :cond_3f

    #@37
    const/4 v14, 0x2

    #@38
    if-eq v13, v14, :cond_3f

    #@3a
    .line 291
    :try_start_3a
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    #@3d
    move-result v13

    #@3e
    goto :goto_34

    #@3f
    .line 294
    :cond_3f
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    .line 295
    .local v5, nodeName:Ljava/lang/String;
    const-string v14, "accessibility-service"

    #@45
    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v14

    #@49
    if-nez v14, :cond_78

    #@4b
    .line 296
    new-instance v14, Lorg/xmlpull/v1/XmlPullParserException;

    #@4d
    const-string v15, "Meta-data does not start withaccessibility-service tag"

    #@4f
    invoke-direct {v14, v15}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@52
    throw v14
    :try_end_53
    .catchall {:try_start_3a .. :try_end_53} :catchall_71
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3a .. :try_end_53} :catch_53

    #@53
    .line 336
    .end local v5           #nodeName:Ljava/lang/String;
    .end local v7           #packageManager:Landroid/content/pm/PackageManager;
    .end local v13           #type:I
    :catch_53
    move-exception v4

    #@54
    .line 337
    .local v4, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_54
    new-instance v14, Lorg/xmlpull/v1/XmlPullParserException;

    #@56
    new-instance v15, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v16, "Unable to create context for: "

    #@5d
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v15

    #@61
    iget-object v0, v12, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@63
    move-object/from16 v16, v0

    #@65
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v15

    #@69
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v15

    #@6d
    invoke-direct {v14, v15}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@70
    throw v14
    :try_end_71
    .catchall {:try_start_54 .. :try_end_71} :catchall_71

    #@71
    .line 340
    .end local v4           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_71
    move-exception v14

    #@72
    if-eqz v9, :cond_77

    #@74
    .line 341
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@77
    :cond_77
    throw v14

    #@78
    .line 300
    .restart local v5       #nodeName:Ljava/lang/String;
    .restart local v7       #packageManager:Landroid/content/pm/PackageManager;
    .restart local v13       #type:I
    :cond_78
    :try_start_78
    invoke-static {v9}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@7b
    move-result-object v2

    #@7c
    .line 301
    .local v2, allAttributes:Landroid/util/AttributeSet;
    iget-object v14, v12, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@7e
    invoke-virtual {v7, v14}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    #@81
    move-result-object v11

    #@82
    .line 303
    .local v11, resources:Landroid/content/res/Resources;
    sget-object v14, Lcom/android/internal/R$styleable;->AccessibilityService:[I

    #@84
    invoke-virtual {v11, v2, v14}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@87
    move-result-object v3

    #@88
    .line 305
    .local v3, asAttributes:Landroid/content/res/TypedArray;
    const/4 v14, 0x2

    #@89
    const/4 v15, 0x0

    #@8a
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@8d
    move-result v14

    #@8e
    move-object/from16 v0, p0

    #@90
    iput v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@92
    .line 308
    const/4 v14, 0x3

    #@93
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@96
    move-result-object v8

    #@97
    .line 310
    .local v8, packageNamez:Ljava/lang/String;
    if-eqz v8, :cond_a3

    #@99
    .line 311
    const-string v14, "(\\s)*,(\\s)*"

    #@9b
    invoke-virtual {v8, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@9e
    move-result-object v14

    #@9f
    move-object/from16 v0, p0

    #@a1
    iput-object v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@a3
    .line 313
    :cond_a3
    const/4 v14, 0x4

    #@a4
    const/4 v15, 0x0

    #@a5
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@a8
    move-result v14

    #@a9
    move-object/from16 v0, p0

    #@ab
    iput v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@ad
    .line 316
    const/4 v14, 0x5

    #@ae
    const/4 v15, 0x0

    #@af
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@b2
    move-result v14

    #@b3
    int-to-long v14, v14

    #@b4
    move-object/from16 v0, p0

    #@b6
    iput-wide v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@b8
    .line 319
    const/4 v14, 0x6

    #@b9
    const/4 v15, 0x0

    #@ba
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@bd
    move-result v14

    #@be
    move-object/from16 v0, p0

    #@c0
    iput v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@c2
    .line 321
    const/4 v14, 0x1

    #@c3
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@c6
    move-result-object v14

    #@c7
    move-object/from16 v0, p0

    #@c9
    iput-object v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mSettingsActivityName:Ljava/lang/String;

    #@cb
    .line 323
    const/4 v14, 0x7

    #@cc
    const/4 v15, 0x0

    #@cd
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@d0
    move-result v14

    #@d1
    move-object/from16 v0, p0

    #@d3
    iput-boolean v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mCanRetrieveWindowContent:Z

    #@d5
    .line 326
    const/4 v14, 0x0

    #@d6
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@d9
    move-result-object v10

    #@da
    .line 328
    .local v10, peekedValue:Landroid/util/TypedValue;
    if-eqz v10, :cond_f4

    #@dc
    .line 329
    iget v14, v10, Landroid/util/TypedValue;->resourceId:I

    #@de
    move-object/from16 v0, p0

    #@e0
    iput v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mDescriptionResId:I

    #@e2
    .line 330
    invoke-virtual {v10}, Landroid/util/TypedValue;->coerceToString()Ljava/lang/CharSequence;

    #@e5
    move-result-object v6

    #@e6
    .line 331
    .local v6, nonLocalizedDescription:Ljava/lang/CharSequence;
    if-eqz v6, :cond_f4

    #@e8
    .line 332
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@eb
    move-result-object v14

    #@ec
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@ef
    move-result-object v14

    #@f0
    move-object/from16 v0, p0

    #@f2
    iput-object v14, v0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mNonLocalizedDescription:Ljava/lang/String;

    #@f4
    .line 335
    .end local v6           #nonLocalizedDescription:Ljava/lang/CharSequence;
    :cond_f4
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_f7
    .catchall {:try_start_78 .. :try_end_f7} :catchall_71
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_78 .. :try_end_f7} :catch_53

    #@f7
    .line 340
    if-eqz v9, :cond_32

    #@f9
    .line 341
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->close()V

    #@fc
    goto/16 :goto_32
.end method

.method static synthetic access$000(Landroid/accessibilityservice/AccessibilityServiceInfo;Landroid/os/Parcel;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->initFromParcel(Landroid/os/Parcel;)V

    #@3
    return-void
.end method

.method private static appendEventTypes(Ljava/lang/StringBuilder;I)V
    .registers 5
    .parameter "stringBuilder"
    .parameter "eventTypes"

    #@0
    .prologue
    .line 530
    const-string v1, "eventTypes:"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 531
    const-string v1, "["

    #@7
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 532
    :cond_a
    :goto_a
    if-eqz p1, :cond_25

    #@c
    .line 533
    const/4 v1, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@10
    move-result v2

    #@11
    shl-int v0, v1, v2

    #@13
    .line 534
    .local v0, eventTypeBit:I
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->eventTypeToString(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 535
    xor-int/lit8 v1, v0, -0x1

    #@1c
    and-int/2addr p1, v1

    #@1d
    .line 536
    if-eqz p1, :cond_a

    #@1f
    .line 537
    const-string v1, ", "

    #@21
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    goto :goto_a

    #@25
    .line 540
    .end local v0           #eventTypeBit:I
    :cond_25
    const-string v1, "]"

    #@27
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 541
    return-void
.end method

.method private static appendFeedbackTypes(Ljava/lang/StringBuilder;I)V
    .registers 5
    .parameter "stringBuilder"
    .parameter "feedbackTypes"

    #@0
    .prologue
    .line 501
    const-string v1, "feedbackTypes:"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 502
    const-string v1, "["

    #@7
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 503
    :cond_a
    :goto_a
    if-eqz p1, :cond_25

    #@c
    .line 504
    const/4 v1, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@10
    move-result v2

    #@11
    shl-int v0, v1, v2

    #@13
    .line 505
    .local v0, feedbackTypeBit:I
    invoke-static {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackTypeToString(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 506
    xor-int/lit8 v1, v0, -0x1

    #@1c
    and-int/2addr p1, v1

    #@1d
    .line 507
    if-eqz p1, :cond_a

    #@1f
    .line 508
    const-string v1, ", "

    #@21
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    goto :goto_a

    #@25
    .line 511
    .end local v0           #feedbackTypeBit:I
    :cond_25
    const-string v1, "]"

    #@27
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 512
    return-void
.end method

.method private static appendFlags(Ljava/lang/StringBuilder;I)V
    .registers 5
    .parameter "stringBuilder"
    .parameter "flags"

    #@0
    .prologue
    .line 544
    const-string v1, "flags:"

    #@2
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 545
    const-string v1, "["

    #@7
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 546
    :cond_a
    :goto_a
    if-eqz p1, :cond_25

    #@c
    .line 547
    const/4 v1, 0x1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@10
    move-result v2

    #@11
    shl-int v0, v1, v2

    #@13
    .line 548
    .local v0, flagBit:I
    invoke-static {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->flagToString(I)Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 549
    xor-int/lit8 v1, v0, -0x1

    #@1c
    and-int/2addr p1, v1

    #@1d
    .line 550
    if-eqz p1, :cond_a

    #@1f
    .line 551
    const-string v1, ", "

    #@21
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    goto :goto_a

    #@25
    .line 554
    .end local v0           #flagBit:I
    :cond_25
    const-string v1, "]"

    #@27
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    .line 555
    return-void
.end method

.method private static appendPackageNames(Ljava/lang/StringBuilder;[Ljava/lang/String;)V
    .registers 5
    .parameter "stringBuilder"
    .parameter "packageNames"

    #@0
    .prologue
    .line 515
    const-string/jumbo v2, "packageNames:"

    #@3
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6
    .line 516
    const-string v2, "["

    #@8
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 517
    if-eqz p1, :cond_22

    #@d
    .line 518
    array-length v1, p1

    #@e
    .line 519
    .local v1, packageNameCount:I
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    :goto_f
    if-ge v0, v1, :cond_22

    #@11
    .line 520
    aget-object v2, p1, v0

    #@13
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 521
    add-int/lit8 v2, v1, -0x1

    #@18
    if-ge v0, v2, :cond_1f

    #@1a
    .line 522
    const-string v2, ", "

    #@1c
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    .line 519
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    #@21
    goto :goto_f

    #@22
    .line 526
    .end local v0           #i:I
    .end local v1           #packageNameCount:I
    :cond_22
    const-string v2, "]"

    #@24
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    .line 527
    return-void
.end method

.method public static feedbackTypeToString(I)Ljava/lang/String;
    .registers 5
    .parameter "feedbackType"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    .line 566
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "["

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 567
    :goto_b
    if-eqz p0, :cond_80

    #@d
    .line 568
    invoke-static {p0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    #@10
    move-result v2

    #@11
    shl-int v1, v3, v2

    #@13
    .line 569
    .local v1, feedbackTypeFlag:I
    xor-int/lit8 v2, v1, -0x1

    #@15
    and-int/2addr p0, v2

    #@16
    .line 570
    sparse-switch v1, :sswitch_data_8a

    #@19
    goto :goto_b

    #@1a
    .line 590
    :sswitch_1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@1d
    move-result v2

    #@1e
    if-le v2, v3, :cond_25

    #@20
    .line 591
    const-string v2, ", "

    #@22
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 593
    :cond_25
    const-string v2, "FEEDBACK_SPOKEN"

    #@27
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    goto :goto_b

    #@2b
    .line 572
    :sswitch_2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@2e
    move-result v2

    #@2f
    if-le v2, v3, :cond_36

    #@31
    .line 573
    const-string v2, ", "

    #@33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 575
    :cond_36
    const-string v2, "FEEDBACK_AUDIBLE"

    #@38
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    goto :goto_b

    #@3c
    .line 578
    :sswitch_3c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@3f
    move-result v2

    #@40
    if-le v2, v3, :cond_47

    #@42
    .line 579
    const-string v2, ", "

    #@44
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    .line 581
    :cond_47
    const-string v2, "FEEDBACK_HAPTIC"

    #@49
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    goto :goto_b

    #@4d
    .line 584
    :sswitch_4d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@50
    move-result v2

    #@51
    if-le v2, v3, :cond_58

    #@53
    .line 585
    const-string v2, ", "

    #@55
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 587
    :cond_58
    const-string v2, "FEEDBACK_GENERIC"

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    goto :goto_b

    #@5e
    .line 596
    :sswitch_5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@61
    move-result v2

    #@62
    if-le v2, v3, :cond_69

    #@64
    .line 597
    const-string v2, ", "

    #@66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 599
    :cond_69
    const-string v2, "FEEDBACK_VISUAL"

    #@6b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    goto :goto_b

    #@6f
    .line 602
    :sswitch_6f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@72
    move-result v2

    #@73
    if-le v2, v3, :cond_7a

    #@75
    .line 603
    const-string v2, ", "

    #@77
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    .line 605
    :cond_7a
    const-string v2, "FEEDBACK_BRAILLE"

    #@7c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    goto :goto_b

    #@80
    .line 609
    .end local v1           #feedbackTypeFlag:I
    :cond_80
    const-string v2, "]"

    #@82
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    .line 610
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v2

    #@89
    return-object v2

    #@8a
    .line 570
    :sswitch_data_8a
    .sparse-switch
        0x1 -> :sswitch_1a
        0x2 -> :sswitch_3c
        0x4 -> :sswitch_2b
        0x8 -> :sswitch_5e
        0x10 -> :sswitch_4d
        0x20 -> :sswitch_6f
    .end sparse-switch
.end method

.method public static flagToString(I)Ljava/lang/String;
    .registers 2
    .parameter "flag"

    #@0
    .prologue
    .line 621
    packed-switch p0, :pswitch_data_e

    #@3
    .line 629
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 623
    :pswitch_5
    const-string v0, "DEFAULT"

    #@7
    goto :goto_4

    #@8
    .line 625
    :pswitch_8
    const-string v0, "FLAG_INCLUDE_NOT_IMPORTANT_VIEWS"

    #@a
    goto :goto_4

    #@b
    .line 627
    :pswitch_b
    const-string v0, "FLAG_REQUEST_TOUCH_EXPLORATION_MODE"

    #@d
    goto :goto_4

    #@e
    .line 621
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_b
    .end packed-switch
.end method

.method private initFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "parcel"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 464
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v0

    #@5
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@7
    .line 465
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@d
    .line 466
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@13
    .line 467
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    #@16
    move-result-wide v2

    #@17
    iput-wide v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@19
    .line 468
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@1f
    .line 469
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mId:Ljava/lang/String;

    #@25
    .line 470
    const/4 v0, 0x0

    #@26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    #@29
    move-result-object v0

    #@2a
    check-cast v0, Landroid/content/pm/ResolveInfo;

    #@2c
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@2e
    .line 471
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mSettingsActivityName:Ljava/lang/String;

    #@34
    .line 472
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    if-ne v0, v1, :cond_4a

    #@3a
    move v0, v1

    #@3b
    :goto_3b
    iput-boolean v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mCanRetrieveWindowContent:Z

    #@3d
    .line 473
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@40
    move-result v0

    #@41
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mDescriptionResId:I

    #@43
    .line 474
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mNonLocalizedDescription:Ljava/lang/String;

    #@49
    .line 475
    return-void

    #@4a
    .line 472
    :cond_4a
    const/4 v0, 0x0

    #@4b
    goto :goto_3b
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 446
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getCanRetrieveWindowContent()Z
    .registers 2

    #@0
    .prologue
    .line 404
    iget-boolean v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mCanRetrieveWindowContent:Z

    #@2
    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 418
    iget-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mNonLocalizedDescription:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 369
    iget-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getResolveInfo()Landroid/content/pm/ResolveInfo;
    .registers 2

    #@0
    .prologue
    .line 380
    iget-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@2
    return-object v0
.end method

.method public getSettingsActivityName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 392
    iget-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mSettingsActivityName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .registers 7
    .parameter "packageManager"

    #@0
    .prologue
    .line 430
    iget v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mDescriptionResId:I

    #@2
    if-nez v2, :cond_7

    #@4
    .line 431
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mNonLocalizedDescription:Ljava/lang/String;

    #@6
    .line 439
    :goto_6
    return-object v2

    #@7
    .line 433
    :cond_7
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@9
    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    #@b
    .line 434
    .local v1, serviceInfo:Landroid/content/pm/ServiceInfo;
    iget-object v2, v1, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    #@d
    iget v3, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mDescriptionResId:I

    #@f
    iget-object v4, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    #@11
    invoke-virtual {p1, v2, v3, v4}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    #@14
    move-result-object v0

    #@15
    .line 436
    .local v0, description:Ljava/lang/CharSequence;
    if-eqz v0, :cond_20

    #@17
    .line 437
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    goto :goto_6

    #@20
    .line 439
    :cond_20
    const/4 v2, 0x0

    #@21
    goto :goto_6
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 480
    .local v0, stringBuilder:Ljava/lang/StringBuilder;
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@7
    invoke-static {v0, v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->appendEventTypes(Ljava/lang/StringBuilder;I)V

    #@a
    .line 481
    const-string v1, ", "

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 482
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@11
    invoke-static {v0, v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->appendPackageNames(Ljava/lang/StringBuilder;[Ljava/lang/String;)V

    #@14
    .line 483
    const-string v1, ", "

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 484
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@1b
    invoke-static {v0, v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->appendFeedbackTypes(Ljava/lang/StringBuilder;I)V

    #@1e
    .line 485
    const-string v1, ", "

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 486
    const-string/jumbo v1, "notificationTimeout: "

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    iget-wide v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@2c
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2f
    .line 487
    const-string v1, ", "

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 488
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@36
    invoke-static {v0, v1}, Landroid/accessibilityservice/AccessibilityServiceInfo;->appendFlags(Ljava/lang/StringBuilder;I)V

    #@39
    .line 489
    const-string v1, ", "

    #@3b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 490
    const-string v1, "id: "

    #@40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mId:Ljava/lang/String;

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 491
    const-string v1, ", "

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 492
    const-string/jumbo v1, "resolveInfo: "

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v1

    #@55
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@57
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    .line 493
    const-string v1, ", "

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 494
    const-string/jumbo v1, "settingsActivityName: "

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    iget-object v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mSettingsActivityName:Ljava/lang/String;

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    .line 495
    const-string v1, ", "

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    .line 496
    const-string/jumbo v1, "retrieveScreenContent: "

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    iget-boolean v2, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mCanRetrieveWindowContent:Z

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7c
    .line 497
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v1

    #@80
    return-object v1
.end method

.method public updateDynamicallyConfigurableProperties(Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 354
    iget v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@2
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@4
    .line 355
    iget-object v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@6
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@8
    .line 356
    iget v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@a
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@c
    .line 357
    iget-wide v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@e
    iput-wide v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@10
    .line 358
    iget v0, p1, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@12
    iput v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@14
    .line 359
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "parcel"
    .parameter "flagz"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 450
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 451
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->packageNames:[Ljava/lang/String;

    #@8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@b
    .line 452
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 453
    iget-wide v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->notificationTimeout:J

    #@12
    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    #@15
    .line 454
    iget v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@17
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1a
    .line 455
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mId:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 456
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    #@21
    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    #@24
    .line 457
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mSettingsActivityName:Ljava/lang/String;

    #@26
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@29
    .line 458
    iget-boolean v1, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mCanRetrieveWindowContent:Z

    #@2b
    if-eqz v1, :cond_2e

    #@2d
    const/4 v0, 0x1

    #@2e
    :cond_2e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 459
    iget v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mDescriptionResId:I

    #@33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 460
    iget-object v0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->mNonLocalizedDescription:Ljava/lang/String;

    #@38
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3b
    .line 461
    return-void
.end method
