.class public Landroid/accessibilityservice/UiTestAutomationBridge;
.super Ljava/lang/Object;
.source "UiTestAutomationBridge.java"


# static fields
.field public static final ACTIVE_WINDOW_ID:I = -0x1

.field private static final FIND_ACCESSIBILITY_NODE_INFO_PREFETCH_FLAGS:I = 0x7

.field private static final LOG_TAG:Ljava/lang/String; = null

#the value of this static final field might be set in the static constructor
.field public static final ROOT_NODE_ID:J = 0x0L

.field private static final TIMEOUT_REGISTER_SERVICE:I = 0x1388

.field public static final UNDEFINED:I = -0x1


# instance fields
.field private volatile mConnectionId:I

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

.field private mListener:Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;

.field private final mLock:Ljava/lang/Object;

.field private volatile mUnprocessedEventAvailable:Z

.field private volatile mWaitingForEventDelivery:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 51
    const-class v0, Landroid/accessibilityservice/UiTestAutomationBridge;

    #@2
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/accessibilityservice/UiTestAutomationBridge;->LOG_TAG:Ljava/lang/String;

    #@8
    .line 57
    sget-wide v0, Landroid/view/accessibility/AccessibilityNodeInfo;->ROOT_NODE_ID:J

    #@a
    sput-wide v0, Landroid/accessibilityservice/UiTestAutomationBridge;->ROOT_NODE_ID:J

    #@c
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 66
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@a
    .line 68
    const/4 v0, -0x1

    #@b
    iput v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@d
    return-void
.end method

.method static synthetic access$000(Landroid/accessibilityservice/UiTestAutomationBridge;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Landroid/accessibilityservice/UiTestAutomationBridge;Landroid/view/accessibility/AccessibilityEvent;)Landroid/view/accessibility/AccessibilityEvent;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Landroid/accessibilityservice/UiTestAutomationBridge;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mWaitingForEventDelivery:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/accessibilityservice/UiTestAutomationBridge;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 49
    iget-boolean v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/accessibilityservice/UiTestAutomationBridge;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput-boolean p1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Landroid/accessibilityservice/UiTestAutomationBridge;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 49
    iput p1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    return p1
.end method

.method private ensureValidConnection(I)V
    .registers 4
    .parameter "connectionId"

    #@0
    .prologue
    .line 491
    const/4 v0, -0x1

    #@1
    if-ne p1, v0, :cond_b

    #@3
    .line 492
    new-instance v0, Ljava/lang/IllegalStateException;

    #@5
    const-string v1, "UiAutomationService not connected. Did you call #register()?"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 495
    :cond_b
    return-void
.end method


# virtual methods
.method public connect()V
    .registers 15

    #@0
    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/accessibilityservice/UiTestAutomationBridge;->isConnected()Z

    #@3
    move-result v10

    #@4
    if-eqz v10, :cond_e

    #@6
    .line 120
    new-instance v10, Ljava/lang/IllegalStateException;

    #@8
    const-string v11, "Already connected."

    #@a
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v10

    #@e
    .line 132
    :cond_e
    new-instance v10, Landroid/os/HandlerThread;

    #@10
    const-string v11, "UiTestAutomationBridge"

    #@12
    invoke-direct {v10, v11}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@15
    iput-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mHandlerThread:Landroid/os/HandlerThread;

    #@17
    .line 133
    iget-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mHandlerThread:Landroid/os/HandlerThread;

    #@19
    const/4 v11, 0x1

    #@1a
    invoke-virtual {v10, v11}, Landroid/os/HandlerThread;->setDaemon(Z)V

    #@1d
    .line 134
    iget-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mHandlerThread:Landroid/os/HandlerThread;

    #@1f
    invoke-virtual {v10}, Landroid/os/HandlerThread;->start()V

    #@22
    .line 135
    iget-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mHandlerThread:Landroid/os/HandlerThread;

    #@24
    invoke-virtual {v10}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@27
    move-result-object v3

    #@28
    .line 137
    .local v3, looper:Landroid/os/Looper;
    new-instance v10, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;

    #@2a
    const/4 v11, 0x0

    #@2b
    new-instance v12, Landroid/accessibilityservice/UiTestAutomationBridge$1;

    #@2d
    invoke-direct {v12, p0}, Landroid/accessibilityservice/UiTestAutomationBridge$1;-><init>(Landroid/accessibilityservice/UiTestAutomationBridge;)V

    #@30
    invoke-direct {v10, v11, v3, v12}, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/accessibilityservice/AccessibilityService$Callbacks;)V

    #@33
    iput-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mListener:Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;

    #@35
    .line 186
    const-string v10, "accessibility"

    #@37
    invoke-static {v10}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@3a
    move-result-object v10

    #@3b
    invoke-static {v10}, Landroid/view/accessibility/IAccessibilityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;

    #@3e
    move-result-object v4

    #@3f
    .line 189
    .local v4, manager:Landroid/view/accessibility/IAccessibilityManager;
    new-instance v2, Landroid/accessibilityservice/AccessibilityServiceInfo;

    #@41
    invoke-direct {v2}, Landroid/accessibilityservice/AccessibilityServiceInfo;-><init>()V

    #@44
    .line 190
    .local v2, info:Landroid/accessibilityservice/AccessibilityServiceInfo;
    const/4 v10, -0x1

    #@45
    iput v10, v2, Landroid/accessibilityservice/AccessibilityServiceInfo;->eventTypes:I

    #@47
    .line 191
    const/16 v10, 0x10

    #@49
    iput v10, v2, Landroid/accessibilityservice/AccessibilityServiceInfo;->feedbackType:I

    #@4b
    .line 192
    iget v10, v2, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@4d
    or-int/lit8 v10, v10, 0x2

    #@4f
    iput v10, v2, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    #@51
    .line 195
    :try_start_51
    iget-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mListener:Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;

    #@53
    invoke-interface {v4, v10, v2}, Landroid/view/accessibility/IAccessibilityManager;->registerUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;Landroid/accessibilityservice/AccessibilityServiceInfo;)V
    :try_end_56
    .catch Landroid/os/RemoteException; {:try_start_51 .. :try_end_56} :catch_65

    #@56
    .line 200
    iget-object v11, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@58
    monitor-enter v11

    #@59
    .line 201
    :try_start_59
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@5c
    move-result-wide v8

    #@5d
    .line 203
    .local v8, startTimeMillis:J
    :goto_5d
    invoke-virtual {p0}, Landroid/accessibilityservice/UiTestAutomationBridge;->isConnected()Z

    #@60
    move-result v10

    #@61
    if-eqz v10, :cond_6e

    #@63
    .line 204
    monitor-exit v11
    :try_end_64
    .catchall {:try_start_59 .. :try_end_64} :catchall_86

    #@64
    return-void

    #@65
    .line 196
    .end local v8           #startTimeMillis:J
    :catch_65
    move-exception v5

    #@66
    .line 197
    .local v5, re:Landroid/os/RemoteException;
    new-instance v10, Ljava/lang/IllegalStateException;

    #@68
    const-string v11, "Cound not register UiAutomationService."

    #@6a
    invoke-direct {v10, v11, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@6d
    throw v10

    #@6e
    .line 206
    .end local v5           #re:Landroid/os/RemoteException;
    .restart local v8       #startTimeMillis:J
    :cond_6e
    :try_start_6e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@71
    move-result-wide v12

    #@72
    sub-long v0, v12, v8

    #@74
    .line 207
    .local v0, elapsedTimeMillis:J
    const-wide/16 v12, 0x1388

    #@76
    sub-long v6, v12, v0

    #@78
    .line 208
    .local v6, remainingTimeMillis:J
    const-wide/16 v12, 0x0

    #@7a
    cmp-long v10, v6, v12

    #@7c
    if-gtz v10, :cond_89

    #@7e
    .line 209
    new-instance v10, Ljava/lang/IllegalStateException;

    #@80
    const-string v12, "Cound not register UiAutomationService."

    #@82
    invoke-direct {v10, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@85
    throw v10

    #@86
    .line 217
    .end local v0           #elapsedTimeMillis:J
    .end local v6           #remainingTimeMillis:J
    .end local v8           #startTimeMillis:J
    :catchall_86
    move-exception v10

    #@87
    monitor-exit v11
    :try_end_88
    .catchall {:try_start_6e .. :try_end_88} :catchall_86

    #@88
    throw v10

    #@89
    .line 212
    .restart local v0       #elapsedTimeMillis:J
    .restart local v6       #remainingTimeMillis:J
    .restart local v8       #startTimeMillis:J
    :cond_89
    :try_start_89
    iget-object v10, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@8b
    invoke-virtual {v10, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_8e
    .catchall {:try_start_89 .. :try_end_8e} :catchall_86
    .catch Ljava/lang/InterruptedException; {:try_start_89 .. :try_end_8e} :catch_8f

    #@8e
    goto :goto_5d

    #@8f
    .line 213
    :catch_8f
    move-exception v10

    #@90
    goto :goto_5d
.end method

.method public disconnect()V
    .registers 5

    #@0
    .prologue
    .line 226
    invoke-virtual {p0}, Landroid/accessibilityservice/UiTestAutomationBridge;->isConnected()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_e

    #@6
    .line 227
    new-instance v2, Ljava/lang/IllegalStateException;

    #@8
    const-string v3, "Already disconnected."

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 230
    :cond_e
    iget-object v2, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mHandlerThread:Landroid/os/HandlerThread;

    #@10
    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    #@13
    .line 232
    const-string v2, "accessibility"

    #@15
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@18
    move-result-object v2

    #@19
    invoke-static {v2}, Landroid/view/accessibility/IAccessibilityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;

    #@1c
    move-result-object v0

    #@1d
    .line 236
    .local v0, manager:Landroid/view/accessibility/IAccessibilityManager;
    :try_start_1d
    iget-object v2, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mListener:Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;

    #@1f
    invoke-interface {v0, v2}, Landroid/view/accessibility/IAccessibilityManager;->unregisterUiTestAutomationService(Landroid/accessibilityservice/IAccessibilityServiceClient;)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_22} :catch_23

    #@22
    .line 240
    :goto_22
    return-void

    #@23
    .line 237
    :catch_23
    move-exception v1

    #@24
    .line 238
    .local v1, re:Landroid/os/RemoteException;
    sget-object v2, Landroid/accessibilityservice/UiTestAutomationBridge;->LOG_TAG:Ljava/lang/String;

    #@26
    const-string v3, "Error while unregistering UiTestAutomationService"

    #@28
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2b
    goto :goto_22
.end method

.method public executeCommandAndWaitForAccessibilityEvent(Ljava/lang/Runnable;Lcom/android/internal/util/Predicate;J)Landroid/view/accessibility/AccessibilityEvent;
    .registers 15
    .parameter "command"
    .parameter
    .parameter "timeoutMillis"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "Lcom/android/internal/util/Predicate",
            "<",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ">;J)",
            "Landroid/view/accessibility/AccessibilityEvent;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/TimeoutException;,
            Ljava/lang/Exception;
        }
    .end annotation

    #@0
    .prologue
    .line 263
    .local p2, predicate:Lcom/android/internal/util/Predicate;,"Lcom/android/internal/util/Predicate<Landroid/view/accessibility/AccessibilityEvent;>;"
    iget-object v7, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v7

    #@3
    .line 265
    const/4 v6, 0x1

    #@4
    :try_start_4
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mWaitingForEventDelivery:Z

    #@6
    .line 266
    const/4 v6, 0x0

    #@7
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@9
    .line 267
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@b
    if-eqz v6, :cond_15

    #@d
    .line 268
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@f
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@12
    .line 269
    const/4 v6, 0x0

    #@13
    iput-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@15
    .line 272
    :cond_15
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    #@18
    .line 274
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1b
    move-result-wide v4

    #@1c
    .line 277
    .local v4, startTimeMillis:J
    :goto_1c
    iget-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@1e
    if-eqz v6, :cond_37

    #@20
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@22
    invoke-interface {p2, v6}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    #@25
    move-result v6

    #@26
    if-eqz v6, :cond_37

    #@28
    .line 278
    const/4 v6, 0x0

    #@29
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mWaitingForEventDelivery:Z

    #@2b
    .line 279
    const/4 v6, 0x0

    #@2c
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@2e
    .line 280
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@30
    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    #@33
    .line 281
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@35
    monitor-exit v7

    #@36
    return-object v6

    #@37
    .line 284
    :cond_37
    const/4 v6, 0x1

    #@38
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mWaitingForEventDelivery:Z

    #@3a
    .line 285
    const/4 v6, 0x0

    #@3b
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@3d
    .line 286
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@3f
    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    #@42
    .line 288
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@45
    move-result-wide v8

    #@46
    sub-long v0, v8, v4

    #@48
    .line 289
    .local v0, elapsedTimeMillis:J
    sub-long v2, p3, v0

    #@4a
    .line 290
    .local v2, remainingTimeMillis:J
    const-wide/16 v8, 0x0

    #@4c
    cmp-long v6, v2, v8

    #@4e
    if-gtz v6, :cond_7d

    #@50
    .line 291
    const/4 v6, 0x0

    #@51
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mWaitingForEventDelivery:Z

    #@53
    .line 292
    const/4 v6, 0x0

    #@54
    iput-boolean v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mUnprocessedEventAvailable:Z

    #@56
    .line 293
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@58
    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    #@5b
    .line 294
    new-instance v6, Ljava/util/concurrent/TimeoutException;

    #@5d
    new-instance v8, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v9, "Expacted event not received within: "

    #@64
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v8

    #@68
    invoke-virtual {v8, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    const-string v9, " ms."

    #@6e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v8

    #@72
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v8

    #@76
    invoke-direct {v6, v8}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    #@79
    throw v6

    #@7a
    .line 303
    .end local v0           #elapsedTimeMillis:J
    .end local v2           #remainingTimeMillis:J
    .end local v4           #startTimeMillis:J
    :catchall_7a
    move-exception v6

    #@7b
    monitor-exit v7
    :try_end_7c
    .catchall {:try_start_4 .. :try_end_7c} :catchall_7a

    #@7c
    throw v6

    #@7d
    .line 298
    .restart local v0       #elapsedTimeMillis:J
    .restart local v2       #remainingTimeMillis:J
    .restart local v4       #startTimeMillis:J
    :cond_7d
    :try_start_7d
    iget-object v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@7f
    invoke-virtual {v6, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_82
    .catchall {:try_start_7d .. :try_end_82} :catchall_7a
    .catch Ljava/lang/InterruptedException; {:try_start_7d .. :try_end_82} :catch_83

    #@82
    goto :goto_1c

    #@83
    .line 299
    :catch_83
    move-exception v6

    #@84
    goto :goto_1c
.end method

.method public findAccessibilityNodeInfoByAccessibilityId(IJ)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 11
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    .line 368
    iget v6, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    .line 369
    .local v6, connectionId:I
    invoke-direct {p0, v6}, Landroid/accessibilityservice/UiTestAutomationBridge;->ensureValidConnection(I)V

    #@5
    .line 370
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@8
    move-result-object v0

    #@9
    iget v1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@b
    const/4 v5, 0x7

    #@c
    move v2, p1

    #@d
    move-wide v3, p2

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method

.method public findAccessibilityNodeInfoByAccessibilityIdInActiveWindow(J)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 4
    .parameter "accessibilityNodeId"

    #@0
    .prologue
    .line 353
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0, p1, p2}, Landroid/accessibilityservice/UiTestAutomationBridge;->findAccessibilityNodeInfoByAccessibilityId(IJ)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public findAccessibilityNodeInfoByViewId(IJI)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 11
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "viewId"

    #@0
    .prologue
    .line 402
    iget v1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    .line 403
    .local v1, connectionId:I
    invoke-direct {p0, v1}, Landroid/accessibilityservice/UiTestAutomationBridge;->ensureValidConnection(I)V

    #@5
    .line 404
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@8
    move-result-object v0

    #@9
    move v2, p1

    #@a
    move-wide v3, p2

    #@b
    move v5, p4

    #@c
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByViewId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public findAccessibilityNodeInfoByViewIdInActiveWindow(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 5
    .parameter "viewId"

    #@0
    .prologue
    .line 384
    const/4 v0, -0x1

    #@1
    sget-wide v1, Landroid/accessibilityservice/UiTestAutomationBridge;->ROOT_NODE_ID:J

    #@3
    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/accessibilityservice/UiTestAutomationBridge;->findAccessibilityNodeInfoByViewId(IJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public findAccessibilityNodeInfosByText(IJLjava/lang/String;)Ljava/util/List;
    .registers 11
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 436
    iget v1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    .line 437
    .local v1, connectionId:I
    invoke-direct {p0, v1}, Landroid/accessibilityservice/UiTestAutomationBridge;->ensureValidConnection(I)V

    #@5
    .line 438
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@8
    move-result-object v0

    #@9
    move v2, p1

    #@a
    move-wide v3, p2

    #@b
    move-object v5, p4

    #@c
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfosByText(IIJLjava/lang/String;)Ljava/util/List;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public findAccessibilityNodeInfosByTextInActiveWindow(Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter "text"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/view/accessibility/AccessibilityNodeInfo;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 417
    const/4 v0, -0x1

    #@1
    sget-wide v1, Landroid/accessibilityservice/UiTestAutomationBridge;->ROOT_NODE_ID:J

    #@3
    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/accessibilityservice/UiTestAutomationBridge;->findAccessibilityNodeInfosByText(IJLjava/lang/String;)Ljava/util/List;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public getLastAccessibilityEvent()Landroid/view/accessibility/AccessibilityEvent;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@2
    return-object v0
.end method

.method public getRootAccessibilityNodeInfoInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;
    .registers 7

    #@0
    .prologue
    .line 483
    iget v1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    .line 484
    .local v1, connectionId:I
    invoke-direct {p0, v1}, Landroid/accessibilityservice/UiTestAutomationBridge;->ensureValidConnection(I)V

    #@5
    .line 485
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@8
    move-result-object v0

    #@9
    const/4 v2, -0x1

    #@a
    sget-wide v3, Landroid/accessibilityservice/UiTestAutomationBridge;->ROOT_NODE_ID:J

    #@c
    const/4 v5, 0x4

    #@d
    invoke-virtual/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityInteractionClient;->findAccessibilityNodeInfoByAccessibilityId(IIJI)Landroid/view/accessibility/AccessibilityNodeInfo;

    #@10
    move-result-object v0

    #@11
    return-object v0
.end method

.method public isConnected()Z
    .registers 3

    #@0
    .prologue
    .line 248
    iget v0, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    const/4 v1, -0x1

    #@3
    if-eq v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 2
    .parameter "event"

    #@0
    .prologue
    .line 100
    return-void
.end method

.method public onInterrupt()V
    .registers 1

    #@0
    .prologue
    .line 111
    return-void
.end method

.method public performAccessibilityAction(IJILandroid/os/Bundle;)Z
    .registers 13
    .parameter "accessibilityWindowId"
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 470
    iget v1, p0, Landroid/accessibilityservice/UiTestAutomationBridge;->mConnectionId:I

    #@2
    .line 471
    .local v1, connectionId:I
    invoke-direct {p0, v1}, Landroid/accessibilityservice/UiTestAutomationBridge;->ensureValidConnection(I)V

    #@5
    .line 472
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@8
    move-result-object v0

    #@9
    move v2, p1

    #@a
    move-wide v3, p2

    #@b
    move v5, p4

    #@c
    move-object v6, p5

    #@d
    invoke-virtual/range {v0 .. v6}, Landroid/view/accessibility/AccessibilityInteractionClient;->performAccessibilityAction(IIJILandroid/os/Bundle;)Z

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public performAccessibilityActionInActiveWindow(JILandroid/os/Bundle;)Z
    .registers 11
    .parameter "accessibilityNodeId"
    .parameter "action"
    .parameter "arguments"

    #@0
    .prologue
    .line 454
    const/4 v1, -0x1

    #@1
    move-object v0, p0

    #@2
    move-wide v2, p1

    #@3
    move v4, p3

    #@4
    move-object v5, p4

    #@5
    invoke-virtual/range {v0 .. v5}, Landroid/accessibilityservice/UiTestAutomationBridge;->performAccessibilityAction(IJILandroid/os/Bundle;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public waitForIdle(JJ)V
    .registers 23
    .parameter "idleTimeout"
    .parameter "globalTimeout"

    #@0
    .prologue
    .line 316
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v12

    #@4
    .line 317
    .local v12, startTimeMillis:J
    move-object/from16 v0, p0

    #@6
    iget-object v14, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@8
    if-eqz v14, :cond_23

    #@a
    move-object/from16 v0, p0

    #@c
    iget-object v14, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@e
    invoke-virtual {v14}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    #@11
    move-result-wide v6

    #@12
    .line 319
    .local v6, lastEventTime:J
    :goto_12
    move-object/from16 v0, p0

    #@14
    iget-object v15, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@16
    monitor-enter v15

    #@17
    .line 321
    :goto_17
    :try_start_17
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1a
    move-result-wide v2

    #@1b
    .line 322
    .local v2, currentTimeMillis:J
    sub-long v10, v2, v6

    #@1d
    .line 323
    .local v10, sinceLastEventTimeMillis:J
    cmp-long v14, v10, p1

    #@1f
    if-lez v14, :cond_28

    #@21
    .line 324
    monitor-exit v15
    :try_end_22
    .catchall {:try_start_17 .. :try_end_22} :catchall_46

    #@22
    .line 332
    :goto_22
    return-void

    #@23
    .line 317
    .end local v2           #currentTimeMillis:J
    .end local v6           #lastEventTime:J
    .end local v10           #sinceLastEventTimeMillis:J
    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26
    move-result-wide v6

    #@27
    goto :goto_12

    #@28
    .line 326
    .restart local v2       #currentTimeMillis:J
    .restart local v6       #lastEventTime:J
    .restart local v10       #sinceLastEventTimeMillis:J
    :cond_28
    :try_start_28
    move-object/from16 v0, p0

    #@2a
    iget-object v14, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@2c
    if-eqz v14, :cond_36

    #@2e
    .line 327
    move-object/from16 v0, p0

    #@30
    iget-object v14, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLastEvent:Landroid/view/accessibility/AccessibilityEvent;

    #@32
    invoke-virtual {v14}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    #@35
    move-result-wide v6

    #@36
    .line 329
    :cond_36
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@39
    move-result-wide v16

    #@3a
    sub-long v4, v16, v12

    #@3c
    .line 330
    .local v4, elapsedTimeMillis:J
    sub-long v8, p3, v4

    #@3e
    .line 331
    .local v8, remainingTimeMillis:J
    const-wide/16 v16, 0x0

    #@40
    cmp-long v14, v8, v16

    #@42
    if-gtz v14, :cond_49

    #@44
    .line 332
    monitor-exit v15

    #@45
    goto :goto_22

    #@46
    .line 340
    .end local v2           #currentTimeMillis:J
    .end local v4           #elapsedTimeMillis:J
    .end local v8           #remainingTimeMillis:J
    .end local v10           #sinceLastEventTimeMillis:J
    :catchall_46
    move-exception v14

    #@47
    monitor-exit v15
    :try_end_48
    .catchall {:try_start_28 .. :try_end_48} :catchall_46

    #@48
    throw v14

    #@49
    .line 335
    .restart local v2       #currentTimeMillis:J
    .restart local v4       #elapsedTimeMillis:J
    .restart local v8       #remainingTimeMillis:J
    .restart local v10       #sinceLastEventTimeMillis:J
    :cond_49
    :try_start_49
    move-object/from16 v0, p0

    #@4b
    iget-object v14, v0, Landroid/accessibilityservice/UiTestAutomationBridge;->mLock:Ljava/lang/Object;

    #@4d
    move-wide/from16 v0, p1

    #@4f
    invoke-virtual {v14, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_52
    .catchall {:try_start_49 .. :try_end_52} :catchall_46
    .catch Ljava/lang/InterruptedException; {:try_start_49 .. :try_end_52} :catch_53

    #@52
    goto :goto_17

    #@53
    .line 336
    :catch_53
    move-exception v14

    #@54
    goto :goto_17
.end method
