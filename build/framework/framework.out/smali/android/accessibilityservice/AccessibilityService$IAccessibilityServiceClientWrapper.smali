.class Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;
.super Landroid/accessibilityservice/IAccessibilityServiceClient$Stub;
.source "AccessibilityService.java"

# interfaces
.implements Lcom/android/internal/os/HandlerCaller$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/accessibilityservice/AccessibilityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IAccessibilityServiceClientWrapper"
.end annotation


# static fields
.field private static final DO_ON_ACCESSIBILITY_EVENT:I = 0x1e

.field private static final DO_ON_GESTURE:I = 0x28

.field private static final DO_ON_INTERRUPT:I = 0x14

.field private static final DO_SET_SET_CONNECTION:I = 0xa

.field static final NO_ID:I = -0x1


# instance fields
.field private final mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

.field private final mCaller:Lcom/android/internal/os/HandlerCaller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/accessibilityservice/AccessibilityService$Callbacks;)V
    .registers 5
    .parameter "context"
    .parameter "looper"
    .parameter "callback"

    #@0
    .prologue
    .line 557
    invoke-direct {p0}, Landroid/accessibilityservice/IAccessibilityServiceClient$Stub;-><init>()V

    #@3
    .line 558
    iput-object p3, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@5
    .line 559
    new-instance v0, Lcom/android/internal/os/HandlerCaller;

    #@7
    invoke-direct {v0, p1, p2, p0}, Lcom/android/internal/os/HandlerCaller;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/android/internal/os/HandlerCaller$Callback;)V

    #@a
    iput-object v0, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@c
    .line 560
    return-void
.end method


# virtual methods
.method public executeMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "message"

    #@0
    .prologue
    .line 584
    iget v4, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v4, :sswitch_data_6c

    #@5
    .line 615
    const-string v4, "AccessibilityService"

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "Unknown message type "

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    iget v6, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 617
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 586
    :sswitch_20
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    check-cast v2, Landroid/view/accessibility/AccessibilityEvent;

    #@24
    .line 587
    .local v2, event:Landroid/view/accessibility/AccessibilityEvent;
    if-eqz v2, :cond_1f

    #@26
    .line 588
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v2}, Landroid/view/accessibility/AccessibilityInteractionClient;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@2d
    .line 589
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@2f
    invoke-interface {v4, v2}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    #@32
    .line 590
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->recycle()V

    #@35
    goto :goto_1f

    #@36
    .line 594
    .end local v2           #event:Landroid/view/accessibility/AccessibilityEvent;
    :sswitch_36
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@38
    invoke-interface {v4}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onInterrupt()V

    #@3b
    goto :goto_1f

    #@3c
    .line 597
    :sswitch_3c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@3e
    .line 598
    .local v1, connectionId:I
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40
    check-cast v0, Landroid/accessibilityservice/IAccessibilityServiceConnection;

    #@42
    .line 600
    .local v0, connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    if-eqz v0, :cond_56

    #@44
    .line 601
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, v1, v0}, Landroid/view/accessibility/AccessibilityInteractionClient;->addConnection(ILandroid/accessibilityservice/IAccessibilityServiceConnection;)V

    #@4b
    .line 603
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@4d
    invoke-interface {v4, v1}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onSetConnectionId(I)V

    #@50
    .line 604
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@52
    invoke-interface {v4}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onServiceConnected()V

    #@55
    goto :goto_1f

    #@56
    .line 606
    :cond_56
    invoke-static {}, Landroid/view/accessibility/AccessibilityInteractionClient;->getInstance()Landroid/view/accessibility/AccessibilityInteractionClient;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4, v1}, Landroid/view/accessibility/AccessibilityInteractionClient;->removeConnection(I)V

    #@5d
    .line 607
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@5f
    const/4 v5, -0x1

    #@60
    invoke-interface {v4, v5}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onSetConnectionId(I)V

    #@63
    goto :goto_1f

    #@64
    .line 611
    .end local v0           #connection:Landroid/accessibilityservice/IAccessibilityServiceConnection;
    .end local v1           #connectionId:I
    :sswitch_64
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@66
    .line 612
    .local v3, gestureId:I
    iget-object v4, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCallback:Landroid/accessibilityservice/AccessibilityService$Callbacks;

    #@68
    invoke-interface {v4, v3}, Landroid/accessibilityservice/AccessibilityService$Callbacks;->onGesture(I)Z

    #@6b
    goto :goto_1f

    #@6c
    .line 584
    :sswitch_data_6c
    .sparse-switch
        0xa -> :sswitch_3c
        0x14 -> :sswitch_36
        0x1e -> :sswitch_20
        0x28 -> :sswitch_64
    .end sparse-switch
.end method

.method public onAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter "event"

    #@0
    .prologue
    .line 574
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0x1e

    #@4
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 575
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 576
    return-void
.end method

.method public onGesture(I)V
    .registers 5
    .parameter "gestureId"

    #@0
    .prologue
    .line 579
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0x28

    #@4
    invoke-virtual {v1, v2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageI(II)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 580
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 581
    return-void
.end method

.method public onInterrupt()V
    .registers 4

    #@0
    .prologue
    .line 569
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0x14

    #@4
    invoke-virtual {v1, v2}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 570
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 571
    return-void
.end method

.method public setConnection(Landroid/accessibilityservice/IAccessibilityServiceConnection;I)V
    .registers 6
    .parameter "connection"
    .parameter "connectionId"

    #@0
    .prologue
    .line 563
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@2
    const/16 v2, 0xa

    #@4
    invoke-virtual {v1, v2, p2, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageIO(IILjava/lang/Object;)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 565
    .local v0, message:Landroid/os/Message;
    iget-object v1, p0, Landroid/accessibilityservice/AccessibilityService$IAccessibilityServiceClientWrapper;->mCaller:Lcom/android/internal/os/HandlerCaller;

    #@a
    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 566
    return-void
.end method
