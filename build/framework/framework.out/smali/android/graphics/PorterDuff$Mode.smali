.class public final enum Landroid/graphics/PorterDuff$Mode;
.super Ljava/lang/Enum;
.source "PorterDuff.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/PorterDuff;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/graphics/PorterDuff$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/graphics/PorterDuff$Mode;

.field public static final enum ADD:Landroid/graphics/PorterDuff$Mode;

.field public static final enum CLEAR:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DARKEN:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DST:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DST_ATOP:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DST_IN:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DST_OUT:Landroid/graphics/PorterDuff$Mode;

.field public static final enum DST_OVER:Landroid/graphics/PorterDuff$Mode;

.field public static final enum LIGHTEN:Landroid/graphics/PorterDuff$Mode;

.field public static final enum MULTIPLY:Landroid/graphics/PorterDuff$Mode;

.field public static final enum OVERLAY:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SCREEN:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SRC:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SRC_IN:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SRC_OUT:Landroid/graphics/PorterDuff$Mode;

.field public static final enum SRC_OVER:Landroid/graphics/PorterDuff$Mode;

.field public static final enum XOR:Landroid/graphics/PorterDuff$Mode;


# instance fields
.field public final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 24
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@7
    const-string v1, "CLEAR"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@e
    .line 26
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@10
    const-string v1, "SRC"

    #@12
    invoke-direct {v0, v1, v5, v5}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    #@17
    .line 28
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@19
    const-string v1, "DST"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DST:Landroid/graphics/PorterDuff$Mode;

    #@20
    .line 30
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@22
    const-string v1, "SRC_OVER"

    #@24
    invoke-direct {v0, v1, v7, v7}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    #@29
    .line 32
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@2b
    const-string v1, "DST_OVER"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DST_OVER:Landroid/graphics/PorterDuff$Mode;

    #@32
    .line 34
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@34
    const-string v1, "SRC_IN"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    #@3d
    .line 36
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@3f
    const-string v1, "DST_IN"

    #@41
    const/4 v2, 0x6

    #@42
    const/4 v3, 0x6

    #@43
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@46
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    #@48
    .line 38
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@4a
    const-string v1, "SRC_OUT"

    #@4c
    const/4 v2, 0x7

    #@4d
    const/4 v3, 0x7

    #@4e
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@51
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    #@53
    .line 40
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@55
    const-string v1, "DST_OUT"

    #@57
    const/16 v2, 0x8

    #@59
    const/16 v3, 0x8

    #@5b
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@5e
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    #@60
    .line 42
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@62
    const-string v1, "SRC_ATOP"

    #@64
    const/16 v2, 0x9

    #@66
    const/16 v3, 0x9

    #@68
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@6b
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@6d
    .line 44
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@6f
    const-string v1, "DST_ATOP"

    #@71
    const/16 v2, 0xa

    #@73
    const/16 v3, 0xa

    #@75
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@78
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@7a
    .line 46
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@7c
    const-string v1, "XOR"

    #@7e
    const/16 v2, 0xb

    #@80
    const/16 v3, 0xb

    #@82
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@85
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    #@87
    .line 49
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@89
    const-string v1, "DARKEN"

    #@8b
    const/16 v2, 0xc

    #@8d
    const/16 v3, 0xc

    #@8f
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@92
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    #@94
    .line 52
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@96
    const-string v1, "LIGHTEN"

    #@98
    const/16 v2, 0xd

    #@9a
    const/16 v3, 0xd

    #@9c
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@9f
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->LIGHTEN:Landroid/graphics/PorterDuff$Mode;

    #@a1
    .line 54
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@a3
    const-string v1, "MULTIPLY"

    #@a5
    const/16 v2, 0xe

    #@a7
    const/16 v3, 0xe

    #@a9
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@ac
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    #@ae
    .line 56
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@b0
    const-string v1, "SCREEN"

    #@b2
    const/16 v2, 0xf

    #@b4
    const/16 v3, 0xf

    #@b6
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@b9
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    #@bb
    .line 58
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@bd
    const-string v1, "ADD"

    #@bf
    const/16 v2, 0x10

    #@c1
    const/16 v3, 0x10

    #@c3
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@c6
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    #@c8
    .line 59
    new-instance v0, Landroid/graphics/PorterDuff$Mode;

    #@ca
    const-string v1, "OVERLAY"

    #@cc
    const/16 v2, 0x11

    #@ce
    const/16 v3, 0x11

    #@d0
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/PorterDuff$Mode;-><init>(Ljava/lang/String;II)V

    #@d3
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->OVERLAY:Landroid/graphics/PorterDuff$Mode;

    #@d5
    .line 22
    const/16 v0, 0x12

    #@d7
    new-array v0, v0, [Landroid/graphics/PorterDuff$Mode;

    #@d9
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    #@db
    aput-object v1, v0, v4

    #@dd
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    #@df
    aput-object v1, v0, v5

    #@e1
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST:Landroid/graphics/PorterDuff$Mode;

    #@e3
    aput-object v1, v0, v6

    #@e5
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    #@e7
    aput-object v1, v0, v7

    #@e9
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OVER:Landroid/graphics/PorterDuff$Mode;

    #@eb
    aput-object v1, v0, v8

    #@ed
    const/4 v1, 0x5

    #@ee
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    #@f0
    aput-object v2, v0, v1

    #@f2
    const/4 v1, 0x6

    #@f3
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    #@f5
    aput-object v2, v0, v1

    #@f7
    const/4 v1, 0x7

    #@f8
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    #@fa
    aput-object v2, v0, v1

    #@fc
    const/16 v1, 0x8

    #@fe
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    #@100
    aput-object v2, v0, v1

    #@102
    const/16 v1, 0x9

    #@104
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@106
    aput-object v2, v0, v1

    #@108
    const/16 v1, 0xa

    #@10a
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    #@10c
    aput-object v2, v0, v1

    #@10e
    const/16 v1, 0xb

    #@110
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    #@112
    aput-object v2, v0, v1

    #@114
    const/16 v1, 0xc

    #@116
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DARKEN:Landroid/graphics/PorterDuff$Mode;

    #@118
    aput-object v2, v0, v1

    #@11a
    const/16 v1, 0xd

    #@11c
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->LIGHTEN:Landroid/graphics/PorterDuff$Mode;

    #@11e
    aput-object v2, v0, v1

    #@120
    const/16 v1, 0xe

    #@122
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    #@124
    aput-object v2, v0, v1

    #@126
    const/16 v1, 0xf

    #@128
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    #@12a
    aput-object v2, v0, v1

    #@12c
    const/16 v1, 0x10

    #@12e
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    #@130
    aput-object v2, v0, v1

    #@132
    const/16 v1, 0x11

    #@134
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->OVERLAY:Landroid/graphics/PorterDuff$Mode;

    #@136
    aput-object v2, v0, v1

    #@138
    sput-object v0, Landroid/graphics/PorterDuff$Mode;->$VALUES:[Landroid/graphics/PorterDuff$Mode;

    #@13a
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "nativeInt"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 62
    iput p3, p0, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@5
    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 22
    const-class v0, Landroid/graphics/PorterDuff$Mode;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/graphics/PorterDuff$Mode;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/graphics/PorterDuff$Mode;
    .registers 1

    #@0
    .prologue
    .line 22
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->$VALUES:[Landroid/graphics/PorterDuff$Mode;

    #@2
    invoke-virtual {v0}, [Landroid/graphics/PorterDuff$Mode;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/graphics/PorterDuff$Mode;

    #@8
    return-object v0
.end method
