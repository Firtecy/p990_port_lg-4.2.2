.class public Landroid/graphics/NinePatch;
.super Ljava/lang/Object;
.source "NinePatch.java"


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private final mChunk:[B

.field private mPaint:Landroid/graphics/Paint;

.field private final mRect:Landroid/graphics/RectF;

.field private mSrcName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V
    .registers 5
    .parameter "bitmap"
    .parameter "chunk"
    .parameter "srcName"

    #@0
    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    new-instance v0, Landroid/graphics/RectF;

    #@5
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@8
    iput-object v0, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@a
    .line 53
    iput-object p1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@c
    .line 54
    iput-object p2, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@e
    .line 55
    iput-object p3, p0, Landroid/graphics/NinePatch;->mSrcName:Ljava/lang/String;

    #@10
    .line 56
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@12
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->ni()I

    #@15
    move-result v0

    #@16
    invoke-static {v0, p2}, Landroid/graphics/NinePatch;->validateNinePatchChunk(I[B)V

    #@19
    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/graphics/NinePatch;)V
    .registers 4
    .parameter "patch"

    #@0
    .prologue
    .line 62
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 42
    new-instance v0, Landroid/graphics/RectF;

    #@5
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@8
    iput-object v0, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@a
    .line 63
    iget-object v0, p1, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@c
    iput-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@e
    .line 64
    iget-object v0, p1, Landroid/graphics/NinePatch;->mChunk:[B

    #@10
    iput-object v0, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@12
    .line 65
    iget-object v0, p1, Landroid/graphics/NinePatch;->mSrcName:Ljava/lang/String;

    #@14
    iput-object v0, p0, Landroid/graphics/NinePatch;->mSrcName:Ljava/lang/String;

    #@16
    .line 66
    iget-object v0, p1, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@18
    if-eqz v0, :cond_23

    #@1a
    .line 67
    new-instance v0, Landroid/graphics/Paint;

    #@1c
    iget-object v1, p1, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@1e
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    #@21
    iput-object v0, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@23
    .line 69
    :cond_23
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@25
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->ni()I

    #@28
    move-result v0

    #@29
    iget-object v1, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@2b
    invoke-static {v0, v1}, Landroid/graphics/NinePatch;->validateNinePatchChunk(I[B)V

    #@2e
    .line 70
    return-void
.end method

.method public static native isNinePatchChunk([B)Z
.end method

.method private static native nativeDraw(ILandroid/graphics/Rect;I[BIII)V
.end method

.method private static native nativeDraw(ILandroid/graphics/RectF;I[BIII)V
.end method

.method private static native nativeGetTransparentRegion(I[BLandroid/graphics/Rect;)I
.end method

.method private static native validateNinePatchChunk(I[B)V
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .registers 10
    .parameter "canvas"
    .parameter "location"

    #@0
    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 101
    iget v0, p1, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@8
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@a
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->ni()I

    #@d
    move-result v2

    #@e
    iget-object v3, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@10
    iget-object v1, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@12
    if-eqz v1, :cond_23

    #@14
    iget-object v1, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@16
    iget v4, v1, Landroid/graphics/Paint;->mNativePaint:I

    #@18
    :goto_18
    iget v5, p1, Landroid/graphics/Canvas;->mDensity:I

    #@1a
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@1c
    iget v6, v1, Landroid/graphics/Bitmap;->mDensity:I

    #@1e
    move-object v1, p2

    #@1f
    invoke-static/range {v0 .. v6}, Landroid/graphics/NinePatch;->nativeDraw(ILandroid/graphics/Rect;I[BIII)V

    #@22
    .line 109
    :goto_22
    return-void

    #@23
    .line 101
    :cond_23
    const/4 v4, 0x0

    #@24
    goto :goto_18

    #@25
    .line 106
    :cond_25
    iget-object v0, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@27
    invoke-virtual {v0, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@2a
    .line 107
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2c
    iget-object v1, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@2e
    iget-object v2, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@30
    iget-object v3, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@32
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawPatch(Landroid/graphics/Bitmap;[BLandroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@35
    goto :goto_22
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .registers 11
    .parameter "canvas"
    .parameter "location"
    .parameter "paint"

    #@0
    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_21

    #@6
    .line 120
    iget v0, p1, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@8
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@a
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->ni()I

    #@d
    move-result v2

    #@e
    iget-object v3, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@10
    if-eqz p3, :cond_1f

    #@12
    iget v4, p3, Landroid/graphics/Paint;->mNativePaint:I

    #@14
    :goto_14
    iget v5, p1, Landroid/graphics/Canvas;->mDensity:I

    #@16
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@18
    iget v6, v1, Landroid/graphics/Bitmap;->mDensity:I

    #@1a
    move-object v1, p2

    #@1b
    invoke-static/range {v0 .. v6}, Landroid/graphics/NinePatch;->nativeDraw(ILandroid/graphics/Rect;I[BIII)V

    #@1e
    .line 127
    :goto_1e
    return-void

    #@1f
    .line 120
    :cond_1f
    const/4 v4, 0x0

    #@20
    goto :goto_14

    #@21
    .line 124
    :cond_21
    iget-object v0, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@23
    invoke-virtual {v0, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    #@26
    .line 125
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@28
    iget-object v1, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@2a
    iget-object v2, p0, Landroid/graphics/NinePatch;->mRect:Landroid/graphics/RectF;

    #@2c
    invoke-virtual {p1, v0, v1, v2, p3}, Landroid/graphics/Canvas;->drawPatch(Landroid/graphics/Bitmap;[BLandroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@2f
    goto :goto_1e
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .registers 10
    .parameter "canvas"
    .parameter "location"

    #@0
    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_25

    #@6
    .line 84
    iget v0, p1, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@8
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@a
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->ni()I

    #@d
    move-result v2

    #@e
    iget-object v3, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@10
    iget-object v1, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@12
    if-eqz v1, :cond_23

    #@14
    iget-object v1, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@16
    iget v4, v1, Landroid/graphics/Paint;->mNativePaint:I

    #@18
    :goto_18
    iget v5, p1, Landroid/graphics/Canvas;->mDensity:I

    #@1a
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@1c
    iget v6, v1, Landroid/graphics/Bitmap;->mDensity:I

    #@1e
    move-object v1, p2

    #@1f
    invoke-static/range {v0 .. v6}, Landroid/graphics/NinePatch;->nativeDraw(ILandroid/graphics/RectF;I[BIII)V

    #@22
    .line 91
    :goto_22
    return-void

    #@23
    .line 84
    :cond_23
    const/4 v4, 0x0

    #@24
    goto :goto_18

    #@25
    .line 89
    :cond_25
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@27
    iget-object v1, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@29
    iget-object v2, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@2b
    invoke-virtual {p1, v0, v1, p2, v2}, Landroid/graphics/Canvas;->drawPatch(Landroid/graphics/Bitmap;[BLandroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@2e
    goto :goto_22
.end method

.method public getDensity()I
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    iget v0, v0, Landroid/graphics/Bitmap;->mDensity:I

    #@4
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getTransparentRegion(Landroid/graphics/Rect;)Landroid/graphics/Region;
    .registers 5
    .parameter "location"

    #@0
    .prologue
    .line 150
    iget-object v1, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->ni()I

    #@5
    move-result v1

    #@6
    iget-object v2, p0, Landroid/graphics/NinePatch;->mChunk:[B

    #@8
    invoke-static {v1, v2, p1}, Landroid/graphics/NinePatch;->nativeGetTransparentRegion(I[BLandroid/graphics/Rect;)I

    #@b
    move-result v0

    #@c
    .line 151
    .local v0, r:I
    if-eqz v0, :cond_14

    #@e
    new-instance v1, Landroid/graphics/Region;

    #@10
    invoke-direct {v1, v0}, Landroid/graphics/Region;-><init>(I)V

    #@13
    :goto_13
    return-object v1

    #@14
    :cond_14
    const/4 v1, 0x0

    #@15
    goto :goto_13
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 138
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final hasAlpha()Z
    .registers 2

    #@0
    .prologue
    .line 146
    iget-object v0, p0, Landroid/graphics/NinePatch;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .registers 2
    .parameter "p"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Landroid/graphics/NinePatch;->mPaint:Landroid/graphics/Paint;

    #@2
    .line 74
    return-void
.end method
