.class public Landroid/graphics/PaintFlagsDrawFilter;
.super Landroid/graphics/DrawFilter;
.source "PaintFlagsDrawFilter.java"


# instance fields
.field public final clearBits:I

.field public final setBits:I


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "clearBits"
    .parameter "setBits"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/graphics/DrawFilter;-><init>()V

    #@3
    .line 34
    iput p1, p0, Landroid/graphics/PaintFlagsDrawFilter;->clearBits:I

    #@5
    .line 35
    iput p2, p0, Landroid/graphics/PaintFlagsDrawFilter;->setBits:I

    #@7
    .line 38
    invoke-static {p1, p2}, Landroid/graphics/PaintFlagsDrawFilter;->nativeConstructor(II)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/DrawFilter;->mNativeInt:I

    #@d
    .line 39
    return-void
.end method

.method private static native nativeConstructor(II)I
.end method
