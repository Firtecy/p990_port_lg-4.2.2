.class public Landroid/graphics/EmbossMaskFilter;
.super Landroid/graphics/MaskFilter;
.source "EmbossMaskFilter.java"


# direct methods
.method public constructor <init>([FFFF)V
    .registers 7
    .parameter "direction"
    .parameter "ambient"
    .parameter "specular"
    .parameter "blurRadius"

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Landroid/graphics/MaskFilter;-><init>()V

    #@3
    .line 30
    array-length v0, p1

    #@4
    const/4 v1, 0x3

    #@5
    if-ge v0, v1, :cond_d

    #@7
    .line 31
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@9
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@c
    throw v0

    #@d
    .line 33
    :cond_d
    invoke-static {p1, p2, p3, p4}, Landroid/graphics/EmbossMaskFilter;->nativeConstructor([FFFF)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/graphics/MaskFilter;->native_instance:I

    #@13
    .line 34
    return-void
.end method

.method private static native nativeConstructor([FFFF)I
.end method
