.class public Landroid/graphics/Typeface;
.super Ljava/lang/Object;
.source "Typeface.java"


# static fields
.field public static final BOLD:I = 0x1

.field public static final BOLD_ITALIC:I = 0x3

.field public static final DEFAULT:Landroid/graphics/Typeface; = null

.field public static final DEFAULT_BOLD:Landroid/graphics/Typeface; = null

.field public static final ITALIC:I = 0x2

.field public static final MONOSPACE:Landroid/graphics/Typeface;

.field public static final NORMAL:I

.field public static final SANS_SERIF:Landroid/graphics/Typeface;

.field public static final SERIF:Landroid/graphics/Typeface;

.field static sDefaults:[Landroid/graphics/Typeface;

.field private static final sTypefaceCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Typeface;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mStyle:I

.field native_instance:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, 0x3

    #@3
    const/4 v1, 0x0

    #@4
    const/4 v3, 0x0

    #@5
    .line 48
    new-instance v0, Landroid/util/SparseArray;

    #@7
    invoke-direct {v0, v4}, Landroid/util/SparseArray;-><init>(I)V

    #@a
    sput-object v0, Landroid/graphics/Typeface;->sTypefaceCache:Landroid/util/SparseArray;

    #@c
    move-object v0, v1

    #@d
    .line 183
    check-cast v0, Ljava/lang/String;

    #@f
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@12
    move-result-object v0

    #@13
    sput-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@15
    move-object v0, v1

    #@16
    .line 184
    check-cast v0, Ljava/lang/String;

    #@18
    invoke-static {v0, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@1b
    move-result-object v0

    #@1c
    sput-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@1e
    .line 185
    const-string/jumbo v0, "sans-serif"

    #@21
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@24
    move-result-object v0

    #@25
    sput-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@27
    .line 186
    const-string/jumbo v0, "serif"

    #@2a
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    #@30
    .line 187
    const-string/jumbo v0, "monospace"

    #@33
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@36
    move-result-object v0

    #@37
    sput-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    #@39
    .line 189
    const/4 v0, 0x4

    #@3a
    new-array v2, v0, [Landroid/graphics/Typeface;

    #@3c
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@3e
    aput-object v0, v2, v3

    #@40
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@42
    aput-object v0, v2, v5

    #@44
    move-object v0, v1

    #@45
    check-cast v0, Ljava/lang/String;

    #@47
    invoke-static {v0, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@4a
    move-result-object v0

    #@4b
    aput-object v0, v2, v6

    #@4d
    check-cast v1, Ljava/lang/String;

    #@4f
    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@52
    move-result-object v0

    #@53
    aput-object v0, v2, v4

    #@55
    sput-object v2, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@57
    .line 195
    return-void
.end method

.method private constructor <init>(I)V
    .registers 4
    .parameter "ni"

    #@0
    .prologue
    .line 173
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 59
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/graphics/Typeface;->mStyle:I

    #@6
    .line 174
    if-nez p1, :cond_11

    #@8
    .line 175
    new-instance v0, Ljava/lang/RuntimeException;

    #@a
    const-string/jumbo v1, "native typeface cannot be made"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 178
    :cond_11
    iput p1, p0, Landroid/graphics/Typeface;->native_instance:I

    #@13
    .line 179
    invoke-static {p1}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@16
    move-result v0

    #@17
    iput v0, p0, Landroid/graphics/Typeface;->mStyle:I

    #@19
    .line 180
    return-void
.end method

.method public static addNewFont(I)I
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 288
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeAddNewFont(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;
    .registers 6
    .parameter "family"
    .parameter "style"

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    .line 104
    .local v0, ni:I
    if-eqz p0, :cond_b

    #@3
    .line 106
    iget v3, p0, Landroid/graphics/Typeface;->mStyle:I

    #@5
    if-ne v3, p1, :cond_9

    #@7
    move-object v2, p0

    #@8
    .line 130
    :cond_8
    :goto_8
    return-object v2

    #@9
    .line 110
    :cond_9
    iget v0, p0, Landroid/graphics/Typeface;->native_instance:I

    #@b
    .line 114
    :cond_b
    sget-object v3, Landroid/graphics/Typeface;->sTypefaceCache:Landroid/util/SparseArray;

    #@d
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, Landroid/util/SparseArray;

    #@13
    .line 116
    .local v1, styles:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/graphics/Typeface;>;"
    if-eqz v1, :cond_1d

    #@15
    .line 117
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v2

    #@19
    check-cast v2, Landroid/graphics/Typeface;

    #@1b
    .line 118
    .local v2, typeface:Landroid/graphics/Typeface;
    if-nez v2, :cond_8

    #@1d
    .line 123
    .end local v2           #typeface:Landroid/graphics/Typeface;
    :cond_1d
    new-instance v2, Landroid/graphics/Typeface;

    #@1f
    invoke-static {v0, p1}, Landroid/graphics/Typeface;->nativeCreateFromTypeface(II)I

    #@22
    move-result v3

    #@23
    invoke-direct {v2, v3}, Landroid/graphics/Typeface;-><init>(I)V

    #@26
    .line 124
    .restart local v2       #typeface:Landroid/graphics/Typeface;
    if-nez v1, :cond_33

    #@28
    .line 125
    new-instance v1, Landroid/util/SparseArray;

    #@2a
    .end local v1           #styles:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/graphics/Typeface;>;"
    const/4 v3, 0x4

    #@2b
    invoke-direct {v1, v3}, Landroid/util/SparseArray;-><init>(I)V

    #@2e
    .line 126
    .restart local v1       #styles:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Landroid/graphics/Typeface;>;"
    sget-object v3, Landroid/graphics/Typeface;->sTypefaceCache:Landroid/util/SparseArray;

    #@30
    invoke-virtual {v3, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@33
    .line 128
    :cond_33
    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@36
    goto :goto_8
.end method

.method public static create(Ljava/lang/String;I)Landroid/graphics/Typeface;
    .registers 4
    .parameter "familyName"
    .parameter "style"

    #@0
    .prologue
    .line 88
    new-instance v0, Landroid/graphics/Typeface;

    #@2
    invoke-static {p0, p1}, Landroid/graphics/Typeface;->nativeCreate(Ljava/lang/String;I)I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, v1}, Landroid/graphics/Typeface;-><init>(I)V

    #@9
    return-object v0
.end method

.method public static createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    .registers 4
    .parameter "mgr"
    .parameter "path"

    #@0
    .prologue
    .line 149
    new-instance v0, Landroid/graphics/Typeface;

    #@2
    invoke-static {p0, p1}, Landroid/graphics/Typeface;->nativeCreateFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, v1}, Landroid/graphics/Typeface;-><init>(I)V

    #@9
    return-object v0
.end method

.method public static createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 159
    new-instance v0, Landroid/graphics/Typeface;

    #@2
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Landroid/graphics/Typeface;->nativeCreateFromFile(Ljava/lang/String;)I

    #@9
    move-result v1

    #@a
    invoke-direct {v0, v1}, Landroid/graphics/Typeface;-><init>(I)V

    #@d
    return-object v0
.end method

.method public static createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 169
    new-instance v0, Landroid/graphics/Typeface;

    #@2
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeCreateFromFile(Ljava/lang/String;)I

    #@5
    move-result v1

    #@6
    invoke-direct {v0, v1}, Landroid/graphics/Typeface;-><init>(I)V

    #@9
    return-object v0
.end method

.method public static defaultFromStyle(I)Landroid/graphics/Typeface;
    .registers 2
    .parameter "style"

    #@0
    .prologue
    .line 139
    sget-object v0, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@2
    aget-object v0, v0, p0

    #@4
    return-object v0
.end method

.method public static getCheckNewFont(I)I
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 287
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeGetCheckNewFont(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getDefaultTypefaceIndex()I
    .registers 1

    #@0
    .prologue
    .line 275
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetDefaultTypefaceIndex()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getDownloadFontDstPath()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 281
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetDownloadFontDstPath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getDownloadFontName(I)Ljava/lang/String;
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 278
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeGetDownloadFontName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getDownloadFontSrcPath()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 280
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetDownloadFontSrcPath()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getFontFullPath(I)Ljava/lang/String;
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 279
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeGetFontFullPath(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getFontWebFaceName(I)Ljava/lang/String;
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 283
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeGetFontWebFaceName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getNewFontAppName(I)Ljava/lang/String;
    .registers 2
    .parameter "fontIndex"

    #@0
    .prologue
    .line 286
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeGetNewFontAppName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static getNumAllFonts()I
    .registers 1

    #@0
    .prologue
    .line 276
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetNumAllFonts()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getNumEmbeddedFonts()I
    .registers 1

    #@0
    .prologue
    .line 277
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetNumEmbeddedFonts()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getNumNewFonts()I
    .registers 1

    #@0
    .prologue
    .line 285
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetNumNewFonts()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static getSystemDefaultTypefaceIndex()I
    .registers 1

    #@0
    .prologue
    .line 282
    invoke-static {}, Landroid/graphics/Typeface;->nativeGetSystemDefaultTypefaceIndex()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private static nativeAddNewFont(I)I
	.registers 1
	return p1
.end method

.method private static native nativeCreate(Ljava/lang/String;I)I
.end method

.method private static native nativeCreateFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)I
.end method

.method private static native nativeCreateFromFile(Ljava/lang/String;)I
.end method

.method private static native nativeCreateFromTypeface(II)I
.end method

.method private static nativeGetCheckNewFont(I)I
	.registers 1
	return p1
.end method

.method private static nativeGetDefaultTypefaceIndex()I
	.registers 1
	const/4 v0, 0x0
	return v0
.end method

.method private static nativeGetDownloadFontDstPath()Ljava/lang/String;
	.registers 1
	const-string v0, ""
	return v0
.end method

.method private static nativeGetDownloadFontName(I)Ljava/lang/String;
	.registers 2
	const-string v0, ""
	return v0
.end method

.method private static nativeGetDownloadFontSrcPath()Ljava/lang/String;
	.registers 1
	const-string v0, ""
	return v0
.end method

.method private static nativeGetFontFullPath(I)Ljava/lang/String;
	.registers 2
	const-string v0, ""
	return v0
.end method

.method private static nativeGetFontWebFaceName(I)Ljava/lang/String;
	.registers 2
	const-string v0, ""
	return v0
.end method

.method private static nativeGetNewFontAppName(I)Ljava/lang/String;
	.registers 2
	const-string v0, ""
	return v0
.end method

.method private static nativeGetNumAllFonts()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method private static nativeGetNumEmbeddedFonts()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method private static nativeGetNumNewFonts()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method private static native nativeGetStyle(I)I
.end method

.method private static nativeGetSystemDefaultTypefaceIndex()I
	.registers 1
	const/4 v0, 0x0
	return v0
.end method

.method private static nativeSaveNewFont()V
	.registers 0
	return-void
.end method

.method private static nativeSelectDefaultTypeface(I)V
	.registers 1
	return-void	
.end method

.method private static native nativeUnref(I)V
.end method

.method private static nativeUpdateFontConfiguration()V
	.registers 0
	return-void
.end method

.method private static nativeUpdateFontManager()I
	.registers 1
	const/4 v0, 0x1
	return v0
.end method

.method public static saveNewFont()V
    .registers 0

    #@0
    .prologue
    .line 289
    invoke-static {}, Landroid/graphics/Typeface;->nativeSaveNewFont()V

    #@3
    return-void
.end method

.method public static selectDefaultTypeface(I)V
    .registers 1
    .parameter "fontIndex"

    #@0
    .prologue
    .line 274
    invoke-static {p0}, Landroid/graphics/Typeface;->nativeSelectDefaultTypeface(I)V

    #@3
    return-void
.end method

.method public static native setGammaForText(FF)V
.end method

.method public static updateFontConfiguration()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v1, 0x0

    #@3
    const/4 v4, 0x3

    #@4
    const/4 v3, 0x2

    #@5
    .line 243
    invoke-static {}, Landroid/graphics/Typeface;->nativeUpdateFontConfiguration()V

    #@8
    .line 245
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@a
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@c
    if-eqz v0, :cond_15

    #@e
    .line 246
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@10
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@12
    invoke-static {v0}, Landroid/graphics/Typeface;->nativeUnref(I)V

    #@15
    .line 248
    :cond_15
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@17
    invoke-static {v1, v5}, Landroid/graphics/Typeface;->nativeCreate(Ljava/lang/String;I)I

    #@1a
    move-result v2

    #@1b
    iput v2, v0, Landroid/graphics/Typeface;->native_instance:I

    #@1d
    .line 249
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@1f
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@21
    iget v2, v2, Landroid/graphics/Typeface;->native_instance:I

    #@23
    invoke-static {v2}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@26
    move-result v2

    #@27
    invoke-virtual {v0, v2}, Landroid/graphics/Typeface;->setStyle(I)V

    #@2a
    .line 251
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@2c
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@2e
    if-eqz v0, :cond_37

    #@30
    .line 252
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@32
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@34
    invoke-static {v0}, Landroid/graphics/Typeface;->nativeUnref(I)V

    #@37
    .line 254
    :cond_37
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@39
    invoke-static {v1, v6}, Landroid/graphics/Typeface;->nativeCreate(Ljava/lang/String;I)I

    #@3c
    move-result v2

    #@3d
    iput v2, v0, Landroid/graphics/Typeface;->native_instance:I

    #@3f
    .line 255
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@41
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@43
    iget v2, v2, Landroid/graphics/Typeface;->native_instance:I

    #@45
    invoke-static {v2}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@48
    move-result v2

    #@49
    invoke-virtual {v0, v2}, Landroid/graphics/Typeface;->setStyle(I)V

    #@4c
    .line 257
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@4e
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@50
    if-eqz v0, :cond_59

    #@52
    .line 258
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@54
    iget v0, v0, Landroid/graphics/Typeface;->native_instance:I

    #@56
    invoke-static {v0}, Landroid/graphics/Typeface;->nativeUnref(I)V

    #@59
    .line 260
    :cond_59
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@5b
    const-string/jumbo v2, "sans-serif"

    #@5e
    invoke-static {v2, v5}, Landroid/graphics/Typeface;->nativeCreate(Ljava/lang/String;I)I

    #@61
    move-result v2

    #@62
    iput v2, v0, Landroid/graphics/Typeface;->native_instance:I

    #@64
    .line 261
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@66
    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    #@68
    iget v2, v2, Landroid/graphics/Typeface;->native_instance:I

    #@6a
    invoke-static {v2}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@6d
    move-result v2

    #@6e
    invoke-virtual {v0, v2}, Landroid/graphics/Typeface;->setStyle(I)V

    #@71
    .line 263
    const/4 v0, 0x4

    #@72
    new-array v2, v0, [Landroid/graphics/Typeface;

    #@74
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    #@76
    aput-object v0, v2, v5

    #@78
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    #@7a
    aput-object v0, v2, v6

    #@7c
    move-object v0, v1

    #@7d
    check-cast v0, Ljava/lang/String;

    #@7f
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@82
    move-result-object v0

    #@83
    aput-object v0, v2, v3

    #@85
    check-cast v1, Ljava/lang/String;

    #@87
    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@8a
    move-result-object v0

    #@8b
    aput-object v0, v2, v4

    #@8d
    sput-object v2, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@8f
    .line 270
    sget-object v0, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@91
    aget-object v0, v0, v3

    #@93
    sget-object v1, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@95
    aget-object v1, v1, v3

    #@97
    iget v1, v1, Landroid/graphics/Typeface;->native_instance:I

    #@99
    invoke-static {v1}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@9c
    move-result v1

    #@9d
    invoke-virtual {v0, v1}, Landroid/graphics/Typeface;->setStyle(I)V

    #@a0
    .line 271
    sget-object v0, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@a2
    aget-object v0, v0, v4

    #@a4
    sget-object v1, Landroid/graphics/Typeface;->sDefaults:[Landroid/graphics/Typeface;

    #@a6
    aget-object v1, v1, v4

    #@a8
    iget v1, v1, Landroid/graphics/Typeface;->native_instance:I

    #@aa
    invoke-static {v1}, Landroid/graphics/Typeface;->nativeGetStyle(I)I

    #@ad
    move-result v1

    #@ae
    invoke-virtual {v0, v1}, Landroid/graphics/Typeface;->setStyle(I)V

    #@b1
    .line 272
    return-void
.end method

.method public static updateFontManager()I
    .registers 1

    #@0
    .prologue
    .line 284
    invoke-static {}, Landroid/graphics/Typeface;->nativeUpdateFontManager()I

    #@3
    move-result v0

    #@4
    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 207
    if-ne p0, p1, :cond_5

    #@4
    .line 212
    :cond_4
    :goto_4
    return v1

    #@5
    .line 208
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 210
    check-cast v0, Landroid/graphics/Typeface;

    #@16
    .line 212
    .local v0, typeface:Landroid/graphics/Typeface;
    iget v3, p0, Landroid/graphics/Typeface;->mStyle:I

    #@18
    iget v4, v0, Landroid/graphics/Typeface;->mStyle:I

    #@1a
    if-ne v3, v4, :cond_22

    #@1c
    iget v3, p0, Landroid/graphics/Typeface;->native_instance:I

    #@1e
    iget v4, v0, Landroid/graphics/Typeface;->native_instance:I

    #@20
    if-eq v3, v4, :cond_4

    #@22
    :cond_22
    move v1, v2

    #@23
    goto :goto_4
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 199
    :try_start_0
    iget v0, p0, Landroid/graphics/Typeface;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Typeface;->nativeUnref(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 201
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 203
    return-void

    #@9
    .line 201
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public getStyle()I
    .registers 2

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/graphics/Typeface;->mStyle:I

    #@2
    return v0
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 217
    iget v0, p0, Landroid/graphics/Typeface;->native_instance:I

    #@2
    .line 218
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@4
    iget v2, p0, Landroid/graphics/Typeface;->mStyle:I

    #@6
    add-int v0, v1, v2

    #@8
    .line 219
    return v0
.end method

.method public final isBold()Z
    .registers 2

    #@0
    .prologue
    .line 68
    iget v0, p0, Landroid/graphics/Typeface;->mStyle:I

    #@2
    and-int/lit8 v0, v0, 0x1

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public final isItalic()Z
    .registers 2

    #@0
    .prologue
    .line 73
    iget v0, p0, Landroid/graphics/Typeface;->mStyle:I

    #@2
    and-int/lit8 v0, v0, 0x2

    #@4
    if-eqz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public setStyle(I)V
    .registers 2
    .parameter "style"

    #@0
    .prologue
    .line 290
    iput p1, p0, Landroid/graphics/Typeface;->mStyle:I

    #@2
    return-void
.end method
