.class public Landroid/graphics/ColorMatrix;
.super Ljava/lang/Object;
.source "ColorMatrix.java"


# instance fields
.field private final mArray:[F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    const/16 v0, 0x14

    #@5
    new-array v0, v0, [F

    #@7
    iput-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@9
    .line 45
    invoke-virtual {p0}, Landroid/graphics/ColorMatrix;->reset()V

    #@c
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/graphics/ColorMatrix;)V
    .registers 6
    .parameter "src"

    #@0
    .prologue
    const/16 v3, 0x14

    #@2
    const/4 v2, 0x0

    #@3
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 38
    new-array v0, v3, [F

    #@8
    iput-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@a
    .line 59
    iget-object v0, p1, Landroid/graphics/ColorMatrix;->mArray:[F

    #@c
    iget-object v1, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@e
    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@11
    .line 60
    return-void
.end method

.method public constructor <init>([F)V
    .registers 5
    .parameter "src"

    #@0
    .prologue
    const/16 v2, 0x14

    #@2
    const/4 v1, 0x0

    #@3
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 38
    new-array v0, v2, [F

    #@8
    iput-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@a
    .line 52
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@c
    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@f
    .line 53
    return-void
.end method


# virtual methods
.method public final getArray()[F
    .registers 2

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@2
    return-object v0
.end method

.method public postConcat(Landroid/graphics/ColorMatrix;)V
    .registers 2
    .parameter "postmatrix"

    #@0
    .prologue
    .line 195
    invoke-virtual {p0, p1, p0}, Landroid/graphics/ColorMatrix;->setConcat(Landroid/graphics/ColorMatrix;Landroid/graphics/ColorMatrix;)V

    #@3
    .line 196
    return-void
.end method

.method public preConcat(Landroid/graphics/ColorMatrix;)V
    .registers 2
    .parameter "prematrix"

    #@0
    .prologue
    .line 187
    invoke-virtual {p0, p0, p1}, Landroid/graphics/ColorMatrix;->setConcat(Landroid/graphics/ColorMatrix;Landroid/graphics/ColorMatrix;)V

    #@3
    .line 188
    return-void
.end method

.method public reset()V
    .registers 8

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@2
    .line 77
    .local v0, a:[F
    const/16 v1, 0x13

    #@4
    .local v1, i:I
    :goto_4
    if-lez v1, :cond_c

    #@6
    .line 78
    const/4 v2, 0x0

    #@7
    aput v2, v0, v1

    #@9
    .line 77
    add-int/lit8 v1, v1, -0x1

    #@b
    goto :goto_4

    #@c
    .line 80
    :cond_c
    const/4 v2, 0x0

    #@d
    const/4 v3, 0x6

    #@e
    const/16 v4, 0xc

    #@10
    const/16 v5, 0x12

    #@12
    const/high16 v6, 0x3f80

    #@14
    aput v6, v0, v5

    #@16
    aput v6, v0, v4

    #@18
    aput v6, v0, v3

    #@1a
    aput v6, v0, v2

    #@1c
    .line 81
    return-void
.end method

.method public set(Landroid/graphics/ColorMatrix;)V
    .registers 6
    .parameter "src"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 87
    iget-object v0, p1, Landroid/graphics/ColorMatrix;->mArray:[F

    #@3
    iget-object v1, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@5
    const/16 v2, 0x14

    #@7
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@a
    .line 88
    return-void
.end method

.method public set([F)V
    .registers 5
    .parameter "src"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 94
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@3
    const/16 v1, 0x14

    #@5
    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8
    .line 95
    return-void
.end method

.method public setConcat(Landroid/graphics/ColorMatrix;Landroid/graphics/ColorMatrix;)V
    .registers 16
    .parameter "matA"
    .parameter "matB"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    const/4 v11, 0x0

    #@2
    const/16 v10, 0x14

    #@4
    .line 155
    const/4 v6, 0x0

    #@5
    .line 157
    .local v6, tmp:[F
    if-eq p1, p0, :cond_9

    #@7
    if-ne p2, p0, :cond_46

    #@9
    .line 158
    :cond_9
    new-array v6, v10, [F

    #@b
    .line 164
    :goto_b
    iget-object v0, p1, Landroid/graphics/ColorMatrix;->mArray:[F

    #@d
    .line 165
    .local v0, a:[F
    iget-object v1, p2, Landroid/graphics/ColorMatrix;->mArray:[F

    #@f
    .line 166
    .local v1, b:[F
    const/4 v3, 0x0

    #@10
    .line 167
    .local v3, index:I
    const/4 v5, 0x0

    #@11
    .local v5, j:I
    :goto_11
    if-ge v5, v10, :cond_7a

    #@13
    .line 168
    const/4 v2, 0x0

    #@14
    .local v2, i:I
    move v4, v3

    #@15
    .end local v3           #index:I
    .local v4, index:I
    :goto_15
    if-ge v2, v12, :cond_49

    #@17
    .line 169
    add-int/lit8 v3, v4, 0x1

    #@19
    .end local v4           #index:I
    .restart local v3       #index:I
    add-int/lit8 v7, v5, 0x0

    #@1b
    aget v7, v0, v7

    #@1d
    add-int/lit8 v8, v2, 0x0

    #@1f
    aget v8, v1, v8

    #@21
    mul-float/2addr v7, v8

    #@22
    add-int/lit8 v8, v5, 0x1

    #@24
    aget v8, v0, v8

    #@26
    add-int/lit8 v9, v2, 0x5

    #@28
    aget v9, v1, v9

    #@2a
    mul-float/2addr v8, v9

    #@2b
    add-float/2addr v7, v8

    #@2c
    add-int/lit8 v8, v5, 0x2

    #@2e
    aget v8, v0, v8

    #@30
    add-int/lit8 v9, v2, 0xa

    #@32
    aget v9, v1, v9

    #@34
    mul-float/2addr v8, v9

    #@35
    add-float/2addr v7, v8

    #@36
    add-int/lit8 v8, v5, 0x3

    #@38
    aget v8, v0, v8

    #@3a
    add-int/lit8 v9, v2, 0xf

    #@3c
    aget v9, v1, v9

    #@3e
    mul-float/2addr v8, v9

    #@3f
    add-float/2addr v7, v8

    #@40
    aput v7, v6, v4

    #@42
    .line 168
    add-int/lit8 v2, v2, 0x1

    #@44
    move v4, v3

    #@45
    .end local v3           #index:I
    .restart local v4       #index:I
    goto :goto_15

    #@46
    .line 161
    .end local v0           #a:[F
    .end local v1           #b:[F
    .end local v2           #i:I
    .end local v4           #index:I
    .end local v5           #j:I
    :cond_46
    iget-object v6, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@48
    goto :goto_b

    #@49
    .line 172
    .restart local v0       #a:[F
    .restart local v1       #b:[F
    .restart local v2       #i:I
    .restart local v4       #index:I
    .restart local v5       #j:I
    :cond_49
    add-int/lit8 v3, v4, 0x1

    #@4b
    .end local v4           #index:I
    .restart local v3       #index:I
    add-int/lit8 v7, v5, 0x0

    #@4d
    aget v7, v0, v7

    #@4f
    aget v8, v1, v12

    #@51
    mul-float/2addr v7, v8

    #@52
    add-int/lit8 v8, v5, 0x1

    #@54
    aget v8, v0, v8

    #@56
    const/16 v9, 0x9

    #@58
    aget v9, v1, v9

    #@5a
    mul-float/2addr v8, v9

    #@5b
    add-float/2addr v7, v8

    #@5c
    add-int/lit8 v8, v5, 0x2

    #@5e
    aget v8, v0, v8

    #@60
    const/16 v9, 0xe

    #@62
    aget v9, v1, v9

    #@64
    mul-float/2addr v8, v9

    #@65
    add-float/2addr v7, v8

    #@66
    add-int/lit8 v8, v5, 0x3

    #@68
    aget v8, v0, v8

    #@6a
    const/16 v9, 0x13

    #@6c
    aget v9, v1, v9

    #@6e
    mul-float/2addr v8, v9

    #@6f
    add-float/2addr v7, v8

    #@70
    add-int/lit8 v8, v5, 0x4

    #@72
    aget v8, v0, v8

    #@74
    add-float/2addr v7, v8

    #@75
    aput v7, v6, v4

    #@77
    .line 167
    add-int/lit8 v5, v5, 0x5

    #@79
    goto :goto_11

    #@7a
    .line 177
    .end local v2           #i:I
    :cond_7a
    iget-object v7, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@7c
    if-eq v6, v7, :cond_83

    #@7e
    .line 178
    iget-object v7, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@80
    invoke-static {v6, v11, v7, v11, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@83
    .line 180
    :cond_83
    return-void
.end method

.method public setRGB2YUV()V
    .registers 5

    #@0
    .prologue
    const/high16 v3, 0x3f00

    #@2
    .line 222
    invoke-virtual {p0}, Landroid/graphics/ColorMatrix;->reset()V

    #@5
    .line 223
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@7
    .line 225
    .local v0, m:[F
    const/4 v1, 0x0

    #@8
    const v2, 0x3e991687

    #@b
    aput v2, v0, v1

    #@d
    const/4 v1, 0x1

    #@e
    const v2, 0x3f1645a2

    #@11
    aput v2, v0, v1

    #@13
    const/4 v1, 0x2

    #@14
    const v2, 0x3de978d5

    #@17
    aput v2, v0, v1

    #@19
    .line 226
    const/4 v1, 0x5

    #@1a
    const v2, -0x41d335d2

    #@1d
    aput v2, v0, v1

    #@1f
    const/4 v1, 0x6

    #@20
    const v2, -0x41566517

    #@23
    aput v2, v0, v1

    #@25
    const/4 v1, 0x7

    #@26
    aput v3, v0, v1

    #@28
    .line 227
    const/16 v1, 0xa

    #@2a
    aput v3, v0, v1

    #@2c
    const/16 v1, 0xb

    #@2e
    const v2, -0x4129a177

    #@31
    aput v2, v0, v1

    #@33
    const/16 v1, 0xc

    #@35
    const v2, -0x42597a25

    #@38
    aput v2, v0, v1

    #@3a
    .line 228
    return-void
.end method

.method public setRotate(IF)V
    .registers 11
    .parameter "axis"
    .parameter "degrees"

    #@0
    .prologue
    const/16 v7, 0xc

    #@2
    const/4 v6, 0x6

    #@3
    const/4 v5, 0x0

    #@4
    .line 120
    invoke-virtual {p0}, Landroid/graphics/ColorMatrix;->reset()V

    #@7
    .line 121
    const v3, 0x40490fdb

    #@a
    mul-float/2addr v3, p2

    #@b
    const/high16 v4, 0x4334

    #@d
    div-float v1, v3, v4

    #@f
    .line 122
    .local v1, radians:F
    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    #@12
    move-result v0

    #@13
    .line 123
    .local v0, cosine:F
    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    #@16
    move-result v2

    #@17
    .line 124
    .local v2, sine:F
    packed-switch p1, :pswitch_data_5e

    #@1a
    .line 144
    new-instance v3, Ljava/lang/RuntimeException;

    #@1c
    invoke-direct {v3}, Ljava/lang/RuntimeException;-><init>()V

    #@1f
    throw v3

    #@20
    .line 127
    :pswitch_20
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@22
    iget-object v4, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@24
    aput v0, v4, v7

    #@26
    aput v0, v3, v6

    #@28
    .line 128
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@2a
    const/4 v4, 0x7

    #@2b
    aput v2, v3, v4

    #@2d
    .line 129
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@2f
    const/16 v4, 0xb

    #@31
    neg-float v5, v2

    #@32
    aput v5, v3, v4

    #@34
    .line 146
    :goto_34
    return-void

    #@35
    .line 133
    :pswitch_35
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@37
    iget-object v4, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@39
    aput v0, v4, v7

    #@3b
    aput v0, v3, v5

    #@3d
    .line 134
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@3f
    const/4 v4, 0x2

    #@40
    neg-float v5, v2

    #@41
    aput v5, v3, v4

    #@43
    .line 135
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@45
    const/16 v4, 0xa

    #@47
    aput v2, v3, v4

    #@49
    goto :goto_34

    #@4a
    .line 139
    :pswitch_4a
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@4c
    iget-object v4, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@4e
    aput v0, v4, v6

    #@50
    aput v0, v3, v5

    #@52
    .line 140
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@54
    const/4 v4, 0x1

    #@55
    aput v2, v3, v4

    #@57
    .line 141
    iget-object v3, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@59
    const/4 v4, 0x5

    #@5a
    neg-float v5, v2

    #@5b
    aput v5, v3, v4

    #@5d
    goto :goto_34

    #@5e
    .line 124
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_20
        :pswitch_35
        :pswitch_4a
    .end packed-switch
.end method

.method public setSaturation(F)V
    .registers 9
    .parameter "sat"

    #@0
    .prologue
    .line 205
    invoke-virtual {p0}, Landroid/graphics/ColorMatrix;->reset()V

    #@3
    .line 206
    iget-object v4, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@5
    .line 208
    .local v4, m:[F
    const/high16 v5, 0x3f80

    #@7
    sub-float v3, v5, p1

    #@9
    .line 209
    .local v3, invSat:F
    const v5, 0x3e5a1cac

    #@c
    mul-float v2, v5, v3

    #@e
    .line 210
    .local v2, R:F
    const v5, 0x3f370a3d

    #@11
    mul-float v1, v5, v3

    #@13
    .line 211
    .local v1, G:F
    const v5, 0x3d9374bc

    #@16
    mul-float v0, v5, v3

    #@18
    .line 213
    .local v0, B:F
    const/4 v5, 0x0

    #@19
    add-float v6, v2, p1

    #@1b
    aput v6, v4, v5

    #@1d
    const/4 v5, 0x1

    #@1e
    aput v1, v4, v5

    #@20
    const/4 v5, 0x2

    #@21
    aput v0, v4, v5

    #@23
    .line 214
    const/4 v5, 0x5

    #@24
    aput v2, v4, v5

    #@26
    const/4 v5, 0x6

    #@27
    add-float v6, v1, p1

    #@29
    aput v6, v4, v5

    #@2b
    const/4 v5, 0x7

    #@2c
    aput v0, v4, v5

    #@2e
    .line 215
    const/16 v5, 0xa

    #@30
    aput v2, v4, v5

    #@32
    const/16 v5, 0xb

    #@34
    aput v1, v4, v5

    #@36
    const/16 v5, 0xc

    #@38
    add-float v6, v0, p1

    #@3a
    aput v6, v4, v5

    #@3c
    .line 216
    return-void
.end method

.method public setScale(FFFF)V
    .registers 8
    .parameter "rScale"
    .parameter "gScale"
    .parameter "bScale"
    .parameter "aScale"

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@2
    .line 104
    .local v0, a:[F
    const/16 v1, 0x13

    #@4
    .local v1, i:I
    :goto_4
    if-lez v1, :cond_c

    #@6
    .line 105
    const/4 v2, 0x0

    #@7
    aput v2, v0, v1

    #@9
    .line 104
    add-int/lit8 v1, v1, -0x1

    #@b
    goto :goto_4

    #@c
    .line 107
    :cond_c
    const/4 v2, 0x0

    #@d
    aput p1, v0, v2

    #@f
    .line 108
    const/4 v2, 0x6

    #@10
    aput p2, v0, v2

    #@12
    .line 109
    const/16 v2, 0xc

    #@14
    aput p3, v0, v2

    #@16
    .line 110
    const/16 v2, 0x12

    #@18
    aput p4, v0, v2

    #@1a
    .line 111
    return-void
.end method

.method public setYUV2RGB()V
    .registers 5

    #@0
    .prologue
    const/high16 v3, 0x3f80

    #@2
    .line 234
    invoke-virtual {p0}, Landroid/graphics/ColorMatrix;->reset()V

    #@5
    .line 235
    iget-object v0, p0, Landroid/graphics/ColorMatrix;->mArray:[F

    #@7
    .line 237
    .local v0, m:[F
    const/4 v1, 0x2

    #@8
    const v2, 0x3fb374bc

    #@b
    aput v2, v0, v1

    #@d
    .line 238
    const/4 v1, 0x5

    #@e
    aput v3, v0, v1

    #@10
    const/4 v1, 0x6

    #@11
    const v2, -0x414fcce2

    #@14
    aput v2, v0, v1

    #@16
    const/4 v1, 0x7

    #@17
    const v2, -0x40c92e1f

    #@1a
    aput v2, v0, v1

    #@1c
    .line 239
    const/16 v1, 0xa

    #@1e
    aput v3, v0, v1

    #@20
    const/16 v1, 0xb

    #@22
    const v2, 0x3fe2d0e5

    #@25
    aput v2, v0, v1

    #@27
    const/16 v1, 0xc

    #@29
    const/4 v2, 0x0

    #@2a
    aput v2, v0, v1

    #@2c
    .line 240
    return-void
.end method
