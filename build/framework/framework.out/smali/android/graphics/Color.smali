.class public Landroid/graphics/Color;
.super Ljava/lang/Object;
.source "Color.java"


# static fields
.field public static final BLACK:I = -0x1000000

.field public static final BLUE:I = -0xffff01

.field public static final CYAN:I = -0xff0001

.field public static final DKGRAY:I = -0xbbbbbc

.field public static final GRAY:I = -0x777778

.field public static final GREEN:I = -0xff0100

.field public static final LTGRAY:I = -0x333334

.field public static final MAGENTA:I = -0xff01

.field public static final RED:I = -0x10000

.field public static final TRANSPARENT:I = 0x0

.field public static final WHITE:I = -0x1

.field public static final YELLOW:I = -0x100

.field private static final sColorNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 372
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@7
    .line 373
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@9
    const-string v1, "black"

    #@b
    const/high16 v2, -0x100

    #@d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 374
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@16
    const-string v1, "darkgray"

    #@18
    const v2, -0xbbbbbc

    #@1b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22
    .line 375
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@24
    const-string v1, "gray"

    #@26
    const v2, -0x777778

    #@29
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 376
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@32
    const-string/jumbo v1, "lightgray"

    #@35
    const v2, -0x333334

    #@38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 377
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@41
    const-string/jumbo v1, "white"

    #@44
    const/4 v2, -0x1

    #@45
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4c
    .line 378
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@4e
    const-string/jumbo v1, "red"

    #@51
    const/high16 v2, -0x1

    #@53
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    .line 379
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@5c
    const-string v1, "green"

    #@5e
    const v2, -0xff0100

    #@61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@68
    .line 380
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@6a
    const-string v1, "blue"

    #@6c
    const v2, -0xffff01

    #@6f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@72
    move-result-object v2

    #@73
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@76
    .line 381
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@78
    const-string/jumbo v1, "yellow"

    #@7b
    const/16 v2, -0x100

    #@7d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@84
    .line 382
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@86
    const-string v1, "cyan"

    #@88
    const v2, -0xff0001

    #@8b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v2

    #@8f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@92
    .line 383
    sget-object v0, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@94
    const-string/jumbo v1, "magenta"

    #@97
    const v2, -0xff01

    #@9a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a1
    .line 384
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static HSBtoColor(FFF)I
    .registers 16
    .parameter "h"
    .parameter "s"
    .parameter "b"

    #@0
    .prologue
    const/high16 v12, 0x437f

    #@2
    const/4 v9, 0x0

    #@3
    const/high16 v11, 0x3f80

    #@5
    .line 254
    invoke-static {p0, v9, v11}, Landroid/util/MathUtils;->constrain(FFF)F

    #@8
    move-result p0

    #@9
    .line 255
    invoke-static {p1, v9, v11}, Landroid/util/MathUtils;->constrain(FFF)F

    #@c
    move-result p1

    #@d
    .line 256
    invoke-static {p2, v9, v11}, Landroid/util/MathUtils;->constrain(FFF)F

    #@10
    move-result p2

    #@11
    .line 258
    const/4 v7, 0x0

    #@12
    .line 259
    .local v7, red:F
    const/4 v2, 0x0

    #@13
    .line 260
    .local v2, green:F
    const/4 v0, 0x0

    #@14
    .line 262
    .local v0, blue:F
    float-to-int v9, p0

    #@15
    int-to-float v9, v9

    #@16
    sub-float v9, p0, v9

    #@18
    const/high16 v10, 0x40c0

    #@1a
    mul-float v3, v9, v10

    #@1c
    .line 263
    .local v3, hf:F
    float-to-int v4, v3

    #@1d
    .line 264
    .local v4, ihf:I
    int-to-float v9, v4

    #@1e
    sub-float v1, v3, v9

    #@20
    .line 265
    .local v1, f:F
    sub-float v9, v11, p1

    #@22
    mul-float v5, p2, v9

    #@24
    .line 266
    .local v5, pv:F
    mul-float v9, p1, v1

    #@26
    sub-float v9, v11, v9

    #@28
    mul-float v6, p2, v9

    #@2a
    .line 267
    .local v6, qv:F
    sub-float v9, v11, v1

    #@2c
    mul-float/2addr v9, p1

    #@2d
    sub-float v9, v11, v9

    #@2f
    mul-float v8, p2, v9

    #@31
    .line 269
    .local v8, tv:F
    packed-switch v4, :pswitch_data_60

    #@34
    .line 302
    :goto_34
    const/high16 v9, -0x100

    #@36
    mul-float v10, v7, v12

    #@38
    float-to-int v10, v10

    #@39
    shl-int/lit8 v10, v10, 0x10

    #@3b
    or-int/2addr v9, v10

    #@3c
    mul-float v10, v2, v12

    #@3e
    float-to-int v10, v10

    #@3f
    shl-int/lit8 v10, v10, 0x8

    #@41
    or-int/2addr v9, v10

    #@42
    mul-float v10, v0, v12

    #@44
    float-to-int v10, v10

    #@45
    or-int/2addr v9, v10

    #@46
    return v9

    #@47
    .line 271
    :pswitch_47
    move v7, p2

    #@48
    .line 272
    move v2, v8

    #@49
    .line 273
    move v0, v5

    #@4a
    .line 274
    goto :goto_34

    #@4b
    .line 276
    :pswitch_4b
    move v7, v6

    #@4c
    .line 277
    move v2, p2

    #@4d
    .line 278
    move v0, v5

    #@4e
    .line 279
    goto :goto_34

    #@4f
    .line 281
    :pswitch_4f
    move v7, v5

    #@50
    .line 282
    move v2, p2

    #@51
    .line 283
    move v0, v8

    #@52
    .line 284
    goto :goto_34

    #@53
    .line 286
    :pswitch_53
    move v7, v5

    #@54
    .line 287
    move v2, v6

    #@55
    .line 288
    move v0, p2

    #@56
    .line 289
    goto :goto_34

    #@57
    .line 291
    :pswitch_57
    move v7, v8

    #@58
    .line 292
    move v2, v5

    #@59
    .line 293
    move v0, p2

    #@5a
    .line 294
    goto :goto_34

    #@5b
    .line 296
    :pswitch_5b
    move v7, p2

    #@5c
    .line 297
    move v2, v5

    #@5d
    .line 298
    move v0, v6

    #@5e
    goto :goto_34

    #@5f
    .line 269
    nop

    #@60
    :pswitch_data_60
    .packed-switch 0x0
        :pswitch_47
        :pswitch_4b
        :pswitch_4f
        :pswitch_53
        :pswitch_57
        :pswitch_5b
    .end packed-switch
.end method

.method public static HSBtoColor([F)I
    .registers 4
    .parameter "hsb"

    #@0
    .prologue
    .line 237
    const/4 v0, 0x0

    #@1
    aget v0, p0, v0

    #@3
    const/4 v1, 0x1

    #@4
    aget v1, p0, v1

    #@6
    const/4 v2, 0x2

    #@7
    aget v2, p0, v2

    #@9
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->HSBtoColor(FFF)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public static HSVToColor(I[F)I
    .registers 4
    .parameter "alpha"
    .parameter "hsv"

    #@0
    .prologue
    .line 360
    array-length v0, p1

    #@1
    const/4 v1, 0x3

    #@2
    if-ge v0, v1, :cond_c

    #@4
    .line 361
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "3 components required for hsv"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 363
    :cond_c
    invoke-static {p0, p1}, Landroid/graphics/Color;->nativeHSVToColor(I[F)I

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public static HSVToColor([F)I
    .registers 2
    .parameter "hsv"

    #@0
    .prologue
    .line 345
    const/16 v0, 0xff

    #@2
    invoke-static {v0, p0}, Landroid/graphics/Color;->HSVToColor(I[F)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public static RGBToHSV(III[F)V
    .registers 6
    .parameter "red"
    .parameter "green"
    .parameter "blue"
    .parameter "hsv"

    #@0
    .prologue
    .line 317
    array-length v0, p3

    #@1
    const/4 v1, 0x3

    #@2
    if-ge v0, v1, :cond_c

    #@4
    .line 318
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "3 components required for hsv"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 320
    :cond_c
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/Color;->nativeRGBToHSV(III[F)V

    #@f
    .line 321
    return-void
.end method

.method public static alpha(I)I
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 55
    ushr-int/lit8 v0, p0, 0x18

    #@2
    return v0
.end method

.method public static argb(IIII)I
    .registers 6
    .parameter "alpha"
    .parameter "red"
    .parameter "green"
    .parameter "blue"

    #@0
    .prologue
    .line 107
    shl-int/lit8 v0, p0, 0x18

    #@2
    shl-int/lit8 v1, p1, 0x10

    #@4
    or-int/2addr v0, v1

    #@5
    shl-int/lit8 v1, p2, 0x8

    #@7
    or-int/2addr v0, v1

    #@8
    or-int/2addr v0, p3

    #@9
    return v0
.end method

.method public static blue(I)I
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 79
    and-int/lit16 v0, p0, 0xff

    #@2
    return v0
.end method

.method public static brightness(I)F
    .registers 7
    .parameter "color"

    #@0
    .prologue
    .line 187
    shr-int/lit8 v4, p0, 0x10

    #@2
    and-int/lit16 v3, v4, 0xff

    #@4
    .line 188
    .local v3, r:I
    shr-int/lit8 v4, p0, 0x8

    #@6
    and-int/lit16 v2, v4, 0xff

    #@8
    .line 189
    .local v2, g:I
    and-int/lit16 v1, p0, 0xff

    #@a
    .line 191
    .local v1, b:I
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v4

    #@e
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v0

    #@12
    .line 193
    .local v0, V:I
    int-to-float v4, v0

    #@13
    const/high16 v5, 0x437f

    #@15
    div-float/2addr v4, v5

    #@16
    return v4
.end method

.method public static colorToHSV(I[F)V
    .registers 5
    .parameter "color"
    .parameter "hsv"

    #@0
    .prologue
    .line 332
    shr-int/lit8 v0, p0, 0x10

    #@2
    and-int/lit16 v0, v0, 0xff

    #@4
    shr-int/lit8 v1, p0, 0x8

    #@6
    and-int/lit16 v1, v1, 0xff

    #@8
    and-int/lit16 v2, p0, 0xff

    #@a
    invoke-static {v0, v1, v2, p1}, Landroid/graphics/Color;->RGBToHSV(III[F)V

    #@d
    .line 333
    return-void
.end method

.method public static green(I)I
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 71
    shr-int/lit8 v0, p0, 0x8

    #@2
    and-int/lit16 v0, v0, 0xff

    #@4
    return v0
.end method

.method public static hue(I)F
    .registers 12
    .parameter "color"

    #@0
    .prologue
    .line 118
    shr-int/lit8 v10, p0, 0x10

    #@2
    and-int/lit16 v7, v10, 0xff

    #@4
    .line 119
    .local v7, r:I
    shr-int/lit8 v10, p0, 0x8

    #@6
    and-int/lit16 v6, v10, 0xff

    #@8
    .line 120
    .local v6, g:I
    and-int/lit16 v2, p0, 0xff

    #@a
    .line 122
    .local v2, b:I
    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v10

    #@e
    invoke-static {v2, v10}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v1

    #@12
    .line 123
    .local v1, V:I
    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v10

    #@16
    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v8

    #@1a
    .line 127
    .local v8, temp:I
    if-ne v1, v8, :cond_1e

    #@1c
    .line 128
    const/4 v0, 0x0

    #@1d
    .line 149
    .local v0, H:F
    :cond_1d
    :goto_1d
    return v0

    #@1e
    .line 130
    .end local v0           #H:F
    :cond_1e
    sub-int v10, v1, v8

    #@20
    int-to-float v9, v10

    #@21
    .line 131
    .local v9, vtemp:F
    sub-int v10, v1, v7

    #@23
    int-to-float v10, v10

    #@24
    div-float v5, v10, v9

    #@26
    .line 132
    .local v5, cr:F
    sub-int v10, v1, v6

    #@28
    int-to-float v10, v10

    #@29
    div-float v4, v10, v9

    #@2b
    .line 133
    .local v4, cg:F
    sub-int v10, v1, v2

    #@2d
    int-to-float v10, v10

    #@2e
    div-float v3, v10, v9

    #@30
    .line 135
    .local v3, cb:F
    if-ne v7, v1, :cond_40

    #@32
    .line 136
    sub-float v0, v3, v4

    #@34
    .line 143
    .restart local v0       #H:F
    :goto_34
    const/high16 v10, 0x40c0

    #@36
    div-float/2addr v0, v10

    #@37
    .line 144
    const/4 v10, 0x0

    #@38
    cmpg-float v10, v0, v10

    #@3a
    if-gez v10, :cond_1d

    #@3c
    .line 145
    const/high16 v10, 0x3f80

    #@3e
    add-float/2addr v0, v10

    #@3f
    goto :goto_1d

    #@40
    .line 137
    .end local v0           #H:F
    :cond_40
    if-ne v6, v1, :cond_48

    #@42
    .line 138
    const/high16 v10, 0x4000

    #@44
    add-float/2addr v10, v5

    #@45
    sub-float v0, v10, v3

    #@47
    .restart local v0       #H:F
    goto :goto_34

    #@48
    .line 140
    .end local v0           #H:F
    :cond_48
    const/high16 v10, 0x4080

    #@4a
    add-float/2addr v10, v4

    #@4b
    sub-float v0, v10, v5

    #@4d
    .restart local v0       #H:F
    goto :goto_34
.end method

.method private static native nativeHSVToColor(I[F)I
.end method

.method private static native nativeRGBToHSV(III[F)V
.end method

.method public static parseColor(Ljava/lang/String;)I
    .registers 5
    .parameter "colorString"

    #@0
    .prologue
    .line 206
    const/4 v2, 0x0

    #@1
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    #@4
    move-result v2

    #@5
    const/16 v3, 0x23

    #@7
    if-ne v2, v3, :cond_31

    #@9
    .line 208
    const/4 v2, 0x1

    #@a
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    const/16 v3, 0x10

    #@10
    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    #@13
    move-result-wide v0

    #@14
    .line 209
    .local v0, color:J
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@17
    move-result v2

    #@18
    const/4 v3, 0x7

    #@19
    if-ne v2, v3, :cond_21

    #@1b
    .line 211
    const-wide/32 v2, -0x1000000

    #@1e
    or-long/2addr v0, v2

    #@1f
    .line 215
    :cond_1f
    long-to-int v2, v0

    #@20
    .line 219
    .end local v0           #color:J
    :goto_20
    return v2

    #@21
    .line 212
    .restart local v0       #color:J
    :cond_21
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@24
    move-result v2

    #@25
    const/16 v3, 0x9

    #@27
    if-eq v2, v3, :cond_1f

    #@29
    .line 213
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@2b
    const-string v3, "Unknown color"

    #@2d
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@30
    throw v2

    #@31
    .line 217
    .end local v0           #color:J
    :cond_31
    sget-object v2, Landroid/graphics/Color;->sColorNameMap:Ljava/util/HashMap;

    #@33
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@35
    invoke-virtual {p0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3c
    move-result-object v0

    #@3d
    check-cast v0, Ljava/lang/Integer;

    #@3f
    .line 218
    .local v0, color:Ljava/lang/Integer;
    if-eqz v0, :cond_46

    #@41
    .line 219
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@44
    move-result v2

    #@45
    goto :goto_20

    #@46
    .line 222
    :cond_46
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@48
    const-string v3, "Unknown color"

    #@4a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@4d
    throw v2
.end method

.method public static red(I)I
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 63
    shr-int/lit8 v0, p0, 0x10

    #@2
    and-int/lit16 v0, v0, 0xff

    #@4
    return v0
.end method

.method public static rgb(III)I
    .registers 5
    .parameter "red"
    .parameter "green"
    .parameter "blue"

    #@0
    .prologue
    .line 93
    const/high16 v0, -0x100

    #@2
    shl-int/lit8 v1, p0, 0x10

    #@4
    or-int/2addr v0, v1

    #@5
    shl-int/lit8 v1, p1, 0x8

    #@7
    or-int/2addr v0, v1

    #@8
    or-int/2addr v0, p2

    #@9
    return v0
.end method

.method public static saturation(I)F
    .registers 9
    .parameter "color"

    #@0
    .prologue
    .line 160
    shr-int/lit8 v6, p0, 0x10

    #@2
    and-int/lit16 v4, v6, 0xff

    #@4
    .line 161
    .local v4, r:I
    shr-int/lit8 v6, p0, 0x8

    #@6
    and-int/lit16 v3, v6, 0xff

    #@8
    .line 162
    .local v3, g:I
    and-int/lit16 v2, p0, 0xff

    #@a
    .line 165
    .local v2, b:I
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    #@d
    move-result v6

    #@e
    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    #@11
    move-result v1

    #@12
    .line 166
    .local v1, V:I
    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    #@15
    move-result v6

    #@16
    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    #@19
    move-result v5

    #@1a
    .line 170
    .local v5, temp:I
    if-ne v1, v5, :cond_1e

    #@1c
    .line 171
    const/4 v0, 0x0

    #@1d
    .line 176
    .local v0, S:F
    :goto_1d
    return v0

    #@1e
    .line 173
    .end local v0           #S:F
    :cond_1e
    sub-int v6, v1, v5

    #@20
    int-to-float v6, v6

    #@21
    int-to-float v7, v1

    #@22
    div-float v0, v6, v7

    #@24
    .restart local v0       #S:F
    goto :goto_1d
.end method
