.class public Landroid/graphics/PorterDuffXfermode;
.super Landroid/graphics/Xfermode;
.source "PorterDuffXfermode.java"


# instance fields
.field public final mode:Landroid/graphics/PorterDuff$Mode;


# direct methods
.method public constructor <init>(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 30
    invoke-direct {p0}, Landroid/graphics/Xfermode;-><init>()V

    #@3
    .line 31
    iput-object p1, p0, Landroid/graphics/PorterDuffXfermode;->mode:Landroid/graphics/PorterDuff$Mode;

    #@5
    .line 32
    iget v0, p1, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@7
    invoke-static {v0}, Landroid/graphics/PorterDuffXfermode;->nativeCreateXfermode(I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/Xfermode;->native_instance:I

    #@d
    .line 33
    return-void
.end method

.method private static native nativeCreateXfermode(I)I
.end method
