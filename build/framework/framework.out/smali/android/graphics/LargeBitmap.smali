.class public final Landroid/graphics/LargeBitmap;
.super Ljava/lang/Object;
.source "LargeBitmap.java"


# instance fields
.field private mNativeLargeBitmap:I

.field private mRecycled:Z


# direct methods
.method private constructor <init>(I)V
    .registers 3
    .parameter "lbm"

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 49
    iput p1, p0, Landroid/graphics/LargeBitmap;->mNativeLargeBitmap:I

    #@5
    .line 50
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Landroid/graphics/LargeBitmap;->mRecycled:Z

    #@8
    .line 51
    return-void
.end method

.method private checkRecycled(Ljava/lang/String;)V
    .registers 3
    .parameter "errorMessage"

    #@0
    .prologue
    .line 113
    iget-boolean v0, p0, Landroid/graphics/LargeBitmap;->mRecycled:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 114
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 116
    :cond_a
    return-void
.end method

.method private static native nativeClean(I)V
.end method

.method private static native nativeDecodeRegion(IIIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeGetHeight(I)I
.end method

.method private static native nativeGetWidth(I)I
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 9
    .parameter "rect"
    .parameter "options"

    #@0
    .prologue
    .line 63
    const-string v0, "decodeRegion called on recycled large bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/LargeBitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 64
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@7
    if-ltz v0, :cond_1d

    #@9
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@b
    if-ltz v0, :cond_1d

    #@d
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@f
    invoke-virtual {p0}, Landroid/graphics/LargeBitmap;->getWidth()I

    #@12
    move-result v1

    #@13
    if-gt v0, v1, :cond_1d

    #@15
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@17
    invoke-virtual {p0}, Landroid/graphics/LargeBitmap;->getHeight()I

    #@1a
    move-result v1

    #@1b
    if-le v0, v1, :cond_26

    #@1d
    .line 65
    :cond_1d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string/jumbo v1, "rectangle is not inside the image"

    #@22
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@25
    throw v0

    #@26
    .line 66
    :cond_26
    iget v0, p0, Landroid/graphics/LargeBitmap;->mNativeLargeBitmap:I

    #@28
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@2a
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@2c
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@2e
    iget v4, p1, Landroid/graphics/Rect;->left:I

    #@30
    sub-int/2addr v3, v4

    #@31
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@33
    iget v5, p1, Landroid/graphics/Rect;->top:I

    #@35
    sub-int/2addr v4, v5

    #@36
    move-object v5, p2

    #@37
    invoke-static/range {v0 .. v5}, Landroid/graphics/LargeBitmap;->nativeDecodeRegion(IIIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@3a
    move-result-object v0

    #@3b
    return-object v0
.end method

.method protected finalize()V
    .registers 1

    #@0
    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/graphics/LargeBitmap;->recycle()V

    #@3
    .line 120
    return-void
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 78
    const-string v0, "getHeight called on recycled large bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/LargeBitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 79
    iget v0, p0, Landroid/graphics/LargeBitmap;->mNativeLargeBitmap:I

    #@7
    invoke-static {v0}, Landroid/graphics/LargeBitmap;->nativeGetHeight(I)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 72
    const-string v0, "getWidth called on recycled large bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/LargeBitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 73
    iget v0, p0, Landroid/graphics/LargeBitmap;->mNativeLargeBitmap:I

    #@7
    invoke-static {v0}, Landroid/graphics/LargeBitmap;->nativeGetWidth(I)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public final isRecycled()Z
    .registers 2

    #@0
    .prologue
    .line 105
    iget-boolean v0, p0, Landroid/graphics/LargeBitmap;->mRecycled:Z

    #@2
    return v0
.end method

.method public recycle()V
    .registers 2

    #@0
    .prologue
    .line 92
    iget-boolean v0, p0, Landroid/graphics/LargeBitmap;->mRecycled:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 93
    iget v0, p0, Landroid/graphics/LargeBitmap;->mNativeLargeBitmap:I

    #@6
    invoke-static {v0}, Landroid/graphics/LargeBitmap;->nativeClean(I)V

    #@9
    .line 94
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Landroid/graphics/LargeBitmap;->mRecycled:Z

    #@c
    .line 96
    :cond_c
    return-void
.end method
