.class public Landroid/graphics/Movie;
.super Ljava/lang/Object;
.source "Movie.java"


# instance fields
.field private final mNativeMovie:I


# direct methods
.method private constructor <init>(I)V
    .registers 4
    .parameter "nativeMovie"

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 26
    if-nez p1, :cond_e

    #@5
    .line 27
    new-instance v0, Ljava/lang/RuntimeException;

    #@7
    const-string/jumbo v1, "native movie creation failed"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 29
    :cond_e
    iput p1, p0, Landroid/graphics/Movie;->mNativeMovie:I

    #@10
    .line 30
    return-void
.end method

.method public static native decodeByteArray([BII)Landroid/graphics/Movie;
.end method

.method public static decodeFile(Ljava/lang/String;)Landroid/graphics/Movie;
    .registers 4
    .parameter "pathName"

    #@0
    .prologue
    .line 54
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    #@2
    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_5} :catch_a

    #@5
    .line 59
    .local v1, is:Ljava/io/InputStream;
    invoke-static {v1}, Landroid/graphics/Movie;->decodeTempStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    #@8
    move-result-object v2

    #@9
    .end local v1           #is:Ljava/io/InputStream;
    :goto_9
    return-object v2

    #@a
    .line 56
    :catch_a
    move-exception v0

    #@b
    .line 57
    .local v0, e:Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    #@c
    goto :goto_9
.end method

.method public static native decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;
.end method

.method private static decodeTempStream(Ljava/io/InputStream;)Landroid/graphics/Movie;
    .registers 3
    .parameter "is"

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    .line 74
    .local v0, moov:Landroid/graphics/Movie;
    :try_start_1
    invoke-static {p0}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    #@4
    move-result-object v0

    #@5
    .line 75
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_8} :catch_9

    #@8
    .line 83
    :goto_8
    return-object v0

    #@9
    .line 77
    :catch_9
    move-exception v1

    #@a
    goto :goto_8
.end method

.method private static native nativeDestructor(I)V
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;FF)V
    .registers 5
    .parameter "canvas"
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V

    #@4
    .line 43
    return-void
.end method

.method public native draw(Landroid/graphics/Canvas;FFLandroid/graphics/Paint;)V
.end method

.method public native duration()I
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 65
    :try_start_0
    iget v0, p0, Landroid/graphics/Movie;->mNativeMovie:I

    #@2
    invoke-static {v0}, Landroid/graphics/Movie;->nativeDestructor(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 67
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 69
    return-void

    #@9
    .line 67
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public native height()I
.end method

.method public native isOpaque()Z
.end method

.method public native setTime(I)Z
.end method

.method public native width()I
.end method
