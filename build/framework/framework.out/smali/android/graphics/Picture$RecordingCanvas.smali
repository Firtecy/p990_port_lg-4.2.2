.class Landroid/graphics/Picture$RecordingCanvas;
.super Landroid/graphics/Canvas;
.source "Picture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/Picture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecordingCanvas"
.end annotation


# instance fields
.field private final mPicture:Landroid/graphics/Picture;


# direct methods
.method public constructor <init>(Landroid/graphics/Picture;I)V
    .registers 3
    .parameter "pict"
    .parameter "nativeCanvas"

    #@0
    .prologue
    .line 175
    invoke-direct {p0, p2}, Landroid/graphics/Canvas;-><init>(I)V

    #@3
    .line 176
    iput-object p1, p0, Landroid/graphics/Picture$RecordingCanvas;->mPicture:Landroid/graphics/Picture;

    #@5
    .line 177
    return-void
.end method


# virtual methods
.method public drawPicture(Landroid/graphics/Picture;)V
    .registers 4
    .parameter "picture"

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Landroid/graphics/Picture$RecordingCanvas;->mPicture:Landroid/graphics/Picture;

    #@2
    if-ne v0, p1, :cond_c

    #@4
    .line 188
    new-instance v0, Ljava/lang/RuntimeException;

    #@6
    const-string v1, "Cannot draw a picture into its recording canvas"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 191
    :cond_c
    invoke-super {p0, p1}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@f
    .line 192
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    .line 181
    new-instance v0, Ljava/lang/RuntimeException;

    #@2
    const-string v1, "Cannot call setBitmap on a picture canvas"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
