.class public Landroid/graphics/drawable/AnimatedRotateDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "AnimatedRotateDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements Ljava/lang/Runnable;
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/AnimatedRotateDrawable$1;,
        Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;
    }
.end annotation


# instance fields
.field private mCurrentDegrees:F

.field private mIncrement:F

.field private mMutated:Z

.field private mRunning:Z

.field private mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 48
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/AnimatedRotateDrawable;-><init>(Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;Landroid/content/res/Resources;)V

    #@4
    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;Landroid/content/res/Resources;)V
    .registers 4
    .parameter "rotateState"
    .parameter "res"

    #@0
    .prologue
    .line 51
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 52
    new-instance v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@5
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;-><init>(Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;Landroid/graphics/drawable/AnimatedRotateDrawable;Landroid/content/res/Resources;)V

    #@8
    iput-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@a
    .line 53
    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->init()V

    #@d
    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;Landroid/content/res/Resources;Landroid/graphics/drawable/AnimatedRotateDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/AnimatedRotateDrawable;-><init>(Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method

.method private init()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 57
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@3
    .line 58
    .local v1, state:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;
    const/high16 v2, 0x43b4

    #@5
    iget v3, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mFramesCount:I

    #@7
    int-to-float v3, v3

    #@8
    div-float/2addr v2, v3

    #@9
    iput v2, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mIncrement:F

    #@b
    .line 59
    iget-object v0, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    .line 60
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1b

    #@f
    .line 61
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    #@12
    .line 62
    instance-of v2, v0, Landroid/graphics/drawable/BitmapDrawable;

    #@14
    if-eqz v2, :cond_1b

    #@16
    .line 63
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    #@18
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    #@1b
    .line 66
    :cond_1b
    return-void
.end method

.method private nextFrame()V
    .registers 5

    #@0
    .prologue
    .line 106
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@3
    .line 107
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v0

    #@7
    iget-object v2, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@9
    iget v2, v2, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mFrameDuration:I

    #@b
    int-to-long v2, v2

    #@c
    add-long/2addr v0, v2

    #@d
    invoke-virtual {p0, p0, v0, v1}, Landroid/graphics/drawable/AnimatedRotateDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    #@10
    .line 108
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter "canvas"

    #@0
    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@3
    move-result v5

    #@4
    .line 72
    .local v5, saveCount:I
    iget-object v6, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@6
    .line 73
    .local v6, st:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;
    iget-object v1, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    .line 74
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@b
    move-result-object v0

    #@c
    .line 76
    .local v0, bounds:Landroid/graphics/Rect;
    iget v8, v0, Landroid/graphics/Rect;->right:I

    #@e
    iget v9, v0, Landroid/graphics/Rect;->left:I

    #@10
    sub-int v7, v8, v9

    #@12
    .line 77
    .local v7, w:I
    iget v8, v0, Landroid/graphics/Rect;->bottom:I

    #@14
    iget v9, v0, Landroid/graphics/Rect;->top:I

    #@16
    sub-int v2, v8, v9

    #@18
    .line 79
    .local v2, h:I
    iget-boolean v8, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotXRel:Z

    #@1a
    if-eqz v8, :cond_3e

    #@1c
    int-to-float v8, v7

    #@1d
    iget v9, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotX:F

    #@1f
    mul-float v3, v8, v9

    #@21
    .line 80
    .local v3, px:F
    :goto_21
    iget-boolean v8, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotYRel:Z

    #@23
    if-eqz v8, :cond_41

    #@25
    int-to-float v8, v2

    #@26
    iget v9, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotY:F

    #@28
    mul-float v4, v8, v9

    #@2a
    .line 82
    .local v4, py:F
    :goto_2a
    iget v8, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@2c
    iget v9, v0, Landroid/graphics/Rect;->left:I

    #@2e
    int-to-float v9, v9

    #@2f
    add-float/2addr v9, v3

    #@30
    iget v10, v0, Landroid/graphics/Rect;->top:I

    #@32
    int-to-float v10, v10

    #@33
    add-float/2addr v10, v4

    #@34
    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@37
    .line 84
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@3a
    .line 86
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@3d
    .line 87
    return-void

    #@3e
    .line 79
    .end local v3           #px:F
    .end local v4           #py:F
    :cond_3e
    iget v3, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotX:F

    #@40
    goto :goto_21

    #@41
    .line 80
    .restart local v3       #px:F
    :cond_41
    iget v4, v6, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotY:F

    #@43
    goto :goto_2a
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 145
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@b
    iget-object v1, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@10
    move-result v1

    #@11
    or-int/2addr v0, v1

    #@12
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 213
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->canConstantState()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 214
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@a
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->getChangingConfigurations()I

    #@d
    move-result v1

    #@e
    iput v1, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mChangingConfigurations:I

    #@10
    .line 215
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@12
    .line 217
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 203
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    .line 188
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 21
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    sget-object v14, Lcom/android/internal/R$styleable;->AnimatedRotateDrawable:[I

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, p3

    #@6
    invoke-virtual {v0, v1, v14}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v3

    #@a
    .line 226
    .local v3, a:Landroid/content/res/TypedArray;
    const/4 v14, 0x0

    #@b
    move-object/from16 v0, p0

    #@d
    move-object/from16 v1, p1

    #@f
    move-object/from16 v2, p2

    #@11
    invoke-super {v0, v1, v2, v3, v14}, Landroid/graphics/drawable/Drawable;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@14
    .line 228
    const/4 v14, 0x2

    #@15
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@18
    move-result-object v12

    #@19
    .line 229
    .local v12, tv:Landroid/util/TypedValue;
    iget v14, v12, Landroid/util/TypedValue;->type:I

    #@1b
    const/4 v15, 0x6

    #@1c
    if-ne v14, v15, :cond_a2

    #@1e
    const/4 v7, 0x1

    #@1f
    .line 230
    .local v7, pivotXRel:Z
    :goto_1f
    if-eqz v7, :cond_a5

    #@21
    const/high16 v14, 0x3f80

    #@23
    const/high16 v15, 0x3f80

    #@25
    invoke-virtual {v12, v14, v15}, Landroid/util/TypedValue;->getFraction(FF)F

    #@28
    move-result v6

    #@29
    .line 232
    .local v6, pivotX:F
    :goto_29
    const/4 v14, 0x3

    #@2a
    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@2d
    move-result-object v12

    #@2e
    .line 233
    iget v14, v12, Landroid/util/TypedValue;->type:I

    #@30
    const/4 v15, 0x6

    #@31
    if-ne v14, v15, :cond_aa

    #@33
    const/4 v9, 0x1

    #@34
    .line 234
    .local v9, pivotYRel:Z
    :goto_34
    if-eqz v9, :cond_ac

    #@36
    const/high16 v14, 0x3f80

    #@38
    const/high16 v15, 0x3f80

    #@3a
    invoke-virtual {v12, v14, v15}, Landroid/util/TypedValue;->getFraction(FF)F

    #@3d
    move-result v8

    #@3e
    .line 236
    .local v8, pivotY:F
    :goto_3e
    const/4 v14, 0x5

    #@3f
    const/16 v15, 0xc

    #@41
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@44
    move-result v14

    #@45
    move-object/from16 v0, p0

    #@47
    invoke-virtual {v0, v14}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesCount(I)V

    #@4a
    .line 237
    const/4 v14, 0x4

    #@4b
    const/16 v15, 0x96

    #@4d
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    #@50
    move-result v14

    #@51
    move-object/from16 v0, p0

    #@53
    invoke-virtual {v0, v14}, Landroid/graphics/drawable/AnimatedRotateDrawable;->setFramesDuration(I)V

    #@56
    .line 239
    const/4 v14, 0x1

    #@57
    const/4 v15, 0x0

    #@58
    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@5b
    move-result v10

    #@5c
    .line 240
    .local v10, res:I
    const/4 v4, 0x0

    #@5d
    .line 241
    .local v4, drawable:Landroid/graphics/drawable/Drawable;
    if-lez v10, :cond_65

    #@5f
    .line 242
    move-object/from16 v0, p1

    #@61
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@64
    move-result-object v4

    #@65
    .line 245
    :cond_65
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@68
    .line 247
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@6b
    move-result v5

    #@6c
    .line 249
    .local v5, outerDepth:I
    :cond_6c
    :goto_6c
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@6f
    move-result v13

    #@70
    .local v13, type:I
    const/4 v14, 0x1

    #@71
    if-eq v13, v14, :cond_b1

    #@73
    const/4 v14, 0x3

    #@74
    if-ne v13, v14, :cond_7c

    #@76
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@79
    move-result v14

    #@7a
    if-le v14, v5, :cond_b1

    #@7c
    .line 252
    :cond_7c
    const/4 v14, 0x2

    #@7d
    if-ne v13, v14, :cond_6c

    #@7f
    .line 256
    invoke-static/range {p1 .. p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@82
    move-result-object v4

    #@83
    if-nez v4, :cond_6c

    #@85
    .line 257
    const-string v14, "drawable"

    #@87
    new-instance v15, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v16, "Bad element under <animated-rotate>: "

    #@8e
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v15

    #@92
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@95
    move-result-object v16

    #@96
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v15

    #@9a
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v15

    #@9e
    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a1
    goto :goto_6c

    #@a2
    .line 229
    .end local v4           #drawable:Landroid/graphics/drawable/Drawable;
    .end local v5           #outerDepth:I
    .end local v6           #pivotX:F
    .end local v7           #pivotXRel:Z
    .end local v8           #pivotY:F
    .end local v9           #pivotYRel:Z
    .end local v10           #res:I
    .end local v13           #type:I
    :cond_a2
    const/4 v7, 0x0

    #@a3
    goto/16 :goto_1f

    #@a5
    .line 230
    .restart local v7       #pivotXRel:Z
    :cond_a5
    invoke-virtual {v12}, Landroid/util/TypedValue;->getFloat()F

    #@a8
    move-result v6

    #@a9
    goto :goto_29

    #@aa
    .line 233
    .restart local v6       #pivotX:F
    :cond_aa
    const/4 v9, 0x0

    #@ab
    goto :goto_34

    #@ac
    .line 234
    .restart local v9       #pivotYRel:Z
    :cond_ac
    invoke-virtual {v12}, Landroid/util/TypedValue;->getFloat()F

    #@af
    move-result v8

    #@b0
    goto :goto_3e

    #@b1
    .line 262
    .restart local v4       #drawable:Landroid/graphics/drawable/Drawable;
    .restart local v5       #outerDepth:I
    .restart local v8       #pivotY:F
    .restart local v10       #res:I
    .restart local v13       #type:I
    :cond_b1
    if-nez v4, :cond_ba

    #@b3
    .line 263
    const-string v14, "drawable"

    #@b5
    const-string v15, "No drawable specified for <animated-rotate>"

    #@b7
    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 266
    :cond_ba
    move-object/from16 v0, p0

    #@bc
    iget-object v11, v0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@be
    .line 267
    .local v11, rotateState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;
    iput-object v4, v11, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@c0
    .line 268
    iput-boolean v7, v11, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotXRel:Z

    #@c2
    .line 269
    iput v6, v11, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotX:F

    #@c4
    .line 270
    iput-boolean v9, v11, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotYRel:Z

    #@c6
    .line 271
    iput v8, v11, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mPivotY:F

    #@c8
    .line 273
    invoke-direct/range {p0 .. p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->init()V

    #@cb
    .line 275
    if-eqz v4, :cond_d2

    #@cd
    .line 276
    move-object/from16 v0, p0

    #@cf
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@d2
    .line 278
    :cond_d2
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 166
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 167
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 168
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 170
    :cond_9
    return-void
.end method

.method public isRunning()Z
    .registers 2

    #@0
    .prologue
    .line 102
    iget-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mRunning:Z

    #@2
    return v0
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 291
    iget-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_14

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_14

    #@a
    .line 292
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@c
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@11
    .line 293
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mMutated:Z

    #@14
    .line 295
    :cond_14
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "bounds"

    #@0
    .prologue
    .line 198
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@6
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@8
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@a
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@f
    .line 199
    return-void
.end method

.method public run()V
    .registers 4

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@2
    iget v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mIncrement:F

    #@4
    add-float/2addr v0, v1

    #@5
    iput v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@7
    .line 114
    iget v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@9
    const/high16 v1, 0x43b4

    #@b
    iget v2, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mIncrement:F

    #@d
    sub-float/2addr v1, v2

    #@e
    cmpl-float v0, v0, v1

    #@10
    if-lez v0, :cond_15

    #@12
    .line 115
    const/4 v0, 0x0

    #@13
    iput v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@15
    .line 117
    :cond_15
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->invalidateSelf()V

    #@18
    .line 118
    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->nextFrame()V

    #@1b
    .line 119
    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 6
    .parameter "who"
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 173
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 174
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 175
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    #@9
    .line 177
    :cond_9
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 152
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@7
    .line 153
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@7
    .line 158
    return-void
.end method

.method public setFramesCount(I)V
    .registers 4
    .parameter "framesCount"

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mFramesCount:I

    #@4
    .line 282
    const/high16 v0, 0x43b4

    #@6
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@8
    iget v1, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mFramesCount:I

    #@a
    int-to-float v1, v1

    #@b
    div-float/2addr v0, v1

    #@c
    iput v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mIncrement:F

    #@e
    .line 283
    return-void
.end method

.method public setFramesDuration(I)V
    .registers 3
    .parameter "framesDuration"

    #@0
    .prologue
    .line 286
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mFrameDuration:I

    #@4
    .line 287
    return-void
.end method

.method public setVisible(ZZ)Z
    .registers 5
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    .line 123
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mState:Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;

    #@2
    iget-object v1, v1, Landroid/graphics/drawable/AnimatedRotateDrawable$AnimatedRotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@7
    .line 124
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@a
    move-result v0

    #@b
    .line 125
    .local v0, changed:Z
    if-eqz p1, :cond_18

    #@d
    .line 126
    if-nez v0, :cond_11

    #@f
    if-eqz p2, :cond_17

    #@11
    .line 127
    :cond_11
    const/4 v1, 0x0

    #@12
    iput v1, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mCurrentDegrees:F

    #@14
    .line 128
    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->nextFrame()V

    #@17
    .line 133
    :cond_17
    :goto_17
    return v0

    #@18
    .line 131
    :cond_18
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@1b
    goto :goto_17
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 90
    iget-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mRunning:Z

    #@2
    if-nez v0, :cond_a

    #@4
    .line 91
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mRunning:Z

    #@7
    .line 92
    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->nextFrame()V

    #@a
    .line 94
    :cond_a
    return-void
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 97
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/drawable/AnimatedRotateDrawable;->mRunning:Z

    #@3
    .line 98
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@6
    .line 99
    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 180
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 181
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 182
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    #@9
    .line 184
    :cond_9
    return-void
.end method
