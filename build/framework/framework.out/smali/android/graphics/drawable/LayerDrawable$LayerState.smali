.class Landroid/graphics/drawable/LayerDrawable$LayerState;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "LayerDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/LayerDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LayerState"
.end annotation


# instance fields
.field private mCanConstantState:Z

.field mChangingConfigurations:I

.field private mCheckedConstantState:Z

.field mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

.field mChildrenChangingConfigurations:I

.field private mHaveOpacity:Z

.field private mHaveStateful:Z

.field mNum:I

.field private mOpacity:I

.field private mStateful:Z


# direct methods
.method constructor <init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/graphics/drawable/LayerDrawable;Landroid/content/res/Resources;)V
    .registers 10
    .parameter "orig"
    .parameter "owner"
    .parameter "res"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 624
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    #@4
    .line 615
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveOpacity:Z

    #@6
    .line 618
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveStateful:Z

    #@8
    .line 625
    if-eqz p1, :cond_77

    #@a
    .line 626
    iget-object v3, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@c
    .line 627
    .local v3, origChildDrawable:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget v0, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@e
    .line 629
    .local v0, N:I
    iput v0, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@10
    .line 630
    new-array v5, v0, [Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@12
    iput-object v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@14
    .line 632
    iget v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChangingConfigurations:I

    #@16
    iput v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChangingConfigurations:I

    #@18
    .line 633
    iget v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@1a
    iput v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@1c
    .line 635
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    if-ge v1, v0, :cond_61

    #@1f
    .line 636
    iget-object v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@21
    new-instance v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@23
    invoke-direct {v4}, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;-><init>()V

    #@26
    aput-object v4, v5, v1

    #@28
    .line 637
    .local v4, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    aget-object v2, v3, v1

    #@2a
    .line 638
    .local v2, or:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    if-eqz p3, :cond_54

    #@2c
    .line 639
    iget-object v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2e
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5, p3}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@35
    move-result-object v5

    #@36
    iput-object v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@38
    .line 643
    :goto_38
    iget-object v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3a
    invoke-virtual {v5, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@3d
    .line 644
    iget v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@3f
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@41
    .line 645
    iget v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@43
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@45
    .line 646
    iget v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@47
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@49
    .line 647
    iget v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@4b
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@4d
    .line 648
    iget v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@4f
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@51
    .line 635
    add-int/lit8 v1, v1, 0x1

    #@53
    goto :goto_1d

    #@54
    .line 641
    :cond_54
    iget-object v5, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@56
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    #@5d
    move-result-object v5

    #@5e
    iput-object v5, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@60
    goto :goto_38

    #@61
    .line 651
    .end local v2           #or:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v4           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_61
    iget-boolean v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveOpacity:Z

    #@63
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveOpacity:Z

    #@65
    .line 652
    iget v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mOpacity:I

    #@67
    iput v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mOpacity:I

    #@69
    .line 653
    iget-boolean v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveStateful:Z

    #@6b
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveStateful:Z

    #@6d
    .line 654
    iget-boolean v5, p1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mStateful:Z

    #@6f
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mStateful:Z

    #@71
    .line 655
    const/4 v5, 0x1

    #@72
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCanConstantState:Z

    #@74
    iput-boolean v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCheckedConstantState:Z

    #@76
    .line 660
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v3           #origChildDrawable:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :goto_76
    return-void

    #@77
    .line 657
    :cond_77
    iput v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@79
    .line 658
    const/4 v5, 0x0

    #@7a
    iput-object v5, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@7c
    goto :goto_76
.end method


# virtual methods
.method public canConstantState()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 712
    iget-boolean v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCheckedConstantState:Z

    #@3
    if-nez v2, :cond_21

    #@5
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@7
    if-eqz v2, :cond_21

    #@9
    .line 713
    iput-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCanConstantState:Z

    #@b
    .line 714
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@d
    .line 715
    .local v0, N:I
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v0, :cond_1f

    #@10
    .line 716
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@12
    aget-object v2, v2, v1

    #@14
    iget-object v2, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@19
    move-result-object v2

    #@1a
    if-nez v2, :cond_24

    #@1c
    .line 717
    const/4 v2, 0x0

    #@1d
    iput-boolean v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCanConstantState:Z

    #@1f
    .line 721
    :cond_1f
    iput-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCheckedConstantState:Z

    #@21
    .line 724
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_21
    iget-boolean v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mCanConstantState:Z

    #@23
    return v2

    #@24
    .line 715
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_24
    add-int/lit8 v1, v1, 0x1

    #@26
    goto :goto_e
.end method

.method public getChangingConfigurations()I
    .registers 2

    #@0
    .prologue
    .line 674
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChangingConfigurations:I

    #@2
    return v0
.end method

.method public final getOpacity()I
    .registers 6

    #@0
    .prologue
    .line 678
    iget-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveOpacity:Z

    #@2
    if-eqz v3, :cond_7

    #@4
    .line 679
    iget v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mOpacity:I

    #@6
    .line 689
    :goto_6
    return v2

    #@7
    .line 682
    :cond_7
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@9
    .line 683
    .local v0, N:I
    if-lez v0, :cond_2a

    #@b
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@d
    const/4 v4, 0x0

    #@e
    aget-object v3, v3, v4

    #@10
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@15
    move-result v2

    #@16
    .line 684
    .local v2, op:I
    :goto_16
    const/4 v1, 0x1

    #@17
    .local v1, i:I
    :goto_17
    if-ge v1, v0, :cond_2c

    #@19
    .line 685
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@1b
    aget-object v3, v3, v1

    #@1d
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1f
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@22
    move-result v3

    #@23
    invoke-static {v2, v3}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    #@26
    move-result v2

    #@27
    .line 684
    add-int/lit8 v1, v1, 0x1

    #@29
    goto :goto_17

    #@2a
    .line 683
    .end local v1           #i:I
    .end local v2           #op:I
    :cond_2a
    const/4 v2, -0x2

    #@2b
    goto :goto_16

    #@2c
    .line 687
    .restart local v1       #i:I
    .restart local v2       #op:I
    :cond_2c
    iput v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mOpacity:I

    #@2e
    .line 688
    const/4 v3, 0x1

    #@2f
    iput-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveOpacity:Z

    #@31
    goto :goto_6
.end method

.method public final isStateful()Z
    .registers 5

    #@0
    .prologue
    .line 693
    iget-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveStateful:Z

    #@2
    if-eqz v3, :cond_7

    #@4
    .line 694
    iget-boolean v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mStateful:Z

    #@6
    .line 708
    :goto_6
    return v2

    #@7
    .line 697
    :cond_7
    const/4 v2, 0x0

    #@8
    .line 698
    .local v2, stateful:Z
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@a
    .line 699
    .local v0, N:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_1a

    #@d
    .line 700
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@f
    aget-object v3, v3, v1

    #@11
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_20

    #@19
    .line 701
    const/4 v2, 0x1

    #@1a
    .line 706
    :cond_1a
    iput-boolean v2, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mStateful:Z

    #@1c
    .line 707
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mHaveStateful:Z

    #@1f
    goto :goto_6

    #@20
    .line 699
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_b
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 664
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V

    #@6
    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "res"

    #@0
    .prologue
    .line 669
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    #@2
    invoke-direct {v0, p0, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V

    #@5
    return-object v0
.end method
