.class public abstract Landroid/graphics/drawable/Drawable;
.super Ljava/lang/Object;
.source "Drawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/Drawable$ConstantState;,
        Landroid/graphics/drawable/Drawable$Callback;
    }
.end annotation


# static fields
.field private static final ZERO_BOUNDS_RECT:Landroid/graphics/Rect;


# instance fields
.field private mBounds:Landroid/graphics/Rect;

.field private mCallback:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable$Callback;",
            ">;"
        }
    .end annotation
.end field

.field private mChangingConfigurations:I

.field private mLayoutDirection:I

.field private mLevel:I

.field private mStateSet:[I

.field private mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 117
    new-instance v0, Landroid/graphics/Rect;

    #@2
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/drawable/Drawable;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 116
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 119
    sget-object v0, Landroid/util/StateSet;->WILD_CARD:[I

    #@6
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mStateSet:[I

    #@8
    .line 120
    iput v1, p0, Landroid/graphics/drawable/Drawable;->mLevel:I

    #@a
    .line 121
    iput v1, p0, Landroid/graphics/drawable/Drawable;->mChangingConfigurations:I

    #@c
    .line 122
    sget-object v0, Landroid/graphics/drawable/Drawable;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    #@e
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@10
    .line 123
    const/4 v0, 0x0

    #@11
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mCallback:Ljava/lang/ref/WeakReference;

    #@13
    .line 124
    const/4 v0, 0x1

    #@14
    iput-boolean v0, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@16
    .line 955
    return-void
.end method

.method public static createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter "pathName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 905
    if-nez p0, :cond_4

    #@3
    .line 914
    :cond_3
    :goto_3
    return-object v0

    #@4
    .line 909
    :cond_4
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@7
    move-result-object v1

    #@8
    .line 910
    .local v1, bm:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_3

    #@a
    move-object v2, v0

    #@b
    move-object v3, v0

    #@c
    move-object v4, v0

    #@d
    move-object v5, p0

    #@e
    .line 911
    invoke-static/range {v0 .. v5}, Landroid/graphics/drawable/Drawable;->drawableFromBitmap(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@11
    move-result-object v0

    #@12
    goto :goto_3
.end method

.method public static createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "res"
    .parameter "value"
    .parameter "is"
    .parameter "srcName"

    #@0
    .prologue
    .line 753
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    .registers 14
    .parameter "res"
    .parameter "value"
    .parameter "is"
    .parameter "srcName"
    .parameter "opts"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 763
    if-nez p2, :cond_6

    #@4
    move-object v0, v5

    #@5
    .line 811
    :goto_5
    return-object v0

    #@6
    .line 773
    :cond_6
    new-instance v3, Landroid/graphics/Rect;

    #@8
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    #@b
    .line 782
    .local v3, pad:Landroid/graphics/Rect;
    if-nez p4, :cond_12

    #@d
    new-instance p4, Landroid/graphics/BitmapFactory$Options;

    #@f
    .end local p4
    invoke-direct {p4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@12
    .line 783
    .restart local p4
    :cond_12
    if-eqz p0, :cond_6c

    #@14
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@17
    move-result-object v0

    #@18
    iget v0, v0, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@1a
    :goto_1a
    iput v0, p4, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@1c
    .line 786
    if-eqz p0, :cond_3a

    #@1e
    invoke-virtual {p0}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@21
    move-result-object v0

    #@22
    invoke-virtual {v0}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_3a

    #@28
    .line 787
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@2a
    iput v0, p4, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@2c
    .line 790
    if-eqz p3, :cond_3a

    #@2e
    const-string/jumbo v0, "xhdpi"

    #@31
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@34
    move-result v0

    #@35
    const/4 v7, -0x1

    #@36
    if-eq v0, v7, :cond_3a

    #@38
    .line 791
    iput-boolean v8, p4, Landroid/graphics/BitmapFactory$Options;->inForceScaled:Z

    #@3a
    .line 796
    :cond_3a
    invoke-static {p0, p1, p2, v3, p4}, Landroid/graphics/BitmapFactory;->decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@3d
    move-result-object v1

    #@3e
    .line 797
    .local v1, bm:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_6f

    #@40
    .line 798
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    #@43
    move-result-object v2

    #@44
    .line 799
    .local v2, np:[B
    if-eqz v2, :cond_4c

    #@46
    invoke-static {v2}, Landroid/graphics/NinePatch;->isNinePatchChunk([B)Z

    #@49
    move-result v0

    #@4a
    if-nez v0, :cond_4e

    #@4c
    .line 800
    :cond_4c
    const/4 v2, 0x0

    #@4d
    .line 801
    const/4 v3, 0x0

    #@4e
    .line 803
    :cond_4e
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getLayoutBounds()[I

    #@51
    move-result-object v6

    #@52
    .line 804
    .local v6, layoutBounds:[I
    const/4 v4, 0x0

    #@53
    .line 805
    .local v4, layoutBoundsRect:Landroid/graphics/Rect;
    if-eqz v6, :cond_65

    #@55
    .line 806
    new-instance v4, Landroid/graphics/Rect;

    #@57
    .end local v4           #layoutBoundsRect:Landroid/graphics/Rect;
    const/4 v0, 0x0

    #@58
    aget v0, v6, v0

    #@5a
    aget v5, v6, v8

    #@5c
    const/4 v7, 0x2

    #@5d
    aget v7, v6, v7

    #@5f
    const/4 v8, 0x3

    #@60
    aget v8, v6, v8

    #@62
    invoke-direct {v4, v0, v5, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    #@65
    .restart local v4       #layoutBoundsRect:Landroid/graphics/Rect;
    :cond_65
    move-object v0, p0

    #@66
    move-object v5, p3

    #@67
    .line 809
    invoke-static/range {v0 .. v5}, Landroid/graphics/drawable/Drawable;->drawableFromBitmap(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    #@6a
    move-result-object v0

    #@6b
    goto :goto_5

    #@6c
    .line 783
    .end local v1           #bm:Landroid/graphics/Bitmap;
    .end local v2           #np:[B
    .end local v4           #layoutBoundsRect:Landroid/graphics/Rect;
    .end local v6           #layoutBounds:[I
    :cond_6c
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@6e
    goto :goto_1a

    #@6f
    .restart local v1       #bm:Landroid/graphics/Bitmap;
    :cond_6f
    move-object v0, v5

    #@70
    .line 811
    goto :goto_5
.end method

.method public static createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "is"
    .parameter "srcName"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 744
    invoke-static {v0, v0, p0, p1, v0}, Landroid/graphics/drawable/Drawable;->createFromResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "r"
    .parameter "parser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 821
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    #@4
    move-result-object v0

    #@5
    .line 825
    .local v0, attrs:Landroid/util/AttributeSet;
    :cond_5
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@8
    move-result v2

    #@9
    .local v2, type:I
    if-eq v2, v4, :cond_e

    #@b
    const/4 v3, 0x1

    #@c
    if-ne v2, v3, :cond_5

    #@e
    .line 829
    :cond_e
    if-eq v2, v4, :cond_18

    #@10
    .line 830
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    #@12
    const-string v4, "No start tag found"

    #@14
    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@17
    throw v3

    #@18
    .line 833
    :cond_18
    invoke-static {p0, p1, v0}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@1b
    move-result-object v1

    #@1c
    .line 835
    .local v1, drawable:Landroid/graphics/drawable/Drawable;
    if-nez v1, :cond_3b

    #@1e
    .line 836
    new-instance v3, Ljava/lang/RuntimeException;

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "Unknown initial tag: "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@3a
    throw v3

    #@3b
    .line 839
    :cond_3b
    return-object v1
.end method

.method public static createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 851
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 853
    .local v1, name:Ljava/lang/String;
    const-string/jumbo v2, "selector"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v2

    #@b
    if-eqz v2, :cond_16

    #@d
    .line 854
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    #@f
    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    #@12
    .line 896
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    :cond_12
    :goto_12
    invoke-virtual {v0, p0, p1, p2}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@15
    .line 897
    return-object v0

    #@16
    .line 855
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_16
    const-string/jumbo v2, "level-list"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_25

    #@1f
    .line 856
    new-instance v0, Landroid/graphics/drawable/LevelListDrawable;

    #@21
    invoke-direct {v0}, Landroid/graphics/drawable/LevelListDrawable;-><init>()V

    #@24
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@25
    .line 861
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_25
    const-string/jumbo v2, "layer-list"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v2

    #@2c
    if-eqz v2, :cond_34

    #@2e
    .line 862
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    #@30
    invoke-direct {v0}, Landroid/graphics/drawable/LayerDrawable;-><init>()V

    #@33
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@34
    .line 863
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_34
    const-string/jumbo v2, "transition"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_43

    #@3d
    .line 864
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    #@3f
    invoke-direct {v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>()V

    #@42
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@43
    .line 865
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_43
    const-string v2, "color"

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48
    move-result v2

    #@49
    if-eqz v2, :cond_51

    #@4b
    .line 866
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    #@4d
    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    #@50
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@51
    .line 867
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_51
    const-string/jumbo v2, "shape"

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v2

    #@58
    if-eqz v2, :cond_60

    #@5a
    .line 868
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    #@5c
    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    #@5f
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@60
    .line 869
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_60
    const-string/jumbo v2, "scale"

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v2

    #@67
    if-eqz v2, :cond_6f

    #@69
    .line 870
    new-instance v0, Landroid/graphics/drawable/ScaleDrawable;

    #@6b
    invoke-direct {v0}, Landroid/graphics/drawable/ScaleDrawable;-><init>()V

    #@6e
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@6f
    .line 871
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_6f
    const-string v2, "clip"

    #@71
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v2

    #@75
    if-eqz v2, :cond_7d

    #@77
    .line 872
    new-instance v0, Landroid/graphics/drawable/ClipDrawable;

    #@79
    invoke-direct {v0}, Landroid/graphics/drawable/ClipDrawable;-><init>()V

    #@7c
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@7d
    .line 873
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_7d
    const-string/jumbo v2, "rotate"

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@83
    move-result v2

    #@84
    if-eqz v2, :cond_8c

    #@86
    .line 874
    new-instance v0, Landroid/graphics/drawable/RotateDrawable;

    #@88
    invoke-direct {v0}, Landroid/graphics/drawable/RotateDrawable;-><init>()V

    #@8b
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto :goto_12

    #@8c
    .line 875
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_8c
    const-string v2, "animated-rotate"

    #@8e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v2

    #@92
    if-eqz v2, :cond_9b

    #@94
    .line 876
    new-instance v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    #@96
    invoke-direct {v0}, Landroid/graphics/drawable/AnimatedRotateDrawable;-><init>()V

    #@99
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto/16 :goto_12

    #@9b
    .line 877
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_9b
    const-string v2, "animation-list"

    #@9d
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v2

    #@a1
    if-eqz v2, :cond_aa

    #@a3
    .line 878
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    #@a5
    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    #@a8
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto/16 :goto_12

    #@aa
    .line 879
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_aa
    const-string v2, "inset"

    #@ac
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@af
    move-result v2

    #@b0
    if-eqz v2, :cond_b9

    #@b2
    .line 880
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    #@b4
    invoke-direct {v0}, Landroid/graphics/drawable/InsetDrawable;-><init>()V

    #@b7
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    goto/16 :goto_12

    #@b9
    .line 881
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_b9
    const-string v2, "bitmap"

    #@bb
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@be
    move-result v2

    #@bf
    if-eqz v2, :cond_d4

    #@c1
    .line 882
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    #@c3
    invoke-direct {v0, p0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    #@c6
    .line 883
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    if-eqz p0, :cond_12

    #@c8
    move-object v2, v0

    #@c9
    .line 884
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    #@cb
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@ce
    move-result-object v3

    #@cf
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V

    #@d2
    goto/16 :goto_12

    #@d4
    .line 886
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_d4
    const-string/jumbo v2, "nine-patch"

    #@d7
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@da
    move-result v2

    #@db
    if-eqz v2, :cond_f0

    #@dd
    .line 887
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    #@df
    invoke-direct {v0}, Landroid/graphics/drawable/NinePatchDrawable;-><init>()V

    #@e2
    .line 888
    .restart local v0       #drawable:Landroid/graphics/drawable/Drawable;
    if-eqz p0, :cond_12

    #@e4
    move-object v2, v0

    #@e5
    .line 889
    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    #@e7
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@ea
    move-result-object v3

    #@eb
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/NinePatchDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V

    #@ee
    goto/16 :goto_12

    #@f0
    .line 892
    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_f0
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@f2
    new-instance v3, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@fa
    move-result-object v4

    #@fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    const-string v4, ": invalid drawable tag "

    #@101
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v3

    #@105
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v3

    #@10d
    invoke-direct {v2, v3}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@110
    throw v2
.end method

.method private static drawableFromBitmap(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 13
    .parameter "res"
    .parameter "bm"
    .parameter "np"
    .parameter "pad"
    .parameter "layoutBounds"
    .parameter "srcName"

    #@0
    .prologue
    .line 994
    if-eqz p2, :cond_e

    #@2
    .line 995
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    #@4
    move-object v1, p0

    #@5
    move-object v2, p1

    #@6
    move-object v3, p2

    #@7
    move-object v4, p3

    #@8
    move-object v5, p4

    #@9
    move-object v6, p5

    #@a
    invoke-direct/range {v0 .. v6}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)V

    #@d
    .line 998
    :goto_d
    return-object v0

    #@e
    :cond_e
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    #@10
    invoke-direct {v0, p0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    #@13
    goto :goto_d
.end method

.method public static resolveOpacity(II)I
    .registers 4
    .parameter "op1"
    .parameter "op2"

    #@0
    .prologue
    const/4 v0, -0x2

    #@1
    const/4 v1, -0x3

    #@2
    .line 600
    if-ne p0, p1, :cond_5

    #@4
    .line 612
    .end local p0
    :goto_4
    return p0

    #@5
    .line 603
    .restart local p0
    :cond_5
    if-eqz p0, :cond_9

    #@7
    if-nez p1, :cond_b

    #@9
    .line 604
    :cond_9
    const/4 p0, 0x0

    #@a
    goto :goto_4

    #@b
    .line 606
    :cond_b
    if-eq p0, v1, :cond_f

    #@d
    if-ne p1, v1, :cond_11

    #@f
    :cond_f
    move p0, v1

    #@10
    .line 607
    goto :goto_4

    #@11
    .line 609
    :cond_11
    if-eq p0, v0, :cond_15

    #@13
    if-ne p1, v0, :cond_17

    #@15
    :cond_15
    move p0, v0

    #@16
    .line 610
    goto :goto_4

    #@17
    .line 612
    :cond_17
    const/4 p0, -0x1

    #@18
    goto :goto_4
.end method


# virtual methods
.method public clearColorFilter()V
    .registers 2

    #@0
    .prologue
    .line 423
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@4
    .line 424
    return-void
.end method

.method public final copyBounds()Landroid/graphics/Rect;
    .registers 3

    #@0
    .prologue
    .line 183
    new-instance v0, Landroid/graphics/Rect;

    #@2
    iget-object v1, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@4
    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@7
    return-object v0
.end method

.method public final copyBounds(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5
    .line 172
    return-void
.end method

.method public abstract draw(Landroid/graphics/Canvas;)V
.end method

.method public final getBounds()Landroid/graphics/Rect;
    .registers 3

    #@0
    .prologue
    .line 202
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@2
    sget-object v1, Landroid/graphics/drawable/Drawable;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    #@4
    if-ne v0, v1, :cond_d

    #@6
    .line 203
    new-instance v0, Landroid/graphics/Rect;

    #@8
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@d
    .line 206
    :cond_d
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@f
    return-object v0
.end method

.method public getCallback()Landroid/graphics/drawable/Drawable$Callback;
    .registers 2

    #@0
    .prologue
    .line 320
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mCallback:Ljava/lang/ref/WeakReference;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 321
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mCallback:Ljava/lang/ref/WeakReference;

    #@6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/graphics/drawable/Drawable$Callback;

    #@c
    .line 323
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method public getChangingConfigurations()I
    .registers 2

    #@0
    .prologue
    .line 236
    iget v0, p0, Landroid/graphics/drawable/Drawable;->mChangingConfigurations:I

    #@2
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 2

    #@0
    .prologue
    .line 988
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public getCurrent()Landroid/graphics/drawable/Drawable;
    .registers 1

    #@0
    .prologue
    .line 495
    return-object p0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 669
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 661
    const/4 v0, -0x1

    #@1
    return v0
.end method

.method public getLayoutDirection()I
    .registers 2

    #@0
    .prologue
    .line 384
    iget v0, p0, Landroid/graphics/drawable/Drawable;->mLayoutDirection:I

    #@2
    return v0
.end method

.method public getLayoutInsets()Landroid/graphics/Insets;
    .registers 2

    #@0
    .prologue
    .line 719
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@2
    return-object v0
.end method

.method public final getLevel()I
    .registers 2

    #@0
    .prologue
    .line 528
    iget v0, p0, Landroid/graphics/drawable/Drawable;->mLevel:I

    #@2
    return v0
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 696
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@3
    move-result v0

    #@4
    .line 697
    .local v0, intrinsicHeight:I
    if-lez v0, :cond_7

    #@6
    .end local v0           #intrinsicHeight:I
    :goto_6
    return v0

    #@7
    .restart local v0       #intrinsicHeight:I
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 682
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@3
    move-result v0

    #@4
    .line 683
    .local v0, intrinsicWidth:I
    if-lez v0, :cond_7

    #@6
    .end local v0           #intrinsicWidth:I
    :goto_6
    return v0

    #@7
    .restart local v0       #intrinsicWidth:I
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public abstract getOpacity()I
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 708
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    #@4
    .line 709
    return v0
.end method

.method public getState()[I
    .registers 2

    #@0
    .prologue
    .line 478
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mStateSet:[I

    #@2
    return-object v0
.end method

.method public getTransparentRegion()Landroid/graphics/Region;
    .registers 2

    #@0
    .prologue
    .line 629
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 923
    sget-object v1, Lcom/android/internal/R$styleable;->Drawable:[I

    #@2
    invoke-virtual {p1, p3, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@5
    move-result-object v0

    #@6
    .line 924
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    #@7
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/graphics/drawable/Drawable;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@a
    .line 925
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@d
    .line 926
    return-void
.end method

.method inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V
    .registers 6
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .parameter "visibleAttr"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 938
    iget-boolean v0, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@2
    invoke-virtual {p3, p4, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@5
    move-result v0

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@8
    .line 939
    return-void
.end method

.method public invalidateSelf()V
    .registers 2

    #@0
    .prologue
    .line 336
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 337
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 338
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 340
    :cond_9
    return-void
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 437
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final isVisible()Z
    .registers 2

    #@0
    .prologue
    .line 555
    iget-boolean v0, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@2
    return v0
.end method

.method public jumpToCurrentState()V
    .registers 1

    #@0
    .prologue
    .line 486
    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 1

    #@0
    .prologue
    .line 737
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "bounds"

    #@0
    .prologue
    .line 654
    return-void
.end method

.method protected onLevelChange(I)Z
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 649
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected onStateChange([I)Z
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 641
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public scheduleSelf(Ljava/lang/Runnable;J)V
    .registers 5
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 353
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 354
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 355
    invoke-interface {v0, p0, p1, p2, p3}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    #@9
    .line 357
    :cond_9
    return-void
.end method

.method public abstract setAlpha(I)V
.end method

.method public setBounds(IIII)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 141
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@2
    .line 143
    .local v0, oldBounds:Landroid/graphics/Rect;
    sget-object v1, Landroid/graphics/drawable/Drawable;->ZERO_BOUNDS_RECT:Landroid/graphics/Rect;

    #@4
    if-ne v0, v1, :cond_d

    #@6
    .line 144
    new-instance v0, Landroid/graphics/Rect;

    #@8
    .end local v0           #oldBounds:Landroid/graphics/Rect;
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@d
    .line 147
    .restart local v0       #oldBounds:Landroid/graphics/Rect;
    :cond_d
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@f
    if-ne v1, p1, :cond_1d

    #@11
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@13
    if-ne v1, p2, :cond_1d

    #@15
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@17
    if-ne v1, p3, :cond_1d

    #@19
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@1b
    if-eq v1, p4, :cond_27

    #@1d
    .line 149
    :cond_1d
    iget-object v1, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@1f
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    #@22
    .line 150
    iget-object v1, p0, Landroid/graphics/drawable/Drawable;->mBounds:Landroid/graphics/Rect;

    #@24
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@27
    .line 152
    :cond_27
    return-void
.end method

.method public setBounds(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "bounds"

    #@0
    .prologue
    .line 159
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@b
    .line 160
    return-void
.end method

.method public final setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 308
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@2
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@5
    iput-object v0, p0, Landroid/graphics/drawable/Drawable;->mCallback:Ljava/lang/ref/WeakReference;

    #@7
    .line 309
    return-void
.end method

.method public setChangingConfigurations(I)V
    .registers 2
    .parameter "configs"

    #@0
    .prologue
    .line 219
    iput p1, p0, Landroid/graphics/drawable/Drawable;->mChangingConfigurations:I

    #@2
    .line 220
    return-void
.end method

.method public setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 4
    .parameter "color"
    .parameter "mode"

    #@0
    .prologue
    .line 419
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    #@5
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@8
    .line 420
    return-void
.end method

.method public abstract setColorFilter(Landroid/graphics/ColorFilter;)V
.end method

.method public setDither(Z)V
    .registers 2
    .parameter "dither"

    #@0
    .prologue
    .line 244
    return-void
.end method

.method public setFilterBitmap(Z)V
    .registers 2
    .parameter "filter"

    #@0
    .prologue
    .line 252
    return-void
.end method

.method public setLayoutDirection(I)V
    .registers 3
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 397
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getLayoutDirection()I

    #@3
    move-result v0

    #@4
    if-eq v0, p1, :cond_8

    #@6
    .line 398
    iput p1, p0, Landroid/graphics/drawable/Drawable;->mLayoutDirection:I

    #@8
    .line 400
    :cond_8
    return-void
.end method

.method public final setLevel(I)Z
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 515
    iget v0, p0, Landroid/graphics/drawable/Drawable;->mLevel:I

    #@2
    if-eq v0, p1, :cond_b

    #@4
    .line 516
    iput p1, p0, Landroid/graphics/drawable/Drawable;->mLevel:I

    #@6
    .line 517
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->onLevelChange(I)Z

    #@9
    move-result v0

    #@a
    .line 519
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public setState([I)Z
    .registers 3
    .parameter "stateSet"

    #@0
    .prologue
    .line 463
    iget-object v0, p0, Landroid/graphics/drawable/Drawable;->mStateSet:[I

    #@2
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_f

    #@8
    .line 464
    iput-object p1, p0, Landroid/graphics/drawable/Drawable;->mStateSet:[I

    #@a
    .line 465
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->onStateChange([I)Z

    #@d
    move-result v0

    #@e
    .line 467
    :goto_e
    return v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public setVisible(ZZ)Z
    .registers 5
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    .line 546
    iget-boolean v1, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@2
    if-eq v1, p1, :cond_d

    #@4
    const/4 v0, 0x1

    #@5
    .line 547
    .local v0, changed:Z
    :goto_5
    if-eqz v0, :cond_c

    #@7
    .line 548
    iput-boolean p1, p0, Landroid/graphics/drawable/Drawable;->mVisible:Z

    #@9
    .line 549
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    #@c
    .line 551
    :cond_c
    return v0

    #@d
    .line 546
    .end local v0           #changed:Z
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_5
.end method

.method public unscheduleSelf(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 369
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 370
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 371
    invoke-interface {v0, p0, p1}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    #@9
    .line 373
    :cond_9
    return-void
.end method
