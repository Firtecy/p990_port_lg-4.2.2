.class public abstract Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "DrawableContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/DrawableContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DrawableContainerState"
.end annotation


# instance fields
.field mCanConstantState:Z

.field mChangingConfigurations:I

.field mCheckedConstantState:Z

.field mChildrenChangingConfigurations:I

.field mComputedConstantSize:Z

.field mConstantHeight:I

.field mConstantMinimumHeight:I

.field mConstantMinimumWidth:I

.field mConstantPadding:Landroid/graphics/Rect;

.field mConstantSize:Z

.field mConstantWidth:I

.field mDither:Z

.field mDrawables:[Landroid/graphics/drawable/Drawable;

.field mEnterFadeDuration:I

.field mExitFadeDuration:I

.field mHaveStateful:Z

.field mNumChildren:I

.field mOpacity:I

.field final mOwner:Landroid/graphics/drawable/DrawableContainer;

.field mPaddingChecked:Z

.field mStateful:Z

.field mVariablePadding:Z


# direct methods
.method constructor <init>(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;Landroid/graphics/drawable/DrawableContainer;Landroid/content/res/Resources;)V
    .registers 10
    .parameter "orig"
    .parameter "owner"
    .parameter "res"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 463
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    #@5
    .line 437
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mVariablePadding:Z

    #@7
    .line 438
    const/4 v3, 0x0

    #@8
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@a
    .line 440
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantSize:Z

    #@c
    .line 441
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@e
    .line 449
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@10
    .line 455
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mPaddingChecked:Z

    #@12
    .line 457
    iput-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@14
    .line 464
    iput-object p2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mOwner:Landroid/graphics/drawable/DrawableContainer;

    #@16
    .line 466
    if-eqz p1, :cond_9f

    #@18
    .line 467
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChangingConfigurations:I

    #@1a
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChangingConfigurations:I

    #@1c
    .line 468
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChildrenChangingConfigurations:I

    #@1e
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChildrenChangingConfigurations:I

    #@20
    .line 470
    iget-object v2, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@22
    .line 472
    .local v2, origDr:[Landroid/graphics/drawable/Drawable;
    array-length v3, v2

    #@23
    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    #@25
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@27
    .line 473
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@29
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@2b
    .line 475
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@2d
    .line 476
    .local v0, N:I
    const/4 v1, 0x0

    #@2e
    .local v1, i:I
    :goto_2e
    if-ge v1, v0, :cond_59

    #@30
    .line 477
    if-eqz p3, :cond_4a

    #@32
    .line 478
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@34
    aget-object v4, v2, v1

    #@36
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4, p3}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    #@3d
    move-result-object v4

    #@3e
    aput-object v4, v3, v1

    #@40
    .line 482
    :goto_40
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@42
    aget-object v3, v3, v1

    #@44
    invoke-virtual {v3, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@47
    .line 476
    add-int/lit8 v1, v1, 0x1

    #@49
    goto :goto_2e

    #@4a
    .line 480
    :cond_4a
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@4c
    aget-object v4, v2, v1

    #@4e
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@51
    move-result-object v4

    #@52
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    #@55
    move-result-object v4

    #@56
    aput-object v4, v3, v1

    #@58
    goto :goto_40

    #@59
    .line 485
    :cond_59
    iput-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCanConstantState:Z

    #@5b
    iput-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCheckedConstantState:Z

    #@5d
    .line 486
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mVariablePadding:Z

    #@5f
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mVariablePadding:Z

    #@61
    .line 487
    iget-object v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@63
    if-eqz v3, :cond_6e

    #@65
    .line 488
    new-instance v3, Landroid/graphics/Rect;

    #@67
    iget-object v4, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@69
    invoke-direct {v3, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@6c
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@6e
    .line 490
    :cond_6e
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantSize:Z

    #@70
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantSize:Z

    #@72
    .line 491
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@74
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@76
    .line 492
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@78
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@7a
    .line 493
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@7c
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@7e
    .line 494
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@80
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@82
    .line 495
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@84
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@86
    .line 497
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mOpacity:I

    #@88
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mOpacity:I

    #@8a
    .line 498
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@8c
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@8e
    .line 499
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mStateful:Z

    #@90
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mStateful:Z

    #@92
    .line 501
    iget-boolean v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@94
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@96
    .line 503
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@98
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@9a
    .line 504
    iget v3, p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@9c
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@9e
    .line 511
    .end local v0           #N:I
    .end local v1           #i:I
    .end local v2           #origDr:[Landroid/graphics/drawable/Drawable;
    :goto_9e
    return-void

    #@9f
    .line 507
    :cond_9f
    const/16 v3, 0xa

    #@a1
    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    #@a3
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@a5
    .line 508
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@a7
    .line 509
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCanConstantState:Z

    #@a9
    iput-boolean v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCheckedConstantState:Z

    #@ab
    goto :goto_9e
.end method


# virtual methods
.method public final addChild(Landroid/graphics/drawable/Drawable;)I
    .registers 6
    .parameter "dr"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 519
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@3
    .line 521
    .local v0, pos:I
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@5
    array-length v1, v1

    #@6
    if-lt v0, v1, :cond_d

    #@8
    .line 522
    add-int/lit8 v1, v0, 0xa

    #@a
    invoke-virtual {p0, v0, v1}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->growArray(II)V

    #@d
    .line 525
    :cond_d
    const/4 v1, 0x1

    #@e
    invoke-virtual {p1, v3, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@11
    .line 526
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mOwner:Landroid/graphics/drawable/DrawableContainer;

    #@13
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@16
    .line 528
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@18
    aput-object p1, v1, v0

    #@1a
    .line 529
    iget v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@1c
    add-int/lit8 v1, v1, 0x1

    #@1e
    iput v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@20
    .line 530
    iget v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChildrenChangingConfigurations:I

    #@22
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@25
    move-result v2

    #@26
    or-int/2addr v1, v2

    #@27
    iput v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChildrenChangingConfigurations:I

    #@29
    .line 531
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@2b
    .line 533
    const/4 v1, 0x0

    #@2c
    iput-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@2e
    .line 534
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mPaddingChecked:Z

    #@30
    .line 535
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@32
    .line 537
    return v0
.end method

.method public declared-synchronized canConstantState()Z
    .registers 4

    #@0
    .prologue
    .line 694
    monitor-enter p0

    #@1
    :try_start_1
    iget-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCheckedConstantState:Z

    #@3
    if-nez v2, :cond_1d

    #@5
    .line 695
    const/4 v2, 0x1

    #@6
    iput-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCanConstantState:Z

    #@8
    .line 696
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@a
    .line 697
    .local v0, N:I
    const/4 v1, 0x0

    #@b
    .local v1, i:I
    :goto_b
    if-ge v1, v0, :cond_1a

    #@d
    .line 698
    iget-object v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@f
    aget-object v2, v2, v1

    #@11
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    #@14
    move-result-object v2

    #@15
    if-nez v2, :cond_21

    #@17
    .line 699
    const/4 v2, 0x0

    #@18
    iput-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCanConstantState:Z

    #@1a
    .line 703
    :cond_1a
    const/4 v2, 0x1

    #@1b
    iput-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCheckedConstantState:Z

    #@1d
    .line 706
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_1d
    iget-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mCanConstantState:Z
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_24

    #@1f
    monitor-exit p0

    #@20
    return v2

    #@21
    .line 697
    .restart local v0       #N:I
    .restart local v1       #i:I
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_b

    #@24
    .line 694
    .end local v0           #N:I
    .end local v1           #i:I
    :catchall_24
    move-exception v2

    #@25
    monitor-exit p0

    #@26
    throw v2
.end method

.method protected computeConstantSize()V
    .registers 7

    #@0
    .prologue
    .line 622
    const/4 v5, 0x1

    #@1
    iput-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@3
    .line 624
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildCount()I

    #@6
    move-result v0

    #@7
    .line 625
    .local v0, N:I
    iget-object v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@9
    .line 626
    .local v2, drawables:[Landroid/graphics/drawable/Drawable;
    const/4 v5, -0x1

    #@a
    iput v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@c
    iput v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@e
    .line 627
    const/4 v5, 0x0

    #@f
    iput v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@11
    iput v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@13
    .line 628
    const/4 v3, 0x0

    #@14
    .local v3, i:I
    :goto_14
    if-ge v3, v0, :cond_43

    #@16
    .line 629
    aget-object v1, v2, v3

    #@18
    .line 630
    .local v1, dr:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1b
    move-result v4

    #@1c
    .line 631
    .local v4, s:I
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@1e
    if-le v4, v5, :cond_22

    #@20
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@22
    .line 632
    :cond_22
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@25
    move-result v4

    #@26
    .line 633
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@28
    if-le v4, v5, :cond_2c

    #@2a
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@2c
    .line 634
    :cond_2c
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@2f
    move-result v4

    #@30
    .line 635
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@32
    if-le v4, v5, :cond_36

    #@34
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@36
    .line 636
    :cond_36
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@39
    move-result v4

    #@3a
    .line 637
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@3c
    if-le v4, v5, :cond_40

    #@3e
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@40
    .line 628
    :cond_40
    add-int/lit8 v3, v3, 0x1

    #@42
    goto :goto_14

    #@43
    .line 639
    .end local v1           #dr:Landroid/graphics/drawable/Drawable;
    .end local v4           #s:I
    :cond_43
    return-void
.end method

.method public getChangingConfigurations()I
    .registers 2

    #@0
    .prologue
    .line 515
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChangingConfigurations:I

    #@2
    return v0
.end method

.method public final getChildCount()I
    .registers 2

    #@0
    .prologue
    .line 541
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@2
    return v0
.end method

.method public final getChildren()[Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 545
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public final getConstantHeight()I
    .registers 2

    #@0
    .prologue
    .line 598
    iget-boolean v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 599
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->computeConstantSize()V

    #@7
    .line 602
    :cond_7
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@9
    return v0
.end method

.method public final getConstantMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 614
    iget-boolean v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 615
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->computeConstantSize()V

    #@7
    .line 618
    :cond_7
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@9
    return v0
.end method

.method public final getConstantMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 606
    iget-boolean v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 607
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->computeConstantSize()V

    #@7
    .line 610
    :cond_7
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@9
    return v0
.end method

.method public final getConstantPadding()Landroid/graphics/Rect;
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 557
    iget-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mVariablePadding:Z

    #@3
    if-eqz v5, :cond_7

    #@5
    .line 558
    const/4 v3, 0x0

    #@6
    .line 578
    :goto_6
    return-object v3

    #@7
    .line 560
    :cond_7
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@9
    if-nez v5, :cond_f

    #@b
    iget-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mPaddingChecked:Z

    #@d
    if-eqz v5, :cond_12

    #@f
    .line 561
    :cond_f
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@11
    goto :goto_6

    #@12
    .line 564
    :cond_12
    const/4 v3, 0x0

    #@13
    .line 565
    .local v3, r:Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    #@15
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    #@18
    .line 566
    .local v4, t:Landroid/graphics/Rect;
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildCount()I

    #@1b
    move-result v0

    #@1c
    .line 567
    .local v0, N:I
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@1e
    .line 568
    .local v1, drawables:[Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    if-ge v2, v0, :cond_5b

    #@21
    .line 569
    aget-object v5, v1, v2

    #@23
    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_58

    #@29
    .line 570
    if-nez v3, :cond_30

    #@2b
    new-instance v3, Landroid/graphics/Rect;

    #@2d
    .end local v3           #r:Landroid/graphics/Rect;
    invoke-direct {v3, v7, v7, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    #@30
    .line 571
    .restart local v3       #r:Landroid/graphics/Rect;
    :cond_30
    iget v5, v4, Landroid/graphics/Rect;->left:I

    #@32
    iget v6, v3, Landroid/graphics/Rect;->left:I

    #@34
    if-le v5, v6, :cond_3a

    #@36
    iget v5, v4, Landroid/graphics/Rect;->left:I

    #@38
    iput v5, v3, Landroid/graphics/Rect;->left:I

    #@3a
    .line 572
    :cond_3a
    iget v5, v4, Landroid/graphics/Rect;->top:I

    #@3c
    iget v6, v3, Landroid/graphics/Rect;->top:I

    #@3e
    if-le v5, v6, :cond_44

    #@40
    iget v5, v4, Landroid/graphics/Rect;->top:I

    #@42
    iput v5, v3, Landroid/graphics/Rect;->top:I

    #@44
    .line 573
    :cond_44
    iget v5, v4, Landroid/graphics/Rect;->right:I

    #@46
    iget v6, v3, Landroid/graphics/Rect;->right:I

    #@48
    if-le v5, v6, :cond_4e

    #@4a
    iget v5, v4, Landroid/graphics/Rect;->right:I

    #@4c
    iput v5, v3, Landroid/graphics/Rect;->right:I

    #@4e
    .line 574
    :cond_4e
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    #@50
    iget v6, v3, Landroid/graphics/Rect;->bottom:I

    #@52
    if-le v5, v6, :cond_58

    #@54
    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    #@56
    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    #@58
    .line 568
    :cond_58
    add-int/lit8 v2, v2, 0x1

    #@5a
    goto :goto_1f

    #@5b
    .line 577
    :cond_5b
    const/4 v5, 0x1

    #@5c
    iput-boolean v5, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mPaddingChecked:Z

    #@5e
    .line 578
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantPadding:Landroid/graphics/Rect;

    #@60
    goto :goto_6
.end method

.method public final getConstantWidth()I
    .registers 2

    #@0
    .prologue
    .line 590
    iget-boolean v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 591
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->computeConstantSize()V

    #@7
    .line 594
    :cond_7
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@9
    return v0
.end method

.method public final getEnterFadeDuration()I
    .registers 2

    #@0
    .prologue
    .line 646
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@2
    return v0
.end method

.method public final getExitFadeDuration()I
    .registers 2

    #@0
    .prologue
    .line 654
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@2
    return v0
.end method

.method public final getOpacity()I
    .registers 6

    #@0
    .prologue
    .line 658
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildCount()I

    #@3
    move-result v0

    #@4
    .line 659
    .local v0, N:I
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@6
    .line 660
    .local v1, drawables:[Landroid/graphics/drawable/Drawable;
    if-lez v0, :cond_1f

    #@8
    const/4 v4, 0x0

    #@9
    aget-object v4, v1, v4

    #@b
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@e
    move-result v3

    #@f
    .line 661
    .local v3, op:I
    :goto_f
    const/4 v2, 0x1

    #@10
    .local v2, i:I
    :goto_10
    if-ge v2, v0, :cond_21

    #@12
    .line 662
    aget-object v4, v1, v2

    #@14
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@17
    move-result v4

    #@18
    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->resolveOpacity(II)I

    #@1b
    move-result v3

    #@1c
    .line 661
    add-int/lit8 v2, v2, 0x1

    #@1e
    goto :goto_10

    #@1f
    .line 660
    .end local v2           #i:I
    .end local v3           #op:I
    :cond_1f
    const/4 v3, -0x2

    #@20
    goto :goto_f

    #@21
    .line 664
    .restart local v2       #i:I
    .restart local v3       #op:I
    :cond_21
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mOpacity:I

    #@23
    .line 665
    return v3
.end method

.method public growArray(II)V
    .registers 6
    .parameter "oldSize"
    .parameter "newSize"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 688
    new-array v0, p2, [Landroid/graphics/drawable/Drawable;

    #@3
    .line 689
    .local v0, newDrawables:[Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@5
    invoke-static {v1, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@8
    .line 690
    iput-object v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@a
    .line 691
    return-void
.end method

.method public final isConstantSize()Z
    .registers 2

    #@0
    .prologue
    .line 586
    iget-boolean v0, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantSize:Z

    #@2
    return v0
.end method

.method public final isStateful()Z
    .registers 5

    #@0
    .prologue
    .line 669
    iget-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@2
    if-eqz v3, :cond_7

    #@4
    .line 670
    iget-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mStateful:Z

    #@6
    .line 684
    :goto_6
    return v2

    #@7
    .line 673
    :cond_7
    const/4 v2, 0x0

    #@8
    .line 674
    .local v2, stateful:Z
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildCount()I

    #@b
    move-result v0

    #@c
    .line 675
    .local v0, N:I
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_1a

    #@f
    .line 676
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@11
    aget-object v3, v3, v1

    #@13
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@16
    move-result v3

    #@17
    if-eqz v3, :cond_20

    #@19
    .line 677
    const/4 v2, 0x1

    #@1a
    .line 682
    :cond_1a
    iput-boolean v2, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mStateful:Z

    #@1c
    .line 683
    const/4 v3, 0x1

    #@1d
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mHaveStateful:Z

    #@1f
    goto :goto_6

    #@20
    .line 675
    :cond_20
    add-int/lit8 v1, v1, 0x1

    #@22
    goto :goto_d
.end method

.method public final setConstantSize(Z)V
    .registers 2
    .parameter "constant"

    #@0
    .prologue
    .line 582
    iput-boolean p1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantSize:Z

    #@2
    .line 583
    return-void
.end method

.method public final setEnterFadeDuration(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 642
    iput p1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@2
    .line 643
    return-void
.end method

.method public final setExitFadeDuration(I)V
    .registers 2
    .parameter "duration"

    #@0
    .prologue
    .line 650
    iput p1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@2
    .line 651
    return-void
.end method

.method public final setVariablePadding(Z)V
    .registers 2
    .parameter "variable"

    #@0
    .prologue
    .line 553
    iput-boolean p1, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mVariablePadding:Z

    #@2
    .line 554
    return-void
.end method
