.class public Landroid/graphics/drawable/TransitionDrawable;
.super Landroid/graphics/drawable/LayerDrawable;
.source "TransitionDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/TransitionDrawable$1;,
        Landroid/graphics/drawable/TransitionDrawable$TransitionState;
    }
.end annotation


# static fields
.field private static final TRANSITION_NONE:I = 0x2

.field private static final TRANSITION_RUNNING:I = 0x1

.field private static final TRANSITION_STARTING:I


# instance fields
.field private mAlpha:I

.field private mCrossFade:Z

.field private mDuration:I

.field private mFrom:I

.field private mOriginalDuration:I

.field private mReverse:Z

.field private mStartTimeMillis:J

.field private mTo:I

.field private mTransitionState:I


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 88
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable$TransitionState;

    #@3
    invoke-direct {v1, v0, v0, v0}, Landroid/graphics/drawable/TransitionDrawable$TransitionState;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/graphics/drawable/TransitionDrawable;Landroid/content/res/Resources;)V

    #@6
    check-cast v0, Landroid/content/res/Resources;

    #@8
    invoke-direct {p0, v1, v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/content/res/Resources;)V

    #@b
    .line 89
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/content/res/Resources;)V
    .registers 4
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/LayerDrawable;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V

    #@3
    .line 62
    const/4 v0, 0x2

    #@4
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@6
    .line 70
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@9
    .line 93
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/content/res/Resources;Landroid/graphics/drawable/TransitionDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/TransitionDrawable;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;[Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "state"
    .parameter "layers"

    #@0
    .prologue
    .line 96
    invoke-direct {p0, p2, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/LayerDrawable$LayerState;)V

    #@3
    .line 62
    const/4 v0, 0x2

    #@4
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@6
    .line 70
    const/4 v0, 0x0

    #@7
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@9
    .line 97
    return-void
.end method

.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "layers"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 78
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable$TransitionState;

    #@3
    invoke-direct {v0, v1, v1, v1}, Landroid/graphics/drawable/TransitionDrawable$TransitionState;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/graphics/drawable/TransitionDrawable;Landroid/content/res/Resources;)V

    #@6
    invoke-direct {p0, v0, p1}, Landroid/graphics/drawable/TransitionDrawable;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;[Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 79
    return-void
.end method


# virtual methods
.method createConstantState(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)Landroid/graphics/drawable/LayerDrawable$LayerState;
    .registers 4
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 101
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable$TransitionState;

    #@2
    check-cast p1, Landroid/graphics/drawable/TransitionDrawable$TransitionState;

    #@4
    .end local p1
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/TransitionDrawable$TransitionState;-><init>(Landroid/graphics/drawable/TransitionDrawable$TransitionState;Landroid/graphics/drawable/TransitionDrawable;Landroid/content/res/Resources;)V

    #@7
    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter "canvas"

    #@0
    .prologue
    const/high16 v13, 0x3f80

    #@2
    const/16 v12, 0xff

    #@4
    const/4 v7, 0x0

    #@5
    const/4 v6, 0x1

    #@6
    .line 167
    const/4 v4, 0x1

    #@7
    .line 169
    .local v4, done:Z
    iget v8, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@9
    packed-switch v8, :pswitch_data_8c

    #@c
    .line 187
    :cond_c
    :goto_c
    iget v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@e
    .line 188
    .local v0, alpha:I
    iget-boolean v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mCrossFade:Z

    #@10
    .line 189
    .local v2, crossFade:Z
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@12
    iget-object v1, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@14
    .line 191
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    if-eqz v4, :cond_64

    #@16
    .line 194
    if-eqz v2, :cond_1a

    #@18
    if-nez v0, :cond_21

    #@1a
    .line 195
    :cond_1a
    aget-object v7, v1, v7

    #@1c
    iget-object v7, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@21
    .line 197
    :cond_21
    if-ne v0, v12, :cond_2a

    #@23
    .line 198
    aget-object v6, v1, v6

    #@25
    iget-object v6, v6, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@27
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@2a
    .line 223
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 171
    .end local v0           #alpha:I
    .end local v1           #array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v2           #crossFade:Z
    :pswitch_2b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2e
    move-result-wide v8

    #@2f
    iput-wide v8, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@31
    .line 172
    const/4 v4, 0x0

    #@32
    .line 173
    iput v6, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@34
    goto :goto_c

    #@35
    .line 177
    :pswitch_35
    iget-wide v8, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@37
    const-wide/16 v10, 0x0

    #@39
    cmp-long v8, v8, v10

    #@3b
    if-ltz v8, :cond_c

    #@3d
    .line 178
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@40
    move-result-wide v8

    #@41
    iget-wide v10, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@43
    sub-long/2addr v8, v10

    #@44
    long-to-float v8, v8

    #@45
    iget v9, p0, Landroid/graphics/drawable/TransitionDrawable;->mDuration:I

    #@47
    int-to-float v9, v9

    #@48
    div-float v5, v8, v9

    #@4a
    .line 180
    .local v5, normalized:F
    cmpl-float v8, v5, v13

    #@4c
    if-ltz v8, :cond_62

    #@4e
    move v4, v6

    #@4f
    .line 181
    :goto_4f
    invoke-static {v5, v13}, Ljava/lang/Math;->min(FF)F

    #@52
    move-result v5

    #@53
    .line 182
    iget v8, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@55
    int-to-float v8, v8

    #@56
    iget v9, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@58
    iget v10, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@5a
    sub-int/2addr v9, v10

    #@5b
    int-to-float v9, v9

    #@5c
    mul-float/2addr v9, v5

    #@5d
    add-float/2addr v8, v9

    #@5e
    float-to-int v8, v8

    #@5f
    iput v8, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@61
    goto :goto_c

    #@62
    :cond_62
    move v4, v7

    #@63
    .line 180
    goto :goto_4f

    #@64
    .line 204
    .end local v5           #normalized:F
    .restart local v0       #alpha:I
    .restart local v1       #array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .restart local v2       #crossFade:Z
    :cond_64
    aget-object v7, v1, v7

    #@66
    iget-object v3, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@68
    .line 205
    .local v3, d:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_6f

    #@6a
    .line 206
    rsub-int v7, v0, 0xff

    #@6c
    invoke-virtual {v3, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@6f
    .line 208
    :cond_6f
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@72
    .line 209
    if-eqz v2, :cond_77

    #@74
    .line 210
    invoke-virtual {v3, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@77
    .line 213
    :cond_77
    if-lez v0, :cond_86

    #@79
    .line 214
    aget-object v6, v1, v6

    #@7b
    iget-object v3, v6, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@7d
    .line 215
    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@80
    .line 216
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@83
    .line 217
    invoke-virtual {v3, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@86
    .line 220
    :cond_86
    if-nez v4, :cond_2a

    #@88
    .line 221
    invoke-virtual {p0}, Landroid/graphics/drawable/TransitionDrawable;->invalidateSelf()V

    #@8b
    goto :goto_2a

    #@8c
    .line 169
    :pswitch_data_8c
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_35
    .end packed-switch
.end method

.method public isCrossFadeEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 243
    iget-boolean v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mCrossFade:Z

    #@2
    return v0
.end method

.method public resetTransition()V
    .registers 2

    #@0
    .prologue
    .line 123
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@3
    .line 124
    const/4 v0, 0x2

    #@4
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@6
    .line 125
    invoke-virtual {p0}, Landroid/graphics/drawable/TransitionDrawable;->invalidateSelf()V

    #@9
    .line 126
    return-void
.end method

.method public reverseTransition(I)V
    .registers 11
    .parameter "duration"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/16 v4, 0xff

    #@3
    const/4 v3, 0x0

    #@4
    .line 137
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v0

    #@8
    .line 139
    .local v0, time:J
    iget-wide v5, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@a
    sub-long v5, v0, v5

    #@c
    iget v7, p0, Landroid/graphics/drawable/TransitionDrawable;->mDuration:I

    #@e
    int-to-long v7, v7

    #@f
    cmp-long v5, v5, v7

    #@11
    if-lez v5, :cond_32

    #@13
    .line 140
    iget v5, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@15
    if-nez v5, :cond_29

    #@17
    .line 141
    iput v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@19
    .line 142
    iput v4, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@1b
    .line 143
    iput v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@1d
    .line 144
    iput-boolean v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@1f
    .line 151
    :goto_1f
    iput p1, p0, Landroid/graphics/drawable/TransitionDrawable;->mOriginalDuration:I

    #@21
    iput p1, p0, Landroid/graphics/drawable/TransitionDrawable;->mDuration:I

    #@23
    .line 152
    iput v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@25
    .line 153
    invoke-virtual {p0}, Landroid/graphics/drawable/TransitionDrawable;->invalidateSelf()V

    #@28
    .line 163
    :goto_28
    return-void

    #@29
    .line 146
    :cond_29
    iput v4, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@2b
    .line 147
    iput v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@2d
    .line 148
    iput v4, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@2f
    .line 149
    iput-boolean v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@31
    goto :goto_1f

    #@32
    .line 157
    :cond_32
    iget-boolean v5, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@34
    if-nez v5, :cond_51

    #@36
    :goto_36
    iput-boolean v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@38
    .line 158
    iget v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@3a
    iput v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@3c
    .line 159
    iget-boolean v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@3e
    if-eqz v2, :cond_53

    #@40
    move v2, v3

    #@41
    :goto_41
    iput v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@43
    .line 160
    iget-boolean v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@45
    if-eqz v2, :cond_55

    #@47
    iget-wide v4, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@49
    sub-long v4, v0, v4

    #@4b
    :goto_4b
    long-to-int v2, v4

    #@4c
    iput v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mDuration:I

    #@4e
    .line 162
    iput v3, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@50
    goto :goto_28

    #@51
    :cond_51
    move v2, v3

    #@52
    .line 157
    goto :goto_36

    #@53
    :cond_53
    move v2, v4

    #@54
    .line 159
    goto :goto_41

    #@55
    .line 160
    :cond_55
    iget v2, p0, Landroid/graphics/drawable/TransitionDrawable;->mOriginalDuration:I

    #@57
    int-to-long v4, v2

    #@58
    iget-wide v6, p0, Landroid/graphics/drawable/TransitionDrawable;->mStartTimeMillis:J

    #@5a
    sub-long v6, v0, v6

    #@5c
    sub-long/2addr v4, v6

    #@5d
    goto :goto_4b
.end method

.method public setCrossFadeEnabled(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 234
    iput-boolean p1, p0, Landroid/graphics/drawable/TransitionDrawable;->mCrossFade:Z

    #@2
    .line 235
    return-void
.end method

.method public startTransition(I)V
    .registers 4
    .parameter "durationMillis"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 110
    iput v1, p0, Landroid/graphics/drawable/TransitionDrawable;->mFrom:I

    #@3
    .line 111
    const/16 v0, 0xff

    #@5
    iput v0, p0, Landroid/graphics/drawable/TransitionDrawable;->mTo:I

    #@7
    .line 112
    iput v1, p0, Landroid/graphics/drawable/TransitionDrawable;->mAlpha:I

    #@9
    .line 113
    iput p1, p0, Landroid/graphics/drawable/TransitionDrawable;->mOriginalDuration:I

    #@b
    iput p1, p0, Landroid/graphics/drawable/TransitionDrawable;->mDuration:I

    #@d
    .line 114
    iput-boolean v1, p0, Landroid/graphics/drawable/TransitionDrawable;->mReverse:Z

    #@f
    .line 115
    iput v1, p0, Landroid/graphics/drawable/TransitionDrawable;->mTransitionState:I

    #@11
    .line 116
    invoke-virtual {p0}, Landroid/graphics/drawable/TransitionDrawable;->invalidateSelf()V

    #@14
    .line 117
    return-void
.end method
