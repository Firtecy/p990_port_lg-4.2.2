.class public Landroid/graphics/drawable/NinePatchDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "NinePatchDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/NinePatchDrawable$1;,
        Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;
    }
.end annotation


# static fields
.field private static final DEFAULT_DITHER:Z = true


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mLayoutInsets:Landroid/graphics/Insets;

.field private mMutated:Z

.field private mNinePatch:Landroid/graphics/NinePatch;

.field private mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

.field private mPadding:Landroid/graphics/Rect;

.field private mPaint:Landroid/graphics/Paint;

.field private mTargetDensity:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 59
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@5
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mLayoutInsets:Landroid/graphics/Insets;

    #@7
    .line 63
    const/16 v0, 0xa0

    #@9
    iput v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@b
    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Landroid/graphics/Rect;Ljava/lang/String;)V
    .registers 9
    .parameter "res"
    .parameter "bitmap"
    .parameter "chunk"
    .parameter "padding"
    .parameter "layoutInsets"
    .parameter "srcName"

    #@0
    .prologue
    .line 100
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    new-instance v1, Landroid/graphics/NinePatch;

    #@4
    invoke-direct {v1, p2, p3, p6}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    #@7
    invoke-direct {v0, v1, p4, p5}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@a
    invoke-direct {p0, v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@d
    .line 101
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@f
    iget v1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@11
    iput v1, v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mTargetDensity:I

    #@13
    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V
    .registers 8
    .parameter "res"
    .parameter "bitmap"
    .parameter "chunk"
    .parameter "padding"
    .parameter "srcName"

    #@0
    .prologue
    .line 88
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    new-instance v1, Landroid/graphics/NinePatch;

    #@4
    invoke-direct {v1, p2, p3, p5}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    #@7
    invoke-direct {v0, v1, p4}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;)V

    #@a
    invoke-direct {p0, v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@d
    .line 89
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@f
    iget v1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@11
    iput v1, v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mTargetDensity:I

    #@13
    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/NinePatch;)V
    .registers 5
    .parameter "res"
    .parameter "patch"

    #@0
    .prologue
    .line 119
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    new-instance v1, Landroid/graphics/Rect;

    #@4
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@7
    invoke-direct {v0, p2, v1}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;)V

    #@a
    invoke-direct {p0, v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@d
    .line 120
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@f
    iget v1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@11
    iput v1, v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mTargetDensity:I

    #@13
    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V
    .registers 7
    .parameter "bitmap"
    .parameter "chunk"
    .parameter "padding"
    .parameter "srcName"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 79
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    new-instance v1, Landroid/graphics/NinePatch;

    #@4
    invoke-direct {v1, p1, p2, p4}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    #@7
    invoke-direct {v0, v1, p3}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;)V

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {p0, v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@e
    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/graphics/NinePatch;)V
    .registers 4
    .parameter "patch"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 111
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    new-instance v1, Landroid/graphics/Rect;

    #@4
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@7
    invoke-direct {v0, p1, v1}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;)V

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {p0, v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@e
    .line 112
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V
    .registers 4
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 453
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 59
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@5
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mLayoutInsets:Landroid/graphics/Insets;

    #@7
    .line 63
    const/16 v0, 0xa0

    #@9
    iput v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@b
    .line 454
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/NinePatchDrawable;->setNinePatchState(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@e
    .line 455
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;Landroid/graphics/drawable/NinePatchDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method

.method private computeBitmapSize()V
    .registers 6

    #@0
    .prologue
    .line 193
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@2
    invoke-virtual {v4}, Landroid/graphics/NinePatch;->getDensity()I

    #@5
    move-result v1

    #@6
    .line 194
    .local v1, sdensity:I
    iget v3, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@8
    .line 195
    .local v3, tdensity:I
    if-ne v1, v3, :cond_21

    #@a
    .line 196
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@c
    invoke-virtual {v4}, Landroid/graphics/NinePatch;->getWidth()I

    #@f
    move-result v4

    #@10
    iput v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapWidth:I

    #@12
    .line 197
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@14
    invoke-virtual {v4}, Landroid/graphics/NinePatch;->getHeight()I

    #@17
    move-result v4

    #@18
    iput v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapHeight:I

    #@1a
    .line 198
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@1c
    iget-object v4, v4, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mLayoutInsets:Landroid/graphics/Insets;

    #@1e
    iput-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mLayoutInsets:Landroid/graphics/Insets;

    #@20
    .line 217
    :goto_20
    return-void

    #@21
    .line 200
    :cond_21
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@23
    invoke-virtual {v4}, Landroid/graphics/NinePatch;->getWidth()I

    #@26
    move-result v4

    #@27
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@2a
    move-result v4

    #@2b
    iput v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapWidth:I

    #@2d
    .line 202
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@2f
    invoke-virtual {v4}, Landroid/graphics/NinePatch;->getHeight()I

    #@32
    move-result v4

    #@33
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@36
    move-result v4

    #@37
    iput v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapHeight:I

    #@39
    .line 204
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@3b
    iget-object v4, v4, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mPadding:Landroid/graphics/Rect;

    #@3d
    if-eqz v4, :cond_72

    #@3f
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPadding:Landroid/graphics/Rect;

    #@41
    if-eqz v4, :cond_72

    #@43
    .line 205
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPadding:Landroid/graphics/Rect;

    #@45
    .line 206
    .local v0, dest:Landroid/graphics/Rect;
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@47
    iget-object v2, v4, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mPadding:Landroid/graphics/Rect;

    #@49
    .line 207
    .local v2, src:Landroid/graphics/Rect;
    if-ne v0, v2, :cond_52

    #@4b
    .line 208
    new-instance v0, Landroid/graphics/Rect;

    #@4d
    .end local v0           #dest:Landroid/graphics/Rect;
    invoke-direct {v0, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@50
    .restart local v0       #dest:Landroid/graphics/Rect;
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPadding:Landroid/graphics/Rect;

    #@52
    .line 210
    :cond_52
    iget v4, v2, Landroid/graphics/Rect;->left:I

    #@54
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@57
    move-result v4

    #@58
    iput v4, v0, Landroid/graphics/Rect;->left:I

    #@5a
    .line 211
    iget v4, v2, Landroid/graphics/Rect;->top:I

    #@5c
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@5f
    move-result v4

    #@60
    iput v4, v0, Landroid/graphics/Rect;->top:I

    #@62
    .line 212
    iget v4, v2, Landroid/graphics/Rect;->right:I

    #@64
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@67
    move-result v4

    #@68
    iput v4, v0, Landroid/graphics/Rect;->right:I

    #@6a
    .line 213
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    #@6c
    invoke-static {v4, v1, v3}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@6f
    move-result v4

    #@70
    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    #@72
    .line 215
    .end local v0           #dest:Landroid/graphics/Rect;
    .end local v2           #src:Landroid/graphics/Rect;
    :cond_72
    iget-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@74
    iget-object v4, v4, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mLayoutInsets:Landroid/graphics/Insets;

    #@76
    invoke-static {v4, v1, v3}, Landroid/graphics/drawable/NinePatchDrawable;->scaleFromDensity(Landroid/graphics/Insets;II)Landroid/graphics/Insets;

    #@79
    move-result-object v4

    #@7a
    iput-object v4, p0, Landroid/graphics/drawable/NinePatchDrawable;->mLayoutInsets:Landroid/graphics/Insets;

    #@7c
    goto :goto_20
.end method

.method private static scaleFromDensity(Landroid/graphics/Insets;II)Landroid/graphics/Insets;
    .registers 8
    .parameter "insets"
    .parameter "sdensity"
    .parameter "tdensity"

    #@0
    .prologue
    .line 185
    iget v4, p0, Landroid/graphics/Insets;->left:I

    #@2
    invoke-static {v4, p1, p2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@5
    move-result v1

    #@6
    .line 186
    .local v1, left:I
    iget v4, p0, Landroid/graphics/Insets;->top:I

    #@8
    invoke-static {v4, p1, p2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@b
    move-result v3

    #@c
    .line 187
    .local v3, top:I
    iget v4, p0, Landroid/graphics/Insets;->right:I

    #@e
    invoke-static {v4, p1, p2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@11
    move-result v2

    #@12
    .line 188
    .local v2, right:I
    iget v4, p0, Landroid/graphics/Insets;->bottom:I

    #@14
    invoke-static {v4, p1, p2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@17
    move-result v0

    #@18
    .line 189
    .local v0, bottom:I
    invoke-static {v1, v3, v2, v0}, Landroid/graphics/Insets;->of(IIII)Landroid/graphics/Insets;

    #@1b
    move-result-object v4

    #@1c
    return-object v4
.end method

.method private setNinePatchState(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V
    .registers 5
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 124
    iput-object p1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    .line 125
    iget-object v0, p1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mNinePatch:Landroid/graphics/NinePatch;

    #@4
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@6
    .line 126
    iget-object v0, p1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mPadding:Landroid/graphics/Rect;

    #@8
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPadding:Landroid/graphics/Rect;

    #@a
    .line 127
    if-eqz p2, :cond_26

    #@c
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@f
    move-result-object v0

    #@10
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@12
    :goto_12
    iput v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@14
    .line 130
    iget-boolean v0, p1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mDither:Z

    #@16
    const/4 v1, 0x1

    #@17
    if-eq v0, v1, :cond_1e

    #@19
    .line 133
    iget-boolean v0, p1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mDither:Z

    #@1b
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setDither(Z)V

    #@1e
    .line 135
    :cond_1e
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@20
    if-eqz v0, :cond_25

    #@22
    .line 136
    invoke-direct {p0}, Landroid/graphics/drawable/NinePatchDrawable;->computeBitmapSize()V

    #@25
    .line 138
    :cond_25
    return-void

    #@26
    .line 127
    :cond_26
    iget v0, p1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mTargetDensity:I

    #@28
    goto :goto_12
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@2
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v1

    #@6
    iget-object v2, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@8
    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@b
    .line 222
    return-void
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 226
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 388
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@2
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getChangingConfigurations()I

    #@5
    move-result v1

    #@6
    iput v1, v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mChangingConfigurations:I

    #@8
    .line 389
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@a
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 358
    iget v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapHeight:I

    #@2
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 350
    iget v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapWidth:I

    #@2
    return v0
.end method

.method public getLayoutInsets()Landroid/graphics/Insets;
    .registers 2

    #@0
    .prologue
    .line 240
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mLayoutInsets:Landroid/graphics/Insets;

    #@2
    return-object v0
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 368
    iget v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapHeight:I

    #@2
    return v0
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 363
    iget v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mBitmapWidth:I

    #@2
    return v0
.end method

.method public getOpacity()I
    .registers 3

    #@0
    .prologue
    .line 377
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@2
    invoke-virtual {v0}, Landroid/graphics/NinePatch;->hasAlpha()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_16

    #@8
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@a
    if-eqz v0, :cond_18

    #@c
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@e
    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    #@11
    move-result v0

    #@12
    const/16 v1, 0xff

    #@14
    if-ge v0, v1, :cond_18

    #@16
    :cond_16
    const/4 v0, -0x3

    #@17
    :goto_17
    return v0

    #@18
    :cond_18
    const/4 v0, -0x1

    #@19
    goto :goto_17
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPadding:Landroid/graphics/Rect;

    #@2
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5
    .line 232
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method public getPaint()Landroid/graphics/Paint;
    .registers 3

    #@0
    .prologue
    .line 338
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_11

    #@4
    .line 339
    new-instance v0, Landroid/graphics/Paint;

    #@6
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    #@9
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@b
    .line 340
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@d
    const/4 v1, 0x1

    #@e
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    #@11
    .line 342
    :cond_11
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@13
    return-object v0
.end method

.method public getTransparentRegion()Landroid/graphics/Region;
    .registers 3

    #@0
    .prologue
    .line 383
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@2
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getBounds()Landroid/graphics/Rect;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/graphics/NinePatch;->getTransparentRegion(Landroid/graphics/Rect;)Landroid/graphics/Region;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 18
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 282
    invoke-super/range {p0 .. p3}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@3
    .line 284
    sget-object v10, Lcom/android/internal/R$styleable;->NinePatchDrawable:[I

    #@5
    move-object/from16 v0, p3

    #@7
    invoke-virtual {p1, v0, v10}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@a
    move-result-object v1

    #@b
    .line 286
    .local v1, a:Landroid/content/res/TypedArray;
    const/4 v10, 0x0

    #@c
    const/4 v11, 0x0

    #@d
    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@10
    move-result v4

    #@11
    .line 287
    .local v4, id:I
    if-nez v4, :cond_30

    #@13
    .line 288
    new-instance v10, Lorg/xmlpull/v1/XmlPullParserException;

    #@15
    new-instance v11, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@1d
    move-result-object v12

    #@1e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v11

    #@22
    const-string v12, ": <nine-patch> requires a valid src attribute"

    #@24
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v11

    #@28
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v11

    #@2c
    invoke-direct {v10, v11}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@2f
    throw v10

    #@30
    .line 292
    :cond_30
    const/4 v10, 0x1

    #@31
    const/4 v11, 0x1

    #@32
    invoke-virtual {v1, v10, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@35
    move-result v3

    #@36
    .line 295
    .local v3, dither:Z
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    #@38
    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@3b
    .line 296
    .local v7, options:Landroid/graphics/BitmapFactory$Options;
    if-eqz v3, :cond_40

    #@3d
    .line 297
    const/4 v10, 0x0

    #@3e
    iput-boolean v10, v7, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    #@40
    .line 299
    :cond_40
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@43
    move-result-object v10

    #@44
    iget v10, v10, Landroid/util/DisplayMetrics;->noncompatDensityDpi:I

    #@46
    iput v10, v7, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@48
    .line 302
    if-eqz p1, :cond_58

    #@4a
    invoke-virtual {p1}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    #@4d
    move-result-object v10

    #@4e
    invoke-virtual {v10}, Landroid/content/res/CompatibilityInfo;->needsSecondaryDpi()Z

    #@51
    move-result v10

    #@52
    if-eqz v10, :cond_58

    #@54
    .line 303
    sget v10, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@56
    iput v10, v7, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@58
    .line 306
    :cond_58
    new-instance v8, Landroid/graphics/Rect;

    #@5a
    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    #@5d
    .line 307
    .local v8, padding:Landroid/graphics/Rect;
    new-instance v6, Landroid/graphics/Rect;

    #@5f
    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    #@62
    .line 308
    .local v6, layoutInsets:Landroid/graphics/Rect;
    const/4 v2, 0x0

    #@63
    .line 311
    .local v2, bitmap:Landroid/graphics/Bitmap;
    :try_start_63
    new-instance v9, Landroid/util/TypedValue;

    #@65
    invoke-direct {v9}, Landroid/util/TypedValue;-><init>()V

    #@68
    .line 312
    .local v9, value:Landroid/util/TypedValue;
    invoke-virtual {p1, v4, v9}, Landroid/content/res/Resources;->openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;

    #@6b
    move-result-object v5

    #@6c
    .line 314
    .local v5, is:Ljava/io/InputStream;
    invoke-static {p1, v9, v5, v8, v7}, Landroid/graphics/BitmapFactory;->decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@6f
    move-result-object v2

    #@70
    .line 316
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_73
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_73} :catch_d2

    #@73
    .line 321
    .end local v5           #is:Ljava/io/InputStream;
    .end local v9           #value:Landroid/util/TypedValue;
    :goto_73
    if-nez v2, :cond_92

    #@75
    .line 322
    new-instance v10, Lorg/xmlpull/v1/XmlPullParserException;

    #@77
    new-instance v11, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@7f
    move-result-object v12

    #@80
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v11

    #@84
    const-string v12, ": <nine-patch> requires a valid src attribute"

    #@86
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v11

    #@8a
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v11

    #@8e
    invoke-direct {v10, v11}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@91
    throw v10

    #@92
    .line 324
    :cond_92
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    #@95
    move-result-object v10

    #@96
    if-nez v10, :cond_b5

    #@98
    .line 325
    new-instance v10, Lorg/xmlpull/v1/XmlPullParserException;

    #@9a
    new-instance v11, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@a2
    move-result-object v12

    #@a3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v11

    #@a7
    const-string v12, ": <nine-patch> requires a valid 9-patch source image"

    #@a9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v11

    #@ad
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v11

    #@b1
    invoke-direct {v10, v11}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@b4
    throw v10

    #@b5
    .line 329
    :cond_b5
    new-instance v10, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@b7
    new-instance v11, Landroid/graphics/NinePatch;

    #@b9
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    #@bc
    move-result-object v12

    #@bd
    const-string v13, "XML 9-patch"

    #@bf
    invoke-direct {v11, v2, v12, v13}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    #@c2
    invoke-direct {v10, v11, v8, v6, v3}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/NinePatch;Landroid/graphics/Rect;Landroid/graphics/Rect;Z)V

    #@c5
    invoke-direct {p0, v10, p1}, Landroid/graphics/drawable/NinePatchDrawable;->setNinePatchState(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;Landroid/content/res/Resources;)V

    #@c8
    .line 332
    iget-object v10, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@ca
    iget v11, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@cc
    iput v11, v10, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mTargetDensity:I

    #@ce
    .line 334
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    #@d1
    .line 335
    return-void

    #@d2
    .line 317
    :catch_d2
    move-exception v10

    #@d3
    goto :goto_73
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 394
    iget-boolean v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_1c

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_1c

    #@a
    .line 395
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@c
    iget-object v1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@e
    invoke-direct {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;-><init>(Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;)V

    #@11
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@13
    .line 396
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatchState:Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;

    #@15
    iget-object v0, v0, Landroid/graphics/drawable/NinePatchDrawable$NinePatchState;->mNinePatch:Landroid/graphics/NinePatch;

    #@17
    iput-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@19
    .line 397
    const/4 v0, 0x1

    #@1a
    iput-boolean v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mMutated:Z

    #@1c
    .line 399
    :cond_1c
    return-object p0
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_9

    #@4
    const/16 v0, 0xff

    #@6
    if-ne p1, v0, :cond_9

    #@8
    .line 251
    :goto_8
    return-void

    #@9
    .line 249
    :cond_9
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    #@10
    .line 250
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->invalidateSelf()V

    #@13
    goto :goto_8
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_7

    #@4
    if-nez p1, :cond_7

    #@6
    .line 261
    :goto_6
    return-void

    #@7
    .line 259
    :cond_7
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@e
    .line 260
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->invalidateSelf()V

    #@11
    goto :goto_6
.end method

.method public setDither(Z)V
    .registers 3
    .parameter "dither"

    #@0
    .prologue
    .line 265
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mPaint:Landroid/graphics/Paint;

    #@2
    if-nez v0, :cond_8

    #@4
    const/4 v0, 0x1

    #@5
    if-ne p1, v0, :cond_8

    #@7
    .line 271
    :goto_7
    return-void

    #@8
    .line 269
    :cond_8
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    #@f
    .line 270
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->invalidateSelf()V

    #@12
    goto :goto_7
.end method

.method public setFilterBitmap(Z)V
    .registers 3
    .parameter "filter"

    #@0
    .prologue
    .line 275
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@7
    .line 276
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->invalidateSelf()V

    #@a
    .line 277
    return-void
.end method

.method public setTargetDensity(I)V
    .registers 3
    .parameter "density"

    #@0
    .prologue
    .line 175
    iget v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@2
    if-eq p1, v0, :cond_14

    #@4
    .line 176
    if-nez p1, :cond_8

    #@6
    const/16 p1, 0xa0

    #@8
    .end local p1
    :cond_8
    iput p1, p0, Landroid/graphics/drawable/NinePatchDrawable;->mTargetDensity:I

    #@a
    .line 177
    iget-object v0, p0, Landroid/graphics/drawable/NinePatchDrawable;->mNinePatch:Landroid/graphics/NinePatch;

    #@c
    if-eqz v0, :cond_11

    #@e
    .line 178
    invoke-direct {p0}, Landroid/graphics/drawable/NinePatchDrawable;->computeBitmapSize()V

    #@11
    .line 180
    :cond_11
    invoke-virtual {p0}, Landroid/graphics/drawable/NinePatchDrawable;->invalidateSelf()V

    #@14
    .line 182
    :cond_14
    return-void
.end method

.method public setTargetDensity(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getDensity()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setTargetDensity(I)V

    #@7
    .line 152
    return-void
.end method

.method public setTargetDensity(Landroid/util/DisplayMetrics;)V
    .registers 3
    .parameter "metrics"

    #@0
    .prologue
    .line 163
    iget v0, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@2
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setTargetDensity(I)V

    #@5
    .line 164
    return-void
.end method
