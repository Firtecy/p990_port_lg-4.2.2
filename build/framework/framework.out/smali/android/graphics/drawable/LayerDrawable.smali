.class public Landroid/graphics/drawable/LayerDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "LayerDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/LayerDrawable$LayerState;,
        Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    }
.end annotation


# instance fields
.field mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

.field private mMutated:Z

.field private mOpacityOverride:I

.field private mPaddingB:[I

.field private mPaddingL:[I

.field private mPaddingR:[I

.field private mPaddingT:[I

.field private final mTmpRect:Landroid/graphics/Rect;


# direct methods
.method constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 95
    move-object v0, v1

    #@2
    check-cast v0, Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@4
    invoke-direct {p0, v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V

    #@7
    .line 96
    return-void
.end method

.method constructor <init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V
    .registers 5
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 98
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 52
    const/4 v1, 0x0

    #@4
    iput v1, p0, Landroid/graphics/drawable/LayerDrawable;->mOpacityOverride:I

    #@6
    .line 58
    new-instance v1, Landroid/graphics/Rect;

    #@8
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@b
    iput-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mTmpRect:Landroid/graphics/Rect;

    #@d
    .line 99
    invoke-virtual {p0, p1, p2}, Landroid/graphics/drawable/LayerDrawable;->createConstantState(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@10
    move-result-object v0

    #@11
    .line 100
    .local v0, as:Landroid/graphics/drawable/LayerDrawable$LayerState;
    iput-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@13
    .line 101
    iget v1, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@15
    if-lez v1, :cond_1a

    #@17
    .line 102
    invoke-direct {p0}, Landroid/graphics/drawable/LayerDrawable;->ensurePadding()V

    #@1a
    .line 104
    :cond_1a
    return-void
.end method

.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "layers"

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/LayerDrawable$LayerState;)V

    #@4
    .line 68
    return-void
.end method

.method constructor <init>([Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/LayerDrawable$LayerState;)V
    .registers 9
    .parameter "layers"
    .parameter "state"

    #@0
    .prologue
    .line 78
    const/4 v3, 0x0

    #@1
    invoke-direct {p0, p2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)V

    #@4
    .line 79
    array-length v1, p1

    #@5
    .line 80
    .local v1, length:I
    new-array v2, v1, [Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@7
    .line 82
    .local v2, r:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    const/4 v0, 0x0

    #@8
    .local v0, i:I
    :goto_8
    if-ge v0, v1, :cond_2c

    #@a
    .line 83
    new-instance v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@c
    invoke-direct {v3}, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;-><init>()V

    #@f
    aput-object v3, v2, v0

    #@11
    .line 84
    aget-object v3, v2, v0

    #@13
    aget-object v4, p1, v0

    #@15
    iput-object v4, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@17
    .line 85
    aget-object v3, p1, v0

    #@19
    invoke-virtual {v3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@1c
    .line 86
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@1e
    iget v4, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@20
    aget-object v5, p1, v0

    #@22
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@25
    move-result v5

    #@26
    or-int/2addr v4, v5

    #@27
    iput v4, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@29
    .line 82
    add-int/lit8 v0, v0, 0x1

    #@2b
    goto :goto_8

    #@2c
    .line 88
    :cond_2c
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2e
    iput v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@30
    .line 89
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@32
    iput-object v2, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@34
    .line 91
    invoke-direct {p0}, Landroid/graphics/drawable/LayerDrawable;->ensurePadding()V

    #@37
    .line 92
    return-void
.end method

.method private addLayer(Landroid/graphics/drawable/Drawable;IIIII)V
    .registers 15
    .parameter "layer"
    .parameter "id"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 186
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@3
    .line 187
    .local v4, st:Landroid/graphics/drawable/LayerDrawable$LayerState;
    iget-object v6, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@5
    if-eqz v6, :cond_45

    #@7
    iget-object v6, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@9
    array-length v0, v6

    #@a
    .line 188
    .local v0, N:I
    :goto_a
    iget v2, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@c
    .line 189
    .local v2, i:I
    if-lt v2, v0, :cond_1b

    #@e
    .line 190
    add-int/lit8 v6, v0, 0xa

    #@10
    new-array v3, v6, [Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@12
    .line 191
    .local v3, nu:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    if-lez v2, :cond_19

    #@14
    .line 192
    iget-object v6, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@16
    invoke-static {v6, v5, v3, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@19
    .line 194
    :cond_19
    iput-object v3, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@1b
    .line 197
    .end local v3           #nu:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_1b
    iget-object v5, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@1d
    iget v6, v5, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@1f
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@22
    move-result v7

    #@23
    or-int/2addr v6, v7

    #@24
    iput v6, v5, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@26
    .line 199
    new-instance v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@28
    invoke-direct {v1}, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;-><init>()V

    #@2b
    .line 200
    .local v1, childDrawable:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v5, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@2d
    aput-object v1, v5, v2

    #@2f
    .line 201
    iput p2, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@31
    .line 202
    iput-object p1, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@33
    .line 203
    iput p3, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@35
    .line 204
    iput p4, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@37
    .line 205
    iput p5, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@39
    .line 206
    iput p6, v1, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@3b
    .line 207
    iget v5, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@3d
    add-int/lit8 v5, v5, 0x1

    #@3f
    iput v5, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@41
    .line 209
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@44
    .line 210
    return-void

    #@45
    .end local v0           #N:I
    .end local v1           #childDrawable:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v2           #i:I
    :cond_45
    move v0, v5

    #@46
    .line 187
    goto :goto_a
.end method

.method private ensurePadding()V
    .registers 3

    #@0
    .prologue
    .line 556
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget v0, v1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@4
    .line 557
    .local v0, N:I
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@6
    if-eqz v1, :cond_e

    #@8
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@a
    array-length v1, v1

    #@b
    if-lt v1, v0, :cond_e

    #@d
    .line 564
    :goto_d
    return-void

    #@e
    .line 560
    :cond_e
    new-array v1, v0, [I

    #@10
    iput-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@12
    .line 561
    new-array v1, v0, [I

    #@14
    iput-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@16
    .line 562
    new-array v1, v0, [I

    #@18
    iput-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@1a
    .line 563
    new-array v1, v0, [I

    #@1c
    iput-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@1e
    goto :goto_d
.end method

.method private reapplyPadding(ILandroid/graphics/drawable/LayerDrawable$ChildDrawable;)Z
    .registers 6
    .parameter "i"
    .parameter "r"

    #@0
    .prologue
    .line 542
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mTmpRect:Landroid/graphics/Rect;

    #@2
    .line 543
    .local v0, rect:Landroid/graphics/Rect;
    iget-object v1, p2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@7
    .line 544
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@9
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@b
    aget v2, v2, p1

    #@d
    if-ne v1, v2, :cond_27

    #@f
    iget v1, v0, Landroid/graphics/Rect;->top:I

    #@11
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@13
    aget v2, v2, p1

    #@15
    if-ne v1, v2, :cond_27

    #@17
    iget v1, v0, Landroid/graphics/Rect;->right:I

    #@19
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@1b
    aget v2, v2, p1

    #@1d
    if-ne v1, v2, :cond_27

    #@1f
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    #@21
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@23
    aget v2, v2, p1

    #@25
    if-eq v1, v2, :cond_41

    #@27
    .line 546
    :cond_27
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@29
    iget v2, v0, Landroid/graphics/Rect;->left:I

    #@2b
    aput v2, v1, p1

    #@2d
    .line 547
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@2f
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@31
    aput v2, v1, p1

    #@33
    .line 548
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@35
    iget v2, v0, Landroid/graphics/Rect;->right:I

    #@37
    aput v2, v1, p1

    #@39
    .line 549
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@3b
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    #@3d
    aput v2, v1, p1

    #@3f
    .line 550
    const/4 v1, 0x1

    #@40
    .line 552
    :goto_40
    return v1

    #@41
    :cond_41
    const/4 v1, 0x0

    #@42
    goto :goto_40
.end method


# virtual methods
.method createConstantState(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/content/res/Resources;)Landroid/graphics/drawable/LayerDrawable$LayerState;
    .registers 4
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 107
    new-instance v0, Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/LayerDrawable$LayerState;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/graphics/drawable/LayerDrawable;Landroid/content/res/Resources;)V

    #@5
    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    #@0
    .prologue
    .line 342
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 343
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 344
    .local v0, N:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_15

    #@b
    .line 345
    aget-object v3, v1, v2

    #@d
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@12
    .line 344
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_9

    #@15
    .line 347
    :cond_15
    return-void
.end method

.method public findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 219
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v2, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 221
    .local v1, layers:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v2, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v2, v2, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    add-int/lit8 v0, v2, -0x1

    #@a
    .local v0, i:I
    :goto_a
    if-ltz v0, :cond_1a

    #@c
    .line 222
    aget-object v2, v1, v0

    #@e
    iget v2, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@10
    if-ne v2, p1, :cond_17

    #@12
    .line 223
    aget-object v2, v1, v0

    #@14
    iget-object v2, v2, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    .line 227
    :goto_16
    return-object v2

    #@17
    .line 221
    :cond_17
    add-int/lit8 v0, v0, -0x1

    #@19
    goto :goto_a

    #@1a
    .line 227
    :cond_1a
    const/4 v2, 0x0

    #@1b
    goto :goto_16
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 351
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@b
    iget v1, v1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildrenChangingConfigurations:I

    #@d
    or-int/2addr v0, v1

    #@e
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable$LayerState;->canConstantState()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 569
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@a
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getChangingConfigurations()I

    #@d
    move-result v1

    #@e
    iput v1, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChangingConfigurations:I

    #@10
    .line 570
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@12
    .line 572
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public getDrawable(I)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 256
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    aget-object v0, v0, p1

    #@6
    iget-object v0, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    return-object v0
.end method

.method public getId(I)I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 267
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    aget-object v0, v0, p1

    #@6
    iget v0, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@8
    return v0
.end method

.method public getIntrinsicHeight()I
    .registers 11

    #@0
    .prologue
    .line 525
    const/4 v3, -0x1

    #@1
    .line 526
    .local v3, height:I
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@3
    iget-object v1, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@5
    .line 527
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@7
    iget v0, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@9
    .line 528
    .local v0, N:I
    const/4 v6, 0x0

    #@a
    .local v6, padT:I
    const/4 v5, 0x0

    #@b
    .line 529
    .local v5, padB:I
    const/4 v4, 0x0

    #@c
    .local v4, i:I
    :goto_c
    if-ge v4, v0, :cond_2f

    #@e
    .line 530
    aget-object v7, v1, v4

    #@10
    .line 531
    .local v7, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@15
    move-result v8

    #@16
    iget v9, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@18
    add-int/2addr v8, v9

    #@19
    iget v9, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@1b
    add-int/2addr v8, v9

    #@1c
    add-int/2addr v8, v6

    #@1d
    add-int v2, v8, v5

    #@1f
    .line 532
    .local v2, h:I
    if-le v2, v3, :cond_22

    #@21
    .line 533
    move v3, v2

    #@22
    .line 535
    :cond_22
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@24
    aget v8, v8, v4

    #@26
    add-int/2addr v6, v8

    #@27
    .line 536
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@29
    aget v8, v8, v4

    #@2b
    add-int/2addr v5, v8

    #@2c
    .line 529
    add-int/lit8 v4, v4, 0x1

    #@2e
    goto :goto_c

    #@2f
    .line 538
    .end local v2           #h:I
    .end local v7           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_2f
    return v3
.end method

.method public getIntrinsicWidth()I
    .registers 11

    #@0
    .prologue
    .line 506
    const/4 v7, -0x1

    #@1
    .line 507
    .local v7, width:I
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@3
    iget-object v1, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@5
    .line 508
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@7
    iget v0, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@9
    .line 509
    .local v0, N:I
    const/4 v3, 0x0

    #@a
    .local v3, padL:I
    const/4 v4, 0x0

    #@b
    .line 510
    .local v4, padR:I
    const/4 v2, 0x0

    #@c
    .local v2, i:I
    :goto_c
    if-ge v2, v0, :cond_2f

    #@e
    .line 511
    aget-object v5, v1, v2

    #@10
    .line 512
    .local v5, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, v5, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@15
    move-result v8

    #@16
    iget v9, v5, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@18
    add-int/2addr v8, v9

    #@19
    iget v9, v5, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@1b
    add-int/2addr v8, v9

    #@1c
    add-int/2addr v8, v3

    #@1d
    add-int v6, v8, v4

    #@1f
    .line 514
    .local v6, w:I
    if-le v6, v7, :cond_22

    #@21
    .line 515
    move v7, v6

    #@22
    .line 517
    :cond_22
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@24
    aget v8, v8, v2

    #@26
    add-int/2addr v3, v8

    #@27
    .line 518
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@29
    aget v8, v8, v2

    #@2b
    add-int/2addr v4, v8

    #@2c
    .line 510
    add-int/lit8 v2, v2, 0x1

    #@2e
    goto :goto_c

    #@2f
    .line 520
    .end local v5           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v6           #w:I
    :cond_2f
    return v7
.end method

.method public getNumberOfLayers()I
    .registers 2

    #@0
    .prologue
    .line 245
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget v0, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@4
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 433
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable;->mOpacityOverride:I

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 434
    iget v0, p0, Landroid/graphics/drawable/LayerDrawable;->mOpacityOverride:I

    #@6
    .line 436
    :goto_6
    return v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable$LayerState;->getOpacity()I

    #@c
    move-result v0

    #@d
    goto :goto_6
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 7
    .parameter "padding"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 361
    iput v3, p1, Landroid/graphics/Rect;->left:I

    #@3
    .line 362
    iput v3, p1, Landroid/graphics/Rect;->top:I

    #@5
    .line 363
    iput v3, p1, Landroid/graphics/Rect;->right:I

    #@7
    .line 364
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    #@9
    .line 365
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@b
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@d
    .line 366
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@f
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@11
    .line 367
    .local v0, N:I
    const/4 v2, 0x0

    #@12
    .local v2, i:I
    :goto_12
    if-ge v2, v0, :cond_40

    #@14
    .line 368
    aget-object v3, v1, v2

    #@16
    invoke-direct {p0, v2, v3}, Landroid/graphics/drawable/LayerDrawable;->reapplyPadding(ILandroid/graphics/drawable/LayerDrawable$ChildDrawable;)Z

    #@19
    .line 369
    iget v3, p1, Landroid/graphics/Rect;->left:I

    #@1b
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@1d
    aget v4, v4, v2

    #@1f
    add-int/2addr v3, v4

    #@20
    iput v3, p1, Landroid/graphics/Rect;->left:I

    #@22
    .line 370
    iget v3, p1, Landroid/graphics/Rect;->top:I

    #@24
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@26
    aget v4, v4, v2

    #@28
    add-int/2addr v3, v4

    #@29
    iput v3, p1, Landroid/graphics/Rect;->top:I

    #@2b
    .line 371
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@2d
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@2f
    aget v4, v4, v2

    #@31
    add-int/2addr v3, v4

    #@32
    iput v3, p1, Landroid/graphics/Rect;->right:I

    #@34
    .line 372
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@36
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@38
    aget v4, v4, v2

    #@3a
    add-int/2addr v3, v4

    #@3b
    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    #@3d
    .line 367
    add-int/lit8 v2, v2, 0x1

    #@3f
    goto :goto_12

    #@40
    .line 374
    :cond_40
    const/4 v3, 0x1

    #@41
    return v3
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 20
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 113
    invoke-super/range {p0 .. p3}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@3
    .line 117
    sget-object v2, Lcom/android/internal/R$styleable;->LayerDrawable:[I

    #@5
    move-object/from16 v0, p1

    #@7
    move-object/from16 v1, p3

    #@9
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@c
    move-result-object v9

    #@d
    .line 119
    .local v9, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    #@e
    const/4 v14, 0x0

    #@f
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getInt(II)I

    #@12
    move-result v2

    #@13
    move-object/from16 v0, p0

    #@15
    iput v2, v0, Landroid/graphics/drawable/LayerDrawable;->mOpacityOverride:I

    #@17
    .line 122
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    #@1a
    .line 124
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@1d
    move-result v2

    #@1e
    add-int/lit8 v12, v2, 0x1

    #@20
    .line 127
    .local v12, innerDepth:I
    :cond_20
    :goto_20
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@23
    move-result v13

    #@24
    .local v13, type:I
    const/4 v2, 0x1

    #@25
    if-eq v13, v2, :cond_b3

    #@27
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@2a
    move-result v10

    #@2b
    .local v10, depth:I
    if-ge v10, v12, :cond_30

    #@2d
    const/4 v2, 0x3

    #@2e
    if-eq v13, v2, :cond_b3

    #@30
    .line 128
    :cond_30
    const/4 v2, 0x2

    #@31
    if-ne v13, v2, :cond_20

    #@33
    .line 132
    if-gt v10, v12, :cond_20

    #@35
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@38
    move-result-object v2

    #@39
    const-string/jumbo v14, "item"

    #@3c
    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3f
    move-result v2

    #@40
    if-eqz v2, :cond_20

    #@42
    .line 136
    sget-object v2, Lcom/android/internal/R$styleable;->LayerDrawableItem:[I

    #@44
    move-object/from16 v0, p1

    #@46
    move-object/from16 v1, p3

    #@48
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4b
    move-result-object v9

    #@4c
    .line 139
    const/4 v2, 0x2

    #@4d
    const/4 v14, 0x0

    #@4e
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@51
    move-result v5

    #@52
    .line 141
    .local v5, left:I
    const/4 v2, 0x3

    #@53
    const/4 v14, 0x0

    #@54
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@57
    move-result v6

    #@58
    .line 143
    .local v6, top:I
    const/4 v2, 0x4

    #@59
    const/4 v14, 0x0

    #@5a
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@5d
    move-result v7

    #@5e
    .line 145
    .local v7, right:I
    const/4 v2, 0x5

    #@5f
    const/4 v14, 0x0

    #@60
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@63
    move-result v8

    #@64
    .line 147
    .local v8, bottom:I
    const/4 v2, 0x1

    #@65
    const/4 v14, 0x0

    #@66
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@69
    move-result v11

    #@6a
    .line 149
    .local v11, drawableRes:I
    const/4 v2, 0x0

    #@6b
    const/4 v14, -0x1

    #@6c
    invoke-virtual {v9, v2, v14}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@6f
    move-result v4

    #@70
    .line 152
    .local v4, id:I
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    #@73
    .line 155
    if-eqz v11, :cond_81

    #@75
    .line 156
    move-object/from16 v0, p1

    #@77
    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7a
    move-result-object v3

    #@7b
    .local v3, dr:Landroid/graphics/drawable/Drawable;
    :goto_7b
    move-object/from16 v2, p0

    #@7d
    .line 168
    invoke-direct/range {v2 .. v8}, Landroid/graphics/drawable/LayerDrawable;->addLayer(Landroid/graphics/drawable/Drawable;IIIII)V

    #@80
    goto :goto_20

    #@81
    .line 158
    .end local v3           #dr:Landroid/graphics/drawable/Drawable;
    :cond_81
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@84
    move-result v13

    #@85
    const/4 v2, 0x4

    #@86
    if-eq v13, v2, :cond_81

    #@88
    .line 160
    const/4 v2, 0x2

    #@89
    if-eq v13, v2, :cond_ae

    #@8b
    .line 161
    new-instance v2, Lorg/xmlpull/v1/XmlPullParserException;

    #@8d
    new-instance v14, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@95
    move-result-object v15

    #@96
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v14

    #@9a
    const-string v15, ": <item> tag requires a \'drawable\' attribute or "

    #@9c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v14

    #@a0
    const-string v15, "child tag defining a drawable"

    #@a2
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v14

    #@a6
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v14

    #@aa
    invoke-direct {v2, v14}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@ad
    throw v2

    #@ae
    .line 165
    :cond_ae
    invoke-static/range {p1 .. p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@b1
    move-result-object v3

    #@b2
    .restart local v3       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_7b

    #@b3
    .line 171
    .end local v3           #dr:Landroid/graphics/drawable/Drawable;
    .end local v4           #id:I
    .end local v5           #left:I
    .end local v6           #top:I
    .end local v7           #right:I
    .end local v8           #bottom:I
    .end local v10           #depth:I
    .end local v11           #drawableRes:I
    :cond_b3
    invoke-direct/range {p0 .. p0}, Landroid/graphics/drawable/LayerDrawable;->ensurePadding()V

    #@b6
    .line 172
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/LayerDrawable;->getState()[I

    #@b9
    move-result-object v2

    #@ba
    move-object/from16 v0, p0

    #@bc
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    #@bf
    .line 173
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 318
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 319
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 320
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 322
    :cond_9
    return-void
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 441
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable$LayerState;->isStateful()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 7

    #@0
    .prologue
    .line 577
    iget-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable;->mMutated:Z

    #@2
    if-nez v3, :cond_2c

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v3

    #@8
    if-ne v3, p0, :cond_2c

    #@a
    .line 578
    new-instance v3, Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@c
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@e
    const/4 v5, 0x0

    #@f
    invoke-direct {v3, v4, p0, v5}, Landroid/graphics/drawable/LayerDrawable$LayerState;-><init>(Landroid/graphics/drawable/LayerDrawable$LayerState;Landroid/graphics/drawable/LayerDrawable;Landroid/content/res/Resources;)V

    #@12
    iput-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@14
    .line 579
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@16
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@18
    .line 580
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@1a
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@1c
    .line 581
    .local v0, N:I
    const/4 v2, 0x0

    #@1d
    .local v2, i:I
    :goto_1d
    if-ge v2, v0, :cond_29

    #@1f
    .line 582
    aget-object v3, v1, v2

    #@21
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@23
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@26
    .line 581
    add-int/lit8 v2, v2, 0x1

    #@28
    goto :goto_1d

    #@29
    .line 584
    :cond_29
    const/4 v3, 0x1

    #@2a
    iput-boolean v3, p0, Landroid/graphics/drawable/LayerDrawable;->mMutated:Z

    #@2c
    .line 586
    .end local v0           #N:I
    .end local v1           #array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v2           #i:I
    :cond_2c
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 16
    .parameter "bounds"

    #@0
    .prologue
    .line 488
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 489
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v8, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 490
    .local v0, N:I
    const/4 v4, 0x0

    #@9
    .local v4, padL:I
    const/4 v6, 0x0

    #@a
    .local v6, padT:I
    const/4 v5, 0x0

    #@b
    .local v5, padR:I
    const/4 v3, 0x0

    #@c
    .line 491
    .local v3, padB:I
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    if-ge v2, v0, :cond_45

    #@f
    .line 492
    aget-object v7, v1, v2

    #@11
    .line 493
    .local v7, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v8, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    iget v9, p1, Landroid/graphics/Rect;->left:I

    #@15
    iget v10, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@17
    add-int/2addr v9, v10

    #@18
    add-int/2addr v9, v4

    #@19
    iget v10, p1, Landroid/graphics/Rect;->top:I

    #@1b
    iget v11, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@1d
    add-int/2addr v10, v11

    #@1e
    add-int/2addr v10, v6

    #@1f
    iget v11, p1, Landroid/graphics/Rect;->right:I

    #@21
    iget v12, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@23
    sub-int/2addr v11, v12

    #@24
    sub-int/2addr v11, v5

    #@25
    iget v12, p1, Landroid/graphics/Rect;->bottom:I

    #@27
    iget v13, v7, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@29
    sub-int/2addr v12, v13

    #@2a
    sub-int/2addr v12, v3

    #@2b
    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2e
    .line 497
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingL:[I

    #@30
    aget v8, v8, v2

    #@32
    add-int/2addr v4, v8

    #@33
    .line 498
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingR:[I

    #@35
    aget v8, v8, v2

    #@37
    add-int/2addr v5, v8

    #@38
    .line 499
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingT:[I

    #@3a
    aget v8, v8, v2

    #@3c
    add-int/2addr v6, v8

    #@3d
    .line 500
    iget-object v8, p0, Landroid/graphics/drawable/LayerDrawable;->mPaddingB:[I

    #@3f
    aget v8, v8, v2

    #@41
    add-int/2addr v3, v8

    #@42
    .line 491
    add-int/lit8 v2, v2, 0x1

    #@44
    goto :goto_d

    #@45
    .line 502
    .end local v7           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_45
    return-void
.end method

.method protected onLevelChange(I)Z
    .registers 9
    .parameter "level"

    #@0
    .prologue
    .line 467
    iget-object v6, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v6, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 468
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v6, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v6, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 469
    .local v0, N:I
    const/4 v4, 0x0

    #@9
    .line 470
    .local v4, paddingChanged:Z
    const/4 v2, 0x0

    #@a
    .line 471
    .local v2, changed:Z
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_22

    #@d
    .line 472
    aget-object v5, v1, v3

    #@f
    .line 473
    .local v5, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v6, v5, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_18

    #@17
    .line 474
    const/4 v2, 0x1

    #@18
    .line 476
    :cond_18
    invoke-direct {p0, v3, v5}, Landroid/graphics/drawable/LayerDrawable;->reapplyPadding(ILandroid/graphics/drawable/LayerDrawable$ChildDrawable;)Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_1f

    #@1e
    .line 477
    const/4 v4, 0x1

    #@1f
    .line 471
    :cond_1f
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_b

    #@22
    .line 480
    .end local v5           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_22
    if-eqz v4, :cond_2b

    #@24
    .line 481
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getBounds()Landroid/graphics/Rect;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/LayerDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@2b
    .line 483
    :cond_2b
    return v2
.end method

.method protected onStateChange([I)Z
    .registers 9
    .parameter "state"

    #@0
    .prologue
    .line 446
    iget-object v6, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v6, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 447
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v6, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v6, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 448
    .local v0, N:I
    const/4 v4, 0x0

    #@9
    .line 449
    .local v4, paddingChanged:Z
    const/4 v2, 0x0

    #@a
    .line 450
    .local v2, changed:Z
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_22

    #@d
    .line 451
    aget-object v5, v1, v3

    #@f
    .line 452
    .local v5, r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v6, v5, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_18

    #@17
    .line 453
    const/4 v2, 0x1

    #@18
    .line 455
    :cond_18
    invoke-direct {p0, v3, v5}, Landroid/graphics/drawable/LayerDrawable;->reapplyPadding(ILandroid/graphics/drawable/LayerDrawable$ChildDrawable;)Z

    #@1b
    move-result v6

    #@1c
    if-eqz v6, :cond_1f

    #@1e
    .line 456
    const/4 v4, 0x1

    #@1f
    .line 450
    :cond_1f
    add-int/lit8 v3, v3, 0x1

    #@21
    goto :goto_b

    #@22
    .line 459
    .end local v5           #r:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    :cond_22
    if-eqz v4, :cond_2b

    #@24
    .line 460
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getBounds()Landroid/graphics/Rect;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/LayerDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@2b
    .line 462
    :cond_2b
    return v2
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 6
    .parameter "who"
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 325
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 326
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 327
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    #@9
    .line 329
    :cond_9
    return-void
.end method

.method public setAlpha(I)V
    .registers 6
    .parameter "alpha"

    #@0
    .prologue
    .line 399
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 400
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 401
    .local v0, N:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_15

    #@b
    .line 402
    aget-object v3, v1, v2

    #@d
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@12
    .line 401
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_9

    #@15
    .line 404
    :cond_15
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 6
    .parameter "cf"

    #@0
    .prologue
    .line 408
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 409
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 410
    .local v0, N:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_15

    #@b
    .line 411
    aget-object v3, v1, v2

    #@d
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@12
    .line 410
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_9

    #@15
    .line 413
    :cond_15
    return-void
.end method

.method public setDither(Z)V
    .registers 6
    .parameter "dither"

    #@0
    .prologue
    .line 390
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 391
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    .line 392
    .local v0, N:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_15

    #@b
    .line 393
    aget-object v3, v1, v2

    #@d
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    #@12
    .line 392
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_9

    #@15
    .line 395
    :cond_15
    return-void
.end method

.method public setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z
    .registers 8
    .parameter "id"
    .parameter "drawable"

    #@0
    .prologue
    .line 279
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v2, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    .line 281
    .local v2, layers:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget v3, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@8
    add-int/lit8 v1, v3, -0x1

    #@a
    .local v1, i:I
    :goto_a
    if-ltz v1, :cond_3b

    #@c
    .line 282
    aget-object v3, v2, v1

    #@e
    iget v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@10
    if-ne v3, p1, :cond_38

    #@12
    .line 283
    aget-object v3, v2, v1

    #@14
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v3, :cond_2d

    #@18
    .line 284
    if-eqz p2, :cond_25

    #@1a
    .line 285
    aget-object v3, v2, v1

    #@1c
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1e
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@21
    move-result-object v0

    #@22
    .line 286
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@25
    .line 288
    .end local v0           #bounds:Landroid/graphics/Rect;
    :cond_25
    aget-object v3, v2, v1

    #@27
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@29
    const/4 v4, 0x0

    #@2a
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@2d
    .line 290
    :cond_2d
    if-eqz p2, :cond_32

    #@2f
    .line 291
    invoke-virtual {p2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@32
    .line 293
    :cond_32
    aget-object v3, v2, v1

    #@34
    iput-object p2, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@36
    .line 294
    const/4 v3, 0x1

    #@37
    .line 298
    :goto_37
    return v3

    #@38
    .line 281
    :cond_38
    add-int/lit8 v1, v1, -0x1

    #@3a
    goto :goto_a

    #@3b
    .line 298
    :cond_3b
    const/4 v3, 0x0

    #@3c
    goto :goto_37
.end method

.method public setId(II)V
    .registers 4
    .parameter "index"
    .parameter "id"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    aget-object v0, v0, p1

    #@6
    iput p2, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mId:I

    #@8
    .line 238
    return-void
.end method

.method public setLayerInset(IIIII)V
    .registers 8
    .parameter "index"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    #@0
    .prologue
    .line 308
    iget-object v1, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@2
    iget-object v1, v1, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@4
    aget-object v0, v1, p1

    #@6
    .line 309
    .local v0, childDrawable:Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iput p2, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetL:I

    #@8
    .line 310
    iput p3, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetT:I

    #@a
    .line 311
    iput p4, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetR:I

    #@c
    .line 312
    iput p5, v0, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mInsetB:I

    #@e
    .line 313
    return-void
.end method

.method public setLayoutDirection(I)V
    .registers 6
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 592
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getLayoutDirection()I

    #@3
    move-result v3

    #@4
    if-eq v3, p1, :cond_1b

    #@6
    .line 593
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@8
    iget-object v1, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@a
    .line 594
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v3, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@c
    iget v0, v3, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@e
    .line 595
    .local v0, N:I
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    if-ge v2, v0, :cond_1b

    #@11
    .line 596
    aget-object v3, v1, v2

    #@13
    iget-object v3, v3, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@18
    .line 595
    add-int/lit8 v2, v2, 0x1

    #@1a
    goto :goto_f

    #@1b
    .line 599
    .end local v0           #N:I
    .end local v1           #array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    .end local v2           #i:I
    :cond_1b
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@1e
    .line 600
    return-void
.end method

.method public setOpacity(I)V
    .registers 2
    .parameter "opacity"

    #@0
    .prologue
    .line 428
    iput p1, p0, Landroid/graphics/drawable/LayerDrawable;->mOpacityOverride:I

    #@2
    .line 429
    return-void
.end method

.method public setVisible(ZZ)Z
    .registers 8
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    .line 379
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@3
    move-result v2

    #@4
    .line 380
    .local v2, changed:Z
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@6
    iget-object v1, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mChildren:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;

    #@8
    .line 381
    .local v1, array:[Landroid/graphics/drawable/LayerDrawable$ChildDrawable;
    iget-object v4, p0, Landroid/graphics/drawable/LayerDrawable;->mLayerState:Landroid/graphics/drawable/LayerDrawable$LayerState;

    #@a
    iget v0, v4, Landroid/graphics/drawable/LayerDrawable$LayerState;->mNum:I

    #@c
    .line 382
    .local v0, N:I
    const/4 v3, 0x0

    #@d
    .local v3, i:I
    :goto_d
    if-ge v3, v0, :cond_19

    #@f
    .line 383
    aget-object v4, v1, v3

    #@11
    iget-object v4, v4, Landroid/graphics/drawable/LayerDrawable$ChildDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {v4, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@16
    .line 382
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_d

    #@19
    .line 385
    :cond_19
    return v2
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 332
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 333
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 334
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    #@9
    .line 336
    :cond_9
    return-void
.end method
