.class public Landroid/graphics/drawable/LevelListDrawable;
.super Landroid/graphics/drawable/DrawableContainer;
.source "LevelListDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/LevelListDrawable$1;,
        Landroid/graphics/drawable/LevelListDrawable$LevelListState;
    }
.end annotation


# instance fields
.field private final mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

.field private mMutated:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 63
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/LevelListDrawable;-><init>(Landroid/graphics/drawable/LevelListDrawable$LevelListState;Landroid/content/res/Resources;)V

    #@4
    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/LevelListDrawable$LevelListState;Landroid/content/res/Resources;)V
    .registers 5
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 212
    invoke-direct {p0}, Landroid/graphics/drawable/DrawableContainer;-><init>()V

    #@3
    .line 213
    new-instance v0, Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@5
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;-><init>(Landroid/graphics/drawable/LevelListDrawable$LevelListState;Landroid/graphics/drawable/LevelListDrawable;Landroid/content/res/Resources;)V

    #@8
    .line 214
    .local v0, as:Landroid/graphics/drawable/LevelListDrawable$LevelListState;
    iput-object v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@a
    .line 215
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/LevelListDrawable;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    #@d
    .line 216
    invoke-virtual {p0}, Landroid/graphics/drawable/LevelListDrawable;->getLevel()I

    #@10
    move-result v1

    #@11
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/LevelListDrawable;->onLevelChange(I)Z

    #@14
    .line 217
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/LevelListDrawable$LevelListState;Landroid/content/res/Resources;Landroid/graphics/drawable/LevelListDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/LevelListDrawable;-><init>(Landroid/graphics/drawable/LevelListDrawable$LevelListState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method


# virtual methods
.method public addLevel(IILandroid/graphics/drawable/Drawable;)V
    .registers 5
    .parameter "low"
    .parameter "high"
    .parameter "drawable"

    #@0
    .prologue
    .line 67
    if-eqz p3, :cond_e

    #@2
    .line 68
    iget-object v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@4
    invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->addLevel(IILandroid/graphics/drawable/Drawable;)V

    #@7
    .line 70
    invoke-virtual {p0}, Landroid/graphics/drawable/LevelListDrawable;->getLevel()I

    #@a
    move-result v0

    #@b
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/LevelListDrawable;->onLevelChange(I)Z

    #@e
    .line 72
    :cond_e
    return-void
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 15
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/DrawableContainer;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@3
    .line 93
    const/4 v6, 0x0

    #@4
    .line 95
    .local v6, low:I
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@7
    move-result v8

    #@8
    add-int/lit8 v5, v8, 0x1

    #@a
    .line 98
    .local v5, innerDepth:I
    :cond_a
    :goto_a
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@d
    move-result v7

    #@e
    .local v7, type:I
    const/4 v8, 0x1

    #@f
    if-eq v7, v8, :cond_a4

    #@11
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@14
    move-result v1

    #@15
    .local v1, depth:I
    if-ge v1, v5, :cond_1a

    #@17
    const/4 v8, 0x3

    #@18
    if-eq v7, v8, :cond_a4

    #@1a
    .line 100
    :cond_1a
    const/4 v8, 0x2

    #@1b
    if-ne v7, v8, :cond_a

    #@1d
    .line 104
    if-gt v1, v5, :cond_a

    #@1f
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    const-string/jumbo v9, "item"

    #@26
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v8

    #@2a
    if-eqz v8, :cond_a

    #@2c
    .line 108
    sget-object v8, Lcom/android/internal/R$styleable;->LevelListDrawableItem:[I

    #@2e
    invoke-virtual {p1, p3, v8}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@31
    move-result-object v0

    #@32
    .line 111
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v8, 0x1

    #@33
    const/4 v9, 0x0

    #@34
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@37
    move-result v6

    #@38
    .line 113
    const/4 v8, 0x2

    #@39
    const/4 v9, 0x0

    #@3a
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    #@3d
    move-result v4

    #@3e
    .line 115
    .local v4, high:I
    const/4 v8, 0x0

    #@3f
    const/4 v9, 0x0

    #@40
    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@43
    move-result v3

    #@44
    .line 118
    .local v3, drawableRes:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@47
    .line 120
    if-gez v4, :cond_66

    #@49
    .line 121
    new-instance v8, Lorg/xmlpull/v1/XmlPullParserException;

    #@4b
    new-instance v9, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@53
    move-result-object v10

    #@54
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v9

    #@58
    const-string v10, ": <item> tag requires a \'maxLevel\' attribute"

    #@5a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v9

    #@5e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v9

    #@62
    invoke-direct {v8, v9}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@65
    throw v8

    #@66
    .line 126
    :cond_66
    if-eqz v3, :cond_72

    #@68
    .line 127
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@6b
    move-result-object v2

    #@6c
    .line 140
    .local v2, dr:Landroid/graphics/drawable/Drawable;
    :goto_6c
    iget-object v8, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@6e
    invoke-virtual {v8, v6, v4, v2}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->addLevel(IILandroid/graphics/drawable/Drawable;)V

    #@71
    goto :goto_a

    #@72
    .line 129
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    :cond_72
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@75
    move-result v7

    #@76
    const/4 v8, 0x4

    #@77
    if-eq v7, v8, :cond_72

    #@79
    .line 131
    const/4 v8, 0x2

    #@7a
    if-eq v7, v8, :cond_9f

    #@7c
    .line 132
    new-instance v8, Lorg/xmlpull/v1/XmlPullParserException;

    #@7e
    new-instance v9, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@86
    move-result-object v10

    #@87
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v9

    #@8b
    const-string v10, ": <item> tag requires a \'drawable\' attribute or "

    #@8d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v9

    #@91
    const-string v10, "child tag defining a drawable"

    #@93
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v9

    #@97
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v9

    #@9b
    invoke-direct {v8, v9}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@9e
    throw v8

    #@9f
    .line 137
    :cond_9f
    invoke-static {p1, p2, p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@a2
    move-result-object v2

    #@a3
    .restart local v2       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_6c

    #@a4
    .line 143
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #depth:I
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    .end local v3           #drawableRes:I
    .end local v4           #high:I
    :cond_a4
    invoke-virtual {p0}, Landroid/graphics/drawable/LevelListDrawable;->getLevel()I

    #@a7
    move-result v8

    #@a8
    invoke-virtual {p0, v8}, Landroid/graphics/drawable/LevelListDrawable;->onLevelChange(I)Z

    #@ab
    .line 144
    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 148
    iget-boolean v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_2f

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/DrawableContainer;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_2f

    #@a
    .line 149
    iget-object v1, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@c
    iget-object v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@e
    invoke-static {v0}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->access$000(Landroid/graphics/drawable/LevelListDrawable$LevelListState;)[I

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, [I

    #@18
    invoke-static {v1, v0}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->access$002(Landroid/graphics/drawable/LevelListDrawable$LevelListState;[I)[I

    #@1b
    .line 150
    iget-object v1, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@1d
    iget-object v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@1f
    invoke-static {v0}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->access$100(Landroid/graphics/drawable/LevelListDrawable$LevelListState;)[I

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    check-cast v0, [I

    #@29
    invoke-static {v1, v0}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->access$102(Landroid/graphics/drawable/LevelListDrawable$LevelListState;[I)[I

    #@2c
    .line 151
    const/4 v0, 0x1

    #@2d
    iput-boolean v0, p0, Landroid/graphics/drawable/LevelListDrawable;->mMutated:Z

    #@2f
    .line 153
    :cond_2f
    return-object p0
.end method

.method protected onLevelChange(I)Z
    .registers 4
    .parameter "level"

    #@0
    .prologue
    .line 78
    iget-object v1, p0, Landroid/graphics/drawable/LevelListDrawable;->mLevelListState:Landroid/graphics/drawable/LevelListDrawable$LevelListState;

    #@2
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/LevelListDrawable$LevelListState;->indexOfLevel(I)I

    #@5
    move-result v0

    #@6
    .line 79
    .local v0, idx:I
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/LevelListDrawable;->selectDrawable(I)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_e

    #@c
    .line 80
    const/4 v1, 0x1

    #@d
    .line 82
    :goto_d
    return v1

    #@e
    :cond_e
    invoke-super {p0, p1}, Landroid/graphics/drawable/DrawableContainer;->onLevelChange(I)Z

    #@11
    move-result v1

    #@12
    goto :goto_d
.end method
