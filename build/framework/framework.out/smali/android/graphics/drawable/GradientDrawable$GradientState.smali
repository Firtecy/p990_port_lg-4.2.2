.class final Landroid/graphics/drawable/GradientDrawable$GradientState;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "GradientDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/GradientDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GradientState"
.end annotation


# instance fields
.field private mCenterX:F

.field private mCenterY:F

.field public mChangingConfigurations:I

.field public mColors:[I

.field public mGradient:I

.field private mGradientRadius:F

.field public mHasSolidColor:Z

.field public mHeight:I

.field public mInnerRadius:I

.field public mInnerRadiusRatio:F

.field private mOpaque:Z

.field public mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

.field public mPadding:Landroid/graphics/Rect;

.field public mPositions:[F

.field public mRadius:F

.field public mRadiusArray:[F

.field public mShape:I

.field public mSolidColor:I

.field public mStrokeColor:I

.field public mStrokeDashGap:F

.field public mStrokeDashWidth:F

.field public mStrokeWidth:I

.field public mTempColors:[I

.field public mTempPositions:[F

.field public mThickness:I

.field public mThicknessRatio:F

.field private mUseLevel:Z

.field private mUseLevelForShape:Z

.field public mWidth:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const/high16 v0, 0x3f00

    #@4
    .line 1108
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    #@7
    .line 1074
    iput v2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@9
    .line 1075
    iput v2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@b
    .line 1083
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@d
    .line 1090
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@f
    .line 1091
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@11
    .line 1096
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@13
    .line 1097
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@15
    .line 1098
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@17
    .line 1109
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mChangingConfigurations:I

    #@19
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mChangingConfigurations:I

    #@1b
    .line 1110
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@1d
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@1f
    .line 1111
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@21
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@23
    .line 1112
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@25
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@27
    .line 1113
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@29
    if-eqz v0, :cond_35

    #@2b
    .line 1114
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@2d
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, [I

    #@33
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@35
    .line 1116
    :cond_35
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@37
    if-eqz v0, :cond_43

    #@39
    .line 1117
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@3b
    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    #@3e
    move-result-object v0

    #@3f
    check-cast v0, [F

    #@41
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@43
    .line 1119
    :cond_43
    iget-boolean v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@45
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@47
    .line 1120
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mSolidColor:I

    #@49
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mSolidColor:I

    #@4b
    .line 1121
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@4d
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@4f
    .line 1122
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@51
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@53
    .line 1123
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashWidth:F

    #@55
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashWidth:F

    #@57
    .line 1124
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashGap:F

    #@59
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashGap:F

    #@5b
    .line 1125
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@5d
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@5f
    .line 1126
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@61
    if-eqz v0, :cond_6d

    #@63
    .line 1127
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@65
    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    #@68
    move-result-object v0

    #@69
    check-cast v0, [F

    #@6b
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@6d
    .line 1129
    :cond_6d
    iget-object v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPadding:Landroid/graphics/Rect;

    #@6f
    if-eqz v0, :cond_7a

    #@71
    .line 1130
    new-instance v0, Landroid/graphics/Rect;

    #@73
    iget-object v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPadding:Landroid/graphics/Rect;

    #@75
    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@78
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPadding:Landroid/graphics/Rect;

    #@7a
    .line 1132
    :cond_7a
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@7c
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@7e
    .line 1133
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@80
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@82
    .line 1134
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadiusRatio:F

    #@84
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadiusRatio:F

    #@86
    .line 1135
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThicknessRatio:F

    #@88
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThicknessRatio:F

    #@8a
    .line 1136
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@8c
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@8e
    .line 1137
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@90
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@92
    .line 1138
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@94
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@96
    .line 1139
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@98
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@9a
    .line 1140
    iget v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@9c
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@9e
    .line 1141
    iget-boolean v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevel:Z

    #@a0
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevel:Z

    #@a2
    .line 1142
    iget-boolean v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevelForShape:Z

    #@a4
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevelForShape:Z

    #@a6
    .line 1143
    iget-boolean v0, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@a8
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@aa
    .line 1144
    return-void
.end method

.method constructor <init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V
    .registers 6
    .parameter "orientation"
    .parameter "colors"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, -0x1

    #@2
    const/high16 v0, 0x3f00

    #@4
    .line 1103
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    #@7
    .line 1074
    iput v2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@9
    .line 1075
    iput v2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@b
    .line 1083
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@d
    .line 1090
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@f
    .line 1091
    iput v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@11
    .line 1096
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@13
    .line 1097
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@15
    .line 1098
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@17
    .line 1104
    iput-object p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@19
    .line 1105
    invoke-virtual {p0, p2}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setColors([I)V

    #@1c
    .line 1106
    return-void
.end method

.method static synthetic access$000(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevel:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Landroid/graphics/drawable/GradientDrawable$GradientState;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1072
    iput-boolean p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevel:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevelForShape:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Landroid/graphics/drawable/GradientDrawable$GradientState;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1072
    iput-boolean p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mUseLevelForShape:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@2
    return v0
.end method

.method static synthetic access$300(Landroid/graphics/drawable/GradientDrawable$GradientState;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@2
    return v0
.end method

.method static synthetic access$302(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1072
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@2
    return p1
.end method

.method static synthetic access$400(Landroid/graphics/drawable/GradientDrawable$GradientState;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@2
    return v0
.end method

.method static synthetic access$402(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1072
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@2
    return p1
.end method

.method static synthetic access$500(Landroid/graphics/drawable/GradientDrawable$GradientState;)F
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    iget v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@2
    return v0
.end method

.method static synthetic access$502(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1072
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@2
    return p1
.end method

.method static synthetic access$600(Landroid/graphics/drawable/GradientDrawable$GradientState;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1072
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@3
    return-void
.end method

.method private computeOpacity()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1189
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@3
    if-eqz v1, :cond_8

    #@5
    .line 1190
    iput-boolean v3, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@7
    .line 1219
    :goto_7
    return-void

    #@8
    .line 1194
    :cond_8
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@a
    const/4 v2, 0x0

    #@b
    cmpl-float v1, v1, v2

    #@d
    if-gtz v1, :cond_13

    #@f
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@11
    if-eqz v1, :cond_16

    #@13
    .line 1195
    :cond_13
    iput-boolean v3, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@15
    goto :goto_7

    #@16
    .line 1199
    :cond_16
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@18
    if-lez v1, :cond_25

    #@1a
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@1c
    invoke-static {v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->isOpaque(I)Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_25

    #@22
    .line 1200
    iput-boolean v3, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@24
    goto :goto_7

    #@25
    .line 1204
    :cond_25
    iget-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@27
    if-eqz v1, :cond_32

    #@29
    .line 1205
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mSolidColor:I

    #@2b
    invoke-static {v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->isOpaque(I)Z

    #@2e
    move-result v1

    #@2f
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@31
    goto :goto_7

    #@32
    .line 1209
    :cond_32
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@34
    if-eqz v1, :cond_4c

    #@36
    .line 1210
    const/4 v0, 0x0

    #@37
    .local v0, i:I
    :goto_37
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@39
    array-length v1, v1

    #@3a
    if-ge v0, v1, :cond_4c

    #@3c
    .line 1211
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@3e
    aget v1, v1, v0

    #@40
    invoke-static {v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->isOpaque(I)Z

    #@43
    move-result v1

    #@44
    if-nez v1, :cond_49

    #@46
    .line 1212
    iput-boolean v3, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@48
    goto :goto_7

    #@49
    .line 1210
    :cond_49
    add-int/lit8 v0, v0, 0x1

    #@4b
    goto :goto_37

    #@4c
    .line 1218
    .end local v0           #i:I
    :cond_4c
    const/4 v1, 0x1

    #@4d
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOpaque:Z

    #@4f
    goto :goto_7
.end method

.method private static isOpaque(I)Z
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 1222
    shr-int/lit8 v0, p0, 0x18

    #@2
    and-int/lit16 v0, v0, 0xff

    #@4
    const/16 v1, 0xff

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method


# virtual methods
.method public getChangingConfigurations()I
    .registers 2

    #@0
    .prologue
    .line 1158
    iget v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mChangingConfigurations:I

    #@2
    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 1148
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;Landroid/graphics/drawable/GradientDrawable$1;)V

    #@6
    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "res"

    #@0
    .prologue
    .line 1153
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;Landroid/graphics/drawable/GradientDrawable$1;)V

    #@6
    return-object v0
.end method

.method public setColors([I)V
    .registers 3
    .parameter "colors"

    #@0
    .prologue
    .line 1176
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@3
    .line 1177
    iput-object p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@5
    .line 1178
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@8
    .line 1179
    return-void
.end method

.method public setCornerRadii([F)V
    .registers 3
    .parameter "radii"

    #@0
    .prologue
    .line 1248
    iput-object p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@2
    .line 1249
    if-nez p1, :cond_7

    #@4
    .line 1250
    const/4 v0, 0x0

    #@5
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@7
    .line 1252
    :cond_7
    return-void
.end method

.method public setCornerRadius(F)V
    .registers 3
    .parameter "radius"

    #@0
    .prologue
    .line 1240
    const/4 v0, 0x0

    #@1
    cmpg-float v0, p1, v0

    #@3
    if-gez v0, :cond_6

    #@5
    .line 1241
    const/4 p1, 0x0

    #@6
    .line 1243
    :cond_6
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@8
    .line 1244
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@b
    .line 1245
    return-void
.end method

.method public setGradientCenter(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1171
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterX:F

    #@2
    .line 1172
    iput p2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mCenterY:F

    #@4
    .line 1173
    return-void
.end method

.method public setGradientRadius(F)V
    .registers 2
    .parameter "gradientRadius"

    #@0
    .prologue
    .line 1260
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradientRadius:F

    #@2
    .line 1261
    return-void
.end method

.method public setGradientType(I)V
    .registers 2
    .parameter "gradient"

    #@0
    .prologue
    .line 1167
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@2
    .line 1168
    return-void
.end method

.method public setShape(I)V
    .registers 2
    .parameter "shape"

    #@0
    .prologue
    .line 1162
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@2
    .line 1163
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@5
    .line 1164
    return-void
.end method

.method public setSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1255
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@2
    .line 1256
    iput p2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@4
    .line 1257
    return-void
.end method

.method public setSolidColor(I)V
    .registers 3
    .parameter "argb"

    #@0
    .prologue
    .line 1182
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@3
    .line 1183
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mSolidColor:I

    #@5
    .line 1184
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@8
    .line 1185
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@b
    .line 1186
    return-void
.end method

.method public setStroke(II)V
    .registers 3
    .parameter "width"
    .parameter "color"

    #@0
    .prologue
    .line 1226
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@2
    .line 1227
    iput p2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@4
    .line 1228
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@7
    .line 1229
    return-void
.end method

.method public setStroke(IIFF)V
    .registers 5
    .parameter "width"
    .parameter "color"
    .parameter "dashWidth"
    .parameter "dashGap"

    #@0
    .prologue
    .line 1232
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@2
    .line 1233
    iput p2, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@4
    .line 1234
    iput p3, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashWidth:F

    #@6
    .line 1235
    iput p4, p0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashGap:F

    #@8
    .line 1236
    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->computeOpacity()V

    #@b
    .line 1237
    return-void
.end method
