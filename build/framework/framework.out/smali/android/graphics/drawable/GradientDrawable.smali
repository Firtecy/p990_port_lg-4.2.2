.class public Landroid/graphics/drawable/GradientDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "GradientDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/GradientDrawable$1;,
        Landroid/graphics/drawable/GradientDrawable$GradientState;,
        Landroid/graphics/drawable/GradientDrawable$Orientation;
    }
.end annotation


# static fields
.field public static final LINE:I = 0x2

.field public static final LINEAR_GRADIENT:I = 0x0

.field public static final OVAL:I = 0x1

.field public static final RADIAL_GRADIENT:I = 0x1

.field public static final RECTANGLE:I = 0x0

.field public static final RING:I = 0x3

.field public static final SWEEP_GRADIENT:I = 0x2


# instance fields
.field private mAlpha:I

.field private mColorFilter:Landroid/graphics/ColorFilter;

.field private mDither:Z

.field private final mFillPaint:Landroid/graphics/Paint;

.field private mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

.field private mLayerPaint:Landroid/graphics/Paint;

.field private mMutated:Z

.field private mPadding:Landroid/graphics/Rect;

.field private final mPath:Landroid/graphics/Path;

.field private mPathIsDirty:Z

.field private final mRect:Landroid/graphics/RectF;

.field private mRectIsDirty:Z

.field private mRingPath:Landroid/graphics/Path;

.field private mStrokePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 154
    new-instance v0, Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable$GradientState;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    #@8
    invoke-direct {p0, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@b
    .line 155
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1264
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@4
    .line 115
    new-instance v0, Landroid/graphics/Paint;

    #@6
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    #@9
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@b
    .line 119
    const/16 v0, 0xff

    #@d
    iput v0, p0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@f
    .line 122
    new-instance v0, Landroid/graphics/Path;

    #@11
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    #@14
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPath:Landroid/graphics/Path;

    #@16
    .line 123
    new-instance v0, Landroid/graphics/RectF;

    #@18
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@1d
    .line 129
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@1f
    .line 1265
    iput-object p1, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@21
    .line 1266
    invoke-direct {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->initializeWithState(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@24
    .line 1267
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@26
    .line 1268
    const/4 v0, 0x0

    #@27
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mMutated:Z

    #@29
    .line 1269
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/GradientDrawable$GradientState;Landroid/graphics/drawable/GradientDrawable$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 77
    invoke-direct {p0, p1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V
    .registers 4
    .parameter "orientation"
    .parameter "colors"

    #@0
    .prologue
    .line 162
    new-instance v0, Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-direct {v0, p1, p2}, Landroid/graphics/drawable/GradientDrawable$GradientState;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    #@5
    invoke-direct {p0, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@8
    .line 163
    return-void
.end method

.method private buildRing(Landroid/graphics/drawable/GradientDrawable$GradientState;)Landroid/graphics/Path;
    .registers 16
    .parameter "st"

    #@0
    .prologue
    const/4 v13, -0x1

    #@1
    const/high16 v12, 0x4000

    #@3
    const/4 v11, 0x0

    #@4
    const/high16 v8, 0x43b4

    #@6
    .line 560
    iget-object v9, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@8
    if-eqz v9, :cond_17

    #@a
    invoke-static {p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$100(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_14

    #@10
    iget-boolean v9, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@12
    if-nez v9, :cond_17

    #@14
    :cond_14
    iget-object v3, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@16
    .line 608
    :goto_16
    return-object v3

    #@17
    .line 561
    :cond_17
    iput-boolean v11, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@19
    .line 563
    invoke-static {p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$100(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@1c
    move-result v9

    #@1d
    if-eqz v9, :cond_94

    #@1f
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->getLevel()I

    #@22
    move-result v9

    #@23
    int-to-float v9, v9

    #@24
    mul-float/2addr v9, v8

    #@25
    const v10, 0x461c4000

    #@28
    div-float v4, v9, v10

    #@2a
    .line 565
    .local v4, sweep:F
    :goto_2a
    new-instance v0, Landroid/graphics/RectF;

    #@2c
    iget-object v9, p0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@2e
    invoke-direct {v0, v9}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@31
    .line 567
    .local v0, bounds:Landroid/graphics/RectF;
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    #@34
    move-result v9

    #@35
    div-float v6, v9, v12

    #@37
    .line 568
    .local v6, x:F
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    #@3a
    move-result v9

    #@3b
    div-float v7, v9, v12

    #@3d
    .line 570
    .local v7, y:F
    iget v9, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@3f
    if-eq v9, v13, :cond_96

    #@41
    iget v9, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@43
    int-to-float v5, v9

    #@44
    .line 573
    .local v5, thickness:F
    :goto_44
    iget v9, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@46
    if-eq v9, v13, :cond_9f

    #@48
    iget v9, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@4a
    int-to-float v2, v9

    #@4b
    .line 576
    .local v2, radius:F
    :goto_4b
    new-instance v1, Landroid/graphics/RectF;

    #@4d
    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@50
    .line 577
    .local v1, innerBounds:Landroid/graphics/RectF;
    sub-float v9, v6, v2

    #@52
    sub-float v10, v7, v2

    #@54
    invoke-virtual {v1, v9, v10}, Landroid/graphics/RectF;->inset(FF)V

    #@57
    .line 579
    new-instance v0, Landroid/graphics/RectF;

    #@59
    .end local v0           #bounds:Landroid/graphics/RectF;
    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@5c
    .line 580
    .restart local v0       #bounds:Landroid/graphics/RectF;
    neg-float v9, v5

    #@5d
    neg-float v10, v5

    #@5e
    invoke-virtual {v0, v9, v10}, Landroid/graphics/RectF;->inset(FF)V

    #@61
    .line 582
    iget-object v9, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@63
    if-nez v9, :cond_a8

    #@65
    .line 583
    new-instance v9, Landroid/graphics/Path;

    #@67
    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    #@6a
    iput-object v9, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@6c
    .line 588
    :goto_6c
    iget-object v3, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@6e
    .line 591
    .local v3, ringPath:Landroid/graphics/Path;
    cmpg-float v8, v4, v8

    #@70
    if-gez v8, :cond_ae

    #@72
    const/high16 v8, -0x3c4c

    #@74
    cmpl-float v8, v4, v8

    #@76
    if-lez v8, :cond_ae

    #@78
    .line 592
    sget-object v8, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    #@7a
    invoke-virtual {v3, v8}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    #@7d
    .line 594
    add-float v8, v6, v2

    #@7f
    invoke-virtual {v3, v8, v7}, Landroid/graphics/Path;->moveTo(FF)V

    #@82
    .line 596
    add-float v8, v6, v2

    #@84
    add-float/2addr v8, v5

    #@85
    invoke-virtual {v3, v8, v7}, Landroid/graphics/Path;->lineTo(FF)V

    #@88
    .line 598
    const/4 v8, 0x0

    #@89
    invoke-virtual {v3, v0, v8, v4, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    #@8c
    .line 600
    neg-float v8, v4

    #@8d
    invoke-virtual {v3, v1, v4, v8, v11}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    #@90
    .line 601
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    #@93
    goto :goto_16

    #@94
    .end local v0           #bounds:Landroid/graphics/RectF;
    .end local v1           #innerBounds:Landroid/graphics/RectF;
    .end local v2           #radius:F
    .end local v3           #ringPath:Landroid/graphics/Path;
    .end local v4           #sweep:F
    .end local v5           #thickness:F
    .end local v6           #x:F
    .end local v7           #y:F
    :cond_94
    move v4, v8

    #@95
    .line 563
    goto :goto_2a

    #@96
    .line 570
    .restart local v0       #bounds:Landroid/graphics/RectF;
    .restart local v4       #sweep:F
    .restart local v6       #x:F
    .restart local v7       #y:F
    :cond_96
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    #@99
    move-result v9

    #@9a
    iget v10, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThicknessRatio:F

    #@9c
    div-float v5, v9, v10

    #@9e
    goto :goto_44

    #@9f
    .line 573
    .restart local v5       #thickness:F
    :cond_9f
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    #@a2
    move-result v9

    #@a3
    iget v10, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadiusRatio:F

    #@a5
    div-float v2, v9, v10

    #@a7
    goto :goto_4b

    #@a8
    .line 585
    .restart local v1       #innerBounds:Landroid/graphics/RectF;
    .restart local v2       #radius:F
    :cond_a8
    iget-object v9, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@aa
    invoke-virtual {v9}, Landroid/graphics/Path;->reset()V

    #@ad
    goto :goto_6c

    #@ae
    .line 604
    .restart local v3       #ringPath:Landroid/graphics/Path;
    :cond_ae
    sget-object v8, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@b0
    invoke-virtual {v3, v0, v8}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    #@b3
    .line 605
    sget-object v8, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    #@b5
    invoke-virtual {v3, v1, v8}, Landroid/graphics/Path;->addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    #@b8
    goto/16 :goto_16
.end method

.method private ensureValidRect()Z
    .registers 26

    #@0
    .prologue
    .line 686
    move-object/from16 v0, p0

    #@2
    iget-boolean v2, v0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@4
    if-eqz v2, :cond_b0

    #@6
    .line 687
    const/4 v2, 0x0

    #@7
    move-object/from16 v0, p0

    #@9
    iput-boolean v2, v0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@b
    .line 689
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/GradientDrawable;->getBounds()Landroid/graphics/Rect;

    #@e
    move-result-object v15

    #@f
    .line 690
    .local v15, bounds:Landroid/graphics/Rect;
    const/16 v18, 0x0

    #@11
    .line 692
    .local v18, inset:F
    move-object/from16 v0, p0

    #@13
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@15
    if-eqz v2, :cond_23

    #@17
    .line 693
    move-object/from16 v0, p0

    #@19
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@1b
    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    #@1e
    move-result v2

    #@1f
    const/high16 v8, 0x3f00

    #@21
    mul-float v18, v2, v8

    #@23
    .line 696
    :cond_23
    move-object/from16 v0, p0

    #@25
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@27
    move-object/from16 v22, v0

    #@29
    .line 698
    .local v22, st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@2d
    iget v8, v15, Landroid/graphics/Rect;->left:I

    #@2f
    int-to-float v8, v8

    #@30
    add-float v8, v8, v18

    #@32
    iget v9, v15, Landroid/graphics/Rect;->top:I

    #@34
    int-to-float v9, v9

    #@35
    add-float v9, v9, v18

    #@37
    iget v10, v15, Landroid/graphics/Rect;->right:I

    #@39
    int-to-float v10, v10

    #@3a
    sub-float v10, v10, v18

    #@3c
    iget v11, v15, Landroid/graphics/Rect;->bottom:I

    #@3e
    int-to-float v11, v11

    #@3f
    sub-float v11, v11, v18

    #@41
    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    #@44
    .line 701
    move-object/from16 v0, v22

    #@46
    iget-object v7, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@48
    .line 702
    .local v7, colors:[I
    if-eqz v7, :cond_b0

    #@4a
    .line 703
    move-object/from16 v0, p0

    #@4c
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@4e
    move-object/from16 v21, v0

    #@50
    .line 706
    .local v21, r:Landroid/graphics/RectF;
    move-object/from16 v0, v22

    #@52
    iget v2, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@54
    if-nez v2, :cond_141

    #@56
    .line 707
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$000(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@59
    move-result v2

    #@5a
    if-eqz v2, :cond_bc

    #@5c
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/GradientDrawable;->getLevel()I

    #@5f
    move-result v2

    #@60
    int-to-float v2, v2

    #@61
    const v8, 0x461c4000

    #@64
    div-float v20, v2, v8

    #@66
    .line 708
    .local v20, level:F
    :goto_66
    sget-object v2, Landroid/graphics/drawable/GradientDrawable$1;->$SwitchMap$android$graphics$drawable$GradientDrawable$Orientation:[I

    #@68
    move-object/from16 v0, v22

    #@6a
    iget-object v8, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@6c
    invoke-virtual {v8}, Landroid/graphics/drawable/GradientDrawable$Orientation;->ordinal()I

    #@6f
    move-result v8

    #@70
    aget v2, v2, v8

    #@72
    packed-switch v2, :pswitch_data_286

    #@75
    .line 738
    move-object/from16 v0, v21

    #@77
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@79
    .local v3, x0:F
    move-object/from16 v0, v21

    #@7b
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@7d
    .line 739
    .local v4, y0:F
    move-object/from16 v0, v21

    #@7f
    iget v2, v0, Landroid/graphics/RectF;->right:F

    #@81
    mul-float v5, v20, v2

    #@83
    .local v5, x1:F
    move-object/from16 v0, v21

    #@85
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    #@87
    mul-float v6, v20, v2

    #@89
    .line 743
    .local v6, y1:F
    :goto_89
    move-object/from16 v0, p0

    #@8b
    iget-object v10, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@8d
    new-instance v2, Landroid/graphics/LinearGradient;

    #@8f
    move-object/from16 v0, v22

    #@91
    iget-object v8, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@93
    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@95
    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    #@98
    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@9b
    .line 745
    move-object/from16 v0, p0

    #@9d
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@9f
    iget-boolean v2, v2, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@a1
    if-nez v2, :cond_b0

    #@a3
    .line 746
    move-object/from16 v0, p0

    #@a5
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@a7
    move-object/from16 v0, p0

    #@a9
    iget v8, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@ab
    shl-int/lit8 v8, v8, 0x18

    #@ad
    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    #@b0
    .line 796
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    .end local v7           #colors:[I
    .end local v15           #bounds:Landroid/graphics/Rect;
    .end local v18           #inset:F
    .end local v20           #level:F
    .end local v21           #r:Landroid/graphics/RectF;
    .end local v22           #st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    :cond_b0
    :goto_b0
    move-object/from16 v0, p0

    #@b2
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@b4
    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    #@b7
    move-result v2

    #@b8
    if-nez v2, :cond_283

    #@ba
    const/4 v2, 0x1

    #@bb
    :goto_bb
    return v2

    #@bc
    .line 707
    .restart local v7       #colors:[I
    .restart local v15       #bounds:Landroid/graphics/Rect;
    .restart local v18       #inset:F
    .restart local v21       #r:Landroid/graphics/RectF;
    .restart local v22       #st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    :cond_bc
    const/high16 v20, 0x3f80

    #@be
    goto :goto_66

    #@bf
    .line 710
    .restart local v20       #level:F
    :pswitch_bf
    move-object/from16 v0, v21

    #@c1
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@c3
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@c5
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@c7
    .line 711
    .restart local v4       #y0:F
    move v5, v3

    #@c8
    .restart local v5       #x1:F
    move-object/from16 v0, v21

    #@ca
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    #@cc
    mul-float v6, v20, v2

    #@ce
    .line 712
    .restart local v6       #y1:F
    goto :goto_89

    #@cf
    .line 714
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_cf
    move-object/from16 v0, v21

    #@d1
    iget v3, v0, Landroid/graphics/RectF;->right:F

    #@d3
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@d5
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@d7
    .line 715
    .restart local v4       #y0:F
    move-object/from16 v0, v21

    #@d9
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@db
    mul-float v5, v20, v2

    #@dd
    .restart local v5       #x1:F
    move-object/from16 v0, v21

    #@df
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    #@e1
    mul-float v6, v20, v2

    #@e3
    .line 716
    .restart local v6       #y1:F
    goto :goto_89

    #@e4
    .line 718
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_e4
    move-object/from16 v0, v21

    #@e6
    iget v3, v0, Landroid/graphics/RectF;->right:F

    #@e8
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@ea
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@ec
    .line 719
    .restart local v4       #y0:F
    move-object/from16 v0, v21

    #@ee
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@f0
    mul-float v5, v20, v2

    #@f2
    .restart local v5       #x1:F
    move v6, v4

    #@f3
    .line 720
    .restart local v6       #y1:F
    goto :goto_89

    #@f4
    .line 722
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_f4
    move-object/from16 v0, v21

    #@f6
    iget v3, v0, Landroid/graphics/RectF;->right:F

    #@f8
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@fa
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    #@fc
    .line 723
    .restart local v4       #y0:F
    move-object/from16 v0, v21

    #@fe
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@100
    mul-float v5, v20, v2

    #@102
    .restart local v5       #x1:F
    move-object/from16 v0, v21

    #@104
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@106
    mul-float v6, v20, v2

    #@108
    .line 724
    .restart local v6       #y1:F
    goto :goto_89

    #@109
    .line 726
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_109
    move-object/from16 v0, v21

    #@10b
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@10d
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@10f
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    #@111
    .line 727
    .restart local v4       #y0:F
    move v5, v3

    #@112
    .restart local v5       #x1:F
    move-object/from16 v0, v21

    #@114
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@116
    mul-float v6, v20, v2

    #@118
    .line 728
    .restart local v6       #y1:F
    goto/16 :goto_89

    #@11a
    .line 730
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_11a
    move-object/from16 v0, v21

    #@11c
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@11e
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@120
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    #@122
    .line 731
    .restart local v4       #y0:F
    move-object/from16 v0, v21

    #@124
    iget v2, v0, Landroid/graphics/RectF;->right:F

    #@126
    mul-float v5, v20, v2

    #@128
    .restart local v5       #x1:F
    move-object/from16 v0, v21

    #@12a
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@12c
    mul-float v6, v20, v2

    #@12e
    .line 732
    .restart local v6       #y1:F
    goto/16 :goto_89

    #@130
    .line 734
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    :pswitch_130
    move-object/from16 v0, v21

    #@132
    iget v3, v0, Landroid/graphics/RectF;->left:F

    #@134
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@136
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@138
    .line 735
    .restart local v4       #y0:F
    move-object/from16 v0, v21

    #@13a
    iget v2, v0, Landroid/graphics/RectF;->right:F

    #@13c
    mul-float v5, v20, v2

    #@13e
    .restart local v5       #x1:F
    move v6, v4

    #@13f
    .line 736
    .restart local v6       #y1:F
    goto/16 :goto_89

    #@141
    .line 748
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v5           #x1:F
    .end local v6           #y1:F
    .end local v20           #level:F
    :cond_141
    move-object/from16 v0, v22

    #@143
    iget v2, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@145
    const/4 v8, 0x1

    #@146
    if-ne v2, v8, :cond_1b2

    #@148
    .line 749
    move-object/from16 v0, v21

    #@14a
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@14c
    move-object/from16 v0, v21

    #@14e
    iget v8, v0, Landroid/graphics/RectF;->right:F

    #@150
    move-object/from16 v0, v21

    #@152
    iget v9, v0, Landroid/graphics/RectF;->left:F

    #@154
    sub-float/2addr v8, v9

    #@155
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$300(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@158
    move-result v9

    #@159
    mul-float/2addr v8, v9

    #@15a
    add-float v3, v2, v8

    #@15c
    .line 750
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@15e
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@160
    move-object/from16 v0, v21

    #@162
    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    #@164
    move-object/from16 v0, v21

    #@166
    iget v9, v0, Landroid/graphics/RectF;->top:F

    #@168
    sub-float/2addr v8, v9

    #@169
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$400(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@16c
    move-result v9

    #@16d
    mul-float/2addr v8, v9

    #@16e
    add-float v4, v2, v8

    #@170
    .line 752
    .restart local v4       #y0:F
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$000(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@173
    move-result v2

    #@174
    if-eqz v2, :cond_1af

    #@176
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/GradientDrawable;->getLevel()I

    #@179
    move-result v2

    #@17a
    int-to-float v2, v2

    #@17b
    const v8, 0x461c4000

    #@17e
    div-float v20, v2, v8

    #@180
    .line 754
    .restart local v20       #level:F
    :goto_180
    move-object/from16 v0, p0

    #@182
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@184
    new-instance v8, Landroid/graphics/RadialGradient;

    #@186
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$500(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@189
    move-result v9

    #@18a
    mul-float v11, v20, v9

    #@18c
    const/4 v13, 0x0

    #@18d
    sget-object v14, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    #@18f
    move v9, v3

    #@190
    move v10, v4

    #@191
    move-object v12, v7

    #@192
    invoke-direct/range {v8 .. v14}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    #@195
    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@198
    .line 757
    move-object/from16 v0, p0

    #@19a
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@19c
    iget-boolean v2, v2, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@19e
    if-nez v2, :cond_b0

    #@1a0
    .line 758
    move-object/from16 v0, p0

    #@1a2
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@1a4
    move-object/from16 v0, p0

    #@1a6
    iget v8, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@1a8
    shl-int/lit8 v8, v8, 0x18

    #@1aa
    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    #@1ad
    goto/16 :goto_b0

    #@1af
    .line 752
    .end local v20           #level:F
    :cond_1af
    const/high16 v20, 0x3f80

    #@1b1
    goto :goto_180

    #@1b2
    .line 760
    .end local v3           #x0:F
    .end local v4           #y0:F
    :cond_1b2
    move-object/from16 v0, v22

    #@1b4
    iget v2, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@1b6
    const/4 v8, 0x2

    #@1b7
    if-ne v2, v8, :cond_b0

    #@1b9
    .line 761
    move-object/from16 v0, v21

    #@1bb
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@1bd
    move-object/from16 v0, v21

    #@1bf
    iget v8, v0, Landroid/graphics/RectF;->right:F

    #@1c1
    move-object/from16 v0, v21

    #@1c3
    iget v9, v0, Landroid/graphics/RectF;->left:F

    #@1c5
    sub-float/2addr v8, v9

    #@1c6
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$300(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@1c9
    move-result v9

    #@1ca
    mul-float/2addr v8, v9

    #@1cb
    add-float v3, v2, v8

    #@1cd
    .line 762
    .restart local v3       #x0:F
    move-object/from16 v0, v21

    #@1cf
    iget v2, v0, Landroid/graphics/RectF;->top:F

    #@1d1
    move-object/from16 v0, v21

    #@1d3
    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    #@1d5
    move-object/from16 v0, v21

    #@1d7
    iget v9, v0, Landroid/graphics/RectF;->top:F

    #@1d9
    sub-float/2addr v8, v9

    #@1da
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$400(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@1dd
    move-result v9

    #@1de
    mul-float/2addr v8, v9

    #@1df
    add-float v4, v2, v8

    #@1e1
    .line 764
    .restart local v4       #y0:F
    move-object/from16 v23, v7

    #@1e3
    .line 765
    .local v23, tempColors:[I
    const/16 v24, 0x0

    #@1e5
    .line 767
    .local v24, tempPositions:[F
    invoke-static/range {v22 .. v22}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$000(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@1e8
    move-result v2

    #@1e9
    if-eqz v2, :cond_25c

    #@1eb
    .line 768
    move-object/from16 v0, v22

    #@1ed
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mTempColors:[I

    #@1ef
    move-object/from16 v23, v0

    #@1f1
    .line 769
    array-length v0, v7

    #@1f2
    move/from16 v19, v0

    #@1f4
    .line 770
    .local v19, length:I
    if-eqz v23, :cond_1fd

    #@1f6
    move-object/from16 v0, v23

    #@1f8
    array-length v2, v0

    #@1f9
    add-int/lit8 v8, v19, 0x1

    #@1fb
    if-eq v2, v8, :cond_209

    #@1fd
    .line 771
    :cond_1fd
    add-int/lit8 v2, v19, 0x1

    #@1ff
    new-array v0, v2, [I

    #@201
    move-object/from16 v23, v0

    #@203
    .end local v23           #tempColors:[I
    move-object/from16 v0, v23

    #@205
    move-object/from16 v1, v22

    #@207
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mTempColors:[I

    #@209
    .line 773
    .restart local v23       #tempColors:[I
    :cond_209
    const/4 v2, 0x0

    #@20a
    const/4 v8, 0x0

    #@20b
    move-object/from16 v0, v23

    #@20d
    move/from16 v1, v19

    #@20f
    invoke-static {v7, v2, v0, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@212
    .line 774
    add-int/lit8 v2, v19, -0x1

    #@214
    aget v2, v7, v2

    #@216
    aput v2, v23, v19

    #@218
    .line 776
    move-object/from16 v0, v22

    #@21a
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mTempPositions:[F

    #@21c
    move-object/from16 v24, v0

    #@21e
    .line 777
    const/high16 v2, 0x3f80

    #@220
    add-int/lit8 v8, v19, -0x1

    #@222
    int-to-float v8, v8

    #@223
    div-float v16, v2, v8

    #@225
    .line 778
    .local v16, fraction:F
    if-eqz v24, :cond_22e

    #@227
    move-object/from16 v0, v24

    #@229
    array-length v2, v0

    #@22a
    add-int/lit8 v8, v19, 0x1

    #@22c
    if-eq v2, v8, :cond_23a

    #@22e
    .line 779
    :cond_22e
    add-int/lit8 v2, v19, 0x1

    #@230
    new-array v0, v2, [F

    #@232
    move-object/from16 v24, v0

    #@234
    .end local v24           #tempPositions:[F
    move-object/from16 v0, v24

    #@236
    move-object/from16 v1, v22

    #@238
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mTempPositions:[F

    #@23a
    .line 782
    .restart local v24       #tempPositions:[F
    :cond_23a
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/GradientDrawable;->getLevel()I

    #@23d
    move-result v2

    #@23e
    int-to-float v2, v2

    #@23f
    const v8, 0x461c4000

    #@242
    div-float v20, v2, v8

    #@244
    .line 783
    .restart local v20       #level:F
    const/16 v17, 0x0

    #@246
    .local v17, i:I
    :goto_246
    move/from16 v0, v17

    #@248
    move/from16 v1, v19

    #@24a
    if-ge v0, v1, :cond_258

    #@24c
    .line 784
    move/from16 v0, v17

    #@24e
    int-to-float v2, v0

    #@24f
    mul-float v2, v2, v16

    #@251
    mul-float v2, v2, v20

    #@253
    aput v2, v24, v17

    #@255
    .line 783
    add-int/lit8 v17, v17, 0x1

    #@257
    goto :goto_246

    #@258
    .line 786
    :cond_258
    const/high16 v2, 0x3f80

    #@25a
    aput v2, v24, v19

    #@25c
    .line 789
    .end local v16           #fraction:F
    .end local v17           #i:I
    .end local v19           #length:I
    .end local v20           #level:F
    :cond_25c
    move-object/from16 v0, p0

    #@25e
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@260
    new-instance v8, Landroid/graphics/SweepGradient;

    #@262
    move-object/from16 v0, v23

    #@264
    move-object/from16 v1, v24

    #@266
    invoke-direct {v8, v3, v4, v0, v1}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    #@269
    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@26c
    .line 790
    move-object/from16 v0, p0

    #@26e
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@270
    iget-boolean v2, v2, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@272
    if-nez v2, :cond_b0

    #@274
    .line 791
    move-object/from16 v0, p0

    #@276
    iget-object v2, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@278
    move-object/from16 v0, p0

    #@27a
    iget v8, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@27c
    shl-int/lit8 v8, v8, 0x18

    #@27e
    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setColor(I)V

    #@281
    goto/16 :goto_b0

    #@283
    .line 796
    .end local v3           #x0:F
    .end local v4           #y0:F
    .end local v7           #colors:[I
    .end local v15           #bounds:Landroid/graphics/Rect;
    .end local v18           #inset:F
    .end local v21           #r:Landroid/graphics/RectF;
    .end local v22           #st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    .end local v23           #tempColors:[I
    .end local v24           #tempPositions:[F
    :cond_283
    const/4 v2, 0x0

    #@284
    goto/16 :goto_bb

    #@286
    .line 708
    :pswitch_data_286
    .packed-switch 0x1
        :pswitch_bf
        :pswitch_cf
        :pswitch_e4
        :pswitch_f4
        :pswitch_109
        :pswitch_11a
        :pswitch_130
    .end packed-switch
.end method

.method private static getFloatOrFraction(Landroid/content/res/TypedArray;IF)F
    .registers 9
    .parameter "a"
    .parameter "index"
    .parameter "defaultValue"

    #@0
    .prologue
    const/high16 v5, 0x3f80

    #@2
    .line 1037
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@5
    move-result-object v0

    #@6
    .line 1038
    .local v0, tv:Landroid/util/TypedValue;
    move v1, p2

    #@7
    .line 1039
    .local v1, v:F
    if-eqz v0, :cond_15

    #@9
    .line 1040
    iget v3, v0, Landroid/util/TypedValue;->type:I

    #@b
    const/4 v4, 0x6

    #@c
    if-ne v3, v4, :cond_16

    #@e
    const/4 v2, 0x1

    #@f
    .line 1041
    .local v2, vIsFraction:Z
    :goto_f
    if-eqz v2, :cond_18

    #@11
    invoke-virtual {v0, v5, v5}, Landroid/util/TypedValue;->getFraction(FF)F

    #@14
    move-result v1

    #@15
    .line 1043
    .end local v2           #vIsFraction:Z
    :cond_15
    :goto_15
    return v1

    #@16
    .line 1040
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_f

    #@18
    .line 1041
    .restart local v2       #vIsFraction:Z
    :cond_18
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    #@1b
    move-result v1

    #@1c
    goto :goto_15
.end method

.method private initializeWithState(Landroid/graphics/drawable/GradientDrawable$GradientState;)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 1272
    iget-boolean v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@5
    if-eqz v1, :cond_4f

    #@7
    .line 1273
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@9
    iget v2, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mSolidColor:I

    #@b
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@e
    .line 1280
    :cond_e
    :goto_e
    iget-object v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPadding:Landroid/graphics/Rect;

    #@10
    iput-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mPadding:Landroid/graphics/Rect;

    #@12
    .line 1281
    iget v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@14
    if-ltz v1, :cond_4e

    #@16
    .line 1282
    new-instance v1, Landroid/graphics/Paint;

    #@18
    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    #@1b
    iput-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@1d
    .line 1283
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@1f
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@21
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@24
    .line 1284
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@26
    iget v2, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeWidth:I

    #@28
    int-to-float v2, v2

    #@29
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@2c
    .line 1285
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@2e
    iget v2, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeColor:I

    #@30
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    #@33
    .line 1287
    iget v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashWidth:F

    #@35
    cmpl-float v1, v1, v3

    #@37
    if-eqz v1, :cond_4e

    #@39
    .line 1288
    new-instance v0, Landroid/graphics/DashPathEffect;

    #@3b
    const/4 v1, 0x2

    #@3c
    new-array v1, v1, [F

    #@3e
    iget v2, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashWidth:F

    #@40
    aput v2, v1, v4

    #@42
    iget v2, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mStrokeDashGap:F

    #@44
    aput v2, v1, v5

    #@46
    invoke-direct {v0, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    #@49
    .line 1290
    .local v0, e:Landroid/graphics/DashPathEffect;
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@4b
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    #@4e
    .line 1293
    .end local v0           #e:Landroid/graphics/DashPathEffect;
    :cond_4e
    return-void

    #@4f
    .line 1274
    :cond_4f
    iget-object v1, p1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@51
    if-nez v1, :cond_e

    #@53
    .line 1278
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@55
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@58
    goto :goto_e
.end method

.method private modulateAlpha(I)I
    .registers 5
    .parameter "alpha"

    #@0
    .prologue
    .line 378
    iget v1, p0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@2
    iget v2, p0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@4
    shr-int/lit8 v2, v2, 0x7

    #@6
    add-int v0, v1, v2

    #@8
    .line 379
    .local v0, scale:I
    mul-int v1, p1, v0

    #@a
    shr-int/lit8 v1, v1, 0x8

    #@c
    return v1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 23
    .parameter "canvas"

    #@0
    .prologue
    .line 425
    invoke-direct/range {p0 .. p0}, Landroid/graphics/drawable/GradientDrawable;->ensureValidRect()Z

    #@3
    move-result v3

    #@4
    if-nez v3, :cond_7

    #@6
    .line 557
    :cond_6
    :goto_6
    return-void

    #@7
    .line 432
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@b
    invoke-virtual {v3}, Landroid/graphics/Paint;->getAlpha()I

    #@e
    move-result v15

    #@f
    .line 433
    .local v15, prevFillAlpha:I
    move-object/from16 v0, p0

    #@11
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@13
    if-eqz v3, :cond_eb

    #@15
    move-object/from16 v0, p0

    #@17
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@19
    invoke-virtual {v3}, Landroid/graphics/Paint;->getAlpha()I

    #@1c
    move-result v16

    #@1d
    .line 435
    .local v16, prevStrokeAlpha:I
    :goto_1d
    move-object/from16 v0, p0

    #@1f
    invoke-direct {v0, v15}, Landroid/graphics/drawable/GradientDrawable;->modulateAlpha(I)I

    #@22
    move-result v10

    #@23
    .line 436
    .local v10, currFillAlpha:I
    move-object/from16 v0, p0

    #@25
    move/from16 v1, v16

    #@27
    invoke-direct {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->modulateAlpha(I)I

    #@2a
    move-result v11

    #@2b
    .line 438
    .local v11, currStrokeAlpha:I
    if-lez v11, :cond_ef

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@31
    if-eqz v3, :cond_ef

    #@33
    move-object/from16 v0, p0

    #@35
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@37
    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    #@3a
    move-result v3

    #@3b
    const/4 v4, 0x0

    #@3c
    cmpl-float v3, v3, v4

    #@3e
    if-lez v3, :cond_ef

    #@40
    const/4 v13, 0x1

    #@41
    .line 440
    .local v13, haveStroke:Z
    :goto_41
    if-lez v10, :cond_f2

    #@43
    const/4 v12, 0x1

    #@44
    .line 441
    .local v12, haveFill:Z
    :goto_44
    move-object/from16 v0, p0

    #@46
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@48
    move-object/from16 v19, v0

    #@4a
    .line 447
    .local v19, st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    if-eqz v13, :cond_f5

    #@4c
    if-eqz v12, :cond_f5

    #@4e
    move-object/from16 v0, v19

    #@50
    iget v3, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@52
    const/4 v4, 0x2

    #@53
    if-eq v3, v4, :cond_f5

    #@55
    const/16 v3, 0xff

    #@57
    if-ge v11, v3, :cond_f5

    #@59
    move-object/from16 v0, p0

    #@5b
    iget v3, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@5d
    const/16 v4, 0xff

    #@5f
    if-lt v3, v4, :cond_67

    #@61
    move-object/from16 v0, p0

    #@63
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@65
    if-eqz v3, :cond_f5

    #@67
    :cond_67
    const/16 v20, 0x1

    #@69
    .line 457
    .local v20, useLayer:Z
    :goto_69
    if-eqz v20, :cond_f9

    #@6b
    .line 458
    move-object/from16 v0, p0

    #@6d
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@6f
    if-nez v3, :cond_7a

    #@71
    .line 459
    new-instance v3, Landroid/graphics/Paint;

    #@73
    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    #@76
    move-object/from16 v0, p0

    #@78
    iput-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@7a
    .line 461
    :cond_7a
    move-object/from16 v0, p0

    #@7c
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@7e
    move-object/from16 v0, p0

    #@80
    iget-boolean v4, v0, Landroid/graphics/drawable/GradientDrawable;->mDither:Z

    #@82
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setDither(Z)V

    #@85
    .line 462
    move-object/from16 v0, p0

    #@87
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@89
    move-object/from16 v0, p0

    #@8b
    iget v4, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@8d
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    #@90
    .line 463
    move-object/from16 v0, p0

    #@92
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@94
    move-object/from16 v0, p0

    #@96
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@98
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@9b
    .line 465
    move-object/from16 v0, p0

    #@9d
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@9f
    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    #@a2
    move-result v18

    #@a3
    .line 466
    .local v18, rad:F
    move-object/from16 v0, p0

    #@a5
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@a7
    iget v3, v3, Landroid/graphics/RectF;->left:F

    #@a9
    sub-float v4, v3, v18

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@af
    iget v3, v3, Landroid/graphics/RectF;->top:F

    #@b1
    sub-float v5, v3, v18

    #@b3
    move-object/from16 v0, p0

    #@b5
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@b7
    iget v3, v3, Landroid/graphics/RectF;->right:F

    #@b9
    add-float v6, v3, v18

    #@bb
    move-object/from16 v0, p0

    #@bd
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@bf
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    #@c1
    add-float v7, v3, v18

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget-object v8, v0, Landroid/graphics/drawable/GradientDrawable;->mLayerPaint:Landroid/graphics/Paint;

    #@c7
    const/4 v9, 0x4

    #@c8
    move-object/from16 v3, p1

    #@ca
    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    #@cd
    .line 472
    move-object/from16 v0, p0

    #@cf
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@d1
    const/4 v4, 0x0

    #@d2
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@d5
    .line 473
    move-object/from16 v0, p0

    #@d7
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@d9
    const/4 v4, 0x0

    #@da
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@dd
    .line 491
    .end local v18           #rad:F
    :cond_dd
    :goto_dd
    move-object/from16 v0, v19

    #@df
    iget v3, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mShape:I

    #@e1
    packed-switch v3, :pswitch_data_29c

    #@e4
    .line 549
    :cond_e4
    :goto_e4
    if-eqz v20, :cond_287

    #@e6
    .line 550
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    #@e9
    goto/16 :goto_6

    #@eb
    .line 433
    .end local v10           #currFillAlpha:I
    .end local v11           #currStrokeAlpha:I
    .end local v12           #haveFill:Z
    .end local v13           #haveStroke:Z
    .end local v16           #prevStrokeAlpha:I
    .end local v19           #st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    .end local v20           #useLayer:Z
    :cond_eb
    const/16 v16, 0x0

    #@ed
    goto/16 :goto_1d

    #@ef
    .line 438
    .restart local v10       #currFillAlpha:I
    .restart local v11       #currStrokeAlpha:I
    .restart local v16       #prevStrokeAlpha:I
    :cond_ef
    const/4 v13, 0x0

    #@f0
    goto/16 :goto_41

    #@f2
    .line 440
    .restart local v13       #haveStroke:Z
    :cond_f2
    const/4 v12, 0x0

    #@f3
    goto/16 :goto_44

    #@f5
    .line 447
    .restart local v12       #haveFill:Z
    .restart local v19       #st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    :cond_f5
    const/16 v20, 0x0

    #@f7
    goto/16 :goto_69

    #@f9
    .line 478
    .restart local v20       #useLayer:Z
    :cond_f9
    move-object/from16 v0, p0

    #@fb
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@fd
    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setAlpha(I)V

    #@100
    .line 479
    move-object/from16 v0, p0

    #@102
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@104
    move-object/from16 v0, p0

    #@106
    iget-boolean v4, v0, Landroid/graphics/drawable/GradientDrawable;->mDither:Z

    #@108
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setDither(Z)V

    #@10b
    .line 480
    move-object/from16 v0, p0

    #@10d
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@10f
    move-object/from16 v0, p0

    #@111
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@113
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@116
    .line 481
    move-object/from16 v0, p0

    #@118
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@11a
    if-eqz v3, :cond_131

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@120
    iget-boolean v3, v3, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHasSolidColor:Z

    #@122
    if-nez v3, :cond_131

    #@124
    .line 482
    move-object/from16 v0, p0

    #@126
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@128
    move-object/from16 v0, p0

    #@12a
    iget v4, v0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@12c
    shl-int/lit8 v4, v4, 0x18

    #@12e
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    #@131
    .line 484
    :cond_131
    if-eqz v13, :cond_dd

    #@133
    .line 485
    move-object/from16 v0, p0

    #@135
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@137
    invoke-virtual {v3, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    #@13a
    .line 486
    move-object/from16 v0, p0

    #@13c
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@13e
    move-object/from16 v0, p0

    #@140
    iget-boolean v4, v0, Landroid/graphics/drawable/GradientDrawable;->mDither:Z

    #@142
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setDither(Z)V

    #@145
    .line 487
    move-object/from16 v0, p0

    #@147
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@149
    move-object/from16 v0, p0

    #@14b
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@14d
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@150
    goto :goto_dd

    #@151
    .line 493
    :pswitch_151
    move-object/from16 v0, v19

    #@153
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@155
    if-eqz v3, :cond_1a2

    #@157
    .line 494
    move-object/from16 v0, p0

    #@159
    iget-boolean v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@15b
    if-nez v3, :cond_163

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget-boolean v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@161
    if-eqz v3, :cond_184

    #@163
    .line 495
    :cond_163
    move-object/from16 v0, p0

    #@165
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPath:Landroid/graphics/Path;

    #@167
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    #@16a
    .line 496
    move-object/from16 v0, p0

    #@16c
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPath:Landroid/graphics/Path;

    #@16e
    move-object/from16 v0, p0

    #@170
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@172
    move-object/from16 v0, v19

    #@174
    iget-object v6, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadiusArray:[F

    #@176
    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@178
    invoke-virtual {v3, v4, v6, v7}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    #@17b
    .line 497
    const/4 v3, 0x0

    #@17c
    move-object/from16 v0, p0

    #@17e
    iput-boolean v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@180
    move-object/from16 v0, p0

    #@182
    iput-boolean v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@184
    .line 499
    :cond_184
    move-object/from16 v0, p0

    #@186
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPath:Landroid/graphics/Path;

    #@188
    move-object/from16 v0, p0

    #@18a
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@18c
    move-object/from16 v0, p1

    #@18e
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@191
    .line 500
    if-eqz v13, :cond_e4

    #@193
    .line 501
    move-object/from16 v0, p0

    #@195
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mPath:Landroid/graphics/Path;

    #@197
    move-object/from16 v0, p0

    #@199
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@19b
    move-object/from16 v0, p1

    #@19d
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@1a0
    goto/16 :goto_e4

    #@1a2
    .line 503
    :cond_1a2
    move-object/from16 v0, v19

    #@1a4
    iget v3, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@1a6
    const/4 v4, 0x0

    #@1a7
    cmpl-float v3, v3, v4

    #@1a9
    if-lez v3, :cond_1f5

    #@1ab
    .line 509
    move-object/from16 v0, v19

    #@1ad
    iget v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mRadius:F

    #@1af
    move/from16 v18, v0

    #@1b1
    .line 510
    .restart local v18       #rad:F
    move-object/from16 v0, p0

    #@1b3
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@1b5
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    #@1b8
    move-result v3

    #@1b9
    move-object/from16 v0, p0

    #@1bb
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@1bd
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    #@1c0
    move-result v4

    #@1c1
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    #@1c4
    move-result v3

    #@1c5
    const/high16 v4, 0x3f00

    #@1c7
    mul-float v17, v3, v4

    #@1c9
    .line 511
    .local v17, r:F
    cmpl-float v3, v18, v17

    #@1cb
    if-lez v3, :cond_1cf

    #@1cd
    .line 512
    move/from16 v18, v17

    #@1cf
    .line 514
    :cond_1cf
    move-object/from16 v0, p0

    #@1d1
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@1d3
    move-object/from16 v0, p0

    #@1d5
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@1d7
    move-object/from16 v0, p1

    #@1d9
    move/from16 v1, v18

    #@1db
    move/from16 v2, v18

    #@1dd
    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    #@1e0
    .line 515
    if-eqz v13, :cond_e4

    #@1e2
    .line 516
    move-object/from16 v0, p0

    #@1e4
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@1ea
    move-object/from16 v0, p1

    #@1ec
    move/from16 v1, v18

    #@1ee
    move/from16 v2, v18

    #@1f0
    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    #@1f3
    goto/16 :goto_e4

    #@1f5
    .line 519
    .end local v17           #r:F
    .end local v18           #rad:F
    :cond_1f5
    move-object/from16 v0, p0

    #@1f7
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@1f9
    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    #@1fc
    move-result v3

    #@1fd
    if-nez v3, :cond_20f

    #@1ff
    move-object/from16 v0, p0

    #@201
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@203
    if-nez v3, :cond_20f

    #@205
    move-object/from16 v0, p0

    #@207
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@209
    invoke-virtual {v3}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    #@20c
    move-result-object v3

    #@20d
    if-eqz v3, :cond_21c

    #@20f
    .line 521
    :cond_20f
    move-object/from16 v0, p0

    #@211
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@213
    move-object/from16 v0, p0

    #@215
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@217
    move-object/from16 v0, p1

    #@219
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@21c
    .line 523
    :cond_21c
    if-eqz v13, :cond_e4

    #@21e
    .line 524
    move-object/from16 v0, p0

    #@220
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@222
    move-object/from16 v0, p0

    #@224
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@226
    move-object/from16 v0, p1

    #@228
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@22b
    goto/16 :goto_e4

    #@22d
    .line 529
    :pswitch_22d
    move-object/from16 v0, p0

    #@22f
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@231
    move-object/from16 v0, p0

    #@233
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@235
    move-object/from16 v0, p1

    #@237
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@23a
    .line 530
    if-eqz v13, :cond_e4

    #@23c
    .line 531
    move-object/from16 v0, p0

    #@23e
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@240
    move-object/from16 v0, p0

    #@242
    iget-object v4, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@244
    move-object/from16 v0, p1

    #@246
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@249
    goto/16 :goto_e4

    #@24b
    .line 535
    :pswitch_24b
    move-object/from16 v0, p0

    #@24d
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mRect:Landroid/graphics/RectF;

    #@24f
    move-object/from16 v17, v0

    #@251
    .line 536
    .local v17, r:Landroid/graphics/RectF;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/RectF;->centerY()F

    #@254
    move-result v5

    #@255
    .line 537
    .local v5, y:F
    move-object/from16 v0, v17

    #@257
    iget v4, v0, Landroid/graphics/RectF;->left:F

    #@259
    move-object/from16 v0, v17

    #@25b
    iget v6, v0, Landroid/graphics/RectF;->right:F

    #@25d
    move-object/from16 v0, p0

    #@25f
    iget-object v8, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@261
    move-object/from16 v3, p1

    #@263
    move v7, v5

    #@264
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    #@267
    goto/16 :goto_e4

    #@269
    .line 541
    .end local v5           #y:F
    .end local v17           #r:Landroid/graphics/RectF;
    :pswitch_269
    move-object/from16 v0, p0

    #@26b
    move-object/from16 v1, v19

    #@26d
    invoke-direct {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->buildRing(Landroid/graphics/drawable/GradientDrawable$GradientState;)Landroid/graphics/Path;

    #@270
    move-result-object v14

    #@271
    .line 542
    .local v14, path:Landroid/graphics/Path;
    move-object/from16 v0, p0

    #@273
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@275
    move-object/from16 v0, p1

    #@277
    invoke-virtual {v0, v14, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@27a
    .line 543
    if-eqz v13, :cond_e4

    #@27c
    .line 544
    move-object/from16 v0, p0

    #@27e
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@280
    move-object/from16 v0, p1

    #@282
    invoke-virtual {v0, v14, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@285
    goto/16 :goto_e4

    #@287
    .line 552
    .end local v14           #path:Landroid/graphics/Path;
    :cond_287
    move-object/from16 v0, p0

    #@289
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@28b
    invoke-virtual {v3, v15}, Landroid/graphics/Paint;->setAlpha(I)V

    #@28e
    .line 553
    if-eqz v13, :cond_6

    #@290
    .line 554
    move-object/from16 v0, p0

    #@292
    iget-object v3, v0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@294
    move/from16 v0, v16

    #@296
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    #@299
    goto/16 :goto_6

    #@29b
    .line 491
    nop

    #@29c
    :pswitch_data_29c
    .packed-switch 0x0
        :pswitch_151
        :pswitch_22d
        :pswitch_24b
        :pswitch_269
    .end packed-switch
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 630
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 1058
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->getChangingConfigurations()I

    #@5
    move-result v1

    #@6
    iput v1, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mChangingConfigurations:I

    #@8
    .line 1059
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@a
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 1053
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    iget v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mHeight:I

    #@4
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 1048
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    iget v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mWidth:I

    #@4
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 659
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-static {v0}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$200(Landroid/graphics/drawable/GradientDrawable$GradientState;)Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, -0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, -0x3

    #@b
    goto :goto_9
.end method

.method public getOrientation()Landroid/graphics/drawable/GradientDrawable$Orientation;
    .registers 2

    #@0
    .prologue
    .line 386
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@4
    return-object v0
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    .line 167
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPadding:Landroid/graphics/Rect;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 168
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPadding:Landroid/graphics/Rect;

    #@6
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@9
    .line 169
    const/4 v0, 0x1

    #@a
    .line 171
    :goto_a
    return v0

    #@b
    :cond_b
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@e
    move-result v0

    #@f
    goto :goto_a
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 41
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 804
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@4
    move-object/from16 v24, v0

    #@6
    .line 806
    .local v24, st:Landroid/graphics/drawable/GradientDrawable$GradientState;
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawable:[I

    #@8
    move-object/from16 v0, p1

    #@a
    move-object/from16 v1, p3

    #@c
    move-object/from16 v2, v31

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@11
    move-result-object v4

    #@12
    .line 809
    .local v4, a:Landroid/content/res/TypedArray;
    const/16 v31, 0x1

    #@14
    move-object/from16 v0, p0

    #@16
    move-object/from16 v1, p1

    #@18
    move-object/from16 v2, p2

    #@1a
    move/from16 v3, v31

    #@1c
    invoke-super {v0, v1, v2, v4, v3}, Landroid/graphics/drawable/Drawable;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@1f
    .line 812
    const/16 v31, 0x2

    #@21
    const/16 v32, 0x0

    #@23
    move/from16 v0, v31

    #@25
    move/from16 v1, v32

    #@27
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@2a
    move-result v23

    #@2b
    .line 814
    .local v23, shapeType:I
    const/16 v31, 0x0

    #@2d
    const/16 v32, 0x0

    #@2f
    move/from16 v0, v31

    #@31
    move/from16 v1, v32

    #@33
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@36
    move-result v14

    #@37
    .line 817
    .local v14, dither:Z
    const/16 v31, 0x3

    #@39
    move/from16 v0, v23

    #@3b
    move/from16 v1, v31

    #@3d
    if-ne v0, v1, :cond_b6

    #@3f
    .line 818
    const/16 v31, 0x6

    #@41
    const/16 v32, -0x1

    #@43
    move/from16 v0, v31

    #@45
    move/from16 v1, v32

    #@47
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@4a
    move-result v31

    #@4b
    move/from16 v0, v31

    #@4d
    move-object/from16 v1, v24

    #@4f
    iput v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@51
    .line 820
    move-object/from16 v0, v24

    #@53
    iget v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadius:I

    #@55
    move/from16 v31, v0

    #@57
    const/16 v32, -0x1

    #@59
    move/from16 v0, v31

    #@5b
    move/from16 v1, v32

    #@5d
    if-ne v0, v1, :cond_71

    #@5f
    .line 821
    const/16 v31, 0x3

    #@61
    const/high16 v32, 0x4040

    #@63
    move/from16 v0, v31

    #@65
    move/from16 v1, v32

    #@67
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@6a
    move-result v31

    #@6b
    move/from16 v0, v31

    #@6d
    move-object/from16 v1, v24

    #@6f
    iput v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mInnerRadiusRatio:F

    #@71
    .line 824
    :cond_71
    const/16 v31, 0x7

    #@73
    const/16 v32, -0x1

    #@75
    move/from16 v0, v31

    #@77
    move/from16 v1, v32

    #@79
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@7c
    move-result v31

    #@7d
    move/from16 v0, v31

    #@7f
    move-object/from16 v1, v24

    #@81
    iput v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@83
    .line 826
    move-object/from16 v0, v24

    #@85
    iget v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThickness:I

    #@87
    move/from16 v31, v0

    #@89
    const/16 v32, -0x1

    #@8b
    move/from16 v0, v31

    #@8d
    move/from16 v1, v32

    #@8f
    if-ne v0, v1, :cond_a3

    #@91
    .line 827
    const/16 v31, 0x4

    #@93
    const/high16 v32, 0x4110

    #@95
    move/from16 v0, v31

    #@97
    move/from16 v1, v32

    #@99
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@9c
    move-result v31

    #@9d
    move/from16 v0, v31

    #@9f
    move-object/from16 v1, v24

    #@a1
    iput v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mThicknessRatio:F

    #@a3
    .line 830
    :cond_a3
    const/16 v31, 0x5

    #@a5
    const/16 v32, 0x1

    #@a7
    move/from16 v0, v31

    #@a9
    move/from16 v1, v32

    #@ab
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@ae
    move-result v31

    #@af
    move-object/from16 v0, v24

    #@b1
    move/from16 v1, v31

    #@b3
    invoke-static {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$102(Landroid/graphics/drawable/GradientDrawable$GradientState;Z)Z

    #@b6
    .line 834
    :cond_b6
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@b9
    .line 836
    move-object/from16 v0, p0

    #@bb
    move/from16 v1, v23

    #@bd
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    #@c0
    .line 837
    move-object/from16 v0, p0

    #@c2
    invoke-virtual {v0, v14}, Landroid/graphics/drawable/GradientDrawable;->setDither(Z)V

    #@c5
    .line 841
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@c8
    move-result v31

    #@c9
    add-int/lit8 v19, v31, 0x1

    #@cb
    .line 844
    .local v19, innerDepth:I
    :cond_cb
    :goto_cb
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@ce
    move-result v29

    #@cf
    .local v29, type:I
    const/16 v31, 0x1

    #@d1
    move/from16 v0, v29

    #@d3
    move/from16 v1, v31

    #@d5
    if-eq v0, v1, :cond_52c

    #@d7
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@da
    move-result v13

    #@db
    .local v13, depth:I
    move/from16 v0, v19

    #@dd
    if-ge v13, v0, :cond_e7

    #@df
    const/16 v31, 0x3

    #@e1
    move/from16 v0, v29

    #@e3
    move/from16 v1, v31

    #@e5
    if-eq v0, v1, :cond_52c

    #@e7
    .line 846
    :cond_e7
    const/16 v31, 0x2

    #@e9
    move/from16 v0, v29

    #@eb
    move/from16 v1, v31

    #@ed
    if-ne v0, v1, :cond_cb

    #@ef
    .line 850
    move/from16 v0, v19

    #@f1
    if-gt v13, v0, :cond_cb

    #@f3
    .line 854
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@f6
    move-result-object v20

    #@f7
    .line 856
    .local v20, name:Ljava/lang/String;
    const-string/jumbo v31, "size"

    #@fa
    move-object/from16 v0, v20

    #@fc
    move-object/from16 v1, v31

    #@fe
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@101
    move-result v31

    #@102
    if-eqz v31, :cond_135

    #@104
    .line 857
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawableSize:[I

    #@106
    move-object/from16 v0, p1

    #@108
    move-object/from16 v1, p3

    #@10a
    move-object/from16 v2, v31

    #@10c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10f
    move-result-object v4

    #@110
    .line 859
    const/16 v31, 0x1

    #@112
    const/16 v32, -0x1

    #@114
    move/from16 v0, v31

    #@116
    move/from16 v1, v32

    #@118
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@11b
    move-result v30

    #@11c
    .line 861
    .local v30, width:I
    const/16 v31, 0x0

    #@11e
    const/16 v32, -0x1

    #@120
    move/from16 v0, v31

    #@122
    move/from16 v1, v32

    #@124
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@127
    move-result v18

    #@128
    .line 863
    .local v18, height:I
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@12b
    .line 864
    move-object/from16 v0, p0

    #@12d
    move/from16 v1, v30

    #@12f
    move/from16 v2, v18

    #@131
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    #@134
    goto :goto_cb

    #@135
    .line 865
    .end local v18           #height:I
    .end local v30           #width:I
    :cond_135
    const-string v31, "gradient"

    #@137
    move-object/from16 v0, v20

    #@139
    move-object/from16 v1, v31

    #@13b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13e
    move-result v31

    #@13f
    if-eqz v31, :cond_351

    #@141
    .line 866
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawableGradient:[I

    #@143
    move-object/from16 v0, p1

    #@145
    move-object/from16 v1, p3

    #@147
    move-object/from16 v2, v31

    #@149
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@14c
    move-result-object v4

    #@14d
    .line 868
    const/16 v31, 0x0

    #@14f
    const/16 v32, 0x0

    #@151
    move/from16 v0, v31

    #@153
    move/from16 v1, v32

    #@155
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@158
    move-result v25

    #@159
    .line 870
    .local v25, startColor:I
    const/16 v31, 0x8

    #@15b
    move/from16 v0, v31

    #@15d
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    #@160
    move-result v17

    #@161
    .line 872
    .local v17, hasCenterColor:Z
    const/16 v31, 0x8

    #@163
    const/16 v32, 0x0

    #@165
    move/from16 v0, v31

    #@167
    move/from16 v1, v32

    #@169
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@16c
    move-result v9

    #@16d
    .line 874
    .local v9, centerColor:I
    const/16 v31, 0x1

    #@16f
    const/16 v32, 0x0

    #@171
    move/from16 v0, v31

    #@173
    move/from16 v1, v32

    #@175
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@178
    move-result v15

    #@179
    .line 876
    .local v15, endColor:I
    const/16 v31, 0x4

    #@17b
    const/16 v32, 0x0

    #@17d
    move/from16 v0, v31

    #@17f
    move/from16 v1, v32

    #@181
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@184
    move-result v16

    #@185
    .line 880
    .local v16, gradientType:I
    const/16 v31, 0x5

    #@187
    const/high16 v32, 0x3f00

    #@189
    move/from16 v0, v31

    #@18b
    move/from16 v1, v32

    #@18d
    invoke-static {v4, v0, v1}, Landroid/graphics/drawable/GradientDrawable;->getFloatOrFraction(Landroid/content/res/TypedArray;IF)F

    #@190
    move-result v31

    #@191
    move-object/from16 v0, v24

    #@193
    move/from16 v1, v31

    #@195
    invoke-static {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$302(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F

    #@198
    .line 885
    const/16 v31, 0x6

    #@19a
    const/high16 v32, 0x3f00

    #@19c
    move/from16 v0, v31

    #@19e
    move/from16 v1, v32

    #@1a0
    invoke-static {v4, v0, v1}, Landroid/graphics/drawable/GradientDrawable;->getFloatOrFraction(Landroid/content/res/TypedArray;IF)F

    #@1a3
    move-result v31

    #@1a4
    move-object/from16 v0, v24

    #@1a6
    move/from16 v1, v31

    #@1a8
    invoke-static {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$402(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F

    #@1ab
    .line 890
    const/16 v31, 0x2

    #@1ad
    const/16 v32, 0x0

    #@1af
    move/from16 v0, v31

    #@1b1
    move/from16 v1, v32

    #@1b3
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1b6
    move-result v31

    #@1b7
    move-object/from16 v0, v24

    #@1b9
    move/from16 v1, v31

    #@1bb
    invoke-static {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$002(Landroid/graphics/drawable/GradientDrawable$GradientState;Z)Z

    #@1be
    .line 892
    move/from16 v0, v16

    #@1c0
    move-object/from16 v1, v24

    #@1c2
    iput v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mGradient:I

    #@1c4
    .line 894
    if-nez v16, :cond_2c1

    #@1c6
    .line 895
    const/16 v31, 0x3

    #@1c8
    const/16 v32, 0x0

    #@1ca
    move/from16 v0, v31

    #@1cc
    move/from16 v1, v32

    #@1ce
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@1d1
    move-result v31

    #@1d2
    move/from16 v0, v31

    #@1d4
    float-to-int v5, v0

    #@1d5
    .line 897
    .local v5, angle:I
    rem-int/lit16 v5, v5, 0x168

    #@1d7
    .line 898
    rem-int/lit8 v31, v5, 0x2d

    #@1d9
    if-eqz v31, :cond_1fe

    #@1db
    .line 899
    new-instance v31, Lorg/xmlpull/v1/XmlPullParserException;

    #@1dd
    new-instance v32, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    #@1e5
    move-result-object v33

    #@1e6
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v32

    #@1ea
    const-string v33, "<gradient> tag requires \'angle\' attribute to "

    #@1ec
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v32

    #@1f0
    const-string v33, "be a multiple of 45"

    #@1f2
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v32

    #@1f6
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f9
    move-result-object v32

    #@1fa
    invoke-direct/range {v31 .. v32}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@1fd
    throw v31

    #@1fe
    .line 904
    :cond_1fe
    sparse-switch v5, :sswitch_data_536

    #@201
    .line 945
    .end local v5           #angle:I
    :cond_201
    :goto_201
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@204
    .line 947
    if-eqz v17, :cond_32d

    #@206
    .line 948
    const/16 v31, 0x3

    #@208
    move/from16 v0, v31

    #@20a
    new-array v0, v0, [I

    #@20c
    move-object/from16 v31, v0

    #@20e
    move-object/from16 v0, v31

    #@210
    move-object/from16 v1, v24

    #@212
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@214
    .line 949
    move-object/from16 v0, v24

    #@216
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@218
    move-object/from16 v31, v0

    #@21a
    const/16 v32, 0x0

    #@21c
    aput v25, v31, v32

    #@21e
    .line 950
    move-object/from16 v0, v24

    #@220
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@222
    move-object/from16 v31, v0

    #@224
    const/16 v32, 0x1

    #@226
    aput v9, v31, v32

    #@228
    .line 951
    move-object/from16 v0, v24

    #@22a
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@22c
    move-object/from16 v31, v0

    #@22e
    const/16 v32, 0x2

    #@230
    aput v15, v31, v32

    #@232
    .line 953
    const/16 v31, 0x3

    #@234
    move/from16 v0, v31

    #@236
    new-array v0, v0, [F

    #@238
    move-object/from16 v31, v0

    #@23a
    move-object/from16 v0, v31

    #@23c
    move-object/from16 v1, v24

    #@23e
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@240
    .line 954
    move-object/from16 v0, v24

    #@242
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@244
    move-object/from16 v31, v0

    #@246
    const/16 v32, 0x0

    #@248
    const/16 v33, 0x0

    #@24a
    aput v33, v31, v32

    #@24c
    .line 956
    move-object/from16 v0, v24

    #@24e
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@250
    move-object/from16 v32, v0

    #@252
    const/16 v33, 0x1

    #@254
    invoke-static/range {v24 .. v24}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$300(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@257
    move-result v31

    #@258
    const/high16 v34, 0x3f00

    #@25a
    cmpl-float v31, v31, v34

    #@25c
    if-eqz v31, :cond_327

    #@25e
    invoke-static/range {v24 .. v24}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$300(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@261
    move-result v31

    #@262
    :goto_262
    aput v31, v32, v33

    #@264
    .line 957
    move-object/from16 v0, v24

    #@266
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPositions:[F

    #@268
    move-object/from16 v31, v0

    #@26a
    const/16 v32, 0x2

    #@26c
    const/high16 v33, 0x3f80

    #@26e
    aput v33, v31, v32

    #@270
    goto/16 :goto_cb

    #@272
    .line 906
    .restart local v5       #angle:I
    :sswitch_272
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@274
    move-object/from16 v0, v31

    #@276
    move-object/from16 v1, v24

    #@278
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@27a
    goto :goto_201

    #@27b
    .line 909
    :sswitch_27b
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->BL_TR:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@27d
    move-object/from16 v0, v31

    #@27f
    move-object/from16 v1, v24

    #@281
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@283
    goto/16 :goto_201

    #@285
    .line 912
    :sswitch_285
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@287
    move-object/from16 v0, v31

    #@289
    move-object/from16 v1, v24

    #@28b
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@28d
    goto/16 :goto_201

    #@28f
    .line 915
    :sswitch_28f
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->BR_TL:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@291
    move-object/from16 v0, v31

    #@293
    move-object/from16 v1, v24

    #@295
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@297
    goto/16 :goto_201

    #@299
    .line 918
    :sswitch_299
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@29b
    move-object/from16 v0, v31

    #@29d
    move-object/from16 v1, v24

    #@29f
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2a1
    goto/16 :goto_201

    #@2a3
    .line 921
    :sswitch_2a3
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->TR_BL:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2a5
    move-object/from16 v0, v31

    #@2a7
    move-object/from16 v1, v24

    #@2a9
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2ab
    goto/16 :goto_201

    #@2ad
    .line 924
    :sswitch_2ad
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2af
    move-object/from16 v0, v31

    #@2b1
    move-object/from16 v1, v24

    #@2b3
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2b5
    goto/16 :goto_201

    #@2b7
    .line 927
    :sswitch_2b7
    sget-object v31, Landroid/graphics/drawable/GradientDrawable$Orientation;->TL_BR:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2b9
    move-object/from16 v0, v31

    #@2bb
    move-object/from16 v1, v24

    #@2bd
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@2bf
    goto/16 :goto_201

    #@2c1
    .line 931
    .end local v5           #angle:I
    :cond_2c1
    const/16 v31, 0x7

    #@2c3
    move/from16 v0, v31

    #@2c5
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@2c8
    move-result-object v28

    #@2c9
    .line 933
    .local v28, tv:Landroid/util/TypedValue;
    if-eqz v28, :cond_2fc

    #@2cb
    .line 934
    move-object/from16 v0, v28

    #@2cd
    iget v0, v0, Landroid/util/TypedValue;->type:I

    #@2cf
    move/from16 v31, v0

    #@2d1
    const/16 v32, 0x6

    #@2d3
    move/from16 v0, v31

    #@2d5
    move/from16 v1, v32

    #@2d7
    if-ne v0, v1, :cond_2f4

    #@2d9
    const/16 v22, 0x1

    #@2db
    .line 935
    .local v22, radiusRel:Z
    :goto_2db
    if-eqz v22, :cond_2f7

    #@2dd
    const/high16 v31, 0x3f80

    #@2df
    const/high16 v32, 0x3f80

    #@2e1
    move-object/from16 v0, v28

    #@2e3
    move/from16 v1, v31

    #@2e5
    move/from16 v2, v32

    #@2e7
    invoke-virtual {v0, v1, v2}, Landroid/util/TypedValue;->getFraction(FF)F

    #@2ea
    move-result v31

    #@2eb
    :goto_2eb
    move-object/from16 v0, v24

    #@2ed
    move/from16 v1, v31

    #@2ef
    invoke-static {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$502(Landroid/graphics/drawable/GradientDrawable$GradientState;F)F

    #@2f2
    goto/16 :goto_201

    #@2f4
    .line 934
    .end local v22           #radiusRel:Z
    :cond_2f4
    const/16 v22, 0x0

    #@2f6
    goto :goto_2db

    #@2f7
    .line 935
    .restart local v22       #radiusRel:Z
    :cond_2f7
    invoke-virtual/range {v28 .. v28}, Landroid/util/TypedValue;->getFloat()F

    #@2fa
    move-result v31

    #@2fb
    goto :goto_2eb

    #@2fc
    .line 937
    .end local v22           #radiusRel:Z
    :cond_2fc
    const/16 v31, 0x1

    #@2fe
    move/from16 v0, v16

    #@300
    move/from16 v1, v31

    #@302
    if-ne v0, v1, :cond_201

    #@304
    .line 938
    new-instance v31, Lorg/xmlpull/v1/XmlPullParserException;

    #@306
    new-instance v32, Ljava/lang/StringBuilder;

    #@308
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@30b
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    #@30e
    move-result-object v33

    #@30f
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@312
    move-result-object v32

    #@313
    const-string v33, "<gradient> tag requires \'gradientRadius\' "

    #@315
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@318
    move-result-object v32

    #@319
    const-string v33, "attribute with radial type"

    #@31b
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31e
    move-result-object v32

    #@31f
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@322
    move-result-object v32

    #@323
    invoke-direct/range {v31 .. v32}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@326
    throw v31

    #@327
    .line 956
    .end local v28           #tv:Landroid/util/TypedValue;
    :cond_327
    invoke-static/range {v24 .. v24}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$400(Landroid/graphics/drawable/GradientDrawable$GradientState;)F

    #@32a
    move-result v31

    #@32b
    goto/16 :goto_262

    #@32d
    .line 959
    :cond_32d
    const/16 v31, 0x2

    #@32f
    move/from16 v0, v31

    #@331
    new-array v0, v0, [I

    #@333
    move-object/from16 v31, v0

    #@335
    move-object/from16 v0, v31

    #@337
    move-object/from16 v1, v24

    #@339
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@33b
    .line 960
    move-object/from16 v0, v24

    #@33d
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@33f
    move-object/from16 v31, v0

    #@341
    const/16 v32, 0x0

    #@343
    aput v25, v31, v32

    #@345
    .line 961
    move-object/from16 v0, v24

    #@347
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mColors:[I

    #@349
    move-object/from16 v31, v0

    #@34b
    const/16 v32, 0x1

    #@34d
    aput v15, v31, v32

    #@34f
    goto/16 :goto_cb

    #@351
    .line 964
    .end local v9           #centerColor:I
    .end local v15           #endColor:I
    .end local v16           #gradientType:I
    .end local v17           #hasCenterColor:Z
    .end local v25           #startColor:I
    :cond_351
    const-string/jumbo v31, "solid"

    #@354
    move-object/from16 v0, v20

    #@356
    move-object/from16 v1, v31

    #@358
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@35b
    move-result v31

    #@35c
    if-eqz v31, :cond_380

    #@35e
    .line 965
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawableSolid:[I

    #@360
    move-object/from16 v0, p1

    #@362
    move-object/from16 v1, p3

    #@364
    move-object/from16 v2, v31

    #@366
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@369
    move-result-object v4

    #@36a
    .line 967
    const/16 v31, 0x0

    #@36c
    const/16 v32, 0x0

    #@36e
    move/from16 v0, v31

    #@370
    move/from16 v1, v32

    #@372
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@375
    move-result v6

    #@376
    .line 969
    .local v6, argb:I
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@379
    .line 970
    move-object/from16 v0, p0

    #@37b
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    #@37e
    goto/16 :goto_cb

    #@380
    .line 971
    .end local v6           #argb:I
    :cond_380
    const-string/jumbo v31, "stroke"

    #@383
    move-object/from16 v0, v20

    #@385
    move-object/from16 v1, v31

    #@387
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38a
    move-result v31

    #@38b
    if-eqz v31, :cond_3e3

    #@38d
    .line 972
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawableStroke:[I

    #@38f
    move-object/from16 v0, p1

    #@391
    move-object/from16 v1, p3

    #@393
    move-object/from16 v2, v31

    #@395
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@398
    move-result-object v4

    #@399
    .line 974
    const/16 v31, 0x0

    #@39b
    const/16 v32, 0x0

    #@39d
    move/from16 v0, v31

    #@39f
    move/from16 v1, v32

    #@3a1
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@3a4
    move-result v30

    #@3a5
    .line 976
    .restart local v30       #width:I
    const/16 v31, 0x1

    #@3a7
    const/16 v32, 0x0

    #@3a9
    move/from16 v0, v31

    #@3ab
    move/from16 v1, v32

    #@3ad
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@3b0
    move-result v10

    #@3b1
    .line 978
    .local v10, color:I
    const/16 v31, 0x2

    #@3b3
    const/16 v32, 0x0

    #@3b5
    move/from16 v0, v31

    #@3b7
    move/from16 v1, v32

    #@3b9
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@3bc
    move-result v12

    #@3bd
    .line 980
    .local v12, dashWidth:F
    const/16 v31, 0x0

    #@3bf
    cmpl-float v31, v12, v31

    #@3c1
    if-eqz v31, :cond_3db

    #@3c3
    .line 981
    const/16 v31, 0x3

    #@3c5
    const/16 v32, 0x0

    #@3c7
    move/from16 v0, v31

    #@3c9
    move/from16 v1, v32

    #@3cb
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@3ce
    move-result v11

    #@3cf
    .line 983
    .local v11, dashGap:F
    move-object/from16 v0, p0

    #@3d1
    move/from16 v1, v30

    #@3d3
    invoke-virtual {v0, v1, v10, v12, v11}, Landroid/graphics/drawable/GradientDrawable;->setStroke(IIFF)V

    #@3d6
    .line 987
    .end local v11           #dashGap:F
    :goto_3d6
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@3d9
    goto/16 :goto_cb

    #@3db
    .line 985
    :cond_3db
    move-object/from16 v0, p0

    #@3dd
    move/from16 v1, v30

    #@3df
    invoke-virtual {v0, v1, v10}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    #@3e2
    goto :goto_3d6

    #@3e3
    .line 988
    .end local v10           #color:I
    .end local v12           #dashWidth:F
    .end local v30           #width:I
    :cond_3e3
    const-string v31, "corners"

    #@3e5
    move-object/from16 v0, v20

    #@3e7
    move-object/from16 v1, v31

    #@3e9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ec
    move-result v31

    #@3ed
    if-eqz v31, :cond_4a3

    #@3ef
    .line 989
    sget-object v31, Lcom/android/internal/R$styleable;->DrawableCorners:[I

    #@3f1
    move-object/from16 v0, p1

    #@3f3
    move-object/from16 v1, p3

    #@3f5
    move-object/from16 v2, v31

    #@3f7
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@3fa
    move-result-object v4

    #@3fb
    .line 991
    const/16 v31, 0x0

    #@3fd
    const/16 v32, 0x0

    #@3ff
    move/from16 v0, v31

    #@401
    move/from16 v1, v32

    #@403
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@406
    move-result v21

    #@407
    .line 993
    .local v21, radius:I
    move/from16 v0, v21

    #@409
    int-to-float v0, v0

    #@40a
    move/from16 v31, v0

    #@40c
    move-object/from16 v0, p0

    #@40e
    move/from16 v1, v31

    #@410
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    #@413
    .line 994
    const/16 v31, 0x1

    #@415
    move/from16 v0, v31

    #@417
    move/from16 v1, v21

    #@419
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@41c
    move-result v26

    #@41d
    .line 996
    .local v26, topLeftRadius:I
    const/16 v31, 0x2

    #@41f
    move/from16 v0, v31

    #@421
    move/from16 v1, v21

    #@423
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@426
    move-result v27

    #@427
    .line 998
    .local v27, topRightRadius:I
    const/16 v31, 0x3

    #@429
    move/from16 v0, v31

    #@42b
    move/from16 v1, v21

    #@42d
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@430
    move-result v7

    #@431
    .line 1000
    .local v7, bottomLeftRadius:I
    const/16 v31, 0x4

    #@433
    move/from16 v0, v31

    #@435
    move/from16 v1, v21

    #@437
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@43a
    move-result v8

    #@43b
    .line 1002
    .local v8, bottomRightRadius:I
    move/from16 v0, v26

    #@43d
    move/from16 v1, v21

    #@43f
    if-ne v0, v1, :cond_44f

    #@441
    move/from16 v0, v27

    #@443
    move/from16 v1, v21

    #@445
    if-ne v0, v1, :cond_44f

    #@447
    move/from16 v0, v21

    #@449
    if-ne v7, v0, :cond_44f

    #@44b
    move/from16 v0, v21

    #@44d
    if-eq v8, v0, :cond_49e

    #@44f
    .line 1005
    :cond_44f
    const/16 v31, 0x8

    #@451
    move/from16 v0, v31

    #@453
    new-array v0, v0, [F

    #@455
    move-object/from16 v31, v0

    #@457
    const/16 v32, 0x0

    #@459
    move/from16 v0, v26

    #@45b
    int-to-float v0, v0

    #@45c
    move/from16 v33, v0

    #@45e
    aput v33, v31, v32

    #@460
    const/16 v32, 0x1

    #@462
    move/from16 v0, v26

    #@464
    int-to-float v0, v0

    #@465
    move/from16 v33, v0

    #@467
    aput v33, v31, v32

    #@469
    const/16 v32, 0x2

    #@46b
    move/from16 v0, v27

    #@46d
    int-to-float v0, v0

    #@46e
    move/from16 v33, v0

    #@470
    aput v33, v31, v32

    #@472
    const/16 v32, 0x3

    #@474
    move/from16 v0, v27

    #@476
    int-to-float v0, v0

    #@477
    move/from16 v33, v0

    #@479
    aput v33, v31, v32

    #@47b
    const/16 v32, 0x4

    #@47d
    int-to-float v0, v8

    #@47e
    move/from16 v33, v0

    #@480
    aput v33, v31, v32

    #@482
    const/16 v32, 0x5

    #@484
    int-to-float v0, v8

    #@485
    move/from16 v33, v0

    #@487
    aput v33, v31, v32

    #@489
    const/16 v32, 0x6

    #@48b
    int-to-float v0, v7

    #@48c
    move/from16 v33, v0

    #@48e
    aput v33, v31, v32

    #@490
    const/16 v32, 0x7

    #@492
    int-to-float v0, v7

    #@493
    move/from16 v33, v0

    #@495
    aput v33, v31, v32

    #@497
    move-object/from16 v0, p0

    #@499
    move-object/from16 v1, v31

    #@49b
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    #@49e
    .line 1012
    :cond_49e
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@4a1
    goto/16 :goto_cb

    #@4a3
    .line 1013
    .end local v7           #bottomLeftRadius:I
    .end local v8           #bottomRightRadius:I
    .end local v21           #radius:I
    .end local v26           #topLeftRadius:I
    .end local v27           #topRightRadius:I
    :cond_4a3
    const-string/jumbo v31, "padding"

    #@4a6
    move-object/from16 v0, v20

    #@4a8
    move-object/from16 v1, v31

    #@4aa
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4ad
    move-result v31

    #@4ae
    if-eqz v31, :cond_50e

    #@4b0
    .line 1014
    sget-object v31, Lcom/android/internal/R$styleable;->GradientDrawablePadding:[I

    #@4b2
    move-object/from16 v0, p1

    #@4b4
    move-object/from16 v1, p3

    #@4b6
    move-object/from16 v2, v31

    #@4b8
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4bb
    move-result-object v4

    #@4bc
    .line 1016
    new-instance v31, Landroid/graphics/Rect;

    #@4be
    const/16 v32, 0x0

    #@4c0
    const/16 v33, 0x0

    #@4c2
    move/from16 v0, v32

    #@4c4
    move/from16 v1, v33

    #@4c6
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@4c9
    move-result v32

    #@4ca
    const/16 v33, 0x1

    #@4cc
    const/16 v34, 0x0

    #@4ce
    move/from16 v0, v33

    #@4d0
    move/from16 v1, v34

    #@4d2
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@4d5
    move-result v33

    #@4d6
    const/16 v34, 0x2

    #@4d8
    const/16 v35, 0x0

    #@4da
    move/from16 v0, v34

    #@4dc
    move/from16 v1, v35

    #@4de
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@4e1
    move-result v34

    #@4e2
    const/16 v35, 0x3

    #@4e4
    const/16 v36, 0x0

    #@4e6
    move/from16 v0, v35

    #@4e8
    move/from16 v1, v36

    #@4ea
    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@4ed
    move-result v35

    #@4ee
    invoke-direct/range {v31 .. v35}, Landroid/graphics/Rect;-><init>(IIII)V

    #@4f1
    move-object/from16 v0, v31

    #@4f3
    move-object/from16 v1, p0

    #@4f5
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable;->mPadding:Landroid/graphics/Rect;

    #@4f7
    .line 1025
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    #@4fa
    .line 1026
    move-object/from16 v0, p0

    #@4fc
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@4fe
    move-object/from16 v31, v0

    #@500
    move-object/from16 v0, p0

    #@502
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mPadding:Landroid/graphics/Rect;

    #@504
    move-object/from16 v32, v0

    #@506
    move-object/from16 v0, v32

    #@508
    move-object/from16 v1, v31

    #@50a
    iput-object v0, v1, Landroid/graphics/drawable/GradientDrawable$GradientState;->mPadding:Landroid/graphics/Rect;

    #@50c
    goto/16 :goto_cb

    #@50e
    .line 1028
    :cond_50e
    const-string v31, "drawable"

    #@510
    new-instance v32, Ljava/lang/StringBuilder;

    #@512
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@515
    const-string v33, "Bad element under <shape>: "

    #@517
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51a
    move-result-object v32

    #@51b
    move-object/from16 v0, v32

    #@51d
    move-object/from16 v1, v20

    #@51f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@522
    move-result-object v32

    #@523
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@526
    move-result-object v32

    #@527
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52a
    goto/16 :goto_cb

    #@52c
    .line 1033
    .end local v13           #depth:I
    .end local v20           #name:Ljava/lang/String;
    :cond_52c
    move-object/from16 v0, p0

    #@52e
    iget-object v0, v0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@530
    move-object/from16 v31, v0

    #@532
    invoke-static/range {v31 .. v31}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$600(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@535
    .line 1034
    return-void

    #@536
    .line 904
    :sswitch_data_536
    .sparse-switch
        0x0 -> :sswitch_272
        0x2d -> :sswitch_27b
        0x5a -> :sswitch_285
        0x87 -> :sswitch_28f
        0xb4 -> :sswitch_299
        0xe1 -> :sswitch_2a3
        0x10e -> :sswitch_2ad
        0x13b -> :sswitch_2b7
    .end sparse-switch
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 1064
    iget-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_1b

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_1b

    #@a
    .line 1065
    new-instance v0, Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@c
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@e
    invoke-direct {v0, v1}, Landroid/graphics/drawable/GradientDrawable$GradientState;-><init>(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@11
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@13
    .line 1066
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@15
    invoke-direct {p0, v0}, Landroid/graphics/drawable/GradientDrawable;->initializeWithState(Landroid/graphics/drawable/GradientDrawable$GradientState;)V

    #@18
    .line 1067
    const/4 v0, 0x1

    #@19
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mMutated:Z

    #@1b
    .line 1069
    :cond_1b
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "r"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 664
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@4
    .line 665
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@7
    .line 666
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@9
    .line 667
    iput-boolean v1, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@b
    .line 668
    return-void
.end method

.method protected onLevelChange(I)Z
    .registers 3
    .parameter "level"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 672
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onLevelChange(I)Z

    #@4
    .line 673
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@6
    .line 674
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@8
    .line 675
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 676
    return v0
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 635
    iget v0, p0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 636
    iput p1, p0, Landroid/graphics/drawable/GradientDrawable;->mAlpha:I

    #@6
    .line 637
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@9
    .line 639
    :cond_9
    return-void
.end method

.method public setColor(I)V
    .registers 3
    .parameter "argb"

    #@0
    .prologue
    .line 623
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setSolidColor(I)V

    #@5
    .line 624
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mFillPaint:Landroid/graphics/Paint;

    #@7
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    #@a
    .line 625
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@d
    .line 626
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 651
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 652
    iput-object p1, p0, Landroid/graphics/drawable/GradientDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    #@6
    .line 653
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@9
    .line 655
    :cond_9
    return-void
.end method

.method public setColors([I)V
    .registers 3
    .parameter "colors"

    #@0
    .prologue
    .line 418
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setColors([I)V

    #@5
    .line 419
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@8
    .line 420
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 421
    return-void
.end method

.method public setCornerRadii([F)V
    .registers 3
    .parameter "radii"

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setCornerRadii([F)V

    #@5
    .line 193
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@8
    .line 194
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 195
    return-void
.end method

.method public setCornerRadius(F)V
    .registers 3
    .parameter "radius"

    #@0
    .prologue
    .line 212
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setCornerRadius(F)V

    #@5
    .line 213
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@8
    .line 214
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 215
    return-void
.end method

.method public setDither(Z)V
    .registers 3
    .parameter "dither"

    #@0
    .prologue
    .line 643
    iget-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mDither:Z

    #@2
    if-eq p1, v0, :cond_9

    #@4
    .line 644
    iput-boolean p1, p0, Landroid/graphics/drawable/GradientDrawable;->mDither:Z

    #@6
    .line 645
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@9
    .line 647
    :cond_9
    return-void
.end method

.method public setGradientCenter(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 335
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setGradientCenter(FF)V

    #@5
    .line 336
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@8
    .line 337
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 338
    return-void
.end method

.method public setGradientRadius(F)V
    .registers 3
    .parameter "gradientRadius"

    #@0
    .prologue
    .line 353
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setGradientRadius(F)V

    #@5
    .line 354
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@8
    .line 355
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 356
    return-void
.end method

.method public setGradientType(I)V
    .registers 3
    .parameter "gradient"

    #@0
    .prologue
    .line 316
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setGradientType(I)V

    #@5
    .line 317
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@8
    .line 318
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 319
    return-void
.end method

.method public setOrientation(Landroid/graphics/drawable/GradientDrawable$Orientation;)V
    .registers 3
    .parameter "orientation"

    #@0
    .prologue
    .line 400
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    iput-object p1, v0, Landroid/graphics/drawable/GradientDrawable$GradientState;->mOrientation:Landroid/graphics/drawable/GradientDrawable$Orientation;

    #@4
    .line 401
    const/4 v0, 0x1

    #@5
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@7
    .line 402
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@a
    .line 403
    return-void
.end method

.method public setShape(I)V
    .registers 3
    .parameter "shape"

    #@0
    .prologue
    .line 298
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRingPath:Landroid/graphics/Path;

    #@3
    .line 299
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@6
    .line 300
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@8
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setShape(I)V

    #@b
    .line 301
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@e
    .line 302
    return-void
.end method

.method public setSize(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 281
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setSize(II)V

    #@5
    .line 282
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mPathIsDirty:Z

    #@8
    .line 283
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 284
    return-void
.end method

.method public setStroke(II)V
    .registers 4
    .parameter "width"
    .parameter "color"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 231
    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/graphics/drawable/GradientDrawable;->setStroke(IIFF)V

    #@4
    .line 232
    return-void
.end method

.method public setStroke(IIFF)V
    .registers 10
    .parameter "width"
    .parameter "color"
    .parameter "dashWidth"
    .parameter "dashGap"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 250
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@4
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/drawable/GradientDrawable$GradientState;->setStroke(IIFF)V

    #@7
    .line 252
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@9
    if-nez v1, :cond_19

    #@b
    .line 253
    new-instance v1, Landroid/graphics/Paint;

    #@d
    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    #@10
    iput-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@12
    .line 254
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@14
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@16
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@19
    .line 256
    :cond_19
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@1b
    int-to-float v2, p1

    #@1c
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    #@1f
    .line 257
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@21
    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    #@24
    .line 259
    const/4 v0, 0x0

    #@25
    .line 260
    .local v0, e:Landroid/graphics/DashPathEffect;
    cmpl-float v1, p3, v3

    #@27
    if-lez v1, :cond_36

    #@29
    .line 261
    new-instance v0, Landroid/graphics/DashPathEffect;

    #@2b
    .end local v0           #e:Landroid/graphics/DashPathEffect;
    const/4 v1, 0x2

    #@2c
    new-array v1, v1, [F

    #@2e
    const/4 v2, 0x0

    #@2f
    aput p3, v1, v2

    #@31
    aput p4, v1, v4

    #@33
    invoke-direct {v0, v1, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    #@36
    .line 263
    .restart local v0       #e:Landroid/graphics/DashPathEffect;
    :cond_36
    iget-object v1, p0, Landroid/graphics/drawable/GradientDrawable;->mStrokePaint:Landroid/graphics/Paint;

    #@38
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    #@3b
    .line 264
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@3e
    .line 265
    return-void
.end method

.method public setUseLevel(Z)V
    .registers 3
    .parameter "useLevel"

    #@0
    .prologue
    .line 372
    iget-object v0, p0, Landroid/graphics/drawable/GradientDrawable;->mGradientState:Landroid/graphics/drawable/GradientDrawable$GradientState;

    #@2
    invoke-static {v0, p1}, Landroid/graphics/drawable/GradientDrawable$GradientState;->access$002(Landroid/graphics/drawable/GradientDrawable$GradientState;Z)Z

    #@5
    .line 373
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/graphics/drawable/GradientDrawable;->mRectIsDirty:Z

    #@8
    .line 374
    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    #@b
    .line 375
    return-void
.end method
