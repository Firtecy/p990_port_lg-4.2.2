.class public Landroid/graphics/drawable/ShapeDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "ShapeDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/ShapeDrawable$1;,
        Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;,
        Landroid/graphics/drawable/ShapeDrawable$ShapeState;
    }
.end annotation


# instance fields
.field private mMutated:Z

.field private mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    check-cast v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@3
    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;)V

    #@6
    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 78
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 79
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@5
    invoke-direct {v0, p1}, Landroid/graphics/drawable/ShapeDrawable$ShapeState;-><init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;)V

    #@8
    iput-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@a
    .line 80
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;Landroid/graphics/drawable/ShapeDrawable$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;)V

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/shapes/Shape;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 73
    const/4 v0, 0x0

    #@1
    check-cast v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@3
    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/ShapeDrawable$ShapeState;)V

    #@6
    .line 75
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@8
    iput-object p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@a
    .line 76
    return-void
.end method

.method private static modulateAlpha(II)I
    .registers 4
    .parameter "paintAlpha"
    .parameter "alpha"

    #@0
    .prologue
    .line 199
    ushr-int/lit8 v1, p1, 0x7

    #@2
    add-int v0, p1, v1

    #@4
    .line 200
    .local v0, scale:I
    mul-int v1, p0, v0

    #@6
    ushr-int/lit8 v1, v1, 0x8

    #@8
    return v1
.end method

.method private updateShape()V
    .registers 7

    #@0
    .prologue
    .line 354
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@4
    if-eqz v3, :cond_30

    #@6
    .line 355
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->getBounds()Landroid/graphics/Rect;

    #@9
    move-result-object v1

    #@a
    .line 356
    .local v1, r:Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    #@d
    move-result v2

    #@e
    .line 357
    .local v2, w:I
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    #@11
    move-result v0

    #@12
    .line 359
    .local v0, h:I
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@14
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@16
    int-to-float v4, v2

    #@17
    int-to-float v5, v0

    #@18
    invoke-virtual {v3, v4, v5}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    #@1b
    .line 360
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@1d
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShaderFactory:Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    #@1f
    if-eqz v3, :cond_30

    #@21
    .line 361
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@23
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@25
    iget-object v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@27
    iget-object v4, v4, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShaderFactory:Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    #@29
    invoke-virtual {v4, v2, v0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;->resize(II)Landroid/graphics/Shader;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    #@30
    .line 364
    .end local v0           #h:I
    .end local v1           #r:Landroid/graphics/Rect;
    .end local v2           #w:I
    :cond_30
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@33
    .line 365
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter "canvas"

    #@0
    .prologue
    .line 214
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->getBounds()Landroid/graphics/Rect;

    #@3
    move-result-object v3

    #@4
    .line 215
    .local v3, r:Landroid/graphics/Rect;
    iget-object v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@6
    iget-object v1, v4, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@8
    .line 217
    .local v1, paint:Landroid/graphics/Paint;
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    #@b
    move-result v2

    #@c
    .line 218
    .local v2, prevAlpha:I
    iget-object v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@e
    iget v4, v4, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mAlpha:I

    #@10
    invoke-static {v2, v4}, Landroid/graphics/drawable/ShapeDrawable;->modulateAlpha(II)I

    #@13
    move-result v4

    #@14
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    #@17
    .line 220
    iget-object v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@19
    iget-object v4, v4, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@1b
    if-eqz v4, :cond_38

    #@1d
    .line 222
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@20
    move-result v0

    #@21
    .line 223
    .local v0, count:I
    iget v4, v3, Landroid/graphics/Rect;->left:I

    #@23
    int-to-float v4, v4

    #@24
    iget v5, v3, Landroid/graphics/Rect;->top:I

    #@26
    int-to-float v5, v5

    #@27
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    #@2a
    .line 224
    iget-object v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2c
    iget-object v4, v4, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@2e
    invoke-virtual {p0, v4, p1, v1}, Landroid/graphics/drawable/ShapeDrawable;->onDraw(Landroid/graphics/drawable/shapes/Shape;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    #@31
    .line 225
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@34
    .line 231
    .end local v0           #count:I
    :goto_34
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    #@37
    .line 232
    return-void

    #@38
    .line 227
    :cond_38
    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    #@3b
    goto :goto_34
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 236
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 369
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->getChangingConfigurations()I

    #@5
    move-result v1

    #@6
    iput v1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mChangingConfigurations:I

    #@8
    .line 370
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@a
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mIntrinsicHeight:I

    #@4
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 180
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mIntrinsicWidth:I

    #@4
    return v0
.end method

.method public getOpacity()I
    .registers 4

    #@0
    .prologue
    .line 261
    iget-object v2, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v2, v2, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@4
    if-nez v2, :cond_1e

    #@6
    .line 262
    iget-object v2, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@8
    iget-object v1, v2, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@a
    .line 263
    .local v1, p:Landroid/graphics/Paint;
    invoke-virtual {v1}, Landroid/graphics/Paint;->getXfermode()Landroid/graphics/Xfermode;

    #@d
    move-result-object v2

    #@e
    if-nez v2, :cond_1e

    #@10
    .line 264
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    #@13
    move-result v0

    #@14
    .line 265
    .local v0, alpha:I
    if-nez v0, :cond_18

    #@16
    .line 266
    const/4 v2, -0x2

    #@17
    .line 274
    .end local v0           #alpha:I
    .end local v1           #p:Landroid/graphics/Paint;
    :goto_17
    return v2

    #@18
    .line 268
    .restart local v0       #alpha:I
    .restart local v1       #p:Landroid/graphics/Paint;
    :cond_18
    const/16 v2, 0xff

    #@1a
    if-ne v0, v2, :cond_1e

    #@1c
    .line 269
    const/4 v2, -0x1

    #@1d
    goto :goto_17

    #@1e
    .line 274
    .end local v0           #alpha:I
    .end local v1           #p:Landroid/graphics/Paint;
    :cond_1e
    const/4 v2, -0x3

    #@1f
    goto :goto_17
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@4
    if-eqz v0, :cond_f

    #@6
    .line 191
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@8
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@a
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@d
    .line 192
    const/4 v0, 0x1

    #@e
    .line 194
    :goto_e
    return v0

    #@f
    :cond_f
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@12
    move-result v0

    #@13
    goto :goto_e
.end method

.method public getPaint()Landroid/graphics/Paint;
    .registers 2

    #@0
    .prologue
    .line 119
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@4
    return-object v0
.end method

.method public getShaderFactory()Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
    .registers 2

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShaderFactory:Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    #@4
    return-object v0
.end method

.method public getShape()Landroid/graphics/drawable/shapes/Shape;
    .registers 2

    #@0
    .prologue
    .line 86
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@4
    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 16
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x2

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    const/4 v7, 0x0

    #@5
    .line 318
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@8
    .line 320
    sget-object v6, Lcom/android/internal/R$styleable;->ShapeDrawable:[I

    #@a
    invoke-virtual {p1, p3, v6}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@d
    move-result-object v0

    #@e
    .line 322
    .local v0, a:Landroid/content/res/TypedArray;
    iget-object v6, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@10
    iget-object v6, v6, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@12
    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    #@15
    move-result v1

    #@16
    .line 323
    .local v1, color:I
    invoke-virtual {v0, v11, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    #@19
    move-result v1

    #@1a
    .line 324
    iget-object v6, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@1c
    iget-object v6, v6, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@1e
    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@21
    .line 326
    invoke-virtual {v0, v8, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@24
    move-result v2

    #@25
    .line 327
    .local v2, dither:Z
    iget-object v6, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@27
    iget-object v6, v6, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@29
    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setDither(Z)V

    #@2c
    .line 329
    invoke-virtual {v0, v10, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@2f
    move-result v6

    #@30
    float-to-int v6, v6

    #@31
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicWidth(I)V

    #@34
    .line 331
    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@37
    move-result v6

    #@38
    float-to-int v6, v6

    #@39
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/ShapeDrawable;->setIntrinsicHeight(I)V

    #@3c
    .line 334
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@3f
    .line 337
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@42
    move-result v4

    #@43
    .line 339
    .local v4, outerDepth:I
    :cond_43
    :goto_43
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@46
    move-result v5

    #@47
    .local v5, type:I
    if-eq v5, v9, :cond_80

    #@49
    if-ne v5, v11, :cond_51

    #@4b
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@4e
    move-result v6

    #@4f
    if-le v6, v4, :cond_80

    #@51
    .line 340
    :cond_51
    if-ne v5, v10, :cond_43

    #@53
    .line 344
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    .line 346
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p0, v3, p1, p2, p3}, Landroid/graphics/drawable/ShapeDrawable;->inflateTag(Ljava/lang/String;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Z

    #@5a
    move-result v6

    #@5b
    if-nez v6, :cond_43

    #@5d
    .line 347
    const-string v6, "drawable"

    #@5f
    new-instance v7, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v8, "Unknown element: "

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    const-string v8, " for ShapeDrawable "

    #@70
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v7

    #@74
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v7

    #@7c
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    goto :goto_43

    #@80
    .line 351
    .end local v3           #name:Ljava/lang/String;
    :cond_80
    return-void
.end method

.method protected inflateTag(Ljava/lang/String;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Z
    .registers 12
    .parameter "name"
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 296
    const-string/jumbo v3, "padding"

    #@5
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v3

    #@9
    if-eqz v3, :cond_2a

    #@b
    .line 297
    sget-object v3, Lcom/android/internal/R$styleable;->ShapeDrawablePadding:[I

    #@d
    invoke-virtual {p2, p4, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@10
    move-result-object v0

    #@11
    .line 299
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@14
    move-result v3

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@18
    move-result v4

    #@19
    const/4 v5, 0x2

    #@1a
    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@1d
    move-result v5

    #@1e
    const/4 v6, 0x3

    #@1f
    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    #@22
    move-result v2

    #@23
    invoke-virtual {p0, v3, v4, v5, v2}, Landroid/graphics/drawable/ShapeDrawable;->setPadding(IIII)V

    #@26
    .line 308
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@29
    .line 312
    .end local v0           #a:Landroid/content/res/TypedArray;
    :goto_29
    return v1

    #@2a
    :cond_2a
    move v1, v2

    #@2b
    goto :goto_29
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 375
    iget-boolean v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mMutated:Z

    #@3
    if-nez v1, :cond_3f

    #@5
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@8
    move-result-object v1

    #@9
    if-ne v1, p0, :cond_3f

    #@b
    .line 376
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@d
    iget-object v1, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@f
    if-eqz v1, :cond_40

    #@11
    .line 377
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@13
    new-instance v2, Landroid/graphics/Paint;

    #@15
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@17
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@19
    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    #@1c
    iput-object v2, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@1e
    .line 381
    :goto_1e
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@20
    iget-object v1, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@22
    if-eqz v1, :cond_4a

    #@24
    .line 382
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@26
    new-instance v2, Landroid/graphics/Rect;

    #@28
    iget-object v3, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2a
    iget-object v3, v3, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@2c
    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    #@2f
    iput-object v2, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@31
    .line 387
    :goto_31
    :try_start_31
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@33
    iget-object v2, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@35
    iget-object v2, v2, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@37
    invoke-virtual {v2}, Landroid/graphics/drawable/shapes/Shape;->clone()Landroid/graphics/drawable/shapes/Shape;

    #@3a
    move-result-object v2

    #@3b
    iput-object v2, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;
    :try_end_3d
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_31 .. :try_end_3d} :catch_54

    #@3d
    .line 391
    iput-boolean v4, p0, Landroid/graphics/drawable/ShapeDrawable;->mMutated:Z

    #@3f
    .line 393
    .end local p0
    :cond_3f
    :goto_3f
    return-object p0

    #@40
    .line 379
    .restart local p0
    :cond_40
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@42
    new-instance v2, Landroid/graphics/Paint;

    #@44
    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    #@47
    iput-object v2, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@49
    goto :goto_1e

    #@4a
    .line 384
    :cond_4a
    iget-object v1, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@4c
    new-instance v2, Landroid/graphics/Rect;

    #@4e
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    #@51
    iput-object v2, v1, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@53
    goto :goto_31

    #@54
    .line 388
    :catch_54
    move-exception v0

    #@55
    .line 389
    .local v0, e:Ljava/lang/CloneNotSupportedException;
    const/4 p0, 0x0

    #@56
    goto :goto_3f
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 2
    .parameter "bounds"

    #@0
    .prologue
    .line 285
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@3
    .line 286
    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;->updateShape()V

    #@6
    .line 287
    return-void
.end method

.method protected onDraw(Landroid/graphics/drawable/shapes/Shape;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .registers 4
    .parameter "shape"
    .parameter "canvas"
    .parameter "paint"

    #@0
    .prologue
    .line 209
    invoke-virtual {p1, p2, p3}, Landroid/graphics/drawable/shapes/Shape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    #@3
    .line 210
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mAlpha:I

    #@4
    .line 250
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@7
    .line 251
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    #@7
    .line 256
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@a
    .line 257
    return-void
.end method

.method public setDither(Z)V
    .registers 3
    .parameter "dither"

    #@0
    .prologue
    .line 279
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPaint:Landroid/graphics/Paint;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    #@7
    .line 280
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@a
    .line 281
    return-void
.end method

.method public setIntrinsicHeight(I)V
    .registers 3
    .parameter "height"

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mIntrinsicHeight:I

    #@4
    .line 175
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@7
    .line 176
    return-void
.end method

.method public setIntrinsicWidth(I)V
    .registers 3
    .parameter "width"

    #@0
    .prologue
    .line 164
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mIntrinsicWidth:I

    #@4
    .line 165
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@7
    .line 166
    return-void
.end method

.method public setPadding(IIII)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 131
    or-int v0, p1, p2

    #@2
    or-int/2addr v0, p3

    #@3
    or-int/2addr v0, p4

    #@4
    if-nez v0, :cond_f

    #@6
    .line 132
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@8
    const/4 v1, 0x0

    #@9
    iput-object v1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@b
    .line 139
    :goto_b
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@e
    .line 140
    return-void

    #@f
    .line 134
    :cond_f
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@11
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@13
    if-nez v0, :cond_1e

    #@15
    .line 135
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@17
    new-instance v1, Landroid/graphics/Rect;

    #@19
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@1c
    iput-object v1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@1e
    .line 137
    :cond_1e
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@20
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@22
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    #@25
    goto :goto_b
.end method

.method public setPadding(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "padding"

    #@0
    .prologue
    .line 147
    if-nez p1, :cond_b

    #@2
    .line 148
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@4
    const/4 v1, 0x0

    #@5
    iput-object v1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@7
    .line 155
    :goto_7
    invoke-virtual {p0}, Landroid/graphics/drawable/ShapeDrawable;->invalidateSelf()V

    #@a
    .line 156
    return-void

    #@b
    .line 150
    :cond_b
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@d
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@f
    if-nez v0, :cond_1a

    #@11
    .line 151
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@13
    new-instance v1, Landroid/graphics/Rect;

    #@15
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    #@18
    iput-object v1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@1a
    .line 153
    :cond_1a
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@1c
    iget-object v0, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mPadding:Landroid/graphics/Rect;

    #@1e
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@21
    goto :goto_7
.end method

.method public setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V
    .registers 3
    .parameter "fact"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iput-object p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShaderFactory:Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    #@4
    .line 105
    return-void
.end method

.method public setShape(Landroid/graphics/drawable/shapes/Shape;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Landroid/graphics/drawable/ShapeDrawable;->mShapeState:Landroid/graphics/drawable/ShapeDrawable$ShapeState;

    #@2
    iput-object p1, v0, Landroid/graphics/drawable/ShapeDrawable$ShapeState;->mShape:Landroid/graphics/drawable/shapes/Shape;

    #@4
    .line 94
    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;->updateShape()V

    #@7
    .line 95
    return-void
.end method
