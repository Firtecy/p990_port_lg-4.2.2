.class public Landroid/graphics/drawable/RotateDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "RotateDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/RotateDrawable$1;,
        Landroid/graphics/drawable/RotateDrawable$RotateState;
    }
.end annotation


# static fields
.field private static final MAX_LEVEL:F = 10000.0f


# instance fields
.field private mMutated:Z

.field private mState:Landroid/graphics/drawable/RotateDrawable$RotateState;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 59
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/RotateDrawable;-><init>(Landroid/graphics/drawable/RotateDrawable$RotateState;Landroid/content/res/Resources;)V

    #@4
    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/RotateDrawable$RotateState;Landroid/content/res/Resources;)V
    .registers 4
    .parameter "rotateState"
    .parameter "res"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 70
    new-instance v0, Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@5
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/RotateDrawable$RotateState;-><init>(Landroid/graphics/drawable/RotateDrawable$RotateState;Landroid/graphics/drawable/RotateDrawable;Landroid/content/res/Resources;)V

    #@8
    iput-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@a
    .line 71
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/RotateDrawable$RotateState;Landroid/content/res/Resources;Landroid/graphics/drawable/RotateDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/RotateDrawable;-><init>(Landroid/graphics/drawable/RotateDrawable$RotateState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 12
    .parameter "canvas"

    #@0
    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@3
    move-result v4

    #@4
    .line 76
    .local v4, saveCount:I
    iget-object v7, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@6
    iget-object v7, v7, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@b
    move-result-object v0

    #@c
    .line 78
    .local v0, bounds:Landroid/graphics/Rect;
    iget v7, v0, Landroid/graphics/Rect;->right:I

    #@e
    iget v8, v0, Landroid/graphics/Rect;->left:I

    #@10
    sub-int v6, v7, v8

    #@12
    .line 79
    .local v6, w:I
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    #@14
    iget v8, v0, Landroid/graphics/Rect;->top:I

    #@16
    sub-int v1, v7, v8

    #@18
    .line 81
    .local v1, h:I
    iget-object v5, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@1a
    .line 83
    .local v5, st:Landroid/graphics/drawable/RotateDrawable$RotateState;
    iget-boolean v7, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotXRel:Z

    #@1c
    if-eqz v7, :cond_42

    #@1e
    int-to-float v7, v6

    #@1f
    iget v8, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotX:F

    #@21
    mul-float v2, v7, v8

    #@23
    .line 84
    .local v2, px:F
    :goto_23
    iget-boolean v7, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotYRel:Z

    #@25
    if-eqz v7, :cond_45

    #@27
    int-to-float v7, v1

    #@28
    iget v8, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotY:F

    #@2a
    mul-float v3, v7, v8

    #@2c
    .line 86
    .local v3, py:F
    :goto_2c
    iget v7, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mCurrentDegrees:F

    #@2e
    iget v8, v0, Landroid/graphics/Rect;->left:I

    #@30
    int-to-float v8, v8

    #@31
    add-float/2addr v8, v2

    #@32
    iget v9, v0, Landroid/graphics/Rect;->top:I

    #@34
    int-to-float v9, v9

    #@35
    add-float/2addr v9, v3

    #@36
    invoke-virtual {p1, v7, v8, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    #@39
    .line 88
    iget-object v7, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@3b
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@3e
    .line 90
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    #@41
    .line 91
    return-void

    #@42
    .line 83
    .end local v2           #px:F
    .end local v3           #py:F
    :cond_42
    iget v2, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotX:F

    #@44
    goto :goto_23

    #@45
    .line 84
    .restart local v2       #px:F
    :cond_45
    iget v3, v5, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotY:F

    #@47
    goto :goto_2c
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 102
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/RotateDrawable$RotateState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    iget-object v1, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@b
    iget-object v1, v1, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@10
    move-result v1

    #@11
    or-int/2addr v0, v1

    #@12
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/RotateDrawable$RotateState;->canConstantState()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 195
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@a
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getChangingConfigurations()I

    #@d
    move-result v1

    #@e
    iput v1, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mChangingConfigurations:I

    #@10
    .line 196
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@12
    .line 198
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 97
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "padding"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 22
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 205
    sget-object v15, Lcom/android/internal/R$styleable;->RotateDrawable:[I

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, p3

    #@6
    invoke-virtual {v0, v1, v15}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v3

    #@a
    .line 208
    .local v3, a:Landroid/content/res/TypedArray;
    const/4 v15, 0x0

    #@b
    move-object/from16 v0, p0

    #@d
    move-object/from16 v1, p1

    #@f
    move-object/from16 v2, p2

    #@11
    invoke-super {v0, v1, v2, v3, v15}, Landroid/graphics/drawable/Drawable;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@14
    .line 211
    const/4 v15, 0x4

    #@15
    invoke-virtual {v3, v15}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@18
    move-result-object v13

    #@19
    .line 214
    .local v13, tv:Landroid/util/TypedValue;
    if-nez v13, :cond_89

    #@1b
    .line 215
    const/4 v8, 0x1

    #@1c
    .line 216
    .local v8, pivotXRel:Z
    const/high16 v7, 0x3f00

    #@1e
    .line 222
    .local v7, pivotX:F
    :goto_1e
    const/4 v15, 0x5

    #@1f
    invoke-virtual {v3, v15}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    #@22
    move-result-object v13

    #@23
    .line 225
    if-nez v13, :cond_a6

    #@25
    .line 226
    const/4 v10, 0x1

    #@26
    .line 227
    .local v10, pivotYRel:Z
    const/high16 v9, 0x3f00

    #@28
    .line 233
    .local v9, pivotY:F
    :goto_28
    const/4 v15, 0x2

    #@29
    const/16 v16, 0x0

    #@2b
    move/from16 v0, v16

    #@2d
    invoke-virtual {v3, v15, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@30
    move-result v5

    #@31
    .line 235
    .local v5, fromDegrees:F
    const/4 v15, 0x3

    #@32
    const/high16 v16, 0x43b4

    #@34
    move/from16 v0, v16

    #@36
    invoke-virtual {v3, v15, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    #@39
    move-result v12

    #@3a
    .line 238
    .local v12, toDegrees:F
    const/4 v15, 0x1

    #@3b
    const/16 v16, 0x0

    #@3d
    move/from16 v0, v16

    #@3f
    invoke-virtual {v3, v15, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@42
    move-result v11

    #@43
    .line 240
    .local v11, res:I
    const/4 v4, 0x0

    #@44
    .line 241
    .local v4, drawable:Landroid/graphics/drawable/Drawable;
    if-lez v11, :cond_4c

    #@46
    .line 242
    move-object/from16 v0, p1

    #@48
    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@4b
    move-result-object v4

    #@4c
    .line 245
    :cond_4c
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@4f
    .line 247
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@52
    move-result v6

    #@53
    .line 249
    .local v6, outerDepth:I
    :cond_53
    :goto_53
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@56
    move-result v14

    #@57
    .local v14, type:I
    const/4 v15, 0x1

    #@58
    if-eq v14, v15, :cond_c4

    #@5a
    const/4 v15, 0x3

    #@5b
    if-ne v14, v15, :cond_63

    #@5d
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@60
    move-result v15

    #@61
    if-le v15, v6, :cond_c4

    #@63
    .line 252
    :cond_63
    const/4 v15, 0x2

    #@64
    if-ne v14, v15, :cond_53

    #@66
    .line 256
    invoke-static/range {p1 .. p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@69
    move-result-object v4

    #@6a
    if-nez v4, :cond_53

    #@6c
    .line 257
    const-string v15, "drawable"

    #@6e
    new-instance v16, Ljava/lang/StringBuilder;

    #@70
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v17, "Bad element under <rotate>: "

    #@75
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v16

    #@79
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@7c
    move-result-object v17

    #@7d
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v16

    #@81
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v16

    #@85
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_53

    #@89
    .line 218
    .end local v4           #drawable:Landroid/graphics/drawable/Drawable;
    .end local v5           #fromDegrees:F
    .end local v6           #outerDepth:I
    .end local v7           #pivotX:F
    .end local v8           #pivotXRel:Z
    .end local v9           #pivotY:F
    .end local v10           #pivotYRel:Z
    .end local v11           #res:I
    .end local v12           #toDegrees:F
    .end local v14           #type:I
    :cond_89
    iget v15, v13, Landroid/util/TypedValue;->type:I

    #@8b
    const/16 v16, 0x6

    #@8d
    move/from16 v0, v16

    #@8f
    if-ne v15, v0, :cond_9f

    #@91
    const/4 v8, 0x1

    #@92
    .line 219
    .restart local v8       #pivotXRel:Z
    :goto_92
    if-eqz v8, :cond_a1

    #@94
    const/high16 v15, 0x3f80

    #@96
    const/high16 v16, 0x3f80

    #@98
    move/from16 v0, v16

    #@9a
    invoke-virtual {v13, v15, v0}, Landroid/util/TypedValue;->getFraction(FF)F

    #@9d
    move-result v7

    #@9e
    .restart local v7       #pivotX:F
    :goto_9e
    goto :goto_1e

    #@9f
    .line 218
    .end local v7           #pivotX:F
    .end local v8           #pivotXRel:Z
    :cond_9f
    const/4 v8, 0x0

    #@a0
    goto :goto_92

    #@a1
    .line 219
    .restart local v8       #pivotXRel:Z
    :cond_a1
    invoke-virtual {v13}, Landroid/util/TypedValue;->getFloat()F

    #@a4
    move-result v7

    #@a5
    goto :goto_9e

    #@a6
    .line 229
    .restart local v7       #pivotX:F
    :cond_a6
    iget v15, v13, Landroid/util/TypedValue;->type:I

    #@a8
    const/16 v16, 0x6

    #@aa
    move/from16 v0, v16

    #@ac
    if-ne v15, v0, :cond_bd

    #@ae
    const/4 v10, 0x1

    #@af
    .line 230
    .restart local v10       #pivotYRel:Z
    :goto_af
    if-eqz v10, :cond_bf

    #@b1
    const/high16 v15, 0x3f80

    #@b3
    const/high16 v16, 0x3f80

    #@b5
    move/from16 v0, v16

    #@b7
    invoke-virtual {v13, v15, v0}, Landroid/util/TypedValue;->getFraction(FF)F

    #@ba
    move-result v9

    #@bb
    .restart local v9       #pivotY:F
    :goto_bb
    goto/16 :goto_28

    #@bd
    .line 229
    .end local v9           #pivotY:F
    .end local v10           #pivotYRel:Z
    :cond_bd
    const/4 v10, 0x0

    #@be
    goto :goto_af

    #@bf
    .line 230
    .restart local v10       #pivotYRel:Z
    :cond_bf
    invoke-virtual {v13}, Landroid/util/TypedValue;->getFloat()F

    #@c2
    move-result v9

    #@c3
    goto :goto_bb

    #@c4
    .line 262
    .restart local v4       #drawable:Landroid/graphics/drawable/Drawable;
    .restart local v5       #fromDegrees:F
    .restart local v6       #outerDepth:I
    .restart local v9       #pivotY:F
    .restart local v11       #res:I
    .restart local v12       #toDegrees:F
    .restart local v14       #type:I
    :cond_c4
    if-nez v4, :cond_cd

    #@c6
    .line 263
    const-string v15, "drawable"

    #@c8
    const-string v16, "No drawable specified for <rotate>"

    #@ca
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@cd
    .line 266
    :cond_cd
    move-object/from16 v0, p0

    #@cf
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@d1
    iput-object v4, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@d3
    .line 267
    move-object/from16 v0, p0

    #@d5
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@d7
    iput-boolean v8, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotXRel:Z

    #@d9
    .line 268
    move-object/from16 v0, p0

    #@db
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@dd
    iput v7, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotX:F

    #@df
    .line 269
    move-object/from16 v0, p0

    #@e1
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@e3
    iput-boolean v10, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotYRel:Z

    #@e5
    .line 270
    move-object/from16 v0, p0

    #@e7
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@e9
    iput v9, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mPivotY:F

    #@eb
    .line 271
    move-object/from16 v0, p0

    #@ed
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@ef
    move-object/from16 v0, p0

    #@f1
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@f3
    move-object/from16 v16, v0

    #@f5
    move-object/from16 v0, v16

    #@f7
    iput v5, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mCurrentDegrees:F

    #@f9
    iput v5, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mFromDegrees:F

    #@fb
    .line 272
    move-object/from16 v0, p0

    #@fd
    iget-object v15, v0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@ff
    iput v12, v15, Landroid/graphics/drawable/RotateDrawable$RotateState;->mToDegrees:F

    #@101
    .line 274
    if-eqz v4, :cond_108

    #@103
    .line 275
    move-object/from16 v0, p0

    #@105
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@108
    .line 277
    :cond_108
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 120
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 121
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 122
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@9
    .line 124
    :cond_9
    return-void
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 281
    iget-boolean v0, p0, Landroid/graphics/drawable/RotateDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_14

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_14

    #@a
    .line 282
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@c
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@e
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@11
    .line 283
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/graphics/drawable/RotateDrawable;->mMutated:Z

    #@14
    .line 285
    :cond_14
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 7
    .parameter "bounds"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@6
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@8
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@a
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@c
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@f
    .line 180
    return-void
.end method

.method protected onLevelChange(I)Z
    .registers 7
    .parameter "level"

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@7
    .line 166
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getBounds()Landroid/graphics/Rect;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/RotateDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@e
    .line 168
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@10
    iget-object v1, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@12
    iget v1, v1, Landroid/graphics/drawable/RotateDrawable$RotateState;->mFromDegrees:F

    #@14
    iget-object v2, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@16
    iget v2, v2, Landroid/graphics/drawable/RotateDrawable$RotateState;->mToDegrees:F

    #@18
    iget-object v3, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@1a
    iget v3, v3, Landroid/graphics/drawable/RotateDrawable$RotateState;->mFromDegrees:F

    #@1c
    sub-float/2addr v2, v3

    #@1d
    int-to-float v3, p1

    #@1e
    const v4, 0x461c4000

    #@21
    div-float/2addr v3, v4

    #@22
    mul-float/2addr v2, v3

    #@23
    add-float/2addr v1, v2

    #@24
    iput v1, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mCurrentDegrees:F

    #@26
    .line 172
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->invalidateSelf()V

    #@29
    .line 173
    const/4 v0, 0x1

    #@2a
    return v0
.end method

.method protected onStateChange([I)Z
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 158
    iget-object v1, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v1, v1, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@7
    move-result v0

    #@8
    .line 159
    .local v0, changed:Z
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getBounds()Landroid/graphics/Rect;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/RotateDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@f
    .line 160
    return v0
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 6
    .parameter "who"
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 128
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 129
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    #@9
    .line 131
    :cond_9
    return-void
.end method

.method public setAlpha(I)V
    .registers 3
    .parameter "alpha"

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@7
    .line 109
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@7
    .line 113
    return-void
.end method

.method public setVisible(ZZ)Z
    .registers 4
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Landroid/graphics/drawable/RotateDrawable;->mState:Landroid/graphics/drawable/RotateDrawable$RotateState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/RotateDrawable$RotateState;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@7
    .line 148
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 134
    invoke-virtual {p0}, Landroid/graphics/drawable/RotateDrawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@3
    move-result-object v0

    #@4
    .line 135
    .local v0, callback:Landroid/graphics/drawable/Drawable$Callback;
    if-eqz v0, :cond_9

    #@6
    .line 136
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    #@9
    .line 138
    :cond_9
    return-void
.end method
