.class final Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;
.super Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;
.source "MipmapDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/MipmapDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MipmapContainerState"
.end annotation


# instance fields
.field private mMipmapHeights:[I


# direct methods
.method constructor <init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/graphics/drawable/MipmapDrawable;Landroid/content/res/Resources;)V
    .registers 5
    .parameter "orig"
    .parameter "owner"
    .parameter "res"

    #@0
    .prologue
    .line 193
    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;-><init>(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;Landroid/graphics/drawable/DrawableContainer;Landroid/content/res/Resources;)V

    #@3
    .line 195
    if-eqz p1, :cond_e

    #@5
    .line 196
    iget-object v0, p1, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@7
    iput-object v0, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@9
    .line 202
    :goto_9
    const/4 v0, 0x1

    #@a
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->setConstantSize(Z)V

    #@d
    .line 203
    return-void

    #@e
    .line 198
    :cond_e
    invoke-virtual {p0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->getChildren()[Landroid/graphics/drawable/Drawable;

    #@11
    move-result-object v0

    #@12
    array-length v0, v0

    #@13
    new-array v0, v0, [I

    #@15
    iput-object v0, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@17
    goto :goto_9
.end method

.method static synthetic access$000(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;)[I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 189
    iget-object v0, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@2
    return-object v0
.end method

.method static synthetic access$002(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;[I)[I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 189
    iput-object p1, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@2
    return-object p1
.end method


# virtual methods
.method public addDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 8
    .parameter "drawable"

    #@0
    .prologue
    .line 243
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->addChild(Landroid/graphics/drawable/Drawable;)I

    #@3
    move-result v1

    #@4
    .line 246
    .local v1, pos:I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@7
    move-result v0

    #@8
    .line 248
    .local v0, drawableHeight:I
    :goto_8
    if-lez v1, :cond_2d

    #@a
    .line 249
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@c
    add-int/lit8 v5, v1, -0x1

    #@e
    aget-object v2, v4, v5

    #@10
    .line 250
    .local v2, previousDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@13
    move-result v3

    #@14
    .line 252
    .local v3, previousIntrinsicHeight:I
    if-ge v0, v3, :cond_2d

    #@16
    .line 253
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@18
    aput-object v2, v4, v1

    #@1a
    .line 254
    iget-object v4, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@1c
    aput v3, v4, v1

    #@1e
    .line 256
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@20
    add-int/lit8 v5, v1, -0x1

    #@22
    aput-object p1, v4, v5

    #@24
    .line 257
    iget-object v4, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@26
    add-int/lit8 v5, v1, -0x1

    #@28
    aput v0, v4, v5

    #@2a
    .line 258
    add-int/lit8 v1, v1, -0x1

    #@2c
    .line 262
    goto :goto_8

    #@2d
    .line 263
    .end local v2           #previousDrawable:Landroid/graphics/drawable/Drawable;
    .end local v3           #previousIntrinsicHeight:I
    :cond_2d
    return-void
.end method

.method protected computeConstantSize()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 271
    invoke-virtual {p0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->getChildCount()I

    #@4
    move-result v0

    #@5
    .line 272
    .local v0, N:I
    if-lez v0, :cond_2d

    #@7
    .line 273
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@9
    aget-object v2, v3, v4

    #@b
    .line 274
    .local v2, smallestDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@e
    move-result v3

    #@f
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@11
    .line 275
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@14
    move-result v3

    #@15
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@17
    .line 277
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@19
    add-int/lit8 v4, v0, -0x1

    #@1b
    aget-object v1, v3, v4

    #@1d
    .line 278
    .local v1, largestDrawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@20
    move-result v3

    #@21
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@23
    .line 279
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@26
    move-result v3

    #@27
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@29
    .line 284
    .end local v1           #largestDrawable:Landroid/graphics/drawable/Drawable;
    .end local v2           #smallestDrawable:Landroid/graphics/drawable/Drawable;
    :goto_29
    const/4 v3, 0x1

    #@2a
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mComputedConstantSize:Z

    #@2c
    .line 285
    return-void

    #@2d
    .line 281
    :cond_2d
    const/4 v3, -0x1

    #@2e
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantHeight:I

    #@30
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantWidth:I

    #@32
    .line 282
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumHeight:I

    #@34
    iput v4, p0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mConstantMinimumWidth:I

    #@36
    goto :goto_29
.end method

.method public growArray(II)V
    .registers 6
    .parameter "oldSize"
    .parameter "newSize"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 299
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->growArray(II)V

    #@4
    .line 300
    new-array v0, p2, [I

    #@6
    .line 301
    .local v0, newInts:[I
    iget-object v1, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@8
    invoke-static {v1, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@b
    .line 302
    iput-object v0, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@d
    .line 303
    return-void
.end method

.method public indexForBounds(Landroid/graphics/Rect;)I
    .registers 6
    .parameter "bounds"

    #@0
    .prologue
    .line 217
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@3
    move-result v1

    #@4
    .line 218
    .local v1, boundsHeight:I
    invoke-virtual {p0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->getChildCount()I

    #@7
    move-result v0

    #@8
    .line 219
    .local v0, N:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v0, :cond_15

    #@b
    .line 220
    iget-object v3, p0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->mMipmapHeights:[I

    #@d
    aget v3, v3, v2

    #@f
    if-gt v1, v3, :cond_12

    #@11
    .line 230
    .end local v2           #i:I
    :goto_11
    return v2

    #@12
    .line 219
    .restart local v2       #i:I
    :cond_12
    add-int/lit8 v2, v2, 0x1

    #@14
    goto :goto_9

    #@15
    .line 226
    :cond_15
    if-lez v0, :cond_1a

    #@17
    .line 227
    add-int/lit8 v2, v0, -0x1

    #@19
    goto :goto_11

    #@1a
    .line 230
    :cond_1a
    const/4 v2, -0x1

    #@1b
    goto :goto_11
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 289
    new-instance v0, Landroid/graphics/drawable/MipmapDrawable;

    #@3
    invoke-direct {v0, p0, v1, v1}, Landroid/graphics/drawable/MipmapDrawable;-><init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;Landroid/graphics/drawable/MipmapDrawable$1;)V

    #@6
    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter "res"

    #@0
    .prologue
    .line 294
    new-instance v0, Landroid/graphics/drawable/MipmapDrawable;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, p0, p1, v1}, Landroid/graphics/drawable/MipmapDrawable;-><init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;Landroid/graphics/drawable/MipmapDrawable$1;)V

    #@6
    return-object v0
.end method
