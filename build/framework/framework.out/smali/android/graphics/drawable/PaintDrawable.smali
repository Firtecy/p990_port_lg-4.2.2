.class public Landroid/graphics/drawable/PaintDrawable;
.super Landroid/graphics/drawable/ShapeDrawable;
.source "PaintDrawable.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    #@3
    .line 33
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 35
    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    #@3
    .line 36
    invoke-virtual {p0}, Landroid/graphics/drawable/PaintDrawable;->getPaint()Landroid/graphics/Paint;

    #@6
    move-result-object v0

    #@7
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    #@a
    .line 37
    return-void
.end method


# virtual methods
.method protected inflateTag(Ljava/lang/String;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Z
    .registers 14
    .parameter "name"
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"

    #@0
    .prologue
    .line 75
    const-string v6, "corners"

    #@2
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v6

    #@6
    if-eqz v6, :cond_60

    #@8
    .line 76
    sget-object v6, Lcom/android/internal/R$styleable;->DrawableCorners:[I

    #@a
    invoke-virtual {p2, p4, v6}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@d
    move-result-object v0

    #@e
    .line 78
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v6, 0x0

    #@f
    const/4 v7, 0x0

    #@10
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@13
    move-result v3

    #@14
    .line 80
    .local v3, radius:I
    int-to-float v6, v3

    #@15
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadius(F)V

    #@18
    .line 84
    const/4 v6, 0x1

    #@19
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1c
    move-result v4

    #@1d
    .line 86
    .local v4, topLeftRadius:I
    const/4 v6, 0x2

    #@1e
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@21
    move-result v5

    #@22
    .line 88
    .local v5, topRightRadius:I
    const/4 v6, 0x3

    #@23
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@26
    move-result v1

    #@27
    .line 90
    .local v1, bottomLeftRadius:I
    const/4 v6, 0x4

    #@28
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@2b
    move-result v2

    #@2c
    .line 93
    .local v2, bottomRightRadius:I
    if-ne v4, v3, :cond_34

    #@2e
    if-ne v5, v3, :cond_34

    #@30
    if-ne v1, v3, :cond_34

    #@32
    if-eq v2, v3, :cond_5b

    #@34
    .line 95
    :cond_34
    const/16 v6, 0x8

    #@36
    new-array v6, v6, [F

    #@38
    const/4 v7, 0x0

    #@39
    int-to-float v8, v4

    #@3a
    aput v8, v6, v7

    #@3c
    const/4 v7, 0x1

    #@3d
    int-to-float v8, v4

    #@3e
    aput v8, v6, v7

    #@40
    const/4 v7, 0x2

    #@41
    int-to-float v8, v5

    #@42
    aput v8, v6, v7

    #@44
    const/4 v7, 0x3

    #@45
    int-to-float v8, v5

    #@46
    aput v8, v6, v7

    #@48
    const/4 v7, 0x4

    #@49
    int-to-float v8, v1

    #@4a
    aput v8, v6, v7

    #@4c
    const/4 v7, 0x5

    #@4d
    int-to-float v8, v1

    #@4e
    aput v8, v6, v7

    #@50
    const/4 v7, 0x6

    #@51
    int-to-float v8, v2

    #@52
    aput v8, v6, v7

    #@54
    const/4 v7, 0x7

    #@55
    int-to-float v8, v2

    #@56
    aput v8, v6, v7

    #@58
    invoke-virtual {p0, v6}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadii([F)V

    #@5b
    .line 102
    :cond_5b
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@5e
    .line 103
    const/4 v6, 0x1

    #@5f
    .line 105
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #bottomLeftRadius:I
    .end local v2           #bottomRightRadius:I
    .end local v3           #radius:I
    .end local v4           #topLeftRadius:I
    .end local v5           #topRightRadius:I
    :goto_5f
    return v6

    #@60
    :cond_60
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/ShapeDrawable;->inflateTag(Ljava/lang/String;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Z

    #@63
    move-result v6

    #@64
    goto :goto_5f
.end method

.method public setCornerRadii([F)V
    .registers 4
    .parameter "radii"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 62
    if-nez p1, :cond_10

    #@3
    .line 63
    invoke-virtual {p0}, Landroid/graphics/drawable/PaintDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    #@6
    move-result-object v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 64
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/PaintDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    #@c
    .line 69
    :cond_c
    :goto_c
    invoke-virtual {p0}, Landroid/graphics/drawable/PaintDrawable;->invalidateSelf()V

    #@f
    .line 70
    return-void

    #@10
    .line 67
    :cond_10
    new-instance v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    #@12
    invoke-direct {v0, p1, v1, v1}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    #@15
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/PaintDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    #@18
    goto :goto_c
.end method

.method public setCornerRadius(F)V
    .registers 6
    .parameter "radius"

    #@0
    .prologue
    const/16 v3, 0x8

    #@2
    .line 45
    const/4 v1, 0x0

    #@3
    .line 46
    .local v1, radii:[F
    const/4 v2, 0x0

    #@4
    cmpl-float v2, p1, v2

    #@6
    if-lez v2, :cond_12

    #@8
    .line 47
    new-array v1, v3, [F

    #@a
    .line 48
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v3, :cond_12

    #@d
    .line 49
    aput p1, v1, v0

    #@f
    .line 48
    add-int/lit8 v0, v0, 0x1

    #@11
    goto :goto_b

    #@12
    .line 52
    .end local v0           #i:I
    :cond_12
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadii([F)V

    #@15
    .line 53
    return-void
.end method
