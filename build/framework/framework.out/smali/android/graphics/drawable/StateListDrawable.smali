.class public Landroid/graphics/drawable/StateListDrawable;
.super Landroid/graphics/drawable/DrawableContainer;
.source "StateListDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/StateListDrawable$1;,
        Landroid/graphics/drawable/StateListDrawable$StateListState;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEFAULT_DITHER:Z = true

.field private static final TAG:Ljava/lang/String; = "StateListDrawable"


# instance fields
.field private mMutated:Z

.field private final mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 75
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/StateListDrawable;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;)V

    #@4
    .line 76
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;)V
    .registers 5
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 323
    invoke-direct {p0}, Landroid/graphics/drawable/DrawableContainer;-><init>()V

    #@3
    .line 324
    new-instance v0, Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@5
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/StateListDrawable$StateListState;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;)V

    #@8
    .line 325
    .local v0, as:Landroid/graphics/drawable/StateListDrawable$StateListState;
    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@a
    .line 326
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/StateListDrawable;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    #@d
    .line 327
    invoke-virtual {p0}, Landroid/graphics/drawable/StateListDrawable;->getState()[I

    #@10
    move-result-object v1

    #@11
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    #@14
    .line 328
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;Landroid/graphics/drawable/StateListDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/StateListDrawable;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method


# virtual methods
.method public addState([ILandroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "stateSet"
    .parameter "drawable"

    #@0
    .prologue
    .line 86
    if-eqz p2, :cond_e

    #@2
    .line 87
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@4
    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/StateListDrawable$StateListState;->addStateSet([ILandroid/graphics/drawable/Drawable;)I

    #@7
    .line 89
    invoke-virtual {p0}, Landroid/graphics/drawable/StateListDrawable;->getState()[I

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    #@e
    .line 91
    :cond_e
    return-void
.end method

.method public getStateCount()I
    .registers 2

    #@0
    .prologue
    .line 205
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable$StateListState;->getChildCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getStateDrawable(I)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable$StateListState;->getChildren()[Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    aget-object v0, v0, p1

    #@8
    return-object v0
.end method

.method public getStateDrawableIndex([I)I
    .registers 3
    .parameter "stateSet"

    #@0
    .prologue
    .line 244
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    invoke-static {v0, p1}, Landroid/graphics/drawable/StateListDrawable$StateListState;->access$000(Landroid/graphics/drawable/StateListDrawable$StateListState;[I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getStateListState()Landroid/graphics/drawable/StateListDrawable$StateListState;
    .registers 2

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    return-object v0
.end method

.method public getStateSet(I)[I
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    iget-object v0, v0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    #@4
    aget-object v0, v0, p1

    #@6
    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 22
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 117
    sget-object v15, Lcom/android/internal/R$styleable;->StateListDrawable:[I

    #@2
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, p3

    #@6
    invoke-virtual {v0, v1, v15}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v3

    #@a
    .line 120
    .local v3, a:Landroid/content/res/TypedArray;
    const/4 v15, 0x1

    #@b
    move-object/from16 v0, p0

    #@d
    move-object/from16 v1, p1

    #@f
    move-object/from16 v2, p2

    #@11
    invoke-super {v0, v1, v2, v3, v15}, Landroid/graphics/drawable/DrawableContainer;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@14
    .line 123
    move-object/from16 v0, p0

    #@16
    iget-object v15, v0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@18
    const/16 v16, 0x2

    #@1a
    const/16 v17, 0x0

    #@1c
    move/from16 v0, v16

    #@1e
    move/from16 v1, v17

    #@20
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@23
    move-result v16

    #@24
    invoke-virtual/range {v15 .. v16}, Landroid/graphics/drawable/StateListDrawable$StateListState;->setVariablePadding(Z)V

    #@27
    .line 125
    move-object/from16 v0, p0

    #@29
    iget-object v15, v0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2b
    const/16 v16, 0x3

    #@2d
    const/16 v17, 0x0

    #@2f
    move/from16 v0, v16

    #@31
    move/from16 v1, v17

    #@33
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@36
    move-result v16

    #@37
    invoke-virtual/range {v15 .. v16}, Landroid/graphics/drawable/StateListDrawable$StateListState;->setConstantSize(Z)V

    #@3a
    .line 127
    move-object/from16 v0, p0

    #@3c
    iget-object v15, v0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@3e
    const/16 v16, 0x4

    #@40
    const/16 v17, 0x0

    #@42
    move/from16 v0, v16

    #@44
    move/from16 v1, v17

    #@46
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@49
    move-result v16

    #@4a
    invoke-virtual/range {v15 .. v16}, Landroid/graphics/drawable/StateListDrawable$StateListState;->setEnterFadeDuration(I)V

    #@4d
    .line 129
    move-object/from16 v0, p0

    #@4f
    iget-object v15, v0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@51
    const/16 v16, 0x5

    #@53
    const/16 v17, 0x0

    #@55
    move/from16 v0, v16

    #@57
    move/from16 v1, v17

    #@59
    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    #@5c
    move-result v16

    #@5d
    invoke-virtual/range {v15 .. v16}, Landroid/graphics/drawable/StateListDrawable$StateListState;->setExitFadeDuration(I)V

    #@60
    .line 132
    const/4 v15, 0x0

    #@61
    const/16 v16, 0x1

    #@63
    move/from16 v0, v16

    #@65
    invoke-virtual {v3, v15, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@68
    move-result v15

    #@69
    move-object/from16 v0, p0

    #@6b
    invoke-virtual {v0, v15}, Landroid/graphics/drawable/StateListDrawable;->setDither(Z)V

    #@6e
    .line 135
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    #@71
    .line 139
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@74
    move-result v15

    #@75
    add-int/lit8 v8, v15, 0x1

    #@77
    .line 142
    .local v8, innerDepth:I
    :cond_77
    :goto_77
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@7a
    move-result v14

    #@7b
    .local v14, type:I
    const/4 v15, 0x1

    #@7c
    if-eq v14, v15, :cond_114

    #@7e
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@81
    move-result v4

    #@82
    .local v4, depth:I
    if-ge v4, v8, :cond_87

    #@84
    const/4 v15, 0x3

    #@85
    if-eq v14, v15, :cond_114

    #@87
    .line 144
    :cond_87
    const/4 v15, 0x2

    #@88
    if-ne v14, v15, :cond_77

    #@8a
    .line 148
    if-gt v4, v8, :cond_77

    #@8c
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@8f
    move-result-object v15

    #@90
    const-string/jumbo v16, "item"

    #@93
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@96
    move-result v15

    #@97
    if-eqz v15, :cond_77

    #@99
    .line 152
    const/4 v6, 0x0

    #@9a
    .line 155
    .local v6, drawableRes:I
    const/4 v9, 0x0

    #@9b
    .line 156
    .local v9, j:I
    invoke-interface/range {p3 .. p3}, Landroid/util/AttributeSet;->getAttributeCount()I

    #@9e
    move-result v11

    #@9f
    .line 157
    .local v11, numAttrs:I
    new-array v13, v11, [I

    #@a1
    .line 158
    .local v13, states:[I
    const/4 v7, 0x0

    #@a2
    .local v7, i:I
    move v10, v9

    #@a3
    .end local v9           #j:I
    .local v10, j:I
    :goto_a3
    if-ge v7, v11, :cond_ad

    #@a5
    .line 159
    move-object/from16 v0, p3

    #@a7
    invoke-interface {v0, v7}, Landroid/util/AttributeSet;->getAttributeNameResource(I)I

    #@aa
    move-result v12

    #@ab
    .line 160
    .local v12, stateResId:I
    if-nez v12, :cond_c1

    #@ad
    .line 169
    .end local v12           #stateResId:I
    :cond_ad
    invoke-static {v13, v10}, Landroid/util/StateSet;->trimStateSet([II)[I

    #@b0
    move-result-object v13

    #@b1
    .line 172
    if-eqz v6, :cond_e2

    #@b3
    .line 173
    move-object/from16 v0, p1

    #@b5
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@b8
    move-result-object v5

    #@b9
    .line 186
    .local v5, dr:Landroid/graphics/drawable/Drawable;
    :goto_b9
    move-object/from16 v0, p0

    #@bb
    iget-object v15, v0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@bd
    invoke-virtual {v15, v13, v5}, Landroid/graphics/drawable/StateListDrawable$StateListState;->addStateSet([ILandroid/graphics/drawable/Drawable;)I

    #@c0
    goto :goto_77

    #@c1
    .line 161
    .end local v5           #dr:Landroid/graphics/drawable/Drawable;
    .restart local v12       #stateResId:I
    :cond_c1
    const v15, 0x1010199

    #@c4
    if-ne v12, v15, :cond_d2

    #@c6
    .line 162
    const/4 v15, 0x0

    #@c7
    move-object/from16 v0, p3

    #@c9
    invoke-interface {v0, v7, v15}, Landroid/util/AttributeSet;->getAttributeResourceValue(II)I

    #@cc
    move-result v6

    #@cd
    move v9, v10

    #@ce
    .line 158
    .end local v10           #j:I
    .end local v12           #stateResId:I
    .restart local v9       #j:I
    :goto_ce
    add-int/lit8 v7, v7, 0x1

    #@d0
    move v10, v9

    #@d1
    .end local v9           #j:I
    .restart local v10       #j:I
    goto :goto_a3

    #@d2
    .line 164
    .restart local v12       #stateResId:I
    :cond_d2
    add-int/lit8 v9, v10, 0x1

    #@d4
    .end local v10           #j:I
    .restart local v9       #j:I
    const/4 v15, 0x0

    #@d5
    move-object/from16 v0, p3

    #@d7
    invoke-interface {v0, v7, v15}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    #@da
    move-result v15

    #@db
    if-eqz v15, :cond_e0

    #@dd
    .end local v12           #stateResId:I
    :goto_dd
    aput v12, v13, v10

    #@df
    goto :goto_ce

    #@e0
    .restart local v12       #stateResId:I
    :cond_e0
    neg-int v12, v12

    #@e1
    goto :goto_dd

    #@e2
    .line 175
    .end local v9           #j:I
    .end local v12           #stateResId:I
    .restart local v10       #j:I
    :cond_e2
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@e5
    move-result v14

    #@e6
    const/4 v15, 0x4

    #@e7
    if-eq v14, v15, :cond_e2

    #@e9
    .line 177
    const/4 v15, 0x2

    #@ea
    if-eq v14, v15, :cond_10f

    #@ec
    .line 178
    new-instance v15, Lorg/xmlpull/v1/XmlPullParserException;

    #@ee
    new-instance v16, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@f6
    move-result-object v17

    #@f7
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v16

    #@fb
    const-string v17, ": <item> tag requires a \'drawable\' attribute or "

    #@fd
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v16

    #@101
    const-string v17, "child tag defining a drawable"

    #@103
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v16

    #@107
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v16

    #@10b
    invoke-direct/range {v15 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@10e
    throw v15

    #@10f
    .line 183
    :cond_10f
    invoke-static/range {p1 .. p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@112
    move-result-object v5

    #@113
    .restart local v5       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_b9

    #@114
    .line 189
    .end local v4           #depth:I
    .end local v5           #dr:Landroid/graphics/drawable/Drawable;
    .end local v6           #drawableRes:I
    .end local v7           #i:I
    .end local v10           #j:I
    .end local v11           #numAttrs:I
    .end local v13           #states:[I
    :cond_114
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/drawable/StateListDrawable;->getState()[I

    #@117
    move-result-object v15

    #@118
    move-object/from16 v0, p0

    #@11a
    invoke-virtual {v0, v15}, Landroid/graphics/drawable/StateListDrawable;->onStateChange([I)Z

    #@11d
    .line 190
    return-void
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 95
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 7

    #@0
    .prologue
    .line 249
    iget-boolean v4, p0, Landroid/graphics/drawable/StateListDrawable;->mMutated:Z

    #@2
    if-nez v4, :cond_2e

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/DrawableContainer;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v4

    #@8
    if-ne v4, p0, :cond_2e

    #@a
    .line 250
    iget-object v4, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@c
    iget-object v3, v4, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    #@e
    .line 251
    .local v3, sets:[[I
    array-length v0, v3

    #@f
    .line 252
    .local v0, count:I
    iget-object v4, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@11
    new-array v5, v0, [[I

    #@13
    iput-object v5, v4, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    #@15
    .line 253
    const/4 v1, 0x0

    #@16
    .local v1, i:I
    :goto_16
    if-ge v1, v0, :cond_2b

    #@18
    .line 254
    aget-object v2, v3, v1

    #@1a
    .line 255
    .local v2, set:[I
    if-eqz v2, :cond_28

    #@1c
    .line 256
    iget-object v4, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@1e
    iget-object v5, v4, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    #@20
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    #@23
    move-result-object v4

    #@24
    check-cast v4, [I

    #@26
    aput-object v4, v5, v1

    #@28
    .line 253
    :cond_28
    add-int/lit8 v1, v1, 0x1

    #@2a
    goto :goto_16

    #@2b
    .line 259
    .end local v2           #set:[I
    :cond_2b
    const/4 v4, 0x1

    #@2c
    iput-boolean v4, p0, Landroid/graphics/drawable/StateListDrawable;->mMutated:Z

    #@2e
    .line 261
    .end local v0           #count:I
    .end local v1           #i:I
    .end local v3           #sets:[[I
    :cond_2e
    return-object p0
.end method

.method protected onStateChange([I)Z
    .registers 5
    .parameter "stateSet"

    #@0
    .prologue
    .line 100
    iget-object v1, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@2
    invoke-static {v1, p1}, Landroid/graphics/drawable/StateListDrawable$StateListState;->access$000(Landroid/graphics/drawable/StateListDrawable$StateListState;[I)I

    #@5
    move-result v0

    #@6
    .line 103
    .local v0, idx:I
    if-gez v0, :cond_10

    #@8
    .line 104
    iget-object v1, p0, Landroid/graphics/drawable/StateListDrawable;->mStateListState:Landroid/graphics/drawable/StateListDrawable$StateListState;

    #@a
    sget-object v2, Landroid/util/StateSet;->WILD_CARD:[I

    #@c
    invoke-static {v1, v2}, Landroid/graphics/drawable/StateListDrawable$StateListState;->access$000(Landroid/graphics/drawable/StateListDrawable$StateListState;[I)I

    #@f
    move-result v0

    #@10
    .line 106
    :cond_10
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/StateListDrawable;->selectDrawable(I)Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_18

    #@16
    .line 107
    const/4 v1, 0x1

    #@17
    .line 109
    :goto_17
    return v1

    #@18
    :cond_18
    invoke-super {p0, p1}, Landroid/graphics/drawable/DrawableContainer;->onStateChange([I)Z

    #@1b
    move-result v1

    #@1c
    goto :goto_17
.end method

.method public setLayoutDirection(I)V
    .registers 5
    .parameter "layoutDirection"

    #@0
    .prologue
    .line 267
    invoke-virtual {p0}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    #@3
    move-result v1

    #@4
    .line 268
    .local v1, numStates:I
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    if-ge v0, v1, :cond_11

    #@7
    .line 269
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setLayoutDirection(I)V

    #@e
    .line 268
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_5

    #@11
    .line 271
    :cond_11
    invoke-super {p0, p1}, Landroid/graphics/drawable/DrawableContainer;->setLayoutDirection(I)V

    #@14
    .line 272
    return-void
.end method
