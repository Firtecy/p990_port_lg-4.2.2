.class public Landroid/graphics/drawable/DrawableContainer;
.super Landroid/graphics/drawable/Drawable;
.source "DrawableContainer.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEFAULT_DITHER:Z = true

.field private static final TAG:Ljava/lang/String; = "DrawableContainer"


# instance fields
.field private mAlpha:I

.field private mAnimationRunnable:Ljava/lang/Runnable;

.field private mColorFilter:Landroid/graphics/ColorFilter;

.field private mCurIndex:I

.field private mCurrDrawable:Landroid/graphics/drawable/Drawable;

.field private mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

.field private mEnterAnimationEnd:J

.field private mExitAnimationEnd:J

.field private mLastDrawable:Landroid/graphics/drawable/Drawable;

.field private mMutated:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 49
    const/16 v0, 0xff

    #@5
    iput v0, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@7
    .line 52
    const/4 v0, -0x1

    #@8
    iput v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurIndex:I

    #@a
    .line 428
    return-void
.end method


# virtual methods
.method animate(Z)V
    .registers 14
    .parameter "schedule"

    #@0
    .prologue
    const-wide/16 v10, 0xff

    #@2
    const/4 v9, 0x0

    #@3
    const-wide/16 v7, 0x0

    #@5
    .line 354
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@8
    move-result-wide v2

    #@9
    .line 355
    .local v2, now:J
    const/4 v1, 0x0

    #@a
    .line 356
    .local v1, animating:Z
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    if-eqz v4, :cond_6b

    #@e
    .line 357
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@10
    cmp-long v4, v4, v7

    #@12
    if-eqz v4, :cond_27

    #@14
    .line 358
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@16
    cmp-long v4, v4, v2

    #@18
    if-gtz v4, :cond_4e

    #@1a
    .line 359
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@1c
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@1f
    move-result-object v4

    #@20
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@22
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@25
    .line 360
    iput-wide v7, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@27
    .line 372
    :cond_27
    :goto_27
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@29
    if-eqz v4, :cond_89

    #@2b
    .line 373
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@2d
    cmp-long v4, v4, v7

    #@2f
    if-eqz v4, :cond_41

    #@31
    .line 374
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@33
    cmp-long v4, v4, v2

    #@35
    if-gtz v4, :cond_6e

    #@37
    .line 375
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@39
    invoke-virtual {v4, v9, v9}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@3c
    .line 376
    const/4 v4, 0x0

    #@3d
    iput-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@3f
    .line 377
    iput-wide v7, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@41
    .line 390
    :cond_41
    :goto_41
    if-eqz p1, :cond_4d

    #@43
    if-eqz v1, :cond_4d

    #@45
    .line 391
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mAnimationRunnable:Ljava/lang/Runnable;

    #@47
    const-wide/16 v5, 0x10

    #@49
    add-long/2addr v5, v2

    #@4a
    invoke-virtual {p0, v4, v5, v6}, Landroid/graphics/drawable/DrawableContainer;->scheduleSelf(Ljava/lang/Runnable;J)V

    #@4d
    .line 393
    :cond_4d
    return-void

    #@4e
    .line 362
    :cond_4e
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@50
    sub-long/2addr v4, v2

    #@51
    mul-long/2addr v4, v10

    #@52
    long-to-int v4, v4

    #@53
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@55
    iget v5, v5, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@57
    div-int v0, v4, v5

    #@59
    .line 365
    .local v0, animAlpha:I
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@5b
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@5e
    move-result-object v4

    #@5f
    rsub-int v5, v0, 0xff

    #@61
    iget v6, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@63
    mul-int/2addr v5, v6

    #@64
    div-int/lit16 v5, v5, 0xff

    #@66
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@69
    .line 366
    const/4 v1, 0x1

    #@6a
    .line 367
    goto :goto_27

    #@6b
    .line 370
    .end local v0           #animAlpha:I
    :cond_6b
    iput-wide v7, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@6d
    goto :goto_27

    #@6e
    .line 379
    :cond_6e
    iget-wide v4, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@70
    sub-long/2addr v4, v2

    #@71
    mul-long/2addr v4, v10

    #@72
    long-to-int v4, v4

    #@73
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@75
    iget v5, v5, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@77
    div-int v0, v4, v5

    #@79
    .line 382
    .restart local v0       #animAlpha:I
    iget-object v4, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@7b
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7e
    move-result-object v4

    #@7f
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@81
    mul-int/2addr v5, v0

    #@82
    div-int/lit16 v5, v5, 0xff

    #@84
    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@87
    .line 383
    const/4 v1, 0x1

    #@88
    .line 384
    goto :goto_41

    #@89
    .line 387
    .end local v0           #animAlpha:I
    :cond_89
    iput-wide v7, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@8b
    goto :goto_41
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 66
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@9
    .line 68
    :cond_9
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 69
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@12
    .line 71
    :cond_12
    return-void
.end method

.method public getChangingConfigurations()I
    .registers 3

    #@0
    .prologue
    .line 75
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    #@3
    move-result v0

    #@4
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@6
    iget v1, v1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChangingConfigurations:I

    #@8
    or-int/2addr v0, v1

    #@9
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@b
    iget v1, v1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChildrenChangingConfigurations:I

    #@d
    or-int/2addr v0, v1

    #@e
    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    #@0
    .prologue
    .line 402
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->canConstantState()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 403
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@a
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getChangingConfigurations()I

    #@d
    move-result v1

    #@e
    iput v1, v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mChangingConfigurations:I

    #@10
    .line 404
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@12
    .line 406
    :goto_12
    return-object v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public getCurrent()Landroid/graphics/drawable/Drawable;
    .registers 2

    #@0
    .prologue
    .line 397
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 226
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->isConstantSize()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 227
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getConstantHeight()I

    #@d
    move-result v0

    #@e
    .line 229
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    if-eqz v0, :cond_1a

    #@13
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@18
    move-result v0

    #@19
    goto :goto_e

    #@1a
    :cond_1a
    const/4 v0, -0x1

    #@1b
    goto :goto_e
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 218
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->isConstantSize()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 219
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getConstantWidth()I

    #@d
    move-result v0

    #@e
    .line 221
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    if-eqz v0, :cond_1a

    #@13
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@18
    move-result v0

    #@19
    goto :goto_e

    #@1a
    :cond_1a
    const/4 v0, -0x1

    #@1b
    goto :goto_e
.end method

.method public getLayoutInsets()Landroid/graphics/Insets;
    .registers 2

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-nez v0, :cond_7

    #@4
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getLayoutInsets()Landroid/graphics/Insets;

    #@c
    move-result-object v0

    #@d
    goto :goto_6
.end method

.method public getMinimumHeight()I
    .registers 2

    #@0
    .prologue
    .line 242
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->isConstantSize()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 243
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getConstantMinimumHeight()I

    #@d
    move-result v0

    #@e
    .line 245
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    if-eqz v0, :cond_1a

    #@13
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    #@18
    move-result v0

    #@19
    goto :goto_e

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_e
.end method

.method public getMinimumWidth()I
    .registers 2

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->isConstantSize()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 235
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@a
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getConstantMinimumWidth()I

    #@d
    move-result v0

    #@e
    .line 237
    :goto_e
    return v0

    #@f
    :cond_f
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    if-eqz v0, :cond_1a

    #@13
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@15
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    #@18
    move-result v0

    #@19
    goto :goto_e

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_e
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 280
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    :cond_c
    const/4 v0, -0x2

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@10
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getOpacity()I

    #@13
    move-result v0

    #@14
    goto :goto_d
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .registers 4
    .parameter "padding"

    #@0
    .prologue
    .line 82
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v1}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getConstantPadding()Landroid/graphics/Rect;

    #@5
    move-result-object v0

    #@6
    .line 83
    .local v0, r:Landroid/graphics/Rect;
    if-eqz v0, :cond_d

    #@8
    .line 84
    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@b
    .line 85
    const/4 v1, 0x1

    #@c
    .line 90
    :goto_c
    return v1

    #@d
    .line 87
    :cond_d
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    if-eqz v1, :cond_18

    #@11
    .line 88
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@16
    move-result v1

    #@17
    goto :goto_c

    #@18
    .line 90
    :cond_18
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    #@1b
    move-result v1

    #@1c
    goto :goto_c
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "who"

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_11

    #@4
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 250
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0}, Landroid/graphics/drawable/Drawable$Callback;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    #@11
    .line 252
    :cond_11
    return-void
.end method

.method public isStateful()Z
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->isStateful()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public jumpToCurrentState()V
    .registers 6

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 171
    const/4 v0, 0x0

    #@3
    .line 172
    .local v0, changed:Z
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    if-eqz v1, :cond_10

    #@7
    .line 173
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@9
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@c
    .line 174
    const/4 v1, 0x0

    #@d
    iput-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    .line 175
    const/4 v0, 0x1

    #@10
    .line 177
    :cond_10
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@12
    if-eqz v1, :cond_24

    #@14
    .line 178
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    #@19
    .line 179
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@1b
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@1e
    move-result-object v1

    #@1f
    iget v2, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@21
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@24
    .line 181
    :cond_24
    iget-wide v1, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@26
    cmp-long v1, v1, v3

    #@28
    if-eqz v1, :cond_2d

    #@2a
    .line 182
    iput-wide v3, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@2c
    .line 183
    const/4 v0, 0x1

    #@2d
    .line 185
    :cond_2d
    iget-wide v1, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@2f
    cmp-long v1, v1, v3

    #@31
    if-eqz v1, :cond_36

    #@33
    .line 186
    iput-wide v3, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@35
    .line 187
    const/4 v0, 0x1

    #@36
    .line 189
    :cond_36
    if-eqz v0, :cond_3b

    #@38
    .line 190
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->invalidateSelf()V

    #@3b
    .line 192
    :cond_3b
    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 5

    #@0
    .prologue
    .line 411
    iget-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer;->mMutated:Z

    #@2
    if-nez v3, :cond_28

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v3

    #@8
    if-ne v3, p0, :cond_28

    #@a
    .line 412
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@c
    invoke-virtual {v3}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildCount()I

    #@f
    move-result v0

    #@10
    .line 413
    .local v0, N:I
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@12
    invoke-virtual {v3}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->getChildren()[Landroid/graphics/drawable/Drawable;

    #@15
    move-result-object v1

    #@16
    .line 414
    .local v1, drawables:[Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    #@17
    .local v2, i:I
    :goto_17
    if-ge v2, v0, :cond_25

    #@19
    .line 415
    aget-object v3, v1, v2

    #@1b
    if-eqz v3, :cond_22

    #@1d
    aget-object v3, v1, v2

    #@1f
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@22
    .line 414
    :cond_22
    add-int/lit8 v2, v2, 0x1

    #@24
    goto :goto_17

    #@25
    .line 417
    :cond_25
    const/4 v3, 0x1

    #@26
    iput-boolean v3, p0, Landroid/graphics/drawable/DrawableContainer;->mMutated:Z

    #@28
    .line 419
    .end local v0           #N:I
    .end local v1           #drawables:[Landroid/graphics/drawable/Drawable;
    .end local v2           #i:I
    :cond_28
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 156
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 157
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@9
    .line 159
    :cond_9
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 160
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@12
    .line 162
    :cond_12
    return-void
.end method

.method protected onLevelChange(I)Z
    .registers 3
    .parameter "level"

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 208
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@9
    move-result v0

    #@a
    .line 213
    :goto_a
    return v0

    #@b
    .line 210
    :cond_b
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 211
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@14
    move-result v0

    #@15
    goto :goto_a

    #@16
    .line 213
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_a
.end method

.method protected onStateChange([I)Z
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 196
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 197
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@9
    move-result v0

    #@a
    .line 202
    :goto_a
    return v0

    #@b
    .line 199
    :cond_b
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 200
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@11
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@14
    move-result v0

    #@15
    goto :goto_a

    #@16
    .line 202
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_a
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 6
    .parameter "who"
    .parameter "what"
    .parameter "when"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_11

    #@4
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 256
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0, p2, p3, p4}, Landroid/graphics/drawable/Drawable$Callback;->scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V

    #@11
    .line 258
    :cond_11
    return-void
.end method

.method public selectDrawable(I)Z
    .registers 12
    .parameter "idx"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const-wide/16 v7, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 285
    iget v5, p0, Landroid/graphics/drawable/DrawableContainer;->mCurIndex:I

    #@7
    if-ne p1, v5, :cond_a

    #@9
    .line 350
    :goto_9
    return v3

    #@a
    .line 289
    :cond_a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@d
    move-result-wide v1

    #@e
    .line 295
    .local v1, now:J
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@10
    iget v5, v5, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@12
    if-lez v5, :cond_9f

    #@14
    .line 296
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@16
    if-eqz v5, :cond_1d

    #@18
    .line 297
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@1a
    invoke-virtual {v5, v3, v3}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@1d
    .line 299
    :cond_1d
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@1f
    if-eqz v3, :cond_9a

    #@21
    .line 300
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@23
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@25
    .line 301
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@27
    iget v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@29
    int-to-long v5, v3

    #@2a
    add-long/2addr v5, v1

    #@2b
    iput-wide v5, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@2d
    .line 310
    :cond_2d
    :goto_2d
    if-ltz p1, :cond_af

    #@2f
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@31
    iget v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mNumChildren:I

    #@33
    if-ge p1, v3, :cond_af

    #@35
    .line 311
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@37
    iget-object v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDrawables:[Landroid/graphics/drawable/Drawable;

    #@39
    aget-object v0, v3, p1

    #@3b
    .line 312
    .local v0, d:Landroid/graphics/drawable/Drawable;
    iput-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@3d
    .line 313
    iput p1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurIndex:I

    #@3f
    .line 314
    if-eqz v0, :cond_7a

    #@41
    .line 315
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@44
    .line 316
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@46
    iget v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@48
    if-lez v3, :cond_a9

    #@4a
    .line 317
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@4c
    iget v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@4e
    int-to-long v5, v3

    #@4f
    add-long/2addr v5, v1

    #@50
    iput-wide v5, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@52
    .line 321
    :goto_52
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->isVisible()Z

    #@55
    move-result v3

    #@56
    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@59
    .line 322
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@5b
    iget-boolean v3, v3, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@5d
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    #@60
    .line 323
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mColorFilter:Landroid/graphics/ColorFilter;

    #@62
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@65
    .line 324
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getState()[I

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    #@6c
    .line 325
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getLevel()I

    #@6f
    move-result v3

    #@70
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    #@73
    .line 326
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getBounds()Landroid/graphics/Rect;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    #@7a
    .line 333
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    :cond_7a
    :goto_7a
    iget-wide v5, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@7c
    cmp-long v3, v5, v7

    #@7e
    if-nez v3, :cond_86

    #@80
    iget-wide v5, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@82
    cmp-long v3, v5, v7

    #@84
    if-eqz v3, :cond_94

    #@86
    .line 334
    :cond_86
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mAnimationRunnable:Ljava/lang/Runnable;

    #@88
    if-nez v3, :cond_b5

    #@8a
    .line 335
    new-instance v3, Landroid/graphics/drawable/DrawableContainer$1;

    #@8c
    invoke-direct {v3, p0}, Landroid/graphics/drawable/DrawableContainer$1;-><init>(Landroid/graphics/drawable/DrawableContainer;)V

    #@8f
    iput-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mAnimationRunnable:Ljava/lang/Runnable;

    #@91
    .line 345
    :goto_91
    invoke-virtual {p0, v4}, Landroid/graphics/drawable/DrawableContainer;->animate(Z)V

    #@94
    .line 348
    :cond_94
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->invalidateSelf()V

    #@97
    move v3, v4

    #@98
    .line 350
    goto/16 :goto_9

    #@9a
    .line 303
    :cond_9a
    iput-object v9, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@9c
    .line 304
    iput-wide v7, p0, Landroid/graphics/drawable/DrawableContainer;->mExitAnimationEnd:J

    #@9e
    goto :goto_2d

    #@9f
    .line 306
    :cond_9f
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@a1
    if-eqz v5, :cond_2d

    #@a3
    .line 307
    iget-object v5, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@a5
    invoke-virtual {v5, v3, v3}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@a8
    goto :goto_2d

    #@a9
    .line 319
    .restart local v0       #d:Landroid/graphics/drawable/Drawable;
    :cond_a9
    iget v3, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@ab
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@ae
    goto :goto_52

    #@af
    .line 329
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    :cond_af
    iput-object v9, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@b1
    .line 330
    const/4 v3, -0x1

    #@b2
    iput v3, p0, Landroid/graphics/drawable/DrawableContainer;->mCurIndex:I

    #@b4
    goto :goto_7a

    #@b5
    .line 342
    :cond_b5
    iget-object v3, p0, Landroid/graphics/drawable/DrawableContainer;->mAnimationRunnable:Ljava/lang/Runnable;

    #@b7
    invoke-virtual {p0, v3}, Landroid/graphics/drawable/DrawableContainer;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@ba
    goto :goto_91
.end method

.method public setAlpha(I)V
    .registers 6
    .parameter "alpha"

    #@0
    .prologue
    .line 104
    iget v0, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@2
    if-eq v0, p1, :cond_1b

    #@4
    .line 105
    iput p1, p0, Landroid/graphics/drawable/DrawableContainer;->mAlpha:I

    #@6
    .line 106
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_1b

    #@a
    .line 107
    iget-wide v0, p0, Landroid/graphics/drawable/DrawableContainer;->mEnterAnimationEnd:J

    #@c
    const-wide/16 v2, 0x0

    #@e
    cmp-long v0, v0, v2

    #@10
    if-nez v0, :cond_1c

    #@12
    .line 108
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@14
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    #@1b
    .line 114
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 110
    :cond_1c
    const/4 v0, 0x0

    #@1d
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/DrawableContainer;->animate(Z)V

    #@20
    goto :goto_1b
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .parameter "cf"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    if-eq v0, p1, :cond_13

    #@4
    .line 129
    iput-object p1, p0, Landroid/graphics/drawable/DrawableContainer;->mColorFilter:Landroid/graphics/ColorFilter;

    #@6
    .line 130
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@8
    if-eqz v0, :cond_13

    #@a
    .line 131
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    #@13
    .line 134
    :cond_13
    return-void
.end method

.method protected setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 712
    iput-object p1, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    .line 713
    return-void
.end method

.method public setDither(Z)V
    .registers 4
    .parameter "dither"

    #@0
    .prologue
    .line 118
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    iget-boolean v0, v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@4
    if-eq v0, p1, :cond_1b

    #@6
    .line 119
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@8
    iput-boolean p1, v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@a
    .line 120
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@c
    if-eqz v0, :cond_1b

    #@e
    .line 121
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@10
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    #@13
    move-result-object v0

    #@14
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@16
    iget-boolean v1, v1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mDither:Z

    #@18
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    #@1b
    .line 124
    :cond_1b
    return-void
.end method

.method public setEnterFadeDuration(I)V
    .registers 3
    .parameter "ms"

    #@0
    .prologue
    .line 142
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mEnterFadeDuration:I

    #@4
    .line 143
    return-void
.end method

.method public setExitFadeDuration(I)V
    .registers 3
    .parameter "ms"

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mDrawableContainerState:Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    #@2
    iput p1, v0, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->mExitFadeDuration:I

    #@4
    .line 152
    return-void
.end method

.method public setVisible(ZZ)Z
    .registers 5
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    .line 268
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@3
    move-result v0

    #@4
    .line 269
    .local v0, changed:Z
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@6
    if-eqz v1, :cond_d

    #@8
    .line 270
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mLastDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@d
    .line 272
    :cond_d
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@f
    if-eqz v1, :cond_16

    #@11
    .line 273
    iget-object v1, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@13
    invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    #@16
    .line 275
    :cond_16
    return v0
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    #@0
    .prologue
    .line 261
    iget-object v0, p0, Landroid/graphics/drawable/DrawableContainer;->mCurrDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    if-ne p1, v0, :cond_11

    #@4
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@7
    move-result-object v0

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 262
    invoke-virtual {p0}, Landroid/graphics/drawable/DrawableContainer;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    #@d
    move-result-object v0

    #@e
    invoke-interface {v0, p0, p2}, Landroid/graphics/drawable/Drawable$Callback;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V

    #@11
    .line 264
    :cond_11
    return-void
.end method
