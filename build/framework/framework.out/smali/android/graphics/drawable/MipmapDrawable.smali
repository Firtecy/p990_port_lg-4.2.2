.class public Landroid/graphics/drawable/MipmapDrawable;
.super Landroid/graphics/drawable/DrawableContainer;
.source "MipmapDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/MipmapDrawable$1;,
        Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;
    }
.end annotation


# instance fields
.field private final mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

.field private mMutated:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 89
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/MipmapDrawable;-><init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;)V

    #@4
    .line 90
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;)V
    .registers 4
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    .line 306
    invoke-direct {p0}, Landroid/graphics/drawable/DrawableContainer;-><init>()V

    #@3
    .line 307
    new-instance v0, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@5
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;-><init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/graphics/drawable/MipmapDrawable;Landroid/content/res/Resources;)V

    #@8
    .line 308
    .local v0, as:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;
    iput-object v0, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@a
    .line 309
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/MipmapDrawable;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    #@d
    .line 310
    invoke-direct {p0}, Landroid/graphics/drawable/MipmapDrawable;->onDrawableAdded()V

    #@10
    .line 311
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;Landroid/graphics/drawable/MipmapDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/MipmapDrawable;-><init>(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method

.method private onDrawableAdded()V
    .registers 2

    #@0
    .prologue
    .line 114
    const/4 v0, -0x1

    #@1
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/MipmapDrawable;->selectDrawable(I)Z

    #@4
    .line 115
    invoke-virtual {p0}, Landroid/graphics/drawable/MipmapDrawable;->getBounds()Landroid/graphics/Rect;

    #@7
    move-result-object v0

    #@8
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/MipmapDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    #@b
    .line 116
    return-void
.end method


# virtual methods
.method public addDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "drawable"

    #@0
    .prologue
    .line 102
    if-eqz p1, :cond_a

    #@2
    .line 103
    iget-object v0, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@4
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->addDrawable(Landroid/graphics/drawable/Drawable;)V

    #@7
    .line 104
    invoke-direct {p0}, Landroid/graphics/drawable/MipmapDrawable;->onDrawableAdded()V

    #@a
    .line 106
    :cond_a
    return-void
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 14
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v8, 0x0

    #@2
    .line 134
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/DrawableContainer;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    #@5
    .line 138
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@8
    move-result v6

    #@9
    add-int/lit8 v4, v6, 0x1

    #@b
    .line 141
    .local v4, innerDepth:I
    :cond_b
    :goto_b
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@e
    move-result v5

    #@f
    .local v5, type:I
    const/4 v6, 0x1

    #@10
    if-eq v5, v6, :cond_76

    #@12
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@15
    move-result v1

    #@16
    .local v1, depth:I
    if-ge v1, v4, :cond_1b

    #@18
    const/4 v6, 0x3

    #@19
    if-eq v5, v6, :cond_76

    #@1b
    .line 143
    :cond_1b
    if-ne v5, v9, :cond_b

    #@1d
    .line 147
    if-gt v1, v4, :cond_b

    #@1f
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    const-string/jumbo v7, "item"

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29
    move-result v6

    #@2a
    if-eqz v6, :cond_b

    #@2c
    .line 151
    sget-object v6, Lcom/android/internal/R$styleable;->MipmapDrawableItem:[I

    #@2e
    invoke-virtual {p1, p3, v6}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@31
    move-result-object v0

    #@32
    .line 154
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@35
    move-result v3

    #@36
    .line 157
    .local v3, drawableRes:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@39
    .line 160
    if-eqz v3, :cond_45

    #@3b
    .line 161
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@3e
    move-result-object v2

    #@3f
    .line 174
    .local v2, dr:Landroid/graphics/drawable/Drawable;
    :goto_3f
    iget-object v6, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@41
    invoke-virtual {v6, v2}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->addDrawable(Landroid/graphics/drawable/Drawable;)V

    #@44
    goto :goto_b

    #@45
    .line 163
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    :cond_45
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@48
    move-result v5

    #@49
    const/4 v6, 0x4

    #@4a
    if-eq v5, v6, :cond_45

    #@4c
    .line 165
    if-eq v5, v9, :cond_71

    #@4e
    .line 166
    new-instance v6, Lorg/xmlpull/v1/XmlPullParserException;

    #@50
    new-instance v7, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    const-string v8, ": <item> tag requires a \'drawable\' attribute or "

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, "child tag defining a drawable"

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v7

    #@6d
    invoke-direct {v6, v7}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@70
    throw v6

    #@71
    .line 171
    :cond_71
    invoke-static {p1, p2, p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@74
    move-result-object v2

    #@75
    .restart local v2       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_3f

    #@76
    .line 177
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v1           #depth:I
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    .end local v3           #drawableRes:I
    :cond_76
    invoke-direct {p0}, Landroid/graphics/drawable/MipmapDrawable;->onDrawableAdded()V

    #@79
    .line 178
    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 182
    iget-boolean v0, p0, Landroid/graphics/drawable/MipmapDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_1e

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/DrawableContainer;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_1e

    #@a
    .line 183
    iget-object v1, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@c
    iget-object v0, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@e
    invoke-static {v0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->access$000(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;)[I

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, [I

    #@18
    invoke-static {v1, v0}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->access$002(Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;[I)[I

    #@1b
    .line 184
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Landroid/graphics/drawable/MipmapDrawable;->mMutated:Z

    #@1e
    .line 186
    :cond_1e
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 4
    .parameter "bounds"

    #@0
    .prologue
    .line 122
    iget-object v1, p0, Landroid/graphics/drawable/MipmapDrawable;->mMipmapContainerState:Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;

    #@2
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/MipmapDrawable$MipmapContainerState;->indexForBounds(Landroid/graphics/Rect;)I

    #@5
    move-result v0

    #@6
    .line 125
    .local v0, index:I
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/MipmapDrawable;->selectDrawable(I)Z

    #@9
    .line 127
    invoke-super {p0, p1}, Landroid/graphics/drawable/DrawableContainer;->onBoundsChange(Landroid/graphics/Rect;)V

    #@c
    .line 128
    return-void
.end method
