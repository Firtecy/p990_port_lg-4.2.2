.class public Landroid/graphics/drawable/PictureDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "PictureDrawable.java"


# instance fields
.field private mPicture:Landroid/graphics/Picture;


# direct methods
.method public constructor <init>(Landroid/graphics/Picture;)V
    .registers 2
    .parameter "picture"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    #@3
    .line 45
    iput-object p1, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@5
    .line 46
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 68
    iget-object v1, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@2
    if-eqz v1, :cond_1f

    #@4
    .line 69
    invoke-virtual {p0}, Landroid/graphics/drawable/PictureDrawable;->getBounds()Landroid/graphics/Rect;

    #@7
    move-result-object v0

    #@8
    .line 70
    .local v0, bounds:Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@b
    .line 71
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    #@e
    .line 72
    iget v1, v0, Landroid/graphics/Rect;->left:I

    #@10
    int-to-float v1, v1

    #@11
    iget v2, v0, Landroid/graphics/Rect;->top:I

    #@13
    int-to-float v2, v2

    #@14
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@17
    .line 73
    iget-object v1, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@19
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@1c
    .line 74
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@1f
    .line 76
    .end local v0           #bounds:Landroid/graphics/Rect;
    :cond_1f
    return-void
.end method

.method public getIntrinsicHeight()I
    .registers 2

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Picture;->getHeight()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getIntrinsicWidth()I
    .registers 2

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@2
    if-eqz v0, :cond_b

    #@4
    iget-object v0, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@6
    invoke-virtual {v0}, Landroid/graphics/Picture;->getWidth()I

    #@9
    move-result v0

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, -0x1

    #@c
    goto :goto_a
.end method

.method public getOpacity()I
    .registers 2

    #@0
    .prologue
    .line 91
    const/4 v0, -0x3

    #@1
    return v0
.end method

.method public getPicture()Landroid/graphics/Picture;
    .registers 2

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@2
    return-object v0
.end method

.method public setAlpha(I)V
    .registers 2
    .parameter "alpha"

    #@0
    .prologue
    .line 104
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter "colorFilter"

    #@0
    .prologue
    .line 101
    return-void
.end method

.method public setDither(Z)V
    .registers 2
    .parameter "dither"

    #@0
    .prologue
    .line 98
    return-void
.end method

.method public setFilterBitmap(Z)V
    .registers 2
    .parameter "filter"

    #@0
    .prologue
    .line 95
    return-void
.end method

.method public setPicture(Landroid/graphics/Picture;)V
    .registers 2
    .parameter "picture"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Landroid/graphics/drawable/PictureDrawable;->mPicture:Landroid/graphics/Picture;

    #@2
    .line 64
    return-void
.end method
