.class public Landroid/graphics/drawable/AnimationDrawable;
.super Landroid/graphics/drawable/DrawableContainer;
.source "AnimationDrawable.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/drawable/AnimationDrawable$1;,
        Landroid/graphics/drawable/AnimationDrawable$AnimationState;
    }
.end annotation


# instance fields
.field private final mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

.field private mCurFrame:I

.field private mMutated:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 87
    invoke-direct {p0, v0, v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Landroid/content/res/Resources;)V

    #@4
    .line 88
    return-void
.end method

.method private constructor <init>(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Landroid/content/res/Resources;)V
    .registers 6
    .parameter "state"
    .parameter "res"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 356
    invoke-direct {p0}, Landroid/graphics/drawable/DrawableContainer;-><init>()V

    #@4
    .line 83
    const/4 v1, -0x1

    #@5
    iput v1, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@7
    .line 357
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@9
    invoke-direct {v0, p1, p0, p2}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;-><init>(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Landroid/graphics/drawable/AnimationDrawable;Landroid/content/res/Resources;)V

    #@c
    .line 358
    .local v0, as:Landroid/graphics/drawable/AnimationDrawable$AnimationState;
    iput-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@e
    .line 359
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/AnimationDrawable;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    #@11
    .line 360
    if-eqz p1, :cond_17

    #@13
    .line 361
    const/4 v1, 0x1

    #@14
    invoke-direct {p0, v2, v1, v2}, Landroid/graphics/drawable/AnimationDrawable;->setFrame(IZZ)V

    #@17
    .line 363
    :cond_17
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Landroid/content/res/Resources;Landroid/graphics/drawable/AnimationDrawable$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/AnimationDrawable;-><init>(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Landroid/content/res/Resources;)V

    #@3
    return-void
.end method

.method private nextFrame(Z)V
    .registers 5
    .parameter "unschedule"

    #@0
    .prologue
    .line 211
    iget v2, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@2
    add-int/lit8 v1, v2, 0x1

    #@4
    .line 212
    .local v1, next:I
    iget-object v2, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@6
    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->getChildCount()I

    #@9
    move-result v0

    #@a
    .line 213
    .local v0, N:I
    if-lt v1, v0, :cond_d

    #@c
    .line 214
    const/4 v1, 0x0

    #@d
    .line 216
    :cond_d
    iget-object v2, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@f
    invoke-static {v2}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$100(Landroid/graphics/drawable/AnimationDrawable$AnimationState;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_19

    #@15
    add-int/lit8 v2, v0, -0x1

    #@17
    if-ge v1, v2, :cond_1e

    #@19
    :cond_19
    const/4 v2, 0x1

    #@1a
    :goto_1a
    invoke-direct {p0, v1, p1, v2}, Landroid/graphics/drawable/AnimationDrawable;->setFrame(IZZ)V

    #@1d
    .line 217
    return-void

    #@1e
    .line 216
    :cond_1e
    const/4 v2, 0x0

    #@1f
    goto :goto_1a
.end method

.method private setFrame(IZZ)V
    .registers 8
    .parameter "frame"
    .parameter "unschedule"
    .parameter "animate"

    #@0
    .prologue
    .line 220
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->getChildCount()I

    #@5
    move-result v0

    #@6
    if-lt p1, v0, :cond_9

    #@8
    .line 233
    :cond_8
    :goto_8
    return-void

    #@9
    .line 223
    :cond_9
    iput p1, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@b
    .line 224
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/AnimationDrawable;->selectDrawable(I)Z

    #@e
    .line 225
    if-eqz p2, :cond_13

    #@10
    .line 226
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimationDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@13
    .line 228
    :cond_13
    if-eqz p3, :cond_8

    #@15
    .line 230
    iput p1, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@17
    .line 231
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1a
    move-result-wide v0

    #@1b
    iget-object v2, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@1d
    invoke-static {v2}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$000(Landroid/graphics/drawable/AnimationDrawable$AnimationState;)[I

    #@20
    move-result-object v2

    #@21
    aget v2, v2, p1

    #@23
    int-to-long v2, v2

    #@24
    add-long/2addr v0, v2

    #@25
    invoke-virtual {p0, p0, v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    #@28
    goto :goto_8
.end method


# virtual methods
.method public addFrame(Landroid/graphics/drawable/Drawable;I)V
    .registers 5
    .parameter "frame"
    .parameter "duration"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 204
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    #@6
    .line 205
    iget v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@8
    if-gez v0, :cond_e

    #@a
    .line 206
    const/4 v0, 0x1

    #@b
    invoke-direct {p0, v1, v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setFrame(IZZ)V

    #@e
    .line 208
    :cond_e
    return-void
.end method

.method public getDuration(I)I
    .registers 3
    .parameter "i"

    #@0
    .prologue
    .line 178
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-static {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$000(Landroid/graphics/drawable/AnimationDrawable$AnimationState;)[I

    #@5
    move-result-object v0

    #@6
    aget v0, v0, p1

    #@8
    return v0
.end method

.method public getFrame(I)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->getChildren()[Landroid/graphics/drawable/Drawable;

    #@5
    move-result-object v0

    #@6
    aget-object v0, v0, p1

    #@8
    return-object v0
.end method

.method public getNumberOfFrames()I
    .registers 2

    #@0
    .prologue
    .line 163
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->getChildCount()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 16
    .parameter "r"
    .parameter "parser"
    .parameter "attrs"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, 0x0

    #@3
    .line 239
    sget-object v7, Lcom/android/internal/R$styleable;->AnimationDrawable:[I

    #@5
    invoke-virtual {p1, p3, v7}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@8
    move-result-object v0

    #@9
    .line 242
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-super {p0, p1, p2, v0, v9}, Landroid/graphics/drawable/DrawableContainer;->inflateWithAttributes(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/TypedArray;I)V

    #@c
    .line 245
    iget-object v7, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@e
    invoke-virtual {v0, v10, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@11
    move-result v8

    #@12
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->setVariablePadding(Z)V

    #@15
    .line 248
    iget-object v7, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@17
    invoke-virtual {v0, v11, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@1a
    move-result v8

    #@1b
    invoke-static {v7, v8}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$102(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Z)Z

    #@1e
    .line 251
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@21
    .line 255
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@24
    move-result v7

    #@25
    add-int/lit8 v5, v7, 0x1

    #@27
    .line 257
    .local v5, innerDepth:I
    :cond_27
    :goto_27
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@2a
    move-result v6

    #@2b
    .local v6, type:I
    if-eq v6, v10, :cond_ba

    #@2d
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    #@30
    move-result v1

    #@31
    .local v1, depth:I
    if-ge v1, v5, :cond_36

    #@33
    const/4 v7, 0x3

    #@34
    if-eq v6, v7, :cond_ba

    #@36
    .line 259
    :cond_36
    if-ne v6, v11, :cond_27

    #@38
    .line 263
    if-gt v1, v5, :cond_27

    #@3a
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    #@3d
    move-result-object v7

    #@3e
    const-string/jumbo v8, "item"

    #@41
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_27

    #@47
    .line 267
    sget-object v7, Lcom/android/internal/R$styleable;->AnimationDrawableItem:[I

    #@49
    invoke-virtual {p1, p3, v7}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    #@4c
    move-result-object v0

    #@4d
    .line 268
    const/4 v7, -0x1

    #@4e
    invoke-virtual {v0, v9, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    #@51
    move-result v4

    #@52
    .line 270
    .local v4, duration:I
    if-gez v4, :cond_71

    #@54
    .line 271
    new-instance v7, Lorg/xmlpull/v1/XmlPullParserException;

    #@56
    new-instance v8, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@5e
    move-result-object v9

    #@5f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v8

    #@63
    const-string v9, ": <item> tag requires a \'duration\' attribute"

    #@65
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v8

    #@69
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v8

    #@6d
    invoke-direct {v7, v8}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@70
    throw v7

    #@71
    .line 275
    :cond_71
    invoke-virtual {v0, v10, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@74
    move-result v3

    #@75
    .line 278
    .local v3, drawableRes:I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@78
    .line 281
    if-eqz v3, :cond_89

    #@7a
    .line 282
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    #@7d
    move-result-object v2

    #@7e
    .line 295
    .local v2, dr:Landroid/graphics/drawable/Drawable;
    :goto_7e
    iget-object v7, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@80
    invoke-virtual {v7, v2, v4}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    #@83
    .line 296
    if-eqz v2, :cond_27

    #@85
    .line 297
    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    #@88
    goto :goto_27

    #@89
    .line 284
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    :cond_89
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    #@8c
    move-result v6

    #@8d
    const/4 v7, 0x4

    #@8e
    if-eq v6, v7, :cond_89

    #@90
    .line 287
    if-eq v6, v11, :cond_b5

    #@92
    .line 288
    new-instance v7, Lorg/xmlpull/v1/XmlPullParserException;

    #@94
    new-instance v8, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    #@9c
    move-result-object v9

    #@9d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v8

    #@a1
    const-string v9, ": <item> tag requires a \'drawable\' attribute or child tag"

    #@a3
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v8

    #@a7
    const-string v9, " defining a drawable"

    #@a9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    invoke-direct {v7, v8}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    #@b4
    throw v7

    #@b5
    .line 292
    :cond_b5
    invoke-static {p1, p2, p3}, Landroid/graphics/drawable/Drawable;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/graphics/drawable/Drawable;

    #@b8
    move-result-object v2

    #@b9
    .restart local v2       #dr:Landroid/graphics/drawable/Drawable;
    goto :goto_7e

    #@ba
    .line 301
    .end local v1           #depth:I
    .end local v2           #dr:Landroid/graphics/drawable/Drawable;
    .end local v3           #drawableRes:I
    .end local v4           #duration:I
    :cond_ba
    invoke-direct {p0, v9, v10, v9}, Landroid/graphics/drawable/AnimationDrawable;->setFrame(IZZ)V

    #@bd
    .line 302
    return-void
.end method

.method public isOneShot()Z
    .registers 2

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-static {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$100(Landroid/graphics/drawable/AnimationDrawable$AnimationState;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isRunning()Z
    .registers 3

    #@0
    .prologue
    .line 140
    iget v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@2
    const/4 v1, -0x1

    #@3
    if-le v0, v1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    #@0
    .prologue
    .line 306
    iget-boolean v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mMutated:Z

    #@2
    if-nez v0, :cond_1e

    #@4
    invoke-super {p0}, Landroid/graphics/drawable/DrawableContainer;->mutate()Landroid/graphics/drawable/Drawable;

    #@7
    move-result-object v0

    #@8
    if-ne v0, p0, :cond_1e

    #@a
    .line 307
    iget-object v1, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@c
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@e
    invoke-static {v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$000(Landroid/graphics/drawable/AnimationDrawable$AnimationState;)[I

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    #@15
    move-result-object v0

    #@16
    check-cast v0, [I

    #@18
    invoke-static {v1, v0}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$002(Landroid/graphics/drawable/AnimationDrawable$AnimationState;[I)[I

    #@1b
    .line 308
    const/4 v0, 0x1

    #@1c
    iput-boolean v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mMutated:Z

    #@1e
    .line 310
    :cond_1e
    return-object p0
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 150
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/graphics/drawable/AnimationDrawable;->nextFrame(Z)V

    #@4
    .line 151
    return-void
.end method

.method public setOneShot(Z)V
    .registers 3
    .parameter "oneShot"

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mAnimationState:Landroid/graphics/drawable/AnimationDrawable$AnimationState;

    #@2
    invoke-static {v0, p1}, Landroid/graphics/drawable/AnimationDrawable$AnimationState;->access$102(Landroid/graphics/drawable/AnimationDrawable$AnimationState;Z)Z

    #@5
    .line 195
    return-void
.end method

.method public setVisible(ZZ)Z
    .registers 6
    .parameter "visible"
    .parameter "restart"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 92
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/DrawableContainer;->setVisible(ZZ)Z

    #@4
    move-result v0

    #@5
    .line 93
    .local v0, changed:Z
    if-eqz p1, :cond_10

    #@7
    .line 94
    if-nez v0, :cond_b

    #@9
    if-eqz p2, :cond_f

    #@b
    .line 95
    :cond_b
    const/4 v1, 0x0

    #@c
    invoke-direct {p0, v1, v2, v2}, Landroid/graphics/drawable/AnimationDrawable;->setFrame(IZZ)V

    #@f
    .line 100
    :cond_f
    :goto_f
    return v0

    #@10
    .line 98
    :cond_10
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimationDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@13
    goto :goto_f
.end method

.method public start()V
    .registers 2

    #@0
    .prologue
    .line 116
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_9

    #@6
    .line 117
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimationDrawable;->run()V

    #@9
    .line 119
    :cond_9
    return-void
.end method

.method public stop()V
    .registers 2

    #@0
    .prologue
    .line 129
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_9

    #@6
    .line 130
    invoke-virtual {p0, p0}, Landroid/graphics/drawable/AnimationDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@9
    .line 132
    :cond_9
    return-void
.end method

.method public unscheduleSelf(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 155
    const/4 v0, -0x1

    #@1
    iput v0, p0, Landroid/graphics/drawable/AnimationDrawable;->mCurFrame:I

    #@3
    .line 156
    invoke-super {p0, p1}, Landroid/graphics/drawable/DrawableContainer;->unscheduleSelf(Ljava/lang/Runnable;)V

    #@6
    .line 157
    return-void
.end method
