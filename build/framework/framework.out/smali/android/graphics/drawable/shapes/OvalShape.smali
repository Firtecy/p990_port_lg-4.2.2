.class public Landroid/graphics/drawable/shapes/OvalShape;
.super Landroid/graphics/drawable/shapes/RectShape;
.source "OvalShape.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .registers 4
    .parameter "canvas"
    .parameter "paint"

    #@0
    .prologue
    .line 37
    invoke-virtual {p0}, Landroid/graphics/drawable/shapes/OvalShape;->rect()Landroid/graphics/RectF;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@7
    .line 38
    return-void
.end method
