.class public Landroid/graphics/drawable/shapes/RoundRectShape;
.super Landroid/graphics/drawable/shapes/RectShape;
.source "RoundRectShape.java"


# instance fields
.field private mInnerRadii:[F

.field private mInnerRect:Landroid/graphics/RectF;

.field private mInset:Landroid/graphics/RectF;

.field private mOuterRadii:[F

.field private mPath:Landroid/graphics/Path;


# direct methods
.method public constructor <init>([FLandroid/graphics/RectF;[F)V
    .registers 6
    .parameter "outerRadii"
    .parameter "inset"
    .parameter "innerRadii"

    #@0
    .prologue
    const/16 v1, 0x8

    #@2
    .line 59
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    #@5
    .line 60
    if-eqz p1, :cond_13

    #@7
    array-length v0, p1

    #@8
    if-ge v0, v1, :cond_13

    #@a
    .line 61
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@c
    const-string/jumbo v1, "outer radii must have >= 8 values"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 63
    :cond_13
    if-eqz p3, :cond_20

    #@15
    array-length v0, p3

    #@16
    if-ge v0, v1, :cond_20

    #@18
    .line 64
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@1a
    const-string v1, "inner radii must have >= 8 values"

    #@1c
    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v0

    #@20
    .line 66
    :cond_20
    iput-object p1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@22
    .line 67
    iput-object p2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@24
    .line 68
    iput-object p3, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@26
    .line 70
    if-eqz p2, :cond_2f

    #@28
    .line 71
    new-instance v0, Landroid/graphics/RectF;

    #@2a
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    #@2d
    iput-object v0, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@2f
    .line 73
    :cond_2f
    new-instance v0, Landroid/graphics/Path;

    #@31
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    #@34
    iput-object v0, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@36
    .line 74
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Landroid/graphics/drawable/shapes/RectShape;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/graphics/drawable/shapes/RoundRectShape;->clone()Landroid/graphics/drawable/shapes/RoundRectShape;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public clone()Landroid/graphics/drawable/shapes/RoundRectShape;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 108
    invoke-super {p0}, Landroid/graphics/drawable/shapes/RectShape;->clone()Landroid/graphics/drawable/shapes/RectShape;

    #@4
    move-result-object v0

    #@5
    check-cast v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    #@7
    .line 109
    .local v0, shape:Landroid/graphics/drawable/shapes/RoundRectShape;
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@9
    if-eqz v1, :cond_3f

    #@b
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@d
    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    #@10
    move-result-object v1

    #@11
    check-cast v1, [F

    #@13
    :goto_13
    iput-object v1, v0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@15
    .line 110
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@17
    if-eqz v1, :cond_41

    #@19
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@1b
    invoke-virtual {v1}, [F->clone()Ljava/lang/Object;

    #@1e
    move-result-object v1

    #@1f
    check-cast v1, [F

    #@21
    :goto_21
    iput-object v1, v0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@23
    .line 111
    new-instance v1, Landroid/graphics/RectF;

    #@25
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@27
    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@2a
    iput-object v1, v0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@2c
    .line 112
    new-instance v1, Landroid/graphics/RectF;

    #@2e
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@30
    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    #@33
    iput-object v1, v0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@35
    .line 113
    new-instance v1, Landroid/graphics/Path;

    #@37
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@39
    invoke-direct {v1, v2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    #@3c
    iput-object v1, v0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@3e
    .line 114
    return-object v0

    #@3f
    :cond_3f
    move-object v1, v2

    #@40
    .line 109
    goto :goto_13

    #@41
    :cond_41
    move-object v1, v2

    #@42
    .line 110
    goto :goto_21
.end method

.method public bridge synthetic clone()Landroid/graphics/drawable/shapes/Shape;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/graphics/drawable/shapes/RoundRectShape;->clone()Landroid/graphics/drawable/shapes/RoundRectShape;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/graphics/drawable/shapes/RoundRectShape;->clone()Landroid/graphics/drawable/shapes/RoundRectShape;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .registers 4
    .parameter "canvas"
    .parameter "paint"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@2
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@5
    .line 79
    return-void
.end method

.method protected onResize(FF)V
    .registers 10
    .parameter "w"
    .parameter "h"

    #@0
    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/shapes/RectShape;->onResize(FF)V

    #@3
    .line 85
    invoke-virtual {p0}, Landroid/graphics/drawable/shapes/RoundRectShape;->rect()Landroid/graphics/RectF;

    #@6
    move-result-object v0

    #@7
    .line 86
    .local v0, r:Landroid/graphics/RectF;
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@9
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    #@c
    .line 88
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@e
    if-eqz v1, :cond_62

    #@10
    .line 89
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@12
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mOuterRadii:[F

    #@14
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@16
    invoke-virtual {v1, v0, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    #@19
    .line 93
    :goto_19
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@1b
    if-eqz v1, :cond_61

    #@1d
    .line 94
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@1f
    iget v2, v0, Landroid/graphics/RectF;->left:F

    #@21
    iget-object v3, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@23
    iget v3, v3, Landroid/graphics/RectF;->left:F

    #@25
    add-float/2addr v2, v3

    #@26
    iget v3, v0, Landroid/graphics/RectF;->top:F

    #@28
    iget-object v4, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@2a
    iget v4, v4, Landroid/graphics/RectF;->top:F

    #@2c
    add-float/2addr v3, v4

    #@2d
    iget v4, v0, Landroid/graphics/RectF;->right:F

    #@2f
    iget-object v5, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@31
    iget v5, v5, Landroid/graphics/RectF;->right:F

    #@33
    sub-float/2addr v4, v5

    #@34
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    #@36
    iget-object v6, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInset:Landroid/graphics/RectF;

    #@38
    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    #@3a
    sub-float/2addr v5, v6

    #@3b
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    #@3e
    .line 96
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@40
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    #@43
    move-result v1

    #@44
    cmpg-float v1, v1, p1

    #@46
    if-gez v1, :cond_61

    #@48
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@4a
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    #@4d
    move-result v1

    #@4e
    cmpg-float v1, v1, p2

    #@50
    if-gez v1, :cond_61

    #@52
    .line 97
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@54
    if-eqz v1, :cond_6a

    #@56
    .line 98
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@58
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@5a
    iget-object v3, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRadii:[F

    #@5c
    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    #@5e
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    #@61
    .line 104
    :cond_61
    :goto_61
    return-void

    #@62
    .line 91
    :cond_62
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@64
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@66
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    #@69
    goto :goto_19

    #@6a
    .line 100
    :cond_6a
    iget-object v1, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mPath:Landroid/graphics/Path;

    #@6c
    iget-object v2, p0, Landroid/graphics/drawable/shapes/RoundRectShape;->mInnerRect:Landroid/graphics/RectF;

    #@6e
    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    #@70
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    #@73
    goto :goto_61
.end method
