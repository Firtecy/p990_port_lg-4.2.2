.class final Landroid/graphics/Region$1;
.super Ljava/lang/Object;
.source "Region.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/Region;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Landroid/graphics/Region;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 297
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/graphics/Region;
    .registers 4
    .parameter "p"

    #@0
    .prologue
    .line 304
    #calls: Landroid/graphics/Region;->nativeCreateFromParcel(Landroid/os/Parcel;)I
    invoke-static {p1}, Landroid/graphics/Region;->access$000(Landroid/os/Parcel;)I

    #@3
    move-result v0

    #@4
    .line 305
    .local v0, ni:I
    if-nez v0, :cond_c

    #@6
    .line 306
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    #@b
    throw v1

    #@c
    .line 308
    :cond_c
    new-instance v1, Landroid/graphics/Region;

    #@e
    invoke-direct {v1, v0}, Landroid/graphics/Region;-><init>(I)V

    #@11
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 297
    invoke-virtual {p0, p1}, Landroid/graphics/Region$1;->createFromParcel(Landroid/os/Parcel;)Landroid/graphics/Region;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Landroid/graphics/Region;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 311
    new-array v0, p1, [Landroid/graphics/Region;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 297
    invoke-virtual {p0, p1}, Landroid/graphics/Region$1;->newArray(I)[Landroid/graphics/Region;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
