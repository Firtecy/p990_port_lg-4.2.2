.class public Landroid/graphics/BitmapFactory;
.super Ljava/lang/Object;
.source "BitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/BitmapFactory$Options;
    }
.end annotation


# static fields
.field private static final DECODE_BUFFER_SIZE:I = 0x4000


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    return-void
.end method

.method public static decodeByteArray([BII)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "data"
    .parameter "offset"
    .parameter "length"

    #@0
    .prologue
    .line 452
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "data"
    .parameter "offset"
    .parameter "length"
    .parameter "opts"

    #@0
    .prologue
    .line 431
    or-int v1, p1, p2

    #@2
    if-ltz v1, :cond_9

    #@4
    array-length v1, p0

    #@5
    add-int v2, p1, p2

    #@7
    if-ge v1, v2, :cond_f

    #@9
    .line 432
    :cond_9
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v1

    #@f
    .line 434
    :cond_f
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->nativeDecodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@12
    move-result-object v0

    #@13
    .line 436
    .local v0, bm:Landroid/graphics/Bitmap;
    if-nez v0, :cond_23

    #@15
    if-eqz p3, :cond_23

    #@17
    iget-object v1, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@19
    if-eqz v1, :cond_23

    #@1b
    .line 437
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1d
    const-string v2, "Problem decoding into existing bitmap"

    #@1f
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@22
    throw v1

    #@23
    .line 439
    :cond_23
    return-object v0
.end method

.method public static decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "pathName"

    #@0
    .prologue
    .line 334
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 9
    .parameter "pathName"
    .parameter "opts"

    #@0
    .prologue
    .line 304
    const/4 v0, 0x0

    #@1
    .line 305
    .local v0, bm:Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    #@2
    .line 307
    .local v2, stream:Ljava/io/InputStream;
    :try_start_2
    new-instance v3, Ljava/io/FileInputStream;

    #@4
    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_37
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_7} :catch_16

    #@7
    .line 308
    .end local v2           #stream:Ljava/io/InputStream;
    .local v3, stream:Ljava/io/InputStream;
    const/4 v4, 0x0

    #@8
    :try_start_8
    invoke-static {v3, v4, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_40
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_b} :catch_43

    #@b
    move-result-object v0

    #@c
    .line 315
    if-eqz v3, :cond_46

    #@e
    .line 317
    :try_start_e
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_11} :catch_13

    #@11
    move-object v2, v3

    #@12
    .line 323
    .end local v3           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    :cond_12
    :goto_12
    return-object v0

    #@13
    .line 318
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v3       #stream:Ljava/io/InputStream;
    :catch_13
    move-exception v4

    #@14
    move-object v2, v3

    #@15
    .line 320
    .end local v3           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    goto :goto_12

    #@16
    .line 309
    :catch_16
    move-exception v1

    #@17
    .line 313
    .local v1, e:Ljava/lang/Exception;
    :goto_17
    :try_start_17
    const-string v4, "BitmapFactory"

    #@19
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "Unable to decode stream: "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_37

    #@2f
    .line 315
    if-eqz v2, :cond_12

    #@31
    .line 317
    :try_start_31
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_34} :catch_35

    #@34
    goto :goto_12

    #@35
    .line 318
    :catch_35
    move-exception v4

    #@36
    goto :goto_12

    #@37
    .line 315
    .end local v1           #e:Ljava/lang/Exception;
    :catchall_37
    move-exception v4

    #@38
    :goto_38
    if-eqz v2, :cond_3d

    #@3a
    .line 317
    :try_start_3a
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_3e

    #@3d
    .line 320
    :cond_3d
    :goto_3d
    throw v4

    #@3e
    .line 318
    :catch_3e
    move-exception v5

    #@3f
    goto :goto_3d

    #@40
    .line 315
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v3       #stream:Ljava/io/InputStream;
    :catchall_40
    move-exception v4

    #@41
    move-object v2, v3

    #@42
    .end local v3           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    goto :goto_38

    #@43
    .line 309
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v3       #stream:Ljava/io/InputStream;
    :catch_43
    move-exception v1

    #@44
    move-object v2, v3

    #@45
    .end local v3           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    goto :goto_17

    #@46
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v3       #stream:Ljava/io/InputStream;
    :cond_46
    move-object v2, v3

    #@47
    .end local v3           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    goto :goto_12
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "fd"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 659
    invoke-static {p0, v0, v0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "fd"
    .parameter "outPadding"
    .parameter "opts"

    #@0
    .prologue
    .line 632
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->nativeIsSeekable(Ljava/io/FileDescriptor;)Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_1f

    #@6
    .line 633
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->nativeDecodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@9
    move-result-object v0

    #@a
    .line 634
    .local v0, bm:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1a

    #@c
    if-eqz p2, :cond_1a

    #@e
    iget-object v2, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@10
    if-eqz v2, :cond_1a

    #@12
    .line 635
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@14
    const-string v3, "Problem decoding into existing bitmap"

    #@16
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@19
    throw v2

    #@1a
    .line 637
    :cond_1a
    invoke-static {v0, p1, p2}, Landroid/graphics/BitmapFactory;->finishDecode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@1d
    move-result-object v2

    #@1e
    .line 645
    .end local v0           #bm:Landroid/graphics/Bitmap;
    :goto_1e
    return-object v2

    #@1f
    .line 639
    :cond_1f
    new-instance v1, Ljava/io/FileInputStream;

    #@21
    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    #@24
    .line 641
    .local v1, fis:Ljava/io/FileInputStream;
    :try_start_24
    invoke-static {v1, p1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_2e

    #@27
    move-result-object v2

    #@28
    .line 644
    :try_start_28
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_1e

    #@2c
    .line 645
    :catch_2c
    move-exception v3

    #@2d
    goto :goto_1e

    #@2e
    .line 643
    :catchall_2e
    move-exception v2

    #@2f
    .line 644
    :try_start_2f
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_32} :catch_33

    #@32
    .line 645
    :goto_32
    throw v2

    #@33
    :catch_33
    move-exception v3

    #@34
    goto :goto_32
.end method

.method public static decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "res"
    .parameter "id"

    #@0
    .prologue
    .line 414
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "res"
    .parameter "id"
    .parameter "opts"

    #@0
    .prologue
    .line 377
    const/4 v0, 0x0

    #@1
    .line 378
    .local v0, bm:Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    #@2
    .line 381
    .local v1, is:Ljava/io/InputStream;
    :try_start_2
    new-instance v2, Landroid/util/TypedValue;

    #@4
    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    #@7
    .line 382
    .local v2, value:Landroid/util/TypedValue;
    invoke-virtual {p0, p1, v2}, Landroid/content/res/Resources;->openRawResource(ILandroid/util/TypedValue;)Ljava/io/InputStream;

    #@a
    move-result-object v1

    #@b
    .line 384
    const/4 v3, 0x0

    #@c
    invoke-static {p0, v2, v1, v3, p2}, Landroid/graphics/BitmapFactory;->decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_2e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_f} :catch_25

    #@f
    move-result-object v0

    #@10
    .line 392
    if-eqz v1, :cond_15

    #@12
    :try_start_12
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_15} :catch_35

    #@15
    .line 398
    .end local v2           #value:Landroid/util/TypedValue;
    :cond_15
    :goto_15
    if-nez v0, :cond_39

    #@17
    if-eqz p2, :cond_39

    #@19
    iget-object v3, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@1b
    if-eqz v3, :cond_39

    #@1d
    .line 399
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v4, "Problem decoding into existing bitmap"

    #@21
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v3

    #@25
    .line 385
    :catch_25
    move-exception v3

    #@26
    .line 392
    if-eqz v1, :cond_15

    #@28
    :try_start_28
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2b} :catch_2c

    #@2b
    goto :goto_15

    #@2c
    .line 393
    :catch_2c
    move-exception v3

    #@2d
    goto :goto_15

    #@2e
    .line 391
    :catchall_2e
    move-exception v3

    #@2f
    .line 392
    if-eqz v1, :cond_34

    #@31
    :try_start_31
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_34} :catch_37

    #@34
    .line 395
    :cond_34
    :goto_34
    throw v3

    #@35
    .line 393
    .restart local v2       #value:Landroid/util/TypedValue;
    :catch_35
    move-exception v3

    #@36
    goto :goto_15

    #@37
    .end local v2           #value:Landroid/util/TypedValue;
    :catch_37
    move-exception v4

    #@38
    goto :goto_34

    #@39
    .line 402
    :cond_39
    return-object v0
.end method

.method public static decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "res"
    .parameter "value"
    .parameter "is"
    .parameter "pad"
    .parameter "opts"

    #@0
    .prologue
    .line 344
    if-nez p4, :cond_7

    #@2
    .line 345
    new-instance p4, Landroid/graphics/BitmapFactory$Options;

    #@4
    .end local p4
    invoke-direct {p4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    #@7
    .line 348
    .restart local p4
    :cond_7
    iget v1, p4, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@9
    if-nez v1, :cond_15

    #@b
    if-eqz p1, :cond_15

    #@d
    .line 349
    iget v0, p1, Landroid/util/TypedValue;->density:I

    #@f
    .line 350
    .local v0, density:I
    if-nez v0, :cond_28

    #@11
    .line 351
    const/16 v1, 0xa0

    #@13
    iput v1, p4, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@15
    .line 357
    .end local v0           #density:I
    :cond_15
    :goto_15
    iget v1, p4, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@17
    if-nez v1, :cond_23

    #@19
    if-eqz p0, :cond_23

    #@1b
    .line 358
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    #@1e
    move-result-object v1

    #@1f
    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@21
    iput v1, p4, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@23
    .line 361
    :cond_23
    invoke-static {p2, p3, p4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@26
    move-result-object v1

    #@27
    return-object v1

    #@28
    .line 352
    .restart local v0       #density:I
    :cond_28
    const v1, 0xffff

    #@2b
    if-eq v0, v1, :cond_15

    #@2d
    .line 353
    iput v0, p4, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@2f
    goto :goto_15
.end method

.method public static decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "is"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 614
    invoke-static {p0, v0, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 16
    .parameter "is"
    .parameter "outPadding"
    .parameter "opts"

    #@0
    .prologue
    const/16 v3, 0x4000

    #@2
    const/4 v4, 0x1

    #@3
    .line 476
    if-nez p0, :cond_7

    #@5
    .line 477
    const/4 v7, 0x0

    #@6
    .line 555
    .end local p0
    :cond_6
    :goto_6
    return-object v7

    #@7
    .line 482
    .restart local p0
    :cond_7
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_13

    #@d
    .line 483
    new-instance v10, Ljava/io/BufferedInputStream;

    #@f
    invoke-direct {v10, p0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    #@12
    .end local p0
    .local v10, is:Ljava/io/InputStream;
    move-object p0, v10

    #@13
    .line 489
    .end local v10           #is:Ljava/io/InputStream;
    .restart local p0
    :cond_13
    const/16 v0, 0x400

    #@15
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->mark(I)V

    #@18
    .line 492
    const/4 v9, 0x1

    #@19
    .line 494
    .local v9, finish:Z
    const/4 v12, 0x0

    #@1a
    .line 495
    .local v12, targetDensity:I
    const/4 v11, 0x0

    #@1b
    .line 496
    .local v11, isResize:Z
    if-eqz p2, :cond_34

    #@1d
    iget-boolean v0, p2, Landroid/graphics/BitmapFactory$Options;->inForceScaled:Z

    #@1f
    if-eqz v0, :cond_34

    #@21
    .line 498
    iget v12, p2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@23
    .line 499
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@25
    sget v2, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@27
    if-ne v0, v2, :cond_76

    #@29
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@2b
    if-ne v12, v0, :cond_76

    #@2d
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@2f
    sget v2, Landroid/util/DisplayMetrics;->DENSITY_DEVICE_SECONDARY:I

    #@31
    if-eq v0, v2, :cond_76

    #@33
    move v11, v4

    #@34
    .line 503
    :cond_34
    :goto_34
    instance-of v0, p0, Landroid/content/res/AssetManager$AssetInputStream;

    #@36
    if-eqz v0, :cond_7d

    #@38
    .line 504
    check-cast p0, Landroid/content/res/AssetManager$AssetInputStream;

    #@3a
    .end local p0
    invoke-virtual {p0}, Landroid/content/res/AssetManager$AssetInputStream;->getAssetInt()I

    #@3d
    move-result v6

    #@3e
    .line 506
    .local v6, asset:I
    if-eqz p2, :cond_48

    #@40
    iget-boolean v0, p2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    #@42
    if-eqz v0, :cond_78

    #@44
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@46
    if-nez v0, :cond_78

    #@48
    .line 507
    :cond_48
    const/high16 v5, 0x3f80

    #@4a
    .line 508
    .local v5, scale:F
    if-eqz p2, :cond_5a

    #@4c
    if-nez v11, :cond_5a

    #@4e
    .line 509
    iget v8, p2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@50
    .line 510
    .local v8, density:I
    iget v12, p2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@52
    .line 511
    if-eqz v8, :cond_5a

    #@54
    if-eqz v12, :cond_5a

    #@56
    .line 512
    int-to-float v0, v12

    #@57
    int-to-float v2, v8

    #@58
    div-float v5, v0, v2

    #@5a
    .line 516
    .end local v8           #density:I
    :cond_5a
    invoke-static {v6, p1, p2, v4, v5}, Landroid/graphics/BitmapFactory;->nativeDecodeAsset(ILandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;ZF)Landroid/graphics/Bitmap;

    #@5d
    move-result-object v7

    #@5e
    .line 517
    .local v7, bm:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_65

    #@60
    if-eqz v12, :cond_65

    #@62
    invoke-virtual {v7, v12}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@65
    .line 519
    :cond_65
    const/4 v9, 0x0

    #@66
    .line 551
    .end local v5           #scale:F
    .end local v6           #asset:I
    :goto_66
    if-nez v7, :cond_b7

    #@68
    if-eqz p2, :cond_b7

    #@6a
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@6c
    if-eqz v0, :cond_b7

    #@6e
    .line 552
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@70
    const-string v2, "Problem decoding into existing bitmap"

    #@72
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@75
    throw v0

    #@76
    .line 499
    .end local v7           #bm:Landroid/graphics/Bitmap;
    .restart local p0
    :cond_76
    const/4 v11, 0x0

    #@77
    goto :goto_34

    #@78
    .line 521
    .end local p0
    .restart local v6       #asset:I
    :cond_78
    invoke-static {v6, p1, p2}, Landroid/graphics/BitmapFactory;->nativeDecodeAsset(ILandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@7b
    move-result-object v7

    #@7c
    .restart local v7       #bm:Landroid/graphics/Bitmap;
    goto :goto_66

    #@7d
    .line 528
    .end local v6           #asset:I
    .end local v7           #bm:Landroid/graphics/Bitmap;
    .restart local p0
    :cond_7d
    const/4 v1, 0x0

    #@7e
    .line 529
    .local v1, tempStorage:[B
    if-eqz p2, :cond_82

    #@80
    iget-object v1, p2, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    #@82
    .line 530
    :cond_82
    if-nez v1, :cond_86

    #@84
    new-array v1, v3, [B

    #@86
    .line 532
    :cond_86
    if-eqz p2, :cond_90

    #@88
    iget-boolean v0, p2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    #@8a
    if-eqz v0, :cond_b2

    #@8c
    iget-object v0, p2, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    #@8e
    if-nez v0, :cond_b2

    #@90
    .line 533
    :cond_90
    const/high16 v5, 0x3f80

    #@92
    .line 534
    .restart local v5       #scale:F
    if-eqz p2, :cond_a2

    #@94
    if-nez v11, :cond_a2

    #@96
    .line 535
    iget v8, p2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@98
    .line 536
    .restart local v8       #density:I
    iget v12, p2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@9a
    .line 537
    if-eqz v8, :cond_a2

    #@9c
    if-eqz v12, :cond_a2

    #@9e
    .line 538
    int-to-float v0, v12

    #@9f
    int-to-float v2, v8

    #@a0
    div-float v5, v0, v2

    #@a2
    .end local v8           #density:I
    :cond_a2
    move-object v0, p0

    #@a3
    move-object v2, p1

    #@a4
    move-object v3, p2

    #@a5
    .line 542
    invoke-static/range {v0 .. v5}, Landroid/graphics/BitmapFactory;->nativeDecodeStream(Ljava/io/InputStream;[BLandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;ZF)Landroid/graphics/Bitmap;

    #@a8
    move-result-object v7

    #@a9
    .line 543
    .restart local v7       #bm:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_b0

    #@ab
    if-eqz v12, :cond_b0

    #@ad
    invoke-virtual {v7, v12}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@b0
    .line 545
    :cond_b0
    const/4 v9, 0x0

    #@b1
    .line 546
    goto :goto_66

    #@b2
    .line 547
    .end local v5           #scale:F
    .end local v7           #bm:Landroid/graphics/Bitmap;
    :cond_b2
    invoke-static {p0, v1, p1, p2}, Landroid/graphics/BitmapFactory;->nativeDecodeStream(Ljava/io/InputStream;[BLandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@b5
    move-result-object v7

    #@b6
    .restart local v7       #bm:Landroid/graphics/Bitmap;
    goto :goto_66

    #@b7
    .line 555
    .end local v1           #tempStorage:[B
    .end local p0
    :cond_b7
    if-eqz v9, :cond_6

    #@b9
    invoke-static {v7, p1, p2}, Landroid/graphics/BitmapFactory;->finishDecode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@bc
    move-result-object v7

    #@bd
    goto/16 :goto_6
.end method

.method private static finishDecode(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 16
    .parameter "bm"
    .parameter "outPadding"
    .parameter "opts"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/high16 v12, 0x3f00

    #@3
    .line 559
    if-eqz p0, :cond_7

    #@5
    if-nez p2, :cond_8

    #@7
    .line 600
    :cond_7
    :goto_7
    return-object p0

    #@8
    .line 563
    :cond_8
    iget v0, p2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    #@a
    .line 564
    .local v0, density:I
    if-eqz v0, :cond_7

    #@c
    .line 568
    invoke-virtual {p0, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@f
    .line 569
    iget v8, p2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    #@11
    .line 570
    .local v8, targetDensity:I
    if-eqz v8, :cond_7

    #@13
    if-eq v0, v8, :cond_7

    #@15
    iget v10, p2, Landroid/graphics/BitmapFactory$Options;->inScreenDensity:I

    #@17
    if-eq v0, v10, :cond_7

    #@19
    .line 573
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    #@1c
    move-result-object v5

    #@1d
    .line 574
    .local v5, np:[B
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getLayoutBounds()[I

    #@20
    move-result-object v3

    #@21
    .line 575
    .local v3, lb:[I
    if-eqz v5, :cond_71

    #@23
    invoke-static {v5}, Landroid/graphics/NinePatch;->isNinePatchChunk([B)Z

    #@26
    move-result v10

    #@27
    if-eqz v10, :cond_71

    #@29
    move v2, v9

    #@2a
    .line 576
    .local v2, isNinePatch:Z
    :goto_2a
    iget-boolean v10, p2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    #@2c
    if-nez v10, :cond_30

    #@2e
    if-eqz v2, :cond_7

    #@30
    .line 577
    :cond_30
    int-to-float v10, v8

    #@31
    int-to-float v11, v0

    #@32
    div-float v7, v10, v11

    #@34
    .line 578
    .local v7, scale:F
    const/high16 v10, 0x3f80

    #@36
    cmpl-float v10, v7, v10

    #@38
    if-eqz v10, :cond_76

    #@3a
    .line 579
    move-object v6, p0

    #@3b
    .line 580
    .local v6, oldBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@3e
    move-result v10

    #@3f
    int-to-float v10, v10

    #@40
    mul-float/2addr v10, v7

    #@41
    add-float/2addr v10, v12

    #@42
    float-to-int v10, v10

    #@43
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@46
    move-result v11

    #@47
    int-to-float v11, v11

    #@48
    mul-float/2addr v11, v7

    #@49
    add-float/2addr v11, v12

    #@4a
    float-to-int v11, v11

    #@4b
    invoke-static {v6, v10, v11, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@4e
    move-result-object p0

    #@4f
    .line 582
    if-eq p0, v6, :cond_54

    #@51
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    #@54
    .line 584
    :cond_54
    if-eqz v2, :cond_5d

    #@56
    .line 585
    invoke-static {v5, v7, p1}, Landroid/graphics/BitmapFactory;->nativeScaleNinePatch([BFLandroid/graphics/Rect;)[B

    #@59
    move-result-object v5

    #@5a
    .line 586
    invoke-virtual {p0, v5}, Landroid/graphics/Bitmap;->setNinePatchChunk([B)V

    #@5d
    .line 588
    :cond_5d
    if-eqz v3, :cond_76

    #@5f
    .line 589
    array-length v9, v3

    #@60
    new-array v4, v9, [I

    #@62
    .line 590
    .local v4, newLb:[I
    const/4 v1, 0x0

    #@63
    .local v1, i:I
    :goto_63
    array-length v9, v3

    #@64
    if-ge v1, v9, :cond_73

    #@66
    .line 591
    aget v9, v3, v1

    #@68
    int-to-float v9, v9

    #@69
    mul-float/2addr v9, v7

    #@6a
    add-float/2addr v9, v12

    #@6b
    float-to-int v9, v9

    #@6c
    aput v9, v4, v1

    #@6e
    .line 590
    add-int/lit8 v1, v1, 0x1

    #@70
    goto :goto_63

    #@71
    .line 575
    .end local v1           #i:I
    .end local v2           #isNinePatch:Z
    .end local v4           #newLb:[I
    .end local v6           #oldBitmap:Landroid/graphics/Bitmap;
    .end local v7           #scale:F
    :cond_71
    const/4 v2, 0x0

    #@72
    goto :goto_2a

    #@73
    .line 593
    .restart local v1       #i:I
    .restart local v2       #isNinePatch:Z
    .restart local v4       #newLb:[I
    .restart local v6       #oldBitmap:Landroid/graphics/Bitmap;
    .restart local v7       #scale:F
    :cond_73
    invoke-virtual {p0, v4}, Landroid/graphics/Bitmap;->setLayoutBounds([I)V

    #@76
    .line 597
    .end local v1           #i:I
    .end local v4           #newLb:[I
    .end local v6           #oldBitmap:Landroid/graphics/Bitmap;
    :cond_76
    invoke-virtual {p0, v8}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@79
    goto :goto_7
.end method

.method private static native nativeDecodeAsset(ILandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeAsset(ILandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;ZF)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeStream(Ljava/io/InputStream;[BLandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeStream(Ljava/io/InputStream;[BLandroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;ZF)Landroid/graphics/Bitmap;
.end method

.method private static native nativeIsSeekable(Ljava/io/FileDescriptor;)Z
.end method

.method private static native nativeScaleNinePatch([BFLandroid/graphics/Rect;)[B
.end method
