.class public final Landroid/graphics/BitmapRegionDecoder;
.super Ljava/lang/Object;
.source "BitmapRegionDecoder.java"


# instance fields
.field private mNativeBitmapRegionDecoder:I

.field private final mNativeLock:Ljava/lang/Object;

.field private mRecycled:Z


# direct methods
.method private constructor <init>(I)V
    .registers 3
    .parameter "decoder"

    #@0
    .prologue
    .line 170
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 41
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeLock:Ljava/lang/Object;

    #@a
    .line 171
    iput p1, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeBitmapRegionDecoder:I

    #@c
    .line 172
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Landroid/graphics/BitmapRegionDecoder;->mRecycled:Z

    #@f
    .line 173
    return-void
.end method

.method private checkRecycled(Ljava/lang/String;)V
    .registers 3
    .parameter "errorMessage"

    #@0
    .prologue
    .line 245
    iget-boolean v0, p0, Landroid/graphics/BitmapRegionDecoder;->mRecycled:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 248
    :cond_a
    return-void
.end method

.method private static native nativeClean(I)V
.end method

.method private static native nativeDecodeRegion(IIIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeGetHeight(I)I
.end method

.method private static native nativeGetWidth(I)I
.end method

.method private static native nativeNewInstance(IZ)Landroid/graphics/BitmapRegionDecoder;
.end method

.method private static native nativeNewInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;
.end method

.method private static native nativeNewInstance(Ljava/io/InputStream;[BZ)Landroid/graphics/BitmapRegionDecoder;
.end method

.method private static native nativeNewInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;
.end method

.method public static newInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;
    .registers 3
    .parameter "fd"
    .parameter "isShareable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    invoke-static {p0, p1}, Landroid/graphics/BitmapRegionDecoder;->nativeNewInstance(Ljava/io/FileDescriptor;Z)Landroid/graphics/BitmapRegionDecoder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public static newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    .registers 6
    .parameter "is"
    .parameter "isShareable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v3, 0x4000

    #@2
    .line 113
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_e

    #@8
    .line 114
    new-instance v0, Ljava/io/BufferedInputStream;

    #@a
    invoke-direct {v0, p0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    #@d
    .end local p0
    .local v0, is:Ljava/io/InputStream;
    move-object p0, v0

    #@e
    .line 117
    .end local v0           #is:Ljava/io/InputStream;
    .restart local p0
    :cond_e
    instance-of v2, p0, Landroid/content/res/AssetManager$AssetInputStream;

    #@10
    if-eqz v2, :cond_1d

    #@12
    .line 118
    check-cast p0, Landroid/content/res/AssetManager$AssetInputStream;

    #@14
    .end local p0
    invoke-virtual {p0}, Landroid/content/res/AssetManager$AssetInputStream;->getAssetInt()I

    #@17
    move-result v2

    #@18
    invoke-static {v2, p1}, Landroid/graphics/BitmapRegionDecoder;->nativeNewInstance(IZ)Landroid/graphics/BitmapRegionDecoder;

    #@1b
    move-result-object v2

    #@1c
    .line 126
    :goto_1c
    return-object v2

    #@1d
    .line 125
    .restart local p0
    :cond_1d
    new-array v1, v3, [B

    #@1f
    .line 126
    .local v1, tempStorage:[B
    invoke-static {p0, v1, p1}, Landroid/graphics/BitmapRegionDecoder;->nativeNewInstance(Ljava/io/InputStream;[BZ)Landroid/graphics/BitmapRegionDecoder;

    #@22
    move-result-object v2

    #@23
    goto :goto_1c
.end method

.method public static newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;
    .registers 7
    .parameter "pathName"
    .parameter "isShareable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 147
    const/4 v0, 0x0

    #@1
    .line 148
    .local v0, decoder:Landroid/graphics/BitmapRegionDecoder;
    const/4 v1, 0x0

    #@2
    .line 151
    .local v1, stream:Ljava/io/InputStream;
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    #@4
    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_11

    #@7
    .line 152
    .end local v1           #stream:Ljava/io/InputStream;
    .local v2, stream:Ljava/io/InputStream;
    :try_start_7
    invoke-static {v2, p1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_1c

    #@a
    move-result-object v0

    #@b
    .line 154
    if-eqz v2, :cond_10

    #@d
    .line 156
    :try_start_d
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_18

    #@10
    .line 162
    :cond_10
    :goto_10
    return-object v0

    #@11
    .line 154
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v1       #stream:Ljava/io/InputStream;
    :catchall_11
    move-exception v3

    #@12
    :goto_12
    if-eqz v1, :cond_17

    #@14
    .line 156
    :try_start_14
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_17} :catch_1a

    #@17
    .line 159
    :cond_17
    :goto_17
    throw v3

    #@18
    .line 157
    .end local v1           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    :catch_18
    move-exception v3

    #@19
    goto :goto_10

    #@1a
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v1       #stream:Ljava/io/InputStream;
    :catch_1a
    move-exception v4

    #@1b
    goto :goto_17

    #@1c
    .line 154
    .end local v1           #stream:Ljava/io/InputStream;
    .restart local v2       #stream:Ljava/io/InputStream;
    :catchall_1c
    move-exception v3

    #@1d
    move-object v1, v2

    #@1e
    .end local v2           #stream:Ljava/io/InputStream;
    .restart local v1       #stream:Ljava/io/InputStream;
    goto :goto_12
.end method

.method public static newInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;
    .registers 6
    .parameter "data"
    .parameter "offset"
    .parameter "length"
    .parameter "isShareable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    or-int v0, p1, p2

    #@2
    if-ltz v0, :cond_9

    #@4
    array-length v0, p0

    #@5
    add-int v1, p1, p2

    #@7
    if-ge v0, v1, :cond_f

    #@9
    .line 64
    :cond_9
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v0

    #@f
    .line 66
    :cond_f
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapRegionDecoder;->nativeNewInstance([BIIZ)Landroid/graphics/BitmapRegionDecoder;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method


# virtual methods
.method public decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 10
    .parameter "rect"
    .parameter "options"

    #@0
    .prologue
    .line 185
    iget-object v6, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeLock:Ljava/lang/Object;

    #@2
    monitor-enter v6

    #@3
    .line 186
    :try_start_3
    const-string v0, "decodeRegion called on recycled region decoder"

    #@5
    invoke-direct {p0, v0}, Landroid/graphics/BitmapRegionDecoder;->checkRecycled(Ljava/lang/String;)V

    #@8
    .line 187
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@a
    if-lez v0, :cond_20

    #@c
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@e
    if-lez v0, :cond_20

    #@10
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@12
    invoke-virtual {p0}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    #@15
    move-result v1

    #@16
    if-ge v0, v1, :cond_20

    #@18
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@1a
    invoke-virtual {p0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    #@1d
    move-result v1

    #@1e
    if-lt v0, v1, :cond_2c

    #@20
    .line 189
    :cond_20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@22
    const-string/jumbo v1, "rectangle is outside the image"

    #@25
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v0

    #@29
    .line 192
    :catchall_29
    move-exception v0

    #@2a
    monitor-exit v6
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_29

    #@2b
    throw v0

    #@2c
    .line 190
    :cond_2c
    :try_start_2c
    iget v0, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeBitmapRegionDecoder:I

    #@2e
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@30
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@32
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@34
    iget v4, p1, Landroid/graphics/Rect;->left:I

    #@36
    sub-int/2addr v3, v4

    #@37
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@39
    iget v5, p1, Landroid/graphics/Rect;->top:I

    #@3b
    sub-int/2addr v4, v5

    #@3c
    move-object v5, p2

    #@3d
    invoke-static/range {v0 .. v5}, Landroid/graphics/BitmapRegionDecoder;->nativeDecodeRegion(IIIIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    #@40
    move-result-object v0

    #@41
    monitor-exit v6
    :try_end_42
    .catchall {:try_start_2c .. :try_end_42} :catchall_29

    #@42
    return-object v0
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 253
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 255
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 257
    return-void

    #@7
    .line 255
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public getHeight()I
    .registers 3

    #@0
    .prologue
    .line 205
    iget-object v1, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 206
    :try_start_3
    const-string v0, "getHeight called on recycled region decoder"

    #@5
    invoke-direct {p0, v0}, Landroid/graphics/BitmapRegionDecoder;->checkRecycled(Ljava/lang/String;)V

    #@8
    .line 207
    iget v0, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeBitmapRegionDecoder:I

    #@a
    invoke-static {v0}, Landroid/graphics/BitmapRegionDecoder;->nativeGetHeight(I)I

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 208
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public getWidth()I
    .registers 3

    #@0
    .prologue
    .line 197
    iget-object v1, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 198
    :try_start_3
    const-string v0, "getWidth called on recycled region decoder"

    #@5
    invoke-direct {p0, v0}, Landroid/graphics/BitmapRegionDecoder;->checkRecycled(Ljava/lang/String;)V

    #@8
    .line 199
    iget v0, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeBitmapRegionDecoder:I

    #@a
    invoke-static {v0}, Landroid/graphics/BitmapRegionDecoder;->nativeGetWidth(I)I

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 200
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method public final isRecycled()Z
    .registers 2

    #@0
    .prologue
    .line 237
    iget-boolean v0, p0, Landroid/graphics/BitmapRegionDecoder;->mRecycled:Z

    #@2
    return v0
.end method

.method public recycle()V
    .registers 3

    #@0
    .prologue
    .line 222
    iget-object v1, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 223
    :try_start_3
    iget-boolean v0, p0, Landroid/graphics/BitmapRegionDecoder;->mRecycled:Z

    #@5
    if-nez v0, :cond_f

    #@7
    .line 224
    iget v0, p0, Landroid/graphics/BitmapRegionDecoder;->mNativeBitmapRegionDecoder:I

    #@9
    invoke-static {v0}, Landroid/graphics/BitmapRegionDecoder;->nativeClean(I)V

    #@c
    .line 225
    const/4 v0, 0x1

    #@d
    iput-boolean v0, p0, Landroid/graphics/BitmapRegionDecoder;->mRecycled:Z

    #@f
    .line 227
    :cond_f
    monitor-exit v1

    #@10
    .line 228
    return-void

    #@11
    .line 227
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method
