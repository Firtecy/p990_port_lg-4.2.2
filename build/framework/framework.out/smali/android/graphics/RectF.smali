.class public Landroid/graphics/RectF;
.super Ljava/lang/Object;
.source "RectF.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bottom:F

.field public left:F

.field public right:F

.field public top:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 557
    new-instance v0, Landroid/graphics/RectF$1;

    #@2
    invoke-direct {v0}, Landroid/graphics/RectF$1;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/RectF;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public constructor <init>(FFFF)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 54
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 55
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@5
    .line 56
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@7
    .line 57
    iput p3, p0, Landroid/graphics/RectF;->right:F

    #@9
    .line 58
    iput p4, p0, Landroid/graphics/RectF;->bottom:F

    #@b
    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    if-nez p1, :cond_f

    #@5
    .line 81
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@8
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@a
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@c
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@e
    .line 88
    :goto_e
    return-void

    #@f
    .line 83
    :cond_f
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@11
    int-to-float v0, v0

    #@12
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@14
    .line 84
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@16
    int-to-float v0, v0

    #@17
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@19
    .line 85
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@1b
    int-to-float v0, v0

    #@1c
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@1e
    .line 86
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@20
    int-to-float v0, v0

    #@21
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@23
    goto :goto_e
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 69
    if-nez p1, :cond_f

    #@5
    .line 70
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@8
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@a
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@c
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@e
    .line 77
    :goto_e
    return-void

    #@f
    .line 72
    :cond_f
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@11
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@13
    .line 73
    iget v0, p1, Landroid/graphics/RectF;->top:F

    #@15
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@17
    .line 74
    iget v0, p1, Landroid/graphics/RectF;->right:F

    #@19
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@1b
    .line 75
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    #@1d
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@1f
    goto :goto_e
.end method

.method public static intersects(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .registers 4
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 435
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p1, Landroid/graphics/RectF;->right:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_22

    #@8
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_22

    #@10
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@12
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpg-float v0, v0, v1

    #@16
    if-gez v0, :cond_22

    #@18
    iget v0, p1, Landroid/graphics/RectF;->top:F

    #@1a
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@1c
    cmpg-float v0, v0, v1

    #@1e
    if-gez v0, :cond_22

    #@20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method


# virtual methods
.method public final centerX()F
    .registers 3

    #@0
    .prologue
    .line 170
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@4
    add-float/2addr v0, v1

    #@5
    const/high16 v1, 0x3f00

    #@7
    mul-float/2addr v0, v1

    #@8
    return v0
.end method

.method public final centerY()F
    .registers 3

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@4
    add-float/2addr v0, v1

    #@5
    const/high16 v1, 0x3f00

    #@7
    mul-float/2addr v0, v1

    #@8
    return v0
.end method

.method public contains(FF)Z
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 287
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_2a

    #@8
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_2a

    #@10
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@12
    cmpl-float v0, p1, v0

    #@14
    if-ltz v0, :cond_2a

    #@16
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@18
    cmpg-float v0, p1, v0

    #@1a
    if-gez v0, :cond_2a

    #@1c
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@1e
    cmpl-float v0, p2, v0

    #@20
    if-ltz v0, :cond_2a

    #@22
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@24
    cmpg-float v0, p2, v0

    #@26
    if-gez v0, :cond_2a

    #@28
    const/4 v0, 0x1

    #@29
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public contains(FFFF)Z
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 305
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_2a

    #@8
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_2a

    #@10
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@12
    cmpg-float v0, v0, p1

    #@14
    if-gtz v0, :cond_2a

    #@16
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@18
    cmpg-float v0, v0, p2

    #@1a
    if-gtz v0, :cond_2a

    #@1c
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@1e
    cmpl-float v0, v0, p3

    #@20
    if-ltz v0, :cond_2a

    #@22
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@24
    cmpl-float v0, v0, p4

    #@26
    if-ltz v0, :cond_2a

    #@28
    const/4 v0, 0x1

    #@29
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public contains(Landroid/graphics/RectF;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 321
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_32

    #@8
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_32

    #@10
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@12
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@14
    cmpg-float v0, v0, v1

    #@16
    if-gtz v0, :cond_32

    #@18
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@1a
    iget v1, p1, Landroid/graphics/RectF;->top:F

    #@1c
    cmpg-float v0, v0, v1

    #@1e
    if-gtz v0, :cond_32

    #@20
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@22
    iget v1, p1, Landroid/graphics/RectF;->right:F

    #@24
    cmpl-float v0, v0, v1

    #@26
    if-ltz v0, :cond_32

    #@28
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@2a
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    #@2c
    cmpl-float v0, v0, v1

    #@2e
    if-ltz v0, :cond_32

    #@30
    const/4 v0, 0x1

    #@31
    :goto_31
    return v0

    #@32
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_31
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 542
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 92
    if-ne p0, p1, :cond_5

    #@4
    .line 96
    :cond_4
    :goto_4
    return v1

    #@5
    .line 93
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 95
    check-cast v0, Landroid/graphics/RectF;

    #@16
    .line 96
    .local v0, r:Landroid/graphics/RectF;
    iget v3, p0, Landroid/graphics/RectF;->left:F

    #@18
    iget v4, v0, Landroid/graphics/RectF;->left:F

    #@1a
    cmpl-float v3, v3, v4

    #@1c
    if-nez v3, :cond_36

    #@1e
    iget v3, p0, Landroid/graphics/RectF;->top:F

    #@20
    iget v4, v0, Landroid/graphics/RectF;->top:F

    #@22
    cmpl-float v3, v3, v4

    #@24
    if-nez v3, :cond_36

    #@26
    iget v3, p0, Landroid/graphics/RectF;->right:F

    #@28
    iget v4, v0, Landroid/graphics/RectF;->right:F

    #@2a
    cmpl-float v3, v3, v4

    #@2c
    if-nez v3, :cond_36

    #@2e
    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    #@30
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    #@32
    cmpl-float v3, v3, v4

    #@34
    if-eqz v3, :cond_4

    #@36
    :cond_36
    move v1, v2

    #@37
    goto :goto_4
.end method

.method public hashCode()I
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 101
    iget v2, p0, Landroid/graphics/RectF;->left:F

    #@4
    cmpl-float v2, v2, v4

    #@6
    if-eqz v2, :cond_3f

    #@8
    iget v2, p0, Landroid/graphics/RectF;->left:F

    #@a
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@d
    move-result v0

    #@e
    .line 102
    .local v0, result:I
    :goto_e
    mul-int/lit8 v3, v0, 0x1f

    #@10
    iget v2, p0, Landroid/graphics/RectF;->top:F

    #@12
    cmpl-float v2, v2, v4

    #@14
    if-eqz v2, :cond_41

    #@16
    iget v2, p0, Landroid/graphics/RectF;->top:F

    #@18
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@1b
    move-result v2

    #@1c
    :goto_1c
    add-int v0, v3, v2

    #@1e
    .line 103
    mul-int/lit8 v3, v0, 0x1f

    #@20
    iget v2, p0, Landroid/graphics/RectF;->right:F

    #@22
    cmpl-float v2, v2, v4

    #@24
    if-eqz v2, :cond_43

    #@26
    iget v2, p0, Landroid/graphics/RectF;->right:F

    #@28
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@2b
    move-result v2

    #@2c
    :goto_2c
    add-int v0, v3, v2

    #@2e
    .line 104
    mul-int/lit8 v2, v0, 0x1f

    #@30
    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    #@32
    cmpl-float v3, v3, v4

    #@34
    if-eqz v3, :cond_3c

    #@36
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@38
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    #@3b
    move-result v1

    #@3c
    :cond_3c
    add-int v0, v2, v1

    #@3e
    .line 105
    return v0

    #@3f
    .end local v0           #result:I
    :cond_3f
    move v0, v1

    #@40
    .line 101
    goto :goto_e

    #@41
    .restart local v0       #result:I
    :cond_41
    move v2, v1

    #@42
    .line 102
    goto :goto_1c

    #@43
    :cond_43
    move v2, v1

    #@44
    .line 103
    goto :goto_2c
.end method

.method public final height()F
    .registers 3

    #@0
    .prologue
    .line 162
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@4
    sub-float/2addr v0, v1

    #@5
    return v0
.end method

.method public inset(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 269
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    add-float/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@5
    .line 270
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@7
    add-float/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    .line 271
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@c
    sub-float/2addr v0, p1

    #@d
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@f
    .line 272
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@11
    sub-float/2addr v0, p2

    #@12
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    .line 273
    return-void
.end method

.method public intersect(FFFF)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 346
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    cmpg-float v0, v0, p3

    #@4
    if-gez v0, :cond_3a

    #@6
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@8
    cmpg-float v0, p1, v0

    #@a
    if-gez v0, :cond_3a

    #@c
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@e
    cmpg-float v0, v0, p4

    #@10
    if-gez v0, :cond_3a

    #@12
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpg-float v0, p2, v0

    #@16
    if-gez v0, :cond_3a

    #@18
    .line 348
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@1a
    cmpg-float v0, v0, p1

    #@1c
    if-gez v0, :cond_20

    #@1e
    .line 349
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@20
    .line 351
    :cond_20
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@22
    cmpg-float v0, v0, p2

    #@24
    if-gez v0, :cond_28

    #@26
    .line 352
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@28
    .line 354
    :cond_28
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@2a
    cmpl-float v0, v0, p3

    #@2c
    if-lez v0, :cond_30

    #@2e
    .line 355
    iput p3, p0, Landroid/graphics/RectF;->right:F

    #@30
    .line 357
    :cond_30
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@32
    cmpl-float v0, v0, p4

    #@34
    if-lez v0, :cond_38

    #@36
    .line 358
    iput p4, p0, Landroid/graphics/RectF;->bottom:F

    #@38
    .line 360
    :cond_38
    const/4 v0, 0x1

    #@39
    .line 362
    :goto_39
    return v0

    #@3a
    :cond_3a
    const/4 v0, 0x0

    #@3b
    goto :goto_39
.end method

.method public intersect(Landroid/graphics/RectF;)Z
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 377
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p1, Landroid/graphics/RectF;->top:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->right:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/RectF;->intersect(FFFF)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public intersects(FFFF)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 420
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    cmpg-float v0, v0, p3

    #@4
    if-gez v0, :cond_1a

    #@6
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@8
    cmpg-float v0, p1, v0

    #@a
    if-gez v0, :cond_1a

    #@c
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@e
    cmpg-float v0, v0, p4

    #@10
    if-gez v0, :cond_1a

    #@12
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpg-float v0, p2, v0

    #@16
    if-gez v0, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 3

    #@0
    .prologue
    .line 146
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@4
    cmpl-float v0, v0, v1

    #@6
    if-gez v0, :cond_10

    #@8
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@c
    cmpl-float v0, v0, v1

    #@e
    if-ltz v0, :cond_12

    #@10
    :cond_10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public offset(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 239
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    add-float/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@5
    .line 240
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@7
    add-float/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    .line 241
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@c
    add-float/2addr v0, p1

    #@d
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@f
    .line 242
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@11
    add-float/2addr v0, p2

    #@12
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    .line 243
    return-void
.end method

.method public offsetTo(FF)V
    .registers 5
    .parameter "newLeft"
    .parameter "newTop"

    #@0
    .prologue
    .line 253
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->left:F

    #@4
    sub-float v1, p1, v1

    #@6
    add-float/2addr v0, v1

    #@7
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@9
    .line 254
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@b
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@d
    sub-float v1, p2, v1

    #@f
    add-float/2addr v0, v1

    #@10
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@12
    .line 255
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@14
    .line 256
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@16
    .line 257
    return-void
.end method

.method public printShortString(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    const/16 v1, 0x2c

    #@2
    .line 137
    const/16 v0, 0x5b

    #@4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@7
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@f
    .line 138
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@11
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@14
    const-string v0, "]["

    #@16
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@1b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@1e
    .line 139
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@21
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@23
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(F)V

    #@26
    const/16 v0, 0x5d

    #@28
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@2b
    .line 140
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 582
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@6
    .line 583
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@c
    .line 584
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@12
    .line 585
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@18
    .line 586
    return-void
.end method

.method public round(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "dst"

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    invoke-static {v0}, Lcom/android/internal/util/FastMath;->round(F)I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@8
    invoke-static {v1}, Lcom/android/internal/util/FastMath;->round(F)I

    #@b
    move-result v1

    #@c
    iget v2, p0, Landroid/graphics/RectF;->right:F

    #@e
    invoke-static {v2}, Lcom/android/internal/util/FastMath;->round(F)I

    #@11
    move-result v2

    #@12
    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    invoke-static {v3}, Lcom/android/internal/util/FastMath;->round(F)I

    #@17
    move-result v3

    #@18
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@1b
    .line 446
    return-void
.end method

.method public roundOut(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "dst"

    #@0
    .prologue
    .line 453
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    #@5
    move-result v0

    #@6
    float-to-int v0, v0

    #@7
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@9
    invoke-static {v1}, Landroid/util/FloatMath;->floor(F)F

    #@c
    move-result v1

    #@d
    float-to-int v1, v1

    #@e
    iget v2, p0, Landroid/graphics/RectF;->right:F

    #@10
    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    #@13
    move-result v2

    #@14
    float-to-int v2, v2

    #@15
    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    #@17
    invoke-static {v3}, Landroid/util/FloatMath;->ceil(F)F

    #@1a
    move-result v3

    #@1b
    float-to-int v3, v3

    #@1c
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    #@1f
    .line 455
    return-void
.end method

.method public set(FFFF)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 199
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@2
    .line 200
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@4
    .line 201
    iput p3, p0, Landroid/graphics/RectF;->right:F

    #@6
    .line 202
    iput p4, p0, Landroid/graphics/RectF;->bottom:F

    #@8
    .line 203
    return-void
.end method

.method public set(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 225
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    int-to-float v0, v0

    #@3
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@5
    .line 226
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@7
    int-to-float v0, v0

    #@8
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    .line 227
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@c
    int-to-float v0, v0

    #@d
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@f
    .line 228
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@11
    int-to-float v0, v0

    #@12
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    .line 229
    return-void
.end method

.method public set(Landroid/graphics/RectF;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 212
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@2
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@4
    .line 213
    iget v0, p1, Landroid/graphics/RectF;->top:F

    #@6
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@8
    .line 214
    iget v0, p1, Landroid/graphics/RectF;->right:F

    #@a
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@c
    .line 215
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    #@e
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@10
    .line 216
    return-void
.end method

.method public setEmpty()V
    .registers 2

    #@0
    .prologue
    .line 185
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@3
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@5
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@7
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@9
    .line 186
    return-void
.end method

.method public setIntersect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .registers 5
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 393
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p2, Landroid/graphics/RectF;->right:F

    #@4
    cmpg-float v0, v0, v1

    #@6
    if-gez v0, :cond_4a

    #@8
    iget v0, p2, Landroid/graphics/RectF;->left:F

    #@a
    iget v1, p1, Landroid/graphics/RectF;->right:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_4a

    #@10
    iget v0, p1, Landroid/graphics/RectF;->top:F

    #@12
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpg-float v0, v0, v1

    #@16
    if-gez v0, :cond_4a

    #@18
    iget v0, p2, Landroid/graphics/RectF;->top:F

    #@1a
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    #@1c
    cmpg-float v0, v0, v1

    #@1e
    if-gez v0, :cond_4a

    #@20
    .line 395
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@22
    iget v1, p2, Landroid/graphics/RectF;->left:F

    #@24
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@27
    move-result v0

    #@28
    iput v0, p0, Landroid/graphics/RectF;->left:F

    #@2a
    .line 396
    iget v0, p1, Landroid/graphics/RectF;->top:F

    #@2c
    iget v1, p2, Landroid/graphics/RectF;->top:F

    #@2e
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    #@31
    move-result v0

    #@32
    iput v0, p0, Landroid/graphics/RectF;->top:F

    #@34
    .line 397
    iget v0, p1, Landroid/graphics/RectF;->right:F

    #@36
    iget v1, p2, Landroid/graphics/RectF;->right:F

    #@38
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@3b
    move-result v0

    #@3c
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@3e
    .line 398
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    #@40
    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    #@42
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    #@45
    move-result v0

    #@46
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@48
    .line 399
    const/4 v0, 0x1

    #@49
    .line 401
    :goto_49
    return v0

    #@4a
    :cond_4a
    const/4 v0, 0x0

    #@4b
    goto :goto_49
.end method

.method public sort()V
    .registers 4

    #@0
    .prologue
    .line 526
    iget v1, p0, Landroid/graphics/RectF;->left:F

    #@2
    iget v2, p0, Landroid/graphics/RectF;->right:F

    #@4
    cmpl-float v1, v1, v2

    #@6
    if-lez v1, :cond_10

    #@8
    .line 527
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@a
    .line 528
    .local v0, temp:F
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@c
    iput v1, p0, Landroid/graphics/RectF;->left:F

    #@e
    .line 529
    iput v0, p0, Landroid/graphics/RectF;->right:F

    #@10
    .line 531
    .end local v0           #temp:F
    :cond_10
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@12
    iget v2, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpl-float v1, v1, v2

    #@16
    if-lez v1, :cond_20

    #@18
    .line 532
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@1a
    .line 533
    .restart local v0       #temp:F
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@1c
    iput v1, p0, Landroid/graphics/RectF;->top:F

    #@1e
    .line 534
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    #@20
    .line 536
    .end local v0           #temp:F
    :cond_20
    return-void
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x20

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    invoke-virtual {p0, v0}, Landroid/graphics/RectF;->toShortString(Ljava/lang/StringBuilder;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public toShortString(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .registers 4
    .parameter "sb"

    #@0
    .prologue
    const/16 v1, 0x2c

    #@2
    .line 125
    const/4 v0, 0x0

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    #@6
    .line 126
    const/16 v0, 0x5b

    #@8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@13
    .line 127
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@18
    const-string v0, "]["

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@1f
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@22
    .line 128
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@27
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@2a
    const/16 v0, 0x5d

    #@2c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2f
    .line 129
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "RectF("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/graphics/RectF;->left:F

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/graphics/RectF;->top:F

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", "

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, ")"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    return-object v0
.end method

.method public union(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 506
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    cmpg-float v0, p1, v0

    #@4
    if-gez v0, :cond_11

    #@6
    .line 507
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@8
    .line 511
    :cond_8
    :goto_8
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@a
    cmpg-float v0, p2, v0

    #@c
    if-gez v0, :cond_1a

    #@e
    .line 512
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@10
    .line 516
    :cond_10
    :goto_10
    return-void

    #@11
    .line 508
    :cond_11
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@13
    cmpl-float v0, p1, v0

    #@15
    if-lez v0, :cond_8

    #@17
    .line 509
    iput p1, p0, Landroid/graphics/RectF;->right:F

    #@19
    goto :goto_8

    #@1a
    .line 513
    :cond_1a
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@1c
    cmpl-float v0, p2, v0

    #@1e
    if-lez v0, :cond_10

    #@20
    .line 514
    iput p2, p0, Landroid/graphics/RectF;->bottom:F

    #@22
    goto :goto_10
.end method

.method public union(FFFF)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 468
    cmpg-float v0, p1, p3

    #@2
    if-gez v0, :cond_38

    #@4
    cmpg-float v0, p2, p4

    #@6
    if-gez v0, :cond_38

    #@8
    .line 469
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@a
    iget v1, p0, Landroid/graphics/RectF;->right:F

    #@c
    cmpg-float v0, v0, v1

    #@e
    if-gez v0, :cond_39

    #@10
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@12
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    #@14
    cmpg-float v0, v0, v1

    #@16
    if-gez v0, :cond_39

    #@18
    .line 470
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@1a
    cmpl-float v0, v0, p1

    #@1c
    if-lez v0, :cond_20

    #@1e
    .line 471
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@20
    .line 472
    :cond_20
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@22
    cmpl-float v0, v0, p2

    #@24
    if-lez v0, :cond_28

    #@26
    .line 473
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@28
    .line 474
    :cond_28
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@2a
    cmpg-float v0, v0, p3

    #@2c
    if-gez v0, :cond_30

    #@2e
    .line 475
    iput p3, p0, Landroid/graphics/RectF;->right:F

    #@30
    .line 476
    :cond_30
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@32
    cmpg-float v0, v0, p4

    #@34
    if-gez v0, :cond_38

    #@36
    .line 477
    iput p4, p0, Landroid/graphics/RectF;->bottom:F

    #@38
    .line 485
    :cond_38
    :goto_38
    return-void

    #@39
    .line 479
    :cond_39
    iput p1, p0, Landroid/graphics/RectF;->left:F

    #@3b
    .line 480
    iput p2, p0, Landroid/graphics/RectF;->top:F

    #@3d
    .line 481
    iput p3, p0, Landroid/graphics/RectF;->right:F

    #@3f
    .line 482
    iput p4, p0, Landroid/graphics/RectF;->bottom:F

    #@41
    goto :goto_38
.end method

.method public union(Landroid/graphics/RectF;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 495
    iget v0, p1, Landroid/graphics/RectF;->left:F

    #@2
    iget v1, p1, Landroid/graphics/RectF;->top:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->right:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/RectF;->union(FFFF)V

    #@b
    .line 496
    return-void
.end method

.method public final width()F
    .registers 3

    #@0
    .prologue
    .line 154
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@2
    iget v1, p0, Landroid/graphics/RectF;->left:F

    #@4
    sub-float/2addr v0, v1

    #@5
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 551
    iget v0, p0, Landroid/graphics/RectF;->left:F

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@5
    .line 552
    iget v0, p0, Landroid/graphics/RectF;->top:F

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@a
    .line 553
    iget v0, p0, Landroid/graphics/RectF;->right:F

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@f
    .line 554
    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@14
    .line 555
    return-void
.end method
