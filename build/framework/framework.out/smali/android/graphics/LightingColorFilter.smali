.class public Landroid/graphics/LightingColorFilter;
.super Landroid/graphics/ColorFilter;
.source "LightingColorFilter.java"


# direct methods
.method public constructor <init>(II)V
    .registers 4
    .parameter "mul"
    .parameter "add"

    #@0
    .prologue
    .line 31
    invoke-direct {p0}, Landroid/graphics/ColorFilter;-><init>()V

    #@3
    .line 32
    invoke-static {p1, p2}, Landroid/graphics/LightingColorFilter;->native_CreateLightingFilter(II)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@9
    .line 33
    iget v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@b
    invoke-static {v0, p1, p2}, Landroid/graphics/LightingColorFilter;->nCreateLightingFilter(III)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@11
    .line 34
    return-void
.end method

.method private static native nCreateLightingFilter(III)I
.end method

.method private static native native_CreateLightingFilter(II)I
.end method
