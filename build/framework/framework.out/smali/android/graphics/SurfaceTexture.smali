.class public Landroid/graphics/SurfaceTexture;
.super Ljava/lang/Object;
.source "SurfaceTexture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/SurfaceTexture$EventHandler;,
        Landroid/graphics/SurfaceTexture$OutOfResourcesException;,
        Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
    }
.end annotation


# instance fields
.field private mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

.field private mOnFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

.field private mSurfaceTexture:I


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 317
    invoke-static {}, Landroid/graphics/SurfaceTexture;->nativeClassInit()V

    #@3
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "texName"

    #@0
    .prologue
    .line 97
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Landroid/graphics/SurfaceTexture;-><init>(IZ)V

    #@4
    .line 98
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 5
    .parameter "texName"
    .parameter "allowSynchronousMode"

    #@0
    .prologue
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 113
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@6
    move-result-object v0

    #@7
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_19

    #@9
    .line 114
    new-instance v1, Landroid/graphics/SurfaceTexture$EventHandler;

    #@b
    invoke-direct {v1, p0, v0}, Landroid/graphics/SurfaceTexture$EventHandler;-><init>(Landroid/graphics/SurfaceTexture;Landroid/os/Looper;)V

    #@e
    iput-object v1, p0, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@10
    .line 120
    :goto_10
    new-instance v1, Ljava/lang/ref/WeakReference;

    #@12
    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@15
    invoke-direct {p0, p1, v1, p2}, Landroid/graphics/SurfaceTexture;->nativeInit(ILjava/lang/Object;Z)V

    #@18
    .line 121
    return-void

    #@19
    .line 115
    :cond_19
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    #@1c
    move-result-object v0

    #@1d
    if-eqz v0, :cond_27

    #@1f
    .line 116
    new-instance v1, Landroid/graphics/SurfaceTexture$EventHandler;

    #@21
    invoke-direct {v1, p0, v0}, Landroid/graphics/SurfaceTexture$EventHandler;-><init>(Landroid/graphics/SurfaceTexture;Landroid/os/Looper;)V

    #@24
    iput-object v1, p0, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@26
    goto :goto_10

    #@27
    .line 118
    :cond_27
    const/4 v1, 0x0

    #@28
    iput-object v1, p0, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@2a
    goto :goto_10
.end method

.method static synthetic access$000(Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Landroid/graphics/SurfaceTexture;->mOnFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@2
    return-object v0
.end method

.method private native nativeAttachToGLContext(I)I
.end method

.method private static native nativeClassInit()V
.end method

.method private native nativeDetachFromGLContext()I
.end method

.method private native nativeFinalize()V
.end method

.method private native nativeGetQueuedCount()I
.end method

.method private native nativeGetTimestamp()J
.end method

.method private native nativeGetTransformMatrix([F)V
.end method

.method private native nativeInit(ILjava/lang/Object;Z)V
.end method

.method private native nativeRelease()V
.end method

.method private native nativeSetDefaultBufferSize(II)V
.end method

.method private native nativeUpdateTexImage()V
.end method

.method private static postEventFromNative(Ljava/lang/Object;)V
    .registers 5
    .parameter "selfRef"

    #@0
    .prologue
    .line 289
    move-object v2, p0

    #@1
    check-cast v2, Ljava/lang/ref/WeakReference;

    #@3
    .line 290
    .local v2, weakSelf:Ljava/lang/ref/WeakReference;
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Landroid/graphics/SurfaceTexture;

    #@9
    .line 291
    .local v1, st:Landroid/graphics/SurfaceTexture;
    if-nez v1, :cond_c

    #@b
    .line 299
    :cond_b
    :goto_b
    return-void

    #@c
    .line 295
    :cond_c
    iget-object v3, v1, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@e
    if-eqz v3, :cond_b

    #@10
    .line 296
    iget-object v3, v1, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@12
    invoke-virtual {v3}, Landroid/graphics/SurfaceTexture$EventHandler;->obtainMessage()Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    .line 297
    .local v0, m:Landroid/os/Message;
    iget-object v3, v1, Landroid/graphics/SurfaceTexture;->mEventHandler:Landroid/graphics/SurfaceTexture$EventHandler;

    #@18
    invoke-virtual {v3, v0}, Landroid/graphics/SurfaceTexture$EventHandler;->sendMessage(Landroid/os/Message;)Z

    #@1b
    goto :goto_b
.end method


# virtual methods
.method public attachToGLContext(I)V
    .registers 5
    .parameter "texName"

    #@0
    .prologue
    .line 197
    invoke-direct {p0, p1}, Landroid/graphics/SurfaceTexture;->nativeAttachToGLContext(I)I

    #@3
    move-result v0

    #@4
    .line 198
    .local v0, err:I
    if-eqz v0, :cond_e

    #@6
    .line 199
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    const-string v2, "Error during detachFromGLContext (see logcat for details)"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 201
    :cond_e
    return-void
.end method

.method public detachFromGLContext()V
    .registers 4

    #@0
    .prologue
    .line 177
    invoke-direct {p0}, Landroid/graphics/SurfaceTexture;->nativeDetachFromGLContext()I

    #@3
    move-result v0

    #@4
    .line 178
    .local v0, err:I
    if-eqz v0, :cond_e

    #@6
    .line 179
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    const-string v2, "Error during detachFromGLContext (see logcat for details)"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 181
    :cond_e
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 265
    :try_start_0
    invoke-direct {p0}, Landroid/graphics/SurfaceTexture;->nativeFinalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    #@3
    .line 267
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@6
    .line 269
    return-void

    #@7
    .line 267
    :catchall_7
    move-exception v0

    #@8
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@b
    throw v0
.end method

.method public getTimestamp()J
    .registers 3

    #@0
    .prologue
    .line 241
    invoke-direct {p0}, Landroid/graphics/SurfaceTexture;->nativeGetTimestamp()J

    #@3
    move-result-wide v0

    #@4
    return-wide v0
.end method

.method public getTransformMatrix([F)V
    .registers 4
    .parameter "mtx"

    #@0
    .prologue
    .line 221
    array-length v0, p1

    #@1
    const/16 v1, 0x10

    #@3
    if-eq v0, v1, :cond_b

    #@5
    .line 222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@a
    throw v0

    #@b
    .line 224
    :cond_b
    invoke-direct {p0, p1}, Landroid/graphics/SurfaceTexture;->nativeGetTransformMatrix([F)V

    #@e
    .line 225
    return-void
.end method

.method public release()V
    .registers 1

    #@0
    .prologue
    .line 260
    invoke-direct {p0}, Landroid/graphics/SurfaceTexture;->nativeRelease()V

    #@3
    .line 261
    return-void
.end method

.method public setDefaultBufferSize(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Landroid/graphics/SurfaceTexture;->nativeSetDefaultBufferSize(II)V

    #@3
    .line 154
    return-void
.end method

.method public setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V
    .registers 2
    .parameter "l"

    #@0
    .prologue
    .line 130
    iput-object p1, p0, Landroid/graphics/SurfaceTexture;->mOnFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    #@2
    .line 131
    return-void
.end method

.method public updateTexImage()V
    .registers 1

    #@0
    .prologue
    .line 162
    invoke-direct {p0}, Landroid/graphics/SurfaceTexture;->nativeUpdateTexImage()V

    #@3
    .line 163
    return-void
.end method
