.class public Landroid/graphics/TemporaryBuffer;
.super Ljava/lang/Object;
.source "TemporaryBuffer.java"


# static fields
.field private static sTemp:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 48
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/graphics/TemporaryBuffer;->sTemp:[C

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static obtain(I)[C
    .registers 4
    .parameter "len"

    #@0
    .prologue
    .line 28
    const-class v2, Landroid/graphics/TemporaryBuffer;

    #@2
    monitor-enter v2

    #@3
    .line 29
    :try_start_3
    sget-object v0, Landroid/graphics/TemporaryBuffer;->sTemp:[C

    #@5
    .line 30
    .local v0, buf:[C
    const/4 v1, 0x0

    #@6
    sput-object v1, Landroid/graphics/TemporaryBuffer;->sTemp:[C

    #@8
    .line 31
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_15

    #@9
    .line 33
    if-eqz v0, :cond_e

    #@b
    array-length v1, v0

    #@c
    if-ge v1, p0, :cond_14

    #@e
    .line 34
    :cond_e
    invoke-static {p0}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@11
    move-result v1

    #@12
    new-array v0, v1, [C

    #@14
    .line 37
    :cond_14
    return-object v0

    #@15
    .line 31
    .end local v0           #buf:[C
    :catchall_15
    move-exception v1

    #@16
    :try_start_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method public static recycle([C)V
    .registers 3
    .parameter "temp"

    #@0
    .prologue
    .line 41
    array-length v0, p0

    #@1
    const/16 v1, 0x3e8

    #@3
    if-le v0, v1, :cond_6

    #@5
    .line 46
    :goto_5
    return-void

    #@6
    .line 43
    :cond_6
    const-class v1, Landroid/graphics/TemporaryBuffer;

    #@8
    monitor-enter v1

    #@9
    .line 44
    :try_start_9
    sput-object p0, Landroid/graphics/TemporaryBuffer;->sTemp:[C

    #@b
    .line 45
    monitor-exit v1

    #@c
    goto :goto_5

    #@d
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method
