.class Landroid/graphics/Bitmap$BitmapFinalizer;
.super Ljava/lang/Object;
.source "Bitmap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BitmapFinalizer"
.end annotation


# instance fields
.field private final mNativeBitmap:I


# direct methods
.method constructor <init>(I)V
    .registers 2
    .parameter "nativeBitmap"

    #@0
    .prologue
    .line 1375
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1376
    iput p1, p0, Landroid/graphics/Bitmap$BitmapFinalizer;->mNativeBitmap:I

    #@5
    .line 1377
    return-void
.end method


# virtual methods
.method public finalize()V
    .registers 3

    #@0
    .prologue
    .line 1382
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_10
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_3} :catch_9

    #@3
    .line 1386
    iget v0, p0, Landroid/graphics/Bitmap$BitmapFinalizer;->mNativeBitmap:I

    #@5
    invoke-static {v0}, Landroid/graphics/Bitmap;->access$100(I)V

    #@8
    .line 1388
    :goto_8
    return-void

    #@9
    .line 1383
    :catch_9
    move-exception v0

    #@a
    .line 1386
    iget v0, p0, Landroid/graphics/Bitmap$BitmapFinalizer;->mNativeBitmap:I

    #@c
    invoke-static {v0}, Landroid/graphics/Bitmap;->access$100(I)V

    #@f
    goto :goto_8

    #@10
    :catchall_10
    move-exception v0

    #@11
    iget v1, p0, Landroid/graphics/Bitmap$BitmapFinalizer;->mNativeBitmap:I

    #@13
    invoke-static {v1}, Landroid/graphics/Bitmap;->access$100(I)V

    #@16
    throw v0
.end method
