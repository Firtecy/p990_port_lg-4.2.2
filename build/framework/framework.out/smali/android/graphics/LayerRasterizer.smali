.class public Landroid/graphics/LayerRasterizer;
.super Landroid/graphics/Rasterizer;
.source "LayerRasterizer.java"


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/graphics/Rasterizer;-><init>()V

    #@3
    .line 21
    invoke-static {}, Landroid/graphics/LayerRasterizer;->nativeConstructor()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/graphics/Rasterizer;->native_instance:I

    #@9
    .line 22
    return-void
.end method

.method private static native nativeAddLayer(IIFF)V
.end method

.method private static native nativeConstructor()I
.end method


# virtual methods
.method public addLayer(Landroid/graphics/Paint;)V
    .registers 5
    .parameter "paint"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 34
    iget v0, p0, Landroid/graphics/Rasterizer;->native_instance:I

    #@3
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@5
    invoke-static {v0, v1, v2, v2}, Landroid/graphics/LayerRasterizer;->nativeAddLayer(IIFF)V

    #@8
    .line 35
    return-void
.end method

.method public addLayer(Landroid/graphics/Paint;FF)V
    .registers 6
    .parameter "paint"
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 30
    iget v0, p0, Landroid/graphics/Rasterizer;->native_instance:I

    #@2
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v0, v1, p2, p3}, Landroid/graphics/LayerRasterizer;->nativeAddLayer(IIFF)V

    #@7
    .line 31
    return-void
.end method
