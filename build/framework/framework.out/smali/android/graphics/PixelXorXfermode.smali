.class public Landroid/graphics/PixelXorXfermode;
.super Landroid/graphics/Xfermode;
.source "PixelXorXfermode.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "opColor"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/graphics/Xfermode;-><init>()V

    #@3
    .line 29
    invoke-static {p1}, Landroid/graphics/PixelXorXfermode;->nativeCreate(I)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/graphics/Xfermode;->native_instance:I

    #@9
    .line 30
    return-void
.end method

.method private static native nativeCreate(I)I
.end method
