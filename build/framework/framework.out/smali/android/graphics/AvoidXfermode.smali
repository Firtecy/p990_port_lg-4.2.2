.class public Landroid/graphics/AvoidXfermode;
.super Landroid/graphics/Xfermode;
.source "AvoidXfermode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/AvoidXfermode$Mode;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(IILandroid/graphics/AvoidXfermode$Mode;)V
    .registers 6
    .parameter "opColor"
    .parameter "tolerance"
    .parameter "mode"

    #@0
    .prologue
    .line 52
    invoke-direct {p0}, Landroid/graphics/Xfermode;-><init>()V

    #@3
    .line 53
    if-ltz p2, :cond_9

    #@5
    const/16 v0, 0xff

    #@7
    if-le p2, v0, :cond_12

    #@9
    .line 54
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string/jumbo v1, "tolerance must be 0..255"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 56
    :cond_12
    iget v0, p3, Landroid/graphics/AvoidXfermode$Mode;->nativeInt:I

    #@14
    invoke-static {p1, p2, v0}, Landroid/graphics/AvoidXfermode;->nativeCreate(III)I

    #@17
    move-result v0

    #@18
    iput v0, p0, Landroid/graphics/Xfermode;->native_instance:I

    #@1a
    .line 57
    return-void
.end method

.method private static native nativeCreate(III)I
.end method
