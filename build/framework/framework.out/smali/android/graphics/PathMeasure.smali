.class public Landroid/graphics/PathMeasure;
.super Ljava/lang/Object;
.source "PathMeasure.java"


# static fields
.field public static final POSITION_MATRIX_FLAG:I = 0x1

.field public static final TANGENT_MATRIX_FLAG:I = 0x2


# instance fields
.field private mPath:Landroid/graphics/Path;

.field private final native_instance:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 32
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Landroid/graphics/PathMeasure;->mPath:Landroid/graphics/Path;

    #@7
    .line 33
    invoke-static {v1, v1}, Landroid/graphics/PathMeasure;->native_create(IZ)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@d
    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Path;Z)V
    .registers 4
    .parameter "path"
    .parameter "forceClosed"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 52
    iput-object p1, p0, Landroid/graphics/PathMeasure;->mPath:Landroid/graphics/Path;

    #@5
    .line 53
    if-eqz p1, :cond_12

    #@7
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@a
    move-result v0

    #@b
    :goto_b
    invoke-static {v0, p2}, Landroid/graphics/PathMeasure;->native_create(IZ)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@11
    .line 55
    return-void

    #@12
    .line 53
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_b
.end method

.method private static native native_create(IZ)I
.end method

.method private static native native_destroy(I)V
.end method

.method private static native native_getLength(I)F
.end method

.method private static native native_getMatrix(IFII)Z
.end method

.method private static native native_getPosTan(IF[F[F)Z
.end method

.method private static native native_getSegment(IFFIZ)Z
.end method

.method private static native native_isClosed(I)Z
.end method

.method private static native native_nextContour(I)Z
.end method

.method private static native native_setPath(IIZ)V
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 138
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/PathMeasure;->native_destroy(I)V

    #@5
    .line 139
    return-void
.end method

.method public getLength()F
    .registers 2

    #@0
    .prologue
    .line 72
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/PathMeasure;->native_getLength(I)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMatrix(FLandroid/graphics/Matrix;I)Z
    .registers 6
    .parameter "distance"
    .parameter "matrix"
    .parameter "flags"

    #@0
    .prologue
    .line 108
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    iget v1, p2, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, p1, v1, p3}, Landroid/graphics/PathMeasure;->native_getMatrix(IFII)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public getPosTan(F[F[F)Z
    .registers 6
    .parameter "distance"
    .parameter "pos"
    .parameter "tan"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 87
    if-eqz p2, :cond_6

    #@3
    array-length v0, p2

    #@4
    if-lt v0, v1, :cond_b

    #@6
    :cond_6
    if-eqz p3, :cond_11

    #@8
    array-length v0, p3

    #@9
    if-ge v0, v1, :cond_11

    #@b
    .line 89
    :cond_b
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@d
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@10
    throw v0

    #@11
    .line 91
    :cond_11
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@13
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/PathMeasure;->native_getPosTan(IF[F[F)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public getSegment(FFLandroid/graphics/Path;Z)Z
    .registers 7
    .parameter "startD"
    .parameter "stopD"
    .parameter "dst"
    .parameter "startWithMoveTo"

    #@0
    .prologue
    .line 119
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    invoke-virtual {p3}, Landroid/graphics/Path;->ni()I

    #@5
    move-result v1

    #@6
    invoke-static {v0, p1, p2, v1, p4}, Landroid/graphics/PathMeasure;->native_getSegment(IFFIZ)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public isClosed()Z
    .registers 2

    #@0
    .prologue
    .line 126
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/PathMeasure;->native_isClosed(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public nextContour()Z
    .registers 2

    #@0
    .prologue
    .line 134
    iget v0, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/PathMeasure;->native_nextContour(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public setPath(Landroid/graphics/Path;Z)V
    .registers 5
    .parameter "path"
    .parameter "forceClosed"

    #@0
    .prologue
    .line 61
    iput-object p1, p0, Landroid/graphics/PathMeasure;->mPath:Landroid/graphics/Path;

    #@2
    .line 62
    iget v1, p0, Landroid/graphics/PathMeasure;->native_instance:I

    #@4
    if-eqz p1, :cond_e

    #@6
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@9
    move-result v0

    #@a
    :goto_a
    invoke-static {v1, v0, p2}, Landroid/graphics/PathMeasure;->native_setPath(IIZ)V

    #@d
    .line 65
    return-void

    #@e
    .line 62
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_a
.end method
