.class public final Landroid/graphics/Bitmap;
.super Ljava/lang/Object;
.source "Bitmap.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Bitmap$2;,
        Landroid/graphics/Bitmap$BitmapFinalizer;,
        Landroid/graphics/Bitmap$CompressFormat;,
        Landroid/graphics/Bitmap$Config;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public static final DENSITY_NONE:I = 0x0

.field private static final WORKING_COMPRESS_STORAGE:I = 0x1000

.field private static volatile sDefaultDensity:I

.field private static volatile sScaleMatrix:Landroid/graphics/Matrix;


# instance fields
.field public mBuffer:[B

.field mDensity:I

.field private final mFinalizer:Landroid/graphics/Bitmap$BitmapFinalizer;

.field private mHeight:I

.field private final mIsMutable:Z

.field private mLayoutBounds:[I

.field public final mNativeBitmap:I

.field private mNinePatchChunk:[B

.field private mRecycled:Z

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 70
    const/4 v0, -0x1

    #@1
    sput v0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@3
    .line 1262
    new-instance v0, Landroid/graphics/Bitmap$1;

    #@5
    invoke-direct {v0}, Landroid/graphics/Bitmap$1;-><init>()V

    #@8
    sput-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    #@a
    return-void
.end method

.method constructor <init>(I[BZ[BI)V
    .registers 13
    .parameter "nativeBitmap"
    .parameter "buffer"
    .parameter "isMutable"
    .parameter "ninePatchChunk"
    .parameter "density"

    #@0
    .prologue
    .line 99
    const/4 v5, 0x0

    #@1
    move-object v0, p0

    #@2
    move v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move-object v4, p4

    #@6
    move v6, p5

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/graphics/Bitmap;-><init>(I[BZ[B[II)V

    #@a
    .line 100
    return-void
.end method

.method constructor <init>(I[BZ[B[II)V
    .registers 9
    .parameter "nativeBitmap"
    .parameter "buffer"
    .parameter "isMutable"
    .parameter "ninePatchChunk"
    .parameter "layoutBounds"
    .parameter "density"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 111
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 61
    iput v0, p0, Landroid/graphics/Bitmap;->mWidth:I

    #@6
    .line 62
    iput v0, p0, Landroid/graphics/Bitmap;->mHeight:I

    #@8
    .line 66
    invoke-static {}, Landroid/graphics/Bitmap;->getDefaultDensity()I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@e
    .line 112
    if-nez p1, :cond_18

    #@10
    .line 113
    new-instance v0, Ljava/lang/RuntimeException;

    #@12
    const-string v1, "internal error: native bitmap is 0"

    #@14
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@17
    throw v0

    #@18
    .line 116
    :cond_18
    iput-object p2, p0, Landroid/graphics/Bitmap;->mBuffer:[B

    #@1a
    .line 118
    iput p1, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@1c
    .line 119
    new-instance v0, Landroid/graphics/Bitmap$BitmapFinalizer;

    #@1e
    invoke-direct {v0, p1}, Landroid/graphics/Bitmap$BitmapFinalizer;-><init>(I)V

    #@21
    iput-object v0, p0, Landroid/graphics/Bitmap;->mFinalizer:Landroid/graphics/Bitmap$BitmapFinalizer;

    #@23
    .line 121
    iput-boolean p3, p0, Landroid/graphics/Bitmap;->mIsMutable:Z

    #@25
    .line 122
    iput-object p4, p0, Landroid/graphics/Bitmap;->mNinePatchChunk:[B

    #@27
    .line 123
    iput-object p5, p0, Landroid/graphics/Bitmap;->mLayoutBounds:[I

    #@29
    .line 124
    if-ltz p6, :cond_2d

    #@2b
    .line 125
    iput p6, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@2d
    .line 127
    :cond_2d
    return-void
.end method

.method static synthetic access$000(Landroid/os/Parcel;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-static {p0}, Landroid/graphics/Bitmap;->nativeCreateFromParcel(Landroid/os/Parcel;)Landroid/graphics/Bitmap;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 29
    invoke-static {p0}, Landroid/graphics/Bitmap;->nativeDestructor(I)V

    #@3
    return-void
.end method

.method private checkPixelAccess(II)V
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1153
    invoke-static {p1, p2}, Landroid/graphics/Bitmap;->checkXYSign(II)V

    #@3
    .line 1154
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@6
    move-result v0

    #@7
    if-lt p1, v0, :cond_12

    #@9
    .line 1155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@b
    const-string/jumbo v1, "x must be < bitmap.width()"

    #@e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@11
    throw v0

    #@12
    .line 1157
    :cond_12
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@15
    move-result v0

    #@16
    if-lt p2, v0, :cond_21

    #@18
    .line 1158
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string/jumbo v1, "y must be < bitmap.height()"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 1160
    :cond_21
    return-void
.end method

.method private checkPixelsAccess(IIIIII[I)V
    .registers 12
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "offset"
    .parameter "stride"
    .parameter "pixels"

    #@0
    .prologue
    .line 1176
    invoke-static {p1, p2}, Landroid/graphics/Bitmap;->checkXYSign(II)V

    #@3
    .line 1177
    if-gez p3, :cond_e

    #@5
    .line 1178
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@7
    const-string/jumbo v3, "width must be >= 0"

    #@a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@d
    throw v2

    #@e
    .line 1180
    :cond_e
    if-gez p4, :cond_18

    #@10
    .line 1181
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@12
    const-string v3, "height must be >= 0"

    #@14
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@17
    throw v2

    #@18
    .line 1183
    :cond_18
    add-int v2, p1, p3

    #@1a
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@1d
    move-result v3

    #@1e
    if-le v2, v3, :cond_29

    #@20
    .line 1184
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@22
    const-string/jumbo v3, "x + width must be <= bitmap.width()"

    #@25
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@28
    throw v2

    #@29
    .line 1187
    :cond_29
    add-int v2, p2, p4

    #@2b
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@2e
    move-result v3

    #@2f
    if-le v2, v3, :cond_3a

    #@31
    .line 1188
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@33
    const-string/jumbo v3, "y + height must be <= bitmap.height()"

    #@36
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@39
    throw v2

    #@3a
    .line 1191
    :cond_3a
    invoke-static {p6}, Ljava/lang/Math;->abs(I)I

    #@3d
    move-result v2

    #@3e
    if-ge v2, p3, :cond_48

    #@40
    .line 1192
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@42
    const-string v3, "abs(stride) must be >= width"

    #@44
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@47
    throw v2

    #@48
    .line 1194
    :cond_48
    add-int/lit8 v2, p4, -0x1

    #@4a
    mul-int/2addr v2, p6

    #@4b
    add-int v0, p5, v2

    #@4d
    .line 1195
    .local v0, lastScanline:I
    array-length v1, p7

    #@4e
    .line 1196
    .local v1, length:I
    if-ltz p5, :cond_5a

    #@50
    add-int v2, p5, p3

    #@52
    if-gt v2, v1, :cond_5a

    #@54
    if-ltz v0, :cond_5a

    #@56
    add-int v2, v0, p3

    #@58
    if-le v2, v1, :cond_60

    #@5a
    .line 1199
    :cond_5a
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@5c
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@5f
    throw v2

    #@60
    .line 1201
    :cond_60
    return-void
.end method

.method private checkRecycled(Ljava/lang/String;)V
    .registers 3
    .parameter "errorMessage"

    #@0
    .prologue
    .line 242
    iget-boolean v0, p0, Landroid/graphics/Bitmap;->mRecycled:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 243
    new-instance v0, Ljava/lang/IllegalStateException;

    #@6
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 245
    :cond_a
    return-void
.end method

.method private static checkWidthHeight(II)V
    .registers 4
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 269
    if-gtz p0, :cond_b

    #@2
    .line 270
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "width must be > 0"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 272
    :cond_b
    if-gtz p1, :cond_15

    #@d
    .line 273
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v1, "height must be > 0"

    #@11
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v0

    #@15
    .line 275
    :cond_15
    return-void
.end method

.method private static checkXYSign(II)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 254
    if-gez p0, :cond_b

    #@2
    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "x must be >= 0"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 257
    :cond_b
    if-gez p1, :cond_16

    #@d
    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@f
    const-string/jumbo v1, "y must be >= 0"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 260
    :cond_16
    return-void
.end method

.method public static createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 633
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static createBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "width"
    .parameter "height"
    .parameter "config"
    .parameter "hasAlpha"

    #@0
    .prologue
    .line 666
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0, p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "src"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 497
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@4
    move-result v0

    #@5
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@8
    move-result v1

    #@9
    invoke-static {p0, v2, v2, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public static createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    .registers 12
    .parameter "source"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 514
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    move-object v0, p0

    #@3
    move v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move v4, p4

    #@7
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    .registers 24
    .parameter "source"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "m"
    .parameter "filter"

    #@0
    .prologue
    .line 543
    invoke-static/range {p1 .. p2}, Landroid/graphics/Bitmap;->checkXYSign(II)V

    #@3
    .line 544
    invoke-static/range {p3 .. p4}, Landroid/graphics/Bitmap;->checkWidthHeight(II)V

    #@6
    .line 545
    add-int v13, p1, p3

    #@8
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@b
    move-result v14

    #@c
    if-le v13, v14, :cond_17

    #@e
    .line 546
    new-instance v13, Ljava/lang/IllegalArgumentException;

    #@10
    const-string/jumbo v14, "x + width must be <= bitmap.width()"

    #@13
    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@16
    throw v13

    #@17
    .line 548
    :cond_17
    add-int v13, p2, p4

    #@19
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@1c
    move-result v14

    #@1d
    if-le v13, v14, :cond_28

    #@1f
    .line 549
    new-instance v13, Ljava/lang/IllegalArgumentException;

    #@21
    const-string/jumbo v14, "y + height must be <= bitmap.height()"

    #@24
    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v13

    #@28
    .line 553
    :cond_28
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->isMutable()Z

    #@2b
    move-result v13

    #@2c
    if-nez v13, :cond_4b

    #@2e
    if-nez p1, :cond_4b

    #@30
    if-nez p2, :cond_4b

    #@32
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@35
    move-result v13

    #@36
    move/from16 v0, p3

    #@38
    if-ne v0, v13, :cond_4b

    #@3a
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@3d
    move-result v13

    #@3e
    move/from16 v0, p4

    #@40
    if-ne v0, v13, :cond_4b

    #@42
    if-eqz p5, :cond_4a

    #@44
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Matrix;->isIdentity()Z

    #@47
    move-result v13

    #@48
    if-eqz v13, :cond_4b

    #@4a
    .line 620
    .end local p0
    :cond_4a
    :goto_4a
    return-object p0

    #@4b
    .line 558
    .restart local p0
    :cond_4b
    move/from16 v9, p3

    #@4d
    .line 559
    .local v9, neww:I
    move/from16 v8, p4

    #@4f
    .line 560
    .local v8, newh:I
    new-instance v3, Landroid/graphics/Canvas;

    #@51
    invoke-direct {v3}, Landroid/graphics/Canvas;-><init>()V

    #@54
    .line 564
    .local v3, canvas:Landroid/graphics/Canvas;
    new-instance v11, Landroid/graphics/Rect;

    #@56
    add-int v13, p1, p3

    #@58
    add-int v14, p2, p4

    #@5a
    move/from16 v0, p1

    #@5c
    move/from16 v1, p2

    #@5e
    invoke-direct {v11, v0, v1, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    #@61
    .line 565
    .local v11, srcR:Landroid/graphics/Rect;
    new-instance v6, Landroid/graphics/RectF;

    #@63
    const/4 v13, 0x0

    #@64
    const/4 v14, 0x0

    #@65
    move/from16 v0, p3

    #@67
    int-to-float v15, v0

    #@68
    move/from16 v0, p4

    #@6a
    int-to-float v0, v0

    #@6b
    move/from16 v16, v0

    #@6d
    move/from16 v0, v16

    #@6f
    invoke-direct {v6, v13, v14, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    #@72
    .line 567
    .local v6, dstR:Landroid/graphics/RectF;
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@74
    .line 568
    .local v7, newConfig:Landroid/graphics/Bitmap$Config;
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@77
    move-result-object v4

    #@78
    .line 570
    .local v4, config:Landroid/graphics/Bitmap$Config;
    if-eqz v4, :cond_87

    #@7a
    .line 571
    sget-object v13, Landroid/graphics/Bitmap$2;->$SwitchMap$android$graphics$Bitmap$Config:[I

    #@7c
    invoke-virtual {v4}, Landroid/graphics/Bitmap$Config;->ordinal()I

    #@7f
    move-result v14

    #@80
    aget v13, v13, v14

    #@82
    packed-switch v13, :pswitch_data_108

    #@85
    .line 582
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@87
    .line 587
    :cond_87
    :goto_87
    if-eqz p5, :cond_8f

    #@89
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Matrix;->isIdentity()Z

    #@8c
    move-result v13

    #@8d
    if-eqz v13, :cond_b3

    #@8f
    .line 588
    :cond_8f
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@92
    move-result v13

    #@93
    invoke-static {v9, v8, v7, v13}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@96
    move-result-object v2

    #@97
    .line 589
    .local v2, bitmap:Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    #@98
    .line 614
    .end local v7           #newConfig:Landroid/graphics/Bitmap$Config;
    .local v10, paint:Landroid/graphics/Paint;
    :cond_98
    :goto_98
    move-object/from16 v0, p0

    #@9a
    iget v13, v0, Landroid/graphics/Bitmap;->mDensity:I

    #@9c
    iput v13, v2, Landroid/graphics/Bitmap;->mDensity:I

    #@9e
    .line 616
    invoke-virtual {v3, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@a1
    .line 617
    move-object/from16 v0, p0

    #@a3
    invoke-virtual {v3, v0, v11, v6, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    #@a6
    .line 618
    const/4 v13, 0x0

    #@a7
    invoke-virtual {v3, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    #@aa
    move-object/from16 p0, v2

    #@ac
    .line 620
    goto :goto_4a

    #@ad
    .line 573
    .end local v2           #bitmap:Landroid/graphics/Bitmap;
    .end local v10           #paint:Landroid/graphics/Paint;
    .restart local v7       #newConfig:Landroid/graphics/Bitmap$Config;
    :pswitch_ad
    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@af
    .line 574
    goto :goto_87

    #@b0
    .line 576
    :pswitch_b0
    sget-object v7, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@b2
    .line 577
    goto :goto_87

    #@b3
    .line 591
    :cond_b3
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Matrix;->rectStaysRect()Z

    #@b6
    move-result v13

    #@b7
    if-nez v13, :cond_104

    #@b9
    const/4 v12, 0x1

    #@ba
    .line 593
    .local v12, transformed:Z
    :goto_ba
    new-instance v5, Landroid/graphics/RectF;

    #@bc
    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    #@bf
    .line 594
    .local v5, deviceR:Landroid/graphics/RectF;
    move-object/from16 v0, p5

    #@c1
    invoke-virtual {v0, v5, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    #@c4
    .line 596
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    #@c7
    move-result v13

    #@c8
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    #@cb
    move-result v9

    #@cc
    .line 597
    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    #@cf
    move-result v13

    #@d0
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    #@d3
    move-result v8

    #@d4
    .line 599
    if-eqz v12, :cond_d8

    #@d6
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@d8
    .end local v7           #newConfig:Landroid/graphics/Bitmap$Config;
    :cond_d8
    if-nez v12, :cond_e0

    #@da
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@dd
    move-result v13

    #@de
    if-eqz v13, :cond_106

    #@e0
    :cond_e0
    const/4 v13, 0x1

    #@e1
    :goto_e1
    invoke-static {v9, v8, v7, v13}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@e4
    move-result-object v2

    #@e5
    .line 602
    .restart local v2       #bitmap:Landroid/graphics/Bitmap;
    iget v13, v5, Landroid/graphics/RectF;->left:F

    #@e7
    neg-float v13, v13

    #@e8
    iget v14, v5, Landroid/graphics/RectF;->top:F

    #@ea
    neg-float v14, v14

    #@eb
    invoke-virtual {v3, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    #@ee
    .line 603
    move-object/from16 v0, p5

    #@f0
    invoke-virtual {v3, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    #@f3
    .line 605
    new-instance v10, Landroid/graphics/Paint;

    #@f5
    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    #@f8
    .line 606
    .restart local v10       #paint:Landroid/graphics/Paint;
    move/from16 v0, p6

    #@fa
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    #@fd
    .line 607
    if-eqz v12, :cond_98

    #@ff
    .line 608
    const/4 v13, 0x1

    #@100
    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    #@103
    goto :goto_98

    #@104
    .line 591
    .end local v2           #bitmap:Landroid/graphics/Bitmap;
    .end local v5           #deviceR:Landroid/graphics/RectF;
    .end local v10           #paint:Landroid/graphics/Paint;
    .end local v12           #transformed:Z
    .restart local v7       #newConfig:Landroid/graphics/Bitmap$Config;
    :cond_104
    const/4 v12, 0x0

    #@105
    goto :goto_ba

    #@106
    .line 599
    .end local v7           #newConfig:Landroid/graphics/Bitmap$Config;
    .restart local v5       #deviceR:Landroid/graphics/RectF;
    .restart local v12       #transformed:Z
    :cond_106
    const/4 v13, 0x0

    #@107
    goto :goto_e1

    #@108
    .line 571
    :pswitch_data_108
    .packed-switch 0x1
        :pswitch_ad
        :pswitch_b0
    .end packed-switch
.end method

.method public static createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "display"
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 649
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "display"
    .parameter "width"
    .parameter "height"
    .parameter "config"
    .parameter "hasAlpha"

    #@0
    .prologue
    .line 686
    if-lez p1, :cond_4

    #@2
    if-gtz p2, :cond_d

    #@4
    .line 687
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@6
    const-string/jumbo v1, "width and height must be > 0"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 689
    :cond_d
    const/4 v0, 0x0

    #@e
    const/4 v1, 0x0

    #@f
    iget v5, p3, Landroid/graphics/Bitmap$Config;->nativeInt:I

    #@11
    const/4 v6, 0x1

    #@12
    move v2, p1

    #@13
    move v3, p1

    #@14
    move v4, p2

    #@15
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->nativeCreate([IIIIIIZ)Landroid/graphics/Bitmap;

    #@18
    move-result-object v7

    #@19
    .line 690
    .local v7, bm:Landroid/graphics/Bitmap;
    if-eqz p0, :cond_1f

    #@1b
    .line 691
    iget v0, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@1d
    iput v0, v7, Landroid/graphics/Bitmap;->mDensity:I

    #@1f
    .line 693
    :cond_1f
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@21
    if-ne p3, v0, :cond_31

    #@23
    if-nez p4, :cond_31

    #@25
    .line 694
    iget v0, v7, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@27
    const/high16 v1, -0x100

    #@29
    invoke-static {v0, v1}, Landroid/graphics/Bitmap;->nativeErase(II)V

    #@2c
    .line 695
    iget v0, v7, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2e
    invoke-static {v0, p4}, Landroid/graphics/Bitmap;->nativeSetHasAlpha(IZ)V

    #@31
    .line 702
    :cond_31
    return-object v7
.end method

.method public static createBitmap(Landroid/util/DisplayMetrics;[IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 18
    .parameter "display"
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 751
    invoke-static/range {p4 .. p5}, Landroid/graphics/Bitmap;->checkWidthHeight(II)V

    #@3
    .line 752
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    #@6
    move-result v1

    #@7
    if-ge v1, p4, :cond_11

    #@9
    .line 753
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@b
    const-string v2, "abs(stride) must be >= width"

    #@d
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v1

    #@11
    .line 755
    :cond_11
    add-int/lit8 v1, p5, -0x1

    #@13
    mul-int/2addr v1, p3

    #@14
    add-int v9, p2, v1

    #@16
    .line 756
    .local v9, lastScanline:I
    array-length v10, p1

    #@17
    .line 757
    .local v10, length:I
    if-ltz p2, :cond_23

    #@19
    add-int v1, p2, p4

    #@1b
    if-gt v1, v10, :cond_23

    #@1d
    if-ltz v9, :cond_23

    #@1f
    add-int v1, v9, p4

    #@21
    if-le v1, v10, :cond_29

    #@23
    .line 759
    :cond_23
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@25
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@28
    throw v1

    #@29
    .line 761
    :cond_29
    if-lez p4, :cond_2d

    #@2b
    if-gtz p5, :cond_36

    #@2d
    .line 762
    :cond_2d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2f
    const-string/jumbo v2, "width and height must be > 0"

    #@32
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@35
    throw v1

    #@36
    .line 764
    :cond_36
    move-object/from16 v0, p6

    #@38
    iget v6, v0, Landroid/graphics/Bitmap$Config;->nativeInt:I

    #@3a
    const/4 v7, 0x0

    #@3b
    move-object v1, p1

    #@3c
    move v2, p2

    #@3d
    move v3, p3

    #@3e
    move v4, p4

    #@3f
    move/from16 v5, p5

    #@41
    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->nativeCreate([IIIIIIZ)Landroid/graphics/Bitmap;

    #@44
    move-result-object v8

    #@45
    .line 766
    .local v8, bm:Landroid/graphics/Bitmap;
    if-eqz p0, :cond_4b

    #@47
    .line 767
    iget v1, p0, Landroid/util/DisplayMetrics;->densityDpi:I

    #@49
    iput v1, v8, Landroid/graphics/Bitmap;->mDensity:I

    #@4b
    .line 769
    :cond_4b
    return-object v8
.end method

.method public static createBitmap(Landroid/util/DisplayMetrics;[IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 12
    .parameter "display"
    .parameter "colors"
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 810
    const/4 v2, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v3, p2

    #@4
    move v4, p2

    #@5
    move v5, p3

    #@6
    move-object v6, p4

    #@7
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;[IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 725
    const/4 v0, 0x0

    #@1
    move-object v1, p0

    #@2
    move v2, p1

    #@3
    move v3, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    move-object v6, p5

    #@7
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;[IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .registers 11
    .parameter "colors"
    .parameter "width"
    .parameter "height"
    .parameter "config"

    #@0
    .prologue
    .line 788
    const/4 v0, 0x0

    #@1
    const/4 v2, 0x0

    #@2
    move-object v1, p0

    #@3
    move v3, p1

    #@4
    move v4, p1

    #@5
    move v5, p2

    #@6
    move-object v6, p3

    #@7
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;[IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public static createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .registers 14
    .parameter "src"
    .parameter "dstWidth"
    .parameter "dstHeight"
    .parameter "filter"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 464
    const-class v2, Landroid/graphics/Bitmap;

    #@3
    monitor-enter v2

    #@4
    .line 466
    :try_start_4
    sget-object v5, Landroid/graphics/Bitmap;->sScaleMatrix:Landroid/graphics/Matrix;

    #@6
    .line 467
    .local v5, m:Landroid/graphics/Matrix;
    const/4 v0, 0x0

    #@7
    sput-object v0, Landroid/graphics/Bitmap;->sScaleMatrix:Landroid/graphics/Matrix;

    #@9
    .line 468
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_4 .. :try_end_a} :catchall_36

    #@a
    .line 470
    if-nez v5, :cond_11

    #@c
    .line 471
    new-instance v5, Landroid/graphics/Matrix;

    #@e
    .end local v5           #m:Landroid/graphics/Matrix;
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    #@11
    .line 474
    .restart local v5       #m:Landroid/graphics/Matrix;
    :cond_11
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@14
    move-result v3

    #@15
    .line 475
    .local v3, width:I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@18
    move-result v4

    #@19
    .line 476
    .local v4, height:I
    int-to-float v0, p1

    #@1a
    int-to-float v2, v3

    #@1b
    div-float v8, v0, v2

    #@1d
    .line 477
    .local v8, sx:F
    int-to-float v0, p2

    #@1e
    int-to-float v2, v4

    #@1f
    div-float v9, v0, v2

    #@21
    .line 478
    .local v9, sy:F
    invoke-virtual {v5, v8, v9}, Landroid/graphics/Matrix;->setScale(FF)V

    #@24
    move-object v0, p0

    #@25
    move v2, v1

    #@26
    move v6, p3

    #@27
    .line 479
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    #@2a
    move-result-object v7

    #@2b
    .line 481
    .local v7, b:Landroid/graphics/Bitmap;
    const-class v1, Landroid/graphics/Bitmap;

    #@2d
    monitor-enter v1

    #@2e
    .line 483
    :try_start_2e
    sget-object v0, Landroid/graphics/Bitmap;->sScaleMatrix:Landroid/graphics/Matrix;

    #@30
    if-nez v0, :cond_34

    #@32
    .line 484
    sput-object v5, Landroid/graphics/Bitmap;->sScaleMatrix:Landroid/graphics/Matrix;

    #@34
    .line 486
    :cond_34
    monitor-exit v1
    :try_end_35
    .catchall {:try_start_2e .. :try_end_35} :catchall_39

    #@35
    .line 488
    return-object v7

    #@36
    .line 468
    .end local v3           #width:I
    .end local v4           #height:I
    .end local v5           #m:Landroid/graphics/Matrix;
    .end local v7           #b:Landroid/graphics/Bitmap;
    .end local v8           #sx:F
    .end local v9           #sy:F
    :catchall_36
    move-exception v0

    #@37
    :try_start_37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_36

    #@38
    throw v0

    #@39
    .line 486
    .restart local v3       #width:I
    .restart local v4       #height:I
    .restart local v5       #m:Landroid/graphics/Matrix;
    .restart local v7       #b:Landroid/graphics/Bitmap;
    .restart local v8       #sx:F
    .restart local v9       #sy:F
    :catchall_39
    move-exception v0

    #@3a
    :try_start_3a
    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_3a .. :try_end_3b} :catchall_39

    #@3b
    throw v0
.end method

.method static getDefaultDensity()I
    .registers 1

    #@0
    .prologue
    .line 82
    sget v0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@2
    if-ltz v0, :cond_7

    #@4
    .line 83
    sget v0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@6
    .line 86
    :goto_6
    return v0

    #@7
    .line 85
    :cond_7
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    #@9
    sput v0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@b
    .line 86
    sget v0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@d
    goto :goto_6
.end method

.method private static native nativeCompress(IIILjava/io/OutputStream;[B)Z
.end method

.method private static native nativeConfig(I)I
.end method

.method private static native nativeCopy(IIZ)Landroid/graphics/Bitmap;
.end method

.method private static native nativeCopyPixelsFromBuffer(ILjava/nio/Buffer;)V
.end method

.method private static native nativeCopyPixelsToBuffer(ILjava/nio/Buffer;)V
.end method

.method private static native nativeCreate([IIIIIIZ)Landroid/graphics/Bitmap;
.end method

.method private static native nativeCreateFromParcel(Landroid/os/Parcel;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDestructor(I)V
.end method

.method private static native nativeErase(II)V
.end method

.method private static native nativeExtractAlpha(II[I)Landroid/graphics/Bitmap;
.end method

.method private static native nativeGenerationId(I)I
.end method

.method private static native nativeGetPixel(III)I
.end method

.method private static native nativeGetPixels(I[IIIIIII)V
.end method

.method private static native nativeHasAlpha(I)Z
.end method

.method private static native nativeHasMipMap(I)Z
.end method

.method private static native nativeHeight(I)I
.end method

.method private static native nativePrepareToDraw(I)V
.end method

.method private static native nativeRecycle(I)Z
.end method

.method private static native nativeRowBytes(I)I
.end method

.method private static native nativeSameAs(II)Z
.end method

.method private static native nativeSetHasAlpha(IZ)V
.end method

.method private static native nativeSetHasMipMap(IZ)V
.end method

.method private static native nativeSetPixel(IIII)V
.end method

.method private static native nativeSetPixels(I[IIIIIII)V
.end method

.method private static native nativeWidth(I)I
.end method

.method private static native nativeWriteToParcel(IZILandroid/os/Parcel;)Z
.end method

.method public static scaleFromDensity(III)I
    .registers 5
    .parameter "size"
    .parameter "sdensity"
    .parameter "tdensity"

    #@0
    .prologue
    .line 975
    if-eqz p1, :cond_6

    #@2
    if-eqz p2, :cond_6

    #@4
    if-ne p1, p2, :cond_7

    #@6
    .line 980
    .end local p0
    :cond_6
    :goto_6
    return p0

    #@7
    .restart local p0
    :cond_7
    mul-int v0, p0, p2

    #@9
    shr-int/lit8 v1, p1, 0x1

    #@b
    add-int/2addr v0, v1

    #@c
    div-int p0, v0, p1

    #@e
    goto :goto_6
.end method

.method public static setDefaultDensity(I)V
    .registers 1
    .parameter "density"

    #@0
    .prologue
    .line 78
    sput p0, Landroid/graphics/Bitmap;->sDefaultDensity:I

    #@2
    .line 79
    return-void
.end method


# virtual methods
.method public compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    .registers 7
    .parameter "format"
    .parameter "quality"
    .parameter "stream"

    #@0
    .prologue
    .line 867
    const-string v0, "Can\'t compress a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 869
    if-nez p3, :cond_d

    #@7
    .line 870
    new-instance v0, Ljava/lang/NullPointerException;

    #@9
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@c
    throw v0

    #@d
    .line 872
    :cond_d
    if-ltz p2, :cond_13

    #@f
    const/16 v0, 0x64

    #@11
    if-le p2, v0, :cond_1c

    #@13
    .line 873
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@15
    const-string/jumbo v1, "quality must be 0..100"

    #@18
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1b
    throw v0

    #@1c
    .line 875
    :cond_1c
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@1e
    iget v1, p1, Landroid/graphics/Bitmap$CompressFormat;->nativeInt:I

    #@20
    const/16 v2, 0x1000

    #@22
    new-array v2, v2, [B

    #@24
    invoke-static {v0, v1, p2, p3, v2}, Landroid/graphics/Bitmap;->nativeCompress(IIILjava/io/OutputStream;[B)Z

    #@27
    move-result v0

    #@28
    return v0
.end method

.method public copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .registers 6
    .parameter "config"
    .parameter "isMutable"

    #@0
    .prologue
    .line 441
    const-string v1, "Can\'t copy a recycled bitmap"

    #@2
    invoke-direct {p0, v1}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 442
    iget v1, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@7
    iget v2, p1, Landroid/graphics/Bitmap$Config;->nativeInt:I

    #@9
    invoke-static {v1, v2, p2}, Landroid/graphics/Bitmap;->nativeCopy(IIZ)Landroid/graphics/Bitmap;

    #@c
    move-result-object v0

    #@d
    .line 443
    .local v0, b:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_13

    #@f
    .line 444
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@11
    iput v1, v0, Landroid/graphics/Bitmap;->mDensity:I

    #@13
    .line 446
    :cond_13
    return-object v0
.end method

.method public copyPixelsFromBuffer(Ljava/nio/Buffer;)V
    .registers 13
    .parameter "src"

    #@0
    .prologue
    .line 399
    const-string v7, "copyPixelsFromBuffer called on recycled bitmap"

    #@2
    invoke-direct {p0, v7}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 401
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    #@8
    move-result v4

    #@9
    .line 403
    .local v4, elements:I
    instance-of v7, p1, Ljava/nio/ByteBuffer;

    #@b
    if-eqz v7, :cond_22

    #@d
    .line 404
    const/4 v6, 0x0

    #@e
    .line 413
    .local v6, shift:I
    :goto_e
    int-to-long v7, v4

    #@f
    shl-long v2, v7, v6

    #@11
    .line 414
    .local v2, bufferBytes:J
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getByteCount()I

    #@14
    move-result v7

    #@15
    int-to-long v0, v7

    #@16
    .line 416
    .local v0, bitmapBytes:J
    cmp-long v7, v2, v0

    #@18
    if-gez v7, :cond_37

    #@1a
    .line 417
    new-instance v7, Ljava/lang/RuntimeException;

    #@1c
    const-string v8, "Buffer not large enough for pixels"

    #@1e
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@21
    throw v7

    #@22
    .line 405
    .end local v0           #bitmapBytes:J
    .end local v2           #bufferBytes:J
    .end local v6           #shift:I
    :cond_22
    instance-of v7, p1, Ljava/nio/ShortBuffer;

    #@24
    if-eqz v7, :cond_28

    #@26
    .line 406
    const/4 v6, 0x1

    #@27
    .restart local v6       #shift:I
    goto :goto_e

    #@28
    .line 407
    .end local v6           #shift:I
    :cond_28
    instance-of v7, p1, Ljava/nio/IntBuffer;

    #@2a
    if-eqz v7, :cond_2e

    #@2c
    .line 408
    const/4 v6, 0x2

    #@2d
    .restart local v6       #shift:I
    goto :goto_e

    #@2e
    .line 410
    .end local v6           #shift:I
    :cond_2e
    new-instance v7, Ljava/lang/RuntimeException;

    #@30
    const-string/jumbo v8, "unsupported Buffer subclass"

    #@33
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@36
    throw v7

    #@37
    .line 420
    .restart local v0       #bitmapBytes:J
    .restart local v2       #bufferBytes:J
    .restart local v6       #shift:I
    :cond_37
    iget v7, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@39
    invoke-static {v7, p1}, Landroid/graphics/Bitmap;->nativeCopyPixelsFromBuffer(ILjava/nio/Buffer;)V

    #@3c
    .line 423
    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    #@3f
    move-result v5

    #@40
    .line 424
    .local v5, position:I
    int-to-long v7, v5

    #@41
    shr-long v9, v0, v6

    #@43
    add-long/2addr v7, v9

    #@44
    long-to-int v5, v7

    #@45
    .line 425
    invoke-virtual {p1, v5}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    #@48
    .line 426
    return-void
.end method

.method public copyPixelsToBuffer(Ljava/nio/Buffer;)V
    .registers 13
    .parameter "dst"

    #@0
    .prologue
    .line 365
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    #@3
    move-result v2

    #@4
    .line 367
    .local v2, elements:I
    instance-of v7, p1, Ljava/nio/ByteBuffer;

    #@6
    if-eqz v7, :cond_1d

    #@8
    .line 368
    const/4 v6, 0x0

    #@9
    .line 377
    .local v6, shift:I
    :goto_9
    int-to-long v7, v2

    #@a
    shl-long v0, v7, v6

    #@c
    .line 378
    .local v0, bufferSize:J
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getByteCount()I

    #@f
    move-result v7

    #@10
    int-to-long v3, v7

    #@11
    .line 380
    .local v3, pixelSize:J
    cmp-long v7, v0, v3

    #@13
    if-gez v7, :cond_32

    #@15
    .line 381
    new-instance v7, Ljava/lang/RuntimeException;

    #@17
    const-string v8, "Buffer not large enough for pixels"

    #@19
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1c
    throw v7

    #@1d
    .line 369
    .end local v0           #bufferSize:J
    .end local v3           #pixelSize:J
    .end local v6           #shift:I
    :cond_1d
    instance-of v7, p1, Ljava/nio/ShortBuffer;

    #@1f
    if-eqz v7, :cond_23

    #@21
    .line 370
    const/4 v6, 0x1

    #@22
    .restart local v6       #shift:I
    goto :goto_9

    #@23
    .line 371
    .end local v6           #shift:I
    :cond_23
    instance-of v7, p1, Ljava/nio/IntBuffer;

    #@25
    if-eqz v7, :cond_29

    #@27
    .line 372
    const/4 v6, 0x2

    #@28
    .restart local v6       #shift:I
    goto :goto_9

    #@29
    .line 374
    .end local v6           #shift:I
    :cond_29
    new-instance v7, Ljava/lang/RuntimeException;

    #@2b
    const-string/jumbo v8, "unsupported Buffer subclass"

    #@2e
    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@31
    throw v7

    #@32
    .line 384
    .restart local v0       #bufferSize:J
    .restart local v3       #pixelSize:J
    .restart local v6       #shift:I
    :cond_32
    iget v7, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@34
    invoke-static {v7, p1}, Landroid/graphics/Bitmap;->nativeCopyPixelsToBuffer(ILjava/nio/Buffer;)V

    #@37
    .line 387
    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    #@3a
    move-result v5

    #@3b
    .line 388
    .local v5, position:I
    int-to-long v7, v5

    #@3c
    shr-long v9, v3, v6

    #@3e
    add-long/2addr v7, v9

    #@3f
    long-to-int v5, v7

    #@40
    .line 389
    invoke-virtual {p1, v5}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    #@43
    .line 390
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 1286
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public eraseColor(I)V
    .registers 4
    .parameter "c"

    #@0
    .prologue
    .line 1088
    const-string v0, "Can\'t erase a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1089
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isMutable()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_13

    #@b
    .line 1090
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    const-string v1, "cannot erase immutable bitmaps"

    #@f
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@12
    throw v0

    #@13
    .line 1092
    :cond_13
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@15
    invoke-static {v0, p1}, Landroid/graphics/Bitmap;->nativeErase(II)V

    #@18
    .line 1093
    return-void
.end method

.method public extractAlpha()Landroid/graphics/Bitmap;
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1309
    invoke-virtual {p0, v0, v0}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "paint"
    .parameter "offsetXY"

    #@0
    .prologue
    .line 1338
    const-string v2, "Can\'t extractAlpha on a recycled bitmap"

    #@2
    invoke-direct {p0, v2}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1339
    if-eqz p1, :cond_19

    #@7
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@9
    .line 1340
    .local v1, nativePaint:I
    :goto_9
    iget v2, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@b
    invoke-static {v2, v1, p2}, Landroid/graphics/Bitmap;->nativeExtractAlpha(II[I)Landroid/graphics/Bitmap;

    #@e
    move-result-object v0

    #@f
    .line 1341
    .local v0, bm:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1b

    #@11
    .line 1342
    new-instance v2, Ljava/lang/RuntimeException;

    #@13
    const-string v3, "Failed to extractAlpha on Bitmap"

    #@15
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@18
    throw v2

    #@19
    .line 1339
    .end local v0           #bm:Landroid/graphics/Bitmap;
    .end local v1           #nativePaint:I
    :cond_19
    const/4 v1, 0x0

    #@1a
    goto :goto_9

    #@1b
    .line 1344
    .restart local v0       #bm:Landroid/graphics/Bitmap;
    .restart local v1       #nativePaint:I
    :cond_1b
    iget v2, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@1d
    iput v2, v0, Landroid/graphics/Bitmap;->mDensity:I

    #@1f
    .line 1345
    return-object v0
.end method

.method public final getByteCount()I
    .registers 3

    #@0
    .prologue
    .line 1000
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    #@3
    move-result v0

    #@4
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@7
    move-result v1

    #@8
    mul-int/2addr v0, v1

    #@9
    return v0
.end method

.method public final getConfig()Landroid/graphics/Bitmap$Config;
    .registers 2

    #@0
    .prologue
    .line 1008
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeConfig(I)I

    #@5
    move-result v0

    #@6
    invoke-static {v0}, Landroid/graphics/Bitmap$Config;->nativeToConfig(I)Landroid/graphics/Bitmap$Config;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public getDensity()I
    .registers 2

    #@0
    .prologue
    .line 150
    iget v0, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@2
    return v0
.end method

.method public getGenerationId()I
    .registers 2

    #@0
    .prologue
    .line 234
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeGenerationId(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getHeight()I
    .registers 3

    #@0
    .prologue
    .line 914
    iget v0, p0, Landroid/graphics/Bitmap;->mHeight:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_e

    #@5
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@7
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeHeight(I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/Bitmap;->mHeight:I

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    iget v0, p0, Landroid/graphics/Bitmap;->mHeight:I

    #@10
    goto :goto_d
.end method

.method public getLayoutBounds()[I
    .registers 2

    #@0
    .prologue
    .line 826
    iget-object v0, p0, Landroid/graphics/Bitmap;->mLayoutBounds:[I

    #@2
    return-object v0
.end method

.method public getNinePatchChunk()[B
    .registers 2

    #@0
    .prologue
    .line 818
    iget-object v0, p0, Landroid/graphics/Bitmap;->mNinePatchChunk:[B

    #@2
    return-object v0
.end method

.method public getPixel(II)I
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 1106
    const-string v0, "Can\'t call getPixel() on a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1107
    invoke-direct {p0, p1, p2}, Landroid/graphics/Bitmap;->checkPixelAccess(II)V

    #@8
    .line 1108
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@a
    invoke-static {v0, p1, p2}, Landroid/graphics/Bitmap;->nativeGetPixel(III)I

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public getPixels([IIIIIII)V
    .registers 16
    .parameter "pixels"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1136
    const-string v0, "Can\'t call getPixels() on a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1137
    if-eqz p6, :cond_9

    #@7
    if-nez p7, :cond_a

    #@9
    .line 1143
    :cond_9
    :goto_9
    return-void

    #@a
    :cond_a
    move-object v0, p0

    #@b
    move v1, p4

    #@c
    move v2, p5

    #@d
    move v3, p6

    #@e
    move v4, p7

    #@f
    move v5, p2

    #@10
    move v6, p3

    #@11
    move-object v7, p1

    #@12
    .line 1140
    invoke-direct/range {v0 .. v7}, Landroid/graphics/Bitmap;->checkPixelsAccess(IIIIII[I)V

    #@15
    .line 1141
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@17
    move-object v1, p1

    #@18
    move v2, p2

    #@19
    move v3, p3

    #@1a
    move v4, p4

    #@1b
    move v5, p5

    #@1c
    move v6, p6

    #@1d
    move v7, p7

    #@1e
    invoke-static/range {v0 .. v7}, Landroid/graphics/Bitmap;->nativeGetPixels(I[IIIIIII)V

    #@21
    goto :goto_9
.end method

.method public final getRowBytes()I
    .registers 2

    #@0
    .prologue
    .line 992
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeRowBytes(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getScaledHeight(I)I
    .registers 4
    .parameter "targetDensity"

    #@0
    .prologue
    .line 968
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    invoke-static {v0, v1, p1}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getScaledHeight(Landroid/graphics/Canvas;)I
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 930
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    iget v2, p1, Landroid/graphics/Canvas;->mDensity:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getScaledHeight(Landroid/util/DisplayMetrics;)I
    .registers 5
    .parameter "metrics"

    #@0
    .prologue
    .line 946
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    iget v2, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getScaledWidth(I)I
    .registers 4
    .parameter "targetDensity"

    #@0
    .prologue
    .line 957
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    invoke-static {v0, v1, p1}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public getScaledWidth(Landroid/graphics/Canvas;)I
    .registers 5
    .parameter "canvas"

    #@0
    .prologue
    .line 922
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    iget v2, p1, Landroid/graphics/Canvas;->mDensity:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public getScaledWidth(Landroid/util/DisplayMetrics;)I
    .registers 5
    .parameter "metrics"

    #@0
    .prologue
    .line 938
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@6
    iget v2, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->scaleFromDensity(III)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public final getWidth()I
    .registers 3

    #@0
    .prologue
    .line 909
    iget v0, p0, Landroid/graphics/Bitmap;->mWidth:I

    #@2
    const/4 v1, -0x1

    #@3
    if-ne v0, v1, :cond_e

    #@5
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@7
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeWidth(I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/Bitmap;->mWidth:I

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    iget v0, p0, Landroid/graphics/Bitmap;->mWidth:I

    #@10
    goto :goto_d
.end method

.method public final hasAlpha()Z
    .registers 2

    #@0
    .prologue
    .line 1020
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeHasAlpha(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final hasMipMap()Z
    .registers 2

    #@0
    .prologue
    .line 1055
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeHasMipMap(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final isMutable()Z
    .registers 2

    #@0
    .prologue
    .line 883
    iget-boolean v0, p0, Landroid/graphics/Bitmap;->mIsMutable:Z

    #@2
    return v0
.end method

.method public final isPremultiplied()Z
    .registers 3

    #@0
    .prologue
    .line 904
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    #@3
    move-result-object v0

    #@4
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@6
    if-eq v0, v1, :cond_10

    #@8
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public final isRecycled()Z
    .registers 2

    #@0
    .prologue
    .line 223
    iget-boolean v0, p0, Landroid/graphics/Bitmap;->mRecycled:Z

    #@2
    return v0
.end method

.method final ni()I
    .registers 2

    #@0
    .prologue
    .line 1444
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    return v0
.end method

.method public prepareToDraw()V
    .registers 2

    #@0
    .prologue
    .line 1369
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativePrepareToDraw(I)V

    #@5
    .line 1370
    return-void
.end method

.method public recycle()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 203
    iget-boolean v0, p0, Landroid/graphics/Bitmap;->mRecycled:Z

    #@3
    if-nez v0, :cond_14

    #@5
    .line 204
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@7
    invoke-static {v0}, Landroid/graphics/Bitmap;->nativeRecycle(I)Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_11

    #@d
    .line 209
    iput-object v1, p0, Landroid/graphics/Bitmap;->mBuffer:[B

    #@f
    .line 210
    iput-object v1, p0, Landroid/graphics/Bitmap;->mNinePatchChunk:[B

    #@11
    .line 212
    :cond_11
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/graphics/Bitmap;->mRecycled:Z

    #@14
    .line 214
    :cond_14
    return-void
.end method

.method public sameAs(Landroid/graphics/Bitmap;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 1354
    if-eq p0, p1, :cond_e

    #@2
    if-eqz p1, :cond_10

    #@4
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@6
    iget v1, p1, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@8
    invoke-static {v0, v1}, Landroid/graphics/Bitmap;->nativeSameAs(II)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    :cond_e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public setDensity(I)V
    .registers 2
    .parameter "density"

    #@0
    .prologue
    .line 167
    iput p1, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@2
    .line 168
    return-void
.end method

.method public setHasAlpha(Z)V
    .registers 3
    .parameter "hasAlpha"

    #@0
    .prologue
    .line 1034
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Bitmap;->nativeSetHasAlpha(IZ)V

    #@5
    .line 1035
    return-void
.end method

.method public final setHasMipMap(Z)V
    .registers 3
    .parameter "hasMipMap"

    #@0
    .prologue
    .line 1079
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Bitmap;->nativeSetHasMipMap(IZ)V

    #@5
    .line 1080
    return-void
.end method

.method public setLayoutBounds([I)V
    .registers 2
    .parameter "bounds"

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Landroid/graphics/Bitmap;->mLayoutBounds:[I

    #@2
    .line 189
    return-void
.end method

.method public setNinePatchChunk([B)V
    .registers 2
    .parameter "chunk"

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Landroid/graphics/Bitmap;->mNinePatchChunk:[B

    #@2
    .line 179
    return-void
.end method

.method public setPixel(III)V
    .registers 5
    .parameter "x"
    .parameter "y"
    .parameter "color"

    #@0
    .prologue
    .line 1217
    const-string v0, "Can\'t call setPixel() on a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1218
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isMutable()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_11

    #@b
    .line 1219
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@10
    throw v0

    #@11
    .line 1221
    :cond_11
    invoke-direct {p0, p1, p2}, Landroid/graphics/Bitmap;->checkPixelAccess(II)V

    #@14
    .line 1222
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@16
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Bitmap;->nativeSetPixel(IIII)V

    #@19
    .line 1223
    return-void
.end method

.method public setPixels([IIIIIII)V
    .registers 16
    .parameter "pixels"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 1250
    const-string v0, "Can\'t call setPixels() on a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1251
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isMutable()Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_11

    #@b
    .line 1252
    new-instance v0, Ljava/lang/IllegalStateException;

    #@d
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@10
    throw v0

    #@11
    .line 1254
    :cond_11
    if-eqz p6, :cond_15

    #@13
    if-nez p7, :cond_16

    #@15
    .line 1260
    :cond_15
    :goto_15
    return-void

    #@16
    :cond_16
    move-object v0, p0

    #@17
    move v1, p4

    #@18
    move v2, p5

    #@19
    move v3, p6

    #@1a
    move v4, p7

    #@1b
    move v5, p2

    #@1c
    move v6, p3

    #@1d
    move-object v7, p1

    #@1e
    .line 1257
    invoke-direct/range {v0 .. v7}, Landroid/graphics/Bitmap;->checkPixelsAccess(IIIIII[I)V

    #@21
    .line 1258
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@23
    move-object v1, p1

    #@24
    move v2, p2

    #@25
    move v3, p3

    #@26
    move v4, p4

    #@27
    move v5, p5

    #@28
    move v6, p6

    #@29
    move v7, p7

    #@2a
    invoke-static/range {v0 .. v7}, Landroid/graphics/Bitmap;->nativeSetPixels(I[IIIIIII)V

    #@2d
    goto :goto_15
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "p"
    .parameter "flags"

    #@0
    .prologue
    .line 1295
    const-string v0, "Can\'t parcel a recycled bitmap"

    #@2
    invoke-direct {p0, v0}, Landroid/graphics/Bitmap;->checkRecycled(Ljava/lang/String;)V

    #@5
    .line 1296
    iget v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:I

    #@7
    iget-boolean v1, p0, Landroid/graphics/Bitmap;->mIsMutable:Z

    #@9
    iget v2, p0, Landroid/graphics/Bitmap;->mDensity:I

    #@b
    invoke-static {v0, v1, v2, p1}, Landroid/graphics/Bitmap;->nativeWriteToParcel(IZILandroid/os/Parcel;)Z

    #@e
    move-result v0

    #@f
    if-nez v0, :cond_1a

    #@11
    .line 1297
    new-instance v0, Ljava/lang/RuntimeException;

    #@13
    const-string/jumbo v1, "native writeToParcel failed"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 1299
    :cond_1a
    return-void
.end method
