.class public Landroid/graphics/Matrix;
.super Ljava/lang/Object;
.source "Matrix.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Matrix$ScaleToFit;
    }
.end annotation


# static fields
.field public static IDENTITY_MATRIX:Landroid/graphics/Matrix; = null

.field public static final MPERSP_0:I = 0x6

.field public static final MPERSP_1:I = 0x7

.field public static final MPERSP_2:I = 0x8

.field public static final MSCALE_X:I = 0x0

.field public static final MSCALE_Y:I = 0x4

.field public static final MSKEW_X:I = 0x1

.field public static final MSKEW_Y:I = 0x3

.field public static final MTRANS_X:I = 0x2

.field public static final MTRANS_Y:I = 0x5


# instance fields
.field public native_instance:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 41
    new-instance v0, Landroid/graphics/Matrix$1;

    #@2
    invoke-direct {v0}, Landroid/graphics/Matrix$1;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/Matrix;->IDENTITY_MATRIX:Landroid/graphics/Matrix;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 230
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 231
    const/4 v0, 0x0

    #@4
    invoke-static {v0}, Landroid/graphics/Matrix;->native_create(I)I

    #@7
    move-result v0

    #@8
    iput v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@a
    .line 232
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Matrix;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 238
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 239
    if-eqz p1, :cond_e

    #@5
    iget v0, p1, Landroid/graphics/Matrix;->native_instance:I

    #@7
    :goto_7
    invoke-static {v0}, Landroid/graphics/Matrix;->native_create(I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@d
    .line 240
    return-void

    #@e
    .line 239
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_7
.end method

.method private static checkPointArrays([FI[FII)V
    .registers 8
    .parameter "src"
    .parameter "srcIndex"
    .parameter "dst"
    .parameter "dstIndex"
    .parameter "pointCount"

    #@0
    .prologue
    .line 544
    shl-int/lit8 v2, p4, 0x1

    #@2
    add-int v1, p1, v2

    #@4
    .line 545
    .local v1, srcStop:I
    shl-int/lit8 v2, p4, 0x1

    #@6
    add-int v0, p3, v2

    #@8
    .line 546
    .local v0, dstStop:I
    or-int v2, p4, p1

    #@a
    or-int/2addr v2, p3

    #@b
    or-int/2addr v2, v1

    #@c
    or-int/2addr v2, v0

    #@d
    if-ltz v2, :cond_15

    #@f
    array-length v2, p0

    #@10
    if-gt v1, v2, :cond_15

    #@12
    array-length v2, p2

    #@13
    if-le v0, v2, :cond_1b

    #@15
    .line 548
    :cond_15
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@17
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@1a
    throw v2

    #@1b
    .line 550
    :cond_1b
    return-void
.end method

.method private static native finalizer(I)V
.end method

.method private static native native_create(I)I
.end method

.method private static native native_equals(II)Z
.end method

.method private static native native_getValues(I[F)V
.end method

.method private static native native_invert(II)Z
.end method

.method private static native native_isIdentity(I)Z
.end method

.method private static native native_mapPoints(I[FI[FIIZ)V
.end method

.method private static native native_mapRadius(IF)F
.end method

.method private static native native_mapRect(ILandroid/graphics/RectF;Landroid/graphics/RectF;)Z
.end method

.method private static native native_postConcat(II)Z
.end method

.method private static native native_postRotate(IF)Z
.end method

.method private static native native_postRotate(IFFF)Z
.end method

.method private static native native_postScale(IFF)Z
.end method

.method private static native native_postScale(IFFFF)Z
.end method

.method private static native native_postSkew(IFF)Z
.end method

.method private static native native_postSkew(IFFFF)Z
.end method

.method private static native native_postTranslate(IFF)Z
.end method

.method private static native native_preConcat(II)Z
.end method

.method private static native native_preRotate(IF)Z
.end method

.method private static native native_preRotate(IFFF)Z
.end method

.method private static native native_preScale(IFF)Z
.end method

.method private static native native_preScale(IFFFF)Z
.end method

.method private static native native_preSkew(IFF)Z
.end method

.method private static native native_preSkew(IFFFF)Z
.end method

.method private static native native_preTranslate(IFF)Z
.end method

.method private static native native_rectStaysRect(I)Z
.end method

.method private static native native_reset(I)V
.end method

.method private static native native_set(II)V
.end method

.method private static native native_setConcat(III)Z
.end method

.method private static native native_setPolyToPoly(I[FI[FII)Z
.end method

.method private static native native_setRectToRect(ILandroid/graphics/RectF;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setRotate(IF)V
.end method

.method private static native native_setRotate(IFFF)V
.end method

.method private static native native_setScale(IFF)V
.end method

.method private static native native_setScale(IFFFF)V
.end method

.method private static native native_setSinCos(IFF)V
.end method

.method private static native native_setSinCos(IFFFF)V
.end method

.method private static native native_setSkew(IFF)V
.end method

.method private static native native_setSkew(IFFFF)V
.end method

.method private static native native_setTranslate(IFF)V
.end method

.method private static native native_setValues(I[F)V
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter "obj"

    #@0
    .prologue
    .line 274
    if-eqz p1, :cond_14

    #@2
    instance-of v0, p1, Landroid/graphics/Matrix;

    #@4
    if-eqz v0, :cond_14

    #@6
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@8
    check-cast p1, Landroid/graphics/Matrix;

    #@a
    .end local p1
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@c
    invoke-static {v0, v1}, Landroid/graphics/Matrix;->native_equals(II)Z

    #@f
    move-result v0

    #@10
    if-eqz v0, :cond_14

    #@12
    const/4 v0, 0x1

    #@13
    :goto_13
    return v0

    #@14
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_13
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 787
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Matrix;->finalizer(I)V

    #@5
    .line 788
    return-void
.end method

.method public getValues([F)V
    .registers 4
    .parameter "values"

    #@0
    .prologue
    .line 720
    array-length v0, p1

    #@1
    const/16 v1, 0x9

    #@3
    if-ge v0, v1, :cond_b

    #@5
    .line 721
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@7
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@a
    throw v0

    #@b
    .line 723
    :cond_b
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@d
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_getValues(I[F)V

    #@10
    .line 724
    return-void
.end method

.method public invert(Landroid/graphics/Matrix;)Z
    .registers 4
    .parameter "inverse"

    #@0
    .prologue
    .line 581
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Matrix;->native_invert(II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public isIdentity()Z
    .registers 2

    #@0
    .prologue
    .line 247
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Matrix;->native_isIdentity(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public mapPoints([F)V
    .registers 8
    .parameter "pts"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 664
    array-length v0, p1

    #@2
    shr-int/lit8 v5, v0, 0x1

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v3, p1

    #@7
    move v4, v2

    #@8
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapPoints([FI[FII)V

    #@b
    .line 665
    return-void
.end method

.method public mapPoints([FI[FII)V
    .registers 13
    .parameter "dst"
    .parameter "dstIndex"
    .parameter "src"
    .parameter "srcIndex"
    .parameter "pointCount"

    #@0
    .prologue
    .line 597
    invoke-static {p3, p4, p1, p2, p5}, Landroid/graphics/Matrix;->checkPointArrays([FI[FII)V

    #@3
    .line 598
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@5
    const/4 v6, 0x1

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move-object v3, p3

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Matrix;->native_mapPoints(I[FI[FIIZ)V

    #@e
    .line 600
    return-void
.end method

.method public mapPoints([F[F)V
    .registers 9
    .parameter "dst"
    .parameter "src"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 633
    array-length v0, p1

    #@2
    array-length v1, p2

    #@3
    if-eq v0, v1, :cond_b

    #@5
    .line 634
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@7
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@a
    throw v0

    #@b
    .line 636
    :cond_b
    array-length v0, p1

    #@c
    shr-int/lit8 v5, v0, 0x1

    #@e
    move-object v0, p0

    #@f
    move-object v1, p1

    #@10
    move-object v3, p2

    #@11
    move v4, v2

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapPoints([FI[FII)V

    #@15
    .line 637
    return-void
.end method

.method public mapRadius(F)F
    .registers 3
    .parameter "radius"

    #@0
    .prologue
    .line 714
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_mapRadius(IF)F

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public mapRect(Landroid/graphics/RectF;)Z
    .registers 3
    .parameter "rect"

    #@0
    .prologue
    .line 705
    invoke-virtual {p0, p1, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .registers 4
    .parameter "dst"
    .parameter "src"

    #@0
    .prologue
    .line 690
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_a

    #@4
    .line 691
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v0

    #@a
    .line 693
    :cond_a
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@c
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_mapRect(ILandroid/graphics/RectF;Landroid/graphics/RectF;)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public mapVectors([F)V
    .registers 8
    .parameter "vecs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 677
    array-length v0, p1

    #@2
    shr-int/lit8 v5, v0, 0x1

    #@4
    move-object v0, p0

    #@5
    move-object v1, p1

    #@6
    move-object v3, p1

    #@7
    move v4, v2

    #@8
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapVectors([FI[FII)V

    #@b
    .line 678
    return-void
.end method

.method public mapVectors([FI[FII)V
    .registers 13
    .parameter "dst"
    .parameter "dstIndex"
    .parameter "src"
    .parameter "srcIndex"
    .parameter "vectorCount"

    #@0
    .prologue
    .line 619
    invoke-static {p3, p4, p1, p2, p5}, Landroid/graphics/Matrix;->checkPointArrays([FI[FII)V

    #@3
    .line 620
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@5
    const/4 v6, 0x0

    #@6
    move-object v1, p1

    #@7
    move v2, p2

    #@8
    move-object v3, p3

    #@9
    move v4, p4

    #@a
    move v5, p5

    #@b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Matrix;->native_mapPoints(I[FI[FIIZ)V

    #@e
    .line 622
    return-void
.end method

.method public mapVectors([F[F)V
    .registers 9
    .parameter "dst"
    .parameter "src"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 651
    array-length v0, p1

    #@2
    array-length v1, p2

    #@3
    if-eq v0, v1, :cond_b

    #@5
    .line 652
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@7
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@a
    throw v0

    #@b
    .line 654
    :cond_b
    array-length v0, p1

    #@c
    shr-int/lit8 v5, v0, 0x1

    #@e
    move-object v0, p0

    #@f
    move-object v1, p1

    #@10
    move-object v3, p2

    #@11
    move v4, v2

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapVectors([FI[FII)V

    #@15
    .line 655
    return-void
.end method

.method final ni()I
    .registers 2

    #@0
    .prologue
    .line 791
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    return v0
.end method

.method public postConcat(Landroid/graphics/Matrix;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 482
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Matrix;->native_postConcat(II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public postRotate(F)Z
    .registers 3
    .parameter "degrees"

    #@0
    .prologue
    .line 458
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_postRotate(IF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postRotate(FFF)Z
    .registers 5
    .parameter "degrees"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 450
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Matrix;->native_postRotate(IFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postScale(FF)Z
    .registers 4
    .parameter "sx"
    .parameter "sy"

    #@0
    .prologue
    .line 442
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_postScale(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postScale(FFFF)Z
    .registers 6
    .parameter "sx"
    .parameter "sy"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 434
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_postScale(IFFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postSkew(FF)Z
    .registers 4
    .parameter "kx"
    .parameter "ky"

    #@0
    .prologue
    .line 474
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_postSkew(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postSkew(FFFF)Z
    .registers 6
    .parameter "kx"
    .parameter "ky"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 466
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_postSkew(IFFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public postTranslate(FF)Z
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 426
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_postTranslate(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preConcat(Landroid/graphics/Matrix;)Z
    .registers 4
    .parameter "other"

    #@0
    .prologue
    .line 418
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Matrix;->native_preConcat(II)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public preRotate(F)Z
    .registers 3
    .parameter "degrees"

    #@0
    .prologue
    .line 394
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_preRotate(IF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preRotate(FFF)Z
    .registers 5
    .parameter "degrees"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 386
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Matrix;->native_preRotate(IFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preScale(FF)Z
    .registers 4
    .parameter "sx"
    .parameter "sy"

    #@0
    .prologue
    .line 378
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_preScale(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preScale(FFFF)Z
    .registers 6
    .parameter "sx"
    .parameter "sy"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 370
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_preScale(IFFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preSkew(FF)Z
    .registers 4
    .parameter "kx"
    .parameter "ky"

    #@0
    .prologue
    .line 410
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_preSkew(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preSkew(FFFF)Z
    .registers 6
    .parameter "kx"
    .parameter "ky"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 402
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_preSkew(IFFFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public preTranslate(FF)Z
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 362
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_preTranslate(IFF)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public printShortString(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    .line 774
    const/16 v1, 0x9

    #@2
    new-array v0, v1, [F

    #@4
    .line 775
    .local v0, values:[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    #@7
    .line 776
    const/16 v1, 0x5b

    #@9
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@c
    .line 777
    const/4 v1, 0x0

    #@d
    aget v1, v0, v1

    #@f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@12
    const-string v1, ", "

    #@14
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17
    const/4 v1, 0x1

    #@18
    aget v1, v0, v1

    #@1a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@22
    .line 778
    const/4 v1, 0x2

    #@23
    aget v1, v0, v1

    #@25
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@28
    const-string v1, "]["

    #@2a
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d
    .line 779
    const/4 v1, 0x3

    #@2e
    aget v1, v0, v1

    #@30
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@33
    const-string v1, ", "

    #@35
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38
    const/4 v1, 0x4

    #@39
    aget v1, v0, v1

    #@3b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@3e
    const-string v1, ", "

    #@40
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@43
    .line 780
    const/4 v1, 0x5

    #@44
    aget v1, v0, v1

    #@46
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@49
    const-string v1, "]["

    #@4b
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4e
    .line 781
    const/4 v1, 0x6

    #@4f
    aget v1, v0, v1

    #@51
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@54
    const-string v1, ", "

    #@56
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@59
    const/4 v1, 0x7

    #@5a
    aget v1, v0, v1

    #@5c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@5f
    const-string v1, ", "

    #@61
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64
    .line 782
    const/16 v1, 0x8

    #@66
    aget v1, v0, v1

    #@68
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(F)V

    #@6b
    const/16 v1, 0x5d

    #@6d
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@70
    .line 784
    return-void
.end method

.method public rectStaysRect()Z
    .registers 2

    #@0
    .prologue
    .line 256
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Matrix;->native_rectStaysRect(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 281
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Matrix;->native_reset(I)V

    #@5
    .line 282
    return-void
.end method

.method public set(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 264
    if-nez p1, :cond_6

    #@2
    .line 265
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    #@5
    .line 269
    :goto_5
    return-void

    #@6
    .line 267
    :cond_6
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@8
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@a
    invoke-static {v0, v1}, Landroid/graphics/Matrix;->native_set(II)V

    #@d
    goto :goto_5
.end method

.method public setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z
    .registers 6
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 353
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    iget v2, p2, Landroid/graphics/Matrix;->native_instance:I

    #@6
    invoke-static {v0, v1, v2}, Landroid/graphics/Matrix;->native_setConcat(III)Z

    #@9
    move-result v0

    #@a
    return v0
.end method

.method public setPolyToPoly([FI[FII)Z
    .registers 12
    .parameter "src"
    .parameter "srcIndex"
    .parameter "dst"
    .parameter "dstIndex"
    .parameter "pointCount"

    #@0
    .prologue
    .line 567
    const/4 v0, 0x4

    #@1
    if-le p5, v0, :cond_9

    #@3
    .line 568
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@8
    throw v0

    #@9
    .line 570
    :cond_9
    invoke-static {p1, p2, p3, p4, p5}, Landroid/graphics/Matrix;->checkPointArrays([FI[FII)V

    #@c
    .line 571
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@e
    move-object v1, p1

    #@f
    move v2, p2

    #@10
    move-object v3, p3

    #@11
    move v4, p4

    #@12
    move v5, p5

    #@13
    invoke-static/range {v0 .. v5}, Landroid/graphics/Matrix;->native_setPolyToPoly(I[FI[FII)Z

    #@16
    move-result v0

    #@17
    return v0
.end method

.method public setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z
    .registers 6
    .parameter "src"
    .parameter "dst"
    .parameter "stf"

    #@0
    .prologue
    .line 533
    if-eqz p2, :cond_4

    #@2
    if-nez p1, :cond_a

    #@4
    .line 534
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    #@6
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@9
    throw v0

    #@a
    .line 536
    :cond_a
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@c
    iget v1, p3, Landroid/graphics/Matrix$ScaleToFit;->nativeInt:I

    #@e
    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Matrix;->native_setRectToRect(ILandroid/graphics/RectF;Landroid/graphics/RectF;I)Z

    #@11
    move-result v0

    #@12
    return v0
.end method

.method public setRotate(F)V
    .registers 3
    .parameter "degrees"

    #@0
    .prologue
    .line 316
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_setRotate(IF)V

    #@5
    .line 317
    return-void
.end method

.method public setRotate(FFF)V
    .registers 5
    .parameter "degrees"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 309
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Matrix;->native_setRotate(IFFF)V

    #@5
    .line 310
    return-void
.end method

.method public setScale(FF)V
    .registers 4
    .parameter "sx"
    .parameter "sy"

    #@0
    .prologue
    .line 300
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_setScale(IFF)V

    #@5
    .line 301
    return-void
.end method

.method public setScale(FFFF)V
    .registers 6
    .parameter "sx"
    .parameter "sy"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 295
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_setScale(IFFFF)V

    #@5
    .line 296
    return-void
.end method

.method public setSinCos(FF)V
    .registers 4
    .parameter "sinValue"
    .parameter "cosValue"

    #@0
    .prologue
    .line 330
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_setSinCos(IFF)V

    #@5
    .line 331
    return-void
.end method

.method public setSinCos(FFFF)V
    .registers 6
    .parameter "sinValue"
    .parameter "cosValue"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 325
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_setSinCos(IFFFF)V

    #@5
    .line 326
    return-void
.end method

.method public setSkew(FF)V
    .registers 4
    .parameter "kx"
    .parameter "ky"

    #@0
    .prologue
    .line 344
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_setSkew(IFF)V

    #@5
    .line 345
    return-void
.end method

.method public setSkew(FFFF)V
    .registers 6
    .parameter "kx"
    .parameter "ky"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 339
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Matrix;->native_setSkew(IFFFF)V

    #@5
    .line 340
    return-void
.end method

.method public setTranslate(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 286
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Matrix;->native_setTranslate(IFF)V

    #@5
    .line 287
    return-void
.end method

.method public setValues([F)V
    .registers 4
    .parameter "values"

    #@0
    .prologue
    .line 733
    array-length v0, p1

    #@1
    const/16 v1, 0x9

    #@3
    if-ge v0, v1, :cond_b

    #@5
    .line 734
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@7
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@a
    throw v0

    #@b
    .line 736
    :cond_b
    iget v0, p0, Landroid/graphics/Matrix;->native_instance:I

    #@d
    invoke-static {v0, p1}, Landroid/graphics/Matrix;->native_setValues(I[F)V

    #@10
    .line 737
    return-void
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 749
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 750
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->toShortString(Ljava/lang/StringBuilder;)V

    #@a
    .line 751
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method public toShortString(Ljava/lang/StringBuilder;)V
    .registers 4
    .parameter "sb"

    #@0
    .prologue
    .line 758
    const/16 v1, 0x9

    #@2
    new-array v0, v1, [F

    #@4
    .line 759
    .local v0, values:[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    #@7
    .line 760
    const/16 v1, 0x5b

    #@9
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@c
    .line 761
    const/4 v1, 0x0

    #@d
    aget v1, v0, v1

    #@f
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@12
    const-string v1, ", "

    #@14
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    const/4 v1, 0x1

    #@18
    aget v1, v0, v1

    #@1a
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1d
    const-string v1, ", "

    #@1f
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 762
    const/4 v1, 0x2

    #@23
    aget v1, v0, v1

    #@25
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@28
    const-string v1, "]["

    #@2a
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 763
    const/4 v1, 0x3

    #@2e
    aget v1, v0, v1

    #@30
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@33
    const-string v1, ", "

    #@35
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    const/4 v1, 0x4

    #@39
    aget v1, v0, v1

    #@3b
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@3e
    const-string v1, ", "

    #@40
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 764
    const/4 v1, 0x5

    #@44
    aget v1, v0, v1

    #@46
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@49
    const-string v1, "]["

    #@4b
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 765
    const/4 v1, 0x6

    #@4f
    aget v1, v0, v1

    #@51
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@54
    const-string v1, ", "

    #@56
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    const/4 v1, 0x7

    #@5a
    aget v1, v0, v1

    #@5c
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@5f
    const-string v1, ", "

    #@61
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 766
    const/16 v1, 0x8

    #@66
    aget v1, v0, v1

    #@68
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@6b
    const/16 v1, 0x5d

    #@6d
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@70
    .line 767
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 740
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x40

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 741
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "Matrix{"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 742
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->toShortString(Ljava/lang/StringBuilder;)V

    #@f
    .line 743
    const/16 v1, 0x7d

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 744
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    return-object v1
.end method
