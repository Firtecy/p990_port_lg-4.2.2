.class public Landroid/graphics/DiscretePathEffect;
.super Landroid/graphics/PathEffect;
.source "DiscretePathEffect.java"


# direct methods
.method public constructor <init>(FF)V
    .registers 4
    .parameter "segmentLength"
    .parameter "deviation"

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Landroid/graphics/PathEffect;-><init>()V

    #@3
    .line 26
    invoke-static {p1, p2}, Landroid/graphics/DiscretePathEffect;->nativeCreate(FF)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/graphics/PathEffect;->native_instance:I

    #@9
    .line 27
    return-void
.end method

.method private static native nativeCreate(FF)I
.end method
