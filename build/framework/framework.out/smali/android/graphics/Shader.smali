.class public Landroid/graphics/Shader;
.super Ljava/lang/Object;
.source "Shader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Shader$TileMode;
    }
.end annotation


# instance fields
.field private mLocalMatrix:Landroid/graphics/Matrix;

.field public native_instance:I

.field public native_shader:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    return-void
.end method

.method private static native nativeDestructor(II)V
.end method

.method private static native nativeSetLocalMatrix(III)V
.end method


# virtual methods
.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 87
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_b

    #@3
    .line 89
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@5
    iget v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@7
    invoke-static {v0, v1}, Landroid/graphics/Shader;->nativeDestructor(II)V

    #@a
    .line 91
    return-void

    #@b
    .line 89
    :catchall_b
    move-exception v0

    #@c
    iget v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@e
    iget v2, p0, Landroid/graphics/Shader;->native_shader:I

    #@10
    invoke-static {v1, v2}, Landroid/graphics/Shader;->nativeDestructor(II)V

    #@13
    throw v0
.end method

.method public getLocalMatrix(Landroid/graphics/Matrix;)Z
    .registers 4
    .parameter "localM"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 67
    iget-object v1, p0, Landroid/graphics/Shader;->mLocalMatrix:Landroid/graphics/Matrix;

    #@3
    if-eqz v1, :cond_13

    #@5
    .line 68
    iget-object v1, p0, Landroid/graphics/Shader;->mLocalMatrix:Landroid/graphics/Matrix;

    #@7
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    #@a
    .line 69
    iget-object v1, p0, Landroid/graphics/Shader;->mLocalMatrix:Landroid/graphics/Matrix;

    #@c
    invoke-virtual {v1}, Landroid/graphics/Matrix;->isIdentity()Z

    #@f
    move-result v1

    #@10
    if-nez v1, :cond_13

    #@12
    const/4 v0, 0x1

    #@13
    .line 71
    :cond_13
    return v0
.end method

.method public setLocalMatrix(Landroid/graphics/Matrix;)V
    .registers 5
    .parameter "localM"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Landroid/graphics/Shader;->mLocalMatrix:Landroid/graphics/Matrix;

    #@2
    .line 81
    iget v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@4
    iget v2, p0, Landroid/graphics/Shader;->native_shader:I

    #@6
    if-nez p1, :cond_d

    #@8
    const/4 v0, 0x0

    #@9
    :goto_9
    invoke-static {v1, v2, v0}, Landroid/graphics/Shader;->nativeSetLocalMatrix(III)V

    #@c
    .line 83
    return-void

    #@d
    .line 81
    :cond_d
    iget v0, p1, Landroid/graphics/Matrix;->native_instance:I

    #@f
    goto :goto_9
.end method
