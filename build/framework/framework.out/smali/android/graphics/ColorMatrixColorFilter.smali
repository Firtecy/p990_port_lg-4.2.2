.class public Landroid/graphics/ColorMatrixColorFilter;
.super Landroid/graphics/ColorFilter;
.source "ColorMatrixColorFilter.java"


# direct methods
.method public constructor <init>(Landroid/graphics/ColorMatrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Landroid/graphics/ColorFilter;-><init>()V

    #@3
    .line 28
    invoke-virtual {p1}, Landroid/graphics/ColorMatrix;->getArray()[F

    #@6
    move-result-object v0

    #@7
    .line 29
    .local v0, colorMatrix:[F
    invoke-static {v0}, Landroid/graphics/ColorMatrixColorFilter;->nativeColorMatrixFilter([F)I

    #@a
    move-result v1

    #@b
    iput v1, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@d
    .line 30
    iget v1, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@f
    invoke-static {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;->nColorMatrixFilter(I[F)I

    #@12
    move-result v1

    #@13
    iput v1, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@15
    .line 31
    return-void
.end method

.method public constructor <init>([F)V
    .registers 4
    .parameter "array"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/graphics/ColorFilter;-><init>()V

    #@3
    .line 41
    array-length v0, p1

    #@4
    const/16 v1, 0x14

    #@6
    if-ge v0, v1, :cond_e

    #@8
    .line 42
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@a
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@d
    throw v0

    #@e
    .line 44
    :cond_e
    invoke-static {p1}, Landroid/graphics/ColorMatrixColorFilter;->nativeColorMatrixFilter([F)I

    #@11
    move-result v0

    #@12
    iput v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@14
    .line 45
    iget v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@16
    invoke-static {v0, p1}, Landroid/graphics/ColorMatrixColorFilter;->nColorMatrixFilter(I[F)I

    #@19
    move-result v0

    #@1a
    iput v0, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@1c
    .line 46
    return-void
.end method

.method private static native nColorMatrixFilter(I[F)I
.end method

.method private static native nativeColorMatrixFilter([F)I
.end method
