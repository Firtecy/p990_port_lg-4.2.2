.class public Landroid/graphics/RadialGradient;
.super Landroid/graphics/Shader;
.source "RadialGradient.java"


# direct methods
.method public constructor <init>(FFFIILandroid/graphics/Shader$TileMode;)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "radius"
    .parameter "color0"
    .parameter "color1"
    .parameter "tile"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 57
    const/4 v0, 0x0

    #@4
    cmpg-float v0, p3, v0

    #@6
    if-gtz v0, :cond_11

    #@8
    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "radius must be > 0"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 60
    :cond_11
    iget v5, p6, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@13
    move v0, p1

    #@14
    move v1, p2

    #@15
    move v2, p3

    #@16
    move v3, p4

    #@17
    move v4, p5

    #@18
    invoke-static/range {v0 .. v5}, Landroid/graphics/RadialGradient;->nativeCreate2(FFFIII)I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@1e
    .line 61
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@20
    iget v6, p6, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@22
    move v1, p1

    #@23
    move v2, p2

    #@24
    move v3, p3

    #@25
    move v4, p4

    #@26
    move v5, p5

    #@27
    invoke-static/range {v0 .. v6}, Landroid/graphics/RadialGradient;->nativePostCreate2(IFFFIII)I

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/graphics/Shader;->native_shader:I

    #@2d
    .line 63
    return-void
.end method

.method public constructor <init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V
    .registers 14
    .parameter "x"
    .parameter "y"
    .parameter "radius"
    .parameter "colors"
    .parameter "positions"
    .parameter "tile"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 33
    const/4 v0, 0x0

    #@4
    cmpg-float v0, p3, v0

    #@6
    if-gtz v0, :cond_11

    #@8
    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@a
    const-string/jumbo v1, "radius must be > 0"

    #@d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@10
    throw v0

    #@11
    .line 36
    :cond_11
    array-length v0, p4

    #@12
    const/4 v1, 0x2

    #@13
    if-ge v0, v1, :cond_1e

    #@15
    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@17
    const-string/jumbo v1, "needs >= 2 number of colors"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 39
    :cond_1e
    if-eqz p5, :cond_2c

    #@20
    array-length v0, p4

    #@21
    array-length v1, p5

    #@22
    if-eq v0, v1, :cond_2c

    #@24
    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@26
    const-string v1, "color and position arrays must be of equal length"

    #@28
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v0

    #@2c
    .line 42
    :cond_2c
    iget v5, p6, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@2e
    move v0, p1

    #@2f
    move v1, p2

    #@30
    move v2, p3

    #@31
    move-object v3, p4

    #@32
    move-object v4, p5

    #@33
    invoke-static/range {v0 .. v5}, Landroid/graphics/RadialGradient;->nativeCreate1(FFF[I[FI)I

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@39
    .line 43
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@3b
    iget v6, p6, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@3d
    move v1, p1

    #@3e
    move v2, p2

    #@3f
    move v3, p3

    #@40
    move-object v4, p4

    #@41
    move-object v5, p5

    #@42
    invoke-static/range {v0 .. v6}, Landroid/graphics/RadialGradient;->nativePostCreate1(IFFF[I[FI)I

    #@45
    move-result v0

    #@46
    iput v0, p0, Landroid/graphics/Shader;->native_shader:I

    #@48
    .line 45
    return-void
.end method

.method private static native nativeCreate1(FFF[I[FI)I
.end method

.method private static native nativeCreate2(FFFIII)I
.end method

.method private static native nativePostCreate1(IFFF[I[FI)I
.end method

.method private static native nativePostCreate2(IFFFIII)I
.end method
