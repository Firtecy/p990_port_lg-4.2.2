.class public Landroid/graphics/Path;
.super Ljava/lang/Object;
.source "Path.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Path$Direction;,
        Landroid/graphics/Path$FillType;
    }
.end annotation


# static fields
.field static final sFillTypeArray:[Landroid/graphics/Path$FillType;


# instance fields
.field public isSimplePath:Z

.field private mDetectSimplePaths:Z

.field private mLastDirection:Landroid/graphics/Path$Direction;

.field public final mNativePath:I

.field public rects:Landroid/graphics/Region;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 122
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Landroid/graphics/Path$FillType;

    #@3
    const/4 v1, 0x0

    #@4
    sget-object v2, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    sget-object v2, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    sget-object v2, Landroid/graphics/Path$FillType;->INVERSE_WINDING:Landroid/graphics/Path$FillType;

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    sget-object v2, Landroid/graphics/Path$FillType;->INVERSE_EVEN_ODD:Landroid/graphics/Path$FillType;

    #@15
    aput-object v2, v0, v1

    #@17
    sput-object v0, Landroid/graphics/Path;->sFillTypeArray:[Landroid/graphics/Path$FillType;

    #@19
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 48
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    const/4 v0, 0x1

    #@4
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@6
    .line 43
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@9
    .line 49
    invoke-static {}, Landroid/graphics/Path;->init1()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@f
    .line 50
    invoke-static {}, Landroid/view/HardwareRenderer;->isAvailable()Z

    #@12
    move-result v0

    #@13
    iput-boolean v0, p0, Landroid/graphics/Path;->mDetectSimplePaths:Z

    #@15
    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Path;)V
    .registers 5
    .parameter "src"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    const/4 v1, 0x1

    #@4
    iput-boolean v1, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@6
    .line 43
    const/4 v1, 0x0

    #@7
    iput-object v1, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@9
    .line 59
    const/4 v0, 0x0

    #@a
    .line 60
    .local v0, valNative:I
    if-eqz p1, :cond_1f

    #@c
    .line 61
    iget v0, p1, Landroid/graphics/Path;->mNativePath:I

    #@e
    .line 62
    iget-boolean v1, p1, Landroid/graphics/Path;->isSimplePath:Z

    #@10
    iput-boolean v1, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@12
    .line 63
    iget-object v1, p1, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@14
    if-eqz v1, :cond_1f

    #@16
    .line 64
    new-instance v1, Landroid/graphics/Region;

    #@18
    iget-object v2, p1, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@1a
    invoke-direct {v1, v2}, Landroid/graphics/Region;-><init>(Landroid/graphics/Region;)V

    #@1d
    iput-object v1, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@1f
    .line 67
    :cond_1f
    invoke-static {v0}, Landroid/graphics/Path;->init2(I)I

    #@22
    move-result v1

    #@23
    iput v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@25
    .line 68
    invoke-static {}, Landroid/view/HardwareRenderer;->isAvailable()Z

    #@28
    move-result v1

    #@29
    iput-boolean v1, p0, Landroid/graphics/Path;->mDetectSimplePaths:Z

    #@2b
    .line 69
    return-void
.end method

.method private detectSimplePath(FFFFLandroid/graphics/Path$Direction;)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "dir"

    #@0
    .prologue
    .line 389
    iget-boolean v0, p0, Landroid/graphics/Path;->mDetectSimplePaths:Z

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 390
    iget-object v0, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@6
    if-nez v0, :cond_a

    #@8
    .line 391
    iput-object p5, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@a
    .line 393
    :cond_a
    iget-object v0, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@c
    if-eq v0, p5, :cond_12

    #@e
    .line 394
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@11
    .line 400
    :cond_11
    :goto_11
    return-void

    #@12
    .line 396
    :cond_12
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@14
    if-nez v0, :cond_1d

    #@16
    new-instance v0, Landroid/graphics/Region;

    #@18
    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    #@1b
    iput-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@1d
    .line 397
    :cond_1d
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@1f
    float-to-int v1, p1

    #@20
    float-to-int v2, p2

    #@21
    float-to-int v3, p3

    #@22
    float-to-int v4, p4

    #@23
    sget-object v5, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@25
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    #@28
    goto :goto_11
.end method

.method private static native finalizer(I)V
.end method

.method private static native init1()I
.end method

.method private static native init2(I)I
.end method

.method private static native native_addArc(ILandroid/graphics/RectF;FF)V
.end method

.method private static native native_addCircle(IFFFI)V
.end method

.method private static native native_addOval(ILandroid/graphics/RectF;I)V
.end method

.method private static native native_addPath(II)V
.end method

.method private static native native_addPath(IIFF)V
.end method

.method private static native native_addPath(III)V
.end method

.method private static native native_addRect(IFFFFI)V
.end method

.method private static native native_addRect(ILandroid/graphics/RectF;I)V
.end method

.method private static native native_addRoundRect(ILandroid/graphics/RectF;FFI)V
.end method

.method private static native native_addRoundRect(ILandroid/graphics/RectF;[FI)V
.end method

.method private static native native_arcTo(ILandroid/graphics/RectF;FFZ)V
.end method

.method private static native native_close(I)V
.end method

.method private static native native_computeBounds(ILandroid/graphics/RectF;)V
.end method

.method private static native native_cubicTo(IFFFFFF)V
.end method

.method private static native native_getFillType(I)I
.end method

.method private static native native_incReserve(II)V
.end method

.method private static native native_isEmpty(I)Z
.end method

.method private static native native_isRect(ILandroid/graphics/RectF;)Z
.end method

.method private static native native_lineTo(IFF)V
.end method

.method private static native native_moveTo(IFF)V
.end method

.method private static native native_offset(IFF)V
.end method

.method private static native native_offset(IFFI)V
.end method

.method private static native native_quadTo(IFFFF)V
.end method

.method private static native native_rCubicTo(IFFFFFF)V
.end method

.method private static native native_rLineTo(IFF)V
.end method

.method private static native native_rMoveTo(IFF)V
.end method

.method private static native native_rQuadTo(IFFFF)V
.end method

.method private static native native_reset(I)V
.end method

.method private static native native_rewind(I)V
.end method

.method private static native native_set(II)V
.end method

.method private static native native_setFillType(II)V
.end method

.method private static native native_setLastPoint(IFF)V
.end method

.method private static native native_transform(II)V
.end method

.method private static native native_transform(III)V
.end method


# virtual methods
.method public addArc(Landroid/graphics/RectF;FF)V
    .registers 6
    .parameter "oval"
    .parameter "startAngle"
    .parameter "sweepAngle"

    #@0
    .prologue
    .line 465
    if-nez p1, :cond_b

    #@2
    .line 466
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "need oval parameter"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 468
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@e
    .line 469
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@10
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Path;->native_addArc(ILandroid/graphics/RectF;FF)V

    #@13
    .line 470
    return-void
.end method

.method public addCircle(FFFLandroid/graphics/Path$Direction;)V
    .registers 7
    .parameter "x"
    .parameter "y"
    .parameter "radius"
    .parameter "dir"

    #@0
    .prologue
    .line 453
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 454
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    iget v1, p4, Landroid/graphics/Path$Direction;->nativeInt:I

    #@7
    invoke-static {v0, p1, p2, p3, v1}, Landroid/graphics/Path;->native_addCircle(IFFFI)V

    #@a
    .line 455
    return-void
.end method

.method public addOval(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V
    .registers 5
    .parameter "oval"
    .parameter "dir"

    #@0
    .prologue
    .line 437
    if-nez p1, :cond_b

    #@2
    .line 438
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "need oval parameter"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 440
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@e
    .line 441
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@10
    iget v1, p2, Landroid/graphics/Path$Direction;->nativeInt:I

    #@12
    invoke-static {v0, p1, v1}, Landroid/graphics/Path;->native_addOval(ILandroid/graphics/RectF;I)V

    #@15
    .line 442
    return-void
.end method

.method public addPath(Landroid/graphics/Path;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 525
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 526
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    iget v1, p1, Landroid/graphics/Path;->mNativePath:I

    #@7
    invoke-static {v0, v1}, Landroid/graphics/Path;->native_addPath(II)V

    #@a
    .line 527
    return-void
.end method

.method public addPath(Landroid/graphics/Path;FF)V
    .registers 6
    .parameter "src"
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 515
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 516
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    iget v1, p1, Landroid/graphics/Path;->mNativePath:I

    #@7
    invoke-static {v0, v1, p2, p3}, Landroid/graphics/Path;->native_addPath(IIFF)V

    #@a
    .line 517
    return-void
.end method

.method public addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V
    .registers 6
    .parameter "src"
    .parameter "matrix"

    #@0
    .prologue
    .line 535
    iget-boolean v0, p1, Landroid/graphics/Path;->isSimplePath:Z

    #@2
    if-nez v0, :cond_7

    #@4
    const/4 v0, 0x0

    #@5
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@7
    .line 536
    :cond_7
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@9
    iget v1, p1, Landroid/graphics/Path;->mNativePath:I

    #@b
    iget v2, p2, Landroid/graphics/Matrix;->native_instance:I

    #@d
    invoke-static {v0, v1, v2}, Landroid/graphics/Path;->native_addPath(III)V

    #@10
    .line 537
    return-void
.end method

.method public addRect(FFFFLandroid/graphics/Path$Direction;)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "dir"

    #@0
    .prologue
    .line 426
    invoke-direct/range {p0 .. p5}, Landroid/graphics/Path;->detectSimplePath(FFFFLandroid/graphics/Path$Direction;)V

    #@3
    .line 427
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    iget v5, p5, Landroid/graphics/Path$Direction;->nativeInt:I

    #@7
    move v1, p1

    #@8
    move v2, p2

    #@9
    move v3, p3

    #@a
    move v4, p4

    #@b
    invoke-static/range {v0 .. v5}, Landroid/graphics/Path;->native_addRect(IFFFFI)V

    #@e
    .line 428
    return-void
.end method

.method public addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V
    .registers 9
    .parameter "rect"
    .parameter "dir"

    #@0
    .prologue
    .line 409
    if-nez p1, :cond_b

    #@2
    .line 410
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "need rect parameter"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 412
    :cond_b
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@d
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@f
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@11
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@13
    move-object v0, p0

    #@14
    move-object v5, p2

    #@15
    invoke-direct/range {v0 .. v5}, Landroid/graphics/Path;->detectSimplePath(FFFFLandroid/graphics/Path$Direction;)V

    #@18
    .line 413
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@1a
    iget v1, p2, Landroid/graphics/Path$Direction;->nativeInt:I

    #@1c
    invoke-static {v0, p1, v1}, Landroid/graphics/Path;->native_addRect(ILandroid/graphics/RectF;I)V

    #@1f
    .line 414
    return-void
.end method

.method public addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V
    .registers 7
    .parameter "rect"
    .parameter "rx"
    .parameter "ry"
    .parameter "dir"

    #@0
    .prologue
    .line 481
    if-nez p1, :cond_b

    #@2
    .line 482
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "need rect parameter"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 484
    :cond_b
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@e
    .line 485
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@10
    iget v1, p4, Landroid/graphics/Path$Direction;->nativeInt:I

    #@12
    invoke-static {v0, p1, p2, p3, v1}, Landroid/graphics/Path;->native_addRoundRect(ILandroid/graphics/RectF;FFI)V

    #@15
    .line 486
    return-void
.end method

.method public addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V
    .registers 6
    .parameter "rect"
    .parameter "radii"
    .parameter "dir"

    #@0
    .prologue
    .line 498
    if-nez p1, :cond_b

    #@2
    .line 499
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "need rect parameter"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 501
    :cond_b
    array-length v0, p2

    #@c
    const/16 v1, 0x8

    #@e
    if-ge v0, v1, :cond_19

    #@10
    .line 502
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@12
    const-string/jumbo v1, "radii[] needs 8 values"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 504
    :cond_19
    const/4 v0, 0x0

    #@1a
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@1c
    .line 505
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@1e
    iget v1, p3, Landroid/graphics/Path$Direction;->nativeInt:I

    #@20
    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Path;->native_addRoundRect(ILandroid/graphics/RectF;[FI)V

    #@23
    .line 506
    return-void
.end method

.method public arcTo(Landroid/graphics/RectF;FF)V
    .registers 6
    .parameter "oval"
    .parameter "startAngle"
    .parameter "sweepAngle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 359
    iput-boolean v1, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 360
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2, p3, v1}, Landroid/graphics/Path;->native_arcTo(ILandroid/graphics/RectF;FFZ)V

    #@8
    .line 361
    return-void
.end method

.method public arcTo(Landroid/graphics/RectF;FFZ)V
    .registers 6
    .parameter "oval"
    .parameter "startAngle"
    .parameter "sweepAngle"
    .parameter "forceMoveTo"

    #@0
    .prologue
    .line 343
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 344
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Path;->native_arcTo(ILandroid/graphics/RectF;FFZ)V

    #@8
    .line 345
    return-void
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 368
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 369
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0}, Landroid/graphics/Path;->native_close(I)V

    #@8
    .line 370
    return-void
.end method

.method public computeBounds(Landroid/graphics/RectF;Z)V
    .registers 4
    .parameter "bounds"
    .parameter "exact"

    #@0
    .prologue
    .line 199
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Path;->native_computeBounds(ILandroid/graphics/RectF;)V

    #@5
    .line 200
    return-void
.end method

.method public cubicTo(FFFFFF)V
    .registers 14
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"
    .parameter "x3"
    .parameter "y3"

    #@0
    .prologue
    .line 313
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 314
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    move v1, p1

    #@6
    move v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move v6, p6

    #@b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Path;->native_cubicTo(IFFFFFF)V

    #@e
    .line 315
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 607
    :try_start_0
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0}, Landroid/graphics/Path;->finalizer(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 609
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 611
    return-void

    #@9
    .line 609
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public getFillType()Landroid/graphics/Path$FillType;
    .registers 3

    #@0
    .prologue
    .line 136
    sget-object v0, Landroid/graphics/Path;->sFillTypeArray:[Landroid/graphics/Path$FillType;

    #@2
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@4
    invoke-static {v1}, Landroid/graphics/Path;->native_getFillType(I)I

    #@7
    move-result v1

    #@8
    aget-object v0, v0, v1

    #@a
    return-object v0
.end method

.method public incReserve(I)V
    .registers 3
    .parameter "extraPtCount"

    #@0
    .prologue
    .line 210
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Path;->native_incReserve(II)V

    #@5
    .line 211
    return-void
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 173
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0}, Landroid/graphics/Path;->native_isEmpty(I)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public isInverseFillType()Z
    .registers 3

    #@0
    .prologue
    .line 154
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v1}, Landroid/graphics/Path;->native_getFillType(I)I

    #@5
    move-result v0

    #@6
    .line 155
    .local v0, ft:I
    and-int/lit8 v1, v0, 0x2

    #@8
    if-eqz v1, :cond_c

    #@a
    const/4 v1, 0x1

    #@b
    :goto_b
    return v1

    #@c
    :cond_c
    const/4 v1, 0x0

    #@d
    goto :goto_b
.end method

.method public isRect(Landroid/graphics/RectF;)Z
    .registers 3
    .parameter "rect"

    #@0
    .prologue
    .line 186
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Path;->native_isRect(ILandroid/graphics/RectF;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public lineTo(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 246
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 247
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_lineTo(IFF)V

    #@8
    .line 248
    return-void
.end method

.method public moveTo(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 220
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_moveTo(IFF)V

    #@5
    .line 221
    return-void
.end method

.method final ni()I
    .registers 2

    #@0
    .prologue
    .line 614
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    return v0
.end method

.method public offset(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 563
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 564
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_offset(IFF)V

    #@8
    .line 565
    return-void
.end method

.method public offset(FFLandroid/graphics/Path;)V
    .registers 6
    .parameter "dx"
    .parameter "dy"
    .parameter "dst"

    #@0
    .prologue
    .line 548
    const/4 v0, 0x0

    #@1
    .line 549
    .local v0, dstNative:I
    if-eqz p3, :cond_8

    #@3
    .line 550
    iget v0, p3, Landroid/graphics/Path;->mNativePath:I

    #@5
    .line 551
    const/4 v1, 0x0

    #@6
    iput-boolean v1, p3, Landroid/graphics/Path;->isSimplePath:Z

    #@8
    .line 553
    :cond_8
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@a
    invoke-static {v1, p1, p2, v0}, Landroid/graphics/Path;->native_offset(IFFI)V

    #@d
    .line 554
    return-void
.end method

.method public quadTo(FFFF)V
    .registers 6
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"

    #@0
    .prologue
    .line 276
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 277
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Path;->native_quadTo(IFFFF)V

    #@8
    .line 278
    return-void
.end method

.method public rCubicTo(FFFFFF)V
    .registers 14
    .parameter "x1"
    .parameter "y1"
    .parameter "x2"
    .parameter "y2"
    .parameter "x3"
    .parameter "y3"

    #@0
    .prologue
    .line 324
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 325
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    move v1, p1

    #@6
    move v2, p2

    #@7
    move v3, p3

    #@8
    move v4, p4

    #@9
    move v5, p5

    #@a
    move v6, p6

    #@b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Path;->native_rCubicTo(IFFFFFF)V

    #@e
    .line 326
    return-void
.end method

.method public rLineTo(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 261
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 262
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_rLineTo(IFF)V

    #@8
    .line 263
    return-void
.end method

.method public rMoveTo(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 234
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_rMoveTo(IFF)V

    #@5
    .line 235
    return-void
.end method

.method public rQuadTo(FFFF)V
    .registers 6
    .parameter "dx1"
    .parameter "dy1"
    .parameter "dx2"
    .parameter "dy2"

    #@0
    .prologue
    .line 295
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 296
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Path;->native_rQuadTo(IFFFF)V

    #@8
    .line 297
    return-void
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    .line 76
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 77
    iget-boolean v0, p0, Landroid/graphics/Path;->mDetectSimplePaths:Z

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 78
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@a
    .line 79
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@c
    if-eqz v0, :cond_13

    #@e
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@10
    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    #@13
    .line 81
    :cond_13
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@15
    invoke-static {v0}, Landroid/graphics/Path;->native_reset(I)V

    #@18
    .line 82
    return-void
.end method

.method public rewind()V
    .registers 2

    #@0
    .prologue
    .line 89
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 90
    iget-boolean v0, p0, Landroid/graphics/Path;->mDetectSimplePaths:Z

    #@5
    if-eqz v0, :cond_13

    #@7
    .line 91
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Landroid/graphics/Path;->mLastDirection:Landroid/graphics/Path$Direction;

    #@a
    .line 92
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@c
    if-eqz v0, :cond_13

    #@e
    iget-object v0, p0, Landroid/graphics/Path;->rects:Landroid/graphics/Region;

    #@10
    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    #@13
    .line 94
    :cond_13
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@15
    invoke-static {v0}, Landroid/graphics/Path;->native_rewind(I)V

    #@18
    .line 95
    return-void
.end method

.method public set(Landroid/graphics/Path;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 100
    if-eq p0, p1, :cond_d

    #@2
    .line 101
    iget-boolean v0, p1, Landroid/graphics/Path;->isSimplePath:Z

    #@4
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@6
    .line 102
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@8
    iget v1, p1, Landroid/graphics/Path;->mNativePath:I

    #@a
    invoke-static {v0, v1}, Landroid/graphics/Path;->native_set(II)V

    #@d
    .line 104
    :cond_d
    return-void
.end method

.method public setFillType(Landroid/graphics/Path$FillType;)V
    .registers 4
    .parameter "ft"

    #@0
    .prologue
    .line 145
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    iget v1, p1, Landroid/graphics/Path$FillType;->nativeInt:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Path;->native_setFillType(II)V

    #@7
    .line 146
    return-void
.end method

.method public setLastPoint(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 574
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 575
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    invoke-static {v0, p1, p2}, Landroid/graphics/Path;->native_setLastPoint(IFF)V

    #@8
    .line 576
    return-void
.end method

.method public toggleInverseFillType()V
    .registers 3

    #@0
    .prologue
    .line 162
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@2
    invoke-static {v1}, Landroid/graphics/Path;->native_getFillType(I)I

    #@5
    move-result v0

    #@6
    .line 163
    .local v0, ft:I
    xor-int/lit8 v0, v0, 0x2

    #@8
    .line 164
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@a
    invoke-static {v1, v0}, Landroid/graphics/Path;->native_setFillType(II)V

    #@d
    .line 165
    return-void
.end method

.method public transform(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 601
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Landroid/graphics/Path;->isSimplePath:Z

    #@3
    .line 602
    iget v0, p0, Landroid/graphics/Path;->mNativePath:I

    #@5
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@7
    invoke-static {v0, v1}, Landroid/graphics/Path;->native_transform(II)V

    #@a
    .line 603
    return-void
.end method

.method public transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V
    .registers 6
    .parameter "matrix"
    .parameter "dst"

    #@0
    .prologue
    .line 587
    const/4 v0, 0x0

    #@1
    .line 588
    .local v0, dstNative:I
    if-eqz p2, :cond_8

    #@3
    .line 589
    const/4 v1, 0x0

    #@4
    iput-boolean v1, p2, Landroid/graphics/Path;->isSimplePath:Z

    #@6
    .line 590
    iget v0, p2, Landroid/graphics/Path;->mNativePath:I

    #@8
    .line 592
    :cond_8
    iget v1, p0, Landroid/graphics/Path;->mNativePath:I

    #@a
    iget v2, p1, Landroid/graphics/Matrix;->native_instance:I

    #@c
    invoke-static {v1, v2, v0}, Landroid/graphics/Path;->native_transform(III)V

    #@f
    .line 593
    return-void
.end method
