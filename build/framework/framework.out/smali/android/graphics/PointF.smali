.class public Landroid/graphics/PointF;
.super Ljava/lang/Object;
.source "PointF.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public x:F

.field public y:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 134
    new-instance v0, Landroid/graphics/PointF$1;

    #@2
    invoke-direct {v0}, Landroid/graphics/PointF$1;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/PointF;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public constructor <init>(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    iput p1, p0, Landroid/graphics/PointF;->x:F

    #@5
    .line 35
    iput p2, p0, Landroid/graphics/PointF;->y:F

    #@7
    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Point;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iget v0, p1, Landroid/graphics/Point;->x:I

    #@5
    int-to-float v0, v0

    #@6
    iput v0, p0, Landroid/graphics/PointF;->x:F

    #@8
    .line 40
    iget v0, p1, Landroid/graphics/Point;->y:I

    #@a
    int-to-float v0, v0

    #@b
    iput v0, p0, Landroid/graphics/PointF;->y:F

    #@d
    .line 41
    return-void
.end method

.method public static length(FF)F
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 112
    mul-float v0, p0, p0

    #@2
    mul-float v1, p1, p1

    #@4
    add-float/2addr v0, v1

    #@5
    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    #@8
    move-result v0

    #@9
    return v0
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 120
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final equals(FF)Z
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 73
    iget v0, p0, Landroid/graphics/PointF;->x:F

    #@2
    cmpl-float v0, v0, p1

    #@4
    if-nez v0, :cond_e

    #@6
    iget v0, p0, Landroid/graphics/PointF;->y:F

    #@8
    cmpl-float v0, v0, p2

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 78
    if-ne p0, p1, :cond_5

    #@4
    .line 86
    :cond_4
    :goto_4
    return v1

    #@5
    .line 79
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 81
    check-cast v0, Landroid/graphics/PointF;

    #@16
    .line 83
    .local v0, pointF:Landroid/graphics/PointF;
    iget v3, v0, Landroid/graphics/PointF;->x:F

    #@18
    iget v4, p0, Landroid/graphics/PointF;->x:F

    #@1a
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_22

    #@20
    move v1, v2

    #@21
    goto :goto_4

    #@22
    .line 84
    :cond_22
    iget v3, v0, Landroid/graphics/PointF;->y:F

    #@24
    iget v4, p0, Landroid/graphics/PointF;->y:F

    #@26
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    #@29
    move-result v3

    #@2a
    if-eqz v3, :cond_4

    #@2c
    move v1, v2

    #@2d
    goto :goto_4
.end method

.method public hashCode()I
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    .line 91
    iget v2, p0, Landroid/graphics/PointF;->x:F

    #@4
    cmpl-float v2, v2, v4

    #@6
    if-eqz v2, :cond_1f

    #@8
    iget v2, p0, Landroid/graphics/PointF;->x:F

    #@a
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    #@d
    move-result v0

    #@e
    .line 92
    .local v0, result:I
    :goto_e
    mul-int/lit8 v2, v0, 0x1f

    #@10
    iget v3, p0, Landroid/graphics/PointF;->y:F

    #@12
    cmpl-float v3, v3, v4

    #@14
    if-eqz v3, :cond_1c

    #@16
    iget v1, p0, Landroid/graphics/PointF;->y:F

    #@18
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    #@1b
    move-result v1

    #@1c
    :cond_1c
    add-int v0, v2, v1

    #@1e
    .line 93
    return v0

    #@1f
    .end local v0           #result:I
    :cond_1f
    move v0, v1

    #@20
    .line 91
    goto :goto_e
.end method

.method public final length()F
    .registers 3

    #@0
    .prologue
    .line 105
    iget v0, p0, Landroid/graphics/PointF;->x:F

    #@2
    iget v1, p0, Landroid/graphics/PointF;->y:F

    #@4
    invoke-static {v0, v1}, Landroid/graphics/PointF;->length(FF)F

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public final negate()V
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/graphics/PointF;->x:F

    #@2
    neg-float v0, v0

    #@3
    iput v0, p0, Landroid/graphics/PointF;->x:F

    #@5
    .line 61
    iget v0, p0, Landroid/graphics/PointF;->y:F

    #@7
    neg-float v0, v0

    #@8
    iput v0, p0, Landroid/graphics/PointF;->y:F

    #@a
    .line 62
    return-void
.end method

.method public final offset(FF)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 65
    iget v0, p0, Landroid/graphics/PointF;->x:F

    #@2
    add-float/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/PointF;->x:F

    #@5
    .line 66
    iget v0, p0, Landroid/graphics/PointF;->y:F

    #@7
    add-float/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/PointF;->y:F

    #@a
    .line 67
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/graphics/PointF;->x:F

    #@6
    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/graphics/PointF;->y:F

    #@c
    .line 161
    return-void
.end method

.method public final set(FF)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 47
    iput p1, p0, Landroid/graphics/PointF;->x:F

    #@2
    .line 48
    iput p2, p0, Landroid/graphics/PointF;->y:F

    #@4
    .line 49
    return-void
.end method

.method public final set(Landroid/graphics/PointF;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 55
    iget v0, p1, Landroid/graphics/PointF;->x:F

    #@2
    iput v0, p0, Landroid/graphics/PointF;->x:F

    #@4
    .line 56
    iget v0, p1, Landroid/graphics/PointF;->y:F

    #@6
    iput v0, p0, Landroid/graphics/PointF;->y:F

    #@8
    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PointF("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/graphics/PointF;->x:F

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/graphics/PointF;->y:F

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ")"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 130
    iget v0, p0, Landroid/graphics/PointF;->x:F

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@5
    .line 131
    iget v0, p0, Landroid/graphics/PointF;->y:F

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@a
    .line 132
    return-void
.end method
