.class public Landroid/graphics/BlurMaskFilter;
.super Landroid/graphics/MaskFilter;
.source "BlurMaskFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/BlurMaskFilter$Blur;
    }
.end annotation


# direct methods
.method public constructor <init>(FLandroid/graphics/BlurMaskFilter$Blur;)V
    .registers 4
    .parameter "radius"
    .parameter "style"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/graphics/MaskFilter;-><init>()V

    #@3
    .line 47
    iget v0, p2, Landroid/graphics/BlurMaskFilter$Blur;->native_int:I

    #@5
    invoke-static {p1, v0}, Landroid/graphics/BlurMaskFilter;->nativeConstructor(FI)I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/graphics/MaskFilter;->native_instance:I

    #@b
    .line 48
    return-void
.end method

.method private static native nativeConstructor(FI)I
.end method
