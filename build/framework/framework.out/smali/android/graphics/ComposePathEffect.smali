.class public Landroid/graphics/ComposePathEffect;
.super Landroid/graphics/PathEffect;
.source "ComposePathEffect.java"


# direct methods
.method public constructor <init>(Landroid/graphics/PathEffect;Landroid/graphics/PathEffect;)V
    .registers 5
    .parameter "outerpe"
    .parameter "innerpe"

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Landroid/graphics/PathEffect;-><init>()V

    #@3
    .line 26
    iget v0, p1, Landroid/graphics/PathEffect;->native_instance:I

    #@5
    iget v1, p2, Landroid/graphics/PathEffect;->native_instance:I

    #@7
    invoke-static {v0, v1}, Landroid/graphics/ComposePathEffect;->nativeCreate(II)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/PathEffect;->native_instance:I

    #@d
    .line 28
    return-void
.end method

.method private static native nativeCreate(II)I
.end method
