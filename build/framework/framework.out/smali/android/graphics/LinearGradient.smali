.class public Landroid/graphics/LinearGradient;
.super Landroid/graphics/Shader;
.source "LinearGradient.java"


# direct methods
.method public constructor <init>(FFFFIILandroid/graphics/Shader$TileMode;)V
    .registers 18
    .parameter "x0"
    .parameter "y0"
    .parameter "x1"
    .parameter "y1"
    .parameter "color0"
    .parameter "color1"
    .parameter "tile"

    #@0
    .prologue
    .line 54
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 55
    move-object/from16 v0, p7

    #@5
    iget v8, v0, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move v3, p2

    #@a
    move v4, p3

    #@b
    move v5, p4

    #@c
    move v6, p5

    #@d
    move/from16 v7, p6

    #@f
    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;->nativeCreate2(FFFFIII)I

    #@12
    move-result v1

    #@13
    iput v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@15
    .line 56
    iget v2, p0, Landroid/graphics/Shader;->native_instance:I

    #@17
    move-object/from16 v0, p7

    #@19
    iget v9, v0, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@1b
    move-object v1, p0

    #@1c
    move v3, p1

    #@1d
    move v4, p2

    #@1e
    move v5, p3

    #@1f
    move v6, p4

    #@20
    move v7, p5

    #@21
    move/from16 v8, p6

    #@23
    invoke-direct/range {v1 .. v9}, Landroid/graphics/LinearGradient;->nativePostCreate2(IFFFFIII)I

    #@26
    move-result v1

    #@27
    iput v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@29
    .line 58
    return-void
.end method

.method public constructor <init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
    .registers 18
    .parameter "x0"
    .parameter "y0"
    .parameter "x1"
    .parameter "y1"
    .parameter "colors"
    .parameter "positions"
    .parameter "tile"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 33
    array-length v1, p5

    #@4
    const/4 v2, 0x2

    #@5
    if-ge v1, v2, :cond_10

    #@7
    .line 34
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v2, "needs >= 2 number of colors"

    #@c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v1

    #@10
    .line 36
    :cond_10
    if-eqz p6, :cond_20

    #@12
    array-length v1, p5

    #@13
    move-object/from16 v0, p6

    #@15
    array-length v2, v0

    #@16
    if-eq v1, v2, :cond_20

    #@18
    .line 37
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string v2, "color and position arrays must be of equal length"

    #@1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v1

    #@20
    .line 39
    :cond_20
    move-object/from16 v0, p7

    #@22
    iget v8, v0, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@24
    move-object v1, p0

    #@25
    move v2, p1

    #@26
    move v3, p2

    #@27
    move v4, p3

    #@28
    move v5, p4

    #@29
    move-object v6, p5

    #@2a
    move-object/from16 v7, p6

    #@2c
    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;->nativeCreate1(FFFF[I[FI)I

    #@2f
    move-result v1

    #@30
    iput v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@32
    .line 40
    iget v2, p0, Landroid/graphics/Shader;->native_instance:I

    #@34
    move-object/from16 v0, p7

    #@36
    iget v9, v0, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@38
    move-object v1, p0

    #@39
    move v3, p1

    #@3a
    move v4, p2

    #@3b
    move v5, p3

    #@3c
    move v6, p4

    #@3d
    move-object v7, p5

    #@3e
    move-object/from16 v8, p6

    #@40
    invoke-direct/range {v1 .. v9}, Landroid/graphics/LinearGradient;->nativePostCreate1(IFFFF[I[FI)I

    #@43
    move-result v1

    #@44
    iput v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@46
    .line 42
    return-void
.end method

.method private native nativeCreate1(FFFF[I[FI)I
.end method

.method private native nativeCreate2(FFFFIII)I
.end method

.method private native nativePostCreate1(IFFFF[I[FI)I
.end method

.method private native nativePostCreate2(IFFFFIII)I
.end method
