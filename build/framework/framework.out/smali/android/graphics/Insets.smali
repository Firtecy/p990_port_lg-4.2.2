.class public Landroid/graphics/Insets;
.super Ljava/lang/Object;
.source "Insets.java"


# static fields
.field public static final NONE:Landroid/graphics/Insets;


# instance fields
.field public final bottom:I

.field public final left:I

.field public final right:I

.field public final top:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 29
    new-instance v0, Landroid/graphics/Insets;

    #@3
    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Insets;-><init>(IIII)V

    #@6
    sput-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@8
    return-void
.end method

.method private constructor <init>(IIII)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 36
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 37
    iput p1, p0, Landroid/graphics/Insets;->left:I

    #@5
    .line 38
    iput p2, p0, Landroid/graphics/Insets;->top:I

    #@7
    .line 39
    iput p3, p0, Landroid/graphics/Insets;->right:I

    #@9
    .line 40
    iput p4, p0, Landroid/graphics/Insets;->bottom:I

    #@b
    .line 41
    return-void
.end method

.method public static of(IIII)Landroid/graphics/Insets;
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 56
    if-nez p0, :cond_b

    #@2
    if-nez p1, :cond_b

    #@4
    if-nez p2, :cond_b

    #@6
    if-nez p3, :cond_b

    #@8
    .line 57
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@a
    .line 59
    :goto_a
    return-object v0

    #@b
    :cond_b
    new-instance v0, Landroid/graphics/Insets;

    #@d
    invoke-direct {v0, p0, p1, p2, p3}, Landroid/graphics/Insets;-><init>(IIII)V

    #@10
    goto :goto_a
.end method

.method public static of(Landroid/graphics/Rect;)Landroid/graphics/Insets;
    .registers 5
    .parameter "r"

    #@0
    .prologue
    .line 70
    if-nez p0, :cond_5

    #@2
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    #@4
    :goto_4
    return-object v0

    #@5
    :cond_5
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@7
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@9
    iget v2, p0, Landroid/graphics/Rect;->right:I

    #@b
    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    #@d
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Insets;->of(IIII)Landroid/graphics/Insets;

    #@10
    move-result-object v0

    #@11
    goto :goto_4
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 83
    if-ne p0, p1, :cond_5

    #@4
    .line 93
    :cond_4
    :goto_4
    return v1

    #@5
    .line 84
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 86
    check-cast v0, Landroid/graphics/Insets;

    #@16
    .line 88
    .local v0, insets:Landroid/graphics/Insets;
    iget v3, p0, Landroid/graphics/Insets;->bottom:I

    #@18
    iget v4, v0, Landroid/graphics/Insets;->bottom:I

    #@1a
    if-eq v3, v4, :cond_1e

    #@1c
    move v1, v2

    #@1d
    goto :goto_4

    #@1e
    .line 89
    :cond_1e
    iget v3, p0, Landroid/graphics/Insets;->left:I

    #@20
    iget v4, v0, Landroid/graphics/Insets;->left:I

    #@22
    if-eq v3, v4, :cond_26

    #@24
    move v1, v2

    #@25
    goto :goto_4

    #@26
    .line 90
    :cond_26
    iget v3, p0, Landroid/graphics/Insets;->right:I

    #@28
    iget v4, v0, Landroid/graphics/Insets;->right:I

    #@2a
    if-eq v3, v4, :cond_2e

    #@2c
    move v1, v2

    #@2d
    goto :goto_4

    #@2e
    .line 91
    :cond_2e
    iget v3, p0, Landroid/graphics/Insets;->top:I

    #@30
    iget v4, v0, Landroid/graphics/Insets;->top:I

    #@32
    if-eq v3, v4, :cond_4

    #@34
    move v1, v2

    #@35
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 98
    iget v0, p0, Landroid/graphics/Insets;->left:I

    #@2
    .line 99
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@4
    iget v2, p0, Landroid/graphics/Insets;->top:I

    #@6
    add-int v0, v1, v2

    #@8
    .line 100
    mul-int/lit8 v1, v0, 0x1f

    #@a
    iget v2, p0, Landroid/graphics/Insets;->right:I

    #@c
    add-int v0, v1, v2

    #@e
    .line 101
    mul-int/lit8 v1, v0, 0x1f

    #@10
    iget v2, p0, Landroid/graphics/Insets;->bottom:I

    #@12
    add-int v0, v1, v2

    #@14
    .line 102
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Insets{left="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/graphics/Insets;->left:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", top="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/graphics/Insets;->top:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", right="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Landroid/graphics/Insets;->right:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, ", bottom="

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Landroid/graphics/Insets;->bottom:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const/16 v1, 0x7d

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    return-object v0
.end method
