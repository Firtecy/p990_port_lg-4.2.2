.class public final Landroid/graphics/Rect;
.super Ljava/lang/Object;
.source "Rect.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLATTENED_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field public bottom:I

.field public left:I

.field public right:I

.field public top:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 39
    const-string v0, "(-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+)"

    #@2
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Landroid/graphics/Rect;->FLATTENED_PATTERN:Ljava/util/regex/Pattern;

    #@8
    .line 557
    new-instance v0, Landroid/graphics/Rect$1;

    #@a
    invoke-direct {v0}, Landroid/graphics/Rect$1;-><init>()V

    #@d
    sput-object v0, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public constructor <init>(IIII)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 58
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@5
    .line 59
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@7
    .line 60
    iput p3, p0, Landroid/graphics/Rect;->right:I

    #@9
    .line 61
    iput p4, p0, Landroid/graphics/Rect;->bottom:I

    #@b
    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "r"

    #@0
    .prologue
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 72
    if-nez p1, :cond_f

    #@5
    .line 73
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@8
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@a
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@c
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@e
    .line 80
    :goto_e
    return-void

    #@f
    .line 75
    :cond_f
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@11
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@13
    .line 76
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@15
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@17
    .line 77
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@19
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@1b
    .line 78
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@1d
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@1f
    goto :goto_e
.end method

.method public static intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 4
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 458
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_1a

    #@6
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@8
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@a
    if-ge v0, v1, :cond_1a

    #@c
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@e
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@10
    if-ge v0, v1, :cond_1a

    #@12
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@14
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@16
    if-ge v0, v1, :cond_1a

    #@18
    const/4 v0, 0x1

    #@19
    :goto_19
    return v0

    #@1a
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_19
.end method

.method public static unflattenFromString(Ljava/lang/String;)Landroid/graphics/Rect;
    .registers 7
    .parameter "str"

    #@0
    .prologue
    .line 155
    sget-object v1, Landroid/graphics/Rect;->FLATTENED_PATTERN:Ljava/util/regex/Pattern;

    #@2
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@5
    move-result-object v0

    #@6
    .line 156
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_e

    #@c
    .line 157
    const/4 v1, 0x0

    #@d
    .line 159
    :goto_d
    return-object v1

    #@e
    :cond_e
    new-instance v1, Landroid/graphics/Rect;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@18
    move-result v2

    #@19
    const/4 v3, 0x2

    #@1a
    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@21
    move-result v3

    #@22
    const/4 v4, 0x3

    #@23
    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@26
    move-result-object v4

    #@27
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2a
    move-result v4

    #@2b
    const/4 v5, 0x4

    #@2c
    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@33
    move-result v5

    #@34
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    #@37
    goto :goto_d
.end method


# virtual methods
.method public final centerX()I
    .registers 3

    #@0
    .prologue
    .line 204
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    add-int/2addr v0, v1

    #@5
    shr-int/lit8 v0, v0, 0x1

    #@7
    return v0
.end method

.method public final centerY()I
    .registers 3

    #@0
    .prologue
    .line 213
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@4
    add-int/2addr v0, v1

    #@5
    shr-int/lit8 v0, v0, 0x1

    #@7
    return v0
.end method

.method public contains(II)Z
    .registers 5
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 323
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_1e

    #@6
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@a
    if-ge v0, v1, :cond_1e

    #@c
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@e
    if-lt p1, v0, :cond_1e

    #@10
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@12
    if-ge p1, v0, :cond_1e

    #@14
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@16
    if-lt p2, v0, :cond_1e

    #@18
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@1a
    if-ge p2, v0, :cond_1e

    #@1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method public contains(IIII)Z
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 341
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_1e

    #@6
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@a
    if-ge v0, v1, :cond_1e

    #@c
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@e
    if-gt v0, p1, :cond_1e

    #@10
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@12
    if-gt v0, p2, :cond_1e

    #@14
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@16
    if-lt v0, p3, :cond_1e

    #@18
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@1a
    if-lt v0, p4, :cond_1e

    #@1c
    const/4 v0, 0x1

    #@1d
    :goto_1d
    return v0

    #@1e
    :cond_1e
    const/4 v0, 0x0

    #@1f
    goto :goto_1d
.end method

.method public contains(Landroid/graphics/Rect;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 357
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_26

    #@6
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@a
    if-ge v0, v1, :cond_26

    #@c
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@e
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@10
    if-gt v0, v1, :cond_26

    #@12
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@14
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@16
    if-gt v0, v1, :cond_26

    #@18
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1a
    iget v1, p1, Landroid/graphics/Rect;->right:I

    #@1c
    if-lt v0, v1, :cond_26

    #@1e
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@20
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@22
    if-lt v0, v1, :cond_26

    #@24
    const/4 v0, 0x1

    #@25
    :goto_25
    return v0

    #@26
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_25
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 542
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 84
    if-ne p0, p1, :cond_5

    #@4
    .line 88
    :cond_4
    :goto_4
    return v1

    #@5
    .line 85
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 87
    check-cast v0, Landroid/graphics/Rect;

    #@16
    .line 88
    .local v0, r:Landroid/graphics/Rect;
    iget v3, p0, Landroid/graphics/Rect;->left:I

    #@18
    iget v4, v0, Landroid/graphics/Rect;->left:I

    #@1a
    if-ne v3, v4, :cond_2e

    #@1c
    iget v3, p0, Landroid/graphics/Rect;->top:I

    #@1e
    iget v4, v0, Landroid/graphics/Rect;->top:I

    #@20
    if-ne v3, v4, :cond_2e

    #@22
    iget v3, p0, Landroid/graphics/Rect;->right:I

    #@24
    iget v4, v0, Landroid/graphics/Rect;->right:I

    #@26
    if-ne v3, v4, :cond_2e

    #@28
    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    #@2a
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    #@2c
    if-eq v3, v4, :cond_4

    #@2e
    :cond_2e
    move v1, v2

    #@2f
    goto :goto_4
.end method

.method public final exactCenterX()F
    .registers 3

    #@0
    .prologue
    .line 220
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    add-int/2addr v0, v1

    #@5
    int-to-float v0, v0

    #@6
    const/high16 v1, 0x3f00

    #@8
    mul-float/2addr v0, v1

    #@9
    return v0
.end method

.method public final exactCenterY()F
    .registers 3

    #@0
    .prologue
    .line 227
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@4
    add-int/2addr v0, v1

    #@5
    int-to-float v0, v0

    #@6
    const/high16 v1, 0x3f00

    #@8
    mul-float/2addr v0, v1

    #@9
    return v0
.end method

.method public flattenToString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/16 v2, 0x20

    #@2
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 140
    .local v0, sb:Ljava/lang/StringBuilder;
    iget v1, p0, Landroid/graphics/Rect;->left:I

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c
    .line 141
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@f
    .line 142
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    .line 143
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@17
    .line 144
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    .line 145
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1f
    .line 146
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    .line 147
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    return-object v1
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 93
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    .line 94
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@4
    iget v2, p0, Landroid/graphics/Rect;->top:I

    #@6
    add-int v0, v1, v2

    #@8
    .line 95
    mul-int/lit8 v1, v0, 0x1f

    #@a
    iget v2, p0, Landroid/graphics/Rect;->right:I

    #@c
    add-int v0, v1, v2

    #@e
    .line 96
    mul-int/lit8 v1, v0, 0x1f

    #@10
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    #@12
    add-int v0, v1, v2

    #@14
    .line 97
    return v0
.end method

.method public final height()I
    .registers 3

    #@0
    .prologue
    .line 195
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public inset(II)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 305
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@5
    .line 306
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@7
    add-int/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@a
    .line 307
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@c
    sub-int/2addr v0, p1

    #@d
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@f
    .line 308
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@11
    sub-int/2addr v0, p2

    #@12
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@14
    .line 309
    return-void
.end method

.method public intersect(IIII)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 381
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    if-ge v0, p3, :cond_2a

    #@4
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@6
    if-ge p1, v0, :cond_2a

    #@8
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@a
    if-ge v0, p4, :cond_2a

    #@c
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@e
    if-ge p2, v0, :cond_2a

    #@10
    .line 382
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@12
    if-ge v0, p1, :cond_16

    #@14
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@16
    .line 383
    :cond_16
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@18
    if-ge v0, p2, :cond_1c

    #@1a
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@1c
    .line 384
    :cond_1c
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1e
    if-le v0, p3, :cond_22

    #@20
    iput p3, p0, Landroid/graphics/Rect;->right:I

    #@22
    .line 385
    :cond_22
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@24
    if-le v0, p4, :cond_28

    #@26
    iput p4, p0, Landroid/graphics/Rect;->bottom:I

    #@28
    .line 386
    :cond_28
    const/4 v0, 0x1

    #@29
    .line 388
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_29
.end method

.method public intersect(Landroid/graphics/Rect;)Z
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 403
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/Rect;->intersect(IIII)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public intersects(IIII)Z
    .registers 6
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 444
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    if-ge v0, p3, :cond_12

    #@4
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@6
    if-ge p1, v0, :cond_12

    #@8
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@a
    if-ge v0, p4, :cond_12

    #@c
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@e
    if-ge p2, v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method public final isEmpty()Z
    .registers 3

    #@0
    .prologue
    .line 179
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_c

    #@6
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@a
    if-lt v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public offset(II)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 275
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@5
    .line 276
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@7
    add-int/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@a
    .line 277
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@c
    add-int/2addr v0, p1

    #@d
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@f
    .line 278
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@11
    add-int/2addr v0, p2

    #@12
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@14
    .line 279
    return-void
.end method

.method public offsetTo(II)V
    .registers 5
    .parameter "newLeft"
    .parameter "newTop"

    #@0
    .prologue
    .line 289
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->left:I

    #@4
    sub-int v1, p1, v1

    #@6
    add-int/2addr v0, v1

    #@7
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@9
    .line 290
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@b
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@d
    sub-int v1, p2, v1

    #@f
    add-int/2addr v0, v1

    #@10
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@12
    .line 291
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@14
    .line 292
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@16
    .line 293
    return-void
.end method

.method public printShortString(Ljava/io/PrintWriter;)V
    .registers 4
    .parameter "pw"

    #@0
    .prologue
    const/16 v1, 0x2c

    #@2
    .line 170
    const/16 v0, 0x5b

    #@4
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@7
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@f
    .line 171
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@11
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@14
    const-string v0, "]["

    #@16
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1e
    .line 172
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(C)V

    #@21
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@23
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    #@26
    const/16 v0, 0x5d

    #@28
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(C)V

    #@2b
    .line 173
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 582
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@6
    .line 583
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@c
    .line 584
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@12
    .line 585
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@18
    .line 586
    return-void
.end method

.method public scale(F)V
    .registers 4
    .parameter "scale"

    #@0
    .prologue
    const/high16 v1, 0x3f00

    #@2
    .line 593
    const/high16 v0, 0x3f80

    #@4
    cmpl-float v0, p1, v0

    #@6
    if-eqz v0, :cond_28

    #@8
    .line 594
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@a
    int-to-float v0, v0

    #@b
    mul-float/2addr v0, p1

    #@c
    add-float/2addr v0, v1

    #@d
    float-to-int v0, v0

    #@e
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@10
    .line 595
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@12
    int-to-float v0, v0

    #@13
    mul-float/2addr v0, p1

    #@14
    add-float/2addr v0, v1

    #@15
    float-to-int v0, v0

    #@16
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@18
    .line 596
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1a
    int-to-float v0, v0

    #@1b
    mul-float/2addr v0, p1

    #@1c
    add-float/2addr v0, v1

    #@1d
    float-to-int v0, v0

    #@1e
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@20
    .line 597
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@22
    int-to-float v0, v0

    #@23
    mul-float/2addr v0, p1

    #@24
    add-float/2addr v0, v1

    #@25
    float-to-int v0, v0

    #@26
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@28
    .line 599
    :cond_28
    return-void
.end method

.method public set(IIII)V
    .registers 5
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 248
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@2
    .line 249
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@4
    .line 250
    iput p3, p0, Landroid/graphics/Rect;->right:I

    #@6
    .line 251
    iput p4, p0, Landroid/graphics/Rect;->bottom:I

    #@8
    .line 252
    return-void
.end method

.method public set(Landroid/graphics/Rect;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 261
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@4
    .line 262
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@6
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    .line 263
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@a
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@c
    .line 264
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@e
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@10
    .line 265
    return-void
.end method

.method public setEmpty()V
    .registers 2

    #@0
    .prologue
    .line 234
    const/4 v0, 0x0

    #@1
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@3
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@5
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@7
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@9
    .line 235
    return-void
.end method

.method public setIntersect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .registers 5
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 419
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@4
    if-ge v0, v1, :cond_42

    #@6
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@8
    iget v1, p1, Landroid/graphics/Rect;->right:I

    #@a
    if-ge v0, v1, :cond_42

    #@c
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@e
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@10
    if-ge v0, v1, :cond_42

    #@12
    iget v0, p2, Landroid/graphics/Rect;->top:I

    #@14
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    #@16
    if-ge v0, v1, :cond_42

    #@18
    .line 420
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@1a
    iget v1, p2, Landroid/graphics/Rect;->left:I

    #@1c
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@1f
    move-result v0

    #@20
    iput v0, p0, Landroid/graphics/Rect;->left:I

    #@22
    .line 421
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@24
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@26
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    #@29
    move-result v0

    #@2a
    iput v0, p0, Landroid/graphics/Rect;->top:I

    #@2c
    .line 422
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@2e
    iget v1, p2, Landroid/graphics/Rect;->right:I

    #@30
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@33
    move-result v0

    #@34
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@36
    .line 423
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@38
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    #@3a
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@3d
    move-result v0

    #@3e
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@40
    .line 424
    const/4 v0, 0x1

    #@41
    .line 426
    :goto_41
    return v0

    #@42
    :cond_42
    const/4 v0, 0x0

    #@43
    goto :goto_41
.end method

.method public sort()V
    .registers 4

    #@0
    .prologue
    .line 526
    iget v1, p0, Landroid/graphics/Rect;->left:I

    #@2
    iget v2, p0, Landroid/graphics/Rect;->right:I

    #@4
    if-le v1, v2, :cond_e

    #@6
    .line 527
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@8
    .line 528
    .local v0, temp:I
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@a
    iput v1, p0, Landroid/graphics/Rect;->left:I

    #@c
    .line 529
    iput v0, p0, Landroid/graphics/Rect;->right:I

    #@e
    .line 531
    .end local v0           #temp:I
    :cond_e
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@10
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    #@12
    if-le v1, v2, :cond_1c

    #@14
    .line 532
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@16
    .line 533
    .restart local v0       #temp:I
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@18
    iput v1, p0, Landroid/graphics/Rect;->top:I

    #@1a
    .line 534
    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    #@1c
    .line 536
    .end local v0           #temp:I
    :cond_1c
    return-void
.end method

.method public toShortString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x20

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    invoke-virtual {p0, v0}, Landroid/graphics/Rect;->toShortString(Ljava/lang/StringBuilder;)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public toShortString(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .registers 4
    .parameter "sb"

    #@0
    .prologue
    const/16 v1, 0x2c

    #@2
    .line 121
    const/4 v0, 0x0

    #@3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    #@6
    .line 122
    const/16 v0, 0x5b

    #@8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@d
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@13
    .line 123
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    const-string v0, "]["

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1f
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    .line 124
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@25
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@27
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    const/16 v0, 0x5d

    #@2c
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2f
    .line 125
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    const/16 v1, 0x20

    #@4
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    #@7
    .line 103
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "Rect("

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    iget v1, p0, Landroid/graphics/Rect;->left:I

    #@e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    .line 104
    iget v1, p0, Landroid/graphics/Rect;->top:I

    #@18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    const-string v1, " - "

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    .line 105
    const-string v1, ", "

    #@27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    const-string v1, ")"

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    return-object v1
.end method

.method public union(II)V
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 506
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    if-ge p1, v0, :cond_d

    #@4
    .line 507
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@6
    .line 511
    :cond_6
    :goto_6
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@8
    if-ge p2, v0, :cond_14

    #@a
    .line 512
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@c
    .line 516
    :cond_c
    :goto_c
    return-void

    #@d
    .line 508
    :cond_d
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@f
    if-le p1, v0, :cond_6

    #@11
    .line 509
    iput p1, p0, Landroid/graphics/Rect;->right:I

    #@13
    goto :goto_6

    #@14
    .line 513
    :cond_14
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@16
    if-le p2, v0, :cond_c

    #@18
    .line 514
    iput p2, p0, Landroid/graphics/Rect;->bottom:I

    #@1a
    goto :goto_c
.end method

.method public union(IIII)V
    .registers 7
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    #@0
    .prologue
    .line 472
    if-ge p1, p3, :cond_28

    #@2
    if-ge p2, p4, :cond_28

    #@4
    .line 473
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@6
    iget v1, p0, Landroid/graphics/Rect;->right:I

    #@8
    if-ge v0, v1, :cond_29

    #@a
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@c
    iget v1, p0, Landroid/graphics/Rect;->bottom:I

    #@e
    if-ge v0, v1, :cond_29

    #@10
    .line 474
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@12
    if-le v0, p1, :cond_16

    #@14
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@16
    .line 475
    :cond_16
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@18
    if-le v0, p2, :cond_1c

    #@1a
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@1c
    .line 476
    :cond_1c
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@1e
    if-ge v0, p3, :cond_22

    #@20
    iput p3, p0, Landroid/graphics/Rect;->right:I

    #@22
    .line 477
    :cond_22
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@24
    if-ge v0, p4, :cond_28

    #@26
    iput p4, p0, Landroid/graphics/Rect;->bottom:I

    #@28
    .line 485
    :cond_28
    :goto_28
    return-void

    #@29
    .line 479
    :cond_29
    iput p1, p0, Landroid/graphics/Rect;->left:I

    #@2b
    .line 480
    iput p2, p0, Landroid/graphics/Rect;->top:I

    #@2d
    .line 481
    iput p3, p0, Landroid/graphics/Rect;->right:I

    #@2f
    .line 482
    iput p4, p0, Landroid/graphics/Rect;->bottom:I

    #@31
    goto :goto_28
.end method

.method public union(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "r"

    #@0
    .prologue
    .line 495
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@4
    iget v2, p1, Landroid/graphics/Rect;->right:I

    #@6
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    #@8
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    #@b
    .line 496
    return-void
.end method

.method public final width()I
    .registers 3

    #@0
    .prologue
    .line 187
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@2
    iget v1, p0, Landroid/graphics/Rect;->left:I

    #@4
    sub-int/2addr v0, v1

    #@5
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 551
    iget v0, p0, Landroid/graphics/Rect;->left:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 552
    iget v0, p0, Landroid/graphics/Rect;->top:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 553
    iget v0, p0, Landroid/graphics/Rect;->right:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 554
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 555
    return-void
.end method
