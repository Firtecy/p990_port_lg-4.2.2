.class public Landroid/graphics/Interpolator;
.super Ljava/lang/Object;
.source "Interpolator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Interpolator$Result;
    }
.end annotation


# instance fields
.field private mFrameCount:I

.field private mValueCount:I

.field private final native_instance:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "valueCount"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 24
    iput p1, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@6
    .line 25
    iput v0, p0, Landroid/graphics/Interpolator;->mFrameCount:I

    #@8
    .line 26
    invoke-static {p1, v0}, Landroid/graphics/Interpolator;->nativeConstructor(II)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@e
    .line 27
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "valueCount"
    .parameter "frameCount"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput p1, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@5
    .line 31
    iput p2, p0, Landroid/graphics/Interpolator;->mFrameCount:I

    #@7
    .line 32
    invoke-static {p1, p2}, Landroid/graphics/Interpolator;->nativeConstructor(II)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@d
    .line 33
    return-void
.end method

.method private static native nativeConstructor(II)I
.end method

.method private static native nativeDestructor(I)V
.end method

.method private static native nativeReset(III)V
.end method

.method private static native nativeSetKeyFrame(III[F[F)V
.end method

.method private static native nativeSetRepeatMirror(IFZ)V
.end method

.method private static native nativeTimeToValues(II[F)I
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 149
    iget v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@2
    invoke-static {v0}, Landroid/graphics/Interpolator;->nativeDestructor(I)V

    #@5
    .line 150
    return-void
.end method

.method public final getKeyFrameCount()I
    .registers 2

    #@0
    .prologue
    .line 56
    iget v0, p0, Landroid/graphics/Interpolator;->mFrameCount:I

    #@2
    return v0
.end method

.method public final getValueCount()I
    .registers 2

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@2
    return v0
.end method

.method public reset(I)V
    .registers 3
    .parameter "valueCount"

    #@0
    .prologue
    .line 41
    const/4 v0, 0x2

    #@1
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Interpolator;->reset(II)V

    #@4
    .line 42
    return-void
.end method

.method public reset(II)V
    .registers 4
    .parameter "valueCount"
    .parameter "frameCount"

    #@0
    .prologue
    .line 50
    iput p1, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@2
    .line 51
    iput p2, p0, Landroid/graphics/Interpolator;->mFrameCount:I

    #@4
    .line 52
    iget v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@6
    invoke-static {v0, p1, p2}, Landroid/graphics/Interpolator;->nativeReset(III)V

    #@9
    .line 53
    return-void
.end method

.method public setKeyFrame(II[F)V
    .registers 5
    .parameter "index"
    .parameter "msec"
    .parameter "values"

    #@0
    .prologue
    .line 74
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/graphics/Interpolator;->setKeyFrame(II[F[F)V

    #@4
    .line 75
    return-void
.end method

.method public setKeyFrame(II[F[F)V
    .registers 7
    .parameter "index"
    .parameter "msec"
    .parameter "values"
    .parameter "blend"

    #@0
    .prologue
    .line 88
    if-ltz p1, :cond_6

    #@2
    iget v0, p0, Landroid/graphics/Interpolator;->mFrameCount:I

    #@4
    if-lt p1, v0, :cond_c

    #@6
    .line 89
    :cond_6
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@8
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@b
    throw v0

    #@c
    .line 91
    :cond_c
    array-length v0, p3

    #@d
    iget v1, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@f
    if-ge v0, v1, :cond_17

    #@11
    .line 92
    new-instance v0, Ljava/lang/ArrayStoreException;

    #@13
    invoke-direct {v0}, Ljava/lang/ArrayStoreException;-><init>()V

    #@16
    throw v0

    #@17
    .line 94
    :cond_17
    if-eqz p4, :cond_23

    #@19
    array-length v0, p4

    #@1a
    const/4 v1, 0x4

    #@1b
    if-ge v0, v1, :cond_23

    #@1d
    .line 95
    new-instance v0, Ljava/lang/ArrayStoreException;

    #@1f
    invoke-direct {v0}, Ljava/lang/ArrayStoreException;-><init>()V

    #@22
    throw v0

    #@23
    .line 97
    :cond_23
    iget v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@25
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Interpolator;->nativeSetKeyFrame(III[F[F)V

    #@28
    .line 98
    return-void
.end method

.method public setRepeatMirror(FZ)V
    .registers 4
    .parameter "repeatCount"
    .parameter "mirror"

    #@0
    .prologue
    .line 106
    const/4 v0, 0x0

    #@1
    cmpl-float v0, p1, v0

    #@3
    if-ltz v0, :cond_a

    #@5
    .line 107
    iget v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@7
    invoke-static {v0, p1, p2}, Landroid/graphics/Interpolator;->nativeSetRepeatMirror(IFZ)V

    #@a
    .line 109
    :cond_a
    return-void
.end method

.method public timeToValues(I[F)Landroid/graphics/Interpolator$Result;
    .registers 5
    .parameter "msec"
    .parameter "values"

    #@0
    .prologue
    .line 137
    if-eqz p2, :cond_d

    #@2
    array-length v0, p2

    #@3
    iget v1, p0, Landroid/graphics/Interpolator;->mValueCount:I

    #@5
    if-ge v0, v1, :cond_d

    #@7
    .line 138
    new-instance v0, Ljava/lang/ArrayStoreException;

    #@9
    invoke-direct {v0}, Ljava/lang/ArrayStoreException;-><init>()V

    #@c
    throw v0

    #@d
    .line 140
    :cond_d
    iget v0, p0, Landroid/graphics/Interpolator;->native_instance:I

    #@f
    invoke-static {v0, p1, p2}, Landroid/graphics/Interpolator;->nativeTimeToValues(II[F)I

    #@12
    move-result v0

    #@13
    packed-switch v0, :pswitch_data_20

    #@16
    .line 143
    sget-object v0, Landroid/graphics/Interpolator$Result;->FREEZE_END:Landroid/graphics/Interpolator$Result;

    #@18
    :goto_18
    return-object v0

    #@19
    .line 141
    :pswitch_19
    sget-object v0, Landroid/graphics/Interpolator$Result;->NORMAL:Landroid/graphics/Interpolator$Result;

    #@1b
    goto :goto_18

    #@1c
    .line 142
    :pswitch_1c
    sget-object v0, Landroid/graphics/Interpolator$Result;->FREEZE_START:Landroid/graphics/Interpolator$Result;

    #@1e
    goto :goto_18

    #@1f
    .line 140
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_19
        :pswitch_1c
    .end packed-switch
.end method

.method public timeToValues([F)Landroid/graphics/Interpolator$Result;
    .registers 4
    .parameter "values"

    #@0
    .prologue
    .line 122
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3
    move-result-wide v0

    #@4
    long-to-int v0, v0

    #@5
    invoke-virtual {p0, v0, p1}, Landroid/graphics/Interpolator;->timeToValues(I[F)Landroid/graphics/Interpolator$Result;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method
