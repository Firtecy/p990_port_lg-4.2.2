.class public Landroid/graphics/Paint;
.super Ljava/lang/Object;
.source "Paint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Paint$FontMetricsInt;,
        Landroid/graphics/Paint$FontMetrics;,
        Landroid/graphics/Paint$Align;,
        Landroid/graphics/Paint$Join;,
        Landroid/graphics/Paint$Cap;,
        Landroid/graphics/Paint$Style;
    }
.end annotation


# static fields
.field public static final ANTI_ALIAS_FLAG:I = 0x1

.field public static final BIDI_DEFAULT_LTR:I = 0x2

.field public static final BIDI_DEFAULT_RTL:I = 0x3

.field private static final BIDI_FLAG_MASK:I = 0x7

.field public static final BIDI_FORCE_LTR:I = 0x4

.field public static final BIDI_FORCE_RTL:I = 0x5

.field public static final BIDI_LTR:I = 0x0

.field private static final BIDI_MAX_FLAG_VALUE:I = 0x5

.field public static final BIDI_RTL:I = 0x1

.field public static final CURSOR_AFTER:I = 0x0

.field public static final CURSOR_AT:I = 0x4

.field public static final CURSOR_AT_OR_AFTER:I = 0x1

.field public static final CURSOR_AT_OR_BEFORE:I = 0x3

.field public static final CURSOR_BEFORE:I = 0x2

.field private static final CURSOR_OPT_MAX_VALUE:I = 0x4

.field static final DEFAULT_PAINT_FLAGS:I = 0x100

.field public static final DEV_KERN_TEXT_FLAG:I = 0x100

.field public static final DIRECTION_LTR:I = 0x0

.field public static final DIRECTION_RTL:I = 0x1

.field public static final DITHER_FLAG:I = 0x4

.field public static final FAKE_BOLD_TEXT_FLAG:I = 0x20

.field public static final FILTER_BITMAP_FLAG:I = 0x2

.field public static final HINTING_OFF:I = 0x0

.field public static final HINTING_ON:I = 0x1

.field public static final LINEAR_TEXT_FLAG:I = 0x40

.field public static final STRIKE_THRU_TEXT_FLAG:I = 0x10

.field public static final SUBPIXEL_TEXT_FLAG:I = 0x80

.field public static final UNDERLINE_TEXT_FLAG:I = 0x8

.field static final sAlignArray:[Landroid/graphics/Paint$Align;

.field static final sCapArray:[Landroid/graphics/Paint$Cap;

.field static final sJoinArray:[Landroid/graphics/Paint$Join;

.field static final sStyleArray:[Landroid/graphics/Paint$Style;


# instance fields
.field public hasShadow:Z

.field public mBidiFlags:I

.field private mColorFilter:Landroid/graphics/ColorFilter;

.field private mCompatScaling:F

.field private mHasCompatScaling:Z

.field private mInvCompatScaling:F

.field private mLocale:Ljava/util/Locale;

.field private mMaskFilter:Landroid/graphics/MaskFilter;

.field public mNativePaint:I

.field private mPathEffect:Landroid/graphics/PathEffect;

.field private mRasterizer:Landroid/graphics/Rasterizer;

.field private mShader:Landroid/graphics/Shader;

.field private mTypeface:Landroid/graphics/Typeface;

.field private mXfermode:Landroid/graphics/Xfermode;

.field public shadowColor:I

.field public shadowDx:F

.field public shadowDy:F

.field public shadowRadius:F


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v4, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 77
    new-array v0, v5, [Landroid/graphics/Paint$Style;

    #@6
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@8
    aput-object v1, v0, v2

    #@a
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    #@c
    aput-object v1, v0, v3

    #@e
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    #@10
    aput-object v1, v0, v4

    #@12
    sput-object v0, Landroid/graphics/Paint;->sStyleArray:[Landroid/graphics/Paint$Style;

    #@14
    .line 80
    new-array v0, v5, [Landroid/graphics/Paint$Cap;

    #@16
    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    #@18
    aput-object v1, v0, v2

    #@1a
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    #@1c
    aput-object v1, v0, v3

    #@1e
    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    #@20
    aput-object v1, v0, v4

    #@22
    sput-object v0, Landroid/graphics/Paint;->sCapArray:[Landroid/graphics/Paint$Cap;

    #@24
    .line 83
    new-array v0, v5, [Landroid/graphics/Paint$Join;

    #@26
    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    #@28
    aput-object v1, v0, v2

    #@2a
    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    #@2c
    aput-object v1, v0, v3

    #@2e
    sget-object v1, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    #@30
    aput-object v1, v0, v4

    #@32
    sput-object v0, Landroid/graphics/Paint;->sJoinArray:[Landroid/graphics/Paint$Join;

    #@34
    .line 86
    new-array v0, v5, [Landroid/graphics/Paint$Align;

    #@36
    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    #@38
    aput-object v1, v0, v2

    #@3a
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    #@3c
    aput-object v1, v0, v3

    #@3e
    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    #@40
    aput-object v1, v0, v4

    #@42
    sput-object v0, Landroid/graphics/Paint;->sAlignArray:[Landroid/graphics/Paint$Align;

    #@44
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 338
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/graphics/Paint;-><init>(I)V

    #@4
    .line 339
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 347
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    const/4 v0, 0x2

    #@4
    iput v0, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@6
    .line 348
    invoke-static {}, Landroid/graphics/Paint;->native_init()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@c
    .line 349
    or-int/lit16 v0, p1, 0x100

    #@e
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setFlags(I)V

    #@11
    .line 354
    const/high16 v0, 0x3f80

    #@13
    iput v0, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@15
    iput v0, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@17
    .line 355
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextLocale(Ljava/util/Locale;)V

    #@1e
    .line 356
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Paint;)V
    .registers 3
    .parameter "paint"

    #@0
    .prologue
    .line 365
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    const/4 v0, 0x2

    #@4
    iput v0, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@6
    .line 366
    iget v0, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@8
    invoke-static {v0}, Landroid/graphics/Paint;->native_initWithPaint(I)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@e
    .line 367
    invoke-direct {p0, p1}, Landroid/graphics/Paint;->setClassVariablesFrom(Landroid/graphics/Paint;)V

    #@11
    .line 368
    return-void
.end method

.method private static native finalizer(I)V
.end method

.method private native nSetShadowLayer(FFFI)V
.end method

.method private static native nativeGetCharArrayBounds(I[CIILandroid/graphics/Rect;)V
.end method

.method private static native nativeGetStringBounds(ILjava/lang/String;IILandroid/graphics/Rect;)V
.end method

.method private native native_breakText(Ljava/lang/String;ZF[F)I
.end method

.method private native native_breakText([CIIF[F)I
.end method

.method private static native native_getFillPath(III)Z
.end method

.method private static native native_getStrokeCap(I)I
.end method

.method private static native native_getStrokeJoin(I)I
.end method

.method private static native native_getStyle(I)I
.end method

.method private static native native_getTextAlign(I)I
.end method

.method private static native native_getTextGlyphs(ILjava/lang/String;IIIII[C)I
.end method

.method private static native native_getTextPath(IILjava/lang/String;IIFFI)V
.end method

.method private static native native_getTextPath(II[CIIFFI)V
.end method

.method private static native native_getTextRunAdvances(ILjava/lang/String;IIIII[FII)F
.end method

.method private static native native_getTextRunAdvances(I[CIIIII[FII)F
.end method

.method private native native_getTextRunCursor(ILjava/lang/String;IIIII)I
.end method

.method private native native_getTextRunCursor(I[CIIIII)I
.end method

.method private static native native_getTextWidths(ILjava/lang/String;II[F)I
.end method

.method private static native native_getTextWidths(I[CII[F)I
.end method

.method private static native native_init()I
.end method

.method private static native native_initWithPaint(I)I
.end method

.method private native native_measureText(Ljava/lang/String;)F
.end method

.method private native native_measureText(Ljava/lang/String;II)F
.end method

.method private native native_measureText([CII)F
.end method

.method private static native native_reset(I)V
.end method

.method private static native native_set(II)V
.end method

.method private static native native_setColorFilter(II)I
.end method

.method private static native native_setMaskFilter(II)I
.end method

.method private static native native_setPathEffect(II)I
.end method

.method private static native native_setRasterizer(II)I
.end method

.method private static native native_setShader(II)I
.end method

.method private static native native_setStrokeCap(II)V
.end method

.method private static native native_setStrokeJoin(II)V
.end method

.method private static native native_setStyle(II)V
.end method

.method private static native native_setTextAlign(II)V
.end method

.method private static native native_setTextLocale(ILjava/lang/String;)V
.end method

.method private static native native_setTypeface(II)I
.end method

.method private static native native_setXfermode(II)I
.end method

.method private setClassVariablesFrom(Landroid/graphics/Paint;)V
    .registers 3
    .parameter "paint"

    #@0
    .prologue
    .line 420
    iget-object v0, p1, Landroid/graphics/Paint;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    iput-object v0, p0, Landroid/graphics/Paint;->mColorFilter:Landroid/graphics/ColorFilter;

    #@4
    .line 421
    iget-object v0, p1, Landroid/graphics/Paint;->mMaskFilter:Landroid/graphics/MaskFilter;

    #@6
    iput-object v0, p0, Landroid/graphics/Paint;->mMaskFilter:Landroid/graphics/MaskFilter;

    #@8
    .line 422
    iget-object v0, p1, Landroid/graphics/Paint;->mPathEffect:Landroid/graphics/PathEffect;

    #@a
    iput-object v0, p0, Landroid/graphics/Paint;->mPathEffect:Landroid/graphics/PathEffect;

    #@c
    .line 423
    iget-object v0, p1, Landroid/graphics/Paint;->mRasterizer:Landroid/graphics/Rasterizer;

    #@e
    iput-object v0, p0, Landroid/graphics/Paint;->mRasterizer:Landroid/graphics/Rasterizer;

    #@10
    .line 424
    iget-object v0, p1, Landroid/graphics/Paint;->mShader:Landroid/graphics/Shader;

    #@12
    iput-object v0, p0, Landroid/graphics/Paint;->mShader:Landroid/graphics/Shader;

    #@14
    .line 425
    iget-object v0, p1, Landroid/graphics/Paint;->mTypeface:Landroid/graphics/Typeface;

    #@16
    iput-object v0, p0, Landroid/graphics/Paint;->mTypeface:Landroid/graphics/Typeface;

    #@18
    .line 426
    iget-object v0, p1, Landroid/graphics/Paint;->mXfermode:Landroid/graphics/Xfermode;

    #@1a
    iput-object v0, p0, Landroid/graphics/Paint;->mXfermode:Landroid/graphics/Xfermode;

    #@1c
    .line 428
    iget-boolean v0, p1, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@1e
    iput-boolean v0, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@20
    .line 429
    iget v0, p1, Landroid/graphics/Paint;->mCompatScaling:F

    #@22
    iput v0, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@24
    .line 430
    iget v0, p1, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@26
    iput v0, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@28
    .line 432
    iget-boolean v0, p1, Landroid/graphics/Paint;->hasShadow:Z

    #@2a
    iput-boolean v0, p0, Landroid/graphics/Paint;->hasShadow:Z

    #@2c
    .line 433
    iget v0, p1, Landroid/graphics/Paint;->shadowDx:F

    #@2e
    iput v0, p0, Landroid/graphics/Paint;->shadowDx:F

    #@30
    .line 434
    iget v0, p1, Landroid/graphics/Paint;->shadowDy:F

    #@32
    iput v0, p0, Landroid/graphics/Paint;->shadowDy:F

    #@34
    .line 435
    iget v0, p1, Landroid/graphics/Paint;->shadowRadius:F

    #@36
    iput v0, p0, Landroid/graphics/Paint;->shadowRadius:F

    #@38
    .line 436
    iget v0, p1, Landroid/graphics/Paint;->shadowColor:I

    #@3a
    iput v0, p0, Landroid/graphics/Paint;->shadowColor:I

    #@3c
    .line 438
    iget v0, p1, Landroid/graphics/Paint;->mBidiFlags:I

    #@3e
    iput v0, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@40
    .line 439
    iget-object v0, p1, Landroid/graphics/Paint;->mLocale:Ljava/util/Locale;

    #@42
    iput-object v0, p0, Landroid/graphics/Paint;->mLocale:Ljava/util/Locale;

    #@44
    .line 440
    return-void
.end method


# virtual methods
.method public native ascent()F
.end method

.method public breakText(Ljava/lang/CharSequence;IIZF[F)I
    .registers 14
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "measureForwards"
    .parameter "maxWidth"
    .parameter "measuredWidth"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1468
    if-nez p1, :cond_c

    #@3
    .line 1469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v2, "text cannot be null"

    #@8
    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1471
    :cond_c
    or-int v0, p2, p3

    #@e
    sub-int v3, p3, p2

    #@10
    or-int/2addr v0, v3

    #@11
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v3

    #@15
    sub-int/2addr v3, p3

    #@16
    or-int/2addr v0, v3

    #@17
    if-gez v0, :cond_1f

    #@19
    .line 1472
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@1b
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1e
    throw v0

    #@1f
    .line 1475
    :cond_1f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@22
    move-result v0

    #@23
    if-eqz v0, :cond_27

    #@25
    if-ne p2, p3, :cond_29

    #@27
    :cond_27
    move v6, v2

    #@28
    .line 1495
    .end local p1
    :goto_28
    return v6

    #@29
    .line 1478
    .restart local p1
    :cond_29
    if-nez p2, :cond_3c

    #@2b
    instance-of v0, p1, Ljava/lang/String;

    #@2d
    if-eqz v0, :cond_3c

    #@2f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@32
    move-result v0

    #@33
    if-ne p3, v0, :cond_3c

    #@35
    .line 1479
    check-cast p1, Ljava/lang/String;

    #@37
    .end local p1
    invoke-virtual {p0, p1, p4, p5, p6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    #@3a
    move-result v6

    #@3b
    goto :goto_28

    #@3c
    .line 1483
    .restart local p1
    :cond_3c
    sub-int v0, p3, p2

    #@3e
    invoke-static {v0}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@41
    move-result-object v1

    #@42
    .line 1486
    .local v1, buf:[C
    invoke-static {p1, p2, p3, v1, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@45
    .line 1488
    if-eqz p4, :cond_54

    #@47
    .line 1489
    sub-int v3, p3, p2

    #@49
    move-object v0, p0

    #@4a
    move v4, p5

    #@4b
    move-object v5, p6

    #@4c
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Paint;->breakText([CIIF[F)I

    #@4f
    move-result v6

    #@50
    .line 1494
    .local v6, result:I
    :goto_50
    invoke-static {v1}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@53
    goto :goto_28

    #@54
    .line 1491
    .end local v6           #result:I
    :cond_54
    sub-int v0, p3, p2

    #@56
    neg-int v3, v0

    #@57
    move-object v0, p0

    #@58
    move v4, p5

    #@59
    move-object v5, p6

    #@5a
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Paint;->breakText([CIIF[F)I

    #@5d
    move-result v6

    #@5e
    .restart local v6       #result:I
    goto :goto_50
.end method

.method public breakText(Ljava/lang/String;ZF[F)I
    .registers 10
    .parameter "text"
    .parameter "measureForwards"
    .parameter "maxWidth"
    .parameter "measuredWidth"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1516
    if-nez p1, :cond_c

    #@3
    .line 1517
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v3, "text cannot be null"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 1520
    :cond_c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v3

    #@10
    if-nez v3, :cond_14

    #@12
    move v1, v2

    #@13
    .line 1533
    :cond_13
    :goto_13
    return v1

    #@14
    .line 1523
    :cond_14
    iget-boolean v3, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@16
    if-nez v3, :cond_1d

    #@18
    .line 1524
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/graphics/Paint;->native_breakText(Ljava/lang/String;ZF[F)I

    #@1b
    move-result v1

    #@1c
    goto :goto_13

    #@1d
    .line 1527
    :cond_1d
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@20
    move-result v0

    #@21
    .line 1528
    .local v0, oldSize:F
    iget v3, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@23
    mul-float/2addr v3, v0

    #@24
    invoke-virtual {p0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    #@27
    .line 1529
    iget v3, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@29
    mul-float/2addr v3, p3

    #@2a
    invoke-direct {p0, p1, p2, v3, p4}, Landroid/graphics/Paint;->native_breakText(Ljava/lang/String;ZF[F)I

    #@2d
    move-result v1

    #@2e
    .line 1531
    .local v1, res:I
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    #@31
    .line 1532
    if-eqz p4, :cond_13

    #@33
    aget v3, p4, v2

    #@35
    iget v4, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@37
    mul-float/2addr v3, v4

    #@38
    aput v3, p4, v2

    #@3a
    goto :goto_13
.end method

.method public breakText([CIIF[F)I
    .registers 15
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "maxWidth"
    .parameter "measuredWidth"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1423
    if-nez p1, :cond_c

    #@3
    .line 1424
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v1, "text cannot be null"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 1426
    :cond_c
    if-ltz p2, :cond_16

    #@e
    array-length v0, p1

    #@f
    sub-int/2addr v0, p2

    #@10
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    #@13
    move-result v1

    #@14
    if-ge v0, v1, :cond_1c

    #@16
    .line 1427
    :cond_16
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@18
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@1b
    throw v0

    #@1c
    .line 1430
    :cond_1c
    array-length v0, p1

    #@1d
    if-eqz v0, :cond_21

    #@1f
    if-nez p3, :cond_23

    #@21
    :cond_21
    move v7, v8

    #@22
    .line 1443
    :cond_22
    :goto_22
    return v7

    #@23
    .line 1433
    :cond_23
    iget-boolean v0, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@25
    if-nez v0, :cond_2c

    #@27
    .line 1434
    invoke-direct/range {p0 .. p5}, Landroid/graphics/Paint;->native_breakText([CIIF[F)I

    #@2a
    move-result v7

    #@2b
    goto :goto_22

    #@2c
    .line 1437
    :cond_2c
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@2f
    move-result v6

    #@30
    .line 1438
    .local v6, oldSize:F
    iget v0, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@32
    mul-float/2addr v0, v6

    #@33
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    #@36
    .line 1439
    iget v0, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@38
    mul-float v4, p4, v0

    #@3a
    move-object v0, p0

    #@3b
    move-object v1, p1

    #@3c
    move v2, p2

    #@3d
    move v3, p3

    #@3e
    move-object v5, p5

    #@3f
    invoke-direct/range {v0 .. v5}, Landroid/graphics/Paint;->native_breakText([CIIF[F)I

    #@42
    move-result v7

    #@43
    .line 1441
    .local v7, res:I
    invoke-virtual {p0, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    #@46
    .line 1442
    if-eqz p5, :cond_22

    #@48
    aget v0, p5, v8

    #@4a
    iget v1, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@4c
    mul-float/2addr v0, v1

    #@4d
    aput v0, p5, v8

    #@4f
    goto :goto_22
.end method

.method public clearShadowLayer()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 1044
    iput-boolean v1, p0, Landroid/graphics/Paint;->hasShadow:Z

    #@4
    .line 1045
    invoke-direct {p0, v0, v0, v0, v1}, Landroid/graphics/Paint;->nSetShadowLayer(FFFI)V

    #@7
    .line 1046
    return-void
.end method

.method public native descent()F
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 2179
    :try_start_0
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    invoke-static {v0}, Landroid/graphics/Paint;->finalizer(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 2181
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 2183
    return-void

    #@9
    .line 2181
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public native getAlpha()I
.end method

.method public getBidiFlags()I
    .registers 2

    #@0
    .prologue
    .line 461
    iget v0, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@2
    return v0
.end method

.method public native getColor()I
.end method

.method public getColorFilter()Landroid/graphics/ColorFilter;
    .registers 2

    #@0
    .prologue
    .line 860
    iget-object v0, p0, Landroid/graphics/Paint;->mColorFilter:Landroid/graphics/ColorFilter;

    #@2
    return-object v0
.end method

.method public getFillPath(Landroid/graphics/Path;Landroid/graphics/Path;)Z
    .registers 6
    .parameter "src"
    .parameter "dst"

    #@0
    .prologue
    .line 824
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@5
    move-result v1

    #@6
    invoke-virtual {p2}, Landroid/graphics/Path;->ni()I

    #@9
    move-result v2

    #@a
    invoke-static {v0, v1, v2}, Landroid/graphics/Paint;->native_getFillPath(III)Z

    #@d
    move-result v0

    #@e
    return v0
.end method

.method public native getFlags()I
.end method

.method public native getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F
.end method

.method public getFontMetrics()Landroid/graphics/Paint$FontMetrics;
    .registers 2

    #@0
    .prologue
    .line 1228
    new-instance v0, Landroid/graphics/Paint$FontMetrics;

    #@2
    invoke-direct {v0}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    #@5
    .line 1229
    .local v0, fm:Landroid/graphics/Paint$FontMetrics;
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    #@8
    .line 1230
    return-object v0
.end method

.method public native getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I
.end method

.method public getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;
    .registers 2

    #@0
    .prologue
    .line 1264
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    #@2
    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    #@5
    .line 1265
    .local v0, fm:Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@8
    .line 1266
    return-object v0
.end method

.method public getFontSpacing()F
    .registers 2

    #@0
    .prologue
    .line 1277
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public native getHinting()I
.end method

.method public getMaskFilter()Landroid/graphics/MaskFilter;
    .registers 2

    #@0
    .prologue
    .line 939
    iget-object v0, p0, Landroid/graphics/Paint;->mMaskFilter:Landroid/graphics/MaskFilter;

    #@2
    return-object v0
.end method

.method public getPathEffect()Landroid/graphics/PathEffect;
    .registers 2

    #@0
    .prologue
    .line 911
    iget-object v0, p0, Landroid/graphics/Paint;->mPathEffect:Landroid/graphics/PathEffect;

    #@2
    return-object v0
.end method

.method public getRasterizer()Landroid/graphics/Rasterizer;
    .registers 2

    #@0
    .prologue
    .line 1001
    iget-object v0, p0, Landroid/graphics/Paint;->mRasterizer:Landroid/graphics/Rasterizer;

    #@2
    return-object v0
.end method

.method public getShader()Landroid/graphics/Shader;
    .registers 2

    #@0
    .prologue
    .line 833
    iget-object v0, p0, Landroid/graphics/Paint;->mShader:Landroid/graphics/Shader;

    #@2
    return-object v0
.end method

.method public getStrokeCap()Landroid/graphics/Paint$Cap;
    .registers 3

    #@0
    .prologue
    .line 780
    sget-object v0, Landroid/graphics/Paint;->sCapArray:[Landroid/graphics/Paint$Cap;

    #@2
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v1}, Landroid/graphics/Paint;->native_getStrokeCap(I)I

    #@7
    move-result v1

    #@8
    aget-object v0, v0, v1

    #@a
    return-object v0
.end method

.method public getStrokeJoin()Landroid/graphics/Paint$Join;
    .registers 3

    #@0
    .prologue
    .line 799
    sget-object v0, Landroid/graphics/Paint;->sJoinArray:[Landroid/graphics/Paint$Join;

    #@2
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v1}, Landroid/graphics/Paint;->native_getStrokeJoin(I)I

    #@7
    move-result v1

    #@8
    aget-object v0, v0, v1

    #@a
    return-object v0
.end method

.method public native getStrokeMiter()F
.end method

.method public native getStrokeWidth()F
.end method

.method public getStyle()Landroid/graphics/Paint$Style;
    .registers 3

    #@0
    .prologue
    .line 669
    sget-object v0, Landroid/graphics/Paint;->sStyleArray:[Landroid/graphics/Paint$Style;

    #@2
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v1}, Landroid/graphics/Paint;->native_getStyle(I)I

    #@7
    move-result v1

    #@8
    aget-object v0, v0, v1

    #@a
    return-object v0
.end method

.method public getTextAlign()Landroid/graphics/Paint$Align;
    .registers 3

    #@0
    .prologue
    .line 1057
    sget-object v0, Landroid/graphics/Paint;->sAlignArray:[Landroid/graphics/Paint$Align;

    #@2
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v1}, Landroid/graphics/Paint;->native_getTextAlign(I)I

    #@7
    move-result v1

    #@8
    aget-object v0, v0, v1

    #@a
    return-object v0
.end method

.method public getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V
    .registers 7
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "bounds"

    #@0
    .prologue
    .line 2147
    or-int v0, p2, p3

    #@2
    sub-int v1, p3, p2

    #@4
    or-int/2addr v0, v1

    #@5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v1

    #@9
    sub-int/2addr v1, p3

    #@a
    or-int/2addr v0, v1

    #@b
    if-gez v0, :cond_13

    #@d
    .line 2148
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@f
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@12
    throw v0

    #@13
    .line 2150
    :cond_13
    if-nez p4, :cond_1e

    #@15
    .line 2151
    new-instance v0, Ljava/lang/NullPointerException;

    #@17
    const-string/jumbo v1, "need bounds Rect"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 2153
    :cond_1e
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@20
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Paint;->nativeGetStringBounds(ILjava/lang/String;IILandroid/graphics/Rect;)V

    #@23
    .line 2154
    return-void
.end method

.method public getTextBounds([CIILandroid/graphics/Rect;)V
    .registers 7
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "bounds"

    #@0
    .prologue
    .line 2167
    or-int v0, p2, p3

    #@2
    if-ltz v0, :cond_9

    #@4
    add-int v0, p2, p3

    #@6
    array-length v1, p1

    #@7
    if-le v0, v1, :cond_f

    #@9
    .line 2168
    :cond_9
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v0

    #@f
    .line 2170
    :cond_f
    if-nez p4, :cond_1a

    #@11
    .line 2171
    new-instance v0, Ljava/lang/NullPointerException;

    #@13
    const-string/jumbo v1, "need bounds Rect"

    #@16
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 2173
    :cond_1a
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@1c
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Paint;->nativeGetCharArrayBounds(I[CIILandroid/graphics/Rect;)V

    #@1f
    .line 2174
    return-void
.end method

.method public getTextGlyphs(Ljava/lang/String;IIIII[C)I
    .registers 16
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "glyphs"

    #@0
    .prologue
    .line 1692
    if-nez p1, :cond_b

    #@2
    .line 1693
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "text cannot be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 1695
    :cond_b
    if-eqz p6, :cond_2a

    #@d
    const/4 v0, 0x1

    #@e
    if-eq p6, v0, :cond_2a

    #@10
    .line 1696
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string/jumbo v2, "unknown flags value: "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@29
    throw v0

    #@2a
    .line 1698
    :cond_2a
    or-int v0, p2, p3

    #@2c
    or-int/2addr v0, p4

    #@2d
    or-int/2addr v0, p5

    #@2e
    sub-int v1, p3, p2

    #@30
    or-int/2addr v0, v1

    #@31
    sub-int v1, p2, p4

    #@33
    or-int/2addr v0, v1

    #@34
    sub-int v1, p5, p3

    #@36
    or-int/2addr v0, v1

    #@37
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3a
    move-result v1

    #@3b
    sub-int/2addr v1, p3

    #@3c
    or-int/2addr v0, v1

    #@3d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@40
    move-result v1

    #@41
    sub-int/2addr v1, p5

    #@42
    or-int/2addr v0, v1

    #@43
    if-gez v0, :cond_4b

    #@45
    .line 1701
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@47
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@4a
    throw v0

    #@4b
    .line 1703
    :cond_4b
    sub-int v0, p3, p2

    #@4d
    array-length v1, p7

    #@4e
    if-le v0, v1, :cond_56

    #@50
    .line 1704
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@52
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@55
    throw v0

    #@56
    .line 1706
    :cond_56
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@58
    move-object v1, p1

    #@59
    move v2, p2

    #@5a
    move v3, p3

    #@5b
    move v4, p4

    #@5c
    move v5, p5

    #@5d
    move v6, p6

    #@5e
    move-object v7, p7

    #@5f
    invoke-static/range {v0 .. v7}, Landroid/graphics/Paint;->native_getTextGlyphs(ILjava/lang/String;IIIII[C)I

    #@62
    move-result v0

    #@63
    return v0
.end method

.method public getTextLocale()Ljava/util/Locale;
    .registers 2

    #@0
    .prologue
    .line 1078
    iget-object v0, p0, Landroid/graphics/Paint;->mLocale:Ljava/util/Locale;

    #@2
    return-object v0
.end method

.method public getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V
    .registers 15
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "path"

    #@0
    .prologue
    .line 2129
    or-int v0, p2, p3

    #@2
    sub-int v1, p3, p2

    #@4
    or-int/2addr v0, v1

    #@5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v1

    #@9
    sub-int/2addr v1, p3

    #@a
    or-int/2addr v0, v1

    #@b
    if-gez v0, :cond_13

    #@d
    .line 2130
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@f
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@12
    throw v0

    #@13
    .line 2132
    :cond_13
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@15
    iget v1, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@17
    invoke-virtual {p6}, Landroid/graphics/Path;->ni()I

    #@1a
    move-result v7

    #@1b
    move-object v2, p1

    #@1c
    move v3, p2

    #@1d
    move v4, p3

    #@1e
    move v5, p4

    #@1f
    move v6, p5

    #@20
    invoke-static/range {v0 .. v7}, Landroid/graphics/Paint;->native_getTextPath(IILjava/lang/String;IIFFI)V

    #@23
    .line 2134
    return-void
.end method

.method public getTextPath([CIIFFLandroid/graphics/Path;)V
    .registers 15
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "x"
    .parameter "y"
    .parameter "path"

    #@0
    .prologue
    .line 2107
    or-int v0, p2, p3

    #@2
    if-ltz v0, :cond_9

    #@4
    add-int v0, p2, p3

    #@6
    array-length v1, p1

    #@7
    if-le v0, v1, :cond_f

    #@9
    .line 2108
    :cond_9
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v0

    #@f
    .line 2110
    :cond_f
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@11
    iget v1, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@13
    invoke-virtual {p6}, Landroid/graphics/Path;->ni()I

    #@16
    move-result v7

    #@17
    move-object v2, p1

    #@18
    move v3, p2

    #@19
    move v4, p3

    #@1a
    move v5, p4

    #@1b
    move v6, p5

    #@1c
    invoke-static/range {v0 .. v7}, Landroid/graphics/Paint;->native_getTextPath(II[CIIFFI)V

    #@1f
    .line 2112
    return-void
.end method

.method public getTextRunAdvances(Ljava/lang/CharSequence;IIIII[FI)F
    .registers 19
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"

    #@0
    .prologue
    .line 1782
    const/4 v9, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move/from16 v6, p6

    #@9
    move-object/from16 v7, p7

    #@b
    move/from16 v8, p8

    #@d
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances(Ljava/lang/CharSequence;IIIII[FII)F

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public getTextRunAdvances(Ljava/lang/CharSequence;IIIII[FII)F
    .registers 23
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"
    .parameter "reserved"

    #@0
    .prologue
    .line 1797
    if-nez p1, :cond_b

    #@2
    .line 1798
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "text cannot be null"

    #@7
    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1800
    :cond_b
    or-int v2, p2, p3

    #@d
    or-int v2, v2, p4

    #@f
    or-int v2, v2, p5

    #@11
    or-int v2, v2, p8

    #@13
    sub-int v4, p3, p2

    #@15
    or-int/2addr v2, v4

    #@16
    sub-int v4, p2, p4

    #@18
    or-int/2addr v2, v4

    #@19
    sub-int v4, p5, p3

    #@1b
    or-int/2addr v2, v4

    #@1c
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@1f
    move-result v4

    #@20
    sub-int v4, v4, p5

    #@22
    or-int/2addr v4, v2

    #@23
    if-nez p7, :cond_2f

    #@25
    const/4 v2, 0x0

    #@26
    :goto_26
    or-int/2addr v2, v4

    #@27
    if-gez v2, :cond_38

    #@29
    .line 1805
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@2b
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@2e
    throw v2

    #@2f
    .line 1800
    :cond_2f
    move-object/from16 v0, p7

    #@31
    array-length v2, v0

    #@32
    sub-int v2, v2, p8

    #@34
    sub-int v6, p3, p2

    #@36
    sub-int/2addr v2, v6

    #@37
    goto :goto_26

    #@38
    .line 1808
    :cond_38
    instance-of v2, p1, Ljava/lang/String;

    #@3a
    if-eqz v2, :cond_54

    #@3c
    move-object v3, p1

    #@3d
    .line 1809
    check-cast v3, Ljava/lang/String;

    #@3f
    move-object v2, p0

    #@40
    move v4, p2

    #@41
    move/from16 v5, p3

    #@43
    move/from16 v6, p4

    #@45
    move/from16 v7, p5

    #@47
    move/from16 v8, p6

    #@49
    move-object/from16 v9, p7

    #@4b
    move/from16 v10, p8

    #@4d
    move/from16 v11, p9

    #@4f
    invoke-virtual/range {v2 .. v11}, Landroid/graphics/Paint;->getTextRunAdvances(Ljava/lang/String;IIIII[FII)F

    #@52
    move-result v12

    #@53
    .line 1832
    :goto_53
    return v12

    #@54
    .line 1812
    :cond_54
    instance-of v2, p1, Landroid/text/SpannedString;

    #@56
    if-nez v2, :cond_5c

    #@58
    instance-of v2, p1, Landroid/text/SpannableString;

    #@5a
    if-eqz v2, :cond_75

    #@5c
    .line 1814
    :cond_5c
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5f
    move-result-object v3

    #@60
    move-object v2, p0

    #@61
    move v4, p2

    #@62
    move/from16 v5, p3

    #@64
    move/from16 v6, p4

    #@66
    move/from16 v7, p5

    #@68
    move/from16 v8, p6

    #@6a
    move-object/from16 v9, p7

    #@6c
    move/from16 v10, p8

    #@6e
    move/from16 v11, p9

    #@70
    invoke-virtual/range {v2 .. v11}, Landroid/graphics/Paint;->getTextRunAdvances(Ljava/lang/String;IIIII[FII)F

    #@73
    move-result v12

    #@74
    goto :goto_53

    #@75
    .line 1817
    :cond_75
    instance-of v2, p1, Landroid/text/GraphicsOperations;

    #@77
    if-eqz v2, :cond_8f

    #@79
    move-object v2, p1

    #@7a
    .line 1818
    check-cast v2, Landroid/text/GraphicsOperations;

    #@7c
    move v3, p2

    #@7d
    move/from16 v4, p3

    #@7f
    move/from16 v5, p4

    #@81
    move/from16 v6, p5

    #@83
    move/from16 v7, p6

    #@85
    move-object/from16 v8, p7

    #@87
    move/from16 v9, p8

    #@89
    move-object v10, p0

    #@8a
    invoke-interface/range {v2 .. v10}, Landroid/text/GraphicsOperations;->getTextRunAdvances(IIIII[FILandroid/graphics/Paint;)F

    #@8d
    move-result v12

    #@8e
    goto :goto_53

    #@8f
    .line 1821
    :cond_8f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@92
    move-result v2

    #@93
    if-eqz v2, :cond_99

    #@95
    move/from16 v0, p3

    #@97
    if-ne v0, p2, :cond_9b

    #@99
    .line 1822
    :cond_99
    const/4 v12, 0x0

    #@9a
    goto :goto_53

    #@9b
    .line 1825
    :cond_9b
    sub-int v7, p5, p4

    #@9d
    .line 1826
    .local v7, contextLen:I
    sub-int v5, p3, p2

    #@9f
    .line 1827
    .local v5, len:I
    invoke-static {v7}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@a2
    move-result-object v3

    #@a3
    .line 1828
    .local v3, buf:[C
    const/4 v2, 0x0

    #@a4
    move/from16 v0, p4

    #@a6
    move/from16 v1, p5

    #@a8
    invoke-static {p1, v0, v1, v3, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@ab
    .line 1829
    sub-int v4, p2, p4

    #@ad
    const/4 v6, 0x0

    #@ae
    move-object v2, p0

    #@af
    move/from16 v8, p6

    #@b1
    move-object/from16 v9, p7

    #@b3
    move/from16 v10, p8

    #@b5
    move/from16 v11, p9

    #@b7
    invoke-virtual/range {v2 .. v11}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@ba
    move-result v12

    #@bb
    .line 1831
    .local v12, result:F
    invoke-static {v3}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@be
    goto :goto_53
.end method

.method public getTextRunAdvances(Ljava/lang/String;IIIII[FI)F
    .registers 19
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"

    #@0
    .prologue
    .line 1879
    const/4 v9, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move/from16 v6, p6

    #@9
    move-object/from16 v7, p7

    #@b
    move/from16 v8, p8

    #@d
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances(Ljava/lang/String;IIIII[FII)F

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public getTextRunAdvances(Ljava/lang/String;IIIII[FII)F
    .registers 26
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"
    .parameter "reserved"

    #@0
    .prologue
    .line 1929
    if-nez p1, :cond_b

    #@2
    .line 1930
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "text cannot be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1932
    :cond_b
    if-eqz p6, :cond_2e

    #@d
    const/4 v2, 0x1

    #@e
    move/from16 v0, p6

    #@10
    if-eq v0, v2, :cond_2e

    #@12
    .line 1933
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string/jumbo v4, "unknown flags value: "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    move/from16 v0, p6

    #@22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v2

    #@2e
    .line 1935
    :cond_2e
    or-int v2, p2, p3

    #@30
    or-int v2, v2, p4

    #@32
    or-int v2, v2, p5

    #@34
    or-int v2, v2, p8

    #@36
    sub-int v3, p3, p2

    #@38
    or-int/2addr v2, v3

    #@39
    sub-int v3, p2, p4

    #@3b
    or-int/2addr v2, v3

    #@3c
    sub-int v3, p5, p3

    #@3e
    or-int/2addr v2, v3

    #@3f
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@42
    move-result v3

    #@43
    sub-int v3, v3, p5

    #@45
    or-int/2addr v3, v2

    #@46
    if-nez p7, :cond_52

    #@48
    const/4 v2, 0x0

    #@49
    :goto_49
    or-int/2addr v2, v3

    #@4a
    if-gez v2, :cond_5b

    #@4c
    .line 1940
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@4e
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@51
    throw v2

    #@52
    .line 1935
    :cond_52
    move-object/from16 v0, p7

    #@54
    array-length v2, v0

    #@55
    sub-int v2, v2, p8

    #@57
    sub-int v4, p3, p2

    #@59
    sub-int/2addr v2, v4

    #@5a
    goto :goto_49

    #@5b
    .line 1943
    :cond_5b
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    #@5e
    move-result v2

    #@5f
    if-eqz v2, :cond_67

    #@61
    move/from16 v0, p2

    #@63
    move/from16 v1, p3

    #@65
    if-ne v0, v1, :cond_69

    #@67
    .line 1944
    :cond_67
    const/4 v2, 0x0

    #@68
    .line 1963
    :goto_68
    return v2

    #@69
    .line 1947
    :cond_69
    move-object/from16 v0, p0

    #@6b
    iget-boolean v2, v0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@6d
    if-nez v2, :cond_8a

    #@6f
    .line 1948
    move-object/from16 v0, p0

    #@71
    iget v2, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@73
    move-object/from16 v3, p1

    #@75
    move/from16 v4, p2

    #@77
    move/from16 v5, p3

    #@79
    move/from16 v6, p4

    #@7b
    move/from16 v7, p5

    #@7d
    move/from16 v8, p6

    #@7f
    move-object/from16 v9, p7

    #@81
    move/from16 v10, p8

    #@83
    move/from16 v11, p9

    #@85
    invoke-static/range {v2 .. v11}, Landroid/graphics/Paint;->native_getTextRunAdvances(ILjava/lang/String;IIIII[FII)F

    #@88
    move-result v2

    #@89
    goto :goto_68

    #@8a
    .line 1952
    :cond_8a
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Paint;->getTextSize()F

    #@8d
    move-result v14

    #@8e
    .line 1953
    .local v14, oldSize:F
    move-object/from16 v0, p0

    #@90
    iget v2, v0, Landroid/graphics/Paint;->mCompatScaling:F

    #@92
    mul-float/2addr v2, v14

    #@93
    move-object/from16 v0, p0

    #@95
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@98
    .line 1954
    move-object/from16 v0, p0

    #@9a
    iget v2, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@9c
    move-object/from16 v3, p1

    #@9e
    move/from16 v4, p2

    #@a0
    move/from16 v5, p3

    #@a2
    move/from16 v6, p4

    #@a4
    move/from16 v7, p5

    #@a6
    move/from16 v8, p6

    #@a8
    move-object/from16 v9, p7

    #@aa
    move/from16 v10, p8

    #@ac
    move/from16 v11, p9

    #@ae
    invoke-static/range {v2 .. v11}, Landroid/graphics/Paint;->native_getTextRunAdvances(ILjava/lang/String;IIIII[FII)F

    #@b1
    move-result v15

    #@b2
    .line 1956
    .local v15, totalAdvance:F
    move-object/from16 v0, p0

    #@b4
    invoke-virtual {v0, v14}, Landroid/graphics/Paint;->setTextSize(F)V

    #@b7
    .line 1958
    if-eqz p7, :cond_cd

    #@b9
    .line 1959
    move/from16 v13, p8

    #@bb
    .local v13, i:I
    sub-int v2, p3, p2

    #@bd
    add-int v12, v13, v2

    #@bf
    .local v12, e:I
    :goto_bf
    if-ge v13, v12, :cond_cd

    #@c1
    .line 1960
    aget v2, p7, v13

    #@c3
    move-object/from16 v0, p0

    #@c5
    iget v3, v0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@c7
    mul-float/2addr v2, v3

    #@c8
    aput v2, p7, v13

    #@ca
    .line 1959
    add-int/lit8 v13, v13, 0x1

    #@cc
    goto :goto_bf

    #@cd
    .line 1963
    .end local v12           #e:I
    .end local v13           #i:I
    :cond_cd
    move-object/from16 v0, p0

    #@cf
    iget v2, v0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@d1
    mul-float/2addr v2, v15

    #@d2
    goto :goto_68
.end method

.method public getTextRunAdvances([CIIIII[FI)F
    .registers 19
    .parameter "chars"
    .parameter "index"
    .parameter "count"
    .parameter "contextIndex"
    .parameter "contextCount"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"

    #@0
    .prologue
    .line 1720
    const/4 v9, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move/from16 v6, p6

    #@9
    move-object/from16 v7, p7

    #@b
    move/from16 v8, p8

    #@d
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public getTextRunAdvances([CIIIII[FII)F
    .registers 25
    .parameter "chars"
    .parameter "index"
    .parameter "count"
    .parameter "contextIndex"
    .parameter "contextCount"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesIndex"
    .parameter "reserved"

    #@0
    .prologue
    .line 1735
    if-nez p1, :cond_b

    #@2
    .line 1736
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "text cannot be null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1738
    :cond_b
    if-eqz p6, :cond_2e

    #@d
    const/4 v1, 0x1

    #@e
    move/from16 v0, p6

    #@10
    if-eq v0, v1, :cond_2e

    #@12
    .line 1739
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string/jumbo v3, "unknown flags value: "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    move/from16 v0, p6

    #@22
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2d
    throw v1

    #@2e
    .line 1741
    :cond_2e
    or-int v1, p2, p3

    #@30
    or-int v1, v1, p4

    #@32
    or-int v1, v1, p5

    #@34
    or-int v1, v1, p8

    #@36
    sub-int v2, p2, p4

    #@38
    or-int/2addr v1, v2

    #@39
    sub-int v2, p5, p3

    #@3b
    or-int/2addr v1, v2

    #@3c
    add-int v2, p4, p5

    #@3e
    add-int v3, p2, p3

    #@40
    sub-int/2addr v2, v3

    #@41
    or-int/2addr v1, v2

    #@42
    move-object/from16 v0, p1

    #@44
    array-length v2, v0

    #@45
    add-int v3, p4, p5

    #@47
    sub-int/2addr v2, v3

    #@48
    or-int/2addr v2, v1

    #@49
    if-nez p7, :cond_55

    #@4b
    const/4 v1, 0x0

    #@4c
    :goto_4c
    or-int/2addr v1, v2

    #@4d
    if-gez v1, :cond_5c

    #@4f
    .line 1747
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@51
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@54
    throw v1

    #@55
    .line 1741
    :cond_55
    move-object/from16 v0, p7

    #@57
    array-length v1, v0

    #@58
    add-int v3, p8, p3

    #@5a
    sub-int/2addr v1, v3

    #@5b
    goto :goto_4c

    #@5c
    .line 1750
    :cond_5c
    move-object/from16 v0, p1

    #@5e
    array-length v1, v0

    #@5f
    if-eqz v1, :cond_63

    #@61
    if-nez p3, :cond_65

    #@63
    .line 1751
    :cond_63
    const/4 v1, 0x0

    #@64
    .line 1769
    :goto_64
    return v1

    #@65
    .line 1753
    :cond_65
    iget-boolean v1, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@67
    if-nez v1, :cond_82

    #@69
    .line 1754
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@6b
    move-object/from16 v2, p1

    #@6d
    move/from16 v3, p2

    #@6f
    move/from16 v4, p3

    #@71
    move/from16 v5, p4

    #@73
    move/from16 v6, p5

    #@75
    move/from16 v7, p6

    #@77
    move-object/from16 v8, p7

    #@79
    move/from16 v9, p8

    #@7b
    move/from16 v10, p9

    #@7d
    invoke-static/range {v1 .. v10}, Landroid/graphics/Paint;->native_getTextRunAdvances(I[CIIIII[FII)F

    #@80
    move-result v1

    #@81
    goto :goto_64

    #@82
    .line 1758
    :cond_82
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@85
    move-result v13

    #@86
    .line 1759
    .local v13, oldSize:F
    iget v1, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@88
    mul-float/2addr v1, v13

    #@89
    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    #@8c
    .line 1760
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@8e
    move-object/from16 v2, p1

    #@90
    move/from16 v3, p2

    #@92
    move/from16 v4, p3

    #@94
    move/from16 v5, p4

    #@96
    move/from16 v6, p5

    #@98
    move/from16 v7, p6

    #@9a
    move-object/from16 v8, p7

    #@9c
    move/from16 v9, p8

    #@9e
    move/from16 v10, p9

    #@a0
    invoke-static/range {v1 .. v10}, Landroid/graphics/Paint;->native_getTextRunAdvances(I[CIIIII[FII)F

    #@a3
    move-result v14

    #@a4
    .line 1762
    .local v14, res:F
    invoke-virtual {p0, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    #@a7
    .line 1764
    if-eqz p7, :cond_b9

    #@a9
    .line 1765
    move/from16 v12, p8

    #@ab
    .local v12, i:I
    add-int v11, v12, p3

    #@ad
    .local v11, e:I
    :goto_ad
    if-ge v12, v11, :cond_b9

    #@af
    .line 1766
    aget v1, p7, v12

    #@b1
    iget v2, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@b3
    mul-float/2addr v1, v2

    #@b4
    aput v1, p7, v12

    #@b6
    .line 1765
    add-int/lit8 v12, v12, 0x1

    #@b8
    goto :goto_ad

    #@b9
    .line 1769
    .end local v11           #e:I
    .end local v12           #i:I
    :cond_b9
    iget v1, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@bb
    mul-float/2addr v1, v14

    #@bc
    goto :goto_64
.end method

.method public getTextRunCursor(Ljava/lang/CharSequence;IIIII)I
    .registers 15
    .parameter "text"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "offset"
    .parameter "cursorOpt"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2035
    instance-of v0, p1, Ljava/lang/String;

    #@3
    if-nez v0, :cond_d

    #@5
    instance-of v0, p1, Landroid/text/SpannedString;

    #@7
    if-nez v0, :cond_d

    #@9
    instance-of v0, p1, Landroid/text/SpannableString;

    #@b
    if-eqz v0, :cond_1c

    #@d
    .line 2037
    :cond_d
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    move-object v0, p0

    #@12
    move v2, p2

    #@13
    move v3, p3

    #@14
    move v4, p4

    #@15
    move v5, p5

    #@16
    move v6, p6

    #@17
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor(Ljava/lang/String;IIIII)I

    #@1a
    move-result v7

    #@1b
    .line 2050
    :goto_1b
    return v7

    #@1c
    .line 2040
    :cond_1c
    instance-of v0, p1, Landroid/text/GraphicsOperations;

    #@1e
    if-eqz v0, :cond_2e

    #@20
    move-object v0, p1

    #@21
    .line 2041
    check-cast v0, Landroid/text/GraphicsOperations;

    #@23
    move v1, p2

    #@24
    move v2, p3

    #@25
    move v3, p4

    #@26
    move v4, p5

    #@27
    move v5, p6

    #@28
    move-object v6, p0

    #@29
    invoke-interface/range {v0 .. v6}, Landroid/text/GraphicsOperations;->getTextRunCursor(IIIIILandroid/graphics/Paint;)I

    #@2c
    move-result v7

    #@2d
    goto :goto_1b

    #@2e
    .line 2045
    :cond_2e
    sub-int v3, p3, p2

    #@30
    .line 2046
    .local v3, contextLen:I
    invoke-static {v3}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@33
    move-result-object v1

    #@34
    .line 2047
    .local v1, buf:[C
    invoke-static {p1, p2, p3, v1, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@37
    .line 2048
    sub-int v5, p5, p2

    #@39
    move-object v0, p0

    #@3a
    move v4, p4

    #@3b
    move v6, p6

    #@3c
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor([CIIIII)I

    #@3f
    move-result v7

    #@40
    .line 2049
    .local v7, result:I
    invoke-static {v1}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@43
    goto :goto_1b
.end method

.method public getTextRunCursor(Ljava/lang/String;IIIII)I
    .registers 15
    .parameter "text"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "offset"
    .parameter "cursorOpt"

    #@0
    .prologue
    .line 2081
    or-int v0, p2, p3

    #@2
    or-int/2addr v0, p5

    #@3
    sub-int v1, p3, p2

    #@5
    or-int/2addr v0, v1

    #@6
    sub-int v1, p5, p2

    #@8
    or-int/2addr v0, v1

    #@9
    sub-int v1, p3, p5

    #@b
    or-int/2addr v0, v1

    #@c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v1

    #@10
    sub-int/2addr v1, p3

    #@11
    or-int/2addr v0, v1

    #@12
    or-int/2addr v0, p6

    #@13
    if-ltz v0, :cond_18

    #@15
    const/4 v0, 0x4

    #@16
    if-le p6, v0, :cond_1e

    #@18
    .line 2085
    :cond_18
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@1a
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1d
    throw v0

    #@1e
    .line 2088
    :cond_1e
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@20
    move-object v0, p0

    #@21
    move-object v2, p1

    #@22
    move v3, p2

    #@23
    move v4, p3

    #@24
    move v5, p4

    #@25
    move v6, p5

    #@26
    move v7, p6

    #@27
    invoke-direct/range {v0 .. v7}, Landroid/graphics/Paint;->native_getTextRunCursor(ILjava/lang/String;IIIII)I

    #@2a
    move-result v0

    #@2b
    return v0
.end method

.method public getTextRunCursor([CIIIII)I
    .registers 16
    .parameter "text"
    .parameter "contextStart"
    .parameter "contextLength"
    .parameter "flags"
    .parameter "offset"
    .parameter "cursorOpt"

    #@0
    .prologue
    .line 1994
    add-int v8, p2, p3

    #@2
    .line 1995
    .local v8, contextEnd:I
    or-int v0, p2, v8

    #@4
    or-int/2addr v0, p5

    #@5
    sub-int v1, v8, p2

    #@7
    or-int/2addr v0, v1

    #@8
    sub-int v1, p5, p2

    #@a
    or-int/2addr v0, v1

    #@b
    sub-int v1, v8, p5

    #@d
    or-int/2addr v0, v1

    #@e
    array-length v1, p1

    #@f
    sub-int/2addr v1, v8

    #@10
    or-int/2addr v0, v1

    #@11
    or-int/2addr v0, p6

    #@12
    if-ltz v0, :cond_17

    #@14
    const/4 v0, 0x4

    #@15
    if-le p6, v0, :cond_1d

    #@17
    .line 1999
    :cond_17
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@19
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1c
    throw v0

    #@1d
    .line 2002
    :cond_1d
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@1f
    move-object v0, p0

    #@20
    move-object v2, p1

    #@21
    move v3, p2

    #@22
    move v4, p3

    #@23
    move v5, p4

    #@24
    move v6, p5

    #@25
    move v7, p6

    #@26
    invoke-direct/range {v0 .. v7}, Landroid/graphics/Paint;->native_getTextRunCursor(I[CIIIII)I

    #@29
    move-result v0

    #@2a
    return v0
.end method

.method public native getTextScaleX()F
.end method

.method public native getTextSize()F
.end method

.method public native getTextSkewX()F
.end method

.method public getTextWidths(Ljava/lang/CharSequence;II[F)I
    .registers 9
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "widths"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1588
    if-nez p1, :cond_c

    #@3
    .line 1589
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v3, "text cannot be null"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 1591
    :cond_c
    or-int v2, p2, p3

    #@e
    sub-int v3, p3, p2

    #@10
    or-int/2addr v2, v3

    #@11
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v3

    #@15
    sub-int/2addr v3, p3

    #@16
    or-int/2addr v2, v3

    #@17
    if-gez v2, :cond_1f

    #@19
    .line 1592
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@1b
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1e
    throw v2

    #@1f
    .line 1594
    :cond_1f
    sub-int v2, p3, p2

    #@21
    array-length v3, p4

    #@22
    if-le v2, v3, :cond_2a

    #@24
    .line 1595
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@26
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@29
    throw v2

    #@2a
    .line 1598
    :cond_2a
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@2d
    move-result v2

    #@2e
    if-eqz v2, :cond_32

    #@30
    if-ne p2, p3, :cond_33

    #@32
    .line 1617
    .end local p1
    :cond_32
    :goto_32
    return v1

    #@33
    .line 1601
    .restart local p1
    :cond_33
    instance-of v2, p1, Ljava/lang/String;

    #@35
    if-eqz v2, :cond_3e

    #@37
    .line 1602
    check-cast p1, Ljava/lang/String;

    #@39
    .end local p1
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    #@3c
    move-result v1

    #@3d
    goto :goto_32

    #@3e
    .line 1604
    .restart local p1
    :cond_3e
    instance-of v2, p1, Landroid/text/SpannedString;

    #@40
    if-nez v2, :cond_46

    #@42
    instance-of v2, p1, Landroid/text/SpannableString;

    #@44
    if-eqz v2, :cond_4f

    #@46
    .line 1606
    :cond_46
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {p0, v2, p2, p3, p4}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    #@4d
    move-result v1

    #@4e
    goto :goto_32

    #@4f
    .line 1608
    :cond_4f
    instance-of v2, p1, Landroid/text/GraphicsOperations;

    #@51
    if-eqz v2, :cond_5a

    #@53
    .line 1609
    check-cast p1, Landroid/text/GraphicsOperations;

    #@55
    .end local p1
    invoke-interface {p1, p2, p3, p4, p0}, Landroid/text/GraphicsOperations;->getTextWidths(II[FLandroid/graphics/Paint;)I

    #@58
    move-result v1

    #@59
    goto :goto_32

    #@5a
    .line 1613
    .restart local p1
    :cond_5a
    sub-int v2, p3, p2

    #@5c
    invoke-static {v2}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@5f
    move-result-object v0

    #@60
    .line 1614
    .local v0, buf:[C
    invoke-static {p1, p2, p3, v0, v1}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@63
    .line 1615
    sub-int v2, p3, p2

    #@65
    invoke-virtual {p0, v0, v1, v2, p4}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    #@68
    move-result v1

    #@69
    .line 1616
    .local v1, result:I
    invoke-static {v0}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@6c
    goto :goto_32
.end method

.method public getTextWidths(Ljava/lang/String;II[F)I
    .registers 10
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "widths"

    #@0
    .prologue
    .line 1631
    if-nez p1, :cond_b

    #@2
    .line 1632
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "text cannot be null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 1634
    :cond_b
    or-int v3, p2, p3

    #@d
    sub-int v4, p3, p2

    #@f
    or-int/2addr v3, v4

    #@10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13
    move-result v4

    #@14
    sub-int/2addr v4, p3

    #@15
    or-int/2addr v3, v4

    #@16
    if-gez v3, :cond_1e

    #@18
    .line 1635
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    #@1a
    invoke-direct {v3}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1d
    throw v3

    #@1e
    .line 1637
    :cond_1e
    sub-int v3, p3, p2

    #@20
    array-length v4, p4

    #@21
    if-le v3, v4, :cond_29

    #@23
    .line 1638
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@25
    invoke-direct {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@28
    throw v3

    #@29
    .line 1641
    :cond_29
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_31

    #@2f
    if-ne p2, p3, :cond_33

    #@31
    .line 1642
    :cond_31
    const/4 v2, 0x0

    #@32
    .line 1655
    :cond_32
    :goto_32
    return v2

    #@33
    .line 1644
    :cond_33
    iget-boolean v3, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@35
    if-nez v3, :cond_3e

    #@37
    .line 1645
    iget v3, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@39
    invoke-static {v3, p1, p2, p3, p4}, Landroid/graphics/Paint;->native_getTextWidths(ILjava/lang/String;II[F)I

    #@3c
    move-result v2

    #@3d
    goto :goto_32

    #@3e
    .line 1648
    :cond_3e
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@41
    move-result v1

    #@42
    .line 1649
    .local v1, oldSize:F
    iget v3, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@44
    mul-float/2addr v3, v1

    #@45
    invoke-virtual {p0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    #@48
    .line 1650
    iget v3, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4a
    invoke-static {v3, p1, p2, p3, p4}, Landroid/graphics/Paint;->native_getTextWidths(ILjava/lang/String;II[F)I

    #@4d
    move-result v2

    #@4e
    .line 1651
    .local v2, res:I
    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    #@51
    .line 1652
    const/4 v0, 0x0

    #@52
    .local v0, i:I
    :goto_52
    if-ge v0, v2, :cond_32

    #@54
    .line 1653
    aget v3, p4, v0

    #@56
    iget v4, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@58
    mul-float/2addr v3, v4

    #@59
    aput v3, p4, v0

    #@5b
    .line 1652
    add-int/lit8 v0, v0, 0x1

    #@5d
    goto :goto_52
.end method

.method public getTextWidths(Ljava/lang/String;[F)I
    .registers 5
    .parameter "text"
    .parameter "widths"

    #@0
    .prologue
    .line 1667
    const/4 v0, 0x0

    #@1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v1

    #@5
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    #@8
    move-result v0

    #@9
    return v0
.end method

.method public getTextWidths([CII[F)I
    .registers 10
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "widths"

    #@0
    .prologue
    .line 1551
    if-nez p1, :cond_b

    #@2
    .line 1552
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v4, "text cannot be null"

    #@7
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v3

    #@b
    .line 1554
    :cond_b
    or-int v3, p2, p3

    #@d
    if-ltz v3, :cond_17

    #@f
    add-int v3, p2, p3

    #@11
    array-length v4, p1

    #@12
    if-gt v3, v4, :cond_17

    #@14
    array-length v3, p4

    #@15
    if-le p3, v3, :cond_1d

    #@17
    .line 1556
    :cond_17
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@19
    invoke-direct {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@1c
    throw v3

    #@1d
    .line 1559
    :cond_1d
    array-length v3, p1

    #@1e
    if-eqz v3, :cond_22

    #@20
    if-nez p3, :cond_24

    #@22
    .line 1560
    :cond_22
    const/4 v2, 0x0

    #@23
    .line 1573
    :cond_23
    :goto_23
    return v2

    #@24
    .line 1562
    :cond_24
    iget-boolean v3, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@26
    if-nez v3, :cond_2f

    #@28
    .line 1563
    iget v3, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2a
    invoke-static {v3, p1, p2, p3, p4}, Landroid/graphics/Paint;->native_getTextWidths(I[CII[F)I

    #@2d
    move-result v2

    #@2e
    goto :goto_23

    #@2f
    .line 1566
    :cond_2f
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@32
    move-result v1

    #@33
    .line 1567
    .local v1, oldSize:F
    iget v3, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@35
    mul-float/2addr v3, v1

    #@36
    invoke-virtual {p0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    #@39
    .line 1568
    iget v3, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@3b
    invoke-static {v3, p1, p2, p3, p4}, Landroid/graphics/Paint;->native_getTextWidths(I[CII[F)I

    #@3e
    move-result v2

    #@3f
    .line 1569
    .local v2, res:I
    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    #@42
    .line 1570
    const/4 v0, 0x0

    #@43
    .local v0, i:I
    :goto_43
    if-ge v0, v2, :cond_23

    #@45
    .line 1571
    aget v3, p4, v0

    #@47
    iget v4, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@49
    mul-float/2addr v3, v4

    #@4a
    aput v3, p4, v0

    #@4c
    .line 1570
    add-int/lit8 v0, v0, 0x1

    #@4e
    goto :goto_43
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .registers 2

    #@0
    .prologue
    .line 971
    iget-object v0, p0, Landroid/graphics/Paint;->mTypeface:Landroid/graphics/Typeface;

    #@2
    return-object v0
.end method

.method public getXfermode()Landroid/graphics/Xfermode;
    .registers 2

    #@0
    .prologue
    .line 884
    iget-object v0, p0, Landroid/graphics/Paint;->mXfermode:Landroid/graphics/Xfermode;

    #@2
    return-object v0
.end method

.method public final isAntiAlias()Z
    .registers 2

    #@0
    .prologue
    .line 512
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x1

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isDither()Z
    .registers 2

    #@0
    .prologue
    .line 536
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x4

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isFakeBoldText()Z
    .registers 2

    #@0
    .prologue
    .line 627
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x20

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isFilterBitmap()Z
    .registers 2

    #@0
    .prologue
    .line 647
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x2

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isLinearText()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 558
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x40

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isStrikeThruText()Z
    .registers 2

    #@0
    .prologue
    .line 610
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x10

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isSubpixelText()Z
    .registers 2

    #@0
    .prologue
    .line 576
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit16 v0, v0, 0x80

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public final isUnderlineText()Z
    .registers 2

    #@0
    .prologue
    .line 593
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFlags()I

    #@3
    move-result v0

    #@4
    and-int/lit8 v0, v0, 0x8

    #@6
    if-eqz v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public measureText(Ljava/lang/CharSequence;II)F
    .registers 9
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 1378
    if-nez p1, :cond_c

    #@3
    .line 1379
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@5
    const-string/jumbo v3, "text cannot be null"

    #@8
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@b
    throw v2

    #@c
    .line 1381
    :cond_c
    or-int v2, p2, p3

    #@e
    sub-int v3, p3, p2

    #@10
    or-int/2addr v2, v3

    #@11
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@14
    move-result v3

    #@15
    sub-int/2addr v3, p3

    #@16
    or-int/2addr v2, v3

    #@17
    if-gez v2, :cond_1f

    #@19
    .line 1382
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@1b
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1e
    throw v2

    #@1f
    .line 1385
    :cond_1f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_27

    #@25
    if-ne p2, p3, :cond_29

    #@27
    .line 1386
    :cond_27
    const/4 v1, 0x0

    #@28
    .line 1403
    .end local p1
    :goto_28
    return v1

    #@29
    .line 1388
    .restart local p1
    :cond_29
    instance-of v2, p1, Ljava/lang/String;

    #@2b
    if-eqz v2, :cond_34

    #@2d
    .line 1389
    check-cast p1, Ljava/lang/String;

    #@2f
    .end local p1
    invoke-virtual {p0, p1, p2, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    #@32
    move-result v1

    #@33
    goto :goto_28

    #@34
    .line 1391
    .restart local p1
    :cond_34
    instance-of v2, p1, Landroid/text/SpannedString;

    #@36
    if-nez v2, :cond_3c

    #@38
    instance-of v2, p1, Landroid/text/SpannableString;

    #@3a
    if-eqz v2, :cond_45

    #@3c
    .line 1393
    :cond_3c
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {p0, v2, p2, p3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    #@43
    move-result v1

    #@44
    goto :goto_28

    #@45
    .line 1395
    :cond_45
    instance-of v2, p1, Landroid/text/GraphicsOperations;

    #@47
    if-eqz v2, :cond_50

    #@49
    .line 1396
    check-cast p1, Landroid/text/GraphicsOperations;

    #@4b
    .end local p1
    invoke-interface {p1, p2, p3, p0}, Landroid/text/GraphicsOperations;->measureText(IILandroid/graphics/Paint;)F

    #@4e
    move-result v1

    #@4f
    goto :goto_28

    #@50
    .line 1399
    .restart local p1
    :cond_50
    sub-int v2, p3, p2

    #@52
    invoke-static {v2}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@55
    move-result-object v0

    #@56
    .line 1400
    .local v0, buf:[C
    invoke-static {p1, p2, p3, v0, v4}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@59
    .line 1401
    sub-int v2, p3, p2

    #@5b
    invoke-virtual {p0, v0, v4, v2}, Landroid/graphics/Paint;->measureText([CII)F

    #@5e
    move-result v1

    #@5f
    .line 1402
    .local v1, result:F
    invoke-static {v0}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@62
    goto :goto_28
.end method

.method public measureText(Ljava/lang/String;)F
    .registers 6
    .parameter "text"

    #@0
    .prologue
    .line 1351
    if-nez p1, :cond_b

    #@2
    .line 1352
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "text cannot be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1355
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@e
    move-result v2

    #@f
    if-nez v2, :cond_13

    #@11
    .line 1356
    const/4 v2, 0x0

    #@12
    .line 1364
    :goto_12
    return v2

    #@13
    .line 1359
    :cond_13
    iget-boolean v2, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@15
    if-nez v2, :cond_1c

    #@17
    invoke-direct {p0, p1}, Landroid/graphics/Paint;->native_measureText(Ljava/lang/String;)F

    #@1a
    move-result v2

    #@1b
    goto :goto_12

    #@1c
    .line 1360
    :cond_1c
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@1f
    move-result v0

    #@20
    .line 1361
    .local v0, oldSize:F
    iget v2, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@22
    mul-float/2addr v2, v0

    #@23
    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@26
    .line 1362
    invoke-direct {p0, p1}, Landroid/graphics/Paint;->native_measureText(Ljava/lang/String;)F

    #@29
    move-result v1

    #@2a
    .line 1363
    .local v1, w:F
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    #@2d
    .line 1364
    iget v2, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@2f
    mul-float/2addr v2, v1

    #@30
    goto :goto_12
.end method

.method public measureText(Ljava/lang/String;II)F
    .registers 8
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1321
    if-nez p1, :cond_b

    #@2
    .line 1322
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "text cannot be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1324
    :cond_b
    or-int v2, p2, p3

    #@d
    sub-int v3, p3, p2

    #@f
    or-int/2addr v2, v3

    #@10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@13
    move-result v3

    #@14
    sub-int/2addr v3, p3

    #@15
    or-int/2addr v2, v3

    #@16
    if-gez v2, :cond_1e

    #@18
    .line 1325
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    #@1a
    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@1d
    throw v2

    #@1e
    .line 1328
    :cond_1e
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_26

    #@24
    if-ne p2, p3, :cond_28

    #@26
    .line 1329
    :cond_26
    const/4 v2, 0x0

    #@27
    .line 1339
    :goto_27
    return v2

    #@28
    .line 1331
    :cond_28
    iget-boolean v2, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@2a
    if-nez v2, :cond_31

    #@2c
    .line 1332
    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/Paint;->native_measureText(Ljava/lang/String;II)F

    #@2f
    move-result v2

    #@30
    goto :goto_27

    #@31
    .line 1335
    :cond_31
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@34
    move-result v0

    #@35
    .line 1336
    .local v0, oldSize:F
    iget v2, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@37
    mul-float/2addr v2, v0

    #@38
    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@3b
    .line 1337
    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/Paint;->native_measureText(Ljava/lang/String;II)F

    #@3e
    move-result v1

    #@3f
    .line 1338
    .local v1, w:F
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    #@42
    .line 1339
    iget v2, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@44
    mul-float/2addr v2, v1

    #@45
    goto :goto_27
.end method

.method public measureText([CII)F
    .registers 8
    .parameter "text"
    .parameter "index"
    .parameter "count"

    #@0
    .prologue
    .line 1289
    if-nez p1, :cond_b

    #@2
    .line 1290
    new-instance v2, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v3, "text cannot be null"

    #@7
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v2

    #@b
    .line 1292
    :cond_b
    or-int v2, p2, p3

    #@d
    if-ltz v2, :cond_14

    #@f
    add-int v2, p2, p3

    #@11
    array-length v3, p1

    #@12
    if-le v2, v3, :cond_1a

    #@14
    .line 1293
    :cond_14
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@16
    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@19
    throw v2

    #@1a
    .line 1296
    :cond_1a
    array-length v2, p1

    #@1b
    if-eqz v2, :cond_1f

    #@1d
    if-nez p3, :cond_21

    #@1f
    .line 1297
    :cond_1f
    const/4 v2, 0x0

    #@20
    .line 1307
    :goto_20
    return v2

    #@21
    .line 1299
    :cond_21
    iget-boolean v2, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@23
    if-nez v2, :cond_2a

    #@25
    .line 1300
    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/Paint;->native_measureText([CII)F

    #@28
    move-result v2

    #@29
    goto :goto_20

    #@2a
    .line 1303
    :cond_2a
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTextSize()F

    #@2d
    move-result v0

    #@2e
    .line 1304
    .local v0, oldSize:F
    iget v2, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@30
    mul-float/2addr v2, v0

    #@31
    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    #@34
    .line 1305
    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/Paint;->native_measureText([CII)F

    #@37
    move-result v1

    #@38
    .line 1306
    .local v1, w:F
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    #@3b
    .line 1307
    iget v2, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@3d
    mul-float/2addr v2, v1

    #@3e
    goto :goto_20
.end method

.method public reset()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    const/4 v3, 0x0

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x0

    #@5
    .line 372
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v0}, Landroid/graphics/Paint;->native_reset(I)V

    #@a
    .line 373
    const/16 v0, 0x100

    #@c
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setFlags(I)V

    #@f
    .line 380
    iput-object v1, p0, Landroid/graphics/Paint;->mColorFilter:Landroid/graphics/ColorFilter;

    #@11
    .line 381
    iput-object v1, p0, Landroid/graphics/Paint;->mMaskFilter:Landroid/graphics/MaskFilter;

    #@13
    .line 382
    iput-object v1, p0, Landroid/graphics/Paint;->mPathEffect:Landroid/graphics/PathEffect;

    #@15
    .line 383
    iput-object v1, p0, Landroid/graphics/Paint;->mRasterizer:Landroid/graphics/Rasterizer;

    #@17
    .line 384
    iput-object v1, p0, Landroid/graphics/Paint;->mShader:Landroid/graphics/Shader;

    #@19
    .line 385
    iput-object v1, p0, Landroid/graphics/Paint;->mTypeface:Landroid/graphics/Typeface;

    #@1b
    .line 386
    iput-object v1, p0, Landroid/graphics/Paint;->mXfermode:Landroid/graphics/Xfermode;

    #@1d
    .line 388
    iput-boolean v3, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@1f
    .line 389
    iput v4, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@21
    .line 390
    iput v4, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@23
    .line 392
    iput-boolean v3, p0, Landroid/graphics/Paint;->hasShadow:Z

    #@25
    .line 393
    iput v2, p0, Landroid/graphics/Paint;->shadowDx:F

    #@27
    .line 394
    iput v2, p0, Landroid/graphics/Paint;->shadowDy:F

    #@29
    .line 395
    iput v2, p0, Landroid/graphics/Paint;->shadowRadius:F

    #@2b
    .line 396
    iput v3, p0, Landroid/graphics/Paint;->shadowColor:I

    #@2d
    .line 398
    const/4 v0, 0x2

    #@2e
    iput v0, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@30
    .line 399
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextLocale(Ljava/util/Locale;)V

    #@37
    .line 400
    return-void
.end method

.method public set(Landroid/graphics/Paint;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    .line 408
    if-eq p0, p1, :cond_c

    #@2
    .line 410
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@6
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_set(II)V

    #@9
    .line 411
    invoke-direct {p0, p1}, Landroid/graphics/Paint;->setClassVariablesFrom(Landroid/graphics/Paint;)V

    #@c
    .line 413
    :cond_c
    return-void
.end method

.method public setARGB(IIII)V
    .registers 7
    .parameter "a"
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 730
    shl-int/lit8 v0, p1, 0x18

    #@2
    shl-int/lit8 v1, p2, 0x10

    #@4
    or-int/2addr v0, v1

    #@5
    shl-int/lit8 v1, p3, 0x8

    #@7
    or-int/2addr v0, v1

    #@8
    or-int/2addr v0, p4

    #@9
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setColor(I)V

    #@c
    .line 731
    return-void
.end method

.method public native setAlpha(I)V
.end method

.method public native setAntiAlias(Z)V
.end method

.method public setBidiFlags(I)V
    .registers 5
    .parameter "flags"

    #@0
    .prologue
    .line 470
    and-int/lit8 p1, p1, 0x7

    #@2
    .line 471
    const/4 v0, 0x5

    #@3
    if-le p1, v0, :cond_1f

    #@5
    .line 472
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string/jumbo v2, "unknown bidi flag: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 474
    :cond_1f
    iput p1, p0, Landroid/graphics/Paint;->mBidiFlags:I

    #@21
    .line 475
    return-void
.end method

.method public native setColor(I)V
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
    .registers 4
    .parameter "filter"

    #@0
    .prologue
    .line 870
    const/4 v0, 0x0

    #@1
    .line 871
    .local v0, filterNative:I
    if-eqz p1, :cond_5

    #@3
    .line 872
    iget v0, p1, Landroid/graphics/ColorFilter;->native_instance:I

    #@5
    .line 873
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setColorFilter(II)I

    #@a
    .line 874
    iput-object p1, p0, Landroid/graphics/Paint;->mColorFilter:Landroid/graphics/ColorFilter;

    #@c
    .line 875
    return-object p1
.end method

.method public setCompatibilityScaling(F)V
    .registers 7
    .parameter "factor"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    .line 444
    float-to-double v0, p1

    #@3
    const-wide/high16 v2, 0x3ff0

    #@5
    cmpl-double v0, v0, v2

    #@7
    if-nez v0, :cond_11

    #@9
    .line 445
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@c
    .line 446
    iput v4, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@e
    iput v4, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@10
    .line 452
    :goto_10
    return-void

    #@11
    .line 448
    :cond_11
    const/4 v0, 0x1

    #@12
    iput-boolean v0, p0, Landroid/graphics/Paint;->mHasCompatScaling:Z

    #@14
    .line 449
    iput p1, p0, Landroid/graphics/Paint;->mCompatScaling:F

    #@16
    .line 450
    div-float v0, v4, p1

    #@18
    iput v0, p0, Landroid/graphics/Paint;->mInvCompatScaling:F

    #@1a
    goto :goto_10
.end method

.method public native setDither(Z)V
.end method

.method public native setFakeBoldText(Z)V
.end method

.method public native setFilterBitmap(Z)V
.end method

.method public native setFlags(I)V
.end method

.method public native setHinting(I)V
.end method

.method public native setLinearText(Z)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;
    .registers 4
    .parameter "maskfilter"

    #@0
    .prologue
    .line 953
    const/4 v0, 0x0

    #@1
    .line 954
    .local v0, maskfilterNative:I
    if-eqz p1, :cond_5

    #@3
    .line 955
    iget v0, p1, Landroid/graphics/MaskFilter;->native_instance:I

    #@5
    .line 957
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setMaskFilter(II)I

    #@a
    .line 958
    iput-object p1, p0, Landroid/graphics/Paint;->mMaskFilter:Landroid/graphics/MaskFilter;

    #@c
    .line 959
    return-object p1
.end method

.method public setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;
    .registers 4
    .parameter "effect"

    #@0
    .prologue
    .line 924
    const/4 v0, 0x0

    #@1
    .line 925
    .local v0, effectNative:I
    if-eqz p1, :cond_5

    #@3
    .line 926
    iget v0, p1, Landroid/graphics/PathEffect;->native_instance:I

    #@5
    .line 928
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setPathEffect(II)I

    #@a
    .line 929
    iput-object p1, p0, Landroid/graphics/Paint;->mPathEffect:Landroid/graphics/PathEffect;

    #@c
    .line 930
    return-object p1
.end method

.method public setRasterizer(Landroid/graphics/Rasterizer;)Landroid/graphics/Rasterizer;
    .registers 4
    .parameter "rasterizer"

    #@0
    .prologue
    .line 1015
    const/4 v0, 0x0

    #@1
    .line 1016
    .local v0, rasterizerNative:I
    if-eqz p1, :cond_5

    #@3
    .line 1017
    iget v0, p1, Landroid/graphics/Rasterizer;->native_instance:I

    #@5
    .line 1019
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setRasterizer(II)I

    #@a
    .line 1020
    iput-object p1, p0, Landroid/graphics/Paint;->mRasterizer:Landroid/graphics/Rasterizer;

    #@c
    .line 1021
    return-object p1
.end method

.method public setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
    .registers 4
    .parameter "shader"

    #@0
    .prologue
    .line 846
    const/4 v0, 0x0

    #@1
    .line 847
    .local v0, shaderNative:I
    if-eqz p1, :cond_5

    #@3
    .line 848
    iget v0, p1, Landroid/graphics/Shader;->native_instance:I

    #@5
    .line 849
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setShader(II)I

    #@a
    .line 850
    iput-object p1, p0, Landroid/graphics/Paint;->mShader:Landroid/graphics/Shader;

    #@c
    .line 851
    return-object p1
.end method

.method public setShadowLayer(FFFI)V
    .registers 6
    .parameter "radius"
    .parameter "dx"
    .parameter "dy"
    .parameter "color"

    #@0
    .prologue
    .line 1030
    const/4 v0, 0x0

    #@1
    cmpl-float v0, p1, v0

    #@3
    if-lez v0, :cond_14

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    iput-boolean v0, p0, Landroid/graphics/Paint;->hasShadow:Z

    #@8
    .line 1031
    iput p1, p0, Landroid/graphics/Paint;->shadowRadius:F

    #@a
    .line 1032
    iput p2, p0, Landroid/graphics/Paint;->shadowDx:F

    #@c
    .line 1033
    iput p3, p0, Landroid/graphics/Paint;->shadowDy:F

    #@e
    .line 1034
    iput p4, p0, Landroid/graphics/Paint;->shadowColor:I

    #@10
    .line 1035
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/graphics/Paint;->nSetShadowLayer(FFFI)V

    #@13
    .line 1036
    return-void

    #@14
    .line 1030
    :cond_14
    const/4 v0, 0x0

    #@15
    goto :goto_6
.end method

.method public native setStrikeThruText(Z)V
.end method

.method public setStrokeCap(Landroid/graphics/Paint$Cap;)V
    .registers 4
    .parameter "cap"

    #@0
    .prologue
    .line 790
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    iget v1, p1, Landroid/graphics/Paint$Cap;->nativeInt:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_setStrokeCap(II)V

    #@7
    .line 791
    return-void
.end method

.method public setStrokeJoin(Landroid/graphics/Paint$Join;)V
    .registers 4
    .parameter "join"

    #@0
    .prologue
    .line 809
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    iget v1, p1, Landroid/graphics/Paint$Join;->nativeInt:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_setStrokeJoin(II)V

    #@7
    .line 810
    return-void
.end method

.method public native setStrokeMiter(F)V
.end method

.method public native setStrokeWidth(F)V
.end method

.method public setStyle(Landroid/graphics/Paint$Style;)V
    .registers 4
    .parameter "style"

    #@0
    .prologue
    .line 680
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    iget v1, p1, Landroid/graphics/Paint$Style;->nativeInt:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_setStyle(II)V

    #@7
    .line 681
    return-void
.end method

.method public native setSubpixelText(Z)V
.end method

.method public setTextAlign(Landroid/graphics/Paint$Align;)V
    .registers 4
    .parameter "align"

    #@0
    .prologue
    .line 1069
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@2
    iget v1, p1, Landroid/graphics/Paint$Align;->nativeInt:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_setTextAlign(II)V

    #@7
    .line 1070
    return-void
.end method

.method public setTextLocale(Ljava/util/Locale;)V
    .registers 4
    .parameter "locale"

    #@0
    .prologue
    .line 1108
    if-nez p1, :cond_b

    #@2
    .line 1109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v1, "locale cannot be null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 1111
    :cond_b
    iget-object v0, p0, Landroid/graphics/Paint;->mLocale:Ljava/util/Locale;

    #@d
    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_14

    #@13
    .line 1114
    :goto_13
    return-void

    #@14
    .line 1112
    :cond_14
    iput-object p1, p0, Landroid/graphics/Paint;->mLocale:Ljava/util/Locale;

    #@16
    .line 1113
    iget v0, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@18
    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/graphics/Paint;->native_setTextLocale(ILjava/lang/String;)V

    #@1f
    goto :goto_13
.end method

.method public native setTextScaleX(F)V
.end method

.method public native setTextSize(F)V
.end method

.method public native setTextSkewX(F)V
.end method

.method public setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    .registers 4
    .parameter "typeface"

    #@0
    .prologue
    .line 984
    const/4 v0, 0x0

    #@1
    .line 985
    .local v0, typefaceNative:I
    if-eqz p1, :cond_5

    #@3
    .line 986
    iget v0, p1, Landroid/graphics/Typeface;->native_instance:I

    #@5
    .line 988
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setTypeface(II)I

    #@a
    .line 989
    iput-object p1, p0, Landroid/graphics/Paint;->mTypeface:Landroid/graphics/Typeface;

    #@c
    .line 990
    return-object p1
.end method

.method public native setUnderlineText(Z)V
.end method

.method public setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;
    .registers 4
    .parameter "xfermode"

    #@0
    .prologue
    .line 897
    const/4 v0, 0x0

    #@1
    .line 898
    .local v0, xfermodeNative:I
    if-eqz p1, :cond_5

    #@3
    .line 899
    iget v0, p1, Landroid/graphics/Xfermode;->native_instance:I

    #@5
    .line 900
    :cond_5
    iget v1, p0, Landroid/graphics/Paint;->mNativePaint:I

    #@7
    invoke-static {v1, v0}, Landroid/graphics/Paint;->native_setXfermode(II)I

    #@a
    .line 901
    iput-object p1, p0, Landroid/graphics/Paint;->mXfermode:Landroid/graphics/Xfermode;

    #@c
    .line 902
    return-object p1
.end method
