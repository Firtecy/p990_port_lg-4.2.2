.class public Landroid/graphics/Point;
.super Ljava/lang/Object;
.source "Point.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public x:I

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 117
    new-instance v0, Landroid/graphics/Point$1;

    #@2
    invoke-direct {v0}, Landroid/graphics/Point$1;-><init>()V

    #@5
    sput-object v0, Landroid/graphics/Point;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 33
    iput p1, p0, Landroid/graphics/Point;->x:I

    #@5
    .line 34
    iput p2, p0, Landroid/graphics/Point;->y:I

    #@7
    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Point;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 38
    iget v0, p1, Landroid/graphics/Point;->x:I

    #@5
    iput v0, p0, Landroid/graphics/Point;->x:I

    #@7
    .line 39
    iget v0, p1, Landroid/graphics/Point;->y:I

    #@9
    iput v0, p0, Landroid/graphics/Point;->y:I

    #@b
    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public final equals(II)Z
    .registers 4
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 70
    iget v0, p0, Landroid/graphics/Point;->x:I

    #@2
    if-ne v0, p1, :cond_a

    #@4
    iget v0, p0, Landroid/graphics/Point;->y:I

    #@6
    if-ne v0, p2, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter "o"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 75
    if-ne p0, p1, :cond_5

    #@4
    .line 83
    :cond_4
    :goto_4
    return v1

    #@5
    .line 76
    :cond_5
    if-eqz p1, :cond_11

    #@7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@a
    move-result-object v3

    #@b
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@e
    move-result-object v4

    #@f
    if-eq v3, v4, :cond_13

    #@11
    :cond_11
    move v1, v2

    #@12
    goto :goto_4

    #@13
    :cond_13
    move-object v0, p1

    #@14
    .line 78
    check-cast v0, Landroid/graphics/Point;

    #@16
    .line 80
    .local v0, point:Landroid/graphics/Point;
    iget v3, p0, Landroid/graphics/Point;->x:I

    #@18
    iget v4, v0, Landroid/graphics/Point;->x:I

    #@1a
    if-eq v3, v4, :cond_1e

    #@1c
    move v1, v2

    #@1d
    goto :goto_4

    #@1e
    .line 81
    :cond_1e
    iget v3, p0, Landroid/graphics/Point;->y:I

    #@20
    iget v4, v0, Landroid/graphics/Point;->y:I

    #@22
    if-eq v3, v4, :cond_4

    #@24
    move v1, v2

    #@25
    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    #@0
    .prologue
    .line 88
    iget v0, p0, Landroid/graphics/Point;->x:I

    #@2
    .line 89
    .local v0, result:I
    mul-int/lit8 v1, v0, 0x1f

    #@4
    iget v2, p0, Landroid/graphics/Point;->y:I

    #@6
    add-int v0, v1, v2

    #@8
    .line 90
    return v0
.end method

.method public final negate()V
    .registers 2

    #@0
    .prologue
    .line 54
    iget v0, p0, Landroid/graphics/Point;->x:I

    #@2
    neg-int v0, v0

    #@3
    iput v0, p0, Landroid/graphics/Point;->x:I

    #@5
    .line 55
    iget v0, p0, Landroid/graphics/Point;->y:I

    #@7
    neg-int v0, v0

    #@8
    iput v0, p0, Landroid/graphics/Point;->y:I

    #@a
    .line 56
    return-void
.end method

.method public final offset(II)V
    .registers 4
    .parameter "dx"
    .parameter "dy"

    #@0
    .prologue
    .line 62
    iget v0, p0, Landroid/graphics/Point;->x:I

    #@2
    add-int/2addr v0, p1

    #@3
    iput v0, p0, Landroid/graphics/Point;->x:I

    #@5
    .line 63
    iget v0, p0, Landroid/graphics/Point;->y:I

    #@7
    add-int/2addr v0, p2

    #@8
    iput v0, p0, Landroid/graphics/Point;->y:I

    #@a
    .line 64
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Landroid/graphics/Point;->x:I

    #@6
    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Landroid/graphics/Point;->y:I

    #@c
    .line 144
    return-void
.end method

.method public set(II)V
    .registers 3
    .parameter "x"
    .parameter "y"

    #@0
    .prologue
    .line 46
    iput p1, p0, Landroid/graphics/Point;->x:I

    #@2
    .line 47
    iput p2, p0, Landroid/graphics/Point;->y:I

    #@4
    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Point("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Landroid/graphics/Point;->x:I

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Landroid/graphics/Point;->y:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ")"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 113
    iget v0, p0, Landroid/graphics/Point;->x:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 114
    iget v0, p0, Landroid/graphics/Point;->y:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 115
    return-void
.end method
