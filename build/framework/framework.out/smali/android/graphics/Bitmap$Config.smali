.class public final enum Landroid/graphics/Bitmap$Config;
.super Ljava/lang/Enum;
.source "Bitmap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/graphics/Bitmap$Config;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/graphics/Bitmap$Config;

.field public static final enum ALPHA_8:Landroid/graphics/Bitmap$Config;

.field public static final enum ARGB_4444:Landroid/graphics/Bitmap$Config;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum ARGB_8888:Landroid/graphics/Bitmap$Config;

.field public static final enum RGB_565:Landroid/graphics/Bitmap$Config;

.field private static sConfigs:[Landroid/graphics/Bitmap$Config;


# instance fields
.field final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x2

    #@5
    .line 291
    new-instance v0, Landroid/graphics/Bitmap$Config;

    #@7
    const-string v1, "ALPHA_8"

    #@9
    invoke-direct {v0, v1, v4, v3}, Landroid/graphics/Bitmap$Config;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@e
    .line 307
    new-instance v0, Landroid/graphics/Bitmap$Config;

    #@10
    const-string v1, "RGB_565"

    #@12
    invoke-direct {v0, v1, v5, v7}, Landroid/graphics/Bitmap$Config;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@17
    .line 324
    new-instance v0, Landroid/graphics/Bitmap$Config;

    #@19
    const-string v1, "ARGB_4444"

    #@1b
    const/4 v2, 0x5

    #@1c
    invoke-direct {v0, v1, v3, v2}, Landroid/graphics/Bitmap$Config;-><init>(Ljava/lang/String;II)V

    #@1f
    sput-object v0, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    #@21
    .line 335
    new-instance v0, Landroid/graphics/Bitmap$Config;

    #@23
    const-string v1, "ARGB_8888"

    #@25
    const/4 v2, 0x6

    #@26
    invoke-direct {v0, v1, v6, v2}, Landroid/graphics/Bitmap$Config;-><init>(Ljava/lang/String;II)V

    #@29
    sput-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@2b
    .line 282
    new-array v0, v7, [Landroid/graphics/Bitmap$Config;

    #@2d
    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@2f
    aput-object v1, v0, v4

    #@31
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@33
    aput-object v1, v0, v5

    #@35
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    #@37
    aput-object v1, v0, v3

    #@39
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@3b
    aput-object v1, v0, v6

    #@3d
    sput-object v0, Landroid/graphics/Bitmap$Config;->$VALUES:[Landroid/graphics/Bitmap$Config;

    #@3f
    .line 340
    const/4 v0, 0x7

    #@40
    new-array v0, v0, [Landroid/graphics/Bitmap$Config;

    #@42
    const/4 v1, 0x0

    #@43
    aput-object v1, v0, v4

    #@45
    const/4 v1, 0x0

    #@46
    aput-object v1, v0, v5

    #@48
    sget-object v1, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    #@4a
    aput-object v1, v0, v3

    #@4c
    const/4 v1, 0x0

    #@4d
    aput-object v1, v0, v6

    #@4f
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    #@51
    aput-object v1, v0, v7

    #@53
    const/4 v1, 0x5

    #@54
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    #@56
    aput-object v2, v0, v1

    #@58
    const/4 v1, 0x6

    #@59
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    #@5b
    aput-object v2, v0, v1

    #@5d
    sput-object v0, Landroid/graphics/Bitmap$Config;->sConfigs:[Landroid/graphics/Bitmap$Config;

    #@5f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "ni"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 344
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 345
    iput p3, p0, Landroid/graphics/Bitmap$Config;->nativeInt:I

    #@5
    .line 346
    return-void
.end method

.method static nativeToConfig(I)Landroid/graphics/Bitmap$Config;
    .registers 2
    .parameter "ni"

    #@0
    .prologue
    .line 349
    sget-object v0, Landroid/graphics/Bitmap$Config;->sConfigs:[Landroid/graphics/Bitmap$Config;

    #@2
    aget-object v0, v0, p0

    #@4
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 282
    const-class v0, Landroid/graphics/Bitmap$Config;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/graphics/Bitmap$Config;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/graphics/Bitmap$Config;
    .registers 1

    #@0
    .prologue
    .line 282
    sget-object v0, Landroid/graphics/Bitmap$Config;->$VALUES:[Landroid/graphics/Bitmap$Config;

    #@2
    invoke-virtual {v0}, [Landroid/graphics/Bitmap$Config;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/graphics/Bitmap$Config;

    #@8
    return-object v0
.end method
