.class public final enum Landroid/graphics/Region$Op;
.super Ljava/lang/Enum;
.source "Region.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/Region;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Op"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/graphics/Region$Op;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/graphics/Region$Op;

.field public static final enum DIFFERENCE:Landroid/graphics/Region$Op;

.field public static final enum INTERSECT:Landroid/graphics/Region$Op;

.field public static final enum REPLACE:Landroid/graphics/Region$Op;

.field public static final enum REVERSE_DIFFERENCE:Landroid/graphics/Region$Op;

.field public static final enum UNION:Landroid/graphics/Region$Op;

.field public static final enum XOR:Landroid/graphics/Region$Op;


# instance fields
.field public final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 30
    new-instance v0, Landroid/graphics/Region$Op;

    #@7
    const-string v1, "DIFFERENCE"

    #@9
    invoke-direct {v0, v1, v4, v4}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@c
    sput-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    #@e
    .line 31
    new-instance v0, Landroid/graphics/Region$Op;

    #@10
    const-string v1, "INTERSECT"

    #@12
    invoke-direct {v0, v1, v5, v5}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@15
    sput-object v0, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@17
    .line 32
    new-instance v0, Landroid/graphics/Region$Op;

    #@19
    const-string v1, "UNION"

    #@1b
    invoke-direct {v0, v1, v6, v6}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@1e
    sput-object v0, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@20
    .line 33
    new-instance v0, Landroid/graphics/Region$Op;

    #@22
    const-string v1, "XOR"

    #@24
    invoke-direct {v0, v1, v7, v7}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@27
    sput-object v0, Landroid/graphics/Region$Op;->XOR:Landroid/graphics/Region$Op;

    #@29
    .line 34
    new-instance v0, Landroid/graphics/Region$Op;

    #@2b
    const-string v1, "REVERSE_DIFFERENCE"

    #@2d
    invoke-direct {v0, v1, v8, v8}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@30
    sput-object v0, Landroid/graphics/Region$Op;->REVERSE_DIFFERENCE:Landroid/graphics/Region$Op;

    #@32
    .line 35
    new-instance v0, Landroid/graphics/Region$Op;

    #@34
    const-string v1, "REPLACE"

    #@36
    const/4 v2, 0x5

    #@37
    const/4 v3, 0x5

    #@38
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/Region$Op;-><init>(Ljava/lang/String;II)V

    #@3b
    sput-object v0, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@3d
    .line 29
    const/4 v0, 0x6

    #@3e
    new-array v0, v0, [Landroid/graphics/Region$Op;

    #@40
    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    #@42
    aput-object v1, v0, v4

    #@44
    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@46
    aput-object v1, v0, v5

    #@48
    sget-object v1, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    #@4a
    aput-object v1, v0, v6

    #@4c
    sget-object v1, Landroid/graphics/Region$Op;->XOR:Landroid/graphics/Region$Op;

    #@4e
    aput-object v1, v0, v7

    #@50
    sget-object v1, Landroid/graphics/Region$Op;->REVERSE_DIFFERENCE:Landroid/graphics/Region$Op;

    #@52
    aput-object v1, v0, v8

    #@54
    const/4 v1, 0x5

    #@55
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    #@57
    aput-object v2, v0, v1

    #@59
    sput-object v0, Landroid/graphics/Region$Op;->$VALUES:[Landroid/graphics/Region$Op;

    #@5b
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter "nativeInt"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    #@0
    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    .line 38
    iput p3, p0, Landroid/graphics/Region$Op;->nativeInt:I

    #@5
    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/graphics/Region$Op;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 29
    const-class v0, Landroid/graphics/Region$Op;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/graphics/Region$Op;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/graphics/Region$Op;
    .registers 1

    #@0
    .prologue
    .line 29
    sget-object v0, Landroid/graphics/Region$Op;->$VALUES:[Landroid/graphics/Region$Op;

    #@2
    invoke-virtual {v0}, [Landroid/graphics/Region$Op;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/graphics/Region$Op;

    #@8
    return-object v0
.end method
