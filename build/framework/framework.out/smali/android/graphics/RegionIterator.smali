.class public Landroid/graphics/RegionIterator;
.super Ljava/lang/Object;
.source "RegionIterator.java"


# instance fields
.field private final mNativeIter:I


# direct methods
.method public constructor <init>(Landroid/graphics/Region;)V
    .registers 3
    .parameter "region"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 29
    invoke-virtual {p1}, Landroid/graphics/Region;->ni()I

    #@6
    move-result v0

    #@7
    invoke-static {v0}, Landroid/graphics/RegionIterator;->nativeConstructor(I)I

    #@a
    move-result v0

    #@b
    iput v0, p0, Landroid/graphics/RegionIterator;->mNativeIter:I

    #@d
    .line 30
    return-void
.end method

.method private static native nativeConstructor(I)I
.end method

.method private static native nativeDestructor(I)V
.end method

.method private static native nativeNext(ILandroid/graphics/Rect;)Z
.end method


# virtual methods
.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 45
    iget v0, p0, Landroid/graphics/RegionIterator;->mNativeIter:I

    #@2
    invoke-static {v0}, Landroid/graphics/RegionIterator;->nativeDestructor(I)V

    #@5
    .line 46
    return-void
.end method

.method public final next(Landroid/graphics/Rect;)Z
    .registers 4
    .parameter "r"

    #@0
    .prologue
    .line 38
    if-nez p1, :cond_a

    #@2
    .line 39
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "The Rect must be provided"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 41
    :cond_a
    iget v0, p0, Landroid/graphics/RegionIterator;->mNativeIter:I

    #@c
    invoke-static {v0, p1}, Landroid/graphics/RegionIterator;->nativeNext(ILandroid/graphics/Rect;)Z

    #@f
    move-result v0

    #@10
    return v0
.end method
