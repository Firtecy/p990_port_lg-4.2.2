.class public Landroid/graphics/SweepGradient;
.super Landroid/graphics/Shader;
.source "SweepGradient.java"


# direct methods
.method public constructor <init>(FFII)V
    .registers 6
    .parameter "cx"
    .parameter "cy"
    .parameter "color0"
    .parameter "color1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 57
    invoke-static {p1, p2, p3, p4}, Landroid/graphics/SweepGradient;->nativeCreate2(FFII)I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@9
    .line 58
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@b
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/SweepGradient;->nativePostCreate2(IFFII)I

    #@e
    move-result v0

    #@f
    iput v0, p0, Landroid/graphics/Shader;->native_shader:I

    #@11
    .line 59
    return-void
.end method

.method public constructor <init>(FF[I[F)V
    .registers 7
    .parameter "cx"
    .parameter "cy"
    .parameter "colors"
    .parameter "positions"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 37
    array-length v0, p3

    #@4
    const/4 v1, 0x2

    #@5
    if-ge v0, v1, :cond_10

    #@7
    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@9
    const-string/jumbo v1, "needs >= 2 number of colors"

    #@c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@f
    throw v0

    #@10
    .line 40
    :cond_10
    if-eqz p4, :cond_1e

    #@12
    array-length v0, p3

    #@13
    array-length v1, p4

    #@14
    if-eq v0, v1, :cond_1e

    #@16
    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@18
    const-string v1, "color and position arrays must be of equal length"

    #@1a
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@1d
    throw v0

    #@1e
    .line 44
    :cond_1e
    invoke-static {p1, p2, p3, p4}, Landroid/graphics/SweepGradient;->nativeCreate1(FF[I[F)I

    #@21
    move-result v0

    #@22
    iput v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@24
    .line 45
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@26
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/SweepGradient;->nativePostCreate1(IFF[I[F)I

    #@29
    move-result v0

    #@2a
    iput v0, p0, Landroid/graphics/Shader;->native_shader:I

    #@2c
    .line 46
    return-void
.end method

.method private static native nativeCreate1(FF[I[F)I
.end method

.method private static native nativeCreate2(FFII)I
.end method

.method private static native nativePostCreate1(IFF[I[F)I
.end method

.method private static native nativePostCreate2(IFFII)I
.end method
