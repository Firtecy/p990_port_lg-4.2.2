.class public Landroid/graphics/ComposeShader;
.super Landroid/graphics/Shader;
.source "ComposeShader.java"


# instance fields
.field private final mShaderA:Landroid/graphics/Shader;

.field private final mShaderB:Landroid/graphics/Shader;


# direct methods
.method public constructor <init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/PorterDuff$Mode;)V
    .registers 8
    .parameter "shaderA"
    .parameter "shaderB"
    .parameter "mode"

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 62
    iput-object p1, p0, Landroid/graphics/ComposeShader;->mShaderA:Landroid/graphics/Shader;

    #@5
    .line 63
    iput-object p2, p0, Landroid/graphics/ComposeShader;->mShaderB:Landroid/graphics/Shader;

    #@7
    .line 64
    iget v0, p1, Landroid/graphics/Shader;->native_instance:I

    #@9
    iget v1, p2, Landroid/graphics/Shader;->native_instance:I

    #@b
    iget v2, p3, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@d
    invoke-static {v0, v1, v2}, Landroid/graphics/ComposeShader;->nativeCreate2(III)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@13
    .line 66
    iget v0, p0, Landroid/graphics/Shader;->native_instance:I

    #@15
    iget v1, p1, Landroid/graphics/Shader;->native_shader:I

    #@17
    iget v2, p2, Landroid/graphics/Shader;->native_shader:I

    #@19
    iget v3, p3, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@1b
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/ComposeShader;->nativePostCreate2(IIII)I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/graphics/Shader;->native_shader:I

    #@21
    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Shader;Landroid/graphics/Shader;Landroid/graphics/Xfermode;)V
    .registers 9
    .parameter "shaderA"
    .parameter "shaderB"
    .parameter "mode"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 39
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@4
    .line 40
    iput-object p1, p0, Landroid/graphics/ComposeShader;->mShaderA:Landroid/graphics/Shader;

    #@6
    .line 41
    iput-object p2, p0, Landroid/graphics/ComposeShader;->mShaderB:Landroid/graphics/Shader;

    #@8
    .line 42
    iget v3, p1, Landroid/graphics/Shader;->native_instance:I

    #@a
    iget v4, p2, Landroid/graphics/Shader;->native_instance:I

    #@c
    if-eqz p3, :cond_2f

    #@e
    iget v1, p3, Landroid/graphics/Xfermode;->native_instance:I

    #@10
    :goto_10
    invoke-static {v3, v4, v1}, Landroid/graphics/ComposeShader;->nativeCreate1(III)I

    #@13
    move-result v1

    #@14
    iput v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@16
    .line 44
    instance-of v1, p3, Landroid/graphics/PorterDuffXfermode;

    #@18
    if-eqz v1, :cond_31

    #@1a
    .line 45
    check-cast p3, Landroid/graphics/PorterDuffXfermode;

    #@1c
    .end local p3
    iget-object v0, p3, Landroid/graphics/PorterDuffXfermode;->mode:Landroid/graphics/PorterDuff$Mode;

    #@1e
    .line 46
    .local v0, pdMode:Landroid/graphics/PorterDuff$Mode;
    iget v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@20
    iget v3, p1, Landroid/graphics/Shader;->native_shader:I

    #@22
    iget v4, p2, Landroid/graphics/Shader;->native_shader:I

    #@24
    if-eqz v0, :cond_28

    #@26
    iget v2, v0, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@28
    :cond_28
    invoke-static {v1, v3, v4, v2}, Landroid/graphics/ComposeShader;->nativePostCreate2(IIII)I

    #@2b
    move-result v1

    #@2c
    iput v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@2e
    .line 52
    .end local v0           #pdMode:Landroid/graphics/PorterDuff$Mode;
    :goto_2e
    return-void

    #@2f
    .restart local p3
    :cond_2f
    move v1, v2

    #@30
    .line 42
    goto :goto_10

    #@31
    .line 49
    :cond_31
    iget v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@33
    iget v3, p1, Landroid/graphics/Shader;->native_shader:I

    #@35
    iget v4, p2, Landroid/graphics/Shader;->native_shader:I

    #@37
    if-eqz p3, :cond_3b

    #@39
    iget v2, p3, Landroid/graphics/Xfermode;->native_instance:I

    #@3b
    :cond_3b
    invoke-static {v1, v3, v4, v2}, Landroid/graphics/ComposeShader;->nativePostCreate1(IIII)I

    #@3e
    move-result v1

    #@3f
    iput v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@41
    goto :goto_2e
.end method

.method private static native nativeCreate1(III)I
.end method

.method private static native nativeCreate2(III)I
.end method

.method private static native nativePostCreate1(IIII)I
.end method

.method private static native nativePostCreate2(IIII)I
.end method
