.class public Landroid/graphics/PorterDuffColorFilter;
.super Landroid/graphics/ColorFilter;
.source "PorterDuffColorFilter.java"


# direct methods
.method public constructor <init>(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 5
    .parameter "srcColor"
    .parameter "mode"

    #@0
    .prologue
    .line 27
    invoke-direct {p0}, Landroid/graphics/ColorFilter;-><init>()V

    #@3
    .line 28
    iget v0, p2, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@5
    invoke-static {p1, v0}, Landroid/graphics/PorterDuffColorFilter;->native_CreatePorterDuffFilter(II)I

    #@8
    move-result v0

    #@9
    iput v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@b
    .line 29
    iget v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@d
    iget v1, p2, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@f
    invoke-static {v0, p1, v1}, Landroid/graphics/PorterDuffColorFilter;->nCreatePorterDuffFilter(III)I

    #@12
    move-result v0

    #@13
    iput v0, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@15
    .line 30
    return-void
.end method

.method private static native nCreatePorterDuffFilter(III)I
.end method

.method private static native native_CreatePorterDuffFilter(II)I
.end method
