.class public Landroid/graphics/Canvas;
.super Ljava/lang/Object;
.source "Canvas.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/graphics/Canvas$VertexMode;,
        Landroid/graphics/Canvas$EdgeType;,
        Landroid/graphics/Canvas$CanvasFinalizer;
    }
.end annotation


# static fields
.field public static final ALL_SAVE_FLAG:I = 0x1f

.field public static final CLIP_SAVE_FLAG:I = 0x2

.field public static final CLIP_TO_LAYER_SAVE_FLAG:I = 0x10

.field public static final DIRECTION_LTR:I = 0x0

.field public static final DIRECTION_RTL:I = 0x1

.field public static final FULL_COLOR_LAYER_SAVE_FLAG:I = 0x8

.field public static final HAS_ALPHA_LAYER_SAVE_FLAG:I = 0x4

.field public static final MATRIX_SAVE_FLAG:I = 0x1

.field private static final MAXMIMUM_BITMAP_SIZE:I = 0x7ffe


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field protected mDensity:I

.field private mDrawFilter:Landroid/graphics/DrawFilter;

.field private final mFinalizer:Landroid/graphics/Canvas$CanvasFinalizer;

.field final mNativeCanvas:I

.field protected mScreenDensity:I

.field private mSurfaceFormat:I


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 110
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    iput v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@6
    .line 59
    iput v0, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@8
    .line 112
    invoke-static {v0}, Landroid/graphics/Canvas;->initRaster(I)I

    #@b
    move-result v0

    #@c
    iput v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@e
    .line 113
    new-instance v0, Landroid/graphics/Canvas$CanvasFinalizer;

    #@10
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@12
    invoke-direct {v0, v1}, Landroid/graphics/Canvas$CanvasFinalizer;-><init>(I)V

    #@15
    iput-object v0, p0, Landroid/graphics/Canvas;->mFinalizer:Landroid/graphics/Canvas$CanvasFinalizer;

    #@17
    .line 114
    return-void
.end method

.method constructor <init>(I)V
    .registers 3
    .parameter "nativeCanvas"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 136
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    iput v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@6
    .line 59
    iput v0, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@8
    .line 137
    if-nez p1, :cond_10

    #@a
    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    #@c
    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    #@f
    throw v0

    #@10
    .line 140
    :cond_10
    iput p1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@12
    .line 141
    new-instance v0, Landroid/graphics/Canvas$CanvasFinalizer;

    #@14
    invoke-direct {v0, p1}, Landroid/graphics/Canvas$CanvasFinalizer;-><init>(I)V

    #@17
    iput-object v0, p0, Landroid/graphics/Canvas;->mFinalizer:Landroid/graphics/Canvas$CanvasFinalizer;

    #@19
    .line 142
    invoke-static {}, Landroid/graphics/Bitmap;->getDefaultDensity()I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@1f
    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 125
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 52
    iput v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@6
    .line 59
    iput v0, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@8
    .line 126
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_16

    #@e
    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    #@10
    const-string v1, "Immutable bitmap passed to Canvas constructor"

    #@12
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@15
    throw v0

    #@16
    .line 129
    :cond_16
    invoke-static {p1}, Landroid/graphics/Canvas;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    #@19
    .line 130
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@1c
    move-result v0

    #@1d
    invoke-static {v0}, Landroid/graphics/Canvas;->initRaster(I)I

    #@20
    move-result v0

    #@21
    iput v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@23
    .line 131
    new-instance v0, Landroid/graphics/Canvas$CanvasFinalizer;

    #@25
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@27
    invoke-direct {v0, v1}, Landroid/graphics/Canvas$CanvasFinalizer;-><init>(I)V

    #@2a
    iput-object v0, p0, Landroid/graphics/Canvas;->mFinalizer:Landroid/graphics/Canvas$CanvasFinalizer;

    #@2c
    .line 132
    iput-object p1, p0, Landroid/graphics/Canvas;->mBitmap:Landroid/graphics/Bitmap;

    #@2e
    .line 133
    iget v0, p1, Landroid/graphics/Bitmap;->mDensity:I

    #@30
    iput v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@32
    .line 134
    return-void
.end method

.method static synthetic access$000(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 39
    invoke-static {p0}, Landroid/graphics/Canvas;->finalizer(I)V

    #@3
    return-void
.end method

.method protected static checkRange(III)V
    .registers 4
    .parameter "length"
    .parameter "offset"
    .parameter "count"

    #@0
    .prologue
    .line 1203
    or-int v0, p1, p2

    #@2
    if-ltz v0, :cond_8

    #@4
    add-int v0, p1, p2

    #@6
    if-le v0, p0, :cond_e

    #@8
    .line 1204
    :cond_8
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@a
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@d
    throw v0

    #@e
    .line 1206
    :cond_e
    return-void
.end method

.method private static native finalizer(I)V
.end method

.method public static native freeCaches()V
.end method

.method public static native freeTextLayoutCaches()V
.end method

.method private static native initRaster(I)I
.end method

.method private static native nativeDrawBitmapMatrix(IIII)V
.end method

.method private static native nativeDrawBitmapMesh(IIII[FI[III)V
.end method

.method private static native nativeDrawVertices(III[FI[FI[II[SIII)V
.end method

.method private static native nativeSetDrawFilter(II)V
.end method

.method private static native native_clipPath(III)Z
.end method

.method private static native native_clipRect(IFFFFI)Z
.end method

.method private static native native_clipRegion(III)Z
.end method

.method private static native native_concat(II)V
.end method

.method private static native native_drawARGB(IIIII)V
.end method

.method private static native native_drawArc(ILandroid/graphics/RectF;FFZI)V
.end method

.method private native native_drawBitmap(IIFFIIII)V
.end method

.method private static native native_drawBitmap(IILandroid/graphics/Rect;Landroid/graphics/Rect;III)V
.end method

.method private native native_drawBitmap(IILandroid/graphics/Rect;Landroid/graphics/RectF;III)V
.end method

.method private static native native_drawBitmap(I[IIIFFIIZI)V
.end method

.method private static native native_drawCircle(IFFFI)V
.end method

.method private static native native_drawColor(II)V
.end method

.method private static native native_drawColor(III)V
.end method

.method private static native native_drawLine(IFFFFI)V
.end method

.method private static native native_drawOval(ILandroid/graphics/RectF;I)V
.end method

.method private static native native_drawPaint(II)V
.end method

.method private static native native_drawPath(III)V
.end method

.method private static native native_drawPicture(II)V
.end method

.method private static native native_drawPosText(ILjava/lang/String;[FI)V
.end method

.method private static native native_drawPosText(I[CII[FI)V
.end method

.method private static native native_drawRGB(IIII)V
.end method

.method private static native native_drawRect(IFFFFI)V
.end method

.method private static native native_drawRect(ILandroid/graphics/RectF;I)V
.end method

.method private static native native_drawRoundRect(ILandroid/graphics/RectF;FFI)V
.end method

.method private static native native_drawText(ILjava/lang/String;IIFFII)V
.end method

.method private static native native_drawText(I[CIIFFII)V
.end method

.method private static native native_drawTextOnPath(ILjava/lang/String;IFFII)V
.end method

.method private static native native_drawTextOnPath(I[CIIIFFII)V
.end method

.method private static native native_drawTextRun(ILjava/lang/String;IIIIFFII)V
.end method

.method private static native native_drawTextRun(I[CIIIIFFII)V
.end method

.method private static native native_getCTM(II)V
.end method

.method private static native native_getClipBounds(ILandroid/graphics/Rect;)Z
.end method

.method private static native native_quickReject(IFFFFI)Z
.end method

.method private static native native_quickReject(III)Z
.end method

.method private static native native_quickReject(ILandroid/graphics/RectF;I)Z
.end method

.method private static native native_saveLayer(IFFFFII)I
.end method

.method private static native native_saveLayer(ILandroid/graphics/RectF;II)I
.end method

.method private static native native_saveLayerAlpha(IFFFFII)I
.end method

.method private static native native_saveLayerAlpha(ILandroid/graphics/RectF;II)I
.end method

.method private static native native_setBitmap(II)V
.end method

.method private static native native_setMatrix(II)V
.end method

.method private static throwIfRecycled(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    #@0
    .prologue
    .line 1025
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_1f

    #@6
    .line 1026
    new-instance v0, Ljava/lang/RuntimeException;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "Canvas: trying to use a recycled bitmap "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1e
    throw v0

    #@1f
    .line 1028
    :cond_1f
    return-void
.end method


# virtual methods
.method public clipPath(Landroid/graphics/Path;)Z
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 638
    sget-object v0, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z
    .registers 6
    .parameter "path"
    .parameter "op"

    #@0
    .prologue
    .line 628
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@5
    move-result v1

    #@6
    iget v2, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Canvas;->native_clipPath(III)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public native clipRect(FFFF)Z
.end method

.method public clipRect(FFFFLandroid/graphics/Region$Op;)Z
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "op"

    #@0
    .prologue
    .line 587
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v5, p5, Landroid/graphics/Region$Op;->nativeInt:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_clipRect(IFFFFI)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public native clipRect(IIII)Z
.end method

.method public native clipRect(Landroid/graphics/Rect;)Z
.end method

.method public clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "rect"
    .parameter "op"

    #@0
    .prologue
    .line 549
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p1, Landroid/graphics/Rect;->left:I

    #@4
    int-to-float v1, v1

    #@5
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@7
    int-to-float v2, v2

    #@8
    iget v3, p1, Landroid/graphics/Rect;->right:I

    #@a
    int-to-float v3, v3

    #@b
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    #@d
    int-to-float v4, v4

    #@e
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@10
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_clipRect(IFFFFI)Z

    #@13
    move-result v0

    #@14
    return v0
.end method

.method public native clipRect(Landroid/graphics/RectF;)Z
.end method

.method public clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z
    .registers 9
    .parameter "rect"
    .parameter "op"

    #@0
    .prologue
    .line 536
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p1, Landroid/graphics/RectF;->left:F

    #@4
    iget v2, p1, Landroid/graphics/RectF;->top:F

    #@6
    iget v3, p1, Landroid/graphics/RectF;->right:F

    #@8
    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    #@a
    iget v5, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@c
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_clipRect(IFFFFI)Z

    #@f
    move-result v0

    #@10
    return v0
.end method

.method public clipRegion(Landroid/graphics/Region;)Z
    .registers 3
    .parameter "region"

    #@0
    .prologue
    .line 667
    sget-object v0, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    #@2
    invoke-virtual {p0, p1, v0}, Landroid/graphics/Canvas;->clipRegion(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public clipRegion(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z
    .registers 6
    .parameter "region"
    .parameter "op"

    #@0
    .prologue
    .line 653
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Region;->ni()I

    #@5
    move-result v1

    #@6
    iget v2, p2, Landroid/graphics/Region$Op;->nativeInt:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Canvas;->native_clipRegion(III)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public concat(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 486
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Canvas;->native_concat(II)V

    #@7
    .line 487
    return-void
.end method

.method public drawARGB(IIII)V
    .registers 6
    .parameter "a"
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 801
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-static {v0, p1, p2, p3, p4}, Landroid/graphics/Canvas;->native_drawARGB(IIIII)V

    #@5
    .line 802
    return-void
.end method

.method public drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V
    .registers 12
    .parameter "oval"
    .parameter "startAngle"
    .parameter "sweepAngle"
    .parameter "useCenter"
    .parameter "paint"

    #@0
    .prologue
    .line 989
    if-nez p1, :cond_8

    #@2
    .line 990
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 992
    :cond_8
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@a
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@c
    move-object v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move v4, p4

    #@10
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_drawArc(ILandroid/graphics/RectF;FFZI)V

    #@13
    .line 994
    return-void
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    .registers 14
    .parameter "bitmap"
    .parameter "left"
    .parameter "top"
    .parameter "paint"

    #@0
    .prologue
    .line 1065
    invoke-static {p1}, Landroid/graphics/Canvas;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    #@3
    .line 1066
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@8
    move-result v2

    #@9
    if-eqz p4, :cond_1a

    #@b
    iget v5, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@d
    :goto_d
    iget v6, p0, Landroid/graphics/Canvas;->mDensity:I

    #@f
    iget v7, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@11
    iget v8, p1, Landroid/graphics/Bitmap;->mDensity:I

    #@13
    move-object v0, p0

    #@14
    move v3, p2

    #@15
    move v4, p3

    #@16
    invoke-direct/range {v0 .. v8}, Landroid/graphics/Canvas;->native_drawBitmap(IIFFIIII)V

    #@19
    .line 1068
    return-void

    #@1a
    .line 1066
    :cond_1a
    const/4 v5, 0x0

    #@1b
    goto :goto_d
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    .registers 8
    .parameter "bitmap"
    .parameter "matrix"
    .parameter "paint"

    #@0
    .prologue
    .line 1195
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@5
    move-result v2

    #@6
    invoke-virtual {p2}, Landroid/graphics/Matrix;->ni()I

    #@9
    move-result v3

    #@a
    if-eqz p3, :cond_12

    #@c
    iget v0, p3, Landroid/graphics/Paint;->mNativePaint:I

    #@e
    :goto_e
    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Canvas;->nativeDrawBitmapMatrix(IIII)V

    #@11
    .line 1197
    return-void

    #@12
    .line 1195
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_e
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .registers 12
    .parameter "bitmap"
    .parameter "src"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    .line 1124
    if-nez p3, :cond_8

    #@2
    .line 1125
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 1127
    :cond_8
    invoke-static {p1}, Landroid/graphics/Canvas;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    #@b
    .line 1128
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@d
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@10
    move-result v1

    #@11
    if-eqz p4, :cond_1f

    #@13
    iget v4, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@15
    :goto_15
    iget v5, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@17
    iget v6, p1, Landroid/graphics/Bitmap;->mDensity:I

    #@19
    move-object v2, p2

    #@1a
    move-object v3, p3

    #@1b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Canvas;->native_drawBitmap(IILandroid/graphics/Rect;Landroid/graphics/Rect;III)V

    #@1e
    .line 1130
    return-void

    #@1f
    .line 1128
    :cond_1f
    const/4 v4, 0x0

    #@20
    goto :goto_15
.end method

.method public drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 13
    .parameter "bitmap"
    .parameter "src"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    .line 1093
    if-nez p3, :cond_8

    #@2
    .line 1094
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 1096
    :cond_8
    invoke-static {p1}, Landroid/graphics/Canvas;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    #@b
    .line 1097
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@d
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@10
    move-result v2

    #@11
    if-eqz p4, :cond_20

    #@13
    iget v5, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@15
    :goto_15
    iget v6, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@17
    iget v7, p1, Landroid/graphics/Bitmap;->mDensity:I

    #@19
    move-object v0, p0

    #@1a
    move-object v3, p2

    #@1b
    move-object v4, p3

    #@1c
    invoke-direct/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawBitmap(IILandroid/graphics/Rect;Landroid/graphics/RectF;III)V

    #@1f
    .line 1099
    return-void

    #@20
    .line 1097
    :cond_20
    const/4 v5, 0x0

    #@21
    goto :goto_15
.end method

.method public drawBitmap([IIIFFIIZLandroid/graphics/Paint;)V
    .registers 23
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "hasAlpha"
    .parameter "paint"

    #@0
    .prologue
    .line 1154
    if-gez p6, :cond_b

    #@2
    .line 1155
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@4
    const-string/jumbo v2, "width must be >= 0"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1157
    :cond_b
    if-gez p7, :cond_15

    #@d
    .line 1158
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@f
    const-string v2, "height must be >= 0"

    #@11
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@14
    throw v1

    #@15
    .line 1160
    :cond_15
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->abs(I)I

    #@18
    move-result v1

    #@19
    move/from16 v0, p6

    #@1b
    if-ge v1, v0, :cond_25

    #@1d
    .line 1161
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@1f
    const-string v2, "abs(stride) must be >= width"

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 1163
    :cond_25
    add-int/lit8 v1, p7, -0x1

    #@27
    mul-int v1, v1, p3

    #@29
    add-int v11, p2, v1

    #@2b
    .line 1164
    .local v11, lastScanline:I
    array-length v12, p1

    #@2c
    .line 1165
    .local v12, length:I
    if-ltz p2, :cond_38

    #@2e
    add-int v1, p2, p6

    #@30
    if-gt v1, v12, :cond_38

    #@32
    if-ltz v11, :cond_38

    #@34
    add-int v1, v11, p6

    #@36
    if-le v1, v12, :cond_3e

    #@38
    .line 1167
    :cond_38
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@3a
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@3d
    throw v1

    #@3e
    .line 1170
    :cond_3e
    if-eqz p6, :cond_42

    #@40
    if-nez p7, :cond_43

    #@42
    .line 1176
    :cond_42
    :goto_42
    return-void

    #@43
    .line 1174
    :cond_43
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@45
    if-eqz p9, :cond_5d

    #@47
    move-object/from16 v0, p9

    #@49
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@4b
    :goto_4b
    move-object v2, p1

    #@4c
    move v3, p2

    #@4d
    move/from16 v4, p3

    #@4f
    move/from16 v5, p4

    #@51
    move/from16 v6, p5

    #@53
    move/from16 v7, p6

    #@55
    move/from16 v8, p7

    #@57
    move/from16 v9, p8

    #@59
    invoke-static/range {v1 .. v10}, Landroid/graphics/Canvas;->native_drawBitmap(I[IIIFFIIZI)V

    #@5c
    goto :goto_42

    #@5d
    :cond_5d
    const/4 v10, 0x0

    #@5e
    goto :goto_4b
.end method

.method public drawBitmap([IIIIIIIZLandroid/graphics/Paint;)V
    .registers 20
    .parameter "colors"
    .parameter "offset"
    .parameter "stride"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"
    .parameter "hasAlpha"
    .parameter "paint"

    #@0
    .prologue
    .line 1183
    int-to-float v4, p4

    #@1
    int-to-float v5, p5

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move/from16 v6, p6

    #@8
    move/from16 v7, p7

    #@a
    move/from16 v8, p8

    #@c
    move-object/from16 v9, p9

    #@e
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawBitmap([IIIFFIIZLandroid/graphics/Paint;)V

    #@11
    .line 1185
    return-void
.end method

.method public drawBitmapMesh(Landroid/graphics/Bitmap;II[FI[IILandroid/graphics/Paint;)V
    .registers 20
    .parameter "bitmap"
    .parameter "meshWidth"
    .parameter "meshHeight"
    .parameter "verts"
    .parameter "vertOffset"
    .parameter "colors"
    .parameter "colorOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 1236
    or-int v1, p2, p3

    #@2
    or-int v1, v1, p5

    #@4
    or-int v1, v1, p7

    #@6
    if-gez v1, :cond_e

    #@8
    .line 1237
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@a
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@d
    throw v1

    #@e
    .line 1239
    :cond_e
    if-eqz p2, :cond_12

    #@10
    if-nez p3, :cond_13

    #@12
    .line 1252
    :cond_12
    :goto_12
    return-void

    #@13
    .line 1242
    :cond_13
    add-int/lit8 v1, p2, 0x1

    #@15
    add-int/lit8 v2, p3, 0x1

    #@17
    mul-int v10, v1, v2

    #@19
    .line 1244
    .local v10, count:I
    array-length v1, p4

    #@1a
    mul-int/lit8 v2, v10, 0x2

    #@1c
    move/from16 v0, p5

    #@1e
    invoke-static {v1, v0, v2}, Landroid/graphics/Canvas;->checkRange(III)V

    #@21
    .line 1245
    if-eqz p6, :cond_2b

    #@23
    .line 1247
    move-object/from16 v0, p6

    #@25
    array-length v1, v0

    #@26
    move/from16 v0, p7

    #@28
    invoke-static {v1, v0, v10}, Landroid/graphics/Canvas;->checkRange(III)V

    #@2b
    .line 1249
    :cond_2b
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2d
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@30
    move-result v2

    #@31
    if-eqz p8, :cond_44

    #@33
    move-object/from16 v0, p8

    #@35
    iget v9, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@37
    :goto_37
    move v3, p2

    #@38
    move v4, p3

    #@39
    move-object v5, p4

    #@3a
    move/from16 v6, p5

    #@3c
    move-object/from16 v7, p6

    #@3e
    move/from16 v8, p7

    #@40
    invoke-static/range {v1 .. v9}, Landroid/graphics/Canvas;->nativeDrawBitmapMesh(IIII[FI[III)V

    #@43
    goto :goto_12

    #@44
    :cond_44
    const/4 v9, 0x0

    #@45
    goto :goto_37
.end method

.method public drawCircle(FFFLandroid/graphics/Paint;)V
    .registers 7
    .parameter "cx"
    .parameter "cy"
    .parameter "radius"
    .parameter "paint"

    #@0
    .prologue
    .line 961
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v0, p1, p2, p3, v1}, Landroid/graphics/Canvas;->native_drawCircle(IFFFI)V

    #@7
    .line 962
    return-void
.end method

.method public drawColor(I)V
    .registers 3
    .parameter "color"

    #@0
    .prologue
    .line 811
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Canvas;->native_drawColor(II)V

    #@5
    .line 812
    return-void
.end method

.method public drawColor(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 5
    .parameter "color"
    .parameter "mode"

    #@0
    .prologue
    .line 822
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p2, Landroid/graphics/PorterDuff$Mode;->nativeInt:I

    #@4
    invoke-static {v0, p1, v1}, Landroid/graphics/Canvas;->native_drawColor(III)V

    #@7
    .line 823
    return-void
.end method

.method public drawLine(FFFFLandroid/graphics/Paint;)V
    .registers 12
    .parameter "startX"
    .parameter "startY"
    .parameter "stopX"
    .parameter "stopY"
    .parameter "paint"

    #@0
    .prologue
    .line 876
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_drawLine(IFFFFI)V

    #@b
    .line 877
    return-void
.end method

.method public native drawLines([FIILandroid/graphics/Paint;)V
.end method

.method public drawLines([FLandroid/graphics/Paint;)V
    .registers 5
    .parameter "pts"
    .parameter "paint"

    #@0
    .prologue
    .line 897
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    #@5
    .line 898
    return-void
.end method

.method public drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 5
    .parameter "oval"
    .parameter "paint"

    #@0
    .prologue
    .line 944
    if-nez p1, :cond_8

    #@2
    .line 945
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 947
    :cond_8
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@a
    iget v1, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@c
    invoke-static {v0, p1, v1}, Landroid/graphics/Canvas;->native_drawOval(ILandroid/graphics/RectF;I)V

    #@f
    .line 948
    return-void
.end method

.method public drawPaint(Landroid/graphics/Paint;)V
    .registers 4
    .parameter "paint"

    #@0
    .prologue
    .line 833
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p1, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Canvas;->native_drawPaint(II)V

    #@7
    .line 834
    return-void
.end method

.method public drawPatch(Landroid/graphics/Bitmap;[BLandroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 5
    .parameter "bitmap"
    .parameter "chunks"
    .parameter "dst"
    .parameter "paint"

    #@0
    .prologue
    .line 1043
    return-void
.end method

.method public drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .registers 6
    .parameter "path"
    .parameter "paint"

    #@0
    .prologue
    .line 1021
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@5
    move-result v1

    #@6
    iget v2, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Canvas;->native_drawPath(III)V

    #@b
    .line 1022
    return-void
.end method

.method public drawPicture(Landroid/graphics/Picture;)V
    .registers 4
    .parameter "picture"

    #@0
    .prologue
    .line 1582
    invoke-virtual {p1}, Landroid/graphics/Picture;->endRecording()V

    #@3
    .line 1583
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@5
    invoke-virtual {p1}, Landroid/graphics/Picture;->ni()I

    #@8
    move-result v1

    #@9
    invoke-static {v0, v1}, Landroid/graphics/Canvas;->native_drawPicture(II)V

    #@c
    .line 1584
    return-void
.end method

.method public drawPicture(Landroid/graphics/Picture;Landroid/graphics/Rect;)V
    .registers 6
    .parameter "picture"
    .parameter "dst"

    #@0
    .prologue
    .line 1603
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    #@3
    .line 1604
    iget v0, p2, Landroid/graphics/Rect;->left:I

    #@5
    int-to-float v0, v0

    #@6
    iget v1, p2, Landroid/graphics/Rect;->top:I

    #@8
    int-to-float v1, v1

    #@9
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@c
    .line 1605
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@f
    move-result v0

    #@10
    if-lez v0, :cond_31

    #@12
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@15
    move-result v0

    #@16
    if-lez v0, :cond_31

    #@18
    .line 1606
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    #@1b
    move-result v0

    #@1c
    int-to-float v0, v0

    #@1d
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@20
    move-result v1

    #@21
    int-to-float v1, v1

    #@22
    div-float/2addr v0, v1

    #@23
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    #@26
    move-result v1

    #@27
    int-to-float v1, v1

    #@28
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@2b
    move-result v2

    #@2c
    int-to-float v2, v2

    #@2d
    div-float/2addr v1, v2

    #@2e
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    #@31
    .line 1609
    :cond_31
    invoke-virtual {p0, p1}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@34
    .line 1610
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    #@37
    .line 1611
    return-void
.end method

.method public drawPicture(Landroid/graphics/Picture;Landroid/graphics/RectF;)V
    .registers 6
    .parameter "picture"
    .parameter "dst"

    #@0
    .prologue
    .line 1590
    invoke-virtual {p0}, Landroid/graphics/Canvas;->save()I

    #@3
    .line 1591
    iget v0, p2, Landroid/graphics/RectF;->left:F

    #@5
    iget v1, p2, Landroid/graphics/RectF;->top:F

    #@7
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@a
    .line 1592
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@d
    move-result v0

    #@e
    if-lez v0, :cond_2d

    #@10
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@13
    move-result v0

    #@14
    if-lez v0, :cond_2d

    #@16
    .line 1593
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    #@19
    move-result v0

    #@1a
    invoke-virtual {p1}, Landroid/graphics/Picture;->getWidth()I

    #@1d
    move-result v1

    #@1e
    int-to-float v1, v1

    #@1f
    div-float/2addr v0, v1

    #@20
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    #@23
    move-result v1

    #@24
    invoke-virtual {p1}, Landroid/graphics/Picture;->getHeight()I

    #@27
    move-result v2

    #@28
    int-to-float v2, v2

    #@29
    div-float/2addr v1, v2

    #@2a
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    #@2d
    .line 1595
    :cond_2d
    invoke-virtual {p0, p1}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;)V

    #@30
    .line 1596
    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    #@33
    .line 1597
    return-void
.end method

.method public native drawPoint(FFLandroid/graphics/Paint;)V
.end method

.method public native drawPoints([FIILandroid/graphics/Paint;)V
.end method

.method public drawPoints([FLandroid/graphics/Paint;)V
    .registers 5
    .parameter "pts"
    .parameter "paint"

    #@0
    .prologue
    .line 858
    const/4 v0, 0x0

    #@1
    array-length v1, p1

    #@2
    invoke-virtual {p0, p1, v0, v1, p2}, Landroid/graphics/Canvas;->drawPoints([FIILandroid/graphics/Paint;)V

    #@5
    .line 859
    return-void
.end method

.method public drawPosText(Ljava/lang/String;[FLandroid/graphics/Paint;)V
    .registers 6
    .parameter "text"
    .parameter "pos"
    .parameter "paint"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1525
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    mul-int/lit8 v0, v0, 0x2

    #@6
    array-length v1, p2

    #@7
    if-le v0, v1, :cond_f

    #@9
    .line 1526
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@b
    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@e
    throw v0

    #@f
    .line 1528
    :cond_f
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@11
    iget v1, p3, Landroid/graphics/Paint;->mNativePaint:I

    #@13
    invoke-static {v0, p1, p2, v1}, Landroid/graphics/Canvas;->native_drawPosText(ILjava/lang/String;[FI)V

    #@16
    .line 1529
    return-void
.end method

.method public drawPosText([CII[FLandroid/graphics/Paint;)V
    .registers 12
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "pos"
    .parameter "paint"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 1505
    if-ltz p2, :cond_c

    #@2
    add-int v0, p2, p3

    #@4
    array-length v1, p1

    #@5
    if-gt v0, v1, :cond_c

    #@7
    mul-int/lit8 v0, p3, 0x2

    #@9
    array-length v1, p4

    #@a
    if-le v0, v1, :cond_12

    #@c
    .line 1506
    :cond_c
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@e
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@11
    throw v0

    #@12
    .line 1508
    :cond_12
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@14
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@16
    move-object v1, p1

    #@17
    move v2, p2

    #@18
    move v3, p3

    #@19
    move-object v4, p4

    #@1a
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_drawPosText(I[CII[FI)V

    #@1d
    .line 1510
    return-void
.end method

.method public drawRGB(III)V
    .registers 5
    .parameter "r"
    .parameter "g"
    .parameter "b"

    #@0
    .prologue
    .line 788
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Canvas;->native_drawRGB(IIII)V

    #@5
    .line 789
    return-void
.end method

.method public drawRect(FFFFLandroid/graphics/Paint;)V
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "paint"

    #@0
    .prologue
    .line 934
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_drawRect(IFFFFI)V

    #@b
    .line 935
    return-void
.end method

.method public drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .registers 9
    .parameter "r"
    .parameter "paint"

    #@0
    .prologue
    .line 919
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@2
    int-to-float v1, v0

    #@3
    iget v0, p1, Landroid/graphics/Rect;->top:I

    #@5
    int-to-float v2, v0

    #@6
    iget v0, p1, Landroid/graphics/Rect;->right:I

    #@8
    int-to-float v3, v0

    #@9
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    #@b
    int-to-float v4, v0

    #@c
    move-object v0, p0

    #@d
    move-object v5, p2

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@11
    .line 920
    return-void
.end method

.method public drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .registers 5
    .parameter "rect"
    .parameter "paint"

    #@0
    .prologue
    .line 908
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@4
    invoke-static {v0, p1, v1}, Landroid/graphics/Canvas;->native_drawRect(ILandroid/graphics/RectF;I)V

    #@7
    .line 909
    return-void
.end method

.method public drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
    .registers 7
    .parameter "rect"
    .parameter "rx"
    .parameter "ry"
    .parameter "paint"

    #@0
    .prologue
    .line 1006
    if-nez p1, :cond_8

    #@2
    .line 1007
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    #@7
    throw v0

    #@8
    .line 1009
    :cond_8
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@a
    iget v1, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@c
    invoke-static {v0, p1, p2, p3, v1}, Landroid/graphics/Canvas;->native_drawRoundRect(ILandroid/graphics/RectF;FFI)V

    #@f
    .line 1011
    return-void
.end method

.method public drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
    .registers 15
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1382
    instance-of v0, p1, Ljava/lang/String;

    #@3
    if-nez v0, :cond_d

    #@5
    instance-of v0, p1, Landroid/text/SpannedString;

    #@7
    if-nez v0, :cond_d

    #@9
    instance-of v0, p1, Landroid/text/SpannableString;

    #@b
    if-eqz v0, :cond_1f

    #@d
    .line 1384
    :cond_d
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@f
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@15
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@17
    move v2, p2

    #@18
    move v3, p3

    #@19
    move v4, p4

    #@1a
    move v5, p5

    #@1b
    invoke-static/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawText(ILjava/lang/String;IIFFII)V

    #@1e
    .line 1396
    :goto_1e
    return-void

    #@1f
    .line 1386
    :cond_1f
    instance-of v0, p1, Landroid/text/GraphicsOperations;

    #@21
    if-eqz v0, :cond_30

    #@23
    move-object v0, p1

    #@24
    .line 1387
    check-cast v0, Landroid/text/GraphicsOperations;

    #@26
    move-object v1, p0

    #@27
    move v2, p2

    #@28
    move v3, p3

    #@29
    move v4, p4

    #@2a
    move v5, p5

    #@2b
    move-object v6, p6

    #@2c
    invoke-interface/range {v0 .. v6}, Landroid/text/GraphicsOperations;->drawText(Landroid/graphics/Canvas;IIFFLandroid/graphics/Paint;)V

    #@2f
    goto :goto_1e

    #@30
    .line 1390
    :cond_30
    sub-int v0, p3, p2

    #@32
    invoke-static {v0}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@35
    move-result-object v1

    #@36
    .line 1391
    .local v1, buf:[C
    invoke-static {p1, p2, p3, v1, v2}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@39
    .line 1392
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@3b
    sub-int v3, p3, p2

    #@3d
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@3f
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@41
    move v4, p4

    #@42
    move v5, p5

    #@43
    invoke-static/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawText(I[CIIFFII)V

    #@46
    .line 1394
    invoke-static {v1}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@49
    goto :goto_1e
.end method

.method public drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    .registers 13
    .parameter "text"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1345
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v3

    #@7
    iget v6, p4, Landroid/graphics/Paint;->mBidiFlags:I

    #@9
    iget v7, p4, Landroid/graphics/Paint;->mNativePaint:I

    #@b
    move-object v1, p1

    #@c
    move v4, p2

    #@d
    move v5, p3

    #@e
    invoke-static/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawText(ILjava/lang/String;IIFFII)V

    #@11
    .line 1347
    return-void
.end method

.method public drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V
    .registers 15
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1361
    or-int v0, p2, p3

    #@2
    sub-int v1, p3, p2

    #@4
    or-int/2addr v0, v1

    #@5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@8
    move-result v1

    #@9
    sub-int/2addr v1, p3

    #@a
    or-int/2addr v0, v1

    #@b
    if-gez v0, :cond_13

    #@d
    .line 1362
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@f
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@12
    throw v0

    #@13
    .line 1364
    :cond_13
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@15
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@17
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@19
    move-object v1, p1

    #@1a
    move v2, p2

    #@1b
    move v3, p3

    #@1c
    move v4, p4

    #@1d
    move v5, p5

    #@1e
    invoke-static/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawText(ILjava/lang/String;IIFFII)V

    #@21
    .line 1366
    return-void
.end method

.method public drawText([CIIFFLandroid/graphics/Paint;)V
    .registers 15
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "x"
    .parameter "y"
    .parameter "paint"

    #@0
    .prologue
    .line 1327
    or-int v0, p2, p3

    #@2
    add-int v1, p2, p3

    #@4
    or-int/2addr v0, v1

    #@5
    array-length v1, p1

    #@6
    sub-int/2addr v1, p2

    #@7
    sub-int/2addr v1, p3

    #@8
    or-int/2addr v0, v1

    #@9
    if-gez v0, :cond_11

    #@b
    .line 1329
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    #@d
    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@10
    throw v0

    #@11
    .line 1331
    :cond_11
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@13
    iget v6, p6, Landroid/graphics/Paint;->mBidiFlags:I

    #@15
    iget v7, p6, Landroid/graphics/Paint;->mNativePaint:I

    #@17
    move-object v1, p1

    #@18
    move v2, p2

    #@19
    move v3, p3

    #@1a
    move v4, p4

    #@1b
    move v5, p5

    #@1c
    invoke-static/range {v0 .. v7}, Landroid/graphics/Canvas;->native_drawText(I[CIIFFII)V

    #@1f
    .line 1333
    return-void
.end method

.method public drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V
    .registers 13
    .parameter "text"
    .parameter "path"
    .parameter "hOffset"
    .parameter "vOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 1568
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_16

    #@6
    .line 1569
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@8
    invoke-virtual {p2}, Landroid/graphics/Path;->ni()I

    #@b
    move-result v2

    #@c
    iget v5, p5, Landroid/graphics/Paint;->mBidiFlags:I

    #@e
    iget v6, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@10
    move-object v1, p1

    #@11
    move v3, p3

    #@12
    move v4, p4

    #@13
    invoke-static/range {v0 .. v6}, Landroid/graphics/Canvas;->native_drawTextOnPath(ILjava/lang/String;IFFII)V

    #@16
    .line 1572
    :cond_16
    return-void
.end method

.method public drawTextOnPath([CIILandroid/graphics/Path;FFLandroid/graphics/Paint;)V
    .registers 18
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "path"
    .parameter "hOffset"
    .parameter "vOffset"
    .parameter "paint"

    #@0
    .prologue
    .line 1546
    if-ltz p2, :cond_7

    #@2
    add-int v1, p2, p3

    #@4
    array-length v2, p1

    #@5
    if-le v1, v2, :cond_d

    #@7
    .line 1547
    :cond_7
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    #@9
    invoke-direct {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    #@c
    throw v1

    #@d
    .line 1549
    :cond_d
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@f
    invoke-virtual {p4}, Landroid/graphics/Path;->ni()I

    #@12
    move-result v5

    #@13
    move-object/from16 v0, p7

    #@15
    iget v8, v0, Landroid/graphics/Paint;->mBidiFlags:I

    #@17
    move-object/from16 v0, p7

    #@19
    iget v9, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@1b
    move-object v2, p1

    #@1c
    move v3, p2

    #@1d
    move v4, p3

    #@1e
    move v6, p5

    #@1f
    move/from16 v7, p6

    #@21
    invoke-static/range {v1 .. v9}, Landroid/graphics/Canvas;->native_drawTextOnPath(I[CIIIFFII)V

    #@24
    .line 1552
    return-void
.end method

.method public drawTextRun(Ljava/lang/CharSequence;IIIIFFILandroid/graphics/Paint;)V
    .registers 21
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "x"
    .parameter "y"
    .parameter "dir"
    .parameter "paint"

    #@0
    .prologue
    .line 1459
    if-nez p1, :cond_b

    #@2
    .line 1460
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v3, "text is null"

    #@7
    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1462
    :cond_b
    if-nez p9, :cond_16

    #@d
    .line 1463
    new-instance v1, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v3, "paint is null"

    #@12
    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1

    #@16
    .line 1465
    :cond_16
    or-int v1, p2, p3

    #@18
    sub-int v3, p3, p2

    #@1a
    or-int/2addr v1, v3

    #@1b
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@1e
    move-result v3

    #@1f
    sub-int/2addr v3, p3

    #@20
    or-int/2addr v1, v3

    #@21
    if-gez v1, :cond_29

    #@23
    .line 1466
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@25
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@28
    throw v1

    #@29
    .line 1469
    :cond_29
    if-nez p8, :cond_4f

    #@2b
    const/4 v9, 0x0

    #@2c
    .line 1471
    .local v9, flags:I
    :goto_2c
    instance-of v1, p1, Ljava/lang/String;

    #@2e
    if-nez v1, :cond_38

    #@30
    instance-of v1, p1, Landroid/text/SpannedString;

    #@32
    if-nez v1, :cond_38

    #@34
    instance-of v1, p1, Landroid/text/SpannableString;

    #@36
    if-eqz v1, :cond_51

    #@38
    .line 1473
    :cond_38
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@3a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    move-object/from16 v0, p9

    #@40
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@42
    move v3, p2

    #@43
    move v4, p3

    #@44
    move v5, p4

    #@45
    move/from16 v6, p5

    #@47
    move/from16 v7, p6

    #@49
    move/from16 v8, p7

    #@4b
    invoke-static/range {v1 .. v10}, Landroid/graphics/Canvas;->native_drawTextRun(ILjava/lang/String;IIIIFFII)V

    #@4e
    .line 1487
    :goto_4e
    return-void

    #@4f
    .line 1469
    .end local v9           #flags:I
    :cond_4f
    const/4 v9, 0x1

    #@50
    goto :goto_2c

    #@51
    .line 1475
    .restart local v9       #flags:I
    :cond_51
    instance-of v1, p1, Landroid/text/GraphicsOperations;

    #@53
    if-eqz v1, :cond_68

    #@55
    move-object v1, p1

    #@56
    .line 1476
    check-cast v1, Landroid/text/GraphicsOperations;

    #@58
    move-object v2, p0

    #@59
    move v3, p2

    #@5a
    move v4, p3

    #@5b
    move v5, p4

    #@5c
    move/from16 v6, p5

    #@5e
    move/from16 v7, p6

    #@60
    move/from16 v8, p7

    #@62
    move-object/from16 v10, p9

    #@64
    invoke-interface/range {v1 .. v10}, Landroid/text/GraphicsOperations;->drawTextRun(Landroid/graphics/Canvas;IIIIFFILandroid/graphics/Paint;)V

    #@67
    goto :goto_4e

    #@68
    .line 1479
    :cond_68
    sub-int v6, p5, p4

    #@6a
    .line 1480
    .local v6, contextLen:I
    sub-int v4, p3, p2

    #@6c
    .line 1481
    .local v4, len:I
    invoke-static {v6}, Landroid/graphics/TemporaryBuffer;->obtain(I)[C

    #@6f
    move-result-object v2

    #@70
    .line 1482
    .local v2, buf:[C
    const/4 v1, 0x0

    #@71
    move/from16 v0, p5

    #@73
    invoke-static {p1, p4, v0, v2, v1}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@76
    .line 1483
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@78
    sub-int v3, p2, p4

    #@7a
    const/4 v5, 0x0

    #@7b
    move-object/from16 v0, p9

    #@7d
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@7f
    move/from16 v7, p6

    #@81
    move/from16 v8, p7

    #@83
    invoke-static/range {v1 .. v10}, Landroid/graphics/Canvas;->native_drawTextRun(I[CIIIIFFII)V

    #@86
    .line 1485
    invoke-static {v2}, Landroid/graphics/TemporaryBuffer;->recycle([C)V

    #@89
    goto :goto_4e
.end method

.method public drawTextRun([CIIIIFFILandroid/graphics/Paint;)V
    .registers 21
    .parameter "text"
    .parameter "index"
    .parameter "count"
    .parameter "contextIndex"
    .parameter "contextCount"
    .parameter "x"
    .parameter "y"
    .parameter "dir"
    .parameter "paint"

    #@0
    .prologue
    .line 1422
    if-nez p1, :cond_b

    #@2
    .line 1423
    new-instance v1, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v2, "text is null"

    #@7
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v1

    #@b
    .line 1425
    :cond_b
    if-nez p9, :cond_16

    #@d
    .line 1426
    new-instance v1, Ljava/lang/NullPointerException;

    #@f
    const-string/jumbo v2, "paint is null"

    #@12
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@15
    throw v1

    #@16
    .line 1428
    :cond_16
    or-int v1, p2, p3

    #@18
    array-length v2, p1

    #@19
    sub-int/2addr v2, p2

    #@1a
    sub-int/2addr v2, p3

    #@1b
    or-int/2addr v1, v2

    #@1c
    if-gez v1, :cond_24

    #@1e
    .line 1429
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@20
    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    #@23
    throw v1

    #@24
    .line 1431
    :cond_24
    if-eqz p8, :cond_47

    #@26
    const/4 v1, 0x1

    #@27
    move/from16 v0, p8

    #@29
    if-eq v0, v1, :cond_47

    #@2b
    .line 1432
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@2d
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string/jumbo v3, "unknown dir: "

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    move/from16 v0, p8

    #@3b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@46
    throw v1

    #@47
    .line 1435
    :cond_47
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@49
    move-object/from16 v0, p9

    #@4b
    iget v10, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@4d
    move-object v2, p1

    #@4e
    move v3, p2

    #@4f
    move v4, p3

    #@50
    move v5, p4

    #@51
    move/from16 v6, p5

    #@53
    move/from16 v7, p6

    #@55
    move/from16 v8, p7

    #@57
    move/from16 v9, p8

    #@59
    invoke-static/range {v1 .. v10}, Landroid/graphics/Canvas;->native_drawTextRun(I[CIIIIFFII)V

    #@5c
    .line 1437
    return-void
.end method

.method public drawVertices(Landroid/graphics/Canvas$VertexMode;I[FI[FI[II[SIILandroid/graphics/Paint;)V
    .registers 28
    .parameter "mode"
    .parameter "vertexCount"
    .parameter "verts"
    .parameter "vertOffset"
    .parameter "texs"
    .parameter "texOffset"
    .parameter "colors"
    .parameter "colorOffset"
    .parameter "indices"
    .parameter "indexOffset"
    .parameter "indexCount"
    .parameter "paint"

    #@0
    .prologue
    .line 1302
    move-object/from16 v0, p3

    #@2
    array-length v2, v0

    #@3
    move/from16 v0, p4

    #@5
    move/from16 v1, p2

    #@7
    invoke-static {v2, v0, v1}, Landroid/graphics/Canvas;->checkRange(III)V

    #@a
    .line 1303
    if-eqz p5, :cond_16

    #@c
    .line 1304
    move-object/from16 v0, p5

    #@e
    array-length v2, v0

    #@f
    move/from16 v0, p6

    #@11
    move/from16 v1, p2

    #@13
    invoke-static {v2, v0, v1}, Landroid/graphics/Canvas;->checkRange(III)V

    #@16
    .line 1306
    :cond_16
    if-eqz p7, :cond_22

    #@18
    .line 1307
    move-object/from16 v0, p7

    #@1a
    array-length v2, v0

    #@1b
    div-int/lit8 v3, p2, 0x2

    #@1d
    move/from16 v0, p8

    #@1f
    invoke-static {v2, v0, v3}, Landroid/graphics/Canvas;->checkRange(III)V

    #@22
    .line 1309
    :cond_22
    if-eqz p9, :cond_2e

    #@24
    .line 1310
    move-object/from16 v0, p9

    #@26
    array-length v2, v0

    #@27
    move/from16 v0, p10

    #@29
    move/from16 v1, p11

    #@2b
    invoke-static {v2, v0, v1}, Landroid/graphics/Canvas;->checkRange(III)V

    #@2e
    .line 1312
    :cond_2e
    iget v2, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@30
    move-object/from16 v0, p1

    #@32
    iget v3, v0, Landroid/graphics/Canvas$VertexMode;->nativeInt:I

    #@34
    move-object/from16 v0, p12

    #@36
    iget v14, v0, Landroid/graphics/Paint;->mNativePaint:I

    #@38
    move/from16 v4, p2

    #@3a
    move-object/from16 v5, p3

    #@3c
    move/from16 v6, p4

    #@3e
    move-object/from16 v7, p5

    #@40
    move/from16 v8, p6

    #@42
    move-object/from16 v9, p7

    #@44
    move/from16 v10, p8

    #@46
    move-object/from16 v11, p9

    #@48
    move/from16 v12, p10

    #@4a
    move/from16 v13, p11

    #@4c
    invoke-static/range {v2 .. v14}, Landroid/graphics/Canvas;->nativeDrawVertices(III[FI[FI[II[SIII)V

    #@4f
    .line 1315
    return-void
.end method

.method public final getClipBounds()Landroid/graphics/Rect;
    .registers 2

    #@0
    .prologue
    .line 774
    new-instance v0, Landroid/graphics/Rect;

    #@2
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@5
    .line 775
    .local v0, r:Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    #@8
    .line 776
    return-object v0
.end method

.method public getClipBounds(Landroid/graphics/Rect;)Z
    .registers 3
    .parameter "bounds"

    #@0
    .prologue
    .line 765
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-static {v0, p1}, Landroid/graphics/Canvas;->native_getClipBounds(ILandroid/graphics/Rect;)Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getDensity()I
    .registers 2

    #@0
    .prologue
    .line 244
    iget v0, p0, Landroid/graphics/Canvas;->mDensity:I

    #@2
    return v0
.end method

.method public getDrawFilter()Landroid/graphics/DrawFilter;
    .registers 2

    #@0
    .prologue
    .line 671
    iget-object v0, p0, Landroid/graphics/Canvas;->mDrawFilter:Landroid/graphics/DrawFilter;

    #@2
    return-object v0
.end method

.method protected getGL()Ljavax/microedition/khronos/opengles/GL;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 154
    const/4 v0, 0x0

    #@1
    return-object v0
.end method

.method public native getHeight()I
.end method

.method public final getMatrix()Landroid/graphics/Matrix;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 522
    new-instance v0, Landroid/graphics/Matrix;

    #@2
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    #@5
    .line 524
    .local v0, m:Landroid/graphics/Matrix;
    invoke-virtual {p0, v0}, Landroid/graphics/Canvas;->getMatrix(Landroid/graphics/Matrix;)V

    #@8
    .line 525
    return-object v0
.end method

.method public getMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "ctm"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 513
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p1, Landroid/graphics/Matrix;->native_instance:I

    #@4
    invoke-static {v0, v1}, Landroid/graphics/Canvas;->native_getCTM(II)V

    #@7
    .line 514
    return-void
.end method

.method public getMaximumBitmapHeight()I
    .registers 2

    #@0
    .prologue
    .line 290
    const/16 v0, 0x7ffe

    #@2
    return v0
.end method

.method public getMaximumBitmapWidth()I
    .registers 2

    #@0
    .prologue
    .line 279
    const/16 v0, 0x7ffe

    #@2
    return v0
.end method

.method public native getSaveCount()I
.end method

.method public native getWidth()I
.end method

.method public isHardwareAccelerated()Z
    .registers 2

    #@0
    .prologue
    .line 167
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public native isOpaque()Z
.end method

.method public quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z
    .registers 12
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "type"

    #@0
    .prologue
    .line 753
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v5, p5, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v3, p3

    #@7
    move v4, p4

    #@8
    invoke-static/range {v0 .. v5}, Landroid/graphics/Canvas;->native_quickReject(IFFFFI)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public quickReject(Landroid/graphics/Path;Landroid/graphics/Canvas$EdgeType;)Z
    .registers 6
    .parameter "path"
    .parameter "type"

    #@0
    .prologue
    .line 729
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    invoke-virtual {p1}, Landroid/graphics/Path;->ni()I

    #@5
    move-result v1

    #@6
    iget v2, p2, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@8
    invoke-static {v0, v1, v2}, Landroid/graphics/Canvas;->native_quickReject(III)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public quickReject(Landroid/graphics/RectF;Landroid/graphics/Canvas$EdgeType;)Z
    .registers 5
    .parameter "rect"
    .parameter "type"

    #@0
    .prologue
    .line 709
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    iget v1, p2, Landroid/graphics/Canvas$EdgeType;->nativeInt:I

    #@4
    invoke-static {v0, p1, v1}, Landroid/graphics/Canvas;->native_quickReject(ILandroid/graphics/RectF;I)Z

    #@7
    move-result v0

    #@8
    return v0
.end method

.method public native restore()V
.end method

.method public native restoreToCount(I)V
.end method

.method public native rotate(F)V
.end method

.method public final rotate(FFF)V
    .registers 6
    .parameter "degrees"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 467
    invoke-virtual {p0, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    #@3
    .line 468
    invoke-virtual {p0, p1}, Landroid/graphics/Canvas;->rotate(F)V

    #@6
    .line 469
    neg-float v0, p2

    #@7
    neg-float v1, p3

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@b
    .line 470
    return-void
.end method

.method public native save()I
.end method

.method public native save(I)I
.end method

.method public saveLayer(FFFFLandroid/graphics/Paint;I)I
    .registers 14
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "paint"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 359
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    if-eqz p5, :cond_10

    #@4
    iget v5, p5, Landroid/graphics/Paint;->mNativePaint:I

    #@6
    :goto_6
    move v1, p1

    #@7
    move v2, p2

    #@8
    move v3, p3

    #@9
    move v4, p4

    #@a
    move v6, p6

    #@b
    invoke-static/range {v0 .. v6}, Landroid/graphics/Canvas;->native_saveLayer(IFFFFII)I

    #@e
    move-result v0

    #@f
    return v0

    #@10
    :cond_10
    const/4 v5, 0x0

    #@11
    goto :goto_6
.end method

.method public saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I
    .registers 6
    .parameter "bounds"
    .parameter "paint"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 349
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    if-eqz p2, :cond_b

    #@4
    iget v0, p2, Landroid/graphics/Paint;->mNativePaint:I

    #@6
    :goto_6
    invoke-static {v1, p1, v0, p3}, Landroid/graphics/Canvas;->native_saveLayer(ILandroid/graphics/RectF;II)I

    #@9
    move-result v0

    #@a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_6
.end method

.method public saveLayerAlpha(FFFFII)I
    .registers 14
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"
    .parameter "alpha"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 390
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    invoke-static/range {v0 .. v6}, Landroid/graphics/Canvas;->native_saveLayerAlpha(IFFFFII)I

    #@b
    move-result v0

    #@c
    return v0
.end method

.method public saveLayerAlpha(Landroid/graphics/RectF;II)I
    .registers 6
    .parameter "bounds"
    .parameter "alpha"
    .parameter "saveFlags"

    #@0
    .prologue
    .line 381
    const/16 v0, 0xff

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    #@6
    move-result v1

    #@7
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    #@a
    move-result p2

    #@b
    .line 382
    iget v0, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@d
    invoke-static {v0, p1, p2, p3}, Landroid/graphics/Canvas;->native_saveLayerAlpha(ILandroid/graphics/RectF;II)I

    #@10
    move-result v0

    #@11
    return v0
.end method

.method public native scale(FF)V
.end method

.method public final scale(FFFF)V
    .registers 7
    .parameter "sx"
    .parameter "sy"
    .parameter "px"
    .parameter "py"

    #@0
    .prologue
    .line 447
    invoke-virtual {p0, p3, p4}, Landroid/graphics/Canvas;->translate(FF)V

    #@3
    .line 448
    invoke-virtual {p0, p1, p2}, Landroid/graphics/Canvas;->scale(FF)V

    #@6
    .line 449
    neg-float v0, p3

    #@7
    neg-float v1, p4

    #@8
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    #@b
    .line 450
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .registers 5
    .parameter "bitmap"

    #@0
    .prologue
    .line 180
    invoke-virtual {p0}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_e

    #@6
    .line 181
    new-instance v1, Ljava/lang/RuntimeException;

    #@8
    const-string v2, "Can\'t set a bitmap device on a GL canvas"

    #@a
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@d
    throw v1

    #@e
    .line 184
    :cond_e
    const/4 v0, 0x0

    #@f
    .line 185
    .local v0, pointer:I
    if-eqz p1, :cond_28

    #@11
    .line 186
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_1d

    #@17
    .line 187
    new-instance v1, Ljava/lang/IllegalStateException;

    #@19
    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    #@1c
    throw v1

    #@1d
    .line 189
    :cond_1d
    invoke-static {p1}, Landroid/graphics/Canvas;->throwIfRecycled(Landroid/graphics/Bitmap;)V

    #@20
    .line 190
    iget v1, p1, Landroid/graphics/Bitmap;->mDensity:I

    #@22
    iput v1, p0, Landroid/graphics/Canvas;->mDensity:I

    #@24
    .line 191
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@27
    move-result v0

    #@28
    .line 194
    :cond_28
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2a
    invoke-static {v1, v0}, Landroid/graphics/Canvas;->native_setBitmap(II)V

    #@2d
    .line 195
    iput-object p1, p0, Landroid/graphics/Canvas;->mBitmap:Landroid/graphics/Bitmap;

    #@2f
    .line 196
    return-void
.end method

.method public setDensity(I)V
    .registers 3
    .parameter "density"

    #@0
    .prologue
    .line 260
    iget-object v0, p0, Landroid/graphics/Canvas;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 261
    iget-object v0, p0, Landroid/graphics/Canvas;->mBitmap:Landroid/graphics/Bitmap;

    #@6
    invoke-virtual {v0, p1}, Landroid/graphics/Bitmap;->setDensity(I)V

    #@9
    .line 263
    :cond_9
    iput p1, p0, Landroid/graphics/Canvas;->mDensity:I

    #@b
    .line 264
    return-void
.end method

.method public setDrawFilter(Landroid/graphics/DrawFilter;)V
    .registers 4
    .parameter "filter"

    #@0
    .prologue
    .line 675
    const/4 v0, 0x0

    #@1
    .line 676
    .local v0, nativeFilter:I
    if-eqz p1, :cond_5

    #@3
    .line 677
    iget v0, p1, Landroid/graphics/DrawFilter;->mNativeInt:I

    #@5
    .line 679
    :cond_5
    iput-object p1, p0, Landroid/graphics/Canvas;->mDrawFilter:Landroid/graphics/DrawFilter;

    #@7
    .line 680
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@9
    invoke-static {v1, v0}, Landroid/graphics/Canvas;->nativeSetDrawFilter(II)V

    #@c
    .line 681
    return-void
.end method

.method public setMatrix(Landroid/graphics/Matrix;)V
    .registers 4
    .parameter "matrix"

    #@0
    .prologue
    .line 503
    iget v1, p0, Landroid/graphics/Canvas;->mNativeCanvas:I

    #@2
    if-nez p1, :cond_9

    #@4
    const/4 v0, 0x0

    #@5
    :goto_5
    invoke-static {v1, v0}, Landroid/graphics/Canvas;->native_setMatrix(II)V

    #@8
    .line 505
    return-void

    #@9
    .line 503
    :cond_9
    iget v0, p1, Landroid/graphics/Matrix;->native_instance:I

    #@b
    goto :goto_5
.end method

.method public setScreenDensity(I)V
    .registers 2
    .parameter "density"

    #@0
    .prologue
    .line 268
    iput p1, p0, Landroid/graphics/Canvas;->mScreenDensity:I

    #@2
    .line 269
    return-void
.end method

.method public setViewport(II)V
    .registers 3
    .parameter "width"
    .parameter "height"

    #@0
    .prologue
    .line 208
    return-void
.end method

.method public native skew(FF)V
.end method

.method public native translate(FF)V
.end method
