.class public Landroid/graphics/YuvImage;
.super Ljava/lang/Object;
.source "YuvImage.java"


# static fields
.field private static final WORKING_COMPRESS_STORAGE:I = 0x1000


# instance fields
.field private mData:[B

.field private mFormat:I

.field private mHeight:I

.field private mStrides:[I

.field private mWidth:I


# direct methods
.method public constructor <init>([BIII[I)V
    .registers 8
    .parameter "yuv"
    .parameter "format"
    .parameter "width"
    .parameter "height"
    .parameter "strides"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    const/16 v0, 0x11

    #@5
    if-eq p2, v0, :cond_14

    #@7
    const/16 v0, 0x14

    #@9
    if-eq p2, v0, :cond_14

    #@b
    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@d
    const-string/jumbo v1, "only support ImageFormat.NV21 and ImageFormat.YUY2 for now"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 87
    :cond_14
    if-lez p3, :cond_18

    #@16
    if-gtz p4, :cond_21

    #@18
    .line 88
    :cond_18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@1a
    const-string/jumbo v1, "width and height must large than 0"

    #@1d
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@20
    throw v0

    #@21
    .line 92
    :cond_21
    if-nez p1, :cond_2c

    #@23
    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@25
    const-string/jumbo v1, "yuv cannot be null"

    #@28
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@2b
    throw v0

    #@2c
    .line 96
    :cond_2c
    if-nez p5, :cond_3d

    #@2e
    .line 97
    invoke-direct {p0, p3, p2}, Landroid/graphics/YuvImage;->calculateStrides(II)[I

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@34
    .line 102
    :goto_34
    iput-object p1, p0, Landroid/graphics/YuvImage;->mData:[B

    #@36
    .line 103
    iput p2, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@38
    .line 104
    iput p3, p0, Landroid/graphics/YuvImage;->mWidth:I

    #@3a
    .line 105
    iput p4, p0, Landroid/graphics/YuvImage;->mHeight:I

    #@3c
    .line 106
    return-void

    #@3d
    .line 99
    :cond_3d
    iput-object p5, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@3f
    goto :goto_34
.end method

.method private adjustRectangle(Landroid/graphics/Rect;)V
    .registers 6
    .parameter "rect"

    #@0
    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@3
    move-result v1

    #@4
    .line 216
    .local v1, width:I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@7
    move-result v0

    #@8
    .line 217
    .local v0, height:I
    iget v2, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@a
    const/16 v3, 0x11

    #@c
    if-ne v2, v3, :cond_28

    #@e
    .line 219
    and-int/lit8 v1, v1, -0x2

    #@10
    .line 220
    and-int/lit8 v0, v0, -0x2

    #@12
    .line 221
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@14
    and-int/lit8 v2, v2, -0x2

    #@16
    iput v2, p1, Landroid/graphics/Rect;->left:I

    #@18
    .line 222
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@1a
    and-int/lit8 v2, v2, -0x2

    #@1c
    iput v2, p1, Landroid/graphics/Rect;->top:I

    #@1e
    .line 223
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@20
    add-int/2addr v2, v1

    #@21
    iput v2, p1, Landroid/graphics/Rect;->right:I

    #@23
    .line 224
    iget v2, p1, Landroid/graphics/Rect;->top:I

    #@25
    add-int/2addr v2, v0

    #@26
    iput v2, p1, Landroid/graphics/Rect;->bottom:I

    #@28
    .line 227
    :cond_28
    iget v2, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@2a
    const/16 v3, 0x14

    #@2c
    if-ne v2, v3, :cond_3b

    #@2e
    .line 229
    and-int/lit8 v1, v1, -0x2

    #@30
    .line 230
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@32
    and-int/lit8 v2, v2, -0x2

    #@34
    iput v2, p1, Landroid/graphics/Rect;->left:I

    #@36
    .line 231
    iget v2, p1, Landroid/graphics/Rect;->left:I

    #@38
    add-int/2addr v2, v1

    #@39
    iput v2, p1, Landroid/graphics/Rect;->right:I

    #@3b
    .line 233
    :cond_3b
    return-void
.end method

.method private calculateStrides(II)[I
    .registers 8
    .parameter "width"
    .parameter "format"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 200
    const/4 v0, 0x0

    #@3
    .line 201
    .local v0, strides:[I
    const/16 v2, 0x11

    #@5
    if-ne p2, v2, :cond_10

    #@7
    .line 202
    const/4 v2, 0x2

    #@8
    new-array v0, v2, [I

    #@a
    .end local v0           #strides:[I
    aput p1, v0, v3

    #@c
    aput p1, v0, v4

    #@e
    .restart local v0       #strides:[I
    move-object v1, v0

    #@f
    .line 211
    .end local v0           #strides:[I
    .local v1, strides:[I
    :goto_f
    return-object v1

    #@10
    .line 206
    .end local v1           #strides:[I
    .restart local v0       #strides:[I
    :cond_10
    const/16 v2, 0x14

    #@12
    if-ne p2, v2, :cond_1c

    #@14
    .line 207
    new-array v0, v4, [I

    #@16
    .end local v0           #strides:[I
    mul-int/lit8 v2, p1, 0x2

    #@18
    aput v2, v0, v3

    #@1a
    .restart local v0       #strides:[I
    move-object v1, v0

    #@1b
    .line 208
    .end local v0           #strides:[I
    .restart local v1       #strides:[I
    goto :goto_f

    #@1c
    .end local v1           #strides:[I
    .restart local v0       #strides:[I
    :cond_1c
    move-object v1, v0

    #@1d
    .line 211
    .end local v0           #strides:[I
    .restart local v1       #strides:[I
    goto :goto_f
.end method

.method private static native nativeCompressToJpeg([BIII[I[IILjava/io/OutputStream;[B)Z
.end method


# virtual methods
.method calculateOffsets(II)[I
    .registers 9
    .parameter "left"
    .parameter "top"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 183
    const/4 v0, 0x0

    #@3
    .line 184
    .local v0, offsets:[I
    iget v2, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@5
    const/16 v3, 0x11

    #@7
    if-ne v2, v3, :cond_2c

    #@9
    .line 185
    const/4 v2, 0x2

    #@a
    new-array v0, v2, [I

    #@c
    .end local v0           #offsets:[I
    iget-object v2, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@e
    aget v2, v2, v4

    #@10
    mul-int/2addr v2, p2

    #@11
    add-int/2addr v2, p1

    #@12
    aput v2, v0, v4

    #@14
    iget v2, p0, Landroid/graphics/YuvImage;->mHeight:I

    #@16
    iget-object v3, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@18
    aget v3, v3, v4

    #@1a
    mul-int/2addr v2, v3

    #@1b
    div-int/lit8 v3, p2, 0x2

    #@1d
    iget-object v4, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@1f
    aget v4, v4, v5

    #@21
    mul-int/2addr v3, v4

    #@22
    add-int/2addr v2, v3

    #@23
    div-int/lit8 v3, p1, 0x2

    #@25
    mul-int/lit8 v3, v3, 0x2

    #@27
    add-int/2addr v2, v3

    #@28
    aput v2, v0, v5

    #@2a
    .restart local v0       #offsets:[I
    move-object v1, v0

    #@2b
    .line 196
    .end local v0           #offsets:[I
    .local v1, offsets:[I
    :goto_2b
    return-object v1

    #@2c
    .line 191
    .end local v1           #offsets:[I
    .restart local v0       #offsets:[I
    :cond_2c
    iget v2, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@2e
    const/16 v3, 0x14

    #@30
    if-ne v2, v3, :cond_42

    #@32
    .line 192
    new-array v0, v5, [I

    #@34
    .end local v0           #offsets:[I
    iget-object v2, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@36
    aget v2, v2, v4

    #@38
    mul-int/2addr v2, p2

    #@39
    div-int/lit8 v3, p1, 0x2

    #@3b
    mul-int/lit8 v3, v3, 0x4

    #@3d
    add-int/2addr v2, v3

    #@3e
    aput v2, v0, v4

    #@40
    .restart local v0       #offsets:[I
    move-object v1, v0

    #@41
    .line 193
    .end local v0           #offsets:[I
    .restart local v1       #offsets:[I
    goto :goto_2b

    #@42
    .end local v1           #offsets:[I
    .restart local v0       #offsets:[I
    :cond_42
    move-object v1, v0

    #@43
    .line 196
    .end local v0           #offsets:[I
    .restart local v1       #offsets:[I
    goto :goto_2b
.end method

.method public compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z
    .registers 14
    .parameter "rectangle"
    .parameter "quality"
    .parameter "stream"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 124
    new-instance v9, Landroid/graphics/Rect;

    #@3
    iget v0, p0, Landroid/graphics/YuvImage;->mWidth:I

    #@5
    iget v1, p0, Landroid/graphics/YuvImage;->mHeight:I

    #@7
    invoke-direct {v9, v2, v2, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    #@a
    .line 125
    .local v9, wholeImage:Landroid/graphics/Rect;
    invoke-virtual {v9, p1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_19

    #@10
    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@12
    const-string/jumbo v1, "rectangle is not inside the image"

    #@15
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@18
    throw v0

    #@19
    .line 130
    :cond_19
    if-ltz p2, :cond_1f

    #@1b
    const/16 v0, 0x64

    #@1d
    if-le p2, v0, :cond_28

    #@1f
    .line 131
    :cond_1f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@21
    const-string/jumbo v1, "quality must be 0..100"

    #@24
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@27
    throw v0

    #@28
    .line 134
    :cond_28
    if-nez p3, :cond_33

    #@2a
    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@2c
    const-string/jumbo v1, "stream cannot be null"

    #@2f
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@32
    throw v0

    #@33
    .line 138
    :cond_33
    invoke-direct {p0, p1}, Landroid/graphics/YuvImage;->adjustRectangle(Landroid/graphics/Rect;)V

    #@36
    .line 139
    iget v0, p1, Landroid/graphics/Rect;->left:I

    #@38
    iget v1, p1, Landroid/graphics/Rect;->top:I

    #@3a
    invoke-virtual {p0, v0, v1}, Landroid/graphics/YuvImage;->calculateOffsets(II)[I

    #@3d
    move-result-object v4

    #@3e
    .line 141
    .local v4, offsets:[I
    iget-object v0, p0, Landroid/graphics/YuvImage;->mData:[B

    #@40
    iget v1, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@42
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    #@45
    move-result v2

    #@46
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    #@49
    move-result v3

    #@4a
    iget-object v5, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@4c
    const/16 v6, 0x1000

    #@4e
    new-array v8, v6, [B

    #@50
    move v6, p2

    #@51
    move-object v7, p3

    #@52
    invoke-static/range {v0 .. v8}, Landroid/graphics/YuvImage;->nativeCompressToJpeg([BIII[I[IILjava/io/OutputStream;[B)Z

    #@55
    move-result v0

    #@56
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 179
    iget v0, p0, Landroid/graphics/YuvImage;->mHeight:I

    #@2
    return v0
.end method

.method public getStrides()[I
    .registers 2

    #@0
    .prologue
    .line 165
    iget-object v0, p0, Landroid/graphics/YuvImage;->mStrides:[I

    #@2
    return-object v0
.end method

.method public getWidth()I
    .registers 2

    #@0
    .prologue
    .line 172
    iget v0, p0, Landroid/graphics/YuvImage;->mWidth:I

    #@2
    return v0
.end method

.method public getYuvData()[B
    .registers 2

    #@0
    .prologue
    .line 151
    iget-object v0, p0, Landroid/graphics/YuvImage;->mData:[B

    #@2
    return-object v0
.end method

.method public getYuvFormat()I
    .registers 2

    #@0
    .prologue
    .line 158
    iget v0, p0, Landroid/graphics/YuvImage;->mFormat:I

    #@2
    return v0
.end method
