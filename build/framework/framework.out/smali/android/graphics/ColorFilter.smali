.class public Landroid/graphics/ColorFilter;
.super Ljava/lang/Object;
.source "ColorFilter.java"


# instance fields
.field public nativeColorFilter:I

.field native_instance:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static native finalizer(II)V
.end method


# virtual methods
.method protected finalize()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 35
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_b

    #@3
    .line 37
    iget v0, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@5
    iget v1, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@7
    invoke-static {v0, v1}, Landroid/graphics/ColorFilter;->finalizer(II)V

    #@a
    .line 39
    return-void

    #@b
    .line 37
    :catchall_b
    move-exception v0

    #@c
    iget v1, p0, Landroid/graphics/ColorFilter;->native_instance:I

    #@e
    iget v2, p0, Landroid/graphics/ColorFilter;->nativeColorFilter:I

    #@10
    invoke-static {v1, v2}, Landroid/graphics/ColorFilter;->finalizer(II)V

    #@13
    throw v0
.end method
