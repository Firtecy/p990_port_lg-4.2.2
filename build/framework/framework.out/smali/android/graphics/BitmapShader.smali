.class public Landroid/graphics/BitmapShader;
.super Landroid/graphics/Shader;
.source "BitmapShader.java"


# instance fields
.field public final mBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V
    .registers 8
    .parameter "bitmap"
    .parameter "tileX"
    .parameter "tileY"

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/graphics/Shader;-><init>()V

    #@3
    .line 39
    iput-object p1, p0, Landroid/graphics/BitmapShader;->mBitmap:Landroid/graphics/Bitmap;

    #@5
    .line 40
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->ni()I

    #@8
    move-result v0

    #@9
    .line 41
    .local v0, b:I
    iget v1, p2, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@b
    iget v2, p3, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@d
    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapShader;->nativeCreate(III)I

    #@10
    move-result v1

    #@11
    iput v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@13
    .line 42
    iget v1, p0, Landroid/graphics/Shader;->native_instance:I

    #@15
    iget v2, p2, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@17
    iget v3, p3, Landroid/graphics/Shader$TileMode;->nativeInt:I

    #@19
    invoke-static {v1, v0, v2, v3}, Landroid/graphics/BitmapShader;->nativePostCreate(IIII)I

    #@1c
    move-result v1

    #@1d
    iput v1, p0, Landroid/graphics/Shader;->native_shader:I

    #@1f
    .line 43
    return-void
.end method

.method private static native nativeCreate(III)I
.end method

.method private static native nativePostCreate(IIII)I
.end method
