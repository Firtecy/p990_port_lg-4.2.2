.class public Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
.super Ljava/lang/Object;
.source "EmojiFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/emoji/EmojiFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UnicodeRangeContainer"
.end annotation


# instance fields
.field private final mEightByteCodeMax:Ljava/lang/String;

.field private final mEightByteCodeMin:Ljava/lang/String;

.field private final mExtraRangeMax:Ljava/lang/String;

.field private final mExtraRangeMin:Ljava/lang/String;

.field private final mOneFRangeMax:Ljava/lang/String;

.field private final mOneFRangeMin:Ljava/lang/String;

.field private final mThousandRangeMax:Ljava/lang/String;

.field private final mThousandRangeMin:Ljava/lang/String;

.field final synthetic this$0:Landroid/emoji/EmojiFactory;


# direct methods
.method public constructor <init>(Landroid/emoji/EmojiFactory;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 170
    iput-object p1, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->this$0:Landroid/emoji/EmojiFactory;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 171
    const-string v0, "-1"

    #@7
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMax:Ljava/lang/String;

    #@9
    .line 172
    const-string v0, "-1"

    #@b
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMin:Ljava/lang/String;

    #@d
    .line 173
    const-string v0, "-1"

    #@f
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMax:Ljava/lang/String;

    #@11
    .line 174
    const-string v0, "-1"

    #@13
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMin:Ljava/lang/String;

    #@15
    .line 175
    const-string v0, "-1"

    #@17
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMax:Ljava/lang/String;

    #@19
    .line 176
    const-string v0, "-1"

    #@1b
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMin:Ljava/lang/String;

    #@1d
    .line 177
    const-string v0, "-1"

    #@1f
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMax:Ljava/lang/String;

    #@21
    .line 178
    const-string v0, "-1"

    #@23
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMin:Ljava/lang/String;

    #@25
    .line 179
    return-void
.end method

.method public constructor <init>(Landroid/emoji/EmojiFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter "onefMax"
    .parameter "onefMin"
    .parameter "thousandMax"
    .parameter "thousandMin"

    #@0
    .prologue
    .line 181
    iput-object p1, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->this$0:Landroid/emoji/EmojiFactory;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 182
    iput-object p2, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMax:Ljava/lang/String;

    #@7
    .line 183
    iput-object p3, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMin:Ljava/lang/String;

    #@9
    .line 184
    iput-object p4, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMax:Ljava/lang/String;

    #@b
    .line 185
    iput-object p5, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMin:Ljava/lang/String;

    #@d
    .line 186
    const-string v0, "-1"

    #@f
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMax:Ljava/lang/String;

    #@11
    .line 187
    const-string v0, "-1"

    #@13
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMin:Ljava/lang/String;

    #@15
    .line 188
    const-string v0, "-1"

    #@17
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMax:Ljava/lang/String;

    #@19
    .line 189
    const-string v0, "-1"

    #@1b
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMin:Ljava/lang/String;

    #@1d
    .line 190
    return-void
.end method

.method public constructor <init>(Landroid/emoji/EmojiFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter "onefMax"
    .parameter "onefMin"
    .parameter "thousandMax"
    .parameter "thousandMin"
    .parameter "extraMax"
    .parameter "extraMin"

    #@0
    .prologue
    .line 192
    iput-object p1, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->this$0:Landroid/emoji/EmojiFactory;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 193
    iput-object p2, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMax:Ljava/lang/String;

    #@7
    .line 194
    iput-object p3, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMin:Ljava/lang/String;

    #@9
    .line 195
    iput-object p4, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMax:Ljava/lang/String;

    #@b
    .line 196
    iput-object p5, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMin:Ljava/lang/String;

    #@d
    .line 197
    iput-object p6, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMax:Ljava/lang/String;

    #@f
    .line 198
    iput-object p7, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMin:Ljava/lang/String;

    #@11
    .line 199
    const-string v0, "-1"

    #@13
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMax:Ljava/lang/String;

    #@15
    .line 200
    const-string v0, "-1"

    #@17
    iput-object v0, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMin:Ljava/lang/String;

    #@19
    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/emoji/EmojiFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter "onefMax"
    .parameter "onefMin"
    .parameter "thousandMax"
    .parameter "thousandMin"
    .parameter "extraMax"
    .parameter "extraMin"
    .parameter "eightMax"
    .parameter "eightMin"

    #@0
    .prologue
    .line 206
    iput-object p1, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->this$0:Landroid/emoji/EmojiFactory;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 207
    iput-object p2, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMax:Ljava/lang/String;

    #@7
    .line 208
    iput-object p3, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mOneFRangeMin:Ljava/lang/String;

    #@9
    .line 209
    iput-object p4, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMax:Ljava/lang/String;

    #@b
    .line 210
    iput-object p5, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mThousandRangeMin:Ljava/lang/String;

    #@d
    .line 211
    iput-object p6, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMax:Ljava/lang/String;

    #@f
    .line 212
    iput-object p7, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mExtraRangeMin:Ljava/lang/String;

    #@11
    .line 213
    iput-object p8, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMax:Ljava/lang/String;

    #@13
    .line 214
    iput-object p9, p0, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;->mEightByteCodeMin:Ljava/lang/String;

    #@15
    .line 215
    return-void
.end method
