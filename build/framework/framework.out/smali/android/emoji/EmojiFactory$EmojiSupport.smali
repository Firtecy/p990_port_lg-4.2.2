.class public final Landroid/emoji/EmojiFactory$EmojiSupport;
.super Ljava/lang/Object;
.source "EmojiFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/emoji/EmojiFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "EmojiSupport"
.end annotation


# instance fields
.field private mEightByteRangeMaxInt:I

.field private mEightByteRangeMinInt:I

.field private final mEmojiVersion:I

.field private mExtraRangeMaxInt:I

.field private mExtraRangeMinInt:I

.field private mOneFRangeMaxInt:I

.field private mOneFRangeMinInt:I

.field private final mRangeContainer:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;

.field private mThousandRangeMaxInt:I

.field private mThousandRangeMinInt:I

.field final synthetic this$0:Landroid/emoji/EmojiFactory;


# direct methods
.method public constructor <init>(Landroid/emoji/EmojiFactory;)V
    .registers 26
    .parameter

    #@0
    .prologue
    .line 233
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p0

    #@4
    iput-object v0, v1, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@6
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@9
    .line 223
    const/4 v3, -0x1

    #@a
    move-object/from16 v0, p0

    #@c
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@e
    .line 224
    const/4 v3, -0x1

    #@f
    move-object/from16 v0, p0

    #@11
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@13
    .line 225
    const/4 v3, -0x1

    #@14
    move-object/from16 v0, p0

    #@16
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@18
    .line 226
    const/4 v3, -0x1

    #@19
    move-object/from16 v0, p0

    #@1b
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@1d
    .line 227
    const/4 v3, -0x1

    #@1e
    move-object/from16 v0, p0

    #@20
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@22
    .line 228
    const/4 v3, -0x1

    #@23
    move-object/from16 v0, p0

    #@25
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@27
    .line 229
    const/4 v3, -0x1

    #@28
    move-object/from16 v0, p0

    #@2a
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@2c
    .line 230
    const/4 v3, -0x1

    #@2d
    move-object/from16 v0, p0

    #@2f
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@31
    .line 234
    const/16 v23, 0x0

    #@33
    .line 237
    .local v23, version:I
    sget-boolean v3, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@35
    if-nez v3, :cond_8d

    #@37
    .line 238
    const/4 v3, -0x1

    #@38
    move-object/from16 v0, p0

    #@3a
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@3c
    .line 239
    const/4 v3, -0x1

    #@3d
    move-object/from16 v0, p0

    #@3f
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@41
    .line 240
    const/4 v3, -0x1

    #@42
    move-object/from16 v0, p0

    #@44
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@46
    .line 241
    const/4 v3, -0x1

    #@47
    move-object/from16 v0, p0

    #@49
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@4b
    .line 242
    const/4 v3, -0x1

    #@4c
    move-object/from16 v0, p0

    #@4e
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@50
    .line 243
    const/4 v3, -0x1

    #@51
    move-object/from16 v0, p0

    #@53
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@55
    .line 244
    const/4 v3, -0x1

    #@56
    move-object/from16 v0, p0

    #@58
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@5a
    .line 245
    const/4 v3, -0x1

    #@5b
    move-object/from16 v0, p0

    #@5d
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@5f
    .line 246
    new-instance v2, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;

    #@61
    move-object/from16 v0, p1

    #@63
    invoke-direct {v2, v0}, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;-><init>(Landroid/emoji/EmojiFactory;)V

    #@66
    .line 337
    .local v2, rangeTemp:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
    :goto_66
    move/from16 v0, v23

    #@68
    move-object/from16 v1, p0

    #@6a
    iput v0, v1, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@6c
    .line 338
    move-object/from16 v0, p0

    #@6e
    iput-object v2, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mRangeContainer:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;

    #@70
    .line 339
    const-string v3, "Emoji"

    #@72
    new-instance v4, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v5, "Emoji version is "

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    move-object/from16 v0, p0

    #@7f
    iget v5, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 343
    return-void

    #@8d
    .line 248
    .end local v2           #rangeTemp:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
    :cond_8d
    invoke-static {}, Landroid/content/res/AssetManager;->getSystem()Landroid/content/res/AssetManager;

    #@90
    move-result-object v12

    #@91
    .line 249
    .local v12, assetMngr:Landroid/content/res/AssetManager;
    const/16 v17, 0x0

    #@93
    .line 252
    .local v17, is:Ljava/io/InputStream;
    :try_start_93
    const-string v3, "emoji/emoji_table.csv"

    #@95
    const/4 v4, 0x3

    #@96
    invoke-virtual {v12, v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_93 .. :try_end_99} :catch_260

    #@99
    move-result-object v17

    #@9a
    .line 261
    :goto_9a
    if-eqz v17, :cond_217

    #@9c
    .line 263
    :try_start_9c
    const-string v3, "Emoji"

    #@9e
    new-instance v4, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v5, "get inputStream from csv file.. "

    #@a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v4

    #@a9
    move-object/from16 v0, v17

    #@ab
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 264
    new-instance v13, Ljava/io/BufferedReader;

    #@b8
    new-instance v3, Ljava/io/InputStreamReader;

    #@ba
    move-object/from16 v0, v17

    #@bc
    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    #@bf
    invoke-direct {v13, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    #@c2
    .line 266
    .local v13, br:Ljava/io/BufferedReader;
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@c5
    move-result-object v16

    #@c6
    .line 267
    .local v16, firstlineStr:Ljava/lang/String;
    const-string v3, "Emoji"

    #@c8
    new-instance v4, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v5, "get string form csv file.. "

    #@cf
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v4

    #@d3
    move-object/from16 v0, v16

    #@d5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v4

    #@dd
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 270
    if-eqz v16, :cond_103

    #@e2
    .line 271
    const-string v3, ","

    #@e4
    move-object/from16 v0, v16

    #@e6
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@e9
    move-result-object v22

    #@ea
    .line 273
    .local v22, str_version:[Ljava/lang/String;
    const/4 v3, 0x0

    #@eb
    aget-object v3, v22, v3

    #@ed
    const-string/jumbo v4, "version"

    #@f0
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@f3
    move-result v3

    #@f4
    if-nez v3, :cond_103

    #@f6
    move-object/from16 v0, v22

    #@f8
    array-length v3, v0

    #@f9
    const/4 v4, 0x1

    #@fa
    if-le v3, v4, :cond_103

    #@fc
    .line 274
    const/4 v3, 0x1

    #@fd
    aget-object v3, v22, v3

    #@ff
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@102
    move-result v23

    #@103
    .line 280
    .end local v22           #str_version:[Ljava/lang/String;
    :cond_103
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@106
    move-result-object v16

    #@107
    .line 281
    if-eqz v16, :cond_13c

    #@109
    .line 282
    const-string v3, ","

    #@10b
    move-object/from16 v0, v16

    #@10d
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@110
    move-result-object v20

    #@111
    .line 283
    .local v20, str_oneFRange:[Ljava/lang/String;
    const/4 v3, 0x0

    #@112
    aget-object v3, v20, v3

    #@114
    const-string v4, "fourbyterange"

    #@116
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@119
    move-result v3

    #@11a
    if-nez v3, :cond_13c

    #@11c
    move-object/from16 v0, v20

    #@11e
    array-length v3, v0

    #@11f
    const/4 v4, 0x3

    #@120
    if-ne v3, v4, :cond_13c

    #@122
    .line 284
    const/4 v3, 0x1

    #@123
    aget-object v3, v20, v3

    #@125
    const/16 v4, 0x10

    #@127
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@12a
    move-result v3

    #@12b
    move-object/from16 v0, p0

    #@12d
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@12f
    .line 285
    const/4 v3, 0x2

    #@130
    aget-object v3, v20, v3

    #@132
    const/16 v4, 0x10

    #@134
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@137
    move-result v3

    #@138
    move-object/from16 v0, p0

    #@13a
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@13c
    .line 289
    .end local v20           #str_oneFRange:[Ljava/lang/String;
    :cond_13c
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@13f
    move-result-object v16

    #@140
    .line 290
    if-eqz v16, :cond_176

    #@142
    .line 291
    const-string v3, ","

    #@144
    move-object/from16 v0, v16

    #@146
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@149
    move-result-object v21

    #@14a
    .line 292
    .local v21, str_unicodeRange:[Ljava/lang/String;
    const/4 v3, 0x0

    #@14b
    aget-object v3, v21, v3

    #@14d
    const-string/jumbo v4, "unicoderange"

    #@150
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@153
    move-result v3

    #@154
    if-nez v3, :cond_176

    #@156
    move-object/from16 v0, v21

    #@158
    array-length v3, v0

    #@159
    const/4 v4, 0x3

    #@15a
    if-ne v3, v4, :cond_176

    #@15c
    .line 293
    const/4 v3, 0x1

    #@15d
    aget-object v3, v21, v3

    #@15f
    const/16 v4, 0x10

    #@161
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@164
    move-result v3

    #@165
    move-object/from16 v0, p0

    #@167
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@169
    .line 294
    const/4 v3, 0x2

    #@16a
    aget-object v3, v21, v3

    #@16c
    const/16 v4, 0x10

    #@16e
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@171
    move-result v3

    #@172
    move-object/from16 v0, p0

    #@174
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@176
    .line 298
    .end local v21           #str_unicodeRange:[Ljava/lang/String;
    :cond_176
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@179
    move-result-object v16

    #@17a
    .line 299
    if-eqz v16, :cond_1af

    #@17c
    .line 300
    const-string v3, ","

    #@17e
    move-object/from16 v0, v16

    #@180
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@183
    move-result-object v19

    #@184
    .line 301
    .local v19, str_extraRange:[Ljava/lang/String;
    const/4 v3, 0x0

    #@185
    aget-object v3, v19, v3

    #@187
    const-string v4, "extrarange"

    #@189
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@18c
    move-result v3

    #@18d
    if-nez v3, :cond_1af

    #@18f
    move-object/from16 v0, v19

    #@191
    array-length v3, v0

    #@192
    const/4 v4, 0x3

    #@193
    if-ne v3, v4, :cond_1af

    #@195
    .line 302
    const/4 v3, 0x1

    #@196
    aget-object v3, v19, v3

    #@198
    const/16 v4, 0x10

    #@19a
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@19d
    move-result v3

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@1a2
    .line 303
    const/4 v3, 0x2

    #@1a3
    aget-object v3, v19, v3

    #@1a5
    const/16 v4, 0x10

    #@1a7
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1aa
    move-result v3

    #@1ab
    move-object/from16 v0, p0

    #@1ad
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@1af
    .line 307
    .end local v19           #str_extraRange:[Ljava/lang/String;
    :cond_1af
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1b2
    move-result-object v16

    #@1b3
    .line 308
    if-eqz v16, :cond_1e8

    #@1b5
    .line 309
    const-string v3, ","

    #@1b7
    move-object/from16 v0, v16

    #@1b9
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1bc
    move-result-object v19

    #@1bd
    .line 310
    .restart local v19       #str_extraRange:[Ljava/lang/String;
    const/4 v3, 0x0

    #@1be
    aget-object v3, v19, v3

    #@1c0
    const-string v4, "eightbyterange"

    #@1c2
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1c5
    move-result v3

    #@1c6
    if-nez v3, :cond_1e8

    #@1c8
    move-object/from16 v0, v19

    #@1ca
    array-length v3, v0

    #@1cb
    const/4 v4, 0x3

    #@1cc
    if-ne v3, v4, :cond_1e8

    #@1ce
    .line 311
    const/4 v3, 0x1

    #@1cf
    aget-object v3, v19, v3

    #@1d1
    const/16 v4, 0x10

    #@1d3
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1d6
    move-result v3

    #@1d7
    move-object/from16 v0, p0

    #@1d9
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@1db
    .line 312
    const/4 v3, 0x2

    #@1dc
    aget-object v3, v19, v3

    #@1de
    const/16 v4, 0x10

    #@1e0
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1e3
    move-result v3

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iput v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@1e8
    .line 316
    .end local v19           #str_extraRange:[Ljava/lang/String;
    :cond_1e8
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1eb
    move-result-object v16

    #@1ec
    .line 317
    if-eqz v16, :cond_214

    #@1ee
    .line 318
    const-string v3, ","

    #@1f0
    move-object/from16 v0, v16

    #@1f2
    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@1f5
    move-result-object v18

    #@1f6
    .line 319
    .local v18, keycheck:[Ljava/lang/String;
    const/4 v3, 0x0

    #@1f7
    aget-object v3, v18, v3

    #@1f9
    const-string v4, "default"

    #@1fb
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1fe
    move-result v3

    #@1ff
    if-nez v3, :cond_20d

    #@201
    const/4 v3, 0x1

    #@202
    aget-object v3, v18, v3

    #@204
    const-string/jumbo v4, "start"

    #@207
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@20a
    move-result v3

    #@20b
    if-eqz v3, :cond_214

    #@20d
    .line 320
    :cond_20d
    const-string v3, "Emoji"

    #@20f
    const-string v4, "error : emoji table might have more information check emoji table"

    #@211
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@214
    .line 324
    .end local v18           #keycheck:[Ljava/lang/String;
    :cond_214
    invoke-virtual/range {v17 .. v17}, Ljava/io/InputStream;->close()V
    :try_end_217
    .catch Ljava/io/IOException; {:try_start_9c .. :try_end_217} :catch_285

    #@217
    .line 331
    .end local v13           #br:Ljava/io/BufferedReader;
    .end local v16           #firstlineStr:Ljava/lang/String;
    :cond_217
    :goto_217
    new-instance v2, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;

    #@219
    move-object/from16 v0, p0

    #@21b
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@21d
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@220
    move-result-object v4

    #@221
    move-object/from16 v0, p0

    #@223
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@225
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@228
    move-result-object v5

    #@229
    move-object/from16 v0, p0

    #@22b
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@22d
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@230
    move-result-object v6

    #@231
    move-object/from16 v0, p0

    #@233
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@235
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@238
    move-result-object v7

    #@239
    move-object/from16 v0, p0

    #@23b
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@23d
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@240
    move-result-object v8

    #@241
    move-object/from16 v0, p0

    #@243
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@245
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@248
    move-result-object v9

    #@249
    move-object/from16 v0, p0

    #@24b
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@24d
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@250
    move-result-object v10

    #@251
    move-object/from16 v0, p0

    #@253
    iget v3, v0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@255
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@258
    move-result-object v11

    #@259
    move-object/from16 v3, p1

    #@25b
    invoke-direct/range {v2 .. v11}, Landroid/emoji/EmojiFactory$UnicodeRangeContainer;-><init>(Landroid/emoji/EmojiFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@25e
    .restart local v2       #rangeTemp:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
    goto/16 :goto_66

    #@260
    .line 253
    .end local v2           #rangeTemp:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
    :catch_260
    move-exception v14

    #@261
    .line 255
    .local v14, e:Ljava/io/IOException;
    :try_start_261
    const-string v3, "emoji_base/emoji_table.csv"

    #@263
    const/4 v4, 0x3

    #@264
    invoke-virtual {v12, v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_267
    .catch Ljava/io/IOException; {:try_start_261 .. :try_end_267} :catch_26a

    #@267
    move-result-object v17

    #@268
    goto/16 :goto_9a

    #@26a
    .line 256
    :catch_26a
    move-exception v15

    #@26b
    .line 257
    .local v15, ex:Ljava/io/IOException;
    const-string v3, "Emoji"

    #@26d
    new-instance v4, Ljava/lang/StringBuilder;

    #@26f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@272
    const-string v5, "error : emoji table asset is not found"

    #@274
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@277
    move-result-object v4

    #@278
    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@27b
    move-result-object v4

    #@27c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27f
    move-result-object v4

    #@280
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@283
    goto/16 :goto_9a

    #@285
    .line 325
    .end local v14           #e:Ljava/io/IOException;
    .end local v15           #ex:Ljava/io/IOException;
    :catch_285
    move-exception v14

    #@286
    .line 326
    .restart local v14       #e:Ljava/io/IOException;
    const-string v3, "Emoji"

    #@288
    new-instance v4, Ljava/lang/StringBuilder;

    #@28a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28d
    const-string v5, "error : while reading emoji table"

    #@28f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v4

    #@293
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@296
    move-result-object v4

    #@297
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29a
    move-result-object v4

    #@29b
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29e
    goto/16 :goto_217
.end method


# virtual methods
.method public getEmojiUnicodeRange()Landroid/emoji/EmojiFactory$UnicodeRangeContainer;
    .registers 4

    #@0
    .prologue
    .line 346
    sget-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@2
    if-eqz v0, :cond_8

    #@4
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@6
    if-nez v0, :cond_a

    #@8
    .line 347
    :cond_8
    const/4 v0, 0x0

    #@9
    .line 353
    :goto_9
    return-object v0

    #@a
    .line 348
    :cond_a
    const-string v0, "Emoji"

    #@c
    const-string v1, "Display UnicodeRange.."

    #@e
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 349
    const-string v0, "Emoji"

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "First Range : "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string/jumbo v2, "~"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 350
    const-string v0, "Emoji"

    #@3a
    new-instance v1, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v2, "Second Range : "

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    const-string/jumbo v2, "~"

    #@4e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v1

    #@52
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@54
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v1

    #@5c
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 351
    const-string v0, "Emoji"

    #@61
    new-instance v1, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v2, "Third Range : "

    #@68
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v1

    #@6c
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    const-string/jumbo v2, "~"

    #@75
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v1

    #@79
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@7b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 352
    const-string v0, "Emoji"

    #@88
    new-instance v1, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v2, "Eight byte Range : "

    #@8f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v1

    #@93
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v1

    #@99
    const-string/jumbo v2, "~"

    #@9c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v1

    #@a0
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@a2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v1

    #@a6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v1

    #@aa
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    .line 353
    iget-object v0, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mRangeContainer:Landroid/emoji/EmojiFactory$UnicodeRangeContainer;

    #@af
    goto/16 :goto_9
.end method

.method public isInCountryCodeRange(I)Z
    .registers 4
    .parameter "pua"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 399
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@3
    if-eqz v1, :cond_9

    #@5
    sget-object v1, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@7
    if-nez v1, :cond_a

    #@9
    .line 409
    :cond_9
    :goto_9
    return v0

    #@a
    .line 402
    :cond_a
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@c
    packed-switch v1, :pswitch_data_1a

    #@f
    goto :goto_9

    #@10
    .line 405
    :pswitch_10
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMaxInt:I

    #@12
    if-gt p1, v1, :cond_9

    #@14
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEightByteRangeMinInt:I

    #@16
    if-lt p1, v1, :cond_9

    #@18
    .line 406
    const/4 v0, 0x1

    #@19
    goto :goto_9

    #@1a
    .line 402
    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_10
    .end packed-switch
.end method

.method public isInCountryCodeTable([I)Z
    .registers 4
    .parameter "puaArray"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 413
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@3
    if-eqz v1, :cond_9

    #@5
    sget-object v1, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@7
    if-nez v1, :cond_a

    #@9
    .line 421
    :cond_9
    :goto_9
    return v0

    #@a
    .line 415
    :cond_a
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@c
    packed-switch v1, :pswitch_data_1e

    #@f
    goto :goto_9

    #@10
    .line 418
    :pswitch_10
    iget-object v0, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@12
    iget-object v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@14
    invoke-static {v1}, Landroid/emoji/EmojiFactory;->access$100(Landroid/emoji/EmojiFactory;)I

    #@17
    move-result v1

    #@18
    invoke-static {v0, v1, p1}, Landroid/emoji/EmojiFactory;->access$300(Landroid/emoji/EmojiFactory;I[I)Z

    #@1b
    move-result v0

    #@1c
    goto :goto_9

    #@1d
    .line 415
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_10
    .end packed-switch
.end method

.method public isInEmojiUnicodeTable(C)Z
    .registers 6
    .parameter "pua"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 378
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@4
    if-eqz v2, :cond_a

    #@6
    sget-object v2, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@8
    if-nez v2, :cond_c

    #@a
    :cond_a
    move v0, v1

    #@b
    .line 395
    :cond_b
    :goto_b
    return v0

    #@c
    .line 381
    :cond_c
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@e
    packed-switch v2, :pswitch_data_42

    #@11
    move v0, v1

    #@12
    .line 393
    goto :goto_b

    #@13
    .line 384
    :pswitch_13
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMaxInt:I

    #@15
    if-gt p1, v2, :cond_29

    #@17
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mThousandRangeMinInt:I

    #@19
    if-lt p1, v2, :cond_29

    #@1b
    iget-object v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@1d
    iget-object v3, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@1f
    invoke-static {v3}, Landroid/emoji/EmojiFactory;->access$100(Landroid/emoji/EmojiFactory;)I

    #@22
    move-result v3

    #@23
    invoke-static {v2, v3, p1}, Landroid/emoji/EmojiFactory;->access$200(Landroid/emoji/EmojiFactory;II)Z

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_b

    #@29
    .line 387
    :cond_29
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMaxInt:I

    #@2b
    if-gt p1, v2, :cond_3f

    #@2d
    iget v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mExtraRangeMinInt:I

    #@2f
    if-lt p1, v2, :cond_3f

    #@31
    iget-object v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@33
    iget-object v3, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@35
    invoke-static {v3}, Landroid/emoji/EmojiFactory;->access$100(Landroid/emoji/EmojiFactory;)I

    #@38
    move-result v3

    #@39
    invoke-static {v2, v3, p1}, Landroid/emoji/EmojiFactory;->access$200(Landroid/emoji/EmojiFactory;II)Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_b

    #@3f
    :cond_3f
    move v0, v1

    #@40
    .line 395
    goto :goto_b

    #@41
    .line 381
    nop

    #@42
    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_13
    .end packed-switch
.end method

.method public isInEmojiUnicodeTable(I)Z
    .registers 5
    .parameter "pua"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 357
    sget-object v1, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@3
    if-eqz v1, :cond_9

    #@5
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@7
    if-nez v1, :cond_a

    #@9
    .line 374
    :cond_9
    :goto_9
    return v0

    #@a
    .line 360
    :cond_a
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@c
    if-eqz v1, :cond_1b

    #@e
    .line 361
    iget-object v0, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@10
    iget-object v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@12
    invoke-static {v1}, Landroid/emoji/EmojiFactory;->access$100(Landroid/emoji/EmojiFactory;)I

    #@15
    move-result v1

    #@16
    invoke-static {v0, v1, p1}, Landroid/emoji/EmojiFactory;->access$200(Landroid/emoji/EmojiFactory;II)Z

    #@19
    move-result v0

    #@1a
    goto :goto_9

    #@1b
    .line 363
    :cond_1b
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mEmojiVersion:I

    #@1d
    packed-switch v1, :pswitch_data_3a

    #@20
    goto :goto_9

    #@21
    .line 366
    :pswitch_21
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMaxInt:I

    #@23
    if-gt p1, v1, :cond_9

    #@25
    iget v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->mOneFRangeMinInt:I

    #@27
    if-lt p1, v1, :cond_9

    #@29
    iget-object v1, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@2b
    iget-object v2, p0, Landroid/emoji/EmojiFactory$EmojiSupport;->this$0:Landroid/emoji/EmojiFactory;

    #@2d
    invoke-static {v2}, Landroid/emoji/EmojiFactory;->access$100(Landroid/emoji/EmojiFactory;)I

    #@30
    move-result v2

    #@31
    invoke-static {v1, v2, p1}, Landroid/emoji/EmojiFactory;->access$200(Landroid/emoji/EmojiFactory;II)Z

    #@34
    move-result v1

    #@35
    if-eqz v1, :cond_9

    #@37
    .line 368
    const/4 v0, 0x1

    #@38
    goto :goto_9

    #@39
    .line 363
    nop

    #@3a
    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_21
    .end packed-switch
.end method
