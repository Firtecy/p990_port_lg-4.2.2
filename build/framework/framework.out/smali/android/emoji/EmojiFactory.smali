.class public final Landroid/emoji/EmojiFactory;
.super Ljava/lang/Object;
.source "EmojiFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/emoji/EmojiFactory$EmojiSupport;,
        Landroid/emoji/EmojiFactory$UnicodeRangeContainer;,
        Landroid/emoji/EmojiFactory$CustomLinkedHashMap;
    }
.end annotation


# static fields
.field public static final EmojiHighMap:[B

.field public static final EmojiLowMap:[B

.field public static mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;


# instance fields
.field private mCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mNativeEmojiFactory:I

.field private sCacheSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/16 v1, 0x100

    #@2
    .line 70
    new-array v0, v1, [B

    #@4
    fill-array-data v0, :array_12

    #@7
    sput-object v0, Landroid/emoji/EmojiFactory;->EmojiLowMap:[B

    #@9
    .line 90
    new-array v0, v1, [B

    #@b
    fill-array-data v0, :array_96

    #@e
    sput-object v0, Landroid/emoji/EmojiFactory;->EmojiHighMap:[B

    #@10
    return-void

    #@11
    .line 70
    nop

    #@12
    :array_12
    .array-data 0x1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    #@96
    .line 90
    :array_96
    .array-data 0x1
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .registers 4
    .parameter "nativeEmojiFactory"
    .parameter "name"

    #@0
    .prologue
    .line 441
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    const/16 v0, 0x64

    #@5
    iput v0, p0, Landroid/emoji/EmojiFactory;->sCacheSize:I

    #@7
    .line 442
    iput p1, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@9
    .line 443
    iput-object p2, p0, Landroid/emoji/EmojiFactory;->mName:Ljava/lang/String;

    #@b
    .line 444
    new-instance v0, Landroid/emoji/EmojiFactory$CustomLinkedHashMap;

    #@d
    invoke-direct {v0, p0}, Landroid/emoji/EmojiFactory$CustomLinkedHashMap;-><init>(Landroid/emoji/EmojiFactory;)V

    #@10
    iput-object v0, p0, Landroid/emoji/EmojiFactory;->mCache:Ljava/util/Map;

    #@12
    .line 445
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 446
    invoke-virtual {p0}, Landroid/emoji/EmojiFactory;->createEmojiSupport()V

    #@19
    .line 447
    :cond_19
    return-void
.end method

.method static synthetic access$000(Landroid/emoji/EmojiFactory;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget v0, p0, Landroid/emoji/EmojiFactory;->sCacheSize:I

    #@2
    return v0
.end method

.method static synthetic access$100(Landroid/emoji/EmojiFactory;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    return v0
.end method

.method static synthetic access$200(Landroid/emoji/EmojiFactory;II)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/emoji/EmojiFactory;->nativeIsInEmojiUnicodeTable(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Landroid/emoji/EmojiFactory;I[I)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/emoji/EmojiFactory;->nativeIsInCountryCodeTable(I[I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private native nativeDestructor(I)V
.end method

.method private native nativeGetAndroidPuaFromVendorSpecificPua(II)I
.end method

.method private native nativeGetAndroidPuaFromVendorSpecificSjis(IC)I
.end method

.method private native nativeGetBitmapFromAndroidPua(II)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetBitmapFromAndroidPua(I[I)Landroid/graphics/Bitmap;
.end method

.method private native nativeGetMaximumAndroidPua(I)I
.end method

.method private native nativeGetMaximumVendorSpecificPua(I)I
.end method

.method private native nativeGetMinimumAndroidPua(I)I
.end method

.method private native nativeGetMinimumVendorSpecificPua(I)I
.end method

.method private native nativeGetVendorSpecificPuaFromAndroidPua(II)I
.end method

.method private native nativeGetVendorSpecificSjisFromAndroidPua(II)I
.end method

.method private native nativeIsInCountryCodeTable(I[I)Z
.end method

.method private native nativeIsInEmojiUnicodeTable(II)Z
.end method

.method public static native newAvailableInstance()Landroid/emoji/EmojiFactory;
.end method

.method public static native newInstance(Ljava/lang/String;)Landroid/emoji/EmojiFactory;
.end method


# virtual methods
.method public createEmojiSupport()V
    .registers 2

    #@0
    .prologue
    .line 148
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 149
    new-instance v0, Landroid/emoji/EmojiFactory$EmojiSupport;

    #@6
    invoke-direct {v0, p0}, Landroid/emoji/EmojiFactory$EmojiSupport;-><init>(Landroid/emoji/EmojiFactory;)V

    #@9
    sput-object v0, Landroid/emoji/EmojiFactory;->mEmojiSupport:Landroid/emoji/EmojiFactory$EmojiSupport;

    #@b
    .line 150
    :cond_b
    return-void
.end method

.method protected finalize()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 452
    :try_start_0
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0}, Landroid/emoji/EmojiFactory;->nativeDestructor(I)V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    #@5
    .line 454
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@8
    .line 456
    return-void

    #@9
    .line 454
    :catchall_9
    move-exception v0

    #@a
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    #@d
    throw v0
.end method

.method public getAndroidPuaFromVendorSpecificPua(I)I
    .registers 3
    .parameter "vsp"

    #@0
    .prologue
    .line 567
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/emoji/EmojiFactory;->nativeGetAndroidPuaFromVendorSpecificPua(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getAndroidPuaFromVendorSpecificPua(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "vspString"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 571
    if-nez p1, :cond_5

    #@3
    .line 572
    const/4 v8, 0x0

    #@4
    .line 591
    :goto_4
    return-object v8

    #@5
    .line 574
    :cond_5
    iget v8, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@7
    invoke-direct {p0, v8}, Landroid/emoji/EmojiFactory;->nativeGetMinimumVendorSpecificPua(I)I

    #@a
    move-result v5

    #@b
    .line 575
    .local v5, minVsp:I
    iget v8, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@d
    invoke-direct {p0, v8}, Landroid/emoji/EmojiFactory;->nativeGetMaximumVendorSpecificPua(I)I

    #@10
    move-result v4

    #@11
    .line 576
    .local v4, maxVsp:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    .line 577
    .local v3, len:I
    invoke-virtual {p1, v9, v3}, Ljava/lang/String;->codePointCount(II)I

    #@18
    move-result v8

    #@19
    new-array v1, v8, [I

    #@1b
    .line 579
    .local v1, codePoints:[I
    const/4 v7, 0x0

    #@1c
    .line 580
    .local v7, new_len:I
    const/4 v2, 0x0

    #@1d
    .local v2, i:I
    :goto_1d
    if-ge v2, v3, :cond_3a

    #@1f
    .line 581
    invoke-virtual {p1, v2}, Ljava/lang/String;->codePointAt(I)I

    #@22
    move-result v0

    #@23
    .line 582
    .local v0, codePoint:I
    if-gt v5, v0, :cond_37

    #@25
    if-gt v0, v4, :cond_37

    #@27
    .line 583
    invoke-virtual {p0, v0}, Landroid/emoji/EmojiFactory;->getAndroidPuaFromVendorSpecificPua(I)I

    #@2a
    move-result v6

    #@2b
    .line 584
    .local v6, newCodePoint:I
    if-lez v6, :cond_37

    #@2d
    .line 585
    aput v6, v1, v7

    #@2f
    .line 580
    .end local v6           #newCodePoint:I
    :goto_2f
    const/4 v8, 0x1

    #@30
    invoke-virtual {p1, v2, v8}, Ljava/lang/String;->offsetByCodePoints(II)I

    #@33
    move-result v2

    #@34
    add-int/lit8 v7, v7, 0x1

    #@36
    goto :goto_1d

    #@37
    .line 589
    :cond_37
    aput v0, v1, v7

    #@39
    goto :goto_2f

    #@3a
    .line 591
    .end local v0           #codePoint:I
    :cond_3a
    new-instance v8, Ljava/lang/String;

    #@3c
    invoke-direct {v8, v1, v9, v7}, Ljava/lang/String;-><init>([III)V

    #@3f
    goto :goto_4
.end method

.method public getAndroidPuaFromVendorSpecificSjis(C)I
    .registers 3
    .parameter "sjis"

    #@0
    .prologue
    .line 546
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/emoji/EmojiFactory;->nativeGetAndroidPuaFromVendorSpecificSjis(IC)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public declared-synchronized getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;
    .registers 8
    .parameter "pua"

    #@0
    .prologue
    .line 491
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v3, p0, Landroid/emoji/EmojiFactory;->mCache:Ljava/util/Map;

    #@3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v4

    #@7
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Ljava/lang/ref/WeakReference;

    #@d
    .line 492
    .local v0, cache:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;"
    if-nez v0, :cond_27

    #@f
    .line 493
    iget v3, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@11
    invoke-direct {p0, v3, p1}, Landroid/emoji/EmojiFactory;->nativeGetBitmapFromAndroidPua(II)Landroid/graphics/Bitmap;

    #@14
    move-result-object v1

    #@15
    .line 497
    .local v1, ret:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_25

    #@17
    .line 498
    iget-object v3, p0, Landroid/emoji/EmojiFactory;->mCache:Ljava/util/Map;

    #@19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v4

    #@1d
    new-instance v5, Ljava/lang/ref/WeakReference;

    #@1f
    invoke-direct {v5, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@22
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_44

    #@25
    .line 508
    .end local v1           #ret:Landroid/graphics/Bitmap;
    :cond_25
    :goto_25
    monitor-exit p0

    #@26
    return-object v1

    #@27
    .line 502
    :cond_27
    :try_start_27
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Landroid/graphics/Bitmap;

    #@2d
    .line 503
    .local v2, tmp:Landroid/graphics/Bitmap;
    if-nez v2, :cond_47

    #@2f
    .line 504
    iget v3, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@31
    invoke-direct {p0, v3, p1}, Landroid/emoji/EmojiFactory;->nativeGetBitmapFromAndroidPua(II)Landroid/graphics/Bitmap;

    #@34
    move-result-object v1

    #@35
    .line 505
    .restart local v1       #ret:Landroid/graphics/Bitmap;
    iget-object v3, p0, Landroid/emoji/EmojiFactory;->mCache:Ljava/util/Map;

    #@37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a
    move-result-object v4

    #@3b
    new-instance v5, Ljava/lang/ref/WeakReference;

    #@3d
    invoke-direct {v5, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@40
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_43
    .catchall {:try_start_27 .. :try_end_43} :catchall_44

    #@43
    goto :goto_25

    #@44
    .line 491
    .end local v0           #cache:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;"
    .end local v1           #ret:Landroid/graphics/Bitmap;
    .end local v2           #tmp:Landroid/graphics/Bitmap;
    :catchall_44
    move-exception v3

    #@45
    monitor-exit p0

    #@46
    throw v3

    #@47
    .restart local v0       #cache:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/Bitmap;>;"
    .restart local v2       #tmp:Landroid/graphics/Bitmap;
    :cond_47
    move-object v1, v2

    #@48
    .line 508
    goto :goto_25
.end method

.method public declared-synchronized getBitmapFromAndroidPua([I)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "puaArray"

    #@0
    .prologue
    .line 471
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    if-nez v1, :cond_8

    #@5
    .line 472
    const/4 v0, 0x0

    #@6
    .line 476
    :goto_6
    monitor-exit p0

    #@7
    return-object v0

    #@8
    .line 475
    :cond_8
    :try_start_8
    iget v1, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@a
    invoke-direct {p0, v1, p1}, Landroid/emoji/EmojiFactory;->nativeGetBitmapFromAndroidPua(I[I)Landroid/graphics/Bitmap;
    :try_end_d
    .catchall {:try_start_8 .. :try_end_d} :catchall_f

    #@d
    move-result-object v0

    #@e
    .line 476
    .local v0, ret:Landroid/graphics/Bitmap;
    goto :goto_6

    #@f
    .line 471
    .end local v0           #ret:Landroid/graphics/Bitmap;
    :catchall_f
    move-exception v1

    #@10
    monitor-exit p0

    #@11
    throw v1
.end method

.method public declared-synchronized getBitmapFromVendorSpecificPua(I)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "vsp"

    #@0
    .prologue
    .line 536
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/emoji/EmojiFactory;->getAndroidPuaFromVendorSpecificPua(I)I

    #@4
    move-result v0

    #@5
    invoke-virtual {p0, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    #@8
    move-result-object v0

    #@9
    monitor-exit p0

    #@a
    return-object v0

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public declared-synchronized getBitmapFromVendorSpecificSjis(C)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "sjis"

    #@0
    .prologue
    .line 523
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/emoji/EmojiFactory;->getAndroidPuaFromVendorSpecificSjis(C)I

    #@4
    move-result v0

    #@5
    invoke-virtual {p0, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    #@8
    move-result-object v0

    #@9
    monitor-exit p0

    #@a
    return-object v0

    #@b
    :catchall_b
    move-exception v0

    #@c
    monitor-exit p0

    #@d
    throw v0
.end method

.method public getEmojiVersion()V
    .registers 13

    #@0
    .prologue
    .line 111
    sget-boolean v9, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@2
    if-nez v9, :cond_5

    #@4
    .line 145
    :goto_4
    return-void

    #@5
    .line 114
    :cond_5
    new-instance v0, Landroid/content/res/AssetManager;

    #@7
    invoke-direct {v0}, Landroid/content/res/AssetManager;-><init>()V

    #@a
    .line 115
    .local v0, assetMngr:Landroid/content/res/AssetManager;
    const/4 v8, 0x0

    #@b
    .line 116
    .local v8, version:I
    const/4 v4, 0x0

    #@c
    .line 119
    .local v4, is:Ljava/io/InputStream;
    :try_start_c
    const-string v9, "emoji/emoji_table.csv"

    #@e
    const/4 v10, 0x3

    #@f
    invoke-virtual {v0, v9, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_12} :catch_47

    #@12
    move-result-object v4

    #@13
    .line 130
    :goto_13
    :try_start_13
    new-instance v6, Ljava/lang/StringBuffer;

    #@15
    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    #@18
    .line 131
    .local v6, out:Ljava/lang/StringBuffer;
    const/16 v9, 0x40

    #@1a
    new-array v1, v9, [B

    #@1c
    .line 132
    .local v1, b:[B
    :goto_1c
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    #@1f
    move-result v5

    #@20
    .local v5, n:I
    const/4 v9, -0x1

    #@21
    if-eq v5, v9, :cond_6a

    #@23
    .line 133
    new-instance v9, Ljava/lang/String;

    #@25
    const/4 v10, 0x0

    #@26
    invoke-direct {v9, v1, v10, v5}, Ljava/lang/String;-><init>([BII)V

    #@29
    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2c
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_1c

    #@2d
    .line 142
    .end local v1           #b:[B
    .end local v5           #n:I
    .end local v6           #out:Ljava/lang/StringBuffer;
    :catch_2d
    move-exception v2

    #@2e
    .line 143
    .local v2, e:Ljava/io/IOException;
    const-string v9, "Emoji"

    #@30
    new-instance v10, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v11, "error : while reading csv file to get version"

    #@37
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v10

    #@3f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v10

    #@43
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_4

    #@47
    .line 120
    .end local v2           #e:Ljava/io/IOException;
    :catch_47
    move-exception v2

    #@48
    .line 122
    .restart local v2       #e:Ljava/io/IOException;
    :try_start_48
    const-string v9, "emoji_base/emoji_table.csv"

    #@4a
    const/4 v10, 0x3

    #@4b
    invoke-virtual {v0, v9, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4e} :catch_50

    #@4e
    move-result-object v4

    #@4f
    goto :goto_13

    #@50
    .line 123
    :catch_50
    move-exception v3

    #@51
    .line 124
    .local v3, ex:Ljava/io/IOException;
    const-string v9, "Emoji"

    #@53
    new-instance v10, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v11, "error : emoji table asset is not found"

    #@5a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v10

    #@5e
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v10

    #@62
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v10

    #@66
    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_4

    #@6a
    .line 135
    .end local v2           #e:Ljava/io/IOException;
    .end local v3           #ex:Ljava/io/IOException;
    .restart local v1       #b:[B
    .restart local v5       #n:I
    .restart local v6       #out:Ljava/lang/StringBuffer;
    :cond_6a
    :try_start_6a
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    const-string v10, ",\r\n"

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@73
    move-result-object v7

    #@74
    .line 136
    .local v7, str_version:[Ljava/lang/String;
    const-string v9, "Emoji"

    #@76
    new-instance v10, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v11, "get string form csv file.. "

    #@7d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    const/4 v11, 0x0

    #@82
    aget-object v11, v7, v11

    #@84
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v10

    #@88
    const-string v11, " = "

    #@8a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v10

    #@8e
    const/4 v11, 0x1

    #@8f
    aget-object v11, v7, v11

    #@91
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v10

    #@95
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v10

    #@99
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 137
    const/4 v9, 0x0

    #@9d
    aget-object v9, v7, v9

    #@9f
    const-string/jumbo v10, "version"

    #@a2
    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@a5
    move-result v9

    #@a6
    if-nez v9, :cond_af

    #@a8
    .line 138
    const/4 v9, 0x1

    #@a9
    aget-object v9, v7, v9

    #@ab
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@ae
    move-result v8

    #@af
    .line 140
    :cond_af
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    #@b2
    .line 141
    invoke-virtual {v0}, Landroid/content/res/AssetManager;->close()V
    :try_end_b5
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_b5} :catch_2d

    #@b5
    goto/16 :goto_4
.end method

.method public getMaximumAndroidPua()I
    .registers 2

    #@0
    .prologue
    .line 659
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0}, Landroid/emoji/EmojiFactory;->nativeGetMaximumAndroidPua(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getMinimumAndroidPua()I
    .registers 2

    #@0
    .prologue
    .line 651
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0}, Landroid/emoji/EmojiFactory;->nativeGetMinimumAndroidPua(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getVendorSpecificPuaFromAndroidPua(I)I
    .registers 3
    .parameter "pua"

    #@0
    .prologue
    .line 601
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/emoji/EmojiFactory;->nativeGetVendorSpecificPuaFromAndroidPua(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getVendorSpecificPuaFromAndroidPua(Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "puaString"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 605
    if-nez p1, :cond_5

    #@3
    .line 606
    const/4 v8, 0x0

    #@4
    .line 625
    :goto_4
    return-object v8

    #@5
    .line 608
    :cond_5
    iget v8, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@7
    invoke-direct {p0, v8}, Landroid/emoji/EmojiFactory;->nativeGetMinimumAndroidPua(I)I

    #@a
    move-result v5

    #@b
    .line 609
    .local v5, minVsp:I
    iget v8, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@d
    invoke-direct {p0, v8}, Landroid/emoji/EmojiFactory;->nativeGetMaximumAndroidPua(I)I

    #@10
    move-result v4

    #@11
    .line 610
    .local v4, maxVsp:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@14
    move-result v3

    #@15
    .line 611
    .local v3, len:I
    invoke-virtual {p1, v9, v3}, Ljava/lang/String;->codePointCount(II)I

    #@18
    move-result v8

    #@19
    new-array v1, v8, [I

    #@1b
    .line 613
    .local v1, codePoints:[I
    const/4 v7, 0x0

    #@1c
    .line 614
    .local v7, new_len:I
    const/4 v2, 0x0

    #@1d
    .local v2, i:I
    :goto_1d
    if-ge v2, v3, :cond_3a

    #@1f
    .line 615
    invoke-virtual {p1, v2}, Ljava/lang/String;->codePointAt(I)I

    #@22
    move-result v0

    #@23
    .line 616
    .local v0, codePoint:I
    if-gt v5, v0, :cond_37

    #@25
    if-gt v0, v4, :cond_37

    #@27
    .line 617
    invoke-virtual {p0, v0}, Landroid/emoji/EmojiFactory;->getVendorSpecificPuaFromAndroidPua(I)I

    #@2a
    move-result v6

    #@2b
    .line 618
    .local v6, newCodePoint:I
    if-lez v6, :cond_37

    #@2d
    .line 619
    aput v6, v1, v7

    #@2f
    .line 614
    .end local v6           #newCodePoint:I
    :goto_2f
    const/4 v8, 0x1

    #@30
    invoke-virtual {p1, v2, v8}, Ljava/lang/String;->offsetByCodePoints(II)I

    #@33
    move-result v2

    #@34
    add-int/lit8 v7, v7, 0x1

    #@36
    goto :goto_1d

    #@37
    .line 623
    :cond_37
    aput v0, v1, v7

    #@39
    goto :goto_2f

    #@3a
    .line 625
    .end local v0           #codePoint:I
    :cond_3a
    new-instance v8, Ljava/lang/String;

    #@3c
    invoke-direct {v8, v1, v9, v7}, Ljava/lang/String;-><init>([III)V

    #@3f
    goto :goto_4
.end method

.method public getVendorSpecificSjisFromAndroidPua(I)I
    .registers 3
    .parameter "pua"

    #@0
    .prologue
    .line 556
    iget v0, p0, Landroid/emoji/EmojiFactory;->mNativeEmojiFactory:I

    #@2
    invoke-direct {p0, v0, p1}, Landroid/emoji/EmojiFactory;->nativeGetVendorSpecificSjisFromAndroidPua(II)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public name()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 459
    iget-object v0, p0, Landroid/emoji/EmojiFactory;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public quickRejectEmojiCode(C)Z
    .registers 6
    .parameter "code"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 153
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@4
    if-eqz v2, :cond_7

    #@6
    .line 157
    :goto_6
    return v1

    #@7
    .line 156
    :cond_7
    const/16 v2, 0x100

    #@9
    if-ge p1, v2, :cond_15

    #@b
    sget-object v2, Landroid/emoji/EmojiFactory;->EmojiLowMap:[B

    #@d
    aget-byte v2, v2, p1

    #@f
    if-nez v2, :cond_13

    #@11
    .local v0, flag:Z
    :cond_11
    :goto_11
    move v1, v0

    #@12
    .line 157
    goto :goto_6

    #@13
    .end local v0           #flag:Z
    :cond_13
    move v0, v1

    #@14
    .line 156
    goto :goto_11

    #@15
    :cond_15
    sget-object v2, Landroid/emoji/EmojiFactory;->EmojiHighMap:[B

    #@17
    shr-int/lit8 v3, p1, 0x8

    #@19
    aget-byte v2, v2, v3

    #@1b
    if-eqz v2, :cond_11

    #@1d
    move v0, v1

    #@1e
    goto :goto_11
.end method
