.class Landroid/text/MeasuredText;
.super Ljava/lang/Object;
.source "MeasuredText.java"


# static fields
.field private static final localLOGV:Z

.field private static sCached:[Landroid/text/MeasuredText;

.field private static final sLock:[Ljava/lang/Object;


# instance fields
.field mChars:[C

.field mDir:I

.field mEasy:Z

.field mLen:I

.field mLevels:[B

.field private mPos:I

.field mText:Ljava/lang/CharSequence;

.field mTextStart:I

.field mWidths:[F

.field private mWorkPaint:Landroid/text/TextPaint;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 48
    const/4 v0, 0x0

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    sput-object v0, Landroid/text/MeasuredText;->sLock:[Ljava/lang/Object;

    #@5
    .line 49
    const/4 v0, 0x3

    #@6
    new-array v0, v0, [Landroid/text/MeasuredText;

    #@8
    sput-object v0, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 45
    new-instance v0, Landroid/text/TextPaint;

    #@5
    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    #@8
    iput-object v0, p0, Landroid/text/MeasuredText;->mWorkPaint:Landroid/text/TextPaint;

    #@a
    .line 46
    return-void
.end method

.method static obtain()Landroid/text/MeasuredText;
    .registers 6

    #@0
    .prologue
    .line 53
    sget-object v4, Landroid/text/MeasuredText;->sLock:[Ljava/lang/Object;

    #@2
    monitor-enter v4

    #@3
    .line 54
    :try_start_3
    sget-object v3, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@5
    array-length v0, v3

    #@6
    .local v0, i:I
    :cond_6
    add-int/lit8 v0, v0, -0x1

    #@8
    if-ltz v0, :cond_1c

    #@a
    .line 55
    sget-object v3, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@c
    aget-object v3, v3, v0

    #@e
    if-eqz v3, :cond_6

    #@10
    .line 56
    sget-object v3, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@12
    aget-object v1, v3, v0

    #@14
    .line 57
    .local v1, mt:Landroid/text/MeasuredText;
    sget-object v3, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@16
    const/4 v5, 0x0

    #@17
    aput-object v5, v3, v0

    #@19
    .line 58
    monitor-exit v4

    #@1a
    move-object v2, v1

    #@1b
    .line 66
    .end local v1           #mt:Landroid/text/MeasuredText;
    .local v2, mt:Ljava/lang/Object;
    :goto_1b
    return-object v2

    #@1c
    .line 61
    .end local v2           #mt:Ljava/lang/Object;
    :cond_1c
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_24

    #@1d
    .line 62
    new-instance v1, Landroid/text/MeasuredText;

    #@1f
    invoke-direct {v1}, Landroid/text/MeasuredText;-><init>()V

    #@22
    .restart local v1       #mt:Landroid/text/MeasuredText;
    move-object v2, v1

    #@23
    .line 66
    .restart local v2       #mt:Ljava/lang/Object;
    goto :goto_1b

    #@24
    .line 61
    .end local v1           #mt:Landroid/text/MeasuredText;
    .end local v2           #mt:Ljava/lang/Object;
    :catchall_24
    move-exception v3

    #@25
    :try_start_25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v3
.end method

.method static recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;
    .registers 5
    .parameter "mt"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 70
    iput-object v3, p0, Landroid/text/MeasuredText;->mText:Ljava/lang/CharSequence;

    #@3
    .line 71
    iget v1, p0, Landroid/text/MeasuredText;->mLen:I

    #@5
    const/16 v2, 0x3e8

    #@7
    if-ge v1, v2, :cond_20

    #@9
    .line 72
    sget-object v2, Landroid/text/MeasuredText;->sLock:[Ljava/lang/Object;

    #@b
    monitor-enter v2

    #@c
    .line 73
    const/4 v0, 0x0

    #@d
    .local v0, i:I
    :goto_d
    :try_start_d
    sget-object v1, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@f
    array-length v1, v1

    #@10
    if-ge v0, v1, :cond_1f

    #@12
    .line 74
    sget-object v1, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@14
    aget-object v1, v1, v0

    #@16
    if-nez v1, :cond_21

    #@18
    .line 75
    sget-object v1, Landroid/text/MeasuredText;->sCached:[Landroid/text/MeasuredText;

    #@1a
    aput-object p0, v1, v0

    #@1c
    .line 76
    const/4 v1, 0x0

    #@1d
    iput-object v1, p0, Landroid/text/MeasuredText;->mText:Ljava/lang/CharSequence;

    #@1f
    .line 80
    :cond_1f
    monitor-exit v2

    #@20
    .line 82
    .end local v0           #i:I
    :cond_20
    return-object v3

    #@21
    .line 73
    .restart local v0       #i:I
    :cond_21
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_d

    #@24
    .line 80
    :catchall_24
    move-exception v1

    #@25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_d .. :try_end_26} :catchall_24

    #@26
    throw v1
.end method


# virtual methods
.method addStyleRun(Landroid/text/TextPaint;ILandroid/graphics/Paint$FontMetricsInt;)F
    .registers 26
    .parameter "paint"
    .parameter "len"
    .parameter "fm"

    #@0
    .prologue
    .line 162
    if-eqz p3, :cond_9

    #@2
    .line 163
    move-object/from16 v0, p1

    #@4
    move-object/from16 v1, p3

    #@6
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@9
    .line 166
    :cond_9
    move-object/from16 v0, p0

    #@b
    iget v4, v0, Landroid/text/MeasuredText;->mPos:I

    #@d
    .line 167
    .local v4, p:I
    add-int v2, v4, p2

    #@f
    move-object/from16 v0, p0

    #@11
    iput v2, v0, Landroid/text/MeasuredText;->mPos:I

    #@13
    .line 169
    move-object/from16 v0, p0

    #@15
    iget-boolean v2, v0, Landroid/text/MeasuredText;->mEasy:Z

    #@17
    if-eqz v2, :cond_38

    #@19
    .line 170
    move-object/from16 v0, p0

    #@1b
    iget v2, v0, Landroid/text/MeasuredText;->mDir:I

    #@1d
    const/4 v3, 0x1

    #@1e
    if-ne v2, v3, :cond_36

    #@20
    const/4 v8, 0x0

    #@21
    .line 172
    .local v8, flags:I
    :goto_21
    move-object/from16 v0, p0

    #@23
    iget-object v3, v0, Landroid/text/MeasuredText;->mChars:[C

    #@25
    move-object/from16 v0, p0

    #@27
    iget-object v9, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@29
    move-object/from16 v2, p1

    #@2b
    move/from16 v5, p2

    #@2d
    move v6, v4

    #@2e
    move/from16 v7, p2

    #@30
    move v10, v4

    #@31
    invoke-virtual/range {v2 .. v10}, Landroid/text/TextPaint;->getTextRunAdvances([CIIIII[FI)F

    #@34
    move-result v21

    #@35
    .line 189
    :cond_35
    return v21

    #@36
    .line 170
    .end local v8           #flags:I
    :cond_36
    const/4 v8, 0x1

    #@37
    goto :goto_21

    #@38
    .line 175
    :cond_38
    const/16 v21, 0x0

    #@3a
    .line 176
    .local v21, totalAdvance:F
    move-object/from16 v0, p0

    #@3c
    iget-object v2, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@3e
    aget-byte v20, v2, v4

    #@40
    .line 177
    .local v20, level:I
    move v11, v4

    #@41
    .local v11, q:I
    add-int/lit8 v19, v4, 0x1

    #@43
    .local v19, i:I
    add-int v18, v4, p2

    #@45
    .line 178
    .local v18, e:I
    :goto_45
    move/from16 v0, v19

    #@47
    move/from16 v1, v18

    #@49
    if-eq v0, v1, :cond_55

    #@4b
    move-object/from16 v0, p0

    #@4d
    iget-object v2, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@4f
    aget-byte v2, v2, v19

    #@51
    move/from16 v0, v20

    #@53
    if-eq v2, v0, :cond_82

    #@55
    .line 179
    :cond_55
    and-int/lit8 v2, v20, 0x1

    #@57
    if-nez v2, :cond_85

    #@59
    const/4 v8, 0x0

    #@5a
    .line 180
    .restart local v8       #flags:I
    :goto_5a
    move-object/from16 v0, p0

    #@5c
    iget-object v10, v0, Landroid/text/MeasuredText;->mChars:[C

    #@5e
    sub-int v12, v19, v11

    #@60
    sub-int v14, v19, v11

    #@62
    move-object/from16 v0, p0

    #@64
    iget-object v0, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@66
    move-object/from16 v16, v0

    #@68
    move-object/from16 v9, p1

    #@6a
    move v13, v11

    #@6b
    move v15, v8

    #@6c
    move/from16 v17, v11

    #@6e
    invoke-virtual/range {v9 .. v17}, Landroid/text/TextPaint;->getTextRunAdvances([CIIIII[FI)F

    #@71
    move-result v2

    #@72
    add-float v21, v21, v2

    #@74
    .line 182
    move/from16 v0, v19

    #@76
    move/from16 v1, v18

    #@78
    if-eq v0, v1, :cond_35

    #@7a
    .line 185
    move/from16 v11, v19

    #@7c
    .line 186
    move-object/from16 v0, p0

    #@7e
    iget-object v2, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@80
    aget-byte v20, v2, v19

    #@82
    .line 177
    .end local v8           #flags:I
    :cond_82
    add-int/lit8 v19, v19, 0x1

    #@84
    goto :goto_45

    #@85
    .line 179
    :cond_85
    const/4 v8, 0x1

    #@86
    goto :goto_5a
.end method

.method addStyleRun(Landroid/text/TextPaint;[Landroid/text/style/MetricAffectingSpan;ILandroid/graphics/Paint$FontMetricsInt;)F
    .registers 16
    .parameter "paint"
    .parameter "spans"
    .parameter "len"
    .parameter "fm"

    #@0
    .prologue
    .line 195
    iget-object v1, p0, Landroid/text/MeasuredText;->mWorkPaint:Landroid/text/TextPaint;

    #@2
    .line 196
    .local v1, workPaint:Landroid/text/TextPaint;
    invoke-virtual {v1, p1}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@5
    .line 198
    const/4 v2, 0x0

    #@6
    iput v2, v1, Landroid/text/TextPaint;->baselineShift:I

    #@8
    .line 200
    const/4 v0, 0x0

    #@9
    .line 201
    .local v0, replacement:Landroid/text/style/ReplacementSpan;
    const/4 v7, 0x0

    #@a
    .local v7, i:I
    :goto_a
    array-length v2, p2

    #@b
    if-ge v7, v2, :cond_1d

    #@d
    .line 202
    aget-object v8, p2, v7

    #@f
    .line 203
    .local v8, span:Landroid/text/style/MetricAffectingSpan;
    instance-of v2, v8, Landroid/text/style/ReplacementSpan;

    #@11
    if-eqz v2, :cond_19

    #@13
    move-object v0, v8

    #@14
    .line 204
    check-cast v0, Landroid/text/style/ReplacementSpan;

    #@16
    .line 201
    :goto_16
    add-int/lit8 v7, v7, 0x1

    #@18
    goto :goto_a

    #@19
    .line 206
    :cond_19
    invoke-virtual {v8, v1}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@1c
    goto :goto_16

    #@1d
    .line 211
    .end local v8           #span:Landroid/text/style/MetricAffectingSpan;
    :cond_1d
    if-nez v0, :cond_38

    #@1f
    .line 212
    invoke-virtual {p0, v1, p3, p4}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@22
    move-result v10

    #@23
    .line 224
    .local v10, wid:F
    :goto_23
    if-eqz p4, :cond_37

    #@25
    .line 225
    iget v2, v1, Landroid/text/TextPaint;->baselineShift:I

    #@27
    if-gez v2, :cond_67

    #@29
    .line 226
    iget v2, p4, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@2b
    iget v3, v1, Landroid/text/TextPaint;->baselineShift:I

    #@2d
    add-int/2addr v2, v3

    #@2e
    iput v2, p4, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@30
    .line 227
    iget v2, p4, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@32
    iget v3, v1, Landroid/text/TextPaint;->baselineShift:I

    #@34
    add-int/2addr v2, v3

    #@35
    iput v2, p4, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@37
    .line 234
    :cond_37
    :goto_37
    return v10

    #@38
    .line 215
    .end local v10           #wid:F
    :cond_38
    iget-object v2, p0, Landroid/text/MeasuredText;->mText:Ljava/lang/CharSequence;

    #@3a
    iget v3, p0, Landroid/text/MeasuredText;->mTextStart:I

    #@3c
    iget v4, p0, Landroid/text/MeasuredText;->mPos:I

    #@3e
    add-int/2addr v3, v4

    #@3f
    iget v4, p0, Landroid/text/MeasuredText;->mTextStart:I

    #@41
    iget v5, p0, Landroid/text/MeasuredText;->mPos:I

    #@43
    add-int/2addr v4, v5

    #@44
    add-int/2addr v4, p3

    #@45
    move-object v5, p4

    #@46
    invoke-virtual/range {v0 .. v5}, Landroid/text/style/ReplacementSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    #@49
    move-result v2

    #@4a
    int-to-float v10, v2

    #@4b
    .line 217
    .restart local v10       #wid:F
    iget-object v9, p0, Landroid/text/MeasuredText;->mWidths:[F

    #@4d
    .line 218
    .local v9, w:[F
    iget v2, p0, Landroid/text/MeasuredText;->mPos:I

    #@4f
    aput v10, v9, v2

    #@51
    .line 219
    iget v2, p0, Landroid/text/MeasuredText;->mPos:I

    #@53
    add-int/lit8 v7, v2, 0x1

    #@55
    iget v2, p0, Landroid/text/MeasuredText;->mPos:I

    #@57
    add-int v6, v2, p3

    #@59
    .local v6, e:I
    :goto_59
    if-ge v7, v6, :cond_61

    #@5b
    .line 220
    const/4 v2, 0x0

    #@5c
    aput v2, v9, v7

    #@5e
    .line 219
    add-int/lit8 v7, v7, 0x1

    #@60
    goto :goto_59

    #@61
    .line 221
    :cond_61
    iget v2, p0, Landroid/text/MeasuredText;->mPos:I

    #@63
    add-int/2addr v2, p3

    #@64
    iput v2, p0, Landroid/text/MeasuredText;->mPos:I

    #@66
    goto :goto_23

    #@67
    .line 229
    .end local v6           #e:I
    .end local v9           #w:[F
    :cond_67
    iget v2, p4, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@69
    iget v3, v1, Landroid/text/TextPaint;->baselineShift:I

    #@6b
    add-int/2addr v2, v3

    #@6c
    iput v2, p4, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@6e
    .line 230
    iget v2, p4, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@70
    iget v3, v1, Landroid/text/TextPaint;->baselineShift:I

    #@72
    add-int/2addr v2, v3

    #@73
    iput v2, p4, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@75
    goto :goto_37
.end method

.method breakText(IZF)I
    .registers 9
    .parameter "limit"
    .parameter "forwards"
    .parameter "width"

    #@0
    .prologue
    const/16 v4, 0x20

    #@2
    const/4 v3, 0x0

    #@3
    .line 238
    iget-object v1, p0, Landroid/text/MeasuredText;->mWidths:[F

    #@5
    .line 239
    .local v1, w:[F
    if-eqz p2, :cond_23

    #@7
    .line 240
    const/4 v0, 0x0

    #@8
    .line 241
    .local v0, i:I
    :goto_8
    if-ge v0, p1, :cond_11

    #@a
    .line 242
    aget v2, v1, v0

    #@c
    sub-float/2addr p3, v2

    #@d
    .line 243
    cmpg-float v2, p3, v3

    #@f
    if-gez v2, :cond_1e

    #@11
    .line 246
    :cond_11
    :goto_11
    if-lez v0, :cond_21

    #@13
    iget-object v2, p0, Landroid/text/MeasuredText;->mChars:[C

    #@15
    add-int/lit8 v3, v0, -0x1

    #@17
    aget-char v2, v2, v3

    #@19
    if-ne v2, v4, :cond_21

    #@1b
    add-int/lit8 v0, v0, -0x1

    #@1d
    goto :goto_11

    #@1e
    .line 244
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    #@20
    goto :goto_8

    #@21
    :cond_21
    move v2, v0

    #@22
    .line 256
    :goto_22
    return v2

    #@23
    .line 249
    .end local v0           #i:I
    :cond_23
    add-int/lit8 v0, p1, -0x1

    #@25
    .line 250
    .restart local v0       #i:I
    :goto_25
    if-ltz v0, :cond_2e

    #@27
    .line 251
    aget v2, v1, v0

    #@29
    sub-float/2addr p3, v2

    #@2a
    .line 252
    cmpg-float v2, p3, v3

    #@2c
    if-gez v2, :cond_3d

    #@2e
    .line 255
    :cond_2e
    :goto_2e
    add-int/lit8 v2, p1, -0x1

    #@30
    if-ge v0, v2, :cond_40

    #@32
    iget-object v2, p0, Landroid/text/MeasuredText;->mChars:[C

    #@34
    add-int/lit8 v3, v0, 0x1

    #@36
    aget-char v2, v2, v3

    #@38
    if-ne v2, v4, :cond_40

    #@3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_2e

    #@3d
    .line 253
    :cond_3d
    add-int/lit8 v0, v0, -0x1

    #@3f
    goto :goto_25

    #@40
    .line 256
    :cond_40
    sub-int v2, p1, v0

    #@42
    add-int/lit8 v2, v2, -0x1

    #@44
    goto :goto_22
.end method

.method measure(II)F
    .registers 7
    .parameter "start"
    .parameter "limit"

    #@0
    .prologue
    .line 261
    const/4 v2, 0x0

    #@1
    .line 262
    .local v2, width:F
    iget-object v1, p0, Landroid/text/MeasuredText;->mWidths:[F

    #@3
    .line 263
    .local v1, w:[F
    move v0, p1

    #@4
    .local v0, i:I
    :goto_4
    if-ge v0, p2, :cond_c

    #@6
    .line 264
    aget v3, v1, v0

    #@8
    add-float/2addr v2, v3

    #@9
    .line 263
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_4

    #@c
    .line 266
    :cond_c
    return v2
.end method

.method setPara(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)V
    .registers 21
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "textDir"

    #@0
    .prologue
    .line 93
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p0

    #@4
    iput-object v0, v1, Landroid/text/MeasuredText;->mText:Ljava/lang/CharSequence;

    #@6
    .line 94
    move/from16 v0, p2

    #@8
    move-object/from16 v1, p0

    #@a
    iput v0, v1, Landroid/text/MeasuredText;->mTextStart:I

    #@c
    .line 96
    sub-int v9, p3, p2

    #@e
    .line 97
    .local v9, len:I
    move-object/from16 v0, p0

    #@10
    iput v9, v0, Landroid/text/MeasuredText;->mLen:I

    #@12
    .line 98
    const/4 v13, 0x0

    #@13
    move-object/from16 v0, p0

    #@15
    iput v13, v0, Landroid/text/MeasuredText;->mPos:I

    #@17
    .line 100
    move-object/from16 v0, p0

    #@19
    iget-object v13, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@1b
    if-eqz v13, :cond_24

    #@1d
    move-object/from16 v0, p0

    #@1f
    iget-object v13, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@21
    array-length v13, v13

    #@22
    if-ge v13, v9, :cond_2e

    #@24
    .line 101
    :cond_24
    invoke-static {v9}, Lcom/android/internal/util/ArrayUtils;->idealFloatArraySize(I)I

    #@27
    move-result v13

    #@28
    new-array v13, v13, [F

    #@2a
    move-object/from16 v0, p0

    #@2c
    iput-object v13, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@2e
    .line 103
    :cond_2e
    move-object/from16 v0, p0

    #@30
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@32
    if-eqz v13, :cond_3b

    #@34
    move-object/from16 v0, p0

    #@36
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@38
    array-length v13, v13

    #@39
    if-ge v13, v9, :cond_45

    #@3b
    .line 104
    :cond_3b
    invoke-static {v9}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@3e
    move-result v13

    #@3f
    new-array v13, v13, [C

    #@41
    move-object/from16 v0, p0

    #@43
    iput-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@45
    .line 106
    :cond_45
    move-object/from16 v0, p0

    #@47
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@49
    const/4 v14, 0x0

    #@4a
    move-object/from16 v0, p1

    #@4c
    move/from16 v1, p2

    #@4e
    move/from16 v2, p3

    #@50
    invoke-static {v0, v1, v2, v13, v14}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@53
    .line 108
    move-object/from16 v0, p1

    #@55
    instance-of v13, v0, Landroid/text/Spanned;

    #@57
    if-eqz v13, :cond_99

    #@59
    move-object/from16 v10, p1

    #@5b
    .line 109
    check-cast v10, Landroid/text/Spanned;

    #@5d
    .line 110
    .local v10, spanned:Landroid/text/Spanned;
    const-class v13, Landroid/text/style/ReplacementSpan;

    #@5f
    move/from16 v0, p2

    #@61
    move/from16 v1, p3

    #@63
    invoke-interface {v10, v0, v1, v13}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@66
    move-result-object v11

    #@67
    check-cast v11, [Landroid/text/style/ReplacementSpan;

    #@69
    .line 116
    .local v11, spans:[Landroid/text/style/ReplacementSpan;
    const/4 v6, 0x0

    #@6a
    .local v6, i:I
    :goto_6a
    :try_start_6a
    array-length v13, v11

    #@6b
    if-ge v6, v13, :cond_99

    #@6d
    .line 117
    aget-object v13, v11, v6

    #@6f
    invoke-interface {v10, v13}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@72
    move-result v13

    #@73
    sub-int v12, v13, p2

    #@75
    .line 118
    .local v12, startInPara:I
    aget-object v13, v11, v6

    #@77
    invoke-interface {v10, v13}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@7a
    move-result v13

    #@7b
    sub-int v4, v13, p2

    #@7d
    .line 120
    .local v4, endInPara:I
    if-gez v12, :cond_80

    #@7f
    const/4 v12, 0x0

    #@80
    .line 121
    :cond_80
    if-le v4, v9, :cond_83

    #@82
    move v4, v9

    #@83
    .line 122
    :cond_83
    move v8, v12

    #@84
    .local v8, j:I
    :goto_84
    if-ge v8, v4, :cond_92

    #@86
    .line 123
    move-object/from16 v0, p0

    #@88
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@8a
    const v14, 0xfffc

    #@8d
    aput-char v14, v13, v8
    :try_end_8f
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6a .. :try_end_8f} :catch_95

    #@8f
    .line 122
    add-int/lit8 v8, v8, 0x1

    #@91
    goto :goto_84

    #@92
    .line 116
    :cond_92
    add-int/lit8 v6, v6, 0x1

    #@94
    goto :goto_6a

    #@95
    .line 126
    .end local v4           #endInPara:I
    .end local v8           #j:I
    .end local v12           #startInPara:I
    :catch_95
    move-exception v5

    #@96
    .line 127
    .local v5, ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v5}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    #@99
    .line 133
    .end local v5           #ex:Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v6           #i:I
    .end local v10           #spanned:Landroid/text/Spanned;
    .end local v11           #spans:[Landroid/text/style/ReplacementSpan;
    :cond_99
    sget-object v13, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    #@9b
    move-object/from16 v0, p4

    #@9d
    if-eq v0, v13, :cond_ab

    #@9f
    sget-object v13, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@a1
    move-object/from16 v0, p4

    #@a3
    if-eq v0, v13, :cond_ab

    #@a5
    sget-object v13, Landroid/text/TextDirectionHeuristics;->ANYRTL_LTR:Landroid/text/TextDirectionHeuristic;

    #@a7
    move-object/from16 v0, p4

    #@a9
    if-ne v0, v13, :cond_c1

    #@ab
    :cond_ab
    move-object/from16 v0, p0

    #@ad
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@af
    const/4 v14, 0x0

    #@b0
    invoke-static {v13, v14, v9}, Landroid/text/TextUtils;->doesNotNeedBidi([CII)Z

    #@b3
    move-result v13

    #@b4
    if-eqz v13, :cond_c1

    #@b6
    .line 137
    const/4 v13, 0x1

    #@b7
    move-object/from16 v0, p0

    #@b9
    iput v13, v0, Landroid/text/MeasuredText;->mDir:I

    #@bb
    .line 138
    const/4 v13, 0x1

    #@bc
    move-object/from16 v0, p0

    #@be
    iput-boolean v13, v0, Landroid/text/MeasuredText;->mEasy:Z

    #@c0
    .line 159
    :goto_c0
    return-void

    #@c1
    .line 140
    :cond_c1
    move-object/from16 v0, p0

    #@c3
    iget-object v13, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@c5
    if-eqz v13, :cond_ce

    #@c7
    move-object/from16 v0, p0

    #@c9
    iget-object v13, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@cb
    array-length v13, v13

    #@cc
    if-ge v13, v9, :cond_d8

    #@ce
    .line 141
    :cond_ce
    invoke-static {v9}, Lcom/android/internal/util/ArrayUtils;->idealByteArraySize(I)I

    #@d1
    move-result v13

    #@d2
    new-array v13, v13, [B

    #@d4
    move-object/from16 v0, p0

    #@d6
    iput-object v13, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@d8
    .line 144
    :cond_d8
    sget-object v13, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    #@da
    move-object/from16 v0, p4

    #@dc
    if-ne v0, v13, :cond_f6

    #@de
    .line 145
    const/4 v3, 0x1

    #@df
    .line 156
    .local v3, bidiRequest:I
    :goto_df
    move-object/from16 v0, p0

    #@e1
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@e3
    move-object/from16 v0, p0

    #@e5
    iget-object v14, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@e7
    const/4 v15, 0x0

    #@e8
    invoke-static {v3, v13, v14, v9, v15}, Landroid/text/AndroidBidi;->bidi(I[C[BIZ)I

    #@eb
    move-result v13

    #@ec
    move-object/from16 v0, p0

    #@ee
    iput v13, v0, Landroid/text/MeasuredText;->mDir:I

    #@f0
    .line 157
    const/4 v13, 0x0

    #@f1
    move-object/from16 v0, p0

    #@f3
    iput-boolean v13, v0, Landroid/text/MeasuredText;->mEasy:Z

    #@f5
    goto :goto_c0

    #@f6
    .line 146
    .end local v3           #bidiRequest:I
    :cond_f6
    sget-object v13, Landroid/text/TextDirectionHeuristics;->RTL:Landroid/text/TextDirectionHeuristic;

    #@f8
    move-object/from16 v0, p4

    #@fa
    if-ne v0, v13, :cond_fe

    #@fc
    .line 147
    const/4 v3, -0x1

    #@fd
    .restart local v3       #bidiRequest:I
    goto :goto_df

    #@fe
    .line 148
    .end local v3           #bidiRequest:I
    :cond_fe
    sget-object v13, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@100
    move-object/from16 v0, p4

    #@102
    if-ne v0, v13, :cond_106

    #@104
    .line 149
    const/4 v3, 0x2

    #@105
    .restart local v3       #bidiRequest:I
    goto :goto_df

    #@106
    .line 150
    .end local v3           #bidiRequest:I
    :cond_106
    sget-object v13, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_RTL:Landroid/text/TextDirectionHeuristic;

    #@108
    move-object/from16 v0, p4

    #@10a
    if-ne v0, v13, :cond_10e

    #@10c
    .line 151
    const/4 v3, -0x2

    #@10d
    .restart local v3       #bidiRequest:I
    goto :goto_df

    #@10e
    .line 153
    .end local v3           #bidiRequest:I
    :cond_10e
    move-object/from16 v0, p0

    #@110
    iget-object v13, v0, Landroid/text/MeasuredText;->mChars:[C

    #@112
    const/4 v14, 0x0

    #@113
    move-object/from16 v0, p4

    #@115
    invoke-interface {v0, v13, v14, v9}, Landroid/text/TextDirectionHeuristic;->isRtl([CII)Z

    #@118
    move-result v7

    #@119
    .line 154
    .local v7, isRtl:Z
    if-eqz v7, :cond_11d

    #@11b
    const/4 v3, -0x1

    #@11c
    .restart local v3       #bidiRequest:I
    :goto_11c
    goto :goto_df

    #@11d
    .end local v3           #bidiRequest:I
    :cond_11d
    const/4 v3, 0x1

    #@11e
    goto :goto_11c
.end method

.method setPos(I)V
    .registers 3
    .parameter "pos"

    #@0
    .prologue
    .line 86
    iget v0, p0, Landroid/text/MeasuredText;->mTextStart:I

    #@2
    sub-int v0, p1, v0

    #@4
    iput v0, p0, Landroid/text/MeasuredText;->mPos:I

    #@6
    .line 87
    return-void
.end method
