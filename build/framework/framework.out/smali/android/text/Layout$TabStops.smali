.class Landroid/text/Layout$TabStops;
.super Ljava/lang/Object;
.source "Layout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/Layout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TabStops"
.end annotation


# instance fields
.field private mIncrement:I

.field private mNumStops:I

.field private mStops:[I


# direct methods
.method constructor <init>(I[Ljava/lang/Object;)V
    .registers 3
    .parameter "increment"
    .parameter "spans"

    #@0
    .prologue
    .line 1862
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1863
    invoke-virtual {p0, p1, p2}, Landroid/text/Layout$TabStops;->reset(I[Ljava/lang/Object;)V

    #@6
    .line 1864
    return-void
.end method

.method public static nextDefaultStop(FI)F
    .registers 4
    .parameter "h"
    .parameter "inc"

    #@0
    .prologue
    .line 1911
    int-to-float v0, p1

    #@1
    add-float/2addr v0, p0

    #@2
    int-to-float v1, p1

    #@3
    div-float/2addr v0, v1

    #@4
    float-to-int v0, v0

    #@5
    mul-int/2addr v0, p1

    #@6
    int-to-float v0, v0

    #@7
    return v0
.end method


# virtual methods
.method nextTab(F)F
    .registers 7
    .parameter "h"

    #@0
    .prologue
    .line 1897
    iget v1, p0, Landroid/text/Layout$TabStops;->mNumStops:I

    #@2
    .line 1898
    .local v1, ns:I
    if-lez v1, :cond_15

    #@4
    .line 1899
    iget-object v3, p0, Landroid/text/Layout$TabStops;->mStops:[I

    #@6
    .line 1900
    .local v3, stops:[I
    const/4 v0, 0x0

    #@7
    .local v0, i:I
    :goto_7
    if-ge v0, v1, :cond_15

    #@9
    .line 1901
    aget v2, v3, v0

    #@b
    .line 1902
    .local v2, stop:I
    int-to-float v4, v2

    #@c
    cmpl-float v4, v4, p1

    #@e
    if-lez v4, :cond_12

    #@10
    .line 1903
    int-to-float v4, v2

    #@11
    .line 1907
    .end local v0           #i:I
    .end local v2           #stop:I
    .end local v3           #stops:[I
    :goto_11
    return v4

    #@12
    .line 1900
    .restart local v0       #i:I
    .restart local v2       #stop:I
    .restart local v3       #stops:[I
    :cond_12
    add-int/lit8 v0, v0, 0x1

    #@14
    goto :goto_7

    #@15
    .line 1907
    .end local v0           #i:I
    .end local v2           #stop:I
    .end local v3           #stops:[I
    :cond_15
    iget v4, p0, Landroid/text/Layout$TabStops;->mIncrement:I

    #@17
    invoke-static {p1, v4}, Landroid/text/Layout$TabStops;->nextDefaultStop(FI)F

    #@1a
    move-result v4

    #@1b
    goto :goto_11
.end method

.method reset(I[Ljava/lang/Object;)V
    .registers 13
    .parameter "increment"
    .parameter "spans"

    #@0
    .prologue
    .line 1867
    iput p1, p0, Landroid/text/Layout$TabStops;->mIncrement:I

    #@2
    .line 1869
    const/4 v4, 0x0

    #@3
    .line 1870
    .local v4, ns:I
    if-eqz p2, :cond_48

    #@5
    .line 1871
    iget-object v8, p0, Landroid/text/Layout$TabStops;->mStops:[I

    #@7
    .line 1872
    .local v8, stops:[I
    move-object v0, p2

    #@8
    .local v0, arr$:[Ljava/lang/Object;
    array-length v3, v0

    #@9
    .local v3, len$:I
    const/4 v2, 0x0

    #@a
    .local v2, i$:I
    move v5, v4

    #@b
    .end local v4           #ns:I
    .local v5, ns:I
    :goto_b
    if-ge v2, v3, :cond_3a

    #@d
    aget-object v7, v0, v2

    #@f
    .line 1873
    .local v7, o:Ljava/lang/Object;
    instance-of v9, v7, Landroid/text/style/TabStopSpan;

    #@11
    if-eqz v9, :cond_4b

    #@13
    .line 1874
    if-nez v8, :cond_27

    #@15
    .line 1875
    const/16 v9, 0xa

    #@17
    new-array v8, v9, [I

    #@19
    .line 1883
    :cond_19
    :goto_19
    add-int/lit8 v4, v5, 0x1

    #@1b
    .end local v5           #ns:I
    .restart local v4       #ns:I
    check-cast v7, Landroid/text/style/TabStopSpan;

    #@1d
    .end local v7           #o:Ljava/lang/Object;
    invoke-interface {v7}, Landroid/text/style/TabStopSpan;->getTabStop()I

    #@20
    move-result v9

    #@21
    aput v9, v8, v5

    #@23
    .line 1872
    :goto_23
    add-int/lit8 v2, v2, 0x1

    #@25
    move v5, v4

    #@26
    .end local v4           #ns:I
    .restart local v5       #ns:I
    goto :goto_b

    #@27
    .line 1876
    .restart local v7       #o:Ljava/lang/Object;
    :cond_27
    array-length v9, v8

    #@28
    if-ne v5, v9, :cond_19

    #@2a
    .line 1877
    mul-int/lit8 v9, v5, 0x2

    #@2c
    new-array v6, v9, [I

    #@2e
    .line 1878
    .local v6, nstops:[I
    const/4 v1, 0x0

    #@2f
    .local v1, i:I
    :goto_2f
    if-ge v1, v5, :cond_38

    #@31
    .line 1879
    aget v9, v8, v1

    #@33
    aput v9, v6, v1

    #@35
    .line 1878
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_2f

    #@38
    .line 1881
    :cond_38
    move-object v8, v6

    #@39
    goto :goto_19

    #@3a
    .line 1886
    .end local v1           #i:I
    .end local v6           #nstops:[I
    .end local v7           #o:Ljava/lang/Object;
    :cond_3a
    const/4 v9, 0x1

    #@3b
    if-le v5, v9, :cond_41

    #@3d
    .line 1887
    const/4 v9, 0x0

    #@3e
    invoke-static {v8, v9, v5}, Ljava/util/Arrays;->sort([III)V

    #@41
    .line 1889
    :cond_41
    iget-object v9, p0, Landroid/text/Layout$TabStops;->mStops:[I

    #@43
    if-eq v8, v9, :cond_47

    #@45
    .line 1890
    iput-object v8, p0, Landroid/text/Layout$TabStops;->mStops:[I

    #@47
    :cond_47
    move v4, v5

    #@48
    .line 1893
    .end local v0           #arr$:[Ljava/lang/Object;
    .end local v2           #i$:I
    .end local v3           #len$:I
    .end local v5           #ns:I
    .end local v8           #stops:[I
    .restart local v4       #ns:I
    :cond_48
    iput v4, p0, Landroid/text/Layout$TabStops;->mNumStops:I

    #@4a
    .line 1894
    return-void

    #@4b
    .end local v4           #ns:I
    .restart local v0       #arr$:[Ljava/lang/Object;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    .restart local v5       #ns:I
    .restart local v7       #o:Ljava/lang/Object;
    .restart local v8       #stops:[I
    :cond_4b
    move v4, v5

    #@4c
    .end local v5           #ns:I
    .restart local v4       #ns:I
    goto :goto_23
.end method
