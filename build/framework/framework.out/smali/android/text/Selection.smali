.class public Landroid/text/Selection;
.super Ljava/lang/Object;
.source "Selection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/Selection$1;,
        Landroid/text/Selection$END;,
        Landroid/text/Selection$START;,
        Landroid/text/Selection$PositionIterator;
    }
.end annotation


# static fields
.field public static final SELECTION_END:Ljava/lang/Object;

.field public static final SELECTION_START:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 465
    new-instance v0, Landroid/text/Selection$START;

    #@3
    invoke-direct {v0, v1}, Landroid/text/Selection$START;-><init>(Landroid/text/Selection$1;)V

    #@6
    sput-object v0, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@8
    .line 466
    new-instance v0, Landroid/text/Selection$END;

    #@a
    invoke-direct {v0, v1}, Landroid/text/Selection$END;-><init>(Landroid/text/Selection$1;)V

    #@d
    sput-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@f
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static chooseHorizontal(Landroid/text/Layout;III)I
    .registers 11
    .parameter "layout"
    .parameter "direction"
    .parameter "off1"
    .parameter "off2"

    #@0
    .prologue
    .line 417
    invoke-virtual {p0, p2}, Landroid/text/Layout;->getLineForOffset(I)I

    #@3
    move-result v3

    #@4
    .line 418
    .local v3, line1:I
    invoke-virtual {p0, p3}, Landroid/text/Layout;->getLineForOffset(I)I

    #@7
    move-result v4

    #@8
    .line 420
    .local v4, line2:I
    if-ne v3, v4, :cond_21

    #@a
    .line 423
    invoke-virtual {p0, p2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@d
    move-result v0

    #@e
    .line 424
    .local v0, h1:F
    invoke-virtual {p0, p3}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@11
    move-result v1

    #@12
    .line 426
    .local v1, h2:F
    if-gez p1, :cond_1b

    #@14
    .line 429
    cmpg-float v6, v0, v1

    #@16
    if-gez v6, :cond_19

    #@18
    .line 454
    .end local v0           #h1:F
    .end local v1           #h2:F
    .end local p2
    :cond_18
    :goto_18
    return p2

    #@19
    .restart local v0       #h1:F
    .restart local v1       #h2:F
    .restart local p2
    :cond_19
    move p2, p3

    #@1a
    .line 432
    goto :goto_18

    #@1b
    .line 436
    :cond_1b
    cmpl-float v6, v0, v1

    #@1d
    if-gtz v6, :cond_18

    #@1f
    move p2, p3

    #@20
    .line 439
    goto :goto_18

    #@21
    .line 448
    .end local v0           #h1:F
    .end local v1           #h2:F
    :cond_21
    invoke-virtual {p0, p2}, Landroid/text/Layout;->getLineForOffset(I)I

    #@24
    move-result v2

    #@25
    .line 449
    .local v2, line:I
    invoke-virtual {p0, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@28
    move-result v5

    #@29
    .line 451
    .local v5, textdir:I
    if-ne v5, p1, :cond_30

    #@2b
    .line 452
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    #@2e
    move-result p2

    #@2f
    goto :goto_18

    #@30
    .line 454
    :cond_30
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    #@33
    move-result p2

    #@34
    goto :goto_18
.end method

.method public static extendDown(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 9
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 282
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@4
    move-result v0

    #@5
    .line 283
    .local v0, end:I
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@8
    move-result v2

    #@9
    .line 285
    .local v2, line:I
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    #@c
    move-result v4

    #@d
    add-int/lit8 v4, v4, -0x1

    #@f
    if-ge v2, v4, :cond_32

    #@11
    .line 288
    invoke-virtual {p1, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@14
    move-result v4

    #@15
    add-int/lit8 v5, v2, 0x1

    #@17
    invoke-virtual {p1, v5}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@1a
    move-result v5

    #@1b
    if-ne v4, v5, :cond_2b

    #@1d
    .line 290
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@20
    move-result v1

    #@21
    .line 291
    .local v1, h:F
    add-int/lit8 v4, v2, 0x1

    #@23
    invoke-virtual {p1, v4, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@26
    move-result v3

    #@27
    .line 296
    .end local v1           #h:F
    .local v3, move:I
    :goto_27
    invoke-static {p0, v3}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@2a
    .line 303
    .end local v3           #move:I
    :cond_2a
    :goto_2a
    return v6

    #@2b
    .line 293
    :cond_2b
    add-int/lit8 v4, v2, 0x1

    #@2d
    invoke-virtual {p1, v4}, Landroid/text/Layout;->getLineStart(I)I

    #@30
    move-result v3

    #@31
    .restart local v3       #move:I
    goto :goto_27

    #@32
    .line 298
    .end local v3           #move:I
    :cond_32
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@35
    move-result v4

    #@36
    if-eq v0, v4, :cond_2a

    #@38
    .line 299
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@3b
    move-result v4

    #@3c
    invoke-static {p0, v4}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@3f
    goto :goto_2a
.end method

.method public static extendLeft(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 5
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 311
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@4
    move-result v0

    #@5
    .line 312
    .local v0, end:I
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getOffsetToLeftOf(I)I

    #@8
    move-result v1

    #@9
    .line 314
    .local v1, to:I
    if-eq v1, v0, :cond_e

    #@b
    .line 315
    invoke-static {p0, v1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@e
    .line 319
    :cond_e
    return v2
.end method

.method public static extendRight(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 5
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 327
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@4
    move-result v0

    #@5
    .line 328
    .local v0, end:I
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getOffsetToRightOf(I)I

    #@8
    move-result v1

    #@9
    .line 330
    .local v1, to:I
    if-eq v1, v0, :cond_e

    #@b
    .line 331
    invoke-static {p0, v1}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@e
    .line 335
    :cond_e
    return v2
.end method

.method public static final extendSelection(Landroid/text/Spannable;I)V
    .registers 4
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    .line 101
    sget-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    #@5
    move-result v0

    #@6
    if-eq v0, p1, :cond_f

    #@8
    .line 102
    sget-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@a
    const/16 v1, 0x22

    #@c
    invoke-interface {p0, v0, p1, p1, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@f
    .line 103
    :cond_f
    return-void
.end method

.method public static extendToLeftEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 4
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    .line 339
    const/4 v1, -0x1

    #@1
    invoke-static {p0, p1, v1}, Landroid/text/Selection;->findEdge(Landroid/text/Spannable;Landroid/text/Layout;I)I

    #@4
    move-result v0

    #@5
    .line 340
    .local v0, where:I
    invoke-static {p0, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@8
    .line 341
    const/4 v1, 0x1

    #@9
    return v1
.end method

.method public static extendToRightEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 4
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 345
    invoke-static {p0, p1, v1}, Landroid/text/Selection;->findEdge(Landroid/text/Spannable;Landroid/text/Layout;I)I

    #@4
    move-result v0

    #@5
    .line 346
    .local v0, where:I
    invoke-static {p0, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@8
    .line 347
    return v1
.end method

.method public static extendUp(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 9
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 253
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@4
    move-result v0

    #@5
    .line 254
    .local v0, end:I
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@8
    move-result v2

    #@9
    .line 256
    .local v2, line:I
    if-lez v2, :cond_2c

    #@b
    .line 259
    invoke-virtual {p1, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@e
    move-result v4

    #@f
    add-int/lit8 v5, v2, -0x1

    #@11
    invoke-virtual {p1, v5}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@14
    move-result v5

    #@15
    if-ne v4, v5, :cond_25

    #@17
    .line 261
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@1a
    move-result v1

    #@1b
    .line 262
    .local v1, h:F
    add-int/lit8 v4, v2, -0x1

    #@1d
    invoke-virtual {p1, v4, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@20
    move-result v3

    #@21
    .line 267
    .end local v1           #h:F
    .local v3, move:I
    :goto_21
    invoke-static {p0, v3}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@24
    .line 274
    .end local v3           #move:I
    :cond_24
    :goto_24
    return v6

    #@25
    .line 264
    :cond_25
    add-int/lit8 v4, v2, -0x1

    #@27
    invoke-virtual {p1, v4}, Landroid/text/Layout;->getLineStart(I)I

    #@2a
    move-result v3

    #@2b
    .restart local v3       #move:I
    goto :goto_21

    #@2c
    .line 269
    .end local v3           #move:I
    :cond_2c
    if-eqz v0, :cond_24

    #@2e
    .line 270
    const/4 v4, 0x0

    #@2f
    invoke-static {p0, v4}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@32
    goto :goto_24
.end method

.method private static findEdge(Landroid/text/Spannable;Landroid/text/Layout;I)I
    .registers 8
    .parameter "text"
    .parameter "layout"
    .parameter "dir"

    #@0
    .prologue
    .line 399
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@3
    move-result v3

    #@4
    .line 400
    .local v3, pt:I
    invoke-virtual {p1, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    #@7
    move-result v1

    #@8
    .line 401
    .local v1, line:I
    invoke-virtual {p1, v1}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@b
    move-result v2

    #@c
    .line 403
    .local v2, pdir:I
    mul-int v4, p2, v2

    #@e
    if-gez v4, :cond_15

    #@10
    .line 404
    invoke-virtual {p1, v1}, Landroid/text/Layout;->getLineStart(I)I

    #@13
    move-result v0

    #@14
    .line 411
    :cond_14
    :goto_14
    return v0

    #@15
    .line 406
    :cond_15
    invoke-virtual {p1, v1}, Landroid/text/Layout;->getLineEnd(I)I

    #@18
    move-result v0

    #@19
    .line 408
    .local v0, end:I
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    #@1c
    move-result v4

    #@1d
    add-int/lit8 v4, v4, -0x1

    #@1f
    if-eq v1, v4, :cond_14

    #@21
    .line 411
    add-int/lit8 v0, v0, -0x1

    #@23
    goto :goto_14
.end method

.method public static final getSelectionEnd(Ljava/lang/CharSequence;)I
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 49
    instance-of v0, p0, Landroid/text/Spanned;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 50
    check-cast p0, Landroid/text/Spanned;

    #@6
    .end local p0
    sget-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@8
    invoke-interface {p0, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@b
    move-result v0

    #@c
    .line 52
    :goto_c
    return v0

    #@d
    .restart local p0
    :cond_d
    const/4 v0, -0x1

    #@e
    goto :goto_c
.end method

.method public static final getSelectionStart(Ljava/lang/CharSequence;)I
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 38
    instance-of v0, p0, Landroid/text/Spanned;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 39
    check-cast p0, Landroid/text/Spanned;

    #@6
    .end local p0
    sget-object v0, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@8
    invoke-interface {p0, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@b
    move-result v0

    #@c
    .line 41
    :goto_c
    return v0

    #@d
    .restart local p0
    :cond_d
    const/4 v0, -0x1

    #@e
    goto :goto_c
.end method

.method public static moveDown(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 12
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 163
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@5
    move-result v6

    #@6
    .line 164
    .local v6, start:I
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@9
    move-result v0

    #@a
    .line 166
    .local v0, end:I
    if-eq v6, v0, :cond_22

    #@c
    .line 167
    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v4

    #@10
    .line 168
    .local v4, min:I
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v3

    #@14
    .line 170
    .local v3, max:I
    invoke-static {p0, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@17
    .line 172
    if-nez v4, :cond_20

    #@19
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@1c
    move-result v9

    #@1d
    if-ne v3, v9, :cond_20

    #@1f
    .line 196
    .end local v3           #max:I
    .end local v4           #min:I
    :cond_1f
    :goto_1f
    return v7

    #@20
    .restart local v3       #max:I
    .restart local v4       #min:I
    :cond_20
    move v7, v8

    #@21
    .line 176
    goto :goto_1f

    #@22
    .line 178
    .end local v3           #max:I
    .end local v4           #min:I
    :cond_22
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@25
    move-result v2

    #@26
    .line 180
    .local v2, line:I
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    #@29
    move-result v9

    #@2a
    add-int/lit8 v9, v9, -0x1

    #@2c
    if-ge v2, v9, :cond_1f

    #@2e
    .line 183
    invoke-virtual {p1, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@31
    move-result v7

    #@32
    add-int/lit8 v9, v2, 0x1

    #@34
    invoke-virtual {p1, v9}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@37
    move-result v9

    #@38
    if-ne v7, v9, :cond_49

    #@3a
    .line 185
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@3d
    move-result v1

    #@3e
    .line 186
    .local v1, h:F
    add-int/lit8 v7, v2, 0x1

    #@40
    invoke-virtual {p1, v7, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@43
    move-result v5

    #@44
    .line 191
    .end local v1           #h:F
    .local v5, move:I
    :goto_44
    invoke-static {p0, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@47
    move v7, v8

    #@48
    .line 192
    goto :goto_1f

    #@49
    .line 188
    .end local v5           #move:I
    :cond_49
    add-int/lit8 v7, v2, 0x1

    #@4b
    invoke-virtual {p1, v7}, Landroid/text/Layout;->getLineStart(I)I

    #@4e
    move-result v5

    #@4f
    .restart local v5       #move:I
    goto :goto_44
.end method

.method public static moveLeft(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 7
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 205
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@4
    move-result v1

    #@5
    .line 206
    .local v1, start:I
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@8
    move-result v0

    #@9
    .line 208
    .local v0, end:I
    if-eq v1, v0, :cond_14

    #@b
    .line 209
    const/4 v4, -0x1

    #@c
    invoke-static {p1, v4, v1, v0}, Landroid/text/Selection;->chooseHorizontal(Landroid/text/Layout;III)I

    #@f
    move-result v4

    #@10
    invoke-static {p0, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@13
    .line 220
    :goto_13
    return v3

    #@14
    .line 212
    :cond_14
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getOffsetToLeftOf(I)I

    #@17
    move-result v2

    #@18
    .line 214
    .local v2, to:I
    if-eq v2, v0, :cond_1e

    #@1a
    .line 215
    invoke-static {p0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@1d
    goto :goto_13

    #@1e
    .line 220
    :cond_1e
    const/4 v3, 0x0

    #@1f
    goto :goto_13
.end method

.method public static moveRight(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 7
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 230
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@4
    move-result v1

    #@5
    .line 231
    .local v1, start:I
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@8
    move-result v0

    #@9
    .line 233
    .local v0, end:I
    if-eq v1, v0, :cond_13

    #@b
    .line 234
    invoke-static {p1, v3, v1, v0}, Landroid/text/Selection;->chooseHorizontal(Landroid/text/Layout;III)I

    #@e
    move-result v4

    #@f
    invoke-static {p0, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@12
    .line 245
    :goto_12
    return v3

    #@13
    .line 237
    :cond_13
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getOffsetToRightOf(I)I

    #@16
    move-result v2

    #@17
    .line 239
    .local v2, to:I
    if-eq v2, v0, :cond_1d

    #@19
    .line 240
    invoke-static {p0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@1c
    goto :goto_12

    #@1d
    .line 245
    :cond_1d
    const/4 v3, 0x0

    #@1e
    goto :goto_12
.end method

.method public static moveToFollowing(Landroid/text/Spannable;Landroid/text/Selection$PositionIterator;Z)Z
    .registers 5
    .parameter "text"
    .parameter "iter"
    .parameter "extendSelection"

    #@0
    .prologue
    .line 387
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@3
    move-result v1

    #@4
    invoke-interface {p1, v1}, Landroid/text/Selection$PositionIterator;->following(I)I

    #@7
    move-result v0

    #@8
    .line 388
    .local v0, offset:I
    const/4 v1, -0x1

    #@9
    if-eq v0, v1, :cond_10

    #@b
    .line 389
    if-eqz p2, :cond_12

    #@d
    .line 390
    invoke-static {p0, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@10
    .line 395
    :cond_10
    :goto_10
    const/4 v1, 0x1

    #@11
    return v1

    #@12
    .line 392
    :cond_12
    invoke-static {p0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@15
    goto :goto_10
.end method

.method public static moveToLeftEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 4
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    .line 351
    const/4 v1, -0x1

    #@1
    invoke-static {p0, p1, v1}, Landroid/text/Selection;->findEdge(Landroid/text/Spannable;Landroid/text/Layout;I)I

    #@4
    move-result v0

    #@5
    .line 352
    .local v0, where:I
    invoke-static {p0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@8
    .line 353
    const/4 v1, 0x1

    #@9
    return v1
.end method

.method public static moveToPreceding(Landroid/text/Spannable;Landroid/text/Selection$PositionIterator;Z)Z
    .registers 5
    .parameter "text"
    .parameter "iter"
    .parameter "extendSelection"

    #@0
    .prologue
    .line 373
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@3
    move-result v1

    #@4
    invoke-interface {p1, v1}, Landroid/text/Selection$PositionIterator;->preceding(I)I

    #@7
    move-result v0

    #@8
    .line 374
    .local v0, offset:I
    const/4 v1, -0x1

    #@9
    if-eq v0, v1, :cond_10

    #@b
    .line 375
    if-eqz p2, :cond_12

    #@d
    .line 376
    invoke-static {p0, v0}, Landroid/text/Selection;->extendSelection(Landroid/text/Spannable;I)V

    #@10
    .line 381
    :cond_10
    :goto_10
    const/4 v1, 0x1

    #@11
    return v1

    #@12
    .line 378
    :cond_12
    invoke-static {p0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@15
    goto :goto_10
.end method

.method public static moveToRightEdge(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 4
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 357
    invoke-static {p0, p1, v1}, Landroid/text/Selection;->findEdge(Landroid/text/Spannable;Landroid/text/Layout;I)I

    #@4
    move-result v0

    #@5
    .line 358
    .local v0, where:I
    invoke-static {p0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@8
    .line 359
    return v1
.end method

.method public static moveUp(Landroid/text/Spannable;Landroid/text/Layout;)Z
    .registers 12
    .parameter "text"
    .parameter "layout"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 122
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@5
    move-result v6

    #@6
    .line 123
    .local v6, start:I
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@9
    move-result v0

    #@a
    .line 125
    .local v0, end:I
    if-eq v6, v0, :cond_22

    #@c
    .line 126
    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v4

    #@10
    .line 127
    .local v4, min:I
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    #@13
    move-result v3

    #@14
    .line 129
    .local v3, max:I
    invoke-static {p0, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@17
    .line 131
    if-nez v4, :cond_20

    #@19
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@1c
    move-result v9

    #@1d
    if-ne v3, v9, :cond_20

    #@1f
    .line 155
    .end local v3           #max:I
    .end local v4           #min:I
    :cond_1f
    :goto_1f
    return v7

    #@20
    .restart local v3       #max:I
    .restart local v4       #min:I
    :cond_20
    move v7, v8

    #@21
    .line 135
    goto :goto_1f

    #@22
    .line 137
    .end local v3           #max:I
    .end local v4           #min:I
    :cond_22
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    #@25
    move-result v2

    #@26
    .line 139
    .local v2, line:I
    if-lez v2, :cond_1f

    #@28
    .line 142
    invoke-virtual {p1, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@2b
    move-result v7

    #@2c
    add-int/lit8 v9, v2, -0x1

    #@2e
    invoke-virtual {p1, v9}, Landroid/text/Layout;->getParagraphDirection(I)I

    #@31
    move-result v9

    #@32
    if-ne v7, v9, :cond_43

    #@34
    .line 144
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    #@37
    move-result v1

    #@38
    .line 145
    .local v1, h:F
    add-int/lit8 v7, v2, -0x1

    #@3a
    invoke-virtual {p1, v7, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    #@3d
    move-result v5

    #@3e
    .line 150
    .end local v1           #h:F
    .local v5, move:I
    :goto_3e
    invoke-static {p0, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    #@41
    move v7, v8

    #@42
    .line 151
    goto :goto_1f

    #@43
    .line 147
    .end local v5           #move:I
    :cond_43
    add-int/lit8 v7, v2, -0x1

    #@45
    invoke-virtual {p1, v7}, Landroid/text/Layout;->getLineStart(I)I

    #@48
    move-result v5

    #@49
    .restart local v5       #move:I
    goto :goto_3e
.end method

.method public static final removeSelection(Landroid/text/Spannable;)V
    .registers 2
    .parameter "text"

    #@0
    .prologue
    .line 109
    sget-object v0, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@2
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@5
    .line 110
    sget-object v0, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@7
    invoke-interface {p0, v0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@a
    .line 111
    return-void
.end method

.method public static final selectAll(Landroid/text/Spannable;)V
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 94
    const/4 v0, 0x0

    #@1
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    #@4
    move-result v1

    #@5
    invoke-static {p0, v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@8
    .line 95
    return-void
.end method

.method public static final setSelection(Landroid/text/Spannable;I)V
    .registers 2
    .parameter "text"
    .parameter "index"

    #@0
    .prologue
    .line 87
    invoke-static {p0, p1, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    #@3
    .line 88
    return-void
.end method

.method public static setSelection(Landroid/text/Spannable;II)V
    .registers 7
    .parameter "text"
    .parameter "start"
    .parameter "stop"

    #@0
    .prologue
    .line 72
    invoke-static {p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@3
    move-result v1

    #@4
    .line 73
    .local v1, ostart:I
    invoke-static {p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@7
    move-result v0

    #@8
    .line 75
    .local v0, oend:I
    if-ne v1, p1, :cond_c

    #@a
    if-eq v0, p2, :cond_1a

    #@c
    .line 76
    :cond_c
    sget-object v2, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@e
    const/16 v3, 0x222

    #@10
    invoke-interface {p0, v2, p1, p1, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@13
    .line 78
    sget-object v2, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@15
    const/16 v3, 0x22

    #@17
    invoke-interface {p0, v2, p2, p2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@1a
    .line 81
    :cond_1a
    return-void
.end method
