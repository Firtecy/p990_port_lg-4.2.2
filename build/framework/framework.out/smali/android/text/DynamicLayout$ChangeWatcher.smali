.class Landroid/text/DynamicLayout$ChangeWatcher;
.super Ljava/lang/Object;
.source "DynamicLayout.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/text/SpanWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/DynamicLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ChangeWatcher"
.end annotation


# instance fields
.field private mLayout:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/text/DynamicLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/text/DynamicLayout;)V
    .registers 3
    .parameter "layout"

    #@0
    .prologue
    .line 613
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 614
    new-instance v0, Ljava/lang/ref/WeakReference;

    #@5
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@8
    iput-object v0, p0, Landroid/text/DynamicLayout$ChangeWatcher;->mLayout:Ljava/lang/ref/WeakReference;

    #@a
    .line 615
    return-void
.end method

.method private reflow(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "s"
    .parameter "where"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 618
    iget-object v1, p0, Landroid/text/DynamicLayout$ChangeWatcher;->mLayout:Ljava/lang/ref/WeakReference;

    #@2
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/text/DynamicLayout;

    #@8
    .line 620
    .local v0, ml:Landroid/text/DynamicLayout;
    if-eqz v0, :cond_e

    #@a
    .line 621
    invoke-static {v0, p1, p2, p3, p4}, Landroid/text/DynamicLayout;->access$000(Landroid/text/DynamicLayout;Ljava/lang/CharSequence;III)V

    #@d
    .line 624
    .end local p1
    :cond_d
    :goto_d
    return-void

    #@e
    .line 622
    .restart local p1
    :cond_e
    instance-of v1, p1, Landroid/text/Spannable;

    #@10
    if-eqz v1, :cond_d

    #@12
    .line 623
    check-cast p1, Landroid/text/Spannable;

    #@14
    .end local p1
    invoke-interface {p1, p0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@17
    goto :goto_d
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    #@0
    .prologue
    .line 636
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "where"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 628
    return-void
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 7
    .parameter "s"
    .parameter "o"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 639
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 640
    sub-int v0, p4, p3

    #@6
    sub-int v1, p4, p3

    #@8
    invoke-direct {p0, p1, p3, v0, v1}, Landroid/text/DynamicLayout$ChangeWatcher;->reflow(Ljava/lang/CharSequence;III)V

    #@b
    .line 641
    :cond_b
    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .registers 9
    .parameter "s"
    .parameter "o"
    .parameter "start"
    .parameter "end"
    .parameter "nstart"
    .parameter "nend"

    #@0
    .prologue
    .line 649
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    #@2
    if-eqz v0, :cond_12

    #@4
    .line 650
    sub-int v0, p4, p3

    #@6
    sub-int v1, p4, p3

    #@8
    invoke-direct {p0, p1, p3, v0, v1}, Landroid/text/DynamicLayout$ChangeWatcher;->reflow(Ljava/lang/CharSequence;III)V

    #@b
    .line 651
    sub-int v0, p6, p5

    #@d
    sub-int v1, p6, p5

    #@f
    invoke-direct {p0, p1, p5, v0, v1}, Landroid/text/DynamicLayout$ChangeWatcher;->reflow(Ljava/lang/CharSequence;III)V

    #@12
    .line 653
    :cond_12
    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .registers 7
    .parameter "s"
    .parameter "o"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 644
    instance-of v0, p2, Landroid/text/style/UpdateLayout;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 645
    sub-int v0, p4, p3

    #@6
    sub-int v1, p4, p3

    #@8
    invoke-direct {p0, p1, p3, v0, v1}, Landroid/text/DynamicLayout$ChangeWatcher;->reflow(Ljava/lang/CharSequence;III)V

    #@b
    .line 646
    :cond_b
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "where"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 631
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/text/DynamicLayout$ChangeWatcher;->reflow(Ljava/lang/CharSequence;III)V

    #@3
    .line 632
    return-void
.end method
