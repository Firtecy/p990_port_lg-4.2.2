.class public Landroid/text/SpanSet;
.super Ljava/lang/Object;
.source "SpanSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final classType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+TE;>;"
        }
    .end annotation
.end field

.field numberOfSpans:I

.field spanEnds:[I

.field spanFlags:[I

.field spanStarts:[I

.field spans:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TE;>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 39
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    .local p1, type:Ljava/lang/Class;,"Ljava/lang/Class<+TE;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 40
    iput-object p1, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    #@5
    .line 41
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@8
    .line 42
    return-void
.end method


# virtual methods
.method getNextTransition(II)I
    .registers 7
    .parameter "start"
    .parameter "limit"

    #@0
    .prologue
    .line 93
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v3, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@3
    if-ge v0, v3, :cond_1a

    #@5
    .line 94
    iget-object v3, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@7
    aget v2, v3, v0

    #@9
    .line 95
    .local v2, spanStart:I
    iget-object v3, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@b
    aget v1, v3, v0

    #@d
    .line 96
    .local v1, spanEnd:I
    if-le v2, p1, :cond_12

    #@f
    if-ge v2, p2, :cond_12

    #@11
    move p2, v2

    #@12
    .line 97
    :cond_12
    if-le v1, p1, :cond_17

    #@14
    if-ge v1, p2, :cond_17

    #@16
    move p2, v1

    #@17
    .line 93
    :cond_17
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_1

    #@1a
    .line 99
    .end local v1           #spanEnd:I
    .end local v2           #spanStart:I
    :cond_1a
    return p2
.end method

.method public hasSpansIntersecting(II)Z
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 81
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@3
    if-ge v0, v1, :cond_16

    #@5
    .line 83
    iget-object v1, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@7
    aget v1, v1, v0

    #@9
    if-ge v1, p2, :cond_11

    #@b
    iget-object v1, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@d
    aget v1, v1, v0

    #@f
    if-gt v1, p1, :cond_14

    #@11
    .line 81
    :cond_11
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_1

    #@14
    .line 84
    :cond_14
    const/4 v1, 0x1

    #@15
    .line 86
    :goto_15
    return v1

    #@16
    :cond_16
    const/4 v1, 0x0

    #@17
    goto :goto_15
.end method

.method public init(Landroid/text/Spanned;II)V
    .registers 13
    .parameter "spanned"
    .parameter "start"
    .parameter "limit"

    #@0
    .prologue
    .line 46
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    iget-object v7, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    #@2
    invoke-interface {p1, p2, p3, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    .line 47
    .local v0, allSpans:[Ljava/lang/Object;,"[TE;"
    array-length v2, v0

    #@7
    .line 49
    .local v2, length:I
    if-lez v2, :cond_2a

    #@9
    iget-object v7, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@b
    if-eqz v7, :cond_12

    #@d
    iget-object v7, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@f
    array-length v7, v7

    #@10
    if-ge v7, v2, :cond_2a

    #@12
    .line 51
    :cond_12
    iget-object v7, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    #@14
    invoke-static {v7, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@17
    move-result-object v7

    #@18
    check-cast v7, [Ljava/lang/Object;

    #@1a
    check-cast v7, [Ljava/lang/Object;

    #@1c
    iput-object v7, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@1e
    .line 52
    new-array v7, v2, [I

    #@20
    iput-object v7, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@22
    .line 53
    new-array v7, v2, [I

    #@24
    iput-object v7, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@26
    .line 54
    new-array v7, v2, [I

    #@28
    iput-object v7, p0, Landroid/text/SpanSet;->spanFlags:[I

    #@2a
    .line 57
    :cond_2a
    const/4 v7, 0x0

    #@2b
    iput v7, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@2d
    .line 58
    const/4 v1, 0x0

    #@2e
    .local v1, i:I
    :goto_2e
    if-ge v1, v2, :cond_62

    #@30
    .line 59
    aget-object v3, v0, v1

    #@32
    .line 61
    .local v3, span:Ljava/lang/Object;,"TE;"
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@35
    move-result v6

    #@36
    .line 62
    .local v6, spanStart:I
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@39
    move-result v4

    #@3a
    .line 63
    .local v4, spanEnd:I
    if-ne v6, v4, :cond_3f

    #@3c
    .line 58
    :goto_3c
    add-int/lit8 v1, v1, 0x1

    #@3e
    goto :goto_2e

    #@3f
    .line 65
    :cond_3f
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@42
    move-result v5

    #@43
    .line 67
    .local v5, spanFlag:I
    iget-object v7, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@45
    iget v8, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@47
    aput-object v3, v7, v8

    #@49
    .line 68
    iget-object v7, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@4b
    iget v8, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@4d
    aput v6, v7, v8

    #@4f
    .line 69
    iget-object v7, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@51
    iget v8, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@53
    aput v4, v7, v8

    #@55
    .line 70
    iget-object v7, p0, Landroid/text/SpanSet;->spanFlags:[I

    #@57
    iget v8, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@59
    aput v5, v7, v8

    #@5b
    .line 72
    iget v7, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@5d
    add-int/lit8 v7, v7, 0x1

    #@5f
    iput v7, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@61
    goto :goto_3c

    #@62
    .line 74
    .end local v3           #span:Ljava/lang/Object;,"TE;"
    .end local v4           #spanEnd:I
    .end local v5           #spanFlag:I
    .end local v6           #spanStart:I
    :cond_62
    return-void
.end method

.method public recycle()V
    .registers 4

    #@0
    .prologue
    .line 132
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@3
    if-ge v0, v1, :cond_d

    #@5
    .line 133
    iget-object v1, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    #@7
    const/4 v2, 0x0

    #@8
    aput-object v2, v1, v0

    #@a
    .line 132
    add-int/lit8 v0, v0, 0x1

    #@c
    goto :goto_1

    #@d
    .line 135
    :cond_d
    return-void
.end method

.method public updateTransition(IIII)Z
    .registers 8
    .parameter "start"
    .parameter "end"
    .parameter "startOffset"
    .parameter "endOffset"

    #@0
    .prologue
    .line 115
    .local p0, this:Landroid/text/SpanSet;,"Landroid/text/SpanSet<TE;>;"
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    iget v1, p0, Landroid/text/SpanSet;->numberOfSpans:I

    #@3
    if-ge v0, v1, :cond_24

    #@5
    .line 117
    iget-object v1, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@7
    aget v1, v1, v0

    #@9
    if-ne v1, p1, :cond_21

    #@b
    iget-object v1, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@d
    aget v1, v1, v0

    #@f
    if-ne v1, p2, :cond_21

    #@11
    .line 118
    iget-object v1, p0, Landroid/text/SpanSet;->spanStarts:[I

    #@13
    aget v2, v1, v0

    #@15
    sub-int/2addr v2, p3

    #@16
    aput v2, v1, v0

    #@18
    .line 119
    iget-object v1, p0, Landroid/text/SpanSet;->spanEnds:[I

    #@1a
    aget v2, v1, v0

    #@1c
    add-int/2addr v2, p4

    #@1d
    aput v2, v1, v0

    #@1f
    .line 120
    const/4 v1, 0x1

    #@20
    .line 123
    :goto_20
    return v1

    #@21
    .line 115
    :cond_21
    add-int/lit8 v0, v0, 0x1

    #@23
    goto :goto_1

    #@24
    .line 123
    :cond_24
    const/4 v1, 0x0

    #@25
    goto :goto_20
.end method
