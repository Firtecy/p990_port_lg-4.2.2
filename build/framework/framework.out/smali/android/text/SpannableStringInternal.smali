.class abstract Landroid/text/SpannableStringInternal;
.super Ljava/lang/Object;
.source "SpannableStringInternal.java"


# static fields
.field private static final COLUMNS:I = 0x3

.field static final EMPTY:[Ljava/lang/Object; = null

.field private static final END:I = 0x1

.field private static final FLAGS:I = 0x2

.field private static final START:I


# instance fields
.field private mSpanCount:I

.field private mSpanData:[I

.field private mSpans:[Ljava/lang/Object;

.field private mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 366
    const/4 v0, 0x0

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    sput-object v0, Landroid/text/SpannableStringInternal;->EMPTY:[Ljava/lang/Object;

    #@5
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;II)V
    .registers 14
    .parameter "source"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    if-nez p2, :cond_55

    #@5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v7

    #@9
    if-ne p3, v7, :cond_55

    #@b
    .line 28
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@e
    move-result-object v7

    #@f
    iput-object v7, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@11
    .line 32
    :goto_11
    const/4 v7, 0x0

    #@12
    invoke-static {v7}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@15
    move-result v3

    #@16
    .line 33
    .local v3, initial:I
    new-array v7, v3, [Ljava/lang/Object;

    #@18
    iput-object v7, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@1a
    .line 34
    mul-int/lit8 v7, v3, 0x3

    #@1c
    new-array v7, v7, [I

    #@1e
    iput-object v7, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@20
    .line 36
    instance-of v7, p1, Landroid/text/Spanned;

    #@22
    if-eqz v7, :cond_60

    #@24
    move-object v4, p1

    #@25
    .line 37
    check-cast v4, Landroid/text/Spanned;

    #@27
    .line 38
    .local v4, sp:Landroid/text/Spanned;
    const-class v7, Ljava/lang/Object;

    #@29
    invoke-interface {v4, p2, p3, v7}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@2c
    move-result-object v5

    #@2d
    .line 40
    .local v5, spans:[Ljava/lang/Object;
    const/4 v2, 0x0

    #@2e
    .local v2, i:I
    :goto_2e
    array-length v7, v5

    #@2f
    if-ge v2, v7, :cond_60

    #@31
    .line 41
    aget-object v7, v5, v2

    #@33
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@36
    move-result v6

    #@37
    .line 42
    .local v6, st:I
    aget-object v7, v5, v2

    #@39
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@3c
    move-result v0

    #@3d
    .line 43
    .local v0, en:I
    aget-object v7, v5, v2

    #@3f
    invoke-interface {v4, v7}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@42
    move-result v1

    #@43
    .line 45
    .local v1, fl:I
    if-ge v6, p2, :cond_46

    #@45
    .line 46
    move v6, p2

    #@46
    .line 47
    :cond_46
    if-le v0, p3, :cond_49

    #@48
    .line 48
    move v0, p3

    #@49
    .line 50
    :cond_49
    aget-object v7, v5, v2

    #@4b
    sub-int v8, v6, p2

    #@4d
    sub-int v9, v0, p2

    #@4f
    invoke-virtual {p0, v7, v8, v9, v1}, Landroid/text/SpannableStringInternal;->setSpan(Ljava/lang/Object;III)V

    #@52
    .line 40
    add-int/lit8 v2, v2, 0x1

    #@54
    goto :goto_2e

    #@55
    .line 30
    .end local v0           #en:I
    .end local v1           #fl:I
    .end local v2           #i:I
    .end local v3           #initial:I
    .end local v4           #sp:Landroid/text/Spanned;
    .end local v5           #spans:[Ljava/lang/Object;
    .end local v6           #st:I
    :cond_55
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v7, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@5c
    move-result-object v7

    #@5d
    iput-object v7, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@5f
    goto :goto_11

    #@60
    .line 53
    .restart local v3       #initial:I
    :cond_60
    return-void
.end method

.method private checkRange(Ljava/lang/String;II)V
    .registers 8
    .parameter "operation"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 340
    if-ge p3, p2, :cond_29

    #@2
    .line 341
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    const-string v3, " "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-static {p2, p3}, Landroid/text/SpannableStringInternal;->region(II)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " has end before start"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 346
    :cond_29
    invoke-virtual {p0}, Landroid/text/SpannableStringInternal;->length()I

    #@2c
    move-result v0

    #@2d
    .line 348
    .local v0, len:I
    if-gt p2, v0, :cond_31

    #@2f
    if-le p3, v0, :cond_5c

    #@31
    .line 349
    :cond_31
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    const-string v3, " "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-static {p2, p3}, Landroid/text/SpannableStringInternal;->region(II)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, " ends beyond length "

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v1

    #@5c
    .line 354
    :cond_5c
    if-ltz p2, :cond_60

    #@5e
    if-gez p3, :cond_87

    #@60
    .line 355
    :cond_60
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@62
    new-instance v2, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v3, " "

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-static {p2, p3}, Landroid/text/SpannableStringInternal;->region(II)Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    const-string v3, " starts before 0"

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@86
    throw v1

    #@87
    .line 359
    :cond_87
    return-void
.end method

.method private static region(II)Ljava/lang/String;
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " ... "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ")"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    return-object v0
.end method

.method private sendSpanAdded(Ljava/lang/Object;II)V
    .registers 9
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 308
    const-class v3, Landroid/text/SpanWatcher;

    #@2
    invoke-virtual {p0, p2, p3, v3}, Landroid/text/SpannableStringInternal;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, [Landroid/text/SpanWatcher;

    #@8
    .line 309
    .local v2, recip:[Landroid/text/SpanWatcher;
    array-length v1, v2

    #@9
    .line 311
    .local v1, n:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_17

    #@c
    .line 312
    aget-object v4, v2, v0

    #@e
    move-object v3, p0

    #@f
    check-cast v3, Landroid/text/Spannable;

    #@11
    invoke-interface {v4, v3, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@14
    .line 311
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_a

    #@17
    .line 314
    :cond_17
    return-void
.end method

.method private sendSpanChanged(Ljava/lang/Object;IIII)V
    .registers 16
    .parameter "what"
    .parameter "s"
    .parameter "e"
    .parameter "st"
    .parameter "en"

    #@0
    .prologue
    .line 326
    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    #@3
    move-result v0

    #@4
    invoke-static {p3, p5}, Ljava/lang/Math;->max(II)I

    #@7
    move-result v1

    #@8
    const-class v2, Landroid/text/SpanWatcher;

    #@a
    invoke-virtual {p0, v0, v1, v2}, Landroid/text/SpannableStringInternal;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@d
    move-result-object v9

    #@e
    check-cast v9, [Landroid/text/SpanWatcher;

    #@10
    .line 328
    .local v9, recip:[Landroid/text/SpanWatcher;
    array-length v8, v9

    #@11
    .line 330
    .local v8, n:I
    const/4 v7, 0x0

    #@12
    .local v7, i:I
    :goto_12
    if-ge v7, v8, :cond_24

    #@14
    .line 331
    aget-object v0, v9, v7

    #@16
    move-object v1, p0

    #@17
    check-cast v1, Landroid/text/Spannable;

    #@19
    move-object v2, p1

    #@1a
    move v3, p2

    #@1b
    move v4, p3

    #@1c
    move v5, p4

    #@1d
    move v6, p5

    #@1e
    invoke-interface/range {v0 .. v6}, Landroid/text/SpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    #@21
    .line 330
    add-int/lit8 v7, v7, 0x1

    #@23
    goto :goto_12

    #@24
    .line 333
    :cond_24
    return-void
.end method

.method private sendSpanRemoved(Ljava/lang/Object;II)V
    .registers 9
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 317
    const-class v3, Landroid/text/SpanWatcher;

    #@2
    invoke-virtual {p0, p2, p3, v3}, Landroid/text/SpannableStringInternal;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, [Landroid/text/SpanWatcher;

    #@8
    .line 318
    .local v2, recip:[Landroid/text/SpanWatcher;
    array-length v1, v2

    #@9
    .line 320
    .local v1, n:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_17

    #@c
    .line 321
    aget-object v4, v2, v0

    #@e
    move-object v3, p0

    #@f
    check-cast v3, Landroid/text/Spannable;

    #@11
    invoke-interface {v4, v3, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@14
    .line 320
    add-int/lit8 v0, v0, 0x1

    #@16
    goto :goto_a

    #@17
    .line 323
    :cond_17
    return-void
.end method


# virtual methods
.method public final charAt(I)C
    .registers 3
    .parameter "i"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@2
    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public final getChars(II[CI)V
    .registers 6
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "off"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@2
    invoke-virtual {v0, p1, p2, p3, p4}, Ljava/lang/String;->getChars(II[CI)V

    #@5
    .line 71
    return-void
.end method

.method public getSpanEnd(Ljava/lang/Object;)I
    .registers 7
    .parameter "what"

    #@0
    .prologue
    .line 178
    iget v0, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@2
    .line 179
    .local v0, count:I
    iget-object v3, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@4
    .line 180
    .local v3, spans:[Ljava/lang/Object;
    iget-object v1, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@6
    .line 182
    .local v1, data:[I
    add-int/lit8 v2, v0, -0x1

    #@8
    .local v2, i:I
    :goto_8
    if-ltz v2, :cond_18

    #@a
    .line 183
    aget-object v4, v3, v2

    #@c
    if-ne v4, p1, :cond_15

    #@e
    .line 184
    mul-int/lit8 v4, v2, 0x3

    #@10
    add-int/lit8 v4, v4, 0x1

    #@12
    aget v4, v1, v4

    #@14
    .line 188
    :goto_14
    return v4

    #@15
    .line 182
    :cond_15
    add-int/lit8 v2, v2, -0x1

    #@17
    goto :goto_8

    #@18
    .line 188
    :cond_18
    const/4 v4, -0x1

    #@19
    goto :goto_14
.end method

.method public getSpanFlags(Ljava/lang/Object;)I
    .registers 7
    .parameter "what"

    #@0
    .prologue
    .line 192
    iget v0, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@2
    .line 193
    .local v0, count:I
    iget-object v3, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@4
    .line 194
    .local v3, spans:[Ljava/lang/Object;
    iget-object v1, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@6
    .line 196
    .local v1, data:[I
    add-int/lit8 v2, v0, -0x1

    #@8
    .local v2, i:I
    :goto_8
    if-ltz v2, :cond_18

    #@a
    .line 197
    aget-object v4, v3, v2

    #@c
    if-ne v4, p1, :cond_15

    #@e
    .line 198
    mul-int/lit8 v4, v2, 0x3

    #@10
    add-int/lit8 v4, v4, 0x2

    #@12
    aget v4, v1, v4

    #@14
    .line 202
    :goto_14
    return v4

    #@15
    .line 196
    :cond_15
    add-int/lit8 v2, v2, -0x1

    #@17
    goto :goto_8

    #@18
    .line 202
    :cond_18
    const/4 v4, 0x0

    #@19
    goto :goto_14
.end method

.method public getSpanStart(Ljava/lang/Object;)I
    .registers 7
    .parameter "what"

    #@0
    .prologue
    .line 164
    iget v0, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@2
    .line 165
    .local v0, count:I
    iget-object v3, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@4
    .line 166
    .local v3, spans:[Ljava/lang/Object;
    iget-object v1, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@6
    .line 168
    .local v1, data:[I
    add-int/lit8 v2, v0, -0x1

    #@8
    .local v2, i:I
    :goto_8
    if-ltz v2, :cond_18

    #@a
    .line 169
    aget-object v4, v3, v2

    #@c
    if-ne v4, p1, :cond_15

    #@e
    .line 170
    mul-int/lit8 v4, v2, 0x3

    #@10
    add-int/lit8 v4, v4, 0x0

    #@12
    aget v4, v1, v4

    #@14
    .line 174
    :goto_14
    return v4

    #@15
    .line 168
    :cond_15
    add-int/lit8 v2, v2, -0x1

    #@17
    goto :goto_8

    #@18
    .line 174
    :cond_18
    const/4 v4, -0x1

    #@19
    goto :goto_14
.end method

.method public getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 22
    .parameter "queryStart"
    .parameter "queryEnd"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 206
    .local p3, kind:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    const/4 v2, 0x0

    #@1
    .line 208
    .local v2, count:I
    move-object/from16 v0, p0

    #@3
    iget v12, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@5
    .line 209
    .local v12, spanCount:I
    move-object/from16 v0, p0

    #@7
    iget-object v15, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@9
    .line 210
    .local v15, spans:[Ljava/lang/Object;
    move-object/from16 v0, p0

    #@b
    iget-object v4, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@d
    .line 211
    .local v4, data:[I
    const/4 v10, 0x0

    #@e
    .line 212
    .local v10, ret:[Ljava/lang/Object;
    const/4 v11, 0x0

    #@f
    .line 214
    .local v11, ret1:Ljava/lang/Object;
    const/4 v5, 0x0

    #@10
    .local v5, i:I
    move v3, v2

    #@11
    .end local v2           #count:I
    .end local v11           #ret1:Ljava/lang/Object;
    .local v3, count:I
    :goto_11
    if-ge v5, v12, :cond_b2

    #@13
    .line 215
    if-eqz p3, :cond_26

    #@15
    aget-object v16, v15, v5

    #@17
    move-object/from16 v0, p3

    #@19
    move-object/from16 v1, v16

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@1e
    move-result v16

    #@1f
    if-nez v16, :cond_26

    #@21
    move v2, v3

    #@22
    .line 214
    .end local v3           #count:I
    .restart local v2       #count:I
    :goto_22
    add-int/lit8 v5, v5, 0x1

    #@24
    move v3, v2

    #@25
    .end local v2           #count:I
    .restart local v3       #count:I
    goto :goto_11

    #@26
    .line 219
    :cond_26
    mul-int/lit8 v16, v5, 0x3

    #@28
    add-int/lit8 v16, v16, 0x0

    #@2a
    aget v14, v4, v16

    #@2c
    .line 220
    .local v14, spanStart:I
    mul-int/lit8 v16, v5, 0x3

    #@2e
    add-int/lit8 v16, v16, 0x1

    #@30
    aget v13, v4, v16

    #@32
    .line 222
    .local v13, spanEnd:I
    move/from16 v0, p2

    #@34
    if-le v14, v0, :cond_38

    #@36
    move v2, v3

    #@37
    .line 223
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_22

    #@38
    .line 225
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_38
    move/from16 v0, p1

    #@3a
    if-ge v13, v0, :cond_3e

    #@3c
    move v2, v3

    #@3d
    .line 226
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_22

    #@3e
    .line 229
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_3e
    if-eq v14, v13, :cond_52

    #@40
    move/from16 v0, p1

    #@42
    move/from16 v1, p2

    #@44
    if-eq v0, v1, :cond_52

    #@46
    .line 230
    move/from16 v0, p2

    #@48
    if-ne v14, v0, :cond_4c

    #@4a
    move v2, v3

    #@4b
    .line 231
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_22

    #@4c
    .line 233
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_4c
    move/from16 v0, p1

    #@4e
    if-ne v13, v0, :cond_52

    #@50
    move v2, v3

    #@51
    .line 234
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_22

    #@52
    .line 238
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_52
    if-nez v3, :cond_59

    #@54
    .line 239
    aget-object v11, v15, v5

    #@56
    .line 240
    .restart local v11       #ret1:Ljava/lang/Object;
    add-int/lit8 v2, v3, 0x1

    #@58
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_22

    #@59
    .line 242
    .end local v2           #count:I
    .end local v11           #ret1:Ljava/lang/Object;
    .restart local v3       #count:I
    :cond_59
    const/16 v16, 0x1

    #@5b
    move/from16 v0, v16

    #@5d
    if-ne v3, v0, :cond_75

    #@5f
    .line 243
    sub-int v16, v12, v5

    #@61
    add-int/lit8 v16, v16, 0x1

    #@63
    move-object/from16 v0, p3

    #@65
    move/from16 v1, v16

    #@67
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@6a
    move-result-object v16

    #@6b
    check-cast v16, [Ljava/lang/Object;

    #@6d
    move-object/from16 v10, v16

    #@6f
    check-cast v10, [Ljava/lang/Object;

    #@71
    .line 244
    const/16 v16, 0x0

    #@73
    aput-object v11, v10, v16

    #@75
    .line 247
    :cond_75
    mul-int/lit8 v16, v5, 0x3

    #@77
    add-int/lit8 v16, v16, 0x2

    #@79
    aget v16, v4, v16

    #@7b
    const/high16 v17, 0xff

    #@7d
    and-int v9, v16, v17

    #@7f
    .line 248
    .local v9, prio:I
    if-eqz v9, :cond_aa

    #@81
    .line 251
    const/4 v6, 0x0

    #@82
    .local v6, j:I
    :goto_82
    if-ge v6, v3, :cond_94

    #@84
    .line 252
    aget-object v16, v10, v6

    #@86
    move-object/from16 v0, p0

    #@88
    move-object/from16 v1, v16

    #@8a
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringInternal;->getSpanFlags(Ljava/lang/Object;)I

    #@8d
    move-result v16

    #@8e
    const/high16 v17, 0xff

    #@90
    and-int v8, v16, v17

    #@92
    .line 254
    .local v8, p:I
    if-le v9, v8, :cond_a7

    #@94
    .line 259
    .end local v8           #p:I
    :cond_94
    add-int/lit8 v16, v6, 0x1

    #@96
    sub-int v17, v3, v6

    #@98
    move/from16 v0, v16

    #@9a
    move/from16 v1, v17

    #@9c
    invoke-static {v10, v6, v10, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@9f
    .line 260
    aget-object v16, v15, v5

    #@a1
    aput-object v16, v10, v6

    #@a3
    .line 261
    add-int/lit8 v2, v3, 0x1

    #@a5
    .line 262
    .end local v3           #count:I
    .restart local v2       #count:I
    goto/16 :goto_22

    #@a7
    .line 251
    .end local v2           #count:I
    .restart local v3       #count:I
    .restart local v8       #p:I
    :cond_a7
    add-int/lit8 v6, v6, 0x1

    #@a9
    goto :goto_82

    #@aa
    .line 263
    .end local v6           #j:I
    .end local v8           #p:I
    :cond_aa
    add-int/lit8 v2, v3, 0x1

    #@ac
    .end local v3           #count:I
    .restart local v2       #count:I
    aget-object v16, v15, v5

    #@ae
    aput-object v16, v10, v3

    #@b0
    goto/16 :goto_22

    #@b2
    .line 268
    .end local v2           #count:I
    .end local v9           #prio:I
    .end local v13           #spanEnd:I
    .end local v14           #spanStart:I
    .restart local v3       #count:I
    :cond_b2
    if-nez v3, :cond_bb

    #@b4
    .line 269
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@b7
    move-result-object v16

    #@b8
    check-cast v16, [Ljava/lang/Object;

    #@ba
    .line 282
    :goto_ba
    return-object v16

    #@bb
    .line 271
    :cond_bb
    const/16 v16, 0x1

    #@bd
    move/from16 v0, v16

    #@bf
    if-ne v3, v0, :cond_da

    #@c1
    .line 272
    const/16 v16, 0x1

    #@c3
    move-object/from16 v0, p3

    #@c5
    move/from16 v1, v16

    #@c7
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@ca
    move-result-object v16

    #@cb
    check-cast v16, [Ljava/lang/Object;

    #@cd
    move-object/from16 v10, v16

    #@cf
    check-cast v10, [Ljava/lang/Object;

    #@d1
    .line 273
    const/16 v16, 0x0

    #@d3
    aput-object v11, v10, v16

    #@d5
    move-object/from16 v16, v10

    #@d7
    .line 274
    check-cast v16, [Ljava/lang/Object;

    #@d9
    goto :goto_ba

    #@da
    .line 276
    :cond_da
    array-length v0, v10

    #@db
    move/from16 v16, v0

    #@dd
    move/from16 v0, v16

    #@df
    if-ne v3, v0, :cond_e6

    #@e1
    move-object/from16 v16, v10

    #@e3
    .line 277
    check-cast v16, [Ljava/lang/Object;

    #@e5
    goto :goto_ba

    #@e6
    .line 280
    :cond_e6
    move-object/from16 v0, p3

    #@e8
    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@eb
    move-result-object v16

    #@ec
    check-cast v16, [Ljava/lang/Object;

    #@ee
    move-object/from16 v7, v16

    #@f0
    check-cast v7, [Ljava/lang/Object;

    #@f2
    .line 281
    .local v7, nret:[Ljava/lang/Object;
    const/16 v16, 0x0

    #@f4
    const/16 v17, 0x0

    #@f6
    move/from16 v0, v16

    #@f8
    move/from16 v1, v17

    #@fa
    invoke-static {v10, v0, v7, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@fd
    .line 282
    check-cast v7, [Ljava/lang/Object;

    #@ff
    .end local v7           #nret:[Ljava/lang/Object;
    move-object/from16 v16, v7

    #@101
    goto :goto_ba
.end method

.method public final length()I
    .registers 2

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public nextSpanTransition(IILjava/lang/Class;)I
    .registers 11
    .parameter "start"
    .parameter "limit"
    .parameter "kind"

    #@0
    .prologue
    .line 286
    iget v0, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@2
    .line 287
    .local v0, count:I
    iget-object v4, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@4
    .line 288
    .local v4, spans:[Ljava/lang/Object;
    iget-object v1, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@6
    .line 290
    .local v1, data:[I
    if-nez p3, :cond_a

    #@8
    .line 291
    const-class p3, Ljava/lang/Object;

    #@a
    .line 294
    :cond_a
    const/4 v3, 0x0

    #@b
    .local v3, i:I
    :goto_b
    if-ge v3, v0, :cond_36

    #@d
    .line 295
    mul-int/lit8 v6, v3, 0x3

    #@f
    add-int/lit8 v6, v6, 0x0

    #@11
    aget v5, v1, v6

    #@13
    .line 296
    .local v5, st:I
    mul-int/lit8 v6, v3, 0x3

    #@15
    add-int/lit8 v6, v6, 0x1

    #@17
    aget v2, v1, v6

    #@19
    .line 298
    .local v2, en:I
    if-le v5, p1, :cond_26

    #@1b
    if-ge v5, p2, :cond_26

    #@1d
    aget-object v6, v4, v3

    #@1f
    invoke-virtual {p3, v6}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@22
    move-result v6

    #@23
    if-eqz v6, :cond_26

    #@25
    .line 299
    move p2, v5

    #@26
    .line 300
    :cond_26
    if-le v2, p1, :cond_33

    #@28
    if-ge v2, p2, :cond_33

    #@2a
    aget-object v6, v4, v3

    #@2c
    invoke-virtual {p3, v6}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_33

    #@32
    .line 301
    move p2, v2

    #@33
    .line 294
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_b

    #@36
    .line 304
    .end local v2           #en:I
    .end local v5           #st:I
    :cond_36
    return p2
.end method

.method removeSpan(Ljava/lang/Object;)V
    .registers 12
    .parameter "what"

    #@0
    .prologue
    .line 140
    iget v1, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@2
    .line 141
    .local v1, count:I
    iget-object v6, p0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@4
    .line 142
    .local v6, spans:[Ljava/lang/Object;
    iget-object v2, p0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@6
    .line 144
    .local v2, data:[I
    add-int/lit8 v3, v1, -0x1

    #@8
    .local v3, i:I
    :goto_8
    if-ltz v3, :cond_37

    #@a
    .line 145
    aget-object v7, v6, v3

    #@c
    if-ne v7, p1, :cond_38

    #@e
    .line 146
    mul-int/lit8 v7, v3, 0x3

    #@10
    add-int/lit8 v7, v7, 0x0

    #@12
    aget v5, v2, v7

    #@14
    .line 147
    .local v5, ostart:I
    mul-int/lit8 v7, v3, 0x3

    #@16
    add-int/lit8 v7, v7, 0x1

    #@18
    aget v4, v2, v7

    #@1a
    .line 149
    .local v4, oend:I
    add-int/lit8 v7, v3, 0x1

    #@1c
    sub-int v0, v1, v7

    #@1e
    .line 151
    .local v0, c:I
    add-int/lit8 v7, v3, 0x1

    #@20
    invoke-static {v6, v7, v6, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@23
    .line 152
    add-int/lit8 v7, v3, 0x1

    #@25
    mul-int/lit8 v7, v7, 0x3

    #@27
    mul-int/lit8 v8, v3, 0x3

    #@29
    mul-int/lit8 v9, v0, 0x3

    #@2b
    invoke-static {v2, v7, v2, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@2e
    .line 155
    iget v7, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@30
    add-int/lit8 v7, v7, -0x1

    #@32
    iput v7, p0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@34
    .line 157
    invoke-direct {p0, p1, v5, v4}, Landroid/text/SpannableStringInternal;->sendSpanRemoved(Ljava/lang/Object;II)V

    #@37
    .line 161
    .end local v0           #c:I
    .end local v4           #oend:I
    .end local v5           #ostart:I
    :cond_37
    return-void

    #@38
    .line 144
    :cond_38
    add-int/lit8 v3, v3, -0x1

    #@3a
    goto :goto_8
.end method

.method setSpan(Ljava/lang/Object;III)V
    .registers 24
    .parameter "what"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    .line 74
    move/from16 v7, p2

    #@2
    .line 75
    .local v7, nstart:I
    move/from16 v8, p3

    #@4
    .line 77
    .local v8, nend:I
    const-string/jumbo v3, "setSpan"

    #@7
    move-object/from16 v0, p0

    #@9
    move/from16 v1, p2

    #@b
    move/from16 v2, p3

    #@d
    invoke-direct {v0, v3, v1, v2}, Landroid/text/SpannableStringInternal;->checkRange(Ljava/lang/String;II)V

    #@10
    .line 79
    and-int/lit8 v3, p4, 0x33

    #@12
    const/16 v4, 0x33

    #@14
    if-ne v3, v4, :cond_a4

    #@16
    .line 80
    if-eqz p2, :cond_5d

    #@18
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringInternal;->length()I

    #@1b
    move-result v3

    #@1c
    move/from16 v0, p2

    #@1e
    if-eq v0, v3, :cond_5d

    #@20
    .line 81
    add-int/lit8 v3, p2, -0x1

    #@22
    move-object/from16 v0, p0

    #@24
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringInternal;->charAt(I)C

    #@27
    move-result v9

    #@28
    .line 83
    .local v9, c:C
    const/16 v3, 0xa

    #@2a
    if-eq v9, v3, :cond_5d

    #@2c
    .line 84
    new-instance v3, Ljava/lang/RuntimeException;

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v17, "PARAGRAPH span must start at paragraph boundary ("

    #@35
    move-object/from16 v0, v17

    #@37
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    move/from16 v0, p2

    #@3d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    const-string v17, " follows "

    #@43
    move-object/from16 v0, v17

    #@45
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v17, ")"

    #@4f
    move-object/from16 v0, v17

    #@51
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@5c
    throw v3

    #@5d
    .line 89
    .end local v9           #c:C
    :cond_5d
    if-eqz p3, :cond_a4

    #@5f
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringInternal;->length()I

    #@62
    move-result v3

    #@63
    move/from16 v0, p3

    #@65
    if-eq v0, v3, :cond_a4

    #@67
    .line 90
    add-int/lit8 v3, p3, -0x1

    #@69
    move-object/from16 v0, p0

    #@6b
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringInternal;->charAt(I)C

    #@6e
    move-result v9

    #@6f
    .line 92
    .restart local v9       #c:C
    const/16 v3, 0xa

    #@71
    if-eq v9, v3, :cond_a4

    #@73
    .line 93
    new-instance v3, Ljava/lang/RuntimeException;

    #@75
    new-instance v4, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v17, "PARAGRAPH span must end at paragraph boundary ("

    #@7c
    move-object/from16 v0, v17

    #@7e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    move/from16 v0, p3

    #@84
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v4

    #@88
    const-string v17, " follows "

    #@8a
    move-object/from16 v0, v17

    #@8c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v4

    #@90
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    const-string v17, ")"

    #@96
    move-object/from16 v0, v17

    #@98
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v4

    #@a0
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@a3
    throw v3

    #@a4
    .line 99
    .end local v9           #c:C
    :cond_a4
    move-object/from16 v0, p0

    #@a6
    iget v10, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@a8
    .line 100
    .local v10, count:I
    move-object/from16 v0, p0

    #@aa
    iget-object v0, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@ac
    move-object/from16 v16, v0

    #@ae
    .line 101
    .local v16, spans:[Ljava/lang/Object;
    move-object/from16 v0, p0

    #@b0
    iget-object v11, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@b2
    .line 103
    .local v11, data:[I
    const/4 v12, 0x0

    #@b3
    .local v12, i:I
    :goto_b3
    if-ge v12, v10, :cond_e4

    #@b5
    .line 104
    aget-object v3, v16, v12

    #@b7
    move-object/from16 v0, p1

    #@b9
    if-ne v3, v0, :cond_e1

    #@bb
    .line 105
    mul-int/lit8 v3, v12, 0x3

    #@bd
    add-int/lit8 v3, v3, 0x0

    #@bf
    aget v5, v11, v3

    #@c1
    .line 106
    .local v5, ostart:I
    mul-int/lit8 v3, v12, 0x3

    #@c3
    add-int/lit8 v3, v3, 0x1

    #@c5
    aget v6, v11, v3

    #@c7
    .line 108
    .local v6, oend:I
    mul-int/lit8 v3, v12, 0x3

    #@c9
    add-int/lit8 v3, v3, 0x0

    #@cb
    aput p2, v11, v3

    #@cd
    .line 109
    mul-int/lit8 v3, v12, 0x3

    #@cf
    add-int/lit8 v3, v3, 0x1

    #@d1
    aput p3, v11, v3

    #@d3
    .line 110
    mul-int/lit8 v3, v12, 0x3

    #@d5
    add-int/lit8 v3, v3, 0x2

    #@d7
    aput p4, v11, v3

    #@d9
    move-object/from16 v3, p0

    #@db
    move-object/from16 v4, p1

    #@dd
    .line 112
    invoke-direct/range {v3 .. v8}, Landroid/text/SpannableStringInternal;->sendSpanChanged(Ljava/lang/Object;IIII)V

    #@e0
    .line 137
    .end local v5           #ostart:I
    .end local v6           #oend:I
    :cond_e0
    :goto_e0
    return-void

    #@e1
    .line 103
    :cond_e1
    add-int/lit8 v12, v12, 0x1

    #@e3
    goto :goto_b3

    #@e4
    .line 117
    :cond_e4
    move-object/from16 v0, p0

    #@e6
    iget v3, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@e8
    add-int/lit8 v3, v3, 0x1

    #@ea
    move-object/from16 v0, p0

    #@ec
    iget-object v4, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@ee
    array-length v4, v4

    #@ef
    if-lt v3, v4, :cond_133

    #@f1
    .line 118
    move-object/from16 v0, p0

    #@f3
    iget v3, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@f5
    add-int/lit8 v3, v3, 0x1

    #@f7
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@fa
    move-result v14

    #@fb
    .line 119
    .local v14, newsize:I
    new-array v15, v14, [Ljava/lang/Object;

    #@fd
    .line 120
    .local v15, newtags:[Ljava/lang/Object;
    mul-int/lit8 v3, v14, 0x3

    #@ff
    new-array v13, v3, [I

    #@101
    .line 122
    .local v13, newdata:[I
    move-object/from16 v0, p0

    #@103
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@105
    const/4 v4, 0x0

    #@106
    const/16 v17, 0x0

    #@108
    move-object/from16 v0, p0

    #@10a
    iget v0, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@10c
    move/from16 v18, v0

    #@10e
    move/from16 v0, v17

    #@110
    move/from16 v1, v18

    #@112
    invoke-static {v3, v4, v15, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@115
    .line 123
    move-object/from16 v0, p0

    #@117
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@119
    const/4 v4, 0x0

    #@11a
    const/16 v17, 0x0

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget v0, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@120
    move/from16 v18, v0

    #@122
    mul-int/lit8 v18, v18, 0x3

    #@124
    move/from16 v0, v17

    #@126
    move/from16 v1, v18

    #@128
    invoke-static {v3, v4, v13, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12b
    .line 125
    move-object/from16 v0, p0

    #@12d
    iput-object v15, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@12f
    .line 126
    move-object/from16 v0, p0

    #@131
    iput-object v13, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@133
    .line 129
    .end local v13           #newdata:[I
    .end local v14           #newsize:I
    .end local v15           #newtags:[Ljava/lang/Object;
    :cond_133
    move-object/from16 v0, p0

    #@135
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpans:[Ljava/lang/Object;

    #@137
    move-object/from16 v0, p0

    #@139
    iget v4, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@13b
    aput-object p1, v3, v4

    #@13d
    .line 130
    move-object/from16 v0, p0

    #@13f
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@141
    move-object/from16 v0, p0

    #@143
    iget v4, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@145
    mul-int/lit8 v4, v4, 0x3

    #@147
    add-int/lit8 v4, v4, 0x0

    #@149
    aput p2, v3, v4

    #@14b
    .line 131
    move-object/from16 v0, p0

    #@14d
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@14f
    move-object/from16 v0, p0

    #@151
    iget v4, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@153
    mul-int/lit8 v4, v4, 0x3

    #@155
    add-int/lit8 v4, v4, 0x1

    #@157
    aput p3, v3, v4

    #@159
    .line 132
    move-object/from16 v0, p0

    #@15b
    iget-object v3, v0, Landroid/text/SpannableStringInternal;->mSpanData:[I

    #@15d
    move-object/from16 v0, p0

    #@15f
    iget v4, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@161
    mul-int/lit8 v4, v4, 0x3

    #@163
    add-int/lit8 v4, v4, 0x2

    #@165
    aput p4, v3, v4

    #@167
    .line 133
    move-object/from16 v0, p0

    #@169
    iget v3, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@16b
    add-int/lit8 v3, v3, 0x1

    #@16d
    move-object/from16 v0, p0

    #@16f
    iput v3, v0, Landroid/text/SpannableStringInternal;->mSpanCount:I

    #@171
    .line 135
    move-object/from16 v0, p0

    #@173
    instance-of v3, v0, Landroid/text/Spannable;

    #@175
    if-eqz v3, :cond_e0

    #@177
    .line 136
    move-object/from16 v0, p0

    #@179
    move-object/from16 v1, p1

    #@17b
    invoke-direct {v0, v1, v7, v8}, Landroid/text/SpannableStringInternal;->sendSpanAdded(Ljava/lang/Object;II)V

    #@17e
    goto/16 :goto_e0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 64
    iget-object v0, p0, Landroid/text/SpannableStringInternal;->mText:Ljava/lang/String;

    #@2
    return-object v0
.end method
