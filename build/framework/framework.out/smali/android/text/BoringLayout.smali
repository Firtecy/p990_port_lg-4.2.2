.class public Landroid/text/BoringLayout;
.super Landroid/text/Layout;
.source "BoringLayout.java"

# interfaces
.implements Landroid/text/TextUtils$EllipsizeCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/BoringLayout$Metrics;
    }
.end annotation


# static fields
.field private static final FIRST_RIGHT_TO_LEFT:C = '\u0590'

.field private static final sTemp:Landroid/text/TextPaint;


# instance fields
.field mBottom:I

.field private mBottomPadding:I

.field mDesc:I

.field private mDirect:Ljava/lang/String;

.field private mEllipsizedCount:I

.field private mEllipsizedStart:I

.field private mEllipsizedWidth:I

.field private mMax:F

.field private mPaint:Landroid/graphics/Paint;

.field private mTopPadding:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 439
    new-instance v0, Landroid/text/TextPaint;

    #@2
    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    #@5
    sput-object v0, Landroid/text/BoringLayout;->sTemp:Landroid/text/TextPaint;

    #@7
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)V
    .registers 19
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"

    #@0
    .prologue
    .line 128
    invoke-direct/range {p0 .. p6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@3
    .line 130
    iput p3, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@5
    .line 131
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@8
    .line 132
    const/4 v0, 0x0

    #@9
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@b
    .line 134
    const/4 v9, 0x1

    #@c
    move-object v0, p0

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move v3, p3

    #@10
    move-object v4, p4

    #@11
    move v5, p5

    #@12
    move/from16 v6, p6

    #@14
    move-object/from16 v7, p7

    #@16
    move/from16 v8, p8

    #@18
    invoke-virtual/range {v0 .. v9}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZZ)V

    #@1b
    .line 136
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 22
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 149
    invoke-direct/range {p0 .. p6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@3
    .line 153
    if-eqz p9, :cond_b

    #@5
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@7
    move-object/from16 v0, p9

    #@9
    if-ne v0, v1, :cond_28

    #@b
    .line 154
    :cond_b
    iput p3, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@d
    .line 155
    const/4 v1, 0x0

    #@e
    iput v1, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@10
    .line 156
    const/4 v1, 0x0

    #@11
    iput v1, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@13
    .line 157
    const/4 v10, 0x1

    #@14
    .line 169
    .local v10, trust:Z
    :goto_14
    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    #@17
    move-result-object v2

    #@18
    move-object v1, p0

    #@19
    move-object v3, p2

    #@1a
    move v4, p3

    #@1b
    move-object v5, p4

    #@1c
    move/from16 v6, p5

    #@1e
    move/from16 v7, p6

    #@20
    move-object/from16 v8, p7

    #@22
    move/from16 v9, p8

    #@24
    invoke-virtual/range {v1 .. v10}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZZ)V

    #@27
    .line 171
    return-void

    #@28
    .line 159
    .end local v10           #trust:Z
    :cond_28
    move/from16 v0, p10

    #@2a
    int-to-float v3, v0

    #@2b
    const/4 v5, 0x1

    #@2c
    move-object v1, p1

    #@2d
    move-object v2, p2

    #@2e
    move-object/from16 v4, p9

    #@30
    move-object v6, p0

    #@31
    invoke-static/range {v1 .. v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    #@34
    move-result-object v2

    #@35
    move-object v1, p0

    #@36
    move-object v3, p2

    #@37
    move v4, p3

    #@38
    move-object v5, p4

    #@39
    move/from16 v6, p5

    #@3b
    move/from16 v7, p6

    #@3d
    invoke-virtual/range {v1 .. v7}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@40
    .line 165
    move/from16 v0, p10

    #@42
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@44
    .line 166
    const/4 v10, 0x0

    #@45
    .restart local v10       #trust:Z
    goto :goto_14
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;
    .registers 4
    .parameter "text"
    .parameter "paint"

    #@0
    .prologue
    .line 233
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {p0, p1, v0, v1}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@6
    move-result-object v0

    #@7
    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    .registers 4
    .parameter "text"
    .parameter "paint"
    .parameter "metrics"

    #@0
    .prologue
    .line 252
    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    invoke-static {p0, p1, v0, p2}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;)Landroid/text/BoringLayout$Metrics;
    .registers 4
    .parameter "text"
    .parameter "paint"
    .parameter "textDir"

    #@0
    .prologue
    .line 243
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    .registers 28
    .parameter "text"
    .parameter "paint"
    .parameter "textDir"
    .parameter "metrics"

    #@0
    .prologue
    .line 263
    const/16 v5, 0x1f4

    #@2
    invoke-static {v5}, Landroid/text/TextUtils;->obtain(I)[C

    #@5
    move-result-object v23

    #@6
    .line 264
    .local v23, temp:[C
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    #@9
    move-result v8

    #@a
    .line 265
    .local v8, length:I
    const/4 v14, 0x1

    #@b
    .line 268
    .local v14, boring:Z
    const/16 v18, 0x0

    #@d
    .local v18, i:I
    :goto_d
    move/from16 v0, v18

    #@f
    if-ge v0, v8, :cond_3f

    #@11
    .line 269
    move/from16 v0, v18

    #@13
    add-int/lit16 v0, v0, 0x1f4

    #@15
    move/from16 v19, v0

    #@17
    .line 271
    .local v19, j:I
    move/from16 v0, v19

    #@19
    if-le v0, v8, :cond_1d

    #@1b
    .line 272
    move/from16 v19, v8

    #@1d
    .line 274
    :cond_1d
    const/4 v5, 0x0

    #@1e
    move-object/from16 v0, p0

    #@20
    move/from16 v1, v18

    #@22
    move/from16 v2, v19

    #@24
    move-object/from16 v3, v23

    #@26
    invoke-static {v0, v1, v2, v3, v5}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@29
    .line 276
    sub-int v20, v19, v18

    #@2b
    .line 278
    .local v20, n:I
    const/4 v13, 0x0

    #@2c
    .local v13, a:I
    :goto_2c
    move/from16 v0, v20

    #@2e
    if-ge v13, v0, :cond_c4

    #@30
    .line 279
    aget-char v15, v23, v13

    #@32
    .line 281
    .local v15, c:C
    const/16 v5, 0xa

    #@34
    if-eq v15, v5, :cond_3e

    #@36
    const/16 v5, 0x9

    #@38
    if-eq v15, v5, :cond_3e

    #@3a
    const/16 v5, 0x590

    #@3c
    if-lt v15, v5, :cond_8c

    #@3e
    .line 282
    :cond_3e
    const/4 v14, 0x0

    #@3f
    .line 306
    .end local v13           #a:I
    .end local v15           #c:C
    .end local v19           #j:I
    .end local v20           #n:I
    :cond_3f
    :goto_3f
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->recycle([C)V

    #@42
    .line 308
    if-eqz v14, :cond_5d

    #@44
    move-object/from16 v0, p0

    #@46
    instance-of v5, v0, Landroid/text/Spanned;

    #@48
    if-eqz v5, :cond_5d

    #@4a
    move-object/from16 v21, p0

    #@4c
    .line 309
    check-cast v21, Landroid/text/Spanned;

    #@4e
    .line 310
    .local v21, sp:Landroid/text/Spanned;
    const/4 v5, 0x0

    #@4f
    const-class v6, Landroid/text/style/ParagraphStyle;

    #@51
    move-object/from16 v0, v21

    #@53
    invoke-interface {v0, v5, v8, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@56
    move-result-object v22

    #@57
    .line 311
    .local v22, styles:[Ljava/lang/Object;
    move-object/from16 v0, v22

    #@59
    array-length v5, v0

    #@5a
    if-lez v5, :cond_5d

    #@5c
    .line 312
    const/4 v14, 0x0

    #@5d
    .line 316
    .end local v21           #sp:Landroid/text/Spanned;
    .end local v22           #styles:[Ljava/lang/Object;
    :cond_5d
    if-eqz v14, :cond_de

    #@5f
    .line 317
    move-object/from16 v17, p3

    #@61
    .line 318
    .local v17, fm:Landroid/text/BoringLayout$Metrics;
    if-nez v17, :cond_68

    #@63
    .line 319
    new-instance v17, Landroid/text/BoringLayout$Metrics;

    #@65
    .end local v17           #fm:Landroid/text/BoringLayout$Metrics;
    invoke-direct/range {v17 .. v17}, Landroid/text/BoringLayout$Metrics;-><init>()V

    #@68
    .line 322
    .restart local v17       #fm:Landroid/text/BoringLayout$Metrics;
    :cond_68
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@6b
    move-result-object v4

    #@6c
    .line 323
    .local v4, line:Landroid/text/TextLine;
    const/4 v7, 0x0

    #@6d
    const/4 v9, 0x1

    #@6e
    sget-object v10, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@70
    const/4 v11, 0x0

    #@71
    const/4 v12, 0x0

    #@72
    move-object/from16 v5, p1

    #@74
    move-object/from16 v6, p0

    #@76
    invoke-virtual/range {v4 .. v12}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@79
    .line 325
    move-object/from16 v0, v17

    #@7b
    invoke-virtual {v4, v0}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    #@7e
    move-result v5

    #@7f
    invoke-static {v5}, Landroid/util/FloatMath;->ceil(F)F

    #@82
    move-result v5

    #@83
    float-to-int v5, v5

    #@84
    move-object/from16 v0, v17

    #@86
    iput v5, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@88
    .line 326
    invoke-static {v4}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@8b
    .line 330
    .end local v4           #line:Landroid/text/TextLine;
    .end local v17           #fm:Landroid/text/BoringLayout$Metrics;
    :goto_8b
    return-object v17

    #@8c
    .line 287
    .restart local v13       #a:I
    .restart local v15       #c:C
    .restart local v19       #j:I
    .restart local v20       #n:I
    :cond_8c
    :try_start_8c
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@8e
    if-eqz v5, :cond_c0

    #@90
    sget-object v5, Landroid/text/Layout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@92
    invoke-virtual {v5, v15}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@95
    move-result v5

    #@96
    if-nez v5, :cond_c0

    #@98
    .line 288
    move-object/from16 v0, v23

    #@9a
    invoke-static {v0, v13}, Ljava/lang/Character;->codePointAt([CI)I

    #@9d
    move-result v16

    #@9e
    .line 289
    .local v16, code:I
    invoke-static/range {v16 .. v16}, Landroid/text/Layout;->isInEmojiUnicodeTable(I)Z

    #@a1
    move-result v5

    #@a2
    if-nez v5, :cond_bd

    #@a4
    move/from16 v0, v16

    #@a6
    int-to-char v5, v0

    #@a7
    invoke-static {v5}, Landroid/text/Layout;->isInEmojiUnicodeTable(C)Z

    #@aa
    move-result v5

    #@ab
    if-nez v5, :cond_bd

    #@ad
    move-object/from16 v0, v23

    #@af
    invoke-static {v0, v13}, Landroid/text/Layout;->isDiacriticalMark([CI)Z

    #@b2
    move-result v5

    #@b3
    if-nez v5, :cond_bd

    #@b5
    move-object/from16 v0, v23

    #@b7
    invoke-static {v0, v13}, Landroid/text/Layout;->isInCountryCodeTable([CI)Z
    :try_end_ba
    .catch Ljava/lang/NullPointerException; {:try_start_8c .. :try_end_ba} :catch_bf

    #@ba
    move-result v5

    #@bb
    if-eqz v5, :cond_c0

    #@bd
    .line 291
    :cond_bd
    const/4 v14, 0x0

    #@be
    goto :goto_3f

    #@bf
    .line 295
    .end local v16           #code:I
    :catch_bf
    move-exception v5

    #@c0
    .line 278
    :cond_c0
    add-int/lit8 v13, v13, 0x1

    #@c2
    goto/16 :goto_2c

    #@c4
    .line 300
    .end local v15           #c:C
    :cond_c4
    if-eqz p2, :cond_d6

    #@c6
    const/4 v5, 0x0

    #@c7
    move-object/from16 v0, p2

    #@c9
    move-object/from16 v1, v23

    #@cb
    move/from16 v2, v20

    #@cd
    invoke-interface {v0, v1, v5, v2}, Landroid/text/TextDirectionHeuristic;->isRtl([CII)Z

    #@d0
    move-result v5

    #@d1
    if-eqz v5, :cond_d6

    #@d3
    .line 301
    const/4 v14, 0x0

    #@d4
    .line 302
    goto/16 :goto_3f

    #@d6
    .line 268
    :cond_d6
    move/from16 v0, v18

    #@d8
    add-int/lit16 v0, v0, 0x1f4

    #@da
    move/from16 v18, v0

    #@dc
    goto/16 :goto_d

    #@de
    .line 330
    .end local v13           #a:I
    .end local v19           #j:I
    .end local v20           #n:I
    :cond_de
    const/16 v17, 0x0

    #@e0
    goto :goto_8b
.end method

.method public static make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    .registers 17
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"

    #@0
    .prologue
    .line 48
    new-instance v0, Landroid/text/BoringLayout;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move v3, p2

    #@5
    move-object v4, p3

    #@6
    move v5, p4

    #@7
    move v6, p5

    #@8
    move-object v7, p6

    #@9
    move/from16 v8, p7

    #@b
    invoke-direct/range {v0 .. v8}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)V

    #@e
    return-object v0
.end method

.method public static make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    .registers 21
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 59
    new-instance v0, Landroid/text/BoringLayout;

    #@2
    move-object v1, p0

    #@3
    move-object v2, p1

    #@4
    move v3, p2

    #@5
    move-object v4, p3

    #@6
    move v5, p4

    #@7
    move/from16 v6, p5

    #@9
    move-object/from16 v7, p6

    #@b
    move/from16 v8, p7

    #@d
    move-object/from16 v9, p8

    #@f
    move/from16 v10, p9

    #@11
    invoke-direct/range {v0 .. v10}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)V

    #@14
    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 9
    .parameter "c"
    .parameter "highlight"
    .parameter "highlightpaint"
    .parameter "cursorOffset"

    #@0
    .prologue
    .line 414
    iget-object v0, p0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_15

    #@4
    if-nez p2, :cond_15

    #@6
    .line 415
    iget-object v0, p0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    #@8
    const/4 v1, 0x0

    #@9
    iget v2, p0, Landroid/text/BoringLayout;->mBottom:I

    #@b
    iget v3, p0, Landroid/text/BoringLayout;->mDesc:I

    #@d
    sub-int/2addr v2, v3

    #@e
    int-to-float v2, v2

    #@f
    iget-object v3, p0, Landroid/text/BoringLayout;->mPaint:Landroid/graphics/Paint;

    #@11
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    #@14
    .line 419
    :goto_14
    return-void

    #@15
    .line 417
    :cond_15
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    #@18
    goto :goto_14
.end method

.method public ellipsized(II)V
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 425
    iput p1, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@2
    .line 426
    sub-int v0, p2, p1

    #@4
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@6
    .line 427
    return-void
.end method

.method public getBottomPadding()I
    .registers 2

    #@0
    .prologue
    .line 392
    iget v0, p0, Landroid/text/BoringLayout;->mBottomPadding:I

    #@2
    return v0
.end method

.method public getEllipsisCount(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 397
    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@2
    return v0
.end method

.method public getEllipsisStart(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 402
    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@2
    return v0
.end method

.method public getEllipsizedWidth()I
    .registers 2

    #@0
    .prologue
    .line 407
    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@2
    return v0
.end method

.method public getHeight()I
    .registers 2

    #@0
    .prologue
    .line 336
    iget v0, p0, Landroid/text/BoringLayout;->mBottom:I

    #@2
    return v0
.end method

.method public getLineContainsTab(I)Z
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 372
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getLineCount()I
    .registers 2

    #@0
    .prologue
    .line 341
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getLineDescent(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 354
    iget v0, p0, Landroid/text/BoringLayout;->mDesc:I

    #@2
    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 382
    sget-object v0, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@2
    return-object v0
.end method

.method public getLineMax(I)F
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 377
    iget v0, p0, Landroid/text/BoringLayout;->mMax:F

    #@2
    return v0
.end method

.method public getLineStart(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 359
    if-nez p1, :cond_4

    #@2
    .line 360
    const/4 v0, 0x0

    #@3
    .line 362
    :goto_3
    return v0

    #@4
    :cond_4
    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    #@7
    move-result-object v0

    #@8
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    #@b
    move-result v0

    #@c
    goto :goto_3
.end method

.method public getLineTop(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 346
    if-nez p1, :cond_4

    #@2
    .line 347
    const/4 v0, 0x0

    #@3
    .line 349
    :goto_3
    return v0

    #@4
    :cond_4
    iget v0, p0, Landroid/text/BoringLayout;->mBottom:I

    #@6
    goto :goto_3
.end method

.method public getParagraphDirection(I)I
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 367
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public getTopPadding()I
    .registers 2

    #@0
    .prologue
    .line 387
    iget v0, p0, Landroid/text/BoringLayout;->mTopPadding:I

    #@2
    return v0
.end method

.method init(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZZ)V
    .registers 21
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"
    .parameter "trustWidth"

    #@0
    .prologue
    .line 181
    instance-of v2, p1, Ljava/lang/String;

    #@2
    if-eqz v2, :cond_5d

    #@4
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    #@6
    if-ne p4, v2, :cond_5d

    #@8
    .line 182
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@b
    move-result-object v2

    #@c
    iput-object v2, p0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    #@e
    .line 187
    :goto_e
    iput-object p2, p0, Landroid/text/BoringLayout;->mPaint:Landroid/graphics/Paint;

    #@10
    .line 189
    if-eqz p8, :cond_61

    #@12
    .line 190
    move-object/from16 v0, p7

    #@14
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@16
    move-object/from16 v0, p7

    #@18
    iget v3, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@1a
    sub-int v10, v2, v3

    #@1c
    .line 195
    .local v10, spacing:I
    :goto_1c
    const/high16 v2, 0x3f80

    #@1e
    cmpl-float v2, p5, v2

    #@20
    if-nez v2, :cond_27

    #@22
    const/4 v2, 0x0

    #@23
    cmpl-float v2, p6, v2

    #@25
    if-eqz v2, :cond_30

    #@27
    .line 196
    :cond_27
    int-to-float v2, v10

    #@28
    mul-float v2, v2, p5

    #@2a
    add-float v2, v2, p6

    #@2c
    const/high16 v3, 0x3f00

    #@2e
    add-float/2addr v2, v3

    #@2f
    float-to-int v10, v2

    #@30
    .line 199
    :cond_30
    iput v10, p0, Landroid/text/BoringLayout;->mBottom:I

    #@32
    .line 201
    if-eqz p8, :cond_6c

    #@34
    .line 202
    move-object/from16 v0, p7

    #@36
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@38
    add-int/2addr v2, v10

    #@39
    iput v2, p0, Landroid/text/BoringLayout;->mDesc:I

    #@3b
    .line 207
    :goto_3b
    if-eqz p9, :cond_74

    #@3d
    .line 208
    move-object/from16 v0, p7

    #@3f
    iget v2, v0, Landroid/text/BoringLayout$Metrics;->width:I

    #@41
    int-to-float v2, v2

    #@42
    iput v2, p0, Landroid/text/BoringLayout;->mMax:F

    #@44
    .line 222
    :goto_44
    if-eqz p8, :cond_5c

    #@46
    .line 223
    move-object/from16 v0, p7

    #@48
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@4a
    move-object/from16 v0, p7

    #@4c
    iget v3, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@4e
    sub-int/2addr v2, v3

    #@4f
    iput v2, p0, Landroid/text/BoringLayout;->mTopPadding:I

    #@51
    .line 224
    move-object/from16 v0, p7

    #@53
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@55
    move-object/from16 v0, p7

    #@57
    iget v3, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@59
    sub-int/2addr v2, v3

    #@5a
    iput v2, p0, Landroid/text/BoringLayout;->mBottomPadding:I

    #@5c
    .line 226
    :cond_5c
    return-void

    #@5d
    .line 184
    .end local v10           #spacing:I
    :cond_5d
    const/4 v2, 0x0

    #@5e
    iput-object v2, p0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    #@60
    goto :goto_e

    #@61
    .line 192
    :cond_61
    move-object/from16 v0, p7

    #@63
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@65
    move-object/from16 v0, p7

    #@67
    iget v3, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@69
    sub-int v10, v2, v3

    #@6b
    .restart local v10       #spacing:I
    goto :goto_1c

    #@6c
    .line 204
    :cond_6c
    move-object/from16 v0, p7

    #@6e
    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@70
    add-int/2addr v2, v10

    #@71
    iput v2, p0, Landroid/text/BoringLayout;->mDesc:I

    #@73
    goto :goto_3b

    #@74
    .line 215
    :cond_74
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    #@77
    move-result-object v1

    #@78
    .line 216
    .local v1, line:Landroid/text/TextLine;
    const/4 v4, 0x0

    #@79
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@7c
    move-result v5

    #@7d
    const/4 v6, 0x1

    #@7e
    sget-object v7, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@80
    const/4 v8, 0x0

    #@81
    const/4 v9, 0x0

    #@82
    move-object v2, p2

    #@83
    move-object v3, p1

    #@84
    invoke-virtual/range {v1 .. v9}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;)V

    #@87
    .line 218
    const/4 v2, 0x0

    #@88
    invoke-virtual {v1, v2}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    #@8b
    move-result v2

    #@8c
    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    #@8f
    move-result v2

    #@90
    float-to-int v2, v2

    #@91
    int-to-float v2, v2

    #@92
    iput v2, p0, Landroid/text/BoringLayout;->mMax:F

    #@94
    .line 219
    invoke-static {v1}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    #@97
    goto :goto_44
.end method

.method public replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    .registers 19
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"

    #@0
    .prologue
    .line 74
    invoke-virtual/range {p0 .. p6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@3
    .line 77
    iput p3, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@5
    .line 78
    const/4 v0, 0x0

    #@6
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@8
    .line 79
    const/4 v0, 0x0

    #@9
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@b
    .line 81
    const/4 v9, 0x1

    #@c
    move-object v0, p0

    #@d
    move-object v1, p1

    #@e
    move-object v2, p2

    #@f
    move v3, p3

    #@10
    move-object v4, p4

    #@11
    move v5, p5

    #@12
    move/from16 v6, p6

    #@14
    move-object/from16 v7, p7

    #@16
    move/from16 v8, p8

    #@18
    invoke-virtual/range {v0 .. v9}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZZ)V

    #@1b
    .line 83
    return-object p0
.end method

.method public replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    .registers 22
    .parameter "source"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "metrics"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 100
    if-eqz p9, :cond_8

    #@2
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@4
    move-object/from16 v0, p9

    #@6
    if-ne v0, v1, :cond_28

    #@8
    .line 101
    :cond_8
    invoke-virtual/range {p0 .. p6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@b
    .line 104
    iput p3, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@d
    .line 105
    const/4 v1, 0x0

    #@e
    iput v1, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    #@10
    .line 106
    const/4 v1, 0x0

    #@11
    iput v1, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    #@13
    .line 107
    const/4 v10, 0x1

    #@14
    .line 118
    .local v10, trust:Z
    :goto_14
    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    #@17
    move-result-object v2

    #@18
    move-object v1, p0

    #@19
    move-object v3, p2

    #@1a
    move v4, p3

    #@1b
    move-object v5, p4

    #@1c
    move/from16 v6, p5

    #@1e
    move/from16 v7, p6

    #@20
    move-object/from16 v8, p7

    #@22
    move/from16 v9, p8

    #@24
    invoke-virtual/range {v1 .. v10}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZZ)V

    #@27
    .line 120
    return-object p0

    #@28
    .line 109
    .end local v10           #trust:Z
    :cond_28
    move/from16 v0, p10

    #@2a
    int-to-float v3, v0

    #@2b
    const/4 v5, 0x1

    #@2c
    move-object v1, p1

    #@2d
    move-object v2, p2

    #@2e
    move-object/from16 v4, p9

    #@30
    move-object v6, p0

    #@31
    invoke-static/range {v1 .. v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    #@34
    move-result-object v2

    #@35
    move-object v1, p0

    #@36
    move-object v3, p2

    #@37
    move v4, p3

    #@38
    move-object v5, p4

    #@39
    move/from16 v6, p5

    #@3b
    move/from16 v7, p6

    #@3d
    invoke-virtual/range {v1 .. v7}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@40
    .line 114
    move/from16 v0, p10

    #@42
    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    #@44
    .line 115
    const/4 v10, 0x0

    #@45
    .restart local v10       #trust:Z
    goto :goto_14
.end method
