.class public Landroid/text/DynamicLayout;
.super Landroid/text/Layout;
.source "DynamicLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/DynamicLayout$ChangeWatcher;
    }
.end annotation


# static fields
.field private static final BLOCK_MINIMUM_CHARACTER_LENGTH:I = 0x190

.field private static final COLUMNS_ELLIPSIZE:I = 0x5

.field private static final COLUMNS_NORMAL:I = 0x3

.field private static final DESCENT:I = 0x2

.field private static final DIR:I = 0x0

.field private static final DIR_SHIFT:I = 0x1e

.field private static final ELLIPSIS_COUNT:I = 0x4

.field private static final ELLIPSIS_START:I = 0x3

.field private static final ELLIPSIS_UNDEFINED:I = -0x80000000

.field public static final INVALID_BLOCK_INDEX:I = -0x1

.field private static final PRIORITY:I = 0x80

.field private static final START:I = 0x0

.field private static final START_MASK:I = 0x1fffffff

.field private static final TAB:I = 0x0

.field private static final TAB_MASK:I = 0x20000000

.field private static final TOP:I = 0x1

.field private static final sLock:[Ljava/lang/Object;

.field private static sStaticLayout:Landroid/text/StaticLayout;


# instance fields
.field private mBase:Ljava/lang/CharSequence;

.field private mBlockEndLines:[I

.field private mBlockIndices:[I

.field private mBottomPadding:I

.field private mDisplay:Ljava/lang/CharSequence;

.field private mEllipsize:Z

.field private mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

.field private mEllipsizedWidth:I

.field private mIncludePad:Z

.field private mInts:Landroid/text/PackedIntVector;

.field private mNumberOfBlocks:I

.field private mObjects:Landroid/text/PackedObjectVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/PackedObjectVector",
            "<",
            "Landroid/text/Layout$Directions;",
            ">;"
        }
    .end annotation
.end field

.field private mTopPadding:I

.field private mWatcher:Landroid/text/DynamicLayout$ChangeWatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 703
    new-instance v0, Landroid/text/StaticLayout;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-direct {v0, v1}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;)V

    #@6
    sput-object v0, Landroid/text/DynamicLayout;->sStaticLayout:Landroid/text/StaticLayout;

    #@8
    .line 705
    const/4 v0, 0x0

    #@9
    new-array v0, v0, [Ljava/lang/Object;

    #@b
    sput-object v0, Landroid/text/DynamicLayout;->sLock:[Ljava/lang/Object;

    #@d
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 17
    .parameter "base"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 49
    move-object v0, p0

    #@1
    move-object v1, p1

    #@2
    move-object v2, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move-object v5, p4

    #@6
    move v6, p5

    #@7
    move v7, p6

    #@8
    move/from16 v8, p7

    #@a
    invoke-direct/range {v0 .. v8}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    #@d
    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 20
    .parameter "base"
    .parameter "display"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 63
    const/4 v9, 0x0

    #@1
    const/4 v10, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move v4, p4

    #@7
    move-object/from16 v5, p5

    #@9
    move/from16 v6, p6

    #@b
    move/from16 v7, p7

    #@d
    move/from16 v8, p8

    #@f
    invoke-direct/range {v0 .. v10}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    #@12
    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 23
    .parameter "base"
    .parameter "display"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 80
    sget-object v6, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move-object v2, p2

    #@5
    move-object v3, p3

    #@6
    move/from16 v4, p4

    #@8
    move-object/from16 v5, p5

    #@a
    move/from16 v7, p6

    #@c
    move/from16 v8, p7

    #@e
    move/from16 v9, p8

    #@10
    move-object/from16 v10, p9

    #@12
    move/from16 v11, p10

    #@14
    invoke-direct/range {v0 .. v11}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;I)V

    #@17
    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 31
    .parameter "base"
    .parameter "display"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "textDir"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 99
    if-nez p10, :cond_f2

    #@2
    move-object/from16 v3, p2

    #@4
    :goto_4
    move-object/from16 v2, p0

    #@6
    move-object/from16 v4, p3

    #@8
    move/from16 v5, p4

    #@a
    move-object/from16 v6, p5

    #@c
    move-object/from16 v7, p6

    #@e
    move/from16 v8, p7

    #@10
    move/from16 v9, p8

    #@12
    invoke-direct/range {v2 .. v9}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V

    #@15
    .line 106
    move-object/from16 v0, p1

    #@17
    move-object/from16 v1, p0

    #@19
    iput-object v0, v1, Landroid/text/DynamicLayout;->mBase:Ljava/lang/CharSequence;

    #@1b
    .line 107
    move-object/from16 v0, p2

    #@1d
    move-object/from16 v1, p0

    #@1f
    iput-object v0, v1, Landroid/text/DynamicLayout;->mDisplay:Ljava/lang/CharSequence;

    #@21
    .line 109
    if-eqz p10, :cond_10a

    #@23
    .line 110
    new-instance v2, Landroid/text/PackedIntVector;

    #@25
    const/4 v3, 0x5

    #@26
    invoke-direct {v2, v3}, Landroid/text/PackedIntVector;-><init>(I)V

    #@29
    move-object/from16 v0, p0

    #@2b
    iput-object v2, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2d
    .line 111
    move/from16 v0, p11

    #@2f
    move-object/from16 v1, p0

    #@31
    iput v0, v1, Landroid/text/DynamicLayout;->mEllipsizedWidth:I

    #@33
    .line 112
    move-object/from16 v0, p10

    #@35
    move-object/from16 v1, p0

    #@37
    iput-object v0, v1, Landroid/text/DynamicLayout;->mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

    #@39
    .line 119
    :goto_39
    new-instance v2, Landroid/text/PackedObjectVector;

    #@3b
    const/4 v3, 0x1

    #@3c
    invoke-direct {v2, v3}, Landroid/text/PackedObjectVector;-><init>(I)V

    #@3f
    move-object/from16 v0, p0

    #@41
    iput-object v2, v0, Landroid/text/DynamicLayout;->mObjects:Landroid/text/PackedObjectVector;

    #@43
    .line 121
    move/from16 v0, p9

    #@45
    move-object/from16 v1, p0

    #@47
    iput-boolean v0, v1, Landroid/text/DynamicLayout;->mIncludePad:Z

    #@49
    .line 131
    if-eqz p10, :cond_62

    #@4b
    .line 132
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getText()Ljava/lang/CharSequence;

    #@4e
    move-result-object v13

    #@4f
    check-cast v13, Landroid/text/Layout$Ellipsizer;

    #@51
    .line 134
    .local v13, e:Landroid/text/Layout$Ellipsizer;
    move-object/from16 v0, p0

    #@53
    iput-object v0, v13, Landroid/text/Layout$Ellipsizer;->mLayout:Landroid/text/Layout;

    #@55
    .line 135
    move/from16 v0, p11

    #@57
    iput v0, v13, Landroid/text/Layout$Ellipsizer;->mWidth:I

    #@59
    .line 136
    move-object/from16 v0, p10

    #@5b
    iput-object v0, v13, Landroid/text/Layout$Ellipsizer;->mMethod:Landroid/text/TextUtils$TruncateAt;

    #@5d
    .line 137
    const/4 v2, 0x1

    #@5e
    move-object/from16 v0, p0

    #@60
    iput-boolean v2, v0, Landroid/text/DynamicLayout;->mEllipsize:Z

    #@62
    .line 146
    .end local v13           #e:Landroid/text/Layout$Ellipsizer;
    :cond_62
    if-eqz p10, :cond_121

    #@64
    .line 147
    const/4 v2, 0x5

    #@65
    new-array v0, v2, [I

    #@67
    move-object/from16 v18, v0

    #@69
    .line 148
    .local v18, start:[I
    const/4 v2, 0x3

    #@6a
    const/high16 v3, -0x8000

    #@6c
    aput v3, v18, v2

    #@6e
    .line 153
    :goto_6e
    const/4 v2, 0x1

    #@6f
    new-array v12, v2, [Landroid/text/Layout$Directions;

    #@71
    const/4 v2, 0x0

    #@72
    sget-object v3, Landroid/text/DynamicLayout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@74
    aput-object v3, v12, v2

    #@76
    .line 155
    .local v12, dirs:[Landroid/text/Layout$Directions;
    invoke-virtual/range {p3 .. p3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    #@79
    move-result-object v14

    #@7a
    .line 156
    .local v14, fm:Landroid/graphics/Paint$FontMetricsInt;
    iget v10, v14, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@7c
    .line 157
    .local v10, asc:I
    iget v11, v14, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@7e
    .line 159
    .local v11, desc:I
    const/4 v2, 0x0

    #@7f
    const/high16 v3, 0x4000

    #@81
    aput v3, v18, v2

    #@83
    .line 160
    const/4 v2, 0x1

    #@84
    const/4 v3, 0x0

    #@85
    aput v3, v18, v2

    #@87
    .line 161
    const/4 v2, 0x2

    #@88
    aput v11, v18, v2

    #@8a
    .line 162
    move-object/from16 v0, p0

    #@8c
    iget-object v2, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@8e
    const/4 v3, 0x0

    #@8f
    move-object/from16 v0, v18

    #@91
    invoke-virtual {v2, v3, v0}, Landroid/text/PackedIntVector;->insertAt(I[I)V

    #@94
    .line 164
    const/4 v2, 0x1

    #@95
    sub-int v3, v11, v10

    #@97
    aput v3, v18, v2

    #@99
    .line 165
    move-object/from16 v0, p0

    #@9b
    iget-object v2, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@9d
    const/4 v3, 0x1

    #@9e
    move-object/from16 v0, v18

    #@a0
    invoke-virtual {v2, v3, v0}, Landroid/text/PackedIntVector;->insertAt(I[I)V

    #@a3
    .line 167
    move-object/from16 v0, p0

    #@a5
    iget-object v2, v0, Landroid/text/DynamicLayout;->mObjects:Landroid/text/PackedObjectVector;

    #@a7
    const/4 v3, 0x0

    #@a8
    invoke-virtual {v2, v3, v12}, Landroid/text/PackedObjectVector;->insertAt(I[Ljava/lang/Object;)V

    #@ab
    .line 170
    const/4 v2, 0x0

    #@ac
    const/4 v3, 0x0

    #@ad
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@b0
    move-result v4

    #@b1
    move-object/from16 v0, p0

    #@b3
    move-object/from16 v1, p1

    #@b5
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/text/DynamicLayout;->reflow(Ljava/lang/CharSequence;III)V

    #@b8
    .line 172
    move-object/from16 v0, p1

    #@ba
    instance-of v2, v0, Landroid/text/Spannable;

    #@bc
    if-eqz v2, :cond_139

    #@be
    .line 173
    move-object/from16 v0, p0

    #@c0
    iget-object v2, v0, Landroid/text/DynamicLayout;->mWatcher:Landroid/text/DynamicLayout$ChangeWatcher;

    #@c2
    if-nez v2, :cond_cf

    #@c4
    .line 174
    new-instance v2, Landroid/text/DynamicLayout$ChangeWatcher;

    #@c6
    move-object/from16 v0, p0

    #@c8
    invoke-direct {v2, v0}, Landroid/text/DynamicLayout$ChangeWatcher;-><init>(Landroid/text/DynamicLayout;)V

    #@cb
    move-object/from16 v0, p0

    #@cd
    iput-object v2, v0, Landroid/text/DynamicLayout;->mWatcher:Landroid/text/DynamicLayout$ChangeWatcher;

    #@cf
    :cond_cf
    move-object/from16 v16, p1

    #@d1
    .line 177
    check-cast v16, Landroid/text/Spannable;

    #@d3
    .line 178
    .local v16, sp:Landroid/text/Spannable;
    const/4 v2, 0x0

    #@d4
    invoke-interface/range {v16 .. v16}, Landroid/text/Spannable;->length()I

    #@d7
    move-result v3

    #@d8
    const-class v4, Landroid/text/DynamicLayout$ChangeWatcher;

    #@da
    move-object/from16 v0, v16

    #@dc
    invoke-interface {v0, v2, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@df
    move-result-object v17

    #@e0
    check-cast v17, [Landroid/text/DynamicLayout$ChangeWatcher;

    #@e2
    .line 179
    .local v17, spans:[Landroid/text/DynamicLayout$ChangeWatcher;
    const/4 v15, 0x0

    #@e3
    .local v15, i:I
    :goto_e3
    move-object/from16 v0, v17

    #@e5
    array-length v2, v0

    #@e6
    if-ge v15, v2, :cond_128

    #@e8
    .line 180
    aget-object v2, v17, v15

    #@ea
    move-object/from16 v0, v16

    #@ec
    invoke-interface {v0, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    #@ef
    .line 179
    add-int/lit8 v15, v15, 0x1

    #@f1
    goto :goto_e3

    #@f2
    .line 99
    .end local v10           #asc:I
    .end local v11           #desc:I
    .end local v12           #dirs:[Landroid/text/Layout$Directions;
    .end local v14           #fm:Landroid/graphics/Paint$FontMetricsInt;
    .end local v15           #i:I
    .end local v16           #sp:Landroid/text/Spannable;
    .end local v17           #spans:[Landroid/text/DynamicLayout$ChangeWatcher;
    .end local v18           #start:[I
    :cond_f2
    move-object/from16 v0, p2

    #@f4
    instance-of v2, v0, Landroid/text/Spanned;

    #@f6
    if-eqz v2, :cond_101

    #@f8
    new-instance v3, Landroid/text/Layout$SpannedEllipsizer;

    #@fa
    move-object/from16 v0, p2

    #@fc
    invoke-direct {v3, v0}, Landroid/text/Layout$SpannedEllipsizer;-><init>(Ljava/lang/CharSequence;)V

    #@ff
    goto/16 :goto_4

    #@101
    :cond_101
    new-instance v3, Landroid/text/Layout$Ellipsizer;

    #@103
    move-object/from16 v0, p2

    #@105
    invoke-direct {v3, v0}, Landroid/text/Layout$Ellipsizer;-><init>(Ljava/lang/CharSequence;)V

    #@108
    goto/16 :goto_4

    #@10a
    .line 114
    :cond_10a
    new-instance v2, Landroid/text/PackedIntVector;

    #@10c
    const/4 v3, 0x3

    #@10d
    invoke-direct {v2, v3}, Landroid/text/PackedIntVector;-><init>(I)V

    #@110
    move-object/from16 v0, p0

    #@112
    iput-object v2, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@114
    .line 115
    move/from16 v0, p4

    #@116
    move-object/from16 v1, p0

    #@118
    iput v0, v1, Landroid/text/DynamicLayout;->mEllipsizedWidth:I

    #@11a
    .line 116
    const/4 v2, 0x0

    #@11b
    move-object/from16 v0, p0

    #@11d
    iput-object v2, v0, Landroid/text/DynamicLayout;->mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

    #@11f
    goto/16 :goto_39

    #@121
    .line 150
    :cond_121
    const/4 v2, 0x3

    #@122
    new-array v0, v2, [I

    #@124
    move-object/from16 v18, v0

    #@126
    .restart local v18       #start:[I
    goto/16 :goto_6e

    #@128
    .line 182
    .restart local v10       #asc:I
    .restart local v11       #desc:I
    .restart local v12       #dirs:[Landroid/text/Layout$Directions;
    .restart local v14       #fm:Landroid/graphics/Paint$FontMetricsInt;
    .restart local v15       #i:I
    .restart local v16       #sp:Landroid/text/Spannable;
    .restart local v17       #spans:[Landroid/text/DynamicLayout$ChangeWatcher;
    :cond_128
    move-object/from16 v0, p0

    #@12a
    iget-object v2, v0, Landroid/text/DynamicLayout;->mWatcher:Landroid/text/DynamicLayout$ChangeWatcher;

    #@12c
    const/4 v3, 0x0

    #@12d
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    #@130
    move-result v4

    #@131
    const v5, 0x800012

    #@134
    move-object/from16 v0, v16

    #@136
    invoke-interface {v0, v2, v3, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    #@139
    .line 186
    .end local v15           #i:I
    .end local v16           #sp:Landroid/text/Spannable;
    .end local v17           #spans:[Landroid/text/DynamicLayout$ChangeWatcher;
    :cond_139
    return-void
.end method

.method static synthetic access$000(Landroid/text/DynamicLayout;Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/text/DynamicLayout;->reflow(Ljava/lang/CharSequence;III)V

    #@3
    return-void
.end method

.method private addBlockAtOffset(I)V
    .registers 8
    .parameter "offset"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 400
    invoke-virtual {p0, p1}, Landroid/text/DynamicLayout;->getLineForOffset(I)I

    #@4
    move-result v1

    #@5
    .line 402
    .local v1, line:I
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@7
    if-nez v3, :cond_1f

    #@9
    .line 404
    const/4 v3, 0x1

    #@a
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@d
    move-result v3

    #@e
    new-array v3, v3, [I

    #@10
    iput-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@12
    .line 405
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@14
    iget v4, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@16
    aput v1, v3, v4

    #@18
    .line 406
    iget v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@1a
    add-int/lit8 v3, v3, 0x1

    #@1c
    iput v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@1e
    .line 421
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 410
    :cond_1f
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@21
    iget v4, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@23
    add-int/lit8 v4, v4, -0x1

    #@25
    aget v2, v3, v4

    #@27
    .line 411
    .local v2, previousBlockEndLine:I
    if-le v1, v2, :cond_1e

    #@29
    .line 412
    iget v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@2b
    iget-object v4, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@2d
    array-length v4, v4

    #@2e
    if-ne v3, v4, :cond_43

    #@30
    .line 414
    iget v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@32
    add-int/lit8 v3, v3, 0x1

    #@34
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@37
    move-result v3

    #@38
    new-array v0, v3, [I

    #@3a
    .line 415
    .local v0, blockEndLines:[I
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@3c
    iget v4, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@3e
    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@41
    .line 416
    iput-object v0, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@43
    .line 418
    .end local v0           #blockEndLines:[I
    :cond_43
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@45
    iget v4, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@47
    aput v1, v3, v4

    #@49
    .line 419
    iget v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@4b
    add-int/lit8 v3, v3, 0x1

    #@4d
    iput v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@4f
    goto :goto_1e
.end method

.method private createBlocks()V
    .registers 6

    #@0
    .prologue
    .line 372
    const/16 v1, 0x190

    #@2
    .line 373
    .local v1, offset:I
    const/4 v3, 0x0

    #@3
    iput v3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@5
    .line 374
    iget-object v2, p0, Landroid/text/DynamicLayout;->mDisplay:Ljava/lang/CharSequence;

    #@7
    .line 377
    .local v2, text:Ljava/lang/CharSequence;
    :goto_7
    const/16 v3, 0xa

    #@9
    invoke-static {v2, v3, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CI)I

    #@c
    move-result v1

    #@d
    .line 378
    if-gez v1, :cond_2b

    #@f
    .line 379
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    #@12
    move-result v3

    #@13
    invoke-direct {p0, v3}, Landroid/text/DynamicLayout;->addBlockAtOffset(I)V

    #@16
    .line 388
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@18
    array-length v3, v3

    #@19
    new-array v3, v3, [I

    #@1b
    iput-object v3, p0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@1d
    .line 389
    const/4 v0, 0x0

    #@1e
    .local v0, i:I
    :goto_1e
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@20
    array-length v3, v3

    #@21
    if-ge v0, v3, :cond_31

    #@23
    .line 390
    iget-object v3, p0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@25
    const/4 v4, -0x1

    #@26
    aput v4, v3, v0

    #@28
    .line 389
    add-int/lit8 v0, v0, 0x1

    #@2a
    goto :goto_1e

    #@2b
    .line 382
    .end local v0           #i:I
    :cond_2b
    invoke-direct {p0, v1}, Landroid/text/DynamicLayout;->addBlockAtOffset(I)V

    #@2e
    .line 383
    add-int/lit16 v1, v1, 0x190

    #@30
    goto :goto_7

    #@31
    .line 392
    .restart local v0       #i:I
    :cond_31
    return-void
.end method

.method private reflow(Ljava/lang/CharSequence;III)V
    .registers 45
    .parameter "s"
    .parameter "where"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 189
    move-object/from16 v0, p0

    #@2
    iget-object v5, v0, Landroid/text/DynamicLayout;->mBase:Ljava/lang/CharSequence;

    #@4
    move-object/from16 v0, p1

    #@6
    if-eq v0, v5, :cond_9

    #@8
    .line 365
    :goto_8
    return-void

    #@9
    .line 192
    :cond_9
    move-object/from16 v0, p0

    #@b
    iget-object v4, v0, Landroid/text/DynamicLayout;->mDisplay:Ljava/lang/CharSequence;

    #@d
    .line 193
    .local v4, text:Ljava/lang/CharSequence;
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    #@10
    move-result v30

    #@11
    .line 197
    .local v30, len:I
    const/16 v5, 0xa

    #@13
    add-int/lit8 v6, p2, -0x1

    #@15
    invoke-static {v4, v5, v6}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CI)I

    #@18
    move-result v24

    #@19
    .line 198
    .local v24, find:I
    if-gez v24, :cond_8b

    #@1b
    .line 199
    const/16 v24, 0x0

    #@1d
    .line 204
    :goto_1d
    sub-int v20, p2, v24

    #@1f
    .line 205
    .local v20, diff:I
    add-int p3, p3, v20

    #@21
    .line 206
    add-int p4, p4, v20

    #@23
    .line 207
    sub-int p2, p2, v20

    #@25
    .line 212
    const/16 v5, 0xa

    #@27
    add-int v6, p2, p4

    #@29
    invoke-static {v4, v5, v6}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CI)I

    #@2c
    move-result v31

    #@2d
    .line 213
    .local v31, look:I
    if-gez v31, :cond_8e

    #@2f
    .line 214
    move/from16 v31, v30

    #@31
    .line 218
    :goto_31
    add-int v5, p2, p4

    #@33
    sub-int v18, v31, v5

    #@35
    .line 219
    .local v18, change:I
    add-int p3, p3, v18

    #@37
    .line 220
    add-int p4, p4, v18

    #@39
    .line 224
    instance-of v5, v4, Landroid/text/Spanned;

    #@3b
    if-eqz v5, :cond_93

    #@3d
    move-object/from16 v34, v4

    #@3f
    .line 225
    check-cast v34, Landroid/text/Spanned;

    #@41
    .line 229
    .local v34, sp:Landroid/text/Spanned;
    :cond_41
    const/16 v16, 0x0

    #@43
    .line 231
    .local v16, again:Z
    add-int v5, p2, p4

    #@45
    const-class v6, Landroid/text/style/WrapTogetherSpan;

    #@47
    move-object/from16 v0, v34

    #@49
    move/from16 v1, p2

    #@4b
    invoke-interface {v0, v1, v5, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@4e
    move-result-object v25

    #@4f
    .line 234
    .local v25, force:[Ljava/lang/Object;
    const/16 v27, 0x0

    #@51
    .local v27, i:I
    :goto_51
    move-object/from16 v0, v25

    #@53
    array-length v5, v0

    #@54
    move/from16 v0, v27

    #@56
    if-ge v0, v5, :cond_91

    #@58
    .line 235
    aget-object v5, v25, v27

    #@5a
    move-object/from16 v0, v34

    #@5c
    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@5f
    move-result v35

    #@60
    .line 236
    .local v35, st:I
    aget-object v5, v25, v27

    #@62
    move-object/from16 v0, v34

    #@64
    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@67
    move-result v21

    #@68
    .line 238
    .local v21, en:I
    move/from16 v0, v35

    #@6a
    move/from16 v1, p2

    #@6c
    if-ge v0, v1, :cond_78

    #@6e
    .line 239
    const/16 v16, 0x1

    #@70
    .line 241
    sub-int v20, p2, v35

    #@72
    .line 242
    add-int p3, p3, v20

    #@74
    .line 243
    add-int p4, p4, v20

    #@76
    .line 244
    sub-int p2, p2, v20

    #@78
    .line 247
    :cond_78
    add-int v5, p2, p4

    #@7a
    move/from16 v0, v21

    #@7c
    if-le v0, v5, :cond_88

    #@7e
    .line 248
    const/16 v16, 0x1

    #@80
    .line 250
    add-int v5, p2, p4

    #@82
    sub-int v20, v21, v5

    #@84
    .line 251
    add-int p3, p3, v20

    #@86
    .line 252
    add-int p4, p4, v20

    #@88
    .line 234
    :cond_88
    add-int/lit8 v27, v27, 0x1

    #@8a
    goto :goto_51

    #@8b
    .line 201
    .end local v16           #again:Z
    .end local v18           #change:I
    .end local v20           #diff:I
    .end local v21           #en:I
    .end local v25           #force:[Ljava/lang/Object;
    .end local v27           #i:I
    .end local v31           #look:I
    .end local v34           #sp:Landroid/text/Spanned;
    .end local v35           #st:I
    :cond_8b
    add-int/lit8 v24, v24, 0x1

    #@8d
    goto :goto_1d

    #@8e
    .line 216
    .restart local v20       #diff:I
    .restart local v31       #look:I
    :cond_8e
    add-int/lit8 v31, v31, 0x1

    #@90
    goto :goto_31

    #@91
    .line 255
    .restart local v16       #again:Z
    .restart local v18       #change:I
    .restart local v25       #force:[Ljava/lang/Object;
    .restart local v27       #i:I
    .restart local v34       #sp:Landroid/text/Spanned;
    :cond_91
    if-nez v16, :cond_41

    #@93
    .line 260
    .end local v16           #again:Z
    .end local v25           #force:[Ljava/lang/Object;
    .end local v27           #i:I
    .end local v34           #sp:Landroid/text/Spanned;
    :cond_93
    move-object/from16 v0, p0

    #@95
    move/from16 v1, p2

    #@97
    invoke-virtual {v0, v1}, Landroid/text/DynamicLayout;->getLineForOffset(I)I

    #@9a
    move-result v36

    #@9b
    .line 261
    .local v36, startline:I
    move-object/from16 v0, p0

    #@9d
    move/from16 v1, v36

    #@9f
    invoke-virtual {v0, v1}, Landroid/text/DynamicLayout;->getLineTop(I)I

    #@a2
    move-result v37

    #@a3
    .line 263
    .local v37, startv:I
    add-int v5, p2, p3

    #@a5
    move-object/from16 v0, p0

    #@a7
    invoke-virtual {v0, v5}, Landroid/text/DynamicLayout;->getLineForOffset(I)I

    #@aa
    move-result v22

    #@ab
    .line 264
    .local v22, endline:I
    add-int v5, p2, p4

    #@ad
    move/from16 v0, v30

    #@af
    if-ne v5, v0, :cond_b5

    #@b1
    .line 265
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getLineCount()I

    #@b4
    move-result v22

    #@b5
    .line 266
    :cond_b5
    move-object/from16 v0, p0

    #@b7
    move/from16 v1, v22

    #@b9
    invoke-virtual {v0, v1}, Landroid/text/DynamicLayout;->getLineTop(I)I

    #@bc
    move-result v23

    #@bd
    .line 267
    .local v23, endv:I
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getLineCount()I

    #@c0
    move-result v5

    #@c1
    move/from16 v0, v22

    #@c3
    if-ne v0, v5, :cond_20a

    #@c5
    const/16 v29, 0x1

    #@c7
    .line 273
    .local v29, islast:Z
    :goto_c7
    sget-object v6, Landroid/text/DynamicLayout;->sLock:[Ljava/lang/Object;

    #@c9
    monitor-enter v6

    #@ca
    .line 274
    :try_start_ca
    sget-object v3, Landroid/text/DynamicLayout;->sStaticLayout:Landroid/text/StaticLayout;

    #@cc
    .line 275
    .local v3, reflowed:Landroid/text/StaticLayout;
    const/4 v5, 0x0

    #@cd
    sput-object v5, Landroid/text/DynamicLayout;->sStaticLayout:Landroid/text/StaticLayout;

    #@cf
    .line 276
    monitor-exit v6
    :try_end_d0
    .catchall {:try_start_ca .. :try_end_d0} :catchall_20e

    #@d0
    .line 278
    if-nez v3, :cond_211

    #@d2
    .line 279
    new-instance v3, Landroid/text/StaticLayout;

    #@d4
    .end local v3           #reflowed:Landroid/text/StaticLayout;
    const/4 v5, 0x0

    #@d5
    invoke-direct {v3, v5}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;)V

    #@d8
    .line 284
    .restart local v3       #reflowed:Landroid/text/StaticLayout;
    :goto_d8
    add-int v6, p2, p4

    #@da
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getPaint()Landroid/text/TextPaint;

    #@dd
    move-result-object v7

    #@de
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getWidth()I

    #@e1
    move-result v8

    #@e2
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getTextDirectionHeuristic()Landroid/text/TextDirectionHeuristic;

    #@e5
    move-result-object v9

    #@e6
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getSpacingMultiplier()F

    #@e9
    move-result v10

    #@ea
    invoke-virtual/range {p0 .. p0}, Landroid/text/DynamicLayout;->getSpacingAdd()F

    #@ed
    move-result v11

    #@ee
    const/4 v12, 0x0

    #@ef
    const/4 v13, 0x1

    #@f0
    move-object/from16 v0, p0

    #@f2
    iget v5, v0, Landroid/text/DynamicLayout;->mEllipsizedWidth:I

    #@f4
    int-to-float v14, v5

    #@f5
    move-object/from16 v0, p0

    #@f7
    iget-object v15, v0, Landroid/text/DynamicLayout;->mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

    #@f9
    move/from16 v5, p2

    #@fb
    invoke-virtual/range {v3 .. v15}, Landroid/text/StaticLayout;->generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/TextDirectionHeuristic;FFZZFLandroid/text/TextUtils$TruncateAt;)V

    #@fe
    .line 288
    invoke-virtual {v3}, Landroid/text/StaticLayout;->getLineCount()I

    #@101
    move-result v32

    #@102
    .line 294
    .local v32, n:I
    add-int v5, p2, p4

    #@104
    move/from16 v0, v30

    #@106
    if-eq v5, v0, :cond_114

    #@108
    add-int/lit8 v5, v32, -0x1

    #@10a
    invoke-virtual {v3, v5}, Landroid/text/StaticLayout;->getLineStart(I)I

    #@10d
    move-result v5

    #@10e
    add-int v6, p2, p4

    #@110
    if-ne v5, v6, :cond_114

    #@112
    .line 295
    add-int/lit8 v32, v32, -0x1

    #@114
    .line 298
    :cond_114
    move-object/from16 v0, p0

    #@116
    iget-object v5, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@118
    sub-int v6, v22, v36

    #@11a
    move/from16 v0, v36

    #@11c
    invoke-virtual {v5, v0, v6}, Landroid/text/PackedIntVector;->deleteAt(II)V

    #@11f
    .line 299
    move-object/from16 v0, p0

    #@121
    iget-object v5, v0, Landroid/text/DynamicLayout;->mObjects:Landroid/text/PackedObjectVector;

    #@123
    sub-int v6, v22, v36

    #@125
    move/from16 v0, v36

    #@127
    invoke-virtual {v5, v0, v6}, Landroid/text/PackedObjectVector;->deleteAt(II)V

    #@12a
    .line 303
    move/from16 v0, v32

    #@12c
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineTop(I)I

    #@12f
    move-result v26

    #@130
    .line 304
    .local v26, ht:I
    const/16 v39, 0x0

    #@132
    .local v39, toppad:I
    const/16 v17, 0x0

    #@134
    .line 306
    .local v17, botpad:I
    move-object/from16 v0, p0

    #@136
    iget-boolean v5, v0, Landroid/text/DynamicLayout;->mIncludePad:Z

    #@138
    if-eqz v5, :cond_148

    #@13a
    if-nez v36, :cond_148

    #@13c
    .line 307
    invoke-virtual {v3}, Landroid/text/StaticLayout;->getTopPadding()I

    #@13f
    move-result v39

    #@140
    .line 308
    move/from16 v0, v39

    #@142
    move-object/from16 v1, p0

    #@144
    iput v0, v1, Landroid/text/DynamicLayout;->mTopPadding:I

    #@146
    .line 309
    sub-int v26, v26, v39

    #@148
    .line 311
    :cond_148
    move-object/from16 v0, p0

    #@14a
    iget-boolean v5, v0, Landroid/text/DynamicLayout;->mIncludePad:Z

    #@14c
    if-eqz v5, :cond_15c

    #@14e
    if-eqz v29, :cond_15c

    #@150
    .line 312
    invoke-virtual {v3}, Landroid/text/StaticLayout;->getBottomPadding()I

    #@153
    move-result v17

    #@154
    .line 313
    move/from16 v0, v17

    #@156
    move-object/from16 v1, p0

    #@158
    iput v0, v1, Landroid/text/DynamicLayout;->mBottomPadding:I

    #@15a
    .line 314
    add-int v26, v26, v17

    #@15c
    .line 317
    :cond_15c
    move-object/from16 v0, p0

    #@15e
    iget-object v5, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@160
    const/4 v6, 0x0

    #@161
    sub-int v7, p4, p3

    #@163
    move/from16 v0, v36

    #@165
    invoke-virtual {v5, v0, v6, v7}, Landroid/text/PackedIntVector;->adjustValuesBelow(III)V

    #@168
    .line 318
    move-object/from16 v0, p0

    #@16a
    iget-object v5, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@16c
    const/4 v6, 0x1

    #@16d
    sub-int v7, v37, v23

    #@16f
    add-int v7, v7, v26

    #@171
    move/from16 v0, v36

    #@173
    invoke-virtual {v5, v0, v6, v7}, Landroid/text/PackedIntVector;->adjustValuesBelow(III)V

    #@176
    .line 324
    move-object/from16 v0, p0

    #@178
    iget-boolean v5, v0, Landroid/text/DynamicLayout;->mEllipsize:Z

    #@17a
    if-eqz v5, :cond_216

    #@17c
    .line 325
    const/4 v5, 0x5

    #@17d
    new-array v0, v5, [I

    #@17f
    move-object/from16 v28, v0

    #@181
    .line 326
    .local v28, ints:[I
    const/4 v5, 0x3

    #@182
    const/high16 v6, -0x8000

    #@184
    aput v6, v28, v5

    #@186
    .line 331
    :goto_186
    const/4 v5, 0x1

    #@187
    new-array v0, v5, [Landroid/text/Layout$Directions;

    #@189
    move-object/from16 v33, v0

    #@18b
    .line 333
    .local v33, objects:[Landroid/text/Layout$Directions;
    const/16 v27, 0x0

    #@18d
    .restart local v27       #i:I
    :goto_18d
    move/from16 v0, v27

    #@18f
    move/from16 v1, v32

    #@191
    if-ge v0, v1, :cond_21f

    #@193
    .line 334
    const/4 v6, 0x0

    #@194
    move/from16 v0, v27

    #@196
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineStart(I)I

    #@199
    move-result v5

    #@19a
    move/from16 v0, v27

    #@19c
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getParagraphDirection(I)I

    #@19f
    move-result v7

    #@1a0
    shl-int/lit8 v7, v7, 0x1e

    #@1a2
    or-int/2addr v7, v5

    #@1a3
    move/from16 v0, v27

    #@1a5
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineContainsTab(I)Z

    #@1a8
    move-result v5

    #@1a9
    if-eqz v5, :cond_21d

    #@1ab
    const/high16 v5, 0x2000

    #@1ad
    :goto_1ad
    or-int/2addr v5, v7

    #@1ae
    aput v5, v28, v6

    #@1b0
    .line 338
    move/from16 v0, v27

    #@1b2
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineTop(I)I

    #@1b5
    move-result v5

    #@1b6
    add-int v38, v5, v37

    #@1b8
    .line 339
    .local v38, top:I
    if-lez v27, :cond_1bc

    #@1ba
    .line 340
    sub-int v38, v38, v39

    #@1bc
    .line 341
    :cond_1bc
    const/4 v5, 0x1

    #@1bd
    aput v38, v28, v5

    #@1bf
    .line 343
    move/from16 v0, v27

    #@1c1
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineDescent(I)I

    #@1c4
    move-result v19

    #@1c5
    .line 344
    .local v19, desc:I
    add-int/lit8 v5, v32, -0x1

    #@1c7
    move/from16 v0, v27

    #@1c9
    if-ne v0, v5, :cond_1cd

    #@1cb
    .line 345
    add-int v19, v19, v17

    #@1cd
    .line 347
    :cond_1cd
    const/4 v5, 0x2

    #@1ce
    aput v19, v28, v5

    #@1d0
    .line 348
    const/4 v5, 0x0

    #@1d1
    move/from16 v0, v27

    #@1d3
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getLineDirections(I)Landroid/text/Layout$Directions;

    #@1d6
    move-result-object v6

    #@1d7
    aput-object v6, v33, v5

    #@1d9
    .line 350
    move-object/from16 v0, p0

    #@1db
    iget-boolean v5, v0, Landroid/text/DynamicLayout;->mEllipsize:Z

    #@1dd
    if-eqz v5, :cond_1f1

    #@1df
    .line 351
    const/4 v5, 0x3

    #@1e0
    move/from16 v0, v27

    #@1e2
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getEllipsisStart(I)I

    #@1e5
    move-result v6

    #@1e6
    aput v6, v28, v5

    #@1e8
    .line 352
    const/4 v5, 0x4

    #@1e9
    move/from16 v0, v27

    #@1eb
    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->getEllipsisCount(I)I

    #@1ee
    move-result v6

    #@1ef
    aput v6, v28, v5

    #@1f1
    .line 355
    :cond_1f1
    move-object/from16 v0, p0

    #@1f3
    iget-object v5, v0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@1f5
    add-int v6, v36, v27

    #@1f7
    move-object/from16 v0, v28

    #@1f9
    invoke-virtual {v5, v6, v0}, Landroid/text/PackedIntVector;->insertAt(I[I)V

    #@1fc
    .line 356
    move-object/from16 v0, p0

    #@1fe
    iget-object v5, v0, Landroid/text/DynamicLayout;->mObjects:Landroid/text/PackedObjectVector;

    #@200
    add-int v6, v36, v27

    #@202
    move-object/from16 v0, v33

    #@204
    invoke-virtual {v5, v6, v0}, Landroid/text/PackedObjectVector;->insertAt(I[Ljava/lang/Object;)V

    #@207
    .line 333
    add-int/lit8 v27, v27, 0x1

    #@209
    goto :goto_18d

    #@20a
    .line 267
    .end local v3           #reflowed:Landroid/text/StaticLayout;
    .end local v17           #botpad:I
    .end local v19           #desc:I
    .end local v26           #ht:I
    .end local v27           #i:I
    .end local v28           #ints:[I
    .end local v29           #islast:Z
    .end local v32           #n:I
    .end local v33           #objects:[Landroid/text/Layout$Directions;
    .end local v38           #top:I
    .end local v39           #toppad:I
    :cond_20a
    const/16 v29, 0x0

    #@20c
    goto/16 :goto_c7

    #@20e
    .line 276
    .restart local v29       #islast:Z
    :catchall_20e
    move-exception v5

    #@20f
    :try_start_20f
    monitor-exit v6
    :try_end_210
    .catchall {:try_start_20f .. :try_end_210} :catchall_20e

    #@210
    throw v5

    #@211
    .line 281
    .restart local v3       #reflowed:Landroid/text/StaticLayout;
    :cond_211
    invoke-virtual {v3}, Landroid/text/StaticLayout;->prepare()V

    #@214
    goto/16 :goto_d8

    #@216
    .line 328
    .restart local v17       #botpad:I
    .restart local v26       #ht:I
    .restart local v32       #n:I
    .restart local v39       #toppad:I
    :cond_216
    const/4 v5, 0x3

    #@217
    new-array v0, v5, [I

    #@219
    move-object/from16 v28, v0

    #@21b
    .restart local v28       #ints:[I
    goto/16 :goto_186

    #@21d
    .line 334
    .restart local v27       #i:I
    .restart local v33       #objects:[Landroid/text/Layout$Directions;
    :cond_21d
    const/4 v5, 0x0

    #@21e
    goto :goto_1ad

    #@21f
    .line 359
    :cond_21f
    add-int/lit8 v5, v22, -0x1

    #@221
    move-object/from16 v0, p0

    #@223
    move/from16 v1, v36

    #@225
    move/from16 v2, v32

    #@227
    invoke-virtual {v0, v1, v5, v2}, Landroid/text/DynamicLayout;->updateBlocks(III)V

    #@22a
    .line 361
    sget-object v6, Landroid/text/DynamicLayout;->sLock:[Ljava/lang/Object;

    #@22c
    monitor-enter v6

    #@22d
    .line 362
    :try_start_22d
    sput-object v3, Landroid/text/DynamicLayout;->sStaticLayout:Landroid/text/StaticLayout;

    #@22f
    .line 363
    invoke-virtual {v3}, Landroid/text/StaticLayout;->finish()V

    #@232
    .line 364
    monitor-exit v6

    #@233
    goto/16 :goto_8

    #@235
    :catchall_235
    move-exception v5

    #@236
    monitor-exit v6
    :try_end_237
    .catchall {:try_start_22d .. :try_end_237} :catchall_235

    #@237
    throw v5
.end method


# virtual methods
.method public getBlockEndLines()[I
    .registers 2

    #@0
    .prologue
    .line 545
    iget-object v0, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@2
    return-object v0
.end method

.method public getBlockIndices()[I
    .registers 2

    #@0
    .prologue
    .line 552
    iget-object v0, p0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@2
    return-object v0
.end method

.method public getBottomPadding()I
    .registers 2

    #@0
    .prologue
    .line 604
    iget v0, p0, Landroid/text/DynamicLayout;->mBottomPadding:I

    #@2
    return v0
.end method

.method public getEllipsisCount(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 669
    iget-object v0, p0, Landroid/text/DynamicLayout;->mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 670
    const/4 v0, 0x0

    #@5
    .line 673
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@8
    const/4 v1, 0x4

    #@9
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@c
    move-result v0

    #@d
    goto :goto_5
.end method

.method public getEllipsisStart(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 660
    iget-object v0, p0, Landroid/text/DynamicLayout;->mEllipsizeAt:Landroid/text/TextUtils$TruncateAt;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 661
    const/4 v0, 0x0

    #@5
    .line 664
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@8
    const/4 v1, 0x3

    #@9
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@c
    move-result v0

    #@d
    goto :goto_5
.end method

.method public getEllipsizedWidth()I
    .registers 2

    #@0
    .prologue
    .line 609
    iget v0, p0, Landroid/text/DynamicLayout;->mEllipsizedWidth:I

    #@2
    return v0
.end method

.method public getLineContainsTab(I)Z
    .registers 5
    .parameter "line"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 584
    iget-object v1, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@3
    invoke-virtual {v1, p1, v0}, Landroid/text/PackedIntVector;->getValue(II)I

    #@6
    move-result v1

    #@7
    const/high16 v2, 0x2000

    #@9
    and-int/2addr v1, v2

    #@a
    if-eqz v1, :cond_d

    #@c
    const/4 v0, 0x1

    #@d
    :cond_d
    return v0
.end method

.method public getLineCount()I
    .registers 2

    #@0
    .prologue
    .line 564
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2
    invoke-virtual {v0}, Landroid/text/PackedIntVector;->size()I

    #@5
    move-result v0

    #@6
    add-int/lit8 v0, v0, -0x1

    #@8
    return v0
.end method

.method public getLineDescent(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 574
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2
    const/4 v1, 0x2

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 594
    iget-object v0, p0, Landroid/text/DynamicLayout;->mObjects:Landroid/text/PackedObjectVector;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedObjectVector;->getValue(II)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/text/Layout$Directions;

    #@9
    return-object v0
.end method

.method public getLineStart(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 579
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@6
    move-result v0

    #@7
    const v1, 0x1fffffff

    #@a
    and-int/2addr v0, v1

    #@b
    return v0
.end method

.method public getLineTop(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 569
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@6
    move-result v0

    #@7
    return v0
.end method

.method public getNumberOfBlocks()I
    .registers 2

    #@0
    .prologue
    .line 559
    iget v0, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@2
    return v0
.end method

.method public getParagraphDirection(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Landroid/text/DynamicLayout;->mInts:Landroid/text/PackedIntVector;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, p1, v1}, Landroid/text/PackedIntVector;->getValue(II)I

    #@6
    move-result v0

    #@7
    shr-int/lit8 v0, v0, 0x1e

    #@9
    return v0
.end method

.method public getTopPadding()I
    .registers 2

    #@0
    .prologue
    .line 599
    iget v0, p0, Landroid/text/DynamicLayout;->mTopPadding:I

    #@2
    return v0
.end method

.method setBlocksDataForTest([I[II)V
    .registers 7
    .parameter "blockEndLines"
    .parameter "blockIndices"
    .parameter "numberOfBlocks"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 534
    array-length v0, p1

    #@2
    new-array v0, v0, [I

    #@4
    iput-object v0, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@6
    .line 535
    array-length v0, p2

    #@7
    new-array v0, v0, [I

    #@9
    iput-object v0, p0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@b
    .line 536
    iget-object v0, p0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@d
    array-length v1, p1

    #@e
    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@11
    .line 537
    iget-object v0, p0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@13
    array-length v1, p2

    #@14
    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@17
    .line 538
    iput p3, p0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@19
    .line 539
    return-void
.end method

.method updateBlocks(III)V
    .registers 28
    .parameter "startLine"
    .parameter "endLine"
    .parameter "newLineCount"

    #@0
    .prologue
    .line 443
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@4
    move-object/from16 v19, v0

    #@6
    if-nez v19, :cond_c

    #@8
    .line 444
    invoke-direct/range {p0 .. p0}, Landroid/text/DynamicLayout;->createBlocks()V

    #@b
    .line 527
    :cond_b
    :goto_b
    return-void

    #@c
    .line 448
    :cond_c
    const/4 v11, -0x1

    #@d
    .line 449
    .local v11, firstBlock:I
    const/4 v13, -0x1

    #@e
    .line 450
    .local v13, lastBlock:I
    const/4 v12, 0x0

    #@f
    .local v12, i:I
    :goto_f
    move-object/from16 v0, p0

    #@11
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@13
    move/from16 v19, v0

    #@15
    move/from16 v0, v19

    #@17
    if-ge v12, v0, :cond_28

    #@19
    .line 451
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@1d
    move-object/from16 v19, v0

    #@1f
    aget v19, v19, v12

    #@21
    move/from16 v0, v19

    #@23
    move/from16 v1, p1

    #@25
    if-lt v0, v1, :cond_a7

    #@27
    .line 452
    move v11, v12

    #@28
    .line 456
    :cond_28
    move v12, v11

    #@29
    :goto_29
    move-object/from16 v0, p0

    #@2b
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@2d
    move/from16 v19, v0

    #@2f
    move/from16 v0, v19

    #@31
    if-ge v12, v0, :cond_42

    #@33
    .line 457
    move-object/from16 v0, p0

    #@35
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@37
    move-object/from16 v19, v0

    #@39
    aget v19, v19, v12

    #@3b
    move/from16 v0, v19

    #@3d
    move/from16 v1, p2

    #@3f
    if-lt v0, v1, :cond_ab

    #@41
    .line 458
    move v13, v12

    #@42
    .line 462
    :cond_42
    move-object/from16 v0, p0

    #@44
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@46
    move-object/from16 v19, v0

    #@48
    aget v14, v19, v13

    #@4a
    .line 464
    .local v14, lastBlockEndLine:I
    if-nez v11, :cond_af

    #@4c
    const/16 v19, 0x0

    #@4e
    :goto_4e
    move/from16 v0, p1

    #@50
    move/from16 v1, v19

    #@52
    if-le v0, v1, :cond_bc

    #@54
    const/4 v9, 0x1

    #@55
    .line 466
    .local v9, createBlockBefore:Z
    :goto_55
    if-lez p3, :cond_be

    #@57
    const/4 v7, 0x1

    #@58
    .line 467
    .local v7, createBlock:Z
    :goto_58
    move-object/from16 v0, p0

    #@5a
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@5c
    move-object/from16 v19, v0

    #@5e
    aget v19, v19, v13

    #@60
    move/from16 v0, p2

    #@62
    move/from16 v1, v19

    #@64
    if-ge v0, v1, :cond_c0

    #@66
    const/4 v8, 0x1

    #@67
    .line 469
    .local v8, createBlockAfter:Z
    :goto_67
    const/16 v17, 0x0

    #@69
    .line 470
    .local v17, numAddedBlocks:I
    if-eqz v9, :cond_6d

    #@6b
    add-int/lit8 v17, v17, 0x1

    #@6d
    .line 471
    :cond_6d
    if-eqz v7, :cond_71

    #@6f
    add-int/lit8 v17, v17, 0x1

    #@71
    .line 472
    :cond_71
    if-eqz v8, :cond_75

    #@73
    add-int/lit8 v17, v17, 0x1

    #@75
    .line 474
    :cond_75
    sub-int v19, v13, v11

    #@77
    add-int/lit8 v18, v19, 0x1

    #@79
    .line 475
    .local v18, numRemovedBlocks:I
    move-object/from16 v0, p0

    #@7b
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@7d
    move/from16 v19, v0

    #@7f
    add-int v19, v19, v17

    #@81
    sub-int v15, v19, v18

    #@83
    .line 477
    .local v15, newNumberOfBlocks:I
    if-nez v15, :cond_c2

    #@85
    .line 479
    move-object/from16 v0, p0

    #@87
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@89
    move-object/from16 v19, v0

    #@8b
    const/16 v20, 0x0

    #@8d
    const/16 v21, 0x0

    #@8f
    aput v21, v19, v20

    #@91
    .line 480
    move-object/from16 v0, p0

    #@93
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@95
    move-object/from16 v19, v0

    #@97
    const/16 v20, 0x0

    #@99
    const/16 v21, -0x1

    #@9b
    aput v21, v19, v20

    #@9d
    .line 481
    const/16 v19, 0x1

    #@9f
    move/from16 v0, v19

    #@a1
    move-object/from16 v1, p0

    #@a3
    iput v0, v1, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@a5
    goto/16 :goto_b

    #@a7
    .line 450
    .end local v7           #createBlock:Z
    .end local v8           #createBlockAfter:Z
    .end local v9           #createBlockBefore:Z
    .end local v14           #lastBlockEndLine:I
    .end local v15           #newNumberOfBlocks:I
    .end local v17           #numAddedBlocks:I
    .end local v18           #numRemovedBlocks:I
    :cond_a7
    add-int/lit8 v12, v12, 0x1

    #@a9
    goto/16 :goto_f

    #@ab
    .line 456
    :cond_ab
    add-int/lit8 v12, v12, 0x1

    #@ad
    goto/16 :goto_29

    #@af
    .line 464
    .restart local v14       #lastBlockEndLine:I
    :cond_af
    move-object/from16 v0, p0

    #@b1
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@b3
    move-object/from16 v19, v0

    #@b5
    add-int/lit8 v20, v11, -0x1

    #@b7
    aget v19, v19, v20

    #@b9
    add-int/lit8 v19, v19, 0x1

    #@bb
    goto :goto_4e

    #@bc
    :cond_bc
    const/4 v9, 0x0

    #@bd
    goto :goto_55

    #@be
    .line 466
    .restart local v9       #createBlockBefore:Z
    :cond_be
    const/4 v7, 0x0

    #@bf
    goto :goto_58

    #@c0
    .line 467
    .restart local v7       #createBlock:Z
    :cond_c0
    const/4 v8, 0x0

    #@c1
    goto :goto_67

    #@c2
    .line 485
    .restart local v8       #createBlockAfter:Z
    .restart local v15       #newNumberOfBlocks:I
    .restart local v17       #numAddedBlocks:I
    .restart local v18       #numRemovedBlocks:I
    :cond_c2
    move-object/from16 v0, p0

    #@c4
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@c6
    move-object/from16 v19, v0

    #@c8
    move-object/from16 v0, v19

    #@ca
    array-length v0, v0

    #@cb
    move/from16 v19, v0

    #@cd
    move/from16 v0, v19

    #@cf
    if-le v15, v0, :cond_16e

    #@d1
    .line 486
    invoke-static {v15}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@d4
    move-result v16

    #@d5
    .line 487
    .local v16, newSize:I
    move/from16 v0, v16

    #@d7
    new-array v4, v0, [I

    #@d9
    .line 488
    .local v4, blockEndLines:[I
    move/from16 v0, v16

    #@db
    new-array v6, v0, [I

    #@dd
    .line 489
    .local v6, blockIndices:[I
    move-object/from16 v0, p0

    #@df
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@e1
    move-object/from16 v19, v0

    #@e3
    const/16 v20, 0x0

    #@e5
    const/16 v21, 0x0

    #@e7
    move-object/from16 v0, v19

    #@e9
    move/from16 v1, v20

    #@eb
    move/from16 v2, v21

    #@ed
    invoke-static {v0, v1, v4, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@f0
    .line 490
    move-object/from16 v0, p0

    #@f2
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@f4
    move-object/from16 v19, v0

    #@f6
    const/16 v20, 0x0

    #@f8
    const/16 v21, 0x0

    #@fa
    move-object/from16 v0, v19

    #@fc
    move/from16 v1, v20

    #@fe
    move/from16 v2, v21

    #@100
    invoke-static {v0, v1, v6, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@103
    .line 491
    move-object/from16 v0, p0

    #@105
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@107
    move-object/from16 v19, v0

    #@109
    add-int/lit8 v20, v13, 0x1

    #@10b
    add-int v21, v11, v17

    #@10d
    move-object/from16 v0, p0

    #@10f
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@111
    move/from16 v22, v0

    #@113
    sub-int v22, v22, v13

    #@115
    add-int/lit8 v22, v22, -0x1

    #@117
    move-object/from16 v0, v19

    #@119
    move/from16 v1, v20

    #@11b
    move/from16 v2, v21

    #@11d
    move/from16 v3, v22

    #@11f
    invoke-static {v0, v1, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@122
    .line 493
    move-object/from16 v0, p0

    #@124
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@126
    move-object/from16 v19, v0

    #@128
    add-int/lit8 v20, v13, 0x1

    #@12a
    add-int v21, v11, v17

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@130
    move/from16 v22, v0

    #@132
    sub-int v22, v22, v13

    #@134
    add-int/lit8 v22, v22, -0x1

    #@136
    move-object/from16 v0, v19

    #@138
    move/from16 v1, v20

    #@13a
    move/from16 v2, v21

    #@13c
    move/from16 v3, v22

    #@13e
    invoke-static {v0, v1, v6, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@141
    .line 495
    move-object/from16 v0, p0

    #@143
    iput-object v4, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@145
    .line 496
    move-object/from16 v0, p0

    #@147
    iput-object v6, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@149
    .line 504
    .end local v4           #blockEndLines:[I
    .end local v6           #blockIndices:[I
    .end local v16           #newSize:I
    :goto_149
    move-object/from16 v0, p0

    #@14b
    iput v15, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@14d
    .line 505
    sub-int v19, p2, p1

    #@14f
    add-int/lit8 v19, v19, 0x1

    #@151
    sub-int v10, p3, v19

    #@153
    .line 506
    .local v10, deltaLines:I
    add-int v12, v11, v17

    #@155
    :goto_155
    move-object/from16 v0, p0

    #@157
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@159
    move/from16 v19, v0

    #@15b
    move/from16 v0, v19

    #@15d
    if-ge v12, v0, :cond_1a9

    #@15f
    .line 507
    move-object/from16 v0, p0

    #@161
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@163
    move-object/from16 v19, v0

    #@165
    aget v20, v19, v12

    #@167
    add-int v20, v20, v10

    #@169
    aput v20, v19, v12

    #@16b
    .line 506
    add-int/lit8 v12, v12, 0x1

    #@16d
    goto :goto_155

    #@16e
    .line 498
    .end local v10           #deltaLines:I
    :cond_16e
    move-object/from16 v0, p0

    #@170
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@172
    move-object/from16 v19, v0

    #@174
    add-int/lit8 v20, v13, 0x1

    #@176
    move-object/from16 v0, p0

    #@178
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@17a
    move-object/from16 v21, v0

    #@17c
    add-int v22, v11, v17

    #@17e
    move-object/from16 v0, p0

    #@180
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@182
    move/from16 v23, v0

    #@184
    sub-int v23, v23, v13

    #@186
    add-int/lit8 v23, v23, -0x1

    #@188
    invoke-static/range {v19 .. v23}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@18b
    .line 500
    move-object/from16 v0, p0

    #@18d
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@18f
    move-object/from16 v19, v0

    #@191
    add-int/lit8 v20, v13, 0x1

    #@193
    move-object/from16 v0, p0

    #@195
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@197
    move-object/from16 v21, v0

    #@199
    add-int v22, v11, v17

    #@19b
    move-object/from16 v0, p0

    #@19d
    iget v0, v0, Landroid/text/DynamicLayout;->mNumberOfBlocks:I

    #@19f
    move/from16 v23, v0

    #@1a1
    sub-int v23, v23, v13

    #@1a3
    add-int/lit8 v23, v23, -0x1

    #@1a5
    invoke-static/range {v19 .. v23}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1a8
    goto :goto_149

    #@1a9
    .line 510
    .restart local v10       #deltaLines:I
    :cond_1a9
    move v5, v11

    #@1aa
    .line 511
    .local v5, blockIndex:I
    if-eqz v9, :cond_1c2

    #@1ac
    .line 512
    move-object/from16 v0, p0

    #@1ae
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@1b0
    move-object/from16 v19, v0

    #@1b2
    add-int/lit8 v20, p1, -0x1

    #@1b4
    aput v20, v19, v5

    #@1b6
    .line 513
    move-object/from16 v0, p0

    #@1b8
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@1ba
    move-object/from16 v19, v0

    #@1bc
    const/16 v20, -0x1

    #@1be
    aput v20, v19, v5

    #@1c0
    .line 514
    add-int/lit8 v5, v5, 0x1

    #@1c2
    .line 517
    :cond_1c2
    if-eqz v7, :cond_1dc

    #@1c4
    .line 518
    move-object/from16 v0, p0

    #@1c6
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@1c8
    move-object/from16 v19, v0

    #@1ca
    add-int v20, p1, p3

    #@1cc
    add-int/lit8 v20, v20, -0x1

    #@1ce
    aput v20, v19, v5

    #@1d0
    .line 519
    move-object/from16 v0, p0

    #@1d2
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@1d4
    move-object/from16 v19, v0

    #@1d6
    const/16 v20, -0x1

    #@1d8
    aput v20, v19, v5

    #@1da
    .line 520
    add-int/lit8 v5, v5, 0x1

    #@1dc
    .line 523
    :cond_1dc
    if-eqz v8, :cond_b

    #@1de
    .line 524
    move-object/from16 v0, p0

    #@1e0
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockEndLines:[I

    #@1e2
    move-object/from16 v19, v0

    #@1e4
    add-int v20, v14, v10

    #@1e6
    aput v20, v19, v5

    #@1e8
    .line 525
    move-object/from16 v0, p0

    #@1ea
    iget-object v0, v0, Landroid/text/DynamicLayout;->mBlockIndices:[I

    #@1ec
    move-object/from16 v19, v0

    #@1ee
    const/16 v20, -0x1

    #@1f0
    aput v20, v19, v5

    #@1f2
    goto/16 :goto_b
.end method
