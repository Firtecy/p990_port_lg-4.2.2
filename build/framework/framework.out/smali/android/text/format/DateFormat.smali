.class public Landroid/text/format/DateFormat;
.super Ljava/lang/Object;
.source "DateFormat.java"


# static fields
.field public static final AM_PM:C = 'a'

.field public static final CAPITAL_AM_PM:C = 'A'

.field public static final DATE:C = 'd'

.field public static final DAY:C = 'E'

.field public static final HOUR:C = 'h'

.field public static final HOUR_OF_DAY:C = 'k'

.field public static final MINUTE:C = 'm'

.field public static final MONTH:C = 'M'

.field public static final QUOTE:C = '\''

.field public static final SECONDS:C = 's'

.field public static final STANDALONE_MONTH:C = 'L'

.field public static final TIME_ZONE:C = 'z'

.field public static final YEAR:C = 'y'

.field private static sIs24Hour:Z

.field private static sIs24HourLocale:Ljava/util/Locale;

.field private static final sLocaleLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 210
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Landroid/text/format/DateFormat;->sLocaleLock:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static appendQuotedText(Landroid/text/SpannableStringBuilder;II)I
    .registers 7
    .parameter "s"
    .parameter "i"
    .parameter "len"

    #@0
    .prologue
    const/16 v3, 0x27

    #@2
    .line 668
    add-int/lit8 v2, p1, 0x1

    #@4
    if-ge v2, p2, :cond_15

    #@6
    add-int/lit8 v2, p1, 0x1

    #@8
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@b
    move-result v2

    #@c
    if-ne v2, v3, :cond_15

    #@e
    .line 669
    add-int/lit8 v2, p1, 0x1

    #@10
    invoke-virtual {p0, p1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@13
    .line 670
    const/4 v1, 0x1

    #@14
    .line 701
    :cond_14
    :goto_14
    return v1

    #@15
    .line 673
    :cond_15
    const/4 v1, 0x0

    #@16
    .line 676
    .local v1, count:I
    add-int/lit8 v2, p1, 0x1

    #@18
    invoke-virtual {p0, p1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@1b
    .line 677
    add-int/lit8 p2, p2, -0x1

    #@1d
    .line 679
    :goto_1d
    if-ge p1, p2, :cond_14

    #@1f
    .line 680
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@22
    move-result v0

    #@23
    .line 682
    .local v0, c:C
    if-ne v0, v3, :cond_43

    #@25
    .line 684
    add-int/lit8 v2, p1, 0x1

    #@27
    if-ge v2, p2, :cond_3d

    #@29
    add-int/lit8 v2, p1, 0x1

    #@2b
    invoke-virtual {p0, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@2e
    move-result v2

    #@2f
    if-ne v2, v3, :cond_3d

    #@31
    .line 686
    add-int/lit8 v2, p1, 0x1

    #@33
    invoke-virtual {p0, p1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@36
    .line 687
    add-int/lit8 p2, p2, -0x1

    #@38
    .line 688
    add-int/lit8 v1, v1, 0x1

    #@3a
    .line 689
    add-int/lit8 p1, p1, 0x1

    #@3c
    goto :goto_1d

    #@3d
    .line 692
    :cond_3d
    add-int/lit8 v2, p1, 0x1

    #@3f
    invoke-virtual {p0, p1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@42
    goto :goto_14

    #@43
    .line 696
    :cond_43
    add-int/lit8 p1, p1, 0x1

    #@45
    .line 697
    add-int/lit8 v1, v1, 0x1

    #@47
    goto :goto_1d
.end method

.method public static format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    .registers 4
    .parameter "inFormat"
    .parameter "inTimeInMillis"

    #@0
    .prologue
    .line 425
    new-instance v0, Ljava/util/Date;

    #@2
    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    #@5
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    #@8
    move-result-object v0

    #@9
    return-object v0
.end method

.method public static format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;
    .registers 12
    .parameter "inFormat"
    .parameter "inDate"

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/16 v9, 0x9

    #@4
    .line 513
    new-instance v5, Landroid/text/SpannableStringBuilder;

    #@6
    invoke-direct {v5, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@9
    .line 517
    .local v5, s:Landroid/text/SpannableStringBuilder;
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@c
    move-result v3

    #@d
    .line 519
    .local v3, len:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v3, :cond_b0

    #@10
    .line 522
    const/4 v1, 0x1

    #@11
    .line 523
    .local v1, count:I
    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@14
    move-result v0

    #@15
    .line 525
    .local v0, c:I
    const/16 v7, 0x27

    #@17
    if-ne v0, v7, :cond_23

    #@19
    .line 526
    invoke-static {v5, v2, v3}, Landroid/text/format/DateFormat;->appendQuotedText(Landroid/text/SpannableStringBuilder;II)I

    #@1c
    move-result v1

    #@1d
    .line 527
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    #@20
    move-result v3

    #@21
    .line 519
    :cond_21
    :goto_21
    add-int/2addr v2, v1

    #@22
    goto :goto_e

    #@23
    .line 531
    :cond_23
    :goto_23
    add-int v7, v2, v1

    #@25
    if-ge v7, v3, :cond_32

    #@27
    add-int v7, v2, v1

    #@29
    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@2c
    move-result v7

    #@2d
    if-ne v7, v0, :cond_32

    #@2f
    .line 532
    add-int/lit8 v1, v1, 0x1

    #@31
    goto :goto_23

    #@32
    .line 537
    :cond_32
    sparse-switch v0, :sswitch_data_c0

    #@35
    .line 594
    const/4 v4, 0x0

    #@36
    .line 598
    .local v4, replacement:Ljava/lang/String;
    :goto_36
    if-eqz v4, :cond_21

    #@38
    .line 599
    add-int v7, v2, v1

    #@3a
    invoke-virtual {v5, v2, v7, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@3d
    .line 600
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@40
    move-result v1

    #@41
    .line 601
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    #@44
    move-result v3

    #@45
    goto :goto_21

    #@46
    .line 539
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_46
    invoke-virtual {p1, v9}, Ljava/util/Calendar;->get(I)I

    #@49
    move-result v7

    #@4a
    invoke-static {v7}, Landroid/text/format/DateUtils;->getAMPMString(I)Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    .line 540
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@4f
    .line 544
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_4f
    invoke-virtual {p1, v9}, Ljava/util/Calendar;->get(I)I

    #@52
    move-result v7

    #@53
    invoke-static {v7}, Landroid/text/format/DateUtils;->getAMPMString(I)Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    .line 545
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@58
    .line 548
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_58
    const/4 v7, 0x5

    #@59
    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    #@5c
    move-result v7

    #@5d
    invoke-static {v7, v1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    .line 549
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@62
    .line 552
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_62
    const/4 v7, 0x7

    #@63
    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    #@66
    move-result v6

    #@67
    .line 553
    .local v6, temp:I
    const/4 v7, 0x4

    #@68
    if-ge v1, v7, :cond_71

    #@6a
    const/16 v7, 0x14

    #@6c
    :goto_6c
    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    .line 557
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@71
    .end local v4           #replacement:Ljava/lang/String;
    :cond_71
    move v7, v8

    #@72
    .line 553
    goto :goto_6c

    #@73
    .line 560
    .end local v6           #temp:I
    :sswitch_73
    invoke-virtual {p1, v8}, Ljava/util/Calendar;->get(I)I

    #@76
    move-result v6

    #@77
    .line 562
    .restart local v6       #temp:I
    if-nez v6, :cond_7b

    #@79
    .line 563
    const/16 v6, 0xc

    #@7b
    .line 565
    :cond_7b
    invoke-static {v6, v1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@7e
    move-result-object v4

    #@7f
    .line 566
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@80
    .line 569
    .end local v4           #replacement:Ljava/lang/String;
    .end local v6           #temp:I
    :sswitch_80
    const/16 v7, 0xb

    #@82
    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    #@85
    move-result v7

    #@86
    invoke-static {v7, v1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@89
    move-result-object v4

    #@8a
    .line 570
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@8b
    .line 573
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_8b
    const/16 v7, 0xc

    #@8d
    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    #@90
    move-result v7

    #@91
    invoke-static {v7, v1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    .line 574
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@96
    .line 578
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_96
    invoke-static {p1, v1, v0}, Landroid/text/format/DateFormat;->getMonthString(Ljava/util/Calendar;II)Ljava/lang/String;

    #@99
    move-result-object v4

    #@9a
    .line 579
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@9b
    .line 582
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_9b
    const/16 v7, 0xd

    #@9d
    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    #@a0
    move-result v7

    #@a1
    invoke-static {v7, v1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@a4
    move-result-object v4

    #@a5
    .line 583
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@a6
    .line 586
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_a6
    invoke-static {p1, v1}, Landroid/text/format/DateFormat;->getTimeZoneString(Ljava/util/Calendar;I)Ljava/lang/String;

    #@a9
    move-result-object v4

    #@aa
    .line 587
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@ab
    .line 590
    .end local v4           #replacement:Ljava/lang/String;
    :sswitch_ab
    invoke-static {p1, v1}, Landroid/text/format/DateFormat;->getYearString(Ljava/util/Calendar;I)Ljava/lang/String;

    #@ae
    move-result-object v4

    #@af
    .line 591
    .restart local v4       #replacement:Ljava/lang/String;
    goto :goto_36

    #@b0
    .line 605
    .end local v0           #c:I
    .end local v1           #count:I
    .end local v4           #replacement:Ljava/lang/String;
    :cond_b0
    instance-of v7, p0, Landroid/text/Spanned;

    #@b2
    if-eqz v7, :cond_ba

    #@b4
    .line 606
    new-instance v7, Landroid/text/SpannedString;

    #@b6
    invoke-direct {v7, v5}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    #@b9
    .line 608
    :goto_b9
    return-object v7

    #@ba
    :cond_ba
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v7

    #@be
    goto :goto_b9

    #@bf
    .line 537
    nop

    #@c0
    :sswitch_data_c0
    .sparse-switch
        0x41 -> :sswitch_4f
        0x45 -> :sswitch_62
        0x4c -> :sswitch_96
        0x4d -> :sswitch_96
        0x61 -> :sswitch_46
        0x64 -> :sswitch_58
        0x68 -> :sswitch_73
        0x6b -> :sswitch_80
        0x6d -> :sswitch_8b
        0x73 -> :sswitch_9b
        0x79 -> :sswitch_ab
        0x7a -> :sswitch_a6
    .end sparse-switch
.end method

.method public static format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;
    .registers 4
    .parameter "inFormat"
    .parameter "inDate"

    #@0
    .prologue
    .line 436
    new-instance v0, Ljava/util/GregorianCalendar;

    #@2
    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    #@5
    .line 438
    .local v0, c:Ljava/util/Calendar;
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    #@8
    .line 440
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method private static formatZoneOffset(II)Ljava/lang/String;
    .registers 8
    .parameter "offset"
    .parameter "count"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x0

    #@2
    .line 643
    div-int/lit16 p0, p0, 0x3e8

    #@4
    .line 644
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 646
    .local v2, tb:Ljava/lang/StringBuilder;
    if-gez p0, :cond_2a

    #@b
    .line 647
    const-string v3, "-"

    #@d
    invoke-virtual {v2, v4, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 648
    neg-int p0, p0

    #@11
    .line 653
    :goto_11
    div-int/lit16 v0, p0, 0xe10

    #@13
    .line 654
    .local v0, hours:I
    rem-int/lit16 v3, p0, 0xe10

    #@15
    div-int/lit8 v1, v3, 0x3c

    #@17
    .line 656
    .local v1, minutes:I
    invoke-static {v0, v5}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 657
    invoke-static {v1, v5}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    .line 658
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    return-object v3

    #@2a
    .line 650
    .end local v0           #hours:I
    .end local v1           #minutes:I
    :cond_2a
    const-string v3, "+"

    #@2c
    invoke-virtual {v2, v4, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    goto :goto_11
.end method

.method public static getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 288
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "date_format"

    #@6
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 291
    .local v0, value:Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getDateFormatForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/text/DateFormat;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method public static getDateFormatForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/text/DateFormat;
    .registers 4
    .parameter "context"
    .parameter "value"

    #@0
    .prologue
    .line 305
    invoke-static {p0, p1}, Landroid/text/format/DateFormat;->getDateFormatStringForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 307
    .local v0, format:Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    #@6
    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@9
    return-object v1
.end method

.method public static getDateFormatOrder(Landroid/content/Context;)[C
    .registers 15
    .parameter "context"

    #@0
    .prologue
    const/16 v13, 0x79

    #@2
    const/16 v12, 0x64

    #@4
    const/16 v11, 0x4d

    #@6
    .line 381
    const/4 v10, 0x3

    #@7
    new-array v8, v10, [C

    #@9
    fill-array-data v8, :array_42

    #@c
    .line 382
    .local v8, order:[C
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatString(Landroid/content/Context;)Ljava/lang/String;

    #@f
    move-result-object v9

    #@10
    .line 383
    .local v9, value:Ljava/lang/String;
    const/4 v6, 0x0

    #@11
    .line 384
    .local v6, index:I
    const/4 v2, 0x0

    #@12
    .line 385
    .local v2, foundDate:Z
    const/4 v3, 0x0

    #@13
    .line 386
    .local v3, foundMonth:Z
    const/4 v4, 0x0

    #@14
    .line 388
    .local v4, foundYear:Z
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    #@17
    move-result-object v0

    #@18
    .local v0, arr$:[C
    array-length v7, v0

    #@19
    .local v7, len$:I
    const/4 v5, 0x0

    #@1a
    .local v5, i$:I
    :goto_1a
    if-ge v5, v7, :cond_40

    #@1c
    aget-char v1, v0, v5

    #@1e
    .line 389
    .local v1, c:C
    if-nez v2, :cond_27

    #@20
    if-ne v1, v12, :cond_27

    #@22
    .line 390
    const/4 v2, 0x1

    #@23
    .line 391
    aput-char v12, v8, v6

    #@25
    .line 392
    add-int/lit8 v6, v6, 0x1

    #@27
    .line 395
    :cond_27
    if-nez v3, :cond_34

    #@29
    if-eq v1, v11, :cond_2f

    #@2b
    const/16 v10, 0x4c

    #@2d
    if-ne v1, v10, :cond_34

    #@2f
    .line 396
    :cond_2f
    const/4 v3, 0x1

    #@30
    .line 397
    aput-char v11, v8, v6

    #@32
    .line 398
    add-int/lit8 v6, v6, 0x1

    #@34
    .line 401
    :cond_34
    if-nez v4, :cond_3d

    #@36
    if-ne v1, v13, :cond_3d

    #@38
    .line 402
    const/4 v4, 0x1

    #@39
    .line 403
    aput-char v13, v8, v6

    #@3b
    .line 404
    add-int/lit8 v6, v6, 0x1

    #@3d
    .line 388
    :cond_3d
    add-int/lit8 v5, v5, 0x1

    #@3f
    goto :goto_1a

    #@40
    .line 407
    .end local v1           #c:C
    :cond_40
    return-object v8

    #@41
    .line 381
    nop

    #@42
    :array_42
    .array-data 0x2
        0x64t 0x0t
        0x4dt 0x0t
        0x79t 0x0t
    .end array-data
.end method

.method private static getDateFormatString(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 411
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v1

    #@4
    const-string v2, "date_format"

    #@6
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 414
    .local v0, value:Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getDateFormatStringForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    return-object v1
.end method

.method private static getDateFormatStringForSetting(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "context"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x3

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 311
    if-eqz p1, :cond_aa

    #@6
    .line 312
    const/16 v5, 0x4d

    #@8
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    #@b
    move-result v1

    #@c
    .line 313
    .local v1, month:I
    const/16 v5, 0x64

    #@e
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    #@11
    move-result v0

    #@12
    .line 314
    .local v0, day:I
    const/16 v5, 0x79

    #@14
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    #@17
    move-result v4

    #@18
    .line 316
    .local v4, year:I
    if-ltz v1, :cond_aa

    #@1a
    if-ltz v0, :cond_aa

    #@1c
    if-ltz v4, :cond_aa

    #@1e
    .line 317
    const v5, 0x1040050

    #@21
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    .line 318
    .local v2, template:Ljava/lang/String;
    if-ge v4, v1, :cond_54

    #@27
    if-ge v4, v0, :cond_54

    #@29
    .line 319
    if-ge v1, v0, :cond_40

    #@2b
    .line 320
    new-array v5, v6, [Ljava/lang/Object;

    #@2d
    const-string/jumbo v6, "yyyy"

    #@30
    aput-object v6, v5, v7

    #@32
    const-string v6, "MM"

    #@34
    aput-object v6, v5, v8

    #@36
    const-string v6, "dd"

    #@38
    aput-object v6, v5, v9

    #@3a
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3d
    move-result-object p1

    #@3e
    :goto_3e
    move-object v3, p1

    #@3f
    .line 348
    .end local v0           #day:I
    .end local v1           #month:I
    .end local v2           #template:Ljava/lang/String;
    .end local v4           #year:I
    .end local p1
    .local v3, value:Ljava/lang/String;
    :goto_3f
    return-object v3

    #@40
    .line 322
    .end local v3           #value:Ljava/lang/String;
    .restart local v0       #day:I
    .restart local v1       #month:I
    .restart local v2       #template:Ljava/lang/String;
    .restart local v4       #year:I
    .restart local p1
    :cond_40
    new-array v5, v6, [Ljava/lang/Object;

    #@42
    const-string/jumbo v6, "yyyy"

    #@45
    aput-object v6, v5, v7

    #@47
    const-string v6, "dd"

    #@49
    aput-object v6, v5, v8

    #@4b
    const-string v6, "MM"

    #@4d
    aput-object v6, v5, v9

    #@4f
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@52
    move-result-object p1

    #@53
    goto :goto_3e

    #@54
    .line 324
    :cond_54
    if-ge v1, v0, :cond_80

    #@56
    .line 325
    if-ge v0, v4, :cond_6c

    #@58
    .line 326
    new-array v5, v6, [Ljava/lang/Object;

    #@5a
    const-string v6, "MM"

    #@5c
    aput-object v6, v5, v7

    #@5e
    const-string v6, "dd"

    #@60
    aput-object v6, v5, v8

    #@62
    const-string/jumbo v6, "yyyy"

    #@65
    aput-object v6, v5, v9

    #@67
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@6a
    move-result-object p1

    #@6b
    goto :goto_3e

    #@6c
    .line 328
    :cond_6c
    new-array v5, v6, [Ljava/lang/Object;

    #@6e
    const-string v6, "MM"

    #@70
    aput-object v6, v5, v7

    #@72
    const-string/jumbo v6, "yyyy"

    #@75
    aput-object v6, v5, v8

    #@77
    const-string v6, "dd"

    #@79
    aput-object v6, v5, v9

    #@7b
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@7e
    move-result-object p1

    #@7f
    goto :goto_3e

    #@80
    .line 331
    :cond_80
    if-ge v1, v4, :cond_96

    #@82
    .line 332
    new-array v5, v6, [Ljava/lang/Object;

    #@84
    const-string v6, "dd"

    #@86
    aput-object v6, v5, v7

    #@88
    const-string v6, "MM"

    #@8a
    aput-object v6, v5, v8

    #@8c
    const-string/jumbo v6, "yyyy"

    #@8f
    aput-object v6, v5, v9

    #@91
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@94
    move-result-object p1

    #@95
    goto :goto_3e

    #@96
    .line 334
    :cond_96
    new-array v5, v6, [Ljava/lang/Object;

    #@98
    const-string v6, "dd"

    #@9a
    aput-object v6, v5, v7

    #@9c
    const-string/jumbo v6, "yyyy"

    #@9f
    aput-object v6, v5, v8

    #@a1
    const-string v6, "MM"

    #@a3
    aput-object v6, v5, v9

    #@a5
    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@a8
    move-result-object p1

    #@a9
    goto :goto_3e

    #@aa
    .line 347
    .end local v0           #day:I
    .end local v1           #month:I
    .end local v2           #template:Ljava/lang/String;
    .end local v4           #year:I
    :cond_aa
    const v5, 0x104004f

    #@ad
    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@b0
    move-result-object p1

    #@b1
    move-object v3, p1

    #@b2
    .line 348
    .end local p1
    .restart local v3       #value:Ljava/lang/String;
    goto :goto_3f
.end method

.method public static getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 358
    const/4 v0, 0x1

    #@1
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 368
    const/4 v0, 0x2

    #@1
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static getMonthString(Ljava/util/Calendar;II)Ljava/lang/String;
    .registers 8
    .parameter "inDate"
    .parameter "count"
    .parameter "kind"

    #@0
    .prologue
    const/16 v4, 0x14

    #@2
    const/16 v3, 0xa

    #@4
    .line 612
    const/16 v2, 0x4c

    #@6
    if-ne p2, v2, :cond_18

    #@8
    const/4 v1, 0x1

    #@9
    .line 613
    .local v1, standalone:Z
    :goto_9
    const/4 v2, 0x2

    #@a
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    #@d
    move-result v0

    #@e
    .line 615
    .local v0, month:I
    const/4 v2, 0x4

    #@f
    if-lt p1, v2, :cond_1f

    #@11
    .line 616
    if-eqz v1, :cond_1a

    #@13
    invoke-static {v0, v3}, Landroid/text/format/DateUtils;->getStandaloneMonthString(II)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    .line 625
    :goto_17
    return-object v2

    #@18
    .line 612
    .end local v0           #month:I
    .end local v1           #standalone:Z
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_9

    #@1a
    .line 616
    .restart local v0       #month:I
    .restart local v1       #standalone:Z
    :cond_1a
    invoke-static {v0, v3}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    goto :goto_17

    #@1f
    .line 619
    :cond_1f
    const/4 v2, 0x3

    #@20
    if-ne p1, v2, :cond_2e

    #@22
    .line 620
    if-eqz v1, :cond_29

    #@24
    invoke-static {v0, v4}, Landroid/text/format/DateUtils;->getStandaloneMonthString(II)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    goto :goto_17

    #@29
    :cond_29
    invoke-static {v0, v4}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    goto :goto_17

    #@2e
    .line 625
    :cond_2e
    add-int/lit8 v2, v0, 0x1

    #@30
    invoke-static {v2, p1}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    goto :goto_17
.end method

.method public static getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 268
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@3
    move-result v0

    #@4
    .line 271
    .local v0, b24:Z
    if-eqz v0, :cond_13

    #@6
    .line 272
    const v1, 0x104004d

    #@9
    .line 277
    .local v1, res:I
    :goto_9
    new-instance v2, Ljava/text/SimpleDateFormat;

    #@b
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e
    move-result-object v3

    #@f
    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@12
    return-object v2

    #@13
    .line 274
    .end local v1           #res:I
    :cond_13
    const v1, 0x104004c

    #@16
    .restart local v1       #res:I
    goto :goto_9
.end method

.method private static getTimeZoneString(Ljava/util/Calendar;I)Ljava/lang/String;
    .registers 7
    .parameter "inDate"
    .parameter "count"

    #@0
    .prologue
    const/16 v4, 0x10

    #@2
    const/4 v2, 0x0

    #@3
    .line 630
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    #@6
    move-result-object v1

    #@7
    .line 632
    .local v1, tz:Ljava/util/TimeZone;
    const/4 v3, 0x2

    #@8
    if-ge p1, v3, :cond_1a

    #@a
    .line 633
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    #@d
    move-result v2

    #@e
    const/16 v3, 0xf

    #@10
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    #@13
    move-result v3

    #@14
    add-int/2addr v2, v3

    #@15
    invoke-static {v2, p1}, Landroid/text/format/DateFormat;->formatZoneOffset(II)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 638
    :goto_19
    return-object v2

    #@1a
    .line 637
    :cond_1a
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_26

    #@20
    const/4 v0, 0x1

    #@21
    .line 638
    .local v0, dst:Z
    :goto_21
    invoke-virtual {v1, v0, v2}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    goto :goto_19

    #@26
    .end local v0           #dst:Z
    :cond_26
    move v0, v2

    #@27
    .line 637
    goto :goto_21
.end method

.method private static getYearString(Ljava/util/Calendar;I)Ljava/lang/String;
    .registers 8
    .parameter "inDate"
    .parameter "count"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    .line 662
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    #@5
    move-result v0

    #@6
    .line 663
    .local v0, year:I
    if-gt p1, v2, :cond_f

    #@8
    rem-int/lit8 v1, v0, 0x64

    #@a
    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->zeroPad(II)Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    :goto_e
    return-object v1

    #@f
    :cond_f
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@12
    move-result-object v1

    #@13
    const-string v2, "%d"

    #@15
    new-array v3, v3, [Ljava/lang/Object;

    #@17
    const/4 v4, 0x0

    #@18
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v5

    #@1c
    aput-object v5, v3, v4

    #@1e
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    goto :goto_e
.end method

.method public static hasSeconds(Ljava/lang/CharSequence;)Z
    .registers 7
    .parameter "inFormat"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 455
    if-nez p0, :cond_4

    #@3
    .line 473
    :cond_3
    :goto_3
    return v4

    #@4
    .line 457
    :cond_4
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    #@7
    move-result v3

    #@8
    .line 462
    .local v3, length:I
    const/4 v2, 0x0

    #@9
    .local v2, i:I
    :goto_9
    if-ge v2, v3, :cond_3

    #@b
    .line 463
    const/4 v1, 0x1

    #@c
    .line 464
    .local v1, count:I
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@f
    move-result v0

    #@10
    .line 466
    .local v0, c:I
    const/16 v5, 0x27

    #@12
    if-ne v0, v5, :cond_1a

    #@14
    .line 467
    invoke-static {p0, v2, v3}, Landroid/text/format/DateFormat;->skipQuotedText(Ljava/lang/CharSequence;II)I

    #@17
    move-result v1

    #@18
    .line 462
    :cond_18
    add-int/2addr v2, v1

    #@19
    goto :goto_9

    #@1a
    .line 468
    :cond_1a
    const/16 v5, 0x73

    #@1c
    if-ne v0, v5, :cond_18

    #@1e
    .line 469
    const/4 v4, 0x1

    #@1f
    goto :goto_3
.end method

.method public static is24HourFormat(Landroid/content/Context;)Z
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 221
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v5

    #@4
    const-string/jumbo v6, "time_12_24"

    #@7
    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v4

    #@b
    .line 224
    .local v4, value:Ljava/lang/String;
    if-nez v4, :cond_62

    #@d
    .line 225
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@14
    move-result-object v5

    #@15
    iget-object v0, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@17
    .line 227
    .local v0, locale:Ljava/util/Locale;
    sget-object v6, Landroid/text/format/DateFormat;->sLocaleLock:Ljava/lang/Object;

    #@19
    monitor-enter v6

    #@1a
    .line 228
    :try_start_1a
    sget-object v5, Landroid/text/format/DateFormat;->sIs24HourLocale:Ljava/util/Locale;

    #@1c
    if-eqz v5, :cond_2a

    #@1e
    sget-object v5, Landroid/text/format/DateFormat;->sIs24HourLocale:Ljava/util/Locale;

    #@20
    invoke-virtual {v5, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v5

    #@24
    if-eqz v5, :cond_2a

    #@26
    .line 229
    sget-boolean v5, Landroid/text/format/DateFormat;->sIs24Hour:Z

    #@28
    monitor-exit v6

    #@29
    .line 258
    .end local v0           #locale:Ljava/util/Locale;
    :goto_29
    return v5

    #@2a
    .line 231
    .restart local v0       #locale:Ljava/util/Locale;
    :cond_2a
    monitor-exit v6
    :try_end_2b
    .catchall {:try_start_1a .. :try_end_2b} :catchall_56

    #@2b
    .line 233
    const/4 v5, 0x1

    #@2c
    invoke-static {v5, v0}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    #@2f
    move-result-object v1

    #@30
    .line 237
    .local v1, natural:Ljava/text/DateFormat;
    instance-of v5, v1, Ljava/text/SimpleDateFormat;

    #@32
    if-eqz v5, :cond_5c

    #@34
    move-object v3, v1

    #@35
    .line 238
    check-cast v3, Ljava/text/SimpleDateFormat;

    #@37
    .line 239
    .local v3, sdf:Ljava/text/SimpleDateFormat;
    invoke-virtual {v3}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    .line 241
    .local v2, pattern:Ljava/lang/String;
    const/16 v5, 0x48

    #@3d
    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    #@40
    move-result v5

    #@41
    if-ltz v5, :cond_59

    #@43
    .line 242
    const-string v4, "24"

    #@45
    .line 250
    .end local v2           #pattern:Ljava/lang/String;
    .end local v3           #sdf:Ljava/text/SimpleDateFormat;
    :goto_45
    sget-object v6, Landroid/text/format/DateFormat;->sLocaleLock:Ljava/lang/Object;

    #@47
    monitor-enter v6

    #@48
    .line 251
    :try_start_48
    sput-object v0, Landroid/text/format/DateFormat;->sIs24HourLocale:Ljava/util/Locale;

    #@4a
    .line 252
    const-string v5, "24"

    #@4c
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v5

    #@50
    sput-boolean v5, Landroid/text/format/DateFormat;->sIs24Hour:Z

    #@52
    .line 253
    monitor-exit v6
    :try_end_53
    .catchall {:try_start_48 .. :try_end_53} :catchall_5f

    #@53
    .line 255
    sget-boolean v5, Landroid/text/format/DateFormat;->sIs24Hour:Z

    #@55
    goto :goto_29

    #@56
    .line 231
    .end local v1           #natural:Ljava/text/DateFormat;
    :catchall_56
    move-exception v5

    #@57
    :try_start_57
    monitor-exit v6
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_56

    #@58
    throw v5

    #@59
    .line 244
    .restart local v1       #natural:Ljava/text/DateFormat;
    .restart local v2       #pattern:Ljava/lang/String;
    .restart local v3       #sdf:Ljava/text/SimpleDateFormat;
    :cond_59
    const-string v4, "12"

    #@5b
    goto :goto_45

    #@5c
    .line 247
    .end local v2           #pattern:Ljava/lang/String;
    .end local v3           #sdf:Ljava/text/SimpleDateFormat;
    :cond_5c
    const-string v4, "12"

    #@5e
    goto :goto_45

    #@5f
    .line 253
    :catchall_5f
    move-exception v5

    #@60
    :try_start_60
    monitor-exit v6
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_5f

    #@61
    throw v5

    #@62
    .line 258
    .end local v0           #locale:Ljava/util/Locale;
    .end local v1           #natural:Ljava/text/DateFormat;
    :cond_62
    const-string v5, "24"

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v5

    #@68
    goto :goto_29
.end method

.method private static skipQuotedText(Ljava/lang/CharSequence;II)I
    .registers 7
    .parameter "s"
    .parameter "i"
    .parameter "len"

    #@0
    .prologue
    const/16 v3, 0x27

    #@2
    .line 477
    add-int/lit8 v2, p1, 0x1

    #@4
    if-ge v2, p2, :cond_10

    #@6
    add-int/lit8 v2, p1, 0x1

    #@8
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@b
    move-result v2

    #@c
    if-ne v2, v3, :cond_10

    #@e
    .line 478
    const/4 v1, 0x2

    #@f
    .line 502
    :cond_f
    return v1

    #@10
    .line 481
    :cond_10
    const/4 v1, 0x1

    #@11
    .line 483
    .local v1, count:I
    add-int/lit8 p1, p1, 0x1

    #@13
    .line 485
    :goto_13
    if-ge p1, p2, :cond_f

    #@15
    .line 486
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    #@18
    move-result v0

    #@19
    .line 488
    .local v0, c:C
    if-ne v0, v3, :cond_2c

    #@1b
    .line 489
    add-int/lit8 v1, v1, 0x1

    #@1d
    .line 491
    add-int/lit8 v2, p1, 0x1

    #@1f
    if-ge v2, p2, :cond_f

    #@21
    add-int/lit8 v2, p1, 0x1

    #@23
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    #@26
    move-result v2

    #@27
    if-ne v2, v3, :cond_f

    #@29
    .line 492
    add-int/lit8 p1, p1, 0x1

    #@2b
    goto :goto_13

    #@2c
    .line 497
    :cond_2c
    add-int/lit8 p1, p1, 0x1

    #@2e
    .line 498
    add-int/lit8 v1, v1, 0x1

    #@30
    goto :goto_13
.end method

.method private static zeroPad(II)Ljava/lang/String;
    .registers 7
    .parameter "inValue"
    .parameter "inMinDigits"

    #@0
    .prologue
    .line 705
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "%0"

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "d"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    const/4 v2, 0x1

    #@1e
    new-array v2, v2, [Ljava/lang/Object;

    #@20
    const/4 v3, 0x0

    #@21
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@24
    move-result-object v4

    #@25
    aput-object v4, v2, v3

    #@27
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    return-object v0
.end method
