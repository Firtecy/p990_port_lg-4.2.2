.class public Landroid/text/format/DateUtils;
.super Ljava/lang/Object;
.source "DateUtils.java"


# static fields
.field public static final ABBREV_MONTH_FORMAT:Ljava/lang/String; = "%b"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ABBREV_WEEKDAY_FORMAT:Ljava/lang/String; = "%a"

.field public static final DAY_IN_MILLIS:J = 0x5265c00L

.field private static final FAST_FORMAT_HMMSS:Ljava/lang/String; = "%1$d:%2$02d:%3$02d"

.field private static final FAST_FORMAT_MMSS:Ljava/lang/String; = "%1$02d:%2$02d"

.field public static final FORMAT_12HOUR:I = 0x40
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_24HOUR:I = 0x80
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_ABBREV_ALL:I = 0x80000

.field public static final FORMAT_ABBREV_MONTH:I = 0x10000

.field public static final FORMAT_ABBREV_RELATIVE:I = 0x40000

.field public static final FORMAT_ABBREV_TIME:I = 0x4000

.field public static final FORMAT_ABBREV_WEEKDAY:I = 0x8000

.field public static final FORMAT_CAP_AMPM:I = 0x100
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_CAP_MIDNIGHT:I = 0x1000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_CAP_NOON:I = 0x400
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_CAP_NOON_MIDNIGHT:I = 0x1400
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_NO_MIDNIGHT:I = 0x800

.field public static final FORMAT_NO_MONTH_DAY:I = 0x20

.field public static final FORMAT_NO_NOON:I = 0x200

.field public static final FORMAT_NO_NOON_MIDNIGHT:I = 0xa00
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FORMAT_NO_YEAR:I = 0x8

.field public static final FORMAT_NUMERIC_DATE:I = 0x20000

.field public static final FORMAT_SHOW_DATE:I = 0x10

.field public static final FORMAT_SHOW_TIME:I = 0x1

.field public static final FORMAT_SHOW_WEEKDAY:I = 0x2

.field public static final FORMAT_SHOW_YEAR:I = 0x4

.field public static final FORMAT_UTC:I = 0x2000
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HOUR_IN_MILLIS:J = 0x36ee80L

.field public static final HOUR_MINUTE_24:Ljava/lang/String; = "%H:%M"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LENGTH_LONG:I = 0xa
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LENGTH_MEDIUM:I = 0x14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LENGTH_SHORT:I = 0x1e
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LENGTH_SHORTER:I = 0x28
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LENGTH_SHORTEST:I = 0x32
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MINUTE_IN_MILLIS:J = 0xea60L

.field public static final MONTH_DAY_FORMAT:Ljava/lang/String; = "%-d"

.field public static final MONTH_FORMAT:Ljava/lang/String; = "%B"

.field public static final NUMERIC_MONTH_FORMAT:Ljava/lang/String; = "%m"

.field public static final SECOND_IN_MILLIS:J = 0x3e8L

.field private static final TIME_SEPARATOR:C = ':'

.field public static final WEEKDAY_FORMAT:Ljava/lang/String; = "%A"

.field public static final WEEK_IN_MILLIS:J = 0x240c8400L

.field public static final YEAR_FORMAT:Ljava/lang/String; = "%Y"

.field public static final YEAR_FORMAT_TWO_DIGITS:Ljava/lang/String; = "%g"

.field public static final YEAR_IN_MILLIS:J = 0x7528ad000L

.field private static sElapsedFormatHMMSS:Ljava/lang/String;

.field private static sElapsedFormatMMSS:Ljava/lang/String;

.field private static sLastConfig:Landroid/content/res/Configuration;

.field private static final sLock:Ljava/lang/Object;

.field private static sNowTime:Landroid/text/format/Time;

.field private static sStatusTimeFormat:Ljava/text/DateFormat;

.field private static sThenTime:Landroid/text/format/Time;

.field public static final sameMonthTable:[I

.field public static final sameYearTable:[I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    .line 40
    new-instance v0, Ljava/lang/Object;

    #@4
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@7
    sput-object v0, Landroid/text/format/DateUtils;->sLock:Ljava/lang/Object;

    #@9
    .line 127
    new-array v0, v1, [I

    #@b
    fill-array-data v0, :array_18

    #@e
    sput-object v0, Landroid/text/format/DateUtils;->sameYearTable:[I

    #@10
    .line 154
    new-array v0, v1, [I

    #@12
    fill-array-data v0, :array_3c

    #@15
    sput-object v0, Landroid/text/format/DateUtils;->sameMonthTable:[I

    #@17
    return-void

    #@18
    .line 127
    :array_18
    .array-data 0x4
        0x6dt 0x0t 0x4t 0x1t
        0x6et 0x0t 0x4t 0x1t
        0x7at 0x0t 0x4t 0x1t
        0x7ct 0x0t 0x4t 0x1t
        0x6ft 0x0t 0x4t 0x1t
        0x71t 0x0t 0x4t 0x1t
        0x73t 0x0t 0x4t 0x1t
        0x75t 0x0t 0x4t 0x1t
        0x5ft 0x0t 0x4t 0x1t
        0x60t 0x0t 0x4t 0x1t
        0x61t 0x0t 0x4t 0x1t
        0x62t 0x0t 0x4t 0x1t
        0x64t 0x0t 0x4t 0x1t
        0x65t 0x0t 0x4t 0x1t
        0x66t 0x0t 0x4t 0x1t
        0x63t 0x0t 0x4t 0x1t
    .end array-data

    #@3c
    .line 154
    :array_3c
    .array-data 0x4
        0x78t 0x0t 0x4t 0x1t
        0x79t 0x0t 0x4t 0x1t
        0x7bt 0x0t 0x4t 0x1t
        0x77t 0x0t 0x4t 0x1t
        0x70t 0x0t 0x4t 0x1t
        0x72t 0x0t 0x4t 0x1t
        0x74t 0x0t 0x4t 0x1t
        0x76t 0x0t 0x4t 0x1t
        0x5ft 0x0t 0x4t 0x1t
        0x60t 0x0t 0x4t 0x1t
        0x61t 0x0t 0x4t 0x1t
        0x62t 0x0t 0x4t 0x1t
        0x64t 0x0t 0x4t 0x1t
        0x65t 0x0t 0x4t 0x1t
        0x66t 0x0t 0x4t 0x1t
        0x63t 0x0t 0x4t 0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static append(Ljava/lang/StringBuilder;JZC)V
    .registers 11
    .parameter "sb"
    .parameter "value"
    .parameter "pad"
    .parameter "zeroDigit"

    #@0
    .prologue
    const-wide/16 v4, 0xa

    #@2
    .line 675
    cmp-long v0, p1, v4

    #@4
    if-gez v0, :cond_15

    #@6
    .line 676
    if-eqz p3, :cond_b

    #@8
    .line 677
    invoke-virtual {p0, p4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@b
    .line 682
    :cond_b
    :goto_b
    int-to-long v0, p4

    #@c
    rem-long v2, p1, v4

    #@e
    add-long/2addr v0, v2

    #@f
    long-to-int v0, v0

    #@10
    int-to-char v0, v0

    #@11
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@14
    .line 683
    return-void

    #@15
    .line 680
    :cond_15
    int-to-long v0, p4

    #@16
    div-long v2, p1, v4

    #@18
    add-long/2addr v0, v2

    #@19
    long-to-int v0, v0

    #@1a
    int-to-char v0, v0

    #@1b
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@1e
    goto :goto_b
.end method

.method public static assign(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .registers 4
    .parameter "lval"
    .parameter "rval"

    #@0
    .prologue
    .line 916
    invoke-virtual {p0}, Ljava/util/Calendar;->clear()V

    #@3
    .line 917
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    #@6
    move-result-wide v0

    #@7
    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@a
    .line 918
    return-void
.end method

.method public static formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;
    .registers 13
    .parameter "context"
    .parameter "startMillis"
    .parameter "endMillis"
    .parameter "flags"

    #@0
    .prologue
    .line 941
    new-instance v1, Ljava/util/Formatter;

    #@2
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    const/16 v2, 0x32

    #@6
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@c
    move-result-object v2

    #@d
    invoke-direct {v1, v0, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    #@10
    .local v1, f:Ljava/util/Formatter;
    move-object v0, p0

    #@11
    move-wide v2, p1

    #@12
    move-wide v4, p3

    #@13
    move v6, p5

    #@14
    .line 942
    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJI)Ljava/util/Formatter;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    return-object v0
.end method

.method public static formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJI)Ljava/util/Formatter;
    .registers 15
    .parameter "context"
    .parameter "formatter"
    .parameter "startMillis"
    .parameter "endMillis"
    .parameter "flags"

    #@0
    .prologue
    .line 965
    const/4 v7, 0x0

    #@1
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-wide v2, p2

    #@4
    move-wide v4, p4

    #@5
    move v6, p6

    #@6
    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;
    .registers 76
    .parameter "context"
    .parameter "formatter"
    .parameter "startMillis"
    .parameter "endMillis"
    .parameter "flags"
    .parameter "timeZone"

    #@0
    .prologue
    .line 1136
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v39

    #@4
    .line 1137
    .local v39, res:Landroid/content/res/Resources;
    and-int/lit8 v64, p6, 0x1

    #@6
    if-eqz v64, :cond_147

    #@8
    const/16 v42, 0x1

    #@a
    .line 1138
    .local v42, showTime:Z
    :goto_a
    and-int/lit8 v64, p6, 0x2

    #@c
    if-eqz v64, :cond_14b

    #@e
    const/16 v43, 0x1

    #@10
    .line 1139
    .local v43, showWeekDay:Z
    :goto_10
    and-int/lit8 v64, p6, 0x4

    #@12
    if-eqz v64, :cond_14f

    #@14
    const/16 v44, 0x1

    #@16
    .line 1140
    .local v44, showYear:Z
    :goto_16
    and-int/lit8 v64, p6, 0x8

    #@18
    if-eqz v64, :cond_153

    #@1a
    const/16 v37, 0x1

    #@1c
    .line 1141
    .local v37, noYear:Z
    :goto_1c
    move/from16 v0, p6

    #@1e
    and-int/lit16 v0, v0, 0x2000

    #@20
    move/from16 v64, v0

    #@22
    if-eqz v64, :cond_157

    #@24
    const/16 v62, 0x1

    #@26
    .line 1142
    .local v62, useUTC:Z
    :goto_26
    const v64, 0x88000

    #@29
    and-int v64, v64, p6

    #@2b
    if-eqz v64, :cond_15b

    #@2d
    const/4 v6, 0x1

    #@2e
    .line 1143
    .local v6, abbrevWeekDay:Z
    :goto_2e
    const/high16 v64, 0x9

    #@30
    and-int v64, v64, p6

    #@32
    if-eqz v64, :cond_15e

    #@34
    const/4 v4, 0x1

    #@35
    .line 1144
    .local v4, abbrevMonth:Z
    :goto_35
    and-int/lit8 v64, p6, 0x20

    #@37
    if-eqz v64, :cond_161

    #@39
    const/16 v35, 0x1

    #@3b
    .line 1145
    .local v35, noMonthDay:Z
    :goto_3b
    const/high16 v64, 0x2

    #@3d
    and-int v64, v64, p6

    #@3f
    if-eqz v64, :cond_165

    #@41
    const/16 v38, 0x1

    #@43
    .line 1150
    .local v38, numericDate:Z
    :goto_43
    cmp-long v64, p2, p4

    #@45
    if-nez v64, :cond_169

    #@47
    const/16 v32, 0x1

    #@49
    .line 1153
    .local v32, isInstant:Z
    :goto_49
    if-eqz p7, :cond_16d

    #@4b
    .line 1154
    new-instance v45, Landroid/text/format/Time;

    #@4d
    move-object/from16 v0, v45

    #@4f
    move-object/from16 v1, p7

    #@51
    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@54
    .line 1160
    .local v45, startDate:Landroid/text/format/Time;
    :goto_54
    move-object/from16 v0, v45

    #@56
    move-wide/from16 v1, p2

    #@58
    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    #@5b
    .line 1164
    if-eqz v32, :cond_183

    #@5d
    .line 1165
    move-object/from16 v14, v45

    #@5f
    .line 1166
    .local v14, endDate:Landroid/text/format/Time;
    const/4 v12, 0x0

    #@60
    .line 1181
    .local v12, dayDistance:I
    :goto_60
    if-nez v32, :cond_8d

    #@62
    iget v0, v14, Landroid/text/format/Time;->hour:I

    #@64
    move/from16 v64, v0

    #@66
    iget v0, v14, Landroid/text/format/Time;->minute:I

    #@68
    move/from16 v65, v0

    #@6a
    or-int v64, v64, v65

    #@6c
    iget v0, v14, Landroid/text/format/Time;->second:I

    #@6e
    move/from16 v65, v0

    #@70
    or-int v64, v64, v65

    #@72
    if-nez v64, :cond_8d

    #@74
    if-eqz v42, :cond_7c

    #@76
    const/16 v64, 0x1

    #@78
    move/from16 v0, v64

    #@7a
    if-gt v12, v0, :cond_8d

    #@7c
    .line 1184
    :cond_7c
    iget v0, v14, Landroid/text/format/Time;->monthDay:I

    #@7e
    move/from16 v64, v0

    #@80
    add-int/lit8 v64, v64, -0x1

    #@82
    move/from16 v0, v64

    #@84
    iput v0, v14, Landroid/text/format/Time;->monthDay:I

    #@86
    .line 1185
    const/16 v64, 0x1

    #@88
    move/from16 v0, v64

    #@8a
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->normalize(Z)J

    #@8d
    .line 1188
    :cond_8d
    move-object/from16 v0, v45

    #@8f
    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    #@91
    move/from16 v47, v0

    #@93
    .line 1189
    .local v47, startDay:I
    move-object/from16 v0, v45

    #@95
    iget v0, v0, Landroid/text/format/Time;->month:I

    #@97
    move/from16 v50, v0

    #@99
    .line 1190
    .local v50, startMonthNum:I
    move-object/from16 v0, v45

    #@9b
    iget v0, v0, Landroid/text/format/Time;->year:I

    #@9d
    move/from16 v57, v0

    #@9f
    .line 1192
    .local v57, startYear:I
    iget v0, v14, Landroid/text/format/Time;->monthDay:I

    #@a1
    move/from16 v16, v0

    #@a3
    .line 1193
    .local v16, endDay:I
    iget v0, v14, Landroid/text/format/Time;->month:I

    #@a5
    move/from16 v19, v0

    #@a7
    .line 1194
    .local v19, endMonthNum:I
    iget v0, v14, Landroid/text/format/Time;->year:I

    #@a9
    move/from16 v26, v0

    #@ab
    .line 1196
    .local v26, endYear:I
    const-string v56, ""

    #@ad
    .line 1197
    .local v56, startWeekDayString:Ljava/lang/String;
    const-string v25, ""

    #@af
    .line 1198
    .local v25, endWeekDayString:Ljava/lang/String;
    if-eqz v43, :cond_c3

    #@b1
    .line 1199
    const-string v63, ""

    #@b3
    .line 1200
    .local v63, weekDayFormat:Ljava/lang/String;
    if-eqz v6, :cond_1c1

    #@b5
    .line 1201
    const-string v63, "%a"

    #@b7
    .line 1205
    :goto_b7
    move-object/from16 v0, v45

    #@b9
    move-object/from16 v1, v63

    #@bb
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@be
    move-result-object v56

    #@bf
    .line 1206
    if-eqz v32, :cond_1c5

    #@c1
    move-object/from16 v25, v56

    #@c3
    .line 1209
    .end local v63           #weekDayFormat:Ljava/lang/String;
    :cond_c3
    :goto_c3
    const-string v55, ""

    #@c5
    .line 1210
    .local v55, startTimeString:Ljava/lang/String;
    const-string v24, ""

    #@c7
    .line 1211
    .local v24, endTimeString:Ljava/lang/String;
    if-eqz v42, :cond_fc

    #@c9
    .line 1212
    const-string v54, ""

    #@cb
    .line 1213
    .local v54, startTimeFormat:Ljava/lang/String;
    const-string v23, ""

    #@cd
    .line 1214
    .local v23, endTimeFormat:Ljava/lang/String;
    move/from16 v0, p6

    #@cf
    and-int/lit16 v0, v0, 0x80

    #@d1
    move/from16 v64, v0

    #@d3
    if-eqz v64, :cond_1cd

    #@d5
    const/16 v29, 0x1

    #@d7
    .line 1215
    .local v29, force24Hour:Z
    :goto_d7
    and-int/lit8 v64, p6, 0x40

    #@d9
    if-eqz v64, :cond_1d1

    #@db
    const/16 v28, 0x1

    #@dd
    .line 1217
    .local v28, force12Hour:Z
    :goto_dd
    if-eqz v29, :cond_1d5

    #@df
    .line 1218
    const/16 v61, 0x1

    #@e1
    .line 1224
    .local v61, use24Hour:Z
    :goto_e1
    if-eqz v61, :cond_1e1

    #@e3
    .line 1225
    const v64, 0x1040049

    #@e6
    move-object/from16 v0, v39

    #@e8
    move/from16 v1, v64

    #@ea
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@ed
    move-result-object v23

    #@ee
    move-object/from16 v54, v23

    #@f0
    .line 1296
    :cond_f0
    :goto_f0
    move-object/from16 v0, v45

    #@f2
    move-object/from16 v1, v54

    #@f4
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@f7
    move-result-object v55

    #@f8
    .line 1297
    if-eqz v32, :cond_346

    #@fa
    move-object/from16 v24, v55

    #@fc
    .line 1304
    .end local v23           #endTimeFormat:Ljava/lang/String;
    .end local v28           #force12Hour:Z
    .end local v29           #force24Hour:Z
    .end local v54           #startTimeFormat:Ljava/lang/String;
    .end local v61           #use24Hour:Z
    :cond_fc
    :goto_fc
    if-eqz v44, :cond_34e

    #@fe
    .line 1321
    :goto_fe
    if-eqz v38, :cond_377

    #@100
    .line 1322
    const v64, 0x104004e

    #@103
    move-object/from16 v0, v39

    #@105
    move/from16 v1, v64

    #@107
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@10a
    move-result-object v13

    #@10b
    .line 1353
    .local v13, defaultDateFormat:Ljava/lang/String;
    :goto_10b
    if-eqz v43, :cond_3fa

    #@10d
    .line 1354
    if-eqz v42, :cond_3ed

    #@10f
    .line 1355
    const v64, 0x1040067

    #@112
    move-object/from16 v0, v39

    #@114
    move/from16 v1, v64

    #@116
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@119
    move-result-object v30

    #@11a
    .line 1367
    .local v30, fullFormat:Ljava/lang/String;
    :goto_11a
    if-eqz v35, :cond_416

    #@11c
    move/from16 v0, v50

    #@11e
    move/from16 v1, v19

    #@120
    if-ne v0, v1, :cond_416

    #@122
    move/from16 v0, v57

    #@124
    move/from16 v1, v26

    #@126
    if-ne v0, v1, :cond_416

    #@128
    .line 1369
    const-string v64, "%s"

    #@12a
    const/16 v65, 0x1

    #@12c
    move/from16 v0, v65

    #@12e
    new-array v0, v0, [Ljava/lang/Object;

    #@130
    move-object/from16 v65, v0

    #@132
    const/16 v66, 0x0

    #@134
    move-object/from16 v0, v45

    #@136
    invoke-virtual {v0, v13}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@139
    move-result-object v67

    #@13a
    aput-object v67, v65, v66

    #@13c
    move-object/from16 v0, p1

    #@13e
    move-object/from16 v1, v64

    #@140
    move-object/from16 v2, v65

    #@142
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@145
    move-result-object v64

    #@146
    .line 1515
    :goto_146
    return-object v64

    #@147
    .line 1137
    .end local v4           #abbrevMonth:Z
    .end local v6           #abbrevWeekDay:Z
    .end local v12           #dayDistance:I
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    .end local v14           #endDate:Landroid/text/format/Time;
    .end local v16           #endDay:I
    .end local v19           #endMonthNum:I
    .end local v24           #endTimeString:Ljava/lang/String;
    .end local v25           #endWeekDayString:Ljava/lang/String;
    .end local v26           #endYear:I
    .end local v30           #fullFormat:Ljava/lang/String;
    .end local v32           #isInstant:Z
    .end local v35           #noMonthDay:Z
    .end local v37           #noYear:Z
    .end local v38           #numericDate:Z
    .end local v42           #showTime:Z
    .end local v43           #showWeekDay:Z
    .end local v44           #showYear:Z
    .end local v45           #startDate:Landroid/text/format/Time;
    .end local v47           #startDay:I
    .end local v50           #startMonthNum:I
    .end local v55           #startTimeString:Ljava/lang/String;
    .end local v56           #startWeekDayString:Ljava/lang/String;
    .end local v57           #startYear:I
    .end local v62           #useUTC:Z
    :cond_147
    const/16 v42, 0x0

    #@149
    goto/16 :goto_a

    #@14b
    .line 1138
    .restart local v42       #showTime:Z
    :cond_14b
    const/16 v43, 0x0

    #@14d
    goto/16 :goto_10

    #@14f
    .line 1139
    .restart local v43       #showWeekDay:Z
    :cond_14f
    const/16 v44, 0x0

    #@151
    goto/16 :goto_16

    #@153
    .line 1140
    .restart local v44       #showYear:Z
    :cond_153
    const/16 v37, 0x0

    #@155
    goto/16 :goto_1c

    #@157
    .line 1141
    .restart local v37       #noYear:Z
    :cond_157
    const/16 v62, 0x0

    #@159
    goto/16 :goto_26

    #@15b
    .line 1142
    .restart local v62       #useUTC:Z
    :cond_15b
    const/4 v6, 0x0

    #@15c
    goto/16 :goto_2e

    #@15e
    .line 1143
    .restart local v6       #abbrevWeekDay:Z
    :cond_15e
    const/4 v4, 0x0

    #@15f
    goto/16 :goto_35

    #@161
    .line 1144
    .restart local v4       #abbrevMonth:Z
    :cond_161
    const/16 v35, 0x0

    #@163
    goto/16 :goto_3b

    #@165
    .line 1145
    .restart local v35       #noMonthDay:Z
    :cond_165
    const/16 v38, 0x0

    #@167
    goto/16 :goto_43

    #@169
    .line 1150
    .restart local v38       #numericDate:Z
    :cond_169
    const/16 v32, 0x0

    #@16b
    goto/16 :goto_49

    #@16d
    .line 1155
    .restart local v32       #isInstant:Z
    :cond_16d
    if-eqz v62, :cond_17c

    #@16f
    .line 1156
    new-instance v45, Landroid/text/format/Time;

    #@171
    const-string v64, "UTC"

    #@173
    move-object/from16 v0, v45

    #@175
    move-object/from16 v1, v64

    #@177
    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@17a
    .restart local v45       #startDate:Landroid/text/format/Time;
    goto/16 :goto_54

    #@17c
    .line 1158
    .end local v45           #startDate:Landroid/text/format/Time;
    :cond_17c
    new-instance v45, Landroid/text/format/Time;

    #@17e
    invoke-direct/range {v45 .. v45}, Landroid/text/format/Time;-><init>()V

    #@181
    .restart local v45       #startDate:Landroid/text/format/Time;
    goto/16 :goto_54

    #@183
    .line 1168
    :cond_183
    if-eqz p7, :cond_1af

    #@185
    .line 1169
    new-instance v14, Landroid/text/format/Time;

    #@187
    move-object/from16 v0, p7

    #@189
    invoke-direct {v14, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@18c
    .line 1175
    .restart local v14       #endDate:Landroid/text/format/Time;
    :goto_18c
    move-wide/from16 v0, p4

    #@18e
    invoke-virtual {v14, v0, v1}, Landroid/text/format/Time;->set(J)V

    #@191
    .line 1176
    move-object/from16 v0, v45

    #@193
    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    #@195
    move-wide/from16 v64, v0

    #@197
    move-wide/from16 v0, p2

    #@199
    move-wide/from16 v2, v64

    #@19b
    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@19e
    move-result v48

    #@19f
    .line 1177
    .local v48, startJulianDay:I
    iget-wide v0, v14, Landroid/text/format/Time;->gmtoff:J

    #@1a1
    move-wide/from16 v64, v0

    #@1a3
    move-wide/from16 v0, p4

    #@1a5
    move-wide/from16 v2, v64

    #@1a7
    invoke-static {v0, v1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@1aa
    move-result v17

    #@1ab
    .line 1178
    .local v17, endJulianDay:I
    sub-int v12, v17, v48

    #@1ad
    .restart local v12       #dayDistance:I
    goto/16 :goto_60

    #@1af
    .line 1170
    .end local v12           #dayDistance:I
    .end local v14           #endDate:Landroid/text/format/Time;
    .end local v17           #endJulianDay:I
    .end local v48           #startJulianDay:I
    :cond_1af
    if-eqz v62, :cond_1bb

    #@1b1
    .line 1171
    new-instance v14, Landroid/text/format/Time;

    #@1b3
    const-string v64, "UTC"

    #@1b5
    move-object/from16 v0, v64

    #@1b7
    invoke-direct {v14, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@1ba
    .restart local v14       #endDate:Landroid/text/format/Time;
    goto :goto_18c

    #@1bb
    .line 1173
    .end local v14           #endDate:Landroid/text/format/Time;
    :cond_1bb
    new-instance v14, Landroid/text/format/Time;

    #@1bd
    invoke-direct {v14}, Landroid/text/format/Time;-><init>()V

    #@1c0
    .restart local v14       #endDate:Landroid/text/format/Time;
    goto :goto_18c

    #@1c1
    .line 1203
    .restart local v12       #dayDistance:I
    .restart local v16       #endDay:I
    .restart local v19       #endMonthNum:I
    .restart local v25       #endWeekDayString:Ljava/lang/String;
    .restart local v26       #endYear:I
    .restart local v47       #startDay:I
    .restart local v50       #startMonthNum:I
    .restart local v56       #startWeekDayString:Ljava/lang/String;
    .restart local v57       #startYear:I
    .restart local v63       #weekDayFormat:Ljava/lang/String;
    :cond_1c1
    const-string v63, "%A"

    #@1c3
    goto/16 :goto_b7

    #@1c5
    .line 1206
    :cond_1c5
    move-object/from16 v0, v63

    #@1c7
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@1ca
    move-result-object v25

    #@1cb
    goto/16 :goto_c3

    #@1cd
    .line 1214
    .end local v63           #weekDayFormat:Ljava/lang/String;
    .restart local v23       #endTimeFormat:Ljava/lang/String;
    .restart local v24       #endTimeString:Ljava/lang/String;
    .restart local v54       #startTimeFormat:Ljava/lang/String;
    .restart local v55       #startTimeString:Ljava/lang/String;
    :cond_1cd
    const/16 v29, 0x0

    #@1cf
    goto/16 :goto_d7

    #@1d1
    .line 1215
    .restart local v29       #force24Hour:Z
    :cond_1d1
    const/16 v28, 0x0

    #@1d3
    goto/16 :goto_dd

    #@1d5
    .line 1219
    .restart local v28       #force12Hour:Z
    :cond_1d5
    if-eqz v28, :cond_1db

    #@1d7
    .line 1220
    const/16 v61, 0x0

    #@1d9
    .restart local v61       #use24Hour:Z
    goto/16 :goto_e1

    #@1db
    .line 1222
    .end local v61           #use24Hour:Z
    :cond_1db
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    #@1de
    move-result v61

    #@1df
    .restart local v61       #use24Hour:Z
    goto/16 :goto_e1

    #@1e1
    .line 1228
    :cond_1e1
    const v64, 0x84000

    #@1e4
    and-int v64, v64, p6

    #@1e6
    if-eqz v64, :cond_29a

    #@1e8
    const/4 v5, 0x1

    #@1e9
    .line 1229
    .local v5, abbrevTime:Z
    :goto_1e9
    move/from16 v0, p6

    #@1eb
    and-int/lit16 v0, v0, 0x100

    #@1ed
    move/from16 v64, v0

    #@1ef
    if-eqz v64, :cond_29d

    #@1f1
    const/4 v7, 0x1

    #@1f2
    .line 1230
    .local v7, capAMPM:Z
    :goto_1f2
    move/from16 v0, p6

    #@1f4
    and-int/lit16 v0, v0, 0x200

    #@1f6
    move/from16 v64, v0

    #@1f8
    if-eqz v64, :cond_2a0

    #@1fa
    const/16 v36, 0x1

    #@1fc
    .line 1231
    .local v36, noNoon:Z
    :goto_1fc
    move/from16 v0, p6

    #@1fe
    and-int/lit16 v0, v0, 0x400

    #@200
    move/from16 v64, v0

    #@202
    if-eqz v64, :cond_2a4

    #@204
    const/4 v9, 0x1

    #@205
    .line 1232
    .local v9, capNoon:Z
    :goto_205
    move/from16 v0, p6

    #@207
    and-int/lit16 v0, v0, 0x800

    #@209
    move/from16 v64, v0

    #@20b
    if-eqz v64, :cond_2a7

    #@20d
    const/16 v34, 0x1

    #@20f
    .line 1233
    .local v34, noMidnight:Z
    :goto_20f
    move/from16 v0, p6

    #@211
    and-int/lit16 v0, v0, 0x1000

    #@213
    move/from16 v64, v0

    #@215
    if-eqz v64, :cond_2ab

    #@217
    const/4 v8, 0x1

    #@218
    .line 1235
    .local v8, capMidnight:Z
    :goto_218
    move-object/from16 v0, v45

    #@21a
    iget v0, v0, Landroid/text/format/Time;->minute:I

    #@21c
    move/from16 v64, v0

    #@21e
    if-nez v64, :cond_2ae

    #@220
    move-object/from16 v0, v45

    #@222
    iget v0, v0, Landroid/text/format/Time;->second:I

    #@224
    move/from16 v64, v0

    #@226
    if-nez v64, :cond_2ae

    #@228
    const/16 v52, 0x1

    #@22a
    .line 1236
    .local v52, startOnTheHour:Z
    :goto_22a
    iget v0, v14, Landroid/text/format/Time;->minute:I

    #@22c
    move/from16 v64, v0

    #@22e
    if-nez v64, :cond_2b2

    #@230
    iget v0, v14, Landroid/text/format/Time;->second:I

    #@232
    move/from16 v64, v0

    #@234
    if-nez v64, :cond_2b2

    #@236
    const/16 v21, 0x1

    #@238
    .line 1237
    .local v21, endOnTheHour:Z
    :goto_238
    if-eqz v5, :cond_2c1

    #@23a
    if-eqz v52, :cond_2c1

    #@23c
    .line 1238
    if-eqz v7, :cond_2b5

    #@23e
    .line 1239
    const v64, 0x1040362

    #@241
    move-object/from16 v0, v39

    #@243
    move/from16 v1, v64

    #@245
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@248
    move-result-object v54

    #@249
    .line 1255
    :goto_249
    if-nez v32, :cond_279

    #@24b
    .line 1256
    if-eqz v5, :cond_2ea

    #@24d
    if-eqz v21, :cond_2ea

    #@24f
    .line 1257
    if-eqz v7, :cond_2dd

    #@251
    .line 1258
    const v64, 0x1040362

    #@254
    move-object/from16 v0, v39

    #@256
    move/from16 v1, v64

    #@258
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@25b
    move-result-object v23

    #@25c
    .line 1270
    :goto_25c
    iget v0, v14, Landroid/text/format/Time;->hour:I

    #@25e
    move/from16 v64, v0

    #@260
    const/16 v65, 0xc

    #@262
    move/from16 v0, v64

    #@264
    move/from16 v1, v65

    #@266
    if-ne v0, v1, :cond_313

    #@268
    if-eqz v21, :cond_313

    #@26a
    if-nez v36, :cond_313

    #@26c
    .line 1271
    if-eqz v9, :cond_306

    #@26e
    .line 1272
    const v64, 0x10403e4

    #@271
    move-object/from16 v0, v39

    #@273
    move/from16 v1, v64

    #@275
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@278
    move-result-object v23

    #@279
    .line 1285
    :cond_279
    :goto_279
    move-object/from16 v0, v45

    #@27b
    iget v0, v0, Landroid/text/format/Time;->hour:I

    #@27d
    move/from16 v64, v0

    #@27f
    const/16 v65, 0xc

    #@281
    move/from16 v0, v64

    #@283
    move/from16 v1, v65

    #@285
    if-ne v0, v1, :cond_f0

    #@287
    if-eqz v52, :cond_f0

    #@289
    if-nez v36, :cond_f0

    #@28b
    .line 1286
    if-eqz v9, :cond_339

    #@28d
    .line 1287
    const v64, 0x10403e4

    #@290
    move-object/from16 v0, v39

    #@292
    move/from16 v1, v64

    #@294
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@297
    move-result-object v54

    #@298
    goto/16 :goto_f0

    #@29a
    .line 1228
    .end local v5           #abbrevTime:Z
    .end local v7           #capAMPM:Z
    .end local v8           #capMidnight:Z
    .end local v9           #capNoon:Z
    .end local v21           #endOnTheHour:Z
    .end local v34           #noMidnight:Z
    .end local v36           #noNoon:Z
    .end local v52           #startOnTheHour:Z
    :cond_29a
    const/4 v5, 0x0

    #@29b
    goto/16 :goto_1e9

    #@29d
    .line 1229
    .restart local v5       #abbrevTime:Z
    :cond_29d
    const/4 v7, 0x0

    #@29e
    goto/16 :goto_1f2

    #@2a0
    .line 1230
    .restart local v7       #capAMPM:Z
    :cond_2a0
    const/16 v36, 0x0

    #@2a2
    goto/16 :goto_1fc

    #@2a4
    .line 1231
    .restart local v36       #noNoon:Z
    :cond_2a4
    const/4 v9, 0x0

    #@2a5
    goto/16 :goto_205

    #@2a7
    .line 1232
    .restart local v9       #capNoon:Z
    :cond_2a7
    const/16 v34, 0x0

    #@2a9
    goto/16 :goto_20f

    #@2ab
    .line 1233
    .restart local v34       #noMidnight:Z
    :cond_2ab
    const/4 v8, 0x0

    #@2ac
    goto/16 :goto_218

    #@2ae
    .line 1235
    .restart local v8       #capMidnight:Z
    :cond_2ae
    const/16 v52, 0x0

    #@2b0
    goto/16 :goto_22a

    #@2b2
    .line 1236
    .restart local v52       #startOnTheHour:Z
    :cond_2b2
    const/16 v21, 0x0

    #@2b4
    goto :goto_238

    #@2b5
    .line 1241
    .restart local v21       #endOnTheHour:Z
    :cond_2b5
    const v64, 0x1040361

    #@2b8
    move-object/from16 v0, v39

    #@2ba
    move/from16 v1, v64

    #@2bc
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2bf
    move-result-object v54

    #@2c0
    goto :goto_249

    #@2c1
    .line 1244
    :cond_2c1
    if-eqz v7, :cond_2d0

    #@2c3
    .line 1245
    const v64, 0x104004b

    #@2c6
    move-object/from16 v0, v39

    #@2c8
    move/from16 v1, v64

    #@2ca
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2cd
    move-result-object v54

    #@2ce
    goto/16 :goto_249

    #@2d0
    .line 1247
    :cond_2d0
    const v64, 0x104004a

    #@2d3
    move-object/from16 v0, v39

    #@2d5
    move/from16 v1, v64

    #@2d7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2da
    move-result-object v54

    #@2db
    goto/16 :goto_249

    #@2dd
    .line 1260
    :cond_2dd
    const v64, 0x1040361

    #@2e0
    move-object/from16 v0, v39

    #@2e2
    move/from16 v1, v64

    #@2e4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2e7
    move-result-object v23

    #@2e8
    goto/16 :goto_25c

    #@2ea
    .line 1263
    :cond_2ea
    if-eqz v7, :cond_2f9

    #@2ec
    .line 1264
    const v64, 0x104004b

    #@2ef
    move-object/from16 v0, v39

    #@2f1
    move/from16 v1, v64

    #@2f3
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2f6
    move-result-object v23

    #@2f7
    goto/16 :goto_25c

    #@2f9
    .line 1266
    :cond_2f9
    const v64, 0x104004a

    #@2fc
    move-object/from16 v0, v39

    #@2fe
    move/from16 v1, v64

    #@300
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@303
    move-result-object v23

    #@304
    goto/16 :goto_25c

    #@306
    .line 1274
    :cond_306
    const v64, 0x10403e3

    #@309
    move-object/from16 v0, v39

    #@30b
    move/from16 v1, v64

    #@30d
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@310
    move-result-object v23

    #@311
    goto/16 :goto_279

    #@313
    .line 1276
    :cond_313
    iget v0, v14, Landroid/text/format/Time;->hour:I

    #@315
    move/from16 v64, v0

    #@317
    if-nez v64, :cond_279

    #@319
    if-eqz v21, :cond_279

    #@31b
    if-nez v34, :cond_279

    #@31d
    .line 1277
    if-eqz v8, :cond_32c

    #@31f
    .line 1278
    const v64, 0x10403e6

    #@322
    move-object/from16 v0, v39

    #@324
    move/from16 v1, v64

    #@326
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@329
    move-result-object v23

    #@32a
    goto/16 :goto_279

    #@32c
    .line 1280
    :cond_32c
    const v64, 0x10403e5

    #@32f
    move-object/from16 v0, v39

    #@331
    move/from16 v1, v64

    #@333
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@336
    move-result-object v23

    #@337
    goto/16 :goto_279

    #@339
    .line 1289
    :cond_339
    const v64, 0x10403e3

    #@33c
    move-object/from16 v0, v39

    #@33e
    move/from16 v1, v64

    #@340
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@343
    move-result-object v54

    #@344
    goto/16 :goto_f0

    #@346
    .line 1297
    .end local v5           #abbrevTime:Z
    .end local v7           #capAMPM:Z
    .end local v8           #capMidnight:Z
    .end local v9           #capNoon:Z
    .end local v21           #endOnTheHour:Z
    .end local v34           #noMidnight:Z
    .end local v36           #noNoon:Z
    .end local v52           #startOnTheHour:Z
    :cond_346
    move-object/from16 v0, v23

    #@348
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@34b
    move-result-object v24

    #@34c
    goto/16 :goto_fc

    #@34e
    .line 1308
    .end local v23           #endTimeFormat:Ljava/lang/String;
    .end local v28           #force12Hour:Z
    .end local v29           #force24Hour:Z
    .end local v54           #startTimeFormat:Ljava/lang/String;
    .end local v61           #use24Hour:Z
    :cond_34e
    if-eqz v37, :cond_354

    #@350
    .line 1310
    const/16 v44, 0x0

    #@352
    goto/16 :goto_fe

    #@354
    .line 1311
    :cond_354
    move/from16 v0, v57

    #@356
    move/from16 v1, v26

    #@358
    if-eq v0, v1, :cond_35e

    #@35a
    .line 1312
    const/16 v44, 0x1

    #@35c
    goto/16 :goto_fe

    #@35e
    .line 1315
    :cond_35e
    new-instance v10, Landroid/text/format/Time;

    #@360
    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    #@363
    .line 1316
    .local v10, currentTime:Landroid/text/format/Time;
    invoke-virtual {v10}, Landroid/text/format/Time;->setToNow()V

    #@366
    .line 1317
    iget v0, v10, Landroid/text/format/Time;->year:I

    #@368
    move/from16 v64, v0

    #@36a
    move/from16 v0, v57

    #@36c
    move/from16 v1, v64

    #@36e
    if-eq v0, v1, :cond_374

    #@370
    const/16 v44, 0x1

    #@372
    :goto_372
    goto/16 :goto_fe

    #@374
    :cond_374
    const/16 v44, 0x0

    #@376
    goto :goto_372

    #@377
    .line 1323
    .end local v10           #currentTime:Landroid/text/format/Time;
    :cond_377
    if-eqz v44, :cond_3b3

    #@379
    .line 1324
    if-eqz v4, :cond_397

    #@37b
    .line 1325
    if-eqz v35, :cond_38a

    #@37d
    .line 1326
    const v64, 0x104005c

    #@380
    move-object/from16 v0, v39

    #@382
    move/from16 v1, v64

    #@384
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@387
    move-result-object v13

    #@388
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@38a
    .line 1328
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_38a
    const v64, 0x1040056

    #@38d
    move-object/from16 v0, v39

    #@38f
    move/from16 v1, v64

    #@391
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@394
    move-result-object v13

    #@395
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@397
    .line 1331
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_397
    if-eqz v35, :cond_3a6

    #@399
    .line 1332
    const v64, 0x1040059

    #@39c
    move-object/from16 v0, v39

    #@39e
    move/from16 v1, v64

    #@3a0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3a3
    move-result-object v13

    #@3a4
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3a6
    .line 1334
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_3a6
    const v64, 0x1040051

    #@3a9
    move-object/from16 v0, v39

    #@3ab
    move/from16 v1, v64

    #@3ad
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3b0
    move-result-object v13

    #@3b1
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3b3
    .line 1338
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_3b3
    if-eqz v4, :cond_3d1

    #@3b5
    .line 1339
    if-eqz v35, :cond_3c4

    #@3b7
    .line 1340
    const v64, 0x104005b

    #@3ba
    move-object/from16 v0, v39

    #@3bc
    move/from16 v1, v64

    #@3be
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3c1
    move-result-object v13

    #@3c2
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3c4
    .line 1342
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_3c4
    const v64, 0x104005a

    #@3c7
    move-object/from16 v0, v39

    #@3c9
    move/from16 v1, v64

    #@3cb
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3ce
    move-result-object v13

    #@3cf
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3d1
    .line 1345
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_3d1
    if-eqz v35, :cond_3e0

    #@3d3
    .line 1346
    const v64, 0x1040058

    #@3d6
    move-object/from16 v0, v39

    #@3d8
    move/from16 v1, v64

    #@3da
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3dd
    move-result-object v13

    #@3de
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3e0
    .line 1348
    .end local v13           #defaultDateFormat:Ljava/lang/String;
    :cond_3e0
    const v64, 0x1040057

    #@3e3
    move-object/from16 v0, v39

    #@3e5
    move/from16 v1, v64

    #@3e7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3ea
    move-result-object v13

    #@3eb
    .restart local v13       #defaultDateFormat:Ljava/lang/String;
    goto/16 :goto_10b

    #@3ed
    .line 1357
    :cond_3ed
    const v64, 0x1040068

    #@3f0
    move-object/from16 v0, v39

    #@3f2
    move/from16 v1, v64

    #@3f4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@3f7
    move-result-object v30

    #@3f8
    .restart local v30       #fullFormat:Ljava/lang/String;
    goto/16 :goto_11a

    #@3fa
    .line 1360
    .end local v30           #fullFormat:Ljava/lang/String;
    :cond_3fa
    if-eqz v42, :cond_409

    #@3fc
    .line 1361
    const v64, 0x1040069

    #@3ff
    move-object/from16 v0, v39

    #@401
    move/from16 v1, v64

    #@403
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@406
    move-result-object v30

    #@407
    .restart local v30       #fullFormat:Ljava/lang/String;
    goto/16 :goto_11a

    #@409
    .line 1363
    .end local v30           #fullFormat:Ljava/lang/String;
    :cond_409
    const v64, 0x104005e

    #@40c
    move-object/from16 v0, v39

    #@40e
    move/from16 v1, v64

    #@410
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@413
    move-result-object v30

    #@414
    .restart local v30       #fullFormat:Ljava/lang/String;
    goto/16 :goto_11a

    #@416
    .line 1372
    :cond_416
    move/from16 v0, v57

    #@418
    move/from16 v1, v26

    #@41a
    if-ne v0, v1, :cond_41e

    #@41c
    if-eqz v35, :cond_454

    #@41e
    .line 1376
    :cond_41e
    move-object/from16 v0, v45

    #@420
    invoke-virtual {v0, v13}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@423
    move-result-object v46

    #@424
    .line 1377
    .local v46, startDateString:Ljava/lang/String;
    invoke-virtual {v14, v13}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@427
    move-result-object v15

    #@428
    .line 1381
    .local v15, endDateString:Ljava/lang/String;
    const/16 v64, 0x6

    #@42a
    move/from16 v0, v64

    #@42c
    new-array v0, v0, [Ljava/lang/Object;

    #@42e
    move-object/from16 v64, v0

    #@430
    const/16 v65, 0x0

    #@432
    aput-object v56, v64, v65

    #@434
    const/16 v65, 0x1

    #@436
    aput-object v46, v64, v65

    #@438
    const/16 v65, 0x2

    #@43a
    aput-object v55, v64, v65

    #@43c
    const/16 v65, 0x3

    #@43e
    aput-object v25, v64, v65

    #@440
    const/16 v65, 0x4

    #@442
    aput-object v15, v64, v65

    #@444
    const/16 v65, 0x5

    #@446
    aput-object v24, v64, v65

    #@448
    move-object/from16 v0, p1

    #@44a
    move-object/from16 v1, v30

    #@44c
    move-object/from16 v2, v64

    #@44e
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@451
    move-result-object v64

    #@452
    goto/16 :goto_146

    #@454
    .line 1388
    .end local v15           #endDateString:Ljava/lang/String;
    .end local v46           #startDateString:Ljava/lang/String;
    :cond_454
    if-eqz v38, :cond_50e

    #@456
    .line 1389
    const-string v33, "%m"

    #@458
    .line 1396
    .local v33, monthFormat:Ljava/lang/String;
    :goto_458
    move-object/from16 v0, v45

    #@45a
    move-object/from16 v1, v33

    #@45c
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@45f
    move-result-object v51

    #@460
    .line 1397
    .local v51, startMonthString:Ljava/lang/String;
    const-string v64, "%-d"

    #@462
    move-object/from16 v0, v45

    #@464
    move-object/from16 v1, v64

    #@466
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@469
    move-result-object v49

    #@46a
    .line 1398
    .local v49, startMonthDayString:Ljava/lang/String;
    const-string v64, "%Y"

    #@46c
    move-object/from16 v0, v45

    #@46e
    move-object/from16 v1, v64

    #@470
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@473
    move-result-object v58

    #@474
    .line 1400
    .local v58, startYearString:Ljava/lang/String;
    if-eqz v32, :cond_521

    #@476
    const/16 v20, 0x0

    #@478
    .line 1401
    .local v20, endMonthString:Ljava/lang/String;
    :goto_478
    if-eqz v32, :cond_529

    #@47a
    const/16 v18, 0x0

    #@47c
    .line 1402
    .local v18, endMonthDayString:Ljava/lang/String;
    :goto_47c
    if-eqz v32, :cond_533

    #@47e
    const/16 v27, 0x0

    #@480
    .line 1404
    .local v27, endYearString:Ljava/lang/String;
    :goto_480
    move-object/from16 v53, v51

    #@482
    .line 1405
    .local v53, startStandaloneMonthString:Ljava/lang/String;
    move-object/from16 v22, v20

    #@484
    .line 1407
    .local v22, endStandaloneMonthString:Ljava/lang/String;
    if-nez v38, :cond_4aa

    #@486
    if-nez v4, :cond_4aa

    #@488
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@48b
    move-result-object v64

    #@48c
    invoke-virtual/range {v64 .. v64}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@48f
    move-result-object v64

    #@490
    const-string v65, "fa"

    #@492
    invoke-virtual/range {v64 .. v65}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@495
    move-result v64

    #@496
    if-eqz v64, :cond_4aa

    #@498
    .line 1408
    const-string v64, "%-B"

    #@49a
    move-object/from16 v0, v45

    #@49c
    move-object/from16 v1, v64

    #@49e
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@4a1
    move-result-object v53

    #@4a2
    .line 1409
    const-string v64, "%-B"

    #@4a4
    move-object/from16 v0, v64

    #@4a6
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@4a9
    move-result-object v22

    #@4aa
    .line 1412
    :cond_4aa
    move/from16 v0, v50

    #@4ac
    move/from16 v1, v19

    #@4ae
    if-eq v0, v1, :cond_53d

    #@4b0
    .line 1418
    const/16 v31, 0x0

    #@4b2
    .line 1419
    .local v31, index:I
    if-eqz v43, :cond_4b6

    #@4b4
    const/16 v31, 0x1

    #@4b6
    .line 1420
    :cond_4b6
    if-eqz v44, :cond_4ba

    #@4b8
    add-int/lit8 v31, v31, 0x2

    #@4ba
    .line 1421
    :cond_4ba
    if-eqz v42, :cond_4be

    #@4bc
    add-int/lit8 v31, v31, 0x4

    #@4be
    .line 1422
    :cond_4be
    if-eqz v38, :cond_4c2

    #@4c0
    add-int/lit8 v31, v31, 0x8

    #@4c2
    .line 1423
    :cond_4c2
    sget-object v64, Landroid/text/format/DateUtils;->sameYearTable:[I

    #@4c4
    aget v40, v64, v31

    #@4c6
    .line 1424
    .local v40, resId:I
    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4c9
    move-result-object v30

    #@4ca
    .line 1428
    const/16 v64, 0xc

    #@4cc
    move/from16 v0, v64

    #@4ce
    new-array v0, v0, [Ljava/lang/Object;

    #@4d0
    move-object/from16 v64, v0

    #@4d2
    const/16 v65, 0x0

    #@4d4
    aput-object v56, v64, v65

    #@4d6
    const/16 v65, 0x1

    #@4d8
    aput-object v51, v64, v65

    #@4da
    const/16 v65, 0x2

    #@4dc
    aput-object v49, v64, v65

    #@4de
    const/16 v65, 0x3

    #@4e0
    aput-object v58, v64, v65

    #@4e2
    const/16 v65, 0x4

    #@4e4
    aput-object v55, v64, v65

    #@4e6
    const/16 v65, 0x5

    #@4e8
    aput-object v25, v64, v65

    #@4ea
    const/16 v65, 0x6

    #@4ec
    aput-object v20, v64, v65

    #@4ee
    const/16 v65, 0x7

    #@4f0
    aput-object v18, v64, v65

    #@4f2
    const/16 v65, 0x8

    #@4f4
    aput-object v27, v64, v65

    #@4f6
    const/16 v65, 0x9

    #@4f8
    aput-object v24, v64, v65

    #@4fa
    const/16 v65, 0xa

    #@4fc
    aput-object v53, v64, v65

    #@4fe
    const/16 v65, 0xb

    #@500
    aput-object v22, v64, v65

    #@502
    move-object/from16 v0, p1

    #@504
    move-object/from16 v1, v30

    #@506
    move-object/from16 v2, v64

    #@508
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@50b
    move-result-object v64

    #@50c
    goto/16 :goto_146

    #@50e
    .line 1390
    .end local v18           #endMonthDayString:Ljava/lang/String;
    .end local v20           #endMonthString:Ljava/lang/String;
    .end local v22           #endStandaloneMonthString:Ljava/lang/String;
    .end local v27           #endYearString:Ljava/lang/String;
    .end local v31           #index:I
    .end local v33           #monthFormat:Ljava/lang/String;
    .end local v40           #resId:I
    .end local v49           #startMonthDayString:Ljava/lang/String;
    .end local v51           #startMonthString:Ljava/lang/String;
    .end local v53           #startStandaloneMonthString:Ljava/lang/String;
    .end local v58           #startYearString:Ljava/lang/String;
    :cond_50e
    if-eqz v4, :cond_51d

    #@510
    .line 1391
    const v64, 0x104007d

    #@513
    move-object/from16 v0, v39

    #@515
    move/from16 v1, v64

    #@517
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@51a
    move-result-object v33

    #@51b
    .restart local v33       #monthFormat:Ljava/lang/String;
    goto/16 :goto_458

    #@51d
    .line 1394
    .end local v33           #monthFormat:Ljava/lang/String;
    :cond_51d
    const-string v33, "%B"

    #@51f
    .restart local v33       #monthFormat:Ljava/lang/String;
    goto/16 :goto_458

    #@521
    .line 1400
    .restart local v49       #startMonthDayString:Ljava/lang/String;
    .restart local v51       #startMonthString:Ljava/lang/String;
    .restart local v58       #startYearString:Ljava/lang/String;
    :cond_521
    move-object/from16 v0, v33

    #@523
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@526
    move-result-object v20

    #@527
    goto/16 :goto_478

    #@529
    .line 1401
    .restart local v20       #endMonthString:Ljava/lang/String;
    :cond_529
    const-string v64, "%-d"

    #@52b
    move-object/from16 v0, v64

    #@52d
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@530
    move-result-object v18

    #@531
    goto/16 :goto_47c

    #@533
    .line 1402
    .restart local v18       #endMonthDayString:Ljava/lang/String;
    :cond_533
    const-string v64, "%Y"

    #@535
    move-object/from16 v0, v64

    #@537
    invoke-virtual {v14, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@53a
    move-result-object v27

    #@53b
    goto/16 :goto_480

    #@53d
    .line 1436
    .restart local v22       #endStandaloneMonthString:Ljava/lang/String;
    .restart local v27       #endYearString:Ljava/lang/String;
    .restart local v53       #startStandaloneMonthString:Ljava/lang/String;
    :cond_53d
    move/from16 v0, v47

    #@53f
    move/from16 v1, v16

    #@541
    if-eq v0, v1, :cond_5a1

    #@543
    .line 1438
    const/16 v31, 0x0

    #@545
    .line 1439
    .restart local v31       #index:I
    if-eqz v43, :cond_549

    #@547
    const/16 v31, 0x1

    #@549
    .line 1440
    :cond_549
    if-eqz v44, :cond_54d

    #@54b
    add-int/lit8 v31, v31, 0x2

    #@54d
    .line 1441
    :cond_54d
    if-eqz v42, :cond_551

    #@54f
    add-int/lit8 v31, v31, 0x4

    #@551
    .line 1442
    :cond_551
    if-eqz v38, :cond_555

    #@553
    add-int/lit8 v31, v31, 0x8

    #@555
    .line 1443
    :cond_555
    sget-object v64, Landroid/text/format/DateUtils;->sameMonthTable:[I

    #@557
    aget v40, v64, v31

    #@559
    .line 1444
    .restart local v40       #resId:I
    invoke-virtual/range {v39 .. v40}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@55c
    move-result-object v30

    #@55d
    .line 1448
    const/16 v64, 0xc

    #@55f
    move/from16 v0, v64

    #@561
    new-array v0, v0, [Ljava/lang/Object;

    #@563
    move-object/from16 v64, v0

    #@565
    const/16 v65, 0x0

    #@567
    aput-object v56, v64, v65

    #@569
    const/16 v65, 0x1

    #@56b
    aput-object v51, v64, v65

    #@56d
    const/16 v65, 0x2

    #@56f
    aput-object v49, v64, v65

    #@571
    const/16 v65, 0x3

    #@573
    aput-object v58, v64, v65

    #@575
    const/16 v65, 0x4

    #@577
    aput-object v55, v64, v65

    #@579
    const/16 v65, 0x5

    #@57b
    aput-object v25, v64, v65

    #@57d
    const/16 v65, 0x6

    #@57f
    aput-object v20, v64, v65

    #@581
    const/16 v65, 0x7

    #@583
    aput-object v18, v64, v65

    #@585
    const/16 v65, 0x8

    #@587
    aput-object v27, v64, v65

    #@589
    const/16 v65, 0x9

    #@58b
    aput-object v24, v64, v65

    #@58d
    const/16 v65, 0xa

    #@58f
    aput-object v53, v64, v65

    #@591
    const/16 v65, 0xb

    #@593
    aput-object v22, v64, v65

    #@595
    move-object/from16 v0, p1

    #@597
    move-object/from16 v1, v30

    #@599
    move-object/from16 v2, v64

    #@59b
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@59e
    move-result-object v64

    #@59f
    goto/16 :goto_146

    #@5a1
    .line 1457
    .end local v31           #index:I
    .end local v40           #resId:I
    :cond_5a1
    and-int/lit8 v64, p6, 0x10

    #@5a3
    if-eqz v64, :cond_5f2

    #@5a5
    const/16 v41, 0x1

    #@5a7
    .line 1460
    .local v41, showDate:Z
    :goto_5a7
    if-nez v42, :cond_5af

    #@5a9
    if-nez v41, :cond_5af

    #@5ab
    if-nez v43, :cond_5af

    #@5ad
    const/16 v41, 0x1

    #@5af
    .line 1463
    :cond_5af
    const-string v60, ""

    #@5b1
    .line 1464
    .local v60, timeString:Ljava/lang/String;
    if-eqz v42, :cond_5b7

    #@5b3
    .line 1467
    if-eqz v32, :cond_5f5

    #@5b5
    .line 1470
    move-object/from16 v60, v55

    #@5b7
    .line 1480
    :cond_5b7
    :goto_5b7
    const-string v30, ""

    #@5b9
    .line 1481
    const-string v11, ""

    #@5bb
    .line 1482
    .local v11, dateString:Ljava/lang/String;
    if-eqz v41, :cond_64d

    #@5bd
    .line 1483
    move-object/from16 v0, v45

    #@5bf
    invoke-virtual {v0, v13}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@5c2
    move-result-object v11

    #@5c3
    .line 1484
    if-eqz v43, :cond_625

    #@5c5
    .line 1485
    if-eqz v42, :cond_619

    #@5c7
    .line 1487
    const v64, 0x104006a

    #@5ca
    move-object/from16 v0, v39

    #@5cc
    move/from16 v1, v64

    #@5ce
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5d1
    move-result-object v30

    #@5d2
    .line 1515
    :cond_5d2
    :goto_5d2
    const/16 v64, 0x3

    #@5d4
    move/from16 v0, v64

    #@5d6
    new-array v0, v0, [Ljava/lang/Object;

    #@5d8
    move-object/from16 v64, v0

    #@5da
    const/16 v65, 0x0

    #@5dc
    aput-object v60, v64, v65

    #@5de
    const/16 v65, 0x1

    #@5e0
    aput-object v56, v64, v65

    #@5e2
    const/16 v65, 0x2

    #@5e4
    aput-object v11, v64, v65

    #@5e6
    move-object/from16 v0, p1

    #@5e8
    move-object/from16 v1, v30

    #@5ea
    move-object/from16 v2, v64

    #@5ec
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@5ef
    move-result-object v64

    #@5f0
    goto/16 :goto_146

    #@5f2
    .line 1457
    .end local v11           #dateString:Ljava/lang/String;
    .end local v41           #showDate:Z
    .end local v60           #timeString:Ljava/lang/String;
    :cond_5f2
    const/16 v41, 0x0

    #@5f4
    goto :goto_5a7

    #@5f5
    .line 1473
    .restart local v41       #showDate:Z
    .restart local v60       #timeString:Ljava/lang/String;
    :cond_5f5
    const v64, 0x104005d

    #@5f8
    move-object/from16 v0, v39

    #@5fa
    move/from16 v1, v64

    #@5fc
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5ff
    move-result-object v59

    #@600
    .line 1475
    .local v59, timeFormat:Ljava/lang/String;
    const/16 v64, 0x2

    #@602
    move/from16 v0, v64

    #@604
    new-array v0, v0, [Ljava/lang/Object;

    #@606
    move-object/from16 v64, v0

    #@608
    const/16 v65, 0x0

    #@60a
    aput-object v55, v64, v65

    #@60c
    const/16 v65, 0x1

    #@60e
    aput-object v24, v64, v65

    #@610
    move-object/from16 v0, v59

    #@612
    move-object/from16 v1, v64

    #@614
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@617
    move-result-object v60

    #@618
    goto :goto_5b7

    #@619
    .line 1490
    .end local v59           #timeFormat:Ljava/lang/String;
    .restart local v11       #dateString:Ljava/lang/String;
    :cond_619
    const v64, 0x104006b

    #@61c
    move-object/from16 v0, v39

    #@61e
    move/from16 v1, v64

    #@620
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@623
    move-result-object v30

    #@624
    goto :goto_5d2

    #@625
    .line 1493
    :cond_625
    if-eqz v42, :cond_633

    #@627
    .line 1495
    const v64, 0x1040055

    #@62a
    move-object/from16 v0, v39

    #@62c
    move/from16 v1, v64

    #@62e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@631
    move-result-object v30

    #@632
    goto :goto_5d2

    #@633
    .line 1498
    :cond_633
    const-string v64, "%s"

    #@635
    const/16 v65, 0x1

    #@637
    move/from16 v0, v65

    #@639
    new-array v0, v0, [Ljava/lang/Object;

    #@63b
    move-object/from16 v65, v0

    #@63d
    const/16 v66, 0x0

    #@63f
    aput-object v11, v65, v66

    #@641
    move-object/from16 v0, p1

    #@643
    move-object/from16 v1, v64

    #@645
    move-object/from16 v2, v65

    #@647
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@64a
    move-result-object v64

    #@64b
    goto/16 :goto_146

    #@64d
    .line 1501
    :cond_64d
    if-eqz v43, :cond_678

    #@64f
    .line 1502
    if-eqz v42, :cond_65e

    #@651
    .line 1504
    const v64, 0x104006c

    #@654
    move-object/from16 v0, v39

    #@656
    move/from16 v1, v64

    #@658
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@65b
    move-result-object v30

    #@65c
    goto/16 :goto_5d2

    #@65e
    .line 1507
    :cond_65e
    const-string v64, "%s"

    #@660
    const/16 v65, 0x1

    #@662
    move/from16 v0, v65

    #@664
    new-array v0, v0, [Ljava/lang/Object;

    #@666
    move-object/from16 v65, v0

    #@668
    const/16 v66, 0x0

    #@66a
    aput-object v56, v65, v66

    #@66c
    move-object/from16 v0, p1

    #@66e
    move-object/from16 v1, v64

    #@670
    move-object/from16 v2, v65

    #@672
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@675
    move-result-object v64

    #@676
    goto/16 :goto_146

    #@678
    .line 1509
    :cond_678
    if-eqz v42, :cond_5d2

    #@67a
    .line 1510
    const-string v64, "%s"

    #@67c
    const/16 v65, 0x1

    #@67e
    move/from16 v0, v65

    #@680
    new-array v0, v0, [Ljava/lang/Object;

    #@682
    move-object/from16 v65, v0

    #@684
    const/16 v66, 0x0

    #@686
    aput-object v60, v65, v66

    #@688
    move-object/from16 v0, p1

    #@68a
    move-object/from16 v1, v64

    #@68c
    move-object/from16 v2, v65

    #@68e
    invoke-virtual {v0, v1, v2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    #@691
    move-result-object v64

    #@692
    goto/16 :goto_146
.end method

.method public static formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;
    .registers 10
    .parameter "context"
    .parameter "millis"
    .parameter "flags"

    #@0
    .prologue
    .line 1561
    move-object v0, p0

    #@1
    move-wide v1, p1

    #@2
    move-wide v3, p1

    #@3
    move v5, p3

    #@4
    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static formatDuration(J)Ljava/lang/CharSequence;
    .registers 14
    .parameter "millis"

    #@0
    .prologue
    const-wide/32 v10, 0x36ee80

    #@3
    const-wide/32 v6, 0xea60

    #@6
    const/4 v9, 0x1

    #@7
    const/4 v8, 0x0

    #@8
    .line 617
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@b
    move-result-object v2

    #@c
    .line 618
    .local v2, res:Landroid/content/res/Resources;
    cmp-long v4, p0, v10

    #@e
    if-ltz v4, :cond_26

    #@10
    .line 619
    const-wide/32 v4, 0x1b7740

    #@13
    add-long/2addr v4, p0

    #@14
    div-long/2addr v4, v10

    #@15
    long-to-int v0, v4

    #@16
    .line 620
    .local v0, hours:I
    const v4, 0x1130013

    #@19
    new-array v5, v9, [Ljava/lang/Object;

    #@1b
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v6

    #@1f
    aput-object v6, v5, v8

    #@21
    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    .line 628
    .end local v0           #hours:I
    :goto_25
    return-object v4

    #@26
    .line 622
    :cond_26
    cmp-long v4, p0, v6

    #@28
    if-ltz v4, :cond_3f

    #@2a
    .line 623
    const-wide/16 v4, 0x7530

    #@2c
    add-long/2addr v4, p0

    #@2d
    div-long/2addr v4, v6

    #@2e
    long-to-int v1, v4

    #@2f
    .line 624
    .local v1, minutes:I
    const v4, 0x1130012

    #@32
    new-array v5, v9, [Ljava/lang/Object;

    #@34
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v6

    #@38
    aput-object v6, v5, v8

    #@3a
    invoke-virtual {v2, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    goto :goto_25

    #@3f
    .line 627
    .end local v1           #minutes:I
    :cond_3f
    const-wide/16 v4, 0x1f4

    #@41
    add-long/2addr v4, p0

    #@42
    const-wide/16 v6, 0x3e8

    #@44
    div-long/2addr v4, v6

    #@45
    long-to-int v3, v4

    #@46
    .line 628
    .local v3, seconds:I
    const v4, 0x1130011

    #@49
    new-array v5, v9, [Ljava/lang/Object;

    #@4b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v6

    #@4f
    aput-object v6, v5, v8

    #@51
    invoke-virtual {v2, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    #@54
    move-result-object v4

    #@55
    goto :goto_25
.end method

.method public static formatElapsedTime(J)Ljava/lang/String;
    .registers 3
    .parameter "elapsedSeconds"

    #@0
    .prologue
    .line 639
    const/4 v0, 0x0

    #@1
    invoke-static {v0, p0, p1}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;J)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static formatElapsedTime(Ljava/lang/StringBuilder;J)Ljava/lang/String;
    .registers 17
    .parameter "recycle"
    .parameter "elapsedSeconds"

    #@0
    .prologue
    .line 650
    invoke-static {}, Landroid/text/format/DateUtils;->initFormatStrings()V

    #@3
    .line 652
    const-wide/16 v2, 0x0

    #@5
    .line 653
    .local v2, hours:J
    const-wide/16 v4, 0x0

    #@7
    .line 654
    .local v4, minutes:J
    const-wide/16 v6, 0x0

    #@9
    .line 656
    .local v6, seconds:J
    const-wide/16 v0, 0xe10

    #@b
    cmp-long v0, p1, v0

    #@d
    if-ltz v0, :cond_17

    #@f
    .line 657
    const-wide/16 v0, 0xe10

    #@11
    div-long v2, p1, v0

    #@13
    .line 658
    const-wide/16 v0, 0xe10

    #@15
    mul-long/2addr v0, v2

    #@16
    sub-long/2addr p1, v0

    #@17
    .line 660
    :cond_17
    const-wide/16 v0, 0x3c

    #@19
    cmp-long v0, p1, v0

    #@1b
    if-ltz v0, :cond_25

    #@1d
    .line 661
    const-wide/16 v0, 0x3c

    #@1f
    div-long v4, p1, v0

    #@21
    .line 662
    const-wide/16 v0, 0x3c

    #@23
    mul-long/2addr v0, v4

    #@24
    sub-long/2addr p1, v0

    #@25
    .line 664
    :cond_25
    move-wide v6, p1

    #@26
    .line 667
    const-wide/16 v0, 0x0

    #@28
    cmp-long v0, v2, v0

    #@2a
    if-lez v0, :cond_34

    #@2c
    .line 668
    sget-object v1, Landroid/text/format/DateUtils;->sElapsedFormatHMMSS:Ljava/lang/String;

    #@2e
    move-object v0, p0

    #@2f
    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;Ljava/lang/String;JJJ)Ljava/lang/String;

    #@32
    move-result-object v0

    #@33
    .line 670
    :goto_33
    return-object v0

    #@34
    :cond_34
    sget-object v9, Landroid/text/format/DateUtils;->sElapsedFormatMMSS:Ljava/lang/String;

    #@36
    move-object v8, p0

    #@37
    move-wide v10, v4

    #@38
    move-wide v12, v6

    #@39
    invoke-static/range {v8 .. v13}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;Ljava/lang/String;JJ)Ljava/lang/String;

    #@3c
    move-result-object v0

    #@3d
    goto :goto_33
.end method

.method private static formatElapsedTime(Ljava/lang/StringBuilder;Ljava/lang/String;JJ)Ljava/lang/String;
    .registers 12
    .parameter "recycle"
    .parameter "format"
    .parameter "minutes"
    .parameter "seconds"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 715
    const-string v2, "%1$02d:%2$02d"

    #@4
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v2

    #@8
    if-eqz v2, :cond_32

    #@a
    .line 716
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@d
    move-result-object v2

    #@e
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@11
    move-result-object v2

    #@12
    iget-char v1, v2, Llibcore/icu/LocaleData;->zeroDigit:C

    #@14
    .line 718
    .local v1, zeroDigit:C
    move-object v0, p0

    #@15
    .line 719
    .local v0, sb:Ljava/lang/StringBuilder;
    if-nez v0, :cond_2e

    #@17
    .line 720
    new-instance v0, Ljava/lang/StringBuilder;

    #@19
    .end local v0           #sb:Ljava/lang/StringBuilder;
    const/16 v2, 0x8

    #@1b
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@1e
    .line 724
    .restart local v0       #sb:Ljava/lang/StringBuilder;
    :goto_1e
    invoke-static {v0, p2, p3, v4, v1}, Landroid/text/format/DateUtils;->append(Ljava/lang/StringBuilder;JZC)V

    #@21
    .line 725
    const/16 v2, 0x3a

    #@23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@26
    .line 726
    invoke-static {v0, p4, p5, v5, v1}, Landroid/text/format/DateUtils;->append(Ljava/lang/StringBuilder;JZC)V

    #@29
    .line 727
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 729
    .end local v0           #sb:Ljava/lang/StringBuilder;
    .end local v1           #zeroDigit:C
    :goto_2d
    return-object v2

    #@2e
    .line 722
    .restart local v0       #sb:Ljava/lang/StringBuilder;
    .restart local v1       #zeroDigit:C
    :cond_2e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    #@31
    goto :goto_1e

    #@32
    .line 729
    .end local v0           #sb:Ljava/lang/StringBuilder;
    .end local v1           #zeroDigit:C
    :cond_32
    const/4 v2, 0x2

    #@33
    new-array v2, v2, [Ljava/lang/Object;

    #@35
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@38
    move-result-object v3

    #@39
    aput-object v3, v2, v4

    #@3b
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3e
    move-result-object v3

    #@3f
    aput-object v3, v2, v5

    #@41
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    goto :goto_2d
.end method

.method private static formatElapsedTime(Ljava/lang/StringBuilder;Ljava/lang/String;JJJ)Ljava/lang/String;
    .registers 14
    .parameter "recycle"
    .parameter "format"
    .parameter "hours"
    .parameter "minutes"
    .parameter "seconds"

    #@0
    .prologue
    const/16 v3, 0x3a

    #@2
    const/4 v5, 0x1

    #@3
    const/4 v4, 0x0

    #@4
    .line 690
    const-string v2, "%1$d:%2$02d:%3$02d"

    #@6
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_38

    #@c
    .line 691
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@f
    move-result-object v2

    #@10
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@13
    move-result-object v2

    #@14
    iget-char v1, v2, Llibcore/icu/LocaleData;->zeroDigit:C

    #@16
    .line 693
    .local v1, zeroDigit:C
    move-object v0, p0

    #@17
    .line 694
    .local v0, sb:Ljava/lang/StringBuilder;
    if-nez v0, :cond_34

    #@19
    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    #@1b
    .end local v0           #sb:Ljava/lang/StringBuilder;
    const/16 v2, 0x8

    #@1d
    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@20
    .line 699
    .restart local v0       #sb:Ljava/lang/StringBuilder;
    :goto_20
    invoke-static {v0, p2, p3, v4, v1}, Landroid/text/format/DateUtils;->append(Ljava/lang/StringBuilder;JZC)V

    #@23
    .line 700
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@26
    .line 701
    invoke-static {v0, p4, p5, v5, v1}, Landroid/text/format/DateUtils;->append(Ljava/lang/StringBuilder;JZC)V

    #@29
    .line 702
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    .line 703
    invoke-static {v0, p6, p7, v5, v1}, Landroid/text/format/DateUtils;->append(Ljava/lang/StringBuilder;JZC)V

    #@2f
    .line 704
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    .line 706
    .end local v0           #sb:Ljava/lang/StringBuilder;
    .end local v1           #zeroDigit:C
    :goto_33
    return-object v2

    #@34
    .line 697
    .restart local v0       #sb:Ljava/lang/StringBuilder;
    .restart local v1       #zeroDigit:C
    :cond_34
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    #@37
    goto :goto_20

    #@38
    .line 706
    .end local v0           #sb:Ljava/lang/StringBuilder;
    .end local v1           #zeroDigit:C
    :cond_38
    const/4 v2, 0x3

    #@39
    new-array v2, v2, [Ljava/lang/Object;

    #@3b
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3e
    move-result-object v3

    #@3f
    aput-object v3, v2, v4

    #@41
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v3

    #@45
    aput-object v3, v2, v5

    #@47
    const/4 v3, 0x2

    #@48
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4b
    move-result-object v4

    #@4c
    aput-object v4, v2, v3

    #@4e
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    goto :goto_33
.end method

.method public static final formatSameDayTime(JJII)Ljava/lang/CharSequence;
    .registers 14
    .parameter "then"
    .parameter "now"
    .parameter "dateStyle"
    .parameter "timeStyle"

    #@0
    .prologue
    const/4 v7, 0x5

    #@1
    const/4 v6, 0x2

    #@2
    const/4 v5, 0x1

    #@3
    .line 752
    new-instance v2, Ljava/util/GregorianCalendar;

    #@5
    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    #@8
    .line 753
    .local v2, thenCal:Ljava/util/Calendar;
    invoke-virtual {v2, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    #@b
    .line 754
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@e
    move-result-object v3

    #@f
    .line 755
    .local v3, thenDate:Ljava/util/Date;
    new-instance v1, Ljava/util/GregorianCalendar;

    #@11
    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    #@14
    .line 756
    .local v1, nowCal:Ljava/util/Calendar;
    invoke-virtual {v1, p2, p3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    #@17
    .line 760
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    #@1a
    move-result v4

    #@1b
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    #@1e
    move-result v5

    #@1f
    if-ne v4, v5, :cond_3e

    #@21
    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    #@24
    move-result v4

    #@25
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    #@28
    move-result v5

    #@29
    if-ne v4, v5, :cond_3e

    #@2b
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    #@2e
    move-result v4

    #@2f
    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    #@32
    move-result v5

    #@33
    if-ne v4, v5, :cond_3e

    #@35
    .line 763
    invoke-static {p5}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    #@38
    move-result-object v0

    #@39
    .line 767
    .local v0, f:Ljava/text/DateFormat;
    :goto_39
    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    return-object v4

    #@3e
    .line 765
    .end local v0           #f:Ljava/text/DateFormat;
    :cond_3e
    invoke-static {p4}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    #@41
    move-result-object v0

    #@42
    .restart local v0       #f:Ljava/text/DateFormat;
    goto :goto_39
.end method

.method public static getAMPMString(I)Ljava/lang/String;
    .registers 3
    .parameter "ampm"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 266
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@7
    move-result-object v0

    #@8
    iget-object v0, v0, Llibcore/icu/LocaleData;->amPm:[Ljava/lang/String;

    #@a
    add-int/lit8 v1, p0, 0x0

    #@c
    aget-object v0, v0, v1

    #@e
    return-object v0
.end method

.method public static getDayOfWeekString(II)Ljava/lang/String;
    .registers 5
    .parameter "dayOfWeek"
    .parameter "abbrev"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 244
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@7
    move-result-object v0

    #@8
    .line 246
    .local v0, d:Llibcore/icu/LocaleData;
    sparse-switch p1, :sswitch_data_20

    #@b
    .line 252
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortWeekdayNames:[Ljava/lang/String;

    #@d
    .line 254
    .local v1, names:[Ljava/lang/String;
    :goto_d
    aget-object v2, v1, p0

    #@f
    return-object v2

    #@10
    .line 247
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_10
    iget-object v1, v0, Llibcore/icu/LocaleData;->longWeekdayNames:[Ljava/lang/String;

    #@12
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@13
    .line 248
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_13
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortWeekdayNames:[Ljava/lang/String;

    #@15
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@16
    .line 249
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_16
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortWeekdayNames:[Ljava/lang/String;

    #@18
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@19
    .line 250
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_19
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortWeekdayNames:[Ljava/lang/String;

    #@1b
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1c
    .line 251
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_1c
    iget-object v1, v0, Llibcore/icu/LocaleData;->tinyWeekdayNames:[Ljava/lang/String;

    #@1e
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1f
    .line 246
    nop

    #@20
    :sswitch_data_20
    .sparse-switch
        0xa -> :sswitch_10
        0x14 -> :sswitch_13
        0x1e -> :sswitch_16
        0x28 -> :sswitch_19
        0x32 -> :sswitch_1c
    .end sparse-switch
.end method

.method public static getMonthString(II)Ljava/lang/String;
    .registers 5
    .parameter "month"
    .parameter "abbrev"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 286
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@7
    move-result-object v0

    #@8
    .line 288
    .local v0, d:Llibcore/icu/LocaleData;
    sparse-switch p1, :sswitch_data_20

    #@b
    .line 294
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@d
    .line 296
    .local v1, names:[Ljava/lang/String;
    :goto_d
    aget-object v2, v1, p0

    #@f
    return-object v2

    #@10
    .line 289
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_10
    iget-object v1, v0, Llibcore/icu/LocaleData;->longMonthNames:[Ljava/lang/String;

    #@12
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@13
    .line 290
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_13
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@15
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@16
    .line 291
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_16
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@18
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@19
    .line 292
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_19
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@1b
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1c
    .line 293
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_1c
    iget-object v1, v0, Llibcore/icu/LocaleData;->tinyMonthNames:[Ljava/lang/String;

    #@1e
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1f
    .line 288
    nop

    #@20
    :sswitch_data_20
    .sparse-switch
        0xa -> :sswitch_10
        0x14 -> :sswitch_13
        0x1e -> :sswitch_16
        0x28 -> :sswitch_19
        0x32 -> :sswitch_1c
    .end sparse-switch
.end method

.method private static declared-synchronized getNumberOfDaysPassed(JJ)J
    .registers 10
    .parameter "date1"
    .parameter "date2"

    #@0
    .prologue
    .line 469
    const-class v3, Landroid/text/format/DateUtils;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@5
    if-nez v2, :cond_e

    #@7
    .line 470
    new-instance v2, Landroid/text/format/Time;

    #@9
    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    #@c
    sput-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@e
    .line 472
    :cond_e
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@10
    invoke-virtual {v2, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@13
    .line 473
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@15
    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    #@17
    invoke-static {p0, p1, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@1a
    move-result v0

    #@1b
    .line 474
    .local v0, day1:I
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@1d
    invoke-virtual {v2, p2, p3}, Landroid/text/format/Time;->set(J)V

    #@20
    .line 475
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@22
    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    #@24
    invoke-static {p2, p3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@27
    move-result v1

    #@28
    .line 476
    .local v1, day2:I
    sub-int v2, v1, v0

    #@2a
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_31

    #@2d
    move-result v2

    #@2e
    int-to-long v4, v2

    #@2f
    monitor-exit v3

    #@30
    return-wide v4

    #@31
    .line 469
    .end local v0           #day1:I
    .end local v1           #day2:I
    :catchall_31
    move-exception v2

    #@32
    monitor-exit v3

    #@33
    throw v2
.end method

.method public static getRelativeDateTimeString(Landroid/content/Context;JJJI)Ljava/lang/CharSequence;
    .registers 27
    .parameter "c"
    .parameter "time"
    .parameter "minResolution"
    .parameter "transitionResolution"
    .parameter "flags"

    #@0
    .prologue
    .line 504
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v15

    #@4
    .line 506
    .local v15, r:Landroid/content/res/Resources;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@7
    move-result-wide v13

    #@8
    .line 507
    .local v13, now:J
    sub-long v3, v13, p1

    #@a
    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    #@d
    move-result-wide v11

    #@e
    .line 512
    .local v11, duration:J
    const-wide/32 v3, 0x240c8400

    #@11
    cmp-long v3, p5, v3

    #@13
    if-lez v3, :cond_43

    #@15
    .line 513
    const-wide/32 p5, 0x240c8400

    #@18
    .line 518
    :cond_18
    :goto_18
    const/4 v8, 0x1

    #@19
    move-object/from16 v3, p0

    #@1b
    move-wide/from16 v4, p1

    #@1d
    move-wide/from16 v6, p1

    #@1f
    invoke-static/range {v3 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@22
    move-result-object v18

    #@23
    .line 521
    .local v18, timeClause:Ljava/lang/CharSequence;
    cmp-long v3, v11, p5

    #@25
    if-gez v3, :cond_4e

    #@27
    move-wide/from16 v3, p1

    #@29
    move-wide v5, v13

    #@2a
    move-wide/from16 v7, p3

    #@2c
    move/from16 v9, p7

    #@2e
    .line 522
    invoke-static/range {v3 .. v9}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    #@31
    move-result-object v16

    #@32
    .line 523
    .local v16, relativeClause:Ljava/lang/CharSequence;
    const v3, 0x10403e2

    #@35
    const/4 v4, 0x2

    #@36
    new-array v4, v4, [Ljava/lang/Object;

    #@38
    const/4 v5, 0x0

    #@39
    aput-object v16, v4, v5

    #@3b
    const/4 v5, 0x1

    #@3c
    aput-object v18, v4, v5

    #@3e
    invoke-virtual {v15, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@41
    move-result-object v17

    #@42
    .line 529
    .end local v16           #relativeClause:Ljava/lang/CharSequence;
    .local v17, result:Ljava/lang/String;
    :goto_42
    return-object v17

    #@43
    .line 514
    .end local v17           #result:Ljava/lang/String;
    .end local v18           #timeClause:Ljava/lang/CharSequence;
    :cond_43
    const-wide/32 v3, 0x5265c00

    #@46
    cmp-long v3, p5, v3

    #@48
    if-gez v3, :cond_18

    #@4a
    .line 515
    const-wide/32 p5, 0x5265c00

    #@4d
    goto :goto_18

    #@4e
    .line 525
    .restart local v18       #timeClause:Ljava/lang/CharSequence;
    :cond_4e
    const/4 v3, 0x0

    #@4f
    move-object/from16 v0, p0

    #@51
    move-wide/from16 v1, p1

    #@53
    invoke-static {v0, v1, v2, v3}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    #@56
    move-result-object v10

    #@57
    .line 526
    .local v10, dateClause:Ljava/lang/CharSequence;
    const v3, 0x1040054

    #@5a
    const/4 v4, 0x2

    #@5b
    new-array v4, v4, [Ljava/lang/Object;

    #@5d
    const/4 v5, 0x0

    #@5e
    aput-object v10, v4, v5

    #@60
    const/4 v5, 0x1

    #@61
    aput-object v18, v4, v5

    #@63
    invoke-virtual {v15, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@66
    move-result-object v17

    #@67
    .restart local v17       #result:Ljava/lang/String;
    goto :goto_42
.end method

.method private static final getRelativeDayString(Landroid/content/res/Resources;JJ)Ljava/lang/String;
    .registers 19
    .parameter "r"
    .parameter "day"
    .parameter "today"

    #@0
    .prologue
    .line 543
    new-instance v10, Landroid/text/format/Time;

    #@2
    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    #@5
    .line 544
    .local v10, startTime:Landroid/text/format/Time;
    move-wide v0, p1

    #@6
    invoke-virtual {v10, v0, v1}, Landroid/text/format/Time;->set(J)V

    #@9
    .line 545
    new-instance v3, Landroid/text/format/Time;

    #@b
    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    #@e
    .line 546
    .local v3, currentTime:Landroid/text/format/Time;
    move-wide/from16 v0, p3

    #@10
    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    #@13
    .line 548
    iget-wide v11, v10, Landroid/text/format/Time;->gmtoff:J

    #@15
    move-wide v0, p1

    #@16
    invoke-static {v0, v1, v11, v12}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@19
    move-result v9

    #@1a
    .line 549
    .local v9, startDay:I
    iget-wide v11, v3, Landroid/text/format/Time;->gmtoff:J

    #@1c
    move-wide/from16 v0, p3

    #@1e
    invoke-static {v0, v1, v11, v12}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@21
    move-result v2

    #@22
    .line 551
    .local v2, currentDay:I
    sub-int v11, v2, v9

    #@24
    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    #@27
    move-result v4

    #@28
    .line 552
    .local v4, days:I
    cmp-long v11, p3, p1

    #@2a
    if-lez v11, :cond_45

    #@2c
    const/4 v7, 0x1

    #@2d
    .line 555
    .local v7, past:Z
    :goto_2d
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@30
    move-result-object v11

    #@31
    iget-object v6, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@33
    .line 556
    .local v6, locale:Ljava/util/Locale;
    if-nez v6, :cond_39

    #@35
    .line 557
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@38
    move-result-object v6

    #@39
    .line 559
    :cond_39
    const/4 v11, 0x1

    #@3a
    if-ne v4, v11, :cond_4e

    #@3c
    .line 560
    if-eqz v7, :cond_47

    #@3e
    .line 561
    invoke-static {v6}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@41
    move-result-object v11

    #@42
    iget-object v11, v11, Llibcore/icu/LocaleData;->yesterday:Ljava/lang/String;

    #@44
    .line 577
    :goto_44
    return-object v11

    #@45
    .line 552
    .end local v6           #locale:Ljava/util/Locale;
    .end local v7           #past:Z
    :cond_45
    const/4 v7, 0x0

    #@46
    goto :goto_2d

    #@47
    .line 563
    .restart local v6       #locale:Ljava/util/Locale;
    .restart local v7       #past:Z
    :cond_47
    invoke-static {v6}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@4a
    move-result-object v11

    #@4b
    iget-object v11, v11, Llibcore/icu/LocaleData;->tomorrow:Ljava/lang/String;

    #@4d
    goto :goto_44

    #@4e
    .line 565
    :cond_4e
    if-nez v4, :cond_57

    #@50
    .line 566
    invoke-static {v6}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@53
    move-result-object v11

    #@54
    iget-object v11, v11, Llibcore/icu/LocaleData;->today:Ljava/lang/String;

    #@56
    goto :goto_44

    #@57
    .line 570
    :cond_57
    if-eqz v7, :cond_6f

    #@59
    .line 571
    const v8, 0x1130004

    #@5c
    .line 576
    .local v8, resId:I
    :goto_5c
    invoke-virtual {p0, v8, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    .line 577
    .local v5, format:Ljava/lang/String;
    const/4 v11, 0x1

    #@61
    new-array v11, v11, [Ljava/lang/Object;

    #@63
    const/4 v12, 0x0

    #@64
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@67
    move-result-object v13

    #@68
    aput-object v13, v11, v12

    #@6a
    invoke-static {v5, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@6d
    move-result-object v11

    #@6e
    goto :goto_44

    #@6f
    .line 573
    .end local v5           #format:Ljava/lang/String;
    .end local v8           #resId:I
    :cond_6f
    const v8, 0x1130008

    #@72
    .restart local v8       #resId:I
    goto :goto_5c
.end method

.method public static getRelativeTimeSpanString(J)Ljava/lang/CharSequence;
    .registers 8
    .parameter "startTime"

    #@0
    .prologue
    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v2

    #@4
    const-wide/32 v4, 0xea60

    #@7
    move-wide v0, p0

    #@8
    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    #@b
    move-result-object v0

    #@c
    return-object v0
.end method

.method public static getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;
    .registers 13
    .parameter "time"
    .parameter "now"
    .parameter "minResolution"

    #@0
    .prologue
    .line 357
    const v6, 0x10014

    #@3
    .local v6, flags:I
    move-wide v0, p0

    #@4
    move-wide v2, p2

    #@5
    move-wide v4, p4

    #@6
    .line 358
    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public static getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;
    .registers 22
    .parameter "time"
    .parameter "now"
    .parameter "minResolution"
    .parameter "flags"

    #@0
    .prologue
    .line 383
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v13

    #@4
    .line 384
    .local v13, r:Landroid/content/res/Resources;
    const/high16 v0, 0xc

    #@6
    and-int v0, v0, p6

    #@8
    if-eqz v0, :cond_43

    #@a
    const/4 v6, 0x1

    #@b
    .line 386
    .local v6, abbrevRelative:Z
    :goto_b
    cmp-long v0, p2, p0

    #@d
    if-ltz v0, :cond_45

    #@f
    const/4 v12, 0x1

    #@10
    .line 387
    .local v12, past:Z
    :goto_10
    sub-long v0, p2, p0

    #@12
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    #@15
    move-result-wide v9

    #@16
    .line 391
    .local v9, duration:J
    const-wide/32 v0, 0xea60

    #@19
    cmp-long v0, v9, v0

    #@1b
    if-gez v0, :cond_54

    #@1d
    const-wide/32 v0, 0xea60

    #@20
    cmp-long v0, p4, v0

    #@22
    if-gez v0, :cond_54

    #@24
    .line 392
    const-wide/16 v0, 0x3e8

    #@26
    div-long v7, v9, v0

    #@28
    .line 393
    .local v7, count:J
    if-eqz v12, :cond_4a

    #@2a
    .line 394
    if-eqz v6, :cond_47

    #@2c
    .line 395
    const v14, 0x1130009

    #@2f
    .line 457
    .local v14, resId:I
    :goto_2f
    long-to-int v0, v7

    #@30
    invoke-virtual {v13, v14, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    #@33
    move-result-object v11

    #@34
    .line 458
    .local v11, format:Ljava/lang/String;
    const/4 v0, 0x1

    #@35
    new-array v0, v0, [Ljava/lang/Object;

    #@37
    const/4 v1, 0x0

    #@38
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3b
    move-result-object v2

    #@3c
    aput-object v2, v0, v1

    #@3e
    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@41
    move-result-object v0

    #@42
    .end local v7           #count:J
    .end local v11           #format:Ljava/lang/String;
    .end local v14           #resId:I
    :goto_42
    return-object v0

    #@43
    .line 384
    .end local v6           #abbrevRelative:Z
    .end local v9           #duration:J
    .end local v12           #past:Z
    :cond_43
    const/4 v6, 0x0

    #@44
    goto :goto_b

    #@45
    .line 386
    .restart local v6       #abbrevRelative:Z
    :cond_45
    const/4 v12, 0x0

    #@46
    goto :goto_10

    #@47
    .line 397
    .restart local v7       #count:J
    .restart local v9       #duration:J
    .restart local v12       #past:Z
    :cond_47
    const/high16 v14, 0x113

    #@49
    .restart local v14       #resId:I
    goto :goto_2f

    #@4a
    .line 400
    .end local v14           #resId:I
    :cond_4a
    if-eqz v6, :cond_50

    #@4c
    .line 401
    const v14, 0x113000d

    #@4f
    .restart local v14       #resId:I
    goto :goto_2f

    #@50
    .line 403
    .end local v14           #resId:I
    :cond_50
    const v14, 0x1130005

    #@53
    .restart local v14       #resId:I
    goto :goto_2f

    #@54
    .line 406
    .end local v7           #count:J
    .end local v14           #resId:I
    :cond_54
    const-wide/32 v0, 0x36ee80

    #@57
    cmp-long v0, v9, v0

    #@59
    if-gez v0, :cond_7d

    #@5b
    const-wide/32 v0, 0x36ee80

    #@5e
    cmp-long v0, p4, v0

    #@60
    if-gez v0, :cond_7d

    #@62
    .line 407
    const-wide/32 v0, 0xea60

    #@65
    div-long v7, v9, v0

    #@67
    .line 408
    .restart local v7       #count:J
    if-eqz v12, :cond_73

    #@69
    .line 409
    if-eqz v6, :cond_6f

    #@6b
    .line 410
    const v14, 0x113000a

    #@6e
    .restart local v14       #resId:I
    goto :goto_2f

    #@6f
    .line 412
    .end local v14           #resId:I
    :cond_6f
    const v14, 0x1130001

    #@72
    .restart local v14       #resId:I
    goto :goto_2f

    #@73
    .line 415
    .end local v14           #resId:I
    :cond_73
    if-eqz v6, :cond_79

    #@75
    .line 416
    const v14, 0x113000e

    #@78
    .restart local v14       #resId:I
    goto :goto_2f

    #@79
    .line 418
    .end local v14           #resId:I
    :cond_79
    const v14, 0x1130006

    #@7c
    .restart local v14       #resId:I
    goto :goto_2f

    #@7d
    .line 421
    .end local v7           #count:J
    .end local v14           #resId:I
    :cond_7d
    const-wide/32 v0, 0x5265c00

    #@80
    cmp-long v0, v9, v0

    #@82
    if-gez v0, :cond_a6

    #@84
    const-wide/32 v0, 0x5265c00

    #@87
    cmp-long v0, p4, v0

    #@89
    if-gez v0, :cond_a6

    #@8b
    .line 422
    const-wide/32 v0, 0x36ee80

    #@8e
    div-long v7, v9, v0

    #@90
    .line 423
    .restart local v7       #count:J
    if-eqz v12, :cond_9c

    #@92
    .line 424
    if-eqz v6, :cond_98

    #@94
    .line 425
    const v14, 0x113000b

    #@97
    .restart local v14       #resId:I
    goto :goto_2f

    #@98
    .line 427
    .end local v14           #resId:I
    :cond_98
    const v14, 0x1130002

    #@9b
    .restart local v14       #resId:I
    goto :goto_2f

    #@9c
    .line 430
    .end local v14           #resId:I
    :cond_9c
    if-eqz v6, :cond_a2

    #@9e
    .line 431
    const v14, 0x113000f

    #@a1
    .restart local v14       #resId:I
    goto :goto_2f

    #@a2
    .line 433
    .end local v14           #resId:I
    :cond_a2
    const v14, 0x1130007

    #@a5
    .restart local v14       #resId:I
    goto :goto_2f

    #@a6
    .line 436
    .end local v7           #count:J
    .end local v14           #resId:I
    :cond_a6
    const-wide/32 v0, 0x240c8400

    #@a9
    cmp-long v0, v9, v0

    #@ab
    if-gez v0, :cond_d2

    #@ad
    const-wide/32 v0, 0x240c8400

    #@b0
    cmp-long v0, p4, v0

    #@b2
    if-gez v0, :cond_d2

    #@b4
    .line 437
    invoke-static/range {p0 .. p3}, Landroid/text/format/DateUtils;->getNumberOfDaysPassed(JJ)J

    #@b7
    move-result-wide v7

    #@b8
    .line 438
    .restart local v7       #count:J
    if-eqz v12, :cond_c6

    #@ba
    .line 439
    if-eqz v6, :cond_c1

    #@bc
    .line 440
    const v14, 0x113000c

    #@bf
    .restart local v14       #resId:I
    goto/16 :goto_2f

    #@c1
    .line 442
    .end local v14           #resId:I
    :cond_c1
    const v14, 0x1130004

    #@c4
    .restart local v14       #resId:I
    goto/16 :goto_2f

    #@c6
    .line 445
    .end local v14           #resId:I
    :cond_c6
    if-eqz v6, :cond_cd

    #@c8
    .line 446
    const v14, 0x1130010

    #@cb
    .restart local v14       #resId:I
    goto/16 :goto_2f

    #@cd
    .line 448
    .end local v14           #resId:I
    :cond_cd
    const v14, 0x1130008

    #@d0
    .restart local v14       #resId:I
    goto/16 :goto_2f

    #@d2
    .line 454
    .end local v7           #count:J
    .end local v14           #resId:I
    :cond_d2
    const/4 v0, 0x0

    #@d3
    move-wide v1, p0

    #@d4
    move-wide v3, p0

    #@d5
    move/from16 v5, p6

    #@d7
    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@da
    move-result-object v0

    #@db
    goto/16 :goto_42
.end method

.method public static getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;
    .registers 4
    .parameter "c"
    .parameter "millis"

    #@0
    .prologue
    .line 1632
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public static getRelativeTimeSpanString(Landroid/content/Context;JZ)Ljava/lang/CharSequence;
    .registers 20
    .parameter "c"
    .parameter "millis"
    .parameter "withPreposition"

    #@0
    .prologue
    .line 1582
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3
    move-result-wide v8

    #@4
    .line 1583
    .local v8, now:J
    sub-long v2, v8, p1

    #@6
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    #@9
    move-result-wide v13

    #@a
    .line 1585
    .local v13, span:J
    const-class v15, Landroid/text/format/DateUtils;

    #@c
    monitor-enter v15

    #@d
    .line 1586
    :try_start_d
    sget-object v2, Landroid/text/format/DateUtils;->sNowTime:Landroid/text/format/Time;

    #@f
    if-nez v2, :cond_18

    #@11
    .line 1587
    new-instance v2, Landroid/text/format/Time;

    #@13
    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    #@16
    sput-object v2, Landroid/text/format/DateUtils;->sNowTime:Landroid/text/format/Time;

    #@18
    .line 1590
    :cond_18
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@1a
    if-nez v2, :cond_23

    #@1c
    .line 1591
    new-instance v2, Landroid/text/format/Time;

    #@1e
    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    #@21
    sput-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@23
    .line 1594
    :cond_23
    sget-object v2, Landroid/text/format/DateUtils;->sNowTime:Landroid/text/format/Time;

    #@25
    invoke-virtual {v2, v8, v9}, Landroid/text/format/Time;->set(J)V

    #@28
    .line 1595
    sget-object v2, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@2a
    move-wide/from16 v0, p1

    #@2c
    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    #@2f
    .line 1598
    const-wide/32 v2, 0x5265c00

    #@32
    cmp-long v2, v13, v2

    #@34
    if-gez v2, :cond_60

    #@36
    sget-object v2, Landroid/text/format/DateUtils;->sNowTime:Landroid/text/format/Time;

    #@38
    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    #@3a
    sget-object v3, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@3c
    iget v3, v3, Landroid/text/format/Time;->weekDay:I

    #@3e
    if-ne v2, v3, :cond_60

    #@40
    .line 1600
    const/4 v7, 0x1

    #@41
    .local v7, flags:I
    move-object/from16 v2, p0

    #@43
    move-wide/from16 v3, p1

    #@45
    move-wide/from16 v5, p1

    #@47
    .line 1601
    invoke-static/range {v2 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@4a
    move-result-object v12

    #@4b
    .line 1602
    .local v12, result:Ljava/lang/String;
    const v10, 0x10403d4

    #@4e
    .line 1616
    .local v10, prepositionId:I
    :goto_4e
    if-eqz p3, :cond_5e

    #@50
    .line 1617
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@53
    move-result-object v11

    #@54
    .line 1618
    .local v11, res:Landroid/content/res/Resources;
    const/4 v2, 0x1

    #@55
    new-array v2, v2, [Ljava/lang/Object;

    #@57
    const/4 v3, 0x0

    #@58
    aput-object v12, v2, v3

    #@5a
    invoke-virtual {v11, v10, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@5d
    move-result-object v12

    #@5e
    .line 1620
    .end local v11           #res:Landroid/content/res/Resources;
    :cond_5e
    monitor-exit v15

    #@5f
    .line 1621
    return-object v12

    #@60
    .line 1603
    .end local v7           #flags:I
    .end local v10           #prepositionId:I
    .end local v12           #result:Ljava/lang/String;
    :cond_60
    sget-object v2, Landroid/text/format/DateUtils;->sNowTime:Landroid/text/format/Time;

    #@62
    iget v2, v2, Landroid/text/format/Time;->year:I

    #@64
    sget-object v3, Landroid/text/format/DateUtils;->sThenTime:Landroid/text/format/Time;

    #@66
    iget v3, v3, Landroid/text/format/Time;->year:I

    #@68
    if-eq v2, v3, :cond_7b

    #@6a
    .line 1605
    const v7, 0x20014

    #@6d
    .restart local v7       #flags:I
    move-object/from16 v2, p0

    #@6f
    move-wide/from16 v3, p1

    #@71
    move-wide/from16 v5, p1

    #@73
    .line 1606
    invoke-static/range {v2 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@76
    move-result-object v12

    #@77
    .line 1609
    .restart local v12       #result:Ljava/lang/String;
    const v10, 0x10403d3

    #@7a
    .line 1610
    .restart local v10       #prepositionId:I
    goto :goto_4e

    #@7b
    .line 1612
    .end local v7           #flags:I
    .end local v10           #prepositionId:I
    .end local v12           #result:Ljava/lang/String;
    :cond_7b
    const v7, 0x10010

    #@7e
    .restart local v7       #flags:I
    move-object/from16 v2, p0

    #@80
    move-wide/from16 v3, p1

    #@82
    move-wide/from16 v5, p1

    #@84
    .line 1613
    invoke-static/range {v2 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    #@87
    move-result-object v12

    #@88
    .line 1614
    .restart local v12       #result:Ljava/lang/String;
    const v10, 0x10403d3

    #@8b
    .restart local v10       #prepositionId:I
    goto :goto_4e

    #@8c
    .line 1620
    .end local v7           #flags:I
    .end local v10           #prepositionId:I
    .end local v12           #result:Ljava/lang/String;
    :catchall_8c
    move-exception v2

    #@8d
    monitor-exit v15
    :try_end_8e
    .catchall {:try_start_d .. :try_end_8e} :catchall_8c

    #@8e
    throw v2
.end method

.method public static getStandaloneMonthString(II)Ljava/lang/String;
    .registers 5
    .parameter "month"
    .parameter "abbrev"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 320
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@3
    move-result-object v2

    #@4
    invoke-static {v2}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@7
    move-result-object v0

    #@8
    .line 322
    .local v0, d:Llibcore/icu/LocaleData;
    sparse-switch p1, :sswitch_data_20

    #@b
    .line 329
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@d
    .line 331
    .local v1, names:[Ljava/lang/String;
    :goto_d
    aget-object v2, v1, p0

    #@f
    return-object v2

    #@10
    .line 323
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_10
    iget-object v1, v0, Llibcore/icu/LocaleData;->longStandAloneMonthNames:[Ljava/lang/String;

    #@12
    .line 324
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@13
    .line 325
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_13
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@15
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@16
    .line 326
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_16
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@18
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@19
    .line 327
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_19
    iget-object v1, v0, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@1b
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1c
    .line 328
    .end local v1           #names:[Ljava/lang/String;
    :sswitch_1c
    iget-object v1, v0, Llibcore/icu/LocaleData;->tinyMonthNames:[Ljava/lang/String;

    #@1e
    .restart local v1       #names:[Ljava/lang/String;
    goto :goto_d

    #@1f
    .line 322
    nop

    #@20
    :sswitch_data_20
    .sparse-switch
        0xa -> :sswitch_10
        0x14 -> :sswitch_13
        0x1e -> :sswitch_16
        0x28 -> :sswitch_19
        0x32 -> :sswitch_1c
    .end sparse-switch
.end method

.method private static initFormatStrings()V
    .registers 2

    #@0
    .prologue
    .line 581
    sget-object v1, Landroid/text/format/DateUtils;->sLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 582
    :try_start_3
    invoke-static {}, Landroid/text/format/DateUtils;->initFormatStringsLocked()V

    #@6
    .line 583
    monitor-exit v1

    #@7
    .line 584
    return-void

    #@8
    .line 583
    :catchall_8
    move-exception v0

    #@9
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_8

    #@a
    throw v0
.end method

.method private static initFormatStringsLocked()V
    .registers 3

    #@0
    .prologue
    .line 587
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    .line 588
    .local v1, r:Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    .line 589
    .local v0, cfg:Landroid/content/res/Configuration;
    sget-object v2, Landroid/text/format/DateUtils;->sLastConfig:Landroid/content/res/Configuration;

    #@a
    if-eqz v2, :cond_14

    #@c
    sget-object v2, Landroid/text/format/DateUtils;->sLastConfig:Landroid/content/res/Configuration;

    #@e
    invoke-virtual {v2, v0}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    #@11
    move-result v2

    #@12
    if-nez v2, :cond_2f

    #@14
    .line 590
    :cond_14
    sput-object v0, Landroid/text/format/DateUtils;->sLastConfig:Landroid/content/res/Configuration;

    #@16
    .line 591
    const/4 v2, 0x3

    #@17
    invoke-static {v2}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    #@1a
    move-result-object v2

    #@1b
    sput-object v2, Landroid/text/format/DateUtils;->sStatusTimeFormat:Ljava/text/DateFormat;

    #@1d
    .line 592
    const v2, 0x10403e7

    #@20
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    sput-object v2, Landroid/text/format/DateUtils;->sElapsedFormatMMSS:Ljava/lang/String;

    #@26
    .line 593
    const v2, 0x10403e8

    #@29
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    sput-object v2, Landroid/text/format/DateUtils;->sElapsedFormatHMMSS:Ljava/lang/String;

    #@2f
    .line 595
    :cond_2f
    return-void
.end method

.method public static isToday(J)Z
    .registers 8
    .parameter "when"

    #@0
    .prologue
    .line 786
    new-instance v3, Landroid/text/format/Time;

    #@2
    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    #@5
    .line 787
    .local v3, time:Landroid/text/format/Time;
    invoke-virtual {v3, p0, p1}, Landroid/text/format/Time;->set(J)V

    #@8
    .line 789
    iget v2, v3, Landroid/text/format/Time;->year:I

    #@a
    .line 790
    .local v2, thenYear:I
    iget v0, v3, Landroid/text/format/Time;->month:I

    #@c
    .line 791
    .local v0, thenMonth:I
    iget v1, v3, Landroid/text/format/Time;->monthDay:I

    #@e
    .line 793
    .local v1, thenMonthDay:I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@11
    move-result-wide v4

    #@12
    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    #@15
    .line 794
    iget v4, v3, Landroid/text/format/Time;->year:I

    #@17
    if-ne v2, v4, :cond_23

    #@19
    iget v4, v3, Landroid/text/format/Time;->month:I

    #@1b
    if-ne v0, v4, :cond_23

    #@1d
    iget v4, v3, Landroid/text/format/Time;->monthDay:I

    #@1f
    if-ne v1, v4, :cond_23

    #@21
    const/4 v4, 0x1

    #@22
    :goto_22
    return v4

    #@23
    :cond_23
    const/4 v4, 0x0

    #@24
    goto :goto_22
.end method

.method public static isUTC(Ljava/lang/String;)Z
    .registers 5
    .parameter "s"

    #@0
    .prologue
    const/16 v3, 0x5a

    #@2
    const/4 v0, 0x1

    #@3
    .line 806
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v1

    #@7
    const/16 v2, 0x10

    #@9
    if-ne v1, v2, :cond_14

    #@b
    const/16 v1, 0xf

    #@d
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@10
    move-result v1

    #@11
    if-ne v1, v3, :cond_14

    #@13
    .line 813
    :cond_13
    :goto_13
    return v0

    #@14
    .line 809
    :cond_14
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@17
    move-result v1

    #@18
    const/16 v2, 0x9

    #@1a
    if-ne v1, v2, :cond_24

    #@1c
    const/16 v1, 0x8

    #@1e
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@21
    move-result v1

    #@22
    if-eq v1, v3, :cond_13

    #@24
    .line 813
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_13
.end method

.method public static newCalendar(Z)Ljava/util/Calendar;
    .registers 2
    .parameter "zulu"

    #@0
    .prologue
    .line 776
    if-eqz p0, :cond_d

    #@2
    .line 777
    const-string v0, "GMT"

    #@4
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@7
    move-result-object v0

    #@8
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@b
    move-result-object v0

    #@c
    .line 779
    :goto_c
    return-object v0

    #@d
    :cond_d
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    #@10
    move-result-object v0

    #@11
    goto :goto_c
.end method

.method public static final timeString(J)Ljava/lang/CharSequence;
    .registers 5
    .parameter "millis"

    #@0
    .prologue
    .line 603
    sget-object v1, Landroid/text/format/DateUtils;->sLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 604
    :try_start_3
    invoke-static {}, Landroid/text/format/DateUtils;->initFormatStringsLocked()V

    #@6
    .line 605
    sget-object v0, Landroid/text/format/DateUtils;->sStatusTimeFormat:Ljava/text/DateFormat;

    #@8
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v2

    #@c
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    monitor-exit v1

    #@11
    return-object v0

    #@12
    .line 606
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method public static writeDateTime(Ljava/util/Calendar;)Ljava/lang/String;
    .registers 5
    .parameter "cal"

    #@0
    .prologue
    .line 827
    const-string v2, "GMT"

    #@2
    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@5
    move-result-object v1

    #@6
    .line 828
    .local v1, tz:Ljava/util/TimeZone;
    new-instance v0, Ljava/util/GregorianCalendar;

    #@8
    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    #@b
    .line 829
    .local v0, c:Ljava/util/GregorianCalendar;
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    #@e
    move-result-wide v2

    #@f
    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    #@12
    .line 830
    const/4 v2, 0x1

    #@13
    invoke-static {v0, v2}, Landroid/text/format/DateUtils;->writeDateTime(Ljava/util/Calendar;Z)Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    return-object v2
.end method

.method public static writeDateTime(Ljava/util/Calendar;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .registers 10
    .parameter "cal"
    .parameter "sb"

    #@0
    .prologue
    const/16 v7, 0xc

    #@2
    const/16 v6, 0xb

    #@4
    const/4 v5, 0x5

    #@5
    const/4 v4, 0x2

    #@6
    const/4 v3, 0x1

    #@7
    .line 870
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    #@a
    move-result v0

    #@b
    .line 871
    .local v0, n:I
    const/4 v1, 0x3

    #@c
    rem-int/lit8 v2, v0, 0xa

    #@e
    add-int/lit8 v2, v2, 0x30

    #@10
    int-to-char v2, v2

    #@11
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@14
    .line 872
    div-int/lit8 v0, v0, 0xa

    #@16
    .line 873
    rem-int/lit8 v1, v0, 0xa

    #@18
    add-int/lit8 v1, v1, 0x30

    #@1a
    int-to-char v1, v1

    #@1b
    invoke-virtual {p1, v4, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@1e
    .line 874
    div-int/lit8 v0, v0, 0xa

    #@20
    .line 875
    rem-int/lit8 v1, v0, 0xa

    #@22
    add-int/lit8 v1, v1, 0x30

    #@24
    int-to-char v1, v1

    #@25
    invoke-virtual {p1, v3, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@28
    .line 876
    div-int/lit8 v0, v0, 0xa

    #@2a
    .line 877
    const/4 v1, 0x0

    #@2b
    rem-int/lit8 v2, v0, 0xa

    #@2d
    add-int/lit8 v2, v2, 0x30

    #@2f
    int-to-char v2, v2

    #@30
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@33
    .line 879
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    #@36
    move-result v1

    #@37
    add-int/lit8 v0, v1, 0x1

    #@39
    .line 880
    rem-int/lit8 v1, v0, 0xa

    #@3b
    add-int/lit8 v1, v1, 0x30

    #@3d
    int-to-char v1, v1

    #@3e
    invoke-virtual {p1, v5, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@41
    .line 881
    div-int/lit8 v0, v0, 0xa

    #@43
    .line 882
    const/4 v1, 0x4

    #@44
    rem-int/lit8 v2, v0, 0xa

    #@46
    add-int/lit8 v2, v2, 0x30

    #@48
    int-to-char v2, v2

    #@49
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@4c
    .line 884
    invoke-virtual {p0, v5}, Ljava/util/Calendar;->get(I)I

    #@4f
    move-result v0

    #@50
    .line 885
    const/4 v1, 0x7

    #@51
    rem-int/lit8 v2, v0, 0xa

    #@53
    add-int/lit8 v2, v2, 0x30

    #@55
    int-to-char v2, v2

    #@56
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@59
    .line 886
    div-int/lit8 v0, v0, 0xa

    #@5b
    .line 887
    const/4 v1, 0x6

    #@5c
    rem-int/lit8 v2, v0, 0xa

    #@5e
    add-int/lit8 v2, v2, 0x30

    #@60
    int-to-char v2, v2

    #@61
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@64
    .line 889
    const/16 v1, 0x8

    #@66
    const/16 v2, 0x54

    #@68
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@6b
    .line 891
    invoke-virtual {p0, v6}, Ljava/util/Calendar;->get(I)I

    #@6e
    move-result v0

    #@6f
    .line 892
    const/16 v1, 0xa

    #@71
    rem-int/lit8 v2, v0, 0xa

    #@73
    add-int/lit8 v2, v2, 0x30

    #@75
    int-to-char v2, v2

    #@76
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@79
    .line 893
    div-int/lit8 v0, v0, 0xa

    #@7b
    .line 894
    const/16 v1, 0x9

    #@7d
    rem-int/lit8 v2, v0, 0xa

    #@7f
    add-int/lit8 v2, v2, 0x30

    #@81
    int-to-char v2, v2

    #@82
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@85
    .line 896
    invoke-virtual {p0, v7}, Ljava/util/Calendar;->get(I)I

    #@88
    move-result v0

    #@89
    .line 897
    rem-int/lit8 v1, v0, 0xa

    #@8b
    add-int/lit8 v1, v1, 0x30

    #@8d
    int-to-char v1, v1

    #@8e
    invoke-virtual {p1, v7, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@91
    .line 898
    div-int/lit8 v0, v0, 0xa

    #@93
    .line 899
    rem-int/lit8 v1, v0, 0xa

    #@95
    add-int/lit8 v1, v1, 0x30

    #@97
    int-to-char v1, v1

    #@98
    invoke-virtual {p1, v6, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@9b
    .line 901
    const/16 v1, 0xd

    #@9d
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    #@a0
    move-result v0

    #@a1
    .line 902
    const/16 v1, 0xe

    #@a3
    rem-int/lit8 v2, v0, 0xa

    #@a5
    add-int/lit8 v2, v2, 0x30

    #@a7
    int-to-char v2, v2

    #@a8
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@ab
    .line 903
    div-int/lit8 v0, v0, 0xa

    #@ad
    .line 904
    const/16 v1, 0xd

    #@af
    rem-int/lit8 v2, v0, 0xa

    #@b1
    add-int/lit8 v2, v2, 0x30

    #@b3
    int-to-char v2, v2

    #@b4
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@b7
    .line 906
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v1

    #@bb
    return-object v1
.end method

.method public static writeDateTime(Ljava/util/Calendar;Z)Ljava/lang/String;
    .registers 5
    .parameter "cal"
    .parameter "zulu"

    #@0
    .prologue
    const/16 v1, 0x10

    #@2
    const/16 v2, 0xf

    #@4
    .line 845
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    .line 846
    .local v0, sb:Ljava/lang/StringBuilder;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    #@c
    .line 847
    if-eqz p1, :cond_1b

    #@e
    .line 848
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    #@11
    .line 849
    const/16 v1, 0x5a

    #@13
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    #@16
    .line 853
    :goto_16
    invoke-static {p0, v0}, Landroid/text/format/DateUtils;->writeDateTime(Ljava/util/Calendar;Ljava/lang/StringBuilder;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    return-object v1

    #@1b
    .line 851
    :cond_1b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    #@1e
    goto :goto_16
.end method
