.class public Landroid/text/format/Time;
.super Ljava/lang/Object;
.source "Time.java"


# static fields
.field private static final DAYS_PER_MONTH:[I = null

.field public static final EPOCH_JULIAN_DAY:I = 0x253d8c

.field public static final FRIDAY:I = 0x5

.field public static final HOUR:I = 0x3

.field public static final MINUTE:I = 0x2

.field public static final MONDAY:I = 0x1

.field public static final MONDAY_BEFORE_JULIAN_EPOCH:I = 0x253d89

.field public static final MONTH:I = 0x5

.field public static final MONTH_DAY:I = 0x4

.field public static final SATURDAY:I = 0x6

.field public static final SECOND:I = 0x1

.field public static final SUNDAY:I = 0x0

.field public static final THURSDAY:I = 0x4

.field public static final TIMEZONE_UTC:Ljava/lang/String; = "UTC"

.field public static final TUESDAY:I = 0x2

.field public static final WEDNESDAY:I = 0x3

.field public static final WEEK_DAY:I = 0x7

.field public static final WEEK_NUM:I = 0x9

.field public static final YEAR:I = 0x6

.field public static final YEAR_DAY:I = 0x8

.field private static final Y_M_D:Ljava/lang/String; = "%Y-%m-%d"

.field private static final Y_M_D_T_H_M_S_000:Ljava/lang/String; = "%Y-%m-%dT%H:%M:%S.000"

.field private static final Y_M_D_T_H_M_S_000_Z:Ljava/lang/String; = "%Y-%m-%dT%H:%M:%S.000Z"

.field private static sAm:Ljava/lang/String;

.field private static sDateCommand:Ljava/lang/String;

.field private static sDateOnlyFormat:Ljava/lang/String;

.field private static sDateTimeFormat:Ljava/lang/String;

.field private static sLocale:Ljava/util/Locale;

.field private static sLongMonths:[Ljava/lang/String;

.field private static sLongStandaloneMonths:[Ljava/lang/String;

.field private static sLongWeekdays:[Ljava/lang/String;

.field private static sPm:Ljava/lang/String;

.field private static sShortMonths:[Ljava/lang/String;

.field private static sShortWeekdays:[Ljava/lang/String;

.field private static final sThursdayOffset:[I

.field private static sTimeOnlyFormat:Ljava/lang/String;

.field private static sZeroDigit:C


# instance fields
.field public allDay:Z

.field public gmtoff:J

.field public hour:I

.field public isDst:I

.field public minute:I

.field public month:I

.field public monthDay:I

.field public second:I

.field public timezone:Ljava/lang/String;

.field public weekDay:I

.field public year:I

.field public yearDay:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 157
    const-string v0, "%a %b %e %H:%M:%S %Z %Y"

    #@2
    sput-object v0, Landroid/text/format/Time;->sDateCommand:Ljava/lang/String;

    #@4
    .line 220
    const/16 v0, 0xc

    #@6
    new-array v0, v0, [I

    #@8
    fill-array-data v0, :array_16

    #@b
    sput-object v0, Landroid/text/format/Time;->DAYS_PER_MONTH:[I

    #@d
    .line 639
    const/4 v0, 0x7

    #@e
    new-array v0, v0, [I

    #@10
    fill-array-data v0, :array_32

    #@13
    sput-object v0, Landroid/text/format/Time;->sThursdayOffset:[I

    #@15
    return-void

    #@16
    .line 220
    :array_16
    .array-data 0x4
        0x1ft 0x0t 0x0t 0x0t
        0x1ct 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x1et 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x1et 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x1et 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x1et 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
    .end array-data

    #@32
    .line 639
    :array_32
    .array-data 0x4
        0xfdt 0xfft 0xfft 0xfft
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0xfft 0xfft 0xfft 0xfft
        0xfet 0xfft 0xfft 0xfft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 182
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    invoke-direct {p0, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    #@b
    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/text/format/Time;)V
    .registers 2
    .parameter "other"

    #@0
    .prologue
    .line 191
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 192
    invoke-virtual {p0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    #@6
    .line 193
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter "timezone"

    #@0
    .prologue
    .line 165
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 166
    if-nez p1, :cond_e

    #@5
    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string/jumbo v1, "timezone is null!"

    #@a
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@d
    throw v0

    #@e
    .line 169
    :cond_e
    iput-object p1, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@10
    .line 170
    const/16 v0, 0x7b2

    #@12
    iput v0, p0, Landroid/text/format/Time;->year:I

    #@14
    .line 171
    const/4 v0, 0x1

    #@15
    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    #@17
    .line 174
    const/4 v0, -0x1

    #@18
    iput v0, p0, Landroid/text/format/Time;->isDst:I

    #@1a
    .line 175
    return-void
.end method

.method public static compare(Landroid/text/format/Time;Landroid/text/format/Time;)I
    .registers 4
    .parameter "a"
    .parameter "b"

    #@0
    .prologue
    .line 302
    if-nez p0, :cond_a

    #@2
    .line 303
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string v1, "a == null"

    #@6
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@9
    throw v0

    #@a
    .line 304
    :cond_a
    if-nez p1, :cond_14

    #@c
    .line 305
    new-instance v0, Ljava/lang/NullPointerException;

    #@e
    const-string v1, "b == null"

    #@10
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@13
    throw v0

    #@14
    .line 308
    :cond_14
    invoke-static {p0, p1}, Landroid/text/format/Time;->nativeCompare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    #@17
    move-result v0

    #@18
    return v0
.end method

.method private native format1(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static getCurrentTimezone()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 477
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getJulianDay(JJ)I
    .registers 12
    .parameter "millis"
    .parameter "gmtoff"

    #@0
    .prologue
    .line 733
    const-wide/16 v4, 0x3e8

    #@2
    mul-long v2, p2, v4

    #@4
    .line 734
    .local v2, offsetMillis:J
    add-long v4, p0, v2

    #@6
    const-wide/32 v6, 0x5265c00

    #@9
    div-long v0, v4, v6

    #@b
    .line 735
    .local v0, julianDay:J
    long-to-int v4, v0

    #@c
    const v5, 0x253d8c

    #@f
    add-int/2addr v4, v5

    #@10
    return v4
.end method

.method public static getJulianMondayFromWeeksSinceEpoch(I)I
    .registers 3
    .parameter "week"

    #@0
    .prologue
    .line 811
    const v0, 0x253d89

    #@3
    mul-int/lit8 v1, p0, 0x7

    #@5
    add-int/2addr v0, v1

    #@6
    return v0
.end method

.method public static getWeeksSinceEpochFromJulianDay(II)I
    .registers 5
    .parameter "julianDay"
    .parameter "firstDayOfWeek"

    #@0
    .prologue
    .line 792
    rsub-int/lit8 v0, p1, 0x4

    #@2
    .line 793
    .local v0, diff:I
    if-gez v0, :cond_6

    #@4
    .line 794
    add-int/lit8 v0, v0, 0x7

    #@6
    .line 796
    :cond_6
    const v2, 0x253d8c

    #@9
    sub-int v1, v2, v0

    #@b
    .line 797
    .local v1, refDay:I
    sub-int v2, p0, v1

    #@d
    div-int/lit8 v2, v2, 0x7

    #@f
    return v2
.end method

.method public static isEpoch(Landroid/text/format/Time;)Z
    .registers 6
    .parameter "time"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 712
    invoke-virtual {p0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    #@4
    move-result-wide v0

    #@5
    .line 713
    .local v0, millis:J
    const-wide/16 v3, 0x0

    #@7
    invoke-static {v0, v1, v3, v4}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@a
    move-result v3

    #@b
    const v4, 0x253d8c

    #@e
    if-ne v3, v4, :cond_11

    #@10
    :goto_10
    return v2

    #@11
    :cond_11
    const/4 v2, 0x0

    #@12
    goto :goto_10
.end method

.method private localizeDigits(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "s"

    #@0
    .prologue
    .line 357
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@3
    move-result v2

    #@4
    .line 358
    .local v2, length:I
    sget-char v5, Landroid/text/format/Time;->sZeroDigit:C

    #@6
    add-int/lit8 v3, v5, -0x30

    #@8
    .line 359
    .local v3, offsetToLocalizedDigits:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    #@d
    .line 360
    .local v4, result:Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    #@e
    .local v1, i:I
    :goto_e
    if-ge v1, v2, :cond_25

    #@10
    .line 361
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@13
    move-result v0

    #@14
    .line 362
    .local v0, ch:C
    const/16 v5, 0x30

    #@16
    if-lt v0, v5, :cond_1f

    #@18
    const/16 v5, 0x39

    #@1a
    if-gt v0, v5, :cond_1f

    #@1c
    .line 363
    add-int v5, v0, v3

    #@1e
    int-to-char v0, v5

    #@1f
    .line 365
    :cond_1f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@22
    .line 360
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_e

    #@25
    .line 367
    .end local v0           #ch:C
    :cond_25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    return-object v5
.end method

.method private static native nativeCompare(Landroid/text/format/Time;Landroid/text/format/Time;)I
.end method

.method private native nativeParse(Ljava/lang/String;)Z
.end method

.method private native nativeParse3339(Ljava/lang/String;)Z
.end method


# virtual methods
.method public after(Landroid/text/format/Time;)Z
    .registers 3
    .parameter "that"

    #@0
    .prologue
    .line 631
    invoke-static {p0, p1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public before(Landroid/text/format/Time;)Z
    .registers 3
    .parameter "that"

    #@0
    .prologue
    .line 619
    invoke-static {p0, p1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    #@3
    move-result v0

    #@4
    if-gez v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public clear(Ljava/lang/String;)V
    .registers 4
    .parameter "timezone"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 271
    if-nez p1, :cond_c

    #@3
    .line 272
    new-instance v0, Ljava/lang/NullPointerException;

    #@5
    const-string/jumbo v1, "timezone is null!"

    #@8
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@b
    throw v0

    #@c
    .line 274
    :cond_c
    iput-object p1, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@e
    .line 275
    iput-boolean v0, p0, Landroid/text/format/Time;->allDay:Z

    #@10
    .line 276
    iput v0, p0, Landroid/text/format/Time;->second:I

    #@12
    .line 277
    iput v0, p0, Landroid/text/format/Time;->minute:I

    #@14
    .line 278
    iput v0, p0, Landroid/text/format/Time;->hour:I

    #@16
    .line 279
    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    #@18
    .line 280
    iput v0, p0, Landroid/text/format/Time;->month:I

    #@1a
    .line 281
    iput v0, p0, Landroid/text/format/Time;->year:I

    #@1c
    .line 282
    iput v0, p0, Landroid/text/format/Time;->weekDay:I

    #@1e
    .line 283
    iput v0, p0, Landroid/text/format/Time;->yearDay:I

    #@20
    .line 284
    const-wide/16 v0, 0x0

    #@22
    iput-wide v0, p0, Landroid/text/format/Time;->gmtoff:J

    #@24
    .line 285
    const/4 v0, -0x1

    #@25
    iput v0, p0, Landroid/text/format/Time;->isDst:I

    #@27
    .line 286
    return-void
.end method

.method public format(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "format"

    #@0
    .prologue
    .line 321
    const-class v5, Landroid/text/format/Time;

    #@2
    monitor-enter v5

    #@3
    .line 322
    :try_start_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@6
    move-result-object v0

    #@7
    .line 324
    .local v0, locale:Ljava/util/Locale;
    sget-object v4, Landroid/text/format/Time;->sLocale:Ljava/util/Locale;

    #@9
    if-eqz v4, :cond_15

    #@b
    if-eqz v0, :cond_15

    #@d
    sget-object v4, Landroid/text/format/Time;->sLocale:Ljava/util/Locale;

    #@f
    invoke-virtual {v0, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v4

    #@13
    if-nez v4, :cond_60

    #@15
    .line 325
    :cond_15
    invoke-static {v0}, Llibcore/icu/LocaleData;->get(Ljava/util/Locale;)Llibcore/icu/LocaleData;

    #@18
    move-result-object v1

    #@19
    .line 327
    .local v1, localeData:Llibcore/icu/LocaleData;
    iget-object v4, v1, Llibcore/icu/LocaleData;->amPm:[Ljava/lang/String;

    #@1b
    const/4 v6, 0x0

    #@1c
    aget-object v4, v4, v6

    #@1e
    sput-object v4, Landroid/text/format/Time;->sAm:Ljava/lang/String;

    #@20
    .line 328
    iget-object v4, v1, Llibcore/icu/LocaleData;->amPm:[Ljava/lang/String;

    #@22
    const/4 v6, 0x1

    #@23
    aget-object v4, v4, v6

    #@25
    sput-object v4, Landroid/text/format/Time;->sPm:Ljava/lang/String;

    #@27
    .line 329
    iget-char v4, v1, Llibcore/icu/LocaleData;->zeroDigit:C

    #@29
    sput-char v4, Landroid/text/format/Time;->sZeroDigit:C

    #@2b
    .line 331
    iget-object v4, v1, Llibcore/icu/LocaleData;->shortMonthNames:[Ljava/lang/String;

    #@2d
    sput-object v4, Landroid/text/format/Time;->sShortMonths:[Ljava/lang/String;

    #@2f
    .line 332
    iget-object v4, v1, Llibcore/icu/LocaleData;->longMonthNames:[Ljava/lang/String;

    #@31
    sput-object v4, Landroid/text/format/Time;->sLongMonths:[Ljava/lang/String;

    #@33
    .line 333
    iget-object v4, v1, Llibcore/icu/LocaleData;->longStandAloneMonthNames:[Ljava/lang/String;

    #@35
    sput-object v4, Landroid/text/format/Time;->sLongStandaloneMonths:[Ljava/lang/String;

    #@37
    .line 334
    iget-object v4, v1, Llibcore/icu/LocaleData;->shortWeekdayNames:[Ljava/lang/String;

    #@39
    sput-object v4, Landroid/text/format/Time;->sShortWeekdays:[Ljava/lang/String;

    #@3b
    .line 335
    iget-object v4, v1, Llibcore/icu/LocaleData;->longWeekdayNames:[Ljava/lang/String;

    #@3d
    sput-object v4, Landroid/text/format/Time;->sLongWeekdays:[Ljava/lang/String;

    #@3f
    .line 337
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    #@42
    move-result-object v2

    #@43
    .line 338
    .local v2, r:Landroid/content/res/Resources;
    const v4, 0x1040052

    #@46
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    sput-object v4, Landroid/text/format/Time;->sTimeOnlyFormat:Ljava/lang/String;

    #@4c
    .line 339
    const v4, 0x1040051

    #@4f
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    sput-object v4, Landroid/text/format/Time;->sDateOnlyFormat:Ljava/lang/String;

    #@55
    .line 340
    const v4, 0x1040053

    #@58
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@5b
    move-result-object v4

    #@5c
    sput-object v4, Landroid/text/format/Time;->sDateTimeFormat:Ljava/lang/String;

    #@5e
    .line 342
    sput-object v0, Landroid/text/format/Time;->sLocale:Ljava/util/Locale;

    #@60
    .line 345
    .end local v1           #localeData:Llibcore/icu/LocaleData;
    .end local v2           #r:Landroid/content/res/Resources;
    :cond_60
    invoke-direct {p0, p1}, Landroid/text/format/Time;->format1(Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    .line 346
    .local v3, result:Ljava/lang/String;
    sget-char v4, Landroid/text/format/Time;->sZeroDigit:C

    #@66
    const/16 v6, 0x30

    #@68
    if-eq v4, v6, :cond_6e

    #@6a
    .line 347
    invoke-direct {p0, v3}, Landroid/text/format/Time;->localizeDigits(Ljava/lang/String;)Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    .line 349
    :cond_6e
    monitor-exit v5

    #@6f
    return-object v3

    #@70
    .line 350
    .end local v0           #locale:Ljava/util/Locale;
    .end local v3           #result:Ljava/lang/String;
    :catchall_70
    move-exception v4

    #@71
    monitor-exit v5
    :try_end_72
    .catchall {:try_start_3 .. :try_end_72} :catchall_70

    #@72
    throw v4
.end method

.method public native format2445()Ljava/lang/String;
.end method

.method public format3339(Z)Ljava/lang/String;
    .registers 12
    .parameter "allDay"

    #@0
    .prologue
    .line 689
    if-eqz p1, :cond_9

    #@2
    .line 690
    const-string v5, "%Y-%m-%d"

    #@4
    invoke-virtual {p0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v5

    #@8
    .line 700
    :goto_8
    return-object v5

    #@9
    .line 691
    :cond_9
    const-string v5, "UTC"

    #@b
    iget-object v6, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v5

    #@11
    if-eqz v5, :cond_1a

    #@13
    .line 692
    const-string v5, "%Y-%m-%dT%H:%M:%S.000Z"

    #@15
    invoke-virtual {p0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    goto :goto_8

    #@1a
    .line 694
    :cond_1a
    const-string v5, "%Y-%m-%dT%H:%M:%S.000"

    #@1c
    invoke-virtual {p0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 695
    .local v0, base:Ljava/lang/String;
    iget-wide v5, p0, Landroid/text/format/Time;->gmtoff:J

    #@22
    const-wide/16 v7, 0x0

    #@24
    cmp-long v5, v5, v7

    #@26
    if-gez v5, :cond_57

    #@28
    const-string v4, "-"

    #@2a
    .line 696
    .local v4, sign:Ljava/lang/String;
    :goto_2a
    iget-wide v5, p0, Landroid/text/format/Time;->gmtoff:J

    #@2c
    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    #@2f
    move-result-wide v5

    #@30
    long-to-int v3, v5

    #@31
    .line 697
    .local v3, offset:I
    rem-int/lit16 v5, v3, 0xe10

    #@33
    div-int/lit8 v2, v5, 0x3c

    #@35
    .line 698
    .local v2, minutes:I
    div-int/lit16 v1, v3, 0xe10

    #@37
    .line 700
    .local v1, hours:I
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    #@39
    const-string v6, "%s%s%02d:%02d"

    #@3b
    const/4 v7, 0x4

    #@3c
    new-array v7, v7, [Ljava/lang/Object;

    #@3e
    const/4 v8, 0x0

    #@3f
    aput-object v0, v7, v8

    #@41
    const/4 v8, 0x1

    #@42
    aput-object v4, v7, v8

    #@44
    const/4 v8, 0x2

    #@45
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@48
    move-result-object v9

    #@49
    aput-object v9, v7, v8

    #@4b
    const/4 v8, 0x3

    #@4c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v9

    #@50
    aput-object v9, v7, v8

    #@52
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    goto :goto_8

    #@57
    .line 695
    .end local v1           #hours:I
    .end local v2           #minutes:I
    .end local v3           #offset:I
    .end local v4           #sign:Ljava/lang/String;
    :cond_57
    const-string v4, "+"

    #@59
    goto :goto_2a
.end method

.method public getActualMaximum(I)I
    .registers 7
    .parameter "field"

    #@0
    .prologue
    const/16 v0, 0x3b

    #@2
    const/16 v2, 0x1c

    #@4
    .line 231
    packed-switch p1, :pswitch_data_66

    #@7
    .line 261
    new-instance v2, Ljava/lang/RuntimeException;

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "bad field="

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@1f
    throw v2

    #@20
    :pswitch_20
    move v2, v0

    #@21
    .line 256
    :cond_21
    :goto_21
    return v2

    #@22
    :pswitch_22
    move v2, v0

    #@23
    .line 235
    goto :goto_21

    #@24
    .line 237
    :pswitch_24
    const/16 v2, 0x17

    #@26
    goto :goto_21

    #@27
    .line 239
    :pswitch_27
    sget-object v3, Landroid/text/format/Time;->DAYS_PER_MONTH:[I

    #@29
    iget v4, p0, Landroid/text/format/Time;->month:I

    #@2b
    aget v0, v3, v4

    #@2d
    .line 240
    .local v0, n:I
    if-eq v0, v2, :cond_31

    #@2f
    move v2, v0

    #@30
    .line 241
    goto :goto_21

    #@31
    .line 243
    :cond_31
    iget v1, p0, Landroid/text/format/Time;->year:I

    #@33
    .line 244
    .local v1, y:I
    rem-int/lit8 v3, v1, 0x4

    #@35
    if-nez v3, :cond_21

    #@37
    rem-int/lit8 v3, v1, 0x64

    #@39
    if-nez v3, :cond_3f

    #@3b
    rem-int/lit16 v3, v1, 0x190

    #@3d
    if-nez v3, :cond_21

    #@3f
    :cond_3f
    const/16 v2, 0x1d

    #@41
    goto :goto_21

    #@42
    .line 248
    .end local v0           #n:I
    .end local v1           #y:I
    :pswitch_42
    const/16 v2, 0xb

    #@44
    goto :goto_21

    #@45
    .line 250
    :pswitch_45
    const/16 v2, 0x7f5

    #@47
    goto :goto_21

    #@48
    .line 252
    :pswitch_48
    const/4 v2, 0x6

    #@49
    goto :goto_21

    #@4a
    .line 254
    :pswitch_4a
    iget v1, p0, Landroid/text/format/Time;->year:I

    #@4c
    .line 256
    .restart local v1       #y:I
    rem-int/lit8 v2, v1, 0x4

    #@4e
    if-nez v2, :cond_5b

    #@50
    rem-int/lit8 v2, v1, 0x64

    #@52
    if-nez v2, :cond_58

    #@54
    rem-int/lit16 v2, v1, 0x190

    #@56
    if-nez v2, :cond_5b

    #@58
    :cond_58
    const/16 v2, 0x16d

    #@5a
    goto :goto_21

    #@5b
    :cond_5b
    const/16 v2, 0x16c

    #@5d
    goto :goto_21

    #@5e
    .line 259
    .end local v1           #y:I
    :pswitch_5e
    new-instance v2, Ljava/lang/RuntimeException;

    #@60
    const-string v3, "WEEK_NUM not implemented"

    #@62
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@65
    throw v2

    #@66
    .line 231
    :pswitch_data_66
    .packed-switch 0x1
        :pswitch_20
        :pswitch_22
        :pswitch_24
        :pswitch_27
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_4a
        :pswitch_5e
    .end packed-switch
.end method

.method public getWeekNumber()I
    .registers 6

    #@0
    .prologue
    .line 663
    iget v2, p0, Landroid/text/format/Time;->yearDay:I

    #@2
    sget-object v3, Landroid/text/format/Time;->sThursdayOffset:[I

    #@4
    iget v4, p0, Landroid/text/format/Time;->weekDay:I

    #@6
    aget v3, v3, v4

    #@8
    add-int v0, v2, v3

    #@a
    .line 666
    .local v0, closestThursday:I
    if-ltz v0, :cond_15

    #@c
    const/16 v2, 0x16c

    #@e
    if-gt v0, v2, :cond_15

    #@10
    .line 667
    div-int/lit8 v2, v0, 0x7

    #@12
    add-int/lit8 v2, v2, 0x1

    #@14
    .line 674
    :goto_14
    return v2

    #@15
    .line 671
    :cond_15
    new-instance v1, Landroid/text/format/Time;

    #@17
    invoke-direct {v1, p0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    #@1a
    .line 672
    .local v1, temp:Landroid/text/format/Time;
    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    #@1c
    sget-object v3, Landroid/text/format/Time;->sThursdayOffset:[I

    #@1e
    iget v4, p0, Landroid/text/format/Time;->weekDay:I

    #@20
    aget v3, v3, v4

    #@22
    add-int/2addr v2, v3

    #@23
    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    #@25
    .line 673
    const/4 v2, 0x1

    #@26
    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    #@29
    .line 674
    iget v2, v1, Landroid/text/format/Time;->yearDay:I

    #@2b
    div-int/lit8 v2, v2, 0x7

    #@2d
    add-int/lit8 v2, v2, 0x1

    #@2f
    goto :goto_14
.end method

.method public native normalize(Z)J
.end method

.method public parse(Ljava/lang/String;)Z
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 414
    if-nez p1, :cond_b

    #@2
    .line 415
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "time string is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 417
    :cond_b
    invoke-direct {p0, p1}, Landroid/text/format/Time;->nativeParse(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 418
    const-string v0, "UTC"

    #@13
    iput-object v0, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@15
    .line 419
    const/4 v0, 0x1

    #@16
    .line 421
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public parse3339(Ljava/lang/String;)Z
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 461
    if-nez p1, :cond_b

    #@2
    .line 462
    new-instance v0, Ljava/lang/NullPointerException;

    #@4
    const-string/jumbo v1, "time string is null"

    #@7
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@a
    throw v0

    #@b
    .line 464
    :cond_b
    invoke-direct {p0, p1}, Landroid/text/format/Time;->nativeParse3339(Ljava/lang/String;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_17

    #@11
    .line 465
    const-string v0, "UTC"

    #@13
    iput-object v0, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@15
    .line 466
    const/4 v0, 0x1

    #@16
    .line 468
    :goto_16
    return v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_16
.end method

.method public set(III)V
    .registers 6
    .parameter "monthDay"
    .parameter "month"
    .parameter "year"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 598
    const/4 v0, 0x1

    #@2
    iput-boolean v0, p0, Landroid/text/format/Time;->allDay:Z

    #@4
    .line 599
    iput v1, p0, Landroid/text/format/Time;->second:I

    #@6
    .line 600
    iput v1, p0, Landroid/text/format/Time;->minute:I

    #@8
    .line 601
    iput v1, p0, Landroid/text/format/Time;->hour:I

    #@a
    .line 602
    iput p1, p0, Landroid/text/format/Time;->monthDay:I

    #@c
    .line 603
    iput p2, p0, Landroid/text/format/Time;->month:I

    #@e
    .line 604
    iput p3, p0, Landroid/text/format/Time;->year:I

    #@10
    .line 605
    iput v1, p0, Landroid/text/format/Time;->weekDay:I

    #@12
    .line 606
    iput v1, p0, Landroid/text/format/Time;->yearDay:I

    #@14
    .line 607
    const/4 v0, -0x1

    #@15
    iput v0, p0, Landroid/text/format/Time;->isDst:I

    #@17
    .line 608
    const-wide/16 v0, 0x0

    #@19
    iput-wide v0, p0, Landroid/text/format/Time;->gmtoff:J

    #@1b
    .line 609
    return-void
.end method

.method public set(IIIIII)V
    .registers 9
    .parameter "second"
    .parameter "minute"
    .parameter "hour"
    .parameter "monthDay"
    .parameter "month"
    .parameter "year"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 575
    iput-boolean v0, p0, Landroid/text/format/Time;->allDay:Z

    #@3
    .line 576
    iput p1, p0, Landroid/text/format/Time;->second:I

    #@5
    .line 577
    iput p2, p0, Landroid/text/format/Time;->minute:I

    #@7
    .line 578
    iput p3, p0, Landroid/text/format/Time;->hour:I

    #@9
    .line 579
    iput p4, p0, Landroid/text/format/Time;->monthDay:I

    #@b
    .line 580
    iput p5, p0, Landroid/text/format/Time;->month:I

    #@d
    .line 581
    iput p6, p0, Landroid/text/format/Time;->year:I

    #@f
    .line 582
    iput v0, p0, Landroid/text/format/Time;->weekDay:I

    #@11
    .line 583
    iput v0, p0, Landroid/text/format/Time;->yearDay:I

    #@13
    .line 584
    const/4 v0, -0x1

    #@14
    iput v0, p0, Landroid/text/format/Time;->isDst:I

    #@16
    .line 585
    const-wide/16 v0, 0x0

    #@18
    iput-wide v0, p0, Landroid/text/format/Time;->gmtoff:J

    #@1a
    .line 586
    return-void
.end method

.method public native set(J)V
.end method

.method public set(Landroid/text/format/Time;)V
    .registers 4
    .parameter "that"

    #@0
    .prologue
    .line 556
    iget-object v0, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@2
    iput-object v0, p0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    #@4
    .line 557
    iget-boolean v0, p1, Landroid/text/format/Time;->allDay:Z

    #@6
    iput-boolean v0, p0, Landroid/text/format/Time;->allDay:Z

    #@8
    .line 558
    iget v0, p1, Landroid/text/format/Time;->second:I

    #@a
    iput v0, p0, Landroid/text/format/Time;->second:I

    #@c
    .line 559
    iget v0, p1, Landroid/text/format/Time;->minute:I

    #@e
    iput v0, p0, Landroid/text/format/Time;->minute:I

    #@10
    .line 560
    iget v0, p1, Landroid/text/format/Time;->hour:I

    #@12
    iput v0, p0, Landroid/text/format/Time;->hour:I

    #@14
    .line 561
    iget v0, p1, Landroid/text/format/Time;->monthDay:I

    #@16
    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    #@18
    .line 562
    iget v0, p1, Landroid/text/format/Time;->month:I

    #@1a
    iput v0, p0, Landroid/text/format/Time;->month:I

    #@1c
    .line 563
    iget v0, p1, Landroid/text/format/Time;->year:I

    #@1e
    iput v0, p0, Landroid/text/format/Time;->year:I

    #@20
    .line 564
    iget v0, p1, Landroid/text/format/Time;->weekDay:I

    #@22
    iput v0, p0, Landroid/text/format/Time;->weekDay:I

    #@24
    .line 565
    iget v0, p1, Landroid/text/format/Time;->yearDay:I

    #@26
    iput v0, p0, Landroid/text/format/Time;->yearDay:I

    #@28
    .line 566
    iget v0, p1, Landroid/text/format/Time;->isDst:I

    #@2a
    iput v0, p0, Landroid/text/format/Time;->isDst:I

    #@2c
    .line 567
    iget-wide v0, p1, Landroid/text/format/Time;->gmtoff:J

    #@2e
    iput-wide v0, p0, Landroid/text/format/Time;->gmtoff:J

    #@30
    .line 568
    return-void
.end method

.method public setJulianDay(I)J
    .registers 11
    .parameter "julianDay"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 762
    const v4, 0x253d8c

    #@4
    sub-int v4, p1, v4

    #@6
    int-to-long v4, v4

    #@7
    const-wide/32 v6, 0x5265c00

    #@a
    mul-long v2, v4, v6

    #@c
    .line 763
    .local v2, millis:J
    invoke-virtual {p0, v2, v3}, Landroid/text/format/Time;->set(J)V

    #@f
    .line 767
    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    #@11
    invoke-static {v2, v3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    #@14
    move-result v0

    #@15
    .line 768
    .local v0, approximateDay:I
    sub-int v1, p1, v0

    #@17
    .line 769
    .local v1, diff:I
    iget v4, p0, Landroid/text/format/Time;->monthDay:I

    #@19
    add-int/2addr v4, v1

    #@1a
    iput v4, p0, Landroid/text/format/Time;->monthDay:I

    #@1c
    .line 772
    iput v8, p0, Landroid/text/format/Time;->hour:I

    #@1e
    .line 773
    iput v8, p0, Landroid/text/format/Time;->minute:I

    #@20
    .line 774
    iput v8, p0, Landroid/text/format/Time;->second:I

    #@22
    .line 775
    const/4 v4, 0x1

    #@23
    invoke-virtual {p0, v4}, Landroid/text/format/Time;->normalize(Z)J

    #@26
    move-result-wide v2

    #@27
    .line 776
    return-wide v2
.end method

.method public native setToNow()V
.end method

.method public native switchTimezone(Ljava/lang/String;)V
.end method

.method public native toMillis(Z)J
.end method

.method public native toString()Ljava/lang/String;
.end method
