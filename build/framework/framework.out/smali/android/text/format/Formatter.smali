.class public final Landroid/text/format/Formatter;
.super Ljava/lang/Object;
.source "Formatter.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static formatFileSize(Landroid/content/Context;J)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    .line 36
    const/4 v0, 0x0

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;JZ)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method private static formatFileSize(Landroid/content/Context;JZ)Ljava/lang/String;
    .registers 12
    .parameter "context"
    .parameter "number"
    .parameter "shorter"

    #@0
    .prologue
    const/high16 v5, 0x4480

    #@2
    const/high16 v4, 0x4461

    #@4
    const/4 v7, 0x1

    #@5
    const/4 v6, 0x0

    #@6
    .line 48
    if-nez p0, :cond_b

    #@8
    .line 49
    const-string v3, ""

    #@a
    .line 92
    :goto_a
    return-object v3

    #@b
    .line 52
    :cond_b
    long-to-float v0, p1

    #@c
    .line 53
    .local v0, result:F
    const v1, 0x1040088

    #@f
    .line 54
    .local v1, suffix:I
    cmpl-float v3, v0, v4

    #@11
    if-lez v3, :cond_17

    #@13
    .line 55
    const v1, 0x1040089

    #@16
    .line 56
    div-float/2addr v0, v5

    #@17
    .line 58
    :cond_17
    cmpl-float v3, v0, v4

    #@19
    if-lez v3, :cond_1f

    #@1b
    .line 59
    const v1, 0x104008a

    #@1e
    .line 60
    div-float/2addr v0, v5

    #@1f
    .line 62
    :cond_1f
    cmpl-float v3, v0, v4

    #@21
    if-lez v3, :cond_27

    #@23
    .line 63
    const v1, 0x104008b

    #@26
    .line 64
    div-float/2addr v0, v5

    #@27
    .line 66
    :cond_27
    cmpl-float v3, v0, v4

    #@29
    if-lez v3, :cond_2f

    #@2b
    .line 67
    const v1, 0x104008c

    #@2e
    .line 68
    div-float/2addr v0, v5

    #@2f
    .line 70
    :cond_2f
    cmpl-float v3, v0, v4

    #@31
    if-lez v3, :cond_37

    #@33
    .line 71
    const v1, 0x104008d

    #@36
    .line 72
    div-float/2addr v0, v5

    #@37
    .line 75
    :cond_37
    const/high16 v3, 0x3f80

    #@39
    cmpg-float v3, v0, v3

    #@3b
    if-gez v3, :cond_62

    #@3d
    .line 76
    const-string v3, "%.2f"

    #@3f
    new-array v4, v7, [Ljava/lang/Object;

    #@41
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@44
    move-result-object v5

    #@45
    aput-object v5, v4, v6

    #@47
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    .line 92
    .local v2, value:Ljava/lang/String;
    :goto_4b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@4e
    move-result-object v3

    #@4f
    const v4, 0x104008e

    #@52
    const/4 v5, 0x2

    #@53
    new-array v5, v5, [Ljava/lang/Object;

    #@55
    aput-object v2, v5, v6

    #@57
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    aput-object v6, v5, v7

    #@5d
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    goto :goto_a

    #@62
    .line 77
    .end local v2           #value:Ljava/lang/String;
    :cond_62
    const/high16 v3, 0x4120

    #@64
    cmpg-float v3, v0, v3

    #@66
    if-gez v3, :cond_88

    #@68
    .line 78
    if-eqz p3, :cond_79

    #@6a
    .line 79
    const-string v3, "%.1f"

    #@6c
    new-array v4, v7, [Ljava/lang/Object;

    #@6e
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@71
    move-result-object v5

    #@72
    aput-object v5, v4, v6

    #@74
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    .restart local v2       #value:Ljava/lang/String;
    goto :goto_4b

    #@79
    .line 81
    .end local v2           #value:Ljava/lang/String;
    :cond_79
    const-string v3, "%.2f"

    #@7b
    new-array v4, v7, [Ljava/lang/Object;

    #@7d
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@80
    move-result-object v5

    #@81
    aput-object v5, v4, v6

    #@83
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@86
    move-result-object v2

    #@87
    .restart local v2       #value:Ljava/lang/String;
    goto :goto_4b

    #@88
    .line 83
    .end local v2           #value:Ljava/lang/String;
    :cond_88
    const/high16 v3, 0x42c8

    #@8a
    cmpg-float v3, v0, v3

    #@8c
    if-gez v3, :cond_ae

    #@8e
    .line 84
    if-eqz p3, :cond_9f

    #@90
    .line 85
    const-string v3, "%.0f"

    #@92
    new-array v4, v7, [Ljava/lang/Object;

    #@94
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@97
    move-result-object v5

    #@98
    aput-object v5, v4, v6

    #@9a
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@9d
    move-result-object v2

    #@9e
    .restart local v2       #value:Ljava/lang/String;
    goto :goto_4b

    #@9f
    .line 87
    .end local v2           #value:Ljava/lang/String;
    :cond_9f
    const-string v3, "%.2f"

    #@a1
    new-array v4, v7, [Ljava/lang/Object;

    #@a3
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@a6
    move-result-object v5

    #@a7
    aput-object v5, v4, v6

    #@a9
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@ac
    move-result-object v2

    #@ad
    .restart local v2       #value:Ljava/lang/String;
    goto :goto_4b

    #@ae
    .line 90
    .end local v2           #value:Ljava/lang/String;
    :cond_ae
    const-string v3, "%.0f"

    #@b0
    new-array v4, v7, [Ljava/lang/Object;

    #@b2
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    #@b5
    move-result-object v5

    #@b6
    aput-object v5, v4, v6

    #@b8
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@bb
    move-result-object v2

    #@bc
    .restart local v2       #value:Ljava/lang/String;
    goto :goto_4b
.end method

.method public static formatIpAddress(I)Ljava/lang/String;
    .registers 2
    .parameter "ipv4Address"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    .line 111
    invoke-static {p0}, Landroid/net/NetworkUtils;->intToInetAddress(I)Ljava/net/InetAddress;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "number"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x1

    #@1
    invoke-static {p0, p1, p2, v0}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;JZ)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method
