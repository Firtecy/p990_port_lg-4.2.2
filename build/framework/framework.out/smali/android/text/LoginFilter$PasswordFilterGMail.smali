.class public Landroid/text/LoginFilter$PasswordFilterGMail;
.super Landroid/text/LoginFilter;
.source "LoginFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/LoginFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PasswordFilterGMail"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 198
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, v0}, Landroid/text/LoginFilter;-><init>(Z)V

    #@4
    .line 199
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 2
    .parameter "appendInvalid"

    #@0
    .prologue
    .line 202
    invoke-direct {p0, p1}, Landroid/text/LoginFilter;-><init>(Z)V

    #@3
    .line 203
    return-void
.end method


# virtual methods
.method public isAllowed(C)Z
    .registers 4
    .parameter "c"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 208
    const/16 v1, 0x20

    #@3
    if-gt v1, p1, :cond_a

    #@5
    const/16 v1, 0x7f

    #@7
    if-gt p1, v1, :cond_a

    #@9
    .line 213
    :cond_9
    :goto_9
    return v0

    #@a
    .line 211
    :cond_a
    const/16 v1, 0xa0

    #@c
    if-gt v1, p1, :cond_12

    #@e
    const/16 v1, 0xff

    #@10
    if-le p1, v1, :cond_9

    #@12
    .line 213
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_9
.end method
