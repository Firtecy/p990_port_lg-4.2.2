.class public Landroid/text/TextPaint;
.super Landroid/graphics/Paint;
.source "TextPaint.java"


# instance fields
.field public baselineShift:I

.field public bgColor:I

.field public density:F

.field public drawableState:[I

.field public linkColor:I

.field public underlineColor:I

.field public underlineThickness:F


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 45
    invoke-direct {p0}, Landroid/graphics/Paint;-><init>()V

    #@3
    .line 32
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/text/TextPaint;->density:F

    #@7
    .line 37
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/text/TextPaint;->underlineColor:I

    #@a
    .line 46
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "flags"

    #@0
    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/graphics/Paint;-><init>(I)V

    #@3
    .line 32
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/text/TextPaint;->density:F

    #@7
    .line 37
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/text/TextPaint;->underlineColor:I

    #@a
    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Paint;)V
    .registers 3
    .parameter "p"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    #@3
    .line 32
    const/high16 v0, 0x3f80

    #@5
    iput v0, p0, Landroid/text/TextPaint;->density:F

    #@7
    .line 37
    const/4 v0, 0x0

    #@8
    iput v0, p0, Landroid/text/TextPaint;->underlineColor:I

    #@a
    .line 54
    return-void
.end method


# virtual methods
.method public set(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "tp"

    #@0
    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    #@3
    .line 63
    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    #@5
    iput v0, p0, Landroid/text/TextPaint;->bgColor:I

    #@7
    .line 64
    iget v0, p1, Landroid/text/TextPaint;->baselineShift:I

    #@9
    iput v0, p0, Landroid/text/TextPaint;->baselineShift:I

    #@b
    .line 65
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    #@d
    iput v0, p0, Landroid/text/TextPaint;->linkColor:I

    #@f
    .line 66
    iget-object v0, p1, Landroid/text/TextPaint;->drawableState:[I

    #@11
    iput-object v0, p0, Landroid/text/TextPaint;->drawableState:[I

    #@13
    .line 67
    iget v0, p1, Landroid/text/TextPaint;->density:F

    #@15
    iput v0, p0, Landroid/text/TextPaint;->density:F

    #@17
    .line 68
    iget v0, p1, Landroid/text/TextPaint;->underlineColor:I

    #@19
    iput v0, p0, Landroid/text/TextPaint;->underlineColor:I

    #@1b
    .line 69
    iget v0, p1, Landroid/text/TextPaint;->underlineThickness:F

    #@1d
    iput v0, p0, Landroid/text/TextPaint;->underlineThickness:F

    #@1f
    .line 70
    return-void
.end method

.method public setUnderlineText(IF)V
    .registers 3
    .parameter "color"
    .parameter "thickness"

    #@0
    .prologue
    .line 79
    iput p1, p0, Landroid/text/TextPaint;->underlineColor:I

    #@2
    .line 80
    iput p2, p0, Landroid/text/TextPaint;->underlineThickness:F

    #@4
    .line 81
    return-void
.end method
