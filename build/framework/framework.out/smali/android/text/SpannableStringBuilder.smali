.class public Landroid/text/SpannableStringBuilder;
.super Ljava/lang/Object;
.source "SpannableStringBuilder.java"

# interfaces
.implements Ljava/lang/CharSequence;
.implements Landroid/text/GetChars;
.implements Landroid/text/Spannable;
.implements Landroid/text/Editable;
.implements Ljava/lang/Appendable;
.implements Landroid/text/GraphicsOperations;


# static fields
.field private static final END_MASK:I = 0xf

.field private static final MARK:I = 0x1

.field private static final NO_FILTERS:[Landroid/text/InputFilter; = null

.field private static final PARAGRAPH:I = 0x3

.field private static final POINT:I = 0x2

.field private static final SPAN_END_AT_END:I = 0x8000

.field private static final SPAN_END_AT_START:I = 0x4000

.field private static final SPAN_START_AT_END:I = 0x2000

.field private static final SPAN_START_AT_START:I = 0x1000

.field private static final SPAN_START_END_MASK:I = 0xf000

.field private static final START_MASK:I = 0xf0

.field private static final START_SHIFT:I = 0x4


# instance fields
.field private mFilters:[Landroid/text/InputFilter;

.field private mGapLength:I

.field private mGapStart:I

.field private mSpanCount:I

.field private mSpanCountBeforeAdd:I

.field private mSpanEnds:[I

.field private mSpanFlags:[I

.field private mSpanStarts:[I

.field private mSpans:[Ljava/lang/Object;

.field private mText:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 1328
    const/4 v0, 0x0

    #@1
    new-array v0, v0, [Landroid/text/InputFilter;

    #@3
    sput-object v0, Landroid/text/SpannableStringBuilder;->NO_FILTERS:[Landroid/text/InputFilter;

    #@5
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 36
    const-string v0, ""

    #@2
    invoke-direct {p0, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@5
    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "text"

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v1

    #@5
    invoke-direct {p0, p1, v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    #@8
    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;II)V
    .registers 16
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 1329
    sget-object v0, Landroid/text/SpannableStringBuilder;->NO_FILTERS:[Landroid/text/InputFilter;

    #@6
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mFilters:[Landroid/text/InputFilter;

    #@8
    .line 52
    sub-int v11, p3, p2

    #@a
    .line 54
    .local v11, srclen:I
    if-gez v11, :cond_12

    #@c
    new-instance v0, Ljava/lang/StringIndexOutOfBoundsException;

    #@e
    invoke-direct {v0}, Ljava/lang/StringIndexOutOfBoundsException;-><init>()V

    #@11
    throw v0

    #@12
    .line 56
    :cond_12
    add-int/lit8 v0, v11, 0x1

    #@14
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@17
    move-result v8

    #@18
    .line 57
    .local v8, len:I
    new-array v0, v8, [C

    #@1a
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1c
    .line 58
    iput v11, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1e
    .line 59
    sub-int v0, v8, v11

    #@20
    iput v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@22
    .line 61
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@24
    invoke-static {p1, p2, p3, v0, v1}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@27
    .line 63
    iput v1, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@29
    .line 64
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@2c
    move-result v6

    #@2d
    .line 65
    .local v6, alloc:I
    new-array v0, v6, [Ljava/lang/Object;

    #@2f
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@31
    .line 66
    new-array v0, v6, [I

    #@33
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@35
    .line 67
    new-array v0, v6, [I

    #@37
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@39
    .line 68
    new-array v0, v6, [I

    #@3b
    iput-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@3d
    .line 70
    instance-of v0, p1, Landroid/text/Spanned;

    #@3f
    if-eqz v0, :cond_86

    #@41
    move-object v9, p1

    #@42
    .line 71
    check-cast v9, Landroid/text/Spanned;

    #@44
    .line 72
    .local v9, sp:Landroid/text/Spanned;
    const-class v0, Ljava/lang/Object;

    #@46
    invoke-interface {v9, p2, p3, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@49
    move-result-object v10

    #@4a
    .line 74
    .local v10, spans:[Ljava/lang/Object;
    const/4 v7, 0x0

    #@4b
    .local v7, i:I
    :goto_4b
    array-length v0, v10

    #@4c
    if-ge v7, v0, :cond_86

    #@4e
    .line 75
    aget-object v0, v10, v7

    #@50
    instance-of v0, v0, Landroid/text/NoCopySpan;

    #@52
    if-eqz v0, :cond_57

    #@54
    .line 74
    :goto_54
    add-int/lit8 v7, v7, 0x1

    #@56
    goto :goto_4b

    #@57
    .line 79
    :cond_57
    aget-object v0, v10, v7

    #@59
    invoke-interface {v9, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@5c
    move-result v0

    #@5d
    sub-int v3, v0, p2

    #@5f
    .line 80
    .local v3, st:I
    aget-object v0, v10, v7

    #@61
    invoke-interface {v9, v0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@64
    move-result v0

    #@65
    sub-int v4, v0, p2

    #@67
    .line 81
    .local v4, en:I
    aget-object v0, v10, v7

    #@69
    invoke-interface {v9, v0}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@6c
    move-result v5

    #@6d
    .line 83
    .local v5, fl:I
    if-gez v3, :cond_70

    #@6f
    .line 84
    const/4 v3, 0x0

    #@70
    .line 85
    :cond_70
    sub-int v0, p3, p2

    #@72
    if-le v3, v0, :cond_76

    #@74
    .line 86
    sub-int v3, p3, p2

    #@76
    .line 88
    :cond_76
    if-gez v4, :cond_79

    #@78
    .line 89
    const/4 v4, 0x0

    #@79
    .line 90
    :cond_79
    sub-int v0, p3, p2

    #@7b
    if-le v4, v0, :cond_7f

    #@7d
    .line 91
    sub-int v4, p3, p2

    #@7f
    .line 93
    :cond_7f
    aget-object v2, v10, v7

    #@81
    move-object v0, p0

    #@82
    invoke-direct/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@85
    goto :goto_54

    #@86
    .line 96
    .end local v3           #st:I
    .end local v4           #en:I
    .end local v5           #fl:I
    .end local v7           #i:I
    .end local v9           #sp:Landroid/text/Spanned;
    .end local v10           #spans:[Ljava/lang/Object;
    :cond_86
    return-void
.end method

.method private change(IILjava/lang/CharSequence;II)V
    .registers 44
    .parameter "start"
    .parameter "end"
    .parameter "cs"
    .parameter "csStart"
    .parameter "csEnd"

    #@0
    .prologue
    .line 263
    sub-int v33, p2, p1

    #@2
    .line 264
    .local v33, replacedLength:I
    sub-int v34, p5, p4

    #@4
    .line 265
    .local v34, replacementLength:I
    sub-int v12, v34, v33

    #@6
    .line 267
    .local v12, nbNewChars:I
    move-object/from16 v0, p0

    #@8
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@a
    add-int/lit8 v30, v4, -0x1

    #@c
    .local v30, i:I
    :goto_c
    if-ltz v30, :cond_d9

    #@e
    .line 268
    move-object/from16 v0, p0

    #@10
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@12
    aget v7, v4, v30

    #@14
    .line 269
    .local v7, spanStart:I
    move-object/from16 v0, p0

    #@16
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@18
    if-le v7, v4, :cond_1f

    #@1a
    .line 270
    move-object/from16 v0, p0

    #@1c
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1e
    sub-int/2addr v7, v4

    #@1f
    .line 272
    :cond_1f
    move-object/from16 v0, p0

    #@21
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@23
    aget v8, v4, v30

    #@25
    .line 273
    .local v8, spanEnd:I
    move-object/from16 v0, p0

    #@27
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@29
    if-le v8, v4, :cond_30

    #@2b
    .line 274
    move-object/from16 v0, p0

    #@2d
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2f
    sub-int/2addr v8, v4

    #@30
    .line 276
    :cond_30
    move-object/from16 v0, p0

    #@32
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@34
    aget v4, v4, v30

    #@36
    and-int/lit8 v4, v4, 0x33

    #@38
    const/16 v5, 0x33

    #@3a
    if-ne v4, v5, :cond_9a

    #@3c
    .line 277
    move/from16 v32, v7

    #@3e
    .line 278
    .local v32, ost:I
    move/from16 v31, v8

    #@40
    .line 279
    .local v31, oen:I
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@43
    move-result v27

    #@44
    .line 281
    .local v27, clen:I
    move/from16 v0, p1

    #@46
    if-le v7, v0, :cond_62

    #@48
    move/from16 v0, p2

    #@4a
    if-gt v7, v0, :cond_62

    #@4c
    .line 282
    move/from16 v7, p2

    #@4e
    :goto_4e
    move/from16 v0, v27

    #@50
    if-ge v7, v0, :cond_62

    #@52
    .line 283
    move/from16 v0, p2

    #@54
    if-le v7, v0, :cond_be

    #@56
    add-int/lit8 v4, v7, -0x1

    #@58
    move-object/from16 v0, p0

    #@5a
    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@5d
    move-result v4

    #@5e
    const/16 v5, 0xa

    #@60
    if-ne v4, v5, :cond_be

    #@62
    .line 287
    :cond_62
    move/from16 v0, p1

    #@64
    if-le v8, v0, :cond_80

    #@66
    move/from16 v0, p2

    #@68
    if-gt v8, v0, :cond_80

    #@6a
    .line 288
    move/from16 v8, p2

    #@6c
    :goto_6c
    move/from16 v0, v27

    #@6e
    if-ge v8, v0, :cond_80

    #@70
    .line 289
    move/from16 v0, p2

    #@72
    if-le v8, v0, :cond_c1

    #@74
    add-int/lit8 v4, v8, -0x1

    #@76
    move-object/from16 v0, p0

    #@78
    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@7b
    move-result v4

    #@7c
    const/16 v5, 0xa

    #@7e
    if-ne v4, v5, :cond_c1

    #@80
    .line 293
    :cond_80
    move/from16 v0, v32

    #@82
    if-ne v7, v0, :cond_88

    #@84
    move/from16 v0, v31

    #@86
    if-eq v8, v0, :cond_9a

    #@88
    .line 294
    :cond_88
    const/4 v5, 0x0

    #@89
    move-object/from16 v0, p0

    #@8b
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@8d
    aget-object v6, v4, v30

    #@8f
    move-object/from16 v0, p0

    #@91
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@93
    aget v9, v4, v30

    #@95
    move-object/from16 v4, p0

    #@97
    invoke-direct/range {v4 .. v9}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@9a
    .line 297
    .end local v27           #clen:I
    .end local v31           #oen:I
    .end local v32           #ost:I
    :cond_9a
    const/16 v29, 0x0

    #@9c
    .line 298
    .local v29, flags:I
    move/from16 v0, p1

    #@9e
    if-ne v7, v0, :cond_c4

    #@a0
    move/from16 v0, v29

    #@a2
    or-int/lit16 v0, v0, 0x1000

    #@a4
    move/from16 v29, v0

    #@a6
    .line 300
    :cond_a6
    :goto_a6
    move/from16 v0, p1

    #@a8
    if-ne v8, v0, :cond_cf

    #@aa
    move/from16 v0, v29

    #@ac
    or-int/lit16 v0, v0, 0x4000

    #@ae
    move/from16 v29, v0

    #@b0
    .line 302
    :cond_b0
    :goto_b0
    move-object/from16 v0, p0

    #@b2
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@b4
    aget v5, v4, v30

    #@b6
    or-int v5, v5, v29

    #@b8
    aput v5, v4, v30

    #@ba
    .line 267
    add-int/lit8 v30, v30, -0x1

    #@bc
    goto/16 :goto_c

    #@be
    .line 282
    .end local v29           #flags:I
    .restart local v27       #clen:I
    .restart local v31       #oen:I
    .restart local v32       #ost:I
    :cond_be
    add-int/lit8 v7, v7, 0x1

    #@c0
    goto :goto_4e

    #@c1
    .line 288
    :cond_c1
    add-int/lit8 v8, v8, 0x1

    #@c3
    goto :goto_6c

    #@c4
    .line 299
    .end local v27           #clen:I
    .end local v31           #oen:I
    .end local v32           #ost:I
    .restart local v29       #flags:I
    :cond_c4
    add-int v4, p2, v12

    #@c6
    if-ne v7, v4, :cond_a6

    #@c8
    move/from16 v0, v29

    #@ca
    or-int/lit16 v0, v0, 0x2000

    #@cc
    move/from16 v29, v0

    #@ce
    goto :goto_a6

    #@cf
    .line 301
    :cond_cf
    add-int v4, p2, v12

    #@d1
    if-ne v8, v4, :cond_b0

    #@d3
    const v4, 0x8000

    #@d6
    or-int v29, v29, v4

    #@d8
    goto :goto_b0

    #@d9
    .line 305
    .end local v7           #spanStart:I
    .end local v8           #spanEnd:I
    .end local v29           #flags:I
    :cond_d9
    move-object/from16 v0, p0

    #@db
    move/from16 v1, p2

    #@dd
    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;->moveGapTo(I)V

    #@e0
    .line 307
    move-object/from16 v0, p0

    #@e2
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@e4
    if-lt v12, v4, :cond_f6

    #@e6
    .line 308
    move-object/from16 v0, p0

    #@e8
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@ea
    array-length v4, v4

    #@eb
    add-int/2addr v4, v12

    #@ec
    move-object/from16 v0, p0

    #@ee
    iget v5, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@f0
    sub-int/2addr v4, v5

    #@f1
    move-object/from16 v0, p0

    #@f3
    invoke-direct {v0, v4}, Landroid/text/SpannableStringBuilder;->resizeFor(I)V

    #@f6
    .line 311
    :cond_f6
    if-nez v34, :cond_167

    #@f8
    const/4 v15, 0x1

    #@f9
    .line 314
    .local v15, textIsRemoved:Z
    :goto_f9
    if-lez v33, :cond_16c

    #@fb
    .line 319
    const/16 v30, 0x0

    #@fd
    .line 320
    :goto_fd
    move-object/from16 v0, p0

    #@ff
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@101
    move/from16 v0, v30

    #@103
    if-ge v0, v4, :cond_16c

    #@105
    .line 321
    move-object/from16 v0, p0

    #@107
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@109
    aget v4, v4, v30

    #@10b
    and-int/lit8 v4, v4, 0x21

    #@10d
    const/16 v5, 0x21

    #@10f
    if-ne v4, v5, :cond_169

    #@111
    move-object/from16 v0, p0

    #@113
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@115
    aget v4, v4, v30

    #@117
    move/from16 v0, p1

    #@119
    if-lt v4, v0, :cond_169

    #@11b
    move-object/from16 v0, p0

    #@11d
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@11f
    aget v4, v4, v30

    #@121
    move-object/from16 v0, p0

    #@123
    iget v5, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@125
    move-object/from16 v0, p0

    #@127
    iget v6, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@129
    add-int/2addr v5, v6

    #@12a
    if-ge v4, v5, :cond_169

    #@12c
    move-object/from16 v0, p0

    #@12e
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@130
    aget v4, v4, v30

    #@132
    move/from16 v0, p1

    #@134
    if-lt v4, v0, :cond_169

    #@136
    move-object/from16 v0, p0

    #@138
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@13a
    aget v4, v4, v30

    #@13c
    move-object/from16 v0, p0

    #@13e
    iget v5, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@140
    move-object/from16 v0, p0

    #@142
    iget v6, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@144
    add-int/2addr v5, v6

    #@145
    if-ge v4, v5, :cond_169

    #@147
    if-nez v15, :cond_15f

    #@149
    move-object/from16 v0, p0

    #@14b
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@14d
    aget v4, v4, v30

    #@14f
    move/from16 v0, p1

    #@151
    if-gt v4, v0, :cond_15f

    #@153
    move-object/from16 v0, p0

    #@155
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@157
    aget v4, v4, v30

    #@159
    move-object/from16 v0, p0

    #@15b
    iget v5, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@15d
    if-ge v4, v5, :cond_169

    #@15f
    .line 327
    :cond_15f
    move-object/from16 v0, p0

    #@161
    move/from16 v1, v30

    #@163
    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(I)V

    #@166
    goto :goto_fd

    #@167
    .line 311
    .end local v15           #textIsRemoved:Z
    :cond_167
    const/4 v15, 0x0

    #@168
    goto :goto_f9

    #@169
    .line 331
    .restart local v15       #textIsRemoved:Z
    :cond_169
    add-int/lit8 v30, v30, 0x1

    #@16b
    goto :goto_fd

    #@16c
    .line 335
    :cond_16c
    move-object/from16 v0, p0

    #@16e
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@170
    add-int/2addr v4, v12

    #@171
    move-object/from16 v0, p0

    #@173
    iput v4, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@175
    .line 336
    move-object/from16 v0, p0

    #@177
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@179
    sub-int/2addr v4, v12

    #@17a
    move-object/from16 v0, p0

    #@17c
    iput v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@17e
    .line 338
    move-object/from16 v0, p0

    #@180
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@182
    const/4 v5, 0x1

    #@183
    if-ge v4, v5, :cond_190

    #@185
    .line 339
    new-instance v4, Ljava/lang/Exception;

    #@187
    const-string/jumbo v5, "mGapLength < 1"

    #@18a
    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@18d
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    #@190
    .line 341
    :cond_190
    move-object/from16 v0, p0

    #@192
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@194
    move-object/from16 v0, p3

    #@196
    move/from16 v1, p4

    #@198
    move/from16 v2, p5

    #@19a
    move/from16 v3, p1

    #@19c
    invoke-static {v0, v1, v2, v4, v3}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    #@19f
    .line 343
    if-lez v33, :cond_201

    #@1a1
    .line 344
    move-object/from16 v0, p0

    #@1a3
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    iget v5, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1a9
    add-int/2addr v4, v5

    #@1aa
    move-object/from16 v0, p0

    #@1ac
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1ae
    array-length v5, v5

    #@1af
    if-ne v4, v5, :cond_1ff

    #@1b1
    const/4 v14, 0x1

    #@1b2
    .line 346
    .local v14, atEnd:Z
    :goto_1b2
    const/16 v30, 0x0

    #@1b4
    :goto_1b4
    move-object/from16 v0, p0

    #@1b6
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1b8
    move/from16 v0, v30

    #@1ba
    if-ge v0, v4, :cond_201

    #@1bc
    .line 347
    move-object/from16 v0, p0

    #@1be
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@1c0
    aget v4, v4, v30

    #@1c2
    and-int/lit16 v4, v4, 0xf0

    #@1c4
    shr-int/lit8 v13, v4, 0x4

    #@1c6
    .line 348
    .local v13, startFlag:I
    move-object/from16 v0, p0

    #@1c8
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@1ca
    move-object/from16 v0, p0

    #@1cc
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@1ce
    aget v10, v5, v30

    #@1d0
    move-object/from16 v9, p0

    #@1d2
    move/from16 v11, p1

    #@1d4
    invoke-direct/range {v9 .. v15}, Landroid/text/SpannableStringBuilder;->updatedIntervalBound(IIIIZZ)I

    #@1d7
    move-result v5

    #@1d8
    aput v5, v4, v30

    #@1da
    .line 351
    move-object/from16 v0, p0

    #@1dc
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@1de
    aget v4, v4, v30

    #@1e0
    and-int/lit8 v20, v4, 0xf

    #@1e2
    .line 352
    .local v20, endFlag:I
    move-object/from16 v0, p0

    #@1e4
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@1e6
    move-object/from16 v0, p0

    #@1e8
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@1ea
    aget v17, v5, v30

    #@1ec
    move-object/from16 v16, p0

    #@1ee
    move/from16 v18, p1

    #@1f0
    move/from16 v19, v12

    #@1f2
    move/from16 v21, v14

    #@1f4
    move/from16 v22, v15

    #@1f6
    invoke-direct/range {v16 .. v22}, Landroid/text/SpannableStringBuilder;->updatedIntervalBound(IIIIZZ)I

    #@1f9
    move-result v5

    #@1fa
    aput v5, v4, v30

    #@1fc
    .line 346
    add-int/lit8 v30, v30, 0x1

    #@1fe
    goto :goto_1b4

    #@1ff
    .line 344
    .end local v13           #startFlag:I
    .end local v14           #atEnd:Z
    .end local v20           #endFlag:I
    :cond_1ff
    const/4 v14, 0x0

    #@200
    goto :goto_1b2

    #@201
    .line 357
    :cond_201
    move-object/from16 v0, p0

    #@203
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@205
    move-object/from16 v0, p0

    #@207
    iput v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCountBeforeAdd:I

    #@209
    .line 359
    move-object/from16 v0, p3

    #@20b
    instance-of v4, v0, Landroid/text/Spanned;

    #@20d
    if-eqz v4, :cond_26e

    #@20f
    move-object/from16 v35, p3

    #@211
    .line 360
    check-cast v35, Landroid/text/Spanned;

    #@213
    .line 361
    .local v35, sp:Landroid/text/Spanned;
    const-class v4, Ljava/lang/Object;

    #@215
    move-object/from16 v0, v35

    #@217
    move/from16 v1, p4

    #@219
    move/from16 v2, p5

    #@21b
    invoke-interface {v0, v1, v2, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@21e
    move-result-object v36

    #@21f
    .line 363
    .local v36, spans:[Ljava/lang/Object;
    const/16 v30, 0x0

    #@221
    :goto_221
    move-object/from16 v0, v36

    #@223
    array-length v4, v0

    #@224
    move/from16 v0, v30

    #@226
    if-ge v0, v4, :cond_26e

    #@228
    .line 364
    aget-object v4, v36, v30

    #@22a
    move-object/from16 v0, v35

    #@22c
    invoke-interface {v0, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@22f
    move-result v37

    #@230
    .line 365
    .local v37, st:I
    aget-object v4, v36, v30

    #@232
    move-object/from16 v0, v35

    #@234
    invoke-interface {v0, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@237
    move-result v28

    #@238
    .line 367
    .local v28, en:I
    move/from16 v0, v37

    #@23a
    move/from16 v1, p4

    #@23c
    if-ge v0, v1, :cond_240

    #@23e
    move/from16 v37, p4

    #@240
    .line 368
    :cond_240
    move/from16 v0, v28

    #@242
    move/from16 v1, p5

    #@244
    if-le v0, v1, :cond_248

    #@246
    move/from16 v28, p5

    #@248
    .line 371
    :cond_248
    aget-object v4, v36, v30

    #@24a
    move-object/from16 v0, p0

    #@24c
    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    #@24f
    move-result v4

    #@250
    if-gez v4, :cond_26b

    #@252
    .line 372
    const/16 v22, 0x0

    #@254
    aget-object v23, v36, v30

    #@256
    sub-int v4, v37, p4

    #@258
    add-int v24, v4, p1

    #@25a
    sub-int v4, v28, p4

    #@25c
    add-int v25, v4, p1

    #@25e
    aget-object v4, v36, v30

    #@260
    move-object/from16 v0, v35

    #@262
    invoke-interface {v0, v4}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@265
    move-result v26

    #@266
    move-object/from16 v21, p0

    #@268
    invoke-direct/range {v21 .. v26}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@26b
    .line 363
    :cond_26b
    add-int/lit8 v30, v30, 0x1

    #@26d
    goto :goto_221

    #@26e
    .line 377
    .end local v28           #en:I
    .end local v35           #sp:Landroid/text/Spanned;
    .end local v36           #spans:[Ljava/lang/Object;
    .end local v37           #st:I
    :cond_26e
    return-void
.end method

.method private checkRange(Ljava/lang/String;II)V
    .registers 8
    .parameter "operation"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1016
    if-ge p3, p2, :cond_29

    #@2
    .line 1017
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    const-string v3, " "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-static {p2, p3}, Landroid/text/SpannableStringBuilder;->region(II)Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, " has end before start"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@28
    throw v1

    #@29
    .line 1021
    :cond_29
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@2c
    move-result v0

    #@2d
    .line 1023
    .local v0, len:I
    if-gt p2, v0, :cond_31

    #@2f
    if-le p3, v0, :cond_5c

    #@31
    .line 1024
    :cond_31
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    const-string v3, " "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-static {p2, p3}, Landroid/text/SpannableStringBuilder;->region(II)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, " ends beyond length "

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@5b
    throw v1

    #@5c
    .line 1028
    :cond_5c
    if-ltz p2, :cond_60

    #@5e
    if-gez p3, :cond_87

    #@60
    .line 1029
    :cond_60
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@62
    new-instance v2, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v2

    #@6b
    const-string v3, " "

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-static {p2, p3}, Landroid/text/SpannableStringBuilder;->region(II)Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v2

    #@79
    const-string v3, " starts before 0"

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@86
    throw v1

    #@87
    .line 1032
    :cond_87
    return-void
.end method

.method private static hasNonExclusiveExclusiveSpanAt(Ljava/lang/CharSequence;I)Z
    .registers 9
    .parameter "text"
    .parameter "offset"

    #@0
    .prologue
    .line 506
    instance-of v6, p0, Landroid/text/Spanned;

    #@2
    if-eqz v6, :cond_20

    #@4
    move-object v4, p0

    #@5
    .line 507
    check-cast v4, Landroid/text/Spanned;

    #@7
    .line 508
    .local v4, spanned:Landroid/text/Spanned;
    const-class v6, Ljava/lang/Object;

    #@9
    invoke-interface {v4, p1, p1, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@c
    move-result-object v5

    #@d
    .line 509
    .local v5, spans:[Ljava/lang/Object;
    array-length v2, v5

    #@e
    .line 510
    .local v2, length:I
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    if-ge v1, v2, :cond_20

    #@11
    .line 511
    aget-object v3, v5, v1

    #@13
    .line 512
    .local v3, span:Ljava/lang/Object;
    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    #@16
    move-result v0

    #@17
    .line 513
    .local v0, flags:I
    const/16 v6, 0x21

    #@19
    if-eq v0, v6, :cond_1d

    #@1b
    const/4 v6, 0x1

    #@1c
    .line 516
    .end local v0           #flags:I
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #span:Ljava/lang/Object;
    .end local v4           #spanned:Landroid/text/Spanned;
    .end local v5           #spans:[Ljava/lang/Object;
    :goto_1c
    return v6

    #@1d
    .line 510
    .restart local v0       #flags:I
    .restart local v1       #i:I
    .restart local v2       #length:I
    .restart local v3       #span:Ljava/lang/Object;
    .restart local v4       #spanned:Landroid/text/Spanned;
    .restart local v5       #spans:[Ljava/lang/Object;
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    #@1f
    goto :goto_f

    #@20
    .line 516
    .end local v0           #flags:I
    .end local v1           #i:I
    .end local v2           #length:I
    .end local v3           #span:Ljava/lang/Object;
    .end local v4           #spanned:Landroid/text/Spanned;
    .end local v5           #spans:[Ljava/lang/Object;
    :cond_20
    const/4 v6, 0x0

    #@21
    goto :goto_1c
.end method

.method private moveGapTo(I)V
    .registers 14
    .parameter "where"

    #@0
    .prologue
    const/4 v11, 0x3

    #@1
    const/4 v10, 0x2

    #@2
    .line 153
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@4
    if-ne p1, v6, :cond_7

    #@6
    .line 198
    :goto_6
    return-void

    #@7
    .line 156
    :cond_7
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@a
    move-result v6

    #@b
    if-ne p1, v6, :cond_53

    #@d
    const/4 v0, 0x1

    #@e
    .line 158
    .local v0, atEnd:Z
    :goto_e
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@10
    if-ge p1, v6, :cond_55

    #@12
    .line 159
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@14
    sub-int v4, v6, p1

    #@16
    .line 160
    .local v4, overlap:I
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@18
    iget-object v7, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1a
    iget v8, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1c
    iget v9, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1e
    add-int/2addr v8, v9

    #@1f
    sub-int/2addr v8, v4

    #@20
    invoke-static {v6, p1, v7, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@23
    .line 167
    :goto_23
    const/4 v3, 0x0

    #@24
    .local v3, i:I
    :goto_24
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@26
    if-ge v3, v6, :cond_8d

    #@28
    .line 168
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@2a
    aget v5, v6, v3

    #@2c
    .line 169
    .local v5, start:I
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@2e
    aget v1, v6, v3

    #@30
    .line 171
    .local v1, end:I
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@32
    if-le v5, v6, :cond_37

    #@34
    .line 172
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@36
    sub-int/2addr v5, v6

    #@37
    .line 173
    :cond_37
    if-le v5, p1, :cond_67

    #@39
    .line 174
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@3b
    add-int/2addr v5, v6

    #@3c
    .line 182
    :cond_3c
    :goto_3c
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@3e
    if-le v1, v6, :cond_43

    #@40
    .line 183
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@42
    sub-int/2addr v1, v6

    #@43
    .line 184
    :cond_43
    if-le v1, p1, :cond_7b

    #@45
    .line 185
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@47
    add-int/2addr v1, v6

    #@48
    .line 193
    :cond_48
    :goto_48
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@4a
    aput v5, v6, v3

    #@4c
    .line 194
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@4e
    aput v1, v6, v3

    #@50
    .line 167
    add-int/lit8 v3, v3, 0x1

    #@52
    goto :goto_24

    #@53
    .line 156
    .end local v0           #atEnd:Z
    .end local v1           #end:I
    .end local v3           #i:I
    .end local v4           #overlap:I
    .end local v5           #start:I
    :cond_53
    const/4 v0, 0x0

    #@54
    goto :goto_e

    #@55
    .line 162
    .restart local v0       #atEnd:Z
    :cond_55
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@57
    sub-int v4, p1, v6

    #@59
    .line 163
    .restart local v4       #overlap:I
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@5b
    iget v7, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@5d
    add-int/2addr v7, p1

    #@5e
    sub-int/2addr v7, v4

    #@5f
    iget-object v8, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@61
    iget v9, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@63
    invoke-static {v6, v7, v8, v9, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@66
    goto :goto_23

    #@67
    .line 175
    .restart local v1       #end:I
    .restart local v3       #i:I
    .restart local v5       #start:I
    :cond_67
    if-ne v5, p1, :cond_3c

    #@69
    .line 176
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@6b
    aget v6, v6, v3

    #@6d
    and-int/lit16 v6, v6, 0xf0

    #@6f
    shr-int/lit8 v2, v6, 0x4

    #@71
    .line 178
    .local v2, flag:I
    if-eq v2, v10, :cond_77

    #@73
    if-eqz v0, :cond_3c

    #@75
    if-ne v2, v11, :cond_3c

    #@77
    .line 179
    :cond_77
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@79
    add-int/2addr v5, v6

    #@7a
    goto :goto_3c

    #@7b
    .line 186
    .end local v2           #flag:I
    :cond_7b
    if-ne v1, p1, :cond_48

    #@7d
    .line 187
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@7f
    aget v6, v6, v3

    #@81
    and-int/lit8 v2, v6, 0xf

    #@83
    .line 189
    .restart local v2       #flag:I
    if-eq v2, v10, :cond_89

    #@85
    if-eqz v0, :cond_48

    #@87
    if-ne v2, v11, :cond_48

    #@89
    .line 190
    :cond_89
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@8b
    add-int/2addr v1, v6

    #@8c
    goto :goto_48

    #@8d
    .line 197
    .end local v1           #end:I
    .end local v2           #flag:I
    .end local v5           #start:I
    :cond_8d
    iput p1, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@8f
    goto/16 :goto_6
.end method

.method private static region(II)Ljava/lang/String;
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1012
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " ... "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ")"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    return-object v0
.end method

.method private removeSpan(I)V
    .registers 9
    .parameter "i"

    #@0
    .prologue
    .line 412
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@2
    aget-object v2, v4, p1

    #@4
    .line 414
    .local v2, object:Ljava/lang/Object;
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@6
    aget v3, v4, p1

    #@8
    .line 415
    .local v3, start:I
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@a
    aget v1, v4, p1

    #@c
    .line 417
    .local v1, end:I
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@e
    if-le v3, v4, :cond_13

    #@10
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@12
    sub-int/2addr v3, v4

    #@13
    .line 418
    :cond_13
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@15
    if-le v1, v4, :cond_1a

    #@17
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@19
    sub-int/2addr v1, v4

    #@1a
    .line 420
    :cond_1a
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1c
    add-int/lit8 v5, p1, 0x1

    #@1e
    sub-int v0, v4, v5

    #@20
    .line 421
    .local v0, count:I
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@22
    add-int/lit8 v5, p1, 0x1

    #@24
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@26
    invoke-static {v4, v5, v6, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@29
    .line 422
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@2b
    add-int/lit8 v5, p1, 0x1

    #@2d
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@2f
    invoke-static {v4, v5, v6, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@32
    .line 423
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@34
    add-int/lit8 v5, p1, 0x1

    #@36
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@38
    invoke-static {v4, v5, v6, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3b
    .line 424
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@3d
    add-int/lit8 v5, p1, 0x1

    #@3f
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@41
    invoke-static {v4, v5, v6, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@44
    .line 426
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@46
    add-int/lit8 v4, v4, -0x1

    #@48
    iput v4, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@4a
    .line 428
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@4c
    iget v5, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@4e
    const/4 v6, 0x0

    #@4f
    aput-object v6, v4, v5

    #@51
    .line 430
    invoke-direct {p0, v2, v3, v1}, Landroid/text/SpannableStringBuilder;->sendSpanRemoved(Ljava/lang/Object;II)V

    #@54
    .line 431
    return-void
.end method

.method private resizeFor(I)V
    .registers 11
    .parameter "size"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 131
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@3
    array-length v5, v6

    #@4
    .line 132
    .local v5, oldLength:I
    add-int/lit8 v6, p1, 0x1

    #@6
    invoke-static {v6}, Lcom/android/internal/util/ArrayUtils;->idealCharArraySize(I)I

    #@9
    move-result v3

    #@a
    .line 133
    .local v3, newLength:I
    sub-int v1, v3, v5

    #@c
    .line 134
    .local v1, delta:I
    if-nez v1, :cond_f

    #@e
    .line 150
    :cond_e
    return-void

    #@f
    .line 136
    :cond_f
    new-array v4, v3, [C

    #@11
    .line 137
    .local v4, newText:[C
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@13
    iget v7, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@15
    invoke-static {v6, v8, v4, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@18
    .line 138
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1a
    iget v7, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1c
    add-int/2addr v6, v7

    #@1d
    sub-int v0, v5, v6

    #@1f
    .line 139
    .local v0, after:I
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@21
    sub-int v7, v5, v0

    #@23
    sub-int v8, v3, v0

    #@25
    invoke-static {v6, v7, v4, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@28
    .line 140
    iput-object v4, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@2a
    .line 142
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2c
    add-int/2addr v6, v1

    #@2d
    iput v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2f
    .line 143
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@31
    const/4 v7, 0x1

    #@32
    if-ge v6, v7, :cond_3f

    #@34
    .line 144
    new-instance v6, Ljava/lang/Exception;

    #@36
    const-string/jumbo v7, "mGapLength < 1"

    #@39
    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@3c
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    #@3f
    .line 146
    :cond_3f
    const/4 v2, 0x0

    #@40
    .local v2, i:I
    :goto_40
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@42
    if-ge v2, v6, :cond_e

    #@44
    .line 147
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@46
    aget v6, v6, v2

    #@48
    iget v7, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@4a
    if-le v6, v7, :cond_53

    #@4c
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@4e
    aget v7, v6, v2

    #@50
    add-int/2addr v7, v1

    #@51
    aput v7, v6, v2

    #@53
    .line 148
    :cond_53
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@55
    aget v6, v6, v2

    #@57
    iget v7, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@59
    if-le v6, v7, :cond_62

    #@5b
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@5d
    aget v7, v6, v2

    #@5f
    add-int/2addr v7, v1

    #@60
    aput v7, v6, v2

    #@62
    .line 146
    :cond_62
    add-int/lit8 v2, v2, 0x1

    #@64
    goto :goto_40
.end method

.method private sendAfterTextChanged([Landroid/text/TextWatcher;)V
    .registers 5
    .parameter "watchers"

    #@0
    .prologue
    .line 975
    array-length v1, p1

    #@1
    .line 977
    .local v1, n:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_c

    #@4
    .line 978
    aget-object v2, p1, v0

    #@6
    invoke-interface {v2, p0}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    #@9
    .line 977
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_2

    #@c
    .line 980
    :cond_c
    return-void
.end method

.method private sendBeforeTextChanged([Landroid/text/TextWatcher;III)V
    .registers 8
    .parameter "watchers"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 959
    array-length v1, p1

    #@1
    .line 961
    .local v1, n:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_c

    #@4
    .line 962
    aget-object v2, p1, v0

    #@6
    invoke-interface {v2, p0, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    #@9
    .line 961
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_2

    #@c
    .line 964
    :cond_c
    return-void
.end method

.method private sendSpanAdded(Ljava/lang/Object;II)V
    .registers 8
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 983
    const-class v3, Landroid/text/SpanWatcher;

    #@2
    invoke-virtual {p0, p2, p3, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, [Landroid/text/SpanWatcher;

    #@8
    .line 984
    .local v2, recip:[Landroid/text/SpanWatcher;
    array-length v1, v2

    #@9
    .line 986
    .local v1, n:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_14

    #@c
    .line 987
    aget-object v3, v2, v0

    #@e
    invoke-interface {v3, p0, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@11
    .line 986
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_a

    #@14
    .line 989
    :cond_14
    return-void
.end method

.method private sendSpanChanged(Ljava/lang/Object;IIII)V
    .registers 16
    .parameter "what"
    .parameter "oldStart"
    .parameter "oldEnd"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 1003
    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    #@3
    move-result v0

    #@4
    invoke-static {p3, p5}, Ljava/lang/Math;->max(II)I

    #@7
    move-result v1

    #@8
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@b
    move-result v2

    #@c
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@f
    move-result v1

    #@10
    const-class v2, Landroid/text/SpanWatcher;

    #@12
    invoke-virtual {p0, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@15
    move-result-object v9

    #@16
    check-cast v9, [Landroid/text/SpanWatcher;

    #@18
    .line 1005
    .local v9, spanWatchers:[Landroid/text/SpanWatcher;
    array-length v8, v9

    #@19
    .line 1006
    .local v8, n:I
    const/4 v7, 0x0

    #@1a
    .local v7, i:I
    :goto_1a
    if-ge v7, v8, :cond_2a

    #@1c
    .line 1007
    aget-object v0, v9, v7

    #@1e
    move-object v1, p0

    #@1f
    move-object v2, p1

    #@20
    move v3, p2

    #@21
    move v4, p3

    #@22
    move v5, p4

    #@23
    move v6, p5

    #@24
    invoke-interface/range {v0 .. v6}, Landroid/text/SpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    #@27
    .line 1006
    add-int/lit8 v7, v7, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 1009
    :cond_2a
    return-void
.end method

.method private sendSpanRemoved(Ljava/lang/Object;II)V
    .registers 8
    .parameter "what"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 992
    const-class v3, Landroid/text/SpanWatcher;

    #@2
    invoke-virtual {p0, p2, p3, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, [Landroid/text/SpanWatcher;

    #@8
    .line 993
    .local v2, recip:[Landroid/text/SpanWatcher;
    array-length v1, v2

    #@9
    .line 995
    .local v1, n:I
    const/4 v0, 0x0

    #@a
    .local v0, i:I
    :goto_a
    if-ge v0, v1, :cond_14

    #@c
    .line 996
    aget-object v3, v2, v0

    #@e
    invoke-interface {v3, p0, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V

    #@11
    .line 995
    add-int/lit8 v0, v0, 0x1

    #@13
    goto :goto_a

    #@14
    .line 998
    :cond_14
    return-void
.end method

.method private sendTextChanged([Landroid/text/TextWatcher;III)V
    .registers 8
    .parameter "watchers"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 967
    array-length v1, p1

    #@1
    .line 969
    .local v1, n:I
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, v1, :cond_c

    #@4
    .line 970
    aget-object v2, p1, v0

    #@6
    invoke-interface {v2, p0, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    #@9
    .line 969
    add-int/lit8 v0, v0, 0x1

    #@b
    goto :goto_2

    #@c
    .line 972
    :cond_c
    return-void
.end method

.method private sendToSpanWatchers(III)V
    .registers 16
    .parameter "replaceStart"
    .parameter "replaceEnd"
    .parameter "nbNewChars"

    #@0
    .prologue
    const v11, 0x8000

    #@3
    .line 520
    const/4 v6, 0x0

    #@4
    .local v6, i:I
    :goto_4
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCountBeforeAdd:I

    #@6
    if-ge v6, v0, :cond_83

    #@8
    .line 521
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@a
    aget v4, v0, v6

    #@c
    .line 522
    .local v4, spanStart:I
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@e
    aget v5, v0, v6

    #@10
    .line 523
    .local v5, spanEnd:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@12
    if-le v4, v0, :cond_17

    #@14
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@16
    sub-int/2addr v4, v0

    #@17
    .line 524
    :cond_17
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@19
    if-le v5, v0, :cond_1e

    #@1b
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1d
    sub-int/2addr v5, v0

    #@1e
    .line 525
    :cond_1e
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@20
    aget v9, v0, v6

    #@22
    .line 527
    .local v9, spanFlags:I
    add-int v7, p2, p3

    #@24
    .line 528
    .local v7, newReplaceEnd:I
    const/4 v8, 0x0

    #@25
    .line 530
    .local v8, spanChanged:Z
    move v2, v4

    #@26
    .line 531
    .local v2, previousSpanStart:I
    if-le v4, v7, :cond_54

    #@28
    .line 532
    if-eqz p3, :cond_2c

    #@2a
    .line 533
    sub-int/2addr v2, p3

    #@2b
    .line 534
    const/4 v8, 0x1

    #@2c
    .line 549
    :cond_2c
    :goto_2c
    move v3, v5

    #@2d
    .line 550
    .local v3, previousSpanEnd:I
    if-le v5, v7, :cond_68

    #@2f
    .line 551
    if-eqz p3, :cond_33

    #@31
    .line 552
    sub-int/2addr v3, p3

    #@32
    .line 553
    const/4 v8, 0x1

    #@33
    .line 566
    :cond_33
    :goto_33
    if-eqz v8, :cond_47

    #@35
    .line 568
    if-le v2, v3, :cond_7a

    #@37
    .line 569
    const-string v0, "SpannableStringBuilder"

    #@39
    const-string v1, "IndexOutOfBound, previousSpanStart is bigger than previousSpanEnd"

    #@3b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 570
    move v3, v2

    #@3f
    .line 571
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@41
    aget-object v1, v0, v6

    #@43
    move-object v0, p0

    #@44
    invoke-direct/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->sendSpanChanged(Ljava/lang/Object;IIII)V

    #@47
    .line 577
    :cond_47
    :goto_47
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@49
    aget v1, v0, v6

    #@4b
    const v10, -0xf001

    #@4e
    and-int/2addr v1, v10

    #@4f
    aput v1, v0, v6

    #@51
    .line 520
    add-int/lit8 v6, v6, 0x1

    #@53
    goto :goto_4

    #@54
    .line 536
    .end local v3           #previousSpanEnd:I
    :cond_54
    if-lt v4, p1, :cond_2c

    #@56
    .line 538
    if-ne v4, p1, :cond_5e

    #@58
    and-int/lit16 v0, v9, 0x1000

    #@5a
    const/16 v1, 0x1000

    #@5c
    if-eq v0, v1, :cond_2c

    #@5e
    :cond_5e
    if-ne v4, v7, :cond_66

    #@60
    and-int/lit16 v0, v9, 0x2000

    #@62
    const/16 v1, 0x2000

    #@64
    if-eq v0, v1, :cond_2c

    #@66
    .line 545
    :cond_66
    const/4 v8, 0x1

    #@67
    goto :goto_2c

    #@68
    .line 555
    .restart local v3       #previousSpanEnd:I
    :cond_68
    if-lt v5, p1, :cond_33

    #@6a
    .line 557
    if-ne v5, p1, :cond_72

    #@6c
    and-int/lit16 v0, v9, 0x4000

    #@6e
    const/16 v1, 0x4000

    #@70
    if-eq v0, v1, :cond_33

    #@72
    :cond_72
    if-ne v5, v7, :cond_78

    #@74
    and-int v0, v9, v11

    #@76
    if-eq v0, v11, :cond_33

    #@78
    .line 562
    :cond_78
    const/4 v8, 0x1

    #@79
    goto :goto_33

    #@7a
    .line 573
    :cond_7a
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@7c
    aget-object v1, v0, v6

    #@7e
    move-object v0, p0

    #@7f
    invoke-direct/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->sendSpanChanged(Ljava/lang/Object;IIII)V

    #@82
    goto :goto_47

    #@83
    .line 581
    .end local v2           #previousSpanStart:I
    .end local v3           #previousSpanEnd:I
    .end local v4           #spanStart:I
    .end local v5           #spanEnd:I
    .end local v7           #newReplaceEnd:I
    .end local v8           #spanChanged:Z
    .end local v9           #spanFlags:I
    :cond_83
    iget v6, p0, Landroid/text/SpannableStringBuilder;->mSpanCountBeforeAdd:I

    #@85
    :goto_85
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@87
    if-ge v6, v0, :cond_a9

    #@89
    .line 582
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@8b
    aget v4, v0, v6

    #@8d
    .line 583
    .restart local v4       #spanStart:I
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@8f
    aget v5, v0, v6

    #@91
    .line 584
    .restart local v5       #spanEnd:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@93
    if-le v4, v0, :cond_98

    #@95
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@97
    sub-int/2addr v4, v0

    #@98
    .line 585
    :cond_98
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@9a
    if-le v5, v0, :cond_9f

    #@9c
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@9e
    sub-int/2addr v5, v0

    #@9f
    .line 586
    :cond_9f
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@a1
    aget-object v0, v0, v6

    #@a3
    invoke-direct {p0, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->sendSpanAdded(Ljava/lang/Object;II)V

    #@a6
    .line 581
    add-int/lit8 v6, v6, 0x1

    #@a8
    goto :goto_85

    #@a9
    .line 588
    .end local v4           #spanStart:I
    .end local v5           #spanEnd:I
    :cond_a9
    return-void
.end method

.method private setSpan(ZLjava/lang/Object;III)V
    .registers 28
    .parameter "send"
    .parameter "what"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    .line 600
    const-string/jumbo v3, "setSpan"

    #@3
    move-object/from16 v0, p0

    #@5
    move/from16 v1, p3

    #@7
    move/from16 v2, p4

    #@9
    invoke-direct {v0, v3, v1, v2}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@c
    .line 602
    move/from16 v0, p5

    #@e
    and-int/lit16 v3, v0, 0xf0

    #@10
    shr-int/lit8 v12, v3, 0x4

    #@12
    .line 603
    .local v12, flagsStart:I
    const/4 v3, 0x3

    #@13
    if-ne v12, v3, :cond_33

    #@15
    .line 604
    if-eqz p3, :cond_33

    #@17
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@1a
    move-result v3

    #@1b
    move/from16 v0, p3

    #@1d
    if-eq v0, v3, :cond_33

    #@1f
    .line 605
    add-int/lit8 v3, p3, -0x1

    #@21
    move-object/from16 v0, p0

    #@23
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@26
    move-result v9

    #@27
    .line 607
    .local v9, c:C
    const/16 v3, 0xa

    #@29
    if-eq v9, v3, :cond_33

    #@2b
    .line 608
    new-instance v3, Ljava/lang/RuntimeException;

    #@2d
    const-string v4, "PARAGRAPH span must start at paragraph boundary"

    #@2f
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@32
    throw v3

    #@33
    .line 612
    .end local v9           #c:C
    :cond_33
    and-int/lit8 v11, p5, 0xf

    #@35
    .line 613
    .local v11, flagsEnd:I
    const/4 v3, 0x3

    #@36
    if-ne v11, v3, :cond_56

    #@38
    .line 614
    if-eqz p4, :cond_56

    #@3a
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3d
    move-result v3

    #@3e
    move/from16 v0, p4

    #@40
    if-eq v0, v3, :cond_56

    #@42
    .line 615
    add-int/lit8 v3, p4, -0x1

    #@44
    move-object/from16 v0, p0

    #@46
    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    #@49
    move-result v9

    #@4a
    .line 617
    .restart local v9       #c:C
    const/16 v3, 0xa

    #@4c
    if-eq v9, v3, :cond_56

    #@4e
    .line 618
    new-instance v3, Ljava/lang/RuntimeException;

    #@50
    const-string v4, "PARAGRAPH span must end at paragraph boundary"

    #@52
    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    #@55
    throw v3

    #@56
    .line 623
    .end local v9           #c:C
    :cond_56
    const/4 v3, 0x2

    #@57
    if-ne v12, v3, :cond_6c

    #@59
    const/4 v3, 0x1

    #@5a
    if-ne v11, v3, :cond_6c

    #@5c
    move/from16 v0, p3

    #@5e
    move/from16 v1, p4

    #@60
    if-ne v0, v1, :cond_6c

    #@62
    .line 624
    if-eqz p1, :cond_6b

    #@64
    const-string v3, "SpannableStringBuilder"

    #@66
    const-string v4, "SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length"

    #@68
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 697
    :cond_6b
    :goto_6b
    return-void

    #@6c
    .line 632
    :cond_6c
    move/from16 v7, p3

    #@6e
    .line 633
    .local v7, nstart:I
    move/from16 v8, p4

    #@70
    .line 635
    .local v8, nend:I
    move-object/from16 v0, p0

    #@72
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@74
    move/from16 v0, p3

    #@76
    if-le v0, v3, :cond_dd

    #@78
    .line 636
    move-object/from16 v0, p0

    #@7a
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@7c
    add-int p3, p3, v3

    #@7e
    .line 642
    :cond_7e
    :goto_7e
    move-object/from16 v0, p0

    #@80
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@82
    move/from16 v0, p4

    #@84
    if-le v0, v3, :cond_fa

    #@86
    .line 643
    move-object/from16 v0, p0

    #@88
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@8a
    add-int p4, p4, v3

    #@8c
    .line 649
    :cond_8c
    :goto_8c
    move-object/from16 v0, p0

    #@8e
    iget v10, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@90
    .line 650
    .local v10, count:I
    move-object/from16 v0, p0

    #@92
    iget-object v0, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@94
    move-object/from16 v19, v0

    #@96
    .line 652
    .local v19, spans:[Ljava/lang/Object;
    const/4 v13, 0x0

    #@97
    .local v13, i:I
    :goto_97
    if-ge v13, v10, :cond_11c

    #@99
    .line 653
    aget-object v3, v19, v13

    #@9b
    move-object/from16 v0, p2

    #@9d
    if-ne v3, v0, :cond_118

    #@9f
    .line 654
    move-object/from16 v0, p0

    #@a1
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@a3
    aget v5, v3, v13

    #@a5
    .line 655
    .local v5, ostart:I
    move-object/from16 v0, p0

    #@a7
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@a9
    aget v6, v3, v13

    #@ab
    .line 657
    .local v6, oend:I
    move-object/from16 v0, p0

    #@ad
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@af
    if-le v5, v3, :cond_b6

    #@b1
    .line 658
    move-object/from16 v0, p0

    #@b3
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@b5
    sub-int/2addr v5, v3

    #@b6
    .line 659
    :cond_b6
    move-object/from16 v0, p0

    #@b8
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@ba
    if-le v6, v3, :cond_c1

    #@bc
    .line 660
    move-object/from16 v0, p0

    #@be
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@c0
    sub-int/2addr v6, v3

    #@c1
    .line 662
    :cond_c1
    move-object/from16 v0, p0

    #@c3
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@c5
    aput p3, v3, v13

    #@c7
    .line 663
    move-object/from16 v0, p0

    #@c9
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@cb
    aput p4, v3, v13

    #@cd
    .line 664
    move-object/from16 v0, p0

    #@cf
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@d1
    aput p5, v3, v13

    #@d3
    .line 666
    if-eqz p1, :cond_6b

    #@d5
    move-object/from16 v3, p0

    #@d7
    move-object/from16 v4, p2

    #@d9
    invoke-direct/range {v3 .. v8}, Landroid/text/SpannableStringBuilder;->sendSpanChanged(Ljava/lang/Object;IIII)V

    #@dc
    goto :goto_6b

    #@dd
    .line 637
    .end local v5           #ostart:I
    .end local v6           #oend:I
    .end local v10           #count:I
    .end local v13           #i:I
    .end local v19           #spans:[Ljava/lang/Object;
    :cond_dd
    move-object/from16 v0, p0

    #@df
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@e1
    move/from16 v0, p3

    #@e3
    if-ne v0, v3, :cond_7e

    #@e5
    .line 638
    const/4 v3, 0x2

    #@e6
    if-eq v12, v3, :cond_f3

    #@e8
    const/4 v3, 0x3

    #@e9
    if-ne v12, v3, :cond_7e

    #@eb
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@ee
    move-result v3

    #@ef
    move/from16 v0, p3

    #@f1
    if-ne v0, v3, :cond_7e

    #@f3
    .line 639
    :cond_f3
    move-object/from16 v0, p0

    #@f5
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@f7
    add-int p3, p3, v3

    #@f9
    goto :goto_7e

    #@fa
    .line 644
    :cond_fa
    move-object/from16 v0, p0

    #@fc
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@fe
    move/from16 v0, p4

    #@100
    if-ne v0, v3, :cond_8c

    #@102
    .line 645
    const/4 v3, 0x2

    #@103
    if-eq v11, v3, :cond_110

    #@105
    const/4 v3, 0x3

    #@106
    if-ne v11, v3, :cond_8c

    #@108
    invoke-virtual/range {p0 .. p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@10b
    move-result v3

    #@10c
    move/from16 v0, p4

    #@10e
    if-ne v0, v3, :cond_8c

    #@110
    .line 646
    :cond_110
    move-object/from16 v0, p0

    #@112
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@114
    add-int p4, p4, v3

    #@116
    goto/16 :goto_8c

    #@118
    .line 652
    .restart local v10       #count:I
    .restart local v13       #i:I
    .restart local v19       #spans:[Ljava/lang/Object;
    :cond_118
    add-int/lit8 v13, v13, 0x1

    #@11a
    goto/16 :goto_97

    #@11c
    .line 672
    :cond_11c
    move-object/from16 v0, p0

    #@11e
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@120
    add-int/lit8 v3, v3, 0x1

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@126
    array-length v4, v4

    #@127
    if-lt v3, v4, :cond_1ad

    #@129
    .line 673
    move-object/from16 v0, p0

    #@12b
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@12d
    add-int/lit8 v3, v3, 0x1

    #@12f
    invoke-static {v3}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@132
    move-result v14

    #@133
    .line 674
    .local v14, newsize:I
    new-array v0, v14, [Ljava/lang/Object;

    #@135
    move-object/from16 v17, v0

    #@137
    .line 675
    .local v17, newspans:[Ljava/lang/Object;
    new-array v0, v14, [I

    #@139
    move-object/from16 v18, v0

    #@13b
    .line 676
    .local v18, newspanstarts:[I
    new-array v15, v14, [I

    #@13d
    .line 677
    .local v15, newspanends:[I
    new-array v0, v14, [I

    #@13f
    move-object/from16 v16, v0

    #@141
    .line 679
    .local v16, newspanflags:[I
    move-object/from16 v0, p0

    #@143
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@145
    const/4 v4, 0x0

    #@146
    const/16 v20, 0x0

    #@148
    move-object/from16 v0, p0

    #@14a
    iget v0, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@14c
    move/from16 v21, v0

    #@14e
    move-object/from16 v0, v17

    #@150
    move/from16 v1, v20

    #@152
    move/from16 v2, v21

    #@154
    invoke-static {v3, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@157
    .line 680
    move-object/from16 v0, p0

    #@159
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@15b
    const/4 v4, 0x0

    #@15c
    const/16 v20, 0x0

    #@15e
    move-object/from16 v0, p0

    #@160
    iget v0, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@162
    move/from16 v21, v0

    #@164
    move-object/from16 v0, v18

    #@166
    move/from16 v1, v20

    #@168
    move/from16 v2, v21

    #@16a
    invoke-static {v3, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@16d
    .line 681
    move-object/from16 v0, p0

    #@16f
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@171
    const/4 v4, 0x0

    #@172
    const/16 v20, 0x0

    #@174
    move-object/from16 v0, p0

    #@176
    iget v0, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@178
    move/from16 v21, v0

    #@17a
    move/from16 v0, v20

    #@17c
    move/from16 v1, v21

    #@17e
    invoke-static {v3, v4, v15, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@181
    .line 682
    move-object/from16 v0, p0

    #@183
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@185
    const/4 v4, 0x0

    #@186
    const/16 v20, 0x0

    #@188
    move-object/from16 v0, p0

    #@18a
    iget v0, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@18c
    move/from16 v21, v0

    #@18e
    move-object/from16 v0, v16

    #@190
    move/from16 v1, v20

    #@192
    move/from16 v2, v21

    #@194
    invoke-static {v3, v4, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@197
    .line 684
    move-object/from16 v0, v17

    #@199
    move-object/from16 v1, p0

    #@19b
    iput-object v0, v1, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@19d
    .line 685
    move-object/from16 v0, v18

    #@19f
    move-object/from16 v1, p0

    #@1a1
    iput-object v0, v1, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@1a3
    .line 686
    move-object/from16 v0, p0

    #@1a5
    iput-object v15, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@1a7
    .line 687
    move-object/from16 v0, v16

    #@1a9
    move-object/from16 v1, p0

    #@1ab
    iput-object v0, v1, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@1ad
    .line 690
    .end local v14           #newsize:I
    .end local v15           #newspanends:[I
    .end local v16           #newspanflags:[I
    .end local v17           #newspans:[Ljava/lang/Object;
    .end local v18           #newspanstarts:[I
    :cond_1ad
    move-object/from16 v0, p0

    #@1af
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@1b1
    move-object/from16 v0, p0

    #@1b3
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1b5
    aput-object p2, v3, v4

    #@1b7
    .line 691
    move-object/from16 v0, p0

    #@1b9
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@1bb
    move-object/from16 v0, p0

    #@1bd
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1bf
    aput p3, v3, v4

    #@1c1
    .line 692
    move-object/from16 v0, p0

    #@1c3
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@1c5
    move-object/from16 v0, p0

    #@1c7
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1c9
    aput p4, v3, v4

    #@1cb
    .line 693
    move-object/from16 v0, p0

    #@1cd
    iget-object v3, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@1cf
    move-object/from16 v0, p0

    #@1d1
    iget v4, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1d3
    aput p5, v3, v4

    #@1d5
    .line 694
    move-object/from16 v0, p0

    #@1d7
    iget v3, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1d9
    add-int/lit8 v3, v3, 0x1

    #@1db
    move-object/from16 v0, p0

    #@1dd
    iput v3, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@1df
    .line 696
    if-eqz p1, :cond_6b

    #@1e1
    move-object/from16 v0, p0

    #@1e3
    move-object/from16 v1, p2

    #@1e5
    invoke-direct {v0, v1, v7, v8}, Landroid/text/SpannableStringBuilder;->sendSpanAdded(Ljava/lang/Object;II)V

    #@1e8
    goto/16 :goto_6b
.end method

.method private updatedIntervalBound(IIIIZZ)I
    .registers 9
    .parameter "offset"
    .parameter "start"
    .parameter "nbNewChars"
    .parameter "flag"
    .parameter "atEnd"
    .parameter "textIsRemoved"

    #@0
    .prologue
    .line 381
    if-lt p1, p2, :cond_2d

    #@2
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@4
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@6
    add-int/2addr v0, v1

    #@7
    if-ge p1, v0, :cond_2d

    #@9
    .line 382
    const/4 v0, 0x2

    #@a
    if-ne p4, v0, :cond_17

    #@c
    .line 387
    if-nez p6, :cond_10

    #@e
    if-le p1, p2, :cond_2d

    #@10
    .line 388
    :cond_10
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@12
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@14
    add-int p2, v0, v1

    #@16
    .line 408
    .end local p2
    :cond_16
    :goto_16
    return p2

    #@17
    .line 391
    .restart local p2
    :cond_17
    const/4 v0, 0x3

    #@18
    if-ne p4, v0, :cond_23

    #@1a
    .line 392
    if-eqz p5, :cond_2d

    #@1c
    .line 393
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1e
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@20
    add-int p2, v0, v1

    #@22
    goto :goto_16

    #@23
    .line 399
    :cond_23
    if-nez p6, :cond_16

    #@25
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@27
    sub-int/2addr v0, p3

    #@28
    if-lt p1, v0, :cond_16

    #@2a
    .line 403
    iget p2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@2c
    goto :goto_16

    #@2d
    :cond_2d
    move p2, p1

    #@2e
    .line 408
    goto :goto_16
.end method

.method public static valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .registers 2
    .parameter "source"

    #@0
    .prologue
    .line 99
    instance-of v0, p0, Landroid/text/SpannableStringBuilder;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 100
    check-cast p0, Landroid/text/SpannableStringBuilder;

    #@6
    .line 102
    .end local p0
    :goto_6
    return-object p0

    #@7
    .restart local p0
    :cond_7
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@9
    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    #@c
    move-object p0, v0

    #@d
    goto :goto_6
.end method


# virtual methods
.method public bridge synthetic append(C)Landroid/text/Editable;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Landroid/text/Editable;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public append(C)Landroid/text/SpannableStringBuilder;
    .registers 3
    .parameter "text"

    #@0
    .prologue
    .line 258
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .registers 8
    .parameter "text"

    #@0
    .prologue
    .line 246
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v1

    #@4
    .line 247
    .local v1, length:I
    const/4 v4, 0x0

    #@5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@8
    move-result v5

    #@9
    move-object v0, p0

    #@a
    move v2, v1

    #@b
    move-object v3, p1

    #@c
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@f
    move-result-object v0

    #@10
    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .registers 10
    .parameter "text"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 252
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v1

    #@4
    .local v1, length:I
    move-object v0, p0

    #@5
    move v2, v1

    #@6
    move-object v3, p1

    #@7
    move v4, p2

    #@8
    move v5, p3

    #@9
    .line 253
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public charAt(I)C
    .registers 6
    .parameter "where"

    #@0
    .prologue
    .line 110
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@3
    move-result v0

    #@4
    .line 111
    .local v0, len:I
    if-gez p1, :cond_25

    #@6
    .line 112
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "charAt: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, " < 0"

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@24
    throw v1

    #@25
    .line 113
    :cond_25
    if-lt p1, v0, :cond_4a

    #@27
    .line 114
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "charAt: "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, " >= length "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    #@49
    throw v1

    #@4a
    .line 117
    :cond_4a
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@4c
    if-lt p1, v1, :cond_56

    #@4e
    .line 118
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@50
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@52
    add-int/2addr v2, p1

    #@53
    aget-char v1, v1, v2

    #@55
    .line 120
    :goto_55
    return v1

    #@56
    :cond_56
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@58
    aget-char v1, v1, p1

    #@5a
    goto :goto_55
.end method

.method public clear()V
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 222
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@4
    move-result v2

    #@5
    const-string v3, ""

    #@7
    move-object v0, p0

    #@8
    move v4, v1

    #@9
    move v5, v1

    #@a
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@d
    .line 223
    return-void
.end method

.method public clearSpans()V
    .registers 7

    #@0
    .prologue
    .line 227
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    add-int/lit8 v0, v4, -0x1

    #@4
    .local v0, i:I
    :goto_4
    if-ltz v0, :cond_2d

    #@6
    .line 228
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@8
    aget-object v3, v4, v0

    #@a
    .line 229
    .local v3, what:Ljava/lang/Object;
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@c
    aget v2, v4, v0

    #@e
    .line 230
    .local v2, ostart:I
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@10
    aget v1, v4, v0

    #@12
    .line 232
    .local v1, oend:I
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@14
    if-le v2, v4, :cond_19

    #@16
    .line 233
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@18
    sub-int/2addr v2, v4

    #@19
    .line 234
    :cond_19
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1b
    if-le v1, v4, :cond_20

    #@1d
    .line 235
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1f
    sub-int/2addr v1, v4

    #@20
    .line 237
    :cond_20
    iput v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@22
    .line 238
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@24
    const/4 v5, 0x0

    #@25
    aput-object v5, v4, v0

    #@27
    .line 240
    invoke-direct {p0, v3, v2, v1}, Landroid/text/SpannableStringBuilder;->sendSpanRemoved(Ljava/lang/Object;II)V

    #@2a
    .line 227
    add-int/lit8 v0, v0, -0x1

    #@2c
    goto :goto_4

    #@2d
    .line 242
    .end local v1           #oend:I
    .end local v2           #ostart:I
    .end local v3           #what:Ljava/lang/Object;
    :cond_2d
    return-void
.end method

.method public bridge synthetic delete(II)Landroid/text/Editable;
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public delete(II)Landroid/text/SpannableStringBuilder;
    .registers 10
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 212
    const-string v3, ""

    #@3
    move-object v0, p0

    #@4
    move v1, p1

    #@5
    move v2, p2

    #@6
    move v5, v4

    #@7
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@a
    move-result-object v6

    #@b
    .line 214
    .local v6, ret:Landroid/text/SpannableStringBuilder;
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@d
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@10
    move-result v1

    #@11
    mul-int/lit8 v1, v1, 0x2

    #@13
    if-le v0, v1, :cond_1c

    #@15
    .line 215
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@18
    move-result v0

    #@19
    invoke-direct {p0, v0}, Landroid/text/SpannableStringBuilder;->resizeFor(I)V

    #@1c
    .line 217
    :cond_1c
    return-object v6
.end method

.method public drawText(Landroid/graphics/Canvas;IIFFLandroid/graphics/Paint;)V
    .registers 14
    .parameter "c"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "y"
    .parameter "p"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1120
    const-string v0, "drawText"

    #@3
    invoke-direct {p0, v0, p2, p3}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@6
    .line 1122
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@8
    if-gt p3, v0, :cond_17

    #@a
    .line 1123
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@c
    sub-int v3, p3, p2

    #@e
    move-object v0, p1

    #@f
    move v2, p2

    #@10
    move v4, p4

    #@11
    move v5, p5

    #@12
    move-object v6, p6

    #@13
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    #@16
    .line 1133
    :goto_16
    return-void

    #@17
    .line 1124
    :cond_17
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@19
    if-lt p2, v0, :cond_2b

    #@1b
    .line 1125
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1d
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1f
    add-int v2, p2, v0

    #@21
    sub-int v3, p3, p2

    #@23
    move-object v0, p1

    #@24
    move v4, p4

    #@25
    move v5, p5

    #@26
    move-object v6, p6

    #@27
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    #@2a
    goto :goto_16

    #@2b
    .line 1127
    :cond_2b
    sub-int v0, p3, p2

    #@2d
    invoke-static {v0}, Landroid/text/TextUtils;->obtain(I)[C

    #@30
    move-result-object v1

    #@31
    .line 1129
    .local v1, buf:[C
    invoke-virtual {p0, p2, p3, v1, v2}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@34
    .line 1130
    sub-int v3, p3, p2

    #@36
    move-object v0, p1

    #@37
    move v4, p4

    #@38
    move v5, p5

    #@39
    move-object v6, p6

    #@3a
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    #@3d
    .line 1131
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@40
    goto :goto_16
.end method

.method public drawTextRun(Landroid/graphics/Canvas;IIIIFFILandroid/graphics/Paint;)V
    .registers 20
    .parameter "c"
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "x"
    .parameter "y"
    .parameter "flags"
    .parameter "p"

    #@0
    .prologue
    .line 1142
    const-string v0, "drawTextRun"

    #@2
    invoke-direct {p0, v0, p2, p3}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@5
    .line 1144
    sub-int v5, p5, p4

    #@7
    .line 1145
    .local v5, contextLen:I
    sub-int v3, p3, p2

    #@9
    .line 1146
    .local v3, len:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@b
    if-gt p5, v0, :cond_1e

    #@d
    .line 1147
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@f
    move-object v0, p1

    #@10
    move v2, p2

    #@11
    move v4, p4

    #@12
    move/from16 v6, p6

    #@14
    move/from16 v7, p7

    #@16
    move/from16 v8, p8

    #@18
    move-object/from16 v9, p9

    #@1a
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFILandroid/graphics/Paint;)V

    #@1d
    .line 1157
    :goto_1d
    return-void

    #@1e
    .line 1148
    :cond_1e
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@20
    if-lt p4, v0, :cond_39

    #@22
    .line 1149
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@24
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@26
    add-int v2, p2, v0

    #@28
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2a
    add-int v4, p4, v0

    #@2c
    move-object v0, p1

    #@2d
    move/from16 v6, p6

    #@2f
    move/from16 v7, p7

    #@31
    move/from16 v8, p8

    #@33
    move-object/from16 v9, p9

    #@35
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFILandroid/graphics/Paint;)V

    #@38
    goto :goto_1d

    #@39
    .line 1152
    :cond_39
    invoke-static {v5}, Landroid/text/TextUtils;->obtain(I)[C

    #@3c
    move-result-object v1

    #@3d
    .line 1153
    .local v1, buf:[C
    const/4 v0, 0x0

    #@3e
    invoke-virtual {p0, p4, p5, v1, v0}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@41
    .line 1154
    sub-int v2, p2, p4

    #@43
    const/4 v4, 0x0

    #@44
    move-object v0, p1

    #@45
    move/from16 v6, p6

    #@47
    move/from16 v7, p7

    #@49
    move/from16 v8, p8

    #@4b
    move-object/from16 v9, p9

    #@4d
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFILandroid/graphics/Paint;)V

    #@50
    .line 1155
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@53
    goto :goto_1d
.end method

.method public getChars(II[CI)V
    .registers 9
    .parameter "start"
    .parameter "end"
    .parameter "dest"
    .parameter "destoff"

    #@0
    .prologue
    .line 921
    const-string v0, "getChars"

    #@2
    invoke-direct {p0, v0, p1, p2}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@5
    .line 923
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@7
    if-gt p2, v0, :cond_11

    #@9
    .line 924
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@b
    sub-int v1, p2, p1

    #@d
    invoke-static {v0, p1, p3, p4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@10
    .line 933
    :goto_10
    return-void

    #@11
    .line 925
    :cond_11
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@13
    if-lt p1, v0, :cond_20

    #@15
    .line 926
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@17
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@19
    add-int/2addr v1, p1

    #@1a
    sub-int v2, p2, p1

    #@1c
    invoke-static {v0, v1, p3, p4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@1f
    goto :goto_10

    #@20
    .line 928
    :cond_20
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@22
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@24
    sub-int/2addr v1, p1

    #@25
    invoke-static {v0, p1, p3, p4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@28
    .line 929
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@2a
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@2c
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2e
    add-int/2addr v1, v2

    #@2f
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@31
    sub-int/2addr v2, p1

    #@32
    add-int/2addr v2, p4

    #@33
    iget v3, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@35
    sub-int v3, p2, v3

    #@37
    invoke-static {v0, v1, p3, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3a
    goto :goto_10
.end method

.method public getFilters()[Landroid/text/InputFilter;
    .registers 2

    #@0
    .prologue
    .line 1325
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mFilters:[Landroid/text/InputFilter;

    #@2
    return-object v0
.end method

.method public getSpanEnd(Ljava/lang/Object;)I
    .registers 7
    .parameter "what"

    #@0
    .prologue
    .line 738
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    .line 739
    .local v0, count:I
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@4
    .line 741
    .local v2, spans:[Ljava/lang/Object;
    add-int/lit8 v1, v0, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_1b

    #@8
    .line 742
    aget-object v4, v2, v1

    #@a
    if-ne v4, p1, :cond_18

    #@c
    .line 743
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@e
    aget v3, v4, v1

    #@10
    .line 745
    .local v3, where:I
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@12
    if-le v3, v4, :cond_17

    #@14
    .line 746
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@16
    sub-int/2addr v3, v4

    #@17
    .line 752
    .end local v3           #where:I
    :cond_17
    :goto_17
    return v3

    #@18
    .line 741
    :cond_18
    add-int/lit8 v1, v1, -0x1

    #@1a
    goto :goto_6

    #@1b
    .line 752
    :cond_1b
    const/4 v3, -0x1

    #@1c
    goto :goto_17
.end method

.method public getSpanFlags(Ljava/lang/Object;)I
    .registers 6
    .parameter "what"

    #@0
    .prologue
    .line 760
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    .line 761
    .local v0, count:I
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@4
    .line 763
    .local v2, spans:[Ljava/lang/Object;
    add-int/lit8 v1, v0, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_14

    #@8
    .line 764
    aget-object v3, v2, v1

    #@a
    if-ne v3, p1, :cond_11

    #@c
    .line 765
    iget-object v3, p0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@e
    aget v3, v3, v1

    #@10
    .line 769
    :goto_10
    return v3

    #@11
    .line 763
    :cond_11
    add-int/lit8 v1, v1, -0x1

    #@13
    goto :goto_6

    #@14
    .line 769
    :cond_14
    const/4 v3, 0x0

    #@15
    goto :goto_10
.end method

.method public getSpanStart(Ljava/lang/Object;)I
    .registers 7
    .parameter "what"

    #@0
    .prologue
    .line 716
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    .line 717
    .local v0, count:I
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@4
    .line 719
    .local v2, spans:[Ljava/lang/Object;
    add-int/lit8 v1, v0, -0x1

    #@6
    .local v1, i:I
    :goto_6
    if-ltz v1, :cond_1b

    #@8
    .line 720
    aget-object v4, v2, v1

    #@a
    if-ne v4, p1, :cond_18

    #@c
    .line 721
    iget-object v4, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@e
    aget v3, v4, v1

    #@10
    .line 723
    .local v3, where:I
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@12
    if-le v3, v4, :cond_17

    #@14
    .line 724
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@16
    sub-int/2addr v3, v4

    #@17
    .line 730
    .end local v3           #where:I
    :cond_17
    :goto_17
    return v3

    #@18
    .line 719
    :cond_18
    add-int/lit8 v1, v1, -0x1

    #@1a
    goto :goto_6

    #@1b
    .line 730
    :cond_1b
    const/4 v3, -0x1

    #@1c
    goto :goto_17
.end method

.method public getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 26
    .parameter "queryStart"
    .parameter "queryEnd"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    #@0
    .prologue
    .line 779
    .local p3, kind:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    if-nez p3, :cond_7

    #@2
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@5
    move-result-object v13

    #@6
    .line 870
    :cond_6
    :goto_6
    return-object v13

    #@7
    .line 781
    :cond_7
    move-object/from16 v0, p0

    #@9
    iget v15, v0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@b
    .line 782
    .local v15, spanCount:I
    move-object/from16 v0, p0

    #@d
    iget-object v0, v0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@f
    move-object/from16 v18, v0

    #@11
    .line 783
    .local v18, spans:[Ljava/lang/Object;
    move-object/from16 v0, p0

    #@13
    iget-object v0, v0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@15
    move-object/from16 v19, v0

    #@17
    .line 784
    .local v19, starts:[I
    move-object/from16 v0, p0

    #@19
    iget-object v4, v0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@1b
    .line 785
    .local v4, ends:[I
    move-object/from16 v0, p0

    #@1d
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mSpanFlags:[I

    #@1f
    .line 786
    .local v5, flags:[I
    move-object/from16 v0, p0

    #@21
    iget v7, v0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@23
    .line 787
    .local v7, gapstart:I
    move-object/from16 v0, p0

    #@25
    iget v6, v0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@27
    .line 789
    .local v6, gaplen:I
    const/4 v2, 0x0

    #@28
    .line 790
    .local v2, count:I
    const/4 v13, 0x0

    #@29
    .line 791
    .local v13, ret:[Ljava/lang/Object;,"[TT;"
    const/4 v14, 0x0

    #@2a
    .line 793
    .local v14, ret1:Ljava/lang/Object;,"TT;"
    const/4 v8, 0x0

    #@2b
    .local v8, i:I
    move v3, v2

    #@2c
    .end local v2           #count:I
    .end local v14           #ret1:Ljava/lang/Object;,"TT;"
    .local v3, count:I
    :goto_2c
    if-ge v8, v15, :cond_d7

    #@2e
    .line 794
    aget v17, v19, v8

    #@30
    .line 795
    .local v17, spanStart:I
    move/from16 v0, v17

    #@32
    if-le v0, v7, :cond_36

    #@34
    .line 796
    sub-int v17, v17, v6

    #@36
    .line 798
    :cond_36
    move/from16 v0, v17

    #@38
    move/from16 v1, p2

    #@3a
    if-le v0, v1, :cond_41

    #@3c
    move v2, v3

    #@3d
    .line 793
    .end local v3           #count:I
    .restart local v2       #count:I
    :goto_3d
    add-int/lit8 v8, v8, 0x1

    #@3f
    move v3, v2

    #@40
    .end local v2           #count:I
    .restart local v3       #count:I
    goto :goto_2c

    #@41
    .line 802
    :cond_41
    aget v16, v4, v8

    #@43
    .line 803
    .local v16, spanEnd:I
    move/from16 v0, v16

    #@45
    if-le v0, v7, :cond_49

    #@47
    .line 804
    sub-int v16, v16, v6

    #@49
    .line 806
    :cond_49
    move/from16 v0, v16

    #@4b
    move/from16 v1, p1

    #@4d
    if-ge v0, v1, :cond_51

    #@4f
    move v2, v3

    #@50
    .line 807
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_3d

    #@51
    .line 810
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_51
    move/from16 v0, v17

    #@53
    move/from16 v1, v16

    #@55
    if-eq v0, v1, :cond_6d

    #@57
    move/from16 v0, p1

    #@59
    move/from16 v1, p2

    #@5b
    if-eq v0, v1, :cond_6d

    #@5d
    .line 811
    move/from16 v0, v17

    #@5f
    move/from16 v1, p2

    #@61
    if-ne v0, v1, :cond_65

    #@63
    move v2, v3

    #@64
    .line 812
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_3d

    #@65
    .line 813
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_65
    move/from16 v0, v16

    #@67
    move/from16 v1, p1

    #@69
    if-ne v0, v1, :cond_6d

    #@6b
    move v2, v3

    #@6c
    .line 814
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_3d

    #@6d
    .line 818
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_6d
    aget-object v20, v18, v8

    #@6f
    move-object/from16 v0, p3

    #@71
    move-object/from16 v1, v20

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@76
    move-result v20

    #@77
    if-nez v20, :cond_7b

    #@79
    move v2, v3

    #@7a
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_3d

    #@7b
    .line 820
    .end local v2           #count:I
    .restart local v3       #count:I
    :cond_7b
    if-nez v3, :cond_82

    #@7d
    .line 822
    aget-object v14, v18, v8

    #@7f
    .line 823
    .restart local v14       #ret1:Ljava/lang/Object;,"TT;"
    add-int/lit8 v2, v3, 0x1

    #@81
    .end local v3           #count:I
    .restart local v2       #count:I
    goto :goto_3d

    #@82
    .line 825
    .end local v2           #count:I
    .end local v14           #ret1:Ljava/lang/Object;,"TT;"
    .restart local v3       #count:I
    :cond_82
    const/16 v20, 0x1

    #@84
    move/from16 v0, v20

    #@86
    if-ne v3, v0, :cond_9e

    #@88
    .line 827
    sub-int v20, v15, v8

    #@8a
    add-int/lit8 v20, v20, 0x1

    #@8c
    move-object/from16 v0, p3

    #@8e
    move/from16 v1, v20

    #@90
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@93
    move-result-object v20

    #@94
    check-cast v20, [Ljava/lang/Object;

    #@96
    move-object/from16 v13, v20

    #@98
    check-cast v13, [Ljava/lang/Object;

    #@9a
    .line 828
    const/16 v20, 0x0

    #@9c
    aput-object v14, v13, v20

    #@9e
    .line 831
    :cond_9e
    aget v20, v5, v8

    #@a0
    const/high16 v21, 0xff

    #@a2
    and-int v12, v20, v21

    #@a4
    .line 832
    .local v12, prio:I
    if-eqz v12, :cond_cf

    #@a6
    .line 835
    const/4 v9, 0x0

    #@a7
    .local v9, j:I
    :goto_a7
    if-ge v9, v3, :cond_b9

    #@a9
    .line 836
    aget-object v20, v13, v9

    #@ab
    move-object/from16 v0, p0

    #@ad
    move-object/from16 v1, v20

    #@af
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    #@b2
    move-result v20

    #@b3
    const/high16 v21, 0xff

    #@b5
    and-int v11, v20, v21

    #@b7
    .line 838
    .local v11, p:I
    if-le v12, v11, :cond_cc

    #@b9
    .line 843
    .end local v11           #p:I
    :cond_b9
    add-int/lit8 v20, v9, 0x1

    #@bb
    sub-int v21, v3, v9

    #@bd
    move/from16 v0, v20

    #@bf
    move/from16 v1, v21

    #@c1
    invoke-static {v13, v9, v13, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c4
    .line 845
    aget-object v20, v18, v8

    #@c6
    aput-object v20, v13, v9

    #@c8
    .line 846
    add-int/lit8 v2, v3, 0x1

    #@ca
    .line 847
    .end local v3           #count:I
    .restart local v2       #count:I
    goto/16 :goto_3d

    #@cc
    .line 835
    .end local v2           #count:I
    .restart local v3       #count:I
    .restart local v11       #p:I
    :cond_cc
    add-int/lit8 v9, v9, 0x1

    #@ce
    goto :goto_a7

    #@cf
    .line 849
    .end local v9           #j:I
    .end local v11           #p:I
    :cond_cf
    add-int/lit8 v2, v3, 0x1

    #@d1
    .end local v3           #count:I
    .restart local v2       #count:I
    aget-object v20, v18, v8

    #@d3
    aput-object v20, v13, v3

    #@d5
    goto/16 :goto_3d

    #@d7
    .line 854
    .end local v2           #count:I
    .end local v12           #prio:I
    .end local v16           #spanEnd:I
    .end local v17           #spanStart:I
    .restart local v3       #count:I
    :cond_d7
    if-nez v3, :cond_df

    #@d9
    .line 855
    invoke-static/range {p3 .. p3}, Lcom/android/internal/util/ArrayUtils;->emptyArray(Ljava/lang/Class;)[Ljava/lang/Object;

    #@dc
    move-result-object v13

    #@dd
    goto/16 :goto_6

    #@df
    .line 857
    :cond_df
    const/16 v20, 0x1

    #@e1
    move/from16 v0, v20

    #@e3
    if-ne v3, v0, :cond_fb

    #@e5
    .line 859
    const/16 v20, 0x1

    #@e7
    move-object/from16 v0, p3

    #@e9
    move/from16 v1, v20

    #@eb
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@ee
    move-result-object v20

    #@ef
    check-cast v20, [Ljava/lang/Object;

    #@f1
    move-object/from16 v13, v20

    #@f3
    check-cast v13, [Ljava/lang/Object;

    #@f5
    .line 860
    const/16 v20, 0x0

    #@f7
    aput-object v14, v13, v20

    #@f9
    goto/16 :goto_6

    #@fb
    .line 863
    :cond_fb
    array-length v0, v13

    #@fc
    move/from16 v20, v0

    #@fe
    move/from16 v0, v20

    #@100
    if-eq v3, v0, :cond_6

    #@102
    .line 868
    move-object/from16 v0, p3

    #@104
    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    #@107
    move-result-object v20

    #@108
    check-cast v20, [Ljava/lang/Object;

    #@10a
    move-object/from16 v10, v20

    #@10c
    check-cast v10, [Ljava/lang/Object;

    #@10e
    .line 869
    .local v10, nret:[Ljava/lang/Object;,"[TT;"
    const/16 v20, 0x0

    #@110
    const/16 v21, 0x0

    #@112
    move/from16 v0, v20

    #@114
    move/from16 v1, v21

    #@116
    invoke-static {v13, v0, v10, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@119
    move-object v13, v10

    #@11a
    .line 870
    goto/16 :goto_6
.end method

.method public getTextRunAdvances(IIIII[FILandroid/graphics/Paint;)F
    .registers 19
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesPos"
    .parameter "p"

    #@0
    .prologue
    .line 1216
    sub-int v5, p4, p3

    #@2
    .line 1217
    .local v5, contextLen:I
    sub-int v3, p2, p1

    #@4
    .line 1219
    .local v3, len:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@6
    if-gt p2, v0, :cond_18

    #@8
    .line 1220
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@a
    move-object/from16 v0, p8

    #@c
    move v2, p1

    #@d
    move v4, p3

    #@e
    move v6, p5

    #@f
    move-object/from16 v7, p6

    #@11
    move/from16 v8, p7

    #@13
    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FI)F

    #@16
    move-result v9

    #@17
    .line 1233
    .local v9, ret:F
    :goto_17
    return v9

    #@18
    .line 1222
    .end local v9           #ret:F
    :cond_18
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1a
    if-lt p1, v0, :cond_32

    #@1c
    .line 1223
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1e
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@20
    add-int v2, p1, v0

    #@22
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@24
    add-int v4, p3, v0

    #@26
    move-object/from16 v0, p8

    #@28
    move v6, p5

    #@29
    move-object/from16 v7, p6

    #@2b
    move/from16 v8, p7

    #@2d
    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FI)F

    #@30
    move-result v9

    #@31
    .restart local v9       #ret:F
    goto :goto_17

    #@32
    .line 1226
    .end local v9           #ret:F
    :cond_32
    invoke-static {v5}, Landroid/text/TextUtils;->obtain(I)[C

    #@35
    move-result-object v1

    #@36
    .line 1227
    .local v1, buf:[C
    const/4 v0, 0x0

    #@37
    invoke-virtual {p0, p3, p4, v1, v0}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@3a
    .line 1228
    sub-int v2, p1, p3

    #@3c
    const/4 v4, 0x0

    #@3d
    move-object/from16 v0, p8

    #@3f
    move v6, p5

    #@40
    move-object/from16 v7, p6

    #@42
    move/from16 v8, p7

    #@44
    invoke-virtual/range {v0 .. v8}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FI)F

    #@47
    move-result v9

    #@48
    .line 1230
    .restart local v9       #ret:F
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@4b
    goto :goto_17
.end method

.method public getTextRunAdvances(IIIII[FILandroid/graphics/Paint;I)F
    .registers 21
    .parameter "start"
    .parameter "end"
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "advances"
    .parameter "advancesPos"
    .parameter "p"
    .parameter "reserved"

    #@0
    .prologue
    .line 1245
    sub-int v5, p4, p3

    #@2
    .line 1246
    .local v5, contextLen:I
    sub-int v3, p2, p1

    #@4
    .line 1248
    .local v3, len:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@6
    if-gt p2, v0, :cond_1b

    #@8
    .line 1249
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@a
    move-object/from16 v0, p8

    #@c
    move v2, p1

    #@d
    move v4, p3

    #@e
    move/from16 v6, p5

    #@10
    move-object/from16 v7, p6

    #@12
    move/from16 v8, p7

    #@14
    move/from16 v9, p9

    #@16
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@19
    move-result v10

    #@1a
    .line 1262
    .local v10, ret:F
    :goto_1a
    return v10

    #@1b
    .line 1251
    .end local v10           #ret:F
    :cond_1b
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@1d
    if-lt p1, v0, :cond_38

    #@1f
    .line 1252
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@21
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@23
    add-int v2, p1, v0

    #@25
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@27
    add-int v4, p3, v0

    #@29
    move-object/from16 v0, p8

    #@2b
    move/from16 v6, p5

    #@2d
    move-object/from16 v7, p6

    #@2f
    move/from16 v8, p7

    #@31
    move/from16 v9, p9

    #@33
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@36
    move-result v10

    #@37
    .restart local v10       #ret:F
    goto :goto_1a

    #@38
    .line 1255
    .end local v10           #ret:F
    :cond_38
    invoke-static {v5}, Landroid/text/TextUtils;->obtain(I)[C

    #@3b
    move-result-object v1

    #@3c
    .line 1256
    .local v1, buf:[C
    const/4 v0, 0x0

    #@3d
    invoke-virtual {p0, p3, p4, v1, v0}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@40
    .line 1257
    sub-int v2, p1, p3

    #@42
    const/4 v4, 0x0

    #@43
    move-object/from16 v0, p8

    #@45
    move/from16 v6, p5

    #@47
    move-object/from16 v7, p6

    #@49
    move/from16 v8, p7

    #@4b
    move/from16 v9, p9

    #@4d
    invoke-virtual/range {v0 .. v9}, Landroid/graphics/Paint;->getTextRunAdvances([CIIIII[FII)F

    #@50
    move-result v10

    #@51
    .line 1259
    .restart local v10       #ret:F
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@54
    goto :goto_1a
.end method

.method public getTextRunCursor(IIIIILandroid/graphics/Paint;)I
    .registers 15
    .parameter "contextStart"
    .parameter "contextEnd"
    .parameter "flags"
    .parameter "offset"
    .parameter "cursorOpt"
    .parameter "p"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1296
    sub-int v3, p2, p1

    #@3
    .line 1297
    .local v3, contextLen:I
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@5
    if-gt p2, v0, :cond_13

    #@7
    .line 1298
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@9
    move-object v0, p6

    #@a
    move v2, p1

    #@b
    move v4, p3

    #@c
    move v5, p4

    #@d
    move v6, p5

    #@e
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor([CIIIII)I

    #@11
    move-result v7

    #@12
    .line 1311
    .local v7, ret:I
    :goto_12
    return v7

    #@13
    .line 1300
    .end local v7           #ret:I
    :cond_13
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@15
    if-lt p1, v0, :cond_2d

    #@17
    .line 1301
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@19
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1b
    add-int v2, p1, v0

    #@1d
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1f
    add-int v5, p4, v0

    #@21
    move-object v0, p6

    #@22
    move v4, p3

    #@23
    move v6, p5

    #@24
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor([CIIIII)I

    #@27
    move-result v0

    #@28
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@2a
    sub-int v7, v0, v2

    #@2c
    .restart local v7       #ret:I
    goto :goto_12

    #@2d
    .line 1304
    .end local v7           #ret:I
    :cond_2d
    invoke-static {v3}, Landroid/text/TextUtils;->obtain(I)[C

    #@30
    move-result-object v1

    #@31
    .line 1305
    .local v1, buf:[C
    invoke-virtual {p0, p1, p2, v1, v2}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@34
    .line 1306
    sub-int v5, p4, p1

    #@36
    move-object v0, p6

    #@37
    move v4, p3

    #@38
    move v6, p5

    #@39
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextRunCursor([CIIIII)I

    #@3c
    move-result v0

    #@3d
    add-int v7, v0, p1

    #@3f
    .line 1308
    .restart local v7       #ret:I
    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    #@42
    goto :goto_12
.end method

.method public getTextWidths(II[FLandroid/graphics/Paint;)I
    .registers 10
    .parameter "start"
    .parameter "end"
    .parameter "widths"
    .parameter "p"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1188
    const-string v2, "getTextWidths"

    #@3
    invoke-direct {p0, v2, p1, p2}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@6
    .line 1192
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@8
    if-gt p2, v2, :cond_13

    #@a
    .line 1193
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@c
    sub-int v3, p2, p1

    #@e
    invoke-virtual {p4, v2, p1, v3, p3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    #@11
    move-result v1

    #@12
    .line 1204
    .local v1, ret:I
    :goto_12
    return v1

    #@13
    .line 1194
    .end local v1           #ret:I
    :cond_13
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@15
    if-lt p1, v2, :cond_23

    #@17
    .line 1195
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@19
    iget v3, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1b
    add-int/2addr v3, p1

    #@1c
    sub-int v4, p2, p1

    #@1e
    invoke-virtual {p4, v2, v3, v4, p3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    #@21
    move-result v1

    #@22
    .restart local v1       #ret:I
    goto :goto_12

    #@23
    .line 1197
    .end local v1           #ret:I
    :cond_23
    sub-int v2, p2, p1

    #@25
    invoke-static {v2}, Landroid/text/TextUtils;->obtain(I)[C

    #@28
    move-result-object v0

    #@29
    .line 1199
    .local v0, buf:[C
    invoke-virtual {p0, p1, p2, v0, v3}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@2c
    .line 1200
    sub-int v2, p2, p1

    #@2e
    invoke-virtual {p4, v0, v3, v2, p3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    #@31
    move-result v1

    #@32
    .line 1201
    .restart local v1       #ret:I
    invoke-static {v0}, Landroid/text/TextUtils;->recycle([C)V

    #@35
    goto :goto_12
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;)Landroid/text/Editable;
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .registers 9
    .parameter "where"
    .parameter "tb"

    #@0
    .prologue
    .line 207
    const/4 v4, 0x0

    #@1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v5

    #@5
    move-object v0, p0

    #@6
    move v1, p1

    #@7
    move v2, p1

    #@8
    move-object v3, p2

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .registers 11
    .parameter "where"
    .parameter "tb"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 202
    move-object v0, p0

    #@1
    move v1, p1

    #@2
    move v2, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method public length()I
    .registers 3

    #@0
    .prologue
    .line 127
    iget-object v0, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@2
    array-length v0, v0

    #@3
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@5
    sub-int/2addr v0, v1

    #@6
    return v0
.end method

.method public measureText(IILandroid/graphics/Paint;)F
    .registers 9
    .parameter "start"
    .parameter "end"
    .parameter "p"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1164
    const-string/jumbo v2, "measureText"

    #@4
    invoke-direct {p0, v2, p1, p2}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@7
    .line 1168
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@9
    if-gt p2, v2, :cond_14

    #@b
    .line 1169
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@d
    sub-int v3, p2, p1

    #@f
    invoke-virtual {p3, v2, p1, v3}, Landroid/graphics/Paint;->measureText([CII)F

    #@12
    move-result v1

    #@13
    .line 1180
    .local v1, ret:F
    :goto_13
    return v1

    #@14
    .line 1170
    .end local v1           #ret:F
    :cond_14
    iget v2, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@16
    if-lt p1, v2, :cond_24

    #@18
    .line 1171
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mText:[C

    #@1a
    iget v3, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@1c
    add-int/2addr v3, p1

    #@1d
    sub-int v4, p2, p1

    #@1f
    invoke-virtual {p3, v2, v3, v4}, Landroid/graphics/Paint;->measureText([CII)F

    #@22
    move-result v1

    #@23
    .restart local v1       #ret:F
    goto :goto_13

    #@24
    .line 1173
    .end local v1           #ret:F
    :cond_24
    sub-int v2, p2, p1

    #@26
    invoke-static {v2}, Landroid/text/TextUtils;->obtain(I)[C

    #@29
    move-result-object v0

    #@2a
    .line 1175
    .local v0, buf:[C
    invoke-virtual {p0, p1, p2, v0, v3}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@2d
    .line 1176
    sub-int v2, p2, p1

    #@2f
    invoke-virtual {p3, v0, v3, v2}, Landroid/graphics/Paint;->measureText([CII)F

    #@32
    move-result v1

    #@33
    .line 1177
    .restart local v1       #ret:F
    invoke-static {v0}, Landroid/text/TextUtils;->recycle([C)V

    #@36
    goto :goto_13
.end method

.method public nextSpanTransition(IILjava/lang/Class;)I
    .registers 14
    .parameter "start"
    .parameter "limit"
    .parameter "kind"

    #@0
    .prologue
    .line 879
    iget v0, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    .line 880
    .local v0, count:I
    iget-object v6, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@4
    .line 881
    .local v6, spans:[Ljava/lang/Object;
    iget-object v8, p0, Landroid/text/SpannableStringBuilder;->mSpanStarts:[I

    #@6
    .line 882
    .local v8, starts:[I
    iget-object v2, p0, Landroid/text/SpannableStringBuilder;->mSpanEnds:[I

    #@8
    .line 883
    .local v2, ends:[I
    iget v4, p0, Landroid/text/SpannableStringBuilder;->mGapStart:I

    #@a
    .line 884
    .local v4, gapstart:I
    iget v3, p0, Landroid/text/SpannableStringBuilder;->mGapLength:I

    #@c
    .line 886
    .local v3, gaplen:I
    if-nez p3, :cond_10

    #@e
    .line 887
    const-class p3, Ljava/lang/Object;

    #@10
    .line 890
    :cond_10
    const/4 v5, 0x0

    #@11
    .local v5, i:I
    :goto_11
    if-ge v5, v0, :cond_3a

    #@13
    .line 891
    aget v7, v8, v5

    #@15
    .line 892
    .local v7, st:I
    aget v1, v2, v5

    #@17
    .line 894
    .local v1, en:I
    if-le v7, v4, :cond_1a

    #@19
    .line 895
    sub-int/2addr v7, v3

    #@1a
    .line 896
    :cond_1a
    if-le v1, v4, :cond_1d

    #@1c
    .line 897
    sub-int/2addr v1, v3

    #@1d
    .line 899
    :cond_1d
    if-le v7, p1, :cond_2a

    #@1f
    if-ge v7, p2, :cond_2a

    #@21
    aget-object v9, v6, v5

    #@23
    invoke-virtual {p3, v9}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@26
    move-result v9

    #@27
    if-eqz v9, :cond_2a

    #@29
    .line 900
    move p2, v7

    #@2a
    .line 901
    :cond_2a
    if-le v1, p1, :cond_37

    #@2c
    if-ge v1, p2, :cond_37

    #@2e
    aget-object v9, v6, v5

    #@30
    invoke-virtual {p3, v9}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    #@33
    move-result v9

    #@34
    if-eqz v9, :cond_37

    #@36
    .line 902
    move p2, v1

    #@37
    .line 890
    :cond_37
    add-int/lit8 v5, v5, 0x1

    #@39
    goto :goto_11

    #@3a
    .line 905
    .end local v1           #en:I
    .end local v7           #st:I
    :cond_3a
    return p2
.end method

.method public removeSpan(Ljava/lang/Object;)V
    .registers 4
    .parameter "what"

    #@0
    .prologue
    .line 703
    iget v1, p0, Landroid/text/SpannableStringBuilder;->mSpanCount:I

    #@2
    add-int/lit8 v0, v1, -0x1

    #@4
    .local v0, i:I
    :goto_4
    if-ltz v0, :cond_f

    #@6
    .line 704
    iget-object v1, p0, Landroid/text/SpannableStringBuilder;->mSpans:[Ljava/lang/Object;

    #@8
    aget-object v1, v1, v0

    #@a
    if-ne v1, p1, :cond_10

    #@c
    .line 705
    invoke-direct {p0, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(I)V

    #@f
    .line 709
    :cond_f
    return-void

    #@10
    .line 703
    :cond_10
    add-int/lit8 v0, v0, -0x1

    #@12
    goto :goto_4
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;)Landroid/text/Editable;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0, p1, p2, p3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 30
    invoke-virtual/range {p0 .. p5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .registers 10
    .parameter "start"
    .parameter "end"
    .parameter "tb"

    #@0
    .prologue
    .line 435
    const/4 v4, 0x0

    #@1
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v5

    #@5
    move-object v0, p0

    #@6
    move v1, p1

    #@7
    move v2, p2

    #@8
    move-object v3, p3

    #@9
    invoke-virtual/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .registers 29
    .parameter "start"
    .parameter "end"
    .parameter "tb"
    .parameter "tbstart"
    .parameter "tbend"

    #@0
    .prologue
    .line 441
    const-string/jumbo v5, "replace"

    #@3
    move-object/from16 v0, p0

    #@5
    move/from16 v1, p1

    #@7
    move/from16 v2, p2

    #@9
    invoke-direct {v0, v5, v1, v2}, Landroid/text/SpannableStringBuilder;->checkRange(Ljava/lang/String;II)V

    #@c
    .line 443
    move-object/from16 v0, p0

    #@e
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mFilters:[Landroid/text/InputFilter;

    #@10
    array-length v0, v5

    #@11
    move/from16 v16, v0

    #@13
    .line 444
    .local v16, filtercount:I
    const/16 v17, 0x0

    #@15
    .local v17, i:I
    :goto_15
    move/from16 v0, v17

    #@17
    move/from16 v1, v16

    #@19
    if-ge v0, v1, :cond_3e

    #@1b
    .line 445
    move-object/from16 v0, p0

    #@1d
    iget-object v5, v0, Landroid/text/SpannableStringBuilder;->mFilters:[Landroid/text/InputFilter;

    #@1f
    aget-object v5, v5, v17

    #@21
    move-object/from16 v6, p3

    #@23
    move/from16 v7, p4

    #@25
    move/from16 v8, p5

    #@27
    move-object/from16 v9, p0

    #@29
    move/from16 v10, p1

    #@2b
    move/from16 v11, p2

    #@2d
    invoke-interface/range {v5 .. v11}, Landroid/text/InputFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    #@30
    move-result-object v21

    #@31
    .line 447
    .local v21, repl:Ljava/lang/CharSequence;
    if-eqz v21, :cond_3b

    #@33
    .line 448
    move-object/from16 p3, v21

    #@35
    .line 449
    const/16 p4, 0x0

    #@37
    .line 450
    invoke-interface/range {v21 .. v21}, Ljava/lang/CharSequence;->length()I

    #@3a
    move-result p5

    #@3b
    .line 444
    :cond_3b
    add-int/lit8 v17, v17, 0x1

    #@3d
    goto :goto_15

    #@3e
    .line 454
    .end local v21           #repl:Ljava/lang/CharSequence;
    :cond_3e
    sub-int v20, p2, p1

    #@40
    .line 455
    .local v20, origLen:I
    sub-int v18, p5, p4

    #@42
    .line 457
    .local v18, newLen:I
    if-nez v20, :cond_4d

    #@44
    if-nez v18, :cond_4d

    #@46
    invoke-static/range {p3 .. p4}, Landroid/text/SpannableStringBuilder;->hasNonExclusiveExclusiveSpanAt(Ljava/lang/CharSequence;I)Z

    #@49
    move-result v5

    #@4a
    if-nez v5, :cond_4d

    #@4c
    .line 502
    :goto_4c
    return-object p0

    #@4d
    .line 463
    :cond_4d
    add-int v5, p1, v20

    #@4f
    const-class v6, Landroid/text/TextWatcher;

    #@51
    move-object/from16 v0, p0

    #@53
    move/from16 v1, p1

    #@55
    invoke-virtual {v0, v1, v5, v6}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@58
    move-result-object v22

    #@59
    check-cast v22, [Landroid/text/TextWatcher;

    #@5b
    .line 464
    .local v22, textWatchers:[Landroid/text/TextWatcher;
    move-object/from16 v0, p0

    #@5d
    move-object/from16 v1, v22

    #@5f
    move/from16 v2, p1

    #@61
    move/from16 v3, v20

    #@63
    move/from16 v4, v18

    #@65
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->sendBeforeTextChanged([Landroid/text/TextWatcher;III)V

    #@68
    .line 469
    if-eqz v20, :cond_d5

    #@6a
    if-eqz v18, :cond_d5

    #@6c
    const/4 v15, 0x1

    #@6d
    .line 470
    .local v15, adjustSelection:Z
    :goto_6d
    const/4 v8, 0x0

    #@6e
    .line 471
    .local v8, selectionStart:I
    const/4 v12, 0x0

    #@6f
    .line 472
    .local v12, selectionEnd:I
    if-eqz v15, :cond_79

    #@71
    .line 473
    invoke-static/range {p0 .. p0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    #@74
    move-result v8

    #@75
    .line 474
    invoke-static/range {p0 .. p0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    #@78
    move-result v12

    #@79
    .line 477
    :cond_79
    invoke-direct/range {p0 .. p5}, Landroid/text/SpannableStringBuilder;->change(IILjava/lang/CharSequence;II)V

    #@7c
    .line 479
    if-eqz v15, :cond_b4

    #@7e
    .line 480
    move/from16 v0, p1

    #@80
    if-le v8, v0, :cond_99

    #@82
    move/from16 v0, p2

    #@84
    if-ge v8, v0, :cond_99

    #@86
    .line 481
    sub-int v5, v8, p1

    #@88
    mul-int v5, v5, v18

    #@8a
    div-int v19, v5, v20

    #@8c
    .line 482
    .local v19, offset:I
    add-int v8, p1, v19

    #@8e
    .line 484
    const/4 v6, 0x0

    #@8f
    sget-object v7, Landroid/text/Selection;->SELECTION_START:Ljava/lang/Object;

    #@91
    const/16 v10, 0x22

    #@93
    move-object/from16 v5, p0

    #@95
    move v9, v8

    #@96
    invoke-direct/range {v5 .. v10}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@99
    .line 487
    .end local v19           #offset:I
    :cond_99
    move/from16 v0, p1

    #@9b
    if-le v12, v0, :cond_b4

    #@9d
    move/from16 v0, p2

    #@9f
    if-ge v12, v0, :cond_b4

    #@a1
    .line 488
    sub-int v5, v12, p1

    #@a3
    mul-int v5, v5, v18

    #@a5
    div-int v19, v5, v20

    #@a7
    .line 489
    .restart local v19       #offset:I
    add-int v12, p1, v19

    #@a9
    .line 491
    const/4 v10, 0x0

    #@aa
    sget-object v11, Landroid/text/Selection;->SELECTION_END:Ljava/lang/Object;

    #@ac
    const/16 v14, 0x22

    #@ae
    move-object/from16 v9, p0

    #@b0
    move v13, v12

    #@b1
    invoke-direct/range {v9 .. v14}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@b4
    .line 496
    .end local v19           #offset:I
    :cond_b4
    move-object/from16 v0, p0

    #@b6
    move-object/from16 v1, v22

    #@b8
    move/from16 v2, p1

    #@ba
    move/from16 v3, v20

    #@bc
    move/from16 v4, v18

    #@be
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->sendTextChanged([Landroid/text/TextWatcher;III)V

    #@c1
    .line 497
    move-object/from16 v0, p0

    #@c3
    move-object/from16 v1, v22

    #@c5
    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;->sendAfterTextChanged([Landroid/text/TextWatcher;)V

    #@c8
    .line 500
    sub-int v5, v18, v20

    #@ca
    move-object/from16 v0, p0

    #@cc
    move/from16 v1, p1

    #@ce
    move/from16 v2, p2

    #@d0
    invoke-direct {v0, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->sendToSpanWatchers(III)V

    #@d3
    goto/16 :goto_4c

    #@d5
    .line 469
    .end local v8           #selectionStart:I
    .end local v12           #selectionEnd:I
    .end local v15           #adjustSelection:Z
    :cond_d5
    const/4 v15, 0x0

    #@d6
    goto :goto_6d
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .registers 3
    .parameter "filters"

    #@0
    .prologue
    .line 1316
    if-nez p1, :cond_8

    #@2
    .line 1317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    #@4
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    #@7
    throw v0

    #@8
    .line 1320
    :cond_8
    iput-object p1, p0, Landroid/text/SpannableStringBuilder;->mFilters:[Landroid/text/InputFilter;

    #@a
    .line 1321
    return-void
.end method

.method public setSpan(Ljava/lang/Object;III)V
    .registers 11
    .parameter "what"
    .parameter "start"
    .parameter "end"
    .parameter "flags"

    #@0
    .prologue
    .line 596
    const/4 v1, 0x1

    #@1
    move-object v0, p0

    #@2
    move-object v2, p1

    #@3
    move v3, p2

    #@4
    move v4, p3

    #@5
    move v5, p4

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->setSpan(ZLjava/lang/Object;III)V

    #@9
    .line 597
    return-void
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 4
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 913
    new-instance v0, Landroid/text/SpannableStringBuilder;

    #@2
    invoke-direct {v0, p0, p1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    #@5
    return-object v0
.end method

.method public substring(II)Ljava/lang/String;
    .registers 5
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 953
    sub-int v1, p2, p1

    #@2
    new-array v0, v1, [C

    #@4
    .line 954
    .local v0, buf:[C
    const/4 v1, 0x0

    #@5
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@8
    .line 955
    new-instance v1, Ljava/lang/String;

    #@a
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    #@d
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 940
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    #@4
    move-result v1

    #@5
    .line 941
    .local v1, len:I
    new-array v0, v1, [C

    #@7
    .line 943
    .local v0, buf:[C
    invoke-virtual {p0, v2, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->getChars(II[CI)V

    #@a
    .line 944
    new-instance v2, Ljava/lang/String;

    #@c
    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    #@f
    return-object v2
.end method
