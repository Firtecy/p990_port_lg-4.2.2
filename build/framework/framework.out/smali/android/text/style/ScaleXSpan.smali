.class public Landroid/text/style/ScaleXSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "ScaleXSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mProportion:F


# direct methods
.method public constructor <init>(F)V
    .registers 2
    .parameter "proportion"

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 29
    iput p1, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@5
    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@9
    .line 34
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getScaleX()F
    .registers 2

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@2
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 37
    const/4 v0, 0x4

    #@1
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 4
    .parameter "ds"

    #@0
    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextScaleX()F

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@6
    mul-float/2addr v0, v1

    #@7
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    #@a
    .line 55
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 4
    .parameter "ds"

    #@0
    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextScaleX()F

    #@3
    move-result v0

    #@4
    iget v1, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@6
    mul-float/2addr v0, v1

    #@7
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    #@a
    .line 60
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 45
    iget v0, p0, Landroid/text/style/ScaleXSpan;->mProportion:F

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@5
    .line 46
    return-void
.end method
