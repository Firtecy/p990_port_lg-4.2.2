.class public Landroid/text/style/TypefaceSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "TypefaceSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mFamily:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 40
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@9
    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "family"

    #@0
    .prologue
    .line 36
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 37
    iput-object p1, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@5
    .line 38
    return-void
.end method

.method private static apply(Landroid/graphics/Paint;Ljava/lang/String;)V
    .registers 7
    .parameter "paint"
    .parameter "family"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    #@3
    move-result-object v1

    #@4
    .line 77
    .local v1, old:Landroid/graphics/Typeface;
    if-nez v1, :cond_28

    #@6
    .line 78
    const/4 v2, 0x0

    #@7
    .line 83
    .local v2, oldStyle:I
    :goto_7
    invoke-static {p1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@a
    move-result-object v3

    #@b
    .line 84
    .local v3, tf:Landroid/graphics/Typeface;
    invoke-virtual {v3}, Landroid/graphics/Typeface;->getStyle()I

    #@e
    move-result v4

    #@f
    xor-int/lit8 v4, v4, -0x1

    #@11
    and-int v0, v2, v4

    #@13
    .line 86
    .local v0, fake:I
    and-int/lit8 v4, v0, 0x1

    #@15
    if-eqz v4, :cond_1b

    #@17
    .line 87
    const/4 v4, 0x1

    #@18
    invoke-virtual {p0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    #@1b
    .line 90
    :cond_1b
    and-int/lit8 v4, v0, 0x2

    #@1d
    if-eqz v4, :cond_24

    #@1f
    .line 91
    const/high16 v4, -0x4180

    #@21
    invoke-virtual {p0, v4}, Landroid/graphics/Paint;->setTextSkewX(F)V

    #@24
    .line 94
    :cond_24
    invoke-virtual {p0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@27
    .line 95
    return-void

    #@28
    .line 80
    .end local v0           #fake:I
    .end local v2           #oldStyle:I
    .end local v3           #tf:Landroid/graphics/Typeface;
    :cond_28
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    #@2b
    move-result v2

    #@2c
    .restart local v2       #oldStyle:I
    goto :goto_7
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 49
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 45
    const/16 v0, 0xd

    #@2
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "ds"

    #@0
    .prologue
    .line 65
    iget-object v0, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@2
    invoke-static {p1, v0}, Landroid/text/style/TypefaceSpan;->apply(Landroid/graphics/Paint;Ljava/lang/String;)V

    #@5
    .line 66
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "paint"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@2
    invoke-static {p1, v0}, Landroid/text/style/TypefaceSpan;->apply(Landroid/graphics/Paint;Ljava/lang/String;)V

    #@5
    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Landroid/text/style/TypefaceSpan;->mFamily:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 54
    return-void
.end method
