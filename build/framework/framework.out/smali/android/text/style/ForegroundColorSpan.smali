.class public Landroid/text/style/ForegroundColorSpan;
.super Landroid/text/style/CharacterStyle;
.source "ForegroundColorSpan.java"

# interfaces
.implements Landroid/text/style/UpdateAppearance;
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mColor:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 29
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 30
    iput p1, p0, Landroid/text/style/ForegroundColorSpan;->mColor:I

    #@5
    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/ForegroundColorSpan;->mColor:I

    #@9
    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getForegroundColor()I
    .registers 2

    #@0
    .prologue
    .line 50
    iget v0, p0, Landroid/text/style/ForegroundColorSpan;->mColor:I

    #@2
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 38
    const/4 v0, 0x2

    #@1
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "ds"

    #@0
    .prologue
    .line 55
    iget v0, p0, Landroid/text/style/ForegroundColorSpan;->mColor:I

    #@2
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    #@5
    .line 56
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 46
    iget v0, p0, Landroid/text/style/ForegroundColorSpan;->mColor:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 47
    return-void
.end method
