.class public Landroid/text/style/AbsoluteSizeSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "AbsoluteSizeSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private mDip:Z

.field private final mSize:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "size"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 33
    iput p1, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@5
    .line 34
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 3
    .parameter "size"
    .parameter "dip"

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 42
    iput p1, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@5
    .line 43
    iput-boolean p2, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@7
    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@9
    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_13

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    iput-boolean v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@12
    .line 49
    return-void

    #@13
    .line 48
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_10
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 56
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getDip()Z
    .registers 2

    #@0
    .prologue
    .line 69
    iget-boolean v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@2
    return v0
.end method

.method public getSize()I
    .registers 2

    #@0
    .prologue
    .line 65
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@2
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 52
    const/16 v0, 0x10

    #@2
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 4
    .parameter "ds"

    #@0
    .prologue
    .line 74
    iget-boolean v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 75
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@6
    int-to-float v0, v0

    #@7
    iget v1, p1, Landroid/text/TextPaint;->density:F

    #@9
    mul-float/2addr v0, v1

    #@a
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    #@d
    .line 79
    :goto_d
    return-void

    #@e
    .line 77
    :cond_e
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@10
    int-to-float v0, v0

    #@11
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    #@14
    goto :goto_d
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 4
    .parameter "ds"

    #@0
    .prologue
    .line 83
    iget-boolean v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 84
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@6
    int-to-float v0, v0

    #@7
    iget v1, p1, Landroid/text/TextPaint;->density:F

    #@9
    mul-float/2addr v0, v1

    #@a
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    #@d
    .line 88
    :goto_d
    return-void

    #@e
    .line 86
    :cond_e
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@10
    int-to-float v0, v0

    #@11
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    #@14
    goto :goto_d
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 60
    iget v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mSize:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 61
    iget-boolean v0, p0, Landroid/text/style/AbsoluteSizeSpan;->mDip:Z

    #@7
    if-eqz v0, :cond_e

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 62
    return-void

    #@e
    .line 61
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_a
.end method
