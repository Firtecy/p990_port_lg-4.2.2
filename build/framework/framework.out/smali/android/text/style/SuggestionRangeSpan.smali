.class public Landroid/text/style/SuggestionRangeSpan;
.super Landroid/text/style/CharacterStyle;
.source "SuggestionRangeSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private mBackgroundColor:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/text/style/SuggestionRangeSpan;->mBackgroundColor:I

    #@6
    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/SuggestionRangeSpan;->mBackgroundColor:I

    #@9
    .line 40
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 44
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 54
    const/16 v0, 0x15

    #@2
    return v0
.end method

.method public setBackgroundColor(I)V
    .registers 2
    .parameter "backgroundColor"

    #@0
    .prologue
    .line 58
    iput p1, p0, Landroid/text/style/SuggestionRangeSpan;->mBackgroundColor:I

    #@2
    .line 59
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "tp"

    #@0
    .prologue
    .line 63
    iget v0, p0, Landroid/text/style/SuggestionRangeSpan;->mBackgroundColor:I

    #@2
    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    #@4
    .line 64
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 49
    iget v0, p0, Landroid/text/style/SuggestionRangeSpan;->mBackgroundColor:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 50
    return-void
.end method
