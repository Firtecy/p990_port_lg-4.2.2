.class public Landroid/text/style/SuggestionSpan;
.super Landroid/text/style/CharacterStyle;
.source "SuggestionSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# static fields
.field public static final ACTION_SUGGESTION_PICKED:Ljava/lang/String; = "android.text.style.SUGGESTION_PICKED"

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/text/style/SuggestionSpan;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLAG_AUTO_CORRECTION:I = 0x4

.field public static final FLAG_EASY_CORRECT:I = 0x1

.field public static final FLAG_MISSPELLED:I = 0x2

.field public static final SUGGESTIONS_MAX_SIZE:I = 0x5

.field public static final SUGGESTION_SPAN_PICKED_AFTER:Ljava/lang/String; = "after"

.field public static final SUGGESTION_SPAN_PICKED_BEFORE:Ljava/lang/String; = "before"

.field public static final SUGGESTION_SPAN_PICKED_HASHCODE:Ljava/lang/String; = "hashcode"


# instance fields
.field private mAutoCorrectionUnderlineColor:I

.field private mAutoCorrectionUnderlineThickness:F

.field private mEasyCorrectUnderlineColor:I

.field private mEasyCorrectUnderlineThickness:F

.field private mFlags:I

.field private final mHashCode:I

.field private final mLocaleString:Ljava/lang/String;

.field private mMisspelledUnderlineColor:I

.field private mMisspelledUnderlineThickness:F

.field private final mNotificationTargetClassName:Ljava/lang/String;

.field private final mSuggestions:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 276
    new-instance v0, Landroid/text/style/SuggestionSpan$1;

    #@2
    invoke-direct {v0}, Landroid/text/style/SuggestionSpan$1;-><init>()V

    #@5
    sput-object v0, Landroid/text/style/SuggestionSpan;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;[Ljava/lang/String;ILjava/lang/Class;)V
    .registers 10
    .parameter "context"
    .parameter "locale"
    .parameter "suggestions"
    .parameter "flags"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Locale;",
            "[",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 124
    .local p5, notificationTargetClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 125
    const/4 v1, 0x5

    #@4
    array-length v2, p3

    #@5
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    #@8
    move-result v0

    #@9
    .line 126
    .local v0, N:I
    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    check-cast v1, [Ljava/lang/String;

    #@f
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mSuggestions:[Ljava/lang/String;

    #@11
    .line 127
    iput p4, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@13
    .line 128
    if-eqz p2, :cond_33

    #@15
    .line 129
    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@1b
    .line 137
    :goto_1b
    if-eqz p5, :cond_52

    #@1d
    .line 138
    invoke-virtual {p5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@23
    .line 142
    :goto_23
    iget-object v1, p0, Landroid/text/style/SuggestionSpan;->mSuggestions:[Ljava/lang/String;

    #@25
    iget-object v2, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@27
    iget-object v3, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@29
    invoke-static {v1, v2, v3}, Landroid/text/style/SuggestionSpan;->hashCodeInternal([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    move-result v1

    #@2d
    iput v1, p0, Landroid/text/style/SuggestionSpan;->mHashCode:I

    #@2f
    .line 144
    invoke-direct {p0, p1}, Landroid/text/style/SuggestionSpan;->initStyle(Landroid/content/Context;)V

    #@32
    .line 145
    return-void

    #@33
    .line 130
    :cond_33
    if-eqz p1, :cond_46

    #@35
    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@3c
    move-result-object v1

    #@3d
    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    #@3f
    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@45
    goto :goto_1b

    #@46
    .line 133
    :cond_46
    const-string v1, "SuggestionSpan"

    #@48
    const-string v2, "No locale or context specified in SuggestionSpan constructor"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 134
    const-string v1, ""

    #@4f
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@51
    goto :goto_1b

    #@52
    .line 140
    :cond_52
    const-string v1, ""

    #@54
    iput-object v1, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@56
    goto :goto_23
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;I)V
    .registers 10
    .parameter "context"
    .parameter "suggestions"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 102
    move-object v0, p0

    #@2
    move-object v1, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move-object v5, v2

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/content/Context;Ljava/util/Locale;[Ljava/lang/String;ILjava/lang/Class;)V

    #@9
    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 183
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    #@3
    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/text/style/SuggestionSpan;->mSuggestions:[Ljava/lang/String;

    #@9
    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@f
    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@15
    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@1b
    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mHashCode:I

    #@21
    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@24
    move-result v0

    #@25
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@27
    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@2a
    move-result v0

    #@2b
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineThickness:F

    #@2d
    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@30
    move-result v0

    #@31
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@33
    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@36
    move-result v0

    #@37
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineThickness:F

    #@39
    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3c
    move-result v0

    #@3d
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@3f
    .line 194
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    #@42
    move-result v0

    #@43
    iput v0, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineThickness:F

    #@45
    .line 195
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;[Ljava/lang/String;I)V
    .registers 10
    .parameter "locale"
    .parameter "suggestions"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 111
    move-object v0, p0

    #@2
    move-object v2, p1

    #@3
    move-object v3, p2

    #@4
    move v4, p3

    #@5
    move-object v5, v1

    #@6
    invoke-direct/range {v0 .. v5}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/content/Context;Ljava/util/Locale;[Ljava/lang/String;ILjava/lang/Class;)V

    #@9
    .line 112
    return-void
.end method

.method private static hashCodeInternal([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "suggestions"
    .parameter "locale"
    .parameter "notificationTargetClassName"

    #@0
    .prologue
    .line 272
    const/4 v0, 0x4

    #@1
    new-array v0, v0, [Ljava/lang/Object;

    #@3
    const/4 v1, 0x0

    #@4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@7
    move-result-wide v2

    #@8
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b
    move-result-object v2

    #@c
    aput-object v2, v0, v1

    #@e
    const/4 v1, 0x1

    #@f
    aput-object p0, v0, v1

    #@11
    const/4 v1, 0x2

    #@12
    aput-object p1, v0, v1

    #@14
    const/4 v1, 0x3

    #@15
    aput-object p2, v0, v1

    #@17
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    #@1a
    move-result v0

    #@1b
    return v0
.end method

.method private initStyle(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/high16 v4, -0x100

    #@5
    const/4 v3, 0x0

    #@6
    .line 148
    if-nez p1, :cond_15

    #@8
    .line 149
    iput v3, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineThickness:F

    #@a
    .line 150
    iput v3, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineThickness:F

    #@c
    .line 151
    iput v3, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineThickness:F

    #@e
    .line 152
    iput v4, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@10
    .line 153
    iput v4, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@12
    .line 154
    iput v4, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@14
    .line 181
    :goto_14
    return-void

    #@15
    .line 158
    :cond_15
    const v0, 0x10103cd

    #@18
    .line 159
    .local v0, defStyle:I
    sget-object v2, Lcom/android/internal/R$styleable;->SuggestionSpan:[I

    #@1a
    invoke-virtual {p1, v7, v2, v0, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@1d
    move-result-object v1

    #@1e
    .line 161
    .local v1, typedArray:Landroid/content/res/TypedArray;
    invoke-virtual {v1, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@21
    move-result v2

    #@22
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineThickness:F

    #@24
    .line 163
    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    #@27
    move-result v2

    #@28
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@2a
    .line 166
    const v0, 0x10103c3

    #@2d
    .line 167
    sget-object v2, Lcom/android/internal/R$styleable;->SuggestionSpan:[I

    #@2f
    invoke-virtual {p1, v7, v2, v0, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@32
    move-result-object v1

    #@33
    .line 169
    invoke-virtual {v1, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@36
    move-result v2

    #@37
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineThickness:F

    #@39
    .line 171
    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    #@3c
    move-result v2

    #@3d
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@3f
    .line 174
    const v0, 0x10103ce

    #@42
    .line 175
    sget-object v2, Lcom/android/internal/R$styleable;->SuggestionSpan:[I

    #@44
    invoke-virtual {p1, v7, v2, v0, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    #@47
    move-result-object v1

    #@48
    .line 177
    invoke-virtual {v1, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    #@4b
    move-result v2

    #@4c
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineThickness:F

    #@4e
    .line 179
    invoke-virtual {v1, v5, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    #@51
    move-result v2

    #@52
    iput v2, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@54
    goto :goto_14
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 234
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter "o"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 259
    instance-of v1, p1, Landroid/text/style/SuggestionSpan;

    #@3
    if-eqz v1, :cond_10

    #@5
    .line 260
    check-cast p1, Landroid/text/style/SuggestionSpan;

    #@7
    .end local p1
    invoke-virtual {p1}, Landroid/text/style/SuggestionSpan;->hashCode()I

    #@a
    move-result v1

    #@b
    iget v2, p0, Landroid/text/style/SuggestionSpan;->mHashCode:I

    #@d
    if-ne v1, v2, :cond_10

    #@f
    const/4 v0, 0x1

    #@10
    .line 262
    :cond_10
    return v0
.end method

.method public getFlags()I
    .registers 2

    #@0
    .prologue
    .line 225
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@2
    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getNotificationTargetClassName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 221
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 254
    const/16 v0, 0x13

    #@2
    return v0
.end method

.method public getSuggestions()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 201
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mSuggestions:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getUnderlineColor()I
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 314
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@4
    and-int/lit8 v5, v5, 0x2

    #@6
    if-eqz v5, :cond_1e

    #@8
    move v2, v3

    #@9
    .line 315
    .local v2, misspelled:Z
    :goto_9
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@b
    and-int/lit8 v5, v5, 0x1

    #@d
    if-eqz v5, :cond_20

    #@f
    move v1, v3

    #@10
    .line 316
    .local v1, easy:Z
    :goto_10
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@12
    and-int/lit8 v5, v5, 0x4

    #@14
    if-eqz v5, :cond_22

    #@16
    move v0, v3

    #@17
    .line 317
    .local v0, autoCorrection:Z
    :goto_17
    if-eqz v1, :cond_27

    #@19
    .line 318
    if-nez v2, :cond_24

    #@1b
    .line 319
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@1d
    .line 326
    :cond_1d
    :goto_1d
    return v4

    #@1e
    .end local v0           #autoCorrection:Z
    .end local v1           #easy:Z
    .end local v2           #misspelled:Z
    :cond_1e
    move v2, v4

    #@1f
    .line 314
    goto :goto_9

    #@20
    .restart local v2       #misspelled:Z
    :cond_20
    move v1, v4

    #@21
    .line 315
    goto :goto_10

    #@22
    .restart local v1       #easy:Z
    :cond_22
    move v0, v4

    #@23
    .line 316
    goto :goto_17

    #@24
    .line 321
    .restart local v0       #autoCorrection:Z
    :cond_24
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@26
    goto :goto_1d

    #@27
    .line 323
    :cond_27
    if-eqz v0, :cond_1d

    #@29
    .line 324
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@2b
    goto :goto_1d
.end method

.method public hashCode()I
    .registers 2

    #@0
    .prologue
    .line 267
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mHashCode:I

    #@2
    return v0
.end method

.method public setFlags(I)V
    .registers 2
    .parameter "flags"

    #@0
    .prologue
    .line 229
    iput p1, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@2
    .line 230
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 8
    .parameter "tp"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 291
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@4
    and-int/lit8 v5, v5, 0x2

    #@6
    if-eqz v5, :cond_23

    #@8
    move v2, v3

    #@9
    .line 292
    .local v2, misspelled:Z
    :goto_9
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@b
    and-int/lit8 v5, v5, 0x1

    #@d
    if-eqz v5, :cond_25

    #@f
    move v1, v3

    #@10
    .line 293
    .local v1, easy:Z
    :goto_10
    iget v5, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@12
    and-int/lit8 v5, v5, 0x4

    #@14
    if-eqz v5, :cond_27

    #@16
    move v0, v3

    #@17
    .line 294
    .local v0, autoCorrection:Z
    :goto_17
    if-eqz v1, :cond_35

    #@19
    .line 295
    if-nez v2, :cond_29

    #@1b
    .line 296
    iget v3, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@1d
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineThickness:F

    #@1f
    invoke-virtual {p1, v3, v4}, Landroid/text/TextPaint;->setUnderlineText(IF)V

    #@22
    .line 305
    :cond_22
    :goto_22
    return-void

    #@23
    .end local v0           #autoCorrection:Z
    .end local v1           #easy:Z
    .end local v2           #misspelled:Z
    :cond_23
    move v2, v4

    #@24
    .line 291
    goto :goto_9

    #@25
    .restart local v2       #misspelled:Z
    :cond_25
    move v1, v4

    #@26
    .line 292
    goto :goto_10

    #@27
    .restart local v1       #easy:Z
    :cond_27
    move v0, v4

    #@28
    .line 293
    goto :goto_17

    #@29
    .line 297
    .restart local v0       #autoCorrection:Z
    :cond_29
    iget v3, p1, Landroid/text/TextPaint;->underlineColor:I

    #@2b
    if-nez v3, :cond_22

    #@2d
    .line 300
    iget v3, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@2f
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineThickness:F

    #@31
    invoke-virtual {p1, v3, v4}, Landroid/text/TextPaint;->setUnderlineText(IF)V

    #@34
    goto :goto_22

    #@35
    .line 302
    :cond_35
    if-eqz v0, :cond_22

    #@37
    .line 303
    iget v3, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@39
    iget v4, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineThickness:F

    #@3b
    invoke-virtual {p1, v3, v4}, Landroid/text/TextPaint;->setUnderlineText(IF)V

    #@3e
    goto :goto_22
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 239
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mSuggestions:[Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@5
    .line 240
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mFlags:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 241
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mLocaleString:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 242
    iget-object v0, p0, Landroid/text/style/SuggestionSpan;->mNotificationTargetClassName:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 243
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mHashCode:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 244
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineColor:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 245
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mEasyCorrectUnderlineThickness:F

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@23
    .line 246
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineColor:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 247
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mMisspelledUnderlineThickness:F

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@2d
    .line 248
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineColor:I

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 249
    iget v0, p0, Landroid/text/style/SuggestionSpan;->mAutoCorrectionUnderlineThickness:F

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    #@37
    .line 250
    return-void
.end method
