.class public Landroid/text/style/BulletSpan;
.super Ljava/lang/Object;
.source "BulletSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/ParcelableSpan;


# static fields
.field private static final BULLET_RADIUS:I = 0x3

.field public static final STANDARD_GAP_WIDTH:I = 0x2

.field private static sBulletPath:Landroid/graphics/Path;


# instance fields
.field private final mColor:I

.field private final mGapWidth:I

.field private final mWantColor:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 35
    const/4 v0, 0x0

    #@1
    sput-object v0, Landroid/text/style/BulletSpan;->sBulletPath:Landroid/graphics/Path;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 39
    const/4 v0, 0x2

    #@5
    iput v0, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@7
    .line 40
    iput-boolean v1, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@9
    .line 41
    iput v1, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@b
    .line 42
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "gapWidth"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 45
    iput p1, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@6
    .line 46
    iput-boolean v0, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@8
    .line 47
    iput v0, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@a
    .line 48
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4
    .parameter "gapWidth"
    .parameter "color"

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    iput p1, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@5
    .line 52
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@8
    .line 53
    iput p2, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@a
    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@9
    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_19

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    iput-boolean v0, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@12
    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@18
    .line 60
    return-void

    #@19
    .line 58
    :cond_19
    const/4 v0, 0x0

    #@1a
    goto :goto_10
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 67
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .registers 21
    .parameter "c"
    .parameter "p"
    .parameter "x"
    .parameter "dir"
    .parameter "top"
    .parameter "baseline"
    .parameter "bottom"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "first"
    .parameter "l"

    #@0
    .prologue
    .line 84
    check-cast p8, Landroid/text/Spanned;

    #@2
    .end local p8
    move-object/from16 v0, p8

    #@4
    invoke-interface {v0, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@7
    move-result v3

    #@8
    move/from16 v0, p9

    #@a
    if-ne v3, v0, :cond_62

    #@c
    .line 85
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    #@f
    move-result-object v2

    #@10
    .line 86
    .local v2, style:Landroid/graphics/Paint$Style;
    const/4 v1, 0x0

    #@11
    .line 88
    .local v1, oldcolor:I
    iget-boolean v3, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@13
    if-eqz v3, :cond_1e

    #@15
    .line 89
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    #@18
    move-result v1

    #@19
    .line 90
    iget v3, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@1b
    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setColor(I)V

    #@1e
    .line 93
    :cond_1e
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@20
    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@23
    .line 95
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_63

    #@29
    .line 96
    sget-object v3, Landroid/text/style/BulletSpan;->sBulletPath:Landroid/graphics/Path;

    #@2b
    if-nez v3, :cond_40

    #@2d
    .line 97
    new-instance v3, Landroid/graphics/Path;

    #@2f
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    #@32
    sput-object v3, Landroid/text/style/BulletSpan;->sBulletPath:Landroid/graphics/Path;

    #@34
    .line 99
    sget-object v3, Landroid/text/style/BulletSpan;->sBulletPath:Landroid/graphics/Path;

    #@36
    const/4 v4, 0x0

    #@37
    const/4 v5, 0x0

    #@38
    const v6, 0x40666667

    #@3b
    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    #@3d
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    #@40
    .line 102
    :cond_40
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@43
    .line 103
    mul-int/lit8 v3, p4, 0x3

    #@45
    add-int/2addr v3, p3

    #@46
    int-to-float v3, v3

    #@47
    add-int v4, p5, p7

    #@49
    int-to-float v4, v4

    #@4a
    const/high16 v5, 0x4000

    #@4c
    div-float/2addr v4, v5

    #@4d
    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    #@50
    .line 104
    sget-object v3, Landroid/text/style/BulletSpan;->sBulletPath:Landroid/graphics/Path;

    #@52
    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    #@55
    .line 105
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@58
    .line 110
    :goto_58
    iget-boolean v3, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@5a
    if-eqz v3, :cond_5f

    #@5c
    .line 111
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    #@5f
    .line 114
    :cond_5f
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@62
    .line 116
    .end local v1           #oldcolor:I
    .end local v2           #style:Landroid/graphics/Paint$Style;
    :cond_62
    return-void

    #@63
    .line 107
    .restart local v1       #oldcolor:I
    .restart local v2       #style:Landroid/graphics/Paint$Style;
    :cond_63
    mul-int/lit8 v3, p4, 0x3

    #@65
    add-int/2addr v3, p3

    #@66
    int-to-float v3, v3

    #@67
    add-int v4, p5, p7

    #@69
    int-to-float v4, v4

    #@6a
    const/high16 v5, 0x4000

    #@6c
    div-float/2addr v4, v5

    #@6d
    const/high16 v5, 0x4040

    #@6f
    invoke-virtual {p1, v3, v4, v5, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    #@72
    goto :goto_58
.end method

.method public getLeadingMargin(Z)I
    .registers 3
    .parameter "first"

    #@0
    .prologue
    .line 77
    iget v0, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@2
    add-int/lit8 v0, v0, 0x6

    #@4
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 63
    const/16 v0, 0x8

    #@2
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 71
    iget v0, p0, Landroid/text/style/BulletSpan;->mGapWidth:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 72
    iget-boolean v0, p0, Landroid/text/style/BulletSpan;->mWantColor:Z

    #@7
    if-eqz v0, :cond_13

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 73
    iget v0, p0, Landroid/text/style/BulletSpan;->mColor:I

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 74
    return-void

    #@13
    .line 72
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_a
.end method
