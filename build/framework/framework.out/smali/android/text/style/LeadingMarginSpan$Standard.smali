.class public Landroid/text/style/LeadingMarginSpan$Standard;
.super Ljava/lang/Object;
.source "LeadingMarginSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/ParcelableSpan;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/style/LeadingMarginSpan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Standard"
.end annotation


# instance fields
.field private final mFirst:I

.field private final mRest:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "every"

    #@0
    .prologue
    .line 112
    invoke-direct {p0, p1, p1}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    #@3
    .line 113
    return-void
.end method

.method public constructor <init>(II)V
    .registers 3
    .parameter "first"
    .parameter "rest"

    #@0
    .prologue
    .line 102
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 103
    iput p1, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mFirst:I

    #@5
    .line 104
    iput p2, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mRest:I

    #@7
    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 115
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mFirst:I

    #@9
    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@c
    move-result v0

    #@d
    iput v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mRest:I

    #@f
    .line 118
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 125
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .registers 13
    .parameter "c"
    .parameter "p"
    .parameter "x"
    .parameter "dir"
    .parameter "top"
    .parameter "baseline"
    .parameter "bottom"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "first"
    .parameter "layout"

    #@0
    .prologue
    .line 143
    return-void
.end method

.method public getLeadingMargin(Z)I
    .registers 3
    .parameter "first"

    #@0
    .prologue
    .line 134
    if-eqz p1, :cond_5

    #@2
    iget v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mFirst:I

    #@4
    :goto_4
    return v0

    #@5
    :cond_5
    iget v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mRest:I

    #@7
    goto :goto_4
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 121
    const/16 v0, 0xa

    #@2
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 129
    iget v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mFirst:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 130
    iget v0, p0, Landroid/text/style/LeadingMarginSpan$Standard;->mRest:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 131
    return-void
.end method
