.class public Landroid/text/style/DrawableMarginSpan;
.super Ljava/lang/Object;
.source "DrawableMarginSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/style/LineHeightSpan;


# instance fields
.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mPad:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput-object p1, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .registers 3
    .parameter "b"
    .parameter "pad"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    iput-object p1, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@5
    .line 35
    iput p2, p0, Landroid/text/style/DrawableMarginSpan;->mPad:I

    #@7
    .line 36
    return-void
.end method


# virtual methods
.method public chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
    .registers 11
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "istartv"
    .parameter "v"
    .parameter "fm"

    #@0
    .prologue
    .line 61
    check-cast p1, Landroid/text/Spanned;

    #@2
    .end local p1
    invoke-interface {p1, p0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@5
    move-result v2

    #@6
    if-ne p3, v2, :cond_2e

    #@8
    .line 62
    iget-object v2, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@a
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@d
    move-result v0

    #@e
    .line 64
    .local v0, ht:I
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@10
    add-int/2addr v2, p5

    #@11
    iget v3, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@13
    sub-int/2addr v2, v3

    #@14
    sub-int/2addr v2, p4

    #@15
    sub-int v1, v0, v2

    #@17
    .line 65
    .local v1, need:I
    if-lez v1, :cond_1e

    #@19
    .line 66
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@1b
    add-int/2addr v2, v1

    #@1c
    iput v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@1e
    .line 68
    :cond_1e
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@20
    add-int/2addr v2, p5

    #@21
    iget v3, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@23
    sub-int/2addr v2, v3

    #@24
    sub-int/2addr v2, p4

    #@25
    sub-int v1, v0, v2

    #@27
    .line 69
    if-lez v1, :cond_2e

    #@29
    .line 70
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@2b
    add-int/2addr v2, v1

    #@2c
    iput v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@2e
    .line 72
    .end local v0           #ht:I
    .end local v1           #need:I
    :cond_2e
    return-void
.end method

.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .registers 22
    .parameter "c"
    .parameter "p"
    .parameter "x"
    .parameter "dir"
    .parameter "top"
    .parameter "baseline"
    .parameter "bottom"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "first"
    .parameter "layout"

    #@0
    .prologue
    .line 46
    check-cast p8, Landroid/text/Spanned;

    #@2
    .end local p8
    move-object/from16 v0, p8

    #@4
    invoke-interface {v0, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@7
    move-result v5

    #@8
    .line 47
    .local v5, st:I
    move v4, p3

    #@9
    .line 48
    .local v4, ix:I
    move-object/from16 v0, p12

    #@b
    invoke-virtual {v0, v5}, Landroid/text/Layout;->getLineForOffset(I)I

    #@e
    move-result v6

    #@f
    move-object/from16 v0, p12

    #@11
    invoke-virtual {v0, v6}, Landroid/text/Layout;->getLineTop(I)I

    #@14
    move-result v3

    #@15
    .line 50
    .local v3, itop:I
    iget-object v6, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@17
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@1a
    move-result v2

    #@1b
    .line 51
    .local v2, dw:I
    iget-object v6, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@1d
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    #@20
    move-result v1

    #@21
    .line 54
    .local v1, dh:I
    iget-object v6, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@23
    add-int v7, v4, v2

    #@25
    add-int v8, v3, v1

    #@27
    invoke-virtual {v6, v4, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    #@2a
    .line 55
    iget-object v6, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2c
    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@2f
    .line 56
    return-void
.end method

.method public getLeadingMargin(Z)I
    .registers 4
    .parameter "first"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/text/style/DrawableMarginSpan;->mDrawable:Landroid/graphics/drawable/Drawable;

    #@2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/text/style/DrawableMarginSpan;->mPad:I

    #@8
    add-int/2addr v0, v1

    #@9
    return v0
.end method
