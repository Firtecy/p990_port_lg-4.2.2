.class public abstract Landroid/text/style/DynamicDrawableSpan;
.super Landroid/text/style/ReplacementSpan;
.source "DynamicDrawableSpan.java"


# static fields
.field public static final ALIGN_BASELINE:I = 0x1

.field public static final ALIGN_BOTTOM:I = 0x0

.field public static final ALIGN_CENTER:I = 0x63

.field private static final TAG:Ljava/lang/String; = "DynamicDrawableSpan"


# instance fields
.field private mDrawableRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field protected final mVerticalAlignment:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 59
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    #@3
    .line 60
    const/4 v0, 0x0

    #@4
    iput v0, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@6
    .line 61
    return-void
.end method

.method protected constructor <init>(I)V
    .registers 2
    .parameter "verticalAlignment"

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    #@3
    .line 67
    iput p1, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@5
    .line 68
    return-void
.end method

.method private getCachedDrawable()Landroid/graphics/drawable/Drawable;
    .registers 4

    #@0
    .prologue
    .line 139
    iget-object v1, p0, Landroid/text/style/DynamicDrawableSpan;->mDrawableRef:Ljava/lang/ref/WeakReference;

    #@2
    .line 140
    .local v1, wr:Ljava/lang/ref/WeakReference;,"Ljava/lang/ref/WeakReference<Landroid/graphics/drawable/Drawable;>;"
    const/4 v0, 0x0

    #@3
    .line 142
    .local v0, d:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_b

    #@5
    .line 143
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    .end local v0           #d:Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/Drawable;

    #@b
    .line 145
    .restart local v0       #d:Landroid/graphics/drawable/Drawable;
    :cond_b
    if-nez v0, :cond_18

    #@d
    .line 146
    invoke-virtual {p0}, Landroid/text/style/DynamicDrawableSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    #@10
    move-result-object v0

    #@11
    .line 147
    new-instance v2, Ljava/lang/ref/WeakReference;

    #@13
    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    #@16
    iput-object v2, p0, Landroid/text/style/DynamicDrawableSpan;->mDrawableRef:Ljava/lang/ref/WeakReference;

    #@18
    .line 150
    :cond_18
    return-object v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .registers 14
    .parameter "canvas"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "x"
    .parameter "top"
    .parameter "y"
    .parameter "bottom"
    .parameter "paint"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/text/style/DynamicDrawableSpan;->getCachedDrawable()Landroid/graphics/drawable/Drawable;

    #@3
    move-result-object v0

    #@4
    .line 121
    .local v0, b:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    #@7
    .line 123
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@a
    move-result-object v2

    #@b
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    #@d
    sub-int v1, p8, v2

    #@f
    .line 124
    .local v1, transY:I
    iget v2, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@11
    const/4 v3, 0x1

    #@12
    if-ne v2, v3, :cond_26

    #@14
    .line 125
    invoke-virtual {p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    #@17
    move-result-object v2

    #@18
    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@1a
    sub-int/2addr v1, v2

    #@1b
    .line 133
    :cond_1b
    :goto_1b
    int-to-float v2, v1

    #@1c
    invoke-virtual {p1, p5, v2}, Landroid/graphics/Canvas;->translate(FF)V

    #@1f
    .line 134
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    #@22
    .line 135
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    #@25
    .line 136
    return-void

    #@26
    .line 128
    :cond_26
    iget v2, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@28
    const/16 v3, 0x63

    #@2a
    if-ne v2, v3, :cond_1b

    #@2c
    .line 129
    div-int/lit8 v1, v1, 0x2

    #@2e
    goto :goto_1b
.end method

.method public abstract getDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .registers 11
    .parameter "paint"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "fm"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 89
    invoke-direct {p0}, Landroid/text/style/DynamicDrawableSpan;->getCachedDrawable()Landroid/graphics/drawable/Drawable;

    #@4
    move-result-object v0

    #@5
    .line 90
    .local v0, d:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    #@8
    move-result-object v1

    #@9
    .line 92
    .local v1, rect:Landroid/graphics/Rect;
    if-eqz p5, :cond_1d

    #@b
    .line 95
    iget v2, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@d
    const/4 v3, 0x1

    #@e
    if-ne v2, v3, :cond_20

    #@10
    .line 96
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    #@12
    neg-int v2, v2

    #@13
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@15
    .line 97
    iput v4, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@17
    .line 99
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@19
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@1b
    .line 100
    iput v4, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@1d
    .line 113
    :cond_1d
    :goto_1d
    iget v2, v1, Landroid/graphics/Rect;->right:I

    #@1f
    return v2

    #@20
    .line 102
    :cond_20
    iget v2, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@22
    if-nez v2, :cond_30

    #@24
    .line 103
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@26
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    #@28
    sub-int/2addr v2, v3

    #@29
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@2b
    .line 104
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@2d
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@2f
    goto :goto_1d

    #@30
    .line 106
    :cond_30
    iget v2, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@32
    const/16 v3, 0x63

    #@34
    if-ne v2, v3, :cond_1d

    #@36
    .line 107
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@38
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    #@3a
    sub-int/2addr v2, v3

    #@3b
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@3d
    .line 108
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@3f
    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@41
    goto :goto_1d
.end method

.method public getVerticalAlignment()I
    .registers 2

    #@0
    .prologue
    .line 75
    iget v0, p0, Landroid/text/style/DynamicDrawableSpan;->mVerticalAlignment:I

    #@2
    return v0
.end method
