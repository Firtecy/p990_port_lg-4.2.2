.class public Landroid/text/style/StyleSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "StyleSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mStyle:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .parameter "style"

    #@0
    .prologue
    .line 44
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 45
    iput p1, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@5
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 48
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@9
    .line 50
    return-void
.end method

.method private static apply(Landroid/graphics/Paint;I)V
    .registers 8
    .parameter "paint"
    .parameter "style"

    #@0
    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    #@3
    move-result-object v1

    #@4
    .line 85
    .local v1, old:Landroid/graphics/Typeface;
    if-nez v1, :cond_2c

    #@6
    .line 86
    const/4 v2, 0x0

    #@7
    .line 91
    .local v2, oldStyle:I
    :goto_7
    or-int v4, v2, p1

    #@9
    .line 94
    .local v4, want:I
    if-nez v1, :cond_31

    #@b
    .line 95
    invoke-static {v4}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    #@e
    move-result-object v3

    #@f
    .line 100
    .local v3, tf:Landroid/graphics/Typeface;
    :goto_f
    invoke-virtual {v3}, Landroid/graphics/Typeface;->getStyle()I

    #@12
    move-result v5

    #@13
    xor-int/lit8 v5, v5, -0x1

    #@15
    and-int v0, v4, v5

    #@17
    .line 102
    .local v0, fake:I
    and-int/lit8 v5, v0, 0x1

    #@19
    if-eqz v5, :cond_1f

    #@1b
    .line 103
    const/4 v5, 0x1

    #@1c
    invoke-virtual {p0, v5}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    #@1f
    .line 106
    :cond_1f
    and-int/lit8 v5, v0, 0x2

    #@21
    if-eqz v5, :cond_28

    #@23
    .line 107
    const/high16 v5, -0x4180

    #@25
    invoke-virtual {p0, v5}, Landroid/graphics/Paint;->setTextSkewX(F)V

    #@28
    .line 110
    :cond_28
    invoke-virtual {p0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@2b
    .line 111
    return-void

    #@2c
    .line 88
    .end local v0           #fake:I
    .end local v2           #oldStyle:I
    .end local v3           #tf:Landroid/graphics/Typeface;
    .end local v4           #want:I
    :cond_2c
    invoke-virtual {v1}, Landroid/graphics/Typeface;->getStyle()I

    #@2f
    move-result v2

    #@30
    .restart local v2       #oldStyle:I
    goto :goto_7

    #@31
    .line 97
    .restart local v4       #want:I
    :cond_31
    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@34
    move-result-object v3

    #@35
    .restart local v3       #tf:Landroid/graphics/Typeface;
    goto :goto_f
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 57
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 53
    const/4 v0, 0x7

    #@1
    return v0
.end method

.method public getStyle()I
    .registers 2

    #@0
    .prologue
    .line 68
    iget v0, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@2
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "ds"

    #@0
    .prologue
    .line 73
    iget v0, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@2
    invoke-static {p1, v0}, Landroid/text/style/StyleSpan;->apply(Landroid/graphics/Paint;I)V

    #@5
    .line 74
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter "paint"

    #@0
    .prologue
    .line 78
    iget v0, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@2
    invoke-static {p1, v0}, Landroid/text/style/StyleSpan;->apply(Landroid/graphics/Paint;I)V

    #@5
    .line 79
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 61
    iget v0, p0, Landroid/text/style/StyleSpan;->mStyle:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 62
    return-void
.end method
