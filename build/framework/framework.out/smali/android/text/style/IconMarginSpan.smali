.class public Landroid/text/style/IconMarginSpan;
.super Ljava/lang/Object;
.source "IconMarginSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/style/LineHeightSpan;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mPad:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter "b"

    #@0
    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 30
    iput-object p1, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@5
    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .registers 3
    .parameter "b"
    .parameter "pad"

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    iput-object p1, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@5
    .line 35
    iput p2, p0, Landroid/text/style/IconMarginSpan;->mPad:I

    #@7
    .line 36
    return-void
.end method


# virtual methods
.method public chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
    .registers 11
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "istartv"
    .parameter "v"
    .parameter "fm"

    #@0
    .prologue
    .line 58
    check-cast p1, Landroid/text/Spanned;

    #@2
    .end local p1
    invoke-interface {p1, p0}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    #@5
    move-result v2

    #@6
    if-ne p3, v2, :cond_2e

    #@8
    .line 59
    iget-object v2, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@a
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    #@d
    move-result v0

    #@e
    .line 61
    .local v0, ht:I
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@10
    add-int/2addr v2, p5

    #@11
    iget v3, p6, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@13
    sub-int/2addr v2, v3

    #@14
    sub-int/2addr v2, p4

    #@15
    sub-int v1, v0, v2

    #@17
    .line 62
    .local v1, need:I
    if-lez v1, :cond_1e

    #@19
    .line 63
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@1b
    add-int/2addr v2, v1

    #@1c
    iput v2, p6, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@1e
    .line 65
    :cond_1e
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@20
    add-int/2addr v2, p5

    #@21
    iget v3, p6, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@23
    sub-int/2addr v2, v3

    #@24
    sub-int/2addr v2, p4

    #@25
    sub-int v1, v0, v2

    #@27
    .line 66
    if-lez v1, :cond_2e

    #@29
    .line 67
    iget v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@2b
    add-int/2addr v2, v1

    #@2c
    iput v2, p6, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@2e
    .line 69
    .end local v0           #ht:I
    .end local v1           #need:I
    :cond_2e
    return-void
.end method

.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .registers 19
    .parameter "c"
    .parameter "p"
    .parameter "x"
    .parameter "dir"
    .parameter "top"
    .parameter "baseline"
    .parameter "bottom"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "first"
    .parameter "layout"

    #@0
    .prologue
    .line 46
    check-cast p8, Landroid/text/Spanned;

    #@2
    .end local p8
    invoke-interface {p8, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@5
    move-result v2

    #@6
    .line 47
    .local v2, st:I
    move-object/from16 v0, p12

    #@8
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    #@b
    move-result v3

    #@c
    move-object/from16 v0, p12

    #@e
    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineTop(I)I

    #@11
    move-result v1

    #@12
    .line 49
    .local v1, itop:I
    if-gez p4, :cond_1b

    #@14
    .line 50
    iget-object v3, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@16
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    #@19
    move-result v3

    #@1a
    sub-int/2addr p3, v3

    #@1b
    .line 52
    :cond_1b
    iget-object v3, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@1d
    int-to-float v4, p3

    #@1e
    int-to-float v5, v1

    #@1f
    invoke-virtual {p1, v3, v4, v5, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    #@22
    .line 53
    return-void
.end method

.method public getLeadingMargin(Z)I
    .registers 4
    .parameter "first"

    #@0
    .prologue
    .line 39
    iget-object v0, p0, Landroid/text/style/IconMarginSpan;->mBitmap:Landroid/graphics/Bitmap;

    #@2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    #@5
    move-result v0

    #@6
    iget v1, p0, Landroid/text/style/IconMarginSpan;->mPad:I

    #@8
    add-int/2addr v0, v1

    #@9
    return v0
.end method
