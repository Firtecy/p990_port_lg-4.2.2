.class public Landroid/text/style/SpellCheckSpan;
.super Ljava/lang/Object;
.source "SpellCheckSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private mSpellCheckInProgress:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 35
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Landroid/text/style/SpellCheckSpan;->mSpellCheckInProgress:Z

    #@6
    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_d

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    iput-boolean v0, p0, Landroid/text/style/SpellCheckSpan;->mSpellCheckInProgress:Z

    #@c
    .line 40
    return-void

    #@d
    .line 39
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_a
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 52
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 62
    const/16 v0, 0x14

    #@2
    return v0
.end method

.method public isSpellCheckInProgress()Z
    .registers 2

    #@0
    .prologue
    .line 47
    iget-boolean v0, p0, Landroid/text/style/SpellCheckSpan;->mSpellCheckInProgress:Z

    #@2
    return v0
.end method

.method public setSpellCheckInProgress(Z)V
    .registers 2
    .parameter "inProgress"

    #@0
    .prologue
    .line 43
    iput-boolean p1, p0, Landroid/text/style/SpellCheckSpan;->mSpellCheckInProgress:Z

    #@2
    .line 44
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 57
    iget-boolean v0, p0, Landroid/text/style/SpellCheckSpan;->mSpellCheckInProgress:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@8
    .line 58
    return-void

    #@9
    .line 57
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method
