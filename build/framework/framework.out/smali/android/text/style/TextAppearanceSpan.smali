.class public Landroid/text/style/TextAppearanceSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "TextAppearanceSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mStyle:I

.field private final mTextColor:Landroid/content/res/ColorStateList;

.field private final mTextColorLink:Landroid/content/res/ColorStateList;

.field private final mTextSize:I

.field private final mTypeface:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter "context"
    .parameter "appearance"

    #@0
    .prologue
    .line 45
    const/4 v0, -0x1

    #@1
    invoke-direct {p0, p1, p2, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;II)V

    #@4
    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .registers 10
    .parameter "context"
    .parameter "appearance"
    .parameter "colorList"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 56
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@4
    .line 59
    sget-object v4, Lcom/android/internal/R$styleable;->TextAppearance:[I

    #@6
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@9
    move-result-object v0

    #@a
    .line 63
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v4, 0x3

    #@b
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@e
    move-result-object v2

    #@f
    .line 65
    .local v2, textColor:Landroid/content/res/ColorStateList;
    const/4 v4, 0x6

    #@10
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@13
    move-result-object v4

    #@14
    iput-object v4, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@16
    .line 67
    const/4 v4, -0x1

    #@17
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    #@1a
    move-result v4

    #@1b
    iput v4, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@1d
    .line 70
    const/4 v4, 0x2

    #@1e
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@21
    move-result v4

    #@22
    iput v4, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@24
    .line 71
    const/16 v4, 0x8

    #@26
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 72
    .local v1, family:Ljava/lang/String;
    if-eqz v1, :cond_46

    #@2c
    .line 73
    iput-object v1, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@2e
    .line 96
    :goto_2e
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@31
    .line 98
    if-ltz p3, :cond_43

    #@33
    .line 99
    const v4, 0x1030005

    #@36
    sget-object v5, Lcom/android/internal/R$styleable;->Theme:[I

    #@38
    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    #@3b
    move-result-object v0

    #@3c
    .line 102
    invoke-virtual {v0, p3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    #@3f
    move-result-object v2

    #@40
    .line 103
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    #@43
    .line 106
    :cond_43
    iput-object v2, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@45
    .line 107
    return-void

    #@46
    .line 75
    :cond_46
    const/4 v4, 0x1

    #@47
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    #@4a
    move-result v3

    #@4b
    .line 77
    .local v3, tf:I
    packed-switch v3, :pswitch_data_64

    #@4e
    .line 91
    const/4 v4, 0x0

    #@4f
    iput-object v4, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@51
    goto :goto_2e

    #@52
    .line 79
    :pswitch_52
    const-string/jumbo v4, "sans"

    #@55
    iput-object v4, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@57
    goto :goto_2e

    #@58
    .line 83
    :pswitch_58
    const-string/jumbo v4, "serif"

    #@5b
    iput-object v4, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@5d
    goto :goto_2e

    #@5e
    .line 87
    :pswitch_5e
    const-string/jumbo v4, "monospace"

    #@61
    iput-object v4, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@63
    goto :goto_2e

    #@64
    .line 77
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_52
        :pswitch_58
        :pswitch_5e
    .end packed-switch
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "src"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 122
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@4
    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@a
    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v0

    #@e
    iput v0, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@10
    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@13
    move-result v0

    #@14
    iput v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@16
    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v0

    #@1a
    if-eqz v0, :cond_37

    #@1c
    .line 127
    sget-object v0, Landroid/content/res/ColorStateList;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Landroid/content/res/ColorStateList;

    #@24
    iput-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@26
    .line 131
    :goto_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@29
    move-result v0

    #@2a
    if-eqz v0, :cond_3a

    #@2c
    .line 132
    sget-object v0, Landroid/content/res/ColorStateList;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2e
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@31
    move-result-object v0

    #@32
    check-cast v0, Landroid/content/res/ColorStateList;

    #@34
    iput-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@36
    .line 136
    :goto_36
    return-void

    #@37
    .line 129
    :cond_37
    iput-object v1, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@39
    goto :goto_26

    #@3a
    .line 134
    :cond_3a
    iput-object v1, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@3c
    goto :goto_36
.end method

.method public constructor <init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V
    .registers 6
    .parameter "family"
    .parameter "style"
    .parameter "size"
    .parameter "color"
    .parameter "linkColor"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    #@3
    .line 115
    iput-object p1, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@5
    .line 116
    iput p2, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@7
    .line 117
    iput p3, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@9
    .line 118
    iput-object p4, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@b
    .line 119
    iput-object p5, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@d
    .line 120
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 143
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getFamily()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getLinkTextColor()Landroid/content/res/ColorStateList;
    .registers 2

    #@0
    .prologue
    .line 185
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@2
    return-object v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 139
    const/16 v0, 0x11

    #@2
    return v0
.end method

.method public getTextColor()Landroid/content/res/ColorStateList;
    .registers 2

    #@0
    .prologue
    .line 177
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@2
    return-object v0
.end method

.method public getTextSize()I
    .registers 2

    #@0
    .prologue
    .line 193
    iget v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@2
    return v0
.end method

.method public getTextStyle()I
    .registers 2

    #@0
    .prologue
    .line 201
    iget v0, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@2
    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 5
    .parameter "ds"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 206
    invoke-virtual {p0, p1}, Landroid/text/style/TextAppearanceSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@4
    .line 208
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@6
    if-eqz v0, :cond_13

    #@8
    .line 209
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@a
    iget-object v1, p1, Landroid/text/TextPaint;->drawableState:[I

    #@c
    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@f
    move-result v0

    #@10
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    #@13
    .line 212
    :cond_13
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@15
    if-eqz v0, :cond_21

    #@17
    .line 213
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@19
    iget-object v1, p1, Landroid/text/TextPaint;->drawableState:[I

    #@1b
    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    #@1e
    move-result v0

    #@1f
    iput v0, p1, Landroid/text/TextPaint;->linkColor:I

    #@21
    .line 215
    :cond_21
    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 6
    .parameter "ds"

    #@0
    .prologue
    .line 219
    iget-object v3, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@2
    if-nez v3, :cond_8

    #@4
    iget v3, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@6
    if-eqz v3, :cond_3c

    #@8
    .line 220
    :cond_8
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    #@b
    move-result-object v2

    #@c
    .line 221
    .local v2, tf:Landroid/graphics/Typeface;
    const/4 v1, 0x0

    #@d
    .line 223
    .local v1, style:I
    if-eqz v2, :cond_13

    #@f
    .line 224
    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    #@12
    move-result v1

    #@13
    .line 227
    :cond_13
    iget v3, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@15
    or-int/2addr v1, v3

    #@16
    .line 229
    iget-object v3, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@18
    if-eqz v3, :cond_47

    #@1a
    .line 230
    iget-object v3, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@1c
    invoke-static {v3, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    #@1f
    move-result-object v2

    #@20
    .line 237
    :goto_20
    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    #@23
    move-result v3

    #@24
    xor-int/lit8 v3, v3, -0x1

    #@26
    and-int v0, v1, v3

    #@28
    .line 239
    .local v0, fake:I
    and-int/lit8 v3, v0, 0x1

    #@2a
    if-eqz v3, :cond_30

    #@2c
    .line 240
    const/4 v3, 0x1

    #@2d
    invoke-virtual {p1, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    #@30
    .line 243
    :cond_30
    and-int/lit8 v3, v0, 0x2

    #@32
    if-eqz v3, :cond_39

    #@34
    .line 244
    const/high16 v3, -0x4180

    #@36
    invoke-virtual {p1, v3}, Landroid/text/TextPaint;->setTextSkewX(F)V

    #@39
    .line 247
    :cond_39
    invoke-virtual {p1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    #@3c
    .line 250
    .end local v0           #fake:I
    .end local v1           #style:I
    .end local v2           #tf:Landroid/graphics/Typeface;
    :cond_3c
    iget v3, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@3e
    if-lez v3, :cond_46

    #@40
    .line 251
    iget v3, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@42
    int-to-float v3, v3

    #@43
    invoke-virtual {p1, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    #@46
    .line 253
    :cond_46
    return-void

    #@47
    .line 231
    .restart local v1       #style:I
    .restart local v2       #tf:Landroid/graphics/Typeface;
    :cond_47
    if-nez v2, :cond_4e

    #@49
    .line 232
    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    #@4c
    move-result-object v2

    #@4d
    goto :goto_20

    #@4e
    .line 234
    :cond_4e
    invoke-static {v2, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    #@51
    move-result-object v2

    #@52
    goto :goto_20
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 147
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTypeface:Ljava/lang/String;

    #@4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@7
    .line 148
    iget v0, p0, Landroid/text/style/TextAppearanceSpan;->mStyle:I

    #@9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@c
    .line 149
    iget v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextSize:I

    #@e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 150
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@13
    if-eqz v0, :cond_2a

    #@15
    .line 151
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 152
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColor:Landroid/content/res/ColorStateList;

    #@1a
    invoke-virtual {v0, p1, p2}, Landroid/content/res/ColorStateList;->writeToParcel(Landroid/os/Parcel;I)V

    #@1d
    .line 156
    :goto_1d
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@1f
    if-eqz v0, :cond_2e

    #@21
    .line 157
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 158
    iget-object v0, p0, Landroid/text/style/TextAppearanceSpan;->mTextColorLink:Landroid/content/res/ColorStateList;

    #@26
    invoke-virtual {v0, p1, p2}, Landroid/content/res/ColorStateList;->writeToParcel(Landroid/os/Parcel;I)V

    #@29
    .line 162
    :goto_29
    return-void

    #@2a
    .line 154
    :cond_2a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    goto :goto_1d

    #@2e
    .line 160
    :cond_2e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    goto :goto_29
.end method
