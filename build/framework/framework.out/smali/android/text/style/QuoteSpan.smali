.class public Landroid/text/style/QuoteSpan;
.super Ljava/lang/Object;
.source "QuoteSpan.java"

# interfaces
.implements Landroid/text/style/LeadingMarginSpan;
.implements Landroid/text/ParcelableSpan;


# static fields
.field private static final GAP_WIDTH:I = 0x2

.field private static final STRIPE_WIDTH:I = 0x2


# instance fields
.field private final mColor:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 34
    const v0, -0xffff01

    #@6
    iput v0, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@8
    .line 35
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .parameter "color"

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 39
    iput p1, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@5
    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 42
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@9
    .line 44
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 51
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .registers 21
    .parameter "c"
    .parameter "p"
    .parameter "x"
    .parameter "dir"
    .parameter "top"
    .parameter "baseline"
    .parameter "bottom"
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "first"
    .parameter "layout"

    #@0
    .prologue
    .line 70
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    #@3
    move-result-object v7

    #@4
    .line 71
    .local v7, style:Landroid/graphics/Paint$Style;
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    #@7
    move-result v6

    #@8
    .line 73
    .local v6, color:I
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    #@a
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@d
    .line 74
    iget v0, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@f
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    #@12
    .line 76
    int-to-float v1, p3

    #@13
    int-to-float v2, p5

    #@14
    mul-int/lit8 v0, p4, 0x2

    #@16
    add-int/2addr v0, p3

    #@17
    int-to-float v3, v0

    #@18
    int-to-float v4, p7

    #@19
    move-object v0, p1

    #@1a
    move-object v5, p2

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    #@1e
    .line 78
    invoke-virtual {p2, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    #@21
    .line 79
    invoke-virtual {p2, v6}, Landroid/graphics/Paint;->setColor(I)V

    #@24
    .line 80
    return-void
.end method

.method public getColor()I
    .registers 2

    #@0
    .prologue
    .line 59
    iget v0, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@2
    return v0
.end method

.method public getLeadingMargin(Z)I
    .registers 3
    .parameter "first"

    #@0
    .prologue
    .line 63
    const/4 v0, 0x4

    #@1
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 47
    const/16 v0, 0x9

    #@2
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 55
    iget v0, p0, Landroid/text/style/QuoteSpan;->mColor:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 56
    return-void
.end method
