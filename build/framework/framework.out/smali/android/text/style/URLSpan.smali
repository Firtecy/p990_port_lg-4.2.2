.class public Landroid/text/style/URLSpan;
.super Landroid/text/style/ClickableSpan;
.source "URLSpan.java"

# interfaces
.implements Landroid/text/ParcelableSpan;


# instance fields
.field private final mURL:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "src"

    #@0
    .prologue
    .line 37
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    #@3
    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Landroid/text/style/URLSpan;->mURL:Ljava/lang/String;

    #@9
    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    #@0
    .prologue
    .line 33
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    #@3
    .line 34
    iput-object p1, p0, Landroid/text/style/URLSpan;->mURL:Ljava/lang/String;

    #@5
    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 46
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getSpanTypeId()I
    .registers 2

    #@0
    .prologue
    .line 42
    const/16 v0, 0xb

    #@2
    return v0
.end method

.method public getURL()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Landroid/text/style/URLSpan;->mURL:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter "widget"

    #@0
    .prologue
    .line 61
    :try_start_0
    invoke-virtual {p0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    #@3
    move-result-object v4

    #@4
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7
    move-result-object v3

    #@8
    .line 62
    .local v3, uri:Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@b
    move-result-object v0

    #@c
    .line 63
    .local v0, context:Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    #@e
    const-string v4, "android.intent.action.VIEW"

    #@10
    invoke-direct {v2, v4, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@13
    .line 64
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "com.android.browser.application_id"

    #@15
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1c
    .line 65
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1f} :catch_20

    #@1f
    .line 71
    .end local v2           #intent:Landroid/content/Intent;
    .end local v3           #uri:Landroid/net/Uri;
    :goto_1f
    return-void

    #@20
    .line 66
    .end local v0           #context:Landroid/content/Context;
    :catch_20
    move-exception v1

    #@21
    .line 67
    .local v1, ex:Ljava/lang/Exception;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    #@24
    move-result-object v0

    #@25
    .line 68
    .restart local v0       #context:Landroid/content/Context;
    const v4, 0x10403fa

    #@28
    const/4 v5, 0x0

    #@29
    invoke-static {v0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@30
    goto :goto_1f
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Landroid/text/style/URLSpan;->mURL:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 51
    return-void
.end method
