.class final enum Landroid/text/TextDirectionHeuristics$TriState;
.super Ljava/lang/Enum;
.source "TextDirectionHeuristics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/TextDirectionHeuristics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TriState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/text/TextDirectionHeuristics$TriState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/text/TextDirectionHeuristics$TriState;

.field public static final enum FALSE:Landroid/text/TextDirectionHeuristics$TriState;

.field public static final enum TRUE:Landroid/text/TextDirectionHeuristics$TriState;

.field public static final enum UNKNOWN:Landroid/text/TextDirectionHeuristics$TriState;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 69
    new-instance v0, Landroid/text/TextDirectionHeuristics$TriState;

    #@5
    const-string v1, "TRUE"

    #@7
    invoke-direct {v0, v1, v2}, Landroid/text/TextDirectionHeuristics$TriState;-><init>(Ljava/lang/String;I)V

    #@a
    sput-object v0, Landroid/text/TextDirectionHeuristics$TriState;->TRUE:Landroid/text/TextDirectionHeuristics$TriState;

    #@c
    new-instance v0, Landroid/text/TextDirectionHeuristics$TriState;

    #@e
    const-string v1, "FALSE"

    #@10
    invoke-direct {v0, v1, v3}, Landroid/text/TextDirectionHeuristics$TriState;-><init>(Ljava/lang/String;I)V

    #@13
    sput-object v0, Landroid/text/TextDirectionHeuristics$TriState;->FALSE:Landroid/text/TextDirectionHeuristics$TriState;

    #@15
    new-instance v0, Landroid/text/TextDirectionHeuristics$TriState;

    #@17
    const-string v1, "UNKNOWN"

    #@19
    invoke-direct {v0, v1, v4}, Landroid/text/TextDirectionHeuristics$TriState;-><init>(Ljava/lang/String;I)V

    #@1c
    sput-object v0, Landroid/text/TextDirectionHeuristics$TriState;->UNKNOWN:Landroid/text/TextDirectionHeuristics$TriState;

    #@1e
    .line 68
    const/4 v0, 0x3

    #@1f
    new-array v0, v0, [Landroid/text/TextDirectionHeuristics$TriState;

    #@21
    sget-object v1, Landroid/text/TextDirectionHeuristics$TriState;->TRUE:Landroid/text/TextDirectionHeuristics$TriState;

    #@23
    aput-object v1, v0, v2

    #@25
    sget-object v1, Landroid/text/TextDirectionHeuristics$TriState;->FALSE:Landroid/text/TextDirectionHeuristics$TriState;

    #@27
    aput-object v1, v0, v3

    #@29
    sget-object v1, Landroid/text/TextDirectionHeuristics$TriState;->UNKNOWN:Landroid/text/TextDirectionHeuristics$TriState;

    #@2b
    aput-object v1, v0, v4

    #@2d
    sput-object v0, Landroid/text/TextDirectionHeuristics$TriState;->$VALUES:[Landroid/text/TextDirectionHeuristics$TriState;

    #@2f
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/text/TextDirectionHeuristics$TriState;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 68
    const-class v0, Landroid/text/TextDirectionHeuristics$TriState;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/text/TextDirectionHeuristics$TriState;

    #@8
    return-object v0
.end method

.method public static values()[Landroid/text/TextDirectionHeuristics$TriState;
    .registers 1

    #@0
    .prologue
    .line 68
    sget-object v0, Landroid/text/TextDirectionHeuristics$TriState;->$VALUES:[Landroid/text/TextDirectionHeuristics$TriState;

    #@2
    invoke-virtual {v0}, [Landroid/text/TextDirectionHeuristics$TriState;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Landroid/text/TextDirectionHeuristics$TriState;

    #@8
    return-object v0
.end method
