.class public Landroid/text/StaticLayout;
.super Landroid/text/Layout;
.source "StaticLayout.java"


# static fields
.field private static final CHAR_FIRST_CJK:C = '\u2e80'

.field private static final CHAR_FIRST_HIGH_SURROGATE:I = 0xd800

.field private static final CHAR_HYPHEN:C = '-'

.field private static final CHAR_LAST_LOW_SURROGATE:I = 0xdfff

.field private static final CHAR_NEW_LINE:C = '\n'

.field private static final CHAR_SLASH:C = '/'

.field private static final CHAR_SPACE:C = ' '

.field private static final CHAR_TAB:C = '\t'

.field private static final COLUMNS_ELLIPSIZE:I = 0x5

.field private static final COLUMNS_NORMAL:I = 0x3

.field private static final DESCENT:I = 0x2

.field private static final DIR:I = 0x0

.field private static final DIR_SHIFT:I = 0x1e

.field private static final ELLIPSIS_COUNT:I = 0x4

.field private static final ELLIPSIS_START:I = 0x3

.field private static final EXTRA_ROUNDING:D = 0.5

.field private static final START:I = 0x0

.field private static final START_MASK:I = 0x1fffffff

.field private static final TAB:I = 0x0

.field private static final TAB_INCREMENT:I = 0x14

.field private static final TAB_MASK:I = 0x20000000

.field static final TAG:Ljava/lang/String; = "StaticLayout"

.field private static final TOP:I = 0x1


# instance fields
.field private mBottomPadding:I

.field private mColumns:I

.field private mEllipsizedWidth:I

.field private mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

.field private mLineCount:I

.field private mLineDirections:[Landroid/text/Layout$Directions;

.field private mLines:[I

.field private mMaximumVisibleLineCount:I

.field private mMeasured:Landroid/text/MeasuredText;

.field private mTopPadding:I


# direct methods
.method constructor <init>(Ljava/lang/CharSequence;)V
    .registers 9
    .parameter "text"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    .line 160
    const/4 v3, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v1, p1

    #@5
    move-object v4, v2

    #@6
    move v6, v5

    #@7
    invoke-direct/range {v0 .. v6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    #@a
    .line 1094
    const v0, 0x7fffffff

    #@d
    iput v0, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@f
    .line 1119
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    #@11
    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    #@14
    iput-object v0, p0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    #@16
    .line 162
    const/4 v0, 0x5

    #@17
    iput v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@19
    .line 163
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@1b
    mul-int/lit8 v0, v0, 0x2

    #@1d
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@20
    move-result v0

    #@21
    new-array v0, v0, [I

    #@23
    iput-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@25
    .line 164
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@27
    mul-int/lit8 v0, v0, 0x2

    #@29
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@2c
    move-result v0

    #@2d
    new-array v0, v0, [Landroid/text/Layout$Directions;

    #@2f
    iput-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@31
    .line 166
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@34
    move-result-object v0

    #@35
    iput-object v0, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@37
    .line 167
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@39
    if-eqz v0, :cond_47

    #@3b
    .line 168
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@3d
    mul-int/lit8 v0, v0, 0x2

    #@3f
    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->idealBooleanArraySize(I)I

    #@42
    move-result v0

    #@43
    new-array v0, v0, [Z

    #@45
    iput-object v0, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@47
    .line 169
    :cond_47
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 22
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 76
    const/4 v10, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    move-object v0, p0

    #@3
    move-object v1, p1

    #@4
    move v2, p2

    #@5
    move v3, p3

    #@6
    move-object/from16 v4, p4

    #@8
    move/from16 v5, p5

    #@a
    move-object/from16 v6, p6

    #@c
    move/from16 v7, p7

    #@e
    move/from16 v8, p8

    #@10
    move/from16 v9, p9

    #@12
    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    #@15
    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 26
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"

    #@0
    .prologue
    .line 98
    sget-object v7, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    #@2
    const v13, 0x7fffffff

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move/from16 v2, p2

    #@9
    move/from16 v3, p3

    #@b
    move-object/from16 v4, p4

    #@d
    move/from16 v5, p5

    #@f
    move-object/from16 v6, p6

    #@11
    move/from16 v8, p7

    #@13
    move/from16 v9, p8

    #@15
    move/from16 v10, p9

    #@17
    move-object/from16 v11, p10

    #@19
    move/from16 v12, p11

    #@1b
    invoke-direct/range {v0 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@1e
    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V
    .registers 25
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "textDir"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 88
    const/4 v11, 0x0

    #@1
    const/4 v12, 0x0

    #@2
    const v13, 0x7fffffff

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move/from16 v2, p2

    #@9
    move/from16 v3, p3

    #@b
    move-object/from16 v4, p4

    #@d
    move/from16 v5, p5

    #@f
    move-object/from16 v6, p6

    #@11
    move-object/from16 v7, p7

    #@13
    move/from16 v8, p8

    #@15
    move/from16 v9, p9

    #@17
    move/from16 v10, p10

    #@19
    invoke-direct/range {v0 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    #@1c
    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V
    .registers 29
    .parameter "source"
    .parameter "bufstart"
    .parameter "bufend"
    .parameter "paint"
    .parameter "outerwidth"
    .parameter "align"
    .parameter "textDir"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "ellipsize"
    .parameter "ellipsizedWidth"
    .parameter "maxLines"

    #@0
    .prologue
    .line 112
    if-nez p11, :cond_94

    #@2
    move-object/from16 v2, p1

    #@4
    :goto_4
    move-object v1, p0

    #@5
    move-object/from16 v3, p4

    #@7
    move/from16 v4, p5

    #@9
    move-object/from16 v5, p6

    #@b
    move-object/from16 v6, p7

    #@d
    move/from16 v7, p8

    #@f
    move/from16 v8, p9

    #@11
    invoke-direct/range {v1 .. v8}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V

    #@14
    .line 1094
    const v1, 0x7fffffff

    #@17
    iput v1, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@19
    .line 1119
    new-instance v1, Landroid/graphics/Paint$FontMetricsInt;

    #@1b
    invoke-direct {v1}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    #@1e
    iput-object v1, p0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    #@20
    .line 127
    if-eqz p11, :cond_ac

    #@22
    .line 128
    invoke-virtual {p0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    #@25
    move-result-object v14

    #@26
    check-cast v14, Landroid/text/Layout$Ellipsizer;

    #@28
    .line 130
    .local v14, e:Landroid/text/Layout$Ellipsizer;
    iput-object p0, v14, Landroid/text/Layout$Ellipsizer;->mLayout:Landroid/text/Layout;

    #@2a
    .line 131
    move/from16 v0, p12

    #@2c
    iput v0, v14, Landroid/text/Layout$Ellipsizer;->mWidth:I

    #@2e
    .line 132
    move-object/from16 v0, p11

    #@30
    iput-object v0, v14, Landroid/text/Layout$Ellipsizer;->mMethod:Landroid/text/TextUtils$TruncateAt;

    #@32
    .line 133
    move/from16 v0, p12

    #@34
    iput v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    #@36
    .line 135
    const/4 v1, 0x5

    #@37
    iput v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@39
    .line 141
    .end local v14           #e:Landroid/text/Layout$Ellipsizer;
    :goto_39
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@3b
    mul-int/lit8 v1, v1, 0x2

    #@3d
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@40
    move-result v1

    #@41
    new-array v1, v1, [I

    #@43
    iput-object v1, p0, Landroid/text/StaticLayout;->mLines:[I

    #@45
    .line 142
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@47
    mul-int/lit8 v1, v1, 0x2

    #@49
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@4c
    move-result v1

    #@4d
    new-array v1, v1, [Landroid/text/Layout$Directions;

    #@4f
    iput-object v1, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@51
    .line 144
    move/from16 v0, p13

    #@53
    iput v0, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@55
    .line 146
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@58
    move-result-object v1

    #@59
    iput-object v1, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@5b
    .line 148
    sget-boolean v1, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@5d
    if-eqz v1, :cond_6b

    #@5f
    .line 149
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@61
    mul-int/lit8 v1, v1, 0x2

    #@63
    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->idealBooleanArraySize(I)I

    #@66
    move-result v1

    #@67
    new-array v1, v1, [Z

    #@69
    iput-object v1, p0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@6b
    .line 151
    :cond_6b
    move/from16 v0, p12

    #@6d
    int-to-float v12, v0

    #@6e
    move-object v1, p0

    #@6f
    move-object/from16 v2, p1

    #@71
    move/from16 v3, p2

    #@73
    move/from16 v4, p3

    #@75
    move-object/from16 v5, p4

    #@77
    move/from16 v6, p5

    #@79
    move-object/from16 v7, p7

    #@7b
    move/from16 v8, p8

    #@7d
    move/from16 v9, p9

    #@7f
    move/from16 v10, p10

    #@81
    move/from16 v11, p10

    #@83
    move-object/from16 v13, p11

    #@85
    invoke-virtual/range {v1 .. v13}, Landroid/text/StaticLayout;->generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/TextDirectionHeuristic;FFZZFLandroid/text/TextUtils$TruncateAt;)V

    #@88
    .line 155
    iget-object v1, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@8a
    invoke-static {v1}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@8d
    move-result-object v1

    #@8e
    iput-object v1, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@90
    .line 156
    const/4 v1, 0x0

    #@91
    iput-object v1, p0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    #@93
    .line 157
    return-void

    #@94
    .line 112
    :cond_94
    move-object/from16 v0, p1

    #@96
    instance-of v1, v0, Landroid/text/Spanned;

    #@98
    if-eqz v1, :cond_a3

    #@9a
    new-instance v2, Landroid/text/Layout$SpannedEllipsizer;

    #@9c
    move-object/from16 v0, p1

    #@9e
    invoke-direct {v2, v0}, Landroid/text/Layout$SpannedEllipsizer;-><init>(Ljava/lang/CharSequence;)V

    #@a1
    goto/16 :goto_4

    #@a3
    :cond_a3
    new-instance v2, Landroid/text/Layout$Ellipsizer;

    #@a5
    move-object/from16 v0, p1

    #@a7
    invoke-direct {v2, v0}, Landroid/text/Layout$Ellipsizer;-><init>(Ljava/lang/CharSequence;)V

    #@aa
    goto/16 :goto_4

    #@ac
    .line 137
    :cond_ac
    const/4 v1, 0x3

    #@ad
    iput v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@af
    .line 138
    move/from16 v0, p5

    #@b1
    iput v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    #@b3
    goto :goto_39
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 18
    .parameter "source"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 56
    const/4 v2, 0x0

    #@1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v3

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v4, p2

    #@8
    move v5, p3

    #@9
    move-object v6, p4

    #@a
    move v7, p5

    #@b
    move/from16 v8, p6

    #@d
    move/from16 v9, p7

    #@f
    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    #@12
    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V
    .registers 20
    .parameter "source"
    .parameter "paint"
    .parameter "width"
    .parameter "align"
    .parameter "textDir"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"

    #@0
    .prologue
    .line 67
    const/4 v2, 0x0

    #@1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    #@4
    move-result v3

    #@5
    move-object v0, p0

    #@6
    move-object v1, p1

    #@7
    move-object v4, p2

    #@8
    move v5, p3

    #@9
    move-object v6, p4

    #@a
    move-object/from16 v7, p5

    #@c
    move/from16 v8, p6

    #@e
    move/from16 v9, p7

    #@10
    move/from16 v10, p8

    #@12
    invoke-direct/range {v0 .. v10}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZ)V

    #@15
    .line 69
    return-void
.end method

.method private calculateEllipsis(II[FIFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;Z)V
    .registers 31
    .parameter "lineStart"
    .parameter "lineEnd"
    .parameter "widths"
    .parameter "widthStart"
    .parameter "avail"
    .parameter "where"
    .parameter "line"
    .parameter "textWidth"
    .parameter "paint"
    .parameter "forceEllipsis"

    #@0
    .prologue
    .line 867
    cmpg-float v17, p8, p5

    #@2
    if-gtz v17, :cond_2f

    #@4
    if-nez p10, :cond_2f

    #@6
    .line 869
    move-object/from16 v0, p0

    #@8
    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    #@a
    move-object/from16 v17, v0

    #@c
    move-object/from16 v0, p0

    #@e
    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    #@10
    move/from16 v18, v0

    #@12
    mul-int v18, v18, p7

    #@14
    add-int/lit8 v18, v18, 0x3

    #@16
    const/16 v19, 0x0

    #@18
    aput v19, v17, v18

    #@1a
    .line 870
    move-object/from16 v0, p0

    #@1c
    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    #@1e
    move-object/from16 v17, v0

    #@20
    move-object/from16 v0, p0

    #@22
    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    #@24
    move/from16 v18, v0

    #@26
    mul-int v18, v18, p7

    #@28
    add-int/lit8 v18, v18, 0x4

    #@2a
    const/16 v19, 0x0

    #@2c
    aput v19, v17, v18

    #@2e
    .line 964
    :goto_2e
    return-void

    #@2f
    .line 874
    :cond_2f
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@31
    move-object/from16 v0, p6

    #@33
    move-object/from16 v1, v17

    #@35
    if-ne v0, v1, :cond_9e

    #@37
    sget-object v17, Landroid/text/StaticLayout;->ELLIPSIS_TWO_DOTS:[C

    #@39
    :goto_39
    const/16 v18, 0x0

    #@3b
    const/16 v19, 0x1

    #@3d
    move-object/from16 v0, p9

    #@3f
    move-object/from16 v1, v17

    #@41
    move/from16 v2, v18

    #@43
    move/from16 v3, v19

    #@45
    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText([CII)F

    #@48
    move-result v6

    #@49
    .line 877
    .local v6, ellipsisWidth:F
    const/4 v5, 0x0

    #@4a
    .line 878
    .local v5, ellipsisStart:I
    const/4 v4, 0x0

    #@4b
    .line 879
    .local v4, ellipsisCount:I
    sub-int v10, p2, p1

    #@4d
    .line 882
    .local v10, len:I
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    #@4f
    move-object/from16 v0, p6

    #@51
    move-object/from16 v1, v17

    #@53
    if-ne v0, v1, :cond_b8

    #@55
    .line 883
    move-object/from16 v0, p0

    #@57
    iget v0, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@59
    move/from16 v17, v0

    #@5b
    const/16 v18, 0x1

    #@5d
    move/from16 v0, v17

    #@5f
    move/from16 v1, v18

    #@61
    if-ne v0, v1, :cond_a6

    #@63
    .line 884
    const/4 v15, 0x0

    #@64
    .line 887
    .local v15, sum:F
    move v7, v10

    #@65
    .local v7, i:I
    :goto_65
    if-ltz v7, :cond_77

    #@67
    .line 888
    add-int/lit8 v17, v7, -0x1

    #@69
    add-int v17, v17, p1

    #@6b
    sub-int v17, v17, p4

    #@6d
    aget v16, p3, v17

    #@6f
    .line 890
    .local v16, w:F
    add-float v17, v16, v15

    #@71
    add-float v17, v17, v6

    #@73
    cmpl-float v17, v17, p5

    #@75
    if-lez v17, :cond_a1

    #@77
    .line 897
    .end local v16           #w:F
    :cond_77
    const/4 v5, 0x0

    #@78
    .line 898
    move v4, v7

    #@79
    .line 962
    .end local v7           #i:I
    .end local v15           #sum:F
    :cond_79
    :goto_79
    move-object/from16 v0, p0

    #@7b
    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    #@7d
    move-object/from16 v17, v0

    #@7f
    move-object/from16 v0, p0

    #@81
    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    #@83
    move/from16 v18, v0

    #@85
    mul-int v18, v18, p7

    #@87
    add-int/lit8 v18, v18, 0x3

    #@89
    aput v5, v17, v18

    #@8b
    .line 963
    move-object/from16 v0, p0

    #@8d
    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    #@8f
    move-object/from16 v17, v0

    #@91
    move-object/from16 v0, p0

    #@93
    iget v0, v0, Landroid/text/StaticLayout;->mColumns:I

    #@95
    move/from16 v18, v0

    #@97
    mul-int v18, v18, p7

    #@99
    add-int/lit8 v18, v18, 0x4

    #@9b
    aput v4, v17, v18

    #@9d
    goto :goto_2e

    #@9e
    .line 874
    .end local v4           #ellipsisCount:I
    .end local v5           #ellipsisStart:I
    .end local v6           #ellipsisWidth:F
    .end local v10           #len:I
    :cond_9e
    sget-object v17, Landroid/text/StaticLayout;->ELLIPSIS_NORMAL:[C

    #@a0
    goto :goto_39

    #@a1
    .line 894
    .restart local v4       #ellipsisCount:I
    .restart local v5       #ellipsisStart:I
    .restart local v6       #ellipsisWidth:F
    .restart local v7       #i:I
    .restart local v10       #len:I
    .restart local v15       #sum:F
    .restart local v16       #w:F
    :cond_a1
    add-float v15, v15, v16

    #@a3
    .line 887
    add-int/lit8 v7, v7, -0x1

    #@a5
    goto :goto_65

    #@a6
    .line 900
    .end local v7           #i:I
    .end local v15           #sum:F
    .end local v16           #w:F
    :cond_a6
    const-string v17, "StaticLayout"

    #@a8
    const/16 v18, 0x5

    #@aa
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@ad
    move-result v17

    #@ae
    if-eqz v17, :cond_79

    #@b0
    .line 901
    const-string v17, "StaticLayout"

    #@b2
    const-string v18, "Start Ellipsis only supported with one line"

    #@b4
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    goto :goto_79

    #@b8
    .line 904
    :cond_b8
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    #@ba
    move-object/from16 v0, p6

    #@bc
    move-object/from16 v1, v17

    #@be
    if-eq v0, v1, :cond_d0

    #@c0
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@c2
    move-object/from16 v0, p6

    #@c4
    move-object/from16 v1, v17

    #@c6
    if-eq v0, v1, :cond_d0

    #@c8
    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    #@ca
    move-object/from16 v0, p6

    #@cc
    move-object/from16 v1, v17

    #@ce
    if-ne v0, v1, :cond_f4

    #@d0
    .line 906
    :cond_d0
    const/4 v15, 0x0

    #@d1
    .line 909
    .restart local v15       #sum:F
    const/4 v7, 0x0

    #@d2
    .restart local v7       #i:I
    :goto_d2
    if-ge v7, v10, :cond_e2

    #@d4
    .line 910
    add-int v17, v7, p1

    #@d6
    sub-int v17, v17, p4

    #@d8
    aget v16, p3, v17

    #@da
    .line 912
    .restart local v16       #w:F
    add-float v17, v16, v15

    #@dc
    add-float v17, v17, v6

    #@de
    cmpl-float v17, v17, p5

    #@e0
    if-lez v17, :cond_ef

    #@e2
    .line 919
    .end local v16           #w:F
    :cond_e2
    move v5, v7

    #@e3
    .line 920
    sub-int v4, v10, v7

    #@e5
    .line 921
    if-eqz p10, :cond_79

    #@e7
    if-nez v4, :cond_79

    #@e9
    if-lez v10, :cond_79

    #@eb
    .line 922
    add-int/lit8 v5, v10, -0x1

    #@ed
    .line 923
    const/4 v4, 0x1

    #@ee
    goto :goto_79

    #@ef
    .line 916
    .restart local v16       #w:F
    :cond_ef
    add-float v15, v15, v16

    #@f1
    .line 909
    add-int/lit8 v7, v7, 0x1

    #@f3
    goto :goto_d2

    #@f4
    .line 927
    .end local v7           #i:I
    .end local v15           #sum:F
    .end local v16           #w:F
    :cond_f4
    move-object/from16 v0, p0

    #@f6
    iget v0, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@f8
    move/from16 v17, v0

    #@fa
    const/16 v18, 0x1

    #@fc
    move/from16 v0, v17

    #@fe
    move/from16 v1, v18

    #@100
    if-ne v0, v1, :cond_13f

    #@102
    .line 928
    const/4 v11, 0x0

    #@103
    .local v11, lsum:F
    const/4 v14, 0x0

    #@104
    .line 929
    .local v14, rsum:F
    const/4 v9, 0x0

    #@105
    .local v9, left:I
    move v13, v10

    #@106
    .line 931
    .local v13, right:I
    sub-float v17, p5, v6

    #@108
    const/high16 v18, 0x4000

    #@10a
    div-float v12, v17, v18

    #@10c
    .line 932
    .local v12, ravail:F
    move v13, v10

    #@10d
    :goto_10d
    if-ltz v13, :cond_11d

    #@10f
    .line 933
    add-int/lit8 v17, v13, -0x1

    #@111
    add-int v17, v17, p1

    #@113
    sub-int v17, v17, p4

    #@115
    aget v16, p3, v17

    #@117
    .line 935
    .restart local v16       #w:F
    add-float v17, v16, v14

    #@119
    cmpl-float v17, v17, v12

    #@11b
    if-lez v17, :cond_135

    #@11d
    .line 942
    .end local v16           #w:F
    :cond_11d
    sub-float v17, p5, v6

    #@11f
    sub-float v8, v17, v14

    #@121
    .line 943
    .local v8, lavail:F
    const/4 v9, 0x0

    #@122
    :goto_122
    if-ge v9, v13, :cond_130

    #@124
    .line 944
    add-int v17, v9, p1

    #@126
    sub-int v17, v17, p4

    #@128
    aget v16, p3, v17

    #@12a
    .line 946
    .restart local v16       #w:F
    add-float v17, v16, v11

    #@12c
    cmpl-float v17, v17, v8

    #@12e
    if-lez v17, :cond_13a

    #@130
    .line 953
    .end local v16           #w:F
    :cond_130
    move v5, v9

    #@131
    .line 954
    sub-int v4, v13, v9

    #@133
    .line 955
    goto/16 :goto_79

    #@135
    .line 939
    .end local v8           #lavail:F
    .restart local v16       #w:F
    :cond_135
    add-float v14, v14, v16

    #@137
    .line 932
    add-int/lit8 v13, v13, -0x1

    #@139
    goto :goto_10d

    #@13a
    .line 950
    .restart local v8       #lavail:F
    :cond_13a
    add-float v11, v11, v16

    #@13c
    .line 943
    add-int/lit8 v9, v9, 0x1

    #@13e
    goto :goto_122

    #@13f
    .line 956
    .end local v8           #lavail:F
    .end local v9           #left:I
    .end local v11           #lsum:F
    .end local v12           #ravail:F
    .end local v13           #right:I
    .end local v14           #rsum:F
    .end local v16           #w:F
    :cond_13f
    const-string v17, "StaticLayout"

    #@141
    const/16 v18, 0x5

    #@143
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    #@146
    move-result v17

    #@147
    if-eqz v17, :cond_79

    #@149
    .line 957
    const-string v17, "StaticLayout"

    #@14b
    const-string v18, "Middle Ellipsis only supported with one line"

    #@14d
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@150
    goto/16 :goto_79
.end method

.method private static final isIdeographic(CZ)Z
    .registers 5
    .parameter "c"
    .parameter "includeNonStarters"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 653
    const/16 v2, 0x2e80

    #@4
    if-lt p0, v2, :cond_b

    #@6
    const/16 v2, 0x2fff

    #@8
    if-gt p0, v2, :cond_b

    #@a
    .line 730
    :cond_a
    :goto_a
    return v0

    #@b
    .line 656
    :cond_b
    const/16 v2, 0x3000

    #@d
    if-eq p0, v2, :cond_a

    #@f
    .line 659
    const/16 v2, 0x3040

    #@11
    if-lt p0, v2, :cond_1f

    #@13
    const/16 v2, 0x309f

    #@15
    if-gt p0, v2, :cond_1f

    #@17
    .line 660
    if-nez p1, :cond_a

    #@19
    .line 661
    sparse-switch p0, :sswitch_data_74

    #@1c
    goto :goto_a

    #@1d
    :sswitch_1d
    move v0, v1

    #@1e
    .line 678
    goto :goto_a

    #@1f
    .line 683
    :cond_1f
    const/16 v2, 0x30a0

    #@21
    if-lt p0, v2, :cond_2f

    #@23
    const/16 v2, 0x30ff

    #@25
    if-gt p0, v2, :cond_2f

    #@27
    .line 684
    if-nez p1, :cond_a

    #@29
    .line 685
    sparse-switch p0, :sswitch_data_b6

    #@2c
    goto :goto_a

    #@2d
    :sswitch_2d
    move v0, v1

    #@2e
    .line 703
    goto :goto_a

    #@2f
    .line 708
    :cond_2f
    const/16 v2, 0x3400

    #@31
    if-lt p0, v2, :cond_37

    #@33
    const/16 v2, 0x4db5

    #@35
    if-le p0, v2, :cond_a

    #@37
    .line 711
    :cond_37
    const/16 v2, 0x4e00

    #@39
    if-lt p0, v2, :cond_40

    #@3b
    const v2, 0x9fbb

    #@3e
    if-le p0, v2, :cond_a

    #@40
    .line 714
    :cond_40
    const v2, 0xf900

    #@43
    if-lt p0, v2, :cond_4a

    #@45
    const v2, 0xfad9

    #@48
    if-le p0, v2, :cond_a

    #@4a
    .line 717
    :cond_4a
    const v2, 0xa000

    #@4d
    if-lt p0, v2, :cond_54

    #@4f
    const v2, 0xa48f

    #@52
    if-le p0, v2, :cond_a

    #@54
    .line 720
    :cond_54
    const v2, 0xa490

    #@57
    if-lt p0, v2, :cond_5e

    #@59
    const v2, 0xa4cf

    #@5c
    if-le p0, v2, :cond_a

    #@5e
    .line 723
    :cond_5e
    const v2, 0xfe62

    #@61
    if-lt p0, v2, :cond_68

    #@63
    const v2, 0xfe66

    #@66
    if-le p0, v2, :cond_a

    #@68
    .line 726
    :cond_68
    const v2, 0xff10

    #@6b
    if-lt p0, v2, :cond_72

    #@6d
    const v2, 0xff19

    #@70
    if-le p0, v2, :cond_a

    #@72
    :cond_72
    move v0, v1

    #@73
    .line 730
    goto :goto_a

    #@74
    .line 661
    :sswitch_data_74
    .sparse-switch
        0x3041 -> :sswitch_1d
        0x3043 -> :sswitch_1d
        0x3045 -> :sswitch_1d
        0x3047 -> :sswitch_1d
        0x3049 -> :sswitch_1d
        0x3063 -> :sswitch_1d
        0x3083 -> :sswitch_1d
        0x3085 -> :sswitch_1d
        0x3087 -> :sswitch_1d
        0x308e -> :sswitch_1d
        0x3095 -> :sswitch_1d
        0x3096 -> :sswitch_1d
        0x309b -> :sswitch_1d
        0x309c -> :sswitch_1d
        0x309d -> :sswitch_1d
        0x309e -> :sswitch_1d
    .end sparse-switch

    #@b6
    .line 685
    :sswitch_data_b6
    .sparse-switch
        0x30a0 -> :sswitch_2d
        0x30a1 -> :sswitch_2d
        0x30a3 -> :sswitch_2d
        0x30a5 -> :sswitch_2d
        0x30a7 -> :sswitch_2d
        0x30a9 -> :sswitch_2d
        0x30c3 -> :sswitch_2d
        0x30e3 -> :sswitch_2d
        0x30e5 -> :sswitch_2d
        0x30e7 -> :sswitch_2d
        0x30ee -> :sswitch_2d
        0x30f5 -> :sswitch_2d
        0x30f6 -> :sswitch_2d
        0x30fb -> :sswitch_2d
        0x30fc -> :sswitch_2d
        0x30fd -> :sswitch_2d
        0x30fe -> :sswitch_2d
    .end sparse-switch
.end method

.method private out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZ[BIZIZZ[C[FILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I
    .registers 58
    .parameter "text"
    .parameter "start"
    .parameter "end"
    .parameter "above"
    .parameter "below"
    .parameter "top"
    .parameter "bottom"
    .parameter "v"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "chooseHt"
    .parameter "chooseHtv"
    .parameter "fm"
    .parameter "hasTabOrEmoji"
    .parameter "needMultiply"
    .parameter "chdirs"
    .parameter "dir"
    .parameter "easy"
    .parameter "bufEnd"
    .parameter "includePad"
    .parameter "trackPad"
    .parameter "chs"
    .parameter "widths"
    .parameter "widthStart"
    .parameter "ellipsize"
    .parameter "ellipsisWidth"
    .parameter "textWidth"
    .parameter "paint"
    .parameter "moreChars"

    #@0
    .prologue
    .line 744
    move-object/from16 v0, p0

    #@2
    iget v0, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@4
    move/from16 v22, v0

    #@6
    .line 745
    .local v22, j:I
    move-object/from16 v0, p0

    #@8
    iget v2, v0, Landroid/text/StaticLayout;->mColumns:I

    #@a
    mul-int v26, v22, v2

    #@c
    .line 746
    .local v26, off:I
    move-object/from16 v0, p0

    #@e
    iget v2, v0, Landroid/text/StaticLayout;->mColumns:I

    #@10
    add-int v2, v2, v26

    #@12
    add-int/lit8 v27, v2, 0x1

    #@14
    .line 747
    .local v27, want:I
    move-object/from16 v0, p0

    #@16
    iget-object v0, v0, Landroid/text/StaticLayout;->mLines:[I

    #@18
    move-object/from16 v24, v0

    #@1a
    .line 749
    .local v24, lines:[I
    move-object/from16 v0, v24

    #@1c
    array-length v2, v0

    #@1d
    move/from16 v0, v27

    #@1f
    if-lt v0, v2, :cond_5d

    #@21
    .line 750
    add-int/lit8 v2, v27, 0x1

    #@23
    invoke-static {v2}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@26
    move-result v25

    #@27
    .line 751
    .local v25, nlen:I
    move/from16 v0, v25

    #@29
    new-array v0, v0, [I

    #@2b
    move-object/from16 v19, v0

    #@2d
    .line 752
    .local v19, grow:[I
    const/4 v2, 0x0

    #@2e
    const/4 v3, 0x0

    #@2f
    move-object/from16 v0, v24

    #@31
    array-length v4, v0

    #@32
    move-object/from16 v0, v24

    #@34
    move-object/from16 v1, v19

    #@36
    invoke-static {v0, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@39
    .line 753
    move-object/from16 v0, v19

    #@3b
    move-object/from16 v1, p0

    #@3d
    iput-object v0, v1, Landroid/text/StaticLayout;->mLines:[I

    #@3f
    .line 754
    move-object/from16 v24, v19

    #@41
    .line 756
    move/from16 v0, v25

    #@43
    new-array v0, v0, [Landroid/text/Layout$Directions;

    #@45
    move-object/from16 v20, v0

    #@47
    .line 757
    .local v20, grow2:[Landroid/text/Layout$Directions;
    move-object/from16 v0, p0

    #@49
    iget-object v2, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@4b
    const/4 v3, 0x0

    #@4c
    const/4 v4, 0x0

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v5, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@51
    array-length v5, v5

    #@52
    move-object/from16 v0, v20

    #@54
    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@57
    .line 759
    move-object/from16 v0, v20

    #@59
    move-object/from16 v1, p0

    #@5b
    iput-object v0, v1, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@5d
    .line 762
    .end local v19           #grow:[I
    .end local v20           #grow2:[Landroid/text/Layout$Directions;
    .end local v25           #nlen:I
    :cond_5d
    if-eqz p11, :cond_c8

    #@5f
    .line 763
    move/from16 v0, p4

    #@61
    move-object/from16 v1, p13

    #@63
    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@65
    .line 764
    move/from16 v0, p5

    #@67
    move-object/from16 v1, p13

    #@69
    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@6b
    .line 765
    move/from16 v0, p6

    #@6d
    move-object/from16 v1, p13

    #@6f
    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@71
    .line 766
    move/from16 v0, p7

    #@73
    move-object/from16 v1, p13

    #@75
    iput v0, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@77
    .line 768
    const/16 v21, 0x0

    #@79
    .local v21, i:I
    :goto_79
    move-object/from16 v0, p11

    #@7b
    array-length v2, v0

    #@7c
    move/from16 v0, v21

    #@7e
    if-ge v0, v2, :cond_b0

    #@80
    .line 769
    aget-object v2, p11, v21

    #@82
    instance-of v2, v2, Landroid/text/style/LineHeightSpan$WithDensity;

    #@84
    if-eqz v2, :cond_9e

    #@86
    .line 770
    aget-object v2, p11, v21

    #@88
    check-cast v2, Landroid/text/style/LineHeightSpan$WithDensity;

    #@8a
    aget v6, p12, v21

    #@8c
    move-object/from16 v3, p1

    #@8e
    move/from16 v4, p2

    #@90
    move/from16 v5, p3

    #@92
    move/from16 v7, p8

    #@94
    move-object/from16 v8, p13

    #@96
    move-object/from16 v9, p28

    #@98
    invoke-interface/range {v2 .. v9}, Landroid/text/style/LineHeightSpan$WithDensity;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    #@9b
    .line 768
    :goto_9b
    add-int/lit8 v21, v21, 0x1

    #@9d
    goto :goto_79

    #@9e
    .line 774
    :cond_9e
    aget-object v2, p11, v21

    #@a0
    aget v6, p12, v21

    #@a2
    move-object/from16 v3, p1

    #@a4
    move/from16 v4, p2

    #@a6
    move/from16 v5, p3

    #@a8
    move/from16 v7, p8

    #@aa
    move-object/from16 v8, p13

    #@ac
    invoke-interface/range {v2 .. v8}, Landroid/text/style/LineHeightSpan;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V

    #@af
    goto :goto_9b

    #@b0
    .line 778
    :cond_b0
    move-object/from16 v0, p13

    #@b2
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@b4
    move/from16 p4, v0

    #@b6
    .line 779
    move-object/from16 v0, p13

    #@b8
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@ba
    move/from16 p5, v0

    #@bc
    .line 780
    move-object/from16 v0, p13

    #@be
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@c0
    move/from16 p6, v0

    #@c2
    .line 781
    move-object/from16 v0, p13

    #@c4
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@c6
    move/from16 p7, v0

    #@c8
    .line 784
    .end local v21           #i:I
    :cond_c8
    if-nez v22, :cond_d6

    #@ca
    .line 785
    if-eqz p21, :cond_d2

    #@cc
    .line 786
    sub-int v2, p6, p4

    #@ce
    move-object/from16 v0, p0

    #@d0
    iput v2, v0, Landroid/text/StaticLayout;->mTopPadding:I

    #@d2
    .line 789
    :cond_d2
    if-eqz p20, :cond_d6

    #@d4
    .line 790
    move/from16 p4, p6

    #@d6
    .line 793
    :cond_d6
    move/from16 v0, p3

    #@d8
    move/from16 v1, p19

    #@da
    if-ne v0, v1, :cond_e8

    #@dc
    .line 794
    if-eqz p21, :cond_e4

    #@de
    .line 795
    sub-int v2, p7, p5

    #@e0
    move-object/from16 v0, p0

    #@e2
    iput v2, v0, Landroid/text/StaticLayout;->mBottomPadding:I

    #@e4
    .line 798
    :cond_e4
    if-eqz p20, :cond_e8

    #@e6
    .line 799
    move/from16 p5, p7

    #@e8
    .line 805
    :cond_e8
    if-eqz p15, :cond_1b3

    #@ea
    .line 806
    sub-int v2, p5, p4

    #@ec
    int-to-float v2, v2

    #@ed
    const/high16 v3, 0x3f80

    #@ef
    sub-float v3, p9, v3

    #@f1
    mul-float/2addr v2, v3

    #@f2
    add-float v2, v2, p10

    #@f4
    float-to-double v15, v2

    #@f5
    .line 807
    .local v15, ex:D
    const-wide/16 v2, 0x0

    #@f7
    cmpl-double v2, v15, v2

    #@f9
    if-ltz v2, :cond_1a9

    #@fb
    .line 808
    const-wide/high16 v2, 0x3fe0

    #@fd
    add-double/2addr v2, v15

    #@fe
    double-to-int v0, v2

    #@ff
    move/from16 v17, v0

    #@101
    .line 816
    .end local v15           #ex:D
    .local v17, extra:I
    :goto_101
    add-int/lit8 v2, v26, 0x0

    #@103
    aput p2, v24, v2

    #@105
    .line 817
    add-int/lit8 v2, v26, 0x1

    #@107
    aput p8, v24, v2

    #@109
    .line 818
    add-int/lit8 v2, v26, 0x2

    #@10b
    add-int v3, p5, v17

    #@10d
    aput v3, v24, v2

    #@10f
    .line 820
    sub-int v2, p5, p4

    #@111
    add-int v2, v2, v17

    #@113
    add-int p8, p8, v2

    #@115
    .line 821
    move-object/from16 v0, p0

    #@117
    iget v2, v0, Landroid/text/StaticLayout;->mColumns:I

    #@119
    add-int v2, v2, v26

    #@11b
    add-int/lit8 v2, v2, 0x0

    #@11d
    aput p3, v24, v2

    #@11f
    .line 822
    move-object/from16 v0, p0

    #@121
    iget v2, v0, Landroid/text/StaticLayout;->mColumns:I

    #@123
    add-int v2, v2, v26

    #@125
    add-int/lit8 v2, v2, 0x1

    #@127
    aput p8, v24, v2

    #@129
    .line 824
    if-eqz p14, :cond_134

    #@12b
    .line 825
    add-int/lit8 v2, v26, 0x0

    #@12d
    aget v3, v24, v2

    #@12f
    const/high16 v4, 0x2000

    #@131
    or-int/2addr v3, v4

    #@132
    aput v3, v24, v2

    #@134
    .line 827
    :cond_134
    add-int/lit8 v2, v26, 0x0

    #@136
    aget v3, v24, v2

    #@138
    shl-int/lit8 v4, p17, 0x1e

    #@13a
    or-int/2addr v3, v4

    #@13b
    aput v3, v24, v2

    #@13d
    .line 828
    sget-object v23, Landroid/text/StaticLayout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    #@13f
    .line 832
    .local v23, linedirs:Landroid/text/Layout$Directions;
    if-eqz p18, :cond_1b7

    #@141
    .line 833
    move-object/from16 v0, p0

    #@143
    iget-object v2, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@145
    aput-object v23, v2, v22

    #@147
    .line 839
    :goto_147
    if-eqz p25, :cond_19e

    #@149
    .line 842
    if-nez v22, :cond_1cf

    #@14b
    const/16 v18, 0x1

    #@14d
    .line 843
    .local v18, firstLine:Z
    :goto_14d
    add-int/lit8 v2, v22, 0x1

    #@14f
    move-object/from16 v0, p0

    #@151
    iget v3, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@153
    if-ne v2, v3, :cond_1d3

    #@155
    const/4 v13, 0x1

    #@156
    .line 844
    .local v13, currentLineIsTheLastVisibleOne:Z
    :goto_156
    if-eqz p29, :cond_1d5

    #@158
    move-object/from16 v0, p0

    #@15a
    iget v2, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@15c
    add-int/lit8 v2, v2, 0x1

    #@15e
    move-object/from16 v0, p0

    #@160
    iget v3, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@162
    if-ne v2, v3, :cond_1d5

    #@164
    const/4 v12, 0x1

    #@165
    .line 846
    .local v12, forceEllipsis:Z
    :goto_165
    move-object/from16 v0, p0

    #@167
    iget v2, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@169
    const/4 v3, 0x1

    #@16a
    if-ne v2, v3, :cond_16e

    #@16c
    if-nez p29, :cond_172

    #@16e
    :cond_16e
    if-eqz v18, :cond_178

    #@170
    if-nez p29, :cond_178

    #@172
    :cond_172
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    #@174
    move-object/from16 v0, p25

    #@176
    if-ne v0, v2, :cond_184

    #@178
    :cond_178
    if-nez v18, :cond_1d7

    #@17a
    if-nez v13, :cond_17e

    #@17c
    if-nez p29, :cond_1d7

    #@17e
    :cond_17e
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    #@180
    move-object/from16 v0, p25

    #@182
    if-ne v0, v2, :cond_1d7

    #@184
    :cond_184
    const/4 v14, 0x1

    #@185
    .line 851
    .local v14, doEllipsis:Z
    :goto_185
    if-eqz v14, :cond_19e

    #@187
    move-object/from16 v2, p0

    #@189
    move/from16 v3, p2

    #@18b
    move/from16 v4, p3

    #@18d
    move-object/from16 v5, p23

    #@18f
    move/from16 v6, p24

    #@191
    move/from16 v7, p26

    #@193
    move-object/from16 v8, p25

    #@195
    move/from16 v9, v22

    #@197
    move/from16 v10, p27

    #@199
    move-object/from16 v11, p28

    #@19b
    .line 852
    invoke-direct/range {v2 .. v12}, Landroid/text/StaticLayout;->calculateEllipsis(II[FIFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;Z)V

    #@19e
    .line 858
    .end local v12           #forceEllipsis:Z
    .end local v13           #currentLineIsTheLastVisibleOne:Z
    .end local v14           #doEllipsis:Z
    .end local v18           #firstLine:Z
    :cond_19e
    move-object/from16 v0, p0

    #@1a0
    iget v2, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@1a2
    add-int/lit8 v2, v2, 0x1

    #@1a4
    move-object/from16 v0, p0

    #@1a6
    iput v2, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@1a8
    .line 859
    return p8

    #@1a9
    .line 810
    .end local v17           #extra:I
    .end local v23           #linedirs:Landroid/text/Layout$Directions;
    .restart local v15       #ex:D
    :cond_1a9
    neg-double v2, v15

    #@1aa
    const-wide/high16 v4, 0x3fe0

    #@1ac
    add-double/2addr v2, v4

    #@1ad
    double-to-int v2, v2

    #@1ae
    neg-int v0, v2

    #@1af
    move/from16 v17, v0

    #@1b1
    .restart local v17       #extra:I
    goto/16 :goto_101

    #@1b3
    .line 813
    .end local v15           #ex:D
    .end local v17           #extra:I
    :cond_1b3
    const/16 v17, 0x0

    #@1b5
    .restart local v17       #extra:I
    goto/16 :goto_101

    #@1b7
    .line 835
    .restart local v23       #linedirs:Landroid/text/Layout$Directions;
    :cond_1b7
    move-object/from16 v0, p0

    #@1b9
    iget-object v8, v0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@1bb
    sub-int v4, p2, p24

    #@1bd
    sub-int v6, p2, p24

    #@1bf
    sub-int v7, p3, p2

    #@1c1
    move/from16 v2, p17

    #@1c3
    move-object/from16 v3, p16

    #@1c5
    move-object/from16 v5, p22

    #@1c7
    invoke-static/range {v2 .. v7}, Landroid/text/AndroidBidi;->directions(I[BI[CII)Landroid/text/Layout$Directions;

    #@1ca
    move-result-object v2

    #@1cb
    aput-object v2, v8, v22

    #@1cd
    goto/16 :goto_147

    #@1cf
    .line 842
    :cond_1cf
    const/16 v18, 0x0

    #@1d1
    goto/16 :goto_14d

    #@1d3
    .line 843
    .restart local v18       #firstLine:Z
    :cond_1d3
    const/4 v13, 0x0

    #@1d4
    goto :goto_156

    #@1d5
    .line 844
    .restart local v13       #currentLineIsTheLastVisibleOne:Z
    :cond_1d5
    const/4 v12, 0x0

    #@1d6
    goto :goto_165

    #@1d7
    .line 846
    .restart local v12       #forceEllipsis:Z
    :cond_1d7
    const/4 v14, 0x0

    #@1d8
    goto :goto_185
.end method


# virtual methods
.method finish()V
    .registers 2

    #@0
    .prologue
    .line 1074
    iget-object v0, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@2
    invoke-static {v0}, Landroid/text/MeasuredText;->recycle(Landroid/text/MeasuredText;)Landroid/text/MeasuredText;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@8
    .line 1075
    return-void
.end method

.method generate(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/TextDirectionHeuristic;FFZZFLandroid/text/TextUtils$TruncateAt;)V
    .registers 157
    .parameter "source"
    .parameter "bufStart"
    .parameter "bufEnd"
    .parameter "paint"
    .parameter "outerWidth"
    .parameter "textDir"
    .parameter "spacingmult"
    .parameter "spacingadd"
    .parameter "includepad"
    .parameter "trackpad"
    .parameter "ellipsizedWidth"
    .parameter "ellipsize"

    #@0
    .prologue
    .line 177
    const/4 v5, 0x0

    #@1
    move-object/from16 v0, p0

    #@3
    iput v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@5
    .line 179
    const/4 v13, 0x0

    #@6
    .line 180
    .local v13, v:I
    const/high16 v5, 0x3f80

    #@8
    cmpl-float v5, p7, v5

    #@a
    if-nez v5, :cond_11

    #@c
    const/4 v5, 0x0

    #@d
    cmpl-float v5, p8, v5

    #@f
    if-eqz v5, :cond_a1

    #@11
    :cond_11
    const/16 v20, 0x1

    #@13
    .line 182
    .local v20, needMultiply:Z
    :goto_13
    move-object/from16 v0, p0

    #@15
    iget-object v0, v0, Landroid/text/StaticLayout;->mFontMetricsInt:Landroid/graphics/Paint$FontMetricsInt;

    #@17
    move-object/from16 v18, v0

    #@19
    .line 183
    .local v18, fm:Landroid/graphics/Paint$FontMetricsInt;
    const/16 v17, 0x0

    #@1b
    .line 185
    .local v17, chooseHtv:[I
    move-object/from16 v0, p0

    #@1d
    iget-object v0, v0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@1f
    move-object/from16 v122, v0

    #@21
    .line 187
    .local v122, measured:Landroid/text/MeasuredText;
    const/16 v137, 0x0

    #@23
    .line 188
    .local v137, spanned:Landroid/text/Spanned;
    move-object/from16 v0, p1

    #@25
    instance-of v5, v0, Landroid/text/Spanned;

    #@27
    if-eqz v5, :cond_2d

    #@29
    move-object/from16 v137, p1

    #@2b
    .line 189
    check-cast v137, Landroid/text/Spanned;

    #@2d
    .line 191
    :cond_2d
    const/16 v80, 0x1

    #@2f
    .line 194
    .local v80, DEFAULT_DIR:I
    move/from16 v29, p2

    #@31
    .local v29, paraStart:I
    :goto_31
    move/from16 v0, v29

    #@33
    move/from16 v1, p3

    #@35
    if-gt v0, v1, :cond_634

    #@37
    .line 195
    const/16 v5, 0xa

    #@39
    move-object/from16 v0, p1

    #@3b
    move/from16 v1, v29

    #@3d
    move/from16 v2, p3

    #@3f
    invoke-static {v0, v5, v1, v2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    #@42
    move-result v38

    #@43
    .line 196
    .local v38, paraEnd:I
    if-gez v38, :cond_a5

    #@45
    .line 197
    move/from16 v38, p3

    #@47
    .line 201
    :goto_47
    move-object/from16 v0, p0

    #@49
    iget v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@4b
    add-int/lit8 v101, v5, 0x1

    #@4d
    .line 202
    .local v101, firstWidthLineLimit:I
    move/from16 v100, p5

    #@4f
    .line 203
    .local v100, firstWidth:I
    move/from16 v131, p5

    #@51
    .line 205
    .local v131, restWidth:I
    const/16 v16, 0x0

    #@53
    .line 207
    .local v16, chooseHt:[Landroid/text/style/LineHeightSpan;
    if-eqz v137, :cond_fd

    #@55
    .line 208
    const-class v5, Landroid/text/style/LeadingMarginSpan;

    #@57
    move-object/from16 v0, v137

    #@59
    move/from16 v1, v29

    #@5b
    move/from16 v2, v38

    #@5d
    invoke-static {v0, v1, v2, v5}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@60
    move-result-object v132

    #@61
    check-cast v132, [Landroid/text/style/LeadingMarginSpan;

    #@63
    .line 210
    .local v132, sp:[Landroid/text/style/LeadingMarginSpan;
    const/16 v110, 0x0

    #@65
    .local v110, i:I
    :goto_65
    move-object/from16 v0, v132

    #@67
    array-length v5, v0

    #@68
    move/from16 v0, v110

    #@6a
    if-ge v0, v5, :cond_a8

    #@6c
    .line 211
    aget-object v119, v132, v110

    #@6e
    .line 212
    .local v119, lms:Landroid/text/style/LeadingMarginSpan;
    aget-object v5, v132, v110

    #@70
    const/4 v6, 0x1

    #@71
    invoke-interface {v5, v6}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    #@74
    move-result v5

    #@75
    sub-int v100, v100, v5

    #@77
    .line 213
    aget-object v5, v132, v110

    #@79
    const/4 v6, 0x0

    #@7a
    invoke-interface {v5, v6}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    #@7d
    move-result v5

    #@7e
    sub-int v131, v131, v5

    #@80
    .line 219
    move-object/from16 v0, v119

    #@82
    instance-of v5, v0, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@84
    if-eqz v5, :cond_9e

    #@86
    move-object/from16 v120, v119

    #@88
    .line 220
    check-cast v120, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    #@8a
    .line 221
    .local v120, lms2:Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;
    move-object/from16 v0, v137

    #@8c
    move-object/from16 v1, v120

    #@8e
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@91
    move-result v5

    #@92
    move-object/from16 v0, p0

    #@94
    invoke-virtual {v0, v5}, Landroid/text/StaticLayout;->getLineForOffset(I)I

    #@97
    move-result v121

    #@98
    .line 222
    .local v121, lmsFirstLine:I
    invoke-interface/range {v120 .. v120}, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;->getLeadingMarginLineCount()I

    #@9b
    move-result v5

    #@9c
    add-int v101, v121, v5

    #@9e
    .line 210
    .end local v120           #lms2:Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;
    .end local v121           #lmsFirstLine:I
    :cond_9e
    add-int/lit8 v110, v110, 0x1

    #@a0
    goto :goto_65

    #@a1
    .line 180
    .end local v16           #chooseHt:[Landroid/text/style/LineHeightSpan;
    .end local v17           #chooseHtv:[I
    .end local v18           #fm:Landroid/graphics/Paint$FontMetricsInt;
    .end local v20           #needMultiply:Z
    .end local v29           #paraStart:I
    .end local v38           #paraEnd:I
    .end local v80           #DEFAULT_DIR:I
    .end local v100           #firstWidth:I
    .end local v101           #firstWidthLineLimit:I
    .end local v110           #i:I
    .end local v119           #lms:Landroid/text/style/LeadingMarginSpan;
    .end local v122           #measured:Landroid/text/MeasuredText;
    .end local v131           #restWidth:I
    .end local v132           #sp:[Landroid/text/style/LeadingMarginSpan;
    .end local v137           #spanned:Landroid/text/Spanned;
    :cond_a1
    const/16 v20, 0x0

    #@a3
    goto/16 :goto_13

    #@a5
    .line 199
    .restart local v17       #chooseHtv:[I
    .restart local v18       #fm:Landroid/graphics/Paint$FontMetricsInt;
    .restart local v20       #needMultiply:Z
    .restart local v29       #paraStart:I
    .restart local v38       #paraEnd:I
    .restart local v80       #DEFAULT_DIR:I
    .restart local v122       #measured:Landroid/text/MeasuredText;
    .restart local v137       #spanned:Landroid/text/Spanned;
    :cond_a5
    add-int/lit8 v38, v38, 0x1

    #@a7
    goto :goto_47

    #@a8
    .line 226
    .restart local v16       #chooseHt:[Landroid/text/style/LineHeightSpan;
    .restart local v100       #firstWidth:I
    .restart local v101       #firstWidthLineLimit:I
    .restart local v110       #i:I
    .restart local v131       #restWidth:I
    .restart local v132       #sp:[Landroid/text/style/LeadingMarginSpan;
    :cond_a8
    const-class v5, Landroid/text/style/LineHeightSpan;

    #@aa
    move-object/from16 v0, v137

    #@ac
    move/from16 v1, v29

    #@ae
    move/from16 v2, v38

    #@b0
    invoke-static {v0, v1, v2, v5}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@b3
    move-result-object v16

    #@b4
    .end local v16           #chooseHt:[Landroid/text/style/LineHeightSpan;
    check-cast v16, [Landroid/text/style/LineHeightSpan;

    #@b6
    .line 228
    .restart local v16       #chooseHt:[Landroid/text/style/LineHeightSpan;
    move-object/from16 v0, v16

    #@b8
    array-length v5, v0

    #@b9
    if-eqz v5, :cond_fd

    #@bb
    .line 229
    if-eqz v17, :cond_c5

    #@bd
    move-object/from16 v0, v17

    #@bf
    array-length v5, v0

    #@c0
    move-object/from16 v0, v16

    #@c2
    array-length v6, v0

    #@c3
    if-ge v5, v6, :cond_d0

    #@c5
    .line 231
    :cond_c5
    move-object/from16 v0, v16

    #@c7
    array-length v5, v0

    #@c8
    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealIntArraySize(I)I

    #@cb
    move-result v5

    #@cc
    new-array v0, v5, [I

    #@ce
    move-object/from16 v17, v0

    #@d0
    .line 235
    :cond_d0
    const/16 v110, 0x0

    #@d2
    :goto_d2
    move-object/from16 v0, v16

    #@d4
    array-length v5, v0

    #@d5
    move/from16 v0, v110

    #@d7
    if-ge v0, v5, :cond_fd

    #@d9
    .line 236
    aget-object v5, v16, v110

    #@db
    move-object/from16 v0, v137

    #@dd
    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    #@e0
    move-result v124

    #@e1
    .line 238
    .local v124, o:I
    move/from16 v0, v124

    #@e3
    move/from16 v1, v29

    #@e5
    if-ge v0, v1, :cond_fa

    #@e7
    .line 242
    move-object/from16 v0, p0

    #@e9
    move/from16 v1, v124

    #@eb
    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineForOffset(I)I

    #@ee
    move-result v5

    #@ef
    move-object/from16 v0, p0

    #@f1
    invoke-virtual {v0, v5}, Landroid/text/StaticLayout;->getLineTop(I)I

    #@f4
    move-result v5

    #@f5
    aput v5, v17, v110

    #@f7
    .line 235
    :goto_f7
    add-int/lit8 v110, v110, 0x1

    #@f9
    goto :goto_d2

    #@fa
    .line 246
    :cond_fa
    aput v13, v17, v110

    #@fc
    goto :goto_f7

    #@fd
    .line 252
    .end local v110           #i:I
    .end local v124           #o:I
    .end local v132           #sp:[Landroid/text/style/LeadingMarginSpan;
    :cond_fd
    move-object/from16 v0, v122

    #@ff
    move-object/from16 v1, p1

    #@101
    move/from16 v2, v29

    #@103
    move/from16 v3, v38

    #@105
    move-object/from16 v4, p6

    #@107
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/MeasuredText;->setPara(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;)V

    #@10a
    .line 253
    move-object/from16 v0, v122

    #@10c
    iget-object v0, v0, Landroid/text/MeasuredText;->mChars:[C

    #@10e
    move-object/from16 v27, v0

    #@110
    .line 254
    .local v27, chs:[C
    move-object/from16 v0, v122

    #@112
    iget-object v0, v0, Landroid/text/MeasuredText;->mWidths:[F

    #@114
    move-object/from16 v28, v0

    #@116
    .line 255
    .local v28, widths:[F
    move-object/from16 v0, v122

    #@118
    iget-object v0, v0, Landroid/text/MeasuredText;->mLevels:[B

    #@11a
    move-object/from16 v21, v0

    #@11c
    .line 256
    .local v21, chdirs:[B
    move-object/from16 v0, v122

    #@11e
    iget v0, v0, Landroid/text/MeasuredText;->mDir:I

    #@120
    move/from16 v22, v0

    #@122
    .line 257
    .local v22, dir:I
    move-object/from16 v0, v122

    #@124
    iget-boolean v0, v0, Landroid/text/MeasuredText;->mEasy:Z

    #@126
    move/from16 v23, v0

    #@128
    .line 259
    .local v23, easy:Z
    move/from16 v143, v100

    #@12a
    .line 261
    .local v143, width:I
    const/16 v62, 0x0

    #@12c
    .line 263
    .local v62, w:F
    move/from16 v7, v29

    #@12e
    .line 267
    .local v7, here:I
    move/from16 v125, v29

    #@130
    .line 268
    .local v125, ok:I
    move/from16 v130, v62

    #@132
    .line 269
    .local v130, okWidth:F
    const/16 v126, 0x0

    #@134
    .local v126, okAscent:I
    const/16 v128, 0x0

    #@136
    .local v128, okDescent:I
    const/16 v129, 0x0

    #@138
    .local v129, okTop:I
    const/16 v127, 0x0

    #@13a
    .line 273
    .local v127, okBottom:I
    move/from16 v102, v29

    #@13c
    .line 274
    .local v102, fit:I
    move/from16 v103, v62

    #@13e
    .line 275
    .local v103, fitWidth:F
    const/16 v39, 0x0

    #@140
    .local v39, fitAscent:I
    const/16 v40, 0x0

    #@142
    .local v40, fitDescent:I
    const/16 v41, 0x0

    #@144
    .local v41, fitTop:I
    const/16 v42, 0x0

    #@146
    .line 277
    .local v42, fitBottom:I
    const/16 v19, 0x0

    #@148
    .line 278
    .local v19, hasTabOrEmoji:Z
    const/16 v109, 0x0

    #@14a
    .line 280
    .local v109, hasTab:Z
    const/16 v108, 0x0

    #@14c
    .line 281
    .local v108, hasEmoji:Z
    const/16 v139, 0x0

    #@14e
    .line 283
    .local v139, tabStops:Landroid/text/Layout$TabStops;
    move/from16 v136, v29

    #@150
    .local v136, spanStart:I
    :goto_150
    move/from16 v0, v136

    #@152
    move/from16 v1, v38

    #@154
    if-ge v0, v1, :cond_5bd

    #@156
    .line 285
    if-nez v137, :cond_23e

    #@158
    .line 286
    move/from16 v134, v38

    #@15a
    .line 287
    .local v134, spanEnd:I
    sub-int v135, v134, v136

    #@15c
    .line 288
    .local v135, spanLen:I
    move-object/from16 v0, v122

    #@15e
    move-object/from16 v1, p4

    #@160
    move/from16 v2, v135

    #@162
    move-object/from16 v3, v18

    #@164
    invoke-virtual {v0, v1, v2, v3}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@167
    .line 337
    :cond_167
    move-object/from16 v0, v18

    #@169
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@16b
    move/from16 v107, v0

    #@16d
    .line 338
    .local v107, fmTop:I
    move-object/from16 v0, v18

    #@16f
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@171
    move/from16 v105, v0

    #@173
    .line 339
    .local v105, fmBottom:I
    move-object/from16 v0, v18

    #@175
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@177
    move/from16 v104, v0

    #@179
    .line 340
    .local v104, fmAscent:I
    move-object/from16 v0, v18

    #@17b
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@17d
    move/from16 v106, v0

    #@17f
    .line 342
    .local v106, fmDescent:I
    move/from16 v115, v136

    #@181
    .local v115, j:I
    :goto_181
    move/from16 v0, v115

    #@183
    move/from16 v1, v134

    #@185
    if-ge v0, v1, :cond_583

    #@187
    .line 343
    sub-int v5, v115, v29

    #@189
    aget-char v95, v27, v5

    #@18b
    .line 345
    .local v95, c:C
    const/16 v112, 0x0

    #@18d
    .line 347
    .local v112, isEmoji:Z
    const/16 v5, 0xa

    #@18f
    move/from16 v0, v95

    #@191
    if-ne v0, v5, :cond_329

    #@193
    .line 481
    :cond_193
    :goto_193
    const/16 v5, 0x20

    #@195
    move/from16 v0, v95

    #@197
    if-eq v0, v5, :cond_19f

    #@199
    const/16 v5, 0x9

    #@19b
    move/from16 v0, v95

    #@19d
    if-ne v0, v5, :cond_521

    #@19f
    :cond_19f
    const/16 v114, 0x1

    #@1a1
    .line 483
    .local v114, isSpaceOrTab:Z
    :goto_1a1
    move/from16 v0, v143

    #@1a3
    int-to-float v5, v0

    #@1a4
    cmpg-float v5, v62, v5

    #@1a6
    if-lez v5, :cond_1aa

    #@1a8
    if-eqz v114, :cond_529

    #@1aa
    .line 484
    :cond_1aa
    move/from16 v103, v62

    #@1ac
    .line 485
    add-int/lit8 v102, v115, 0x1

    #@1ae
    .line 487
    move/from16 v0, v107

    #@1b0
    move/from16 v1, v41

    #@1b2
    if-ge v0, v1, :cond_1b6

    #@1b4
    .line 488
    move/from16 v41, v107

    #@1b6
    .line 489
    :cond_1b6
    move/from16 v0, v104

    #@1b8
    move/from16 v1, v39

    #@1ba
    if-ge v0, v1, :cond_1be

    #@1bc
    .line 490
    move/from16 v39, v104

    #@1be
    .line 491
    :cond_1be
    move/from16 v0, v106

    #@1c0
    move/from16 v1, v40

    #@1c2
    if-le v0, v1, :cond_1c6

    #@1c4
    .line 492
    move/from16 v40, v106

    #@1c6
    .line 493
    :cond_1c6
    move/from16 v0, v105

    #@1c8
    move/from16 v1, v42

    #@1ca
    if-le v0, v1, :cond_1ce

    #@1cc
    .line 494
    move/from16 v42, v105

    #@1ce
    .line 497
    :cond_1ce
    if-nez v114, :cond_212

    #@1d0
    const/16 v5, 0x2f

    #@1d2
    move/from16 v0, v95

    #@1d4
    if-eq v0, v5, :cond_1dc

    #@1d6
    const/16 v5, 0x2d

    #@1d8
    move/from16 v0, v95

    #@1da
    if-ne v0, v5, :cond_1ee

    #@1dc
    :cond_1dc
    add-int/lit8 v5, v115, 0x1

    #@1de
    move/from16 v0, v134

    #@1e0
    if-ge v5, v0, :cond_212

    #@1e2
    add-int/lit8 v5, v115, 0x1

    #@1e4
    sub-int v5, v5, v29

    #@1e6
    aget-char v5, v27, v5

    #@1e8
    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    #@1eb
    move-result v5

    #@1ec
    if-eqz v5, :cond_212

    #@1ee
    :cond_1ee
    const/16 v5, 0x2e80

    #@1f0
    move/from16 v0, v95

    #@1f2
    if-lt v0, v5, :cond_210

    #@1f4
    const/4 v5, 0x1

    #@1f5
    move/from16 v0, v95

    #@1f7
    invoke-static {v0, v5}, Landroid/text/StaticLayout;->isIdeographic(CZ)Z

    #@1fa
    move-result v5

    #@1fb
    if-eqz v5, :cond_210

    #@1fd
    add-int/lit8 v5, v115, 0x1

    #@1ff
    move/from16 v0, v134

    #@201
    if-ge v5, v0, :cond_210

    #@203
    add-int/lit8 v5, v115, 0x1

    #@205
    sub-int v5, v5, v29

    #@207
    aget-char v5, v27, v5

    #@209
    const/4 v6, 0x0

    #@20a
    invoke-static {v5, v6}, Landroid/text/StaticLayout;->isIdeographic(CZ)Z

    #@20d
    move-result v5

    #@20e
    if-nez v5, :cond_212

    #@210
    :cond_210
    if-eqz v112, :cond_525

    #@212
    :cond_212
    const/16 v113, 0x1

    #@214
    .line 506
    .local v113, isLineBreak:Z
    :goto_214
    if-eqz v113, :cond_23a

    #@216
    .line 507
    move/from16 v130, v62

    #@218
    .line 508
    add-int/lit8 v125, v115, 0x1

    #@21a
    .line 510
    move/from16 v0, v41

    #@21c
    move/from16 v1, v129

    #@21e
    if-ge v0, v1, :cond_222

    #@220
    .line 511
    move/from16 v129, v41

    #@222
    .line 512
    :cond_222
    move/from16 v0, v39

    #@224
    move/from16 v1, v126

    #@226
    if-ge v0, v1, :cond_22a

    #@228
    .line 513
    move/from16 v126, v39

    #@22a
    .line 514
    :cond_22a
    move/from16 v0, v40

    #@22c
    move/from16 v1, v128

    #@22e
    if-le v0, v1, :cond_232

    #@230
    .line 515
    move/from16 v128, v40

    #@232
    .line 516
    :cond_232
    move/from16 v0, v42

    #@234
    move/from16 v1, v127

    #@236
    if-le v0, v1, :cond_23a

    #@238
    .line 517
    move/from16 v127, v42

    #@23a
    .line 342
    .end local v113           #isLineBreak:Z
    :cond_23a
    add-int/lit8 v115, v115, 0x1

    #@23c
    goto/16 :goto_181

    #@23e
    .line 292
    .end local v95           #c:C
    .end local v104           #fmAscent:I
    .end local v105           #fmBottom:I
    .end local v106           #fmDescent:I
    .end local v107           #fmTop:I
    .end local v112           #isEmoji:Z
    .end local v114           #isSpaceOrTab:Z
    .end local v115           #j:I
    .end local v134           #spanEnd:I
    .end local v135           #spanLen:I
    :cond_23e
    const/16 v140, 0x0

    #@240
    .line 293
    .local v140, tmp:[C
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@242
    if-eqz v5, :cond_2db

    #@244
    .line 294
    move-object/from16 v0, v122

    #@246
    iget v5, v0, Landroid/text/MeasuredText;->mLen:I

    #@248
    new-array v0, v5, [C

    #@24a
    move-object/from16 v140, v0

    #@24c
    .line 296
    const/16 v110, 0x0

    #@24e
    .restart local v110       #i:I
    :goto_24e
    move-object/from16 v0, v122

    #@250
    iget v5, v0, Landroid/text/MeasuredText;->mLen:I

    #@252
    move/from16 v0, v110

    #@254
    if-ge v0, v5, :cond_2db

    #@256
    .line 297
    sget-object v5, Landroid/text/StaticLayout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@258
    aget-char v6, v27, v110

    #@25a
    invoke-virtual {v5, v6}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@25d
    move-result v5

    #@25e
    if-nez v5, :cond_294

    #@260
    .line 298
    move-object/from16 v0, v27

    #@262
    move/from16 v1, v110

    #@264
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt([CI)I

    #@267
    move-result v96

    #@268
    .line 299
    .local v96, codept:I
    add-int/lit8 v5, v110, 0x3

    #@26a
    move-object/from16 v0, v122

    #@26c
    iget v6, v0, Landroid/text/MeasuredText;->mLen:I

    #@26e
    if-ge v5, v6, :cond_297

    #@270
    move-object/from16 v0, v27

    #@272
    move/from16 v1, v110

    #@274
    invoke-static {v0, v1}, Landroid/text/StaticLayout;->isInCountryCodeTable([CI)Z

    #@277
    move-result v5

    #@278
    if-eqz v5, :cond_297

    #@27a
    .line 300
    const/16 v115, 0x0

    #@27c
    .restart local v115       #j:I
    :goto_27c
    const/4 v5, 0x4

    #@27d
    move/from16 v0, v115

    #@27f
    if-ge v0, v5, :cond_292

    #@281
    .line 301
    add-int v5, v110, v115

    #@283
    add-int v6, v110, v115

    #@285
    aget-char v6, v27, v6

    #@287
    aput-char v6, v140, v5

    #@289
    .line 302
    add-int v5, v110, v115

    #@28b
    const/16 v6, 0x20

    #@28d
    aput-char v6, v27, v5

    #@28f
    .line 300
    add-int/lit8 v115, v115, 0x1

    #@291
    goto :goto_27c

    #@292
    .line 304
    :cond_292
    add-int/lit8 v110, v110, 0x3

    #@294
    .line 296
    .end local v96           #codept:I
    .end local v115           #j:I
    :cond_294
    :goto_294
    add-int/lit8 v110, v110, 0x1

    #@296
    goto :goto_24e

    #@297
    .line 305
    .restart local v96       #codept:I
    :cond_297
    add-int/lit8 v5, v110, 0x1

    #@299
    move-object/from16 v0, v122

    #@29b
    iget v6, v0, Landroid/text/MeasuredText;->mLen:I

    #@29d
    if-ge v5, v6, :cond_2c6

    #@29f
    invoke-static/range {v96 .. v96}, Landroid/text/StaticLayout;->isInEmojiUnicodeTable(I)Z

    #@2a2
    move-result v5

    #@2a3
    if-nez v5, :cond_2af

    #@2a5
    move-object/from16 v0, v27

    #@2a7
    move/from16 v1, v110

    #@2a9
    invoke-static {v0, v1}, Landroid/text/StaticLayout;->isDiacriticalMark([CI)Z

    #@2ac
    move-result v5

    #@2ad
    if-eqz v5, :cond_2c6

    #@2af
    .line 306
    :cond_2af
    aget-char v5, v27, v110

    #@2b1
    aput-char v5, v140, v110

    #@2b3
    .line 307
    add-int/lit8 v5, v110, 0x1

    #@2b5
    add-int/lit8 v6, v110, 0x1

    #@2b7
    aget-char v6, v27, v6

    #@2b9
    aput-char v6, v140, v5

    #@2bb
    .line 308
    add-int/lit8 v5, v110, 0x1

    #@2bd
    const/16 v6, 0x20

    #@2bf
    aput-char v6, v27, v5

    #@2c1
    aput-char v6, v27, v110

    #@2c3
    .line 309
    add-int/lit8 v110, v110, 0x1

    #@2c5
    goto :goto_294

    #@2c6
    .line 310
    :cond_2c6
    aget-char v5, v27, v110

    #@2c8
    invoke-static {v5}, Landroid/text/StaticLayout;->isInEmojiUnicodeTable(C)Z

    #@2cb
    move-result v5

    #@2cc
    if-eqz v5, :cond_2d7

    #@2ce
    .line 311
    aget-char v5, v27, v110

    #@2d0
    aput-char v5, v140, v110

    #@2d2
    .line 312
    const/16 v5, 0x20

    #@2d4
    aput-char v5, v27, v110

    #@2d6
    goto :goto_294

    #@2d7
    .line 314
    :cond_2d7
    const/4 v5, 0x0

    #@2d8
    aput-char v5, v140, v110

    #@2da
    goto :goto_294

    #@2db
    .line 319
    .end local v96           #codept:I
    .end local v110           #i:I
    :cond_2db
    const-class v5, Landroid/text/style/MetricAffectingSpan;

    #@2dd
    move-object/from16 v0, v137

    #@2df
    move/from16 v1, v136

    #@2e1
    move/from16 v2, v38

    #@2e3
    invoke-interface {v0, v1, v2, v5}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    #@2e6
    move-result v134

    #@2e7
    .line 321
    .restart local v134       #spanEnd:I
    sub-int v135, v134, v136

    #@2e9
    .line 322
    .restart local v135       #spanLen:I
    const-class v5, Landroid/text/style/MetricAffectingSpan;

    #@2eb
    move-object/from16 v0, v137

    #@2ed
    move/from16 v1, v136

    #@2ef
    move/from16 v2, v134

    #@2f1
    invoke-interface {v0, v1, v2, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@2f4
    move-result-object v138

    #@2f5
    check-cast v138, [Landroid/text/style/MetricAffectingSpan;

    #@2f7
    .line 324
    .local v138, spans:[Landroid/text/style/MetricAffectingSpan;
    const-class v5, Landroid/text/style/MetricAffectingSpan;

    #@2f9
    move-object/from16 v0, v138

    #@2fb
    move-object/from16 v1, v137

    #@2fd
    invoke-static {v0, v1, v5}, Landroid/text/TextUtils;->removeEmptySpans([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;

    #@300
    move-result-object v138

    #@301
    .end local v138           #spans:[Landroid/text/style/MetricAffectingSpan;
    check-cast v138, [Landroid/text/style/MetricAffectingSpan;

    #@303
    .line 325
    .restart local v138       #spans:[Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v122

    #@305
    move-object/from16 v1, p4

    #@307
    move-object/from16 v2, v138

    #@309
    move/from16 v3, v135

    #@30b
    move-object/from16 v4, v18

    #@30d
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/MeasuredText;->addStyleRun(Landroid/text/TextPaint;[Landroid/text/style/MetricAffectingSpan;ILandroid/graphics/Paint$FontMetricsInt;)F

    #@310
    .line 328
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@312
    if-eqz v5, :cond_167

    #@314
    .line 329
    const/16 v110, 0x0

    #@316
    .restart local v110       #i:I
    :goto_316
    move-object/from16 v0, v122

    #@318
    iget v5, v0, Landroid/text/MeasuredText;->mLen:I

    #@31a
    move/from16 v0, v110

    #@31c
    if-ge v0, v5, :cond_167

    #@31e
    .line 330
    aget-char v5, v140, v110

    #@320
    if-eqz v5, :cond_326

    #@322
    .line 331
    aget-char v5, v140, v110

    #@324
    aput-char v5, v27, v110

    #@326
    .line 329
    :cond_326
    add-int/lit8 v110, v110, 0x1

    #@328
    goto :goto_316

    #@329
    .line 349
    .end local v110           #i:I
    .end local v138           #spans:[Landroid/text/style/MetricAffectingSpan;
    .end local v140           #tmp:[C
    .restart local v95       #c:C
    .restart local v104       #fmAscent:I
    .restart local v105       #fmBottom:I
    .restart local v106       #fmDescent:I
    .restart local v107       #fmTop:I
    .restart local v112       #isEmoji:Z
    .restart local v115       #j:I
    :cond_329
    const/16 v5, 0x9

    #@32b
    move/from16 v0, v95

    #@32d
    if-ne v0, v5, :cond_36b

    #@32f
    .line 350
    if-nez v109, :cond_355

    #@331
    .line 351
    const/16 v109, 0x1

    #@333
    .line 352
    const/16 v19, 0x1

    #@335
    .line 353
    if-eqz v137, :cond_355

    #@337
    .line 355
    const-class v5, Landroid/text/style/TabStopSpan;

    #@339
    move-object/from16 v0, v137

    #@33b
    move/from16 v1, v29

    #@33d
    move/from16 v2, v38

    #@33f
    invoke-static {v0, v1, v2, v5}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    #@342
    move-result-object v138

    #@343
    check-cast v138, [Landroid/text/style/TabStopSpan;

    #@345
    .line 357
    .local v138, spans:[Landroid/text/style/TabStopSpan;
    move-object/from16 v0, v138

    #@347
    array-length v5, v0

    #@348
    if-lez v5, :cond_355

    #@34a
    .line 358
    new-instance v139, Landroid/text/Layout$TabStops;

    #@34c
    .end local v139           #tabStops:Landroid/text/Layout$TabStops;
    const/16 v5, 0x14

    #@34e
    move-object/from16 v0, v139

    #@350
    move-object/from16 v1, v138

    #@352
    invoke-direct {v0, v5, v1}, Landroid/text/Layout$TabStops;-><init>(I[Ljava/lang/Object;)V

    #@355
    .line 362
    .end local v138           #spans:[Landroid/text/style/TabStopSpan;
    .restart local v139       #tabStops:Landroid/text/Layout$TabStops;
    :cond_355
    if-eqz v139, :cond_361

    #@357
    .line 363
    move-object/from16 v0, v139

    #@359
    move/from16 v1, v62

    #@35b
    invoke-virtual {v0, v1}, Landroid/text/Layout$TabStops;->nextTab(F)F

    #@35e
    move-result v62

    #@35f
    goto/16 :goto_193

    #@361
    .line 365
    :cond_361
    const/16 v5, 0x14

    #@363
    move/from16 v0, v62

    #@365
    invoke-static {v0, v5}, Landroid/text/Layout$TabStops;->nextDefaultStop(FI)F

    #@368
    move-result v62

    #@369
    goto/16 :goto_193

    #@36b
    .line 368
    :cond_36b
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI:Z

    #@36d
    if-eqz v5, :cond_4b2

    #@36f
    .line 369
    const/16 v99, 0x0

    #@371
    .line 370
    .local v99, emojiBytes:I
    sub-int v5, v115, v29

    #@373
    move-object/from16 v0, v27

    #@375
    invoke-static {v0, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@378
    move-result v98

    #@379
    .line 372
    .local v98, emoji:I
    sget-object v5, Landroid/text/StaticLayout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@37b
    move/from16 v0, v95

    #@37d
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory;->quickRejectEmojiCode(C)Z

    #@380
    move-result v5

    #@381
    if-nez v5, :cond_38d

    #@383
    .line 373
    invoke-static/range {v95 .. v95}, Landroid/text/StaticLayout;->isInEmojiUnicodeTable(C)Z

    #@386
    move-result v5

    #@387
    if-eqz v5, :cond_416

    #@389
    .line 374
    const/16 v112, 0x1

    #@38b
    .line 375
    const/16 v99, 0x2

    #@38d
    .line 387
    :cond_38d
    :goto_38d
    move-object/from16 v0, p0

    #@38f
    iget v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@391
    move-object/from16 v0, p0

    #@393
    iget-object v6, v0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@395
    array-length v6, v6

    #@396
    if-lt v5, v6, :cond_3bf

    #@398
    .line 388
    move-object/from16 v0, p0

    #@39a
    iget-object v5, v0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@39c
    array-length v5, v5

    #@39d
    mul-int/lit8 v5, v5, 0x2

    #@39f
    invoke-static {v5}, Lcom/android/internal/util/ArrayUtils;->idealBooleanArraySize(I)I

    #@3a2
    move-result v123

    #@3a3
    .line 389
    .local v123, newLength:I
    move/from16 v0, v123

    #@3a5
    new-array v0, v0, [Z

    #@3a7
    move-object/from16 v118, v0

    #@3a9
    .line 391
    .local v118, lineContainsEmoji:[Z
    move-object/from16 v0, p0

    #@3ab
    iget-object v5, v0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@3ad
    const/4 v6, 0x0

    #@3ae
    const/4 v14, 0x0

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iget-object v15, v0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@3b3
    array-length v15, v15

    #@3b4
    move-object/from16 v0, v118

    #@3b6
    invoke-static {v5, v6, v0, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@3b9
    .line 392
    move-object/from16 v0, v118

    #@3bb
    move-object/from16 v1, p0

    #@3bd
    iput-object v0, v1, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@3bf
    .line 395
    .end local v118           #lineContainsEmoji:[Z
    .end local v123           #newLength:I
    :cond_3bf
    if-eqz v112, :cond_4aa

    #@3c1
    .line 396
    move/from16 v0, v104

    #@3c3
    neg-int v5, v0

    #@3c4
    add-int v5, v5, v106

    #@3c6
    int-to-float v0, v5

    #@3c7
    move/from16 v142, v0

    #@3c9
    .line 398
    .local v142, wid:F
    sget-boolean v5, Lcom/lge/config/ConfigBuildFlags;->CAPP_EMOJI_DCM:Z

    #@3cb
    if-eqz v5, :cond_3ed

    #@3cd
    .line 399
    sget-object v5, Landroid/text/StaticLayout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@3cf
    move/from16 v0, v98

    #@3d1
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@3d4
    move-result-object v94

    #@3d5
    .line 401
    .local v94, bm:Landroid/graphics/Bitmap;
    if-eqz v94, :cond_3ed

    #@3d7
    .line 405
    if-nez v137, :cond_448

    #@3d9
    .line 406
    move-object/from16 v141, p4

    #@3db
    .line 421
    .local v141, whichPaint:Landroid/text/TextPaint;
    :cond_3db
    :goto_3db
    invoke-virtual/range {v94 .. v94}, Landroid/graphics/Bitmap;->getWidth()I

    #@3de
    move-result v5

    #@3df
    int-to-float v5, v5

    #@3e0
    invoke-virtual/range {v141 .. v141}, Landroid/text/TextPaint;->ascent()F

    #@3e3
    move-result v6

    #@3e4
    neg-float v6, v6

    #@3e5
    mul-float/2addr v5, v6

    #@3e6
    invoke-virtual/range {v94 .. v94}, Landroid/graphics/Bitmap;->getHeight()I

    #@3e9
    move-result v6

    #@3ea
    int-to-float v6, v6

    #@3eb
    div-float v142, v5, v6

    #@3ed
    .line 425
    .end local v94           #bm:Landroid/graphics/Bitmap;
    .end local v141           #whichPaint:Landroid/text/TextPaint;
    :cond_3ed
    sub-int v5, v115, v29

    #@3ef
    aput v142, v28, v5

    #@3f1
    .line 427
    const/4 v5, 0x4

    #@3f2
    move/from16 v0, v99

    #@3f4
    if-ne v0, v5, :cond_489

    #@3f6
    .line 428
    add-int/lit8 v5, v115, 0x1

    #@3f8
    sub-int v5, v5, v29

    #@3fa
    const/4 v6, 0x0

    #@3fb
    aput v6, v28, v5

    #@3fd
    .line 435
    :cond_3fd
    add-float v62, v62, v142

    #@3ff
    .line 436
    const/16 v19, 0x1

    #@401
    .line 437
    const/16 v108, 0x1

    #@403
    .line 439
    move-object/from16 v0, p0

    #@405
    iget-object v5, v0, Landroid/text/Layout;->mLineContainsEmoji:[Z

    #@407
    move-object/from16 v0, p0

    #@409
    iget v6, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@40b
    aput-boolean v108, v5, v6

    #@40d
    .line 441
    const/4 v5, 0x4

    #@40e
    move/from16 v0, v99

    #@410
    if-ne v0, v5, :cond_4a0

    #@412
    .line 442
    add-int/lit8 v115, v115, 0x1

    #@414
    goto/16 :goto_193

    #@416
    .line 376
    .end local v142           #wid:F
    :cond_416
    add-int/lit8 v5, v115, 0x3

    #@418
    move/from16 v0, v134

    #@41a
    if-ge v5, v0, :cond_42c

    #@41c
    sub-int v5, v115, v29

    #@41e
    move-object/from16 v0, v27

    #@420
    invoke-static {v0, v5}, Landroid/text/StaticLayout;->isInCountryCodeTable([CI)Z

    #@423
    move-result v5

    #@424
    if-eqz v5, :cond_42c

    #@426
    .line 377
    const/16 v112, 0x1

    #@428
    .line 378
    const/16 v99, 0x8

    #@42a
    goto/16 :goto_38d

    #@42c
    .line 379
    :cond_42c
    add-int/lit8 v5, v115, 0x1

    #@42e
    move/from16 v0, v134

    #@430
    if-ge v5, v0, :cond_38d

    #@432
    .line 380
    invoke-static/range {v98 .. v98}, Landroid/text/StaticLayout;->isInEmojiUnicodeTable(I)Z

    #@435
    move-result v5

    #@436
    if-nez v5, :cond_442

    #@438
    sub-int v5, v115, v29

    #@43a
    move-object/from16 v0, v27

    #@43c
    invoke-static {v0, v5}, Landroid/text/StaticLayout;->isDiacriticalMark([CI)Z

    #@43f
    move-result v5

    #@440
    if-eqz v5, :cond_38d

    #@442
    .line 381
    :cond_442
    const/16 v112, 0x1

    #@444
    .line 382
    const/16 v99, 0x4

    #@446
    goto/16 :goto_38d

    #@448
    .line 408
    .restart local v94       #bm:Landroid/graphics/Bitmap;
    .restart local v142       #wid:F
    :cond_448
    sub-int v5, v115, v29

    #@44a
    sub-int v6, v115, v29

    #@44c
    add-int/lit8 v6, v6, 0x1

    #@44e
    const-class v14, Landroid/text/style/MetricAffectingSpan;

    #@450
    move-object/from16 v0, v137

    #@452
    invoke-interface {v0, v5, v6, v14}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    #@455
    move-result-object v138

    #@456
    check-cast v138, [Landroid/text/style/MetricAffectingSpan;

    #@458
    .line 410
    .local v138, spans:[Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v138

    #@45a
    array-length v5, v0

    #@45b
    if-nez v5, :cond_461

    #@45d
    .line 411
    move-object/from16 v141, p4

    #@45f
    .restart local v141       #whichPaint:Landroid/text/TextPaint;
    goto/16 :goto_3db

    #@461
    .line 413
    .end local v141           #whichPaint:Landroid/text/TextPaint;
    :cond_461
    move-object/from16 v0, p0

    #@463
    iget-object v0, v0, Landroid/text/Layout;->mWorkPaint:Landroid/text/TextPaint;

    #@465
    move-object/from16 v141, v0

    #@467
    .line 414
    .restart local v141       #whichPaint:Landroid/text/TextPaint;
    move-object/from16 v0, v141

    #@469
    move-object/from16 v1, p4

    #@46b
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    #@46e
    .line 415
    move-object/from16 v93, v138

    #@470
    .local v93, arr$:[Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v93

    #@472
    array-length v0, v0

    #@473
    move/from16 v117, v0

    #@475
    .local v117, len$:I
    const/16 v111, 0x0

    #@477
    .local v111, i$:I
    :goto_477
    move/from16 v0, v111

    #@479
    move/from16 v1, v117

    #@47b
    if-ge v0, v1, :cond_3db

    #@47d
    aget-object v133, v93, v111

    #@47f
    .line 416
    .local v133, span:Landroid/text/style/MetricAffectingSpan;
    move-object/from16 v0, v133

    #@481
    move-object/from16 v1, v141

    #@483
    invoke-virtual {v0, v1}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    #@486
    .line 415
    add-int/lit8 v111, v111, 0x1

    #@488
    goto :goto_477

    #@489
    .line 429
    .end local v93           #arr$:[Landroid/text/style/MetricAffectingSpan;
    .end local v94           #bm:Landroid/graphics/Bitmap;
    .end local v111           #i$:I
    .end local v117           #len$:I
    .end local v133           #span:Landroid/text/style/MetricAffectingSpan;
    .end local v138           #spans:[Landroid/text/style/MetricAffectingSpan;
    .end local v141           #whichPaint:Landroid/text/TextPaint;
    :cond_489
    const/16 v5, 0x8

    #@48b
    move/from16 v0, v99

    #@48d
    if-ne v0, v5, :cond_3fd

    #@48f
    .line 430
    const/16 v116, 0x1

    #@491
    .local v116, k:I
    :goto_491
    const/4 v5, 0x4

    #@492
    move/from16 v0, v116

    #@494
    if-ge v0, v5, :cond_3fd

    #@496
    .line 431
    add-int v5, v115, v116

    #@498
    sub-int v5, v5, v29

    #@49a
    const/4 v6, 0x0

    #@49b
    aput v6, v28, v5

    #@49d
    .line 430
    add-int/lit8 v116, v116, 0x1

    #@49f
    goto :goto_491

    #@4a0
    .line 443
    .end local v116           #k:I
    :cond_4a0
    const/16 v5, 0x8

    #@4a2
    move/from16 v0, v99

    #@4a4
    if-ne v0, v5, :cond_193

    #@4a6
    .line 444
    add-int/lit8 v115, v115, 0x3

    #@4a8
    goto/16 :goto_193

    #@4aa
    .line 447
    .end local v142           #wid:F
    :cond_4aa
    sub-int v5, v115, v29

    #@4ac
    aget v5, v28, v5

    #@4ae
    add-float v62, v62, v5

    #@4b0
    goto/16 :goto_193

    #@4b2
    .line 450
    .end local v98           #emoji:I
    .end local v99           #emojiBytes:I
    :cond_4b2
    const v5, 0xd800

    #@4b5
    move/from16 v0, v95

    #@4b7
    if-lt v0, v5, :cond_519

    #@4b9
    const v5, 0xdfff

    #@4bc
    move/from16 v0, v95

    #@4be
    if-gt v0, v5, :cond_519

    #@4c0
    add-int/lit8 v5, v115, 0x1

    #@4c2
    move/from16 v0, v134

    #@4c4
    if-ge v5, v0, :cond_519

    #@4c6
    .line 452
    sub-int v5, v115, v29

    #@4c8
    move-object/from16 v0, v27

    #@4ca
    invoke-static {v0, v5}, Ljava/lang/Character;->codePointAt([CI)I

    #@4cd
    move-result v98

    #@4ce
    .line 454
    .restart local v98       #emoji:I
    sget v5, Landroid/text/StaticLayout;->MIN_EMOJI:I

    #@4d0
    move/from16 v0, v98

    #@4d2
    if-lt v0, v5, :cond_511

    #@4d4
    sget v5, Landroid/text/StaticLayout;->MAX_EMOJI:I

    #@4d6
    move/from16 v0, v98

    #@4d8
    if-gt v0, v5, :cond_511

    #@4da
    .line 455
    sget-object v5, Landroid/text/StaticLayout;->EMOJI_FACTORY:Landroid/emoji/EmojiFactory;

    #@4dc
    move/from16 v0, v98

    #@4de
    invoke-virtual {v5, v0}, Landroid/emoji/EmojiFactory;->getBitmapFromAndroidPua(I)Landroid/graphics/Bitmap;

    #@4e1
    move-result-object v94

    #@4e2
    .line 457
    .restart local v94       #bm:Landroid/graphics/Bitmap;
    if-eqz v94, :cond_509

    #@4e4
    .line 460
    if-nez v137, :cond_502

    #@4e6
    .line 461
    move-object/from16 v141, p4

    #@4e8
    .line 466
    .local v141, whichPaint:Landroid/graphics/Paint;
    :goto_4e8
    invoke-virtual/range {v94 .. v94}, Landroid/graphics/Bitmap;->getWidth()I

    #@4eb
    move-result v5

    #@4ec
    int-to-float v5, v5

    #@4ed
    invoke-virtual/range {v141 .. v141}, Landroid/text/TextPaint;->ascent()F

    #@4f0
    move-result v6

    #@4f1
    neg-float v6, v6

    #@4f2
    mul-float/2addr v5, v6

    #@4f3
    invoke-virtual/range {v94 .. v94}, Landroid/graphics/Bitmap;->getHeight()I

    #@4f6
    move-result v6

    #@4f7
    int-to-float v6, v6

    #@4f8
    div-float v142, v5, v6

    #@4fa
    .line 468
    .restart local v142       #wid:F
    add-float v62, v62, v142

    #@4fc
    .line 469
    const/16 v19, 0x1

    #@4fe
    .line 470
    add-int/lit8 v115, v115, 0x1

    #@500
    .line 471
    goto/16 :goto_193

    #@502
    .line 463
    .end local v141           #whichPaint:Landroid/graphics/Paint;
    .end local v142           #wid:F
    :cond_502
    move-object/from16 v0, p0

    #@504
    iget-object v0, v0, Landroid/text/Layout;->mWorkPaint:Landroid/text/TextPaint;

    #@506
    move-object/from16 v141, v0

    #@508
    .restart local v141       #whichPaint:Landroid/graphics/Paint;
    goto :goto_4e8

    #@509
    .line 472
    .end local v141           #whichPaint:Landroid/graphics/Paint;
    :cond_509
    sub-int v5, v115, v29

    #@50b
    aget v5, v28, v5

    #@50d
    add-float v62, v62, v5

    #@50f
    goto/16 :goto_193

    #@511
    .line 475
    .end local v94           #bm:Landroid/graphics/Bitmap;
    :cond_511
    sub-int v5, v115, v29

    #@513
    aget v5, v28, v5

    #@515
    add-float v62, v62, v5

    #@517
    goto/16 :goto_193

    #@519
    .line 478
    .end local v98           #emoji:I
    :cond_519
    sub-int v5, v115, v29

    #@51b
    aget v5, v28, v5

    #@51d
    add-float v62, v62, v5

    #@51f
    goto/16 :goto_193

    #@521
    .line 481
    :cond_521
    const/16 v114, 0x0

    #@523
    goto/16 :goto_1a1

    #@525
    .line 497
    .restart local v114       #isSpaceOrTab:Z
    :cond_525
    const/16 v113, 0x0

    #@527
    goto/16 :goto_214

    #@529
    .line 522
    :cond_529
    move/from16 v0, v115

    #@52b
    move/from16 v1, v134

    #@52d
    if-ge v0, v1, :cond_587

    #@52f
    const/16 v34, 0x1

    #@531
    .line 527
    .local v34, moreChars:Z
    :goto_531
    move/from16 v0, v125

    #@533
    if-eq v0, v7, :cond_58a

    #@535
    .line 528
    move/from16 v8, v125

    #@537
    .line 529
    .local v8, endPos:I
    move/from16 v9, v126

    #@539
    .line 530
    .local v9, above:I
    move/from16 v10, v128

    #@53b
    .line 531
    .local v10, below:I
    move/from16 v11, v129

    #@53d
    .line 532
    .local v11, top:I
    move/from16 v12, v127

    #@53f
    .line 533
    .local v12, bottom:I
    move/from16 v32, v130

    #@541
    .local v32, currentTextWidth:F
    :goto_541
    move-object/from16 v5, p0

    #@543
    move-object/from16 v6, p1

    #@545
    move/from16 v14, p7

    #@547
    move/from16 v15, p8

    #@549
    move/from16 v24, p3

    #@54b
    move/from16 v25, p9

    #@54d
    move/from16 v26, p10

    #@54f
    move-object/from16 v30, p12

    #@551
    move/from16 v31, p11

    #@553
    move-object/from16 v33, p4

    #@555
    .line 550
    invoke-direct/range {v5 .. v34}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZ[BIZIZZ[C[FILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I

    #@558
    move-result v13

    #@559
    .line 557
    move v7, v8

    #@55a
    .line 558
    add-int/lit8 v115, v7, -0x1

    #@55c
    .line 559
    move/from16 v102, v7

    #@55e
    move/from16 v125, v7

    #@560
    .line 560
    const/16 v62, 0x0

    #@562
    .line 561
    const/16 v42, 0x0

    #@564
    move/from16 v41, v42

    #@566
    move/from16 v40, v42

    #@568
    move/from16 v39, v42

    #@56a
    .line 562
    const/16 v127, 0x0

    #@56c
    move/from16 v129, v127

    #@56e
    move/from16 v128, v127

    #@570
    move/from16 v126, v127

    #@572
    .line 564
    add-int/lit8 v101, v101, -0x1

    #@574
    if-gtz v101, :cond_578

    #@576
    .line 565
    move/from16 v143, v131

    #@578
    .line 568
    :cond_578
    move/from16 v0, v136

    #@57a
    if-ge v7, v0, :cond_5b2

    #@57c
    .line 571
    move-object/from16 v0, v122

    #@57e
    invoke-virtual {v0, v7}, Landroid/text/MeasuredText;->setPos(I)V

    #@581
    .line 572
    move/from16 v134, v7

    #@583
    .line 283
    .end local v8           #endPos:I
    .end local v9           #above:I
    .end local v10           #below:I
    .end local v11           #top:I
    .end local v12           #bottom:I
    .end local v32           #currentTextWidth:F
    .end local v34           #moreChars:Z
    .end local v95           #c:C
    .end local v112           #isEmoji:Z
    .end local v114           #isSpaceOrTab:Z
    :cond_583
    :goto_583
    move/from16 v136, v134

    #@585
    goto/16 :goto_150

    #@587
    .line 522
    .restart local v95       #c:C
    .restart local v112       #isEmoji:Z
    .restart local v114       #isSpaceOrTab:Z
    :cond_587
    const/16 v34, 0x0

    #@589
    goto :goto_531

    #@58a
    .line 534
    .restart local v34       #moreChars:Z
    :cond_58a
    move/from16 v0, v102

    #@58c
    if-eq v0, v7, :cond_59b

    #@58e
    .line 535
    move/from16 v8, v102

    #@590
    .line 536
    .restart local v8       #endPos:I
    move/from16 v9, v39

    #@592
    .line 537
    .restart local v9       #above:I
    move/from16 v10, v40

    #@594
    .line 538
    .restart local v10       #below:I
    move/from16 v11, v41

    #@596
    .line 539
    .restart local v11       #top:I
    move/from16 v12, v42

    #@598
    .line 540
    .restart local v12       #bottom:I
    move/from16 v32, v103

    #@59a
    .restart local v32       #currentTextWidth:F
    goto :goto_541

    #@59b
    .line 542
    .end local v8           #endPos:I
    .end local v9           #above:I
    .end local v10           #below:I
    .end local v11           #top:I
    .end local v12           #bottom:I
    .end local v32           #currentTextWidth:F
    :cond_59b
    add-int/lit8 v8, v7, 0x1

    #@59d
    .line 543
    .restart local v8       #endPos:I
    move-object/from16 v0, v18

    #@59f
    iget v9, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@5a1
    .line 544
    .restart local v9       #above:I
    move-object/from16 v0, v18

    #@5a3
    iget v10, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@5a5
    .line 545
    .restart local v10       #below:I
    move-object/from16 v0, v18

    #@5a7
    iget v11, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@5a9
    .line 546
    .restart local v11       #top:I
    move-object/from16 v0, v18

    #@5ab
    iget v12, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@5ad
    .line 547
    .restart local v12       #bottom:I
    sub-int v5, v7, v29

    #@5af
    aget v32, v28, v5

    #@5b1
    .restart local v32       #currentTextWidth:F
    goto :goto_541

    #@5b2
    .line 576
    :cond_5b2
    move-object/from16 v0, p0

    #@5b4
    iget v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@5b6
    move-object/from16 v0, p0

    #@5b8
    iget v6, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@5ba
    if-lt v5, v6, :cond_23a

    #@5bc
    goto :goto_583

    #@5bd
    .line 583
    .end local v8           #endPos:I
    .end local v9           #above:I
    .end local v10           #below:I
    .end local v11           #top:I
    .end local v12           #bottom:I
    .end local v32           #currentTextWidth:F
    .end local v34           #moreChars:Z
    .end local v95           #c:C
    .end local v104           #fmAscent:I
    .end local v105           #fmBottom:I
    .end local v106           #fmDescent:I
    .end local v107           #fmTop:I
    .end local v112           #isEmoji:Z
    .end local v114           #isSpaceOrTab:Z
    .end local v115           #j:I
    .end local v134           #spanEnd:I
    .end local v135           #spanLen:I
    :cond_5bd
    move/from16 v0, v38

    #@5bf
    if-eq v0, v7, :cond_62c

    #@5c1
    move-object/from16 v0, p0

    #@5c3
    iget v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@5c5
    move-object/from16 v0, p0

    #@5c7
    iget v6, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@5c9
    if-ge v5, v6, :cond_62c

    #@5cb
    .line 584
    or-int v5, v41, v42

    #@5cd
    or-int v5, v5, v40

    #@5cf
    or-int v5, v5, v39

    #@5d1
    if-nez v5, :cond_5f2

    #@5d3
    .line 585
    move-object/from16 v0, p4

    #@5d5
    move-object/from16 v1, v18

    #@5d7
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@5da
    .line 587
    move-object/from16 v0, v18

    #@5dc
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@5de
    move/from16 v41, v0

    #@5e0
    .line 588
    move-object/from16 v0, v18

    #@5e2
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@5e4
    move/from16 v42, v0

    #@5e6
    .line 589
    move-object/from16 v0, v18

    #@5e8
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@5ea
    move/from16 v39, v0

    #@5ec
    .line 590
    move-object/from16 v0, v18

    #@5ee
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@5f0
    move/from16 v40, v0

    #@5f2
    .line 595
    :cond_5f2
    move/from16 v0, v38

    #@5f4
    move/from16 v1, p3

    #@5f6
    if-eq v0, v1, :cond_6eb

    #@5f8
    const/16 v64, 0x1

    #@5fa
    :goto_5fa
    move-object/from16 v35, p0

    #@5fc
    move-object/from16 v36, p1

    #@5fe
    move/from16 v37, v7

    #@600
    move/from16 v43, v13

    #@602
    move/from16 v44, p7

    #@604
    move/from16 v45, p8

    #@606
    move-object/from16 v46, v16

    #@608
    move-object/from16 v47, v17

    #@60a
    move-object/from16 v48, v18

    #@60c
    move/from16 v49, v19

    #@60e
    move/from16 v50, v20

    #@610
    move-object/from16 v51, v21

    #@612
    move/from16 v52, v22

    #@614
    move/from16 v53, v23

    #@616
    move/from16 v54, p3

    #@618
    move/from16 v55, p9

    #@61a
    move/from16 v56, p10

    #@61c
    move-object/from16 v57, v27

    #@61e
    move-object/from16 v58, v28

    #@620
    move/from16 v59, v29

    #@622
    move-object/from16 v60, p12

    #@624
    move/from16 v61, p11

    #@626
    move-object/from16 v63, p4

    #@628
    invoke-direct/range {v35 .. v64}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZ[BIZIZZ[C[FILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I

    #@62b
    move-result v13

    #@62c
    .line 607
    :cond_62c
    move/from16 v29, v38

    #@62e
    .line 609
    move/from16 v0, v38

    #@630
    move/from16 v1, p3

    #@632
    if-ne v0, v1, :cond_6ef

    #@634
    .line 613
    .end local v7           #here:I
    .end local v16           #chooseHt:[Landroid/text/style/LineHeightSpan;
    .end local v19           #hasTabOrEmoji:Z
    .end local v21           #chdirs:[B
    .end local v22           #dir:I
    .end local v23           #easy:Z
    .end local v27           #chs:[C
    .end local v28           #widths:[F
    .end local v38           #paraEnd:I
    .end local v39           #fitAscent:I
    .end local v40           #fitDescent:I
    .end local v41           #fitTop:I
    .end local v42           #fitBottom:I
    .end local v62           #w:F
    .end local v100           #firstWidth:I
    .end local v101           #firstWidthLineLimit:I
    .end local v102           #fit:I
    .end local v103           #fitWidth:F
    .end local v108           #hasEmoji:Z
    .end local v109           #hasTab:Z
    .end local v125           #ok:I
    .end local v126           #okAscent:I
    .end local v127           #okBottom:I
    .end local v128           #okDescent:I
    .end local v129           #okTop:I
    .end local v130           #okWidth:F
    .end local v131           #restWidth:I
    .end local v136           #spanStart:I
    .end local v139           #tabStops:Landroid/text/Layout$TabStops;
    .end local v143           #width:I
    :cond_634
    move/from16 v0, p3

    #@636
    move/from16 v1, p2

    #@638
    if-eq v0, v1, :cond_646

    #@63a
    add-int/lit8 v5, p3, -0x1

    #@63c
    move-object/from16 v0, p1

    #@63e
    invoke-interface {v0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    #@641
    move-result v5

    #@642
    const/16 v6, 0xa

    #@644
    if-ne v5, v6, :cond_6ea

    #@646
    :cond_646
    move-object/from16 v0, p0

    #@648
    iget v5, v0, Landroid/text/StaticLayout;->mLineCount:I

    #@64a
    move-object/from16 v0, p0

    #@64c
    iget v6, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@64e
    if-ge v5, v6, :cond_6ea

    #@650
    .line 617
    move-object/from16 v0, p4

    #@652
    move-object/from16 v1, v18

    #@654
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    #@657
    .line 620
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@65a
    move-result-object v5

    #@65b
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@65e
    move-result-object v97

    #@65f
    .line 621
    .local v97, currentLanguage:Ljava/lang/String;
    if-eqz v97, :cond_69c

    #@661
    invoke-virtual/range {v97 .. v97}, Ljava/lang/String;->length()I

    #@664
    move-result v5

    #@665
    const/4 v6, 0x2

    #@666
    if-lt v5, v6, :cond_69c

    #@668
    .line 622
    const/4 v5, 0x0

    #@669
    const/4 v6, 0x2

    #@66a
    move-object/from16 v0, v97

    #@66c
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@66f
    move-result-object v97

    #@670
    .line 623
    const-string v5, "ar"

    #@672
    move-object/from16 v0, v97

    #@674
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@677
    move-result v5

    #@678
    if-nez v5, :cond_69a

    #@67a
    const-string v5, "fa"

    #@67c
    move-object/from16 v0, v97

    #@67e
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@681
    move-result v5

    #@682
    if-nez v5, :cond_69a

    #@684
    const-string/jumbo v5, "iw"

    #@687
    move-object/from16 v0, v97

    #@689
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68c
    move-result v5

    #@68d
    if-nez v5, :cond_69a

    #@68f
    const-string/jumbo v5, "ku"

    #@692
    move-object/from16 v0, v97

    #@694
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@697
    move-result v5

    #@698
    if-eqz v5, :cond_69c

    #@69a
    .line 625
    :cond_69a
    const/16 v80, -0x1

    #@69c
    .line 629
    :cond_69c
    move-object/from16 v0, v18

    #@69e
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    #@6a0
    move/from16 v67, v0

    #@6a2
    move-object/from16 v0, v18

    #@6a4
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    #@6a6
    move/from16 v68, v0

    #@6a8
    move-object/from16 v0, v18

    #@6aa
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    #@6ac
    move/from16 v69, v0

    #@6ae
    move-object/from16 v0, v18

    #@6b0
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    #@6b2
    move/from16 v70, v0

    #@6b4
    const/16 v74, 0x0

    #@6b6
    const/16 v75, 0x0

    #@6b8
    const/16 v77, 0x0

    #@6ba
    const/16 v79, 0x0

    #@6bc
    const/16 v81, 0x1

    #@6be
    const/16 v85, 0x0

    #@6c0
    const/16 v86, 0x0

    #@6c2
    const/16 v90, 0x0

    #@6c4
    const/16 v92, 0x0

    #@6c6
    move-object/from16 v63, p0

    #@6c8
    move-object/from16 v64, p1

    #@6ca
    move/from16 v65, p3

    #@6cc
    move/from16 v66, p3

    #@6ce
    move/from16 v71, v13

    #@6d0
    move/from16 v72, p7

    #@6d2
    move/from16 v73, p8

    #@6d4
    move-object/from16 v76, v18

    #@6d6
    move/from16 v78, v20

    #@6d8
    move/from16 v82, p3

    #@6da
    move/from16 v83, p9

    #@6dc
    move/from16 v84, p10

    #@6de
    move/from16 v87, p2

    #@6e0
    move-object/from16 v88, p12

    #@6e2
    move/from16 v89, p11

    #@6e4
    move-object/from16 v91, p4

    #@6e6
    invoke-direct/range {v63 .. v92}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZZ[BIZIZZ[C[FILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I

    #@6e9
    move-result v13

    #@6ea
    .line 640
    .end local v97           #currentLanguage:Ljava/lang/String;
    :cond_6ea
    return-void

    #@6eb
    .line 595
    .restart local v7       #here:I
    .restart local v16       #chooseHt:[Landroid/text/style/LineHeightSpan;
    .restart local v19       #hasTabOrEmoji:Z
    .restart local v21       #chdirs:[B
    .restart local v22       #dir:I
    .restart local v23       #easy:Z
    .restart local v27       #chs:[C
    .restart local v28       #widths:[F
    .restart local v38       #paraEnd:I
    .restart local v39       #fitAscent:I
    .restart local v40       #fitDescent:I
    .restart local v41       #fitTop:I
    .restart local v42       #fitBottom:I
    .restart local v62       #w:F
    .restart local v100       #firstWidth:I
    .restart local v101       #firstWidthLineLimit:I
    .restart local v102       #fit:I
    .restart local v103       #fitWidth:F
    .restart local v108       #hasEmoji:Z
    .restart local v109       #hasTab:Z
    .restart local v125       #ok:I
    .restart local v126       #okAscent:I
    .restart local v127       #okBottom:I
    .restart local v128       #okDescent:I
    .restart local v129       #okTop:I
    .restart local v130       #okWidth:F
    .restart local v131       #restWidth:I
    .restart local v136       #spanStart:I
    .restart local v139       #tabStops:Landroid/text/Layout$TabStops;
    .restart local v143       #width:I
    :cond_6eb
    const/16 v64, 0x0

    #@6ed
    goto/16 :goto_5fa

    #@6ef
    .line 194
    :cond_6ef
    move/from16 v29, v38

    #@6f1
    goto/16 :goto_31
.end method

.method public getBottomPadding()I
    .registers 2

    #@0
    .prologue
    .line 1043
    iget v0, p0, Landroid/text/StaticLayout;->mBottomPadding:I

    #@2
    return v0
.end method

.method public getEllipsisCount(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1048
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ge v0, v1, :cond_7

    #@5
    .line 1049
    const/4 v0, 0x0

    #@6
    .line 1052
    :goto_6
    return v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@9
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@b
    mul-int/2addr v1, p1

    #@c
    add-int/lit8 v1, v1, 0x4

    #@e
    aget v0, v0, v1

    #@10
    goto :goto_6
.end method

.method public getEllipsisStart(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1057
    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    #@2
    const/4 v1, 0x5

    #@3
    if-ge v0, v1, :cond_7

    #@5
    .line 1058
    const/4 v0, 0x0

    #@6
    .line 1061
    :goto_6
    return v0

    #@7
    :cond_7
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@9
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@b
    mul-int/2addr v1, p1

    #@c
    add-int/lit8 v1, v1, 0x3

    #@e
    aget v0, v0, v1

    #@10
    goto :goto_6
.end method

.method public getEllipsizedWidth()I
    .registers 2

    #@0
    .prologue
    .line 1066
    iget v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    #@2
    return v0
.end method

.method public getLineContainsTab(I)Z
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1028
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@2
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@4
    mul-int/2addr v1, p1

    #@5
    add-int/lit8 v1, v1, 0x0

    #@7
    aget v0, v0, v1

    #@9
    const/high16 v1, 0x2000

    #@b
    and-int/2addr v0, v1

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public getLineCount()I
    .registers 2

    #@0
    .prologue
    .line 993
    iget v0, p0, Landroid/text/StaticLayout;->mLineCount:I

    #@2
    return v0
.end method

.method public getLineDescent(I)I
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 1008
    iget-object v1, p0, Landroid/text/StaticLayout;->mLines:[I

    #@2
    iget v2, p0, Landroid/text/StaticLayout;->mColumns:I

    #@4
    mul-int/2addr v2, p1

    #@5
    add-int/lit8 v2, v2, 0x2

    #@7
    aget v0, v1, v2

    #@9
    .line 1009
    .local v0, descent:I
    iget v1, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@b
    if-lez v1, :cond_1c

    #@d
    iget v1, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@f
    add-int/lit8 v1, v1, -0x1

    #@11
    if-lt p1, v1, :cond_1c

    #@13
    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    #@15
    if-eq p1, v1, :cond_1c

    #@17
    .line 1011
    invoke-virtual {p0}, Landroid/text/StaticLayout;->getBottomPadding()I

    #@1a
    move-result v1

    #@1b
    add-int/2addr v0, v1

    #@1c
    .line 1013
    :cond_1c
    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .registers 3
    .parameter "line"

    #@0
    .prologue
    .line 1033
    iget-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public getLineForVertical(I)I
    .registers 8
    .parameter "vertical"

    #@0
    .prologue
    .line 972
    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    #@2
    .line 973
    .local v1, high:I
    const/4 v3, -0x1

    #@3
    .line 975
    .local v3, low:I
    iget-object v2, p0, Landroid/text/StaticLayout;->mLines:[I

    #@5
    .line 976
    .local v2, lines:[I
    :goto_5
    sub-int v4, v1, v3

    #@7
    const/4 v5, 0x1

    #@8
    if-le v4, v5, :cond_1b

    #@a
    .line 977
    add-int v4, v1, v3

    #@c
    shr-int/lit8 v0, v4, 0x1

    #@e
    .line 978
    .local v0, guess:I
    iget v4, p0, Landroid/text/StaticLayout;->mColumns:I

    #@10
    mul-int/2addr v4, v0

    #@11
    add-int/lit8 v4, v4, 0x1

    #@13
    aget v4, v2, v4

    #@15
    if-le v4, p1, :cond_19

    #@17
    .line 979
    move v1, v0

    #@18
    goto :goto_5

    #@19
    .line 981
    :cond_19
    move v3, v0

    #@1a
    goto :goto_5

    #@1b
    .line 984
    .end local v0           #guess:I
    :cond_1b
    if-gez v3, :cond_1e

    #@1d
    .line 985
    const/4 v3, 0x0

    #@1e
    .line 987
    .end local v3           #low:I
    :cond_1e
    return v3
.end method

.method public getLineStart(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1018
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@2
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@4
    mul-int/2addr v1, p1

    #@5
    add-int/lit8 v1, v1, 0x0

    #@7
    aget v0, v0, v1

    #@9
    const v1, 0x1fffffff

    #@c
    and-int/2addr v0, v1

    #@d
    return v0
.end method

.method public getLineTop(I)I
    .registers 5
    .parameter "line"

    #@0
    .prologue
    .line 998
    iget-object v1, p0, Landroid/text/StaticLayout;->mLines:[I

    #@2
    iget v2, p0, Landroid/text/StaticLayout;->mColumns:I

    #@4
    mul-int/2addr v2, p1

    #@5
    add-int/lit8 v2, v2, 0x1

    #@7
    aget v0, v1, v2

    #@9
    .line 999
    .local v0, top:I
    iget v1, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@b
    if-lez v1, :cond_1a

    #@d
    iget v1, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    #@f
    if-lt p1, v1, :cond_1a

    #@11
    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    #@13
    if-eq p1, v1, :cond_1a

    #@15
    .line 1001
    invoke-virtual {p0}, Landroid/text/StaticLayout;->getBottomPadding()I

    #@18
    move-result v1

    #@19
    add-int/2addr v0, v1

    #@1a
    .line 1003
    :cond_1a
    return v0
.end method

.method public getParagraphDirection(I)I
    .registers 4
    .parameter "line"

    #@0
    .prologue
    .line 1023
    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    #@2
    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    #@4
    mul-int/2addr v1, p1

    #@5
    add-int/lit8 v1, v1, 0x0

    #@7
    aget v0, v0, v1

    #@9
    shr-int/lit8 v0, v0, 0x1e

    #@b
    return v0
.end method

.method public getTopPadding()I
    .registers 2

    #@0
    .prologue
    .line 1038
    iget v0, p0, Landroid/text/StaticLayout;->mTopPadding:I

    #@2
    return v0
.end method

.method prepare()V
    .registers 2

    #@0
    .prologue
    .line 1070
    invoke-static {}, Landroid/text/MeasuredText;->obtain()Landroid/text/MeasuredText;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Landroid/text/StaticLayout;->mMeasured:Landroid/text/MeasuredText;

    #@6
    .line 1071
    return-void
.end method
