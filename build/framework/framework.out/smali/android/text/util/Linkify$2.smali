.class final Landroid/text/util/Linkify$2;
.super Ljava/lang/Object;
.source "Linkify.java"

# interfaces
.implements Landroid/text/util/Linkify$MatchFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/util/Linkify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public final acceptMatch(Ljava/lang/CharSequence;II)Z
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "end"

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    .line 123
    .local v0, digitCount:I
    move v1, p2

    #@2
    .local v1, i:I
    :goto_2
    if-ge v1, p3, :cond_18

    #@4
    .line 124
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    #@7
    move-result v2

    #@8
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_15

    #@e
    .line 125
    add-int/lit8 v0, v0, 0x1

    #@10
    .line 126
    const/4 v2, 0x5

    #@11
    if-lt v0, v2, :cond_15

    #@13
    .line 127
    const/4 v2, 0x1

    #@14
    .line 131
    :goto_14
    return v2

    #@15
    .line 123
    :cond_15
    add-int/lit8 v1, v1, 0x1

    #@17
    goto :goto_2

    #@18
    .line 131
    :cond_18
    const/4 v2, 0x0

    #@19
    goto :goto_14
.end method
